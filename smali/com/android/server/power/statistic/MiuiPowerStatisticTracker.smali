.class public Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;
.super Ljava/lang/Object;
.source "MiuiPowerStatisticTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;,
        Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;
    }
.end annotation


# static fields
.field private static final ALL_SCREEN_ON:Ljava/lang/String; = "all_screen_on_time"

.field public static final AON_EVENT_NAME:Ljava/lang/String; = "aon_statistic"

.field private static final APP_ID:Ljava/lang/String; = "31000401594"

.field private static final ARRAY_CAPACITY:I = 0xa

.field private static final BLOCK_AVG_TIME:Ljava/lang/String; = "block_avg_time"

.field private static final DEBUG:Z

.field private static final FAILURE_MILLIS:Ljava/lang/String; = "failure_millis"

.field private static final FAILURE_REASONS:Ljava/lang/String; = "failure_reasons"

.field private static final FAILURE_TIMES:Ljava/lang/String; = "failure_times"

.field private static final FEATURE_AON_PROXIMITY_SUPPORT:Ljava/lang/String; = "config_aon_proximity_available"

.field private static final KEY_AON_SCREEN_OFF_ENABLE:Ljava/lang/String; = "aon_screen_off_enable"

.field private static final KEY_AON_SCREEN_ON_ENABLE:Ljava/lang/String; = "aon_screen_on_enable"

.field private static final KEY_SCREEN_OFF_CHECK_ATTENTION_MILLIS:Ljava/lang/String; = "screen_off_check_attention_millis"

.field private static final KEY_SCREEN_OFF_HAS_FACE_COUNT:Ljava/lang/String; = "screen_off_has_face_count"

.field private static final KEY_SCREEN_ON_CHECK_ATTENTION_MILLIS:Ljava/lang/String; = "screen_on_check_attention_millis"

.field private static final KEY_SCREEN_ON_HAS_FACE_COUNT:Ljava/lang/String; = "screen_on_has_face_count"

.field private static final KEY_SCREEN_ON_NO_FACE_COUNT:Ljava/lang/String; = "screen_on_no_face_count"

.field private static final MSG_CONNECTED_STATE_CHANGED:I = 0x8

.field private static final MSG_DIM_STATE_CHANGE:I = 0xb

.field private static final MSG_DUMP:I = 0xa

.field private static final MSG_FOREGROUND_APP_CHANGED:I = 0x9

.field private static final MSG_POWER_STATE_CHANGE:I = 0x4

.field private static final MSG_TOF_GESTURE_CHANGE:I = 0x6

.field private static final MSG_TOF_PROXIMITY_CHANGE:I = 0x7

.field private static final MSG_UNBLOCK_SCREEN:I = 0x3

.field private static final MSG_USER_ATTENTION_CHANGE:I = 0x5

.field private static final MSG_WAKEFULNESS_CHANGE:I = 0x1

.field private static final MSG_WAKEFULNESS_CHANGE_COMPLETE:I = 0x2

.field private static final OFF_AVG_TIME:Ljava/lang/String; = "off_avg_time"

.field private static final OFF_REASON:Ljava/lang/String; = "off_reason"

.field private static final ON_AVG_TIME:Ljava/lang/String; = "on_avg_time"

.field private static final ON_REASON:Ljava/lang/String; = "on_reason"

.field public static final POWER_EVENT_NAME:Ljava/lang/String; = "power_on_off_statistic"

.field private static final STATE_OFF_AVG_TIME:Ljava/lang/String; = "state_off_avg_time"

.field private static final STATE_ON_AVG_TIME:Ljava/lang/String; = "state_on_avg_time"

.field private static final SUCCESS_MILLIS:Ljava/lang/String; = "success_millis"

.field private static final SUCCESS_TIMES:Ljava/lang/String; = "success_times"

.field public static final TAG:Ljava/lang/String; = "MiuiPowerStatisticTracker"

.field private static volatile sInstance:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;


# instance fields
.field private final DEBUG_REPORT_TIME_DURATION:J

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mAllScreenOnTime:J

.field private mAonProximitySupport:Z

.field private mAonScreenOffEnabled:Z

.field private mAonScreenOnEnabled:Z

.field private mBackgroundHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mDimEventStatistic:Lcom/android/server/power/statistic/DimEventStatistic;

.field private mDisplayPortEventStatistic:Lcom/android/server/power/statistic/DisplayPortEventStatistic;

.field private mFailureMillis:J

.field private final mFailureReasons:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFailureTimes:I

.field private mLastWakeTime:J

.field private final mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

.field private mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

.field private mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

.field private mPowerStateOffAvgTime:F

.field private mPowerStateOnAvgTime:F

.field private mScreenOffAvgTime:F

.field private mScreenOffCheckAttentionMillis:J

.field private mScreenOffHasFaceCount:I

.field private mScreenOffLatencyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenOffReasons:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenOnAvgTime:F

.field private mScreenOnBlockerAvgTime:F

.field private mScreenOnCheckAttentionMillis:J

.field private mScreenOnHasFaceCount:I

.field private mScreenOnLatencyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenOnNoFaceCount:I

.field private mScreenOnReasons:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mSuccessMillis:J

.field private mSuccessTimes:I

.field private mSupportAdaptiveSleep:Z

.field private mTofEventStatistic:Lcom/android/server/power/statistic/TofEventStatistic;

.field private mWakefulness:I

.field private mWakefulnessChanging:Z


# direct methods
.method public static synthetic $r8$lambda$GCnAKYsx61UV-3tCeELeqeVRH_k(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->lambda$new$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$qSU8ooZ6RdP3o_XIy0wTWsZ2khI(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;ZJZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->lambda$notifyAonScreenOnOffEvent$1(ZJZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmDimEventStatistic(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)Lcom/android/server/power/statistic/DimEventStatistic;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mDimEventStatistic:Lcom/android/server/power/statistic/DimEventStatistic;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayPortEventStatistic(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)Lcom/android/server/power/statistic/DisplayPortEventStatistic;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mDisplayPortEventStatistic:Lcom/android/server/power/statistic/DisplayPortEventStatistic;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTofEventStatistic(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)Lcom/android/server/power/statistic/TofEventStatistic;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mTofEventStatistic:Lcom/android/server/power/statistic/TofEventStatistic;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleScreenOnUnBlocker(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleScreenOnUnBlocker(II)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 40
    nop

    .line 41
    const-string v0, "debug.miui.power.statistic.dbg"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    sput-boolean v1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG_REPORT_TIME_DURATION:J

    .line 82
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnReasons:Landroid/util/SparseArray;

    .line 83
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffReasons:Landroid/util/SparseArray;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnLatencyList:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffLatencyList:Ljava/util/ArrayList;

    .line 95
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulness:I

    .line 109
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureReasons:Landroid/util/SparseArray;

    .line 141
    new-instance v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-direct {v0, p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    .line 178
    new-instance v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    return-void
.end method

.method private collectLatencyInfo(Ljava/util/ArrayList;Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V
    .locals 2
    .param p2, "powerEvent"    # Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;",
            ">;",
            "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;",
            ")V"
        }
    .end annotation

    .line 371
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    .line 372
    invoke-direct {p0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->recalculateLatencyInfo(Ljava/util/ArrayList;)V

    .line 375
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    new-instance v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-direct {v0, p0, p2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    :cond_1
    return-void
.end method

.method public static getInstance()Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;
    .locals 2

    .line 150
    sget-object v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->sInstance:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-nez v0, :cond_1

    .line 151
    const-class v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    monitor-enter v0

    .line 152
    :try_start_0
    sget-object v1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->sInstance:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-nez v1, :cond_0

    .line 153
    new-instance v1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-direct {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;-><init>()V

    sput-object v1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->sInstance:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    .line 155
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 157
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->sInstance:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    return-object v0
.end method

.method private handleScreenOnUnBlocker(II)V
    .locals 2
    .param p1, "screenOnBlockTime"    # I
    .param p2, "delayMiles"    # I

    .line 240
    iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 244
    :cond_0
    sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleScreenOnUnBlocker: screenOnBlockStartRealTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", delayMiles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPowerStatisticTracker"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    if-lez p2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setDelayByFace(Z)V

    .line 251
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setBlockedScreenLatency(I)V

    .line 252
    return-void

    .line 241
    :cond_3
    :goto_1
    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 2

    .line 180
    sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 181
    const-string v0, "MiuiPowerStatisticTracker"

    const-string v1, "It\'s time to report"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_0
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->reportScheduleEvent()V

    .line 184
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->setReportScheduleEventAlarm()V

    .line 185
    return-void
.end method

.method private synthetic lambda$notifyAonScreenOnOffEvent$1(ZJZ)V
    .locals 2
    .param p1, "isScreenOn"    # Z
    .param p2, "checkTime"    # J
    .param p4, "hasFace"    # Z

    .line 496
    if-eqz p1, :cond_1

    .line 497
    iget-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnCheckAttentionMillis:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnCheckAttentionMillis:J

    .line 498
    if-eqz p4, :cond_0

    .line 499
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnHasFaceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnHasFaceCount:I

    goto :goto_0

    .line 501
    :cond_0
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnNoFaceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnNoFaceCount:I

    goto :goto_0

    .line 504
    :cond_1
    iget-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffCheckAttentionMillis:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffCheckAttentionMillis:J

    .line 505
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffHasFaceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffHasFaceCount:I

    .line 507
    :goto_0
    return-void
.end method

.method private recalculateLatencyInfo(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;",
            ">;)V"
        }
    .end annotation

    .line 381
    .local p1, "latencyInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    return-void

    .line 385
    :cond_0
    const/4 v0, 0x0

    .line 386
    .local v0, "sumWakefulnessChangedLatency":F
    const/4 v1, 0x0

    .line 387
    .local v1, "sumBlockedScreenLatency":F
    const/4 v2, 0x0

    .line 389
    .local v2, "sumDisplayStateLatency":F
    const/4 v3, 0x0

    .line 390
    .local v3, "wakefulnessChangedSize":I
    const/4 v4, 0x0

    .line 391
    .local v4, "blockedScreenSize":I
    const/4 v5, 0x0

    .line 393
    .local v5, "DisplayStateChangedSize":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    .line 394
    .local v7, "powerEvent":Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;
    invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulnessChangedLatency()F

    move-result v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_1

    .line 395
    invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulnessChangedLatency()F

    move-result v8

    add-float/2addr v0, v8

    .line 396
    add-int/lit8 v3, v3, 0x1

    .line 399
    :cond_1
    invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getBlockedScreenLatency()F

    move-result v8

    cmpl-float v8, v8, v9

    if-lez v8, :cond_2

    .line 400
    invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getBlockedScreenLatency()F

    move-result v8

    add-float/2addr v1, v8

    .line 401
    add-int/lit8 v4, v4, 0x1

    .line 404
    :cond_2
    invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getDisplayStateChangedLatency()I

    move-result v8

    if-lez v8, :cond_3

    .line 405
    invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getDisplayStateChangedLatency()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v2, v8

    .line 406
    add-int/lit8 v5, v5, 0x1

    .line 408
    .end local v7    # "powerEvent":Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;
    :cond_3
    goto :goto_0

    .line 410
    :cond_4
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOn()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 411
    iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F

    invoke-direct {p0, v6, v0, v3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F

    move-result v6

    iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F

    .line 413
    iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F

    invoke-direct {p0, v6, v1, v4}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F

    move-result v6

    iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F

    .line 415
    iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F

    invoke-direct {p0, v6, v2, v5}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F

    move-result v6

    iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F

    goto :goto_1

    .line 417
    :cond_5
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v6}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOff()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 418
    iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F

    invoke-direct {p0, v6, v0, v3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F

    move-result v6

    iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F

    .line 420
    iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F

    invoke-direct {p0, v6, v2, v5}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F

    move-result v6

    iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F

    .line 424
    :cond_6
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 425
    return-void
.end method

.method private reportAonScreenOnOffEventByIntent()V
    .locals 5

    .line 452
    sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_ON_SUPPORTED:Z

    if-nez v0, :cond_0

    .line 453
    return-void

    .line 456
    :cond_0
    sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_ON_SUPPORTED:Z

    const/4 v1, 0x1

    const/4 v2, -0x2

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mContext:Landroid/content/Context;

    .line 457
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "miui_people_near_screen_on"

    invoke-static {v0, v4, v3, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v3

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOnEnabled:Z

    .line 459
    sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mContext:Landroid/content/Context;

    .line 460
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "gaze_lock_screen_setting"

    invoke-static {v0, v4, v3, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v3

    :goto_1
    iput-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOffEnabled:Z

    .line 462
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mContext:Landroid/content/Context;

    .line 463
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 462
    const-string v2, "31000401594"

    const-string v3, "aon_statistic"

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 464
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "aon_screen_on_enable"

    iget-boolean v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOnEnabled:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOffEnabled:Z

    .line 465
    const-string v3, "aon_screen_off_enable"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 466
    iget-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOffEnabled:Z

    if-eqz v1, :cond_3

    .line 467
    const-string v1, "screen_on_no_face_count"

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnNoFaceCount:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnHasFaceCount:I

    .line 468
    const-string v3, "screen_on_has_face_count"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnCheckAttentionMillis:J

    .line 469
    const-string v4, "screen_on_check_attention_millis"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 471
    :cond_3
    iget-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOnEnabled:Z

    if-eqz v1, :cond_4

    .line 472
    const-string v1, "screen_off_has_face_count"

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffHasFaceCount:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffCheckAttentionMillis:J

    .line 473
    const-string v4, "screen_off_check_attention_millis"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 475
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reportAonScreenOnOffEventByIntent: intent:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPowerStatisticTracker"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    goto :goto_2

    .line 478
    :catch_0
    move-exception v1

    .line 479
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportAttentionEvent fail! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->resetAonScreenOnOffData()V

    .line 482
    return-void
.end method

.method private reportAttentionEventByIntent()V
    .locals 5

    .line 594
    iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSupportAdaptiveSleep:Z

    if-nez v0, :cond_0

    .line 595
    return-void

    .line 598
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    const-string v1, "31000401594"

    const-string v2, "aon_statistic"

    iget-object v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mContext:Landroid/content/Context;

    .line 599
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 598
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 600
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "success_times"

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessTimes:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "failure_times"

    iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureTimes:I

    .line 601
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "success_millis"

    iget-wide v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessMillis:J

    .line 602
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "failure_millis"

    iget-wide v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureMillis:J

    .line 603
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "failure_reasons"

    iget-object v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureReasons:Landroid/util/SparseArray;

    .line 604
    invoke-virtual {v3}, Landroid/util/SparseArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 605
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 608
    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 606
    :catch_0
    move-exception v0

    .line 607
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reportAttentionEvent fail! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPowerStatisticTracker"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->resetAttentionData()V

    .line 610
    return-void
.end method

.method private reportScheduleEvent()V
    .locals 1

    .line 443
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->reportScreenOnOffEventByIntent()V

    .line 444
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->reportAttentionEventByIntent()V

    .line 445
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->reportAonScreenOnOffEventByIntent()V

    .line 446
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mTofEventStatistic:Lcom/android/server/power/statistic/TofEventStatistic;

    invoke-virtual {v0}, Lcom/android/server/power/statistic/TofEventStatistic;->reportTofEventByIntent()V

    .line 447
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mDisplayPortEventStatistic:Lcom/android/server/power/statistic/DisplayPortEventStatistic;

    invoke-virtual {v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->reportDisplayPortEventByIntent()V

    .line 448
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mDimEventStatistic:Lcom/android/server/power/statistic/DimEventStatistic;

    invoke-virtual {v0}, Lcom/android/server/power/statistic/DimEventStatistic;->reportDimStateByIntent()V

    .line 449
    return-void
.end method

.method private reportScreenOnOffEventByIntent()V
    .locals 5

    .line 543
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnLatencyList:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->recalculateLatencyInfo(Ljava/util/ArrayList;)V

    .line 544
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffLatencyList:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->recalculateLatencyInfo(Ljava/util/ArrayList;)V

    .line 547
    :try_start_0
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    const-string v1, "31000401594"

    const-string v2, "power_on_off_statistic"

    iget-object v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mContext:Landroid/content/Context;

    .line 548
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 547
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 550
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "on_reason"

    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnReasons:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "off_reason"

    iget-object v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffReasons:Landroid/util/SparseArray;

    .line 551
    invoke-virtual {v3}, Landroid/util/SparseArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "on_avg_time"

    iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F

    .line 552
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "off_avg_time"

    iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F

    .line 553
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "block_avg_time"

    iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F

    .line 554
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "state_on_avg_time"

    iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F

    .line 555
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "state_off_avg_time"

    iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F

    .line 556
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "all_screen_on_time"

    iget-wide v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J

    .line 557
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 559
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 560
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reportScreenOnOffEventByIntent fail! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPowerStatisticTracker"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->resetRecordInfo()V

    .line 565
    return-void
.end method

.method private resetAonScreenOnOffData()V
    .locals 3

    .line 485
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOnEnabled:Z

    .line 486
    iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOffEnabled:Z

    .line 487
    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnNoFaceCount:I

    .line 488
    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnHasFaceCount:I

    .line 489
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnCheckAttentionMillis:J

    .line 490
    iput-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffCheckAttentionMillis:J

    .line 491
    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffHasFaceCount:I

    .line 492
    return-void
.end method

.method private resetAttentionData()V
    .locals 2

    .line 613
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessTimes:I

    .line 614
    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureTimes:I

    .line 615
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessMillis:J

    .line 616
    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureMillis:J

    .line 617
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureReasons:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 618
    return-void
.end method

.method private resetAvgTime()V
    .locals 2

    .line 576
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F

    .line 577
    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F

    .line 578
    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F

    .line 579
    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F

    .line 580
    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F

    .line 581
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J

    .line 582
    return-void
.end method

.method private resetRecordInfo()V
    .locals 1

    .line 568
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnReasons:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 569
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffReasons:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 570
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnLatencyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 571
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffLatencyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 572
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->resetAvgTime()V

    .line 573
    return-void
.end method

.method private setReportScheduleEventAlarm()V
    .locals 14

    .line 428
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 429
    .local v0, "now":J
    sget-boolean v2, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z

    if-eqz v2, :cond_0

    const-wide/32 v3, 0x1d4c0

    goto :goto_0

    :cond_0
    const-wide/32 v3, 0x5265c00

    .line 430
    .local v3, "duration":J
    :goto_0
    add-long v12, v0, v3

    .line 431
    .local v12, "nextTime":J
    if-eqz v2, :cond_1

    .line 432
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setReportScheduleEventAlarm: next time: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 433
    invoke-static {v3, v4}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 432
    const-string v5, "MiuiPowerStatisticTracker"

    invoke-static {v5, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    :cond_1
    iget-object v5, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAlarmManager:Landroid/app/AlarmManager;

    if-eqz v5, :cond_2

    .line 437
    const/4 v6, 0x2

    const-string v9, "report_power_statistic"

    iget-object v10, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOnAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    iget-object v11, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    move-wide v7, v12

    invoke-virtual/range {v5 .. v11}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 440
    :cond_2
    return-void
.end method

.method private updateAvgTime(FFI)F
    .locals 2
    .param p1, "curAvg"    # F
    .param p2, "newSum"    # F
    .param p3, "size"    # I

    .line 585
    add-float v0, p2, p1

    add-int/lit8 v1, p3, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private updateScreenReasons(Landroid/util/SparseArray;I)V
    .locals 5
    .param p2, "reason"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Ljava/lang/Long;",
            ">;I)V"
        }
    .end annotation

    .line 589
    .local p1, "array":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Long;>;"
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 590
    .local v0, "curNum":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v0, v1

    invoke-virtual {p1, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 591
    return-void
.end method


# virtual methods
.method public bootCompleted()V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAlarmManager:Landroid/app/AlarmManager;

    .line 175
    invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->setReportScheduleEventAlarm()V

    .line 176
    return-void
.end method

.method public dump()V
    .locals 2

    .line 756
    sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 757
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 759
    :cond_0
    return-void
.end method

.method public dumpLocal()V
    .locals 4

    .line 762
    sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 763
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "MiuiPowerStaticTracker:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 764
    .local v0, "stringBuffer":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nonReasons:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnReasons:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 765
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\noffReasons:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffReasons:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 766
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nscreenOnList:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnLatencyList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 767
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nscreenOffList:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffLatencyList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 768
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nscreenOnAvgTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 769
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nscreenOffAvgTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 770
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nscreenOnBlockerAvgTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 771
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\npowerStateOnAvgTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 772
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\npowerStateOffAvgTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 773
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nallScreenOnTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 775
    const-string v1, "MiuiPowerStatisticTracker"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    .end local v0    # "stringBuffer":Ljava/lang/StringBuffer;
    :cond_0
    return-void
.end method

.method public handleDisplayStateChangedLatencyLocked(I)V
    .locals 1
    .param p1, "latencyMs"    # I

    .line 261
    iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z

    if-nez v0, :cond_0

    .line 262
    return-void

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setDisplayStateChangedLatency(I)V

    .line 266
    return-void
.end method

.method public handleUserAttentionChanged(II)V
    .locals 5
    .param p1, "latencyMs"    # I
    .param p2, "result"    # I

    .line 304
    sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleUserAttentionChanged, attention used:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPowerStatisticTracker"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    :cond_0
    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    if-nez p2, :cond_1

    goto :goto_0

    .line 314
    :cond_1
    iget v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureTimes:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureTimes:I

    .line 315
    iget-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureMillis:J

    int-to-long v3, p1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureMillis:J

    .line 316
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureReasons:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 317
    .local v1, "curNum":I
    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureReasons:Landroid/util/SparseArray;

    add-int/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 311
    .end local v1    # "curNum":I
    :cond_2
    :goto_0
    iget v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessTimes:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessTimes:I

    .line 312
    iget-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessMillis:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessMillis:J

    .line 319
    :goto_1
    return-void
.end method

.method public handleWakefulnessChanged(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V
    .locals 8
    .param p1, "powerEvent"    # Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    .line 195
    sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z

    const-string v1, "MiuiPowerStatisticTracker"

    if-eqz v0, :cond_0

    .line 196
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleWakefulnessChanged. powerEvent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_0
    iget-boolean v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    .line 201
    invoke-virtual {p1, v3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setQuickBreak(Z)V

    .line 204
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulness()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 205
    iput-boolean v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z

    .line 206
    invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mLastWakeTime:J

    .line 207
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnReasons:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getReason()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateScreenReasons(Landroid/util/SparseArray;I)V

    goto :goto_0

    .line 208
    :cond_2
    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulness:I

    invoke-static {v2}, Landroid/os/PowerManagerInternal;->isInteractive(I)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOff()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 209
    iput-boolean v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z

    .line 210
    iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mLastWakeTime:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J

    .line 211
    if-eqz v0, :cond_3

    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update all screen time on to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :cond_3
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffReasons:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getReason()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateScreenReasons(Landroid/util/SparseArray;I)V

    .line 224
    :goto_0
    iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z

    if-eqz v0, :cond_4

    .line 225
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->copyFrom(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V

    .line 226
    invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulness()I

    move-result v0

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulness:I

    .line 228
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mDisplayPortEventStatistic:Lcom/android/server/power/statistic/DisplayPortEventStatistic;

    invoke-static {v0}, Landroid/os/PowerManagerInternal;->isInteractive(I)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->handleInteractiveChanged(Z)V

    .line 230
    :cond_4
    return-void

    .line 217
    :cond_5
    if-eqz v0, :cond_6

    .line 218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "skip: old wakefulness="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulness:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", new wakefulness="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 219
    invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulness()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 218
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_6
    return-void
.end method

.method public handleWakefulnessCompleted(J)V
    .locals 3
    .param p1, "wakefulnessCompletedTime"    # J

    .line 275
    iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z

    if-nez v0, :cond_0

    .line 276
    return-void

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    .line 280
    invoke-virtual {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getEventTime()J

    move-result-wide v0

    sub-long v0, p1, v0

    long-to-int v0, v0

    .line 281
    .local v0, "interactiveChangeLatency":I
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setWakefulnessChangedLatency(I)V

    .line 282
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z

    .line 283
    sget-boolean v1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleWakefulnessCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPowerStatisticTracker"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :cond_1
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isValid()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 287
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 288
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnLatencyList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-direct {p0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->collectLatencyInfo(Ljava/util/ArrayList;Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V

    goto :goto_0

    .line 289
    :cond_2
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOff()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 290
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffLatencyList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-direct {p0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->collectLatencyInfo(Ljava/util/ArrayList;Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V

    .line 293
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerEvent:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->clear()V

    .line 294
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 161
    iput-object p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mContext:Landroid/content/Context;

    .line 162
    new-instance v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    .line 163
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mLastWakeTime:J

    .line 164
    new-instance v0, Lcom/android/server/power/statistic/OneTrackerHelper;

    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/power/statistic/OneTrackerHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    .line 165
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 166
    const v1, 0x1110013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSupportAdaptiveSleep:Z

    .line 167
    const-string v0, "config_aon_proximity_available"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonProximitySupport:Z

    .line 168
    new-instance v0, Lcom/android/server/power/statistic/TofEventStatistic;

    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    const-string v2, "31000401594"

    invoke-direct {v0, v2, p1, v1}, Lcom/android/server/power/statistic/TofEventStatistic;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/power/statistic/OneTrackerHelper;)V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mTofEventStatistic:Lcom/android/server/power/statistic/TofEventStatistic;

    .line 169
    new-instance v0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;

    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    invoke-direct {v0, v2, p1, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/power/statistic/OneTrackerHelper;)V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mDisplayPortEventStatistic:Lcom/android/server/power/statistic/DisplayPortEventStatistic;

    .line 170
    new-instance v0, Lcom/android/server/power/statistic/DimEventStatistic;

    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    invoke-direct {v0, v2, p1, v1}, Lcom/android/server/power/statistic/DimEventStatistic;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/power/statistic/OneTrackerHelper;)V

    iput-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mDimEventStatistic:Lcom/android/server/power/statistic/DimEventStatistic;

    .line 171
    return-void
.end method

.method public notifyAonScreenOnOffEvent(ZZJ)V
    .locals 8
    .param p1, "isScreenOn"    # Z
    .param p2, "hasFace"    # Z
    .param p3, "checkTime"    # J

    .line 495
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$$ExternalSyntheticLambda1;

    move-object v1, v7

    move-object v2, p0

    move v3, p1

    move-wide v4, p3

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;ZJZ)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 508
    return-void
.end method

.method public notifyDimStateChanged(JZ)V
    .locals 3
    .param p1, "now"    # J
    .param p3, "isEnterDim"    # Z

    .line 537
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;

    invoke-direct {v1, p1, p2, p3}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;-><init>(JZ)V

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 539
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 540
    return-void
.end method

.method public notifyDisplayPortConnectStateChanged(JZLjava/lang/String;ILjava/lang/String;)V
    .locals 9
    .param p1, "physicalDisplayId"    # J
    .param p3, "isConnected"    # Z
    .param p4, "productName"    # Ljava/lang/String;
    .param p5, "frameRate"    # I
    .param p6, "resolution"    # Ljava/lang/String;

    .line 525
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 526
    .local v0, "message":Landroid/os/Message;
    new-instance v8, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    move-object v1, v8

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;-><init>(JZLjava/lang/String;ILjava/lang/String;)V

    iput-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 528
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 529
    return-void
.end method

.method public notifyDisplayStateChangedLatencyLocked(I)V
    .locals 2
    .param p1, "latencyMs"    # I

    .line 255
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 256
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 257
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 258
    return-void
.end method

.method public notifyForegroundAppChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "foregroundAppName"    # Ljava/lang/String;

    .line 532
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 533
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 534
    return-void
.end method

.method public notifyGestureEvent(Ljava/lang/String;ZI)V
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "success"    # Z
    .param p3, "label"    # I

    .line 511
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 512
    .local v0, "message":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 513
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 514
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 515
    return-void
.end method

.method public notifyScreenOnUnBlocker(II)V
    .locals 2
    .param p1, "screenOnBlockTime"    # I
    .param p2, "delayMiles"    # I

    .line 233
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 234
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 235
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 236
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 237
    return-void
.end method

.method public notifyTofPowerState(Z)V
    .locals 3
    .param p1, "wakeup"    # Z

    .line 518
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {v1, p1}, Ljava/lang/Boolean;-><init>(Z)V

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 520
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 521
    return-void
.end method

.method public notifyUserAttentionChanged(II)V
    .locals 2
    .param p1, "latencyMs"    # I
    .param p2, "result"    # I

    .line 297
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 298
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 299
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 300
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 301
    return-void
.end method

.method public notifyWakefulnessChangedLocked(IJI)V
    .locals 7
    .param p1, "wakefulness"    # I
    .param p2, "eventTime"    # J
    .param p4, "reason"    # I

    .line 188
    new-instance v6, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    move-object v0, v6

    move-object v1, p0

    move v2, p1

    move v3, p4

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;IIJ)V

    .line 189
    .local v0, "powerEvent":Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 190
    .local v1, "message":Landroid/os/Message;
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 191
    iget-object v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 192
    return-void
.end method

.method public notifyWakefulnessCompletedLocked(J)V
    .locals 2
    .param p1, "wakefulnessCompletedTime"    # J

    .line 269
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 270
    .local v0, "message":Landroid/os/Message;
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, p1, p2}, Ljava/lang/Long;-><init>(J)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 271
    iget-object v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 272
    return-void
.end method
