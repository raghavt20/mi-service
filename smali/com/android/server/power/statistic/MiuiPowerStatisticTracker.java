public class com.android.server.power.statistic.MiuiPowerStatisticTracker {
	 /* .source "MiuiPowerStatisticTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;, */
	 /* Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ALL_SCREEN_ON;
public static final java.lang.String AON_EVENT_NAME;
private static final java.lang.String APP_ID;
private static final Integer ARRAY_CAPACITY;
private static final java.lang.String BLOCK_AVG_TIME;
private static final Boolean DEBUG;
private static final java.lang.String FAILURE_MILLIS;
private static final java.lang.String FAILURE_REASONS;
private static final java.lang.String FAILURE_TIMES;
private static final java.lang.String FEATURE_AON_PROXIMITY_SUPPORT;
private static final java.lang.String KEY_AON_SCREEN_OFF_ENABLE;
private static final java.lang.String KEY_AON_SCREEN_ON_ENABLE;
private static final java.lang.String KEY_SCREEN_OFF_CHECK_ATTENTION_MILLIS;
private static final java.lang.String KEY_SCREEN_OFF_HAS_FACE_COUNT;
private static final java.lang.String KEY_SCREEN_ON_CHECK_ATTENTION_MILLIS;
private static final java.lang.String KEY_SCREEN_ON_HAS_FACE_COUNT;
private static final java.lang.String KEY_SCREEN_ON_NO_FACE_COUNT;
private static final Integer MSG_CONNECTED_STATE_CHANGED;
private static final Integer MSG_DIM_STATE_CHANGE;
private static final Integer MSG_DUMP;
private static final Integer MSG_FOREGROUND_APP_CHANGED;
private static final Integer MSG_POWER_STATE_CHANGE;
private static final Integer MSG_TOF_GESTURE_CHANGE;
private static final Integer MSG_TOF_PROXIMITY_CHANGE;
private static final Integer MSG_UNBLOCK_SCREEN;
private static final Integer MSG_USER_ATTENTION_CHANGE;
private static final Integer MSG_WAKEFULNESS_CHANGE;
private static final Integer MSG_WAKEFULNESS_CHANGE_COMPLETE;
private static final java.lang.String OFF_AVG_TIME;
private static final java.lang.String OFF_REASON;
private static final java.lang.String ON_AVG_TIME;
private static final java.lang.String ON_REASON;
public static final java.lang.String POWER_EVENT_NAME;
private static final java.lang.String STATE_OFF_AVG_TIME;
private static final java.lang.String STATE_ON_AVG_TIME;
private static final java.lang.String SUCCESS_MILLIS;
private static final java.lang.String SUCCESS_TIMES;
public static final java.lang.String TAG;
private static volatile com.android.server.power.statistic.MiuiPowerStatisticTracker sInstance;
/* # instance fields */
private final Long DEBUG_REPORT_TIME_DURATION;
private android.app.AlarmManager mAlarmManager;
private Long mAllScreenOnTime;
private Boolean mAonProximitySupport;
private Boolean mAonScreenOffEnabled;
private Boolean mAonScreenOnEnabled;
private android.os.Handler mBackgroundHandler;
private android.content.Context mContext;
private com.android.server.power.statistic.DimEventStatistic mDimEventStatistic;
private com.android.server.power.statistic.DisplayPortEventStatistic mDisplayPortEventStatistic;
private Long mFailureMillis;
private final android.util.SparseArray mFailureReasons;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mFailureTimes;
private Long mLastWakeTime;
private final android.app.AlarmManager$OnAlarmListener mOnAlarmListener;
private com.android.server.power.statistic.OneTrackerHelper mOneTrackerHelper;
private com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent mPowerEvent;
private Float mPowerStateOffAvgTime;
private Float mPowerStateOnAvgTime;
private Float mScreenOffAvgTime;
private Long mScreenOffCheckAttentionMillis;
private Integer mScreenOffHasFaceCount;
private java.util.ArrayList mScreenOffLatencyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.util.SparseArray mScreenOffReasons;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Float mScreenOnAvgTime;
private Float mScreenOnBlockerAvgTime;
private Long mScreenOnCheckAttentionMillis;
private Integer mScreenOnHasFaceCount;
private java.util.ArrayList mScreenOnLatencyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mScreenOnNoFaceCount;
private android.util.SparseArray mScreenOnReasons;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mSuccessMillis;
private Integer mSuccessTimes;
private Boolean mSupportAdaptiveSleep;
private com.android.server.power.statistic.TofEventStatistic mTofEventStatistic;
private Integer mWakefulness;
private Boolean mWakefulnessChanging;
/* # direct methods */
public static void $r8$lambda$GCnAKYsx61UV-3tCeELeqeVRH_k ( com.android.server.power.statistic.MiuiPowerStatisticTracker p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->lambda$new$0()V */
return;
} // .end method
public static void $r8$lambda$qSU8ooZ6RdP3o_XIy0wTWsZ2khI ( com.android.server.power.statistic.MiuiPowerStatisticTracker p0, Boolean p1, Long p2, Boolean p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->lambda$notifyAonScreenOnOffEvent$1(ZJZ)V */
return;
} // .end method
static com.android.server.power.statistic.DimEventStatistic -$$Nest$fgetmDimEventStatistic ( com.android.server.power.statistic.MiuiPowerStatisticTracker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDimEventStatistic;
} // .end method
static com.android.server.power.statistic.DisplayPortEventStatistic -$$Nest$fgetmDisplayPortEventStatistic ( com.android.server.power.statistic.MiuiPowerStatisticTracker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDisplayPortEventStatistic;
} // .end method
static com.android.server.power.statistic.TofEventStatistic -$$Nest$fgetmTofEventStatistic ( com.android.server.power.statistic.MiuiPowerStatisticTracker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTofEventStatistic;
} // .end method
static void -$$Nest$mhandleScreenOnUnBlocker ( com.android.server.power.statistic.MiuiPowerStatisticTracker p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleScreenOnUnBlocker(II)V */
return;
} // .end method
static com.android.server.power.statistic.MiuiPowerStatisticTracker ( ) {
/* .locals 2 */
/* .line 40 */
/* nop */
/* .line 41 */
final String v0 = "debug.miui.power.statistic.dbg"; // const-string v0, "debug.miui.power.statistic.dbg"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
com.android.server.power.statistic.MiuiPowerStatisticTracker.DEBUG = (v1!= 0);
/* .line 40 */
return;
} // .end method
public com.android.server.power.statistic.MiuiPowerStatisticTracker ( ) {
/* .locals 2 */
/* .line 37 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 42 */
/* const-wide/32 v0, 0x1d4c0 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG_REPORT_TIME_DURATION:J */
/* .line 82 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mScreenOnReasons = v0;
/* .line 83 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mScreenOffReasons = v0;
/* .line 85 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0xa */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mScreenOnLatencyList = v0;
/* .line 86 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mScreenOffLatencyList = v0;
/* .line 95 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulness:I */
/* .line 109 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mFailureReasons = v0;
/* .line 141 */
/* new-instance v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)V */
this.mPowerEvent = v0;
/* .line 178 */
/* new-instance v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)V */
this.mOnAlarmListener = v0;
return;
} // .end method
private void collectLatencyInfo ( java.util.ArrayList p0, com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent p1 ) {
/* .locals 2 */
/* .param p2, "powerEvent" # Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;", */
/* ">;", */
/* "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 371 */
/* .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;>;" */
v0 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* const/16 v1, 0xa */
/* if-le v0, v1, :cond_0 */
/* .line 372 */
/* invoke-direct {p0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->recalculateLatencyInfo(Ljava/util/ArrayList;)V */
/* .line 375 */
} // :cond_0
v0 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p2 ).isValid ( ); // invoke-virtual {p2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isValid()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 376 */
/* new-instance v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
/* invoke-direct {v0, p0, p2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V */
(( java.util.ArrayList ) p1 ).add ( v0 ); // invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 378 */
} // :cond_1
return;
} // .end method
public static com.android.server.power.statistic.MiuiPowerStatisticTracker getInstance ( ) {
/* .locals 2 */
/* .line 150 */
v0 = com.android.server.power.statistic.MiuiPowerStatisticTracker.sInstance;
/* if-nez v0, :cond_1 */
/* .line 151 */
/* const-class v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker; */
/* monitor-enter v0 */
/* .line 152 */
try { // :try_start_0
v1 = com.android.server.power.statistic.MiuiPowerStatisticTracker.sInstance;
/* if-nez v1, :cond_0 */
/* .line 153 */
/* new-instance v1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker; */
/* invoke-direct {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;-><init>()V */
/* .line 155 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 157 */
} // :cond_1
} // :goto_0
v0 = com.android.server.power.statistic.MiuiPowerStatisticTracker.sInstance;
} // .end method
private void handleScreenOnUnBlocker ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "screenOnBlockTime" # I */
/* .param p2, "delayMiles" # I */
/* .line 240 */
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mPowerEvent;
v0 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v0 ).isScreenOn ( ); // invoke-virtual {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOn()Z
/* if-nez v0, :cond_0 */
/* .line 244 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 245 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleScreenOnUnBlocker: screenOnBlockStartRealTime="; // const-string v1, "handleScreenOnUnBlocker: screenOnBlockStartRealTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", delayMiles="; // const-string v1, ", delayMiles="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPowerStatisticTracker"; // const-string v1, "MiuiPowerStatisticTracker"
android.util.Slog .i ( v1,v0 );
/* .line 250 */
} // :cond_1
v0 = this.mPowerEvent;
/* if-lez p2, :cond_2 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v0 ).setDelayByFace ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setDelayByFace(Z)V
/* .line 251 */
v0 = this.mPowerEvent;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v0 ).setBlockedScreenLatency ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setBlockedScreenLatency(I)V
/* .line 252 */
return;
/* .line 241 */
} // :cond_3
} // :goto_1
return;
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 2 */
/* .line 180 */
/* sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 181 */
final String v0 = "MiuiPowerStatisticTracker"; // const-string v0, "MiuiPowerStatisticTracker"
final String v1 = "It\'s time to report"; // const-string v1, "It\'s time to report"
android.util.Slog .i ( v0,v1 );
/* .line 183 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->reportScheduleEvent()V */
/* .line 184 */
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->setReportScheduleEventAlarm()V */
/* .line 185 */
return;
} // .end method
private void lambda$notifyAonScreenOnOffEvent$1 ( Boolean p0, Long p1, Boolean p2 ) { //synthethic
/* .locals 2 */
/* .param p1, "isScreenOn" # Z */
/* .param p2, "checkTime" # J */
/* .param p4, "hasFace" # Z */
/* .line 496 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 497 */
/* iget-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnCheckAttentionMillis:J */
/* add-long/2addr v0, p2 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnCheckAttentionMillis:J */
/* .line 498 */
if ( p4 != null) { // if-eqz p4, :cond_0
/* .line 499 */
/* iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnHasFaceCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnHasFaceCount:I */
/* .line 501 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnNoFaceCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnNoFaceCount:I */
/* .line 504 */
} // :cond_1
/* iget-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffCheckAttentionMillis:J */
/* add-long/2addr v0, p2 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffCheckAttentionMillis:J */
/* .line 505 */
/* iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffHasFaceCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffHasFaceCount:I */
/* .line 507 */
} // :goto_0
return;
} // .end method
private void recalculateLatencyInfo ( java.util.ArrayList p0 ) {
/* .locals 10 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 381 */
/* .local p1, "latencyInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;>;" */
v0 = (( java.util.ArrayList ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 382 */
return;
/* .line 385 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 386 */
/* .local v0, "sumWakefulnessChangedLatency":F */
int v1 = 0; // const/4 v1, 0x0
/* .line 387 */
/* .local v1, "sumBlockedScreenLatency":F */
int v2 = 0; // const/4 v2, 0x0
/* .line 389 */
/* .local v2, "sumDisplayStateLatency":F */
int v3 = 0; // const/4 v3, 0x0
/* .line 390 */
/* .local v3, "wakefulnessChangedSize":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 391 */
/* .local v4, "blockedScreenSize":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 393 */
/* .local v5, "DisplayStateChangedSize":I */
(( java.util.ArrayList ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_4
/* check-cast v7, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
/* .line 394 */
/* .local v7, "powerEvent":Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
v8 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v7 ).getWakefulnessChangedLatency ( ); // invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulnessChangedLatency()F
int v9 = 0; // const/4 v9, 0x0
/* cmpl-float v8, v8, v9 */
/* if-lez v8, :cond_1 */
/* .line 395 */
v8 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v7 ).getWakefulnessChangedLatency ( ); // invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulnessChangedLatency()F
/* add-float/2addr v0, v8 */
/* .line 396 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 399 */
} // :cond_1
v8 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v7 ).getBlockedScreenLatency ( ); // invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getBlockedScreenLatency()F
/* cmpl-float v8, v8, v9 */
/* if-lez v8, :cond_2 */
/* .line 400 */
v8 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v7 ).getBlockedScreenLatency ( ); // invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getBlockedScreenLatency()F
/* add-float/2addr v1, v8 */
/* .line 401 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 404 */
} // :cond_2
v8 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v7 ).getDisplayStateChangedLatency ( ); // invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getDisplayStateChangedLatency()I
/* if-lez v8, :cond_3 */
/* .line 405 */
v8 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v7 ).getDisplayStateChangedLatency ( ); // invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getDisplayStateChangedLatency()I
/* int-to-float v8, v8 */
/* add-float/2addr v2, v8 */
/* .line 406 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 408 */
} // .end local v7 # "powerEvent":Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;
} // :cond_3
/* .line 410 */
} // :cond_4
int v6 = 0; // const/4 v6, 0x0
(( java.util.ArrayList ) p1 ).get ( v6 ); // invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
v7 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v7 ).isScreenOn ( ); // invoke-virtual {v7}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOn()Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 411 */
/* iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F */
v6 = /* invoke-direct {p0, v6, v0, v3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F */
/* iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F */
/* .line 413 */
/* iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F */
v6 = /* invoke-direct {p0, v6, v1, v4}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F */
/* iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F */
/* .line 415 */
/* iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F */
v6 = /* invoke-direct {p0, v6, v2, v5}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F */
/* iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F */
/* .line 417 */
} // :cond_5
(( java.util.ArrayList ) p1 ).get ( v6 ); // invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
v6 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v6 ).isScreenOff ( ); // invoke-virtual {v6}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOff()Z
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 418 */
/* iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F */
v6 = /* invoke-direct {p0, v6, v0, v3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F */
/* iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F */
/* .line 420 */
/* iget v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F */
v6 = /* invoke-direct {p0, v6, v2, v5}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateAvgTime(FFI)F */
/* iput v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F */
/* .line 424 */
} // :cond_6
} // :goto_1
(( java.util.ArrayList ) p1 ).clear ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V
/* .line 425 */
return;
} // .end method
private void reportAonScreenOnOffEventByIntent ( ) {
/* .locals 5 */
/* .line 452 */
/* sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_ON_SUPPORTED:Z */
/* if-nez v0, :cond_0 */
/* .line 453 */
return;
/* .line 456 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_ON_SUPPORTED:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = -2; // const/4 v2, -0x2
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mContext;
/* .line 457 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "miui_people_near_screen_on"; // const-string v4, "miui_people_near_screen_on"
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v4,v3,v2 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v0, v1 */
} // :cond_1
/* move v0, v3 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOnEnabled:Z */
/* .line 459 */
/* sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mContext;
/* .line 460 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "gaze_lock_screen_setting"; // const-string v4, "gaze_lock_screen_setting"
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v4,v3,v2 );
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_2
/* move v1, v3 */
} // :goto_1
/* iput-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOffEnabled:Z */
/* .line 462 */
v0 = this.mOneTrackerHelper;
v1 = this.mContext;
/* .line 463 */
(( android.content.Context ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
/* .line 462 */
final String v2 = "31000401594"; // const-string v2, "31000401594"
final String v3 = "aon_statistic"; // const-string v3, "aon_statistic"
(( com.android.server.power.statistic.OneTrackerHelper ) v0 ).getTrackEventIntent ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 464 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "aon_screen_on_enable"; // const-string v1, "aon_screen_on_enable"
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOnEnabled:Z */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOffEnabled:Z */
/* .line 465 */
final String v3 = "aon_screen_off_enable"; // const-string v3, "aon_screen_off_enable"
(( android.content.Intent ) v1 ).putExtra ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 466 */
/* iget-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOffEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 467 */
final String v1 = "screen_on_no_face_count"; // const-string v1, "screen_on_no_face_count"
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnNoFaceCount:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnHasFaceCount:I */
/* .line 468 */
final String v3 = "screen_on_has_face_count"; // const-string v3, "screen_on_has_face_count"
(( android.content.Intent ) v1 ).putExtra ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnCheckAttentionMillis:J */
/* .line 469 */
final String v4 = "screen_on_check_attention_millis"; // const-string v4, "screen_on_check_attention_millis"
(( android.content.Intent ) v1 ).putExtra ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 471 */
} // :cond_3
/* iget-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOnEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 472 */
final String v1 = "screen_off_has_face_count"; // const-string v1, "screen_off_has_face_count"
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffHasFaceCount:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffCheckAttentionMillis:J */
/* .line 473 */
final String v4 = "screen_off_check_attention_millis"; // const-string v4, "screen_off_check_attention_millis"
(( android.content.Intent ) v1 ).putExtra ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 475 */
} // :cond_4
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reportAonScreenOnOffEventByIntent: intent:"; // const-string v2, "reportAonScreenOnOffEventByIntent: intent:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) v0 ).getExtras ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPowerStatisticTracker"; // const-string v2, "MiuiPowerStatisticTracker"
android.util.Slog .d ( v2,v1 );
/* .line 477 */
try { // :try_start_0
v1 = this.mOneTrackerHelper;
(( com.android.server.power.statistic.OneTrackerHelper ) v1 ).reportTrackEventByIntent ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 480 */
/* .line 478 */
/* :catch_0 */
/* move-exception v1 */
/* .line 479 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "reportAttentionEvent fail! "; // const-string v4, "reportAttentionEvent fail! "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 481 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->resetAonScreenOnOffData()V */
/* .line 482 */
return;
} // .end method
private void reportAttentionEventByIntent ( ) {
/* .locals 5 */
/* .line 594 */
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSupportAdaptiveSleep:Z */
/* if-nez v0, :cond_0 */
/* .line 595 */
return;
/* .line 598 */
} // :cond_0
try { // :try_start_0
v0 = this.mOneTrackerHelper;
final String v1 = "31000401594"; // const-string v1, "31000401594"
final String v2 = "aon_statistic"; // const-string v2, "aon_statistic"
v3 = this.mContext;
/* .line 599 */
(( android.content.Context ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
/* .line 598 */
(( com.android.server.power.statistic.OneTrackerHelper ) v0 ).getTrackEventIntent ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 600 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const-string/jumbo v1, "success_times" */
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessTimes:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
final String v2 = "failure_times"; // const-string v2, "failure_times"
/* iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureTimes:I */
/* .line 601 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* const-string/jumbo v2, "success_millis" */
/* iget-wide v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessMillis:J */
/* .line 602 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
final String v2 = "failure_millis"; // const-string v2, "failure_millis"
/* iget-wide v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureMillis:J */
/* .line 603 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
final String v2 = "failure_reasons"; // const-string v2, "failure_reasons"
v3 = this.mFailureReasons;
/* .line 604 */
(( android.util.SparseArray ) v3 ).toString ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->toString()Ljava/lang/String;
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 605 */
v1 = this.mOneTrackerHelper;
(( com.android.server.power.statistic.OneTrackerHelper ) v1 ).reportTrackEventByIntent ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 608 */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 606 */
/* :catch_0 */
/* move-exception v0 */
/* .line 607 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reportAttentionEvent fail! "; // const-string v2, "reportAttentionEvent fail! "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPowerStatisticTracker"; // const-string v2, "MiuiPowerStatisticTracker"
android.util.Slog .e ( v2,v1 );
/* .line 609 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->resetAttentionData()V */
/* .line 610 */
return;
} // .end method
private void reportScheduleEvent ( ) {
/* .locals 1 */
/* .line 443 */
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->reportScreenOnOffEventByIntent()V */
/* .line 444 */
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->reportAttentionEventByIntent()V */
/* .line 445 */
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->reportAonScreenOnOffEventByIntent()V */
/* .line 446 */
v0 = this.mTofEventStatistic;
(( com.android.server.power.statistic.TofEventStatistic ) v0 ).reportTofEventByIntent ( ); // invoke-virtual {v0}, Lcom/android/server/power/statistic/TofEventStatistic;->reportTofEventByIntent()V
/* .line 447 */
v0 = this.mDisplayPortEventStatistic;
(( com.android.server.power.statistic.DisplayPortEventStatistic ) v0 ).reportDisplayPortEventByIntent ( ); // invoke-virtual {v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->reportDisplayPortEventByIntent()V
/* .line 448 */
v0 = this.mDimEventStatistic;
(( com.android.server.power.statistic.DimEventStatistic ) v0 ).reportDimStateByIntent ( ); // invoke-virtual {v0}, Lcom/android/server/power/statistic/DimEventStatistic;->reportDimStateByIntent()V
/* .line 449 */
return;
} // .end method
private void reportScreenOnOffEventByIntent ( ) {
/* .locals 5 */
/* .line 543 */
v0 = this.mScreenOnLatencyList;
/* invoke-direct {p0, v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->recalculateLatencyInfo(Ljava/util/ArrayList;)V */
/* .line 544 */
v0 = this.mScreenOffLatencyList;
/* invoke-direct {p0, v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->recalculateLatencyInfo(Ljava/util/ArrayList;)V */
/* .line 547 */
try { // :try_start_0
v0 = this.mOneTrackerHelper;
final String v1 = "31000401594"; // const-string v1, "31000401594"
final String v2 = "power_on_off_statistic"; // const-string v2, "power_on_off_statistic"
v3 = this.mContext;
/* .line 548 */
(( android.content.Context ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
/* .line 547 */
(( com.android.server.power.statistic.OneTrackerHelper ) v0 ).getTrackEventIntent ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 550 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "on_reason"; // const-string v1, "on_reason"
v2 = this.mScreenOnReasons;
(( android.util.SparseArray ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->toString()Ljava/lang/String;
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
final String v2 = "off_reason"; // const-string v2, "off_reason"
v3 = this.mScreenOffReasons;
/* .line 551 */
(( android.util.SparseArray ) v3 ).toString ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->toString()Ljava/lang/String;
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
final String v2 = "on_avg_time"; // const-string v2, "on_avg_time"
/* iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F */
/* .line 552 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
final String v2 = "off_avg_time"; // const-string v2, "off_avg_time"
/* iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F */
/* .line 553 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
final String v2 = "block_avg_time"; // const-string v2, "block_avg_time"
/* iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F */
/* .line 554 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* const-string/jumbo v2, "state_on_avg_time" */
/* iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F */
/* .line 555 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* const-string/jumbo v2, "state_off_avg_time" */
/* iget v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F */
/* .line 556 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
final String v2 = "all_screen_on_time"; // const-string v2, "all_screen_on_time"
/* iget-wide v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J */
/* .line 557 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 559 */
v1 = this.mOneTrackerHelper;
(( com.android.server.power.statistic.OneTrackerHelper ) v1 ).reportTrackEventByIntent ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 562 */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 560 */
/* :catch_0 */
/* move-exception v0 */
/* .line 561 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reportScreenOnOffEventByIntent fail! "; // const-string v2, "reportScreenOnOffEventByIntent fail! "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPowerStatisticTracker"; // const-string v2, "MiuiPowerStatisticTracker"
android.util.Slog .e ( v2,v1 );
/* .line 564 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->resetRecordInfo()V */
/* .line 565 */
return;
} // .end method
private void resetAonScreenOnOffData ( ) {
/* .locals 3 */
/* .line 485 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOnEnabled:Z */
/* .line 486 */
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonScreenOffEnabled:Z */
/* .line 487 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnNoFaceCount:I */
/* .line 488 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnHasFaceCount:I */
/* .line 489 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnCheckAttentionMillis:J */
/* .line 490 */
/* iput-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffCheckAttentionMillis:J */
/* .line 491 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffHasFaceCount:I */
/* .line 492 */
return;
} // .end method
private void resetAttentionData ( ) {
/* .locals 2 */
/* .line 613 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessTimes:I */
/* .line 614 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureTimes:I */
/* .line 615 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessMillis:J */
/* .line 616 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureMillis:J */
/* .line 617 */
v0 = this.mFailureReasons;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 618 */
return;
} // .end method
private void resetAvgTime ( ) {
/* .locals 2 */
/* .line 576 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F */
/* .line 577 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F */
/* .line 578 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F */
/* .line 579 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F */
/* .line 580 */
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F */
/* .line 581 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J */
/* .line 582 */
return;
} // .end method
private void resetRecordInfo ( ) {
/* .locals 1 */
/* .line 568 */
v0 = this.mScreenOnReasons;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 569 */
v0 = this.mScreenOffReasons;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 570 */
v0 = this.mScreenOnLatencyList;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 571 */
v0 = this.mScreenOffLatencyList;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 572 */
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->resetAvgTime()V */
/* .line 573 */
return;
} // .end method
private void setReportScheduleEventAlarm ( ) {
/* .locals 14 */
/* .line 428 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 429 */
/* .local v0, "now":J */
/* sget-boolean v2, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* const-wide/32 v3, 0x1d4c0 */
} // :cond_0
/* const-wide/32 v3, 0x5265c00 */
/* .line 430 */
/* .local v3, "duration":J */
} // :goto_0
/* add-long v12, v0, v3 */
/* .line 431 */
/* .local v12, "nextTime":J */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 432 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setReportScheduleEventAlarm: next time: " */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 433 */
android.util.TimeUtils .formatDuration ( v3,v4 );
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 432 */
final String v5 = "MiuiPowerStatisticTracker"; // const-string v5, "MiuiPowerStatisticTracker"
android.util.Slog .d ( v5,v2 );
/* .line 436 */
} // :cond_1
v5 = this.mAlarmManager;
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 437 */
int v6 = 2; // const/4 v6, 0x2
final String v9 = "report_power_statistic"; // const-string v9, "report_power_statistic"
v10 = this.mOnAlarmListener;
v11 = this.mBackgroundHandler;
/* move-wide v7, v12 */
/* invoke-virtual/range {v5 ..v11}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 440 */
} // :cond_2
return;
} // .end method
private Float updateAvgTime ( Float p0, Float p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "curAvg" # F */
/* .param p2, "newSum" # F */
/* .param p3, "size" # I */
/* .line 585 */
/* add-float v0, p2, p1 */
/* add-int/lit8 v1, p3, 0x1 */
/* int-to-float v1, v1 */
/* div-float/2addr v0, v1 */
} // .end method
private void updateScreenReasons ( android.util.SparseArray p0, Integer p1 ) {
/* .locals 5 */
/* .param p2, "reason" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Long;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 589 */
/* .local p1, "array":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Long;>;" */
/* const-wide/16 v0, 0x0 */
java.lang.Long .valueOf ( v0,v1 );
(( android.util.SparseArray ) p1 ).get ( p2, v0 ); // invoke-virtual {p1, p2, v0}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Long; */
/* .line 590 */
/* .local v0, "curNum":Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
/* const-wide/16 v3, 0x1 */
/* add-long/2addr v1, v3 */
java.lang.Long .valueOf ( v1,v2 );
/* move-object v0, v1 */
(( android.util.SparseArray ) p1 ).put ( p2, v1 ); // invoke-virtual {p1, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 591 */
return;
} // .end method
/* # virtual methods */
public void bootCompleted ( ) {
/* .locals 2 */
/* .line 174 */
v0 = this.mContext;
final String v1 = "alarm"; // const-string v1, "alarm"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
this.mAlarmManager = v0;
/* .line 175 */
/* invoke-direct {p0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->setReportScheduleEventAlarm()V */
/* .line 176 */
return;
} // .end method
public void dump ( ) {
/* .locals 2 */
/* .line 756 */
/* sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 757 */
v0 = this.mBackgroundHandler;
/* const/16 v1, 0xa */
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 759 */
} // :cond_0
return;
} // .end method
public void dumpLocal ( ) {
/* .locals 4 */
/* .line 762 */
/* sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 763 */
/* new-instance v0, Ljava/lang/StringBuffer; */
final String v1 = "MiuiPowerStaticTracker:"; // const-string v1, "MiuiPowerStaticTracker:"
/* invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V */
/* .line 764 */
/* .local v0, "stringBuffer":Ljava/lang/StringBuffer; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nonReasons:"; // const-string v2, "\nonReasons:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mScreenOnReasons;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 765 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\noffReasons:"; // const-string v2, "\noffReasons:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mScreenOffReasons;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 766 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nscreenOnList:"; // const-string v2, "\nscreenOnList:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mScreenOnLatencyList;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 767 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nscreenOffList:"; // const-string v2, "\nscreenOffList:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mScreenOffLatencyList;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 768 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nscreenOnAvgTime:"; // const-string v2, "\nscreenOnAvgTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnAvgTime:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 769 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nscreenOffAvgTime:"; // const-string v2, "\nscreenOffAvgTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOffAvgTime:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 770 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nscreenOnBlockerAvgTime:"; // const-string v2, "\nscreenOnBlockerAvgTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mScreenOnBlockerAvgTime:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 771 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\npowerStateOnAvgTime:"; // const-string v2, "\npowerStateOnAvgTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOnAvgTime:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 772 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\npowerStateOffAvgTime:"; // const-string v2, "\npowerStateOffAvgTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mPowerStateOffAvgTime:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 773 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nallScreenOnTime:"; // const-string v2, "\nallScreenOnTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 775 */
final String v1 = "MiuiPowerStatisticTracker"; // const-string v1, "MiuiPowerStatisticTracker"
(( java.lang.StringBuffer ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 777 */
} // .end local v0 # "stringBuffer":Ljava/lang/StringBuffer;
} // :cond_0
return;
} // .end method
public void handleDisplayStateChangedLatencyLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "latencyMs" # I */
/* .line 261 */
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z */
/* if-nez v0, :cond_0 */
/* .line 262 */
return;
/* .line 265 */
} // :cond_0
v0 = this.mPowerEvent;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v0 ).setDisplayStateChangedLatency ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setDisplayStateChangedLatency(I)V
/* .line 266 */
return;
} // .end method
public void handleUserAttentionChanged ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "latencyMs" # I */
/* .param p2, "result" # I */
/* .line 304 */
/* sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 305 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleUserAttentionChanged, attention used:"; // const-string v1, "handleUserAttentionChanged, attention used:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", result:"; // const-string v1, ", result:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPowerStatisticTracker"; // const-string v1, "MiuiPowerStatisticTracker"
android.util.Slog .i ( v1,v0 );
/* .line 309 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-eq p2, v0, :cond_2 */
/* if-nez p2, :cond_1 */
/* .line 314 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureTimes:I */
/* add-int/2addr v1, v0 */
/* iput v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureTimes:I */
/* .line 315 */
/* iget-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureMillis:J */
/* int-to-long v3, p1 */
/* add-long/2addr v1, v3 */
/* iput-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mFailureMillis:J */
/* .line 316 */
v1 = this.mFailureReasons;
int v2 = 0; // const/4 v2, 0x0
java.lang.Integer .valueOf ( v2 );
(( android.util.SparseArray ) v1 ).get ( p2, v2 ); // invoke-virtual {v1, p2, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 317 */
/* .local v1, "curNum":I */
v2 = this.mFailureReasons;
/* add-int/2addr v1, v0 */
java.lang.Integer .valueOf ( v1 );
(( android.util.SparseArray ) v2 ).put ( p2, v0 ); // invoke-virtual {v2, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 311 */
} // .end local v1 # "curNum":I
} // :cond_2
} // :goto_0
/* iget v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessTimes:I */
/* add-int/2addr v1, v0 */
/* iput v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessTimes:I */
/* .line 312 */
/* iget-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessMillis:J */
/* int-to-long v2, p1 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSuccessMillis:J */
/* .line 319 */
} // :goto_1
return;
} // .end method
public void handleWakefulnessChanged ( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent p0 ) {
/* .locals 8 */
/* .param p1, "powerEvent" # Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
/* .line 195 */
/* sget-boolean v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z */
final String v1 = "MiuiPowerStatisticTracker"; // const-string v1, "MiuiPowerStatisticTracker"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 196 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handleWakefulnessChanged.powerEvent="; // const-string v3, "handleWakefulnessChanged.powerEvent="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 200 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z */
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 201 */
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p1 ).setQuickBreak ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setQuickBreak(Z)V
/* .line 204 */
} // :cond_1
v2 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p1 ).getWakefulness ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulness()I
/* if-ne v2, v3, :cond_2 */
/* .line 205 */
/* iput-boolean v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z */
/* .line 206 */
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p1 ).getEventTime ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getEventTime()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mLastWakeTime:J */
/* .line 207 */
v0 = this.mScreenOnReasons;
v1 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p1 ).getReason ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getReason()I
/* invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateScreenReasons(Landroid/util/SparseArray;I)V */
/* .line 208 */
} // :cond_2
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulness:I */
v2 = android.os.PowerManagerInternal .isInteractive ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_5
v2 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p1 ).isScreenOff ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOff()Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 209 */
/* iput-boolean v3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z */
/* .line 210 */
/* iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* iget-wide v6, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mLastWakeTime:J */
/* sub-long/2addr v4, v6 */
/* add-long/2addr v2, v4 */
/* iput-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J */
/* .line 211 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 212 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "update all screen time on to " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAllScreenOnTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 214 */
} // :cond_3
v0 = this.mScreenOffReasons;
v1 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p1 ).getReason ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getReason()I
/* invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->updateScreenReasons(Landroid/util/SparseArray;I)V */
/* .line 224 */
} // :goto_0
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 225 */
v0 = this.mPowerEvent;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v0 ).copyFrom ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->copyFrom(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V
/* .line 226 */
v0 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p1 ).getWakefulness ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulness()I
/* iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulness:I */
/* .line 228 */
v1 = this.mDisplayPortEventStatistic;
v0 = android.os.PowerManagerInternal .isInteractive ( v0 );
(( com.android.server.power.statistic.DisplayPortEventStatistic ) v1 ).handleInteractiveChanged ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->handleInteractiveChanged(Z)V
/* .line 230 */
} // :cond_4
return;
/* .line 217 */
} // :cond_5
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 218 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "skip: old wakefulness=" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulness:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", new wakefulness="; // const-string v2, ", new wakefulness="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 219 */
v2 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) p1 ).getWakefulness ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getWakefulness()I
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 218 */
android.util.Slog .i ( v1,v0 );
/* .line 221 */
} // :cond_6
return;
} // .end method
public void handleWakefulnessCompleted ( Long p0 ) {
/* .locals 3 */
/* .param p1, "wakefulnessCompletedTime" # J */
/* .line 275 */
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z */
/* if-nez v0, :cond_0 */
/* .line 276 */
return;
/* .line 279 */
} // :cond_0
v0 = this.mPowerEvent;
/* .line 280 */
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v0 ).getEventTime ( ); // invoke-virtual {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->getEventTime()J
/* move-result-wide v0 */
/* sub-long v0, p1, v0 */
/* long-to-int v0, v0 */
/* .line 281 */
/* .local v0, "interactiveChangeLatency":I */
v1 = this.mPowerEvent;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v1 ).setWakefulnessChangedLatency ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->setWakefulnessChangedLatency(I)V
/* .line 282 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mWakefulnessChanging:Z */
/* .line 283 */
/* sget-boolean v1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 284 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "handleWakefulnessCompleted: "; // const-string v2, "handleWakefulnessCompleted: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mPowerEvent;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPowerStatisticTracker"; // const-string v2, "MiuiPowerStatisticTracker"
android.util.Slog .i ( v2,v1 );
/* .line 286 */
} // :cond_1
v1 = this.mPowerEvent;
v1 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v1 ).isValid ( ); // invoke-virtual {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isValid()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 287 */
v1 = this.mPowerEvent;
v1 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v1 ).isScreenOn ( ); // invoke-virtual {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOn()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 288 */
v1 = this.mScreenOnLatencyList;
v2 = this.mPowerEvent;
/* invoke-direct {p0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->collectLatencyInfo(Ljava/util/ArrayList;Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V */
/* .line 289 */
} // :cond_2
v1 = this.mPowerEvent;
v1 = (( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v1 ).isScreenOff ( ); // invoke-virtual {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->isScreenOff()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 290 */
v1 = this.mScreenOffLatencyList;
v2 = this.mPowerEvent;
/* invoke-direct {p0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->collectLatencyInfo(Ljava/util/ArrayList;Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V */
/* .line 293 */
} // :cond_3
} // :goto_0
v1 = this.mPowerEvent;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerEvent ) v1 ).clear ( ); // invoke-virtual {v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->clear()V
/* .line 294 */
return;
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 161 */
this.mContext = p1;
/* .line 162 */
/* new-instance v0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler; */
com.android.internal.os.BackgroundThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;Landroid/os/Looper;)V */
this.mBackgroundHandler = v0;
/* .line 163 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mLastWakeTime:J */
/* .line 164 */
/* new-instance v0, Lcom/android/server/power/statistic/OneTrackerHelper; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/android/server/power/statistic/OneTrackerHelper;-><init>(Landroid/content/Context;)V */
this.mOneTrackerHelper = v0;
/* .line 165 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 166 */
/* const v1, 0x1110013 */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mSupportAdaptiveSleep:Z */
/* .line 167 */
final String v0 = "config_aon_proximity_available"; // const-string v0, "config_aon_proximity_available"
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->mAonProximitySupport:Z */
/* .line 168 */
/* new-instance v0, Lcom/android/server/power/statistic/TofEventStatistic; */
v1 = this.mOneTrackerHelper;
final String v2 = "31000401594"; // const-string v2, "31000401594"
/* invoke-direct {v0, v2, p1, v1}, Lcom/android/server/power/statistic/TofEventStatistic;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/power/statistic/OneTrackerHelper;)V */
this.mTofEventStatistic = v0;
/* .line 169 */
/* new-instance v0, Lcom/android/server/power/statistic/DisplayPortEventStatistic; */
v1 = this.mOneTrackerHelper;
/* invoke-direct {v0, v2, p1, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/power/statistic/OneTrackerHelper;)V */
this.mDisplayPortEventStatistic = v0;
/* .line 170 */
/* new-instance v0, Lcom/android/server/power/statistic/DimEventStatistic; */
v1 = this.mOneTrackerHelper;
/* invoke-direct {v0, v2, p1, v1}, Lcom/android/server/power/statistic/DimEventStatistic;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/power/statistic/OneTrackerHelper;)V */
this.mDimEventStatistic = v0;
/* .line 171 */
return;
} // .end method
public void notifyAonScreenOnOffEvent ( Boolean p0, Boolean p1, Long p2 ) {
/* .locals 8 */
/* .param p1, "isScreenOn" # Z */
/* .param p2, "hasFace" # Z */
/* .param p3, "checkTime" # J */
/* .line 495 */
v0 = this.mBackgroundHandler;
/* new-instance v7, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$$ExternalSyntheticLambda1; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move v3, p1 */
/* move-wide v4, p3 */
/* move v6, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;ZJZ)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 508 */
return;
} // .end method
public void notifyDimStateChanged ( Long p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "now" # J */
/* .param p3, "isEnterDim" # Z */
/* .line 537 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent; */
/* invoke-direct {v1, p1, p2, p3}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;-><init>(JZ)V */
/* const/16 v2, 0xb */
(( android.os.Handler ) v0 ).obtainMessage ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 539 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 540 */
return;
} // .end method
public void notifyDisplayPortConnectStateChanged ( Long p0, Boolean p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 9 */
/* .param p1, "physicalDisplayId" # J */
/* .param p3, "isConnected" # Z */
/* .param p4, "productName" # Ljava/lang/String; */
/* .param p5, "frameRate" # I */
/* .param p6, "resolution" # Ljava/lang/String; */
/* .line 525 */
v0 = this.mBackgroundHandler;
/* const/16 v1, 0x8 */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 526 */
/* .local v0, "message":Landroid/os/Message; */
/* new-instance v8, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent; */
/* move-object v1, v8 */
/* move-wide v2, p1 */
/* move v4, p3 */
/* move-object v5, p4 */
/* move v6, p5 */
/* move-object v7, p6 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;-><init>(JZLjava/lang/String;ILjava/lang/String;)V */
this.obj = v8;
/* .line 528 */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 529 */
return;
} // .end method
public void notifyDisplayStateChangedLatencyLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "latencyMs" # I */
/* .line 255 */
v0 = this.mBackgroundHandler;
int v1 = 4; // const/4 v1, 0x4
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 256 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 257 */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 258 */
return;
} // .end method
public void notifyForegroundAppChanged ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "foregroundAppName" # Ljava/lang/String; */
/* .line 532 */
v0 = this.mBackgroundHandler;
/* const/16 v1, 0x9 */
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 533 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 534 */
return;
} // .end method
public void notifyGestureEvent ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "success" # Z */
/* .param p3, "label" # I */
/* .line 511 */
v0 = this.mBackgroundHandler;
int v1 = 6; // const/4 v1, 0x6
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 512 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 513 */
/* iput p3, v0, Landroid/os/Message;->arg2:I */
/* .line 514 */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 515 */
return;
} // .end method
public void notifyScreenOnUnBlocker ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "screenOnBlockTime" # I */
/* .param p2, "delayMiles" # I */
/* .line 233 */
v0 = this.mBackgroundHandler;
int v1 = 3; // const/4 v1, 0x3
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 234 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 235 */
/* iput p2, v0, Landroid/os/Message;->arg2:I */
/* .line 236 */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 237 */
return;
} // .end method
public void notifyTofPowerState ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "wakeup" # Z */
/* .line 518 */
v0 = this.mBackgroundHandler;
/* new-instance v1, Ljava/lang/Boolean; */
/* invoke-direct {v1, p1}, Ljava/lang/Boolean;-><init>(Z)V */
int v2 = 7; // const/4 v2, 0x7
(( android.os.Handler ) v0 ).obtainMessage ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 520 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 521 */
return;
} // .end method
public void notifyUserAttentionChanged ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "latencyMs" # I */
/* .param p2, "result" # I */
/* .line 297 */
v0 = this.mBackgroundHandler;
int v1 = 5; // const/4 v1, 0x5
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 298 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 299 */
/* iput p2, v0, Landroid/os/Message;->arg2:I */
/* .line 300 */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 301 */
return;
} // .end method
public void notifyWakefulnessChangedLocked ( Integer p0, Long p1, Integer p2 ) {
/* .locals 7 */
/* .param p1, "wakefulness" # I */
/* .param p2, "eventTime" # J */
/* .param p4, "reason" # I */
/* .line 188 */
/* new-instance v6, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
/* move-object v0, v6 */
/* move-object v1, p0 */
/* move v2, p1 */
/* move v3, p4 */
/* move-wide v4, p2 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;-><init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;IIJ)V */
/* .line 189 */
/* .local v0, "powerEvent":Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
v1 = this.mBackgroundHandler;
int v2 = 1; // const/4 v2, 0x1
(( android.os.Handler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 190 */
/* .local v1, "message":Landroid/os/Message; */
this.obj = v0;
/* .line 191 */
v2 = this.mBackgroundHandler;
(( android.os.Handler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 192 */
return;
} // .end method
public void notifyWakefulnessCompletedLocked ( Long p0 ) {
/* .locals 2 */
/* .param p1, "wakefulnessCompletedTime" # J */
/* .line 269 */
v0 = this.mBackgroundHandler;
int v1 = 2; // const/4 v1, 0x2
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 270 */
/* .local v0, "message":Landroid/os/Message; */
/* new-instance v1, Ljava/lang/Long; */
/* invoke-direct {v1, p1, p2}, Ljava/lang/Long;-><init>(J)V */
this.obj = v1;
/* .line 271 */
v1 = this.mBackgroundHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 272 */
return;
} // .end method
