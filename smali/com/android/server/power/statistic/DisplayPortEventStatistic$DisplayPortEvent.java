public class com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent {
	 /* .source "DisplayPortEventStatistic.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/power/statistic/DisplayPortEventStatistic; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "DisplayPortEvent" */
} // .end annotation
/* # instance fields */
private Integer mFrameRate;
private Boolean mIsConnected;
private Long mPhysicalDisplayId;
private java.lang.String mProductName;
private java.lang.String mResolution;
/* # direct methods */
 com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ( ) {
/* .locals 0 */
/* .line 72 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ( ) {
/* .locals 0 */
/* .param p1, "physicalDisplayId" # J */
/* .param p3, "isConnected" # Z */
/* .param p4, "productName" # Ljava/lang/String; */
/* .param p5, "frameRate" # I */
/* .param p6, "resolution" # Ljava/lang/String; */
/* .line 75 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 76 */
/* iput-wide p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mPhysicalDisplayId:J */
/* .line 77 */
/* iput-boolean p3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mIsConnected:Z */
/* .line 78 */
this.mProductName = p4;
/* .line 79 */
/* iput p5, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I */
/* .line 80 */
this.mResolution = p6;
/* .line 81 */
return;
} // .end method
/* # virtual methods */
public void copyFrom ( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent p0 ) {
/* .locals 2 */
/* .param p1, "other" # Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent; */
/* .line 124 */
(( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getPhysicalDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getPhysicalDisplayId()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mPhysicalDisplayId:J */
/* .line 125 */
v0 = (( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getIsConnected ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getIsConnected()Z
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mIsConnected:Z */
/* .line 126 */
(( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getProductName ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getProductName()Ljava/lang/String;
this.mProductName = v0;
/* .line 127 */
v0 = (( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getFrameRate ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getFrameRate()I
/* iput v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I */
/* .line 128 */
(( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getResolution ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getResolution()Ljava/lang/String;
this.mResolution = v0;
/* .line 129 */
return;
} // .end method
public Integer getFrameRate ( ) {
/* .locals 1 */
/* .line 108 */
/* iget v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I */
} // .end method
public Boolean getIsConnected ( ) {
/* .locals 1 */
/* .line 92 */
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mIsConnected:Z */
} // .end method
public Long getPhysicalDisplayId ( ) {
/* .locals 2 */
/* .line 84 */
/* iget-wide v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mPhysicalDisplayId:J */
/* return-wide v0 */
} // .end method
public java.lang.String getProductName ( ) {
/* .locals 1 */
/* .line 100 */
v0 = this.mProductName;
} // .end method
public java.lang.String getResolution ( ) {
/* .locals 1 */
/* .line 116 */
v0 = this.mResolution;
} // .end method
public void setFrameRate ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "frameRate" # I */
/* .line 112 */
/* iput p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I */
/* .line 113 */
return;
} // .end method
public void setIsConnected ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isConnected" # Z */
/* .line 96 */
/* iput-boolean p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mIsConnected:Z */
/* .line 97 */
return;
} // .end method
public void setPhysicalDisplayId ( Long p0 ) {
/* .locals 0 */
/* .param p1, "physicalDisplayId" # J */
/* .line 88 */
/* iput-wide p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mPhysicalDisplayId:J */
/* .line 89 */
return;
} // .end method
public void setProductName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "productName" # Ljava/lang/String; */
/* .line 104 */
this.mProductName = p1;
/* .line 105 */
return;
} // .end method
public void setResolution ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "resolution" # Ljava/lang/String; */
/* .line 120 */
this.mResolution = p1;
/* .line 121 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 133 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "[productName="; // const-string v1, "[productName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mProductName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", frameRate="; // const-string v1, ", frameRate="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", resolution="; // const-string v1, ", resolution="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mResolution;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
