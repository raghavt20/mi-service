.class public Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;
.super Ljava/lang/Object;
.source "DimEventStatistic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/statistic/DimEventStatistic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DimEvent"
.end annotation


# instance fields
.field private mDimTime:J

.field private mIsEnterDim:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0
    .param p1, "dimTime"    # J
    .param p3, "isEnterDim"    # Z

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-wide p1, p0, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->mDimTime:J

    .line 80
    iput-boolean p3, p0, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->mIsEnterDim:Z

    .line 81
    return-void
.end method


# virtual methods
.method public getDimTime()J
    .locals 2

    .line 84
    iget-wide v0, p0, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->mDimTime:J

    return-wide v0
.end method

.method public getIsEnterDim()Z
    .locals 1

    .line 92
    iget-boolean v0, p0, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->mIsEnterDim:Z

    return v0
.end method

.method public setDimTime(J)V
    .locals 0
    .param p1, "dimTime"    # J

    .line 88
    iput-wide p1, p0, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->mDimTime:J

    .line 89
    return-void
.end method

.method public setIsEnterDim(Z)V
    .locals 0
    .param p1, "isEnterDim"    # Z

    .line 96
    iput-boolean p1, p0, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->mIsEnterDim:Z

    .line 97
    return-void
.end method
