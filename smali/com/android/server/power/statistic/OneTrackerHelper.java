public class com.android.server.power.statistic.OneTrackerHelper {
	 /* .source "OneTrackerHelper.java" */
	 /* # static fields */
	 private static final java.lang.String APP_ID;
	 private static final java.lang.String EVENT_NAME;
	 private static final Integer FLAG_NON_ANONYMOUS;
	 private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
	 private static final java.lang.String PACKAGE;
	 private static final java.lang.String SERVICE_PACKAGE_NAME;
	 public static final java.lang.String TAG;
	 private static final java.lang.String TRACK_EVENT;
	 /* # instance fields */
	 private android.content.Context mContext;
	 /* # direct methods */
	 public com.android.server.power.statistic.OneTrackerHelper ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 23 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 24 */
		 this.mContext = p1;
		 /* .line 25 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public android.content.Intent getTrackEventIntent ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
		 /* .locals 2 */
		 /* .param p1, "appId" # Ljava/lang/String; */
		 /* .param p2, "eventName" # Ljava/lang/String; */
		 /* .param p3, "packageName" # Ljava/lang/String; */
		 /* .line 28 */
		 /* new-instance v0, Landroid/content/Intent; */
		 final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
		 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 /* .line 29 */
		 /* .local v0, "intent":Landroid/content/Intent; */
		 final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
		 (( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 30 */
		 final String v1 = "APP_ID"; // const-string v1, "APP_ID"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 31 */
		 final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 32 */
		 final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 34 */
		 int v1 = 3; // const/4 v1, 0x3
		 (( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
		 /* .line 35 */
	 } // .end method
	 public void reportTrackEventByIntent ( android.content.Intent p0 ) {
		 /* .locals 3 */
		 /* .param p1, "intent" # Landroid/content/Intent; */
		 /* .line 40 */
		 try { // :try_start_0
			 v0 = this.mContext;
			 v1 = android.os.UserHandle.CURRENT;
			 (( android.content.Context ) v0 ).startServiceAsUser ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 43 */
			 /* .line 41 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 42 */
			 /* .local v0, "e":Ljava/lang/Exception; */
			 /* new-instance v1, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
			 /* const-string/jumbo v2, "send one tracker by intent fail! " */
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 final String v2 = "Power_OneTrackerHelper"; // const-string v2, "Power_OneTrackerHelper"
			 android.util.Slog .e ( v2,v1 );
			 /* .line 44 */
		 } // .end local v0 # "e":Ljava/lang/Exception;
	 } // :goto_0
	 return;
} // .end method
