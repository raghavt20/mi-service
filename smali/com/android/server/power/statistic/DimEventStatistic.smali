.class public Lcom/android/server/power/statistic/DimEventStatistic;
.super Ljava/lang/Object;
.source "DimEventStatistic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final ENTER_DIM_DURATION:Ljava/lang/String; = "enter_dim_duration"

.field private static final ENTER_DIM_TIMES:Ljava/lang/String; = "enter_dim_times"

.field private static final EVENT_NAME:Ljava/lang/String; = "dim_statistic"

.field private static final TAG:Ljava/lang/String; = "DimEventStatistic"


# instance fields
.field private final mAppId:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mEnterDimTimes:I

.field private mLastEnterDimTime:J

.field private final mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

.field private mSumDimDuration:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 9
    nop

    .line 10
    const-string v0, "debug.miui.dp.statistic.dbg"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    sput-boolean v1, Lcom/android/server/power/statistic/DimEventStatistic;->DEBUG:Z

    .line 9
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/power/statistic/OneTrackerHelper;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "oneTrackerHelper"    # Lcom/android/server/power/statistic/OneTrackerHelper;

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mAppId:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mContext:Landroid/content/Context;

    .line 25
    iput-object p3, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    .line 26
    return-void
.end method

.method private recordDimDuration(J)V
    .locals 4
    .param p1, "now"    # J

    .line 67
    iget-wide v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mLastEnterDimTime:J

    sub-long v0, p1, v0

    .line 68
    .local v0, "duration":J
    iget-wide v2, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mSumDimDuration:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mSumDimDuration:J

    .line 69
    return-void
.end method

.method private resetDimStateInfo()V
    .locals 2

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mEnterDimTimes:I

    .line 63
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mSumDimDuration:J

    .line 64
    return-void
.end method


# virtual methods
.method public notifyDimStateChanged(Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;)V
    .locals 3
    .param p1, "dimEvent"    # Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;

    .line 29
    sget-boolean v0, Lcom/android/server/power/statistic/DimEventStatistic;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyDimStateChanged: isEnterDim: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 31
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getIsEnterDim()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enter dim time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 32
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getDimTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 30
    const-string v1, "DimEventStatistic"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getIsEnterDim()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getDimTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mLastEnterDimTime:J

    .line 36
    iget v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mEnterDimTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mEnterDimTimes:I

    goto :goto_0

    .line 38
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getDimTime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DimEventStatistic;->recordDimDuration(J)V

    .line 40
    :goto_0
    return-void
.end method

.method public reportDimStateByIntent()V
    .locals 5

    .line 43
    const-string v0, "DimEventStatistic"

    iget-object v1, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    iget-object v2, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mAppId:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mContext:Landroid/content/Context;

    .line 44
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 43
    const-string v4, "dim_statistic"

    invoke-virtual {v1, v2, v4, v3}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 45
    .local v1, "intent":Landroid/content/Intent;
    iget v2, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mEnterDimTimes:I

    if-eqz v2, :cond_0

    .line 46
    const-string v3, "enter_dim_times"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 47
    const-string v2, "enter_dim_duration"

    iget-wide v3, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mSumDimDuration:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 51
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    invoke-virtual {v2, v1}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    goto :goto_0

    .line 52
    :catch_0
    move-exception v2

    .line 53
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportDimStateByIntent fail! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    sget-boolean v2, Lcom/android/server/power/statistic/DimEventStatistic;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 56
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportDimStateByIntent: data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_1
    invoke-direct {p0}, Lcom/android/server/power/statistic/DimEventStatistic;->resetDimStateInfo()V

    .line 59
    return-void
.end method
