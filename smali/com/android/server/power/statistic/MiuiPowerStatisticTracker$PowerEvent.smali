.class final Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;
.super Ljava/lang/Object;
.source "MiuiPowerStatisticTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PowerEvent"
.end annotation


# instance fields
.field mBlockedScreenLatency:I

.field mDisplayStateChangedLatency:I

.field mEventTime:J

.field mIsDelayByFace:Z

.field mIsQuickBreak:Z

.field mReason:I

.field mWakefulness:I

.field mWakefulnessChangedLatency:I

.field final synthetic this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;


# direct methods
.method constructor <init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)V
    .locals 0

    .line 630
    iput-object p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;IIJ)V
    .locals 0
    .param p2, "wakefulness"    # I
    .param p3, "reason"    # I
    .param p4, "eventTime"    # J

    .line 636
    iput-object p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 637
    iput p2, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulness:I

    .line 638
    iput p3, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mReason:I

    .line 639
    iput-wide p4, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mEventTime:J

    .line 640
    return-void
.end method

.method constructor <init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V
    .locals 0
    .param p2, "powerEvent"    # Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    .line 632
    iput-object p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633
    invoke-virtual {p0, p2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->copyFrom(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V

    .line 634
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 3

    .line 719
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulness:I

    .line 720
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulnessChangedLatency:I

    .line 721
    iput v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mBlockedScreenLatency:I

    .line 722
    iput v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mDisplayStateChangedLatency:I

    .line 723
    iput-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsDelayByFace:Z

    .line 724
    iput-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsQuickBreak:Z

    .line 725
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mEventTime:J

    .line 726
    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mReason:I

    .line 727
    return-void
.end method

.method public copyFrom(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V
    .locals 2
    .param p1, "other"    # Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    .line 730
    iget v0, p1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulness:I

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulness:I

    .line 731
    iget v0, p1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulnessChangedLatency:I

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulnessChangedLatency:I

    .line 732
    iget v0, p1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mBlockedScreenLatency:I

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mBlockedScreenLatency:I

    .line 733
    iget v0, p1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mDisplayStateChangedLatency:I

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mDisplayStateChangedLatency:I

    .line 734
    iget-boolean v0, p1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsDelayByFace:Z

    iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsDelayByFace:Z

    .line 735
    iget-boolean v0, p1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsQuickBreak:Z

    iput-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsQuickBreak:Z

    .line 736
    iget-wide v0, p1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mEventTime:J

    iput-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mEventTime:J

    .line 737
    iget v0, p1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mReason:I

    iput v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mReason:I

    .line 738
    return-void
.end method

.method public getBlockedScreenLatency()F
    .locals 1

    .line 695
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mBlockedScreenLatency:I

    int-to-float v0, v0

    return v0
.end method

.method public getDisplayStateChangedLatency()I
    .locals 1

    .line 707
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mDisplayStateChangedLatency:I

    return v0
.end method

.method public getEventTime()J
    .locals 2

    .line 671
    iget-wide v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mEventTime:J

    return-wide v0
.end method

.method public getReason()I
    .locals 1

    .line 679
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mReason:I

    return v0
.end method

.method public getWakefulness()I
    .locals 1

    .line 667
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulness:I

    return v0
.end method

.method public getWakefulnessChangedLatency()F
    .locals 1

    .line 655
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulnessChangedLatency:I

    int-to-float v0, v0

    return v0
.end method

.method public isDelayByFace()Z
    .locals 1

    .line 711
    iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsDelayByFace:Z

    return v0
.end method

.method public isQuickBreak()Z
    .locals 1

    .line 687
    iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsQuickBreak:Z

    return v0
.end method

.method public isScreenOff()Z
    .locals 2

    .line 651
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulness:I

    if-eqz v0, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isScreenOn()Z
    .locals 2

    .line 647
    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulness:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isValid()Z
    .locals 1

    .line 643
    iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsDelayByFace:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulnessChangedLatency:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsQuickBreak:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setBlockedScreenLatency(I)V
    .locals 0
    .param p1, "latency"    # I

    .line 699
    iput p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mBlockedScreenLatency:I

    .line 700
    return-void
.end method

.method public setDelayByFace(Z)V
    .locals 0
    .param p1, "delayByFace"    # Z

    .line 715
    iput-boolean p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsDelayByFace:Z

    .line 716
    return-void
.end method

.method public setDisplayStateChangedLatency(I)V
    .locals 0
    .param p1, "latency"    # I

    .line 703
    iput p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mDisplayStateChangedLatency:I

    .line 704
    return-void
.end method

.method public setEventTime(J)V
    .locals 0
    .param p1, "eventTime"    # J

    .line 675
    iput-wide p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mEventTime:J

    .line 676
    return-void
.end method

.method public setQuickBreak(Z)V
    .locals 0
    .param p1, "isQuickBreak"    # Z

    .line 691
    iput-boolean p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsQuickBreak:Z

    .line 692
    return-void
.end method

.method public setReason(I)V
    .locals 0
    .param p1, "reason"    # I

    .line 683
    iput p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mReason:I

    .line 684
    return-void
.end method

.method public setWakefulness(I)V
    .locals 0
    .param p1, "wakefulness"    # I

    .line 663
    iput p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulness:I

    .line 664
    return-void
.end method

.method public setWakefulnessChangedLatency(I)V
    .locals 0
    .param p1, "latency"    # I

    .line 659
    iput p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulnessChangedLatency:I

    .line 660
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 742
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "powerEvent:[wakefulness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulness:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; blockedScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mBlockedScreenLatency:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; powerStateChanged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mDisplayStateChangedLatency:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; wakefulnessChanged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mWakefulnessChangedLatency:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; isDelayByFace="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsDelayByFace:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; isQuickBreak="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mIsQuickBreak:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mReason:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; eventTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;->mEventTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
