class com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerStatisticHandler extends android.os.Handler {
	 /* .source "MiuiPowerStatisticTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/power/statistic/MiuiPowerStatisticTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PowerStatisticHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.power.statistic.MiuiPowerStatisticTracker this$0; //synthetic
/* # direct methods */
public com.android.server.power.statistic.MiuiPowerStatisticTracker$PowerStatisticHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 322 */
this.this$0 = p1;
/* .line 323 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 324 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 328 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 358 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.power.statistic.MiuiPowerStatisticTracker .-$$Nest$fgetmDimEventStatistic ( v0 );
v1 = this.obj;
/* check-cast v1, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent; */
(( com.android.server.power.statistic.DimEventStatistic ) v0 ).notifyDimStateChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/DimEventStatistic;->notifyDimStateChanged(Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;)V
/* .line 359 */
/* goto/16 :goto_1 */
/* .line 361 */
/* :pswitch_1 */
v0 = this.this$0;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).dumpLocal ( ); // invoke-virtual {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->dumpLocal()V
/* .line 362 */
v0 = this.this$0;
com.android.server.power.statistic.MiuiPowerStatisticTracker .-$$Nest$fgetmTofEventStatistic ( v0 );
(( com.android.server.power.statistic.TofEventStatistic ) v0 ).dumpLocal ( ); // invoke-virtual {v0}, Lcom/android/server/power/statistic/TofEventStatistic;->dumpLocal()V
/* .line 363 */
/* goto/16 :goto_1 */
/* .line 355 */
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.power.statistic.MiuiPowerStatisticTracker .-$$Nest$fgetmDisplayPortEventStatistic ( v0 );
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
(( com.android.server.power.statistic.DisplayPortEventStatistic ) v0 ).handleForegroundAppChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->handleForegroundAppChanged(Ljava/lang/String;)V
/* .line 356 */
/* .line 351 */
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.power.statistic.MiuiPowerStatisticTracker .-$$Nest$fgetmDisplayPortEventStatistic ( v0 );
v1 = this.obj;
/* check-cast v1, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent; */
(( com.android.server.power.statistic.DisplayPortEventStatistic ) v0 ).handleConnectedStateChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->handleConnectedStateChanged(Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V
/* .line 353 */
/* .line 348 */
/* :pswitch_4 */
v0 = this.this$0;
com.android.server.power.statistic.MiuiPowerStatisticTracker .-$$Nest$fgetmTofEventStatistic ( v0 );
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
(( com.android.server.power.statistic.TofEventStatistic ) v0 ).notifyTofPowerState ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/TofEventStatistic;->notifyTofPowerState(Z)V
/* .line 349 */
/* .line 345 */
/* :pswitch_5 */
v0 = this.this$0;
com.android.server.power.statistic.MiuiPowerStatisticTracker .-$$Nest$fgetmTofEventStatistic ( v0 );
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
/* iget v2, p1, Landroid/os/Message;->arg1:I */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_0 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* iget v2, p1, Landroid/os/Message;->arg2:I */
(( com.android.server.power.statistic.TofEventStatistic ) v0 ).notifyGestureEvent ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Lcom/android/server/power/statistic/TofEventStatistic;->notifyGestureEvent(Ljava/lang/String;ZI)V
/* .line 346 */
/* .line 342 */
/* :pswitch_6 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* iget v2, p1, Landroid/os/Message;->arg2:I */
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).handleUserAttentionChanged ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleUserAttentionChanged(II)V
/* .line 343 */
/* .line 339 */
/* :pswitch_7 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).handleDisplayStateChangedLatencyLocked ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleDisplayStateChangedLatencyLocked(I)V
/* .line 340 */
/* .line 336 */
/* :pswitch_8 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* iget v2, p1, Landroid/os/Message;->arg2:I */
com.android.server.power.statistic.MiuiPowerStatisticTracker .-$$Nest$mhandleScreenOnUnBlocker ( v0,v1,v2 );
/* .line 337 */
/* .line 333 */
/* :pswitch_9 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).handleWakefulnessCompleted ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleWakefulnessCompleted(J)V
/* .line 334 */
/* .line 330 */
/* :pswitch_a */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent; */
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).handleWakefulnessChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleWakefulnessChanged(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V
/* .line 331 */
/* nop */
/* .line 367 */
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
