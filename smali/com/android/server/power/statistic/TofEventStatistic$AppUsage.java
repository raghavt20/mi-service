class com.android.server.power.statistic.TofEventStatistic$AppUsage {
	 /* .source "TofEventStatistic.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/power/statistic/TofEventStatistic; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AppUsage" */
} // .end annotation
/* # instance fields */
private Integer mFailCircleNum;
private Integer mFailDoubleTapNum;
private Integer mFailDownNum;
private Integer mFailLeftNum;
private Integer mFailRightNum;
private Integer mFailUpNum;
private java.lang.String mPackageName;
private Integer mSuccessCircleNum;
private Integer mSuccessDoubleTapNum;
private Integer mSuccessDownNum;
private Integer mSuccessLeftNum;
private Integer mSuccessRightNum;
private Integer mSuccessUpNum;
private java.lang.String mUsageCount;
final com.android.server.power.statistic.TofEventStatistic this$0; //synthetic
/* # direct methods */
public com.android.server.power.statistic.TofEventStatistic$AppUsage ( ) {
/* .locals 0 */
/* .line 207 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 209 */
return;
} // .end method
public com.android.server.power.statistic.TofEventStatistic$AppUsage ( ) {
/* .locals 0 */
/* .param p2, "mPackageName" # Ljava/lang/String; */
/* .param p3, "mUsageCount" # Ljava/lang/String; */
/* .line 211 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 212 */
this.mPackageName = p2;
/* .line 213 */
this.mUsageCount = p3;
/* .line 214 */
return;
} // .end method
/* # virtual methods */
public Integer getFailCircleNum ( ) {
/* .locals 1 */
/* .line 297 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I */
} // .end method
public Integer getFailDoubleTapNum ( ) {
/* .locals 1 */
/* .line 305 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I */
} // .end method
public Integer getFailDownNum ( ) {
/* .locals 1 */
/* .line 273 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I */
} // .end method
public Integer getFailLeftNum ( ) {
/* .locals 1 */
/* .line 281 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I */
} // .end method
public Integer getFailRightNum ( ) {
/* .locals 1 */
/* .line 289 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I */
} // .end method
public Integer getFailUpNum ( ) {
/* .locals 1 */
/* .line 265 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I */
} // .end method
public java.lang.String getPackageName ( ) {
/* .locals 1 */
/* .line 313 */
v0 = this.mPackageName;
} // .end method
public Integer getSuccessCircleNum ( ) {
/* .locals 1 */
/* .line 249 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I */
} // .end method
public Integer getSuccessDoubleTapNum ( ) {
/* .locals 1 */
/* .line 257 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I */
} // .end method
public Integer getSuccessDownNum ( ) {
/* .locals 1 */
/* .line 225 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I */
} // .end method
public Integer getSuccessLeftNum ( ) {
/* .locals 1 */
/* .line 233 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I */
} // .end method
public Integer getSuccessRightNum ( ) {
/* .locals 1 */
/* .line 241 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I */
} // .end method
public Integer getSuccessUpNum ( ) {
/* .locals 1 */
/* .line 217 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I */
} // .end method
public java.lang.String getUsageCount ( ) {
/* .locals 1 */
/* .line 321 */
v0 = this.mUsageCount;
} // .end method
public void reset ( ) {
/* .locals 1 */
/* .line 329 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I */
/* .line 330 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I */
/* .line 331 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I */
/* .line 332 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I */
/* .line 333 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I */
/* .line 334 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I */
/* .line 335 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I */
/* .line 336 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I */
/* .line 337 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I */
/* .line 338 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I */
/* .line 339 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I */
/* .line 340 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I */
/* .line 341 */
return;
} // .end method
public void setFailCircleNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mFailCircleNum" # I */
/* .line 301 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I */
/* .line 302 */
return;
} // .end method
public void setFailDoubleTapNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mFailDoubleTapNum" # I */
/* .line 309 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I */
/* .line 310 */
return;
} // .end method
public void setFailDownNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mFailDownNum" # I */
/* .line 277 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I */
/* .line 278 */
return;
} // .end method
public void setFailLeftNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mFailLeftNum" # I */
/* .line 285 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I */
/* .line 286 */
return;
} // .end method
public void setFailRightNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mFailRightNum" # I */
/* .line 293 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I */
/* .line 294 */
return;
} // .end method
public void setFailUpNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mFailUpNum" # I */
/* .line 269 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I */
/* .line 270 */
return;
} // .end method
public void setGestureEvent ( Boolean p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "success" # Z */
/* .param p2, "label" # I */
/* .line 344 */
/* packed-switch p2, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 381 */
/* :pswitch_1 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 382 */
	 /* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I */
	 /* .line 384 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I */
/* .line 386 */
/* .line 374 */
/* :pswitch_2 */
if ( p1 != null) { // if-eqz p1, :cond_1
	 /* .line 375 */
	 /* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I */
	 /* .line 377 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I */
/* .line 379 */
/* .line 360 */
/* :pswitch_3 */
if ( p1 != null) { // if-eqz p1, :cond_2
	 /* .line 361 */
	 /* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I */
	 /* .line 363 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I */
/* .line 365 */
/* .line 367 */
/* :pswitch_4 */
if ( p1 != null) { // if-eqz p1, :cond_3
	 /* .line 368 */
	 /* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I */
	 /* .line 370 */
} // :cond_3
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I */
/* .line 372 */
/* .line 353 */
/* :pswitch_5 */
if ( p1 != null) { // if-eqz p1, :cond_4
	 /* .line 354 */
	 /* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I */
	 /* .line 356 */
} // :cond_4
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I */
/* .line 358 */
/* .line 346 */
/* :pswitch_6 */
if ( p1 != null) { // if-eqz p1, :cond_5
	 /* .line 347 */
	 /* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I */
	 /* .line 349 */
} // :cond_5
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I */
/* .line 351 */
/* nop */
/* .line 390 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void setPackageName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mPackageName" # Ljava/lang/String; */
/* .line 317 */
this.mPackageName = p1;
/* .line 318 */
return;
} // .end method
public void setSuccessCircleNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mSuccessCircleNum" # I */
/* .line 253 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I */
/* .line 254 */
return;
} // .end method
public void setSuccessDoubleTapNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mSuccessDoubleTapNum" # I */
/* .line 261 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I */
/* .line 262 */
return;
} // .end method
public void setSuccessDownNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mSuccessDownNum" # I */
/* .line 229 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I */
/* .line 230 */
return;
} // .end method
public void setSuccessLeftNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mSuccessLeftNum" # I */
/* .line 237 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I */
/* .line 238 */
return;
} // .end method
public void setSuccessRightNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mSuccessRightNum" # I */
/* .line 245 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I */
/* .line 246 */
return;
} // .end method
public void setSuccessUpNum ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mSuccessUpNum" # I */
/* .line 221 */
/* iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I */
/* .line 222 */
return;
} // .end method
public void setUsageCount ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mUsageCount" # Ljava/lang/String; */
/* .line 325 */
this.mUsageCount = p1;
/* .line 326 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 7 */
/* .line 394 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "{app:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",success:{up:"; // const-string v1, ",success:{up:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",down:"; // const-string v1, ",down:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ",left:"; // const-string v2, ",left:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ",right:"; // const-string v3, ",right:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ",circle:"; // const-string v4, ",circle:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ",doubletap:"; // const-string v5, ",doubletap:"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v6, "},fail:{up:" */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
