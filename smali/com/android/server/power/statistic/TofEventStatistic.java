public class com.android.server.power.statistic.TofEventStatistic {
	 /* .source "TofEventStatistic.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/power/statistic/TofEventStatistic$AppUsage; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String AON_GESTURE_CUE_TONE;
public static final java.lang.String FEATURE_AON_GESTURE_SUPPORT;
private static final java.lang.String FEATURE_TOF_GESTURE_SUPPORT;
private static final java.lang.String FEATURE_TOF_PROXIMITY_SUPPORT;
private static final java.lang.String KEY_AON_APP_USAGE_FREQUENCY;
private static final java.lang.String KEY_AON_GESTURE_CUE_TONE_ENABLE;
private static final java.lang.String KEY_AON_GESTURE_ENABLE;
private static final java.lang.String KEY_AON_GESTURE_USAGE_COUNT;
private static final java.lang.String KEY_TOF_APP_USAGE_FREQUENCY;
private static final java.lang.String KEY_TOF_GESTURE_CUE_TONE_ENABLE;
private static final java.lang.String KEY_TOF_GESTURE_ENABLE;
private static final java.lang.String KEY_TOF_GESTURE_USAGE_COUNT;
private static final java.lang.String KEY_TOF_SCREEN_OFF_ENABLE;
private static final java.lang.String KEY_TOF_SCREEN_OFF_USAGE_COUNT;
private static final java.lang.String KEY_TOF_SCREEN_ON_ENABLE;
private static final java.lang.String KEY_TOF_SCREEN_ON_USAGE_COUNT;
private static final Boolean SUPPORT_AON_GESTURE;
private static final Boolean SUPPORT_TOF_GESTURE;
private static final Boolean SUPPORT_TOF_PROXIMITY;
public static final java.lang.String TAG;
public static final java.lang.String TOF_EVENT_NAME;
private static final java.lang.String TOF_GESTURE_CUE_TONE;
public static final Integer TOF_GESTURE_DOUBLE_PRESS;
public static final Integer TOF_GESTURE_DOWN;
public static final Integer TOF_GESTURE_DRAW_CIRCLE;
public static final Integer TOF_GESTURE_LEFT;
public static final Integer TOF_GESTURE_RIGHT;
public static final Integer TOF_GESTURE_UP;
/* # instance fields */
private Boolean mAonGestureCueToneEnable;
private Boolean mAonGestureEnable;
private java.lang.String mAppId;
private android.content.Context mContext;
private com.android.server.power.statistic.OneTrackerHelper mOneTrackerHelper;
private java.util.Map mTofAppUsageEventsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mTofGestureCueToneEnable;
private Boolean mTofGestureEnable;
private Integer mTofGestureUsageCount;
private Boolean mTofScreenOffEnable;
private Integer mTofScreenOffUsageCount;
private Boolean mTofScreenOnEnable;
private Integer mTofScreenOnUsageCount;
/* # direct methods */
static com.android.server.power.statistic.TofEventStatistic ( ) {
/* .locals 2 */
/* .line 46 */
final String v0 = "config_tof_proximity_available"; // const-string v0, "config_tof_proximity_available"
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.power.statistic.TofEventStatistic.SUPPORT_TOF_PROXIMITY = (v0!= 0);
/* .line 47 */
final String v0 = "config_tof_gesture_available"; // const-string v0, "config_tof_gesture_available"
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.power.statistic.TofEventStatistic.SUPPORT_TOF_GESTURE = (v0!= 0);
/* .line 48 */
final String v0 = "config_aon_gesture_available"; // const-string v0, "config_aon_gesture_available"
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.power.statistic.TofEventStatistic.SUPPORT_AON_GESTURE = (v0!= 0);
return;
} // .end method
public com.android.server.power.statistic.TofEventStatistic ( ) {
/* .locals 1 */
/* .param p1, "appId" # Ljava/lang/String; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "oneTrackerHelper" # Lcom/android/server/power/statistic/OneTrackerHelper; */
/* .line 70 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 59 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mTofAppUsageEventsMap = v0;
/* .line 71 */
this.mAppId = p1;
/* .line 72 */
this.mContext = p2;
/* .line 73 */
this.mOneTrackerHelper = p3;
/* .line 74 */
return;
} // .end method
private java.util.List getTofAppUsageEvents ( java.util.Map p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;", */
/* ">;)", */
/* "Ljava/util/List;" */
/* } */
} // .end annotation
/* .line 146 */
/* .local p1, "appUsageEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 147 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 148 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;>;" */
/* check-cast v3, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage; */
/* .line 149 */
/* .local v3, "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage; */
/* .line 150 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;>;"
} // .end local v3 # "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;
/* .line 151 */
} // :cond_0
} // .end method
private java.lang.String getTofAppUsageEventsString ( ) {
/* .locals 3 */
/* .line 137 */
final String v0 = ""; // const-string v0, ""
/* .line 138 */
/* .local v0, "appUsage":Ljava/lang/String; */
v1 = this.mTofAppUsageEventsMap;
/* invoke-direct {p0, v1}, Lcom/android/server/power/statistic/TofEventStatistic;->getTofAppUsageEvents(Ljava/util/Map;)Ljava/util/List; */
/* .line 139 */
v2 = /* .local v1, "appUsageList":Ljava/util/List; */
/* if-lez v2, :cond_0 */
/* .line 140 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 142 */
} // :cond_0
} // .end method
private void resetTofEventData ( ) {
/* .locals 2 */
/* .line 124 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnEnable:Z */
/* .line 125 */
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffEnable:Z */
/* .line 126 */
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureEnable:Z */
/* .line 127 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureCueToneEnable:Z */
/* .line 128 */
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureEnable:Z */
/* .line 129 */
/* iput-boolean v1, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureCueToneEnable:Z */
/* .line 130 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I */
/* .line 131 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I */
/* .line 132 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I */
/* .line 133 */
v0 = this.mTofAppUsageEventsMap;
/* .line 134 */
return;
} // .end method
/* # virtual methods */
public void dumpLocal ( ) {
/* .locals 3 */
/* .line 179 */
/* new-instance v0, Ljava/lang/StringBuffer; */
final String v1 = "MiuiToFStaticTracker:"; // const-string v1, "MiuiToFStaticTracker:"
/* invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V */
/* .line 180 */
/* .local v0, "stringBuffer":Ljava/lang/StringBuffer; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nScreenOnEnable:"; // const-string v2, "\nScreenOnEnable:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 181 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nScreenOffEnable:"; // const-string v2, "\nScreenOffEnable:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 182 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nGestureEnable:"; // const-string v2, "\nGestureEnable:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 183 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nScreenOnCount:"; // const-string v2, "\nScreenOnCount:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 184 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nScreenOffCount:"; // const-string v2, "\nScreenOffCount:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 185 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nGestureEventCount:"; // const-string v2, "\nGestureEventCount:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 186 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nappUsage:"; // const-string v2, "\nappUsage:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTofAppUsageEventsMap;
/* invoke-direct {p0, v2}, Lcom/android/server/power/statistic/TofEventStatistic;->getTofAppUsageEvents(Ljava/util/Map;)Ljava/util/List; */
(( java.lang.Object ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 188 */
final String v1 = "MiuiTofStatisticTracker"; // const-string v1, "MiuiTofStatisticTracker"
(( java.lang.StringBuffer ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 189 */
return;
} // .end method
public void notifyGestureEvent ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "success" # Z */
/* .param p3, "label" # I */
/* .line 155 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I */
/* .line 157 */
/* if-nez p1, :cond_0 */
/* .line 158 */
return;
/* .line 161 */
} // :cond_0
v0 = v0 = this.mTofAppUsageEventsMap;
/* if-nez v0, :cond_1 */
/* .line 162 */
/* new-instance v0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;-><init>(Lcom/android/server/power/statistic/TofEventStatistic;)V */
/* .line 163 */
/* .local v0, "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage; */
(( com.android.server.power.statistic.TofEventStatistic$AppUsage ) v0 ).setPackageName ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->setPackageName(Ljava/lang/String;)V
/* .line 164 */
v1 = this.mTofAppUsageEventsMap;
/* .line 166 */
} // .end local v0 # "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;
} // :cond_1
v0 = this.mTofAppUsageEventsMap;
/* check-cast v0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage; */
/* .line 167 */
/* .restart local v0 # "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage; */
(( com.android.server.power.statistic.TofEventStatistic$AppUsage ) v0 ).setGestureEvent ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->setGestureEvent(ZI)V
/* .line 168 */
return;
} // .end method
public void notifyTofPowerState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "wakeup" # Z */
/* .line 171 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 172 */
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I */
/* .line 174 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I */
/* .line 176 */
} // :goto_0
return;
} // .end method
public void reportTofEventByIntent ( ) {
/* .locals 8 */
/* .line 77 */
final String v0 = "MiuiTofStatisticTracker"; // const-string v0, "MiuiTofStatisticTracker"
/* sget-boolean v1, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_TOF_GESTURE:Z */
/* if-nez v1, :cond_0 */
/* sget-boolean v2, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_TOF_PROXIMITY:Z */
/* if-nez v2, :cond_0 */
/* sget-boolean v2, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_AON_GESTURE:Z */
/* if-nez v2, :cond_0 */
/* .line 78 */
return;
/* .line 80 */
} // :cond_0
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "miui_tof_screen_on"; // const-string v3, "miui_tof_screen_on"
int v4 = 0; // const/4 v4, 0x0
int v5 = -2; // const/4 v5, -0x2
v2 = android.provider.Settings$Secure .getIntForUser ( v2,v3,v4,v5 );
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v3 */
} // :cond_1
/* move v2, v4 */
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnEnable:Z */
/* .line 82 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "miui_tof_screen_off"; // const-string v6, "miui_tof_screen_off"
v2 = android.provider.Settings$Secure .getIntForUser ( v2,v6,v4,v5 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* move v2, v3 */
} // :cond_2
/* move v2, v4 */
} // :goto_1
/* iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffEnable:Z */
/* .line 84 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "miui_tof_gesture"; // const-string v6, "miui_tof_gesture"
v2 = android.provider.Settings$Secure .getIntForUser ( v2,v6,v4,v5 );
if ( v2 != null) { // if-eqz v2, :cond_3
/* move v2, v3 */
} // :cond_3
/* move v2, v4 */
} // :goto_2
/* iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureEnable:Z */
/* .line 86 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "miui_aon_gesture"; // const-string v6, "miui_aon_gesture"
v2 = android.provider.Settings$Secure .getIntForUser ( v2,v6,v4,v5 );
if ( v2 != null) { // if-eqz v2, :cond_4
/* move v2, v3 */
} // :cond_4
/* move v2, v4 */
} // :goto_3
/* iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureEnable:Z */
/* .line 88 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "miui_tof_gesture_cue_tone"; // const-string v6, "miui_tof_gesture_cue_tone"
int v7 = -1; // const/4 v7, -0x1
v2 = android.provider.Settings$Secure .getIntForUser ( v2,v6,v7,v5 );
if ( v2 != null) { // if-eqz v2, :cond_5
/* move v2, v3 */
} // :cond_5
/* move v2, v4 */
} // :goto_4
/* iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureCueToneEnable:Z */
/* .line 90 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "miui_aon_gesture_cue_tone"; // const-string v6, "miui_aon_gesture_cue_tone"
v2 = android.provider.Settings$Secure .getIntForUser ( v2,v6,v7,v5 );
if ( v2 != null) { // if-eqz v2, :cond_6
/* move v4, v3 */
} // :cond_6
/* iput-boolean v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureCueToneEnable:Z */
/* .line 92 */
v2 = this.mOneTrackerHelper;
v3 = this.mAppId;
v4 = this.mContext;
/* .line 93 */
(( android.content.Context ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
/* .line 92 */
/* const-string/jumbo v5, "tof_statistic" */
(( com.android.server.power.statistic.OneTrackerHelper ) v2 ).getTrackEventIntent ( v3, v5, v4 ); // invoke-virtual {v2, v3, v5, v4}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 94 */
/* .local v2, "intent":Landroid/content/Intent; */
/* sget-boolean v3, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_TOF_PROXIMITY:Z */
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 95 */
/* const-string/jumbo v3, "tof_screen_on_enable" */
/* iget-boolean v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnEnable:Z */
(( android.content.Intent ) v2 ).putExtra ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* iget-boolean v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffEnable:Z */
/* .line 96 */
/* const-string/jumbo v5, "tof_screen_off_enable" */
(( android.content.Intent ) v3 ).putExtra ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* iget v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I */
/* .line 97 */
/* const-string/jumbo v5, "tof_screen_on_count" */
(( android.content.Intent ) v3 ).putExtra ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* iget v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I */
/* .line 98 */
/* const-string/jumbo v5, "tof_screen_off_count" */
(( android.content.Intent ) v3 ).putExtra ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 101 */
} // :cond_7
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 102 */
/* const-string/jumbo v1, "tof_gesture_enable" */
/* iget-boolean v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureEnable:Z */
(( android.content.Intent ) v2 ).putExtra ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* iget v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I */
/* .line 103 */
/* const-string/jumbo v4, "tof_gesture_count" */
(( android.content.Intent ) v1 ).putExtra ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* iget-boolean v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureCueToneEnable:Z */
/* .line 104 */
/* const-string/jumbo v4, "tof_gesture_cue_tone_enable" */
(( android.content.Intent ) v1 ).putExtra ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 105 */
/* invoke-direct {p0}, Lcom/android/server/power/statistic/TofEventStatistic;->getTofAppUsageEventsString()Ljava/lang/String; */
/* const-string/jumbo v4, "tof_app_usage_frequency" */
(( android.content.Intent ) v1 ).putExtra ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 108 */
} // :cond_8
/* sget-boolean v1, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_AON_GESTURE:Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 109 */
final String v1 = "aon_gesture_enable"; // const-string v1, "aon_gesture_enable"
/* iget-boolean v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureEnable:Z */
(( android.content.Intent ) v2 ).putExtra ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* iget v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I */
/* .line 110 */
final String v4 = "aon_gesture_count"; // const-string v4, "aon_gesture_count"
(( android.content.Intent ) v1 ).putExtra ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* iget-boolean v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureCueToneEnable:Z */
/* .line 111 */
final String v4 = "aon_gesture_cue_tone_enable"; // const-string v4, "aon_gesture_cue_tone_enable"
(( android.content.Intent ) v1 ).putExtra ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 112 */
/* invoke-direct {p0}, Lcom/android/server/power/statistic/TofEventStatistic;->getTofAppUsageEventsString()Ljava/lang/String; */
final String v4 = "aon_app_usage_frequency"; // const-string v4, "aon_app_usage_frequency"
(( android.content.Intent ) v1 ).putExtra ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 115 */
} // :cond_9
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "reportGestureEventByIntent: intent:"; // const-string v3, "reportGestureEventByIntent: intent:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) v2 ).getExtras ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 116 */
v1 = this.mOneTrackerHelper;
(( com.android.server.power.statistic.OneTrackerHelper ) v1 ).reportTrackEventByIntent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 119 */
/* .line 117 */
/* :catch_0 */
/* move-exception v1 */
/* .line 118 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "reportTofEvent fail! "; // const-string v4, "reportTofEvent fail! "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 120 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_5
/* invoke-direct {p0}, Lcom/android/server/power/statistic/TofEventStatistic;->resetTofEventData()V */
/* .line 121 */
return;
} // .end method
