public class com.android.server.power.statistic.DimEventStatistic {
	 /* .source "DimEventStatistic.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/power/statistic/DimEventStatistic$DimEvent; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String ENTER_DIM_DURATION;
private static final java.lang.String ENTER_DIM_TIMES;
private static final java.lang.String EVENT_NAME;
private static final java.lang.String TAG;
/* # instance fields */
private final java.lang.String mAppId;
private final android.content.Context mContext;
private Integer mEnterDimTimes;
private Long mLastEnterDimTime;
private final com.android.server.power.statistic.OneTrackerHelper mOneTrackerHelper;
private Long mSumDimDuration;
/* # direct methods */
static com.android.server.power.statistic.DimEventStatistic ( ) {
	 /* .locals 2 */
	 /* .line 9 */
	 /* nop */
	 /* .line 10 */
	 final String v0 = "debug.miui.dp.statistic.dbg"; // const-string v0, "debug.miui.dp.statistic.dbg"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getInt ( v0,v1 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v1 = 1; // const/4 v1, 0x1
	 } // :cond_0
	 com.android.server.power.statistic.DimEventStatistic.DEBUG = (v1!= 0);
	 /* .line 9 */
	 return;
} // .end method
public com.android.server.power.statistic.DimEventStatistic ( ) {
	 /* .locals 0 */
	 /* .param p1, "appId" # Ljava/lang/String; */
	 /* .param p2, "context" # Landroid/content/Context; */
	 /* .param p3, "oneTrackerHelper" # Lcom/android/server/power/statistic/OneTrackerHelper; */
	 /* .line 22 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 23 */
	 this.mAppId = p1;
	 /* .line 24 */
	 this.mContext = p2;
	 /* .line 25 */
	 this.mOneTrackerHelper = p3;
	 /* .line 26 */
	 return;
} // .end method
private void recordDimDuration ( Long p0 ) {
	 /* .locals 4 */
	 /* .param p1, "now" # J */
	 /* .line 67 */
	 /* iget-wide v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mLastEnterDimTime:J */
	 /* sub-long v0, p1, v0 */
	 /* .line 68 */
	 /* .local v0, "duration":J */
	 /* iget-wide v2, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mSumDimDuration:J */
	 /* add-long/2addr v2, v0 */
	 /* iput-wide v2, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mSumDimDuration:J */
	 /* .line 69 */
	 return;
} // .end method
private void resetDimStateInfo ( ) {
	 /* .locals 2 */
	 /* .line 62 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mEnterDimTimes:I */
	 /* .line 63 */
	 /* const-wide/16 v0, 0x0 */
	 /* iput-wide v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mSumDimDuration:J */
	 /* .line 64 */
	 return;
} // .end method
/* # virtual methods */
public void notifyDimStateChanged ( com.android.server.power.statistic.DimEventStatistic$DimEvent p0 ) {
	 /* .locals 3 */
	 /* .param p1, "dimEvent" # Lcom/android/server/power/statistic/DimEventStatistic$DimEvent; */
	 /* .line 29 */
	 /* sget-boolean v0, Lcom/android/server/power/statistic/DimEventStatistic;->DEBUG:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 30 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "notifyDimStateChanged: isEnterDim: "; // const-string v1, "notifyDimStateChanged: isEnterDim: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 31 */
		 v1 = 		 (( com.android.server.power.statistic.DimEventStatistic$DimEvent ) p1 ).getIsEnterDim ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getIsEnterDim()Z
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v1 = ", enter dim time: "; // const-string v1, ", enter dim time: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 32 */
		 (( com.android.server.power.statistic.DimEventStatistic$DimEvent ) p1 ).getDimTime ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getDimTime()J
		 /* move-result-wide v1 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 30 */
		 final String v1 = "DimEventStatistic"; // const-string v1, "DimEventStatistic"
		 android.util.Slog .d ( v1,v0 );
		 /* .line 34 */
	 } // :cond_0
	 v0 = 	 (( com.android.server.power.statistic.DimEventStatistic$DimEvent ) p1 ).getIsEnterDim ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getIsEnterDim()Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 35 */
		 (( com.android.server.power.statistic.DimEventStatistic$DimEvent ) p1 ).getDimTime ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getDimTime()J
		 /* move-result-wide v0 */
		 /* iput-wide v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mLastEnterDimTime:J */
		 /* .line 36 */
		 /* iget v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mEnterDimTimes:I */
		 /* add-int/lit8 v0, v0, 0x1 */
		 /* iput v0, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mEnterDimTimes:I */
		 /* .line 38 */
	 } // :cond_1
	 (( com.android.server.power.statistic.DimEventStatistic$DimEvent ) p1 ).getDimTime ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;->getDimTime()J
	 /* move-result-wide v0 */
	 /* invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DimEventStatistic;->recordDimDuration(J)V */
	 /* .line 40 */
} // :goto_0
return;
} // .end method
public void reportDimStateByIntent ( ) {
/* .locals 5 */
/* .line 43 */
final String v0 = "DimEventStatistic"; // const-string v0, "DimEventStatistic"
v1 = this.mOneTrackerHelper;
v2 = this.mAppId;
v3 = this.mContext;
/* .line 44 */
(( android.content.Context ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
/* .line 43 */
final String v4 = "dim_statistic"; // const-string v4, "dim_statistic"
(( com.android.server.power.statistic.OneTrackerHelper ) v1 ).getTrackEventIntent ( v2, v4, v3 ); // invoke-virtual {v1, v2, v4, v3}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 45 */
/* .local v1, "intent":Landroid/content/Intent; */
/* iget v2, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mEnterDimTimes:I */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 46 */
	 final String v3 = "enter_dim_times"; // const-string v3, "enter_dim_times"
	 (( android.content.Intent ) v1 ).putExtra ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
	 /* .line 47 */
	 final String v2 = "enter_dim_duration"; // const-string v2, "enter_dim_duration"
	 /* iget-wide v3, p0, Lcom/android/server/power/statistic/DimEventStatistic;->mSumDimDuration:J */
	 (( android.content.Intent ) v1 ).putExtra ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
	 /* .line 51 */
} // :cond_0
try { // :try_start_0
	 v2 = this.mOneTrackerHelper;
	 (( com.android.server.power.statistic.OneTrackerHelper ) v2 ).reportTrackEventByIntent ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 54 */
	 /* .line 52 */
	 /* :catch_0 */
	 /* move-exception v2 */
	 /* .line 53 */
	 /* .local v2, "e":Ljava/lang/Exception; */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "reportDimStateByIntent fail! "; // const-string v4, "reportDimStateByIntent fail! "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v0,v3 );
	 /* .line 55 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
/* sget-boolean v2, Lcom/android/server/power/statistic/DimEventStatistic;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 56 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "reportDimStateByIntent: data: "; // const-string v3, "reportDimStateByIntent: data: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) v1 ).getExtras ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 58 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/power/statistic/DimEventStatistic;->resetDimStateInfo()V */
/* .line 59 */
return;
} // .end method
