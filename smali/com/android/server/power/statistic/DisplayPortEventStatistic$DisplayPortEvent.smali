.class public Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;
.super Ljava/lang/Object;
.source "DisplayPortEventStatistic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/statistic/DisplayPortEventStatistic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DisplayPortEvent"
.end annotation


# instance fields
.field private mFrameRate:I

.field private mIsConnected:Z

.field private mPhysicalDisplayId:J

.field private mProductName:Ljava/lang/String;

.field private mResolution:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(JZLjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "physicalDisplayId"    # J
    .param p3, "isConnected"    # Z
    .param p4, "productName"    # Ljava/lang/String;
    .param p5, "frameRate"    # I
    .param p6, "resolution"    # Ljava/lang/String;

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-wide p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mPhysicalDisplayId:J

    .line 77
    iput-boolean p3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mIsConnected:Z

    .line 78
    iput-object p4, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mProductName:Ljava/lang/String;

    .line 79
    iput p5, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I

    .line 80
    iput-object p6, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mResolution:Ljava/lang/String;

    .line 81
    return-void
.end method


# virtual methods
.method public copyFrom(Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V
    .locals 2
    .param p1, "other"    # Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    .line 124
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getPhysicalDisplayId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mPhysicalDisplayId:J

    .line 125
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getIsConnected()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mIsConnected:Z

    .line 126
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getProductName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mProductName:Ljava/lang/String;

    .line 127
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getFrameRate()I

    move-result v0

    iput v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I

    .line 128
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getResolution()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mResolution:Ljava/lang/String;

    .line 129
    return-void
.end method

.method public getFrameRate()I
    .locals 1

    .line 108
    iget v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I

    return v0
.end method

.method public getIsConnected()Z
    .locals 1

    .line 92
    iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mIsConnected:Z

    return v0
.end method

.method public getPhysicalDisplayId()J
    .locals 2

    .line 84
    iget-wide v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mPhysicalDisplayId:J

    return-wide v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mProductName:Ljava/lang/String;

    return-object v0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mResolution:Ljava/lang/String;

    return-object v0
.end method

.method public setFrameRate(I)V
    .locals 0
    .param p1, "frameRate"    # I

    .line 112
    iput p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I

    .line 113
    return-void
.end method

.method public setIsConnected(Z)V
    .locals 0
    .param p1, "isConnected"    # Z

    .line 96
    iput-boolean p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mIsConnected:Z

    .line 97
    return-void
.end method

.method public setPhysicalDisplayId(J)V
    .locals 0
    .param p1, "physicalDisplayId"    # J

    .line 88
    iput-wide p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mPhysicalDisplayId:J

    .line 89
    return-void
.end method

.method public setProductName(Ljava/lang/String;)V
    .locals 0
    .param p1, "productName"    # Ljava/lang/String;

    .line 104
    iput-object p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mProductName:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setResolution(Ljava/lang/String;)V
    .locals 0
    .param p1, "resolution"    # Ljava/lang/String;

    .line 120
    iput-object p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mResolution:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[productName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mProductName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frameRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mFrameRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resolution="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->mResolution:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
