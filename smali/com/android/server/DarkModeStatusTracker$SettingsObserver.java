class com.android.server.DarkModeStatusTracker$SettingsObserver extends android.database.ContentObserver {
	 /* .source "DarkModeStatusTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/DarkModeStatusTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.DarkModeStatusTracker this$0; //synthetic
/* # direct methods */
public com.android.server.DarkModeStatusTracker$SettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/DarkModeStatusTracker; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 323 */
this.this$0 = p1;
/* .line 324 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 325 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 329 */
/* if-nez p2, :cond_0 */
/* .line 330 */
return;
/* .line 332 */
} // :cond_0
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 333 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "uri = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.net.Uri ) p2 ).getLastPathSegment ( ); // invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DarkModeStatusTracker"; // const-string v1, "DarkModeStatusTracker"
android.util.Slog .i ( v1,v0 );
/* .line 334 */
(( android.net.Uri ) p2 ).getLastPathSegment ( ); // invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v2 = 0; // const/4 v2, 0x0
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v1 = "last_app_dark_mode_pkg"; // const-string v1, "last_app_dark_mode_pkg"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 4; // const/4 v0, 0x4
/* :sswitch_1 */
final String v1 = "dark_mode_time_type"; // const-string v1, "dark_mode_time_type"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_2 */
final String v1 = "dark_mode_enable"; // const-string v1, "dark_mode_enable"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* move v0, v2 */
	 /* :sswitch_3 */
	 final String v1 = "dark_mode_contrast_enable"; // const-string v1, "dark_mode_contrast_enable"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 int v0 = 3; // const/4 v0, 0x3
		 /* :sswitch_4 */
		 final String v1 = "dark_mode_time_enable"; // const-string v1, "dark_mode_time_enable"
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 int v0 = 1; // const/4 v0, 0x1
		 } // :goto_0
		 int v0 = -1; // const/4 v0, -0x1
	 } // :goto_1
	 final String v1 = "577.2.0.1.23091"; // const-string v1, "577.2.0.1.23091"
	 /* packed-switch v0, :pswitch_data_0 */
	 /* .line 354 */
	 /* :pswitch_0 */
	 v0 = this.this$0;
	 final String v1 = "577.3.3.1.23094"; // const-string v1, "577.3.3.1.23094"
	 com.android.server.DarkModeStatusTracker .-$$Nest$muploadDarkModeStatusEvent ( v0,v1 );
	 /* .line 351 */
	 /* :pswitch_1 */
	 v0 = this.this$0;
	 final String v1 = "577.3.2.1.23093"; // const-string v1, "577.3.2.1.23093"
	 com.android.server.DarkModeStatusTracker .-$$Nest$muploadDarkModeStatusEvent ( v0,v1 );
	 /* .line 352 */
	 /* .line 348 */
	 /* :pswitch_2 */
	 v0 = this.this$0;
	 com.android.server.DarkModeStatusTracker .-$$Nest$muploadDarkModeStatusEvent ( v0,v1 );
	 /* .line 349 */
	 /* .line 345 */
	 /* :pswitch_3 */
	 v0 = this.this$0;
	 com.android.server.DarkModeStatusTracker .-$$Nest$muploadDarkModeStatusEvent ( v0,v1 );
	 /* .line 346 */
	 /* .line 337 */
	 /* :pswitch_4 */
	 v0 = this.this$0;
	 com.android.server.DarkModeStatusTracker .-$$Nest$fgetmContext ( v0 );
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v1 = "dark_mode_enable_by_setting"; // const-string v1, "dark_mode_enable_by_setting"
	 v0 = 	 android.provider.Settings$System .getIntForUser ( v0,v1,v2,v2 );
	 /* if-nez v0, :cond_2 */
	 /* .line 339 */
	 v0 = this.this$0;
	 final String v1 = "577.5.0.1.23096"; // const-string v1, "577.5.0.1.23096"
	 com.android.server.DarkModeStatusTracker .-$$Nest$muploadDarkModeStatusEvent ( v0,v1 );
	 /* .line 341 */
} // :cond_2
v0 = this.this$0;
final String v1 = "577.4.0.1.23106"; // const-string v1, "577.4.0.1.23106"
com.android.server.DarkModeStatusTracker .-$$Nest$muploadDarkModeStatusEvent ( v0,v1 );
/* .line 343 */
/* nop */
/* .line 358 */
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x56ccb7be -> :sswitch_4 */
/* -0x4b9a7c13 -> :sswitch_3 */
/* -0x2a9ed7aa -> :sswitch_2 */
/* 0x5d2c55b9 -> :sswitch_1 */
/* 0x776698d2 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
