.class Lcom/android/server/ScoutSystemMonitor$1;
.super Ljava/lang/Object;
.source "ScoutSystemMonitor.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ScoutSystemMonitor;->getScoutSystemDetailsAsync(Ljava/util/List;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ScoutSystemMonitor;

.field final synthetic val$handlerCheckers:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/android/server/ScoutSystemMonitor;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ScoutSystemMonitor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 531
    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor$1;->this$0:Lcom/android/server/ScoutSystemMonitor;

    iput-object p2, p0, Lcom/android/server/ScoutSystemMonitor$1;->val$handlerCheckers:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 531
    invoke-virtual {p0}, Lcom/android/server/ScoutSystemMonitor$1;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 11

    .line 533
    const/4 v0, 0x0

    .line 534
    .local v0, "stackTraces":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 535
    .local v1, "details":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/server/ScoutSystemMonitor$1;->val$handlerCheckers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 536
    iget-object v3, p0, Lcom/android/server/ScoutSystemMonitor$1;->val$handlerCheckers:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    .line 537
    .local v3, "mCheck":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThreadTid()I

    move-result v4

    .line 539
    .local v4, "tid":I
    sget-boolean v5, Lcom/android/server/ScoutHelper;->IS_INTERNATIONAL_BUILD:Z

    if-nez v5, :cond_0

    if-lez v4, :cond_0

    .line 540
    invoke-static {v4}, Lcom/android/server/ScoutHelper;->getMiuiStackTraceByTid(I)[Ljava/lang/StackTraceElement;

    move-result-object v5

    .local v5, "st":[Ljava/lang/StackTraceElement;
    goto :goto_1

    .line 542
    .end local v5    # "st":[Ljava/lang/StackTraceElement;
    :cond_0
    invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    .line 544
    .restart local v5    # "st":[Ljava/lang/StackTraceElement;
    :goto_1
    if-eqz v5, :cond_1

    .line 545
    array-length v6, v5

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v6, :cond_1

    aget-object v8, v5, v7

    .line 546
    .local v8, "element":Ljava/lang/StackTraceElement;
    const-string v9, "    at "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 545
    .end local v8    # "element":Ljava/lang/StackTraceElement;
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 549
    :cond_1
    const-string v6, "\n\n"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 535
    .end local v3    # "mCheck":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    .end local v4    # "tid":I
    .end local v5    # "st":[Ljava/lang/StackTraceElement;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 551
    .end local v2    # "i":I
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 552
    if-nez v0, :cond_3

    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_3
    return-object v2
.end method
