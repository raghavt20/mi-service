.class public Lcom/android/server/MQSThread;
.super Ljava/lang/Thread;
.source "MQSThread.java"


# static fields
.field private static final DYNAMIC_DAILY_USE_FORMAT:Ljava/lang/String; = "{\"name\":\"dynamic_dailyuse\",\"audio_event\":{\"dynamic_package_name\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }"

.field private static final TAG:Ljava/lang/String; = "MQSThread.vibrator"

.field private static final VIBRATION_DAILY_USE_FORMAT:Ljava/lang/String; = "{\"name\":\"vibration_dailyuse\",\"audio_event\":{\"vibration_package_name\":\"%s\",\"vibration_effect_id\":\"%s\",\"vibration_count\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }"


# instance fields
.field private final EffectID:Ljava/lang/String;

.field private final PackageName:Ljava/lang/String;

.field private final count:Ljava/lang/String;

.field private mMiuiXlog:Landroid/media/MiuiXlog;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "PKG"    # Ljava/lang/String;

    .line 29
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 27
    new-instance v0, Landroid/media/MiuiXlog;

    invoke-direct {v0}, Landroid/media/MiuiXlog;-><init>()V

    iput-object v0, p0, Lcom/android/server/MQSThread;->mMiuiXlog:Landroid/media/MiuiXlog;

    .line 30
    iput-object p1, p0, Lcom/android/server/MQSThread;->PackageName:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/MQSThread;->EffectID:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/android/server/MQSThread;->count:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "PKG"    # Ljava/lang/String;
    .param p2, "ID"    # Ljava/lang/String;
    .param p3, "nums"    # I

    .line 35
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 27
    new-instance v0, Landroid/media/MiuiXlog;

    invoke-direct {v0}, Landroid/media/MiuiXlog;-><init>()V

    iput-object v0, p0, Lcom/android/server/MQSThread;->mMiuiXlog:Landroid/media/MiuiXlog;

    .line 36
    iput-object p1, p0, Lcom/android/server/MQSThread;->PackageName:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/android/server/MQSThread;->EffectID:Ljava/lang/String;

    .line 38
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MQSThread;->count:Ljava/lang/String;

    .line 39
    return-void
.end method

.method private isAllowedRegion()Z
    .locals 3

    .line 91
    const-string v0, "ro.miui.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "region":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "the region is :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MQSThread.vibrator"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const-string v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public reportDynamicDailyUse()V
    .locals 4

    .line 57
    invoke-direct {p0}, Lcom/android/server/MQSThread;->isAllowedRegion()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    return-void

    .line 60
    :cond_0
    const-string v0, "reportDynamicDailyUse start."

    const-string v1, "MQSThread.vibrator"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v0, p0, Lcom/android/server/MQSThread;->PackageName:Ljava/lang/String;

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v2, "{\"name\":\"dynamic_dailyuse\",\"audio_event\":{\"dynamic_package_name\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }"

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "result":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportDynamicDailyUse:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :try_start_0
    iget-object v2, p0, Lcom/android/server/MQSThread;->mMiuiXlog:Landroid/media/MiuiXlog;

    invoke-virtual {v2, v0}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    goto :goto_0

    .line 68
    :catchall_0
    move-exception v2

    .line 69
    .local v2, "e":Ljava/lang/Throwable;
    const-string v3, "can not use miuiXlogSend!!!"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    .end local v2    # "e":Ljava/lang/Throwable;
    :goto_0
    return-void
.end method

.method public reportVibrationDailyUse()V
    .locals 4

    .line 74
    invoke-direct {p0}, Lcom/android/server/MQSThread;->isAllowedRegion()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    return-void

    .line 77
    :cond_0
    const-string v0, "reportVibrationDailyUse start."

    const-string v1, "MQSThread.vibrator"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/android/server/MQSThread;->PackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/server/MQSThread;->EffectID:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/MQSThread;->count:Ljava/lang/String;

    filled-new-array {v0, v2, v3}, [Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v2, "{\"name\":\"vibration_dailyuse\",\"audio_event\":{\"vibration_package_name\":\"%s\",\"vibration_effect_id\":\"%s\",\"vibration_count\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }"

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "result":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportVibrationDailyUse:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :try_start_0
    iget-object v2, p0, Lcom/android/server/MQSThread;->mMiuiXlog:Landroid/media/MiuiXlog;

    invoke-virtual {v2, v0}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    goto :goto_0

    .line 85
    :catchall_0
    move-exception v2

    .line 86
    .local v2, "e":Ljava/lang/Throwable;
    const-string v3, "can not use miuiXlogSend!!!"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    .end local v2    # "e":Ljava/lang/Throwable;
    :goto_0
    return-void
.end method

.method public run()V
    .locals 3

    .line 43
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/android/server/MQSThread;->EffectID:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/android/server/MQSThread;->reportVibrationDailyUse()V

    goto :goto_0

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/MQSThread;->reportDynamicDailyUse()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    goto :goto_1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 52
    const-string v1, "MQSThread.vibrator"

    const-string v2, "error for MQSThread"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
