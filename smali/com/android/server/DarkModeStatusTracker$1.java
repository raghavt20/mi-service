class com.android.server.DarkModeStatusTracker$1 extends android.database.ContentObserver {
	 /* .source "DarkModeStatusTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/DarkModeStatusTracker;->registerSettingsObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.DarkModeStatusTracker this$0; //synthetic
/* # direct methods */
 com.android.server.DarkModeStatusTracker$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/DarkModeStatusTracker; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 149 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .line 152 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 154 */
com.android.server.DarkModeSuggestProvider .getInstance ( );
v1 = this.this$0;
com.android.server.DarkModeStatusTracker .-$$Nest$fgetmContext ( v1 );
(( com.android.server.DarkModeSuggestProvider ) v0 ).updateCloudDataForDisableRegion ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeSuggestProvider;->updateCloudDataForDisableRegion(Landroid/content/Context;)Ljava/util/Set;
/* .line 153 */
com.android.server.DarkModeOneTrackHelper .setDataDisableRegion ( v0 );
/* .line 155 */
return;
} // .end method
