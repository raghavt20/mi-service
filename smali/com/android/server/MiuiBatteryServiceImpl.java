public class com.android.server.MiuiBatteryServiceImpl extends com.android.server.MiuiBatteryServiceStub {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.MiuiBatteryServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;, */
/* Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String AES_DECRYPTED_TEXT;
private static final java.lang.String AES_KEY;
private static final Integer HANDLE_PRODUCT_ID;
private static final Integer HANDLE_VENDOR_ID;
public static final Integer HIGH_TEMP_STOP_CHARGE_STATE;
private static final java.lang.String KEY_BATTERY_TEMP_ALLOW_KILL;
public static final java.lang.String KEY_FAST_CHARGE_ENABLED;
private static final java.lang.String KEY_SETTING_TEMP_STATE;
private static final Integer SHUTDOWN_TMEIOUT;
private static final java.lang.String TEST_APP_PACKAGENAME_KDDI;
private static final java.lang.String TEST_APP_PACKAGENAME_SB;
private static final Integer WAKELOCK_ACQUIRE;
private static Boolean isSystemReady;
/* # instance fields */
private Integer BtConnectedCount;
private final Boolean DEBUG;
public final java.lang.String SUPPORT_COUNTRY;
private final java.lang.String TAG;
private Boolean isDisCharging;
private volatile android.bluetooth.BluetoothA2dp mA2dp;
private android.database.ContentObserver mBatteryTempThreshholdObserver;
private android.content.BroadcastReceiver mBluetoothStateReceiver;
private Integer mBtConnectState;
private android.content.BroadcastReceiver mChargingStateReceiver;
private android.content.Context mContext;
android.database.ContentObserver mFastChargeObserver;
private com.android.server.MiuiBatteryServiceImpl$BatteryHandler mHandler;
private volatile android.bluetooth.BluetoothHeadset mHeadset;
private Boolean mIsSatisfyTempLevelCondition;
private Boolean mIsSatisfyTempSocCondition;
private Boolean mIsSatisfyTimeRegionCondition;
private Boolean mIsStopCharge;
private Boolean mIsTestMode;
private Integer mLastPhoneBatteryLevel;
private miui.util.IMiCharge mMiCharge;
private final android.bluetooth.BluetoothProfile$ServiceListener mProfileServiceListener;
private Boolean mSupportHighTempStopCharge;
private Boolean mSupportSB;
private Boolean mSupportWirelessCharge;
private android.database.ContentObserver mTempStateObserver;
private android.content.BroadcastReceiver mUsbStateReceiver;
private com.miui.app.MiuiFboServiceInternal miuiFboService;
private android.os.PowerManager pm;
private java.lang.Runnable shutDownRnuable;
private android.os.PowerManager$WakeLock wakeLock;
/* # direct methods */
static Integer -$$Nest$fgetBtConnectedCount ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->BtConnectedCount:I */
} // .end method
static Boolean -$$Nest$fgetDEBUG ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->DEBUG:Z */
} // .end method
static Boolean -$$Nest$fgetisDisCharging ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->isDisCharging:Z */
} // .end method
static Integer -$$Nest$fgetmBtConnectState ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBtConnectState:I */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static com.android.server.MiuiBatteryServiceImpl$BatteryHandler -$$Nest$fgetmHandler ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsSatisfyTempLevelCondition ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempLevelCondition:Z */
} // .end method
static Boolean -$$Nest$fgetmIsSatisfyTempSocCondition ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempSocCondition:Z */
} // .end method
static Boolean -$$Nest$fgetmIsSatisfyTimeRegionCondition ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTimeRegionCondition:Z */
} // .end method
static Boolean -$$Nest$fgetmIsStopCharge ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsStopCharge:Z */
} // .end method
static Boolean -$$Nest$fgetmIsTestMode ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsTestMode:Z */
} // .end method
static Integer -$$Nest$fgetmLastPhoneBatteryLevel ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mLastPhoneBatteryLevel:I */
} // .end method
static miui.util.IMiCharge -$$Nest$fgetmMiCharge ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiCharge;
} // .end method
static Boolean -$$Nest$fgetmSupportWirelessCharge ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportWirelessCharge:Z */
} // .end method
static android.os.PowerManager -$$Nest$fgetpm ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.pm;
} // .end method
static java.lang.Runnable -$$Nest$fgetshutDownRnuable ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.shutDownRnuable;
} // .end method
static android.os.PowerManager$WakeLock -$$Nest$fgetwakeLock ( com.android.server.MiuiBatteryServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.wakeLock;
} // .end method
static void -$$Nest$fputBtConnectedCount ( com.android.server.MiuiBatteryServiceImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->BtConnectedCount:I */
return;
} // .end method
static void -$$Nest$fputisDisCharging ( com.android.server.MiuiBatteryServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->isDisCharging:Z */
return;
} // .end method
static void -$$Nest$fputmA2dp ( com.android.server.MiuiBatteryServiceImpl p0, android.bluetooth.BluetoothA2dp p1 ) { //bridge//synthethic
/* .locals 0 */
this.mA2dp = p1;
return;
} // .end method
static void -$$Nest$fputmBtConnectState ( com.android.server.MiuiBatteryServiceImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBtConnectState:I */
return;
} // .end method
static void -$$Nest$fputmHeadset ( com.android.server.MiuiBatteryServiceImpl p0, android.bluetooth.BluetoothHeadset p1 ) { //bridge//synthethic
/* .locals 0 */
this.mHeadset = p1;
return;
} // .end method
static void -$$Nest$fputmIsSatisfyTempLevelCondition ( com.android.server.MiuiBatteryServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempLevelCondition:Z */
return;
} // .end method
static void -$$Nest$fputmIsSatisfyTempSocCondition ( com.android.server.MiuiBatteryServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempSocCondition:Z */
return;
} // .end method
static void -$$Nest$fputmIsSatisfyTimeRegionCondition ( com.android.server.MiuiBatteryServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTimeRegionCondition:Z */
return;
} // .end method
static void -$$Nest$fputmIsStopCharge ( com.android.server.MiuiBatteryServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsStopCharge:Z */
return;
} // .end method
static void -$$Nest$fputmLastPhoneBatteryLevel ( com.android.server.MiuiBatteryServiceImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mLastPhoneBatteryLevel:I */
return;
} // .end method
static com.android.server.MiuiBatteryServiceImpl ( ) {
/* .locals 2 */
/* .line 97 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.MiuiBatteryServiceImpl.isSystemReady = (v0!= 0);
/* .line 115 */
final String v0 = "com.kddi.hirakuto"; // const-string v0, "com.kddi.hirakuto"
final String v1 = "com.kddi.hirakuto.debug"; // const-string v1, "com.kddi.hirakuto.debug"
/* filled-new-array {v0, v1}, [Ljava/lang/String; */
/* .line 119 */
final String v0 = "com.xiaomi.mihomemanager"; // const-string v0, "com.xiaomi.mihomemanager"
/* filled-new-array {v0}, [Ljava/lang/String; */
return;
} // .end method
public com.android.server.MiuiBatteryServiceImpl ( ) {
/* .locals 8 */
/* .line 100 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceStub;-><init>()V */
/* .line 71 */
final String v0 = "MiuiBatteryServiceImpl"; // const-string v0, "MiuiBatteryServiceImpl"
this.TAG = v0;
/* .line 72 */
final String v0 = "persist.sys.debug_impl"; // const-string v0, "persist.sys.debug_impl"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->DEBUG:Z */
/* .line 83 */
/* iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsStopCharge:Z */
/* .line 84 */
/* iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempSocCondition:Z */
/* .line 85 */
/* iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTimeRegionCondition:Z */
/* .line 86 */
/* iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempLevelCondition:Z */
/* .line 87 */
/* iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsTestMode:Z */
/* .line 89 */
/* iput v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->BtConnectedCount:I */
/* .line 96 */
miui.util.IMiCharge .getInstance ( );
this.mMiCharge = v0;
/* .line 106 */
final String v2 = "IT"; // const-string v2, "IT"
final String v3 = "FR"; // const-string v3, "FR"
final String v4 = "ES"; // const-string v4, "ES"
final String v5 = "DE"; // const-string v5, "DE"
final String v6 = "PL"; // const-string v6, "PL"
final String v7 = "GB"; // const-string v7, "GB"
/* filled-new-array/range {v2 ..v7}, [Ljava/lang/String; */
this.SUPPORT_COUNTRY = v0;
/* .line 129 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mLastPhoneBatteryLevel:I */
/* .line 132 */
/* iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->isDisCharging:Z */
/* .line 133 */
/* iput v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBtConnectState:I */
/* .line 136 */
int v0 = 0; // const/4 v0, 0x0
this.pm = v0;
/* .line 137 */
this.wakeLock = v0;
/* .line 139 */
/* new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$1;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V */
this.mChargingStateReceiver = v0;
/* .line 164 */
/* new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$2;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V */
this.mBluetoothStateReceiver = v0;
/* .line 183 */
/* new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$3;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V */
this.shutDownRnuable = v0;
/* .line 203 */
/* new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$4; */
/* invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$4;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V */
this.mUsbStateReceiver = v0;
/* .line 444 */
/* new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$11; */
/* invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$11;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V */
this.mProfileServiceListener = v0;
/* .line 101 */
v0 = this.mMiCharge;
v0 = (( miui.util.IMiCharge ) v0 ).isWirelessChargingSupported ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->isWirelessChargingSupported()Z
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportWirelessCharge:Z */
/* .line 102 */
v0 = this.mMiCharge;
/* const-string/jumbo v2, "smart_batt" */
v0 = (( miui.util.IMiCharge ) v0 ).isFunctionSupported ( v2 ); // invoke-virtual {v0, v2}, Lmiui/util/IMiCharge;->isFunctionSupported(Ljava/lang/String;)Z
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportSB:Z */
/* .line 103 */
final String v0 = "persist.vendor.high_temp_stop_charge"; // const-string v0, "persist.vendor.high_temp_stop_charge"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportHighTempStopCharge:Z */
/* .line 104 */
return;
} // .end method
private Integer initBtConnectCount ( ) {
/* .locals 6 */
/* .line 478 */
int v0 = 0; // const/4 v0, 0x0
/* .line 479 */
/* .local v0, "a2dpCount":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 481 */
/* .local v1, "headsetCount":I */
android.bluetooth.BluetoothAdapter .getDefaultAdapter ( );
/* .line 482 */
/* .local v2, "btAdapter":Landroid/bluetooth/BluetoothAdapter; */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 483 */
	 v3 = this.mContext;
	 v4 = this.mProfileServiceListener;
	 int v5 = 2; // const/4 v5, 0x2
	 (( android.bluetooth.BluetoothAdapter ) v2 ).getProfileProxy ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z
	 /* .line 484 */
	 v3 = this.mContext;
	 v4 = this.mProfileServiceListener;
	 int v5 = 1; // const/4 v5, 0x1
	 (( android.bluetooth.BluetoothAdapter ) v2 ).getProfileProxy ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z
	 /* .line 486 */
} // :cond_0
v3 = this.mA2dp;
if ( v3 != null) { // if-eqz v3, :cond_1
	 /* .line 487 */
	 v3 = this.mA2dp;
	 v0 = 	 (( android.bluetooth.BluetoothA2dp ) v3 ).getConnectedDevices ( ); // invoke-virtual {v3}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;
	 /* .line 489 */
} // :cond_1
v3 = this.mHeadset;
if ( v3 != null) { // if-eqz v3, :cond_2
	 /* .line 490 */
	 v3 = this.mHeadset;
	 v1 = 	 (( android.bluetooth.BluetoothHeadset ) v3 ).getConnectedDevices ( ); // invoke-virtual {v3}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;
	 /* .line 493 */
} // :cond_2
/* add-int v3, v0, v1 */
} // .end method
private Boolean isTestMUTForJapan ( java.lang.String[] p0 ) {
/* .locals 6 */
/* .param p1, "appPackage" # [Ljava/lang/String; */
/* .line 506 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
int v1 = 0; // const/4 v1, 0x0
(( android.content.pm.PackageManager ) v0 ).getInstalledPackages ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;
/* .line 507 */
/* .local v0, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
java.util.Arrays .asList ( p1 );
/* .line 508 */
/* .local v2, "appPackagesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v4 = } // :goto_0
/* if-ge v3, v4, :cond_1 */
/* .line 509 */
/* check-cast v4, Landroid/content/pm/PackageInfo; */
/* .line 510 */
/* .local v4, "packageInfo":Landroid/content/pm/PackageInfo; */
v5 = v5 = this.packageName;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 511 */
final String v1 = "MiuiBatteryServiceImpl"; // const-string v1, "MiuiBatteryServiceImpl"
final String v5 = "TEST MODE"; // const-string v5, "TEST MODE"
android.util.Slog .d ( v1,v5 );
/* .line 512 */
int v1 = 1; // const/4 v1, 0x1
/* .line 508 */
} // .end local v4 # "packageInfo":Landroid/content/pm/PackageInfo;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 515 */
} // .end local v3 # "i":I
} // :cond_1
} // .end method
private void registerBluetoothStateBroadcastReceiver ( ) {
/* .locals 4 */
/* .line 196 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 197 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.bluetooth.device.action.ACL_CONNECTED"; // const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 198 */
final String v1 = "android.bluetooth.device.action.ACL_DISCONNECTED"; // const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 199 */
v1 = this.mContext;
v2 = this.mBluetoothStateReceiver;
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 200 */
return;
} // .end method
private void registerChargingStateBroadcastReceiver ( ) {
/* .locals 4 */
/* .line 159 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 160 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 161 */
v1 = this.mContext;
v2 = this.mChargingStateReceiver;
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 162 */
return;
} // .end method
private void registerUsbStateBroadcastReceiver ( ) {
/* .locals 4 */
/* .line 497 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 498 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 499 */
final String v1 = "android.hardware.usb.action.USB_DEVICE_ATTACHED"; // const-string v1, "android.hardware.usb.action.USB_DEVICE_ATTACHED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 500 */
final String v1 = "android.hardware.usb.action.USB_DEVICE_DETACHED"; // const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 501 */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 502 */
v1 = this.mContext;
v2 = this.mUsbStateReceiver;
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 503 */
return;
} // .end method
/* # virtual methods */
public void init ( android.content.Context p0 ) {
/* .locals 12 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 240 */
this.mContext = p1;
/* .line 241 */
/* new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler; */
com.android.server.MiuiFgThread .get ( );
(( com.android.server.MiuiFgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiFgThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 242 */
/* const-class v0, Lcom/miui/app/MiuiFboServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/MiuiFboServiceInternal; */
this.miuiFboService = v0;
/* .line 243 */
int v0 = 1; // const/4 v0, 0x1
com.android.server.MiuiBatteryServiceImpl.isSystemReady = (v0!= 0);
/* .line 244 */
/* new-instance v1, Lcom/android/server/MiuiBatteryServiceImpl$5; */
/* new-instance v2, Landroid/os/Handler; */
/* invoke-direct {v2}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v1, p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$5;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Handler;)V */
this.mFastChargeObserver = v1;
/* .line 258 */
/* new-instance v1, Lcom/android/server/MiuiBatteryServiceImpl$6; */
/* new-instance v2, Landroid/os/Handler; */
/* invoke-direct {v2}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v1, p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$6;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Handler;)V */
this.mTempStateObserver = v1;
/* .line 266 */
/* new-instance v1, Lcom/android/server/MiuiBatteryServiceImpl$7; */
/* new-instance v2, Landroid/os/Handler; */
/* invoke-direct {v2}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v1, p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$7;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Handler;)V */
this.mBatteryTempThreshholdObserver = v1;
/* .line 274 */
/* new-instance v1, Lcom/android/server/MiuiBatteryServiceImpl$8; */
/* invoke-direct {v1, p0}, Lcom/android/server/MiuiBatteryServiceImpl$8;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V */
/* .line 297 */
/* .local v1, "stateChangedReceiver":Landroid/content/BroadcastReceiver; */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* .line 298 */
/* .local v2, "intentfilter":Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.BATTERY_CHANGED"; // const-string v3, "android.intent.action.BATTERY_CHANGED"
(( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 299 */
final String v3 = "ro.product.device"; // const-string v3, "ro.product.device"
final String v4 = ""; // const-string v4, ""
android.os.SystemProperties .get ( v3,v4 );
/* const-string/jumbo v6, "yudi" */
v5 = (( java.lang.String ) v5 ).startsWith ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 300 */
final String v5 = "android.intent.action.SCREEN_OFF"; // const-string v5, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v2 ).addAction ( v5 ); // invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 301 */
final String v5 = "android.intent.action.USER_PRESENT"; // const-string v5, "android.intent.action.USER_PRESENT"
(( android.content.IntentFilter ) v2 ).addAction ( v5 ); // invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 303 */
} // :cond_0
v5 = this.mContext;
(( android.content.Context ) v5 ).registerReceiver ( v1, v2 ); // invoke-virtual {v5, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 305 */
v5 = this.mMiCharge;
(( miui.util.IMiCharge ) v5 ).getBatteryChargeType ( ); // invoke-virtual {v5}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;
/* .line 306 */
/* .local v5, "chargeType":Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
if ( v5 != null) { // if-eqz v5, :cond_1
v7 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
/* if-lez v7, :cond_1 */
/* .line 307 */
final String v7 = "persist.vendor.charge.oneTrack"; // const-string v7, "persist.vendor.charge.oneTrack"
v7 = android.os.SystemProperties .getBoolean ( v7,v6 );
/* if-nez v7, :cond_1 */
/* .line 308 */
com.android.server.MiuiBatteryStatsService .getInstance ( p1 );
/* .line 311 */
} // :cond_1
android.os.SystemProperties .get ( v3,v4 );
final String v8 = "mona"; // const-string v8, "mona"
v7 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v7, :cond_2 */
/* .line 312 */
android.os.SystemProperties .get ( v3,v4 );
/* const-string/jumbo v8, "thor" */
v7 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_3
} // :cond_2
/* sget-boolean v7, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v7, :cond_3 */
/* .line 314 */
com.android.server.MiuiBatteryAuthentic .getInstance ( p1 );
/* .line 317 */
} // :cond_3
final String v7 = "ro.miui.customized.region"; // const-string v7, "ro.miui.customized.region"
android.os.SystemProperties .get ( v7,v4 );
final String v9 = "jp_kd"; // const-string v9, "jp_kd"
v8 = (( java.lang.String ) v8 ).startsWith ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 318 */
v8 = com.android.server.MiuiBatteryServiceImpl.TEST_APP_PACKAGENAME_KDDI;
v8 = /* invoke-direct {p0, v8}, Lcom/android/server/MiuiBatteryServiceImpl;->isTestMUTForJapan([Ljava/lang/String;)Z */
/* iput-boolean v8, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsTestMode:Z */
/* .line 321 */
} // :cond_4
android.os.SystemProperties .get ( v7,v4 );
final String v8 = "jp_sb"; // const-string v8, "jp_sb"
v7 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 322 */
v7 = com.android.server.MiuiBatteryServiceImpl.TEST_APP_PACKAGENAME_SB;
v7 = /* invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryServiceImpl;->isTestMUTForJapan([Ljava/lang/String;)Z */
/* iput-boolean v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsTestMode:Z */
/* .line 325 */
} // :cond_5
final String v7 = "persist.vendor.smartchg"; // const-string v7, "persist.vendor.smartchg"
v7 = android.os.SystemProperties .getInt ( v7,v6 );
/* if-lez v7, :cond_6 */
/* .line 326 */
com.android.server.MiuiBatteryIntelligence .getInstance ( p1 );
/* .line 329 */
} // :cond_6
final String v7 = "ro.product.mod_device"; // const-string v7, "ro.product.mod_device"
android.os.SystemProperties .get ( v7,v4 );
/* const-string/jumbo v9, "star" */
v8 = (( java.lang.String ) v8 ).startsWith ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
int v9 = 2; // const/4 v9, 0x2
/* if-nez v8, :cond_7 */
/* .line 330 */
android.os.SystemProperties .get ( v7,v4 );
final String v8 = "mars"; // const-string v8, "mars"
v7 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_9
/* .line 331 */
} // :cond_7
v7 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl;->initBtConnectCount()I */
/* iput v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->BtConnectedCount:I */
/* .line 332 */
/* if-lez v7, :cond_8 */
/* .line 333 */
v7 = this.mHandler;
/* const/16 v8, 0xa */
/* const-wide/16 v10, 0x3e8 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v7 ).sendMessageDelayed ( v8, v0, v10, v11 ); // invoke-virtual {v7, v8, v0, v10, v11}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IZJ)V
/* .line 336 */
} // :cond_8
/* new-instance v7, Lcom/android/server/MiuiBatteryServiceImpl$9; */
/* invoke-direct {v7, p0}, Lcom/android/server/MiuiBatteryServiceImpl$9;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V */
/* .line 365 */
/* .local v7, "bluetoothReceiver":Landroid/content/BroadcastReceiver; */
/* new-instance v8, Landroid/content/IntentFilter; */
/* invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V */
/* .line 366 */
/* .local v8, "filter":Landroid/content/IntentFilter; */
final String v10 = "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"; // const-string v10, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"
(( android.content.IntentFilter ) v8 ).addAction ( v10 ); // invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 367 */
final String v10 = "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"; // const-string v10, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"
(( android.content.IntentFilter ) v8 ).addAction ( v10 ); // invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 368 */
final String v10 = "android.bluetooth.adapter.action.STATE_CHANGED"; // const-string v10, "android.bluetooth.adapter.action.STATE_CHANGED"
(( android.content.IntentFilter ) v8 ).addAction ( v10 ); // invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 369 */
v10 = this.mContext;
(( android.content.Context ) v10 ).registerReceiver ( v7, v8, v9 ); // invoke-virtual {v10, v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 372 */
} // .end local v7 # "bluetoothReceiver":Landroid/content/BroadcastReceiver;
} // .end local v8 # "filter":Landroid/content/IntentFilter;
} // :cond_9
/* iget-boolean v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportSB:Z */
if ( v7 != null) { // if-eqz v7, :cond_a
/* .line 373 */
/* new-instance v7, Lcom/android/server/MiuiBatteryServiceImpl$10; */
/* invoke-direct {v7, p0}, Lcom/android/server/MiuiBatteryServiceImpl$10;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V */
/* .line 386 */
/* .local v7, "updateBattVolIntentReceiver":Landroid/content/BroadcastReceiver; */
/* new-instance v8, Landroid/content/IntentFilter; */
final String v10 = "miui.intent.action.UPDATE_BATTERY_DATA"; // const-string v10, "miui.intent.action.UPDATE_BATTERY_DATA"
/* invoke-direct {v8, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 387 */
/* .restart local v8 # "filter":Landroid/content/IntentFilter; */
final String v10 = "android.intent.action.BOOT_COMPLETED"; // const-string v10, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v8 ).addAction ( v10 ); // invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 388 */
final String v10 = "miui.intent.action.ADJUST_VOLTAGE"; // const-string v10, "miui.intent.action.ADJUST_VOLTAGE"
(( android.content.IntentFilter ) v8 ).addAction ( v10 ); // invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 389 */
v10 = this.mContext;
(( android.content.Context ) v10 ).registerReceiver ( v7, v8, v9 ); // invoke-virtual {v10, v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 392 */
} // .end local v7 # "updateBattVolIntentReceiver":Landroid/content/BroadcastReceiver;
} // .end local v8 # "filter":Landroid/content/IntentFilter;
} // :cond_a
/* iget-boolean v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportHighTempStopCharge:Z */
if ( v7 != null) { // if-eqz v7, :cond_b
/* .line 393 */
v7 = this.mContext;
(( android.content.Context ) v7 ).getContentResolver ( ); // invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v8, "thermal_temp_state_value" */
android.provider.Settings$Secure .getUriFor ( v8 );
v9 = this.mTempStateObserver;
(( android.content.ContentResolver ) v7 ).registerContentObserver ( v8, v6, v9 ); // invoke-virtual {v7, v8, v6, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 395 */
v7 = this.mContext;
(( android.content.Context ) v7 ).getContentResolver ( ); // invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v8 = "allowed_kill_battery_temp_threshhold"; // const-string v8, "allowed_kill_battery_temp_threshhold"
android.provider.Settings$Secure .getUriFor ( v8 );
v9 = this.mBatteryTempThreshholdObserver;
(( android.content.ContentResolver ) v7 ).registerContentObserver ( v8, v6, v9 ); // invoke-virtual {v7, v8, v6, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 400 */
} // :cond_b
android.os.SystemProperties .get ( v3,v4 );
final String v8 = "aurora"; // const-string v8, "aurora"
v7 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_c
/* .line 401 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl;->registerUsbStateBroadcastReceiver()V */
/* .line 404 */
} // :cond_c
v7 = this.mContext;
(( android.content.Context ) v7 ).getContentResolver ( ); // invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 405 */
final String v8 = "key_fast_charge_enabled"; // const-string v8, "key_fast_charge_enabled"
android.provider.Settings$Secure .getUriFor ( v8 );
v9 = this.mFastChargeObserver;
/* .line 404 */
(( android.content.ContentResolver ) v7 ).registerContentObserver ( v8, v6, v9 ); // invoke-virtual {v7, v8, v6, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 408 */
android.os.SystemProperties .get ( v3,v4 );
/* const-string/jumbo v4, "zhuque" */
v3 = (( java.lang.String ) v3 ).startsWith ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_d
/* .line 409 */
v3 = this.mContext;
final String v4 = "power"; // const-string v4, "power"
(( android.content.Context ) v3 ).getSystemService ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v3, Landroid/os/PowerManager; */
this.pm = v3;
/* .line 410 */
final String v4 = "Screenoffshutdown"; // const-string v4, "Screenoffshutdown"
(( android.os.PowerManager ) v3 ).newWakeLock ( v0, v4 ); // invoke-virtual {v3, v0, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
this.wakeLock = v0;
/* .line 411 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl;->registerChargingStateBroadcastReceiver()V */
/* .line 412 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl;->registerBluetoothStateBroadcastReceiver()V */
/* .line 413 */
v0 = this.wakeLock;
/* const-wide/32 v3, 0x57e40 */
(( android.os.PowerManager$WakeLock ) v0 ).acquire ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
/* .line 414 */
v0 = this.mHandler;
v3 = this.shutDownRnuable;
/* const-wide/32 v6, 0x493e0 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v0 ).postDelayed ( v3, v6, v7 ); // invoke-virtual {v0, v3, v6, v7}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 417 */
} // :cond_d
return;
} // .end method
public void setBatteryStatusWithFbo ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "batteryStatus" # I */
/* .param p2, "batteryLevel" # I */
/* .param p3, "batteryTemperature" # I */
/* .line 421 */
/* sget-boolean v0, Lcom/android/server/MiuiBatteryServiceImpl;->isSystemReady:Z */
/* if-nez v0, :cond_0 */
/* .line 422 */
return;
/* .line 424 */
} // :cond_0
v0 = this.miuiFboService;
/* .line 425 */
v0 = v0 = this.miuiFboService;
/* const-wide/16 v1, 0x0 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* const/16 v0, 0x1f4 */
/* if-lt p3, v0, :cond_1 */
/* .line 426 */
v0 = this.miuiFboService;
/* const-string/jumbo v3, "stop" */
int v4 = 3; // const/4 v4, 0x3
/* .line 428 */
return;
/* .line 430 */
} // :cond_1
v0 = v0 = this.miuiFboService;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = v0 = this.miuiFboService;
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/16 v0, 0x1c2 */
/* if-le p3, v0, :cond_2 */
/* .line 432 */
v0 = this.miuiFboService;
/* const-string/jumbo v3, "stopDueTobatteryTemperature" */
int v4 = 6; // const/4 v4, 0x6
/* .line 434 */
return;
/* .line 436 */
} // :cond_2
v0 = v0 = this.miuiFboService;
/* if-nez v0, :cond_3 */
v0 = v0 = this.miuiFboService;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.miuiFboService;
v0 = /* .line 437 */
/* if-nez v0, :cond_3 */
/* const/16 v0, 0x190 */
/* if-ge p3, v0, :cond_3 */
/* .line 438 */
v0 = this.miuiFboService;
final String v3 = "continue"; // const-string v3, "continue"
int v4 = 2; // const/4 v4, 0x2
/* .line 440 */
return;
/* .line 442 */
} // :cond_3
return;
} // .end method
