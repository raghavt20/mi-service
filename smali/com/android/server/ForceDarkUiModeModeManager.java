public class com.android.server.ForceDarkUiModeModeManager {
	 /* .source "ForceDarkUiModeModeManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
final android.os.RemoteCallbackList mAppDarkModeObservers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Landroid/app/IAppDarkModeObserver;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.DarkModeTimeModeManager mDarkModeTimeModeManager;
private com.android.server.ForceDarkAppConfigProvider mForceDarkAppConfigProvider;
private com.android.server.ForceDarkAppListManager mForceDarkAppListManager;
private java.util.HashMap mForceDarkConfigData;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mForceDarkDebug;
private com.android.server.UiModeManagerService mUiModeManagerService;
/* # direct methods */
static com.android.server.UiModeManagerService -$$Nest$fgetmUiModeManagerService ( com.android.server.ForceDarkUiModeModeManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUiModeManagerService;
} // .end method
static java.lang.String -$$Nest$mgetForceDarkAppConfig ( com.android.server.ForceDarkUiModeModeManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/ForceDarkUiModeModeManager;->getForceDarkAppConfig(Ljava/lang/String;)Ljava/lang/String; */
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.ForceDarkUiModeModeManager.TAG;
} // .end method
static com.android.server.ForceDarkUiModeModeManager ( ) {
/* .locals 1 */
/* .line 30 */
/* const-class v0, Landroid/app/UiModeManager; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
return;
} // .end method
public com.android.server.ForceDarkUiModeModeManager ( ) {
/* .locals 2 */
/* .param p1, "uiModeManagerService" # Lcom/android/server/UiModeManagerService; */
/* .line 46 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 33 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mAppDarkModeObservers = v0;
/* .line 47 */
this.mUiModeManagerService = p1;
/* .line 48 */
/* const-string/jumbo v0, "sys.forcedark.debug" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* iput v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkDebug:I */
/* .line 49 */
/* invoke-direct {p0}, Lcom/android/server/ForceDarkUiModeModeManager;->initForceDarkAppConfig()V */
/* .line 50 */
/* new-instance v0, Lcom/android/server/ForceDarkAppListManager; */
(( com.android.server.UiModeManagerService ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
/* invoke-direct {v0, v1, p0}, Lcom/android/server/ForceDarkAppListManager;-><init>(Landroid/content/Context;Lcom/android/server/ForceDarkUiModeModeManager;)V */
this.mForceDarkAppListManager = v0;
/* .line 52 */
/* new-instance v0, Lcom/android/server/DarkModeTimeModeManager; */
v1 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v1 ).getContext ( ); // invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
/* invoke-direct {v0, v1}, Lcom/android/server/DarkModeTimeModeManager;-><init>(Landroid/content/Context;)V */
this.mDarkModeTimeModeManager = v0;
/* .line 54 */
return;
} // .end method
private void dumpMiuiUiModeManagerStub ( ) {
/* .locals 3 */
/* .line 251 */
v0 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v0 ).getContext ( ); // invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
final String v1 = "android.permission.DUMP"; // const-string v1, "android.permission.DUMP"
v0 = (( android.content.Context ) v0 ).checkCallingOrSelfPermission ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 253 */
return;
/* .line 256 */
} // :cond_0
/* const-class v0, Landroid/app/UiModeManager; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mAppDarkModeObservers callback size = "; // const-string v2, "mAppDarkModeObservers callback size = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mAppDarkModeObservers;
/* .line 257 */
v2 = (( android.os.RemoteCallbackList ) v2 ).getRegisteredCallbackCount ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 256 */
android.util.Slog .d ( v0,v1 );
/* .line 258 */
return;
} // .end method
private java.lang.String getForceDarkAppConfig ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 275 */
v0 = this.mForceDarkAppConfigProvider;
(( com.android.server.ForceDarkAppConfigProvider ) v0 ).getForceDarkAppConfig ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/ForceDarkAppConfigProvider;->getForceDarkAppConfig(Ljava/lang/String;)Ljava/lang/String;
} // .end method
private void initForceDarkAppConfig ( ) {
/* .locals 2 */
/* .line 261 */
com.android.server.ForceDarkAppConfigProvider .getInstance ( );
this.mForceDarkAppConfigProvider = v0;
/* .line 262 */
/* new-instance v1, Lcom/android/server/ForceDarkUiModeModeManager$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/ForceDarkUiModeModeManager$1;-><init>(Lcom/android/server/ForceDarkUiModeModeManager;)V */
(( com.android.server.ForceDarkAppConfigProvider ) v0 ).setAppConfigChangeListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ForceDarkAppConfigProvider;->setAppConfigChangeListener(Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;)V
/* .line 272 */
return;
} // .end method
private Boolean isSystemUiProcess ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 300 */
final String v0 = "com.android.systemui"; // const-string v0, "com.android.systemui"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 9 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 209 */
/* iget v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkDebug:I */
/* if-nez v0, :cond_0 */
/* .line 210 */
return;
/* .line 212 */
} // :cond_0
final String v0 = "ForceDarkUiModeManager"; // const-string v0, "ForceDarkUiModeManager"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 213 */
/* const-string/jumbo v0, "sys.debug.darkmode.analysis" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
/* move v1, v2 */
} // :cond_1
com.android.server.DarkModeStatusTracker.DEBUG = (v1!= 0);
/* .line 214 */
v0 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v0 ).getInnerLock ( ); // invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;
/* monitor-enter v0 */
/* .line 215 */
try { // :try_start_0
v1 = this.mAppDarkModeObservers;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 216 */
/* .local v1, "i":I */
} // :goto_0
/* add-int/lit8 v2, v1, -0x1 */
} // .end local v1 # "i":I
/* .local v2, "i":I */
/* if-lez v1, :cond_4 */
/* .line 217 */
v1 = this.mAppDarkModeObservers;
/* .line 218 */
(( android.os.RemoteCallbackList ) v1 ).getBroadcastCookie ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
/* .line 219 */
/* .local v1, "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
v3 = this.mPackageName;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 220 */
v3 = this.mAppDarkModeObservers;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Landroid/app/IAppDarkModeObserver; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 222 */
/* .local v3, "observer":Landroid/app/IAppDarkModeObserver; */
try { // :try_start_1
/* new-instance v4, Landroid/os/Bundle; */
/* invoke-direct {v4}, Landroid/os/Bundle;-><init>()V */
/* .line 223 */
/* .local v4, "bundle":Landroid/os/Bundle; */
/* .line 224 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "package: "; // const-string v6, "package: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mPackageName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 225 */
(( android.os.Bundle ) v4 ).keySet ( ); // invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_2
/* check-cast v6, Ljava/lang/String; */
/* .line 226 */
/* .local v6, "key":Ljava/lang/String; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ": "; // const-string v8, ": "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.os.Bundle ) v4 ).getString ( v6 ); // invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v7 ); // invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 227 */
} // .end local v6 # "key":Ljava/lang/String;
/* .line 228 */
} // :cond_2
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 231 */
} // .end local v4 # "bundle":Landroid/os/Bundle;
/* .line 229 */
/* :catch_0 */
/* move-exception v4 */
/* .line 230 */
/* .local v4, "e":Landroid/os/RemoteException; */
try { // :try_start_2
(( android.os.RemoteException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 233 */
} // .end local v1 # "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
} // .end local v3 # "observer":Landroid/app/IAppDarkModeObserver;
} // .end local v4 # "e":Landroid/os/RemoteException;
} // :cond_3
} // :goto_2
/* move v1, v2 */
/* .line 234 */
} // :cond_4
v1 = this.mAppDarkModeObservers;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 235 */
} // .end local v2 # "i":I
/* monitor-exit v0 */
/* .line 236 */
return;
/* .line 235 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public void notifyAppListChanged ( ) {
/* .locals 5 */
/* .line 279 */
v0 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v0 ).getInnerLock ( ); // invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;
/* monitor-enter v0 */
/* .line 280 */
try { // :try_start_0
v1 = this.mAppDarkModeObservers;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 282 */
/* .local v1, "i":I */
} // :goto_0
/* add-int/lit8 v2, v1, -0x1 */
} // .end local v1 # "i":I
/* .local v2, "i":I */
/* if-lez v1, :cond_1 */
/* .line 283 */
v1 = this.mAppDarkModeObservers;
/* .line 284 */
(( android.os.RemoteCallbackList ) v1 ).getBroadcastCookie ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
/* .line 285 */
/* .local v1, "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
v3 = this.mPackageName;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/ForceDarkUiModeModeManager;->isSystemUiProcess(Ljava/lang/String;)Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 286 */
v3 = this.mAppDarkModeObservers;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Landroid/app/IAppDarkModeObserver; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 288 */
/* .local v3, "observer":Landroid/app/IAppDarkModeObserver; */
try { // :try_start_1
v4 = this.mForceDarkAppListManager;
(( com.android.server.ForceDarkAppListManager ) v4 ).getAllForceDarkMapForSplashScreen ( ); // invoke-virtual {v4}, Lcom/android/server/ForceDarkAppListManager;->getAllForceDarkMapForSplashScreen()Ljava/util/HashMap;
/* .line 289 */
/* .local v4, "forceDarkForSplashScreen":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 292 */
} // .end local v4 # "forceDarkForSplashScreen":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
/* .line 290 */
/* :catch_0 */
/* move-exception v4 */
/* .line 291 */
/* .local v4, "e":Landroid/os/RemoteException; */
try { // :try_start_2
(( android.os.RemoteException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 294 */
} // .end local v1 # "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
} // .end local v3 # "observer":Landroid/app/IAppDarkModeObserver;
} // .end local v4 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_1
/* move v1, v2 */
/* .line 295 */
} // :cond_1
v1 = this.mAppDarkModeObservers;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 296 */
} // .end local v2 # "i":I
/* monitor-exit v0 */
/* .line 297 */
return;
/* .line 296 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "phase" # I */
/* .line 142 */
/* const/16 v0, 0x226 */
/* if-ne p1, v0, :cond_0 */
/* .line 143 */
final String v0 = "forceDarkCouldData"; // const-string v0, "forceDarkCouldData"
final String v1 = "onBootPhase activity manager ready"; // const-string v1, "onBootPhase activity manager ready"
android.util.Log .i ( v0,v1 );
/* .line 145 */
v0 = this.mForceDarkAppListManager;
v1 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v1 ).getContext ( ); // invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
(( com.android.server.ForceDarkAppListManager ) v0 ).onBootPhase ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/ForceDarkAppListManager;->onBootPhase(ILandroid/content/Context;)V
/* .line 146 */
v0 = this.mForceDarkAppConfigProvider;
v1 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v1 ).getContext ( ); // invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
(( com.android.server.ForceDarkAppConfigProvider ) v0 ).onBootPhase ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/ForceDarkAppConfigProvider;->onBootPhase(ILandroid/content/Context;)V
/* .line 147 */
android.view.ForceDarkHelperStub .getInstance ( );
v1 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v1 ).getContext ( ); // invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
(( android.view.ForceDarkHelperStub ) v0 ).initialize ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/ForceDarkHelperStub;->initialize(Landroid/content/Context;)V
/* .line 148 */
v0 = this.mDarkModeTimeModeManager;
v1 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v1 ).getContext ( ); // invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
(( com.android.server.DarkModeTimeModeManager ) v0 ).onBootPhase ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeTimeModeManager;->onBootPhase(Landroid/content/Context;)V
/* .line 149 */
com.android.server.DarkModeStatusTracker .getIntance ( );
v1 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v1 ).getContext ( ); // invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
v2 = this.mForceDarkAppListManager;
(( com.android.server.DarkModeStatusTracker ) v0 ).init ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/DarkModeStatusTracker;->init(Landroid/content/Context;Lcom/android/server/ForceDarkAppListManager;)V
/* .line 152 */
} // :cond_0
return;
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 61 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
final String v2 = "android.app.IUiModeManager"; // const-string v2, "android.app.IUiModeManager"
/* packed-switch p1, :pswitch_data_0 */
/* .line 137 */
/* .line 64 */
/* :pswitch_0 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 65 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 66 */
/* .local v0, "packageName":Ljava/lang/String; */
v2 = (( android.os.Parcel ) p2 ).readBoolean ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z
/* .line 67 */
/* .local v2, "enable":Z */
v3 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 68 */
/* .local v3, "userId":I */
(( com.android.server.ForceDarkUiModeModeManager ) p0 ).setAppDarkModeEnable ( v0, v2, v3 ); // invoke-virtual {p0, v0, v2, v3}, Lcom/android/server/ForceDarkUiModeModeManager;->setAppDarkModeEnable(Ljava/lang/String;ZI)V
/* .line 69 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 70 */
/* .line 72 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v2 # "enable":Z
} // .end local v3 # "userId":I
/* :pswitch_1 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 73 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 74 */
/* .restart local v0 # "packageName":Ljava/lang/String; */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 75 */
/* .local v2, "userId":I */
v3 = this.mForceDarkAppListManager;
v3 = (( com.android.server.ForceDarkAppListManager ) v3 ).getAppDarkModeEnable ( v0, v2 ); // invoke-virtual {v3, v0, v2}, Lcom/android/server/ForceDarkAppListManager;->getAppDarkModeEnable(Ljava/lang/String;I)Z
/* .line 76 */
/* .local v3, "appDarkModeEnable":Z */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 77 */
(( android.os.Parcel ) p3 ).writeBoolean ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 78 */
/* .line 81 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v2 # "userId":I
} // .end local v3 # "appDarkModeEnable":Z
/* :pswitch_2 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 82 */
/* nop */
/* .line 83 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
android.app.IAppDarkModeObserver$Stub .asInterface ( v0 );
/* .line 84 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
v3 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
v4 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 82 */
(( com.android.server.ForceDarkUiModeModeManager ) p0 ).registerAppDarkModeCallback ( v0, v2, v3, v4 ); // invoke-virtual {p0, v0, v2, v3, v4}, Lcom/android/server/ForceDarkUiModeModeManager;->registerAppDarkModeCallback(Landroid/app/IAppDarkModeObserver;Ljava/lang/String;II)V
/* .line 85 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 86 */
/* .line 89 */
/* :pswitch_3 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 90 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 91 */
/* .restart local v0 # "packageName":Ljava/lang/String; */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 92 */
/* invoke-direct {p0, v0}, Lcom/android/server/ForceDarkUiModeModeManager;->getForceDarkAppConfig(Ljava/lang/String;)Ljava/lang/String; */
/* .line 93 */
/* .local v2, "config":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
/* const-string/jumbo v4, "systemserver package:" */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 94 */
v3 = com.android.server.ForceDarkUiModeModeManager.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " null"; // const-string v5, " null"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 96 */
} // :cond_0
v3 = com.android.server.ForceDarkUiModeModeManager.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " configLength:"; // const-string v5, " configLength:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 97 */
v5 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 96 */
android.util.Slog .i ( v3,v4 );
/* .line 99 */
} // :goto_0
(( android.os.Parcel ) p3 ).writeString ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 100 */
/* .line 103 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v2 # "config":Ljava/lang/String;
/* :pswitch_4 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 104 */
(( android.os.Parcel ) p2 ).readLong ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v2 */
/* .line 105 */
/* .local v2, "appLastUpdateTime":J */
v4 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 106 */
/* .local v4, "userId":I */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 107 */
v5 = this.mForceDarkAppListManager;
(( com.android.server.ForceDarkAppListManager ) v5 ).getDarkModeAppList ( v2, v3, v4 ); // invoke-virtual {v5, v2, v3, v4}, Lcom/android/server/ForceDarkAppListManager;->getDarkModeAppList(JI)Lcom/miui/darkmode/DarkModeAppData;
/* .line 108 */
/* .local v5, "darkModeAppData":Lcom/miui/darkmode/DarkModeAppData; */
(( android.os.Parcel ) p3 ).writeParcelable ( v5, v0 ); // invoke-virtual {p3, v5, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
/* .line 109 */
/* .line 111 */
} // .end local v2 # "appLastUpdateTime":J
} // .end local v4 # "userId":I
} // .end local v5 # "darkModeAppData":Lcom/miui/darkmode/DarkModeAppData;
/* :pswitch_5 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 112 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 113 */
/* .restart local v0 # "packageName":Ljava/lang/String; */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 114 */
v2 = this.mForceDarkAppListManager;
v2 = (( com.android.server.ForceDarkAppListManager ) v2 ).getAppForceDarkOrigin ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/ForceDarkAppListManager;->getAppForceDarkOrigin(Ljava/lang/String;)Z
/* .line 115 */
/* .local v2, "forceDarkOriginState":Z */
(( android.os.Parcel ) p3 ).writeBoolean ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 116 */
/* .line 125 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v2 # "forceDarkOriginState":Z
/* :pswitch_6 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 126 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 127 */
/* .restart local v0 # "packageName":Ljava/lang/String; */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 128 */
/* .local v2, "userId":I */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 129 */
v3 = this.mForceDarkAppListManager;
v3 = (( com.android.server.ForceDarkAppListManager ) v3 ).getInterceptRelaunchType ( v0, v2 ); // invoke-virtual {v3, v0, v2}, Lcom/android/server/ForceDarkAppListManager;->getInterceptRelaunchType(Ljava/lang/String;I)I
/* .line 130 */
/* .local v3, "interceptType":I */
(( android.os.Parcel ) p3 ).writeInt ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 131 */
/* .line 119 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v2 # "userId":I
} // .end local v3 # "interceptType":I
/* :pswitch_7 */
(( android.os.Parcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 120 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 121 */
v0 = this.mForceDarkAppListManager;
(( com.android.server.ForceDarkAppListManager ) v0 ).getAllForceDarkMapForSplashScreen ( ); // invoke-virtual {v0}, Lcom/android/server/ForceDarkAppListManager;->getAllForceDarkMapForSplashScreen()Ljava/util/HashMap;
/* .line 122 */
/* .local v0, "forceDarkForSplashScreen":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;" */
(( android.os.Parcel ) p3 ).writeMap ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V
/* .line 123 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0xfffff7 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void registerAppDarkModeCallback ( android.app.IAppDarkModeObserver p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "observer" # Landroid/app/IAppDarkModeObserver; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "processId" # I */
/* .param p4, "userId" # I */
/* .line 186 */
if ( p1 != null) { // if-eqz p1, :cond_3
v0 = android.text.TextUtils .isEmpty ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 191 */
} // :cond_0
v0 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v0 ).getInnerLock ( ); // invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;
/* monitor-enter v0 */
/* .line 192 */
try { // :try_start_0
v1 = this.mAppDarkModeObservers;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 193 */
/* .local v1, "i":I */
} // :goto_0
/* add-int/lit8 v2, v1, -0x1 */
} // .end local v1 # "i":I
/* .local v2, "i":I */
/* if-lez v1, :cond_2 */
/* .line 194 */
v1 = this.mAppDarkModeObservers;
/* .line 195 */
(( android.os.RemoteCallbackList ) v1 ).getBroadcastCookie ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
/* .line 196 */
/* .local v1, "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
v3 = this.mPackageName;
v3 = (( java.lang.String ) v3 ).equals ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget v3, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mProcessId:I */
/* if-ne v3, p3, :cond_1 */
/* iget v3, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mUserId:I */
/* if-ne v3, p4, :cond_1 */
/* .line 199 */
v3 = this.mAppDarkModeObservers;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v4, Landroid/app/IAppDarkModeObserver; */
(( android.os.RemoteCallbackList ) v3 ).unregister ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 201 */
} // .end local v1 # "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
} // :cond_1
/* move v1, v2 */
/* .line 202 */
} // :cond_2
v1 = this.mAppDarkModeObservers;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 203 */
v1 = this.mAppDarkModeObservers;
/* new-instance v3, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
/* invoke-direct {v3, p2, p3, p4}, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;-><init>(Ljava/lang/String;II)V */
(( android.os.RemoteCallbackList ) v1 ).register ( p1, v3 ); // invoke-virtual {v1, p1, v3}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z
/* .line 205 */
/* nop */
} // .end local v2 # "i":I
/* monitor-exit v0 */
/* .line 206 */
return;
/* .line 205 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 187 */
} // :cond_3
} // :goto_1
v0 = com.android.server.ForceDarkUiModeModeManager.TAG;
final String v1 = "registAppDarkModeCallback param is null"; // const-string v1, "registAppDarkModeCallback param is null"
android.util.Slog .i ( v0,v1 );
/* .line 188 */
return;
} // .end method
public void setAppDarkModeEnable ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .param p3, "userId" # I */
/* .line 155 */
v0 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v0 ).getContext ( ); // invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
final String v1 = "android.permission.MODIFY_DAY_NIGHT_MODE"; // const-string v1, "android.permission.MODIFY_DAY_NIGHT_MODE"
v0 = (( android.content.Context ) v0 ).checkCallingOrSelfPermission ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 157 */
v0 = com.android.server.ForceDarkUiModeModeManager.TAG;
/* const-string/jumbo v1, "setAppDarkModeEnable failed, requires MODIFY_DAY_NIGHT_MODE permission" */
android.util.Slog .e ( v0,v1 );
/* .line 159 */
return;
/* .line 162 */
} // :cond_0
v0 = this.mForceDarkAppListManager;
(( com.android.server.ForceDarkAppListManager ) v0 ).setAppDarkModeEnable ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/ForceDarkAppListManager;->setAppDarkModeEnable(Ljava/lang/String;ZI)V
/* .line 164 */
v0 = this.mUiModeManagerService;
(( com.android.server.UiModeManagerService ) v0 ).getInnerLock ( ); // invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;
/* monitor-enter v0 */
/* .line 165 */
try { // :try_start_0
v1 = this.mAppDarkModeObservers;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 166 */
/* .local v1, "i":I */
} // :goto_0
/* add-int/lit8 v2, v1, -0x1 */
} // .end local v1 # "i":I
/* .local v2, "i":I */
/* if-lez v1, :cond_2 */
/* .line 167 */
v1 = this.mAppDarkModeObservers;
/* .line 168 */
(( android.os.RemoteCallbackList ) v1 ).getBroadcastCookie ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
/* .line 169 */
/* .local v1, "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration; */
v3 = this.mPackageName;
if ( v3 != null) { // if-eqz v3, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
v3 = this.mPackageName;
/* .line 170 */
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 171 */
v3 = this.mAppDarkModeObservers;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Landroid/app/IAppDarkModeObserver; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 173 */
/* .local v3, "observer":Landroid/app/IAppDarkModeObserver; */
try { // :try_start_1
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 176 */
/* .line 174 */
/* :catch_0 */
/* move-exception v4 */
/* .line 175 */
/* .local v4, "e":Landroid/os/RemoteException; */
try { // :try_start_2
(( android.os.RemoteException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 178 */
} // .end local v1 # "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
} // .end local v3 # "observer":Landroid/app/IAppDarkModeObserver;
} // .end local v4 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_1
/* move v1, v2 */
/* .line 179 */
} // :cond_2
v1 = this.mAppDarkModeObservers;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 180 */
} // .end local v2 # "i":I
/* monitor-exit v0 */
/* .line 181 */
return;
/* .line 180 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
