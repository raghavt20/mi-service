.class Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;
.super Landroid/database/ContentObserver;
.source "MiuiUiModeManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->registUIModeScaleChangeObserver(Lcom/android/server/UiModeManagerService;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiUiModeManagerServiceStubImpl;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$service:Lcom/android/server/UiModeManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;Landroid/os/Handler;Landroid/content/Context;Lcom/android/server/UiModeManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiUiModeManagerServiceStubImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 151
    iput-object p1, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;->this$0:Lcom/android/server/MiuiUiModeManagerServiceStubImpl;

    iput-object p3, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;->val$service:Lcom/android/server/UiModeManagerService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .line 154
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "ui_mode_scale"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 156
    .local v0, "uiModeType":I
    sget-object v1, Lcom/android/server/UiModeManagerServiceProxy;->mDefaultUiModeType:Lcom/xiaomi/reflect/RefInt;

    iget-object v2, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;->val$service:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v1, v2, v0}, Lcom/xiaomi/reflect/RefInt;->set(Ljava/lang/Object;I)V

    .line 157
    iget-object v1, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;->this$0:Lcom/android/server/MiuiUiModeManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->-$$Nest$fgetmUiModeManagerService(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;)Lcom/android/server/UiModeManagerService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 158
    :try_start_0
    iget-object v2, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;->val$service:Lcom/android/server/UiModeManagerService;

    iget-boolean v2, v2, Lcom/android/server/UiModeManagerService;->mSystemReady:Z

    if-eqz v2, :cond_0

    .line 159
    iget-object v2, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;->val$service:Lcom/android/server/UiModeManagerService;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v3}, Lcom/android/server/UiModeManagerService;->updateLocked(II)V

    .line 161
    :cond_0
    monitor-exit v1

    .line 162
    return-void

    .line 161
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
