public class com.android.server.ForceDarkAppListProvider {
	 /* .source "ForceDarkAppListProvider.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String FORCE_DARK_APP_SETTINGS_FILE_PATH;
private static final java.lang.String FORCE_DARK_APP_SETTINGS_MODULE_NAME;
private static final java.lang.String TAG;
private static volatile com.android.server.ForceDarkAppListProvider sForceDarkAppListProvider;
/* # instance fields */
private com.android.server.ForceDarkAppListProvider$ForceDarkAppListChangeListener mAppListChangeListener;
private java.util.HashMap mCloudForceDarkAppSettings;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/DarkModeAppSettingsInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mLocalForceDarkAppSettings;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/DarkModeAppSettingsInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.ForceDarkAppListProvider ( ) {
/* .locals 1 */
/* .line 27 */
/* const-class v0, Lcom/android/server/ForceDarkAppListProvider; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 29 */
int v0 = 0; // const/4 v0, 0x0
return;
} // .end method
private com.android.server.ForceDarkAppListProvider ( ) {
/* .locals 1 */
/* .line 39 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 36 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mLocalForceDarkAppSettings = v0;
/* .line 37 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCloudForceDarkAppSettings = v0;
/* .line 40 */
/* invoke-direct {p0}, Lcom/android/server/ForceDarkAppListProvider;->initLocalForceDarkAppList()V */
/* .line 41 */
return;
} // .end method
private void fillDarkModeAppSettingsInfo ( com.android.server.DarkModeAppSettingsInfo p0, org.json.JSONObject p1 ) {
/* .locals 8 */
/* .param p1, "darkModeAppSettingsInfo" # Lcom/android/server/DarkModeAppSettingsInfo; */
/* .param p2, "appConfig" # Lorg/json/JSONObject; */
/* .line 172 */
final String v0 = "interceptRelaunch"; // const-string v0, "interceptRelaunch"
final String v1 = "forceDarkSplashScreen"; // const-string v1, "forceDarkSplashScreen"
final String v2 = "forceDarkOrigin"; // const-string v2, "forceDarkOrigin"
final String v3 = "overrideEnableValue"; // const-string v3, "overrideEnableValue"
final String v4 = "adaptStat"; // const-string v4, "adaptStat"
final String v5 = "defaultEnable"; // const-string v5, "defaultEnable"
/* const-string/jumbo v6, "showInSettings" */
try { // :try_start_0
v7 = (( org.json.JSONObject ) p2 ).has ( v6 ); // invoke-virtual {p2, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 173 */
v6 = (( org.json.JSONObject ) p2 ).getBoolean ( v6 ); // invoke-virtual {p2, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
(( com.android.server.DarkModeAppSettingsInfo ) p1 ).setShowInSettings ( v6 ); // invoke-virtual {p1, v6}, Lcom/android/server/DarkModeAppSettingsInfo;->setShowInSettings(Z)V
/* .line 175 */
} // :cond_0
v6 = (( org.json.JSONObject ) p2 ).has ( v5 ); // invoke-virtual {p2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 176 */
v5 = (( org.json.JSONObject ) p2 ).getBoolean ( v5 ); // invoke-virtual {p2, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
(( com.android.server.DarkModeAppSettingsInfo ) p1 ).setDefaultEnable ( v5 ); // invoke-virtual {p1, v5}, Lcom/android/server/DarkModeAppSettingsInfo;->setDefaultEnable(Z)V
/* .line 178 */
} // :cond_1
v5 = (( org.json.JSONObject ) p2 ).has ( v4 ); // invoke-virtual {p2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 179 */
v4 = (( org.json.JSONObject ) p2 ).getInt ( v4 ); // invoke-virtual {p2, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
(( com.android.server.DarkModeAppSettingsInfo ) p1 ).setAdaptStat ( v4 ); // invoke-virtual {p1, v4}, Lcom/android/server/DarkModeAppSettingsInfo;->setAdaptStat(I)V
/* .line 181 */
} // :cond_2
v4 = (( org.json.JSONObject ) p2 ).has ( v3 ); // invoke-virtual {p2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 182 */
v3 = (( org.json.JSONObject ) p2 ).getInt ( v3 ); // invoke-virtual {p2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
(( com.android.server.DarkModeAppSettingsInfo ) p1 ).setOverrideEnableValue ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/server/DarkModeAppSettingsInfo;->setOverrideEnableValue(I)V
/* .line 184 */
} // :cond_3
v3 = (( org.json.JSONObject ) p2 ).has ( v2 ); // invoke-virtual {p2, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 185 */
v2 = (( org.json.JSONObject ) p2 ).getBoolean ( v2 ); // invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
(( com.android.server.DarkModeAppSettingsInfo ) p1 ).setForceDarkOrigin ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/DarkModeAppSettingsInfo;->setForceDarkOrigin(Z)V
/* .line 187 */
} // :cond_4
v2 = (( org.json.JSONObject ) p2 ).has ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 188 */
v1 = (( org.json.JSONObject ) p2 ).getInt ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
(( com.android.server.DarkModeAppSettingsInfo ) p1 ).setForceDarkSplashScreen ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/DarkModeAppSettingsInfo;->setForceDarkSplashScreen(I)V
/* .line 190 */
} // :cond_5
v1 = (( org.json.JSONObject ) p2 ).has ( v0 ); // invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 191 */
v0 = (( org.json.JSONObject ) p2 ).getInt ( v0 ); // invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
(( com.android.server.DarkModeAppSettingsInfo ) p1 ).setInterceptRelaunch ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeAppSettingsInfo;->setInterceptRelaunch(I)V
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 195 */
} // :cond_6
/* .line 193 */
/* :catch_0 */
/* move-exception v0 */
/* .line 194 */
/* .local v0, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 197 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_0
return;
} // .end method
public static com.android.server.ForceDarkAppListProvider getInstance ( ) {
/* .locals 2 */
/* .line 44 */
v0 = com.android.server.ForceDarkAppListProvider.sForceDarkAppListProvider;
/* if-nez v0, :cond_1 */
/* .line 45 */
/* const-class v0, Lcom/android/server/ForceDarkAppListProvider; */
/* monitor-enter v0 */
/* .line 46 */
try { // :try_start_0
v1 = com.android.server.ForceDarkAppListProvider.sForceDarkAppListProvider;
/* if-nez v1, :cond_0 */
/* .line 47 */
/* new-instance v1, Lcom/android/server/ForceDarkAppListProvider; */
/* invoke-direct {v1}, Lcom/android/server/ForceDarkAppListProvider;-><init>()V */
/* .line 49 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 51 */
} // :cond_1
} // :goto_0
v0 = com.android.server.ForceDarkAppListProvider.sForceDarkAppListProvider;
} // .end method
private void initLocalForceDarkAppList ( ) {
/* .locals 5 */
/* .line 86 */
final String v0 = ""; // const-string v0, ""
/* .line 88 */
/* .local v0, "jsonString":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* const-string/jumbo v2, "system_ext/etc/forcedarkconfig/ForceDarkAppSettings.json" */
/* invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 89 */
/* .local v1, "f":Ljava/io/FileInputStream; */
try { // :try_start_1
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/InputStreamReader; */
/* invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 91 */
/* .local v2, "bis":Ljava/io/BufferedReader; */
} // :goto_0
try { // :try_start_2
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "line":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 92 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* move-object v0, v3 */
/* .line 94 */
} // .end local v4 # "line":Ljava/lang/String;
} // :cond_0
try { // :try_start_3
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* .line 96 */
} // .end local v2 # "bis":Ljava/io/BufferedReader;
/* .line 89 */
/* .restart local v2 # "bis":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_4
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_5
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "jsonString":Ljava/lang/String;
} // .end local v1 # "f":Ljava/io/FileInputStream;
} // .end local p0 # "this":Lcom/android/server/ForceDarkAppListProvider;
} // :goto_1
/* throw v3 */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_0 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* .line 88 */
} // .end local v2 # "bis":Ljava/io/BufferedReader;
/* .restart local v0 # "jsonString":Ljava/lang/String; */
/* .restart local v1 # "f":Ljava/io/FileInputStream; */
/* .restart local p0 # "this":Lcom/android/server/ForceDarkAppListProvider; */
/* :catchall_2 */
/* move-exception v2 */
/* .line 94 */
/* :catch_0 */
/* move-exception v2 */
/* .line 95 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_6
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* .line 97 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
try { // :try_start_7
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_1 */
/* .line 99 */
} // .end local v1 # "f":Ljava/io/FileInputStream;
/* .line 88 */
/* .restart local v1 # "f":Ljava/io/FileInputStream; */
} // :goto_3
try { // :try_start_8
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_3 */
/* :catchall_3 */
/* move-exception v3 */
try { // :try_start_9
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "jsonString":Ljava/lang/String;
} // .end local p0 # "this":Lcom/android/server/ForceDarkAppListProvider;
} // :goto_4
/* throw v2 */
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_1 */
/* .line 97 */
} // .end local v1 # "f":Ljava/io/FileInputStream;
/* .restart local v0 # "jsonString":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/android/server/ForceDarkAppListProvider; */
/* :catch_1 */
/* move-exception v1 */
/* .line 98 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 101 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_5
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 102 */
return;
/* .line 105 */
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/android/server/ForceDarkAppListProvider;->parseAppSettings2Map(Ljava/lang/String;)Ljava/util/HashMap; */
this.mLocalForceDarkAppSettings = v1;
/* .line 106 */
return;
} // .end method
static com.android.server.DarkModeAppSettingsInfo lambda$getForceDarkAppSettings$0 ( com.android.server.DarkModeAppSettingsInfo p0, com.android.server.DarkModeAppSettingsInfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "v1" # Lcom/android/server/DarkModeAppSettingsInfo; */
/* .param p1, "v2" # Lcom/android/server/DarkModeAppSettingsInfo; */
/* .line 70 */
} // .end method
static void lambda$getForceDarkAppSettings$1 ( java.util.HashMap p0, java.lang.String p1, com.android.server.DarkModeAppSettingsInfo p2 ) { //synthethic
/* .locals 1 */
/* .param p0, "result" # Ljava/util/HashMap; */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Lcom/android/server/DarkModeAppSettingsInfo; */
/* .line 70 */
/* new-instance v0, Lcom/android/server/ForceDarkAppListProvider$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/ForceDarkAppListProvider$$ExternalSyntheticLambda0;-><init>()V */
(( java.util.HashMap ) p0 ).merge ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Ljava/util/HashMap;->merge(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;
return;
} // .end method
private java.util.HashMap parseAppSettings2Map ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "jsonData" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/DarkModeAppSettingsInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 130 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 131 */
/* .local v0, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;" */
v1 = android.text.TextUtils .isEmpty ( p1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 132 */
/* .line 135 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 136 */
/* .local v1, "appConfigArray":Lorg/json/JSONArray; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-ge v2, v3, :cond_1 */
/* .line 137 */
/* new-instance v3, Lcom/android/server/DarkModeAppSettingsInfo; */
/* invoke-direct {v3}, Lcom/android/server/DarkModeAppSettingsInfo;-><init>()V */
/* .line 138 */
/* .local v3, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo; */
(( org.json.JSONArray ) v1 ).getJSONObject ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 139 */
/* .local v4, "appConfig":Lorg/json/JSONObject; */
final String v5 = "packageName"; // const-string v5, "packageName"
(( org.json.JSONObject ) v4 ).getString ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 140 */
/* .local v5, "packageName":Ljava/lang/String; */
(( com.android.server.DarkModeAppSettingsInfo ) v3 ).setPackageName ( v5 ); // invoke-virtual {v3, v5}, Lcom/android/server/DarkModeAppSettingsInfo;->setPackageName(Ljava/lang/String;)V
/* .line 141 */
/* invoke-direct {p0, v3, v4}, Lcom/android/server/ForceDarkAppListProvider;->fillDarkModeAppSettingsInfo(Lcom/android/server/DarkModeAppSettingsInfo;Lorg/json/JSONObject;)V */
/* .line 142 */
(( java.util.HashMap ) v0 ).put ( v5, v3 ); // invoke-virtual {v0, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 136 */
/* nop */
} // .end local v3 # "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
} // .end local v4 # "appConfig":Lorg/json/JSONObject;
} // .end local v5 # "packageName":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 146 */
} // .end local v1 # "appConfigArray":Lorg/json/JSONArray;
} // .end local v2 # "i":I
} // :cond_1
/* .line 144 */
/* :catch_0 */
/* move-exception v1 */
/* .line 145 */
/* .local v1, "e":Lorg/json/JSONException; */
v2 = com.android.server.ForceDarkAppListProvider.TAG;
final String v3 = "exception when parseAppConfig2Map: "; // const-string v3, "exception when parseAppConfig2Map: "
android.util.Log .e ( v2,v3,v1 );
/* .line 147 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_1
} // .end method
private java.util.HashMap parseAppSettings2Map ( org.json.JSONArray p0 ) {
/* .locals 5 */
/* .param p1, "appConfigArray" # Lorg/json/JSONArray; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lorg/json/JSONArray;", */
/* ")", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/DarkModeAppSettingsInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 151 */
/* if-nez p1, :cond_0 */
/* .line 152 */
int v0 = 0; // const/4 v0, 0x0
/* .line 154 */
} // :cond_0
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 156 */
/* .local v0, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v2, :cond_1 */
/* .line 157 */
/* new-instance v2, Lcom/android/server/DarkModeAppSettingsInfo; */
/* invoke-direct {v2}, Lcom/android/server/DarkModeAppSettingsInfo;-><init>()V */
/* .line 158 */
/* .local v2, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo; */
(( org.json.JSONArray ) p1 ).getJSONObject ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 159 */
/* .local v3, "appConfig":Lorg/json/JSONObject; */
final String v4 = "packageName"; // const-string v4, "packageName"
(( org.json.JSONObject ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 160 */
/* .local v4, "packageName":Ljava/lang/String; */
(( com.android.server.DarkModeAppSettingsInfo ) v2 ).setPackageName ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/DarkModeAppSettingsInfo;->setPackageName(Ljava/lang/String;)V
/* .line 161 */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/ForceDarkAppListProvider;->fillDarkModeAppSettingsInfo(Lcom/android/server/DarkModeAppSettingsInfo;Lorg/json/JSONObject;)V */
/* .line 162 */
(( java.util.HashMap ) v0 ).put ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 156 */
/* nop */
} // .end local v2 # "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
} // .end local v3 # "appConfig":Lorg/json/JSONObject;
} // .end local v4 # "packageName":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 166 */
} // .end local v1 # "i":I
} // :cond_1
/* .line 164 */
/* :catch_0 */
/* move-exception v1 */
/* .line 165 */
/* .local v1, "e":Lorg/json/JSONException; */
v2 = com.android.server.ForceDarkAppListProvider.TAG;
final String v3 = "exception when parseAppConfig2Map: "; // const-string v3, "exception when parseAppConfig2Map: "
android.util.Log .e ( v2,v3,v1 );
/* .line 167 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_1
} // .end method
private void registerDataObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 200 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 201 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/ForceDarkAppListProvider$1; */
/* .line 202 */
com.android.server.MiuiBgThread .getHandler ( );
/* invoke-direct {v2, p0, v3, p1}, Lcom/android/server/ForceDarkAppListProvider$1;-><init>(Lcom/android/server/ForceDarkAppListProvider;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 200 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 209 */
return;
} // .end method
/* # virtual methods */
public Boolean getForceDarkAppDefaultEnable ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 75 */
(( com.android.server.ForceDarkAppListProvider ) p0 ).getForceDarkAppSettings ( ); // invoke-virtual {p0}, Lcom/android/server/ForceDarkAppListProvider;->getForceDarkAppSettings()Ljava/util/HashMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/DarkModeAppSettingsInfo; */
/* .line 76 */
/* .local v0, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.DarkModeAppSettingsInfo ) v0 ).isDefaultEnable ( ); // invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->isDefaultEnable()Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public java.util.HashMap getForceDarkAppSettings ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/DarkModeAppSettingsInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 68 */
/* new-instance v0, Ljava/util/HashMap; */
v1 = this.mLocalForceDarkAppSettings;
/* invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
/* .line 69 */
/* .local v0, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;" */
v1 = this.mCloudForceDarkAppSettings;
/* new-instance v2, Lcom/android/server/ForceDarkAppListProvider$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, v0}, Lcom/android/server/ForceDarkAppListProvider$$ExternalSyntheticLambda1;-><init>(Ljava/util/HashMap;)V */
(( java.util.HashMap ) v1 ).forEach ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 71 */
} // .end method
public void onBootPhase ( Integer p0, android.content.Context p1 ) {
/* .locals 0 */
/* .param p1, "phase" # I */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 80 */
(( com.android.server.ForceDarkAppListProvider ) p0 ).updateCloudForceDarkAppList ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/ForceDarkAppListProvider;->updateCloudForceDarkAppList(Landroid/content/Context;)V
/* .line 81 */
/* invoke-direct {p0, p2}, Lcom/android/server/ForceDarkAppListProvider;->registerDataObserver(Landroid/content/Context;)V */
/* .line 82 */
return;
} // .end method
public void setAppListChangeListener ( com.android.server.ForceDarkAppListProvider$ForceDarkAppListChangeListener p0 ) {
/* .locals 0 */
/* .param p1, "listChangeListener" # Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener; */
/* .line 60 */
this.mAppListChangeListener = p1;
/* .line 61 */
return;
} // .end method
public void updateCloudForceDarkAppList ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 111 */
final String v0 = "force_dark_app_settings_list"; // const-string v0, "force_dark_app_settings_list"
/* .line 112 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 111 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v1,v0,v0,v2,v3 );
/* .line 113 */
/* .local v1, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v1 != null) { // if-eqz v1, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 114 */
v2 = com.android.server.ForceDarkAppListProvider.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getCloudForceDarkAppList: CloudData: "; // const-string v4, "getCloudForceDarkAppList: CloudData: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 115 */
v2 = this.mCloudForceDarkAppSettings;
(( java.util.HashMap ) v2 ).clear ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->clear()V
/* .line 116 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v2 ).getJSONArray ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 117 */
/* .local v0, "appConfigArray":Lorg/json/JSONArray; */
/* invoke-direct {p0, v0}, Lcom/android/server/ForceDarkAppListProvider;->parseAppSettings2Map(Lorg/json/JSONArray;)Ljava/util/HashMap; */
this.mCloudForceDarkAppSettings = v2;
/* .line 119 */
v2 = this.mAppListChangeListener;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 120 */
/* .line 123 */
} // .end local v0 # "appConfigArray":Lorg/json/JSONArray;
} // :cond_0
v0 = com.android.server.ForceDarkAppListProvider.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getCloudForceDarkAppList: mCloudForceDarkAppSettings: "; // const-string v3, "getCloudForceDarkAppList: mCloudForceDarkAppSettings: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCloudForceDarkAppSettings;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 126 */
/* nop */
} // .end local v1 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
/* .line 124 */
/* :catch_0 */
/* move-exception v0 */
/* .line 125 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.ForceDarkAppListProvider.TAG;
final String v2 = "exception when getCloudForceDarkAppList: "; // const-string v2, "exception when getCloudForceDarkAppList: "
android.util.Log .e ( v1,v2,v0 );
/* .line 127 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
