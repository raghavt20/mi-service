.class public Lcom/android/server/MiuiBatteryStatsService;
.super Ljava/lang/Object;
.source "MiuiBatteryStatsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;,
        Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;,
        Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;,
        Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;,
        Lcom/android/server/MiuiBatteryStatsService$TrackBatteryUsbInfo;,
        Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;
    }
.end annotation


# static fields
.field public static final ADJUST_VOLTAGE:Ljava/lang/String; = "miui.intent.action.ADJUST_VOLTAGE"

.field public static final ADJUST_VOLTAGE_TL_EXTRA:Ljava/lang/String; = "miui.intent.extra.ADJUST_VOLTAGE_TL"

.field public static final ADJUST_VOLTAGE_TS_EXTRA:Ljava/lang/String; = "miui.intent.extra.ADJUST_VOLTAGE_TS"

.field public static final CHECK_SOC:Ljava/lang/String; = "miui.intent.action.CHECK_SOC"

.field public static final CYCLE_CHECK:Ljava/lang/String; = "miui.intent.action.CYCLE_CHECK"

.field private static final DAY:J

.field private static final FIVEMIN:J = 0x493e0L

.field private static final HALFMIN:J = 0x7530L

.field private static volatile INSTANCE:Lcom/android/server/MiuiBatteryStatsService; = null

.field public static final LIMIT_TIME:Ljava/lang/String; = "miui.intent.action.LIMIT_TIME"

.field private static final ONEHOUR:J = 0x36ee80L

.field private static final TENMIN:J = 0x927c0L

.field private static final TWOHOUR:J = 0x6ddd00L

.field public static final UPDATE_BATTERY_DATA:Ljava/lang/String; = "miui.intent.action.UPDATE_BATTERY_DATA"

.field private static mIsSatisfyTempLevelCondition:Z

.field private static mIsSatisfyTempSocCondition:Z


# instance fields
.field private final DEBUG:Z

.field private final TAG:Ljava/lang/String;

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mAppUsageStats:Lcom/android/server/MiuiAppUsageStats;

.field private mBatteryInfoFull:Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

.field private mBatteryInfoNormal:Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

.field private mBatteryTempLevel:Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;

.field private mBatteryTempSocTime:Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

.field private mBootCompleted:Z

.field private mChargeEndCapacity:I

.field private mChargeEndTime:J

.field private mChargeMaxTemp:I

.field private mChargeMinTemp:I

.field private mChargeStartCapacity:I

.field private mChargeStartTime:J

.field private final mContext:Landroid/content/Context;

.field private mDischargingCount:I

.field private mFullChargeEndTime:J

.field private mFullChargeStartTime:J

.field private final mHandler:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

.field private mIsHandleIntermittentCharge:Z

.field private mIsOrderedCheckSocTimer:Z

.field private mIsScreenOn:Z

.field private mIsTablet:Z

.field private mLastBatteryStatus:I

.field private mLastBatteryTemp:I

.field private mLastBatteryVoltage:I

.field private mLastLpdState:I

.field private mLastPlugged:Z

.field private mLastSoc:I

.field private mLpdCount:I

.field private mMiCharge:Lmiui/util/IMiCharge;

.field private mOtgConnected:Z

.field private mPendingIntent:Landroid/app/PendingIntent;

.field private mPendingIntentCheckSoc:Landroid/app/PendingIntent;

.field private mPendingIntentCycleCheck:Landroid/app/PendingIntent;

.field private mPendingIntentLimitTime:Landroid/app/PendingIntent;

.field private mPlugType:I

.field private mScreenOnChargingStart:J

.field private mScreenOnTime:J

.field private mStartRecordDischarging:Z

.field private mSupportedCellVolt:Z

.field private mSupportedSB:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->DEBUG:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mAlarmManager:Landroid/app/AlarmManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAppUsageStats(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiAppUsageStats;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mAppUsageStats:Lcom/android/server/MiuiAppUsageStats;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBatteryInfoFull(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mBatteryInfoFull:Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBatteryInfoNormal(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mBatteryInfoNormal:Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBatteryTempLevel(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mBatteryTempLevel:Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBatteryTempSocTime(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mBatteryTempSocTime:Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBootCompleted(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mBootCompleted:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmChargeEndCapacity(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndCapacity:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmChargeMaxTemp(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMaxTemp:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmChargeMinTemp(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMinTemp:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmChargeStartCapacity(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartCapacity:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryStatsService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDischargingCount(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mDischargingCount:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmFullChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeEndTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmFullChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeStartTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mHandler:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsHandleIntermittentCharge(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsHandleIntermittentCharge:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsOrderedCheckSocTimer(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsOrderedCheckSocTimer:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsScreenOn(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsScreenOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsTablet(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsTablet:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastBatteryStatus(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryStatus:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastBatteryTemp(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryTemp:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastBatteryVoltage(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryVoltage:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastLpdState(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastLpdState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastPlugged:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastSoc(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastSoc:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLpdCount(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLpdCount:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mMiCharge:Lmiui/util/IMiCharge;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmOtgConnected(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mOtgConnected:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPendingIntent(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mPendingIntent:Landroid/app/PendingIntent;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPendingIntentCheckSoc(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mPendingIntentCheckSoc:Landroid/app/PendingIntent;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPendingIntentCycleCheck(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mPendingIntentCycleCheck:Landroid/app/PendingIntent;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPendingIntentLimitTime(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mPendingIntentLimitTime:Landroid/app/PendingIntent;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPlugType(Lcom/android/server/MiuiBatteryStatsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mPlugType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnChargingStart:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenOnTime(Lcom/android/server/MiuiBatteryStatsService;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmStartRecordDischarging(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mStartRecordDischarging:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportedCellVolt(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mSupportedCellVolt:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportedSB(Lcom/android/server/MiuiBatteryStatsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mSupportedSB:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmBootCompleted(Lcom/android/server/MiuiBatteryStatsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mBootCompleted:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmChargeEndCapacity(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndCapacity:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmChargeMaxTemp(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMaxTemp:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmChargeMinTemp(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMinTemp:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmChargeStartCapacity(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartCapacity:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDischargingCount(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mDischargingCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFullChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeEndTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFullChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeStartTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsHandleIntermittentCharge(Lcom/android/server/MiuiBatteryStatsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsHandleIntermittentCharge:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsOrderedCheckSocTimer(Lcom/android/server/MiuiBatteryStatsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsOrderedCheckSocTimer:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsScreenOn(Lcom/android/server/MiuiBatteryStatsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsScreenOn:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastBatteryStatus(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryStatus:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastBatteryTemp(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryTemp:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastBatteryVoltage(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryVoltage:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastLpdState(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastLpdState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastPlugged:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastSoc(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastSoc:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLpdCount(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLpdCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmOtgConnected(Lcom/android/server/MiuiBatteryStatsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mOtgConnected:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPlugType(Lcom/android/server/MiuiBatteryStatsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mPlugType:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnChargingStart:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScreenOnTime(Lcom/android/server/MiuiBatteryStatsService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmStartRecordDischarging(Lcom/android/server/MiuiBatteryStatsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mStartRecordDischarging:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSupportedCellVolt(Lcom/android/server/MiuiBatteryStatsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mSupportedCellVolt:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSupportedSB(Lcom/android/server/MiuiBatteryStatsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mSupportedSB:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDAY()J
    .locals 2

    sget-wide v0, Lcom/android/server/MiuiBatteryStatsService;->DAY:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$sfgetmIsSatisfyTempLevelCondition()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/MiuiBatteryStatsService;->mIsSatisfyTempLevelCondition:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmIsSatisfyTempSocCondition()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/MiuiBatteryStatsService;->mIsSatisfyTempSocCondition:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfputmIsSatisfyTempLevelCondition(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/MiuiBatteryStatsService;->mIsSatisfyTempLevelCondition:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmIsSatisfyTempSocCondition(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/MiuiBatteryStatsService;->mIsSatisfyTempSocCondition:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 63
    const-string v0, "persist.sys.report_time"

    const v1, 0x15180

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/android/server/MiuiBatteryStatsService;->DAY:J

    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/MiuiBatteryStatsService;->INSTANCE:Lcom/android/server/MiuiBatteryStatsService;

    .line 71
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/MiuiBatteryStatsService;->mIsSatisfyTempLevelCondition:Z

    .line 72
    sput-boolean v0, Lcom/android/server/MiuiBatteryStatsService;->mIsSatisfyTempSocCondition:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-string v0, "MiuiBatteryStatsService"

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService;->TAG:Ljava/lang/String;

    .line 51
    const-string v1, "persist.sys.debug_stats"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/MiuiBatteryStatsService;->DEBUG:Z

    .line 84
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartTime:J

    .line 85
    iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndTime:J

    .line 86
    iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeStartTime:J

    .line 87
    iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeEndTime:J

    .line 88
    iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnChargingStart:J

    .line 89
    iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnTime:J

    .line 90
    iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartCapacity:I

    .line 91
    iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndCapacity:I

    .line 92
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryStatus:I

    .line 93
    iput v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastSoc:I

    .line 94
    iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMaxTemp:I

    .line 95
    iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMinTemp:I

    .line 96
    iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mDischargingCount:I

    .line 97
    iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsScreenOn:Z

    .line 98
    iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mStartRecordDischarging:Z

    .line 99
    iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsHandleIntermittentCharge:Z

    .line 100
    iput v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mPlugType:I

    .line 101
    iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mOtgConnected:Z

    .line 102
    iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsOrderedCheckSocTimer:Z

    .line 104
    invoke-static {}, Lmiui/util/IMiCharge;->getInstance()Lmiui/util/IMiCharge;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mMiCharge:Lmiui/util/IMiCharge;

    .line 105
    new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    const-string v4, "Full"

    invoke-direct {v3, p0, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mBatteryInfoFull:Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    .line 106
    new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    const-string v4, "Normal"

    invoke-direct {v3, p0, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mBatteryInfoNormal:Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    .line 107
    new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    invoke-direct {v3, p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;)V

    iput-object v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mBatteryTempSocTime:Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    .line 108
    new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;

    invoke-direct {v3, p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;)V

    iput-object v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mBatteryTempLevel:Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;

    .line 128
    iput-object p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mContext:Landroid/content/Context;

    .line 129
    new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;-><init>(Lcom/android/server/MiuiBatteryStatsService;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mHandler:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    .line 130
    new-instance v3, Lcom/android/server/MiuiAppUsageStats;

    invoke-direct {v3}, Lcom/android/server/MiuiAppUsageStats;-><init>()V

    iput-object v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mAppUsageStats:Lcom/android/server/MiuiAppUsageStats;

    .line 131
    const-string v3, "ro.build.characteristics"

    const-string v4, ""

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const-string/jumbo v4, "tablet"

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsTablet:Z

    .line 133
    const-string v3, "alarm"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/AlarmManager;

    iput-object v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mAlarmManager:Landroid/app/AlarmManager;

    .line 135
    new-instance v3, Lcom/android/server/MiuiBatteryStatsService$1;

    invoke-direct {v3, p0}, Lcom/android/server/MiuiBatteryStatsService$1;-><init>(Lcom/android/server/MiuiBatteryStatsService;)V

    .line 317
    .local v3, "stateChangedReceiver":Landroid/content/BroadcastReceiver;
    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.hardware.usb.action.USB_STATE"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 318
    .local v4, "filter":Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 319
    const-string v5, "miui.intent.action.UPDATE_BATTERY_DATA"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 320
    const-string v6, "miui.intent.action.ACTION_SHUTDOWN_DELAY"

    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 321
    const-string v6, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 322
    const-string v6, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 323
    const-string v6, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 324
    const-string v6, "android.intent.action.SCREEN_ON"

    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 325
    const-string v6, "miui.intent.action.LIMIT_TIME"

    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 326
    const-string v7, "miui.intent.action.CYCLE_CHECK"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 327
    const-string v8, "android.hardware.usb.action.USB_PORT_CHANGED"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 328
    const-string v8, "miui.intent.action.CHECK_SOC"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 329
    const/4 v9, 0x2

    invoke-virtual {p1, v3, v4, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 331
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v5, v10

    .line 332
    .local v5, "intent":Landroid/content/Intent;
    const/high16 v10, 0x40000000    # 2.0f

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 333
    const/high16 v11, 0x4000000

    invoke-static {p1, v2, v5, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v12

    iput-object v12, p0, Lcom/android/server/MiuiBatteryStatsService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 335
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v6, v12

    .line 336
    .local v6, "limitTime":Landroid/content/Intent;
    invoke-virtual {v6, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 337
    invoke-static {p1, v2, v6, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v12

    iput-object v12, p0, Lcom/android/server/MiuiBatteryStatsService;->mPendingIntentLimitTime:Landroid/app/PendingIntent;

    .line 339
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v7, v12

    .line 340
    .local v7, "cycleCheck":Landroid/content/Intent;
    invoke-virtual {v7, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 341
    invoke-static {p1, v2, v7, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v12

    iput-object v12, p0, Lcom/android/server/MiuiBatteryStatsService;->mPendingIntentCycleCheck:Landroid/app/PendingIntent;

    .line 343
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v8, v12

    .line 344
    .local v8, "checkSoc":Landroid/content/Intent;
    invoke-virtual {v8, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 345
    invoke-static {p1, v2, v8, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mPendingIntentCheckSoc:Landroid/app/PendingIntent;

    .line 348
    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DAY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v10, Lcom/android/server/MiuiBatteryStatsService;->DAY:J

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mAlarmManager:Landroid/app/AlarmManager;

    .line 350
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v10, p0, Lcom/android/server/MiuiBatteryStatsService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 349
    invoke-virtual {v0, v9, v1, v2, v10}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 351
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/MiuiBatteryStatsService;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 117
    sget-object v0, Lcom/android/server/MiuiBatteryStatsService;->INSTANCE:Lcom/android/server/MiuiBatteryStatsService;

    if-nez v0, :cond_1

    .line 118
    const-class v0, Lcom/android/server/MiuiBatteryStatsService;

    monitor-enter v0

    .line 119
    :try_start_0
    sget-object v1, Lcom/android/server/MiuiBatteryStatsService;->INSTANCE:Lcom/android/server/MiuiBatteryStatsService;

    if-nez v1, :cond_0

    .line 120
    new-instance v1, Lcom/android/server/MiuiBatteryStatsService;

    invoke-direct {v1, p0}, Lcom/android/server/MiuiBatteryStatsService;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/MiuiBatteryStatsService;->INSTANCE:Lcom/android/server/MiuiBatteryStatsService;

    .line 122
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 124
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/MiuiBatteryStatsService;->INSTANCE:Lcom/android/server/MiuiBatteryStatsService;

    return-object v0
.end method
