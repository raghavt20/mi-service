.class public Lcom/android/server/wm/SchedBoostGesturesEvent;
.super Ljava/lang/Object;
.source "SchedBoostGesturesEvent.java"

# interfaces
.implements Lcom/miui/server/input/gesture/MiuiGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;,
        Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;,
        Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;
    }
.end annotation


# static fields
.field private static final MAX_FLING_TIME_MILLIS:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "SchedBoostGestures"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDisplayId:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mGesturesEventListener:Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

.field private mHandler:Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;

.field private mLastFlingTime:J

.field private mScrollFired:Z


# direct methods
.method public static synthetic $r8$lambda$DcdU_oZ1m8deABLpUsOv2ceaqK0(Lcom/android/server/wm/SchedBoostGesturesEvent;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->lambda$onPointerEvent$1(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public static synthetic $r8$lambda$FCKQg3Og1OhtnbQHGwZSF-gBwNM(Lcom/android/server/wm/SchedBoostGesturesEvent;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/SchedBoostGesturesEvent;->lambda$init$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/SchedBoostGesturesEvent;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent;)Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mGesturesEventListener:Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastFlingTime(Lcom/android/server/wm/SchedBoostGesturesEvent;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mLastFlingTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmScrollFired(Lcom/android/server/wm/SchedBoostGesturesEvent;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmLastFlingTime(Lcom/android/server/wm/SchedBoostGesturesEvent;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mLastFlingTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScrollFired(Lcom/android/server/wm/SchedBoostGesturesEvent;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 1
    .param p1, "looper"    # Landroid/os/Looper;

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;

    invoke-direct {v0, p0, p1}, Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mHandler:Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;

    .line 45
    return-void
.end method

.method private synthetic lambda$init$0()V
    .locals 6

    .line 51
    iget v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mDisplayId:I

    .line 52
    .local v0, "displayId":I
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    move-result-object v1

    .line 53
    .local v1, "info":Landroid/view/DisplayInfo;
    if-nez v1, :cond_0

    .line 55
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot create GestureDetector, display removed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SchedBoostGestures"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    return-void

    .line 58
    :cond_0
    new-instance v2, Lcom/android/server/wm/SchedBoostGesturesEvent$1;

    iget-object v3, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;

    invoke-direct {v4, p0}, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;)V

    iget-object v5, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mHandler:Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/android/server/wm/SchedBoostGesturesEvent$1;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mGestureDetector:Landroid/view/GestureDetector;

    .line 60
    return-void
.end method

.method private synthetic lambda$onPointerEvent$1(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 71
    invoke-virtual {p0, p1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->onSyncPointerEvent(Landroid/view/MotionEvent;)V

    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 48
    iput-object p1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mContext:Landroid/content/Context;

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getDisplayId()I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mDisplayId:I

    .line 50
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mHandler:Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;

    new-instance v1, Lcom/android/server/wm/SchedBoostGesturesEvent$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/SchedBoostGesturesEvent$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 61
    invoke-static {p1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 62
    return-void
.end method

.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 70
    invoke-virtual {p1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;

    move-result-object v0

    .line 71
    .local v0, "motionEvent":Landroid/view/MotionEvent;
    iget-object v1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mHandler:Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;

    new-instance v2, Lcom/android/server/wm/SchedBoostGesturesEvent$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0, v0}, Lcom/android/server/wm/SchedBoostGesturesEvent$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;Landroid/view/MotionEvent;)V

    invoke-virtual {v1, v2}, Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 72
    return-void
.end method

.method public onSyncPointerEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 75
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->isTouchEvent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 78
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 92
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mGesturesEventListener:Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    if-eqz v0, :cond_2

    .line 93
    invoke-interface {v0}, Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;->onMove()V

    goto :goto_0

    .line 87
    :pswitch_1
    iget-boolean v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mGesturesEventListener:Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    if-eqz v0, :cond_1

    .line 88
    invoke-interface {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;->onScroll(Z)V

    .line 89
    :cond_1
    iput-boolean v1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z

    .line 90
    goto :goto_0

    .line 80
    :pswitch_2
    iput-boolean v1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z

    .line 81
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mGesturesEventListener:Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    if-eqz v0, :cond_2

    .line 82
    invoke-interface {v0}, Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;->onDown()V

    .line 96
    :cond_2
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    .line 65
    iput-object p1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mGesturesEventListener:Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    .line 66
    return-void
.end method
