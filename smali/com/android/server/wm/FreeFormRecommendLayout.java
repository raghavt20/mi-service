public class com.android.server.wm.FreeFormRecommendLayout extends android.widget.FrameLayout {
	 /* .source "FreeFormRecommendLayout.java" */
	 /* # static fields */
	 private static final Float RECOMMEND_VIEW_TOP_MARGIN;
	 private static final Float RECOMMEND_VIEW_TOP_MARGIN_OFFSET;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private android.widget.RelativeLayout mFreeFormIconContainer;
	 private android.widget.ImageView mFreeFormImageView;
	 private android.view.WindowManager mWindowManager;
	 /* # direct methods */
	 public com.android.server.wm.FreeFormRecommendLayout ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 34 */
		 /* invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V */
		 /* .line 35 */
		 (( com.android.server.wm.FreeFormRecommendLayout ) p0 ).init ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/FreeFormRecommendLayout;->init(Landroid/content/Context;)V
		 /* .line 36 */
		 return;
	 } // .end method
	 public com.android.server.wm.FreeFormRecommendLayout ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .line 39 */
		 /* invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V */
		 /* .line 40 */
		 (( com.android.server.wm.FreeFormRecommendLayout ) p0 ).init ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/FreeFormRecommendLayout;->init(Landroid/content/Context;)V
		 /* .line 41 */
		 return;
	 } // .end method
	 public com.android.server.wm.FreeFormRecommendLayout ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .param p3, "defStyle" # I */
		 /* .line 44 */
		 /* invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V */
		 /* .line 45 */
		 (( com.android.server.wm.FreeFormRecommendLayout ) p0 ).init ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/FreeFormRecommendLayout;->init(Landroid/content/Context;)V
		 /* .line 46 */
		 return;
	 } // .end method
	 public com.android.server.wm.FreeFormRecommendLayout ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .param p3, "defStyleAttr" # I */
		 /* .param p4, "defStyleRes" # I */
		 /* .line 49 */
		 /* invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V */
		 /* .line 50 */
		 (( com.android.server.wm.FreeFormRecommendLayout ) p0 ).init ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/FreeFormRecommendLayout;->init(Landroid/content/Context;)V
		 /* .line 51 */
		 return;
	 } // .end method
	 private Float dipToPx ( Float p0 ) {
		 /* .locals 2 */
		 /* .param p1, "dip" # F */
		 /* .line 121 */
		 /* nop */
		 /* .line 122 */
		 (( com.android.server.wm.FreeFormRecommendLayout ) p0 ).getResources ( ); // invoke-virtual {p0}, Lcom/android/server/wm/FreeFormRecommendLayout;->getResources()Landroid/content/res/Resources;
		 (( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
		 /* .line 121 */
		 int v1 = 1; // const/4 v1, 0x1
		 v0 = 		 android.util.TypedValue .applyDimension ( v1,p1,v0 );
	 } // .end method
	 /* # virtual methods */
	 public android.view.WindowManager$LayoutParams createLayoutParams ( Integer p0 ) {
		 /* .locals 9 */
		 /* .param p1, "topMargin" # I */
		 /* .line 86 */
		 int v6 = -2; // const/4 v6, -0x2
		 /* .line 87 */
		 /* .local v6, "width":I */
		 int v7 = -2; // const/4 v7, -0x2
		 /* .line 88 */
		 /* .local v7, "height":I */
		 /* new-instance v8, Landroid/view/WindowManager$LayoutParams; */
		 /* const/16 v3, 0x7f6 */
		 /* const v4, 0x1000528 */
		 int v5 = 1; // const/4 v5, 0x1
		 /* move-object v0, v8 */
		 /* move v1, v6 */
		 /* move v2, v7 */
		 /* invoke-direct/range {v0 ..v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V */
		 /* .line 98 */
		 /* .local v0, "lp":Landroid/view/WindowManager$LayoutParams; */
		 /* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* or-int/lit8 v1, v1, 0x10 */
		 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* .line 99 */
		 /* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* or-int/lit8 v1, v1, 0x40 */
		 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* .line 100 */
		 /* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* const/high16 v2, 0x20000000 */
		 /* or-int/2addr v1, v2 */
		 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* .line 101 */
		 int v1 = 0; // const/4 v1, 0x0
		 (( android.view.WindowManager$LayoutParams ) v0 ).setFitInsetsTypes ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setFitInsetsTypes(I)V
		 /* .line 102 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
		 /* .line 103 */
		 int v2 = -1; // const/4 v2, -0x1
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I */
		 /* .line 104 */
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->rotationAnimation:I */
		 /* .line 105 */
		 /* const/16 v2, 0x31 */
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I */
		 /* .line 106 */
		 /* const/high16 v2, 0x41200000 # 10.0f */
		 v2 = 		 /* invoke-direct {p0, v2}, Lcom/android/server/wm/FreeFormRecommendLayout;->dipToPx(F)F */
		 /* float-to-int v2, v2 */
		 /* add-int/2addr v2, p1 */
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I */
		 /* .line 107 */
		 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I */
		 /* .line 108 */
		 final String v1 = "FreeForm-RecommendView"; // const-string v1, "FreeForm-RecommendView"
		 (( android.view.WindowManager$LayoutParams ) v0 ).setTitle ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
		 /* .line 109 */
	 } // .end method
	 public android.widget.RelativeLayout getFreeFormIconContainer ( ) {
		 /* .locals 1 */
		 /* .line 113 */
		 v0 = this.mFreeFormIconContainer;
	 } // .end method
	 public void init ( android.content.Context p0 ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 54 */
		 final String v0 = "FreeFormRecommendLayout"; // const-string v0, "FreeFormRecommendLayout"
		 final String v1 = "init FreeFormRecommendLayout "; // const-string v1, "init FreeFormRecommendLayout "
		 android.util.Log .d ( v0,v1 );
		 /* .line 55 */
		 this.mContext = p1;
		 /* .line 56 */
		 /* const-string/jumbo v0, "window" */
		 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/view/WindowManager; */
		 this.mWindowManager = v0;
		 /* .line 57 */
		 return;
	 } // .end method
	 protected void onAttachedToWindow ( ) {
		 /* .locals 0 */
		 /* .line 75 */
		 /* invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V */
		 /* .line 76 */
		 return;
	 } // .end method
	 protected void onConfigurationChanged ( android.content.res.Configuration p0 ) {
		 /* .locals 2 */
		 /* .param p1, "newConfig" # Landroid/content/res/Configuration; */
		 /* .line 61 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "onConfigurationChanged newConfig.orientation="; // const-string v1, "onConfigurationChanged newConfig.orientation="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p1, Landroid/content/res/Configuration;->orientation:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v1 = "FreeFormRecommendLayout"; // const-string v1, "FreeFormRecommendLayout"
		 android.util.Log .d ( v1,v0 );
		 /* .line 62 */
		 return;
	 } // .end method
	 protected void onDetachedFromWindow ( ) {
		 /* .locals 2 */
		 /* .line 81 */
		 /* invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V */
		 /* .line 82 */
		 final String v0 = "FreeFormRecommendLayout"; // const-string v0, "FreeFormRecommendLayout"
		 final String v1 = "onDetachedFromWindow"; // const-string v1, "onDetachedFromWindow"
		 android.util.Log .d ( v0,v1 );
		 /* .line 83 */
		 return;
	 } // .end method
	 protected void onFinishInflate ( ) {
		 /* .locals 2 */
		 /* .line 66 */
		 /* invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V */
		 /* .line 67 */
		 int v0 = 0; // const/4 v0, 0x0
		 (( com.android.server.wm.FreeFormRecommendLayout ) p0 ).getChildAt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/FreeFormRecommendLayout;->getChildAt(I)Landroid/view/View;
		 /* check-cast v1, Landroid/widget/RelativeLayout; */
		 this.mFreeFormIconContainer = v1;
		 /* .line 68 */
		 (( android.widget.RelativeLayout ) v1 ).getChildAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;
		 /* check-cast v0, Landroid/widget/ImageView; */
		 this.mFreeFormImageView = v0;
		 /* .line 70 */
		 final String v0 = "FreeFormRecommendLayout"; // const-string v0, "FreeFormRecommendLayout"
		 final String v1 = "onFinishInflate"; // const-string v1, "onFinishInflate"
		 android.util.Log .d ( v0,v1 );
		 /* .line 71 */
		 return;
	 } // .end method
	 public void setFreeFormIcon ( android.graphics.drawable.Drawable p0 ) {
		 /* .locals 1 */
		 /* .param p1, "freeFormDrawable" # Landroid/graphics/drawable/Drawable; */
		 /* .line 117 */
		 v0 = this.mFreeFormImageView;
		 (( android.widget.ImageView ) v0 ).setImageDrawable ( p1 ); // invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
		 /* .line 118 */
		 return;
	 } // .end method
