.class public Lcom/android/server/wm/MiuiFreeFormCameraStrategy;
.super Ljava/lang/Object;
.source "MiuiFreeFormCameraStrategy.java"


# static fields
.field public static final CAMERA_INITIATED:I = 0x1

.field public static final CAMERA_RELEASED:I = 0x0

.field private static final DEFAULT_CAMERA_ORIENTATION:I = -0x1

.field private static final FREEFORM_CAMERA_ORIENTATION_KEY:Ljava/lang/String; = "persist.vendor.freeform.cam.orientation"

.field private static final FREEFORM_CAMERA_PROP_KEY:Ljava/lang/String; = "persist.vendor.freeform.3appcam"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mCameraStateGlobal:I

.field private mCameraStateInFreeForm:I

.field private mPackageUseCamera:Ljava/lang/String;

.field private mService:Lcom/android/server/wm/MiuiFreeFormManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V
    .locals 2
    .param p1, "service"    # Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string v0, "MiuiFreeFormCameraStrategy"

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->TAG:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I

    .line 29
    iput v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I

    .line 30
    const-string v1, ""

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mPackageUseCamera:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 34
    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setFreeFormCameraProp(I)V

    .line 35
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V

    .line 36
    iput v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I

    .line 37
    iput v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I

    .line 38
    return-void
.end method


# virtual methods
.method public onCameraStateChanged(Ljava/lang/String;I)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "cameraState"    # I

    .line 41
    iput p2, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I

    .line 42
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    move-object v1, p1

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mPackageUseCamera:Ljava/lang/String;

    .line 44
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isInFreeForm(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Landroid/util/MiuiMultiWindowAdapter;->USE_DEFAULT_CAMERA_PIPELINE_APP:Ljava/util/List;

    .line 45
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_2

    .line 48
    :cond_1
    iget v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I

    iget v2, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I

    if-eq v1, v2, :cond_3

    .line 49
    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setFreeFormCameraProp(I)V

    .line 50
    iget v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I

    const-string v2, "MiuiFreeFormCameraStrategy"

    if-ne v1, v0, :cond_2

    .line 51
    const-string v0, "camera opened!"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation()V

    goto :goto_1

    .line 54
    :cond_2
    const-string v0, "camera released!"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V

    .line 58
    :cond_3
    :goto_1
    return-void

    .line 46
    :cond_4
    :goto_2
    return-void
.end method

.method public openCameraInFreeForm(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 76
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isInFreeForm(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mPackageUseCamera:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public rotateCameraIfNeeded()V
    .locals 2

    .line 61
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I

    if-nez v0, :cond_0

    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mPackageUseCamera:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isInFreeForm(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/util/MiuiMultiWindowAdapter;->USE_DEFAULT_CAMERA_PIPELINE_APP:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mPackageUseCamera:Ljava/lang/String;

    .line 64
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 66
    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setFreeFormCameraProp(I)V

    .line 67
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation()V

    goto :goto_0

    .line 70
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setFreeFormCameraProp(I)V

    .line 71
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V

    .line 73
    :cond_2
    :goto_0
    return-void
.end method

.method public setCameraOrientation()V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    .line 93
    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/DisplayContent;->mDisplay:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 92
    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V

    .line 94
    return-void
.end method

.method public setCameraOrientation(I)V
    .locals 6
    .param p1, "orientation"    # I

    .line 80
    const-string v0, "MiuiFreeFormCameraStrategy"

    iget v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I

    if-nez v1, :cond_0

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 81
    return-void

    .line 84
    :cond_0
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "persist.vendor.freeform.cam.orientation:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const-string v1, "persist.vendor.freeform.cam.orientation"

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "set persist.vendor.freeform.cam.orientation error! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public setFreeFormCameraProp(I)V
    .locals 6
    .param p1, "cameraProp"    # I

    .line 97
    const-string v0, "MiuiFreeFormCameraStrategy"

    iput p1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I

    .line 99
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "persist.vendor.freeform.3appcam:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const-string v1, "persist.vendor.freeform.3appcam"

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    goto :goto_0

    .line 101
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "set persist.vendor.freeform.3appcam error! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
