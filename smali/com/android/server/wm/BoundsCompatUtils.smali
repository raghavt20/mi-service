.class public Lcom/android/server/wm/BoundsCompatUtils;
.super Ljava/lang/Object;
.source "BoundsCompatUtils.java"


# static fields
.field private static final M_LOCK:Ljava/lang/Object;

.field private static volatile sSingleInstance:Lcom/android/server/wm/BoundsCompatUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/wm/BoundsCompatUtils;->sSingleInstance:Lcom/android/server/wm/BoundsCompatUtils;

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/wm/BoundsCompatUtils;->M_LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCompatGravity(Landroid/app/WindowConfiguration;I)I
    .locals 2
    .param p0, "windowConfiguration"    # Landroid/app/WindowConfiguration;
    .param p1, "mGravity"    # I

    .line 139
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 140
    invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I

    move-result v0

    if-nez v0, :cond_0

    .line 141
    const/4 p1, 0x5

    goto :goto_0

    .line 142
    :cond_0
    invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 143
    const/4 p1, 0x3

    goto :goto_0

    .line 144
    :cond_1
    invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 145
    const/16 p1, 0x30

    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 147
    const/16 p1, 0x50

    .line 150
    :cond_3
    :goto_0
    return p1
.end method

.method public static getInstance()Lcom/android/server/wm/BoundsCompatUtils;
    .locals 2

    .line 32
    sget-object v0, Lcom/android/server/wm/BoundsCompatUtils;->sSingleInstance:Lcom/android/server/wm/BoundsCompatUtils;

    if-nez v0, :cond_1

    .line 33
    sget-object v0, Lcom/android/server/wm/BoundsCompatUtils;->M_LOCK:Ljava/lang/Object;

    monitor-enter v0

    .line 34
    :try_start_0
    sget-object v1, Lcom/android/server/wm/BoundsCompatUtils;->sSingleInstance:Lcom/android/server/wm/BoundsCompatUtils;

    if-nez v1, :cond_0

    .line 35
    new-instance v1, Lcom/android/server/wm/BoundsCompatUtils;

    invoke-direct {v1}, Lcom/android/server/wm/BoundsCompatUtils;-><init>()V

    sput-object v1, Lcom/android/server/wm/BoundsCompatUtils;->sSingleInstance:Lcom/android/server/wm/BoundsCompatUtils;

    .line 37
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 39
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/wm/BoundsCompatUtils;->sSingleInstance:Lcom/android/server/wm/BoundsCompatUtils;

    return-object v0
.end method


# virtual methods
.method public adaptCompatBounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayContent;)V
    .locals 10
    .param p1, "config"    # Landroid/content/res/Configuration;
    .param p2, "dc"    # Lcom/android/server/wm/DisplayContent;

    .line 82
    iget-object v0, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 83
    .local v0, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    iget v1, p1, Landroid/content/res/Configuration;->densityDpi:I

    if-nez v1, :cond_0

    goto/16 :goto_5

    .line 87
    :cond_0
    iget v1, p1, Landroid/content/res/Configuration;->densityDpi:I

    int-to-float v1, v1

    const/high16 v2, 0x43200000    # 160.0f

    div-float/2addr v1, v2

    .line 88
    .local v1, "density":F
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 89
    .local v2, "width":I
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 91
    .local v3, "height":I
    int-to-float v4, v2

    div-float/2addr v4, v1

    float-to-double v4, v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v4, v4

    iput v4, p1, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    iput v4, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    .line 93
    const/4 v4, 0x1

    if-gt v2, v3, :cond_1

    .line 94
    move v5, v4

    goto :goto_0

    :cond_1
    const/4 v5, 0x2

    :goto_0
    iput v5, p1, Landroid/content/res/Configuration;->orientation:I

    .line 95
    iget-object v5, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    const/4 v8, 0x0

    if-gt v2, v3, :cond_2

    .line 96
    move v9, v8

    goto :goto_1

    :cond_2
    iget-object v9, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v9}, Landroid/app/WindowConfiguration;->getRotation()I

    move-result v9

    .line 95
    :goto_1
    invoke-virtual {v5, v9}, Landroid/app/WindowConfiguration;->setRotation(I)V

    .line 99
    iget v5, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v4, :cond_3

    if-eqz p2, :cond_3

    .line 101
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;

    move-result-object v4

    invoke-virtual {v4, v8, v2, v3}, Lcom/android/server/wm/DisplayPolicy;->getDecorInsetsInfo(III)Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info;

    move-result-object v4

    .line 103
    .local v4, "info":Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info;
    iget-object v5, v4, Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info;->mConfigFrame:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v1

    float-to-double v8, v5

    add-double/2addr v8, v6

    double-to-int v5, v8

    iput v5, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    .line 104
    .end local v4    # "info":Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info;
    goto :goto_2

    .line 105
    :cond_3
    int-to-float v4, v3

    div-float/2addr v4, v1

    float-to-int v4, v4

    iput v4, p1, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    iput v4, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    .line 108
    :goto_2
    if-gt v2, v3, :cond_4

    iget v4, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    goto :goto_3

    :cond_4
    iget v4, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    .line 109
    .local v4, "shortSizeDp":I
    :goto_3
    if-lt v2, v3, :cond_5

    iget v5, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    goto :goto_4

    :cond_5
    iget v5, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    .line 111
    .local v5, "longSizeDp":I
    :goto_4
    iput v4, p1, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    iput v4, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    .line 112
    iget v6, p1, Landroid/content/res/Configuration;->screenLayout:I

    invoke-static {v6}, Landroid/content/res/Configuration;->resetScreenLayout(I)I

    move-result v6

    .line 113
    .local v6, "sl":I
    invoke-static {v6, v5, v4}, Landroid/content/res/Configuration;->reduceScreenLayout(III)I

    move-result v7

    iput v7, p1, Landroid/content/res/Configuration;->screenLayout:I

    .line 114
    return-void

    .line 84
    .end local v1    # "density":F
    .end local v2    # "width":I
    .end local v3    # "height":I
    .end local v4    # "shortSizeDp":I
    .end local v5    # "longSizeDp":I
    .end local v6    # "sl":I
    :cond_6
    :goto_5
    return-void
.end method

.method public getCompatConfiguration(Landroid/content/res/Configuration;FLcom/android/server/wm/DisplayContent;F)Landroid/content/res/Configuration;
    .locals 10
    .param p1, "globalConfig"    # Landroid/content/res/Configuration;
    .param p2, "aspectRatio"    # F
    .param p3, "dp"    # Lcom/android/server/wm/DisplayContent;
    .param p4, "scale"    # F

    .line 52
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 53
    .local v0, "compatConfig":Landroid/content/res/Configuration;
    const/4 v1, 0x0

    cmpg-float v1, p2, v1

    if-gez v1, :cond_0

    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aspectRatio ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",skip"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BoundsCompatUtils"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    return-object v0

    .line 58
    :cond_0
    iget-object v1, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 59
    .local v1, "globalBounds":Landroid/graphics/Rect;
    iget-object v2, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getAppBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 61
    .local v2, "globalAppBounds":Landroid/graphics/Rect;
    new-instance v5, Landroid/graphics/Point;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-direct {v5, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 63
    .local v5, "displaySize":Landroid/graphics/Point;
    invoke-static {}, Landroid/app/servertransaction/BoundsCompat;->getInstance()Landroid/app/servertransaction/BoundsCompat;

    move-result-object v3

    iget-object v4, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    .line 64
    const/16 v6, 0x11

    invoke-static {v4, v6}, Lcom/android/server/wm/BoundsCompatUtils;->getCompatGravity(Landroid/app/WindowConfiguration;I)I

    move-result v6

    iget v7, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x0

    .line 63
    move v4, p2

    move v9, p4

    invoke-virtual/range {v3 .. v9}, Landroid/app/servertransaction/BoundsCompat;->computeCompatBounds(FLandroid/graphics/Point;IIIF)Landroid/graphics/Rect;

    move-result-object v3

    .line 67
    .local v3, "compatBounds":Landroid/graphics/Rect;
    iget-object v4, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v4, v3}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V

    .line 68
    iget-object v4, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v4, v3}, Landroid/app/WindowConfiguration;->setAppBounds(Landroid/graphics/Rect;)V

    .line 69
    iget-object v4, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v4, v3}, Landroid/app/WindowConfiguration;->setMaxBounds(Landroid/graphics/Rect;)V

    .line 70
    invoke-virtual {p0, v0, p3}, Lcom/android/server/wm/BoundsCompatUtils;->adaptCompatBounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayContent;)V

    .line 72
    return-object v0
.end method

.method public getFlipCompatModeByActivity(Lcom/android/server/wm/ActivityRecord;)I
    .locals 7
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 172
    const/4 v0, 0x0

    .line 173
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    const-string v1, "miui.supportFlipFullScreen"

    .line 175
    .local v1, "metaDataKey":Ljava/lang/String;
    :try_start_0
    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 176
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    const-wide/16 v5, 0x80

    invoke-interface {v2, v3, v5, v6, v4}, Landroid/content/pm/IPackageManager;->getActivityInfo(Landroid/content/ComponentName;JI)Landroid/content/pm/ActivityInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    .line 179
    nop

    .line 181
    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    .line 182
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 183
    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    return v2

    .line 185
    :cond_0
    const/4 v2, -0x1

    return v2

    .line 177
    :catch_0
    move-exception v2

    .line 178
    .local v2, "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I
    .locals 6
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    const-string v1, "miui.supportFlipFullScreen"

    .line 157
    .local v1, "metaDataKey":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    .line 158
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    const-wide/16 v4, 0x80

    invoke-interface {v2, p2, v4, v5, v3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    .line 161
    nop

    .line 163
    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 164
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    return v2

    .line 167
    :cond_0
    const/4 v2, -0x1

    return v2

    .line 159
    :catch_0
    move-exception v2

    .line 160
    .local v2, "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public getGlobalScaleByName(Ljava/lang/String;ILandroid/graphics/Rect;)F
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "flipCompatMode"    # I
    .param p3, "bounds"    # Landroid/graphics/Rect;

    .line 117
    const/high16 v0, -0x40800000    # -1.0f

    .line 118
    .local v0, "scale":F
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v1

    .line 119
    invoke-virtual {v1, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getScaleMode(Ljava/lang/String;)I

    move-result v1

    .line 120
    .local v1, "scaleMode":I
    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    invoke-virtual {p3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 121
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v2

    invoke-interface {v2}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 122
    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    .line 123
    sget v0, Lcom/android/server/wm/MiuiSizeCompatService;->FLIP_UNSCALE:F

    goto :goto_0

    .line 124
    :cond_0
    const/4 v2, 0x2

    if-eq p2, v2, :cond_1

    const/4 v2, -0x1

    if-ne p2, v2, :cond_3

    .line 126
    :cond_1
    sget v0, Lcom/android/server/wm/MiuiSizeCompatService;->FLIP_DEFAULT_SCALE:F

    goto :goto_0

    .line 129
    :cond_2
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 130
    .local v2, "longSide":I
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 131
    .local v3, "shortSide":I
    int-to-float v4, v3

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v4, v5

    int-to-float v5, v2

    div-float v0, v4, v5

    .line 135
    .end local v2    # "longSide":I
    .end local v3    # "shortSide":I
    :cond_3
    :goto_0
    return v0
.end method
