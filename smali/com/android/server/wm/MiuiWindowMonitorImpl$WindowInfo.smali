.class Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;
.super Ljava/lang/Object;
.source "MiuiWindowMonitorImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiWindowMonitorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WindowInfo"
.end annotation


# static fields
.field private static final KEY_NAME:Ljava/lang/String; = "name"

.field private static final KEY_TYPE:Ljava/lang/String; = "type"


# instance fields
.field mPackageName:Ljava/lang/String;

.field mPid:I

.field mToken:Landroid/os/IBinder;

.field mType:I

.field mWindowName:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "token"    # Landroid/os/IBinder;
    .param p4, "windowName"    # Ljava/lang/String;
    .param p5, "type"    # I

    .line 634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635
    iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPid:I

    .line 636
    iput-object p2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPackageName:Ljava/lang/String;

    .line 637
    iput-object p3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mToken:Landroid/os/IBinder;

    .line 638
    iput-object p4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mWindowName:Ljava/lang/String;

    .line 639
    iput p5, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I

    .line 640
    return-void
.end method


# virtual methods
.method public toJson()Lorg/json/JSONObject;
    .locals 3

    .line 648
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 650
    .local v0, "jobj":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "name"

    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mWindowName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 654
    nop

    .line 655
    return-object v0

    .line 651
    :catch_0
    move-exception v1

    .line 652
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 653
    const/4 v2, 0x0

    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mWindowName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
