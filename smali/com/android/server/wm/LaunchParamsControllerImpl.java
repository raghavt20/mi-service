public class com.android.server.wm.LaunchParamsControllerImpl implements com.android.server.wm.LaunchParamsControllerStub {
	 /* .source "LaunchParamsControllerImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.wm.LaunchParamsControllerImpl ( ) {
		 /* .locals 0 */
		 /* .line 6 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void registerDefaultModifiers ( com.android.server.wm.LaunchParamsController p0 ) {
		 /* .locals 1 */
		 /* .param p1, "controller" # Lcom/android/server/wm/LaunchParamsController; */
		 /* .line 10 */
		 v0 = 		 com.android.server.wm.MiuiDesktopModeUtils .isEnabled ( );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 12 */
			 /* new-instance v0, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier; */
			 /* invoke-direct {v0}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;-><init>()V */
			 (( com.android.server.wm.LaunchParamsController ) p1 ).registerModifier ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/LaunchParamsController;->registerModifier(Lcom/android/server/wm/LaunchParamsController$LaunchParamsModifier;)V
			 /* .line 14 */
		 } // :cond_0
		 return;
	 } // .end method
