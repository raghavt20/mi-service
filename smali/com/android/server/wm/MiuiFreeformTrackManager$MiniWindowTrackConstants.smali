.class public final Lcom/android/server/wm/MiuiFreeformTrackManager$MiniWindowTrackConstants;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiFreeformTrackManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MiniWindowTrackConstants"
.end annotation


# static fields
.field public static final ENTER_EVENT_NAME:Ljava/lang/String; = "enter"

.field public static final ENTER_TIP:Ljava/lang/String; = "621.2.0.1.14007"

.field public static final ENTER_WAY_NAME1:Ljava/lang/String; = "\u5168\u5c4f\u5e94\u7528_\u5de6\u4e0b\u89d2\u65e0\u6781\u6302\u8d77"

.field public static final ENTER_WAY_NAME2:Ljava/lang/String; = "\u5168\u5c4f\u5e94\u7528_\u53f3\u4e0b\u89d2\u65e0\u6781\u6302\u8d77"

.field public static final ENTER_WAY_NAME3:Ljava/lang/String; = "\u5c0f\u7a97_\u5de6\u4e0b\u89d2\u65e0\u6781\u7f29\u653e"

.field public static final ENTER_WAY_NAME4:Ljava/lang/String; = "\u5c0f\u7a97_\u53f3\u4e0b\u89d2\u65e0\u6781\u7f29\u653e"

.field public static final ENTER_WAY_NAME5:Ljava/lang/String; = "\u63a7\u5236\u4e2d\u5fc3"

.field public static final ENTER_WAY_RECOMMEND:Ljava/lang/String; = "\u63a8\u8350\u5f15\u5bfc"

.field public static final ENTER_WAY_UNPIN:Ljava/lang/String; = "\u8d34\u8fb9\u547c\u51fa"

.field public static final MOVE_EVENT_NAME:Ljava/lang/String; = "move"

.field public static final MOVE_TIP:Ljava/lang/String; = "621.2.0.1.14008"

.field public static final PINED_EVENT_NAME:Ljava/lang/String; = "hide_window"

.field public static final PINED_EXIT_EVENT_NAME:Ljava/lang/String; = "quit_hidden_window"

.field public static final PINED_EXIT_TIP:Ljava/lang/String; = "621.2.1.1.21752"

.field public static final PINED_MOVE_EVENT_NAME:Ljava/lang/String; = "move_hidden_window"

.field public static final PINED_MOVE_TIP:Ljava/lang/String; = "621.2.1.1.21750"

.field public static final PINED_TIP:Ljava/lang/String; = "621.2.1.1.21748"

.field public static final QUIT_EVENT_NAME:Ljava/lang/String; = "quit"

.field public static final QUIT_TIP:Ljava/lang/String; = "621.2.0.1.14009"

.field public static final QUIT_WAY_NAME1:Ljava/lang/String; = "\u5168\u5c4f"

.field public static final QUIT_WAY_NAME2:Ljava/lang/String; = "\u5c0f\u7a97"

.field public static final QUIT_WAY_NAME3:Ljava/lang/String; = "\u5176\u4ed6"

.field public static final QUIT_WAY_NAME4:Ljava/lang/String; = "\u62d6\u62fd\u8fdb\u5165\u5c0f\u7a97"

.field public static final QUIT_WAY_PIN:Ljava/lang/String; = "\u8fdb\u5165\u8ff7\u4f60\u7a97\u8d34\u8fb9"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
