public class com.android.server.wm.MiuiRotationAnimationUtils$EaseSineInOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "MiuiRotationAnimationUtils.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiRotationAnimationUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "EaseSineInOutInterpolator" */
} // .end annotation
/* # direct methods */
public com.android.server.wm.MiuiRotationAnimationUtils$EaseSineInOutInterpolator ( ) {
/* .locals 0 */
/* .line 55 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Float getInterpolation ( Float p0 ) {
/* .locals 4 */
/* .param p1, "input" # F */
/* .line 57 */
/* const-wide v0, 0x400921fb54442d18L # Math.PI */
/* float-to-double v2, p1 */
/* mul-double/2addr v2, v0 */
/* const-wide/high16 v0, 0x3ff0000000000000L # 1.0 */
/* div-double/2addr v2, v0 */
java.lang.Math .cos ( v2,v3 );
/* move-result-wide v0 */
/* double-to-float v0, v0 */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* sub-float/2addr v0, v1 */
/* const/high16 v1, -0x41000000 # -0.5f */
/* mul-float/2addr v0, v1 */
} // .end method
