class com.android.server.wm.ActivityStarterImpl extends com.android.server.wm.ActivityStarterStub {
	 /* .source "ActivityStarterImpl.java" */
	 /* # static fields */
	 private static final java.lang.String APPLICATION_LOCK_NAME;
	 private static final java.util.Set CARLINK_NOT_SUPPORT_IN_MUTIL_DISPLAY_ACTIVITYS;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.lang.String CARLINK_NOT_SUPPORT_IN_MUTIL_DISPLAY_ACTIVITYS_ACTION;
private static final java.util.Set CARLINK_VIRTUAL_DISPLAY_SET;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String EXTRA_ORIGINATING_UID;
public static final java.lang.String MIBI_SDK_SIGN_DEDUCT_ACTIVITY;
public static final java.lang.String PACKAGE_NAME_ALIPAY;
private static final java.lang.String SECURITY_CENTER;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.wm.ActivityTaskManagerService mAtmService;
private android.content.Context mContext;
private java.util.List mDefaultHomePkgNames;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private Integer mLastStartActivityUid;
private com.android.server.PowerConsumptionServiceInternal mPowerConsumptionServiceInternal;
private miui.app.ActivitySecurityHelper mSecurityHelper;
private miui.security.SecurityManagerInternal mSecurityManagerInternal;
private com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
private com.miui.server.SplashScreenServiceDelegate mSplashScreenServiceDelegate;
private Boolean mSystemReady;
/* # direct methods */
public static Boolean $r8$lambda$XFwMJl2kKDnqKk0H2XguenoCh8A ( com.android.server.wm.ActivityRecord p0, Integer p1, Boolean p2, android.content.Intent p3, android.content.ComponentName p4 ) { //synthethic
/* .locals 0 */
p0 = com.android.server.wm.ActivityStarterImpl .matchesPackageName ( p0,p1,p2,p3,p4 );
} // .end method
static com.android.server.wm.ActivityStarterImpl ( ) {
/* .locals 4 */
/* .line 100 */
/* new-instance v0, Ljava/util/HashSet; */
final String v1 = "com.baidu.BaiduMap"; // const-string v1, "com.baidu.BaiduMap"
final String v2 = "com.sinyee.babybus.story"; // const-string v2, "com.sinyee.babybus.story"
final String v3 = "com.autonavi.minimap"; // const-string v3, "com.autonavi.minimap"
/* filled-new-array {v3, v1, v2}, [Ljava/lang/String; */
/* .line 101 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 104 */
/* new-instance v0, Ljava/util/HashSet; */
final String v1 = "com.xiaomi.ucar.minimap"; // const-string v1, "com.xiaomi.ucar.minimap"
final String v2 = "com.miui.car.launcher"; // const-string v2, "com.miui.car.launcher"
final String v3 = "com.miui.carlink"; // const-string v3, "com.miui.carlink"
/* filled-new-array {v3, v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
return;
} // .end method
 com.android.server.wm.ActivityStarterImpl ( ) {
/* .locals 0 */
/* .line 89 */
/* invoke-direct {p0}, Lcom/android/server/wm/ActivityStarterStub;-><init>()V */
return;
} // .end method
private static void checkAndNotify ( Integer p0 ) {
/* .locals 5 */
/* .param p0, "uid" # I */
/* .line 150 */
try { // :try_start_0
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
/* .line 151 */
/* .local v0, "gz":Lcom/miui/server/greeze/GreezeManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.miui.server.greeze.GreezeManagerInternal ) v0 ).isUidFrozen ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/greeze/GreezeManagerInternal;->isUidFrozen(I)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 152 */
/* const-string/jumbo v1, "whetstone.activity" */
/* .line 153 */
android.os.ServiceManager .getService ( v1 );
/* .line 152 */
com.miui.whetstone.server.IWhetstoneActivityManager$Stub .asInterface ( v1 );
/* .line 154 */
/* .local v1, "ws":Lcom/miui/whetstone/server/IWhetstoneActivityManager; */
/* new-instance v2, Landroid/os/Bundle; */
/* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
/* .line 155 */
/* .local v2, "b":Landroid/os/Bundle; */
/* const-string/jumbo v3, "uid" */
(( android.os.Bundle ) v2 ).putInt ( v3, p0 ); // invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 156 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 157 */
/* const/16 v4, 0xb */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 161 */
} // .end local v0 # "gz":Lcom/miui/server/greeze/GreezeManagerInternal;
} // .end local v1 # "ws":Lcom/miui/whetstone/server/IWhetstoneActivityManager;
} // .end local v2 # "b":Landroid/os/Bundle;
} // :cond_0
/* .line 159 */
/* :catch_0 */
/* move-exception v0 */
/* .line 160 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "checkAndNotify error uid = "; // const-string v2, "checkAndNotify error uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ActivityStarterImpl"; // const-string v2, "ActivityStarterImpl"
android.util.Slog .w ( v2,v1 );
/* .line 162 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private android.content.Intent checkStorageRestricted ( android.content.pm.ActivityInfo p0, android.content.Intent p1, Integer p2, java.lang.String p3, java.lang.String p4, android.os.IBinder p5 ) {
/* .locals 6 */
/* .param p1, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "callingUid" # I */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .param p5, "reason" # Ljava/lang/String; */
/* .param p6, "resultTo" # Landroid/os/IBinder; */
/* .line 645 */
/* const-string/jumbo v0, "startActivityAsCaller" */
v0 = android.text.TextUtils .equals ( v0,p5 );
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 646 */
final String v0 = "As caller, check special action!"; // const-string v0, "As caller, check special action!"
final String v1 = "ActivityStarterImpl"; // const-string v1, "ActivityStarterImpl"
android.util.Slog .i ( v1,v0 );
/* .line 647 */
if ( p1 != null) { // if-eqz p1, :cond_4
if ( p6 != null) { // if-eqz p6, :cond_4
/* .line 648 */
com.android.server.wm.ActivityRecord .forTokenLocked ( p6 );
/* .line 649 */
/* .local v0, "record":Lcom/android/server/wm/ActivityRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v2 = "com.miui.securitycenter"; // const-string v2, "com.miui.securitycenter"
v3 = this.packageName;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 650 */
/* .local v2, "fromWakePath":Z */
} // :goto_0
/* if-nez v2, :cond_3 */
final String v3 = "android.intent.action.PICK"; // const-string v3, "android.intent.action.PICK"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = this.packageName;
/* .line 651 */
v3 = android.text.TextUtils .equals ( p4,v3 );
/* if-nez v3, :cond_1 */
v3 = this.mContext;
/* .line 652 */
v3 = miui.app.StorageRestrictedPathManager .isDenyAccessGallery ( v3,p4,p3,p2 );
/* if-nez v3, :cond_2 */
} // :cond_1
/* nop */
/* .line 654 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v4 = "android.intent.action.SEND"; // const-string v4, "android.intent.action.SEND"
v3 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
v3 = this.packageName;
/* .line 655 */
v3 = android.text.TextUtils .equals ( p4,v3 );
/* if-nez v3, :cond_3 */
v3 = this.mContext;
v4 = this.packageName;
v5 = this.applicationInfo;
/* iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 656 */
v3 = miui.app.StorageRestrictedPathManager .isDenyAccessGallery ( v3,v4,v5,p2 );
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 658 */
} // :cond_2
/* const-string/jumbo v3, "startAsCaller to pick pictures, not skip!" */
android.util.Slog .i ( v1,v3 );
/* .line 662 */
} // .end local v0 # "record":Lcom/android/server/wm/ActivityRecord;
} // .end local v2 # "fromWakePath":Z
/* .line 660 */
/* .restart local v0 # "record":Lcom/android/server/wm/ActivityRecord; */
/* .restart local v2 # "fromWakePath":Z */
} // :cond_3
/* .line 663 */
} // .end local v0 # "record":Lcom/android/server/wm/ActivityRecord;
} // .end local v2 # "fromWakePath":Z
} // :cond_4
/* .line 666 */
} // :cond_5
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private com.android.server.wm.ActivityRecord findActivityInSameApplication ( android.content.Intent p0, android.content.pm.ActivityInfo p1, Boolean p2, com.android.server.wm.RootWindowContainer p3 ) {
/* .locals 9 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "info" # Landroid/content/pm/ActivityInfo; */
/* .param p3, "compareIntentFilters" # Z */
/* .param p4, "rwc" # Lcom/android/server/wm/RootWindowContainer; */
/* .line 233 */
v0 = this.applicationInfo;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-nez p4, :cond_0 */
/* .line 237 */
} // :cond_0
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 238 */
/* .local v0, "cls":Landroid/content/ComponentName; */
v2 = this.targetActivity;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 239 */
/* new-instance v2, Landroid/content/ComponentName; */
v3 = this.packageName;
v4 = this.targetActivity;
/* invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v0, v2 */
/* .line 242 */
} // :cond_1
v2 = this.applicationInfo;
/* iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I */
v8 = android.os.UserHandle .getUserId ( v2 );
/* .line 244 */
/* .local v8, "userId":I */
/* new-instance v2, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v2}, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda2;-><init>()V */
/* const-class v3, Lcom/android/server/wm/ActivityRecord; */
/* .line 245 */
com.android.internal.util.function.pooled.PooledLambda .__ ( v3 );
/* .line 246 */
java.lang.Integer .valueOf ( v8 );
java.lang.Boolean .valueOf ( p3 );
/* .line 244 */
/* move-object v6, p1 */
/* move-object v7, v0 */
/* invoke-static/range {v2 ..v7}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainPredicate(Lcom/android/internal/util/function/QuintPredicate;Lcom/android/internal/util/function/pooled/ArgumentPlaceholder;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/android/internal/util/function/pooled/PooledPredicate; */
/* .line 247 */
/* .local v2, "p":Lcom/android/internal/util/function/pooled/PooledPredicate; */
/* if-nez v2, :cond_2 */
/* .line 248 */
/* .line 250 */
} // :cond_2
(( com.android.server.wm.RootWindowContainer ) p4 ).getActivity ( v2 ); // invoke-virtual {p4, v2}, Lcom/android/server/wm/RootWindowContainer;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;
/* .line 251 */
/* .local v1, "r":Lcom/android/server/wm/ActivityRecord; */
/* .line 252 */
/* .line 234 */
} // .end local v0 # "cls":Landroid/content/ComponentName;
} // .end local v1 # "r":Lcom/android/server/wm/ActivityRecord;
} // .end local v2 # "p":Lcom/android/internal/util/function/pooled/PooledPredicate;
} // .end local v8 # "userId":I
} // :cond_3
} // :goto_0
} // .end method
private com.android.server.wm.ActivityRecord findMapActivityInHistory ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.RootWindowContainer p1 ) {
/* .locals 4 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "rwc" # Lcom/android/server/wm/RootWindowContainer; */
/* .line 191 */
int v0 = 0; // const/4 v0, 0x0
/* .line 192 */
/* .local v0, "result":Lcom/android/server/wm/ActivityRecord; */
if ( p1 != null) { // if-eqz p1, :cond_0
v1 = this.intent;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.intent;
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.info;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = com.android.server.wm.ActivityStarterImpl.CARLINK_NOT_SUPPORT_IN_MUTIL_DISPLAY_ACTIVITYS;
v2 = this.intent;
/* .line 194 */
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v1 = /* .line 193 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 195 */
v1 = this.intent;
v2 = this.info;
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {p0, v1, v2, v3, p2}, Lcom/android/server/wm/ActivityStarterImpl;->findActivityInSameApplication(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;ZLcom/android/server/wm/RootWindowContainer;)Lcom/android/server/wm/ActivityRecord; */
/* .line 196 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "findMapActivityInHistory result="; // const-string v2, "findMapActivityInHistory result="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " r="; // const-string v2, " r="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.intent;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " info="; // const-string v2, " info="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.info;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ActivityStarterImpl"; // const-string v2, "ActivityStarterImpl"
android.util.Slog .d ( v2,v1 );
/* .line 198 */
} // :cond_0
} // .end method
static com.android.server.wm.ActivityStarterImpl getInstance ( ) {
/* .locals 1 */
/* .line 138 */
com.android.server.wm.ActivityStarterStub .get ( );
/* check-cast v0, Lcom/android/server/wm/ActivityStarterImpl; */
} // .end method
private java.lang.String getRawPackageName ( android.content.pm.ActivityInfo p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 589 */
v0 = this.packageName;
/* .line 590 */
/* .local v0, "rawPackageName":Ljava/lang/String; */
(( android.content.Intent ) p2 ).getComponent ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v1 != null) { // if-eqz v1, :cond_0
(( android.content.Intent ) p2 ).getComponent ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v1 ).flattenToShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
final String v2 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v2, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
v1 = android.text.TextUtils .equals ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 591 */
final String v1 = "android.intent.extra.INTENT"; // const-string v1, "android.intent.extra.INTENT"
(( android.content.Intent ) p2 ).getParcelableExtra ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v1, Landroid/content/Intent; */
/* .line 592 */
/* .local v1, "rawIntent":Landroid/content/Intent; */
if ( v1 != null) { // if-eqz v1, :cond_0
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 593 */
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 596 */
} // .end local v1 # "rawIntent":Landroid/content/Intent;
} // :cond_0
} // .end method
static Boolean lambda$finishLaunchedFromActivityIfNeeded$1 ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "starting" # Lcom/android/server/wm/ActivityRecord; */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 768 */
/* iget-boolean v0, p1, Lcom/android/server/wm/ActivityRecord;->finishing:Z */
/* if-nez v0, :cond_0 */
/* .line 769 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I
/* iget v1, p0, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I */
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 768 */
} // :goto_0
} // .end method
static void lambda$sendBroadCastToUcar$0 ( android.content.Context p0, android.content.Intent p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 216 */
final String v0 = "miui.car.permission.MI_CARLINK_STATUS"; // const-string v0, "miui.car.permission.MI_CARLINK_STATUS"
(( android.content.Context ) p0 ).sendBroadcast ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
/* .line 217 */
return;
} // .end method
private static Boolean matchesPackageName ( com.android.server.wm.ActivityRecord p0, Integer p1, Boolean p2, android.content.Intent p3, android.content.ComponentName p4 ) {
/* .locals 4 */
/* .param p0, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p1, "userId" # I */
/* .param p2, "compareIntentFilters" # Z */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "cls" # Landroid/content/ComponentName; */
/* .line 258 */
int v0 = 0; // const/4 v0, 0x0
if ( p0 != null) { // if-eqz p0, :cond_5
v1 = (( com.android.server.wm.ActivityRecord ) p0 ).canBeTopRunning ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->canBeTopRunning()Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* iget v1, p0, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
/* if-eq v1, p1, :cond_0 */
/* .line 260 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 261 */
v2 = this.intent;
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = this.intent;
v2 = (( android.content.Intent ) v2 ).filterEquals ( p3 ); // invoke-virtual {v2, p3}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 262 */
/* .line 265 */
} // :cond_1
v2 = this.mActivityComponent;
if ( v2 != null) { // if-eqz v2, :cond_4
/* if-nez p4, :cond_2 */
/* .line 270 */
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "matchesApplication r="; // const-string v3, "matchesApplication r="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mActivityComponent;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " cls="; // const-string v3, " cls="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ActivityStarterImpl"; // const-string v3, "ActivityStarterImpl"
android.util.Slog .d ( v3,v2 );
/* .line 271 */
v2 = this.mActivityComponent;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( android.content.ComponentName ) p4 ).getPackageName ( ); // invoke-virtual {p4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v2 = android.text.TextUtils .equals ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 272 */
/* .line 275 */
} // :cond_3
/* .line 266 */
} // :cond_4
} // :goto_0
/* .line 258 */
} // :cond_5
} // :goto_1
} // .end method
private void recordAppBehavior ( Integer p0, java.lang.String p1, android.content.Intent p2 ) {
/* .locals 7 */
/* .param p1, "behavior" # I */
/* .param p2, "caller" # Ljava/lang/String; */
/* .param p3, "data" # Landroid/content/Intent; */
/* .line 868 */
v0 = this.mSecurityManagerInternal;
/* if-nez v0, :cond_0 */
/* .line 869 */
/* const-class v0, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/security/SecurityManagerInternal; */
this.mSecurityManagerInternal = v0;
/* .line 871 */
} // :cond_0
v1 = this.mSecurityManagerInternal;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 872 */
/* const-wide/16 v4, 0x1 */
miui.security.AppBehavior .parseIntent ( p3 );
/* move v2, p1 */
/* move-object v3, p2 */
/* invoke-virtual/range {v1 ..v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 874 */
} // :cond_1
return;
} // .end method
private android.content.pm.ActivityInfo resolveCheckIntent ( com.android.server.wm.ActivityStarter$Request p0 ) {
/* .locals 11 */
/* .param p1, "request" # Lcom/android/server/wm/ActivityStarter$Request; */
/* .line 671 */
v8 = this.intent;
/* .line 672 */
/* .local v8, "intent":Landroid/content/Intent; */
/* iget v0, p1, Lcom/android/server/wm/ActivityStarter$Request;->userId:I */
/* .line 673 */
/* .local v0, "userId":I */
if ( v8 != null) { // if-eqz v8, :cond_4
(( android.content.Intent ) v8 ).getComponent ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* if-nez v1, :cond_4 */
/* .line 674 */
int v1 = 0; // const/4 v1, 0x0
/* .line 675 */
/* .local v1, "transform":Z */
final String v2 = "miui.intent.action.CHECK_ACCESS_CONTROL"; // const-string v2, "miui.intent.action.CHECK_ACCESS_CONTROL"
(( android.content.Intent ) v8 ).getAction ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 676 */
(( android.content.Intent ) v8 ).getAction ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v3 = "android.app.action.CHECK_ACCESS_CONTROL_PAD"; // const-string v3, "android.app.action.CHECK_ACCESS_CONTROL_PAD"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 677 */
(( android.content.Intent ) v8 ).getAction ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v3 = "android.app.action.CHECK_ALLOW_START_ACTIVITY"; // const-string v3, "android.app.action.CHECK_ALLOW_START_ACTIVITY"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 678 */
(( android.content.Intent ) v8 ).getAction ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v3 = "android.app.action.CHECK_ALLOW_START_ACTIVITY_PAD"; // const-string v3, "android.app.action.CHECK_ALLOW_START_ACTIVITY_PAD"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 679 */
(( android.content.Intent ) v8 ).getAction ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v3 = "com.miui.gamebooster.action.ACCESS_WINDOWCALLACTIVITY"; // const-string v3, "com.miui.gamebooster.action.ACCESS_WINDOWCALLACTIVITY"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
v2 = this.mSecurityManagerInternal;
/* .line 680 */
v2 = (( miui.security.SecurityManagerInternal ) v2 ).isBlockActivity ( v8 ); // invoke-virtual {v2, v8}, Lmiui/security/SecurityManagerInternal;->isBlockActivity(Landroid/content/Intent;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
} // :cond_0
/* move v9, v0 */
/* move v10, v1 */
/* .line 681 */
} // :cond_1
} // :goto_0
/* const/16 v2, 0x3e7 */
/* if-ne v0, v2, :cond_2 */
/* .line 682 */
int v0 = 0; // const/4 v0, 0x0
/* .line 684 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
/* move v9, v0 */
/* move v10, v1 */
/* .line 686 */
} // .end local v0 # "userId":I
} // .end local v1 # "transform":Z
/* .local v9, "userId":I */
/* .local v10, "transform":Z */
} // :goto_1
if ( v10 != null) { // if-eqz v10, :cond_3
/* .line 687 */
v0 = this.mAtmService;
v0 = this.mTaskSupervisor;
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
v4 = this.profilerInfo;
/* iget v6, p1, Lcom/android/server/wm/ActivityStarter$Request;->filterCallingUid:I */
/* iget v7, p1, Lcom/android/server/wm/ActivityStarter$Request;->callingPid:I */
/* move-object v1, v8 */
/* move v5, v9 */
/* invoke-virtual/range {v0 ..v7}, Lcom/android/server/wm/ActivityTaskSupervisor;->resolveActivity(Landroid/content/Intent;Ljava/lang/String;ILandroid/app/ProfilerInfo;III)Landroid/content/pm/ActivityInfo; */
this.activityInfo = v0;
/* .line 692 */
} // .end local v10 # "transform":Z
} // :cond_3
/* move v0, v9 */
} // .end local v9 # "userId":I
/* .restart local v0 # "userId":I */
} // :cond_4
v1 = this.activityInfo;
} // .end method
private void sendBroadCastToUcar ( android.content.Intent p0, android.content.Context p1 ) {
/* .locals 4 */
/* .param p1, "srcIntent" # Landroid/content/Intent; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 202 */
v0 = this.mHandlerThread;
/* if-nez v0, :cond_0 */
/* .line 203 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "carlink-workthread"; // const-string v1, "carlink-workthread"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 204 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 205 */
/* new-instance v0, Landroid/os/Handler; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 208 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 209 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 210 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "not_support_in_mutil_display_activitys_action"; // const-string v1, "not_support_in_mutil_display_activitys_action"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 211 */
final String v1 = "com.miui.carlink"; // const-string v1, "com.miui.carlink"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 212 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 213 */
/* .local v1, "bundle":Landroid/os/Bundle; */
/* const-string/jumbo v2, "src_intent" */
(( android.os.Bundle ) v1 ).putParcelable ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
/* .line 214 */
(( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 215 */
v2 = this.mHandler;
/* new-instance v3, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v3, p2, v0}, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda1;-><init>(Landroid/content/Context;Landroid/content/Intent;)V */
(( android.os.Handler ) v2 ).post ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 219 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v1 # "bundle":Landroid/os/Bundle;
} // :cond_1
return;
} // .end method
private Boolean startActivityByFreeForm ( android.content.pm.ActivityInfo p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 600 */
v0 = this.mSecurityManagerInternal;
/* if-nez v0, :cond_0 */
/* .line 601 */
/* const-class v0, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/security/SecurityManagerInternal; */
this.mSecurityManagerInternal = v0;
/* .line 603 */
} // :cond_0
v0 = this.mSecurityManagerInternal;
v1 = this.packageName;
v2 = this.applicationInfo;
/* iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I */
v2 = android.os.UserHandle .getUserId ( v2 );
v0 = (( miui.security.SecurityManagerInternal ) v0 ).checkGameBoosterPayPassAsUser ( v1, p2, v2 ); // invoke-virtual {v0, v1, p2, v2}, Lmiui/security/SecurityManagerInternal;->checkGameBoosterPayPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z
} // .end method
/* # virtual methods */
void activityIdle ( android.content.pm.ActivityInfo p0 ) {
/* .locals 2 */
/* .param p1, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .line 440 */
/* iget-boolean v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSystemReady:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 442 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 443 */
final String v0 = "ActivityStarterImpl"; // const-string v0, "ActivityStarterImpl"
final String v1 = "aInfo is null!"; // const-string v1, "aInfo is null!"
android.util.Slog .w ( v0,v1 );
/* .line 444 */
return;
/* .line 446 */
} // :cond_1
v0 = this.mSplashScreenServiceDelegate;
(( com.miui.server.SplashScreenServiceDelegate ) v0 ).activityIdle ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/SplashScreenServiceDelegate;->activityIdle(Landroid/content/pm/ActivityInfo;)V
/* .line 447 */
return;
} // .end method
public Boolean carlinkJudge ( android.content.Context p0, com.android.server.wm.Task p1, com.android.server.wm.ActivityRecord p2, com.android.server.wm.RootWindowContainer p3 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "targetRootTask" # Lcom/android/server/wm/Task; */
/* .param p3, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "rwc" # Lcom/android/server/wm/RootWindowContainer; */
/* .line 167 */
/* invoke-direct {p0, p3, p4}, Lcom/android/server/wm/ActivityStarterImpl;->findMapActivityInHistory(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/RootWindowContainer;)Lcom/android/server/wm/ActivityRecord; */
/* .line 169 */
/* .local v0, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 170 */
(( com.android.server.wm.ActivityRecord ) v0 ).getRootTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
if ( v2 != null) { // if-eqz v2, :cond_2
/* if-nez p2, :cond_0 */
/* .line 175 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) v0 ).getRootTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
v2 = this.mDisplayContent;
/* .line 176 */
/* .local v2, "preDisplayContent":Lcom/android/server/wm/DisplayContent; */
v3 = (( com.android.server.wm.Task ) p2 ).getDisplayId ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getDisplayId()I
/* .line 178 */
/* .local v3, "targetDisplayid":I */
v4 = (( com.android.server.wm.ActivityStarterImpl ) p0 ).isCarWithDisplay ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/ActivityStarterImpl;->isCarWithDisplay(Lcom/android/server/wm/DisplayContent;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* if-nez v3, :cond_1 */
/* .line 179 */
final String v1 = "ActivityStarterImpl"; // const-string v1, "ActivityStarterImpl"
final String v4 = "activity is already started in carlink display"; // const-string v4, "activity is already started in carlink display"
android.util.Slog .d ( v1,v4 );
/* .line 180 */
v1 = this.intent;
/* .line 181 */
/* .local v1, "srcIntent":Landroid/content/Intent; */
/* invoke-direct {p0, v1, p1}, Lcom/android/server/wm/ActivityStarterImpl;->sendBroadCastToUcar(Landroid/content/Intent;Landroid/content/Context;)V */
/* .line 182 */
int v4 = 1; // const/4 v4, 0x1
/* .line 185 */
} // .end local v1 # "srcIntent":Landroid/content/Intent;
} // :cond_1
/* .line 172 */
} // .end local v2 # "preDisplayContent":Lcom/android/server/wm/DisplayContent;
} // .end local v3 # "targetDisplayid":I
} // :cond_2
} // :goto_0
} // .end method
public Boolean checkDefaultBrowser ( android.content.Context p0, com.android.server.wm.Task p1, android.content.Intent p2 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .line 935 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_5
if ( p2 != null) { // if-eqz p2, :cond_5
/* if-nez p3, :cond_0 */
/* goto/16 :goto_1 */
/* .line 936 */
} // :cond_0
final String v1 = "android.intent.extra.INTENT"; // const-string v1, "android.intent.extra.INTENT"
(( android.content.Intent ) p3 ).getParcelableExtra ( v1 ); // invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* .line 937 */
/* .local v1, "targetParcelable":Landroid/os/Parcelable; */
/* instance-of v2, v1, Landroid/content/Intent; */
/* if-nez v2, :cond_1 */
/* .line 938 */
/* .line 940 */
} // :cond_1
/* move-object v2, v1 */
/* check-cast v2, Landroid/content/Intent; */
/* .line 942 */
/* .local v2, "rawIntent":Landroid/content/Intent; */
if ( v2 != null) { // if-eqz v2, :cond_2
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 943 */
(( android.content.Intent ) p3 ).getComponent ( ); // invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v3 ).flattenToShortString ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
final String v4 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v4, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
v3 = android.text.TextUtils .equals ( v4,v3 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 944 */
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .local v3, "rawPackageName":Ljava/lang/String; */
/* .line 946 */
} // .end local v3 # "rawPackageName":Ljava/lang/String;
} // :cond_2
(( com.android.server.wm.Task ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;
/* .line 947 */
/* .restart local v3 # "rawPackageName":Ljava/lang/String; */
/* move-object v2, p3 */
/* .line 949 */
} // :goto_0
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v5 = android.os.UserHandle .myUserId ( );
(( android.content.pm.PackageManager ) v4 ).getDefaultBrowserPackageNameAsUser ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getDefaultBrowserPackageNameAsUser(I)Ljava/lang/String;
/* .line 950 */
/* .local v4, "defaultBrowserPackageName":Ljava/lang/String; */
v5 = this.mAtmService;
v5 = v5 = this.mMiuiFreeFormManagerService;
/* .line 951 */
/* .local v5, "checkVGScene":Z */
v6 = android.text.TextUtils .equals ( v3,v4 );
/* .line 952 */
/* .local v6, "isDefaultBrowser":Z */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 953 */
(( android.content.Intent ) v2 ).getData ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* .line 954 */
/* .local v7, "uri":Landroid/net/Uri; */
if ( v7 != null) { // if-eqz v7, :cond_4
(( android.net.Uri ) v7 ).getHost ( ); // invoke-virtual {v7}, Landroid/net/Uri;->getHost()Ljava/lang/String;
if ( v8 != null) { // if-eqz v8, :cond_4
(( android.net.Uri ) v7 ).getScheme ( ); // invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 955 */
(( android.content.Intent ) v2 ).getAction ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 956 */
(( android.net.Uri ) v7 ).getScheme ( ); // invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
final String v9 = "http"; // const-string v9, "http"
v8 = (( java.lang.String ) v8 ).equalsIgnoreCase ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v8, :cond_3 */
/* .line 957 */
(( android.net.Uri ) v7 ).getScheme ( ); // invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
final String v9 = "https"; // const-string v9, "https"
v8 = (( java.lang.String ) v8 ).equalsIgnoreCase ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 958 */
} // :cond_3
/* and-int v0, v5, v6 */
/* .line 961 */
} // .end local v7 # "uri":Landroid/net/Uri;
} // :cond_4
/* .line 935 */
} // .end local v1 # "targetParcelable":Landroid/os/Parcelable;
} // .end local v2 # "rawIntent":Landroid/content/Intent;
} // .end local v3 # "rawPackageName":Ljava/lang/String;
} // .end local v4 # "defaultBrowserPackageName":Ljava/lang/String;
} // .end local v5 # "checkVGScene":Z
} // .end local v6 # "isDefaultBrowser":Z
} // :cond_5
} // :goto_1
} // .end method
public Boolean checkIntentActivityForAppLock ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 6 */
/* .param p1, "intentActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "startActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 913 */
v0 = (( com.android.server.wm.ActivityStarterImpl ) p0 ).isAppLockActivity ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/ActivityStarterImpl;->isAppLockActivity(Lcom/android/server/wm/ActivityRecord;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 915 */
if ( p1 != null) { // if-eqz p1, :cond_7
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
v2 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 917 */
final String v0 = "android.intent.extra.shortcut.NAME"; // const-string v0, "android.intent.extra.shortcut.NAME"
int v2 = 0; // const/4 v2, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
v3 = this.intent;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 918 */
v3 = this.intent;
(( android.content.Intent ) v3 ).getStringExtra ( v0 ); // invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
} // :cond_0
/* move-object v3, v2 */
/* .line 919 */
/* .local v3, "pkgBehindIntentActivity":Ljava/lang/String; */
} // :goto_0
if ( p2 != null) { // if-eqz p2, :cond_1
v4 = this.intent;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 920 */
v2 = this.intent;
(( android.content.Intent ) v2 ).getStringExtra ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
} // :cond_1
/* nop */
} // :goto_1
/* move-object v0, v2 */
/* .line 922 */
/* .local v0, "pkgBehindStartActivity":Ljava/lang/String; */
final String v2 = "originating_uid"; // const-string v2, "originating_uid"
if ( p1 != null) { // if-eqz p1, :cond_2
v4 = this.intent;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 923 */
v4 = this.intent;
v4 = (( android.content.Intent ) v4 ).getIntExtra ( v2, v1 ); // invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
} // :cond_2
/* move v4, v1 */
/* .line 924 */
/* .local v4, "intentActivityUid":I */
} // :goto_2
if ( p2 != null) { // if-eqz p2, :cond_3
v5 = this.intent;
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 925 */
v5 = this.intent;
v2 = (( android.content.Intent ) v5 ).getIntExtra ( v2, v1 ); // invoke-virtual {v5, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
} // :cond_3
/* move v2, v1 */
/* .line 926 */
/* .local v2, "startActivityUid":I */
} // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 927 */
v5 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_5
} // :cond_4
/* if-eq v4, v2, :cond_6 */
} // :cond_5
int v1 = 1; // const/4 v1, 0x1
/* .line 926 */
} // :cond_6
/* .line 931 */
} // .end local v0 # "pkgBehindStartActivity":Ljava/lang/String;
} // .end local v2 # "startActivityUid":I
} // .end local v3 # "pkgBehindIntentActivity":Ljava/lang/String;
} // .end local v4 # "intentActivityUid":I
} // :cond_7
} // .end method
public Boolean checkRunningCompatibility ( android.app.IApplicationThread p0, android.content.pm.ActivityInfo p1, android.content.Intent p2, Integer p3, java.lang.String p4 ) {
/* .locals 6 */
/* .param p1, "caller" # Landroid/app/IApplicationThread; */
/* .param p2, "info" # Landroid/content/pm/ActivityInfo; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "userId" # I */
/* .param p5, "callingPackage" # Ljava/lang/String; */
/* .line 144 */
com.android.server.am.ActivityManagerServiceImpl .getInstance ( );
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* move v4, p4 */
/* move-object v5, p5 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkRunningCompatibility(Landroid/app/IApplicationThread;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ILjava/lang/String;)Z */
} // .end method
com.android.server.wm.SafeActivityOptions checkStartActivityByFreeForm ( android.app.IApplicationThread p0, android.content.pm.ActivityInfo p1, android.content.Intent p2, java.lang.String p3, Boolean p4, Integer p5, java.lang.String p6, com.android.server.wm.SafeActivityOptions p7 ) {
/* .locals 12 */
/* .param p1, "caller" # Landroid/app/IApplicationThread; */
/* .param p2, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "resolvedType" # Ljava/lang/String; */
/* .param p5, "ignoreTargetSecurity" # Z */
/* .param p6, "callingUid" # I */
/* .param p7, "callingPackage" # Ljava/lang/String; */
/* .param p8, "bOptions" # Lcom/android/server/wm/SafeActivityOptions; */
/* .line 541 */
/* move-object v1, p0 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* move-object/from16 v4, p8 */
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 542 */
/* invoke-direct {p0, p2, p3}, Lcom/android/server/wm/ActivityStarterImpl;->getRawPackageName(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;)Ljava/lang/String; */
/* .line 543 */
/* .local v5, "rawPackageName":Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .line 544 */
/* .local v6, "currentFullPackageName":Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
/* .line 545 */
/* .local v7, "currentFullActivity":Lcom/android/server/wm/ActivityRecord; */
v0 = this.mAtmService;
v8 = this.mGlobalLock;
/* monitor-enter v8 */
/* .line 546 */
try { // :try_start_0
v0 = this.mAtmService;
v0 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v0 ).getDefaultDisplay ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;
/* .line 547 */
/* .local v0, "display":Lcom/android/server/wm/DisplayContent; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( com.android.server.wm.DisplayContent ) v0 ).topRunningActivityExcludeFreeform ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 548 */
(( com.android.server.wm.DisplayContent ) v0 ).topRunningActivityExcludeFreeform ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;
(( com.android.server.wm.ActivityRecord ) v9 ).getTask ( ); // invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 549 */
(( com.android.server.wm.DisplayContent ) v0 ).topRunningActivityExcludeFreeform ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;
(( com.android.server.wm.ActivityRecord ) v9 ).getTask ( ); // invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.Task ) v9 ).getPackageName ( ); // invoke-virtual {v9}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;
/* move-object v6, v9 */
/* .line 550 */
(( com.android.server.wm.DisplayContent ) v0 ).topRunningActivityExcludeFreeform ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;
/* move-object v7, v9 */
/* .line 552 */
} // .end local v0 # "display":Lcom/android/server/wm/DisplayContent;
} // :cond_0
/* monitor-exit v8 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 553 */
v0 = /* invoke-direct {p0, p2, p3}, Lcom/android/server/wm/ActivityStarterImpl;->startActivityByFreeForm(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_a
if ( v5 != null) { // if-eqz v5, :cond_a
v0 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_a */
/* .line 554 */
if ( v7 != null) { // if-eqz v7, :cond_1
v0 = this.mActivityComponent;
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "com.mibi.sdk.deduct.ui.SignDeductActivity"; // const-string v0, "com.mibi.sdk.deduct.ui.SignDeductActivity"
v8 = this.mActivityComponent;
/* .line 555 */
(( android.content.ComponentName ) v8 ).getClassName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "com.eg.android.AlipayGphone"; // const-string v0, "com.eg.android.AlipayGphone"
/* .line 556 */
v0 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 557 */
/* .line 559 */
} // :cond_1
v0 = this.mContext;
int v8 = 1; // const/4 v8, 0x1
android.util.MiuiMultiWindowUtils .getActivityOptions ( v0,v5,v8 );
/* .line 560 */
/* .local v8, "options":Landroid/app/ActivityOptions; */
/* if-nez v8, :cond_2 */
/* .line 561 */
/* .line 563 */
} // :cond_2
v0 = this.mSecurityManagerInternal;
(( android.content.Intent ) p3 ).getComponent ( ); // invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v9 ).flattenToShortString ( ); // invoke-virtual {v9}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v0 = (( miui.security.SecurityManagerInternal ) v0 ).isForceLaunchNewTask ( v5, v9 ); // invoke-virtual {v0, v5, v9}, Lmiui/security/SecurityManagerInternal;->isForceLaunchNewTask(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v0, :cond_3 */
v0 = this.mSecurityManagerInternal;
/* .line 564 */
(( android.content.Intent ) p3 ).getComponent ( ); // invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v9 ).flattenToShortString ( ); // invoke-virtual {v9}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v0 = (( miui.security.SecurityManagerInternal ) v0 ).isApplicationLockActivity ( v9 ); // invoke-virtual {v0, v9}, Lmiui/security/SecurityManagerInternal;->isApplicationLockActivity(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 565 */
} // :cond_3
(( android.app.ActivityOptions ) v8 ).setForceLaunchNewTask ( ); // invoke-virtual {v8}, Landroid/app/ActivityOptions;->setForceLaunchNewTask()V
/* .line 567 */
} // :cond_4
if ( v7 != null) { // if-eqz v7, :cond_7
/* .line 568 */
final String v0 = "com.miui.securitycore/com.miui.xspace.ui.activity.XSpaceResolveActivity"; // const-string v0, "com.miui.securitycore/com.miui.xspace.ui.activity.XSpaceResolveActivity"
v9 = this.shortComponentName;
v0 = (( java.lang.String ) v0 ).equals ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 569 */
int v9 = 0; // const/4 v9, 0x0
/* .line 570 */
/* .local v9, "behindActivity":Lcom/android/server/wm/ActivityRecord; */
v0 = this.mAtmService;
v10 = this.mGlobalLock;
/* monitor-enter v10 */
/* .line 571 */
try { // :try_start_1
v0 = this.mAtmService;
v0 = this.mTaskSupervisor;
v0 = this.mRootWindowContainer;
/* .line 572 */
int v11 = 0; // const/4 v11, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).getDisplayContent ( v11 ); // invoke-virtual {v0, v11}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v0 ).getActivityBelow ( v7 ); // invoke-virtual {v0, v7}, Lcom/android/server/wm/DisplayContent;->getActivityBelow(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;
/* move-object v9, v0 */
/* .line 573 */
/* monitor-exit v10 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 574 */
if ( v9 != null) { // if-eqz v9, :cond_5
/* .line 575 */
v0 = (( com.android.server.wm.ActivityRecord ) v9 ).getRootTaskId ( ); // invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
(( android.app.ActivityOptions ) v8 ).setLaunchFromTaskId ( v0 ); // invoke-virtual {v8, v0}, Landroid/app/ActivityOptions;->setLaunchFromTaskId(I)V
/* .line 577 */
} // .end local v9 # "behindActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_5
/* .line 573 */
/* .restart local v9 # "behindActivity":Lcom/android/server/wm/ActivityRecord; */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_2
/* monitor-exit v10 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 578 */
} // .end local v9 # "behindActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_6
v0 = (( com.android.server.wm.ActivityRecord ) v7 ).getRootTaskId ( ); // invoke-virtual {v7}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
(( android.app.ActivityOptions ) v8 ).setLaunchFromTaskId ( v0 ); // invoke-virtual {v8, v0}, Landroid/app/ActivityOptions;->setLaunchFromTaskId(I)V
/* .line 581 */
} // :cond_7
} // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_8
/* invoke-virtual/range {p8 ..p8}, Lcom/android/server/wm/SafeActivityOptions;->getOriginalOptions()Landroid/app/ActivityOptions; */
(( com.android.server.wm.SafeActivityOptions ) v4 ).mergeActivityOptions ( v0, v8 ); // invoke-virtual {v4, v0, v8}, Lcom/android/server/wm/SafeActivityOptions;->mergeActivityOptions(Landroid/app/ActivityOptions;Landroid/app/ActivityOptions;)Landroid/app/ActivityOptions;
} // :cond_8
/* move-object v0, v8 */
/* .line 582 */
} // .end local v8 # "options":Landroid/app/ActivityOptions;
/* .local v0, "options":Landroid/app/ActivityOptions; */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_9
(( android.app.ActivityOptions ) v0 ).toBundle ( ); // invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;
} // :cond_9
int v8 = 0; // const/4 v8, 0x0
} // :goto_2
com.android.server.wm.SafeActivityOptions .fromBundle ( v8 );
/* .line 552 */
} // .end local v0 # "options":Landroid/app/ActivityOptions;
/* :catchall_1 */
/* move-exception v0 */
try { // :try_start_3
/* monitor-exit v8 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v0 */
/* .line 585 */
} // .end local v5 # "rawPackageName":Ljava/lang/String;
} // .end local v6 # "currentFullPackageName":Ljava/lang/String;
} // .end local v7 # "currentFullActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_a
} // .end method
Boolean checkStartActivityPermission ( com.android.server.wm.ActivityStarter$Request p0 ) {
/* .locals 21 */
/* .param p1, "request" # Lcom/android/server/wm/ActivityStarter$Request; */
/* .line 611 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
v0 = this.activityInfo;
int v9 = 1; // const/4 v9, 0x1
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 612 */
com.android.server.am.ActivityManagerServiceImpl .getInstance ( );
v2 = this.caller;
v3 = this.activityInfo;
v4 = this.intent;
/* iget v5, v8, Lcom/android/server/wm/ActivityStarter$Request;->userId:I */
v6 = this.callingPackage;
v0 = /* invoke-virtual/range {v1 ..v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkRunningCompatibility(Landroid/app/IApplicationThread;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ILjava/lang/String;)Z */
int v10 = 0; // const/4 v10, 0x0
/* if-nez v0, :cond_0 */
/* .line 615 */
/* .line 617 */
} // :cond_0
/* iget v0, v8, Lcom/android/server/wm/ActivityStarter$Request;->realCallingUid:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_1 */
/* .line 618 */
v0 = android.os.Binder .getCallingUid ( );
} // :cond_1
/* iget v0, v8, Lcom/android/server/wm/ActivityStarter$Request;->realCallingUid:I */
} // :goto_0
/* move v3, v0 */
/* .line 619 */
/* .local v3, "callingUid":I */
v1 = this.activityInfo;
v2 = this.intent;
v4 = this.callingPackage;
v5 = this.reason;
v6 = this.resultTo;
/* move-object/from16 v0, p0 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/wm/ActivityStarterImpl;->checkStorageRestricted(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ILjava/lang/String;Ljava/lang/String;Landroid/os/IBinder;)Landroid/content/Intent; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 621 */
/* .line 623 */
} // :cond_2
v0 = this.mAtmService;
v1 = this.activityInfo;
v1 = this.packageName;
v2 = this.activityInfo;
v2 = this.processName;
v4 = this.activityInfo;
v4 = this.applicationInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
v0 = com.android.server.wm.WindowProcessUtils .isPackageRunning ( v0,v1,v2,v4 );
/* .line 626 */
/* .local v0, "calleeAlreadyStarted":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 627 */
/* .local v1, "bOptions":Landroid/os/Bundle; */
v2 = this.activityOptions;
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = this.activityOptions;
(( com.android.server.wm.SafeActivityOptions ) v2 ).getOriginalOptions ( ); // invoke-virtual {v2}, Lcom/android/server/wm/SafeActivityOptions;->getOriginalOptions()Landroid/app/ActivityOptions;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 628 */
v2 = this.activityOptions;
(( com.android.server.wm.SafeActivityOptions ) v2 ).getOriginalOptions ( ); // invoke-virtual {v2}, Lcom/android/server/wm/SafeActivityOptions;->getOriginalOptions()Landroid/app/ActivityOptions;
(( android.app.ActivityOptions ) v2 ).toBundle ( ); // invoke-virtual {v2}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;
/* .line 630 */
} // :cond_3
v11 = this.mSecurityHelper;
v12 = this.callingPackage;
v2 = this.activityInfo;
v13 = this.packageName;
v14 = this.intent;
v2 = this.resultTo;
if ( v2 != null) { // if-eqz v2, :cond_4
/* move v15, v9 */
} // :cond_4
/* move v15, v10 */
} // :goto_1
/* iget v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->requestCode:I */
v4 = this.activityInfo;
v4 = this.applicationInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 633 */
v18 = android.os.UserHandle .getUserId ( v4 );
/* .line 630 */
/* move/from16 v16, v2 */
/* move/from16 v17, v0 */
/* move/from16 v19, v3 */
/* move-object/from16 v20, v1 */
/* invoke-virtual/range {v11 ..v20}, Lmiui/app/ActivitySecurityHelper;->getCheckIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;ZIZIILandroid/os/Bundle;)Landroid/content/Intent; */
/* .line 635 */
/* .local v2, "checkIntent":Landroid/content/Intent; */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 636 */
this.intent = v2;
/* .line 639 */
} // .end local v0 # "calleeAlreadyStarted":Z
} // .end local v1 # "bOptions":Landroid/os/Bundle;
} // .end local v2 # "checkIntent":Landroid/content/Intent;
} // .end local v3 # "callingUid":I
} // :cond_5
/* invoke-direct/range {p0 ..p1}, Lcom/android/server/wm/ActivityStarterImpl;->resolveCheckIntent(Lcom/android/server/wm/ActivityStarter$Request;)Landroid/content/pm/ActivityInfo; */
/* .line 640 */
} // .end method
void destroyActivity ( android.content.pm.ActivityInfo p0 ) {
/* .locals 2 */
/* .param p1, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .line 451 */
/* iget-boolean v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSystemReady:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 453 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 454 */
final String v0 = "ActivityStarterImpl"; // const-string v0, "ActivityStarterImpl"
final String v1 = "aInfo is null!"; // const-string v1, "aInfo is null!"
android.util.Slog .w ( v0,v1 );
/* .line 455 */
return;
/* .line 457 */
} // :cond_1
v0 = this.mSplashScreenServiceDelegate;
(( com.miui.server.SplashScreenServiceDelegate ) v0 ).destroyActivity ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/SplashScreenServiceDelegate;->destroyActivity(Landroid/content/pm/ActivityInfo;)V
/* .line 458 */
return;
} // .end method
public android.os.IBinder finishActivity ( android.os.IBinder p0, Integer p1, android.content.Intent p2 ) {
/* .locals 0 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "resultCode" # I */
/* .param p3, "resultData" # Landroid/content/Intent; */
/* .line 420 */
} // .end method
public void finishLaunchMode ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 430 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).finishLaunchMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->finishLaunchMode(Ljava/lang/String;I)V
/* .line 431 */
return;
} // .end method
public Boolean finishLaunchedFromActivityIfNeeded ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 7 */
/* .param p1, "starting" # Lcom/android/server/wm/ActivityRecord; */
/* .line 757 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 758 */
/* .line 761 */
} // :cond_0
v1 = this.mRootWindowContainer;
/* .line 764 */
/* .local v1, "rwc":Lcom/android/server/wm/RootWindowContainer; */
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).isActivityTypeHome ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isActivityTypeHome()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 765 */
v2 = (( com.android.server.wm.RootWindowContainer ) v1 ).getChildCount ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getChildCount()I
int v3 = 1; // const/4 v3, 0x1
/* sub-int/2addr v2, v3 */
/* .local v2, "displayNdx":I */
} // :goto_0
/* if-ltz v2, :cond_2 */
/* .line 766 */
(( com.android.server.wm.RootWindowContainer ) v1 ).getChildAt ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/RootWindowContainer;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
/* check-cast v4, Lcom/android/server/wm/DisplayContent; */
/* .line 767 */
/* .local v4, "display":Lcom/android/server/wm/DisplayContent; */
/* iget-boolean v5, v4, Lcom/android/server/wm/DisplayContent;->isDefaultDisplay:Z */
/* if-nez v5, :cond_1 */
/* .line 768 */
/* new-instance v5, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v5, p1}, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityRecord;)V */
(( com.android.server.wm.DisplayContent ) v4 ).getActivity ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/DisplayContent;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;
/* .line 770 */
/* .local v5, "fromActivity":Lcom/android/server/wm/ActivityRecord; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 771 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Finish launch from activity "; // const-string v6, "Finish launch from activity "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " starting="; // const-string v6, " starting="
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " launchedFromPid="; // const-string v6, " launchedFromPid="
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " launchedFromPackage="; // const-string v6, " launchedFromPackage="
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.launchedFromPackage;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "ActivityStarterImpl"; // const-string v6, "ActivityStarterImpl"
android.util.Slog .e ( v6,v0 );
/* .line 774 */
final String v0 = "finish-launch-from"; // const-string v0, "finish-launch-from"
(( com.android.server.wm.ActivityRecord ) v5 ).finishIfPossible ( v0, v3 ); // invoke-virtual {v5, v0, v3}, Lcom/android/server/wm/ActivityRecord;->finishIfPossible(Ljava/lang/String;Z)I
/* .line 775 */
/* .line 765 */
} // .end local v4 # "display":Lcom/android/server/wm/DisplayContent;
} // .end local v5 # "fromActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_1
/* add-int/lit8 v2, v2, -0x1 */
/* .line 781 */
} // .end local v2 # "displayNdx":I
} // :cond_2
} // .end method
public java.lang.String getRebootReason ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 787 */
final String v0 = "android.intent.extra.USER_REQUESTED_SHUTDOWN"; // const-string v0, "android.intent.extra.USER_REQUESTED_SHUTDOWN"
int v1 = 0; // const/4 v1, 0x0
v0 = (( android.content.Intent ) p1 ).getBooleanExtra ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
/* .line 788 */
/* .local v0, "userRequested":Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const-string/jumbo v1, "userrequested" */
/* .line 789 */
} // :cond_0
final String v1 = "android.intent.extra.REASON"; // const-string v1, "android.intent.extra.REASON"
(( android.content.Intent ) p1 ).getStringExtra ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
} // :goto_0
/* nop */
/* .line 790 */
/* .local v1, "reason":Ljava/lang/String; */
} // .end method
public Integer getTransitionType ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 800 */
int v0 = 1; // const/4 v0, 0x1
/* if-nez p1, :cond_0 */
/* .line 801 */
} // :cond_0
v1 = v1 = this.mActivityRecordStub;
/* if-nez v1, :cond_7 */
if ( p2 != null) { // if-eqz p2, :cond_1
v1 = v1 = this.mActivityRecordStub;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 806 */
} // :cond_1
v1 = v1 = this.mActivityRecordStub;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 807 */
/* const v0, 0x7fffffeb */
/* .line 810 */
} // :cond_2
v1 = com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
/* if-nez v1, :cond_4 */
/* .line 811 */
v1 = com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 821 */
} // :cond_3
/* .line 812 */
} // :cond_4
} // :goto_0
v1 = this.mActivityRecordStub;
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 813 */
v1 = this.mActivityRecordStub;
/* .line 815 */
} // :cond_5
if ( p2 != null) { // if-eqz p2, :cond_6
v1 = this.mActivityRecordStub;
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 816 */
v1 = this.mActivityRecordStub;
/* .line 818 */
} // :cond_6
/* const v0, 0x7ffffff0 */
/* .line 802 */
} // :cond_7
} // :goto_1
v1 = this.mAtmService;
v1 = this.mWindowManager;
v1 = v1 = this.mPolicy;
/* if-nez v1, :cond_a */
if ( p2 != null) { // if-eqz p2, :cond_8
/* .line 803 */
v1 = (( com.android.server.wm.ActivityRecord ) p2 ).inFreeformWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 804 */
} // :cond_8
v0 = v0 = this.mActivityRecordStub;
if ( v0 != null) { // if-eqz v0, :cond_9
/* const v0, 0x7ffffffe */
} // :cond_9
/* const v0, 0x7ffffffc */
} // :goto_2
/* .line 803 */
} // :cond_a
} // :goto_3
} // .end method
void init ( com.android.server.wm.ActivityTaskManagerService p0, android.content.Context p1 ) {
/* .locals 1 */
/* .param p1, "service" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 121 */
this.mContext = p2;
/* .line 122 */
this.mAtmService = p1;
/* .line 123 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v0;
/* .line 124 */
return;
} // .end method
public Boolean isAllowedStartActivity ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p1, "callingUid" # I */
/* .param p2, "callingPid" # I */
/* .param p3, "callingPackage" # Ljava/lang/String; */
/* .line 280 */
v0 = android.os.UserHandle .getAppId ( p1 );
/* const/16 v1, 0x2710 */
int v2 = 1; // const/4 v2, 0x1
/* if-lt v0, v1, :cond_7 */
/* .line 281 */
v0 = com.android.server.am.PendingIntentRecordImpl .containsPendingIntent ( p3 );
/* if-nez v0, :cond_7 */
/* iget v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I */
/* if-eq p1, v0, :cond_7 */
v0 = this.mAtmService;
/* .line 283 */
v0 = (( com.android.server.wm.ActivityTaskManagerService ) v0 ).hasUserVisibleWindow ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/ActivityTaskManagerService;->hasUserVisibleWindow(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_1 */
/* .line 287 */
} // :cond_0
v0 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getAppOpsManager ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getAppOpsManager()Landroid/app/AppOpsManager;
/* .line 288 */
/* .local v0, "appops":Landroid/app/AppOpsManager; */
v1 = this.mAtmService;
v1 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v1 ).getDefaultDisplay ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;
/* .line 289 */
/* .local v1, "display":Lcom/android/server/wm/DisplayContent; */
(( com.android.server.wm.DisplayContent ) v1 ).getFocusedRootTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 290 */
/* .local v3, "stack":Lcom/android/server/wm/Task; */
/* if-nez v3, :cond_1 */
/* .line 291 */
/* .line 293 */
} // :cond_1
(( com.android.server.wm.Task ) v3 ).topRunningActivityLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 294 */
/* .local v4, "r":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v4, :cond_2 */
/* .line 295 */
/* .line 298 */
} // :cond_2
v5 = this.info;
v5 = this.applicationInfo;
/* iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* if-ne p1, v5, :cond_3 */
/* .line 299 */
/* iput p1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I */
/* .line 300 */
/* .line 302 */
} // :cond_3
/* const/16 v5, 0x2725 */
v5 = (( android.app.AppOpsManager ) v0 ).checkOpNoThrow ( v5, p1, p3 ); // invoke-virtual {v0, v5, p1, p3}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 305 */
v5 = this.mAtmService;
v5 = this.mProcessMap;
(( com.android.server.wm.WindowProcessControllerMap ) v5 ).getProcesses ( p1 ); // invoke-virtual {v5, p1}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcesses(I)Landroid/util/ArraySet;
/* .line 306 */
/* .local v5, "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;" */
if ( v5 != null) { // if-eqz v5, :cond_5
v6 = (( android.util.ArraySet ) v5 ).size ( ); // invoke-virtual {v5}, Landroid/util/ArraySet;->size()I
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 307 */
(( android.util.ArraySet ) v5 ).iterator ( ); // invoke-virtual {v5}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_5
/* check-cast v7, Lcom/android/server/wm/WindowProcessController; */
/* .line 308 */
/* .local v7, "app":Lcom/android/server/wm/WindowProcessController; */
if ( v7 != null) { // if-eqz v7, :cond_4
/* iget v8, v7, Lcom/android/server/wm/WindowProcessController;->mUid:I */
/* if-ne v8, p1, :cond_4 */
/* .line 309 */
v8 = (( com.android.server.wm.WindowProcessController ) v7 ).hasForegroundActivities ( ); // invoke-virtual {v7}, Lcom/android/server/wm/WindowProcessController;->hasForegroundActivities()Z
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 310 */
/* iput p1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I */
/* .line 311 */
/* .line 313 */
} // .end local v7 # "app":Lcom/android/server/wm/WindowProcessController;
} // :cond_4
/* .line 315 */
} // :cond_5
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "MIUILOG- Permission Denied Activity : pkg : "; // const-string v6, "MIUILOG- Permission Denied Activity : pkg : "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " uid : "; // const-string v6, " uid : "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " tuid : "; // const-string v6, " tuid : "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.info;
v6 = this.applicationInfo;
/* iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I */
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "ActivityStarterImpl"; // const-string v6, "ActivityStarterImpl"
android.util.Slog .d ( v6,v2 );
/* .line 318 */
int v2 = 0; // const/4 v2, 0x0
/* .line 320 */
} // .end local v5 # "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;"
} // :cond_6
/* .line 284 */
} // .end local v0 # "appops":Landroid/app/AppOpsManager;
} // .end local v1 # "display":Lcom/android/server/wm/DisplayContent;
} // .end local v3 # "stack":Lcom/android/server/wm/Task;
} // .end local v4 # "r":Lcom/android/server/wm/ActivityRecord;
} // :cond_7
} // :goto_1
/* iput p1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I */
/* .line 285 */
} // .end method
public Boolean isAllowedStartActivity ( android.content.Intent p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, android.content.pm.ActivityInfo p6 ) {
/* .locals 24 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callingUid" # I */
/* .param p3, "callingPid" # I */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .param p5, "realCallingUid" # I */
/* .param p6, "realCallingPid" # I */
/* .param p7, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .line 326 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move/from16 v2, p2 */
/* move/from16 v3, p5 */
/* move/from16 v4, p6 */
/* move-object/from16 v5, p7 */
v6 = this.applicationInfo;
/* iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I */
com.android.server.wm.ActivityStarterImpl .checkAndNotify ( v6 );
/* .line 327 */
/* move/from16 v6, p2 */
/* .line 328 */
/* .local v6, "checkingUid":I */
final String v7 = "MIUILOG- Permission Denied Activity : "; // const-string v7, "MIUILOG- Permission Denied Activity : "
/* const/16 v8, 0x2710 */
int v9 = 0; // const/4 v9, 0x0
final String v10 = " uid : "; // const-string v10, " uid : "
final String v11 = "ActivityStarterImpl"; // const-string v11, "ActivityStarterImpl"
/* if-eq v2, v3, :cond_1 */
v12 = /* invoke-static/range {p5 ..p5}, Landroid/os/UserHandle;->getAppId(I)I */
/* if-le v12, v8, :cond_1 */
/* .line 329 */
/* move/from16 v6, p5 */
/* .line 330 */
v12 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v12 ).getProcessController ( v4, v3 ); // invoke-virtual {v12, v4, v3}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(II)Lcom/android/server/wm/WindowProcessController;
/* .line 331 */
/* .local v12, "realApp":Lcom/android/server/wm/WindowProcessController; */
if ( v12 != null) { // if-eqz v12, :cond_0
/* .line 332 */
v13 = this.mInfo;
v13 = this.packageName;
/* move v12, v6 */
/* move-object v6, v13 */
/* move/from16 v13, p3 */
} // .end local p4 # "callingPackage":Ljava/lang/String;
/* .local v13, "callingPackage":Ljava/lang/String; */
/* .line 334 */
} // .end local v13 # "callingPackage":Ljava/lang/String;
/* .restart local p4 # "callingPackage":Ljava/lang/String; */
} // :cond_0
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = " realPid : "; // const-string v8, " realPid : "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " realUid : "; // const-string v8, " realUid : "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " pid : "; // const-string v8, " pid : "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v13, p3 */
(( java.lang.StringBuilder ) v7 ).append ( v13 ); // invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v7 );
/* .line 337 */
/* .line 328 */
} // .end local v12 # "realApp":Lcom/android/server/wm/WindowProcessController;
} // :cond_1
/* move/from16 v13, p3 */
/* .line 340 */
/* move v12, v6 */
/* move-object/from16 v6, p4 */
} // .end local p4 # "callingPackage":Ljava/lang/String;
/* .local v6, "callingPackage":Ljava/lang/String; */
/* .local v12, "checkingUid":I */
} // :goto_0
v14 = android.os.UserHandle .getAppId ( v12 );
/* const/16 v20, 0x1 */
/* if-lt v14, v8, :cond_b */
/* .line 341 */
v8 = com.android.server.am.PendingIntentRecordImpl .containsPendingIntent ( v6 );
/* if-nez v8, :cond_b */
v8 = this.applicationInfo;
v8 = this.packageName;
/* .line 342 */
v8 = com.android.server.am.PendingIntentRecordImpl .containsPendingIntent ( v8 );
/* if-nez v8, :cond_b */
/* iget v8, v0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I */
/* if-eq v12, v8, :cond_b */
v8 = this.mAtmService;
/* .line 344 */
v8 = (( com.android.server.wm.ActivityTaskManagerService ) v8 ).hasUserVisibleWindow ( v12 ); // invoke-virtual {v8, v12}, Lcom/android/server/wm/ActivityTaskManagerService;->hasUserVisibleWindow(I)Z
/* if-nez v8, :cond_b */
v8 = this.name;
/* .line 345 */
final String v14 = "android.service.dreams.DreamActivity"; // const-string v14, "android.service.dreams.DreamActivity"
v8 = (( java.lang.String ) v14 ).equals ( v8 ); // invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
v8 = android.miui.AppOpsUtils .isXOptMode ( );
if ( v8 != null) { // if-eqz v8, :cond_2
/* goto/16 :goto_2 */
/* .line 350 */
} // :cond_2
v8 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v8 ).getAppOpsManager ( ); // invoke-virtual {v8}, Lcom/android/server/wm/ActivityTaskManagerService;->getAppOpsManager()Landroid/app/AppOpsManager;
/* .line 351 */
/* .local v8, "appops":Landroid/app/AppOpsManager; */
v14 = this.mAtmService;
v14 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v14 ).getDefaultDisplay ( ); // invoke-virtual {v14}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;
/* .line 352 */
/* .local v21, "display":Lcom/android/server/wm/DisplayContent; */
/* invoke-virtual/range {v21 ..v21}, Lcom/android/server/wm/DisplayContent;->getFocusedRootTask()Lcom/android/server/wm/Task; */
/* .line 353 */
/* .local v22, "stack":Lcom/android/server/wm/Task; */
/* if-nez v22, :cond_3 */
/* .line 354 */
/* .line 357 */
} // :cond_3
v14 = this.mAtmService;
v14 = this.mWindowManager;
v14 = (( com.android.server.wm.WindowManagerService ) v14 ).isKeyguardLocked ( ); // invoke-virtual {v14}, Lcom/android/server/wm/WindowManagerService;->isKeyguardLocked()Z
final String v15 = " pkg : "; // const-string v15, " pkg : "
if ( v14 != null) { // if-eqz v14, :cond_4
/* .line 358 */
/* const/16 v14, 0x24 */
/* invoke-direct {v0, v14, v6, v1}, Lcom/android/server/wm/ActivityStarterImpl;->recordAppBehavior(ILjava/lang/String;Landroid/content/Intent;)V */
/* .line 359 */
/* const/16 v16, 0x2724 */
/* const/16 v18, 0x0 */
final String v19 = "ActivityTaskManagerServiceInjector#isAllowedStartActivity"; // const-string v19, "ActivityTaskManagerServiceInjector#isAllowedStartActivity"
/* move-object v14, v8 */
/* move-object v9, v15 */
/* move/from16 v15, v16 */
/* move/from16 v16, v12 */
/* move-object/from16 v17, v6 */
v14 = /* invoke-virtual/range {v14 ..v19}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
if ( v14 != null) { // if-eqz v14, :cond_5
/* .line 362 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "MIUILOG- Permission Denied Activity KeyguardLocked: "; // const-string v14, "MIUILOG- Permission Denied Activity KeyguardLocked: "
(( java.lang.StringBuilder ) v7 ).append ( v14 ); // invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v7 );
/* .line 364 */
int v7 = 0; // const/4 v7, 0x0
/* .line 357 */
} // :cond_4
/* move-object v9, v15 */
/* .line 368 */
} // :cond_5
/* invoke-virtual/range {v22 ..v22}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord; */
/* .line 369 */
/* .local v15, "r":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v15, :cond_6 */
/* .line 370 */
/* .line 373 */
} // :cond_6
v14 = this.info;
v14 = this.applicationInfo;
/* iget v14, v14, Landroid/content/pm/ApplicationInfo;->uid:I */
/* if-ne v12, v14, :cond_7 */
/* .line 374 */
v7 = this.applicationInfo;
/* iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iput v7, v0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I */
/* .line 375 */
/* .line 378 */
} // :cond_7
/* const/16 v14, 0x1b */
/* invoke-direct {v0, v14, v6, v1}, Lcom/android/server/wm/ActivityStarterImpl;->recordAppBehavior(ILjava/lang/String;Landroid/content/Intent;)V */
/* .line 379 */
/* const/16 v14, 0x2725 */
v14 = (( android.app.AppOpsManager ) v8 ).checkOpNoThrow ( v14, v12, v6 ); // invoke-virtual {v8, v14, v12, v6}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I
if ( v14 != null) { // if-eqz v14, :cond_a
/* .line 382 */
v14 = this.mAtmService;
v14 = this.mProcessMap;
(( com.android.server.wm.WindowProcessControllerMap ) v14 ).getProcesses ( v12 ); // invoke-virtual {v14, v12}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcesses(I)Landroid/util/ArraySet;
/* .line 383 */
/* .local v23, "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;" */
if ( v23 != null) { // if-eqz v23, :cond_9
v14 = /* invoke-virtual/range {v23 ..v23}, Landroid/util/ArraySet;->size()I */
if ( v14 != null) { // if-eqz v14, :cond_9
/* .line 384 */
/* invoke-virtual/range {v23 ..v23}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator; */
v16 = } // :goto_1
if ( v16 != null) { // if-eqz v16, :cond_9
/* move-object/from16 v2, v16 */
/* check-cast v2, Lcom/android/server/wm/WindowProcessController; */
/* .line 385 */
/* .local v2, "app":Lcom/android/server/wm/WindowProcessController; */
if ( v2 != null) { // if-eqz v2, :cond_8
/* iget v3, v2, Lcom/android/server/wm/WindowProcessController;->mUid:I */
/* if-ne v3, v12, :cond_8 */
/* .line 386 */
v3 = (( com.android.server.wm.WindowProcessController ) v2 ).hasForegroundActivities ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowProcessController;->hasForegroundActivities()Z
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 387 */
v3 = this.applicationInfo;
/* iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iput v3, v0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I */
/* .line 388 */
/* .line 390 */
} // .end local v2 # "app":Lcom/android/server/wm/WindowProcessController;
} // :cond_8
/* move/from16 v2, p2 */
/* move/from16 v3, p5 */
/* .line 392 */
} // :cond_9
/* const/16 v2, 0x2725 */
/* const/16 v18, 0x0 */
final String v19 = "ActivityTaskManagerServiceInjector#isAllowedStartActivity"; // const-string v19, "ActivityTaskManagerServiceInjector#isAllowedStartActivity"
/* move-object v14, v8 */
/* move-object v3, v15 */
} // .end local v15 # "r":Lcom/android/server/wm/ActivityRecord;
/* .local v3, "r":Lcom/android/server/wm/ActivityRecord; */
/* move v15, v2 */
/* move/from16 v16, v12 */
/* move-object/from16 v17, v6 */
/* invoke-virtual/range {v14 ..v19}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
/* .line 394 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v10 ); // invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v12 ); // invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " tuid : "; // const-string v7, " tuid : "
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.info;
v7 = this.applicationInfo;
/* iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I */
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v2 );
/* .line 397 */
int v2 = 0; // const/4 v2, 0x0
/* .line 399 */
} // .end local v3 # "r":Lcom/android/server/wm/ActivityRecord;
} // .end local v23 # "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;"
/* .restart local v15 # "r":Lcom/android/server/wm/ActivityRecord; */
} // :cond_a
/* .line 346 */
} // .end local v8 # "appops":Landroid/app/AppOpsManager;
} // .end local v15 # "r":Lcom/android/server/wm/ActivityRecord;
} // .end local v21 # "display":Lcom/android/server/wm/DisplayContent;
} // .end local v22 # "stack":Lcom/android/server/wm/Task;
} // :cond_b
} // :goto_2
v2 = this.applicationInfo;
/* iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iput v2, v0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I */
/* .line 347 */
} // .end method
public Boolean isAppLockActivity ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "activity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 907 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.mActivityComponent;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mActivityComponent;
/* .line 908 */
(( android.content.ComponentName ) v0 ).flattenToString ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
final String v1 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v1, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 907 */
} // :goto_0
} // .end method
public Boolean isCarWithDisplay ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 2 */
/* .param p1, "dc" # Lcom/android/server/wm/DisplayContent; */
/* .line 222 */
if ( p1 != null) { // if-eqz p1, :cond_0
(( com.android.server.wm.DisplayContent ) p1 ).getDisplay ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 223 */
(( com.android.server.wm.DisplayContent ) p1 ).getDisplay ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;
(( android.view.Display ) v0 ).getName ( ); // invoke-virtual {v0}, Landroid/view/Display;->getName()Ljava/lang/String;
/* .line 224 */
/* .local v0, "displayName":Ljava/lang/String; */
v1 = v1 = com.android.server.wm.ActivityStarterImpl.CARLINK_VIRTUAL_DISPLAY_SET;
/* .line 226 */
} // .end local v0 # "displayName":Ljava/lang/String;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isCarWithDisplay ( com.android.server.wm.RootWindowContainer p0, android.app.ActivityOptions p1 ) {
/* .locals 3 */
/* .param p1, "rootWindowContainer" # Lcom/android/server/wm/RootWindowContainer; */
/* .param p2, "options" # Landroid/app/ActivityOptions; */
/* .line 709 */
/* if-nez p1, :cond_0 */
/* .line 710 */
int v0 = 0; // const/4 v0, 0x0
/* .line 712 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 713 */
/* .local v0, "displayId":I */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 714 */
v0 = (( android.app.ActivityOptions ) p2 ).getLaunchDisplayId ( ); // invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchDisplayId()I
/* .line 716 */
} // :cond_1
/* nop */
/* .line 717 */
(( com.android.server.wm.RootWindowContainer ) p1 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 718 */
/* .local v1, "focusedRootTask":Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 719 */
v0 = (( com.android.server.wm.Task ) v1 ).getDisplayId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getDisplayId()I
/* .line 722 */
} // .end local v1 # "focusedRootTask":Lcom/android/server/wm/Task;
} // :cond_2
} // :goto_0
(( com.android.server.wm.RootWindowContainer ) p1 ).getDisplayContent ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
/* .line 723 */
/* .local v1, "displayContent":Lcom/android/server/wm/DisplayContent; */
v2 = (( com.android.server.wm.ActivityStarterImpl ) p0 ).isCarWithDisplay ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityStarterImpl;->isCarWithDisplay(Lcom/android/server/wm/DisplayContent;)Z
} // .end method
public void logStartActivityError ( Integer p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "err" # I */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 728 */
final String v0 = "ActivityStarterImpl"; // const-string v0, "ActivityStarterImpl"
/* packed-switch p1, :pswitch_data_0 */
/* .line 747 */
/* :pswitch_0 */
/* .line 730 */
/* :pswitch_1 */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Error: Activity not started, unable to resolve "; // const-string v2, "Error: Activity not started, unable to resolve "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 731 */
(( android.content.Intent ) p2 ).toString ( ); // invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 730 */
android.util.Slog .e ( v0,v1 );
/* .line 732 */
/* .line 734 */
/* :pswitch_2 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Error: Activity class "; // const-string v2, "Error: Activity class "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 735 */
(( android.content.Intent ) p2 ).getComponent ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).toShortString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " does not exist."; // const-string v2, " does not exist."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 734 */
android.util.Slog .e ( v0,v1 );
/* .line 737 */
/* .line 739 */
/* :pswitch_3 */
final String v1 = "Error: Activity not started, you requested to both forward and receive its result"; // const-string v1, "Error: Activity not started, you requested to both forward and receive its result"
android.util.Slog .e ( v0,v1 );
/* .line 741 */
/* .line 743 */
/* :pswitch_4 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Error: Activity not started, voice control not allowed for: "; // const-string v2, "Error: Activity not started, voice control not allowed for: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 745 */
/* .line 750 */
/* :catch_0 */
/* move-exception v0 */
/* .line 747 */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Error: Activity not started, unknown error code "; // const-string v2, "Error: Activity not started, unknown error code "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 752 */
} // :goto_1
/* .line 751 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_2
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 753 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
return;
/* :pswitch_data_0 */
/* .packed-switch -0x61 */
/* :pswitch_4 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public Boolean moveToFrontForSplitScreen ( Boolean p0, Boolean p1, com.android.server.wm.Task p2, com.android.server.wm.ActivityRecord p3, com.android.server.wm.ActivityRecord p4 ) {
/* .locals 2 */
/* .param p1, "differentTopTask" # Z */
/* .param p2, "avoidMoveToFront" # Z */
/* .param p3, "targetRootTask" # Lcom/android/server/wm/Task; */
/* .param p4, "intentActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p5, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 832 */
/* if-nez p1, :cond_0 */
/* if-nez p2, :cond_0 */
/* iget-boolean v0, p3, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 834 */
v0 = (( com.android.server.wm.Task ) p3 ).getWindowingMode ( ); // invoke-virtual {p3}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v1 = 6; // const/4 v1, 0x6
/* if-ne v0, v1, :cond_0 */
v0 = com.android.server.wm.ActivityRecord$State.RESUMED;
/* .line 835 */
v0 = (( com.android.server.wm.ActivityRecord ) p4 ).isState ( v0 ); // invoke-virtual {p4, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez p5, :cond_0 */
/* .line 837 */
v0 = (( com.android.server.wm.ActivityRecord ) p4 ).isDescendantOf ( p3 ); // invoke-virtual {p4, p3}, Lcom/android/server/wm/ActivityRecord;->isDescendantOf(Lcom/android/server/wm/WindowContainer;)Z
/* if-nez v0, :cond_0 */
/* .line 838 */
v0 = (( com.android.server.wm.ActivityRecord ) p4 ).getWindowingMode ( ); // invoke-virtual {p4}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 832 */
} // :goto_0
} // .end method
void onSystemReady ( ) {
/* .locals 2 */
/* .line 127 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSystemReady:Z */
/* .line 128 */
/* new-instance v0, Lcom/miui/server/SplashScreenServiceDelegate; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/SplashScreenServiceDelegate;-><init>(Landroid/content/Context;)V */
this.mSplashScreenServiceDelegate = v0;
/* .line 129 */
/* new-instance v0, Lmiui/app/ActivitySecurityHelper; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lmiui/app/ActivitySecurityHelper;-><init>(Landroid/content/Context;)V */
this.mSecurityHelper = v0;
/* .line 130 */
/* const-class v0, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/security/SecurityManagerInternal; */
this.mSecurityManagerInternal = v0;
/* .line 131 */
/* const-class v0, Lcom/android/server/PowerConsumptionServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/PowerConsumptionServiceInternal; */
this.mPowerConsumptionServiceInternal = v0;
/* .line 132 */
return;
} // .end method
public void recordNewIntent ( android.content.Intent p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .line 878 */
final String v0 = "com.baidu.BaiduMap"; // const-string v0, "com.baidu.BaiduMap"
final String v1 = "com.tencent.map"; // const-string v1, "com.tencent.map"
final String v2 = "com.autonavi.minimap"; // const-string v2, "com.autonavi.minimap"
/* filled-new-array {v2, v0, v1}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
/* .line 883 */
/* .local v0, "mMapList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v1 = ""; // const-string v1, ""
/* .line 884 */
/* .local v1, "pkg":Ljava/lang/String; */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 885 */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 887 */
v2 = } // :cond_0
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = android.text.TextUtils .equals ( p2,v1 );
/* if-nez v2, :cond_3 */
/* .line 888 */
final String v2 = "com.miui.carlink"; // const-string v2, "com.miui.carlink"
v2 = android.text.TextUtils .equals ( p2,v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 891 */
} // :cond_1
(( android.content.Intent ) p1 ).getDataString ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;
/* .line 892 */
/* .local v2, "dat":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 893 */
return;
/* .line 897 */
} // :cond_2
try { // :try_start_0
final String v3 = "UTF-8"; // const-string v3, "UTF-8"
java.net.URLDecoder .decode ( v2,v3 );
/* :try_end_0 */
/* .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v2, v3 */
/* .line 900 */
/* .line 898 */
/* :catch_0 */
/* move-exception v3 */
/* .line 899 */
/* .local v3, "e":Ljava/io/UnsupportedEncodingException; */
(( java.io.UnsupportedEncodingException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
/* .line 901 */
} // .end local v3 # "e":Ljava/io/UnsupportedEncodingException;
} // :goto_0
final String v3 = "ActivityStarterImpl"; // const-string v3, "ActivityStarterImpl"
final String v4 = "Ready To Send BroadCast To carlink"; // const-string v4, "Ready To Send BroadCast To carlink"
android.util.Slog .d ( v3,v4 );
/* .line 902 */
/* new-instance v3, Lcom/android/server/wm/ActivityCarWithStarterImpl; */
/* invoke-direct {v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;-><init>()V */
/* .line 903 */
/* .local v3, "activityCarWithStarter":Lcom/android/server/wm/ActivityCarWithStarterImpl; */
v4 = this.mContext;
(( com.android.server.wm.ActivityCarWithStarterImpl ) v3 ).recordCarWithIntent ( v4, p2, v1, v2 ); // invoke-virtual {v3, v4, p2, v1, v2}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordCarWithIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
/* .line 904 */
return;
/* .line 889 */
} // .end local v2 # "dat":Ljava/lang/String;
} // .end local v3 # "activityCarWithStarter":Lcom/android/server/wm/ActivityCarWithStarterImpl;
} // :cond_3
} // :goto_1
return;
} // .end method
android.content.Intent requestSplashScreen ( android.content.Intent p0, android.content.pm.ActivityInfo p1, com.android.server.wm.SafeActivityOptions p2, android.app.IApplicationThread p3 ) {
/* .locals 3 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p3, "options" # Lcom/android/server/wm/SafeActivityOptions; */
/* .param p4, "caller" # Landroid/app/IApplicationThread; */
/* .line 463 */
/* iget-boolean v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSystemReady:Z */
/* if-nez v0, :cond_0 */
/* .line 465 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_5
/* if-nez p2, :cond_1 */
/* .line 471 */
} // :cond_1
v0 = this.mAtmService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 472 */
try { // :try_start_0
v1 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getProcessController ( p4 ); // invoke-virtual {v1, p4}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Landroid/app/IApplicationThread;)Lcom/android/server/wm/WindowProcessController;
/* .line 473 */
/* .local v1, "callerApp":Lcom/android/server/wm/WindowProcessController; */
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 474 */
v2 = this.mAtmService;
v2 = this.mTaskSupervisor;
(( com.android.server.wm.SafeActivityOptions ) p3 ).getOptions ( p1, p2, v1, v2 ); // invoke-virtual {p3, p1, p2, v1, v2}, Lcom/android/server/wm/SafeActivityOptions;->getOptions(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Lcom/android/server/wm/WindowProcessController;Lcom/android/server/wm/ActivityTaskSupervisor;)Landroid/app/ActivityOptions;
/* .line 475 */
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* move-object v1, v2 */
/* .line 476 */
/* .local v1, "checkedOptions":Landroid/app/ActivityOptions; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 478 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 479 */
v0 = (( android.app.ActivityOptions ) v1 ).getLaunchWindowingMode ( ); // invoke-virtual {v1}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I
int v2 = 6; // const/4 v2, 0x6
/* if-eq v0, v2, :cond_3 */
/* .line 480 */
v0 = (( android.app.ActivityOptions ) v1 ).getLaunchWindowingMode ( ); // invoke-virtual {v1}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I
int v2 = 5; // const/4 v2, 0x5
/* if-ne v0, v2, :cond_4 */
/* .line 481 */
} // :cond_3
final String v0 = "ActivityStarterImpl"; // const-string v0, "ActivityStarterImpl"
final String v2 = "The Activity is in freeForm|split windowing mode !"; // const-string v2, "The Activity is in freeForm|split windowing mode !"
android.util.Slog .w ( v0,v2 );
/* .line 482 */
/* .line 485 */
} // :cond_4
v0 = this.mSplashScreenServiceDelegate;
(( com.miui.server.SplashScreenServiceDelegate ) v0 ).requestSplashScreen ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/SplashScreenServiceDelegate;->requestSplashScreen(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Landroid/content/Intent;
/* .line 476 */
} // .end local v1 # "checkedOptions":Landroid/app/ActivityOptions;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 466 */
} // :cond_5
} // :goto_1
final String v0 = "ActivityStarterImpl"; // const-string v0, "ActivityStarterImpl"
final String v1 = "Intent or aInfo is null!"; // const-string v1, "Intent or aInfo is null!"
android.util.Slog .w ( v0,v1 );
/* .line 467 */
} // .end method
android.content.pm.ActivityInfo resolveSplashIntent ( android.content.pm.ActivityInfo p0, android.content.Intent p1, android.app.ProfilerInfo p2, Integer p3, Integer p4, Integer p5 ) {
/* .locals 10 */
/* .param p1, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "profilerInfo" # Landroid/app/ProfilerInfo; */
/* .param p4, "userId" # I */
/* .param p5, "filterCallingUid" # I */
/* .param p6, "callingPid" # I */
/* .line 491 */
/* if-nez p2, :cond_0 */
/* .line 492 */
/* .line 495 */
} // :cond_0
(( android.content.Intent ) p2 ).getComponent ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 496 */
/* .local v8, "component":Landroid/content/ComponentName; */
/* if-nez v8, :cond_1 */
/* .line 497 */
/* .line 499 */
} // :cond_1
final String v0 = "com.miui.systemAdSolution"; // const-string v0, "com.miui.systemAdSolution"
(( android.content.ComponentName ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 500 */
(( android.content.ComponentName ) v8 ).getClassName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
final String v1 = "com.miui.systemAdSolution.splashscreen.SplashActivity"; // const-string v1, "com.miui.systemAdSolution.splashscreen.SplashActivity"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 501 */
/* move-object v9, p0 */
v0 = this.mAtmService;
v0 = this.mTaskSupervisor;
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* move-object v1, p2 */
/* move-object v4, p3 */
/* move v5, p4 */
/* move v6, p5 */
/* move/from16 v7, p6 */
/* invoke-virtual/range {v0 ..v7}, Lcom/android/server/wm/ActivityTaskSupervisor;->resolveActivity(Landroid/content/Intent;Ljava/lang/String;ILandroid/app/ProfilerInfo;III)Landroid/content/pm/ActivityInfo; */
/* .line 500 */
} // :cond_2
/* move-object v9, p0 */
/* .line 499 */
} // :cond_3
/* move-object v9, p0 */
/* .line 504 */
} // :goto_0
} // .end method
public Boolean shouldRebootReasonCheckNull ( ) {
/* .locals 2 */
/* .line 795 */
final String v0 = "persist.sys.stability.rebootreason_check"; // const-string v0, "persist.sys.stability.rebootreason_check"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
public Boolean skipForSplitScreen ( com.android.server.wm.Task p0, com.android.server.wm.Task p1, com.android.server.wm.Task p2, com.android.server.wm.ActivityRecord p3, Integer p4 ) {
/* .locals 6 */
/* .param p1, "targetTask" # Lcom/android/server/wm/Task; */
/* .param p2, "targetRootTask" # Lcom/android/server/wm/Task; */
/* .param p3, "topRootTask" # Lcom/android/server/wm/Task; */
/* .param p4, "top" # Lcom/android/server/wm/ActivityRecord; */
/* .param p5, "launchFlags" # I */
/* .line 843 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* if-nez p1, :cond_0 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* iget-boolean v2, p2, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 846 */
v2 = (( com.android.server.wm.Task ) p2 ).getWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v3 = 6; // const/4 v3, 0x6
/* if-ne v2, v3, :cond_0 */
/* if-eq p3, p2, :cond_0 */
/* .line 848 */
v2 = (( com.android.server.wm.ActivityRecord ) p4 ).isDescendantOf ( p2 ); // invoke-virtual {p4, p2}, Lcom/android/server/wm/ActivityRecord;->isDescendantOf(Lcom/android/server/wm/WindowContainer;)Z
/* if-nez v2, :cond_0 */
/* const/high16 v2, 0x8000000 */
/* and-int/2addr v2, p5 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v1 */
} // :cond_0
/* move v2, v0 */
/* .line 850 */
/* .local v2, "startForSplit":Z */
} // :goto_0
final String v3 = "ActivityStarterImpl"; // const-string v3, "ActivityStarterImpl"
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 851 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "skip for multiple task, top : " */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 852 */
/* .line 855 */
} // :cond_1
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
v4 = v5 = this.packageName;
if ( v4 != null) { // if-eqz v4, :cond_3
/* const/high16 v4, 0x20000000 */
/* and-int/2addr v4, p5 */
/* if-nez v4, :cond_2 */
/* iget v4, p4, Lcom/android/server/wm/ActivityRecord;->launchMode:I */
/* if-ne v1, v4, :cond_3 */
} // :cond_2
/* move v4, v1 */
} // :cond_3
/* move v4, v0 */
/* .line 858 */
/* .local v4, "startForAE":Z */
} // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 859 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "skip for single top activity, top : " */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 860 */
/* .line 863 */
} // :cond_4
} // .end method
public void startActivityUncheckedBefore ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 11 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 697 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.mPowerConsumptionServiceInternal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 698 */
v1 = this.mActivityComponent;
(( android.content.ComponentName ) v1 ).getClassName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
(( com.android.server.PowerConsumptionServiceInternal ) v0 ).noteStartActivityForPowerConsumption ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/PowerConsumptionServiceInternal;->noteStartActivityForPowerConsumption(Ljava/lang/String;)V
/* .line 701 */
} // :cond_0
v2 = this.mSmartPowerService;
v0 = this.info;
v3 = this.name;
/* .line 702 */
v4 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v5 = (( com.android.server.wm.ActivityRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I
v6 = this.packageName;
/* iget v7, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromUid:I */
/* iget v8, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I */
v9 = this.launchedFromPackage;
v0 = this.mAtmService;
/* .line 704 */
(( com.android.server.wm.ActivityRecord ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getProcessName()Ljava/lang/String;
v10 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getProcessController ( v1, v10 ); // invoke-virtual {v0, v1, v10}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;
/* if-nez v0, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* move v10, v0 */
/* .line 701 */
/* invoke-interface/range {v2 ..v10}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onActivityStartUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V */
/* .line 705 */
return;
} // .end method
public void triggerLaunchMode ( java.lang.String p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 426 */
return;
} // .end method
public void updateLastStartActivityUid ( java.lang.String p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "foregroundPackageName" # Ljava/lang/String; */
/* .param p2, "lastUid" # I */
/* .line 511 */
/* if-nez p1, :cond_0 */
return;
/* .line 512 */
} // :cond_0
v0 = this.mDefaultHomePkgNames;
/* if-nez v0, :cond_3 */
/* .line 513 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 514 */
/* .local v0, "homeActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;" */
android.app.AppGlobals .getPackageManager ( );
/* .line 516 */
/* .local v1, "pm":Landroid/content/pm/IPackageManager; */
try { // :try_start_0
/* .line 517 */
/* .local v2, "currentDefaultHome":Landroid/content/ComponentName; */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-lez v3, :cond_2 */
/* .line 518 */
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/content/pm/ResolveInfo; */
/* .line 519 */
/* .local v4, "info":Landroid/content/pm/ResolveInfo; */
v5 = this.mDefaultHomePkgNames;
/* if-nez v5, :cond_1 */
/* .line 520 */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
this.mDefaultHomePkgNames = v5;
/* .line 522 */
} // :cond_1
v5 = this.mDefaultHomePkgNames;
v6 = this.activityInfo;
v6 = this.packageName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 523 */
/* nop */
} // .end local v4 # "info":Landroid/content/pm/ResolveInfo;
/* .line 527 */
} // .end local v2 # "currentDefaultHome":Landroid/content/ComponentName;
} // :cond_2
/* .line 525 */
/* :catch_0 */
/* move-exception v2 */
/* .line 529 */
} // .end local v0 # "homeActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
} // .end local v1 # "pm":Landroid/content/pm/IPackageManager;
} // :cond_3
} // :goto_1
v0 = this.mDefaultHomePkgNames;
/* if-nez v0, :cond_4 */
return;
/* .line 530 */
v0 = } // :cond_4
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 531 */
/* iput p2, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I */
/* .line 533 */
} // :cond_5
return;
} // .end method
