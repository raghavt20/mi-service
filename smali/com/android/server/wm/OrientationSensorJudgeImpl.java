public class com.android.server.wm.OrientationSensorJudgeImpl extends com.android.server.wm.OrientationSensorJudgeStub {
	 /* .source "OrientationSensorJudgeImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler;, */
	 /* Lcom/android/server/wm/OrientationSensorJudgeImpl$TaskStackListenerImpl; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean LOG;
private static final Integer MSG_UPDATE_FOREGROUND_APP;
private static final Integer MSG_UPDATE_FOREGROUND_APP_SYNC;
private static final java.lang.String TAG;
private static final Boolean mSupportUIOrientationV2;
/* # instance fields */
private android.app.IActivityTaskManager mActivityTaskManager;
private com.android.server.wm.ActivityTaskManagerInternal mActivityTaskManagerInternal;
private Integer mDesiredRotation;
private java.lang.String mForegroundAppPackageName;
private android.os.Handler mHandler;
private java.lang.String mLastPackageName;
private Integer mLastReportedRotation;
private com.android.server.wm.WindowOrientationListener$OrientationSensorJudge mOrientationSensorJudge;
private java.lang.String mPendingForegroundAppPackageName;
private android.app.TaskStackListener mTaskStackListener;
/* # direct methods */
public static void $r8$lambda$CBmdnJPE8xJjtqE4B66hgR76txI ( com.android.server.wm.OrientationSensorJudgeImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->lambda$initialize$0()V */
	 return;
} // .end method
static android.app.IActivityTaskManager -$$Nest$fgetmActivityTaskManager ( com.android.server.wm.OrientationSensorJudgeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mActivityTaskManager;
} // .end method
static java.lang.String -$$Nest$fgetmForegroundAppPackageName ( com.android.server.wm.OrientationSensorJudgeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mForegroundAppPackageName;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.wm.OrientationSensorJudgeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static void -$$Nest$fputmPendingForegroundAppPackageName ( com.android.server.wm.OrientationSensorJudgeImpl p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mPendingForegroundAppPackageName = p1;
	 return;
} // .end method
static void -$$Nest$mupdateForegroundApp ( com.android.server.wm.OrientationSensorJudgeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->updateForegroundApp()V */
	 return;
} // .end method
static void -$$Nest$mupdateForegroundAppSync ( com.android.server.wm.OrientationSensorJudgeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->updateForegroundAppSync()V */
	 return;
} // .end method
static Boolean -$$Nest$sfgetLOG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->LOG:Z */
} // .end method
static com.android.server.wm.OrientationSensorJudgeImpl ( ) {
	 /* .locals 2 */
	 /* .line 26 */
	 final String v0 = "debug.orientation.log"; // const-string v0, "debug.orientation.log"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.wm.OrientationSensorJudgeImpl.LOG = (v0!= 0);
	 /* .line 29 */
	 /* nop */
	 /* .line 30 */
	 /* const-string/jumbo v0, "support_ui_orientation_v2" */
	 v0 = 	 miui.util.FeatureParser .getBoolean ( v0,v1 );
	 com.android.server.wm.OrientationSensorJudgeImpl.mSupportUIOrientationV2 = (v0!= 0);
	 /* .line 29 */
	 return;
} // .end method
public com.android.server.wm.OrientationSensorJudgeImpl ( ) {
	 /* .locals 1 */
	 /* .line 24 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeStub;-><init>()V */
	 /* .line 33 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mLastPackageName = v0;
	 /* .line 34 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* iput v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I */
	 /* .line 35 */
	 /* iput v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastReportedRotation:I */
	 return;
} // .end method
private java.lang.String getTopAppPackageName ( ) {
	 /* .locals 3 */
	 /* .line 159 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 160 */
	 /* .local v0, "packageName":Ljava/lang/String; */
	 v1 = this.mActivityTaskManagerInternal;
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 161 */
		 /* nop */
		 /* .line 162 */
		 (( com.android.server.wm.ActivityTaskManagerInternal ) v1 ).getTopApp ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerInternal;->getTopApp()Lcom/android/server/wm/WindowProcessController;
		 /* .line 163 */
		 /* .local v1, "controller":Lcom/android/server/wm/WindowProcessController; */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 v2 = this.mInfo;
			 if ( v2 != null) { // if-eqz v2, :cond_0
				 v2 = this.mInfo;
				 v2 = this.packageName;
				 if ( v2 != null) { // if-eqz v2, :cond_0
					 /* .line 166 */
					 v2 = this.mInfo;
					 v0 = this.packageName;
					 /* .line 169 */
				 } // .end local v1 # "controller":Lcom/android/server/wm/WindowProcessController;
			 } // :cond_0
		 } // .end method
		 private void lambda$initialize$0 ( ) { //synthethic
			 /* .locals 0 */
			 /* .line 56 */
			 /* invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->registerForegroundAppUpdater()V */
			 /* .line 57 */
			 return;
		 } // .end method
		 private void registerForegroundAppUpdater ( ) {
			 /* .locals 2 */
			 /* .line 66 */
			 try { // :try_start_0
				 v0 = this.mActivityTaskManager;
				 v1 = this.mTaskStackListener;
				 /* .line 69 */
				 /* invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->updateForegroundApp()V */
				 /* :try_end_0 */
				 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 72 */
				 /* .line 70 */
				 /* :catch_0 */
				 /* move-exception v0 */
				 /* .line 73 */
			 } // :goto_0
			 return;
		 } // .end method
		 private void updateForegroundApp ( ) {
			 /* .locals 2 */
			 /* .line 81 */
			 com.android.internal.os.BackgroundThread .getHandler ( );
			 /* new-instance v1, Lcom/android/server/wm/OrientationSensorJudgeImpl$1; */
			 /* invoke-direct {v1, p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl$1;-><init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V */
			 (( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
			 /* .line 114 */
			 return;
		 } // .end method
		 private void updateForegroundAppSync ( ) {
			 /* .locals 2 */
			 /* .line 120 */
			 v0 = this.mPendingForegroundAppPackageName;
			 this.mForegroundAppPackageName = v0;
			 /* .line 121 */
			 int v0 = 0; // const/4 v0, 0x0
			 this.mPendingForegroundAppPackageName = v0;
			 /* .line 122 */
			 /* sget-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->LOG:Z */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 123 */
				 /* new-instance v0, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
				 /* const-string/jumbo v1, "updateForegroundAppSync, mPendingPackage = " */
				 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 v1 = this.mForegroundAppPackageName;
				 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 final String v1 = "OrientationSensorJudgeImpl"; // const-string v1, "OrientationSensorJudgeImpl"
				 android.util.Slog .d ( v1,v0 );
				 /* .line 125 */
			 } // :cond_0
			 /* iget v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastReportedRotation:I */
			 int v1 = 4; // const/4 v1, 0x4
			 /* if-ne v0, v1, :cond_1 */
			 /* .line 126 */
			 v0 = this.mOrientationSensorJudge;
			 (( com.android.server.wm.WindowOrientationListener$OrientationSensorJudge ) v0 ).callFinalizeRotation ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->callFinalizeRotation(I)V
			 /* .line 128 */
		 } // :cond_1
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer finalizeRotation ( Integer p0 ) {
		 /* .locals 4 */
		 /* .param p1, "reportedRotation" # I */
		 /* .line 132 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->getTopAppPackageName()Ljava/lang/String; */
		 /* .line 133 */
		 /* .local v0, "packageName":Ljava/lang/String; */
		 /* iput p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastReportedRotation:I */
		 /* .line 134 */
		 /* if-ltz p1, :cond_0 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* if-gt p1, v1, :cond_0 */
		 /* .line 135 */
		 /* iput p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I */
		 /* .line 136 */
		 v1 = this.mOrientationSensorJudge;
		 (( com.android.server.wm.WindowOrientationListener$OrientationSensorJudge ) v1 ).updateDesiredRotation ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->updateDesiredRotation(I)V
		 /* .line 137 */
		 v1 = this.mOrientationSensorJudge;
		 v1 = 		 (( com.android.server.wm.WindowOrientationListener$OrientationSensorJudge ) v1 ).evaluateRotationChangeLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->evaluateRotationChangeLocked()I
		 /* .local v1, "newRotation":I */
		 /* .line 138 */
	 } // .end local v1 # "newRotation":I
} // :cond_0
int v1 = 4; // const/4 v1, 0x4
/* if-ne p1, v1, :cond_2 */
/* .line 139 */
if ( v0 != null) { // if-eqz v0, :cond_1
	 v1 = this.mLastPackageName;
	 v1 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v1, :cond_1 */
	 /* .line 140 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* iput v1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I */
	 /* .line 141 */
	 v2 = this.mOrientationSensorJudge;
	 (( com.android.server.wm.WindowOrientationListener$OrientationSensorJudge ) v2 ).updateDesiredRotation ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->updateDesiredRotation(I)V
	 /* .line 142 */
	 v1 = this.mOrientationSensorJudge;
	 v1 = 	 (( com.android.server.wm.WindowOrientationListener$OrientationSensorJudge ) v1 ).evaluateRotationChangeLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->evaluateRotationChangeLocked()I
	 /* .restart local v1 # "newRotation":I */
	 /* .line 144 */
} // .end local v1 # "newRotation":I
} // :cond_1
int v1 = -1; // const/4 v1, -0x1
/* .restart local v1 # "newRotation":I */
/* .line 147 */
} // .end local v1 # "newRotation":I
} // :cond_2
int v1 = -1; // const/4 v1, -0x1
/* .line 149 */
/* .restart local v1 # "newRotation":I */
} // :goto_0
this.mLastPackageName = v0;
/* .line 150 */
/* sget-boolean v2, Lcom/android/server/wm/OrientationSensorJudgeImpl;->LOG:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 151 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "finalizeRotation: reportedRotation = "; // const-string v3, "finalizeRotation: reportedRotation = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " mDesiredRotation = "; // const-string v3, " mDesiredRotation = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " newRotation = "; // const-string v3, " newRotation = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " mLastPackageName = "; // const-string v3, " mLastPackageName = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLastPackageName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "OrientationSensorJudgeImpl"; // const-string v3, "OrientationSensorJudgeImpl"
android.util.Slog .d ( v3,v2 );
/* .line 155 */
} // :cond_3
} // .end method
public void initialize ( android.content.Context p0, android.os.Looper p1, com.android.server.wm.WindowOrientationListener$OrientationSensorJudge p2 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .param p3, "judge" # Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge; */
/* .line 47 */
/* sget-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mSupportUIOrientationV2:Z */
/* if-nez v0, :cond_0 */
/* .line 48 */
return;
/* .line 50 */
} // :cond_0
/* new-instance v0, Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler; */
/* invoke-direct {v0, p0, p2}, Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler;-><init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 51 */
android.app.ActivityTaskManager .getService ( );
this.mActivityTaskManager = v0;
/* .line 52 */
/* const-class v0, Lcom/android/server/wm/ActivityTaskManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/ActivityTaskManagerInternal; */
this.mActivityTaskManagerInternal = v0;
/* .line 53 */
this.mOrientationSensorJudge = p3;
/* .line 54 */
/* new-instance v0, Lcom/android/server/wm/OrientationSensorJudgeImpl$TaskStackListenerImpl; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl$TaskStackListenerImpl;-><init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V */
this.mTaskStackListener = v0;
/* .line 55 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/OrientationSensorJudgeImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 58 */
return;
} // .end method
public Boolean isSupportUIOrientationV2 ( ) {
/* .locals 1 */
/* .line 181 */
/* sget-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mSupportUIOrientationV2:Z */
} // .end method
public void updateDesiredRotation ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "desiredRotation" # I */
/* .line 173 */
/* iput p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I */
/* .line 174 */
return;
} // .end method
