.class Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$4;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendHelper.java"

# interfaces
.implements Ljava/util/function/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->removeTimeOutTasks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

.field final synthetic val$switchTime:J


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 453
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$4;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iput-wide p2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$4;->val$switchTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public test(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .line 456
    iget-wide v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$4;->val$switchTime:J

    move-object v2, p1

    check-cast v2, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    invoke-virtual {v2}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getSwitchTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$4;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->getMaxTimeFrame()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
