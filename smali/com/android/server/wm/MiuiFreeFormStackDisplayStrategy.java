public class com.android.server.wm.MiuiFreeFormStackDisplayStrategy {
	 /* .source "MiuiFreeFormStackDisplayStrategy.java" */
	 /* # instance fields */
	 private final java.lang.String TAG;
	 private Integer mDefaultMaxFreeformCount;
	 private Integer mDefaultMaxGameFreeformCount;
	 private com.android.server.wm.MiuiFreeFormManagerService mFreeFormManagerService;
	 /* # direct methods */
	 public com.android.server.wm.MiuiFreeFormStackDisplayStrategy ( ) {
		 /* .locals 1 */
		 /* .param p1, "service" # Lcom/android/server/wm/MiuiFreeFormManagerService; */
		 /* .line 32 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 27 */
		 final String v0 = "MiuiFreeFormStackDisplayStrategy"; // const-string v0, "MiuiFreeFormStackDisplayStrategy"
		 this.TAG = v0;
		 /* .line 28 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* iput v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxFreeformCount:I */
		 /* .line 30 */
		 /* iput v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxGameFreeformCount:I */
		 /* .line 33 */
		 this.mFreeFormManagerService = p1;
		 /* .line 34 */
		 return;
	 } // .end method
	 private Boolean isInEmbeddedWindowingMode ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
		 /* .locals 4 */
		 /* .param p1, "stack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
		 /* .line 41 */
		 v0 = this.mTask;
		 (( com.android.server.wm.Task ) v0 ).getDisplayContent ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
		 (( com.android.server.wm.DisplayContent ) v0 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
		 /* .line 42 */
		 int v1 = 0; // const/4 v1, 0x0
		 int v2 = 1; // const/4 v2, 0x1
		 (( com.android.server.wm.TaskDisplayArea ) v0 ).getTopActivity ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/TaskDisplayArea;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
		 /* .line 43 */
		 /* .local v0, "topActivity":Lcom/android/server/wm/ActivityRecord; */
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 v2 = 			 (( com.android.server.wm.ActivityRecord ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
			 v3 = this.mTask;
			 v3 = 			 (( com.android.server.wm.Task ) v3 ).getRootTaskId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getRootTaskId()I
			 /* if-ne v2, v3, :cond_0 */
			 /* .line 46 */
		 } // :cond_0
		 /* nop */
		 /* .line 47 */
		 java.lang.Boolean .valueOf ( v1 );
		 /* filled-new-array {v2}, [Ljava/lang/Object; */
		 /* .line 46 */
		 final String v3 = "isActivityEmbedded"; // const-string v3, "isActivityEmbedded"
		 android.util.MiuiMultiWindowUtils .invoke ( v0,v3,v2 );
		 /* .line 48 */
		 /* .local v2, "activityEmbedded":Ljava/lang/Object; */
		 /* if-nez v2, :cond_1 */
	 } // :cond_1
	 /* move-object v1, v2 */
	 /* check-cast v1, Ljava/lang/Boolean; */
	 v1 = 	 (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
} // :goto_0
/* .line 44 */
} // .end local v2 # "activityEmbedded":Ljava/lang/Object;
} // :cond_2
} // :goto_1
} // .end method
private Boolean isSplitScreenMode ( ) {
/* .locals 1 */
/* .line 37 */
v0 = this.mFreeFormManagerService;
v0 = this.mActivityTaskManagerService;
v0 = (( com.android.server.wm.ActivityTaskManagerService ) v0 ).isInSplitScreenWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z
} // .end method
/* # virtual methods */
public Integer getMaxMiuiFreeFormStackCount ( java.lang.String p0, com.android.server.wm.MiuiFreeFormActivityStack p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "stack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 144 */
v0 = this.mFreeFormManagerService;
v0 = this.mActivityTaskManagerService;
v0 = this.mContext;
v0 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 145 */
int v0 = 4; // const/4 v0, 0x4
/* .line 147 */
} // :cond_0
v0 = this.mFreeFormManagerService;
v0 = this.mActivityTaskManagerService;
v0 = this.mContext;
v0 = android.util.MiuiMultiWindowUtils .multiFreeFormSupported ( v0 );
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
/* .line 148 */
/* .line 150 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* if-nez p2, :cond_2 */
/* .line 151 */
} // :cond_2
/* .line 152 */
/* .local v2, "totalMemory":I */
int v3 = 7; // const/4 v3, 0x7
/* if-lt v2, v3, :cond_5 */
/* .line 153 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->isInEmbeddedWindowingMode(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z */
int v1 = 2; // const/4 v1, 0x2
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 154 */
/* .line 156 */
} // :cond_3
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->isSplitScreenMode()Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 157 */
/* .line 159 */
} // :cond_4
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxFreeformCount:I */
/* .line 160 */
} // :cond_5
int v3 = 5; // const/4 v3, 0x5
/* if-lt v2, v3, :cond_8 */
/* .line 161 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->isInEmbeddedWindowingMode(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 162 */
/* .line 164 */
} // :cond_6
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->isSplitScreenMode()Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 165 */
/* .line 167 */
} // :cond_7
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxFreeformCount:I */
/* .line 168 */
} // :cond_8
int v3 = 3; // const/4 v3, 0x3
/* if-lt v2, v3, :cond_9 */
/* .line 169 */
/* .line 171 */
} // :cond_9
} // .end method
public void onMiuiFreeFormStasckAdded ( java.util.concurrent.ConcurrentHashMap p0, com.android.server.wm.MiuiFreeFormGestureController p1, com.android.server.wm.MiuiFreeFormActivityStack p2 ) {
/* .locals 10 */
/* .param p2, "miuiFreeFormGestureController" # Lcom/android/server/wm/MiuiFreeFormGestureController; */
/* .param p3, "addingStack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/wm/MiuiFreeFormActivityStack;", */
/* ">;", */
/* "Lcom/android/server/wm/MiuiFreeFormGestureController;", */
/* "Lcom/android/server/wm/MiuiFreeFormActivityStack;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 53 */
/* .local p1, "freeFormActivityStacks":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;" */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p3 ).getStackPackageName ( ); // invoke-virtual {p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_a
v0 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHasHadStackAdded:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_4 */
/* .line 54 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHasHadStackAdded:Z */
/* .line 55 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p3 ).getStackPackageName ( ); // invoke-virtual {p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v1 = android.util.MiuiMultiWindowAdapter .isInTopGameList ( v1 );
/* .line 56 */
/* .local v1, "isAddingTopGame":Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 57 */
/* .local v2, "log":Ljava/lang/StringBuilder; */
final String v3 = "onMiuiFreeFormStasckAdded isAddingTopGame= "; // const-string v3, "onMiuiFreeFormStasckAdded isAddingTopGame= "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 58 */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 59 */
final String v3 = " addingStack= "; // const-string v3, " addingStack= "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 60 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p3 ).getStackPackageName ( ); // invoke-virtual {p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 61 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiFreeFormStackDisplayStrategy"; // const-string v4, "MiuiFreeFormStackDisplayStrategy"
com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,v4,v3 );
/* .line 62 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 63 */
v3 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
/* if-nez v3, :cond_3 */
/* .line 64 */
(( java.util.concurrent.ConcurrentHashMap ) p1 ).keySet ( ); // invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/lang/Integer; */
/* .line 65 */
/* .local v5, "taskId":Ljava/lang/Integer; */
(( java.util.concurrent.ConcurrentHashMap ) p1 ).get ( v5 ); // invoke-virtual {p1, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 66 */
/* .local v6, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
v7 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
v8 = this.mTask;
v8 = (( com.android.server.wm.Task ) v8 ).getRootTaskId ( ); // invoke-virtual {v8}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* if-eq v7, v8, :cond_1 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v6 ).getStackPackageName ( ); // invoke-virtual {v6}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v7 = android.util.MiuiMultiWindowAdapter .isInTopGameList ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 67 */
(( com.android.server.wm.MiuiFreeFormGestureController ) p2 ).startExitApplication ( v6 ); // invoke-virtual {p2, v6}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 68 */
final String v3 = "onMiuiFreeFormStasckAdded Max TOP GAME FreeForm Window Num reached!"; // const-string v3, "onMiuiFreeFormStasckAdded Max TOP GAME FreeForm Window Num reached!"
com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,v4,v3 );
/* .line 69 */
return;
/* .line 71 */
} // .end local v5 # "taskId":Ljava/lang/Integer;
} // .end local v6 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_1
} // :cond_2
/* .line 73 */
} // :cond_3
v3 = this.mFreeFormManagerService;
v3 = (( com.android.server.wm.MiuiFreeFormManagerService ) v3 ).getGameFreeFormCount ( p3 ); // invoke-virtual {v3, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getGameFreeFormCount(Lcom/android/server/wm/MiuiFreeFormActivityStack;)I
/* iget v5, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxGameFreeformCount:I */
/* if-lt v3, v5, :cond_4 */
/* .line 74 */
v3 = this.mFreeFormManagerService;
(( com.android.server.wm.MiuiFreeFormManagerService ) v3 ).getBottomGameFreeFormActivityStack ( p3 ); // invoke-virtual {v3, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getBottomGameFreeFormActivityStack(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 75 */
/* .local v3, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 76 */
(( com.android.server.wm.MiuiFreeFormGestureController ) p2 ).startExitApplication ( v3 ); // invoke-virtual {p2, v3}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 77 */
final String v5 = "MiuiDesktopModeStatus isActive, onMiuiFreeFormStasckAdded Max TOP GAME FreeForm Window Num reached!"; // const-string v5, "MiuiDesktopModeStatus isActive, onMiuiFreeFormStasckAdded Max TOP GAME FreeForm Window Num reached!"
com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,v4,v5 );
/* .line 83 */
} // .end local v3 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_4
} // :goto_1
(( com.android.server.wm.MiuiFreeFormActivityStack ) p3 ).getStackPackageName ( ); // invoke-virtual {p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v3 = (( com.android.server.wm.MiuiFreeFormStackDisplayStrategy ) p0 ).getMaxMiuiFreeFormStackCount ( v3, p3 ); // invoke-virtual {p0, v3, p3}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->getMaxMiuiFreeFormStackCount(Ljava/lang/String;Lcom/android/server/wm/MiuiFreeFormActivityStack;)I
/* .line 84 */
/* .local v3, "maxStackCount":I */
v5 = (( java.util.concurrent.ConcurrentHashMap ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->size()I
/* .line 85 */
/* .local v5, "size":I */
int v6 = 0; // const/4 v6, 0x0
v7 = (( java.lang.StringBuilder ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I
(( java.lang.StringBuilder ) v2 ).delete ( v6, v7 ); // invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;
/* .line 86 */
final String v6 = "onMiuiFreeFormStasckAdded size = "; // const-string v6, "onMiuiFreeFormStasckAdded size = "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 87 */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 88 */
final String v6 = " getMaxMiuiFreeFormStackCount() = "; // const-string v6, " getMaxMiuiFreeFormStackCount() = "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 89 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 90 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,v4,v6 );
/* .line 91 */
/* if-le v5, v3, :cond_9 */
/* .line 92 */
v6 = this.mFreeFormManagerService;
(( com.android.server.wm.MiuiFreeFormManagerService ) v6 ).getReplaceFreeForm ( p3 ); // invoke-virtual {v6, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getReplaceFreeForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 93 */
/* .restart local v6 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 94 */
v7 = this.mTask;
if ( v7 != null) { // if-eqz v7, :cond_5
v7 = this.mTask;
(( com.android.server.wm.Task ) v7 ).getResumedActivity ( ); // invoke-virtual {v7}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
if ( v7 != null) { // if-eqz v7, :cond_5
if ( p3 != null) { // if-eqz p3, :cond_5
v7 = this.mTask;
if ( v7 != null) { // if-eqz v7, :cond_5
v7 = this.mTask;
v7 = this.intent;
if ( v7 != null) { // if-eqz v7, :cond_5
v7 = this.mTask;
v7 = this.intent;
/* .line 96 */
(( android.content.Intent ) v7 ).getComponent ( ); // invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v7 != null) { // if-eqz v7, :cond_5
v7 = android.util.MiuiMultiWindowAdapter.sNotExitFreeFormWhenAddOtherFreeFormTask;
v8 = this.mTask;
/* .line 97 */
(( com.android.server.wm.Task ) v8 ).getResumedActivity ( ); // invoke-virtual {v8}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
v8 = this.shortComponentName;
if ( v7 != null) { // if-eqz v7, :cond_5
v7 = android.util.MiuiMultiWindowAdapter.sNotExitFreeFormWhenAddOtherFreeFormTask;
v8 = this.mTask;
/* .line 98 */
(( com.android.server.wm.Task ) v8 ).getResumedActivity ( ); // invoke-virtual {v8}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
v8 = this.shortComponentName;
/* check-cast v7, Ljava/lang/String; */
v8 = this.mTask;
v8 = this.intent;
/* .line 99 */
(( android.content.Intent ) v8 ).getComponent ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v8 ).flattenToShortString ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v7 = (( java.lang.String ) v7 ).equals ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 100 */
return;
/* .line 102 */
} // :cond_5
v7 = this.mTask;
if ( v7 != null) { // if-eqz v7, :cond_6
v7 = this.mTask;
(( com.android.server.wm.Task ) v7 ).getResumedActivity ( ); // invoke-virtual {v7}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
if ( v7 != null) { // if-eqz v7, :cond_6
v7 = this.mTask;
(( com.android.server.wm.Task ) v7 ).getResumedActivity ( ); // invoke-virtual {v7}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
v7 = this.intent;
if ( v7 != null) { // if-eqz v7, :cond_6
v7 = this.mTask;
/* .line 103 */
(( com.android.server.wm.Task ) v7 ).getResumedActivity ( ); // invoke-virtual {v7}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
v7 = this.intent;
(( android.content.Intent ) v7 ).getComponent ( ); // invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v7 != null) { // if-eqz v7, :cond_6
v7 = this.mTask;
if ( v7 != null) { // if-eqz v7, :cond_6
v7 = this.mTask;
v7 = this.intent;
if ( v7 != null) { // if-eqz v7, :cond_6
	 v7 = this.mTask;
	 v7 = this.intent;
	 /* .line 104 */
	 (( android.content.Intent ) v7 ).getComponent ( ); // invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
	 if ( v7 != null) { // if-eqz v7, :cond_6
		 v7 = android.util.MiuiMultiWindowAdapter.HIDE_SELF_IF_NEW_FREEFORM_TASK_WHITE_LIST_ACTIVITY;
		 v8 = this.mTask;
		 /* .line 106 */
		 (( com.android.server.wm.Task ) v8 ).getResumedActivity ( ); // invoke-virtual {v8}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
		 v8 = this.intent;
		 (( android.content.Intent ) v8 ).getComponent ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
		 v7 = 		 (( android.content.ComponentName ) v8 ).getClassName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
		 if ( v7 != null) { // if-eqz v7, :cond_6
			 v7 = android.util.MiuiMultiWindowAdapter.SHOW_HIDDEN_TASK_IF_FINISHED_WHITE_LIST_ACTIVITY;
			 v8 = this.mTask;
			 v8 = this.intent;
			 /* .line 108 */
			 (( android.content.Intent ) v8 ).getComponent ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
			 v7 = 			 (( android.content.ComponentName ) v8 ).getClassName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
			 if ( v7 != null) { // if-eqz v7, :cond_6
				 /* .line 109 */
				 final String v7 = "onMiuiFreeFormStasckAdded before startExitApplication "; // const-string v7, "onMiuiFreeFormStasckAdded before startExitApplication "
				 com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,v4,v7 );
				 /* .line 110 */
				 return;
				 /* .line 112 */
			 } // :cond_6
			 v7 = this.mFreeFormManagerService;
			 v7 = this.mActivityTaskManagerService;
			 v7 = this.mContext;
			 v7 = 			 com.android.server.wm.MiuiDesktopModeUtils .isActive ( v7 );
			 final String v8 = "onMiuiFreeFormStasckAdded Max FreeForm Window Num reached! exit mffas\uff1a "; // const-string v8, "onMiuiFreeFormStasckAdded Max FreeForm Window Num reached! exit mffas\uff1a "
			 if ( v7 != null) { // if-eqz v7, :cond_8
				 /* .line 113 */
				 v7 = this.mFreeFormManagerService;
				 v7 = 				 (( com.android.server.wm.MiuiFreeFormManagerService ) v7 ).getFrontFreeformNum ( p3 ); // invoke-virtual {v7, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFrontFreeformNum(Lcom/android/server/wm/MiuiFreeFormActivityStack;)I
				 /* .line 114 */
				 /* .local v7, "frontSize":I */
				 /* if-lt v7, v3, :cond_7 */
				 /* .line 115 */
				 (( com.android.server.wm.MiuiFreeFormGestureController ) p2 ).startExitApplication ( v6 ); // invoke-virtual {p2, v6}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
				 /* .line 116 */
				 /* new-instance v9, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
				 (( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,v4,v8 );
				 /* .line 119 */
			 } // :cond_7
			 /* new-instance v8, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v9 = "onMiuiFreeFormStasckAdded frontSize\uff1a "; // const-string v9, "onMiuiFreeFormStasckAdded frontSize\uff1a "
			 (( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 final String v9 = ", maxStackCount="; // const-string v9, ", maxStackCount="
			 (( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,v4,v8 );
			 /* .line 121 */
		 } // .end local v7 # "frontSize":I
	 } // :goto_2
	 /* .line 122 */
} // :cond_8
(( com.android.server.wm.MiuiFreeFormGestureController ) p2 ).startExitApplication ( v6 ); // invoke-virtual {p2, v6}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 123 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,v4,v7 );
/* .line 129 */
} // .end local v6 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_9
} // :goto_3
return;
/* .line 53 */
} // .end local v1 # "isAddingTopGame":Z
} // .end local v2 # "log":Ljava/lang/StringBuilder;
} // .end local v3 # "maxStackCount":I
} // .end local v5 # "size":I
} // :cond_a
} // :goto_4
return;
} // .end method
