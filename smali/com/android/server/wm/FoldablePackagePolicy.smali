.class Lcom/android/server/wm/FoldablePackagePolicy;
.super Lcom/android/server/wm/PolicyImpl;
.source "FoldablePackagePolicy.java"


# static fields
.field private static final ACTIVITY_NAME:Ljava/lang/String; = "activity"

.field private static final ACTIVITY_NAME_PRINT_WRITER:Ljava/lang/String; = "activity:activity:..."

.field private static final APP_CONTINUITY_BLACK_LIST_KEY:Ljava/lang/String; = "app_continuity_blacklist"

.field private static final APP_CONTINUITY_ID_KEY:Ljava/lang/String; = "id"

.field private static final APP_CONTINUITY_WHILTE_LIST_KEY:Ljava/lang/String; = "app_continuity_whitelist"

.field private static final APP_INTERCEPT_ALLOWLIST_KEY:Ljava/lang/String; = "app_intercept_allowlist"

.field private static final APP_INTERCEPT_BLACKLIST_KEY:Ljava/lang/String; = "app_intercept_blacklist"

.field private static final APP_INTERCEPT_COMPONENT_ALLOWLIST_KEY:Ljava/lang/String; = "app_intercept_component_allowlist"

.field private static final APP_INTERCEPT_COMPONENT_BLACKLIST_KEY:Ljava/lang/String; = "app_intercept_component_blacklist"

.field private static final APP_RELAUNCH_BLACKLIST_KEY:Ljava/lang/String; = "app_relaunch_blacklist"

.field private static final APP_RESTART_BLACKLIST_KEY:Ljava/lang/String; = "app_restart_blacklist"

.field private static final CALLBACK_NAME_DISPLAY_COMPAT:Ljava/lang/String; = "displayCompat"

.field private static final CALLBACK_NAME_FULLSCREEN:Ljava/lang/String; = "fullscreenpackage"

.field private static final CALLBACK_NAME_FULLSCREEN_COMPONENT:Ljava/lang/String; = "fullscreencomponent"

.field private static final COMMAND_OPTION_ALLOW_LIST:Ljava/lang/String; = "allowlist"

.field private static final COMMAND_OPTION_BLOCK_LIST:Ljava/lang/String; = "blocklist"

.field private static final COMMAND_OPTION_INTERCEPT_COMPONENT_LIST:Ljava/lang/String; = "interceptcomlist"

.field private static final COMMAND_OPTION_INTERCEPT_LIST:Ljava/lang/String; = "interceptlist"

.field private static final COMMAND_OPTION_RELAUNCH_LIST:Ljava/lang/String; = "relaunchlist"

.field private static final COMMAND_OPTION_RESTART_LIST:Ljava/lang/String; = "restartlist"

.field private static final DEFAULT_CONTINUITY_VERSION:I = 0x124f26

.field private static final FORCE_RESIZABLE_ACTIVITIES:Ljava/lang/String; = "force_resizable_activities"

.field private static final FULLSCREEN_DEMO_MODE:Ljava/lang/String; = "demoMode"

.field public static final MIUI_SMART_ROTATION_ACTIVITY_KEY:Ljava/lang/String; = "fullscreen_activity_list"

.field public static final MIUI_SMART_ROTATION_FULLSCREEN_KEY:Ljava/lang/String; = "fullscreen_landscape_list"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field private static final PACKAGE_NAME_PRINT_WRITER:Ljava/lang/String; = "packageName:packageName:..."

.field private static final POLICY_NAME:Ljava/lang/String; = "FoldablePackagePolicy"

.field public static final POLICY_VALUE_ALLOW_LIST:Ljava/lang/String; = "w"

.field public static final POLICY_VALUE_BLOCK_LIST:Ljava/lang/String; = "b"

.field public static final POLICY_VALUE_INTERCEPT_ALLOW_LIST:Ljava/lang/String; = "ia"

.field public static final POLICY_VALUE_INTERCEPT_COMPONENT_ALLOW_LIST:Ljava/lang/String; = "ica"

.field public static final POLICY_VALUE_INTERCEPT_COMPONENT_LIST:Ljava/lang/String; = "ic"

.field public static final POLICY_VALUE_INTERCEPT_LIST:Ljava/lang/String; = "i"

.field public static final POLICY_VALUE_RELAUNCH_LIST:Ljava/lang/String; = "c"

.field public static final POLICY_VALUE_RESTART_LIST:Ljava/lang/String; = "r"

.field private static final PULL_APP_FLAG_COMMAND:Ljava/lang/String; = "-pullAppFlag"

.field private static final PULL_CLOUD_DATA_MODULE_COMMAND:Ljava/lang/String; = "-pullCloudDataModule"

.field private static final SET_FIXED_ASPECT_RATIO:Ljava/lang/String; = "setFixedAspectRatio"

.field private static final SET_FIXED_ASPECT_RATIO_COMMAND:Ljava/lang/String; = "-setFixedAspectRatio"

.field private static final SET_FORCE_DISPLAY_COMPAT_COMMAND:Ljava/lang/String; = "-setForceDisplayCompatMode"

.field private static final SET_FULLSCREEN_COMPONENT_COMMAND:Ljava/lang/String; = "-setFullscreenActivity"

.field private static final SET_FULLSCREEN_LANDSCAPE_COMMAND:Ljava/lang/String; = "-setFullscreenPackage"

.field private static final SET_FULLSCREEN_LANDSCAPE_COMMAND_OLD:Ljava/lang/String; = "-setFullscreenlandscape"

.field private static final SET_FULLSCREEN_USER_COMMAND:Ljava/lang/String; = "-setFullscreenUser"

.field private static final TAG:Ljava/lang/String; = "FoldablePackagePolicy"


# instance fields
.field private final mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

.field private final mCompatModeModuleName:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mContinuityModuleName:Ljava/lang/String;

.field private mContinuityVersion:J

.field private mDevelopmentForceResizable:Z

.field private final mDisplayCompatAllowCloudAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayCompatBlockCloudAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayInterceptCloudAppAllowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayInterceptCloudAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayInterceptComponentCloudAppAllowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayInterceptComponentCloudAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayRelaunchCloudAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayRestartCloudAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mFullScreenActivityCloudList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mFullScreenPackageCloudAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mResources:Landroid/content/res/Resources;

.field private final mSmartRotationNModuleName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V
    .locals 2
    .param p1, "atmServiceImpl"    # Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    .line 125
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageConfigurationController:Lcom/android/server/wm/PackageConfigurationController;

    const-string v1, "FoldablePackagePolicy"

    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/PolicyImpl;-><init>(Lcom/android/server/wm/PackageConfigurationController;Ljava/lang/String;)V

    .line 78
    const-wide/32 v0, 0x124f26

    iput-wide v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityVersion:J

    .line 127
    iput-object p1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    .line 128
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    .line 129
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mResources:Landroid/content/res/Resources;

    .line 133
    const v1, 0x110f00b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    .line 134
    const v1, 0x110f0359

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mCompatModeModuleName:Ljava/lang/String;

    .line 135
    const v1, 0x110f036d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mSmartRotationNModuleName:Ljava/lang/String;

    .line 137
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFoldScreenDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageSettingsManager:Lcom/android/server/wm/PackageSettingsManager;

    iget-object v0, v0, Lcom/android/server/wm/PackageSettingsManager;->mDisplayCompatPackages:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    iget-object v0, v0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mCallback:Ljava/util/function/Consumer;

    const-string v1, "displayCompat"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/FoldablePackagePolicy;->registerCallback(Ljava/lang/String;Ljava/util/function/Consumer;)V

    .line 145
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v0, v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->mFullScreenPackagelistCallback:Ljava/util/function/Consumer;

    const-string v1, "fullscreenpackage"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/FoldablePackagePolicy;->registerCallback(Ljava/lang/String;Ljava/util/function/Consumer;)V

    .line 146
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v0, v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->mFullScreenComponentCallback:Ljava/util/function/Consumer;

    const-string v1, "fullscreencomponent"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/FoldablePackagePolicy;->registerCallback(Ljava/lang/String;Ljava/util/function/Consumer;)V

    .line 151
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatAllowCloudAppList:Ljava/util/List;

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatBlockCloudAppList:Ljava/util/List;

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRestartCloudAppList:Ljava/util/List;

    .line 154
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppList:Ljava/util/List;

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppList:Ljava/util/List;

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppAllowList:Ljava/util/List;

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppAllowList:Ljava/util/List;

    .line 158
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRelaunchCloudAppList:Ljava/util/List;

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mFullScreenPackageCloudAppList:Ljava/util/List;

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mFullScreenActivityCloudList:Ljava/util/List;

    .line 164
    return-void
.end method

.method private executeShellCommandLockedForSmartRotation(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 11
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 265
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationStub;->get()Lcom/android/server/wm/MiuiOrientationStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/MiuiOrientationStub;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    const-string v0, " miui smart rotation is not enabled."

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 267
    return-void

    .line 269
    :cond_0
    const-string v0, ""

    .line 270
    .local v0, "str":Ljava/lang/String;
    array-length v1, p2

    const-string v2, "demoMode"

    const/4 v3, 0x2

    const-string v4, "-setFullscreenUser"

    const-string v5, "-setFullscreenActivity"

    const/4 v6, 0x1

    const-string v7, "-setFullscreenPackage"

    if-eq v1, v6, :cond_1

    array-length v1, p2

    if-ne v1, v3, :cond_2

    :cond_1
    const/4 v1, 0x0

    aget-object v8, p2, v1

    if-nez v8, :cond_3

    .line 271
    :cond_2
    const-string v1, "miui smart rotation(package)"

    invoke-virtual {p0, p3, v1}, Lcom/android/server/wm/FoldablePackagePolicy;->printOptionsRequires(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 272
    const-string v1, "policy(optional)"

    filled-new-array {v2, v1}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p3, v7, v2}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 273
    const-string v2, "packageName"

    filled-new-array {v2, v1}, [Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p3, v7, v3}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 274
    const-string v3, "--remove"

    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p3, v7, v6}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 275
    const-string v6, "packageName:packageName:..."

    filled-new-array {v6, v1}, [Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, p3, v7, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 276
    const-string v7, " or "

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 277
    const-string v8, "miui smart rotation(activity)"

    invoke-virtual {p0, p3, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->printOptionsRequires(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 278
    const-string v8, "activity"

    const-string v9, "policy(forced)"

    filled-new-array {v8, v9}, [Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, p3, v5, v10}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 279
    filled-new-array {v8, v3}, [Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, p3, v5, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 280
    const-string v8, "activity:activity:..."

    filled-new-array {v8, v9}, [Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, p3, v5, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 281
    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 282
    const-string v5, "miui smart rotation(user)"

    invoke-virtual {p0, p3, v5}, Lcom/android/server/wm/FoldablePackagePolicy;->printOptionsRequires(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 283
    filled-new-array {v2, v1}, [Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p3, v4, v5}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 284
    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p3, v4, v2}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 285
    filled-new-array {v6, v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p3, v4, v1}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V

    .line 286
    const-string/jumbo v1, "supported policy options is nr:r:ri:t:w:b:nra:nrs"

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 287
    const-string v1, "please refer https://xiaomi.f.mioffice.cn/docs/dock4JoODrG0prOdqdZ14fWZ17c for more information"

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 289
    :cond_3
    array-length v8, p2

    if-ne v8, v3, :cond_4

    aget-object v8, p2, v6

    if-eqz v8, :cond_4

    .line 290
    aget-object v0, p2, v6

    .line 292
    :cond_4
    aget-object v8, p2, v1

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 298
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationImpl;->getInstance()Lcom/android/server/wm/MiuiOrientationImpl;

    move-result-object v1

    invoke-virtual {v1, p3, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->gotoDemoDebugMode(Ljava/io/PrintWriter;Ljava/lang/String;)V

    goto :goto_2

    .line 301
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_6
    goto :goto_0

    :sswitch_0
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v3, 0x3

    goto :goto_1

    :sswitch_1
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    goto :goto_1

    :sswitch_2
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v3, v1

    goto :goto_1

    :sswitch_3
    const-string v2, "-setFullscreenlandscape"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v3, v6

    goto :goto_1

    :goto_0
    const/4 v3, -0x1

    :goto_1
    const-string v2, ":"

    packed-switch v3, :pswitch_data_0

    goto :goto_2

    .line 314
    :pswitch_0
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationImpl;->getInstance()Lcom/android/server/wm/MiuiOrientationImpl;

    move-result-object v3

    aget-object v1, p2, v1

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, p3, v1, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->setUserSettings(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    goto :goto_2

    .line 307
    :pswitch_1
    aget-object v1, p2, v1

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v2, "fullscreencomponent"

    invoke-virtual {p0, p3, v1, v0, v2}, Lcom/android/server/wm/FoldablePackagePolicy;->executeDebugModeLocked(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    goto :goto_2

    .line 304
    :pswitch_2
    aget-object v1, p2, v1

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v2, "fullscreenpackage"

    invoke-virtual {p0, p3, v1, v0, v2}, Lcom/android/server/wm/FoldablePackagePolicy;->executeDebugModeLocked(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    nop

    .line 323
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x543cf9f5 -> :sswitch_3
        -0xa988c8a -> :sswitch_2
        0x281199df -> :sswitch_1
        0x3b007a9b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method executeShellCommandLocked(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
    .locals 7
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 227
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "-pullCloudDataModule"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_1
    const-string v0, "-setFullscreenUser"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_2
    const-string v0, "-setFullscreenActivity"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v0, "-setFullscreenPackage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_1

    :sswitch_4
    const-string v0, "-pullAppFlag"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_5
    const-string v0, "-setFullscreenlandscape"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 260
    return v1

    .line 241
    :pswitch_0
    sget-object v0, Landroid/appcompat/ApplicationCompatUtilsStub;->MIUI_SUPPORT_APP_CONTINUITY:Ljava/lang/String;

    aget-object v3, p2, v2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, " = "

    const-string v4, " not added!"

    if-nez v0, :cond_1

    aget-object v0, p2, v2

    .line 242
    const-string v5, "android.supports_size_changes"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    aget-object v5, p2, v1

    aget-object v6, p2, v2

    invoke-virtual {v0, v5, v6}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->hasMetaData(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    aget-object v5, p2, v1

    aget-object v6, p2, v2

    invoke-virtual {v0, v5, v6}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 245
    .local v0, "continuityFlag":Z
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, p2, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 246
    .end local v0    # "continuityFlag":Z
    goto :goto_2

    .line 247
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, p2, v2

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 251
    :cond_3
    :goto_2
    const-string v0, "android.max_aspect"

    aget-object v5, p2, v2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 252
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    aget-object v5, p2, v1

    aget-object v6, p2, v2

    invoke-virtual {v0, v5, v6}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->hasMetaData(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, p2, v2

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    aget-object v1, p2, v1

    aget-object v4, p2, v2

    invoke-virtual {v3, v1, v4}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataFloat(Ljava/lang/String;Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 255
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v1, p2, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 258
    :cond_5
    :goto_3
    return v2

    .line 236
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    aget-object v1, p2, v1

    aget-object v3, p2, v2

    invoke-virtual {p0, v0, v1, v3}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 237
    return v2

    .line 232
    :pswitch_2
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/FoldablePackagePolicy;->executeShellCommandLockedForSmartRotation(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 233
    return v2

    :sswitch_data_0
    .sparse-switch
        -0x543cf9f5 -> :sswitch_5
        -0x4869b725 -> :sswitch_4
        -0xa988c8a -> :sswitch_3
        0x281199df -> :sswitch_2
        0x3b007a9b -> :sswitch_1
        0x58969c79 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getContinuityList(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "policyName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 488
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    .line 492
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v0, "app_intercept_component_allowlist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_1
    const-string v0, "app_continuity_blacklist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v0, "app_intercept_blacklist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_3
    const-string v0, "app_restart_blacklist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_4
    const-string v0, "app_relaunch_blacklist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_5
    const-string v0, "app_intercept_allowlist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_6
    const-string v0, "app_continuity_whitelist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_7
    const-string v0, "app_intercept_component_blacklist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 510
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    .line 508
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppAllowList:Ljava/util/List;

    return-object v0

    .line 506
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppAllowList:Ljava/util/List;

    return-object v0

    .line 504
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRelaunchCloudAppList:Ljava/util/List;

    return-object v0

    .line 502
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppList:Ljava/util/List;

    return-object v0

    .line 500
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppList:Ljava/util/List;

    return-object v0

    .line 498
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRestartCloudAppList:Ljava/util/List;

    return-object v0

    .line 496
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatBlockCloudAppList:Ljava/util/List;

    return-object v0

    .line 494
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatAllowCloudAppList:Ljava/util/List;

    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x7487e440 -> :sswitch_7
        -0x71d7e56a -> :sswitch_6
        -0x2aa53af4 -> :sswitch_5
        -0x25be055e -> :sswitch_4
        -0x1b36c7b1 -> :sswitch_3
        0xe9766c2 -> :sswitch_2
        0x4030036c -> :sswitch_1
        0x523b7a0a -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method getContinuityVersion()J
    .locals 2

    .line 515
    iget-wide v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityVersion:J

    return-wide v0
.end method

.method getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "moduleName"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 168
    const-string v0, "FoldablePackagePolicy"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 169
    .local v1, "arrayList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    return-object v1

    .line 173
    :cond_0
    nop

    .line 174
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 173
    const/4 v3, 0x0

    invoke-static {v2, p2, p3, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 175
    .local v2, "data":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getListFromCloud: data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " moduleName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " key="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 178
    return-object v1

    .line 180
    :cond_1
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 181
    .local v3, "apps":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 182
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 184
    .end local v4    # "i":I
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getListFromCloud: mCloudList: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    nop

    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "apps":Lorg/json/JSONArray;
    goto :goto_1

    .line 185
    :catch_0
    move-exception v2

    .line 186
    .local v2, "e":Lorg/json/JSONException;
    const-string v3, "exception when getListFromCloud: "

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 188
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_1
    return-object v1
.end method

.method getLocalVersion()I
    .locals 2

    .line 327
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mResources:Landroid/content/res/Resources;

    const v1, 0x110b0032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method getMapFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "jsonObjectName"    # Ljava/lang/String;
    .param p4, "jsonArrayName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 196
    const-string v0, "FoldablePackagePolicy"

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 198
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    nop

    .line 199
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 198
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, p2, p2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v2

    .line 200
    .local v2, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMayFromCloud: data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 202
    .local v3, "activityList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v2, :cond_0

    .line 203
    return-object v1

    .line 205
    :cond_0
    invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v4, p2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 206
    .local v4, "apps":Lorg/json/JSONArray;
    if-nez v4, :cond_1

    .line 207
    return-object v1

    .line 209
    :cond_1
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_3

    .line 210
    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 211
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 212
    .local v6, "jsonObj":Lorg/json/JSONObject;
    invoke-virtual {v6, p4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 213
    .local v7, "array":Lorg/json/JSONArray;
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v8, v9, :cond_2

    .line 214
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 216
    .end local v8    # "j":I
    :cond_2
    invoke-virtual {v6, p3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v8, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    nop

    .end local v6    # "jsonObj":Lorg/json/JSONObject;
    .end local v7    # "array":Lorg/json/JSONArray;
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 218
    .end local v5    # "i":I
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getMayFromCloud: mCloudList: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    nop

    .end local v2    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v3    # "activityList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "apps":Lorg/json/JSONArray;
    goto :goto_2

    .line 219
    :catch_0
    move-exception v2

    .line 220
    .local v2, "e":Lorg/json/JSONException;
    const-string v3, "exception when getMayFromCloud: "

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 222
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_2
    return-object v1
.end method

.method getPolicyDataMapFromCloud(Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 10
    .param p1, "configurationName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 367
    .local p2, "outPolicyDataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "displayCompat"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    goto :goto_1

    :sswitch_1
    const-string v0, "fullscreencomponent"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_1

    :sswitch_2
    const-string v0, "fullscreenpackage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    const-string v4, ""

    const-string v5, ","

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_f

    .line 463
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mFullScreenActivityCloudList:Ljava/util/List;

    iget-object v6, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mSmartRotationNModuleName:Ljava/lang/String;

    .line 464
    const-string v8, "fullscreen_activity_list"

    invoke-virtual {p0, v6, v7, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 463
    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 465
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mFullScreenActivityCloudList:Ljava/util/List;

    if-eqz v0, :cond_d

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 466
    invoke-virtual {p2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 467
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mFullScreenActivityCloudList:Ljava/util/List;

    new-array v6, v3, [Ljava/lang/String;

    invoke-interface {v0, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 468
    .local v0, "packages":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "item":I
    :goto_2
    array-length v7, v0

    if-ge v6, v7, :cond_2

    .line 469
    aget-object v7, v0, v6

    invoke-virtual {v7, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 470
    .local v7, "split":[Ljava/lang/String;
    array-length v8, v7

    if-ne v8, v1, :cond_1

    aget-object v8, v7, v2

    goto :goto_3

    :cond_1
    move-object v8, v4

    .line 471
    .local v8, "str":Ljava/lang/String;
    :goto_3
    aget-object v9, v7, v3

    invoke-virtual {p2, v9, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    .end local v7    # "split":[Ljava/lang/String;
    .end local v8    # "str":Ljava/lang/String;
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 473
    .end local v0    # "packages":[Ljava/lang/String;
    .end local v6    # "item":I
    :cond_2
    goto/16 :goto_f

    .line 450
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mFullScreenPackageCloudAppList:Ljava/util/List;

    iget-object v6, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mSmartRotationNModuleName:Ljava/lang/String;

    .line 451
    const-string v8, "fullscreen_landscape_list"

    invoke-virtual {p0, v6, v7, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 450
    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 452
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mFullScreenPackageCloudAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 453
    invoke-virtual {p2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 454
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mFullScreenPackageCloudAppList:Ljava/util/List;

    new-array v6, v3, [Ljava/lang/String;

    invoke-interface {v0, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 455
    .restart local v0    # "packages":[Ljava/lang/String;
    const/4 v6, 0x0

    .restart local v6    # "item":I
    :goto_4
    array-length v7, v0

    if-ge v6, v7, :cond_4

    .line 456
    aget-object v7, v0, v6

    invoke-virtual {v7, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 457
    .restart local v7    # "split":[Ljava/lang/String;
    array-length v8, v7

    if-ne v8, v1, :cond_3

    aget-object v8, v7, v2

    goto :goto_5

    :cond_3
    move-object v8, v4

    .line 458
    .restart local v8    # "str":Ljava/lang/String;
    :goto_5
    aget-object v9, v7, v3

    invoke-virtual {p2, v9, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    .end local v7    # "split":[Ljava/lang/String;
    .end local v8    # "str":Ljava/lang/String;
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 460
    .end local v0    # "packages":[Ljava/lang/String;
    .end local v6    # "item":I
    :cond_4
    goto/16 :goto_f

    .line 371
    :pswitch_2
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    .line 372
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    const-string v2, "id"

    .line 371
    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataInt(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityVersion:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    goto :goto_6

    .line 373
    :catch_0
    move-exception v0

    .line 374
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FoldablePackagePolicy"

    const-string v2, "Something is wrong."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 377
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_6
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatAllowCloudAppList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    .line 378
    const-string v4, "app_continuity_whitelist"

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 377
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 379
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatAllowCloudAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 380
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatAllowCloudAppList:Ljava/util/List;

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 381
    .local v0, "allowPackages":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "item":I
    :goto_7
    array-length v2, v0

    if-ge v1, v2, :cond_5

    .line 382
    aget-object v2, v0, v1

    const-string/jumbo v4, "w"

    invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 386
    .end local v0    # "allowPackages":[Ljava/lang/String;
    .end local v1    # "item":I
    :cond_5
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatBlockCloudAppList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    .line 387
    const-string v4, "app_continuity_blacklist"

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 386
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 388
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatBlockCloudAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 389
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayCompatBlockCloudAppList:Ljava/util/List;

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 390
    .local v0, "blockPackages":[Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "item":I
    :goto_8
    array-length v2, v0

    if-ge v1, v2, :cond_6

    .line 391
    aget-object v2, v0, v1

    const-string v4, "b"

    invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 395
    .end local v0    # "blockPackages":[Ljava/lang/String;
    .end local v1    # "item":I
    :cond_6
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRestartCloudAppList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    .line 396
    const-string v4, "app_restart_blacklist"

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 395
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 397
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRestartCloudAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 398
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRestartCloudAppList:Ljava/util/List;

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 399
    .restart local v0    # "blockPackages":[Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "item":I
    :goto_9
    array-length v2, v0

    if-ge v1, v2, :cond_7

    .line 400
    aget-object v2, v0, v1

    const-string v4, "r"

    invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 404
    .end local v0    # "blockPackages":[Ljava/lang/String;
    .end local v1    # "item":I
    :cond_7
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    .line 405
    const-string v4, "app_intercept_blacklist"

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 404
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 406
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 407
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppList:Ljava/util/List;

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 408
    .restart local v0    # "blockPackages":[Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "item":I
    :goto_a
    array-length v2, v0

    if-ge v1, v2, :cond_8

    .line 409
    aget-object v2, v0, v1

    const-string v4, "i"

    invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 413
    .end local v0    # "blockPackages":[Ljava/lang/String;
    .end local v1    # "item":I
    :cond_8
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    .line 414
    const-string v4, "app_intercept_component_blacklist"

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 413
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 415
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 416
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppList:Ljava/util/List;

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 417
    .restart local v0    # "blockPackages":[Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "item":I
    :goto_b
    array-length v2, v0

    if-ge v1, v2, :cond_9

    .line 418
    aget-object v2, v0, v1

    const-string v4, "ic"

    invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 422
    .end local v0    # "blockPackages":[Ljava/lang/String;
    .end local v1    # "item":I
    :cond_9
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppAllowList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    .line 423
    const-string v4, "app_intercept_allowlist"

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 422
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 424
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppAllowList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 425
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptCloudAppAllowList:Ljava/util/List;

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 426
    .restart local v0    # "blockPackages":[Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "item":I
    :goto_c
    array-length v2, v0

    if-ge v1, v2, :cond_a

    .line 427
    aget-object v2, v0, v1

    const-string v4, "ia"

    invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 431
    .end local v0    # "blockPackages":[Ljava/lang/String;
    .end local v1    # "item":I
    :cond_a
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppAllowList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    .line 432
    const-string v4, "app_intercept_component_allowlist"

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 431
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 433
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppAllowList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 434
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayInterceptComponentCloudAppAllowList:Ljava/util/List;

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 435
    .restart local v0    # "blockPackages":[Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "item":I
    :goto_d
    array-length v2, v0

    if-ge v1, v2, :cond_b

    .line 436
    aget-object v2, v0, v1

    const-string v4, "ica"

    invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 440
    .end local v0    # "blockPackages":[Ljava/lang/String;
    .end local v1    # "item":I
    :cond_b
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRelaunchCloudAppList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityModuleName:Ljava/lang/String;

    .line 441
    const-string v4, "app_relaunch_blacklist"

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 440
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 442
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRelaunchCloudAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 443
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mDisplayRelaunchCloudAppList:Ljava/util/List;

    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 444
    .restart local v0    # "blockPackages":[Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "item":I
    :goto_e
    array-length v2, v0

    if-ge v1, v2, :cond_c

    .line 445
    aget-object v2, v0, v1

    const-string v3, "c"

    invoke-virtual {p2, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 447
    .end local v0    # "blockPackages":[Ljava/lang/String;
    .end local v1    # "item":I
    :cond_c
    nop

    .line 479
    :cond_d
    :goto_f
    return-void

    :sswitch_data_0
    .sparse-switch
        0x2c81202b -> :sswitch_2
        0x51dfa3a2 -> :sswitch_1
        0x78901ee4 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method getPolicyDataMapFromLocal(Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 10
    .param p1, "configurationName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 332
    .local p2, "outPolicyDataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "displayCompat"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v0, "fullscreencomponent"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string v0, "fullscreenpackage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    const-string v4, ""

    const-string v5, ","

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_8

    .line 352
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mResources:Landroid/content/res/Resources;

    const v6, 0x1103007d

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 353
    .local v0, "packages":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    array-length v7, v0

    if-ge v6, v7, :cond_2

    .line 354
    aget-object v7, v0, v6

    invoke-virtual {v7, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 355
    .local v7, "split":[Ljava/lang/String;
    array-length v8, v7

    if-ne v8, v2, :cond_1

    aget-object v8, v7, v3

    goto :goto_3

    :cond_1
    move-object v8, v4

    .line 356
    .local v8, "str":Ljava/lang/String;
    :goto_3
    aget-object v9, v7, v1

    invoke-virtual {p2, v9, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    .end local v7    # "split":[Ljava/lang/String;
    .end local v8    # "str":Ljava/lang/String;
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 358
    .end local v6    # "i":I
    :cond_2
    goto :goto_8

    .line 344
    .end local v0    # "packages":[Ljava/lang/String;
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mResources:Landroid/content/res/Resources;

    const v6, 0x1103007e

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 345
    .local v0, "landscapePackages":[Ljava/lang/String;
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_4
    array-length v7, v0

    if-ge v6, v7, :cond_4

    .line 346
    aget-object v7, v0, v6

    invoke-virtual {v7, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 347
    .restart local v7    # "split":[Ljava/lang/String;
    array-length v8, v7

    if-ne v8, v2, :cond_3

    aget-object v8, v7, v3

    goto :goto_5

    :cond_3
    move-object v8, v4

    .line 348
    .restart local v8    # "str":Ljava/lang/String;
    :goto_5
    aget-object v9, v7, v1

    invoke-virtual {p2, v9, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    .end local v7    # "split":[Ljava/lang/String;
    .end local v8    # "str":Ljava/lang/String;
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 350
    .end local v6    # "i":I
    :cond_4
    goto :goto_8

    .line 334
    .end local v0    # "landscapePackages":[Ljava/lang/String;
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mResources:Landroid/content/res/Resources;

    const v1, 0x11030069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 335
    .local v0, "allowPackages":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "item":I
    :goto_6
    array-length v2, v0

    if-ge v1, v2, :cond_5

    .line 336
    aget-object v2, v0, v1

    const-string/jumbo v3, "w"

    invoke-virtual {p2, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 338
    .end local v1    # "item":I
    :cond_5
    iget-object v1, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mResources:Landroid/content/res/Resources;

    const v2, 0x1103006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 339
    .local v1, "blockPackages":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "item":I
    :goto_7
    array-length v3, v1

    if-ge v2, v3, :cond_6

    .line 340
    aget-object v3, v1, v2

    const-string v4, "b"

    invoke-virtual {p2, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 342
    .end local v2    # "item":I
    :cond_6
    nop

    .line 363
    .end local v0    # "allowPackages":[Ljava/lang/String;
    .end local v1    # "blockPackages":[Ljava/lang/String;
    :goto_8
    return-void

    :sswitch_data_0
    .sparse-switch
        0x2c81202b -> :sswitch_2
        0x51dfa3a2 -> :sswitch_1
        0x78901ee4 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method isDisabledConfiguration(Ljava/lang/String;)Z
    .locals 3
    .param p1, "configurationName"    # Ljava/lang/String;

    .line 483
    iget-object v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "force_resizable_activities"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 484
    const-string v0, "displayCompat"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    nop

    .line 483
    :goto_0
    return v2
.end method

.method printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "cmd"    # Ljava/lang/String;
    .param p3, "options"    # [Ljava/lang/String;

    .line 523
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 524
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "item":I
    :goto_0
    array-length v2, p3

    if-ge v1, v2, :cond_0

    .line 525
    aget-object v2, p3, v1

    .line 526
    .local v2, "option":Ljava/lang/String;
    const-string v3, " ["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    .end local v2    # "option":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 530
    .end local v1    # "item":I
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 531
    return-void
.end method

.method printOptionsRequires(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "cmd"    # Ljava/lang/String;

    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " options requires:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 520
    return-void
.end method
