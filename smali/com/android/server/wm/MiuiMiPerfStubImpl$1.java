class com.android.server.wm.MiuiMiPerfStubImpl$1 extends java.util.TimerTask {
	 /* .source "MiuiMiPerfStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiMiPerfStubImpl;->onAfterActivityResumed(Lcom/android/server/wm/ActivityRecord;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMiPerfStubImpl this$0; //synthetic
final java.lang.String val$activityName; //synthetic
final java.lang.String val$packageName; //synthetic
final Integer val$pid; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMiPerfStubImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMiPerfStubImpl; */
/* .line 169 */
this.this$0 = p1;
/* iput p2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->val$pid:I */
this.val$packageName = p3;
this.val$activityName = p4;
/* invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 8 */
/* .line 173 */
final String v0 = "MiuiMiPerfStubImpl"; // const-string v0, "MiuiMiPerfStubImpl"
try { // :try_start_0
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v1 */
	 /* .line 174 */
	 /* .local v1, "startMiperfTime":J */
	 v3 = this.this$0;
	 /* iget v4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->val$pid:I */
	 v5 = this.val$packageName;
	 v6 = this.val$activityName;
	 final String v7 = "onAfterActivityResumed"; // const-string v7, "onAfterActivityResumed"
	 com.android.server.wm.MiuiMiPerfStubImpl .-$$Nest$mmiPerfSystemBoostNotify ( v3,v4,v5,v6,v7 );
	 /* .line 175 */
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v3 */
	 /* sub-long/2addr v3, v1 */
	 /* .line 176 */
	 /* .local v3, "durationMiperf":J */
	 /* const-wide/16 v5, 0x32 */
	 /* cmp-long v5, v3, v5 */
	 /* if-lez v5, :cond_0 */
	 /* .line 177 */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "Call miPerfSystemBoostNotify is timeout, took "; // const-string v6, "Call miPerfSystemBoostNotify is timeout, took "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
	 final String v6 = "ms."; // const-string v6, "ms."
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .w ( v0,v5 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 181 */
} // .end local v1 # "startMiperfTime":J
} // .end local v3 # "durationMiperf":J
} // :cond_0
/* .line 179 */
/* :catch_0 */
/* move-exception v1 */
/* .line 180 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "miPerfSystemBoostNotify, IOException e:"; // const-string v3, "miPerfSystemBoostNotify, IOException e:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 182 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
