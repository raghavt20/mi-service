.class Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;
.super Ljava/lang/Object;
.source "DisplayContentStubImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/DisplayContentStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UpdateFocusRunnable"
.end annotation


# instance fields
.field private mPosted:Z

.field private mWmService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$mpost(Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->post(Z)V

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/WindowManagerService;)V
    .locals 0
    .param p1, "wmService"    # Lcom/android/server/wm/WindowManagerService;

    .line 523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524
    iput-object p1, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mWmService:Lcom/android/server/wm/WindowManagerService;

    .line 525
    return-void
.end method

.method private post(Z)V
    .locals 3
    .param p1, "post"    # Z

    .line 548
    iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mPosted:Z

    if-ne p1, v0, :cond_0

    .line 549
    return-void

    .line 551
    :cond_0
    if-eqz p1, :cond_1

    .line 552
    iget-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, p0, v1, v2}, Lcom/android/server/wm/WindowManagerService$H;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 554
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    invoke-virtual {v0, p0}, Lcom/android/server/wm/WindowManagerService$H;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 556
    :goto_0
    iput-boolean p1, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mPosted:Z

    .line 557
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 529
    iget-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 532
    :try_start_0
    const-string v1, "WindowManager"

    const-string v2, "retry update focused window after 3000ms"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    iget-object v1, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mWmService:Lcom/android/server/wm/WindowManagerService;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Lcom/android/server/wm/WindowManagerService;->updateFocusedWindowLocked(IZ)Z

    .line 543
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 544
    iput-boolean v3, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mPosted:Z

    .line 545
    return-void

    .line 543
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
