.class Lcom/android/server/wm/MultiSenceListener$H;
.super Landroid/os/Handler;
.source "MultiSenceListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MultiSenceListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MultiSenceListener;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MultiSenceListener;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 137
    iput-object p1, p0, Lcom/android/server/wm/MultiSenceListener$H;->this$0:Lcom/android/server/wm/MultiSenceListener;

    .line 138
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 139
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 143
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 144
    .local v0, "taskInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    iget v1, p1, Landroid/os/Message;->what:I

    const-wide/16 v2, 0x20

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 153
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EVENT_MULTI_TASK_ACTION_END: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I

    move-result v4

    invoke-static {v4}, Lmiui/smartpower/MultiTaskActionManager;->actionTypeToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "actionEndDetail":Ljava/lang/String;
    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 155
    iget-object v4, p0, Lcom/android/server/wm/MultiSenceListener$H;->this$0:Lcom/android/server/wm/MultiSenceListener;

    invoke-static {v4, v1}, Lcom/android/server/wm/MultiSenceListener;->-$$Nest$mLOG_IF_DEBUG(Lcom/android/server/wm/MultiSenceListener;Ljava/lang/String;)V

    .line 156
    iget-object v4, p0, Lcom/android/server/wm/MultiSenceListener$H;->this$0:Lcom/android/server/wm/MultiSenceListener;

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lcom/android/server/wm/MultiSenceListener;->-$$Nest$mupdateDynamicSenceInfo(Lcom/android/server/wm/MultiSenceListener;Lmiui/smartpower/MultiTaskActionManager$ActionInfo;Z)V

    .line 157
    iget-object v4, p0, Lcom/android/server/wm/MultiSenceListener$H;->this$0:Lcom/android/server/wm/MultiSenceListener;

    invoke-virtual {v4}, Lcom/android/server/wm/MultiSenceListener;->updateScreenStatus()V

    .line 158
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 159
    goto :goto_0

    .line 146
    .end local v1    # "actionEndDetail":Ljava/lang/String;
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EVENT_MULTI_TASK_ACTION_START: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I

    move-result v4

    invoke-static {v4}, Lmiui/smartpower/MultiTaskActionManager;->actionTypeToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "actionDetail":Ljava/lang/String;
    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 148
    iget-object v4, p0, Lcom/android/server/wm/MultiSenceListener$H;->this$0:Lcom/android/server/wm/MultiSenceListener;

    invoke-static {v4, v1}, Lcom/android/server/wm/MultiSenceListener;->-$$Nest$mLOG_IF_DEBUG(Lcom/android/server/wm/MultiSenceListener;Ljava/lang/String;)V

    .line 149
    iget-object v4, p0, Lcom/android/server/wm/MultiSenceListener$H;->this$0:Lcom/android/server/wm/MultiSenceListener;

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Lcom/android/server/wm/MultiSenceListener;->-$$Nest$mupdateDynamicSenceInfo(Lcom/android/server/wm/MultiSenceListener;Lmiui/smartpower/MultiTaskActionManager$ActionInfo;Z)V

    .line 150
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 151
    nop

    .line 163
    .end local v1    # "actionDetail":Ljava/lang/String;
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
