.class Lcom/android/server/wm/MiuiSizeCompatService$5;
.super Ljava/lang/Object;
.source "MiuiSizeCompatService.java"

# interfaces
.implements Ljava/util/function/BiConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiSizeCompatService;->getMiuiSizeCompatEnabledApps()Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/function/BiConsumer<",
        "Ljava/lang/String;",
        "Landroid/sizecompat/AspectRatioInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatService;

.field final synthetic val$map:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/util/Map;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiSizeCompatService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 733
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService$5;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    iput-object p2, p0, Lcom/android/server/wm/MiuiSizeCompatService$5;->val$map:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 733
    check-cast p1, Ljava/lang/String;

    check-cast p2, Landroid/sizecompat/AspectRatioInfo;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/MiuiSizeCompatService$5;->accept(Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;)V

    return-void
.end method

.method public accept(Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "aspectRatioInfo"    # Landroid/sizecompat/AspectRatioInfo;

    .line 736
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$5;->val$map:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 737
    return-void
.end method
