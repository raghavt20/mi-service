public class com.android.server.wm.MiuiOrientationImpl implements com.android.server.wm.MiuiOrientationStub {
	 /* .source "MiuiOrientationImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;, */
	 /* Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;, */
	 /* Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;, */
	 /* Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;, */
	 /* Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ADAPT_CUTOUT_DISABLE;
private static final Integer ADAPT_CUTOUT_ENABLE;
private static final Integer ADAPT_CUTOUT_UNAVAILABLE;
private static Boolean ENABLED;
private static final Integer FLIP_SCREEN_ROTATION_OUTER;
private static final Integer FOLD_DEVICE_TYPE;
private static Boolean IS_FOLD_SCREEN_DEVICE;
private static Boolean IS_MIUI_OPTIMIZATION;
private static final Integer KEY_INDEX;
private static final java.lang.String KEY_SMART_ROTATION;
private static final java.lang.String METADATA_IGNORE_ORIENTATION_REQUEST;
private static final java.lang.String MIUI_CTS;
private static final java.lang.String MIUI_OPTIMIZATION;
private static final java.lang.String MIUI_ORIENTATION_ENABLE;
private static final java.lang.String MIUI_SMART_ORIENTATION_ENABLE;
private static final java.lang.String MUILTDISPLAY_TYPE;
private static final Integer ORIENTATION_ARRAY_LENGTH;
private static final java.lang.String ORIENTATION_ATTR_PACKAGE_NAME;
private static final java.lang.String ORIENTATION_ATTR_PACKAGE_VALUE;
private static final java.lang.String ORIENTATION_COMMIT_TAG;
private static final java.lang.String ORIENTATION_FILE_PATH;
private static final Integer ORIENTATION_INDEX;
private static final java.lang.String ORIENTATION_TAG;
private static final java.lang.String ORIENTATION_TAG_PACKAGE;
private static final java.lang.String ORIENTATION_USER_SETTINGS_REMOVE;
public static final Integer POLICY_FULLSCREEN_BY_BLOCK_LIST;
public static final Integer POLICY_FULLSCREEN_CAMERA_ROTATE;
public static final Integer POLICY_FULLSCREEN_CAMERA_ROTATE_ALL;
public static final Integer POLICY_FULLSCREEN_DISPLAYCUTOUT_DISABLE;
public static final Integer POLICY_FULLSCREEN_DISPLAYCUTOUT_ENABLE;
public static final Integer POLICY_FULLSCREEN_NOT_ROTATE_APP;
public static final Integer POLICY_FULLSCREEN_NOT_ROTATE_SENSOR;
public static final Integer POLICY_FULLSCREEN_OUTER_BY_ALLOW_LIST;
public static final Integer POLICY_FULLSCREEN_RESUME_CAMERA_ROTATE;
public static final Integer POLICY_FULLSCREEN_UPDATE_CONFIG;
public static final Integer POLICY_FULL_SCREEN_INVALID;
public static final Integer POLICY_FULL_SCREEN_NOT_RELAUNCH;
public static final Integer POLICY_FULL_SCREEN_RELAUNCH;
public static final Integer POLICY_FULL_SCREEN_RELAUNCH_INTERACTIVE;
public static final Integer POLICY_FULL_SCREEN_TOAST;
private static final Integer SCREEN_ROTATION_INNER;
private static final Integer SCREEN_ROTATION_OUTER;
private static final Integer SCREEN_ROTATION_PAD;
private static Boolean SMART_ORIENTATION_ENABLE;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.wm.ActivityTaskManagerService mAtmService;
private android.widget.Toast mCompatLandToast;
private final java.util.Map mComponentMapBySystem;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private Boolean mDemoMode;
private Integer mDemoModePolicy;
private java.util.Map mEmbeddedFullRules;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/wm/MiuiOrientationImpl$FullRule;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.util.AtomicFile mFile;
private android.os.Handler mFileHandler;
private com.android.server.wm.MiuiOrientationImpl$FullScreen3AppCameraStrategy mFullScreen3AppCameraStrategy;
private com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager mFullScreenPackageManager;
private android.os.Handler mHandler;
private com.android.server.wm.ActivityRecord mLastResumeApp;
private com.android.server.wm.MiuiOrientationImpl$MiuiSettingsObserver mMiuiSettingsObserver;
private final java.util.Map mPackagesMapBySystem;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mPackagesMapByUserSettings;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.wm.ActivityTaskManagerServiceImpl mServiceImpl;
private Integer mShowRotationSuggestion;
private com.android.server.wm.MiuiOrientationImpl$SmartOrientationPolicy mSmartOrientationPolicy;
private Boolean mSmartRotationEnabled;
private Boolean mToastVisible;
private java.lang.Runnable mWriteSettingsRunnable;
/* # direct methods */
public static void $r8$lambda$YqC3mw8dr1ljFxLHkurCcsK6mc8 ( com.android.server.wm.MiuiOrientationImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->lambda$showToastWhenLandscapeIfNeed$0()V */
return;
} // .end method
public static void $r8$lambda$YtcGp13sbZ3UkmSjcthGaoul8WI ( com.android.server.wm.MiuiOrientationImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->lambda$showToastWhenLandscapeIfNeed$1()V */
return;
} // .end method
static java.util.Map -$$Nest$fgetmComponentMapBySystem ( com.android.server.wm.MiuiOrientationImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mComponentMapBySystem;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.wm.MiuiOrientationImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static java.util.Map -$$Nest$fgetmEmbeddedFullRules ( com.android.server.wm.MiuiOrientationImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mEmbeddedFullRules;
} // .end method
static android.util.AtomicFile -$$Nest$fgetmFile ( com.android.server.wm.MiuiOrientationImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFile;
} // .end method
static android.os.Handler -$$Nest$fgetmFileHandler ( com.android.server.wm.MiuiOrientationImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFileHandler;
} // .end method
static java.util.Map -$$Nest$fgetmPackagesMapBySystem ( com.android.server.wm.MiuiOrientationImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackagesMapBySystem;
} // .end method
static java.util.Map -$$Nest$fgetmPackagesMapByUserSettings ( com.android.server.wm.MiuiOrientationImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackagesMapByUserSettings;
} // .end method
static Boolean -$$Nest$fgetmSmartRotationEnabled ( com.android.server.wm.MiuiOrientationImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartRotationEnabled:Z */
} // .end method
static java.lang.Runnable -$$Nest$fgetmWriteSettingsRunnable ( com.android.server.wm.MiuiOrientationImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWriteSettingsRunnable;
} // .end method
static void -$$Nest$fputmSmartRotationEnabled ( com.android.server.wm.MiuiOrientationImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartRotationEnabled:Z */
return;
} // .end method
static Boolean -$$Nest$misDisplayFolded ( com.android.server.wm.MiuiOrientationImpl p0, com.android.server.wm.ActivityRecord p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isDisplayFolded(Lcom/android/server/wm/ActivityRecord;)Z */
} // .end method
static Boolean -$$Nest$misFixedAspectRatio ( com.android.server.wm.MiuiOrientationImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$misNeedCameraRotate ( com.android.server.wm.MiuiOrientationImpl p0, com.android.server.wm.ActivityRecord p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotate(Lcom/android/server/wm/ActivityRecord;)Z */
} // .end method
static Boolean -$$Nest$misNeedCameraRotateAll ( com.android.server.wm.MiuiOrientationImpl p0, com.android.server.wm.ActivityRecord p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotateAll(Lcom/android/server/wm/ActivityRecord;)Z */
} // .end method
static Boolean -$$Nest$misNeedCameraRotateInPad ( com.android.server.wm.MiuiOrientationImpl p0, com.android.server.wm.ActivityRecord p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotateInPad(Lcom/android/server/wm/ActivityRecord;)Z */
} // .end method
static Boolean -$$Nest$misNeedRotateWhenCameraResume ( com.android.server.wm.MiuiOrientationImpl p0, com.android.server.wm.ActivityRecord p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedRotateWhenCameraResume(Lcom/android/server/wm/ActivityRecord;)Z */
} // .end method
static Boolean -$$Nest$misNeedRotateWhenCameraResumeInPad ( com.android.server.wm.MiuiOrientationImpl p0, com.android.server.wm.ActivityRecord p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedRotateWhenCameraResumeInPad(Lcom/android/server/wm/ActivityRecord;)Z */
} // .end method
static Boolean -$$Nest$sfgetENABLED ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->ENABLED:Z */
} // .end method
static Boolean -$$Nest$sfgetIS_MIUI_OPTIMIZATION ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_MIUI_OPTIMIZATION:Z */
} // .end method
static void -$$Nest$smwriteSettings ( android.util.AtomicFile p0, java.util.Map p1 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.wm.MiuiOrientationImpl .writeSettings ( p0,p1 );
return;
} // .end method
static com.android.server.wm.MiuiOrientationImpl ( ) {
/* .locals 4 */
/* .line 164 */
/* nop */
/* .line 165 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 164 */
int v1 = 1; // const/4 v1, 0x1
/* xor-int/2addr v0, v1 */
final String v2 = "persist.sys.miui_optimization"; // const-string v2, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v2,v0 );
com.android.server.wm.MiuiOrientationImpl.IS_MIUI_OPTIMIZATION = (v0!= 0);
/* .line 166 */
final String v0 = "persist.sys.muiltdisplay_type"; // const-string v0, "persist.sys.muiltdisplay_type"
int v2 = 0; // const/4 v2, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v2 );
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_0 */
} // :cond_0
/* move v1, v2 */
} // :goto_0
com.android.server.wm.MiuiOrientationImpl.IS_FOLD_SCREEN_DEVICE = (v1!= 0);
/* .line 167 */
final String v0 = "ro.config.miui_orientation_enable"; // const-string v0, "ro.config.miui_orientation_enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v2 );
com.android.server.wm.MiuiOrientationImpl.ENABLED = (v0!= 0);
/* .line 168 */
return;
} // .end method
public com.android.server.wm.MiuiOrientationImpl ( ) {
/* .locals 1 */
/* .line 170 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 920 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mPackagesMapBySystem = v0;
/* .line 921 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mComponentMapBySystem = v0;
/* .line 922 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mPackagesMapByUserSettings = v0;
/* .line 923 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mEmbeddedFullRules = v0;
/* .line 1430 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$2;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V */
this.mWriteSettingsRunnable = v0;
/* .line 171 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_MIUI_OPTIMIZATION:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartRotationEnabled:Z */
/* .line 172 */
return;
} // .end method
static java.lang.String fullScreenModeToString ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "mode" # I */
/* .line 1438 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 1446 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "UnKnown("; // const-string v1, "UnKnown("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1442 */
/* :pswitch_1 */
final String v0 = "fixedorientation"; // const-string v0, "fixedorientation"
/* .line 1440 */
/* :pswitch_2 */
final String v0 = "landscape"; // const-string v0, "landscape"
/* .line 1444 */
/* :pswitch_3 */
/* const-string/jumbo v0, "unspecified" */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch -0x1 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
static java.lang.String fullScreenPolicyToString ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "policy" # I */
/* .line 1533 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1534 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* and-int/lit8 v1, p0, 0x1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1535 */
final String v1 = "nr:"; // const-string v1, "nr:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1537 */
} // :cond_0
/* and-int/lit8 v1, p0, 0x2 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1538 */
final String v1 = "r:"; // const-string v1, "r:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1540 */
} // :cond_1
/* and-int/lit8 v1, p0, 0x4 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1541 */
final String v1 = "ri:"; // const-string v1, "ri:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1543 */
} // :cond_2
/* and-int/lit8 v1, p0, 0x8 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1544 */
/* const-string/jumbo v1, "t:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1546 */
} // :cond_3
/* and-int/lit8 v1, p0, 0x10 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1547 */
/* const-string/jumbo v1, "w:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1549 */
} // :cond_4
/* and-int/lit8 v1, p0, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1550 */
final String v1 = "b:"; // const-string v1, "b:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1552 */
} // :cond_5
/* and-int/lit8 v1, p0, 0x40 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 1553 */
final String v1 = "nra:"; // const-string v1, "nra:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1555 */
} // :cond_6
/* and-int/lit16 v1, p0, 0x80 */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1556 */
final String v1 = "nrs:"; // const-string v1, "nrs:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1558 */
} // :cond_7
/* and-int/lit16 v1, p0, 0x100 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1559 */
final String v1 = "rcr:"; // const-string v1, "rcr:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1561 */
} // :cond_8
/* and-int/lit16 v1, p0, 0x200 */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 1562 */
final String v1 = "cr:"; // const-string v1, "cr:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1564 */
} // :cond_9
/* and-int/lit16 v1, p0, 0x400 */
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 1565 */
final String v1 = "cra:"; // const-string v1, "cra:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1567 */
} // :cond_a
/* and-int/lit16 v1, p0, 0x800 */
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 1568 */
/* const-string/jumbo v1, "uc:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1570 */
} // :cond_b
/* and-int/lit16 v1, p0, 0x1000 */
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 1571 */
final String v1 = "de:"; // const-string v1, "de:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1573 */
} // :cond_c
/* and-int/lit16 v1, p0, 0x2000 */
if ( v1 != null) { // if-eqz v1, :cond_d
/* .line 1574 */
final String v1 = "dd:"; // const-string v1, "dd:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1576 */
} // :cond_d
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_e */
/* .line 1577 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* add-int/lit8 v1, v1, -0x1 */
(( java.lang.StringBuilder ) v0 ).deleteCharAt ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1579 */
} // :cond_e
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
static Integer fullScreenStringToPolicy ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "policyStr" # Ljava/lang/String; */
/* .line 1482 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1483 */
/* .local v0, "policy":I */
v1 = (( java.lang.String ) p0 ).hashCode ( ); // invoke-virtual {p0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* goto/16 :goto_0 */
/* :sswitch_0 */
final String v1 = "rcr"; // const-string v1, "rcr"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0x8 */
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v1 = "nrs"; // const-string v1, "nrs"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 7; // const/4 v1, 0x7
/* goto/16 :goto_1 */
/* :sswitch_2 */
final String v1 = "nra"; // const-string v1, "nra"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 6; // const/4 v1, 0x6
/* goto/16 :goto_1 */
/* :sswitch_3 */
final String v1 = "cra"; // const-string v1, "cra"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xa */
/* goto/16 :goto_1 */
/* :sswitch_4 */
/* const-string/jumbo v1, "uc" */
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xb */
/* :sswitch_5 */
final String v1 = "ri"; // const-string v1, "ri"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 2; // const/4 v1, 0x2
/* :sswitch_6 */
final String v1 = "nr"; // const-string v1, "nr"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 0; // const/4 v1, 0x0
/* :sswitch_7 */
final String v1 = "de"; // const-string v1, "de"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xc */
/* :sswitch_8 */
final String v1 = "dd"; // const-string v1, "dd"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xd */
/* :sswitch_9 */
final String v1 = "cr"; // const-string v1, "cr"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0x9 */
/* :sswitch_a */
/* const-string/jumbo v1, "w" */
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 4; // const/4 v1, 0x4
/* :sswitch_b */
/* const-string/jumbo v1, "t" */
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 3; // const/4 v1, 0x3
/* :sswitch_c */
final String v1 = "r"; // const-string v1, "r"
v1 = (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 1; // const/4 v1, 0x1
	 /* :sswitch_d */
	 final String v1 = "b"; // const-string v1, "b"
	 v1 = 	 (( java.lang.String ) p0 ).equals ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 int v1 = 5; // const/4 v1, 0x5
	 } // :goto_0
	 int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 1524 */
/* :pswitch_0 */
/* const/16 v0, 0x2000 */
/* .line 1525 */
/* .line 1521 */
/* :pswitch_1 */
/* const/16 v0, 0x1000 */
/* .line 1522 */
/* .line 1518 */
/* :pswitch_2 */
/* const/16 v0, 0x800 */
/* .line 1519 */
/* .line 1515 */
/* :pswitch_3 */
/* const/16 v0, 0x400 */
/* .line 1516 */
/* .line 1512 */
/* :pswitch_4 */
/* const/16 v0, 0x200 */
/* .line 1513 */
/* .line 1509 */
/* :pswitch_5 */
/* const/16 v0, 0x100 */
/* .line 1510 */
/* .line 1506 */
/* :pswitch_6 */
/* const/16 v0, 0x80 */
/* .line 1507 */
/* .line 1503 */
/* :pswitch_7 */
/* const/16 v0, 0x40 */
/* .line 1504 */
/* .line 1500 */
/* :pswitch_8 */
/* const/16 v0, 0x20 */
/* .line 1501 */
/* .line 1497 */
/* :pswitch_9 */
/* const/16 v0, 0x10 */
/* .line 1498 */
/* .line 1494 */
/* :pswitch_a */
/* const/16 v0, 0x8 */
/* .line 1495 */
/* .line 1491 */
/* :pswitch_b */
int v0 = 4; // const/4 v0, 0x4
/* .line 1492 */
/* .line 1488 */
/* :pswitch_c */
int v0 = 2; // const/4 v0, 0x2
/* .line 1489 */
/* .line 1485 */
/* :pswitch_d */
int v0 = 1; // const/4 v0, 0x1
/* .line 1486 */
/* nop */
/* .line 1529 */
} // :goto_2
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x62 -> :sswitch_d */
/* 0x72 -> :sswitch_c */
/* 0x74 -> :sswitch_b */
/* 0x77 -> :sswitch_a */
/* 0xc6f -> :sswitch_9 */
/* 0xc80 -> :sswitch_8 */
/* 0xc81 -> :sswitch_7 */
/* 0xdc4 -> :sswitch_6 */
/* 0xe37 -> :sswitch_5 */
/* 0xe8e -> :sswitch_4 */
/* 0x181d2 -> :sswitch_3 */
/* 0x1ab1d -> :sswitch_2 */
/* 0x1ab2f -> :sswitch_1 */
/* 0x1b861 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Integer getDisplayCutoutSetting ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "policy" # I */
/* .line 884 */
/* and-int/lit16 v0, p1, 0x1000 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 885 */
int v0 = 1; // const/4 v0, 0x1
/* .line 886 */
} // :cond_0
/* and-int/lit16 v0, p1, 0x2000 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 887 */
int v0 = 0; // const/4 v0, 0x0
/* .line 889 */
} // :cond_1
int v0 = -1; // const/4 v0, -0x1
} // .end method
static com.android.server.wm.MiuiOrientationImpl getInstance ( ) {
/* .locals 1 */
/* .line 175 */
com.android.server.wm.MiuiOrientationStub .get ( );
/* check-cast v0, Lcom/android/server/wm/MiuiOrientationImpl; */
} // .end method
private static Integer getIntAttribute ( com.android.modules.utils.TypedXmlPullParser p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p0, "parser" # Lcom/android/modules/utils/TypedXmlPullParser; */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "defaultValue" # I */
/* .line 989 */
v0 = int v0 = 0; // const/4 v0, 0x0
} // .end method
private Integer getOrientationMode ( com.android.server.wm.ActivityRecord p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 556 */
int v0 = -1; // const/4 v0, -0x1
/* .line 557 */
/* .local v0, "mode":I */
v1 = this.mFullScreenPackageManager;
v2 = this.mActivityComponent;
/* .line 558 */
(( android.content.ComponentName ) v2 ).flattenToShortString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v1 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v1 ).getOrientationPolicyByComponent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I
/* .line 559 */
/* .local v1, "activityPolicy":I */
int v2 = -1; // const/4 v2, -0x1
/* if-eq v2, v1, :cond_1 */
/* .line 560 */
/* and-int/lit8 v2, v1, 0x20 */
/* if-nez v2, :cond_0 */
/* .line 561 */
int v0 = 1; // const/4 v0, 0x1
/* .line 563 */
} // :cond_0
/* .line 565 */
} // :cond_1
v2 = this.mFullScreenPackageManager;
v2 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v2 ).getOrientationPolicy ( p2 ); // invoke-virtual {v2, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 566 */
/* .local v2, "packagePolicy":I */
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->isInBlackListFromPolicy(I)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 567 */
int v0 = -1; // const/4 v0, -0x1
/* .line 568 */
} // :cond_2
v3 = /* invoke-direct {p0, p2}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 569 */
int v0 = 2; // const/4 v0, 0x2
/* .line 571 */
} // :cond_3
v3 = this.mFullScreenPackageManager;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v3 ).getOrientationMode ( p2 ); // invoke-virtual {v3, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationMode(Ljava/lang/String;)I
/* .line 573 */
} // :goto_0
} // .end method
private Integer getOuterOrientationMode ( com.android.server.wm.ActivityRecord p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 313 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isDisplayFolded(Lcom/android/server/wm/ActivityRecord;)Z */
int v1 = -1; // const/4 v1, -0x1
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isSmartOrientEnable ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 314 */
v0 = this.mFullScreenPackageManager;
v2 = this.mActivityComponent;
/* .line 315 */
(( android.content.ComponentName ) v2 ).flattenToShortString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).getOrientationPolicyByComponent ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I
/* .line 316 */
/* .local v0, "activityPolicy":I */
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v0, :cond_0 */
/* and-int/lit8 v3, v0, 0x10 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 318 */
/* .line 320 */
} // :cond_0
v3 = this.mFullScreenPackageManager;
v3 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v3 ).getOrientationPolicy ( p2 ); // invoke-virtual {v3, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 321 */
/* .local v3, "packagePolicy":I */
/* if-eq v1, v3, :cond_1 */
/* and-int/lit8 v4, v3, 0x10 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 323 */
/* .line 326 */
} // .end local v0 # "activityPolicy":I
} // .end local v3 # "packagePolicy":I
} // :cond_1
} // .end method
private Integer getRelaunchModeFromBundle ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 588 */
int v0 = 0; // const/4 v0, 0x0
/* .line 589 */
/* .local v0, "relaunchMode":I */
/* sget-boolean v1, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 590 */
v1 = this.mEmbeddedFullRules;
/* check-cast v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* .line 591 */
/* .local v1, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mNr:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 592 */
int v0 = 2; // const/4 v0, 0x2
/* .line 593 */
} // :cond_0
/* iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mR:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 594 */
int v0 = 1; // const/4 v0, 0x1
/* .line 595 */
} // :cond_1
/* iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mRi:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 596 */
int v0 = 4; // const/4 v0, 0x4
/* .line 598 */
} // .end local v1 # "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
} // :cond_2
} // :goto_0
} // .end method
private Integer getRelaunchModeFromPolicy ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "policy" # I */
/* .line 577 */
int v0 = 0; // const/4 v0, 0x0
/* .line 578 */
/* .local v0, "relaunchMode":I */
/* and-int/lit8 v1, p1, 0x1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 579 */
int v0 = 2; // const/4 v0, 0x2
/* .line 580 */
} // :cond_0
/* and-int/lit8 v1, p1, 0x2 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 581 */
int v0 = 1; // const/4 v0, 0x1
/* .line 582 */
} // :cond_1
/* and-int/lit8 v1, p1, 0x4 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 583 */
int v0 = 4; // const/4 v0, 0x4
/* .line 584 */
} // :cond_2
} // :goto_0
} // .end method
private Integer getRotationOptionsFromBundle ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 638 */
int v0 = 0; // const/4 v0, 0x0
/* .line 639 */
/* .local v0, "rotationOptions":I */
/* sget-boolean v1, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 640 */
v1 = this.mEmbeddedFullRules;
/* check-cast v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* .line 641 */
/* .local v1, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mNrs:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 642 */
/* or-int/lit8 v0, v0, 0x1 */
/* .line 643 */
} // :cond_0
/* iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mNra:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 644 */
/* or-int/lit8 v0, v0, 0x2 */
/* .line 646 */
} // .end local v1 # "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
} // :cond_1
} // .end method
private Integer getRotationOptionsFromPolicy ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "policy" # I */
/* .line 629 */
int v0 = 0; // const/4 v0, 0x0
/* .line 630 */
/* .local v0, "rotationOptions":I */
/* and-int/lit16 v1, p1, 0x80 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 631 */
/* or-int/lit8 v0, v0, 0x1 */
/* .line 632 */
} // :cond_0
/* and-int/lit8 v1, p1, 0x40 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 633 */
/* or-int/lit8 v0, v0, 0x2 */
/* .line 634 */
} // :cond_1
} // .end method
private static android.util.AtomicFile getSettingsFile ( ) {
/* .locals 3 */
/* .line 927 */
/* new-instance v0, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v2, "system/miui_orientation_settings.xml" */
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 928 */
/* .local v0, "settingsFile":Ljava/io/File; */
/* new-instance v1, Landroid/util/AtomicFile; */
final String v2 = "miui_orientations"; // const-string v2, "miui_orientations"
/* invoke-direct {v1, v0, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;Ljava/lang/String;)V */
} // .end method
private Boolean isDisplayFolded ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 275 */
v0 = this.mAtmService;
v0 = this.mWindowManager;
v0 = v0 = this.mPolicy;
} // .end method
private Boolean isEmbedded ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 443 */
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
v1 = this.mActivityComponent;
/* .line 444 */
v0 = (( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 443 */
} // .end method
private Boolean isEmbedded ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 448 */
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
v0 = /* .line 449 */
/* .line 448 */
} // .end method
private Boolean isFixedAspectRatio ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 416 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 417 */
/* .line 419 */
} // :cond_0
v0 = this.mServiceImpl;
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).getAspectRatio ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getAspectRatio(Ljava/lang/String;)F
int v2 = 0; // const/4 v2, 0x0
/* cmpl-float v0, v0, v2 */
/* if-lez v0, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
private Boolean isFixedAspectRatioInPad ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 424 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mActivityComponent;
/* .line 425 */
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v1 = android.sizecompat.MiuiSizeCompatManager .getMiuiSizeCompatAppRatio ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
int v2 = 0; // const/4 v2, 0x0
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 424 */
} // :cond_0
/* .line 426 */
/* :catch_0 */
/* move-exception v1 */
/* .line 427 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 429 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
private Boolean isFixedAspectRatioInPad ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 434 */
/* nop */
/* .line 435 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = android.sizecompat.MiuiSizeCompatManager .getMiuiSizeCompatAppRatio ( p1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
int v2 = 0; // const/4 v2, 0x0
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 434 */
} // :cond_0
/* .line 436 */
/* :catch_0 */
/* move-exception v1 */
/* .line 437 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 439 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
private Boolean isInBlackList ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 551 */
v0 = this.mActivityComponent;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v0 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getOrientationMode(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;)I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isInBlackListFromPolicy ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "policy" # I */
/* .line 546 */
int v0 = -1; // const/4 v0, -0x1
/* if-eq v0, p1, :cond_0 */
/* and-int/lit8 v0, p1, 0x20 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isLandEnabledForPackagePad ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 453 */
v0 = this.mEmbeddedFullRules;
/* check-cast v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* .line 454 */
/* .local v0, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v1, v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mEnable:Z */
/* .line 455 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean isNeedCameraRotate ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 6 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 486 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isInBlackList(Lcom/android/server/wm/ActivityRecord;)Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_4 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 487 */
} // :cond_0
v0 = this.mFullScreenPackageManager;
v2 = this.mActivityComponent;
/* .line 488 */
(( android.content.ComponentName ) v2 ).flattenToShortString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).getOrientationPolicyByComponent ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I
/* .line 489 */
/* .local v0, "activityPolicy":I */
int v2 = 1; // const/4 v2, 0x1
int v3 = -1; // const/4 v3, -0x1
/* if-eq v3, v0, :cond_1 */
/* and-int/lit16 v4, v0, 0x200 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 491 */
/* .line 493 */
} // :cond_1
v4 = this.mFullScreenPackageManager;
v5 = this.mActivityComponent;
/* .line 494 */
(( android.content.ComponentName ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v4 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v4 ).getOrientationPolicy ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 495 */
/* .local v4, "policy":I */
v5 = this.mActivityComponent;
(( android.content.ComponentName ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v5 = /* invoke-direct {p0, v5}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
/* if-nez v5, :cond_2 */
/* if-eq v3, v4, :cond_3 */
/* and-int/lit16 v3, v4, 0x200 */
if ( v3 != null) { // if-eqz v3, :cond_3
} // :cond_2
/* move v1, v2 */
} // :cond_3
/* .line 486 */
} // .end local v0 # "activityPolicy":I
} // .end local v4 # "policy":I
} // :cond_4
} // :goto_0
} // .end method
private Boolean isNeedCameraRotate ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 501 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_FOLD:Z */
/* if-nez v0, :cond_0 */
/* .line 502 */
} // :cond_0
v0 = this.mFullScreenPackageManager;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).getOrientationPolicy ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 503 */
/* .local v0, "policy":I */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
/* if-nez v2, :cond_1 */
int v2 = -1; // const/4 v2, -0x1
/* if-eq v2, v0, :cond_2 */
/* and-int/lit16 v2, v0, 0x200 */
if ( v2 != null) { // if-eqz v2, :cond_2
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
/* .line 501 */
} // .end local v0 # "policy":I
} // :cond_3
} // :goto_0
} // .end method
private Boolean isNeedCameraRotateAll ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 6 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 509 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isInBlackList(Lcom/android/server/wm/ActivityRecord;)Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_3 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 510 */
} // :cond_0
v0 = this.mFullScreenPackageManager;
v2 = this.mActivityComponent;
/* .line 511 */
(( android.content.ComponentName ) v2 ).flattenToShortString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).getOrientationPolicyByComponent ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I
/* .line 512 */
/* .local v0, "activityPolicy":I */
int v2 = 1; // const/4 v2, 0x1
int v3 = -1; // const/4 v3, -0x1
/* if-eq v3, v0, :cond_1 */
/* and-int/lit16 v4, v0, 0x400 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 514 */
/* .line 516 */
} // :cond_1
v4 = this.mFullScreenPackageManager;
v5 = this.mActivityComponent;
/* .line 517 */
(( android.content.ComponentName ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v4 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v4 ).getOrientationPolicy ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 518 */
/* .local v4, "policy":I */
/* if-eq v3, v4, :cond_2 */
/* and-int/lit16 v3, v4, 0x400 */
if ( v3 != null) { // if-eqz v3, :cond_2
/* move v1, v2 */
} // :cond_2
/* .line 509 */
} // .end local v0 # "activityPolicy":I
} // .end local v4 # "policy":I
} // :cond_3
} // :goto_0
} // .end method
private Boolean isNeedCameraRotateInPad ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 6 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 531 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 532 */
} // :cond_0
v0 = this.mActivityComponent;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 533 */
/* .local v0, "packageName":Ljava/lang/String; */
v2 = this.mEmbeddedFullRules;
/* check-cast v2, Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* .line 534 */
/* .local v2, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
v3 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatioInPad(Lcom/android/server/wm/ActivityRecord;)Z */
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 535 */
v3 = com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
if ( v3 != null) { // if-eqz v3, :cond_1
/* move v3, v4 */
} // :cond_1
/* move v3, v1 */
/* .line 536 */
/* .local v3, "isAppSupportCameraPreview":Z */
} // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* iget-boolean v5, v2, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mCr:Z */
/* if-nez v5, :cond_3 */
} // :cond_2
if ( v3 != null) { // if-eqz v3, :cond_4
} // :cond_3
/* move v1, v4 */
} // :cond_4
} // .end method
private Boolean isNeedCameraRotateInPad ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 523 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 524 */
} // :cond_0
v0 = this.mEmbeddedFullRules;
/* check-cast v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* .line 525 */
/* .local v0, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatioInPad(Ljava/lang/String;)Z */
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 526 */
v2 = com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v3 */
} // :cond_1
/* move v2, v1 */
/* .line 527 */
/* .local v2, "isAppSupportCameraPreview":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v4, v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mCr:Z */
/* if-nez v4, :cond_3 */
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_4
} // :cond_3
/* move v1, v3 */
} // :cond_4
} // .end method
private Boolean isNeedRotateWhenCameraResume ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 6 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 468 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 469 */
} // :cond_0
v0 = this.mFullScreenPackageManager;
v2 = this.mActivityComponent;
/* .line 470 */
(( android.content.ComponentName ) v2 ).flattenToShortString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).getOrientationPolicyByComponent ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I
/* .line 471 */
/* .local v0, "activityPolicy":I */
int v2 = 1; // const/4 v2, 0x1
int v3 = -1; // const/4 v3, -0x1
/* if-eq v3, v0, :cond_2 */
/* and-int/lit16 v4, v0, 0x200 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 473 */
/* and-int/lit16 v3, v0, 0x100 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 474 */
/* .line 476 */
} // :cond_1
/* .line 479 */
} // :cond_2
v4 = this.mFullScreenPackageManager;
v5 = this.mActivityComponent;
/* .line 480 */
(( android.content.ComponentName ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v4 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v4 ).getOrientationPolicy ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 481 */
/* .local v4, "policy":I */
/* if-eq v3, v4, :cond_3 */
/* and-int/lit16 v3, v4, 0x100 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* move v1, v2 */
} // :cond_3
} // .end method
private Boolean isNeedRotateWhenCameraResumeInPad ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 540 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotateInPad(Lcom/android/server/wm/ActivityRecord;)Z */
/* if-nez v0, :cond_0 */
/* .line 541 */
} // :cond_0
v0 = this.mEmbeddedFullRules;
v2 = this.mActivityComponent;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* check-cast v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* .line 542 */
/* .local v0, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v2, v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mRcr:Z */
/* if-nez v2, :cond_2 */
} // :cond_1
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatioInPad(Lcom/android/server/wm/ActivityRecord;)Z */
if ( v2 != null) { // if-eqz v2, :cond_3
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
} // :cond_3
/* .line 540 */
} // .end local v0 # "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
} // :cond_4
} // :goto_0
} // .end method
private void lambda$showToastWhenLandscapeIfNeed$0 ( ) { //synthethic
/* .locals 2 */
/* .line 769 */
v0 = this.mCompatLandToast;
/* const v1, 0x110f029c */
(( android.widget.Toast ) v0 ).setText ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V
/* .line 770 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mToastVisible:Z */
/* .line 771 */
v0 = this.mCompatLandToast;
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 772 */
return;
} // .end method
private void lambda$showToastWhenLandscapeIfNeed$1 ( ) { //synthethic
/* .locals 1 */
/* .line 778 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mToastVisible:Z */
/* .line 779 */
v0 = this.mCompatLandToast;
(( android.widget.Toast ) v0 ).cancel ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V
/* .line 780 */
return;
} // .end method
private Boolean needShowToast ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 7 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 739 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_7
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
/* if-nez v0, :cond_7 */
v0 = this.mAtmService;
v0 = this.mWindowManager;
v0 = this.mPolicy;
v0 = /* .line 741 */
/* if-nez v0, :cond_7 */
/* .line 742 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z
/* if-nez v0, :cond_7 */
/* .line 743 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z */
/* if-nez v0, :cond_7 */
v0 = this.mActivityComponent;
/* .line 744 */
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
/* if-nez v0, :cond_7 */
v0 = com.android.server.wm.ActivityRecord$State.RESUMED;
/* .line 745 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).isState ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I */
/* .line 746 */
v0 = android.content.pm.ActivityInfo .isFixedOrientationPortrait ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_7
v0 = this.mDisplayContent;
/* .line 747 */
v0 = (( com.android.server.wm.DisplayContent ) v0 ).getRotation ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getRotation()I
/* if-nez v0, :cond_0 */
/* .line 749 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z */
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I */
/* and-int/lit8 v0, v0, 0x8 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v1, v2 */
} // :cond_1
/* .line 750 */
} // :cond_2
v0 = this.mFullScreenPackageManager;
v3 = this.mActivityComponent;
/* .line 751 */
(( android.content.ComponentName ) v3 ).flattenToShortString ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).getOrientationPolicyByComponent ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I
/* .line 752 */
/* .local v0, "policy":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 753 */
/* .local v3, "showToast":Z */
int v4 = -1; // const/4 v4, -0x1
/* if-eq v4, v0, :cond_4 */
/* .line 754 */
/* and-int/lit8 v4, v0, 0x8 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* move v1, v2 */
} // :cond_3
/* move v3, v1 */
/* .line 756 */
} // :cond_4
v5 = this.mFullScreenPackageManager;
v6 = this.mActivityComponent;
(( android.content.ComponentName ) v6 ).getPackageName ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v5 ).getOrientationPolicy ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 757 */
/* if-eq v4, v0, :cond_6 */
/* .line 758 */
/* and-int/lit8 v4, v0, 0x8 */
if ( v4 != null) { // if-eqz v4, :cond_5
/* move v1, v2 */
} // :cond_5
/* move v3, v1 */
/* .line 760 */
} // :cond_6
} // :goto_0
/* .line 748 */
} // .end local v0 # "policy":I
} // .end local v3 # "showToast":Z
} // :cond_7
} // :goto_1
} // .end method
static android.util.SparseArray parseAppRequestOrientation ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p0, "value" # Ljava/lang/String; */
/* .line 1452 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v1, v0, [Ljava/lang/String; */
/* .line 1453 */
/* .local v1, "orientationStr":[Ljava/lang/String; */
/* new-instance v2, Landroid/util/SparseArray; */
/* invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V */
/* .line 1454 */
/* .local v2, "orientationArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;" */
v3 = android.text.TextUtils .isEmpty ( p0 );
/* if-nez v3, :cond_0 */
/* .line 1455 */
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) p0 ).split ( v3 ); // invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1458 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v1 */
/* if-ge v3, v4, :cond_2 */
/* .line 1459 */
/* aget-object v4, v1, v3 */
final String v5 = "\\."; // const-string v5, "\\."
(( java.lang.String ) v4 ).split ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1460 */
/* .local v4, "orientationForDiffDevice":[Ljava/lang/String; */
/* array-length v5, v4 */
int v6 = 2; // const/4 v6, 0x2
/* if-ne v5, v6, :cond_1 */
/* .line 1461 */
/* aget-object v5, v4, v0 */
v5 = java.lang.Integer .parseInt ( v5 );
int v6 = 1; // const/4 v6, 0x1
/* aget-object v6, v4, v6 */
/* .line 1462 */
v6 = com.android.server.wm.MiuiOrientationImpl .screenOrientationFromString ( v6 );
java.lang.Integer .valueOf ( v6 );
/* .line 1461 */
(( android.util.SparseArray ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
/* .line 1458 */
} // .end local v4 # "orientationForDiffDevice":[Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1465 */
} // .end local v3 # "i":I
} // :cond_2
} // .end method
static Integer parseFullScreenPolicy ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "value" # Ljava/lang/String; */
/* .line 1469 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Ljava/lang/String; */
/* .line 1470 */
/* .local v0, "policys":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1471 */
/* .local v1, "policy":I */
v2 = android.text.TextUtils .isEmpty ( p0 );
/* if-nez v2, :cond_0 */
/* .line 1472 */
final String v2 = ":"; // const-string v2, ":"
(( java.lang.String ) p0 ).split ( v2 ); // invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1475 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v0 */
/* if-ge v2, v3, :cond_1 */
/* .line 1476 */
/* aget-object v3, v0, v2 */
v3 = com.android.server.wm.MiuiOrientationImpl .fullScreenStringToPolicy ( v3 );
/* or-int/2addr v1, v3 */
/* .line 1475 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1478 */
} // .end local v2 # "i":I
} // :cond_1
} // .end method
private static void readPackage ( com.android.modules.utils.TypedXmlPullParser p0, java.util.HashMap p1 ) {
/* .locals 3 */
/* .param p0, "parser" # Lcom/android/modules/utils/TypedXmlPullParser; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/modules/utils/TypedXmlPullParser;", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/NumberFormatException;, */
/* Lorg/xmlpull/v1/XmlPullParserException;, */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 994 */
/* .local p1, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;" */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "name"; // const-string v1, "name"
/* .line 995 */
/* .local v0, "name":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 996 */
/* const-string/jumbo v1, "value" */
int v2 = 0; // const/4 v2, 0x0
v1 = com.android.server.wm.MiuiOrientationImpl .getIntAttribute ( p0,v1,v2 );
/* .line 997 */
/* .local v1, "value":I */
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) p1 ).put ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 999 */
} // .end local v1 # "value":I
} // :cond_0
com.android.internal.util.XmlUtils .skipCurrentTag ( p0 );
/* .line 1000 */
return;
} // .end method
private static java.util.HashMap readSettings ( android.util.AtomicFile p0 ) {
/* .locals 11 */
/* .param p0, "file" # Landroid/util/AtomicFile; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/AtomicFile;", */
/* ")", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 935 */
final String v0 = "MiuiOrientationImpl"; // const-string v0, "MiuiOrientationImpl"
try { // :try_start_0
(( android.util.AtomicFile ) p0 ).openRead ( ); // invoke-virtual {p0}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .line 939 */
/* .local v1, "stream":Ljava/io/InputStream; */
/* nop */
/* .line 940 */
int v2 = 0; // const/4 v2, 0x0
/* .line 941 */
/* .local v2, "success":Z */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 943 */
/* .local v3, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;" */
try { // :try_start_1
android.util.Xml .resolvePullParser ( v1 );
/* .line 945 */
/* .local v4, "parser":Lcom/android/modules/utils/TypedXmlPullParser; */
v5 = } // :goto_0
/* move v6, v5 */
/* .local v6, "type":I */
int v7 = 1; // const/4 v7, 0x1
int v8 = 2; // const/4 v8, 0x2
/* if-eq v5, v8, :cond_0 */
/* if-eq v6, v7, :cond_0 */
/* .line 950 */
} // :cond_0
/* if-ne v6, v8, :cond_6 */
v5 = /* .line 954 */
/* .line 955 */
/* .local v5, "outerDepth":I */
} // :cond_1
v8 = } // :goto_1
/* move v6, v8 */
/* if-eq v8, v7, :cond_5 */
int v8 = 3; // const/4 v8, 0x3
/* if-ne v6, v8, :cond_2 */
v9 = /* .line 956 */
/* if-le v9, v5, :cond_5 */
/* .line 957 */
} // :cond_2
/* if-eq v6, v8, :cond_1 */
int v8 = 4; // const/4 v8, 0x4
/* if-ne v6, v8, :cond_3 */
/* .line 958 */
/* .line 961 */
} // :cond_3
/* .line 962 */
/* .local v8, "tagName":Ljava/lang/String; */
final String v9 = "package"; // const-string v9, "package"
v9 = (( java.lang.String ) v8 ).equals ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_4
/* .line 963 */
com.android.server.wm.MiuiOrientationImpl .readPackage ( v4,v3 );
/* .line 965 */
} // :cond_4
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Unknown element under <orientation-settings>: "; // const-string v10, "Unknown element under <orientation-settings>: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 966 */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 965 */
android.util.Slog .w ( v0,v9 );
/* .line 967 */
com.android.internal.util.XmlUtils .skipCurrentTag ( v4 );
/* :try_end_1 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/NullPointerException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 969 */
} // .end local v8 # "tagName":Ljava/lang/String;
} // :goto_2
/* .line 970 */
} // :cond_5
int v2 = 1; // const/4 v2, 0x1
/* .line 976 */
} // .end local v4 # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
} // .end local v5 # "outerDepth":I
} // .end local v6 # "type":I
try { // :try_start_2
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 979 */
} // :goto_3
/* .line 977 */
/* :catch_0 */
/* move-exception v4 */
/* .line 978 */
/* .local v4, "ignored":Ljava/io/IOException; */
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
/* .line 980 */
} // .end local v4 # "ignored":Ljava/io/IOException;
/* .line 951 */
/* .local v4, "parser":Lcom/android/modules/utils/TypedXmlPullParser; */
/* .restart local v6 # "type":I */
} // :cond_6
try { // :try_start_3
/* new-instance v5, Ljava/lang/IllegalStateException; */
final String v7 = "no start tag found"; // const-string v7, "no start tag found"
/* invoke-direct {v5, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
} // .end local v1 # "stream":Ljava/io/InputStream;
} // .end local v2 # "success":Z
} // .end local v3 # "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
} // .end local p0 # "file":Landroid/util/AtomicFile;
/* throw v5 */
/* :try_end_3 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Ljava/lang/NullPointerException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 975 */
} // .end local v4 # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
} // .end local v6 # "type":I
/* .restart local v1 # "stream":Ljava/io/InputStream; */
/* .restart local v2 # "success":Z */
/* .restart local v3 # "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* .restart local p0 # "file":Landroid/util/AtomicFile; */
/* :catchall_0 */
/* move-exception v0 */
/* .line 971 */
/* :catch_1 */
/* move-exception v4 */
/* .line 973 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_4
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Failed parsing "; // const-string v6, "Failed parsing "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v5 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 976 */
} // .end local v4 # "e":Ljava/lang/Exception;
try { // :try_start_5
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 981 */
} // :goto_4
/* if-nez v2, :cond_7 */
/* .line 982 */
(( java.util.HashMap ) v3 ).clear ( ); // invoke-virtual {v3}, Ljava/util/HashMap;->clear()V
/* .line 984 */
} // :cond_7
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "read orientation settings: "; // const-string v5, "read orientation settings: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 985 */
/* .line 976 */
} // :goto_5
try { // :try_start_6
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_2 */
/* .line 979 */
/* .line 977 */
/* :catch_2 */
/* move-exception v4 */
/* .line 978 */
/* .local v4, "ignored":Ljava/io/IOException; */
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
/* .line 980 */
} // .end local v4 # "ignored":Ljava/io/IOException;
} // :goto_6
/* throw v0 */
/* .line 936 */
} // .end local v1 # "stream":Ljava/io/InputStream;
} // .end local v2 # "success":Z
} // .end local v3 # "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
/* :catch_3 */
/* move-exception v1 */
/* .line 937 */
/* .local v1, "e":Ljava/io/IOException; */
final String v2 = "No existing orientation settings, starting empty"; // const-string v2, "No existing orientation settings, starting empty"
android.util.Slog .i ( v0,v2 );
/* .line 938 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean retrieveScreenOrientationInner ( com.android.server.wm.ActivityRecord p0, android.content.pm.ActivityInfo p1, android.os.Bundle p2 ) {
/* .locals 6 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "activityInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p3, "appMetaData" # Landroid/os/Bundle; */
/* .line 678 */
final String v0 = "miui.screenInnerOrientation"; // const-string v0, "miui.screenInnerOrientation"
/* const-string/jumbo v1, "unset" */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 679 */
(( android.os.Bundle ) p3 ).getString ( v0, v1 ); // invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 681 */
} // :cond_0
/* nop */
} // :goto_0
/* nop */
/* .line 682 */
/* .local v1, "screenInnerOrientation":Ljava/lang/String; */
v2 = com.android.server.wm.MiuiOrientationImpl .screenOrientationFromString ( v1 );
/* .line 684 */
/* .local v2, "appSpecOrientation":I */
v3 = this.metaData;
final String v4 = "invalid"; // const-string v4, "invalid"
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 685 */
v3 = this.metaData;
(( android.os.Bundle ) v3 ).getString ( v0, v4 ); // invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 687 */
} // :cond_1
/* nop */
} // :goto_1
/* move-object v0, v4 */
/* .line 688 */
/* .local v0, "activityScreenOrientation":Ljava/lang/String; */
/* nop */
/* .line 689 */
v3 = com.android.server.wm.MiuiOrientationImpl .screenOrientationFromString ( v0 );
/* .line 690 */
/* .local v3, "activitySpecOrientation":I */
int v4 = -2; // const/4 v4, -0x2
int v5 = 1; // const/4 v5, 0x1
/* if-lt v3, v4, :cond_2 */
/* .line 691 */
v4 = this.mActivityRecordStub;
/* .line 692 */
/* .line 693 */
} // :cond_2
/* if-eq v2, v4, :cond_3 */
/* .line 694 */
v4 = this.mActivityRecordStub;
/* .line 695 */
/* .line 697 */
} // :cond_3
int v4 = 0; // const/4 v4, 0x0
} // .end method
public static Integer screenOrientationFromString ( java.lang.String p0 ) {
/* .locals 18 */
/* .param p0, "orientation" # Ljava/lang/String; */
/* .line 810 */
/* move-object/from16 v0, p0 */
v1 = /* invoke-virtual/range {p0 ..p0}, Ljava/lang/String;->hashCode()I */
/* const/16 v2, 0xe */
/* const/16 v3, 0xd */
/* const/16 v4, 0xc */
/* const/16 v5, 0xb */
/* const/16 v6, 0xa */
/* const/16 v7, 0x9 */
/* const/16 v8, 0x8 */
int v9 = 7; // const/4 v9, 0x7
int v10 = 6; // const/4 v10, 0x6
int v11 = 5; // const/4 v11, 0x5
int v12 = 4; // const/4 v12, 0x4
int v13 = 3; // const/4 v13, 0x3
int v14 = 2; // const/4 v14, 0x2
int v15 = 1; // const/4 v15, 0x1
/* const/16 v16, 0x0 */
/* const/16 v17, -0x1 */
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* goto/16 :goto_0 */
/* :sswitch_0 */
final String v1 = "nosensor"; // const-string v1, "nosensor"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v9 */
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v1 = "landscape"; // const-string v1, "landscape"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v14 */
/* goto/16 :goto_1 */
/* :sswitch_2 */
final String v1 = "fullUser"; // const-string v1, "fullUser"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xf */
/* goto/16 :goto_1 */
/* :sswitch_3 */
final String v1 = "portrait"; // const-string v1, "portrait"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v13 */
/* goto/16 :goto_1 */
/* :sswitch_4 */
/* const-string/jumbo v1, "unset" */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move/from16 v1, v16 */
/* goto/16 :goto_1 */
/* :sswitch_5 */
/* const-string/jumbo v1, "user" */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v12 */
/* goto/16 :goto_1 */
/* :sswitch_6 */
/* const-string/jumbo v1, "userPortrait" */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v2 */
/* goto/16 :goto_1 */
/* :sswitch_7 */
/* const-string/jumbo v1, "sensorPortrait" */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v7 */
/* goto/16 :goto_1 */
/* :sswitch_8 */
final String v1 = "reversePortrait"; // const-string v1, "reversePortrait"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v5 */
/* :sswitch_9 */
final String v1 = "fullSensor"; // const-string v1, "fullSensor"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v4 */
/* :sswitch_a */
/* const-string/jumbo v1, "userLandscape" */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v3 */
/* :sswitch_b */
/* const-string/jumbo v1, "sensor" */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v10 */
/* :sswitch_c */
final String v1 = "locked"; // const-string v1, "locked"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0x10 */
/* :sswitch_d */
final String v1 = "behind"; // const-string v1, "behind"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v11 */
/* :sswitch_e */
/* const-string/jumbo v1, "unspecified" */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v15 */
/* :sswitch_f */
final String v1 = "reverseLandscape"; // const-string v1, "reverseLandscape"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v6 */
/* :sswitch_10 */
/* const-string/jumbo v1, "sensorLandscape" */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v8 */
} // :goto_0
/* move/from16 v1, v17 */
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 846 */
int v1 = -3; // const/4 v1, -0x3
/* .line 844 */
/* :pswitch_0 */
/* .line 842 */
/* :pswitch_1 */
/* .line 840 */
/* :pswitch_2 */
/* .line 838 */
/* :pswitch_3 */
/* .line 836 */
/* :pswitch_4 */
/* .line 834 */
/* :pswitch_5 */
/* .line 832 */
/* :pswitch_6 */
/* .line 830 */
/* :pswitch_7 */
/* .line 828 */
/* :pswitch_8 */
/* .line 826 */
/* :pswitch_9 */
/* .line 824 */
/* :pswitch_a */
/* .line 822 */
/* :pswitch_b */
/* .line 820 */
/* :pswitch_c */
/* .line 818 */
/* :pswitch_d */
/* .line 816 */
/* :pswitch_e */
/* .line 814 */
/* :pswitch_f */
/* .line 812 */
/* :pswitch_10 */
int v1 = -2; // const/4 v1, -0x2
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x703eafdf -> :sswitch_10 */
/* -0x66042207 -> :sswitch_f */
/* -0x60ed74c9 -> :sswitch_e */
/* -0x5304eec6 -> :sswitch_d */
/* -0x4169ccf6 -> :sswitch_c */
/* -0x35ffac46 -> :sswitch_b */
/* -0x35a9c4d0 -> :sswitch_a */
/* -0x2ff104d7 -> :sswitch_9 */
/* -0x2d278f63 -> :sswitch_8 */
/* -0x1cf7e68b -> :sswitch_7 */
/* -0x24def7a -> :sswitch_6 */
/* 0x36ebcb -> :sswitch_5 */
/* 0x6a47b29 -> :sswitch_4 */
/* 0x2b77bb9b -> :sswitch_3 */
/* 0x4f56a2fa -> :sswitch_2 */
/* 0x5545f2bb -> :sswitch_1 */
/* 0x5c954cbb -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static void writeSettings ( android.util.AtomicFile p0, java.util.Map p1 ) {
/* .locals 12 */
/* .param p0, "file" # Landroid/util/AtomicFile; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/AtomicFile;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1005 */
/* .local p1, "settings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
final String v0 = "package"; // const-string v0, "package"
final String v1 = "orientation-settings"; // const-string v1, "orientation-settings"
final String v2 = "MiuiOrientationImpl"; // const-string v2, "MiuiOrientationImpl"
try { // :try_start_0
(( android.util.AtomicFile ) p0 ).startWrite ( ); // invoke-virtual {p0}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 1009 */
/* .local v3, "stream":Ljava/io/FileOutputStream; */
/* nop */
/* .line 1011 */
int v4 = 0; // const/4 v4, 0x0
/* .line 1013 */
/* .local v4, "success":Z */
try { // :try_start_1
android.util.Xml .resolveSerializer ( v3 );
/* .line 1014 */
/* .local v5, "out":Lcom/android/modules/utils/TypedXmlSerializer; */
int v6 = 1; // const/4 v6, 0x1
java.lang.Boolean .valueOf ( v6 );
int v7 = 0; // const/4 v7, 0x0
/* .line 1015 */
/* .line 1017 */
v8 = } // :goto_0
if ( v8 != null) { // if-eqz v8, :cond_0
/* check-cast v8, Ljava/util/Map$Entry; */
/* .line 1018 */
/* .local v8, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* check-cast v9, Ljava/lang/String; */
/* .line 1019 */
/* .local v9, "packageName":Ljava/lang/String; */
/* check-cast v10, Ljava/lang/Integer; */
v10 = (( java.lang.Integer ) v10 ).intValue ( ); // invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I
/* .line 1021 */
/* .local v10, "value":I */
/* .line 1022 */
final String v11 = "name"; // const-string v11, "name"
/* .line 1023 */
/* const-string/jumbo v11, "value" */
/* .line 1024 */
/* .line 1025 */
/* nop */
} // .end local v8 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
} // .end local v9 # "packageName":Ljava/lang/String;
} // .end local v10 # "value":I
/* .line 1027 */
} // :cond_0
/* .line 1028 */
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1029 */
int v4 = 1; // const/4 v4, 0x1
/* .line 1033 */
} // .end local v5 # "out":Lcom/android/modules/utils/TypedXmlSerializer;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1034 */
} // :goto_1
(( android.util.AtomicFile ) p0 ).finishWrite ( v3 ); // invoke-virtual {p0, v3}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* .line 1036 */
} // :cond_1
(( android.util.AtomicFile ) p0 ).failWrite ( v3 ); // invoke-virtual {p0, v3}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 1038 */
/* .line 1033 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 1030 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1031 */
/* .local v0, "e":Ljava/io/IOException; */
try { // :try_start_2
final String v1 = "Failed to write orientation user settings."; // const-string v1, "Failed to write orientation user settings."
android.util.Slog .w ( v2,v1,v0 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1033 */
/* nop */
} // .end local v0 # "e":Ljava/io/IOException;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1034 */
/* .line 1039 */
} // :goto_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "write orientation settings: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 1040 */
return;
/* .line 1033 */
} // :goto_3
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1034 */
(( android.util.AtomicFile ) p0 ).finishWrite ( v3 ); // invoke-virtual {p0, v3}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* .line 1036 */
} // :cond_2
(( android.util.AtomicFile ) p0 ).failWrite ( v3 ); // invoke-virtual {p0, v3}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 1038 */
} // :goto_4
/* throw v0 */
/* .line 1006 */
} // .end local v3 # "stream":Ljava/io/FileOutputStream;
} // .end local v4 # "success":Z
/* :catch_1 */
/* move-exception v0 */
/* .line 1007 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to write orientation settings: "; // const-string v3, "Failed to write orientation settings: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v1 );
/* .line 1008 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2, Integer p3, Boolean p4, Boolean p5, java.lang.String p6 ) {
/* .locals 2 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .param p4, "opti" # I */
/* .param p5, "dumpAll" # Z */
/* .param p6, "dumpClient" # Z */
/* .param p7, "dumpPackage" # Ljava/lang/String; */
/* .line 895 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
/* if-nez v0, :cond_0 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isSmartOrientEnable ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z
/* if-nez v0, :cond_0 */
/* .line 896 */
final String v0 = "miuiSmartRotation && miuiSmartOrientation not enabled"; // const-string v0, "miuiSmartRotation && miuiSmartOrientation not enabled"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 897 */
return;
/* .line 899 */
} // :cond_0
final String v0 = "miuiSmartRotation PACKAGE SETTINGS MANAGER"; // const-string v0, "miuiSmartRotation PACKAGE SETTINGS MANAGER"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 900 */
v0 = this.mFullScreenPackageManager;
final String v1 = ""; // const-string v1, ""
com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager .-$$Nest$mdump ( v0,p2,v1 );
/* .line 901 */
(( java.io.PrintWriter ) p2 ).println ( ); // invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V
/* .line 902 */
v0 = this.mSmartOrientationPolicy;
(( com.android.server.wm.MiuiOrientationImpl$SmartOrientationPolicy ) v0 ).dump ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 903 */
(( java.io.PrintWriter ) p2 ).println ( ); // invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V
/* .line 904 */
return;
} // .end method
public Integer getOrientationMode ( com.android.server.wm.ActivityRecord p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "orientation" # I */
/* .line 286 */
int v0 = -1; // const/4 v0, -0x1
/* .line 287 */
/* .local v0, "mode":I */
v1 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
/* if-nez v1, :cond_0 */
/* sget-boolean v1, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 288 */
} // :cond_0
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isDisplayFolded(Lcom/android/server/wm/ActivityRecord;)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isFlipDevice ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFlipDevice()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 289 */
} // :cond_1
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z */
if ( v1 != null) { // if-eqz v1, :cond_3
} // :cond_2
/* .line 290 */
} // :cond_3
v1 = this.mActivityComponent;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 291 */
/* .local v1, "packageName":Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 292 */
int v0 = 1; // const/4 v0, 0x1
/* .line 293 */
} // :cond_4
v2 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isFlipDevice ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFlipDevice()Z
/* if-nez v2, :cond_6 */
v2 = this.mServiceImpl;
v3 = this.packageName;
v2 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v2 ).inMiuiGameSizeCompat ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->inMiuiGameSizeCompat(Ljava/lang/String;)Z
/* if-nez v2, :cond_5 */
/* .line 294 */
v2 = android.content.pm.ActivityInfo .isFixedOrientationLandscape ( p2 );
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 295 */
} // :cond_5
int v0 = -1; // const/4 v0, -0x1
/* .line 296 */
} // :cond_6
/* sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v2 != null) { // if-eqz v2, :cond_7
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 297 */
int v0 = 1; // const/4 v0, 0x1
/* .line 298 */
} // :cond_7
v2 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isFlipDevice ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFlipDevice()Z
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 299 */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isDisplayFolded(Lcom/android/server/wm/ActivityRecord;)Z */
if ( v2 != null) { // if-eqz v2, :cond_9
v2 = android.content.pm.ActivityInfo .isFixedOrientationLandscape ( p2 );
/* if-nez v2, :cond_9 */
/* .line 300 */
int v0 = 3; // const/4 v0, 0x3
/* .line 303 */
} // :cond_8
v0 = /* invoke-direct {p0, p1, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->getOrientationMode(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;)I */
/* .line 306 */
} // :cond_9
} // :goto_0
v2 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_ORIENTATION;
v2 = (( com.android.internal.protolog.ProtoLogGroup ) v2 ).isLogToLogcat ( ); // invoke-virtual {v2}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 307 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getOrientationMode packageName="; // const-string v3, "getOrientationMode packageName="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " mode="; // const-string v3, " mode="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 308 */
com.android.server.wm.MiuiOrientationImpl .fullScreenModeToString ( v0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 307 */
final String v3 = "MiuiOrientationImpl"; // const-string v3, "MiuiOrientationImpl"
android.util.Slog .d ( v3,v2 );
/* .line 309 */
} // :cond_a
} // .end method
public com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager getPackageManager ( ) {
/* .locals 1 */
/* .line 239 */
v0 = this.mFullScreenPackageManager;
} // .end method
public Integer getRelaunchMode ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 5 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 603 */
v0 = this.mActivityComponent;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 604 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getRelaunchModeFromBundle(Ljava/lang/String;)I */
/* .line 605 */
/* .local v1, "relaunchMode":I */
v2 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
/* if-nez v2, :cond_4 */
v2 = this.mAtmService;
v2 = this.mWindowManager;
v2 = this.mPolicy;
v2 = /* .line 607 */
/* if-nez v2, :cond_4 */
/* .line 608 */
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z
/* if-nez v2, :cond_4 */
/* .line 609 */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z */
/* if-nez v2, :cond_4 */
/* .line 610 */
v2 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 611 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 612 */
/* iget v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRelaunchModeFromPolicy(I)I */
/* .line 614 */
} // :cond_1
v2 = this.mFullScreenPackageManager;
v3 = this.mActivityComponent;
/* .line 615 */
(( android.content.ComponentName ) v3 ).flattenToShortString ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v2 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v2 ).getOrientationPolicyByComponent ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I
/* .line 616 */
/* .local v2, "policy":I */
int v3 = -1; // const/4 v3, -0x1
/* if-eq v3, v2, :cond_2 */
/* .line 617 */
v1 = /* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRelaunchModeFromPolicy(I)I */
/* .line 619 */
} // :cond_2
v4 = this.mFullScreenPackageManager;
v2 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v4 ).getOrientationPolicy ( v0 ); // invoke-virtual {v4, v0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 620 */
/* if-eq v3, v2, :cond_3 */
/* .line 621 */
v1 = /* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRelaunchModeFromPolicy(I)I */
/* .line 624 */
} // :cond_3
} // :goto_0
/* .line 610 */
} // .end local v2 # "policy":I
} // :cond_4
} // :goto_1
} // .end method
public Integer getRotationOptions ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 6 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 651 */
v0 = this.mActivityComponent;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 652 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptionsFromBundle(Ljava/lang/String;)I */
/* .line 653 */
/* .local v1, "rotationOptions":I */
v2 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
/* if-nez v2, :cond_4 */
v2 = this.mAtmService;
v2 = this.mWindowManager;
v2 = this.mPolicy;
v2 = /* .line 655 */
/* if-nez v2, :cond_4 */
/* .line 656 */
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z
/* if-nez v2, :cond_4 */
/* .line 657 */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z */
/* if-nez v2, :cond_4 */
/* .line 658 */
v2 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 659 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 660 */
/* iget v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptionsFromPolicy(I)I */
/* .line 662 */
} // :cond_1
v2 = this.mFullScreenPackageManager;
v3 = this.mActivityComponent;
/* .line 663 */
(( android.content.ComponentName ) v3 ).flattenToShortString ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v2 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v2 ).getOrientationPolicyByComponent ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I
/* .line 664 */
/* .local v2, "policy":I */
int v3 = -1; // const/4 v3, -0x1
/* if-eq v3, v2, :cond_2 */
/* .line 665 */
v1 = /* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptionsFromPolicy(I)I */
/* .line 667 */
} // :cond_2
v4 = this.mFullScreenPackageManager;
v5 = this.mActivityComponent;
(( android.content.ComponentName ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v2 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v4 ).getOrientationPolicy ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 668 */
/* if-eq v3, v2, :cond_3 */
/* .line 669 */
v1 = /* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptionsFromPolicy(I)I */
/* .line 672 */
} // :cond_3
} // :goto_0
/* .line 658 */
} // .end local v2 # "policy":I
} // :cond_4
} // :goto_1
} // .end method
public Integer getShowRotationSuggestion ( ) {
/* .locals 1 */
/* .line 280 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_FOLD:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 281 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mShowRotationSuggestion:I */
/* .line 280 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void gotoDemoDebugMode ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "value" # Ljava/lang/String; */
/* .line 907 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
/* if-nez v0, :cond_0 */
/* .line 908 */
final String v0 = "miuiSmartRotation not enabled"; // const-string v0, "miuiSmartRotation not enabled"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 909 */
return;
/* .line 911 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z */
/* xor-int/lit8 v0, v0, 0x1 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z */
/* .line 912 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I */
/* .line 913 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 914 */
v0 = com.android.server.wm.MiuiOrientationImpl .parseFullScreenPolicy ( p2 );
/* iput v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I */
/* .line 916 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "goto or leave debug mode="; // const-string v1, "goto or leave debug mode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " policy="; // const-string v1, " policy="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I */
/* .line 917 */
com.android.server.wm.MiuiOrientationImpl .fullScreenPolicyToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 916 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 918 */
return;
} // .end method
public Boolean inFullScreenRelaunchMode ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1640 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p1, Lcom/android/server/wm/ActivityRecord;->mRelaunchInteractive:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void init ( android.content.Context p0, com.android.server.wm.ActivityTaskManagerServiceImpl p1, com.android.server.wm.ActivityTaskManagerService p2 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "serviceImpl" # Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
/* .param p3, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 179 */
this.mContext = p1;
/* .line 180 */
this.mServiceImpl = p2;
/* .line 181 */
this.mAtmService = p3;
/* .line 182 */
/* new-instance v0, Landroid/os/Handler; */
com.android.server.MiuiFgThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 183 */
/* new-instance v0, Landroid/os/Handler; */
com.android.server.MiuiBgThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mFileHandler = v0;
/* .line 184 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/os/Handler;)V */
this.mMiuiSettingsObserver = v0;
/* .line 185 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V */
this.mFullScreenPackageManager = v0;
/* .line 186 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V */
this.mFullScreen3AppCameraStrategy = v0;
/* .line 187 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy; */
v1 = this.mContext;
v2 = this.mServiceImpl;
/* invoke-direct {v0, p0, v1, v2}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V */
this.mSmartOrientationPolicy = v0;
/* .line 188 */
v0 = this.mContext;
final String v1 = ""; // const-string v1, ""
int v2 = 1; // const/4 v2, 0x1
android.widget.Toast .makeText ( v0,v1,v2 );
this.mCompatLandToast = v0;
/* .line 189 */
com.android.server.wm.MiuiOrientationImpl .getSettingsFile ( );
this.mFile = v0;
/* .line 190 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 191 */
/* const v1, 0x110b003e */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mShowRotationSuggestion:I */
/* .line 192 */
final String v0 = "ro.config.miui_smart_orientation_enable"; // const-string v0, "ro.config.miui_smart_orientation_enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.MiuiOrientationImpl.SMART_ORIENTATION_ENABLE = (v0!= 0);
/* .line 193 */
return;
} // .end method
public Boolean isEnabled ( ) {
/* .locals 1 */
/* .line 244 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartRotationEnabled:Z */
} // .end method
public Boolean isFlipDevice ( ) {
/* .locals 1 */
/* .line 806 */
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
} // .end method
public Boolean isIgnoreRequestedOrientation ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 4 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 258 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = this.mAtmService;
v0 = this.mWindowManager;
v0 = v0 = this.mPolicy;
/* if-nez v0, :cond_0 */
/* .line 261 */
} // :cond_0
v0 = this.intent;
v2 = this.mAtmService;
v2 = this.mContext;
/* .line 262 */
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 261 */
/* const/16 v3, 0x80 */
(( android.content.Intent ) v0 ).resolveActivityInfo ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;
/* .line 264 */
/* .local v0, "activityInfo":Landroid/content/pm/ActivityInfo; */
/* if-nez v0, :cond_1 */
/* .line 266 */
} // :cond_1
v2 = this.applicationInfo;
v2 = this.metaData;
/* .line 267 */
/* .local v2, "appMetaData":Landroid/os/Bundle; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 268 */
final String v3 = "miui.isIgnoreOrientationRequest"; // const-string v3, "miui.isIgnoreOrientationRequest"
v3 = (( android.os.Bundle ) v2 ).getBoolean ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
/* .line 269 */
} // :cond_2
/* move v3, v1 */
} // :goto_0
/* nop */
/* .line 270 */
/* .local v3, "ignoreOrientationRequest":Z */
if ( v3 != null) { // if-eqz v3, :cond_3
int v1 = 1; // const/4 v1, 0x1
/* .line 271 */
} // :cond_3
/* .line 259 */
} // .end local v0 # "activityInfo":Landroid/content/pm/ActivityInfo;
} // .end local v2 # "appMetaData":Landroid/os/Bundle;
} // .end local v3 # "ignoreOrientationRequest":Z
} // :cond_4
} // :goto_1
} // .end method
public Boolean isInOuterEnableList ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 252 */
v0 = this.mActivityComponent;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v0 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getOuterOrientationMode(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;)I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public Boolean isLandEnableForPackage ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 459 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 460 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Ljava/lang/String;)Z */
/* if-nez v0, :cond_2 */
/* .line 461 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 463 */
} // :cond_0
v0 = this.mFullScreenPackageManager;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).getOrientationMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationMode(Ljava/lang/String;)I
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
/* move v1, v2 */
} // :cond_1
/* .line 461 */
} // :cond_2
} // :goto_0
} // .end method
public Boolean isNeedAccSensor ( ) {
/* .locals 3 */
/* .line 797 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isSmartOrientEnable ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mLastResumeApp;
/* if-nez v0, :cond_0 */
/* .line 798 */
} // :cond_0
v0 = this.mActivityComponent;
(( android.content.ComponentName ) v0 ).flattenToShortString ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* .line 799 */
/* .local v0, "activityName":Ljava/lang/String; */
v2 = this.mSmartOrientationPolicy;
v2 = (( com.android.server.wm.MiuiOrientationImpl$SmartOrientationPolicy ) v2 ).isNeedAccSensor ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->isNeedAccSensor(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 800 */
int v1 = 1; // const/4 v1, 0x1
/* .line 802 */
} // :cond_1
/* .line 797 */
} // .end local v0 # "activityName":Ljava/lang/String;
} // :cond_2
} // :goto_0
} // .end method
public Boolean isNeedRotateCameraInFullScreen ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 408 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
/* .line 409 */
} // :cond_2
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotate(Ljava/lang/String;)Z */
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mAtmService;
v0 = this.mWindowManager;
v0 = this.mPolicy;
v0 = /* .line 410 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 411 */
} // :cond_3
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotateInPad(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_5
} // :cond_4
/* move v0, v2 */
} // :cond_5
/* move v0, v1 */
/* .line 412 */
/* .local v0, "rotate3AppCamera":Z */
} // :goto_0
/* if-nez v0, :cond_6 */
v3 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isLandEnableForPackage ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnableForPackage(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_7
} // :cond_6
/* move v1, v2 */
} // :cond_7
} // .end method
public Integer isNeedSetDisplayCutout ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 874 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
/* .line 875 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 879 */
} // :cond_0
v0 = this.mFullScreenPackageManager;
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).getOrientationPolicy ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 880 */
/* .local v0, "policy":I */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getDisplayCutoutSetting(I)I */
/* .line 876 */
} // .end local v0 # "policy":I
} // :cond_1
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // .end method
public Boolean isNeedUpdateConfig ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 7 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 379 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 380 */
} // :cond_0
v1 = this.mActivityComponent;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 381 */
/* .local v1, "packageName":Ljava/lang/String; */
/* sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 382 */
v2 = this.mEmbeddedFullRules;
/* check-cast v2, Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* .line 383 */
/* .local v2, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
/* iget-boolean v4, v2, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mUc:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 385 */
} // .end local v2 # "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
} // :cond_1
v2 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
/* if-nez v2, :cond_5 */
v2 = this.mAtmService;
v2 = this.mWindowManager;
v2 = this.mPolicy;
v2 = /* .line 386 */
/* if-nez v2, :cond_5 */
/* .line 387 */
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z
/* if-nez v2, :cond_5 */
/* .line 389 */
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).inMiuiHoverWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMiuiHoverWindowingMode()Z
/* if-nez v2, :cond_5 */
/* .line 391 */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z */
/* if-nez v2, :cond_5 */
/* .line 392 */
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 393 */
} // :cond_2
v2 = this.mFullScreenPackageManager;
v4 = this.mActivityComponent;
/* .line 394 */
(( android.content.ComponentName ) v4 ).flattenToShortString ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v2 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v2 ).getOrientationPolicyByComponent ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I
/* .line 395 */
/* .local v2, "activityPolicy":I */
int v4 = -1; // const/4 v4, -0x1
/* if-eq v4, v2, :cond_3 */
/* and-int/lit16 v5, v2, 0x800 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 397 */
/* .line 400 */
} // :cond_3
v5 = this.mFullScreenPackageManager;
v6 = this.mActivityComponent;
/* .line 401 */
(( android.content.ComponentName ) v6 ).getPackageName ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v5 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v5 ).getOrientationPolicy ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 402 */
/* .local v5, "policy":I */
/* if-eq v4, v5, :cond_4 */
/* and-int/lit16 v4, v5, 0x800 */
if ( v4 != null) { // if-eqz v4, :cond_4
/* move v0, v3 */
} // :cond_4
/* .line 392 */
} // .end local v2 # "activityPolicy":I
} // .end local v5 # "policy":I
} // :cond_5
} // :goto_0
} // .end method
public Boolean isSmartOrientEnable ( ) {
/* .locals 1 */
/* .line 248 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->SMART_ORIENTATION_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_MIUI_OPTIMIZATION:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean needUpdateOrientationForPad ( ) {
/* .locals 3 */
/* .line 1601 */
v0 = this.mAtmService;
v0 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v0 ).getTopResumedActivity ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getTopResumedActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 1602 */
/* .local v0, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = this.packageName;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1603 */
/* .local v1, "packageName":Ljava/lang/String; */
} // :goto_0
/* sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
/* if-nez v2, :cond_1 */
/* .line 1604 */
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1605 */
int v2 = 1; // const/4 v2, 0x1
/* .line 1607 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // .end method
void onSystemReady ( ) {
/* .locals 2 */
/* .line 196 */
v0 = this.mSmartOrientationPolicy;
(( com.android.server.wm.MiuiOrientationImpl$SmartOrientationPolicy ) v0 ).init ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->init()V
/* .line 197 */
v0 = this.mMiuiSettingsObserver;
(( com.android.server.wm.MiuiOrientationImpl$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->observe()V
/* .line 198 */
v0 = this.mFile;
com.android.server.wm.MiuiOrientationImpl .readSettings ( v0 );
/* .line 199 */
/* .local v0, "settings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez v1, :cond_0 */
/* .line 200 */
v1 = this.mPackagesMapByUserSettings;
/* .line 202 */
} // :cond_0
return;
} // .end method
public void overrideOrientationBeforeSensorChanged ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1620 */
/* iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOrientationOptions:I */
int v1 = 1; // const/4 v1, 0x1
/* and-int/2addr v0, v1 */
/* if-nez v0, :cond_0 */
/* move v0, v1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1622 */
/* .local v0, "nonIgnoreSensor":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).setOrientationForAppRequest ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->setOrientationForAppRequest()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1623 */
return;
/* .line 1626 */
} // :cond_1
v2 = this.mDisplayContent;
(( com.android.server.wm.DisplayContent ) v2 ).getDisplayRotation ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayRotation()Lcom/android/server/wm/DisplayRotation;
v2 = (( com.android.server.wm.DisplayRotation ) v2 ).getUserRotationMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayRotation;->getUserRotationMode()I
/* if-ne v2, v1, :cond_2 */
/* .line 1628 */
v2 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).getRotationOptions ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptions(Lcom/android/server/wm/ActivityRecord;)I
/* and-int/2addr v1, v2 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1629 */
return;
/* .line 1632 */
} // :cond_2
/* const-string/jumbo v1, "sensor" */
(( com.android.server.wm.ActivityRecord ) p1 ).overrideOrRestoreOrientationIfNeed ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/ActivityRecord;->overrideOrRestoreOrientationIfNeed(Ljava/lang/String;)V
/* .line 1633 */
return;
} // .end method
public void overrideOrientationForFoldChanged ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1613 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).setOrientationForAppRequest ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->setOrientationForAppRequest()Z
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 1614 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_FOLD:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
/* if-nez v0, :cond_1 */
/* .line 1616 */
} // :cond_1
final String v0 = "fold"; // const-string v0, "fold"
(( com.android.server.wm.ActivityRecord ) p1 ).overrideOrRestoreOrientationIfNeed ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/ActivityRecord;->overrideOrRestoreOrientationIfNeed(Ljava/lang/String;)V
/* .line 1617 */
return;
/* .line 1614 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void reportEvent ( com.android.server.wm.DisplayContent p0, Integer p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "mDisplayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "oldRotation" # I */
/* .param p3, "rotation" # I */
/* .line 1583 */
(( com.android.server.wm.DisplayContent ) p1 ).getLastOrientationSource ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getLastOrientationSource()Lcom/android/server/wm/WindowContainer;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1584 */
(( com.android.server.wm.DisplayContent ) p1 ).getLastOrientationSource ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getLastOrientationSource()Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v0 ).asActivityRecord ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1585 */
/* .local v0, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
} // :goto_0
(( com.android.server.wm.DisplayContent ) p1 ).getDisplayRotation ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayRotation()Lcom/android/server/wm/DisplayRotation;
/* .line 1586 */
/* .local v1, "displayRotation":Lcom/android/server/wm/DisplayRotation; */
v2 = (( com.android.server.wm.DisplayRotation ) v1 ).isAnyPortrait ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/wm/DisplayRotation;->isAnyPortrait(I)Z
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1587 */
v2 = (( com.android.server.wm.DisplayRotation ) v1 ).isLandscapeOrSeascape ( p3 ); // invoke-virtual {v1, p3}, Lcom/android/server/wm/DisplayRotation;->isLandscapeOrSeascape(I)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v3 */
} // :cond_1
/* move v2, v4 */
/* .line 1588 */
/* .local v2, "isToLandscape":Z */
} // :goto_1
v5 = (( com.android.server.wm.DisplayRotation ) v1 ).isLandscapeOrSeascape ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/wm/DisplayRotation;->isLandscapeOrSeascape(I)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1589 */
v5 = (( com.android.server.wm.DisplayRotation ) v1 ).isAnyPortrait ( p3 ); // invoke-virtual {v1, p3}, Lcom/android/server/wm/DisplayRotation;->isAnyPortrait(I)Z
if ( v5 != null) { // if-eqz v5, :cond_2
} // :cond_2
/* move v3, v4 */
/* .line 1590 */
/* .local v3, "isToPortrait":Z */
} // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1591 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1592 */
v4 = this.mAtmService;
/* const/16 v5, 0x20 */
(( com.android.server.wm.ActivityTaskManagerService ) v4 ).updateActivityUsageStats ( v0, v5 ); // invoke-virtual {v4, v0, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->updateActivityUsageStats(Lcom/android/server/wm/ActivityRecord;I)V
/* .line 1594 */
} // :cond_3
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1595 */
v4 = this.mAtmService;
/* const/16 v5, 0x21 */
(( com.android.server.wm.ActivityTaskManagerService ) v4 ).updateActivityUsageStats ( v0, v5 ); // invoke-virtual {v4, v0, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->updateActivityUsageStats(Lcom/android/server/wm/ActivityRecord;I)V
/* .line 1598 */
} // :cond_4
return;
} // .end method
public void retrieveScreenOrientation ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 9 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 702 */
v0 = this.intent;
v1 = this.mAtmService;
v1 = this.mContext;
/* .line 703 */
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 702 */
/* const/16 v2, 0x80 */
(( android.content.Intent ) v0 ).resolveActivityInfo ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;
/* .line 705 */
/* .local v0, "activityInfo":Landroid/content/pm/ActivityInfo; */
/* if-nez v0, :cond_0 */
return;
/* .line 707 */
} // :cond_0
v1 = this.applicationInfo;
v1 = this.metaData;
/* .line 708 */
/* .local v1, "appMetaData":Landroid/os/Bundle; */
v2 = /* invoke-direct {p0, p1, v0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->retrieveScreenOrientationInner(Lcom/android/server/wm/ActivityRecord;Landroid/content/pm/ActivityInfo;Landroid/os/Bundle;)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
return;
/* .line 710 */
} // :cond_1
final String v2 = "miui.screenOrientation"; // const-string v2, "miui.screenOrientation"
/* const-string/jumbo v3, "unset" */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 711 */
(( android.os.Bundle ) v1 ).getString ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 713 */
} // :cond_2
/* move-object v4, v3 */
} // :goto_0
/* nop */
/* .line 714 */
/* .local v4, "appScreenOrientation":Ljava/lang/String; */
v5 = this.metaData;
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 715 */
v5 = this.metaData;
(( android.os.Bundle ) v5 ).getString ( v2, v3 ); // invoke-virtual {v5, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 717 */
} // :cond_3
/* move-object v2, v3 */
} // :goto_1
/* nop */
/* .line 719 */
/* .local v2, "activityScreenOrientation":Ljava/lang/String; */
v5 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_5 */
/* .line 720 */
/* nop */
/* .line 721 */
com.android.server.wm.MiuiOrientationImpl .parseAppRequestOrientation ( v2 );
/* .line 722 */
/* .local v3, "activitySpecOrientation":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;" */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_2
v6 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v5, v6, :cond_4 */
/* .line 723 */
v6 = (( android.util.SparseArray ) v3 ).keyAt ( v5 ); // invoke-virtual {v3, v5}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 724 */
/* .local v6, "key":I */
v7 = this.mActivityRecordStub;
/* .line 725 */
(( android.util.SparseArray ) v3 ).get ( v6 ); // invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v8, Ljava/lang/Integer; */
v8 = (( java.lang.Integer ) v8 ).intValue ( ); // invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I
/* .line 724 */
/* .line 722 */
} // .end local v6 # "key":I
/* add-int/lit8 v5, v5, 0x1 */
} // .end local v3 # "activitySpecOrientation":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;"
} // .end local v5 # "i":I
} // :cond_4
/* .line 727 */
} // :cond_5
v3 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_6 */
/* .line 728 */
/* nop */
/* .line 729 */
com.android.server.wm.MiuiOrientationImpl .parseAppRequestOrientation ( v4 );
/* .line 730 */
/* .local v3, "appSpecOrientation":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;" */
int v5 = 0; // const/4 v5, 0x0
/* .restart local v5 # "i":I */
} // :goto_3
v6 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v5, v6, :cond_7 */
/* .line 731 */
v6 = (( android.util.SparseArray ) v3 ).keyAt ( v5 ); // invoke-virtual {v3, v5}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 732 */
/* .restart local v6 # "key":I */
v7 = this.mActivityRecordStub;
/* .line 733 */
(( android.util.SparseArray ) v3 ).get ( v6 ); // invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v8, Ljava/lang/Integer; */
v8 = (( java.lang.Integer ) v8 ).intValue ( ); // invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I
/* .line 732 */
/* .line 730 */
} // .end local v6 # "key":I
/* add-int/lit8 v5, v5, 0x1 */
/* .line 727 */
} // .end local v3 # "appSpecOrientation":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;"
} // .end local v5 # "i":I
} // :cond_6
} // :goto_4
/* nop */
/* .line 736 */
} // :cond_7
return;
} // .end method
public void setEmbeddedFullRuleData ( java.util.Map p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;>;)V" */
/* } */
} // .end annotation
/* .line 330 */
/* .local p1, "fullRuleMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;>;" */
v0 = this.mEmbeddedFullRules;
/* .line 331 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V */
/* .line 376 */
return;
} // .end method
public void setOrientationOptions ( com.android.server.wm.ActivityRecord p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "options" # I */
/* .line 1636 */
/* iput p2, p1, Lcom/android/server/wm/ActivityRecord;->mOrientationOptions:I */
/* .line 1637 */
return;
} // .end method
public void setUserSettings ( java.io.PrintWriter p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "value" # Ljava/lang/String; */
/* .line 231 */
v0 = this.mFullScreenPackageManager;
(( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).setUserSettings ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->setUserSettings(Ljava/lang/String;Ljava/lang/String;)V
/* .line 232 */
return;
} // .end method
public void setUserSettings ( java.io.PrintWriter p0, java.lang.String[] p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "pkgNames" # [Ljava/lang/String; */
/* .param p3, "value" # Ljava/lang/String; */
/* .line 235 */
v0 = this.mFullScreenPackageManager;
(( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v0 ).setUserSettings ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->setUserSettings([Ljava/lang/String;Ljava/lang/String;)V
/* .line 236 */
return;
} // .end method
public void showToastWhenLandscapeIfNeed ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 765 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->needShowToast(Lcom/android/server/wm/ActivityRecord;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I */
/* .line 766 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).getOrientationMode ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getOrientationMode(Lcom/android/server/wm/ActivityRecord;I)I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 768 */
com.android.server.DisplayThread .getHandler ( );
/* new-instance v1, Lcom/android/server/wm/MiuiOrientationImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiOrientationImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 773 */
} // :cond_0
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mToastVisible:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 775 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z
/* if-nez v0, :cond_1 */
v0 = com.android.server.wm.ActivityRecord$State.RESUMED;
/* .line 776 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).isState ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 777 */
com.android.server.DisplayThread .getHandler ( );
/* new-instance v1, Lcom/android/server/wm/MiuiOrientationImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiOrientationImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 782 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void update3appCameraWhenResume ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 852 */
/* if-nez p1, :cond_0 */
return;
/* .line 853 */
} // :cond_0
this.mLastResumeApp = p1;
/* .line 854 */
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 855 */
} // :cond_1
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z */
/* if-nez v0, :cond_3 */
v0 = this.packageName;
/* .line 856 */
v0 = miui.app.MiuiFreeFormManager .openCameraInFreeForm ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 857 */
} // :cond_2
v0 = this.mFullScreen3AppCameraStrategy;
(( com.android.server.wm.MiuiOrientationImpl$FullScreen3AppCameraStrategy ) v0 ).update3appCameraRotate ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->update3appCameraRotate(Lcom/android/server/wm/ActivityRecord;)V
/* .line 858 */
v0 = this.mFullScreen3AppCameraStrategy;
v1 = this.mDisplayContent;
/* .line 859 */
v1 = (( com.android.server.wm.DisplayContent ) v1 ).getRotation ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getRotation()I
/* .line 858 */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.MiuiOrientationImpl$FullScreen3AppCameraStrategy ) v0 ).update3appCameraRotation ( p1, v1, v2, v2 ); // invoke-virtual {v0, p1, v1, v2, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->update3appCameraRotation(Lcom/android/server/wm/ActivityRecord;IZZ)V
/* .line 860 */
return;
/* .line 856 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void update3appCameraWhenRotateOrFold ( com.android.server.wm.ActivityRecord p0, Integer p1, Boolean p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "rotation" # I */
/* .param p3, "isRotated" # Z */
/* .param p4, "isFoldChanged" # Z */
/* .line 865 */
/* if-nez p1, :cond_0 */
return;
/* .line 866 */
} // :cond_0
v0 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 867 */
} // :cond_1
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z */
/* if-nez v0, :cond_3 */
v0 = this.packageName;
/* .line 868 */
v0 = miui.app.MiuiFreeFormManager .openCameraInFreeForm ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 869 */
} // :cond_2
v0 = this.mFullScreen3AppCameraStrategy;
/* .line 870 */
(( com.android.server.wm.MiuiOrientationImpl$FullScreen3AppCameraStrategy ) v0 ).update3appCameraRotation ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->update3appCameraRotation(Lcom/android/server/wm/ActivityRecord;IZZ)V
/* .line 871 */
return;
/* .line 868 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void updateApplicationInfo ( android.content.pm.ApplicationInfo p0 ) {
/* .locals 4 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .line 785 */
v0 = this.packageName;
/* .line 786 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
return;
/* .line 787 */
} // :cond_0
v1 = this.mFullScreenPackageManager;
v1 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) v1 ).getOrientationPolicy ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I
/* .line 788 */
/* .local v1, "packagePolicy":I */
v2 = (( com.android.server.wm.MiuiOrientationImpl ) p0 ).isSmartOrientEnable ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = this.mSmartOrientationPolicy;
v2 = (( com.android.server.wm.MiuiOrientationImpl$SmartOrientationPolicy ) v2 ).isSupportSmartOrientation ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->isSupportSmartOrientation(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 789 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isSupportSmartOrientation packageName = "; // const-string v3, "isSupportSmartOrientation packageName = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiOrientationImpl"; // const-string v3, "MiuiOrientationImpl"
android.util.Slog .d ( v3,v2 );
/* .line 790 */
/* iget v2, p1, Landroid/content/pm/ApplicationInfo;->privateFlagsExt:I */
/* const/high16 v3, 0x200000 */
/* or-int/2addr v2, v3 */
/* iput v2, p1, Landroid/content/pm/ApplicationInfo;->privateFlagsExt:I */
/* .line 792 */
} // :cond_1
/* iget v2, p1, Landroid/content/pm/ApplicationInfo;->privateFlagsExt:I */
/* const v3, -0x200001 */
/* and-int/2addr v2, v3 */
/* iput v2, p1, Landroid/content/pm/ApplicationInfo;->privateFlagsExt:I */
/* .line 794 */
} // :goto_0
return;
} // .end method
