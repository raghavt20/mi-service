class com.android.server.wm.AppTransitionInjector$2 implements android.view.animation.Animation$AnimationListener {
	 /* .source "AppTransitionInjector.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/AppTransitionInjector;->addAnimationListener(Landroid/view/animation/Animation;Landroid/os/Handler;Landroid/os/IRemoteCallback;Landroid/os/IRemoteCallback;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final android.os.IRemoteCallback val$exitFinishCallback; //synthetic
final android.os.IRemoteCallback val$exitStartCallback; //synthetic
final android.os.Handler val$handler; //synthetic
/* # direct methods */
public static void $r8$lambda$sMT_oQ8Ga3BYZGB9ahzbMMbI2eo ( android.os.IRemoteCallback p0 ) { //synthethic
/* .locals 0 */
com.android.server.wm.AppTransitionInjector .-$$Nest$smdoAnimationCallback ( p0 );
return;
} // .end method
 com.android.server.wm.AppTransitionInjector$2 ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 385 */
this.val$exitStartCallback = p1;
this.val$handler = p2;
this.val$exitFinishCallback = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAnimationEnd ( android.view.animation.Animation p0 ) {
/* .locals 3 */
/* .param p1, "animation" # Landroid/view/animation/Animation; */
/* .line 395 */
v0 = this.val$exitFinishCallback;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 396 */
v0 = this.val$handler;
/* new-instance v1, Lcom/android/server/wm/AppTransitionInjector$2$$ExternalSyntheticLambda0; */
/* invoke-direct {v1}, Lcom/android/server/wm/AppTransitionInjector$2$$ExternalSyntheticLambda0;-><init>()V */
v2 = this.val$exitFinishCallback;
com.android.internal.util.function.pooled.PooledLambda .obtainMessage ( v1,v2 );
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 399 */
} // :cond_0
return;
} // .end method
public void onAnimationRepeat ( android.view.animation.Animation p0 ) {
/* .locals 0 */
/* .param p1, "animation" # Landroid/view/animation/Animation; */
/* .line 403 */
return;
} // .end method
public void onAnimationStart ( android.view.animation.Animation p0 ) {
/* .locals 3 */
/* .param p1, "animation" # Landroid/view/animation/Animation; */
/* .line 388 */
v0 = this.val$exitStartCallback;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 389 */
v0 = this.val$handler;
/* new-instance v1, Lcom/android/server/wm/AppTransitionInjector$2$$ExternalSyntheticLambda0; */
/* invoke-direct {v1}, Lcom/android/server/wm/AppTransitionInjector$2$$ExternalSyntheticLambda0;-><init>()V */
v2 = this.val$exitStartCallback;
com.android.internal.util.function.pooled.PooledLambda .obtainMessage ( v1,v2 );
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 392 */
} // :cond_0
return;
} // .end method
