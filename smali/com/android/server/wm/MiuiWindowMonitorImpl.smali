.class public Lcom/android/server/wm/MiuiWindowMonitorImpl;
.super Lcom/android/server/wm/MiuiWindowMonitorStub;
.source "MiuiWindowMonitorImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;,
        Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;,
        Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    }
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "APP_ID"

.field private static final CONTENT:Ljava/lang/String; = "content"

.field private static final DEFAULT_EXIT_REPORT_THRESHOLD:I = 0x32

.field private static final DEFAULT_KILL_THRESHOLD:I = 0xc8

.field private static final DEFAULT_REPORT_INTERVAL:I = 0x14

.field private static final DEFAULT_TOTAL_WINDOW_THRESHOLD:I = 0xf33

.field private static final DEFAULT_WARN_INTERVAL:I = 0x14

.field private static final EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field private static final EXIT_REPORT_THRESHOLD_PROPERTY:Ljava/lang/String; = "persist.sys.stability.window_monitor.exit_report_threshold"

.field private static final EXTRA_APP_ID:Ljava/lang/String; = "31000401516"

.field private static final EXTRA_EVENT_NAME:Ljava/lang/String; = "app_resource_info"

.field private static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "android"

.field private static final FLAG_MQS_REPORT:I = 0x2

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final FLAG_ONETRACK_REPORT:I = 0x1

.field private static final FOREGROUND_PERSISTENT_APPS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final INTENT_ACTION_ONETRACK:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final INTENT_PACKAGE_ONETRACK:Ljava/lang/String; = "com.miui.analytics"

.field private static final KEY_TOP_TYPE_WINDOWS:Ljava/lang/String; = "key_top_type_windows"

.field private static final KILL_REASON:Ljava/lang/String; = "window_leak_monitor"

.field private static final KILL_THRESHOLD_PROPERTY:Ljava/lang/String; = "persist.sys.stability.window_monitor.kill_threshold"

.field private static final KILL_WHEN_LEAK_PROPERTY:Ljava/lang/String; = "persist.sys.stability.window_monitor.kill_when_leak"

.field private static final LEAK_TYPE_APPLICATION:I = 0x1

.field private static final LEAK_TYPE_NONE:I = 0x0

.field private static final LEAK_TYPE_SYSTEM:I = 0x2

.field private static final PACKAGE:Ljava/lang/String; = "PACKAGE"

.field private static final REPORT_INTERVAL_PROPERTY:Ljava/lang/String; = "persist.sys.stability.window_monitor.report_interval"

.field private static final SWITCH_PROPERTY:Ljava/lang/String; = "persist.sys.stability.window_monitor.enabled"

.field private static final TAG:Ljava/lang/String; = "MiuiWindowMonitorImpl"

.field private static final TOTAL_WINDOW_THRESHOLD_PROPERTY:Ljava/lang/String; = "persist.sys.stability.window_monitor.total_window_threshold"

.field private static final WARN_INTERVAL_PROPERTY:Ljava/lang/String; = "persist.sys.stability.window_monitor.warn_interval"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mMonitorThread:Landroid/os/HandlerThread;

.field private mService:Lcom/android/server/wm/WindowManagerService;

.field private final mWindowsByApp:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;",
            "Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;",
            ">;"
        }
    .end annotation
.end field

.field private mWindowsByToken:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmWindowsByApp(Lcom/android/server/wm/MiuiWindowMonitorImpl;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$maddWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->addWindowInfo(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$misEnabled(Lcom/android/server/wm/MiuiWindowMonitorImpl;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->isEnabled()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mkillApp(Lcom/android/server/wm/MiuiWindowMonitorImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->killApp(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mremoveWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl;Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->removeWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mremoveWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl;Landroid/os/IBinder;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->removeWindowInfo(Landroid/os/IBinder;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportIfNeeded(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportWindowInfo(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 91
    const-string v0, "com.android.nfc"

    const-string v1, "com.miui.voicetrigger"

    const-string v2, "com.android.systemui"

    const-string v3, "com.android.phone"

    filled-new-array {v2, v3, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->FOREGROUND_PERSISTENT_APPS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 39
    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorStub;-><init>()V

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByToken:Ljava/util/HashMap;

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    return-void
.end method

.method private declared-synchronized addWindowInfo(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)V
    .locals 8
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "token"    # Landroid/os/IBinder;
    .param p4, "windowName"    # Ljava/lang/String;
    .param p5, "type"    # I

    monitor-enter p0

    .line 150
    :try_start_0
    new-instance v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    invoke-direct {v0, p1, p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;-><init>(ILjava/lang/String;)V

    .line 151
    .local v0, "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
    new-instance v7, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;

    move-object v1, v7

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;-><init>(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)V

    move-object v1, v7

    .line 152
    .local v1, "info":Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByToken:Ljava/util/HashMap;

    invoke-virtual {v2, p3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    .line 154
    .local v2, "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    if-nez v2, :cond_0

    .line 155
    new-instance v3, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    invoke-direct {v3, p1, p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;-><init>(ILjava/lang/String;)V

    move-object v2, v3

    .line 156
    iget-object v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    .end local p0    # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl;
    :cond_0
    invoke-virtual {v2, p3, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->put(Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;)V

    .line 162
    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->isEnabled()Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_1

    .line 163
    monitor-exit p0

    return-void

    .line 165
    .restart local p0    # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl;
    :cond_1
    :try_start_1
    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->checkProcess(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166
    monitor-exit p0

    return-void

    .line 149
    .end local v0    # "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
    .end local v1    # "info":Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;
    .end local v2    # "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .end local p0    # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl;
    .end local p1    # "pid":I
    .end local p2    # "packageName":Ljava/lang/String;
    .end local p3    # "token":Landroid/os/IBinder;
    .end local p4    # "windowName":Ljava/lang/String;
    .end local p5    # "type":I
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private anyProcessLeak(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .locals 3
    .param p1, "processWindows"    # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    .line 479
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByToken:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getTotalWindowThreshold()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 480
    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->selectTopProcess()Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    move-result-object v0

    .line 481
    .local v0, "most":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    if-eqz v0, :cond_0

    .line 482
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLeakType(I)V

    .line 483
    return-object v0

    .line 488
    .end local v0    # "most":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    .line 489
    return-object v0

    .line 492
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v1

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getKillThreshold()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 493
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLeakType(I)V

    .line 494
    return-object p1

    .line 496
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLeakType(I)V

    .line 497
    return-object v0
.end method

.method private checkProcess(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;)V
    .locals 2
    .param p1, "processWindows"    # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    .line 171
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->warned()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 177
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->anyProcessLeak(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    move-result-object v0

    .line 178
    .local v0, "leakProcess":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    if-eqz v0, :cond_1

    .line 179
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPid()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->dealWithLeakProcess(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;)Z

    .line 180
    return-void

    .line 184
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->warned()Z

    move-result v1

    if-nez v1, :cond_2

    .line 185
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V

    .line 186
    invoke-virtual {p1, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V

    .line 188
    :cond_2
    return-void

    .line 172
    .end local v0    # "leakProcess":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    :cond_3
    :goto_0
    return-void
.end method

.method private dealWithLeakProcess(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;)Z
    .locals 7
    .param p1, "pid"    # I
    .param p2, "processWindows"    # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    .line 192
    invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 193
    invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V

    .line 194
    invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V

    .line 195
    invoke-direct {p0, p1, p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V

    .line 196
    return v2

    .line 200
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->killByPolicy()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLeakType()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 201
    invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V

    .line 202
    invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V

    .line 203
    invoke-direct {p0, p1, p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V

    .line 204
    return v2

    .line 207
    :cond_1
    invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "pkgName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mContext:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/android/server/am/ProcessUtils;->isPersistent(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 209
    .local v3, "persistent":Z
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->getCurAdjByPid(I)I

    move-result v4

    const/16 v5, 0xc8

    if-gt v4, v5, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    sget-object v4, Lcom/android/server/wm/MiuiWindowMonitorImpl;->FOREGROUND_PERSISTENT_APPS:Ljava/util/List;

    .line 210
    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 212
    :cond_3
    invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v4

    .line 214
    .local v4, "curCount":I
    invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLastWarnCount()I

    move-result v5

    sub-int v5, v4, v5

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getWarnInterval()I

    move-result v6

    if-lt v5, v6, :cond_4

    .line 215
    invoke-virtual {p2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V

    .line 216
    invoke-virtual {p2, v4}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLastWarnCount(I)V

    .line 217
    invoke-direct {p0, p2, v0, v3}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->showKillAppDialog(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Ljava/lang/String;Z)V

    goto :goto_0

    .line 219
    :cond_4
    invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V

    .line 220
    invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V

    .line 221
    invoke-direct {p0, p1, p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V

    .line 223
    :goto_0
    return v2

    .line 227
    .end local v4    # "curCount":I
    :cond_5
    invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V

    .line 228
    invoke-virtual {p2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V

    .line 229
    invoke-direct {p0, p1, p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V

    .line 230
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->killApp(I)V

    .line 231
    return v1
.end method

.method private doMqsReport(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "details"    # Ljava/lang/String;
    .param p4, "topTypeWindows"    # Ljava/lang/String;

    .line 554
    new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V

    .line 555
    .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    const/16 v1, 0x1a7

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V

    .line 556
    invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V

    .line 557
    invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V

    .line 558
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V

    .line 559
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "window leaked! package="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V

    .line 560
    invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V

    .line 561
    invoke-virtual {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 562
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "key_top_type_windows"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z

    .line 564
    return-void
.end method

.method private doOneTrackReport(Ljava/lang/String;)V
    .locals 4
    .param p1, "content"    # Ljava/lang/String;

    .line 538
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 539
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 540
    const-string v1, "APP_ID"

    const-string v2, "31000401516"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 541
    const-string v1, "EVENT_NAME"

    const-string v2, "app_resource_info"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 542
    const-string v1, "PACKAGE"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 543
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 544
    .local v1, "params":Landroid/os/Bundle;
    const-string v2, "content"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 546
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 547
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 550
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "params":Landroid/os/Bundle;
    goto :goto_0

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 551
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private getExitReportThreshold()I
    .locals 2

    .line 391
    const-string v0, "persist.sys.stability.window_monitor.exit_report_threshold"

    const/16 v1, 0x32

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getKillThreshold()I
    .locals 2

    .line 387
    const-string v0, "persist.sys.stability.window_monitor.kill_threshold"

    const/16 v1, 0xc8

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getReportInterval()I
    .locals 2

    .line 396
    const-string v0, "persist.sys.stability.window_monitor.report_interval"

    const/16 v1, 0x14

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getTotalWindowThreshold()I
    .locals 2

    .line 404
    const-string v0, "persist.sys.stability.window_monitor.total_window_threshold"

    const/16 v1, 0xf33

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getWarnInterval()I
    .locals 2

    .line 400
    const-string v0, "persist.sys.stability.window_monitor.warn_interval"

    const/16 v1, 0x14

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private isEnabled()Z
    .locals 2

    .line 379
    const-string v0, "persist.sys.stability.window_monitor.enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private killApp(I)V
    .locals 2
    .param p1, "pid"    # I

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "kill proc "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " because it creates too many windows!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiWindowMonitorImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-static {p1}, Landroid/os/Process;->killProcess(I)V

    .line 238
    return-void
.end method

.method private killByPolicy()Z
    .locals 2

    .line 383
    const-string v0, "persist.sys.stability.window_monitor.kill_when_leak"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private removeWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .locals 10
    .param p1, "key"    # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    .line 431
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    .line 432
    .local v0, "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    if-nez v0, :cond_0

    .line 433
    const/4 v1, 0x0

    return-object v1

    .line 436
    :cond_0
    nop

    .line 437
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getWindows()Landroid/util/SparseArray;

    move-result-object v1

    .line 438
    .local v1, "windowsByType":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v3

    .local v3, "size":I
    :goto_0
    if-ge v2, v3, :cond_3

    .line 439
    nop

    .line 440
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    .line 441
    .local v4, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 442
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/HashMap;

    .line 443
    .local v6, "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 444
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/IBinder;

    .line 445
    .local v8, "token":Landroid/os/IBinder;
    iget-object v9, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByToken:Ljava/util/HashMap;

    invoke-virtual {v9, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    .end local v8    # "token":Landroid/os/IBinder;
    goto :goto_2

    .line 448
    .end local v6    # "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    :cond_1
    goto :goto_1

    .line 438
    .end local v4    # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 452
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_3
    return-object v0
.end method

.method private declared-synchronized removeWindowInfo(Landroid/os/IBinder;)V
    .locals 5
    .param p1, "token"    # Landroid/os/IBinder;

    monitor-enter p0

    .line 410
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByToken:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;

    .line 411
    .local v0, "info":Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;
    if-eqz v0, :cond_0

    .line 412
    new-instance v1, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    iget v2, v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPid:I

    iget-object v3, v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPackageName:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;-><init>(ILjava/lang/String;)V

    .line 413
    .local v1, "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    .line 414
    .local v2, "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    if-eqz v2, :cond_0

    .line 415
    iget v3, v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I

    iget-object v4, v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mWindowName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->remove(ILjava/lang/String;Landroid/os/IBinder;)V

    .line 417
    invoke-virtual {v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v3

    if-gtz v3, :cond_0

    .line 418
    iget-object v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    .end local v1    # "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
    .end local v2    # "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .end local p0    # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl;
    :cond_0
    monitor-exit p0

    return-void

    .line 409
    .end local v0    # "info":Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;
    .end local p1    # "token":Landroid/os/IBinder;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V
    .locals 16
    .param p1, "pid"    # I
    .param p2, "processWindows"    # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .param p3, "died"    # Z

    .line 589
    invoke-direct/range {p0 .. p3}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->shouldReport(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)I

    move-result v7

    .line 593
    .local v7, "report":I
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    .line 594
    new-instance v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move/from16 v8, p1

    invoke-direct {v0, v8, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;-><init>(ILjava/lang/String;)V

    .line 595
    .local v0, "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
    move-object/from16 v9, p0

    invoke-direct {v9, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->removeWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    goto :goto_0

    .line 593
    .end local v0    # "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
    :cond_0
    move-object/from16 v9, p0

    move/from16 v8, p1

    .line 597
    :goto_0
    if-nez v7, :cond_1

    .line 598
    return-void

    .line 601
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v10

    .line 602
    .local v10, "infoStr":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 603
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "app:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", to json string error!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiWindowMonitorImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    return-void

    .line 607
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getTopTypeWindows()Ljava/lang/String;

    move-result-object v11

    .line 610
    .local v11, "topTypeWindows":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPid()I

    move-result v12

    .line 611
    .local v12, "reportedPid":I
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPackageName()Ljava/lang/String;

    move-result-object v13

    .line 612
    .local v13, "reportedPkg":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ScoutSystemMonitor;->getInstance()Lcom/android/server/ScoutSystemMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/ScoutSystemMonitor;->getSystemWorkerHandler()Landroid/os/Handler;

    move-result-object v14

    .line 613
    .local v14, "handler":Landroid/os/Handler;
    if-eqz v14, :cond_3

    .line 614
    new-instance v15, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;

    move-object v0, v15

    move-object/from16 v1, p0

    move v2, v12

    move-object v3, v13

    move-object v4, v10

    move-object v5, v11

    move v6, v7

    invoke-direct/range {v0 .. v6}, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v14, v15}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 621
    :cond_3
    return-void
.end method

.method private reportWindowInfo(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "infoStr"    # Ljava/lang/String;
    .param p4, "topTypeWindows"    # Ljava/lang/String;
    .param p5, "report"    # I

    .line 569
    and-int/lit8 v0, p5, 0x1

    const-string v1, "MiuiWindowMonitorImpl"

    if-eqz v0, :cond_0

    .line 570
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doOneTrackReport:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    invoke-direct {p0, p3}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->doOneTrackReport(Ljava/lang/String;)V

    .line 575
    :cond_0
    and-int/lit8 v0, p5, 0x2

    if-eqz v0, :cond_1

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doMqsReport:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->doMqsReport(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :cond_1
    return-void
.end method

.method private selectTopProcess()Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .locals 5

    .line 460
    const/4 v0, 0x0

    .line 461
    .local v0, "most":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    const/4 v1, -0x1

    .line 462
    .local v1, "count":I
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    .line 463
    .local v3, "pl":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    invoke-virtual {v3}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v4

    .line 464
    .local v4, "plCount":I
    if-le v4, v1, :cond_0

    .line 465
    move v1, v4

    .line 466
    move-object v0, v3

    .line 468
    .end local v3    # "pl":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .end local v4    # "plCount":I
    :cond_0
    goto :goto_0

    .line 469
    :cond_1
    return-object v0
.end method

.method private shouldReport(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)I
    .locals 4
    .param p1, "pid"    # I
    .param p2, "processWindows"    # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .param p3, "died"    # Z

    .line 502
    const/4 v0, 0x0

    if-ltz p1, :cond_4

    if-nez p2, :cond_0

    goto :goto_1

    .line 507
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v1

    .line 510
    .local v1, "curCount":I
    if-nez v1, :cond_1

    .line 511
    invoke-virtual {p2, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLastReportCount(I)V

    .line 512
    return v0

    .line 514
    :cond_1
    const/4 v0, 0x0

    .line 516
    .local v0, "report":I
    if-eqz p3, :cond_2

    .line 519
    invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLeakType()I

    move-result v2

    if-nez v2, :cond_3

    .line 520
    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getExitReportThreshold()I

    move-result v2

    if-le v1, v2, :cond_3

    .line 521
    or-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 526
    :cond_2
    invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLeakType()I

    move-result v2

    if-eqz v2, :cond_3

    .line 527
    invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLastReportCount()I

    move-result v2

    sub-int v2, v1, v2

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getReportInterval()I

    move-result v3

    if-lt v2, v3, :cond_3

    .line 528
    or-int/lit8 v0, v0, 0x3

    .line 529
    invoke-virtual {p2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLastReportCount(I)V

    .line 533
    :cond_3
    :goto_0
    return v0

    .line 503
    .end local v0    # "report":I
    .end local v1    # "curCount":I
    :cond_4
    :goto_1
    return v0
.end method

.method private showKillAppDialog(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "processWindows"    # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "persistent"    # Z

    .line 241
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPid()I

    move-result v7

    .line 242
    .local v7, "pid":I
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/android/server/am/ProcessUtils;->getApplicationLabel(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 243
    .local v8, "appLabel":Ljava/lang/String;
    new-instance v9, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move v3, v7

    move-object v4, p2

    move-object v5, v8

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 268
    .local v0, "callback":Lcom/android/server/am/MiuiWarnings$WarningCallback;
    invoke-static {}, Lcom/android/server/UiThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/MiuiWindowMonitorImpl$3;

    invoke-direct {v2, p0, v8, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$3;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Ljava/lang/String;Lcom/android/server/am/MiuiWarnings$WarningCallback;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 273
    return-void
.end method


# virtual methods
.method public addWindowInfoLocked(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)Z
    .locals 10
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "token"    # Landroid/os/IBinder;
    .param p4, "windowName"    # Ljava/lang/String;
    .param p5, "type"    # I

    .line 131
    const/4 v0, 0x0

    if-ltz p1, :cond_2

    if-eqz p3, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 137
    new-instance v9, Lcom/android/server/wm/MiuiWindowMonitorImpl$1;

    move-object v2, v9

    move-object v3, p0

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/android/server/wm/MiuiWindowMonitorImpl$1;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)V

    invoke-virtual {v1, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 144
    :cond_1
    return v0

    .line 132
    :cond_2
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid data: pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", token="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", packageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiWindowMonitorImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    return v0
.end method

.method public declared-synchronized dump(Ljava/io/PrintWriter;)I
    .locals 7
    .param p1, "pw"    # Ljava/io/PrintWriter;

    monitor-enter p0

    .line 342
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 343
    monitor-exit p0

    return v0

    .line 346
    :cond_0
    :try_start_0
    const-string v1, "DUMP MiuiWindowMonitor INFO"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 347
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    const-string v1, "enabled="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    .line 348
    const-string v1, "kill_when_leak="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->killByPolicy()Z

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V

    .line 349
    const-string v1, "kill_threshold="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getKillThreshold()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V

    .line 350
    const-string v1, "exit_report_threshold="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getExitReportThreshold()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V

    .line 351
    const-string/jumbo v1, "warn_interval="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getWarnInterval()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V

    .line 352
    const-string v1, "report_interval="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getReportInterval()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V

    .line 353
    const-string/jumbo v1, "total_window_threshold="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getTotalWindowThreshold()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V

    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 355
    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    .line 356
    .local v1, "procCount":I
    if-nez v1, :cond_1

    .line 357
    const-string v2, "no process has windows"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    monitor-exit p0

    return v0

    .line 361
    .end local p0    # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl;
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 363
    const/4 v2, 0x0

    .line 364
    .local v2, "index":I
    const/4 v3, 0x0

    .line 365
    .local v3, "windowCount":I
    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mWindowsByApp:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    .line 366
    .local v5, "pwl":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    if-eqz v5, :cond_2

    .line 367
    const-string v6, "  Process#"

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v6, " "

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 368
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 369
    invoke-virtual {v5}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v6

    add-int/2addr v3, v6

    .line 370
    add-int/lit8 v2, v2, 0x1

    .line 372
    .end local v5    # "pwl":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
    :cond_2
    goto :goto_0

    .line 373
    :cond_3
    const-string v4, "TOTAL:"

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    const-string v4, " processes, "

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(I)V

    .line 374
    const-string v4, " windows"

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375
    monitor-exit p0

    return v0

    .line 341
    .end local v1    # "procCount":I
    .end local v2    # "index":I
    .end local v3    # "windowCount":I
    .end local p1    # "pw":Ljava/io/PrintWriter;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public init(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/android/server/wm/WindowManagerService;

    .line 112
    iput-object p2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 113
    iput-object p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mContext:Landroid/content/Context;

    .line 114
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "window-leak-monitor-thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mMonitorThread:Landroid/os/HandlerThread;

    .line 115
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 116
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mHandler:Landroid/os/Handler;

    .line 117
    return-void
.end method

.method public onProcessDiedLocked(ILcom/android/server/am/ProcessRecord;)V
    .locals 3
    .param p1, "pid"    # I
    .param p2, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 322
    new-instance v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    invoke-static {p2}, Lcom/android/server/am/ProcessUtils;->getPackageNameByApp(Lcom/android/server/am/ProcessRecord;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;-><init>(ILjava/lang/String;)V

    .line 323
    .local v0, "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 324
    new-instance v2, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 337
    :cond_0
    return-void
.end method

.method public removeWindowInfoLocked(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;

    .line 302
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 303
    new-instance v1, Lcom/android/server/wm/MiuiWindowMonitorImpl$4;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$4;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Landroid/os/IBinder;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 310
    :cond_0
    return-void
.end method
