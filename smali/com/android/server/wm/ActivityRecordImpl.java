public class com.android.server.wm.ActivityRecordImpl implements com.android.server.wm.ActivityRecordStub {
	 /* .source "ActivityRecordImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl; */
	 /* } */
} // .end annotation
/* # static fields */
private static final android.util.ArraySet BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final android.util.ArraySet BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN_IN_SPLIT_MODE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.List COMPAT_WHITE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List FORCE_SKIP_RELAUNCH_ACTIVITY_NAMES;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String NON_RESIZEABLE_PORTRAIT_ACTIVITY;
public static final java.lang.String PERMISSION_ACTIVITY;
public static final java.lang.String PROPERTY_MIUI_BLUR_CHANGED;
public static final java.lang.String PROPERTY_MIUI_BLUR_RELAUNCH;
private static final java.lang.String RESIZEABLE_PORTRAIT_ACTIVITY;
private static java.util.List SKIP_RELAUNCH_ACTIVITY_NAMES;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String STATSD_NON_RESIZEABLE_PORTRAIT_ACTIVITY;
private static final java.lang.String SUPPORT_SIZE_CHANGE_PORTRAIT_ACTIVITY;
private static final java.lang.String TAG;
private static java.util.List sStartWindowBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private miui.security.SecurityManagerInternal mSecurityInternal;
/* # direct methods */
static com.android.server.wm.ActivityRecordImpl ( ) {
/* .locals 4 */
/* .line 87 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 89 */
/* new-instance v1, Landroid/util/ArraySet; */
/* invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V */
/* .line 91 */
/* new-instance v2, Landroid/util/ArraySet; */
/* invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V */
/* .line 95 */
final String v3 = "com.iflytek.inputmethod.miui/com.iflytek.inputmethod.LauncherSettingsActivity"; // const-string v3, "com.iflytek.inputmethod.miui/com.iflytek.inputmethod.LauncherSettingsActivity"
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 96 */
final String v3 = "com.android.camera/.AssistantCamera"; // const-string v3, "com.android.camera/.AssistantCamera"
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 97 */
final String v3 = "com.tencent.mm/.plugin.voip.ui.VideoActivity"; // const-string v3, "com.tencent.mm/.plugin.voip.ui.VideoActivity"
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 98 */
final String v3 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v3, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 99 */
final String v3 = "com.android.deskclock/.alarm.alert.AlarmAlertFullScreenActivity"; // const-string v3, "com.android.deskclock/.alarm.alert.AlarmAlertFullScreenActivity"
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 100 */
/* const-string/jumbo v3, "tv.danmaku.bili/com.bilibili.video.story.StoryVideoActivity" */
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 101 */
/* const-string/jumbo v3, "tv.danmaku.bili/com.bilibili.video.videodetail.VideoDetailsActivity" */
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 102 */
final String v3 = "com.miui.securitycenter/com.miui.applicationlock.TransitionHelper"; // const-string v3, "com.miui.securitycenter/com.miui.applicationlock.TransitionHelper"
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 103 */
final String v3 = "com.android.incallui/.InCallActivity"; // const-string v3, "com.android.incallui/.InCallActivity"
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 104 */
final String v3 = "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"; // const-string v3, "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"
(( android.util.ArraySet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 105 */
final String v1 = "com.xiaomi.smarthome/.SmartHomeMainActivity"; // const-string v1, "com.xiaomi.smarthome/.SmartHomeMainActivity"
(( android.util.ArraySet ) v2 ).add ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 106 */
final String v1 = "android.server.wm.CompatChangeTests$ResizeablePortraitActivity"; // const-string v1, "android.server.wm.CompatChangeTests$ResizeablePortraitActivity"
/* .line 107 */
final String v1 = "android.server.wm.CompatChangeTests$SupportsSizeChangesPortraitActivity"; // const-string v1, "android.server.wm.CompatChangeTests$SupportsSizeChangesPortraitActivity"
/* .line 108 */
final String v1 = "android.server.wm.CompatChangeTests$NonResizeablePortraitActivity"; // const-string v1, "android.server.wm.CompatChangeTests$NonResizeablePortraitActivity"
/* .line 109 */
final String v1 = "com.android.server.cts.device.statsdatom.StatsdCtsNonResizeablePortraitActivity"; // const-string v1, "com.android.server.cts.device.statsdatom.StatsdCtsNonResizeablePortraitActivity"
/* .line 112 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
final String v2 = "com.xiaomi.market"; // const-string v2, "com.xiaomi.market"
final String v3 = "com.google.android.calendar"; // const-string v3, "com.google.android.calendar"
/* filled-new-array {v3, v1, v2}, [Ljava/lang/String; */
/* .line 113 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 937 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 938 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 941 */
v0 = com.android.server.wm.ActivityRecordImpl.SKIP_RELAUNCH_ACTIVITY_NAMES;
final String v1 = "com.miui.child.home.home.MainActivity"; // const-string v1, "com.miui.child.home.home.MainActivity"
/* .line 942 */
v0 = com.android.server.wm.ActivityRecordImpl.SKIP_RELAUNCH_ACTIVITY_NAMES;
final String v1 = "com.android.packageinstaller.permission.ui.GrantPermissionsActivity"; // const-string v1, "com.android.packageinstaller.permission.ui.GrantPermissionsActivity"
/* .line 943 */
v0 = com.android.server.wm.ActivityRecordImpl.FORCE_SKIP_RELAUNCH_ACTIVITY_NAMES;
final String v1 = "com.android.provision.activities.DefaultActivity"; // const-string v1, "com.android.provision.activities.DefaultActivity"
/* .line 944 */
return;
} // .end method
public com.android.server.wm.ActivityRecordImpl ( ) {
/* .locals 0 */
/* .line 119 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 120 */
return;
} // .end method
private void checkSlow ( Long p0, Long p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "startTime" # J */
/* .param p3, "threshold" # J */
/* .param p5, "where" # Ljava/lang/String; */
/* .line 870 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p1 */
/* .line 871 */
/* .local v0, "took":J */
/* cmp-long v2, v0, p3 */
/* if-lez v2, :cond_0 */
/* .line 873 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Slow operation: "; // const-string v3, "Slow operation: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p5 ); // invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " took "; // const-string v3, " took "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms."; // const-string v3, "ms."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ActivityRecordImpl"; // const-string v3, "ActivityRecordImpl"
android.util.Slog .w ( v3,v2 );
/* .line 875 */
} // :cond_0
return;
} // .end method
static com.android.server.wm.ActivityRecordImpl getInstance ( ) {
/* .locals 1 */
/* .line 116 */
com.android.server.wm.ActivityRecordStub .get ( );
/* check-cast v0, Lcom/android/server/wm/ActivityRecordImpl; */
} // .end method
/* # virtual methods */
public Integer addThemeFlag ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 400 */
final String v0 = "com.android.settings"; // const-string v0, "com.android.settings"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/high16 v0, 0x400000 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void adjustSortIfNeeded ( java.util.ArrayList p0, android.util.ArrayMap p1 ) {
/* .locals 10 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/wm/Transition$ChangeInfo;", */
/* ">;", */
/* "Landroid/util/ArrayMap<", */
/* "Lcom/android/server/wm/WindowContainer;", */
/* "Lcom/android/server/wm/Transition$ChangeInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 835 */
/* .local p1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Transition$ChangeInfo;>;" */
/* .local p2, "changes":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Lcom/android/server/wm/WindowContainer;Lcom/android/server/wm/Transition$ChangeInfo;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 836 */
/* .local v0, "targetActivityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/ActivityRecord;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 837 */
/* .local v1, "needSortIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
int v4 = 1; // const/4 v4, 0x1
/* if-ge v2, v3, :cond_4 */
/* .line 838 */
(( java.util.ArrayList ) p1 ).get ( v2 ); // invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/Transition$ChangeInfo; */
/* .line 839 */
/* .local v3, "changeInfo":Lcom/android/server/wm/Transition$ChangeInfo; */
int v5 = 0; // const/4 v5, 0x0
/* .line 840 */
/* .local v5, "windowContainer":Lcom/android/server/wm/WindowContainer; */
(( android.util.ArrayMap ) p2 ).keySet ( ); // invoke-virtual {p2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_1
/* check-cast v7, Lcom/android/server/wm/WindowContainer; */
/* .line 841 */
/* .local v7, "wc":Lcom/android/server/wm/WindowContainer; */
(( android.util.ArrayMap ) p2 ).get ( v7 ); // invoke-virtual {p2, v7}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v8, Lcom/android/server/wm/Transition$ChangeInfo; */
/* .line 842 */
/* .local v8, "curChangeInfo":Lcom/android/server/wm/Transition$ChangeInfo; */
if ( v8 != null) { // if-eqz v8, :cond_0
v9 = (( java.lang.Object ) v8 ).equals ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 843 */
/* move-object v5, v7 */
/* .line 844 */
/* .line 846 */
} // .end local v7 # "wc":Lcom/android/server/wm/WindowContainer;
} // .end local v8 # "curChangeInfo":Lcom/android/server/wm/Transition$ChangeInfo;
} // :cond_0
/* .line 847 */
} // :cond_1
} // :goto_2
if ( v5 != null) { // if-eqz v5, :cond_2
(( com.android.server.wm.WindowContainer ) v5 ).getTopActivity ( v4, v4 ); // invoke-virtual {v5, v4, v4}, Lcom/android/server/wm/WindowContainer;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
} // :cond_2
int v4 = 0; // const/4 v4, 0x0
/* .line 848 */
/* .local v4, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
} // :goto_3
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 849 */
if ( v4 != null) { // if-eqz v4, :cond_3
v6 = (( com.android.server.wm.ActivityRecord ) v4 ).fillsParent ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->fillsParent()Z
/* if-nez v6, :cond_3 */
v6 = (( com.android.server.wm.ActivityRecord ) v4 ).isFinishing ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->isFinishing()Z
if ( v6 != null) { // if-eqz v6, :cond_3
v6 = v6 = this.mActivityRecordStub;
/* if-nez v6, :cond_3 */
/* .line 850 */
java.lang.Integer .valueOf ( v2 );
(( java.util.ArrayList ) v1 ).add ( v6 ); // invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 837 */
} // .end local v3 # "changeInfo":Lcom/android/server/wm/Transition$ChangeInfo;
} // .end local v4 # "activityRecord":Lcom/android/server/wm/ActivityRecord;
} // .end local v5 # "windowContainer":Lcom/android/server/wm/WindowContainer;
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 853 */
} // .end local v2 # "i":I
} // :cond_4
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_4
v3 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v3, :cond_8 */
/* .line 854 */
(( java.util.ArrayList ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 855 */
/* .local v3, "index":I */
/* if-le v3, v4, :cond_7 */
/* .line 856 */
/* add-int/lit8 v5, v3, -0x1 */
(( java.util.ArrayList ) p1 ).get ( v5 ); // invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/wm/Transition$ChangeInfo; */
/* .line 857 */
/* .local v5, "info":Lcom/android/server/wm/Transition$ChangeInfo; */
if ( v5 != null) { // if-eqz v5, :cond_5
v6 = this.mContainer;
v6 = (( com.android.server.wm.Transition$ChangeInfo ) v5 ).getTransitMode ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/Transition$ChangeInfo;->getTransitMode(Lcom/android/server/wm/WindowContainer;)I
} // :cond_5
int v6 = -1; // const/4 v6, -0x1
/* .line 858 */
/* .local v6, "mode":I */
} // :goto_5
/* if-eq v6, v4, :cond_7 */
int v7 = 3; // const/4 v7, 0x3
/* if-ne v6, v7, :cond_6 */
/* .line 859 */
/* .line 861 */
} // :cond_6
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Adjust the order of animation targets, "; // const-string v8, "Adjust the order of animation targets, "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.util.ArrayList ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = " need to higher level,"; // const-string v8, " need to higher level,"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* add-int/lit8 v8, v3, -0x1 */
/* .line 862 */
(( java.util.ArrayList ) v0 ).get ( v8 ); // invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = " need to lower level."; // const-string v8, " need to lower level."
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 861 */
final String v8 = "ActivityRecordImpl"; // const-string v8, "ActivityRecordImpl"
android.util.Slog .d ( v8,v7 );
/* .line 863 */
(( java.util.ArrayList ) p1 ).remove ( v3 ); // invoke-virtual {p1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/wm/Transition$ChangeInfo; */
/* .line 864 */
/* .local v7, "tempInfo":Lcom/android/server/wm/Transition$ChangeInfo; */
/* add-int/lit8 v8, v3, -0x1 */
(( java.util.ArrayList ) p1 ).add ( v8, v7 ); // invoke-virtual {p1, v8, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
/* .line 853 */
} // .end local v3 # "index":I
} // .end local v5 # "info":Lcom/android/server/wm/Transition$ChangeInfo;
} // .end local v6 # "mode":I
} // .end local v7 # "tempInfo":Lcom/android/server/wm/Transition$ChangeInfo;
} // :cond_7
} // :goto_6
/* add-int/lit8 v2, v2, 0x1 */
/* .line 867 */
} // .end local v2 # "i":I
} // :cond_8
return;
} // .end method
public Boolean allowShowSlpha ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 518 */
v0 = com.android.server.wm.AppTransitionInjector .ignoreLaunchedFromSystemSurface ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 519 */
int v0 = 0; // const/4 v0, 0x0
/* .line 521 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean allowTaskSnapshot ( java.lang.String p0, Boolean p1, android.window.TaskSnapshot p2, com.android.server.wm.ActivityRecord p3 ) {
/* .locals 13 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "allowTaskSnapshot" # Z */
/* .param p3, "snapshot" # Landroid/window/TaskSnapshot; */
/* .param p4, "mActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 443 */
/* move-object v0, p1 */
v1 = com.android.server.wm.AppTransitionInjector .disableSnapshot ( p1 );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 444 */
/* .line 446 */
} // :cond_0
v1 = /* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/ActivityRecord;->getUid()I */
v1 = com.android.server.wm.AppTransitionInjector .disableSnapshotForApplock ( p1,v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 447 */
/* .line 449 */
} // :cond_1
v1 = /* invoke-static/range {p4 ..p4}, Lcom/android/server/wm/AppTransitionInjector;->disableSnapshotByComponent(Lcom/android/server/wm/ActivityRecord;)Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 450 */
/* .line 452 */
} // :cond_2
/* if-nez p3, :cond_3 */
/* .line 453 */
/* .line 455 */
} // :cond_3
v1 = /* invoke-virtual/range {p3 ..p3}, Landroid/window/TaskSnapshot;->getWindowingMode()I */
int v3 = 5; // const/4 v3, 0x5
/* if-ne v1, v3, :cond_4 */
/* .line 456 */
v1 = /* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z */
/* if-nez v1, :cond_4 */
/* .line 457 */
/* .line 460 */
} // :cond_4
/* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/ActivityRecord;->findMainWindow()Lcom/android/server/wm/WindowState; */
/* .line 461 */
/* .local v1, "mainWin":Lcom/android/server/wm/WindowState; */
if ( v1 != null) { // if-eqz v1, :cond_5
v3 = (( com.android.server.wm.WindowState ) v1 ).isSecureLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isSecureLocked()Z
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 462 */
/* .line 465 */
} // :cond_5
v3 = com.android.server.wm.MiuiFreezeStub .getInstance ( );
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 466 */
/* .line 469 */
} // :cond_6
v3 = /* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/ActivityRecord;->isLetterboxedForFixedOrientationAndAspectRatio()Z */
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 470 */
/* .line 473 */
} // :cond_7
v3 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 474 */
/* .line 477 */
} // :cond_8
/* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task; */
/* if-nez v3, :cond_9 */
/* .line 478 */
/* .line 480 */
} // :cond_9
/* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task; */
(( com.android.server.wm.Task ) v3 ).getBounds ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
v3 = (( android.graphics.Rect ) v3 ).width ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->width()I
/* .line 481 */
/* .local v3, "taskWidth":I */
/* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task; */
(( com.android.server.wm.Task ) v4 ).getBounds ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
v4 = (( android.graphics.Rect ) v4 ).height ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->height()I
/* .line 482 */
/* .local v4, "taskHeight":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/window/TaskSnapshot;->getSnapshot()Landroid/graphics/GraphicBuffer; */
v5 = (( android.graphics.GraphicBuffer ) v5 ).getWidth ( ); // invoke-virtual {v5}, Landroid/graphics/GraphicBuffer;->getWidth()I
/* .line 483 */
/* .local v5, "snapshotWidth":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/window/TaskSnapshot;->getSnapshot()Landroid/graphics/GraphicBuffer; */
v6 = (( android.graphics.GraphicBuffer ) v6 ).getHeight ( ); // invoke-virtual {v6}, Landroid/graphics/GraphicBuffer;->getHeight()I
/* .line 484 */
/* .local v6, "snapshotHeight":I */
if ( v4 != null) { // if-eqz v4, :cond_d
/* if-nez v6, :cond_a */
/* move-object/from16 v10, p4 */
/* goto/16 :goto_1 */
/* .line 488 */
} // :cond_a
/* int-to-double v7, v3 */
/* int-to-double v9, v4 */
/* div-double/2addr v7, v9 */
/* int-to-double v9, v5 */
/* int-to-double v11, v6 */
/* div-double/2addr v9, v11 */
/* sub-double/2addr v7, v9 */
java.lang.Math .abs ( v7,v8 );
/* move-result-wide v7 */
/* .line 489 */
/* .local v7, "difference":D */
/* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent; */
v9 = (( com.android.server.wm.DisplayContent ) v9 ).getRotation ( ); // invoke-virtual {v9}, Lcom/android/server/wm/DisplayContent;->getRotation()I
v10 = /* invoke-virtual/range {p3 ..p3}, Landroid/window/TaskSnapshot;->getRotation()I */
/* if-ne v9, v10, :cond_c */
/* const-wide v9, 0x3fb999999999999aL # 0.1 */
/* cmpl-double v9, v7, v9 */
/* if-lez v9, :cond_c */
/* .line 490 */
v9 = /* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z */
/* if-nez v9, :cond_b */
/* .line 491 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Task bounds and snapshot don\'t match.task width="; // const-string v10, "Task bounds and snapshot don\'t match.task width="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v3 ); // invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", height="; // const-string v10, ", height="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ": snapshot w="; // const-string v10, ": snapshot w="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", h="; // const-string v10, ", h="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", difference = "; // const-string v10, ", difference = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7, v8 ); // invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v10 = ", name = "; // const-string v10, ", name = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v10, p4 */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v11 = "ActivityRecordImpl"; // const-string v11, "ActivityRecordImpl"
android.util.Slog .d ( v11,v9 );
/* .line 494 */
/* .line 490 */
} // :cond_b
/* move-object/from16 v10, p4 */
/* .line 489 */
} // :cond_c
/* move-object/from16 v10, p4 */
/* .line 497 */
} // :goto_0
/* .line 484 */
} // .end local v7 # "difference":D
} // :cond_d
/* move-object/from16 v10, p4 */
/* .line 485 */
} // :goto_1
} // .end method
public Integer avoidWhiteScreen ( java.lang.String p0, Integer p1, java.lang.String p2, com.android.server.wm.Task p3, Boolean p4, Integer p5, Integer p6, com.android.server.wm.ActivityRecord p7 ) {
/* .locals 15 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "resolvedTheme" # I */
/* .param p3, "taskAffinity" # Ljava/lang/String; */
/* .param p4, "task" # Lcom/android/server/wm/Task; */
/* .param p5, "taskSwitch" # Z */
/* .param p6, "theme" # I */
/* .param p7, "mUserId" # I */
/* .param p8, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 629 */
/* move-object/from16 v1, p1 */
/* move-object/from16 v2, p3 */
/* move-object/from16 v3, p4 */
int v0 = 0; // const/4 v0, 0x0
/* .line 630 */
/* .local v0, "sameAffinity":Z */
/* move/from16 v4, p2 */
/* .line 631 */
/* .local v4, "overrideTheme":I */
int v5 = 1; // const/4 v5, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
if ( v3 != null) { // if-eqz v3, :cond_0
v6 = this.affinity;
v6 = (( java.lang.String ) v2 ).equals ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 632 */
v6 = /* invoke-virtual/range {p8 ..p8}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I */
/* if-ne v6, v5, :cond_0 */
/* .line 633 */
int v0 = 1; // const/4 v0, 0x1
/* move v6, v0 */
/* .line 635 */
} // :cond_0
/* move v6, v0 */
} // .end local v0 # "sameAffinity":Z
/* .local v6, "sameAffinity":Z */
} // :goto_0
if ( p5 != null) { // if-eqz p5, :cond_3
if ( v6 != null) { // if-eqz v6, :cond_3
v0 = v0 = com.android.server.wm.ActivityRecordImpl.sStartWindowBlackList;
/* if-nez v0, :cond_3 */
/* .line 636 */
/* new-instance v0, Landroid/content/Intent; */
final String v7 = "android.intent.action.MAIN"; // const-string v7, "android.intent.action.MAIN"
/* invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* move-object v7, v0 */
/* .line 637 */
/* .local v7, "launcherIntent":Landroid/content/Intent; */
(( android.content.Intent ) v7 ).setPackage ( v1 ); // invoke-virtual {v7, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 638 */
final String v0 = "android.intent.category.LAUNCHER"; // const-string v0, "android.intent.category.LAUNCHER"
(( android.content.Intent ) v7 ).addCategory ( v0 ); // invoke-virtual {v7, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
/* .line 639 */
int v14 = 0; // const/4 v14, 0x0
/* .line 641 */
/* .local v14, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
int v10 = 0; // const/4 v10, 0x0
/* const-wide/16 v11, 0x1 */
/* move-object v9, v7 */
/* move/from16 v13, p7 */
/* invoke-interface/range {v8 ..v13}, Landroid/content/pm/IPackageManager;->queryIntentActivities(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ParceledListSlice; */
/* .line 642 */
(( android.content.pm.ParceledListSlice ) v0 ).getList ( ); // invoke-virtual {v0}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 646 */
} // .end local v14 # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
/* .local v0, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
/* .line 643 */
} // .end local v0 # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
/* .restart local v14 # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
/* :catch_0 */
/* move-exception v0 */
/* .line 644 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v8 = "ActivityRecordImpl"; // const-string v8, "ActivityRecordImpl"
final String v9 = "RemoteException when queryIntentActivities"; // const-string v9, "RemoteException when queryIntentActivities"
android.util.Slog .d ( v8,v9 );
/* .line 645 */
/* new-instance v8, Ljava/util/ArrayList; */
/* invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V */
/* move-object v0, v8 */
/* .line 648 */
} // .end local v14 # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
/* .local v0, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
v8 = } // :goto_1
/* if-ne v8, v5, :cond_2 */
/* .line 649 */
int v5 = 0; // const/4 v5, 0x0
/* check-cast v5, Landroid/content/pm/ResolveInfo; */
/* .line 650 */
/* .local v5, "ri":Landroid/content/pm/ResolveInfo; */
if ( v5 != null) { // if-eqz v5, :cond_1
v8 = this.activityInfo;
if ( v8 != null) { // if-eqz v8, :cond_1
v8 = this.activityInfo;
/* iget v8, v8, Landroid/content/pm/ActivityInfo;->theme:I */
if ( v8 != null) { // if-eqz v8, :cond_1
v8 = this.activityInfo;
/* iget v8, v8, Landroid/content/pm/ActivityInfo;->theme:I */
/* move/from16 v9, p6 */
/* if-eq v8, v9, :cond_4 */
/* .line 652 */
v8 = this.activityInfo;
/* iget v4, v8, Landroid/content/pm/ActivityInfo;->theme:I */
/* .line 650 */
} // :cond_1
/* move/from16 v9, p6 */
/* .line 648 */
} // .end local v5 # "ri":Landroid/content/pm/ResolveInfo;
} // :cond_2
/* move/from16 v9, p6 */
/* .line 635 */
} // .end local v0 # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
} // .end local v7 # "launcherIntent":Landroid/content/Intent;
} // :cond_3
/* move/from16 v9, p6 */
/* .line 656 */
} // :cond_4
} // :goto_2
} // .end method
public Boolean canShowWhenLocked ( android.app.AppOpsManager p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 8 */
/* .param p1, "appOpsManager" # Landroid/app/AppOpsManager; */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "extra" # Ljava/lang/String; */
/* .line 405 */
/* const/16 v1, 0x2724 */
int v4 = 0; // const/4 v4, 0x0
final String v5 = "ActivityRecordImpl#canShowWhenLocked"; // const-string v5, "ActivityRecordImpl#canShowWhenLocked"
/* move-object v0, p1 */
/* move v2, p2 */
/* move-object v3, p3 */
v0 = /* invoke-virtual/range {v0 ..v5}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
/* .line 408 */
/* .local v0, "mode":I */
v1 = this.mSecurityInternal;
/* if-nez v1, :cond_0 */
/* .line 409 */
/* const-class v1, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lmiui/security/SecurityManagerInternal; */
this.mSecurityInternal = v1;
/* .line 411 */
} // :cond_0
v2 = this.mSecurityInternal;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 412 */
/* const/16 v3, 0x24 */
/* const-wide/16 v5, 0x1 */
/* move-object v4, p3 */
/* move-object v7, p4 */
/* invoke-virtual/range {v2 ..v7}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 415 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 416 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "MIUILOG- Show when locked PermissionDenied pkg : "; // const-string v2, "MIUILOG- Show when locked PermissionDenied pkg : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " uid : "; // const-string v2, " uid : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ActivityRecordImpl"; // const-string v2, "ActivityRecordImpl"
android.util.Slog .i ( v2,v1 );
/* .line 417 */
int v1 = 0; // const/4 v1, 0x0
/* .line 419 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
} // .end method
public Boolean canStartCollectingNow ( Integer p0, com.android.server.wm.Transition p1, Integer p2, android.app.ActivityOptions p3, com.android.server.wm.ActivityTaskManagerService p4, com.android.server.wm.ActivityRecord p5 ) {
/* .locals 15 */
/* .param p1, "type" # I */
/* .param p2, "collectingTransition" # Lcom/android/server/wm/Transition; */
/* .param p3, "realCallingUid" # I */
/* .param p4, "options" # Landroid/app/ActivityOptions; */
/* .param p5, "atm" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p6, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 880 */
/* move/from16 v0, p1 */
/* move-object/from16 v1, p2 */
/* move-object/from16 v2, p6 */
/* const v3, 0x7fffffeb */
int v4 = 2; // const/4 v4, 0x2
int v5 = 1; // const/4 v5, 0x1
/* if-ne v0, v3, :cond_2 */
/* .line 881 */
/* iget v3, v1, Lcom/android/server/wm/Transition;->mType:I */
/* const/16 v6, 0xc */
/* if-ne v3, v6, :cond_0 */
/* .line 882 */
/* .line 885 */
} // :cond_0
/* iget v3, v1, Lcom/android/server/wm/Transition;->mType:I */
/* if-ne v3, v4, :cond_2 */
/* .line 886 */
v3 = this.mParticipants;
v3 = (( android.util.ArraySet ) v3 ).isEmpty ( ); // invoke-virtual {v3}, Landroid/util/ArraySet;->isEmpty()Z
/* if-nez v3, :cond_2 */
/* .line 887 */
v3 = this.mParticipants;
(( android.util.ArraySet ) v3 ).iterator ( ); // invoke-virtual {v3}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_2
/* check-cast v6, Lcom/android/server/wm/WindowContainer; */
/* .line 888 */
/* .local v6, "wc":Lcom/android/server/wm/WindowContainer; */
(( com.android.server.wm.WindowContainer ) v6 ).asActivityRecord ( ); // invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
/* .line 889 */
/* .local v7, "ar":Lcom/android/server/wm/ActivityRecord; */
if ( v7 != null) { // if-eqz v7, :cond_1
v8 = v8 = this.mActivityRecordStub;
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 890 */
/* .line 892 */
} // .end local v6 # "wc":Lcom/android/server/wm/WindowContainer;
} // .end local v7 # "ar":Lcom/android/server/wm/ActivityRecord;
} // :cond_1
/* .line 897 */
} // :cond_2
final String v3 = "ActivityRecordImpl"; // const-string v3, "ActivityRecordImpl"
int v6 = 3; // const/4 v6, 0x3
int v7 = 0; // const/4 v7, 0x0
/* if-eq v0, v5, :cond_4 */
/* if-ne v0, v6, :cond_3 */
} // :cond_3
/* move/from16 v10, p3 */
/* move-object/from16 v6, p5 */
} // :cond_4
} // :goto_1
/* iget v8, v1, Lcom/android/server/wm/Transition;->mType:I */
/* if-eq v8, v5, :cond_5 */
/* iget v8, v1, Lcom/android/server/wm/Transition;->mType:I */
/* if-ne v8, v6, :cond_3 */
/* .line 899 */
} // :cond_5
/* move-object/from16 v6, p5 */
v8 = this.mHomeProcess;
/* .line 901 */
/* .local v8, "homeProcess":Lcom/android/server/wm/WindowProcessController; */
if ( v8 != null) { // if-eqz v8, :cond_9
/* iget v9, v8, Lcom/android/server/wm/WindowProcessController;->mUid:I */
/* move/from16 v10, p3 */
/* if-ne v9, v10, :cond_a */
if ( p4 != null) { // if-eqz p4, :cond_a
/* .line 902 */
/* invoke-virtual/range {p4 ..p4}, Landroid/app/ActivityOptions;->getRemoteTransition()Landroid/window/RemoteTransition; */
if ( v9 != null) { // if-eqz v9, :cond_a
/* .line 903 */
v9 = this.mParticipants;
if ( v9 != null) { // if-eqz v9, :cond_8
v9 = this.mParticipants;
/* .line 904 */
v9 = (( android.util.ArraySet ) v9 ).isEmpty ( ); // invoke-virtual {v9}, Landroid/util/ArraySet;->isEmpty()Z
/* if-nez v9, :cond_8 */
/* .line 905 */
v9 = this.mParticipants;
(( android.util.ArraySet ) v9 ).iterator ( ); // invoke-virtual {v9}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v11 = } // :goto_2
if ( v11 != null) { // if-eqz v11, :cond_7
/* check-cast v11, Lcom/android/server/wm/WindowContainer; */
/* .line 906 */
/* .local v11, "wc":Lcom/android/server/wm/WindowContainer; */
(( com.android.server.wm.WindowContainer ) v11 ).asActivityRecord ( ); // invoke-virtual {v11}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
/* .line 907 */
/* .local v12, "ar":Lcom/android/server/wm/ActivityRecord; */
if ( v12 != null) { // if-eqz v12, :cond_6
v13 = (( com.android.server.wm.ActivityRecord ) v12 ).isLaunchSourceType ( v4 ); // invoke-virtual {v12, v4}, Lcom/android/server/wm/ActivityRecord;->isLaunchSourceType(I)Z
if ( v13 != null) { // if-eqz v13, :cond_6
if ( v2 != null) { // if-eqz v2, :cond_6
v13 = this.packageName;
if ( v13 != null) { // if-eqz v13, :cond_6
v13 = this.packageName;
v14 = this.packageName;
/* .line 909 */
v13 = (( java.lang.String ) v13 ).equals ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_6
/* .line 910 */
/* .line 912 */
} // .end local v11 # "wc":Lcom/android/server/wm/WindowContainer;
} // .end local v12 # "ar":Lcom/android/server/wm/ActivityRecord;
} // :cond_6
/* .line 913 */
} // :cond_7
v4 = /* invoke-virtual/range {p6 ..p6}, Lcom/android/server/wm/ActivityRecord;->isActivityTypeHomeOrRecents()Z */
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 914 */
/* .line 917 */
} // :cond_8
final String v4 = "force create transition."; // const-string v4, "force create transition."
android.util.Slog .d ( v3,v4 );
/* .line 918 */
/* .line 901 */
} // :cond_9
/* move/from16 v10, p3 */
/* .line 921 */
} // .end local v8 # "homeProcess":Lcom/android/server/wm/WindowProcessController;
} // :cond_a
} // :goto_3
/* if-ne v0, v5, :cond_c */
/* iget v8, v1, Lcom/android/server/wm/Transition;->mType:I */
/* if-ne v8, v4, :cond_c */
/* .line 922 */
v8 = this.mParticipants;
v8 = (( android.util.ArraySet ) v8 ).isEmpty ( ); // invoke-virtual {v8}, Landroid/util/ArraySet;->isEmpty()Z
/* if-nez v8, :cond_c */
/* .line 923 */
v8 = this.mParticipants;
(( android.util.ArraySet ) v8 ).iterator ( ); // invoke-virtual {v8}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v9 = } // :goto_4
if ( v9 != null) { // if-eqz v9, :cond_c
/* check-cast v9, Lcom/android/server/wm/WindowContainer; */
/* .line 924 */
/* .local v9, "wc":Lcom/android/server/wm/WindowContainer; */
(( com.android.server.wm.WindowContainer ) v9 ).asActivityRecord ( ); // invoke-virtual {v9}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
/* .line 925 */
/* .local v11, "ar":Lcom/android/server/wm/ActivityRecord; */
if ( v11 != null) { // if-eqz v11, :cond_b
/* iget-boolean v12, v11, Lcom/android/server/wm/ActivityRecord;->finishing:Z */
if ( v12 != null) { // if-eqz v12, :cond_b
v12 = (( com.android.server.wm.ActivityRecord ) v11 ).isLaunchSourceType ( v4 ); // invoke-virtual {v11, v4}, Lcom/android/server/wm/ActivityRecord;->isLaunchSourceType(I)Z
if ( v12 != null) { // if-eqz v12, :cond_b
/* .line 926 */
v12 = (( com.android.server.wm.ActivityRecord ) v11 ).occludesParent ( v5 ); // invoke-virtual {v11, v5}, Lcom/android/server/wm/ActivityRecord;->occludesParent(Z)Z
/* if-nez v12, :cond_b */
if ( v2 != null) { // if-eqz v2, :cond_b
v12 = /* invoke-virtual/range {p6 ..p6}, Lcom/android/server/wm/ActivityRecord;->occludesParent()Z */
if ( v12 != null) { // if-eqz v12, :cond_b
/* .line 927 */
final String v4 = "Finish translucent activity start from home , force create open transition."; // const-string v4, "Finish translucent activity start from home , force create open transition."
android.util.Slog .d ( v3,v4 );
/* .line 928 */
/* .line 930 */
} // .end local v9 # "wc":Lcom/android/server/wm/WindowContainer;
} // .end local v11 # "ar":Lcom/android/server/wm/ActivityRecord;
} // :cond_b
/* .line 933 */
} // :cond_c
} // .end method
public void computeHoverModeBounds ( android.content.res.Configuration p0, android.graphics.Rect p1, android.graphics.Rect p2, com.android.server.wm.ActivityRecord p3 ) {
/* .locals 2 */
/* .param p1, "newParentConfiguration" # Landroid/content/res/Configuration; */
/* .param p2, "parentBounds" # Landroid/graphics/Rect; */
/* .param p3, "mTmpBounds" # Landroid/graphics/Rect; */
/* .param p4, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 702 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 703 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 704 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 705 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).computeHoverModeBounds ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiHoverModeInternal;->computeHoverModeBounds(Landroid/content/res/Configuration;Landroid/graphics/Rect;Landroid/graphics/Rect;Lcom/android/server/wm/ActivityRecord;)V
/* .line 708 */
} // :cond_0
return;
} // .end method
public Boolean disableSnapshotForApplock ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 660 */
v0 = com.android.server.wm.AppTransitionInjector .disableSnapshotForApplock ( p1,p2 );
} // .end method
public Boolean disableSplashScreen ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "componentName" # Ljava/lang/String; */
/* .line 501 */
v0 = com.android.server.wm.ActivityRecordImpl.BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN;
v0 = (( android.util.ArraySet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 502 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 506 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 503 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean disableSplashScreenForSplitScreen ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "componentName" # Ljava/lang/String; */
/* .line 510 */
v0 = com.android.server.wm.ActivityRecordImpl.BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN_IN_SPLIT_MODE;
v0 = (( android.util.ArraySet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 511 */
int v0 = 1; // const/4 v0, 0x1
/* .line 514 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void enterFreeformForHoverMode ( com.android.server.wm.Task p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "enter" # Z */
/* .line 778 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 779 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 780 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 781 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).enterFreeformForHoverMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->enterFreeformForHoverMode(Lcom/android/server/wm/Task;Z)V
/* .line 783 */
} // :cond_0
return;
} // .end method
public java.lang.String getClassName ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 541 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.intent;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.intent;
(( android.content.Intent ) v0 ).getComponent ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 542 */
v0 = this.intent;
(( android.content.Intent ) v0 ).getComponent ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v0 ).getClassName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 544 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getOptionsStyle ( android.app.ActivityOptions p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "options" # Landroid/app/ActivityOptions; */
/* .param p2, "isLaunchSourceType" # Z */
/* .line 664 */
v0 = (( android.app.ActivityOptions ) p1 ).getSplashScreenStyle ( ); // invoke-virtual {p1}, Landroid/app/ActivityOptions;->getSplashScreenStyle()I
/* .line 665 */
/* .local v0, "optionStyle":I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* if-nez p2, :cond_0 */
/* .line 666 */
int v0 = 0; // const/4 v0, 0x0
/* .line 668 */
} // :cond_0
} // .end method
public java.lang.String getOwnerPackageName ( com.android.server.wm.Task p0 ) {
/* .locals 9 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 579 */
(( com.android.server.wm.Task ) p1 ).getResumedActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 580 */
/* .local v0, "resumedActivity":Lcom/android/server/wm/ActivityRecord; */
final String v1 = " ownerPackageName = "; // const-string v1, " ownerPackageName = "
final String v2 = "displayId = "; // const-string v2, "displayId = "
final String v3 = "ActivityRecordImpl"; // const-string v3, "ActivityRecordImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 581 */
v4 = this.mDisplayContent;
/* .line 582 */
/* .local v4, "display":Lcom/android/server/wm/DisplayContent; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 583 */
v5 = this.mDisplayInfo;
v5 = this.ownerPackageName;
/* .line 584 */
/* .local v5, "ownerPackageName":Ljava/lang/String; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( com.android.server.wm.Task ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 585 */
/* .line 588 */
} // .end local v4 # "display":Lcom/android/server/wm/DisplayContent;
} // .end local v5 # "ownerPackageName":Ljava/lang/String;
} // :cond_0
(( com.android.server.wm.Task ) p1 ).getRootActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 589 */
/* .local v4, "rootActivity":Lcom/android/server/wm/ActivityRecord; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 590 */
v5 = this.mDisplayContent;
/* .line 591 */
/* .local v5, "display":Lcom/android/server/wm/DisplayContent; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 592 */
v6 = this.mDisplayInfo;
v6 = this.ownerPackageName;
/* .line 593 */
/* .local v6, "ownerPackageName":Ljava/lang/String; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "rootActivity = "; // const-string v8, "rootActivity = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 594 */
v7 = (( com.android.server.wm.Task ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 593 */
android.util.Slog .d ( v3,v1 );
/* .line 595 */
/* .line 598 */
} // .end local v5 # "display":Lcom/android/server/wm/DisplayContent;
} // .end local v6 # "ownerPackageName":Ljava/lang/String;
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean initSplitMode ( com.android.server.wm.ActivityRecord p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 549 */
v0 = (( android.content.Intent ) p2 ).isWebIntent ( ); // invoke-virtual {p2}, Landroid/content/Intent;->isWebIntent()Z
/* const/16 v1, 0x8 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 550 */
(( android.content.Intent ) p2 ).addMiuiFlags ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->addMiuiFlags(I)Landroid/content/Intent;
/* .line 552 */
} // :cond_0
v0 = (( android.content.Intent ) p2 ).getMiuiFlags ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getMiuiFlags()I
/* and-int/lit8 v0, v0, 0x4 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 553 */
v0 = (( android.content.Intent ) p2 ).getMiuiFlags ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getMiuiFlags()I
/* and-int/2addr v0, v1 */
/* if-nez v0, :cond_1 */
/* move v0, v3 */
} // :cond_1
/* move v0, v2 */
/* .line 554 */
/* .local v0, "isSplitMode":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 555 */
v4 = (( com.android.server.wm.ActivityRecord ) p1 ).occludesParent ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/server/wm/ActivityRecord;->occludesParent(Z)Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 556 */
v1 = this.mAtmService;
v1 = this.mWindowManager;
v1 = v1 = this.mPolicy;
/* if-nez v1, :cond_2 */
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z
if ( v1 != null) { // if-eqz v1, :cond_3
} // :cond_2
/* move v2, v3 */
} // :cond_3
(( com.android.server.wm.ActivityRecord ) p1 ).setOccludesParent ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/wm/ActivityRecord;->setOccludesParent(Z)Z
/* .line 557 */
} // :cond_4
v2 = (( android.content.Intent ) p2 ).getMiuiFlags ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getMiuiFlags()I
/* and-int/lit8 v2, v2, 0x10 */
/* if-nez v2, :cond_5 */
/* .line 558 */
(( android.content.Intent ) p2 ).addMiuiFlags ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->addMiuiFlags(I)Landroid/content/Intent;
/* .line 559 */
int v0 = 0; // const/4 v0, 0x0
/* .line 562 */
} // :cond_5
} // :goto_1
} // .end method
public Boolean isCompatibilityMode ( ) {
/* .locals 2 */
/* .line 532 */
/* nop */
/* .line 533 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 532 */
/* xor-int/lit8 v0, v0, 0x1 */
final String v1 = "persist.sys.miui_optimization"; // const-string v1, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
public Boolean isInCompatWhiteList ( android.content.pm.ActivityInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Landroid/content/pm/ActivityInfo; */
/* .line 954 */
v0 = com.android.server.wm.ActivityRecordImpl.COMPAT_WHITE_LIST;
v0 = v1 = this.name;
} // .end method
public Boolean isMiuiMwAnimationBelowStack ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "activity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 537 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isSecSplit ( Boolean p0, android.content.Intent p1 ) {
/* .locals 1 */
/* .param p1, "isSplitMode" # Z */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 567 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( android.content.Intent ) p2 ).getMiuiFlags ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getMiuiFlags()I
/* and-int/lit8 v0, v0, 0x40 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isWorldCirculate ( com.android.server.wm.Task p0 ) {
/* .locals 4 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 571 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 572 */
v1 = (( com.android.server.wm.Task ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I
/* .line 573 */
/* .local v1, "displayId":I */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v2 = "com.xiaomi.mirror"; // const-string v2, "com.xiaomi.mirror"
(( com.android.server.wm.ActivityRecordImpl ) p0 ).getOwnerPackageName ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityRecordImpl;->getOwnerPackageName(Lcom/android/server/wm/Task;)Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* .line 575 */
} // .end local v1 # "displayId":I
} // :cond_1
} // .end method
public Boolean miuiFullscreen ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 525 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 526 */
int v0 = 0; // const/4 v0, 0x0
/* .line 528 */
} // :cond_0
v0 = miui.os.MiuiInit .isRestrictAspect ( p1 );
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
public Boolean needSkipRelaunch ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "activityName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "changes" # I */
/* .line 947 */
v0 = v0 = com.android.server.wm.ActivityRecordImpl.SKIP_RELAUNCH_ACTIVITY_NAMES;
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez p2, :cond_1 */
} // :cond_0
v0 = com.android.server.wm.ActivityRecordImpl.FORCE_SKIP_RELAUNCH_ACTIVITY_NAMES;
v0 = /* .line 948 */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
/* const/high16 v0, -0x80000000 */
/* and-int/2addr v0, p3 */
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 947 */
} // :goto_0
} // .end method
public void notifyAppResumedFinished ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 424 */
v0 = this.mChildren;
v0 = (( com.android.server.wm.WindowList ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowList;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 425 */
(( com.android.server.wm.ActivityRecord ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
v0 = this.mUnknownAppVisibilityController;
(( com.android.server.wm.UnknownAppVisibilityController ) v0 ).appRemovedOrHidden ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/UnknownAppVisibilityController;->appRemovedOrHidden(Lcom/android/server/wm/ActivityRecord;)V
/* .line 427 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
v0 = this.mUnknownAppVisibilityController;
(( com.android.server.wm.UnknownAppVisibilityController ) v0 ).notifyAppResumedFinished ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/UnknownAppVisibilityController;->notifyAppResumedFinished(Lcom/android/server/wm/ActivityRecord;)V
/* .line 429 */
} // :goto_0
return;
} // .end method
public void onArCreated ( com.android.server.wm.ActivityRecord p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "isUnStandardLaunchMode" # Z */
/* .line 693 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 694 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 695 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 696 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onArCreated ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onArCreated(Lcom/android/server/wm/ActivityRecord;Z)V
/* .line 698 */
} // :cond_0
return;
} // .end method
public void onArParentChanged ( com.android.server.wm.TaskFragment p0, com.android.server.wm.TaskFragment p1, com.android.server.wm.ActivityRecord p2 ) {
/* .locals 2 */
/* .param p1, "oldParent" # Lcom/android/server/wm/TaskFragment; */
/* .param p2, "newParent" # Lcom/android/server/wm/TaskFragment; */
/* .param p3, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 755 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 756 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 757 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 758 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onArParentChanged ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/MiuiHoverModeInternal;->onArParentChanged(Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/ActivityRecord;)V
/* .line 760 */
} // :cond_0
return;
} // .end method
public void onArStateChanged ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord$State p1 ) {
/* .locals 9 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "state" # Lcom/android/server/wm/ActivityRecord$State; */
/* .line 763 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 764 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 765 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 766 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onArStateChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onArStateChanged(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;)V
/* .line 769 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
v1 = this.mActivityComponent;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 770 */
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I
java.lang.Integer .valueOf ( v1 );
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
java.lang.Integer .valueOf ( v1 );
v1 = this.mActivityComponent;
/* .line 771 */
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v1 = this.mActivityComponent;
(( android.content.ComponentName ) v1 ).getClassName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v6 = this.processName;
/* .line 772 */
v1 = java.lang.System .identityHashCode ( p1 );
java.lang.Integer .valueOf ( v1 );
v1 = (( com.android.server.wm.ActivityRecord$State ) p2 ).ordinal ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord$State;->ordinal()I
java.lang.Integer .valueOf ( v1 );
/* filled-new-array/range {v2 ..v8}, [Ljava/lang/Object; */
/* .line 770 */
final String v2 = "notifyActivityStateChange"; // const-string v2, "notifyActivityStateChange"
com.android.server.camera.CameraOpt .callMethod ( v2,v1 );
/* .line 775 */
} // :cond_1
return;
} // .end method
public void onArVisibleChanged ( com.android.server.wm.ActivityRecord p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "visible" # Z */
/* .line 720 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 721 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 722 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 723 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onArVisibleChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onArVisibleChanged(Lcom/android/server/wm/ActivityRecord;Z)V
/* .line 725 */
} // :cond_0
return;
} // .end method
public void onWindowsVisible ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .line 603 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
/* .line 604 */
/* .local v0, "atmsImpl":Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).getMiuiSizeCompatIn ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMiuiSizeCompatIn()Lcom/android/server/wm/MiuiSizeCompatInternal;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 605 */
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).getMiuiSizeCompatIn ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMiuiSizeCompatIn()Lcom/android/server/wm/MiuiSizeCompatInternal;
(( com.android.server.wm.MiuiSizeCompatInternal ) v1 ).showWarningNotification ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->showWarningNotification(Lcom/android/server/wm/ActivityRecord;)V
/* .line 607 */
} // :cond_0
return;
} // .end method
public void resetTaskFragmentWindowModeForHoverMode ( com.android.server.wm.TaskFragment p0, android.content.res.Configuration p1 ) {
/* .locals 2 */
/* .param p1, "tf" # Lcom/android/server/wm/TaskFragment; */
/* .param p2, "newParentConfiguration" # Landroid/content/res/Configuration; */
/* .line 738 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 739 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 740 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 741 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).resetTaskFragmentRequestWindowMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->resetTaskFragmentRequestWindowMode(Lcom/android/server/wm/TaskFragment;Landroid/content/res/Configuration;)V
/* .line 743 */
} // :cond_0
return;
} // .end method
public void setRequestedOrientationForHover ( com.android.server.wm.ActivityRecord p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "requestOrientation" # I */
/* .param p3, "orientation" # I */
/* .line 746 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 747 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 748 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 749 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).setOrientationForHoverMode ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/MiuiHoverModeInternal;->setOrientationForHoverMode(Lcom/android/server/wm/ActivityRecord;II)V
/* .line 751 */
} // :cond_0
return;
} // .end method
public void setRequestedWindowModeForHoverMode ( com.android.server.wm.ActivityRecord p0, android.content.res.Configuration p1 ) {
/* .locals 2 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "newParentConfiguration" # Landroid/content/res/Configuration; */
/* .line 729 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 730 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 731 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 732 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).setRequestedWindowModeForHoverMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->setRequestedWindowModeForHoverMode(Lcom/android/server/wm/ActivityRecord;Landroid/content/res/Configuration;)V
/* .line 734 */
} // :cond_0
return;
} // .end method
public Integer shouldClearActivityInfoFlags ( ) {
/* .locals 1 */
/* .line 625 */
v0 = com.android.server.input.padkeyboard.MiuiPadKeyboardManager .shouldClearActivityInfoFlags ( );
} // .end method
public Boolean shouldHookHoverConfig ( android.content.res.Configuration p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 2 */
/* .param p1, "newParentConfiguration" # Landroid/content/res/Configuration; */
/* .param p2, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 711 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 712 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 713 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 714 */
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).shouldHookHoverConfig ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->shouldHookHoverConfig(Landroid/content/res/Configuration;Lcom/android/server/wm/ActivityRecord;)Z
/* .line 716 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean shouldNotBeResume ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 433 */
if ( p1 != null) { // if-eqz p1, :cond_0
(( com.android.server.wm.ActivityRecord ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 435 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayId()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 436 */
(( com.android.server.wm.ActivityRecord ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
v0 = (( com.android.server.wm.DisplayContent ) v0 ).isSleeping ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->isSleeping()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.packageName;
/* .line 437 */
final String v1 = "com.xiaomi.misubscreenui"; // const-string v1, "com.xiaomi.misubscreenui"
v0 = android.text.TextUtils .equals ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 433 */
} // :goto_0
} // .end method
public Boolean shouldRelaunchForBlur ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 11 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .line 787 */
final String v0 = "miui.blurRelaunch"; // const-string v0, "miui.blurRelaunch"
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_5
v2 = this.mAtmService;
if ( v2 != null) { // if-eqz v2, :cond_5
v2 = this.packageName;
/* if-nez v2, :cond_0 */
/* .line 790 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v9 */
/* .line 791 */
/* .local v9, "startTime":J */
v2 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;
/* .line 793 */
/* .local v2, "pm":Landroid/content/pm/IPackageManager; */
try { // :try_start_0
v3 = this.packageName;
/* iget v4, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
int v5 = 0; // const/4 v5, 0x0
/* .line 795 */
/* .local v3, "appProp":Landroid/content/pm/PackageManager$Property; */
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_1
v5 = (( android.content.pm.PackageManager$Property ) v3 ).isBoolean ( ); // invoke-virtual {v3}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = (( android.content.pm.PackageManager$Property ) v3 ).getBoolean ( ); // invoke-virtual {v3}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* move v5, v4 */
} // :cond_1
/* move v5, v1 */
/* .line 796 */
/* .local v5, "appBlur":Z */
} // :goto_0
v6 = this.packageName;
v7 = this.mActivityComponent;
/* .line 797 */
(( android.content.ComponentName ) v7 ).getClassName ( ); // invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* iget v8, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
/* .line 796 */
/* .line 798 */
/* .local v0, "actProp":Landroid/content/pm/PackageManager$Property; */
if ( v5 != null) { // if-eqz v5, :cond_3
if ( v0 != null) { // if-eqz v0, :cond_2
v6 = (( android.content.pm.PackageManager$Property ) v0 ).isBoolean ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z
/* if-nez v6, :cond_3 */
/* .line 799 */
} // :cond_2
/* .line 801 */
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_4
v6 = (( android.content.pm.PackageManager$Property ) v0 ).isBoolean ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z
if ( v6 != null) { // if-eqz v6, :cond_4
v6 = (( android.content.pm.PackageManager$Property ) v0 ).getBoolean ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 802 */
/* .line 806 */
} // .end local v0 # "actProp":Landroid/content/pm/PackageManager$Property;
} // .end local v3 # "appProp":Landroid/content/pm/PackageManager$Property;
} // .end local v5 # "appBlur":Z
} // :cond_4
/* .line 804 */
/* :catch_0 */
/* move-exception v0 */
/* .line 805 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v3 = "ActivityRecordImpl"; // const-string v3, "ActivityRecordImpl"
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 807 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
/* const-wide/16 v6, 0x32 */
/* const-string/jumbo v8, "shouldRelaunchForBlur" */
/* move-object v3, p0 */
/* move-wide v4, v9 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/wm/ActivityRecordImpl;->checkSlow(JJLjava/lang/String;)V */
/* .line 808 */
/* .line 788 */
} // .end local v2 # "pm":Landroid/content/pm/IPackageManager;
} // .end local v9 # "startTime":J
} // :cond_5
} // :goto_2
} // .end method
public Boolean shouldSkipChanged ( com.android.server.wm.ActivityRecord p0, Integer p1 ) {
/* .locals 11 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "changes" # I */
/* .line 813 */
final String v0 = "miui.blurChanged"; // const-string v0, "miui.blurChanged"
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_6
v2 = this.mAtmService;
if ( v2 != null) { // if-eqz v2, :cond_6
v2 = this.packageName;
if ( v2 != null) { // if-eqz v2, :cond_6
/* const/high16 v2, 0x100000 */
/* and-int/2addr v2, p2 */
/* if-nez v2, :cond_0 */
/* goto/16 :goto_4 */
/* .line 817 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v9 */
/* .line 818 */
/* .local v9, "startTime":J */
v2 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;
/* .line 820 */
/* .local v2, "pm":Landroid/content/pm/IPackageManager; */
try { // :try_start_0
v3 = this.packageName;
/* iget v4, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
int v5 = 0; // const/4 v5, 0x0
/* .line 822 */
/* .local v3, "appProp":Landroid/content/pm/PackageManager$Property; */
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_2
v5 = (( android.content.pm.PackageManager$Property ) v3 ).isBoolean ( ); // invoke-virtual {v3}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z
if ( v5 != null) { // if-eqz v5, :cond_2
v5 = (( android.content.pm.PackageManager$Property ) v3 ).getBoolean ( ); // invoke-virtual {v3}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z
/* if-nez v5, :cond_1 */
} // :cond_1
/* move v5, v1 */
} // :cond_2
} // :goto_0
/* move v5, v4 */
/* .line 823 */
/* .local v5, "appPropFalse":Z */
} // :goto_1
v6 = this.packageName;
v7 = this.mActivityComponent;
/* .line 824 */
(( android.content.ComponentName ) v7 ).getClassName ( ); // invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* iget v8, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
/* .line 823 */
/* .line 825 */
/* .local v0, "actProp":Landroid/content/pm/PackageManager$Property; */
if ( v0 != null) { // if-eqz v0, :cond_4
v6 = (( android.content.pm.PackageManager$Property ) v0 ).isBoolean ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z
if ( v6 != null) { // if-eqz v6, :cond_4
v6 = (( android.content.pm.PackageManager$Property ) v0 ).getBoolean ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v6, :cond_3 */
} // :cond_3
/* move v6, v1 */
} // :cond_4
} // :goto_2
/* move v6, v4 */
/* .line 826 */
/* .local v6, "actPropFalse":Z */
} // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_5
if ( v6 != null) { // if-eqz v6, :cond_5
/* move v1, v4 */
} // :cond_5
/* .line 827 */
} // .end local v0 # "actProp":Landroid/content/pm/PackageManager$Property;
} // .end local v3 # "appProp":Landroid/content/pm/PackageManager$Property;
} // .end local v5 # "appPropFalse":Z
} // .end local v6 # "actPropFalse":Z
/* :catch_0 */
/* move-exception v0 */
/* .line 828 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v3 = "ActivityRecordImpl"; // const-string v3, "ActivityRecordImpl"
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 830 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* const-wide/16 v6, 0x32 */
/* const-string/jumbo v8, "shouldSkipChanged" */
/* move-object v3, p0 */
/* move-wide v4, v9 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/wm/ActivityRecordImpl;->checkSlow(JJLjava/lang/String;)V */
/* .line 831 */
/* .line 815 */
} // .end local v2 # "pm":Landroid/content/pm/IPackageManager;
} // .end local v9 # "startTime":J
} // :cond_6
} // :goto_4
} // .end method
public Boolean shouldSkipCompatMode ( com.android.server.wm.WindowContainer p0, com.android.server.wm.WindowContainer p1 ) {
/* .locals 2 */
/* .param p1, "oldParent" # Lcom/android/server/wm/WindowContainer; */
/* .param p2, "newParent" # Lcom/android/server/wm/WindowContainer; */
/* .line 674 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
/* .line 676 */
/* .local v0, "atmsImpl":Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).isInSystemSplitScreen ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
/* if-nez v1, :cond_0 */
/* .line 677 */
v1 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).isInSystemSplitScreen ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 678 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 680 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean skipLetterboxInSplitScreen ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 686 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_2
(( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
/* if-nez v1, :cond_0 */
/* .line 687 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
/* iget-boolean v1, v1, Lcom/android/server/wm/Task;->mSoScRoot:Z */
/* if-nez v1, :cond_1 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 686 */
} // :cond_2
} // :goto_0
} // .end method
public void updateLatterboxParam ( Boolean p0, android.graphics.Point p1 ) {
/* .locals 1 */
/* .param p1, "isEmbedded" # Z */
/* .param p2, "surfaceOrigin" # Landroid/graphics/Point; */
/* .line 618 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 619 */
int v0 = 0; // const/4 v0, 0x0
(( android.graphics.Point ) p2 ).set ( v0, v0 ); // invoke-virtual {p2, v0, v0}, Landroid/graphics/Point;->set(II)V
/* .line 621 */
} // :cond_0
return;
} // .end method
public void updateSpaceToFill ( Boolean p0, android.graphics.Rect p1, android.graphics.Rect p2 ) {
/* .locals 4 */
/* .param p1, "isEmbedded" # Z */
/* .param p2, "spaceToFill" # Landroid/graphics/Rect; */
/* .param p3, "windowFrame" # Landroid/graphics/Rect; */
/* .line 613 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 614 */
/* iget v0, p2, Landroid/graphics/Rect;->left:I */
/* iget v1, p3, Landroid/graphics/Rect;->top:I */
/* iget v2, p2, Landroid/graphics/Rect;->right:I */
/* iget v3, p3, Landroid/graphics/Rect;->bottom:I */
(( android.graphics.Rect ) p2 ).set ( v0, v1, v2, v3 ); // invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V
/* .line 616 */
} // :cond_0
return;
} // .end method
