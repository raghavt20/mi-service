.class Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;
.super Lcom/android/server/wm/Letterbox$LetterboxSurface;
.source "LetterboxImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/LetterboxImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LetterboxSurfaceEx"
.end annotation


# instance fields
.field private mBackgroundBitmap:Landroid/graphics/Bitmap;

.field private mBackgroundSurface:Landroid/view/Surface;

.field mPaint:Landroid/graphics/Paint;

.field final synthetic this$0:Lcom/android/server/wm/LetterboxImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/LetterboxImpl;Ljava/lang/String;Lcom/android/server/wm/LetterboxImpl;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/wm/LetterboxImpl;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "impl"    # Lcom/android/server/wm/LetterboxImpl;

    .line 208
    iput-object p1, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    .line 209
    invoke-static {p3}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmLetterbox(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/Letterbox;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, p2}, Lcom/android/server/wm/Letterbox$LetterboxSurface;-><init>(Lcom/android/server/wm/Letterbox;Ljava/lang/String;)V

    .line 206
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mPaint:Landroid/graphics/Paint;

    .line 210
    return-void
.end method

.method private applyLetterboxSurfaceChangesForActivityEmbedding(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 3
    .param p1, "mSurfaceFrameRelative"    # Landroid/graphics/Rect;
    .param p2, "mLayoutFrameRelative"    # Landroid/graphics/Rect;

    .line 397
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v0}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 398
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 399
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 398
    :goto_0
    return v1

    .line 401
    :cond_1
    return v1
.end method

.method private drawBackgroundBitmapLocked(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 12
    .param p1, "bgBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rect"    # Landroid/graphics/Rect;

    .line 245
    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundSurface:Landroid/view/Surface;

    if-eqz v0, :cond_5

    .line 246
    const/4 v1, 0x0

    .line 248
    .local v1, "canvas":Landroid/graphics/Canvas;
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 251
    goto :goto_0

    .line 249
    :catch_0
    move-exception v0

    .line 250
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v3, "LetterboxImpl"

    const-string v4, "Failed to lock canvas"

    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 252
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :goto_0
    if-nez v1, :cond_0

    .line 253
    return-void

    .line 255
    :cond_0
    const/4 v0, 0x0

    .line 256
    .local v0, "left":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    .line 257
    .local v3, "right":I
    const/4 v4, 0x0

    .line 258
    .local v4, "top":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 259
    .local v5, "bottom":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v7

    if-lt v6, v7, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v7

    if-lt v6, v7, :cond_1

    .line 260
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 261
    .local v6, "inWidth":I
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v7

    .line 262
    .local v7, "outWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 263
    .local v8, "inHeight":I
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v9

    .line 264
    .local v9, "outHeight":I
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 265
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 266
    sub-int v10, v6, v7

    int-to-float v10, v10

    const/high16 v11, 0x3f000000    # 0.5f

    mul-float/2addr v10, v11

    float-to-int v0, v10

    .line 267
    add-int v3, v0, v7

    .line 268
    sub-int v10, v8, v9

    int-to-float v10, v10

    mul-float/2addr v10, v11

    float-to-int v4, v10

    .line 269
    add-int v5, v4, v9

    .line 271
    .end local v6    # "inWidth":I
    .end local v7    # "outWidth":I
    .end local v8    # "inHeight":I
    .end local v9    # "outHeight":I
    :cond_1
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v0, v4, v3, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, p1, v6, p2, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 272
    iget-object v2, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v2}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 273
    iget-object v2, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v2}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getTaskFragment()Lcom/android/server/wm/TaskFragment;

    move-result-object v2

    .line 274
    .local v2, "taskFragment":Lcom/android/server/wm/TaskFragment;
    if-eqz v2, :cond_4

    .line 275
    invoke-virtual {v2}, Lcom/android/server/wm/TaskFragment;->getAdjacentTaskFragment()Lcom/android/server/wm/TaskFragment;

    move-result-object v6

    .line 276
    .local v6, "adjacentTaskFragment":Lcom/android/server/wm/TaskFragment;
    if-eqz v6, :cond_4

    .line 277
    iget-object v7, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v7}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v7

    iget-object v7, v7, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v7, v7, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 278
    .local v7, "context":Landroid/content/Context;
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v8, v8, 0x30

    const/16 v9, 0x20

    if-ne v8, v9, :cond_2

    const/4 v8, 0x1

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    .line 280
    .local v8, "isDarkMode":Z
    :goto_1
    iget-object v9, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mPaint:Landroid/graphics/Paint;

    if-eqz v8, :cond_3

    .line 281
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v10

    iget-object v11, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v11}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v11

    iget-object v11, v11, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v10, v11}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->getSplitLineDarkColor(Ljava/lang/String;)I

    move-result v10

    goto :goto_2

    .line 282
    :cond_3
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v10

    iget-object v11, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v11}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v11

    iget-object v11, v11, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v10, v11}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->getSplitLineLightColor(Ljava/lang/String;)I

    move-result v10

    .line 280
    :goto_2
    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 283
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 284
    .local v9, "splitLineRect":Landroid/graphics/Rect;
    invoke-direct {p0, v2, v6, p2, v9}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getSplitLineRect(Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/TaskFragment;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 285
    invoke-virtual {v9}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    .line 286
    iget-object v10, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v9, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 291
    .end local v2    # "taskFragment":Lcom/android/server/wm/TaskFragment;
    .end local v6    # "adjacentTaskFragment":Lcom/android/server/wm/TaskFragment;
    .end local v7    # "context":Landroid/content/Context;
    .end local v8    # "isDarkMode":Z
    .end local v9    # "splitLineRect":Landroid/graphics/Rect;
    :cond_4
    iget-object v2, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundSurface:Landroid/view/Surface;

    invoke-virtual {v2, v1}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 293
    .end local v0    # "left":I
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "right":I
    .end local v4    # "top":I
    .end local v5    # "bottom":I
    :cond_5
    return-void
.end method

.method private getBitmapSurfaceParams(Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "rect"    # Landroid/graphics/Rect;

    .line 308
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v0}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    if-nez v0, :cond_0

    .line 309
    return-void

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v0}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 311
    .local v0, "dc":Lcom/android/server/wm/DisplayContent;
    iget-object v1, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v1}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayContent;->isFixedRotationLaunchingApp(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 312
    iget-object v1, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v1}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getFixedRotationTransformDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v1

    .line 313
    .local v1, "displayInfo":Landroid/view/DisplayInfo;
    :goto_0
    if-nez v1, :cond_2

    .line 314
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skip getBitmapSurfaceParams, displayInfo == null ,mActivityRecord = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v3}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,isFixedRotationLaunchingApp = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v3}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 316
    invoke-virtual {v0, v3}, Lcom/android/server/wm/DisplayContent;->isFixedRotationLaunchingApp(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 314
    const-string v3, "LetterboxImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    return-void

    .line 320
    :cond_2
    iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    iget v3, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    const/4 v4, 0x0

    invoke-virtual {p1, v4, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 334
    return-void
.end method

.method private getSplitLineRect(Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/TaskFragment;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "tf1"    # Lcom/android/server/wm/TaskFragment;
    .param p2, "tf2"    # Lcom/android/server/wm/TaskFragment;
    .param p3, "bitmapRect"    # Landroid/graphics/Rect;
    .param p4, "rect"    # Landroid/graphics/Rect;

    .line 296
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 297
    invoke-virtual {p1}, Lcom/android/server/wm/TaskFragment;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 298
    .local v0, "tf1Rect":Landroid/graphics/Rect;
    invoke-virtual {p2}, Lcom/android/server/wm/TaskFragment;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 299
    .local v1, "tf2Rect":Landroid/graphics/Rect;
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, v1, Landroid/graphics/Rect;->left:I

    if-ge v2, v3, :cond_0

    .line 300
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, p3, Landroid/graphics/Rect;->top:I

    iget v4, v1, Landroid/graphics/Rect;->left:I

    iget v5, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p4, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 302
    :cond_0
    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v3, p3, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v5, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p4, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 305
    .end local v0    # "tf1Rect":Landroid/graphics/Rect;
    .end local v1    # "tf2Rect":Landroid/graphics/Rect;
    :cond_1
    :goto_0
    return-void
.end method

.method private getWallpaperBitmap()Z
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v0}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 214
    return v1

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v0}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getBlurWallpaperBmp()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 217
    .local v0, "backgroundBitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 218
    return v1

    .line 220
    :cond_1
    iput-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundBitmap:Landroid/graphics/Bitmap;

    .line 221
    const/4 v1, 0x1

    return v1
.end method


# virtual methods
.method public applyLetterboxSurfaceChanges(Landroid/view/SurfaceControl$Transaction;Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/String;)Z
    .locals 1
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "mSurfaceFrameRelative"    # Landroid/graphics/Rect;
    .param p3, "mLayoutFrameRelative"    # Landroid/graphics/Rect;
    .param p4, "mType"    # Ljava/lang/String;

    .line 386
    invoke-virtual {p2, p3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    const-string v0, "bitmapBackground"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 389
    invoke-direct {p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getWallpaperBitmap()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 390
    invoke-direct {p0, p2, p3}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->applyLetterboxSurfaceChangesForActivityEmbedding(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    const/4 v0, 0x1

    return v0

    .line 393
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public createFlipSmallSurface(Landroid/view/SurfaceControl$Transaction;Ljava/util/function/Supplier;)Landroid/view/SurfaceControl;
    .locals 2
    .param p1, "sct"    # Landroid/view/SurfaceControl$Transaction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/SurfaceControl$Transaction;",
            "Ljava/util/function/Supplier<",
            "Landroid/view/SurfaceControl$Builder;",
            ">;)",
            "Landroid/view/SurfaceControl;"
        }
    .end annotation

    .line 339
    .local p2, "supplier":Ljava/util/function/Supplier;, "Ljava/util/function/Supplier<Landroid/view/SurfaceControl$Builder;>;"
    invoke-interface {p2}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceControl$Builder;

    .line 340
    const-string v1, "Letterbox - NBC"

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    .line 341
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setFlags(I)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    .line 342
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->setColorLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    .line 343
    const-string v1, "LetterboxSurface.createSurface"

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    .line 344
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v0

    .line 345
    .local v0, "surfaceControl":Landroid/view/SurfaceControl;
    iget-object v1, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v1, v0, p1}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$mupdateColor(Lcom/android/server/wm/LetterboxImpl;Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;)V

    .line 346
    invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 347
    return-object v0
.end method

.method public createSurface(Landroid/view/SurfaceControl$Transaction;Ljava/lang/String;Ljava/util/function/Supplier;)Landroid/view/SurfaceControl;
    .locals 6
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "mType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/SurfaceControl$Transaction;",
            "Ljava/lang/String;",
            "Ljava/util/function/Supplier<",
            "Landroid/view/SurfaceControl$Builder;",
            ">;)",
            "Landroid/view/SurfaceControl;"
        }
    .end annotation

    .line 352
    .local p3, "mSurfaceControlFactory":Ljava/util/function/Supplier;, "Ljava/util/function/Supplier<Landroid/view/SurfaceControl$Builder;>;"
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 353
    .local v0, "rect":Landroid/graphics/Rect;
    invoke-direct {p0, v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getBitmapSurfaceParams(Landroid/graphics/Rect;)V

    .line 354
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 355
    .local v2, "longSide":I
    iget-object v3, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v3}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v3}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->isLetterboxedForFixedOrientationAndAspectRatio()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v1, v1, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_1

    :cond_1
    :goto_0
    move-object v3, v0

    :goto_1
    move-object v1, v3

    .line 356
    .local v1, "bitmapRect":Landroid/graphics/Rect;
    invoke-interface {p3}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceControl$Builder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Letterbox - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v5}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 357
    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 358
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setFlags(I)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 360
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/SurfaceControl$Builder;->setBufferSize(II)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 361
    const-string v4, "LetterboxSurface.createSurface"

    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 362
    invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v3

    .line 363
    .local v3, "mSurface":Landroid/view/SurfaceControl;
    invoke-direct {p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getWallpaperBitmap()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    .line 364
    new-instance v4, Landroid/view/Surface;

    invoke-direct {v4, v3}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceControl;)V

    iput-object v4, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundSurface:Landroid/view/Surface;

    .line 365
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v5}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " create "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "bitmapBackground"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "LetterboxImpl"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget-object v4, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v4, v1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->drawBackgroundBitmapLocked(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 367
    iget-object v4, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v4}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v4}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->isLetterboxedForFixedOrientationAndAspectRatio()Z

    move-result v4

    if-nez v4, :cond_2

    .line 368
    invoke-virtual {p1, v3}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 371
    :cond_2
    return-object v3
.end method

.method public needRemoveMiuiLetterbox()Z
    .locals 1

    .line 422
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundSurface:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public redrawBitmapSurface()V
    .locals 5

    .line 405
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 406
    .local v0, "rect":Landroid/graphics/Rect;
    invoke-direct {p0, v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getBitmapSurfaceParams(Landroid/graphics/Rect;)V

    .line 407
    iget-object v2, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundSurface:Landroid/view/Surface;

    if-nez v2, :cond_1

    .line 408
    invoke-virtual {p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getSurface()Landroid/view/SurfaceControl;

    move-result-object v2

    if-nez v2, :cond_0

    .line 409
    return-void

    .line 411
    :cond_0
    new-instance v2, Landroid/view/Surface;

    invoke-virtual {p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getSurface()Landroid/view/SurfaceControl;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceControl;)V

    iput-object v2, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundSurface:Landroid/view/Surface;

    .line 413
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 414
    .local v2, "longSide":I
    iget-object v3, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    .line 415
    iget-object v3, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v3}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v3}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->isLetterboxedForFixedOrientationAndAspectRatio()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v1, v1, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_1

    :cond_3
    :goto_0
    move-object v3, v0

    :goto_1
    move-object v1, v3

    .line 416
    .local v1, "bitmapRect":Landroid/graphics/Rect;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v4}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " redraw "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "bitmapBackground"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "LetterboxImpl"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    iget-object v3, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v3, v1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->drawBackgroundBitmapLocked(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 419
    .end local v1    # "bitmapRect":Landroid/graphics/Rect;
    :cond_4
    return-void
.end method

.method public remove()V
    .locals 2

    .line 375
    invoke-super {p0}, Lcom/android/server/wm/Letterbox$LetterboxSurface;->remove()V

    .line 376
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundSurface:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "remove letterbox surface bitmapBackground for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->this$0:Lcom/android/server/wm/LetterboxImpl;

    invoke-static {v1}, Lcom/android/server/wm/LetterboxImpl;->-$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LetterboxImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 379
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mShowed:Z

    .line 380
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mBackgroundSurface:Landroid/view/Surface;

    .line 382
    :cond_0
    return-void
.end method
