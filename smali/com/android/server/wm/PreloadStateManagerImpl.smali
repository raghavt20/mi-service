.class public Lcom/android/server/wm/PreloadStateManagerImpl;
.super Ljava/lang/Object;
.source "PreloadStateManagerImpl.java"

# interfaces
.implements Lcom/android/server/wm/PreloadStateManagerStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;,
        Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;,
        Lcom/android/server/wm/PreloadStateManagerImpl$GreezeCallback;
    }
.end annotation


# static fields
.field public static final ADVANCE_KILL_PRELOADAPP_REASON:Ljava/lang/String; = "advance_kill_preloadApp"

.field public static final DELAY_STOP_PRELOAD_ACTIVITIES_MSG:I = 0xc7

.field public static final MUTE_PRELOAD_UID:Ljava/lang/String; = "mute_preload_uid="

.field private static final PERMISSION_ACTIVITY_NAME:Ljava/lang/String; = "permission.ui.GrantPermissionsActivity"

.field private static final PRELOAD_DISPLAY_NAME:Ljava/lang/String; = "PreloadDisplay"

.field public static final REMOVE_MUTE_PRELOAD_UID:Ljava/lang/String; = "remove_mute_preload_uid="

.field private static final TAG:Ljava/lang/String; = "PreloadStateManagerImpl"

.field public static final TIMEOUT_KILL_PRELOADAPP_REASON:Ljava/lang/String; = "timeout_kill_preloadApp"

.field private static mService:Lcom/android/server/wm/ActivityTaskManagerService;

.field private static final sEnableAudioLock:Ljava/lang/Object;

.field private static sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

.field private static sHorizontalPacakges:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sHorizontalPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

.field private static sPreloadAppStarted:Z

.field private static sPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

.field private static sPreloadUidInfos:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

.field private static sWaitEnableAudioUids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$sfgetsPreloadUidInfos()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$smdisableAudio(ZI)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->disableAudio(ZI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smonPreloadAppKilled(I)V
    .locals 0

    invoke-static {p0}, Lcom/android/server/wm/PreloadStateManagerImpl;->onPreloadAppKilled(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sEnableAudioLock:Ljava/lang/Object;

    .line 69
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sWaitEnableAudioUids:Ljava/util/List;

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sHorizontalPacakges:Ljava/util/Set;

    .line 90
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadAppStarted:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkEnablePreload()Z
    .locals 4

    .line 75
    const/4 v0, 0x0

    :try_start_0
    const-class v1, Lcom/android/server/wm/Task;

    const-string v2, "removeStopPreloadActivityMsg"

    new-array v3, v0, [Ljava/lang/Class;

    invoke-static {v1, v2, v3}, Lmiui/util/ReflectionUtils;->findMethodBestMatch(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    nop

    .line 83
    const/4 v0, 0x1

    return v0

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v2, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 79
    const-string v2, "PreloadStateManagerImpl"

    const-string v3, "preloadApp lack of some changes"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 81
    :cond_0
    return v0
.end method

.method private static createPreloadDisplay(I)Landroid/hardware/display/VirtualDisplay;
    .locals 11
    .param p0, "orientation"    # I

    .line 568
    new-instance v0, Landroid/view/DisplayInfo;

    invoke-direct {v0}, Landroid/view/DisplayInfo;-><init>()V

    .line 569
    .local v0, "info":Landroid/view/DisplayInfo;
    new-instance v6, Landroid/view/Surface;

    invoke-direct {v6}, Landroid/view/Surface;-><init>()V

    .line 570
    .local v6, "surface":Landroid/view/Surface;
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    const-class v2, Landroid/hardware/display/DisplayManager;

    .line 571
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/hardware/display/DisplayManager;

    .line 572
    .local v8, "displayManager":Landroid/hardware/display/DisplayManager;
    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    .line 573
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v9

    .line 574
    .local v9, "origId":J
    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    .line 575
    iget v1, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    .line 576
    .local v1, "h":I
    iget v2, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    iput v2, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    .line 577
    iput v1, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    .line 580
    .end local v1    # "h":I
    :cond_0
    const-string v2, "PreloadDisplay"

    iget v3, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    iget v4, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    iget v5, v0, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    const/4 v7, 0x4

    move-object v1, v8

    invoke-virtual/range {v1 .. v7}, Landroid/hardware/display/DisplayManager;->createVirtualDisplay(Ljava/lang/String;IIILandroid/view/Surface;I)Landroid/hardware/display/VirtualDisplay;

    move-result-object v1

    .line 583
    .local v1, "display":Landroid/hardware/display/VirtualDisplay;
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 584
    return-object v1
.end method

.method private delayFreezeAppForUid(Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;)V
    .locals 5
    .param p1, "info"    # Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    .line 492
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 493
    .local v0, "msgFreeze":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 494
    iget v1, p1, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    invoke-static {v1}, Lcom/android/server/am/PreloadAppControllerImpl;->queryFreezeTimeoutFromUid(I)J

    move-result-wide v1

    .line 495
    .local v1, "freezeTimeout":J
    sget-object v3, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    invoke-virtual {v3, v0, v1, v2}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 496
    sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 497
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "preloadApp onAttachApplication uid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " freezeTimeout="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "PreloadStateManagerImpl"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    :cond_0
    return-void
.end method

.method private static disableAudio(ZI)V
    .locals 2
    .param p0, "disable"    # Z
    .param p1, "uid"    # I

    .line 153
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "preloadApp enableAudio "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PreloadStateManagerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    if-eqz p0, :cond_1

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mute_preload_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    goto :goto_0

    .line 159
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "remove_mute_preload_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 161
    :goto_0
    return-void
.end method

.method public static enableAudio(I)V
    .locals 4
    .param p0, "uid"    # I

    .line 588
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 589
    .local v0, "enableMsg":Landroid/os/Message;
    iput p0, v0, Landroid/os/Message;->arg1:I

    .line 590
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 591
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 592
    return-void
.end method

.method public static getOrCreatePreloadDisplayId()I
    .locals 4

    .line 520
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    if-nez v0, :cond_1

    .line 521
    const-class v0, Lcom/android/server/wm/PreloadStateManagerImpl;

    monitor-enter v0

    .line 522
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->createPreloadDisplay(I)Landroid/hardware/display/VirtualDisplay;

    move-result-object v1

    sput-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    .line 523
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    .local v1, "service":Lcom/miui/server/greeze/GreezeManagerInternal;
    if-eqz v1, :cond_0

    .line 526
    :try_start_1
    new-instance v2, Lcom/android/server/wm/PreloadStateManagerImpl$GreezeCallback;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/android/server/wm/PreloadStateManagerImpl$GreezeCallback;-><init>(Lcom/android/server/wm/PreloadStateManagerImpl$GreezeCallback-IA;)V

    sget v3, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_PRELOAD:I

    invoke-virtual {v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerInternal;->registerCallback(Lmiui/greeze/IGreezeCallback;I)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 529
    :catch_0
    move-exception v2

    .line 530
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 531
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    nop

    .line 532
    .end local v1    # "service":Lcom/miui/server/greeze/GreezeManagerInternal;
    :goto_1
    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 534
    :cond_1
    :goto_2
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    return v0
.end method

.method public static getOrCreatePreloadDisplayId(Ljava/lang/String;)I
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;

    .line 538
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sHorizontalPacakges:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 539
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sHorizontalPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    if-nez v0, :cond_0

    .line 540
    const-class v0, Lcom/android/server/wm/PreloadStateManagerImpl;

    monitor-enter v0

    .line 541
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->createPreloadDisplay(I)Landroid/hardware/display/VirtualDisplay;

    move-result-object v1

    sput-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sHorizontalPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    .line 542
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 544
    :cond_0
    :goto_0
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sHorizontalPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    return v0

    .line 547
    :cond_1
    invoke-static {}, Lcom/android/server/wm/PreloadStateManagerImpl;->getOrCreatePreloadDisplayId()I

    move-result v0

    return v0
.end method

.method private getRunningProcessInfos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lmiui/process/RunningProcessInfo;",
            ">;"
        }
    .end annotation

    .line 146
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    .line 147
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    sput-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    .line 149
    :cond_0
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getAllRunningProcessInfo()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static killPreloadApp(Ljava/lang/String;I)V
    .locals 3
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "uid"    # I

    .line 595
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    .line 596
    const/4 v1, 0x0

    const-string v2, "advance_kill_preloadApp"

    invoke-virtual {v0, p0, v1, v2}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 597
    invoke-static {p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->onPreloadAppKilled(I)V

    .line 598
    return-void
.end method

.method private onFirstProcAttachForUid(I)Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;
    .locals 6
    .param p1, "uid"    # I

    .line 503
    new-instance v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    invoke-direct {v0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;-><init>(I)V

    .line 504
    .local v0, "info":Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    sget-object v2, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->removeMessages(ILjava/lang/Object;)V

    .line 507
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    invoke-virtual {v1, v3}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 508
    .local v1, "msg":Landroid/os/Message;
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 509
    invoke-static {p1}, Lcom/android/server/am/PreloadAppControllerImpl;->queryKillTimeoutFromUid(I)J

    move-result-wide v2

    .line 510
    .local v2, "killTimeout":J
    sget-object v4, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    invoke-virtual {v4, v1, v2, v3}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 512
    sget-boolean v4, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 513
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "preloadApp onFirstProcAttach uid "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " killTimeout="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PreloadStateManagerImpl"

    invoke-static {v5, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    :cond_0
    return-object v0
.end method

.method private static onPreloadAppKilled(I)V
    .locals 4
    .param p0, "uid"    # I

    .line 555
    invoke-static {}, Lcom/android/server/am/PreloadAppControllerImpl;->getInstance()Lcom/android/server/am/PreloadAppControllerImpl;

    move-result-object v0

    .line 556
    .local v0, "controller":Lcom/android/server/am/PreloadAppControllerImpl;
    if-eqz v0, :cond_0

    .line 557
    invoke-virtual {v0, p0}, Lcom/android/server/am/PreloadAppControllerImpl;->onPreloadAppKilled(I)V

    .line 559
    :cond_0
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    const/4 v1, 0x0

    invoke-static {v1, p0}, Lcom/android/server/wm/PreloadStateManagerImpl;->disableAudio(ZI)V

    .line 561
    sget-boolean v1, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 562
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preloadApp is Killed, remove uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PreloadStateManagerImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    :cond_1
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    sget-object v2, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->removeMessages(ILjava/lang/Object;)V

    .line 565
    return-void
.end method

.method private onPreloadStackHasReparent(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;ZLandroid/app/ActivityOptions;)V
    .locals 4
    .param p1, "targetStack"    # Lcom/android/server/wm/Task;
    .param p2, "intentActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "noAnimation"    # Z
    .param p4, "options"    # Landroid/app/ActivityOptions;

    .line 314
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskSupervisor;->mRecentTasks:Lcom/android/server/wm/RecentTasks;

    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/RecentTasks;->add(Lcom/android/server/wm/Task;)V

    .line 316
    iget-object v0, p2, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p2, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    iput-boolean v1, v0, Lcom/android/server/wm/WindowProcessController;->mIsPreloaded:Z

    .line 321
    :cond_0
    :try_start_0
    const-class v0, Lcom/android/server/wm/Task;

    const-string v2, "removeStopPreloadActivityMsg"

    new-array v3, v1, [Ljava/lang/Class;

    invoke-static {v0, v2, v3}, Lmiui/util/ReflectionUtils;->findMethodBestMatch(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 323
    .local v0, "method":Ljava/lang/reflect/Method;
    new-array v2, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    nop

    .end local v0    # "method":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v2, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 326
    const-string v2, "PreloadStateManagerImpl"

    const-string v3, "preloadApp removeStopPreloadActivityMsg reflect Exception:"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 329
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    if-nez p3, :cond_3

    .line 331
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->RESUMED:Lcom/android/server/wm/ActivityRecord$State;

    invoke-virtual {p2, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 332
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->PAUSED:Lcom/android/server/wm/ActivityRecord$State;

    const-string v2, "preload activity reset state"

    invoke-virtual {p2, v0, v2}, Lcom/android/server/wm/ActivityRecord;->setState(Lcom/android/server/wm/ActivityRecord$State;Ljava/lang/String;)V

    .line 333
    invoke-virtual {p2, v1}, Lcom/android/server/wm/ActivityRecord;->setVisible(Z)V

    .line 335
    :cond_2
    invoke-virtual {p2, p4}, Lcom/android/server/wm/ActivityRecord;->updateOptionsLocked(Landroid/app/ActivityOptions;)V

    .line 336
    iget-object v0, p2, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 337
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayContent;->prepareAppTransition(I)V

    .line 339
    :cond_3
    return-void
.end method

.method private setSchedAffinity(I[I)Z
    .locals 6
    .param p1, "pid"    # I
    .param p2, "schedAffinity"    # [I

    .line 171
    const-class v0, Landroid/os/Process;

    .line 172
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1, p2}, [Ljava/lang/Object;

    move-result-object v1

    .line 171
    const-string/jumbo v2, "setSchedAffinity"

    invoke-static {v0, v2, v1}, Lmiui/util/ReflectionUtils;->tryFindMethodBestMatch(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 173
    .local v0, "method":Ljava/lang/reflect/Method;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 174
    return v1

    .line 178
    :cond_0
    const/4 v2, 0x1

    :try_start_0
    const-class v3, Landroid/os/Process;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    aput-object p2, v4, v2

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    goto :goto_0

    .line 179
    :catch_0
    move-exception v3

    .line 180
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 182
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v3, :cond_2

    .line 183
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .local v3, "sb":Ljava/lang/StringBuilder;
    array-length v4, p2

    :goto_1
    if-ge v1, v4, :cond_1

    aget v5, p2, v1

    .line 185
    .local v5, "i":I
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 184
    .end local v5    # "i":I
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 187
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "preloadApp reset cpuset pid:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " schedAffinity:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "PreloadStateManagerImpl"

    invoke-static {v4, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    return v2
.end method


# virtual methods
.method public adjustDisplayId(Lcom/android/server/wm/Session;I)I
    .locals 2
    .param p1, "session"    # Lcom/android/server/wm/Session;
    .param p2, "displayId"    # I

    .line 427
    iget v0, p1, Lcom/android/server/wm/Session;->mUid:I

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 428
    invoke-static {}, Lcom/android/server/wm/PreloadStateManagerImpl;->getOrCreatePreloadDisplayId()I

    move-result p2

    .line 429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "preloadApp adjustDisplayId displayId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PreloadStateManagerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_0
    return p2
.end method

.method public alreadyPreload(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 194
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public checkEnableAudio()V
    .locals 7

    .line 399
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sEnableAudioLock:Ljava/lang/Object;

    monitor-enter v0

    .line 400
    :try_start_0
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sWaitEnableAudioUids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 401
    monitor-exit v0

    return-void

    .line 403
    :cond_0
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sWaitEnableAudioUids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 404
    .local v2, "uid":I
    sget-object v3, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    invoke-virtual {v3}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 405
    .local v3, "message":Landroid/os/Message;
    iput v2, v3, Landroid/os/Message;->arg1:I

    .line 406
    const/4 v4, 0x4

    iput v4, v3, Landroid/os/Message;->what:I

    .line 407
    sget-object v4, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    const-wide/16 v5, 0x64

    invoke-virtual {v4, v3, v5, v6}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 408
    nop

    .end local v2    # "uid":I
    .end local v3    # "message":Landroid/os/Message;
    goto :goto_0

    .line 409
    :cond_1
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sWaitEnableAudioUids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 410
    monitor-exit v0

    .line 411
    return-void

    .line 410
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 761
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "preload enable:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/android/server/am/PreloadAppControllerImpl;->isEnable()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 762
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "preload debug:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 763
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "preload display:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 765
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 766
    .local v1, "uid":Ljava/lang/Integer;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "already preload uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 767
    .end local v1    # "uid":Ljava/lang/Integer;
    goto :goto_0

    .line 768
    :cond_0
    return-void
.end method

.method public getPreloadDisplayId()I
    .locals 1

    .line 551
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    :goto_0
    return v0
.end method

.method public hidePreloadActivity(Lcom/android/server/wm/ActivityRecord;Z)Z
    .locals 1
    .param p1, "next"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "forceStop"    # Z

    .line 375
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    const/4 v0, 0x1

    return v0

    .line 378
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public init(Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 2
    .param p1, "mService"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 94
    sput-object p1, Lcom/android/server/wm/PreloadStateManagerImpl;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 95
    new-instance v0, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    iget-object v1, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mH:Lcom/android/server/wm/ActivityTaskManagerService$H;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService$H;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    .line 96
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sHorizontalPacakges:Ljava/util/Set;

    const-string v1, "com.tencent.tmgp.sgame"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sHorizontalPacakges:Ljava/util/Set;

    const-string v1, "com.tencent.tmgp.pubgmhd"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public isPreloadDisplay(Landroid/view/Display;)Z
    .locals 1
    .param p1, "display"    # Landroid/view/Display;

    .line 198
    if-nez p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    :goto_0
    return v0
.end method

.method public isPreloadDisplayContent(Lcom/android/server/wm/DisplayContent;)Z
    .locals 1
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 203
    if-nez p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    :goto_0
    return v0
.end method

.method public isPreloadDisplayId(I)Z
    .locals 3
    .param p1, "displayId"    # I

    .line 208
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sHorizontalPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 210
    return v1

    .line 214
    :cond_0
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadDisplay:Landroid/hardware/display/VirtualDisplay;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 216
    :cond_1
    invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    move v1, v2

    :goto_0
    return v1
.end method

.method public needNotRelaunch(ILjava/lang/String;)Z
    .locals 1
    .param p1, "displayId"    # I
    .param p2, "packagename"    # Ljava/lang/String;

    .line 228
    invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    invoke-static {p2}, Lcom/android/server/am/PreloadAppControllerImpl;->inRelaunchBlackList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    const/4 v0, 0x1

    return v0

    .line 232
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public needReparent(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityStarter$Request;Lcom/android/server/wm/Task;)Z
    .locals 4
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "request"    # Lcom/android/server/wm/ActivityStarter$Request;
    .param p4, "inTask"    # Lcom/android/server/wm/Task;

    .line 277
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 278
    return v1

    .line 281
    :cond_0
    const/4 v0, 0x1

    if-nez p2, :cond_3

    .line 282
    const-string/jumbo v2, "startActivityFromRecents"

    iget-object v3, p3, Lcom/android/server/wm/ActivityStarter$Request;->reason:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 283
    if-eqz p4, :cond_1

    .line 284
    iput-boolean v1, p4, Lcom/android/server/wm/Task;->inRecents:Z

    .line 286
    :cond_1
    return v0

    .line 288
    :cond_2
    return v1

    .line 289
    :cond_3
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 290
    return v0

    .line 293
    :cond_4
    return v1
.end method

.method public notReplaceDisplayContent(ILcom/android/server/wm/WindowToken;)Z
    .locals 2
    .param p1, "displayId"    # I
    .param p2, "wToken"    # Lcom/android/server/wm/WindowToken;

    .line 464
    invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    invoke-virtual {p2}, Lcom/android/server/wm/WindowToken;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 466
    invoke-virtual {p2}, Lcom/android/server/wm/WindowToken;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 467
    const-string v0, "PreloadStateManagerImpl"

    const-string v1, "preloadApp need replace display content"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    const/4 v0, 0x0

    return v0

    .line 470
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onAttachApplication(Lcom/android/server/wm/WindowProcessController;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/wm/WindowProcessController;

    .line 475
    iget v0, p1, Lcom/android/server/wm/WindowProcessController;->mUid:I

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 476
    return-void

    .line 478
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v0

    iget v1, p1, Lcom/android/server/wm/WindowProcessController;->mUid:I

    invoke-static {v1}, Lcom/android/server/am/PreloadAppControllerImpl;->querySchedAffinityFromUid(I)[I

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->setSchedAffinity(I[I)Z

    .line 479
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    iget v1, p1, Lcom/android/server/wm/WindowProcessController;->mUid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    .line 480
    .local v0, "info":Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;
    if-eqz v0, :cond_1

    iget v1, v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    if-gez v1, :cond_2

    .line 481
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p1, Lcom/android/server/wm/WindowProcessController;->mIsPreloaded:Z

    .line 482
    iget v1, p1, Lcom/android/server/wm/WindowProcessController;->mUid:I

    invoke-direct {p0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->onFirstProcAttachForUid(I)Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    move-result-object v0

    .line 485
    :cond_2
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->isEnable()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 486
    invoke-direct {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->delayFreezeAppForUid(Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;)V

    .line 489
    :cond_3
    return-void
.end method

.method public onMoveStackToDefaultDisplay(ILcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;ZLandroid/app/ActivityOptions;)V
    .locals 2
    .param p1, "displayId"    # I
    .param p2, "targetStack"    # Lcom/android/server/wm/Task;
    .param p3, "intentActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "noAnimation"    # Z
    .param p5, "options"    # Landroid/app/ActivityOptions;

    .line 299
    invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 302
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "preloadApp move task to DefaultDisplay uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 304
    invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";packageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p3, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 303
    const-string v1, "PreloadStateManagerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_0
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/android/server/wm/PreloadStateManagerImpl;->onPreloadStackHasReparent(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;ZLandroid/app/ActivityOptions;)V

    .line 310
    :cond_1
    return-void
.end method

.method public removePreloadTask(ILcom/android/server/wm/ActivityRecord;)V
    .locals 5
    .param p1, "displayId"    # I
    .param p2, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 415
    invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 417
    .local v0, "task":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_0

    .line 418
    sget-object v1, Lcom/android/server/wm/PreloadStateManagerImpl;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    const/4 v2, 0x1

    const-string v3, "remove preloadApp task"

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v2, v3}, Lcom/android/server/wm/ActivityTaskSupervisor;->removeTask(Lcom/android/server/wm/Task;ZZLjava/lang/String;)V

    .line 422
    .end local v0    # "task":Lcom/android/server/wm/Task;
    :cond_0
    return-void
.end method

.method public reparentPreloadStack(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;ZLandroid/app/ActivityOptions;)V
    .locals 4
    .param p1, "targetStack"    # Lcom/android/server/wm/Task;
    .param p2, "intentActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "noAnimation"    # Z
    .param p4, "options"    # Landroid/app/ActivityOptions;

    .line 437
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 438
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 439
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 440
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "preloadApp reparentPreloadStack "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 441
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";packageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 440
    const-string v1, "PreloadStateManagerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :cond_0
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/RootWindowContainer;->moveRootTaskToDisplay(IIZ)V

    .line 446
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/PreloadStateManagerImpl;->onPreloadStackHasReparent(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;ZLandroid/app/ActivityOptions;)V

    .line 448
    :cond_1
    return-void
.end method

.method public replaceIdForPreloadApp(I)I
    .locals 1
    .param p1, "displayId"    # I

    .line 220
    invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x0

    return v0

    .line 223
    :cond_0
    return p1
.end method

.method public repositionDisplayContent(ILjava/lang/Object;)I
    .locals 2
    .param p1, "position"    # I
    .param p2, "displayContent"    # Ljava/lang/Object;

    .line 452
    instance-of v0, p2, Lcom/android/server/wm/DisplayContent;

    if-eqz v0, :cond_0

    .line 453
    move-object v0, p2

    check-cast v0, Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    const-string v0, "PreloadStateManagerImpl"

    const-string v1, "preloadApp need reposition display content"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    const/4 v0, 0x0

    return v0

    .line 458
    :cond_0
    return p1
.end method

.method public resetInTask(Lcom/android/server/wm/Task;)Lcom/android/server/wm/Task;
    .locals 1
    .param p1, "inTask"    # Lcom/android/server/wm/Task;

    .line 164
    sget-boolean v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadAppStarted:Z

    if-eqz v0, :cond_0

    .line 165
    const/4 v0, 0x0

    return-object v0

    .line 167
    :cond_0
    return-object p1
.end method

.method public resumeTopActivityInjector(Lcom/android/server/wm/ActivityRecord;Landroid/os/Handler;)V
    .locals 4
    .param p1, "next"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 383
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 384
    return-void

    .line 387
    :cond_0
    const/16 v0, 0xc7

    invoke-virtual {p2, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 388
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    invoke-static {v1}, Lcom/android/server/am/PreloadAppControllerImpl;->queryStopTimeoutFromUid(I)J

    move-result-wide v1

    .line 389
    .local v1, "stopTimeout":J
    invoke-virtual {p2, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 390
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 391
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preloadApp uid="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " pid="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " will stop after:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "PreloadStateManagerImpl"

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    .end local v1    # "stopTimeout":J
    :cond_1
    return-void
.end method

.method public stackOnParentChanged(Lcom/android/server/wm/DisplayContent;Z)Z
    .locals 1
    .param p1, "display"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "forceStop"    # Z

    .line 367
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 370
    :cond_0
    return p2

    .line 368
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public startActivityInjector(Lcom/android/server/wm/ActivityStarter$Request;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/app/ActivityOptions;Lcom/android/server/wm/Task;)Landroid/app/ActivityOptions;
    .locals 4
    .param p1, "request"    # Lcom/android/server/wm/ActivityStarter$Request;
    .param p2, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "options"    # Landroid/app/ActivityOptions;
    .param p5, "inTask"    # Lcom/android/server/wm/Task;

    .line 238
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadAppStarted:Z

    .line 239
    iget v1, p1, Lcom/android/server/wm/ActivityStarter$Request;->realCallingUid:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 240
    iget v1, p1, Lcom/android/server/wm/ActivityStarter$Request;->realCallingUid:I

    goto :goto_0

    .line 241
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    :goto_0
    nop

    .line 243
    .local v1, "realCallingUid":I
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz p3, :cond_2

    .line 244
    invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 245
    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 246
    :cond_2
    sget-boolean v2, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v2, :cond_3

    .line 247
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "set preloadApp launch PreloadDisplay r="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 248
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " realCallingUid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 247
    const-string v3, "PreloadStateManagerImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_3
    if-nez p4, :cond_4

    .line 252
    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object p4

    .line 254
    :cond_4
    iget-object v2, p2, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/server/wm/PreloadStateManagerImpl;->getOrCreatePreloadDisplayId(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p4, v2}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;

    .line 257
    :cond_5
    invoke-virtual {p0, p2, p3, p1, p5}, Lcom/android/server/wm/PreloadStateManagerImpl;->needReparent(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityStarter$Request;Lcom/android/server/wm/Task;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 259
    if-nez p4, :cond_6

    .line 260
    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object p4

    .line 262
    :cond_6
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadAppStarted:Z

    .line 263
    invoke-virtual {p0, p2}, Lcom/android/server/wm/PreloadStateManagerImpl;->switchAppNormalState(Lcom/android/server/wm/ActivityRecord;)V

    .line 264
    invoke-virtual {p4, v0}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;

    goto :goto_1

    .line 265
    :cond_7
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v0

    const/16 v2, 0x3e8

    if-le v0, v2, :cond_8

    if-eqz p4, :cond_8

    .line 266
    invoke-virtual {p4}, Landroid/app/ActivityOptions;->getLaunchDisplayId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 267
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 269
    invoke-virtual {p0, p2}, Lcom/android/server/wm/PreloadStateManagerImpl;->switchAppPreloadState(Lcom/android/server/wm/ActivityRecord;)V

    .line 272
    :cond_8
    :goto_1
    return-object p4
.end method

.method public stopPreloadActivity(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/Task;)Z
    .locals 3
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "prev"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "task"    # Lcom/android/server/wm/Task;

    .line 343
    const/4 v0, 0x0

    if-nez p1, :cond_1

    .line 344
    invoke-virtual {p3}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 345
    .local v1, "top":Lcom/android/server/wm/ActivityRecord;
    if-nez v1, :cond_0

    .line 346
    return v0

    .line 348
    :cond_0
    move-object p1, v1

    .line 350
    .end local v1    # "top":Lcom/android/server/wm/ActivityRecord;
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "permission.ui.GrantPermissionsActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    .line 351
    move-object p1, p2

    .line 353
    :cond_2
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 354
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 355
    const-string v0, "PreloadStateManagerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preloadApp mReadyPauseActivity="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    :cond_3
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;

    move-result-object v1

    monitor-enter v1

    .line 358
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->makeInvisible()V

    .line 359
    monitor-exit v1

    .line 360
    const/4 v0, 0x1

    return v0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 362
    :cond_4
    return v0
.end method

.method public switchAppNormalState(Lcom/android/server/wm/ActivityRecord;)V
    .locals 7
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 110
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 111
    const-string v0, "PreloadStateManagerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "move preloadApp to defaultDisplay uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "callingUid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 112
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/PreloadStateManagerImpl;->getRunningProcessInfos()Ljava/util/List;

    move-result-object v0

    .line 116
    .local v0, "runningProcessInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
    const/4 v1, 0x0

    .line 117
    .local v1, "procIsRunninng":Z
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/process/RunningProcessInfo;

    .line 118
    .local v3, "info":Lmiui/process/RunningProcessInfo;
    if-eqz v3, :cond_1

    iget v4, v3, Lmiui/process/RunningProcessInfo;->mUid:I

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 119
    const/4 v1, 0x1

    .line 120
    iget v4, v3, Lmiui/process/RunningProcessInfo;->mPid:I

    sget-object v5, Lcom/android/server/am/PreloadAppControllerImpl;->sDefaultSchedAffinity:[I

    invoke-direct {p0, v4, v5}, Lcom/android/server/wm/PreloadStateManagerImpl;->setSchedAffinity(I[I)Z

    .line 122
    .end local v3    # "info":Lmiui/process/RunningProcessInfo;
    :cond_1
    goto :goto_0

    .line 124
    :cond_2
    invoke-static {}, Lcom/android/server/am/PreloadAppControllerImpl;->getInstance()Lcom/android/server/am/PreloadAppControllerImpl;

    move-result-object v2

    .line 125
    .local v2, "preloadController":Lcom/android/server/am/PreloadAppControllerImpl;
    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    .line 126
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v3

    .line 127
    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_3

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string v4, ""

    :goto_1
    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v5, :cond_4

    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    invoke-virtual {v5}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v5

    goto :goto_2

    :cond_4
    const/4 v5, -0x1

    .line 126
    :goto_2
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/am/PreloadAppControllerImpl;->onPreloadAppStarted(ILjava/lang/String;I)V

    .line 130
    :cond_5
    sget-object v3, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    sget-object v4, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    .line 131
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 130
    const/4 v5, 0x2

    invoke-virtual {v3, v5, v4}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->removeMessages(ILjava/lang/Object;)V

    .line 132
    sget-object v3, Lcom/android/server/wm/PreloadStateManagerImpl;->sHandler:Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;

    sget-object v4, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    .line 133
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 132
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->removeMessages(ILjava/lang/Object;)V

    .line 134
    sget-object v3, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->isEnable()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 136
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v4

    filled-new-array {v4}, [I

    move-result-object v4

    sget v5, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_PRELOAD:I

    const-string/jumbo v6, "thaw preloadApp"

    invoke-virtual {v3, v4, v5, v6}, Lcom/miui/server/greeze/GreezeManagerInternal;->thawUids([IILjava/lang/String;)Ljava/util/List;

    .line 140
    :cond_6
    sget-object v3, Lcom/android/server/wm/PreloadStateManagerImpl;->sEnableAudioLock:Ljava/lang/Object;

    monitor-enter v3

    .line 141
    :try_start_0
    sget-object v4, Lcom/android/server/wm/PreloadStateManagerImpl;->sWaitEnableAudioUids:Ljava/util/List;

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    monitor-exit v3

    .line 143
    return-void

    .line 142
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public switchAppPreloadState(Lcom/android/server/wm/ActivityRecord;)V
    .locals 3
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 101
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "First start preloadApp to VirtualDisplay activity uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " callingUid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 103
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 102
    const-string v1, "PreloadStateManagerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    sget-object v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadUidInfos:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    invoke-direct {v2}, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->disableAudio(ZI)V

    .line 107
    return-void
.end method
