class com.android.server.wm.MiuiFreeformTrackManager$16 implements java.lang.Runnable {
	 /* .source "MiuiFreeformTrackManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiFreeformTrackManager;->trackMiniWindowPinedQuitEvent(Ljava/lang/String;Ljava/lang/String;F)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiFreeformTrackManager this$0; //synthetic
final java.lang.String val$applicationName; //synthetic
final java.lang.String val$packageName; //synthetic
final Float val$pinedDuration; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiFreeformTrackManager$16 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiFreeformTrackManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 580 */
this.this$0 = p1;
this.val$packageName = p2;
this.val$applicationName = p3;
/* iput p4, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->val$pinedDuration:F */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 582 */
v0 = this.this$0;
v0 = this.mFreeFormTrackLock;
/* monitor-enter v0 */
/* .line 584 */
try { // :try_start_0
v1 = this.this$0;
com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmITrackBinder ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 585 */
	 /* new-instance v1, Lorg/json/JSONObject; */
	 /* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
	 /* .line 586 */
	 /* .local v1, "jsonData":Lorg/json/JSONObject; */
	 /* const-string/jumbo v2, "tip" */
	 final String v3 = "621.2.1.1.21752"; // const-string v3, "621.2.1.1.21752"
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 587 */
	 v2 = this.this$0;
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$mputCommomParam ( v2,v1 );
	 /* .line 588 */
	 final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
	 final String v3 = "quit_hidden_window"; // const-string v3, "quit_hidden_window"
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 589 */
	 final String v2 = "app_package_name"; // const-string v2, "app_package_name"
	 v3 = this.val$packageName;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 590 */
	 final String v2 = "app_display_name"; // const-string v2, "app_display_name"
	 v3 = this.val$applicationName;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 591 */
	 /* const-string/jumbo v2, "use_duration" */
	 /* iget v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->val$pinedDuration:F */
	 /* float-to-double v3, v3 */
	 (( org.json.JSONObject ) v1 ).put ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
	 /* .line 592 */
	 v2 = this.this$0;
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmITrackBinder ( v2 );
	 final String v3 = "31000000538"; // const-string v3, "31000000538"
	 final String v4 = "android"; // const-string v4, "android"
	 /* .line 593 */
	 (( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
	 /* .line 592 */
	 int v6 = 0; // const/4 v6, 0x0
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 597 */
} // .end local v1 # "jsonData":Lorg/json/JSONObject;
} // :cond_0
/* .line 598 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 595 */
/* :catch_0 */
/* move-exception v1 */
/* .line 596 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 598 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit v0 */
/* .line 599 */
return;
/* .line 598 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
