.class Lcom/android/server/wm/AccountHelper$2;
.super Landroid/app/IMiuiActivityObserver$Stub;
.source "AccountHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/AccountHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/AccountHelper;


# direct methods
.method constructor <init>(Lcom/android/server/wm/AccountHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/AccountHelper;

    .line 216
    iput-object p1, p0, Lcom/android/server/wm/AccountHelper$2;->this$0:Lcom/android/server/wm/AccountHelper;

    invoke-direct {p0}, Landroid/app/IMiuiActivityObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public activityDestroyed(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 256
    return-void
.end method

.method public activityIdle(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 219
    return-void
.end method

.method public activityPaused(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 248
    return-void
.end method

.method public activityResumed(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 223
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isControllerAMonkey()Z

    move-result v0

    const-string v1, "MiuiPermision"

    if-eqz v0, :cond_1

    .line 224
    invoke-static {}, Lcom/android/server/wm/AccountHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    const-string/jumbo v0, "skip account check !"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_0
    return-void

    .line 229
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "className":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 231
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/wm/AccountHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 232
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resume packageName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mListenMode :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/AccountHelper$2;->this$0:Lcom/android/server/wm/AccountHelper;

    invoke-static {v4}, Lcom/android/server/wm/AccountHelper;->-$$Nest$fgetmListenMode(Lcom/android/server/wm/AccountHelper;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_2
    invoke-static {}, Lcom/android/server/wm/AccountHelper;->-$$Nest$sfgetmContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v3, "com.xiaomi"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 236
    .local v1, "accounts":[Landroid/accounts/Account;
    invoke-static {}, Lcom/android/server/wm/AccountHelper;->-$$Nest$sfgetsAccessPackage()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {}, Lcom/android/server/wm/AccountHelper;->-$$Nest$sfgetsAccessActivities()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 237
    iget-object v3, p0, Lcom/android/server/wm/AccountHelper$2;->this$0:Lcom/android/server/wm/AccountHelper;

    invoke-static {v3}, Lcom/android/server/wm/AccountHelper;->-$$Nest$fgetmListenMode(Lcom/android/server/wm/AccountHelper;)I

    move-result v3

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_3

    .line 239
    invoke-static {}, Lcom/android/server/wm/AccountHelper;->-$$Nest$sfgetmCallBack()Lcom/android/server/wm/AccountHelper$AccountCallback;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/server/wm/AccountHelper$AccountCallback;->onWifiSettingFinish()V

    goto :goto_0

    .line 240
    :cond_3
    iget-object v3, p0, Lcom/android/server/wm/AccountHelper$2;->this$0:Lcom/android/server/wm/AccountHelper;

    invoke-static {v3}, Lcom/android/server/wm/AccountHelper;->-$$Nest$fgetmListenMode(Lcom/android/server/wm/AccountHelper;)I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_4

    .line 241
    iget-object v3, p0, Lcom/android/server/wm/AccountHelper$2;->this$0:Lcom/android/server/wm/AccountHelper;

    invoke-static {}, Lcom/android/server/wm/AccountHelper;->-$$Nest$sfgetmContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/server/wm/AccountHelper;->addAccount(Landroid/content/Context;)V

    .line 244
    :cond_4
    :goto_0
    return-void
.end method

.method public activityStopped(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 252
    return-void
.end method

.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 260
    return-object p0
.end method
