.class public Lcom/android/server/wm/MiuiDragAndDropStubImpl;
.super Ljava/lang/Object;
.source "MiuiDragAndDropStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiDragAndDropStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiDragAndDropStubImpl"


# instance fields
.field private mMFDSInternal:Lcom/miui/app/MiuiFreeDragServiceInternal;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-class v0, Lcom/miui/app/MiuiFreeDragServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/MiuiFreeDragServiceInternal;

    iput-object v0, p0, Lcom/android/server/wm/MiuiDragAndDropStubImpl;->mMFDSInternal:Lcom/miui/app/MiuiFreeDragServiceInternal;

    .line 28
    return-void
.end method


# virtual methods
.method public checkMiuiDragAndDropMetaData(Lcom/android/server/wm/ActivityRecord;)V
    .locals 5
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 31
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 32
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 31
    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 34
    .local v0, "infoWithMetaData":Landroid/content/pm/ActivityInfo;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget-object v1, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    .line 35
    .local v1, "metaData":Landroid/os/Bundle;
    :goto_0
    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 36
    const-string v3, "miui.hasDragAndDropFeature"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    nop

    :goto_1
    iput-boolean v2, p1, Lcom/android/server/wm/ActivityRecord;->mHasDragAndDropFeature:Z

    .line 37
    iget-boolean v2, p1, Lcom/android/server/wm/ActivityRecord;->mHasDragAndDropFeature:Z

    if-eqz v2, :cond_2

    .line 38
    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 39
    iget-object v2, p0, Lcom/android/server/wm/MiuiDragAndDropStubImpl;->mMFDSInternal:Lcom/miui/app/MiuiFreeDragServiceInternal;

    if-eqz v2, :cond_2

    .line 40
    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    .line 41
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    .line 40
    invoke-interface {v2, v3, v4}, Lcom/miui/app/MiuiFreeDragServiceInternal;->notifyHasMiuiDragAndDropMetaData(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_2
    return-void
.end method

.method public getDragNotAllowPackages(Lcom/android/server/wm/WindowState;)Z
    .locals 4
    .param p1, "callingWin"    # Lcom/android/server/wm/WindowState;

    .line 53
    invoke-static {}, Lcom/android/server/MiuiCommonCloudServiceStub;->getInstance()Lcom/android/server/MiuiCommonCloudServiceStub;

    move-result-object v0

    const-string v1, "drag_not_allow_packages"

    invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudServiceStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;

    .line 54
    .local v0, "packages":Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;->getPackages()Landroid/util/ArraySet;

    move-result-object v1

    .line 55
    .local v1, "dragNotAllowPackages":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not allow dragging, package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 58
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 57
    const-string v3, "MiuiDragAndDropStubImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const/4 v2, 0x1

    return v2

    .line 61
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public isMiuiFreeForm(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowManagerService;)Z
    .locals 3
    .param p1, "callingWin"    # Lcom/android/server/wm/WindowState;
    .param p2, "mWmService"    # Lcom/android/server/wm/WindowManagerService;

    .line 65
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 66
    .local v0, "ar":Lcom/android/server/wm/ActivityRecord;
    if-nez v0, :cond_0

    .line 67
    const/4 v1, 0x0

    return v1

    .line 69
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v1

    .line 70
    .local v1, "arId":I
    iget-object v2, p2, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    invoke-interface {v2, v1}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->isInMiniFreeFormMode(I)Z

    move-result v2

    .line 71
    .local v2, "result":Z
    return v2
.end method

.method public notifyDragDropResultToOneTrack(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;ZLandroid/content/ClipData;)V
    .locals 1
    .param p1, "dragWindow"    # Lcom/android/server/wm/WindowState;
    .param p2, "dropWindow"    # Lcom/android/server/wm/WindowState;
    .param p3, "result"    # Z
    .param p4, "clipData"    # Landroid/content/ClipData;

    .line 48
    invoke-static {}, Lcom/android/server/wm/OneTrackDragDropHelper;->getInstance()Lcom/android/server/wm/OneTrackDragDropHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/OneTrackDragDropHelper;->notifyDragDropResult(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;ZLandroid/content/ClipData;)V

    .line 50
    return-void
.end method
