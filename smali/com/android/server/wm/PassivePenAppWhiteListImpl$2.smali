.class Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "PassivePenAppWhiteListImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/PassivePenAppWhiteListImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/PassivePenAppWhiteListImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    .line 165
    iput-object p1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 5
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 168
    iget-object v0, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    .line 169
    .local v0, "forePkg":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mForegroundPackageName= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PassivePenAppWhiteListImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    iget-object v1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListApp(Ljava/lang/String;)Z

    move-result v1

    .line 171
    .local v1, "whiteListApp":Z
    iget-object v3, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-virtual {v3, v0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->blackAppAndWhiteAppChanged(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 172
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " whiteListApp= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    iget-object v3, v2, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z

    move-result v3

    iput-boolean v3, v2, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->splitMode:Z

    .line 174
    iget-object v2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    iget-boolean v2, v2, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->splitMode:Z

    if-nez v2, :cond_0

    .line 175
    iget-object v2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->dispatchTopAppWhiteState(I)Z

    .line 178
    :cond_0
    return-void
.end method
