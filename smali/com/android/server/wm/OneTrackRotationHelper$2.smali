.class Lcom/android/server/wm/OneTrackRotationHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "OneTrackRotationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/OneTrackRotationHelper;->initAllListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/OneTrackRotationHelper;


# direct methods
.method constructor <init>(Lcom/android/server/wm/OneTrackRotationHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/OneTrackRotationHelper;

    .line 292
    iput-object p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$2;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 295
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$2;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetmIsInit(Lcom/android/server/wm/OneTrackRotationHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    return-void

    .line 298
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x3

    sparse-switch v1, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v1, "android.intent.action.REBOOT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_1

    :sswitch_1
    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_2
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_3
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 307
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$2;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    iget-object v0, v0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 308
    .local v0, "message2":Landroid/os/Message;
    new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;

    invoke-direct {v1, p2}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 309
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$2;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    iget-object v1, v1, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto :goto_2

    .line 301
    .end local v0    # "message2":Landroid/os/Message;
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$2;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    iget-object v0, v0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 302
    .local v0, "message1":Landroid/os/Message;
    new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;

    invoke-direct {v1, p2}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 303
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 304
    nop

    .line 312
    .end local v0    # "message1":Landroid/os/Message;
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ed8ea7f -> :sswitch_3
        0x311a1d6c -> :sswitch_2
        0x741706da -> :sswitch_1
        0x79950caa -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
