.class Lcom/android/server/wm/TalkbackWatermark$2;
.super Landroid/content/BroadcastReceiver;
.source "TalkbackWatermark.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/TalkbackWatermark;->setupBroadcast()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/TalkbackWatermark;


# direct methods
.method constructor <init>(Lcom/android/server/wm/TalkbackWatermark;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/TalkbackWatermark;

    .line 119
    iput-object p1, p0, Lcom/android/server/wm/TalkbackWatermark$2;->this$0:Lcom/android/server/wm/TalkbackWatermark;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 122
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    goto :goto_0

    :pswitch_0
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_1

    goto :goto_2

    .line 124
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark$2;->this$0:Lcom/android/server/wm/TalkbackWatermark;

    invoke-static {v0}, Lcom/android/server/wm/TalkbackWatermark;->-$$Nest$mrefresh(Lcom/android/server/wm/TalkbackWatermark;)V

    .line 128
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch -0x122164c
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
