.class Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;
.super Landroid/view/animation/Animation;
.source "ScreenRotationAnimationImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/ScreenRotationAnimationImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScreenScaleAnimation"
.end annotation


# instance fields
.field private isGetCurScale:Z

.field private mFinalScale:F

.field private mFirstPhaseDuration:I

.field private mLongEaseInterpolator:Landroid/view/animation/Interpolator;

.field private mMiddleScale:F

.field private mNextPhaseScale:F

.field private mPivotX:F

.field private mPivotXType:I

.field private mPivotXValue:F

.field private mPivotY:F

.field private mPivotYType:I

.field private mPivotYValue:F

.field private mScale:F

.field private mScaleBreakOffset:F

.field private mScaleDelayTime:I

.field private mSecPhaseDuration:I

.field private mShortEaseInterpolator:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(FFIFIF)V
    .locals 1
    .param p1, "middleScale"    # F
    .param p2, "finalScale"    # F
    .param p3, "pivotXType"    # I
    .param p4, "pivotXValue"    # F
    .param p5, "pivotYType"    # I
    .param p6, "pivotYValue"    # F

    .line 435
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 417
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScale:F

    .line 418
    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mNextPhaseScale:F

    .line 419
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->isGetCurScale:Z

    .line 420
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotXType:I

    .line 421
    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotYType:I

    .line 422
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotXValue:F

    .line 423
    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotYValue:F

    .line 428
    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScaleBreakOffset:F

    .line 436
    iput p1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mMiddleScale:F

    .line 437
    iput p2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mFinalScale:F

    .line 438
    iput p4, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotXValue:F

    .line 439
    iput p3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotXType:I

    .line 440
    iput p6, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotYValue:F

    .line 441
    iput p5, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotYType:I

    .line 442
    invoke-direct {p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->initializePivotPoint()V

    .line 443
    return-void
.end method

.method private initializePivotPoint()V
    .locals 1

    .line 446
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotXType:I

    if-nez v0, :cond_0

    .line 447
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotXValue:F

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotX:F

    .line 449
    :cond_0
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotYType:I

    if-nez v0, :cond_1

    .line 450
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotYValue:F

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotY:F

    .line 452
    :cond_1
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .line 522
    invoke-virtual {p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->getScaleFactor()F

    move-result v0

    .line 523
    .local v0, "scale":F
    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotX:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotY:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 524
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScale:F

    invoke-virtual {v1, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    goto :goto_0

    .line 526
    :cond_0
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScale:F

    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotX:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotY:F

    mul-float/2addr v4, v0

    invoke-virtual {v1, v2, v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 528
    :goto_0
    return-void
.end method

.method public getTransformation(JLandroid/view/animation/Transformation;)Z
    .locals 18
    .param p1, "currentTime"    # J
    .param p3, "outTransformation"    # Landroid/view/animation/Transformation;

    .line 478
    move-object/from16 v0, p0

    const/4 v1, 0x0

    .line 480
    .local v1, "more":Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->getStartOffset()J

    move-result-wide v2

    .line 481
    .local v2, "startOffset":J
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->getDuration()J

    move-result-wide v4

    .line 482
    .local v4, "duration":J
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->getStartTime()J

    move-result-wide v6

    .line 484
    .local v6, "startTime":J
    const/4 v8, 0x0

    .line 485
    .local v8, "interpolatedTime":F
    const-wide/16 v9, 0x0

    cmp-long v9, v4, v9

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    if-eqz v9, :cond_0

    .line 486
    add-long v12, v6, v2

    sub-long v12, p1, v12

    long-to-float v9, v12

    long-to-float v12, v4

    div-float/2addr v9, v12

    .local v9, "normalizedTime":F
    goto :goto_0

    .line 489
    .end local v9    # "normalizedTime":F
    :cond_0
    cmp-long v9, p1, v6

    if-gez v9, :cond_1

    move v9, v10

    goto :goto_0

    :cond_1
    move v9, v11

    .line 492
    .restart local v9    # "normalizedTime":F
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->getFillAfter()Z

    move-result v12

    .line 493
    .local v12, "fillAfter":Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->getFillBefore()Z

    move-result v13

    .line 494
    .local v13, "fillBefore":Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->isFillEnabled()Z

    move-result v14

    .line 496
    .local v14, "fillEnabled":Z
    if-nez v14, :cond_2

    invoke-static {v9, v11}, Ljava/lang/Math;->min(FF)F

    move-result v15

    invoke-static {v15, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 498
    :cond_2
    cmpl-float v15, v9, v10

    if-gez v15, :cond_3

    if-eqz v13, :cond_4

    :cond_3
    cmpg-float v15, v9, v11

    if-lez v15, :cond_5

    if-eqz v12, :cond_4

    goto :goto_1

    .line 516
    :cond_4
    move/from16 v17, v1

    goto :goto_2

    .line 499
    :cond_5
    :goto_1
    if-eqz v14, :cond_6

    .line 500
    invoke-static {v9, v11}, Ljava/lang/Math;->min(FF)F

    move-result v15

    invoke-static {v15, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 501
    :cond_6
    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScaleBreakOffset:F

    cmpg-float v10, v9, v10

    if-gtz v10, :cond_7

    .line 502
    iget-object v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mShortEaseInterpolator:Landroid/view/animation/Interpolator;

    long-to-int v11, v4

    int-to-float v11, v11

    mul-float/2addr v11, v9

    iget v15, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mFirstPhaseDuration:I

    int-to-float v15, v15

    div-float/2addr v11, v15

    .line 503
    invoke-interface {v10, v11}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v8

    .line 504
    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mFinalScale:F

    iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mMiddleScale:F

    sub-float/2addr v11, v10

    mul-float/2addr v11, v8

    add-float/2addr v10, v11

    iput v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScale:F

    move/from16 v17, v1

    goto :goto_2

    .line 506
    :cond_7
    iget-boolean v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->isGetCurScale:Z

    if-eqz v10, :cond_8

    .line 507
    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScale:F

    iput v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mNextPhaseScale:F

    .line 508
    const/4 v10, 0x0

    iput-boolean v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->isGetCurScale:Z

    .line 510
    :cond_8
    iget-object v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mLongEaseInterpolator:Landroid/view/animation/Interpolator;

    long-to-int v15, v4

    int-to-float v15, v15

    mul-float/2addr v15, v9

    iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mSecPhaseDuration:I

    move/from16 v17, v1

    .end local v1    # "more":Z
    .local v17, "more":Z
    int-to-float v1, v11

    div-float/2addr v15, v1

    iget v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScaleDelayTime:I

    int-to-float v1, v1

    const/high16 v16, 0x3f800000    # 1.0f

    mul-float v1, v1, v16

    int-to-float v11, v11

    div-float/2addr v1, v11

    sub-float/2addr v15, v1

    invoke-interface {v10, v15}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v8

    .line 513
    iget v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mNextPhaseScale:F

    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mFinalScale:F

    sub-float/2addr v10, v1

    mul-float/2addr v10, v8

    add-float/2addr v1, v10

    iput v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScale:F

    .line 516
    :goto_2
    invoke-super/range {p0 .. p3}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    move-result v1

    .line 517
    .end local v17    # "more":Z
    .restart local v1    # "more":Z
    return v1
.end method

.method public initialize(IIII)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "parentWidth"    # I
    .param p4, "parentHeight"    # I

    .line 532
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 533
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotXType:I

    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotXValue:F

    invoke-virtual {p0, v0, v1, p1, p3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->resolveSize(IFII)F

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotX:F

    .line 534
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotYType:I

    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotYValue:F

    invoke-virtual {p0, v0, v1, p2, p4}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->resolveSize(IFII)F

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mPivotY:F

    .line 535
    return-void
.end method

.method public setAnimationInterpolator(Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V
    .locals 0
    .param p1, "longEaseInterpolator"    # Landroid/view/animation/Interpolator;
    .param p2, "shortEaseInterpolator"    # Landroid/view/animation/Interpolator;

    .line 464
    iput-object p1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mLongEaseInterpolator:Landroid/view/animation/Interpolator;

    .line 465
    iput-object p2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mShortEaseInterpolator:Landroid/view/animation/Interpolator;

    .line 466
    return-void
.end method

.method public setFirstPhaseDuration(I)V
    .locals 0
    .param p1, "firstPhaseDuration"    # I

    .line 455
    iput p1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mFirstPhaseDuration:I

    .line 456
    return-void
.end method

.method public setScaleBreakOffset(F)V
    .locals 0
    .param p1, "scaleBreakOffset"    # F

    .line 469
    iput p1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScaleBreakOffset:F

    .line 470
    return-void
.end method

.method public setScaleDelayTime(I)V
    .locals 0
    .param p1, "scaleDelayTime"    # I

    .line 473
    iput p1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mScaleDelayTime:I

    .line 474
    return-void
.end method

.method public setSecPhaseDuration(I)V
    .locals 0
    .param p1, "secPhaseDuration"    # I

    .line 459
    iput p1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->mSecPhaseDuration:I

    .line 460
    return-void
.end method
