public class com.android.server.wm.OneTrackVulkanHelper {
	 /* .source "OneTrackVulkanHelper.java" */
	 /* # static fields */
	 public static java.lang.String APP_CRASH_INFO;
	 private static final java.lang.String APP_ID;
	 private static final java.lang.String EVENT_NAME;
	 private static final Integer FLAG_NON_ANONYMOUS;
	 private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
	 private static final java.lang.String ONETRACK_ACTION;
	 private static final java.lang.String ONETRACK_PACKAGE_NAME;
	 private static final java.lang.String PACKAGE_NAME;
	 private static final java.lang.String TAG;
	 static android.content.Context mContext;
	 private static com.android.server.wm.OneTrackVulkanHelper sInstance;
	 /* # direct methods */
	 static com.android.server.wm.OneTrackVulkanHelper ( ) {
		 /* .locals 1 */
		 /* .line 13 */
		 /* const-class v0, Lcom/android/server/wm/OneTrackVulkanHelper; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.server.wm.OneTrackVulkanHelper ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static synchronized com.android.server.wm.OneTrackVulkanHelper getInstance ( ) {
		 /* .locals 3 */
		 /* const-class v0, Lcom/android/server/wm/OneTrackVulkanHelper; */
		 /* monitor-enter v0 */
		 /* .line 29 */
		 try { // :try_start_0
			 v1 = com.android.server.wm.OneTrackVulkanHelper.sInstance;
			 /* if-nez v1, :cond_0 */
			 /* .line 30 */
			 /* new-instance v1, Lcom/android/server/wm/OneTrackVulkanHelper; */
			 /* invoke-direct {v1}, Lcom/android/server/wm/OneTrackVulkanHelper;-><init>()V */
			 /* .line 32 */
		 } // :cond_0
		 android.app.ActivityThread .currentActivityThread ( );
		 (( android.app.ActivityThread ) v1 ).getApplication ( ); // invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;
		 /* .line 33 */
		 /* if-nez v1, :cond_1 */
		 /* .line 34 */
		 v1 = com.android.server.wm.OneTrackVulkanHelper.TAG;
		 final String v2 = "init OneTrackVulkanHelper mContext = null"; // const-string v2, "init OneTrackVulkanHelper mContext = null"
		 android.util.Slog .e ( v1,v2 );
		 /* .line 36 */
	 } // :cond_1
	 v1 = com.android.server.wm.OneTrackVulkanHelper.sInstance;
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* monitor-exit v0 */
	 /* .line 28 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* throw v1 */
} // .end method
/* # virtual methods */
public void getCrashInfo ( java.lang.String p0 ) {
	 /* .locals 0 */
	 /* .param p1, "trace" # Ljava/lang/String; */
	 /* .line 40 */
	 /* .line 41 */
	 return;
} // .end method
public void reportOneTrack ( java.lang.String p0 ) {
	 /* .locals 4 */
	 /* .param p1, "packageName" # Ljava/lang/String; */
	 /* .line 45 */
	 try { // :try_start_0
		 /* new-instance v0, Landroid/content/Intent; */
		 final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
		 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 /* .line 46 */
		 /* .local v0, "intent":Landroid/content/Intent; */
		 final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
		 (( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 47 */
		 final String v1 = "APP_ID"; // const-string v1, "APP_ID"
		 final String v2 = "31000401696"; // const-string v2, "31000401696"
		 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 48 */
		 final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
		 final String v2 = "android"; // const-string v2, "android"
		 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 49 */
		 final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
		 final String v2 = "app_use_vulkan"; // const-string v2, "app_use_vulkan"
		 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 50 */
		 /* new-instance v1, Landroid/os/Bundle; */
		 /* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
		 /* .line 52 */
		 /* .local v1, "params":Landroid/os/Bundle; */
		 (( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
		 /* .line 53 */
		 /* const-string/jumbo v2, "vulkan_package_name" */
		 (( android.content.Intent ) v0 ).putExtra ( v2, p1 ); // invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 54 */
		 /* const-string/jumbo v2, "vulkan_crash_info" */
		 v3 = com.android.server.wm.OneTrackVulkanHelper.APP_CRASH_INFO;
		 (( android.content.Intent ) v0 ).putExtra ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 56 */
		 /* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
		 /* if-nez v2, :cond_0 */
		 /* .line 57 */
		 int v2 = 3; // const/4 v2, 0x3
		 (( android.content.Intent ) v0 ).setFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
		 /* .line 60 */
	 } // :cond_0
	 v2 = com.android.server.wm.OneTrackVulkanHelper.mContext;
	 v3 = android.os.UserHandle.CURRENT;
	 (( android.content.Context ) v2 ).startServiceAsUser ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
	 /* .line 61 */
	 v2 = com.android.server.wm.OneTrackVulkanHelper.TAG;
	 final String v3 = "reportOneTrack"; // const-string v3, "reportOneTrack"
	 android.util.Slog .i ( v2,v3 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 64 */
	 /* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v1 # "params":Landroid/os/Bundle;
/* .line 62 */
/* :catch_0 */
/* move-exception v0 */
/* .line 63 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.wm.OneTrackVulkanHelper.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Upload use VulkanApp exception! "; // const-string v3, "Upload use VulkanApp exception! "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2,v0 );
/* .line 65 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
