class com.android.server.wm.MiuiWindowMonitorImpl$ProcessKey {
	 /* .source "MiuiWindowMonitorImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiWindowMonitorImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ProcessKey" */
} // .end annotation
/* # instance fields */
java.lang.String mPackageName;
Integer mPid;
/* # direct methods */
public com.android.server.wm.MiuiWindowMonitorImpl$ProcessKey ( ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 668 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 669 */
/* iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPid:I */
/* .line 670 */
this.mPackageName = p2;
/* .line 671 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( com.android.server.wm.MiuiWindowMonitorImpl$ProcessKey p0 ) {
/* .locals 4 */
/* .param p1, "that" # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
/* .line 674 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 675 */
/* .line 677 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne p0, p1, :cond_1 */
/* .line 678 */
/* .line 680 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPid:I */
/* iget v3, p1, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPid:I */
/* if-ne v2, v3, :cond_2 */
v2 = this.mPackageName;
v3 = this.mPackageName;
/* .line 681 */
v2 = android.text.TextUtils .equals ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 682 */
/* .line 684 */
} // :cond_2
} // .end method
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .line 690 */
try { // :try_start_0
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
v0 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessKey ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->equals(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Z
/* :try_end_0 */
/* .catch Ljava/lang/ClassCastException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 691 */
/* :catch_0 */
/* move-exception v0 */
/* .line 693 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer hashCode ( ) {
/* .locals 2 */
/* .line 698 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
} // .end method
