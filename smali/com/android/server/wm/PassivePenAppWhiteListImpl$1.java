class com.android.server.wm.PassivePenAppWhiteListImpl$1 extends android.content.BroadcastReceiver {
	 /* .source "PassivePenAppWhiteListImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/PassivePenAppWhiteListImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.PassivePenAppWhiteListImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.PassivePenAppWhiteListImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/PassivePenAppWhiteListImpl; */
/* .line 140 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 143 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 144 */
/* .local v0, "mAction":Ljava/lang/String; */
v1 = this.this$0;
v1 = this.mAtmService;
int v2 = 0; // const/4 v2, 0x0
v1 = (( com.android.server.wm.ActivityTaskManagerService ) v1 ).isKeyguardLocked ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/ActivityTaskManagerService;->isKeyguardLocked(I)Z
/* .line 145 */
/* .local v1, "isLocked":Z */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " onReceive= "; // const-string v4, " onReceive= "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "PassivePenAppWhiteListImpl"; // const-string v4, "PassivePenAppWhiteListImpl"
android.util.Slog .d ( v4,v3 );
/* .line 146 */
v3 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v4 = 1; // const/4 v4, 0x1
/* sparse-switch v3, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v3 = "android.intent.action.USER_PRESENT"; // const-string v3, "android.intent.action.USER_PRESENT"
v3 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* move v3, v4 */
/* :sswitch_1 */
final String v3 = "android.intent.action.BOOT_COMPLETED"; // const-string v3, "android.intent.action.BOOT_COMPLETED"
v3 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
	 int v3 = 3; // const/4 v3, 0x3
	 /* :sswitch_2 */
	 final String v3 = "android.intent.action.SCREEN_ON"; // const-string v3, "android.intent.action.SCREEN_ON"
	 v3 = 	 (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* move v3, v2 */
		 /* :sswitch_3 */
		 final String v3 = "android.intent.action.SCREEN_OFF"; // const-string v3, "android.intent.action.SCREEN_OFF"
		 v3 = 		 (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 int v3 = 2; // const/4 v3, 0x2
		 } // :goto_0
		 int v3 = -1; // const/4 v3, -0x1
	 } // :goto_1
	 /* packed-switch v3, :pswitch_data_0 */
	 /* .line 159 */
	 /* :pswitch_0 */
	 v2 = this.this$0;
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v4 = "passivepen"; // const-string v4, "passivepen"
	 com.android.server.wm.PassivePenAppWhiteListImpl .-$$Nest$mreadLocalCloudControlData ( v2,v3,v4 );
	 /* .line 160 */
	 /* .line 154 */
	 /* :pswitch_1 */
	 v3 = this.this$0;
	 v3 = 	 com.android.server.wm.PassivePenAppWhiteListImpl .-$$Nest$fgetwhiteListTopApp ( v3 );
	 /* if-ne v3, v4, :cond_1 */
	 /* .line 155 */
	 v3 = this.this$0;
	 (( com.android.server.wm.PassivePenAppWhiteListImpl ) v3 ).dispatchTopAppWhiteState ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->dispatchTopAppWhiteState(I)Z
	 /* .line 149 */
	 /* :pswitch_2 */
	 v2 = this.this$0;
	 v2 = 	 com.android.server.wm.PassivePenAppWhiteListImpl .-$$Nest$fgetwhiteListTopApp ( v2 );
	 /* if-ne v2, v4, :cond_1 */
	 /* if-nez v1, :cond_1 */
	 v2 = this.this$0;
	 /* iget-boolean v2, v2, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->splitMode:Z */
	 /* if-nez v2, :cond_1 */
	 /* .line 150 */
	 v2 = this.this$0;
	 (( com.android.server.wm.PassivePenAppWhiteListImpl ) v2 ).dispatchTopAppWhiteState ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->dispatchTopAppWhiteState(I)Z
	 /* .line 163 */
} // :cond_1
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7ed8ea7f -> :sswitch_3 */
/* -0x56ac2893 -> :sswitch_2 */
/* 0x2f94f923 -> :sswitch_1 */
/* 0x311a1d6c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
