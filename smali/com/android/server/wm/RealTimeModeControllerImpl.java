public class com.android.server.wm.RealTimeModeControllerImpl implements com.android.server.wm.RealTimeModeControllerStub {
	 /* .source "RealTimeModeControllerImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 public static final Boolean DEBUG;
	 public static Boolean ENABLE_RT_GESTURE;
	 public static Boolean ENABLE_RT_MODE;
	 public static Boolean ENABLE_TEMP_LIMIT_ENABLE;
	 public static Boolean IGNORE_CLOUD_ENABLE;
	 private static final Integer MSG_REGISTER_CLOUD_OBSERVER;
	 private static final java.lang.String PERF_SHIELDER_GESTURE_KEY;
	 private static final java.lang.String PERF_SHIELDER_RTMODE_KEY;
	 private static final java.lang.String PERF_SHIELDER_RTMODE_MOUDLE;
	 private static final java.lang.String RT_ENABLE_CLOUD;
	 private static final java.lang.String RT_ENABLE_TEMPLIMIT_CLOUD;
	 private static final java.lang.String RT_GESTURE_ENABLE_CLOUD;
	 public static final java.util.HashSet RT_GESTURE_WHITE_LIST;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashSet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.lang.String RT_GESTURE_WHITE_LIST_CLOUD;
public static final java.util.HashSet RT_PKG_BLACK_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String RT_PKG_BLACK_LIST_CLOUD;
public static final java.util.HashSet RT_PKG_WHITE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String RT_PKG_WHITE_LIST_CLOUD;
public static Integer RT_TEMPLIMIT_BOTTOM;
private static final java.lang.String RT_TEMPLIMIT_BOTTOM_CLOUD;
public static Integer RT_TEMPLIMIT_CEILING;
private static final java.lang.String RT_TEMPLIMIT_CEILING_CLOUD;
public static final java.lang.String TAG;
private static android.content.Context mContext;
/* # instance fields */
private Boolean isInit;
private com.miui.server.rtboost.SchedBoostManagerInternal mBoostInternal;
private com.android.server.wm.WindowState mCurrentFocus;
private android.os.Handler mH;
private android.os.HandlerThread mHandlerThread;
private final java.lang.Object mLock;
private android.content.pm.PackageManager mPm;
private com.android.server.wm.WindowManagerService mService;
private final java.util.HashMap mUidToAppBitType;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$mregisterCloudObserver ( com.android.server.wm.RealTimeModeControllerImpl p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/RealTimeModeControllerImpl;->registerCloudObserver(Landroid/content/Context;)V */
return;
} // .end method
static void -$$Nest$mupdateCloudControlParas ( com.android.server.wm.RealTimeModeControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->updateCloudControlParas()V */
return;
} // .end method
static android.content.Context -$$Nest$sfgetmContext ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.RealTimeModeControllerImpl.mContext;
} // .end method
static void -$$Nest$smupdateGestureCloudControlParas ( ) { //bridge//synthethic
/* .locals 0 */
com.android.server.wm.RealTimeModeControllerImpl .updateGestureCloudControlParas ( );
return;
} // .end method
static com.android.server.wm.RealTimeModeControllerImpl ( ) {
/* .locals 3 */
/* .line 42 */
int v0 = 0; // const/4 v0, 0x0
/* .line 49 */
/* nop */
/* .line 50 */
final String v0 = "persist.sys.debug_rtmode"; // const-string v0, "persist.sys.debug_rtmode"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.RealTimeModeControllerImpl.DEBUG = (v0!= 0);
/* .line 51 */
/* nop */
/* .line 52 */
final String v0 = "persist.sys.enable_rtmode"; // const-string v0, "persist.sys.enable_rtmode"
int v2 = 1; // const/4 v2, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v2 );
com.android.server.wm.RealTimeModeControllerImpl.ENABLE_RT_MODE = (v0!= 0);
/* .line 53 */
/* nop */
/* .line 54 */
final String v0 = "persist.sys.enable_ignorecloud_rtmode"; // const-string v0, "persist.sys.enable_ignorecloud_rtmode"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.RealTimeModeControllerImpl.IGNORE_CLOUD_ENABLE = (v0!= 0);
/* .line 55 */
/* nop */
/* .line 56 */
final String v0 = "persist.sys.enable_sched_gesture"; // const-string v0, "persist.sys.enable_sched_gesture"
v0 = android.os.SystemProperties .getBoolean ( v0,v2 );
com.android.server.wm.RealTimeModeControllerImpl.ENABLE_RT_GESTURE = (v0!= 0);
/* .line 57 */
/* nop */
/* .line 58 */
final String v0 = "persist.sys.enable_templimit"; // const-string v0, "persist.sys.enable_templimit"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.RealTimeModeControllerImpl.ENABLE_TEMP_LIMIT_ENABLE = (v0!= 0);
/* .line 59 */
/* nop */
/* .line 60 */
final String v0 = "persist.sys.rtmode_templimit_bottom"; // const-string v0, "persist.sys.rtmode_templimit_bottom"
/* const/16 v1, 0x2a */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 61 */
/* nop */
/* .line 62 */
final String v0 = "persist.sys.rtmode_templimit_ceiling"; // const-string v0, "persist.sys.rtmode_templimit_ceiling"
/* const/16 v1, 0x2d */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 76 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 77 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 78 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 90 */
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 91 */
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 92 */
final String v1 = "com.mi.android.globallauncher"; // const-string v1, "com.mi.android.globallauncher"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 93 */
final String v1 = "com.android.provision"; // const-string v1, "com.android.provision"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 95 */
final String v1 = "com.miui.miwallpaper.snowmountain"; // const-string v1, "com.miui.miwallpaper.snowmountain"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 96 */
final String v1 = "com.miui.miwallpaper.earth"; // const-string v1, "com.miui.miwallpaper.earth"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 97 */
final String v1 = "com.miui.miwallpaper.geometry"; // const-string v1, "com.miui.miwallpaper.geometry"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 98 */
final String v1 = "com.miui.miwallpaper.saturn"; // const-string v1, "com.miui.miwallpaper.saturn"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 99 */
final String v1 = "com.miui.miwallpaper.mars"; // const-string v1, "com.miui.miwallpaper.mars"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 100 */
final String v1 = "com.miui.fliphome"; // const-string v1, "com.miui.fliphome"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 101 */
final String v1 = "com.android.quicksearchbox"; // const-string v1, "com.android.quicksearchbox"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 102 */
return;
} // .end method
public com.android.server.wm.RealTimeModeControllerImpl ( ) {
/* .locals 1 */
/* .line 37 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 80 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z */
/* .line 81 */
int v0 = 0; // const/4 v0, 0x0
this.mPm = v0;
/* .line 82 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mUidToAppBitType = v0;
/* .line 83 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
return;
} // .end method
private void boostTopApp ( Integer p0, Long p1 ) {
/* .locals 10 */
/* .param p1, "mode" # I */
/* .param p2, "durationMs" # J */
/* .line 393 */
(( com.android.server.wm.RealTimeModeControllerImpl ) p0 ).getAppPackageName ( ); // invoke-virtual {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getAppPackageName()Ljava/lang/String;
/* .line 394 */
/* .local v6, "focusedPackage":Ljava/lang/String; */
/* if-nez v6, :cond_0 */
/* .line 395 */
final String v0 = "RTMode"; // const-string v0, "RTMode"
final String v1 = "Error: package name null"; // const-string v1, "Error: package name null"
android.util.Slog .e ( v0,v1 );
/* .line 396 */
return;
/* .line 398 */
} // :cond_0
v7 = (( com.android.server.wm.RealTimeModeControllerImpl ) p0 ).couldBoostTopAppProcess ( v6 ); // invoke-virtual {p0, v6}, Lcom/android/server/wm/RealTimeModeControllerImpl;->couldBoostTopAppProcess(Ljava/lang/String;)Z
/* .line 399 */
/* .local v7, "couldBoost":Z */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 400 */
v8 = (( com.android.server.wm.RealTimeModeControllerImpl ) p0 ).getCurrentFocusProcessPid ( ); // invoke-virtual {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getCurrentFocusProcessPid()I
/* .line 401 */
/* .local v8, "pid":I */
v9 = miui.process.ProcessManager .getRenderThreadTidByPid ( v8 );
/* .line 402 */
/* .local v9, "renderThreadTid":I */
/* invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
/* filled-new-array {v8, v9}, [I */
/* move-wide v2, p2 */
/* move-object v4, v6 */
/* move v5, p1 */
/* invoke-interface/range {v0 ..v5}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->beginSchedThreads([IJLjava/lang/String;I)V */
/* .line 404 */
} // .end local v8 # "pid":I
} // .end local v9 # "renderThreadTid":I
/* .line 406 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
/* .line 408 */
} // :goto_0
return;
} // .end method
public static void dump ( java.io.PrintWriter p0, java.lang.String[] p1 ) {
/* .locals 5 */
/* .param p0, "pw" # Ljava/io/PrintWriter; */
/* .param p1, "args" # [Ljava/lang/String; */
/* .line 459 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "persist.sys.debug_rtmode: "; // const-string v1, "persist.sys.debug_rtmode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 460 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "persist.sys.enable_rtmode: "; // const-string v1, "persist.sys.enable_rtmode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 461 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "persist.sys.enable_ignorecloud_rtmode: "; // const-string v1, "persist.sys.enable_ignorecloud_rtmode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->IGNORE_CLOUD_ENABLE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 462 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "persist.sys.enable_sched_gesture: "; // const-string v1, "persist.sys.enable_sched_gesture: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 463 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "persist.sys.enable_templimit: "; // const-string v1, "persist.sys.enable_templimit: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_TEMP_LIMIT_ENABLE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 464 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "rt_templimit_bottom: "; // const-string v1, "rt_templimit_bottom: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 465 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "rt_templimit_ceiling: "; // const-string v1, "rt_templimit_ceiling: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 466 */
final String v0 = "RT_PKG_WHITE_LIST: "; // const-string v0, "RT_PKG_WHITE_LIST: "
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 467 */
v0 = com.android.server.wm.RealTimeModeControllerImpl.RT_PKG_WHITE_LIST;
/* monitor-enter v0 */
/* .line 468 */
try { // :try_start_0
(( java.util.HashSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/lang/String; */
/* .line 469 */
/* .local v2, "white":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v3 ); // invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 470 */
} // .end local v2 # "white":Ljava/lang/String;
/* .line 471 */
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 472 */
final String v0 = "RT_PKG_BLACK_LIST: "; // const-string v0, "RT_PKG_BLACK_LIST: "
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 473 */
v1 = com.android.server.wm.RealTimeModeControllerImpl.RT_PKG_BLACK_LIST;
/* monitor-enter v1 */
/* .line 474 */
try { // :try_start_1
(( java.util.HashSet ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/String; */
/* .line 475 */
/* .local v2, "black":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v3 ); // invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 476 */
} // .end local v2 # "black":Ljava/lang/String;
/* .line 477 */
} // :cond_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 478 */
final String v0 = "RT_GESTURE_WHITE_LIST: "; // const-string v0, "RT_GESTURE_WHITE_LIST: "
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 479 */
v0 = com.android.server.wm.RealTimeModeControllerImpl.RT_GESTURE_WHITE_LIST;
/* monitor-enter v0 */
/* .line 480 */
try { // :try_start_2
(( java.util.HashSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v2 = } // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/String; */
/* .line 481 */
/* .local v2, "gest_white":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v3 ); // invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 482 */
} // .end local v2 # "gest_white":Ljava/lang/String;
/* .line 483 */
} // :cond_2
/* monitor-exit v0 */
/* .line 484 */
return;
/* .line 483 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 477 */
/* :catchall_1 */
/* move-exception v0 */
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v0 */
/* .line 471 */
/* :catchall_2 */
/* move-exception v1 */
try { // :try_start_4
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* throw v1 */
} // .end method
public static com.android.server.wm.RealTimeModeControllerImpl get ( ) {
/* .locals 1 */
/* .line 86 */
com.android.server.wm.RealTimeModeControllerStub .get ( );
/* check-cast v0, Lcom/android/server/wm/RealTimeModeControllerImpl; */
} // .end method
private com.miui.server.rtboost.SchedBoostManagerInternal getSchedBoostService ( ) {
/* .locals 1 */
/* .line 432 */
v0 = this.mBoostInternal;
/* if-nez v0, :cond_0 */
/* .line 433 */
/* const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
this.mBoostInternal = v0;
/* .line 435 */
} // :cond_0
v0 = this.mBoostInternal;
} // .end method
private Boolean isCurrentApp32Bit ( ) {
/* .locals 6 */
/* .line 413 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 414 */
try { // :try_start_0
v1 = this.mCurrentFocus;
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 415 */
/* monitor-exit v0 */
/* .line 417 */
} // :cond_0
/* iget v1, v1, Lcom/android/server/wm/WindowState;->mOwnerUid:I */
/* .line 418 */
/* .local v1, "currentUid":I */
v3 = this.mCurrentFocus;
v3 = this.mSession;
v3 = this.mPackageName;
/* .line 419 */
/* .local v3, "packageName":Ljava/lang/String; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 421 */
/* const/16 v0, 0x2710 */
/* if-ge v1, v0, :cond_1 */
/* .line 422 */
/* .line 423 */
} // :cond_1
v0 = this.mUidToAppBitType;
java.lang.Integer .valueOf ( v1 );
v0 = (( java.util.HashMap ) v0 ).containsKey ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
/* .line 424 */
v0 = com.android.server.wm.RealTimeModeControllerImpl.mContext;
v0 = android.os.MiuiProcess .is32BitApp ( v0,v3 );
/* .line 425 */
/* .local v0, "is32Bit":Z */
v2 = this.mUidToAppBitType;
java.lang.Integer .valueOf ( v1 );
java.lang.Boolean .valueOf ( v0 );
(( java.util.HashMap ) v2 ).put ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 428 */
} // .end local v0 # "is32Bit":Z
} // :cond_2
v0 = this.mUidToAppBitType;
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 419 */
} // .end local v1 # "currentUid":I
} // .end local v3 # "packageName":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public static Boolean isHomeProcess ( com.android.server.wm.WindowProcessController p0 ) {
/* .locals 1 */
/* .param p0, "wpc" # Lcom/android/server/wm/WindowProcessController; */
/* .line 344 */
v0 = (( com.android.server.wm.WindowProcessController ) p0 ).isHomeProcess ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowProcessController;->isHomeProcess()Z
} // .end method
public static Boolean isSystemUIProcess ( com.android.server.wm.WindowProcessController p0 ) {
/* .locals 2 */
/* .param p0, "wpc" # Lcom/android/server/wm/WindowProcessController; */
/* .line 348 */
v0 = this.mName;
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
v0 = android.text.TextUtils .equals ( v0,v1 );
} // .end method
private void registerCloudObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 147 */
/* new-instance v0, Lcom/android/server/wm/RealTimeModeControllerImpl$2; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/RealTimeModeControllerImpl$2;-><init>(Lcom/android/server/wm/RealTimeModeControllerImpl;Landroid/os/Handler;)V */
/* .line 158 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 159 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* .line 158 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 160 */
return;
} // .end method
private void updateCloudControlParas ( ) {
/* .locals 11 */
/* .line 195 */
final String v0 = "RTMode"; // const-string v0, "RTMode"
/* sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->IGNORE_CLOUD_ENABLE:Z */
/* if-nez v1, :cond_7 */
/* .line 196 */
v1 = com.android.server.wm.RealTimeModeControllerImpl.mContext;
/* .line 197 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "perf_shielder_RTMODE"; // const-string v2, "perf_shielder_RTMODE"
final String v3 = ""; // const-string v3, ""
final String v4 = "perf_rtmode"; // const-string v4, "perf_rtmode"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v1,v4,v2,v3 );
/* .line 199 */
/* .local v1, "data":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 200 */
return;
/* .line 203 */
} // :cond_0
try { // :try_start_0
/* new-instance v2, Lorg/json/JSONObject; */
/* invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 204 */
/* .local v2, "jsonObject":Lorg/json/JSONObject; */
final String v3 = "perf_rt_enable"; // const-string v3, "perf_rt_enable"
(( org.json.JSONObject ) v2 ).optString ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 205 */
/* .local v3, "rtModeEnable":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 206 */
v4 = java.lang.Boolean .parseBoolean ( v3 );
com.android.server.wm.RealTimeModeControllerImpl.ENABLE_RT_MODE = (v4!= 0);
/* .line 207 */
/* sget-boolean v4, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 208 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "RTMode enable cloud control set received : "; // const-string v5, "RTMode enable cloud control set received : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 213 */
} // :cond_1
final String v4 = "rt_pkg_white_list"; // const-string v4, "rt_pkg_white_list"
(( org.json.JSONObject ) v2 ).optString ( v4 ); // invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 214 */
/* .local v4, "rtWhiteList":Ljava/lang/String; */
v5 = android.text.TextUtils .isEmpty ( v4 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
final String v6 = ","; // const-string v6, ","
/* if-nez v5, :cond_2 */
/* .line 215 */
try { // :try_start_1
v5 = com.android.server.wm.RealTimeModeControllerImpl.RT_PKG_WHITE_LIST;
(( java.util.HashSet ) v5 ).clear ( ); // invoke-virtual {v5}, Ljava/util/HashSet;->clear()V
/* .line 216 */
v7 = com.android.server.wm.RealTimeModeControllerImpl.mContext;
(( android.content.Context ) v7 ).getResources ( ); // invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 217 */
/* .local v7, "r":Landroid/content/res/Resources; */
/* nop */
/* .line 218 */
/* const v8, 0x110300be */
(( android.content.res.Resources ) v7 ).getStringArray ( v8 ); // invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 219 */
/* .local v8, "whiteList":[Ljava/lang/String; */
java.util.Arrays .asList ( v8 );
(( java.util.HashSet ) v5 ).addAll ( v9 ); // invoke-virtual {v5, v9}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 220 */
(( java.lang.String ) v4 ).split ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v9 );
(( java.util.HashSet ) v5 ).addAll ( v9 ); // invoke-virtual {v5, v9}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 221 */
/* sget-boolean v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 222 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "RTMode rtWhiteList cloud control set received : "; // const-string v9, "RTMode rtWhiteList cloud control set received : "
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v5 );
/* .line 227 */
} // .end local v7 # "r":Landroid/content/res/Resources;
} // .end local v8 # "whiteList":[Ljava/lang/String;
} // :cond_2
final String v5 = "rt_pkg_black_list"; // const-string v5, "rt_pkg_black_list"
(( org.json.JSONObject ) v2 ).optString ( v5 ); // invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 228 */
/* .local v5, "rtBlackList":Ljava/lang/String; */
v7 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v7, :cond_3 */
/* .line 229 */
v7 = com.android.server.wm.RealTimeModeControllerImpl.RT_PKG_BLACK_LIST;
(( java.util.HashSet ) v7 ).clear ( ); // invoke-virtual {v7}, Ljava/util/HashSet;->clear()V
/* .line 230 */
(( java.lang.String ) v5 ).split ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v6 );
(( java.util.HashSet ) v7 ).addAll ( v6 ); // invoke-virtual {v7, v6}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 231 */
/* sget-boolean v6, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 232 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "RTMode rtBlackList cloud control set received : "; // const-string v7, "RTMode rtBlackList cloud control set received : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v6 );
/* .line 237 */
} // :cond_3
final String v6 = "rt_templimit_bottom"; // const-string v6, "rt_templimit_bottom"
(( org.json.JSONObject ) v2 ).optString ( v6 ); // invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 238 */
/* .local v6, "rtTempLimitBottom":Ljava/lang/String; */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 239 */
v7 = java.lang.Integer .parseInt ( v6 );
/* .line 240 */
/* sget-boolean v7, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 241 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "RTMode Temp Limit bottom cloud control set received : "; // const-string v8, "RTMode Temp Limit bottom cloud control set received : "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v7 );
/* .line 246 */
} // :cond_4
final String v7 = "rt_templimit_ceiling"; // const-string v7, "rt_templimit_ceiling"
(( org.json.JSONObject ) v2 ).optString ( v7 ); // invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 247 */
/* .local v7, "rtTempLimitCeiling":Ljava/lang/String; */
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 248 */
v8 = java.lang.Integer .parseInt ( v7 );
/* .line 249 */
/* sget-boolean v8, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 250 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "RTMode Temp Limit ceiling cloud control set received : "; // const-string v9, "RTMode Temp Limit ceiling cloud control set received : "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v8 );
/* .line 255 */
} // :cond_5
final String v8 = "rt_enable_templimit"; // const-string v8, "rt_enable_templimit"
(( org.json.JSONObject ) v2 ).optString ( v8 ); // invoke-virtual {v2, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 256 */
/* .local v8, "rtEnableTempLimit":Ljava/lang/String; */
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 257 */
v9 = java.lang.Boolean .parseBoolean ( v8 );
com.android.server.wm.RealTimeModeControllerImpl.ENABLE_TEMP_LIMIT_ENABLE = (v9!= 0);
/* .line 258 */
/* sget-boolean v9, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v9 != null) { // if-eqz v9, :cond_6
/* .line 259 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "RTMode Temp Limit enable cloud control set received : "; // const-string v10, "RTMode Temp Limit enable cloud control set received : "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v10, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_TEMP_LIMIT_ENABLE:Z */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v9 );
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 265 */
} // .end local v2 # "jsonObject":Lorg/json/JSONObject;
} // .end local v3 # "rtModeEnable":Ljava/lang/String;
} // .end local v4 # "rtWhiteList":Ljava/lang/String;
} // .end local v5 # "rtBlackList":Ljava/lang/String;
} // .end local v6 # "rtTempLimitBottom":Ljava/lang/String;
} // .end local v7 # "rtTempLimitCeiling":Ljava/lang/String;
} // .end local v8 # "rtEnableTempLimit":Ljava/lang/String;
} // :cond_6
/* .line 263 */
/* :catch_0 */
/* move-exception v2 */
/* .line 264 */
/* .local v2, "e":Lorg/json/JSONException; */
/* const-string/jumbo v3, "updateCloudData error :" */
android.util.Slog .e ( v0,v3,v2 );
/* .line 267 */
} // .end local v1 # "data":Ljava/lang/String;
} // .end local v2 # "e":Lorg/json/JSONException;
} // :cond_7
} // :goto_0
return;
} // .end method
private static void updateGestureCloudControlParas ( ) {
/* .locals 7 */
/* .line 163 */
final String v0 = "RTMode"; // const-string v0, "RTMode"
/* sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->IGNORE_CLOUD_ENABLE:Z */
/* if-nez v1, :cond_3 */
/* .line 164 */
v1 = com.android.server.wm.RealTimeModeControllerImpl.mContext;
/* .line 165 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "perf_shielder_GESTURE"; // const-string v2, "perf_shielder_GESTURE"
final String v3 = ""; // const-string v3, ""
final String v4 = "perf_rtmode"; // const-string v4, "perf_rtmode"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v1,v4,v2,v3 );
/* .line 167 */
/* .local v1, "data":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 168 */
return;
/* .line 171 */
} // :cond_0
try { // :try_start_0
/* new-instance v2, Lorg/json/JSONObject; */
/* invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 172 */
/* .local v2, "jsonObject":Lorg/json/JSONObject; */
final String v3 = "perf_rt_gesture_enable"; // const-string v3, "perf_rt_gesture_enable"
(( org.json.JSONObject ) v2 ).optString ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 173 */
/* .local v3, "rtGestureEnable":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 174 */
v4 = java.lang.Boolean .parseBoolean ( v3 );
com.android.server.wm.RealTimeModeControllerImpl.ENABLE_RT_GESTURE = (v4!= 0);
/* .line 175 */
/* sget-boolean v4, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 176 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "rtGestureEnable cloud control set received : "; // const-string v5, "rtGestureEnable cloud control set received : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 181 */
} // :cond_1
final String v4 = "rt_gesture_white_list"; // const-string v4, "rt_gesture_white_list"
(( org.json.JSONObject ) v2 ).optString ( v4 ); // invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 182 */
/* .local v4, "rtGestureList":Ljava/lang/String; */
v5 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v5, :cond_2 */
/* .line 183 */
v5 = com.android.server.wm.RealTimeModeControllerImpl.RT_GESTURE_WHITE_LIST;
final String v6 = ","; // const-string v6, ","
(( java.lang.String ) v4 ).split ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v6 );
(( java.util.HashSet ) v5 ).addAll ( v6 ); // invoke-virtual {v5, v6}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 184 */
/* sget-boolean v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 185 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "rtGestureList cloud control set received : "; // const-string v6, "rtGestureList cloud control set received : "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v5 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 190 */
} // .end local v2 # "jsonObject":Lorg/json/JSONObject;
} // .end local v3 # "rtGestureEnable":Ljava/lang/String;
} // .end local v4 # "rtGestureList":Ljava/lang/String;
} // :cond_2
/* .line 188 */
/* :catch_0 */
/* move-exception v2 */
/* .line 189 */
/* .local v2, "e":Lorg/json/JSONException; */
/* const-string/jumbo v3, "updateCloudData error :" */
android.util.Slog .e ( v0,v3,v2 );
/* .line 192 */
} // .end local v1 # "data":Ljava/lang/String;
} // .end local v2 # "e":Lorg/json/JSONException;
} // :cond_3
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean checkCallerPermission ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 352 */
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z */
final String v1 = "RTMode"; // const-string v1, "RTMode"
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
/* if-nez v0, :cond_0 */
/* .line 360 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = com.android.server.wm.RealTimeModeControllerImpl.RT_GESTURE_WHITE_LIST;
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 361 */
int v0 = 1; // const/4 v0, 0x1
/* .line 363 */
} // :cond_1
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "pkgName is null or has no permission"; // const-string v3, "pkgName is null or has no permission"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 364 */
} // :cond_2
/* .line 353 */
} // :cond_3
} // :goto_0
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 354 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "ENABLE_RT_MODE : "; // const-string v3, "ENABLE_RT_MODE : "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = "ENABLE_RT_GESTURE : "; // const-string v3, "ENABLE_RT_GESTURE : "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 357 */
} // :cond_4
} // .end method
public Boolean checkThreadBoost ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "tid" # I */
/* .line 439 */
/* iget-boolean v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 440 */
/* .line 442 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
/* if-nez v0, :cond_1 */
/* .line 445 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
/* .line 443 */
} // :cond_2
} // :goto_0
} // .end method
public Boolean couldBoostTopAppProcess ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "currentPackage" # Ljava/lang/String; */
/* .line 270 */
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
final String v1 = "RTMode"; // const-string v1, "RTMode"
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_1 */
/* .line 271 */
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "ENABLE_RT_MODE : "; // const-string v3, "ENABLE_RT_MODE : "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 272 */
} // :cond_0
/* .line 274 */
} // :cond_1
v0 = com.android.server.wm.RealTimeModeControllerImpl.RT_PKG_WHITE_LIST;
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = com.android.server.wm.RealTimeModeControllerImpl.RT_PKG_BLACK_LIST;
/* .line 275 */
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 279 */
} // :cond_2
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->isCurrentApp32Bit()Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 280 */
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "is a 32bit app, skip boost!"; // const-string v3, "is a 32bit app, skip boost!"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 281 */
} // :cond_3
/* .line 284 */
} // :cond_4
int v0 = 1; // const/4 v0, 0x1
/* .line 276 */
} // :cond_5
} // :goto_0
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "is not in whitelist or in blacklist!"; // const-string v3, "is not in whitelist or in blacklist!"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 277 */
} // :cond_6
} // .end method
public java.lang.String getAppPackageName ( ) {
/* .locals 4 */
/* .line 289 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mService;
v1 = this.mRoot;
(( com.android.server.wm.RootWindowContainer ) v1 ).getTopFocusedDisplayContent ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getTopFocusedDisplayContent()Lcom/android/server/wm/DisplayContent;
/* .line 290 */
/* .local v1, "dc":Lcom/android/server/wm/DisplayContent; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 291 */
v2 = this.mLock;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 292 */
try { // :try_start_1
v3 = this.mCurrentFocus;
this.mCurrentFocus = v3;
/* .line 293 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 294 */
v3 = this.mSession;
v3 = this.mPackageName;
/* monitor-exit v2 */
/* .line 296 */
} // :cond_0
/* monitor-exit v2 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/wm/RealTimeModeControllerImpl;
try { // :try_start_2
/* throw v3 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 298 */
/* .restart local p0 # "this":Lcom/android/server/wm/RealTimeModeControllerImpl; */
} // :cond_1
} // :goto_0
/* .line 299 */
} // .end local v1 # "dc":Lcom/android/server/wm/DisplayContent;
/* :catch_0 */
/* move-exception v1 */
/* .line 300 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "RTMode"; // const-string v2, "RTMode"
final String v3 = "failed to getAppPackageName"; // const-string v3, "failed to getAppPackageName"
android.util.Slog .e ( v2,v3,v1 );
/* .line 302 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
public Integer getCurrentFocusProcessPid ( ) {
/* .locals 3 */
/* .line 333 */
int v0 = -1; // const/4 v0, -0x1
/* .line 334 */
/* .local v0, "pid":I */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 335 */
try { // :try_start_0
v2 = this.mCurrentFocus;
/* if-nez v2, :cond_0 */
/* .line 336 */
/* monitor-exit v1 */
/* .line 338 */
} // :cond_0
v2 = this.mSession;
/* iget v2, v2, Lcom/android/server/wm/Session;->mPid:I */
/* move v0, v2 */
/* .line 339 */
/* monitor-exit v1 */
/* .line 340 */
/* .line 339 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public com.android.server.wm.WindowProcessController getWindowProcessController ( ) {
/* .locals 6 */
/* .line 309 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mLock;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 310 */
try { // :try_start_1
v2 = this.mCurrentFocus;
/* if-nez v2, :cond_0 */
/* .line 311 */
/* monitor-exit v1 */
/* .line 313 */
} // :cond_0
v2 = this.mActivityRecord;
/* .line 314 */
/* .local v2, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
v3 = this.mCurrentFocus;
v3 = this.mSession;
/* iget v3, v3, Lcom/android/server/wm/Session;->mPid:I */
/* .line 315 */
/* .local v3, "pid":I */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 316 */
try { // :try_start_2
v1 = this.mService;
v1 = this.mAtmService;
v1 = this.mHomeProcess;
/* .line 317 */
/* .local v1, "mHomeProcess":Lcom/android/server/wm/WindowProcessController; */
/* if-nez v2, :cond_1 */
/* .line 318 */
v4 = this.mService;
v4 = this.mAtmService;
v4 = this.mProcessMap;
(( com.android.server.wm.WindowProcessControllerMap ) v4 ).getProcess ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;
/* .line 319 */
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
v4 = this.mName;
v5 = this.packageName;
/* if-ne v4, v5, :cond_2 */
/* .line 321 */
v4 = (( com.android.server.wm.WindowProcessController ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* if-eq v4, v3, :cond_2 */
/* .line 322 */
v4 = this.mService;
v4 = this.mAtmService;
v4 = this.mProcessMap;
(( com.android.server.wm.WindowProcessControllerMap ) v4 ).getProcess ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;
/* .line 324 */
} // :cond_2
v0 = this.app;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 315 */
} // .end local v1 # "mHomeProcess":Lcom/android/server/wm/WindowProcessController;
} // .end local v2 # "activityRecord":Lcom/android/server/wm/ActivityRecord;
} // .end local v3 # "pid":I
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/wm/RealTimeModeControllerImpl;
try { // :try_start_4
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 326 */
/* .restart local p0 # "this":Lcom/android/server/wm/RealTimeModeControllerImpl; */
/* :catch_0 */
/* move-exception v1 */
/* .line 327 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "RTMode"; // const-string v2, "RTMode"
final String v3 = "failed to getWindowProcessController"; // const-string v3, "failed to getWindowProcessController"
android.util.Slog .e ( v2,v3,v1 );
/* .line 329 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 105 */
/* iget-boolean v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 106 */
return;
/* .line 108 */
} // :cond_0
/* .line 111 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 112 */
/* .local v0, "r":Landroid/content/res/Resources; */
/* const v1, 0x110300be */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 113 */
/* .local v1, "whiteList":[Ljava/lang/String; */
v2 = com.android.server.wm.RealTimeModeControllerImpl.RT_PKG_WHITE_LIST;
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v2 ).addAll ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 115 */
/* new-instance v2, Landroid/os/HandlerThread; */
final String v3 = "RealTimeModeControllerTh"; // const-string v3, "RealTimeModeControllerTh"
/* invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v2;
/* .line 116 */
(( android.os.HandlerThread ) v2 ).start ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V
/* .line 117 */
v2 = this.mHandlerThread;
/* .line 118 */
v2 = (( android.os.HandlerThread ) v2 ).getThreadId ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getThreadId()I
/* .line 117 */
int v3 = 2; // const/4 v3, 0x2
android.os.Process .setThreadGroupAndCpuset ( v2,v3 );
/* .line 120 */
/* new-instance v2, Lcom/android/server/wm/RealTimeModeControllerImpl$1; */
v3 = this.mHandlerThread;
(( android.os.HandlerThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/wm/RealTimeModeControllerImpl$1;-><init>(Lcom/android/server/wm/RealTimeModeControllerImpl;Landroid/os/Looper;)V */
this.mH = v2;
/* .line 135 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z */
/* .line 136 */
final String v2 = "RTMode"; // const-string v2, "RTMode"
final String v3 = "init RealTimeModeController"; // const-string v3, "init RealTimeModeController"
android.util.Slog .d ( v2,v3 );
/* .line 137 */
return;
} // .end method
public void onBootPhase ( ) {
/* .locals 2 */
/* .line 140 */
v0 = this.mH;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 141 */
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 142 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mH;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 144 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
public void onDown ( ) {
/* .locals 3 */
/* .line 383 */
v0 = com.android.server.am.MiuiProcessStub .getInstance ( );
/* .line 384 */
com.android.server.am.MiuiProcessStub .getInstance ( );
/* move-result-wide v1 */
/* .line 383 */
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->boostTopApp(IJ)V */
/* .line 385 */
return;
} // .end method
public void onFling ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "durationMs" # I */
/* .line 374 */
return;
} // .end method
public void onMove ( ) {
/* .locals 3 */
/* .line 388 */
v0 = com.android.server.am.MiuiProcessStub .getInstance ( );
/* .line 389 */
com.android.server.am.MiuiProcessStub .getInstance ( );
/* move-result-wide v1 */
/* .line 388 */
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->boostTopApp(IJ)V */
/* .line 390 */
return;
} // .end method
public void onScroll ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "started" # Z */
/* .line 377 */
/* if-nez p1, :cond_0 */
return;
/* .line 378 */
} // :cond_0
v0 = com.android.server.am.MiuiProcessStub .getInstance ( );
/* .line 379 */
com.android.server.am.MiuiProcessStub .getInstance ( );
/* move-result-wide v1 */
/* .line 378 */
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->boostTopApp(IJ)V */
/* .line 380 */
return;
} // .end method
public void setThreadSavedPriority ( Integer[] p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "tid" # [I */
/* .param p2, "prio" # I */
/* .line 449 */
/* iget-boolean v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z */
/* if-nez v0, :cond_0 */
/* .line 450 */
return;
/* .line 452 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z */
/* if-nez v0, :cond_1 */
/* .line 455 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
/* .line 456 */
return;
/* .line 453 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void setWindowManager ( com.android.server.wm.WindowManagerService p0 ) {
/* .locals 0 */
/* .param p1, "mService" # Lcom/android/server/wm/WindowManagerService; */
/* .line 369 */
this.mService = p1;
/* .line 370 */
return;
} // .end method
