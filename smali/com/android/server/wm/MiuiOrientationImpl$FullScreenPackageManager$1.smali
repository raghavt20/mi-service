.class Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$1;
.super Ljava/lang/Object;
.source "MiuiOrientationImpl.java"

# interfaces
.implements Ljava/util/function/BiConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/function/BiConsumer<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    .line 1061
    iput-object p1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$1;->this$1:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1061
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$1;->accept(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public accept(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 1064
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$1;->this$1:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v0, v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmComponentMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p2}, Lcom/android/server/wm/MiuiOrientationImpl;->parseFullScreenPolicy(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1065
    return-void
.end method
