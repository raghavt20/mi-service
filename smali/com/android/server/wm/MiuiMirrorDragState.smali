.class public Lcom/android/server/wm/MiuiMirrorDragState;
.super Ljava/lang/Object;
.source "MiuiMirrorDragState.java"


# static fields
.field private static final DRAG_FLAGS_URI_ACCESS:I = 0x3

.field private static final DRAG_FLAGS_URI_PERMISSIONS:I = 0xc3

.field private static final TAG:Ljava/lang/String; = "MiuiMirrorDragState"


# instance fields
.field mBroadcastDisplayIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mCrossProfileCopyAllowed:Z

.field mCurrentDisplayContent:Lcom/android/server/wm/DisplayContent;

.field mCurrentDisplayId:I

.field mCurrentX:F

.field mCurrentY:F

.field mData:Landroid/content/ClipData;

.field mDataDescription:Landroid/content/ClipDescription;

.field final mDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

.field mDragInProgress:Z

.field mDragResult:Z

.field mFlags:I

.field private mIsClosing:Z

.field mLocalWin:Landroid/os/IBinder;

.field private mMirrorServiceInternal:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

.field mNotifiedWindows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/wm/WindowState;",
            ">;"
        }
    .end annotation
.end field

.field mPid:I

.field final mService:Lcom/android/server/wm/WindowManagerService;

.field mSourceDisplayId:I

.field mSourceUserId:I

.field mTargetWindow:Lcom/android/server/wm/WindowState;

.field mToken:Landroid/os/IBinder;

.field mUid:I


# direct methods
.method public static synthetic $r8$lambda$PizEWj1j54G_mkmKU2MvlZkxsnA(Lcom/android/server/wm/MiuiMirrorDragState;FFLcom/android/server/wm/WindowState;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiMirrorDragState;->lambda$broadcastDragStartedLocked$0(FFLcom/android/server/wm/WindowState;)V

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/MiuiMirrorDragDropController;ILandroid/os/IBinder;)V
    .locals 1
    .param p1, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "controller"    # Lcom/android/server/wm/MiuiMirrorDragDropController;
    .param p3, "flags"    # I
    .param p4, "localWin"    # Landroid/os/IBinder;

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getInstance()Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mMirrorServiceInternal:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    .line 69
    iput-object p1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 70
    iput-object p2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    .line 71
    iput p3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I

    .line 72
    iput-object p4, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mLocalWin:Landroid/os/IBinder;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mNotifiedWindows:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mBroadcastDisplayIds:Ljava/util/ArrayList;

    .line 75
    return-void
.end method

.method private broadcastDragEndedLocked()V
    .locals 18

    .line 144
    move-object/from16 v1, p0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    .line 146
    .local v2, "myPid":I
    iget-object v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mNotifiedWindows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/android/server/wm/WindowState;

    .line 147
    .local v4, "ws":Lcom/android/server/wm/WindowState;
    const/4 v0, 0x0

    .line 148
    .local v0, "x":F
    const/4 v5, 0x0

    .line 149
    .local v5, "y":F
    iget-boolean v6, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z

    if-nez v6, :cond_0

    iget-object v6, v4, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v6, v6, Lcom/android/server/wm/Session;->mPid:I

    iget v7, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mPid:I

    if-ne v6, v7, :cond_0

    .line 151
    iget v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F

    .line 152
    iget v5, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F

    move/from16 v17, v5

    move v5, v0

    goto :goto_1

    .line 154
    :cond_0
    move/from16 v17, v5

    move v5, v0

    .end local v0    # "x":F
    .local v5, "x":F
    .local v17, "y":F
    :goto_1
    const/4 v6, 0x4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    iget-boolean v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z

    move v7, v5

    move/from16 v8, v17

    move/from16 v16, v0

    invoke-static/range {v6 .. v16}, Landroid/view/DragEvent;->obtain(IFFFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Landroid/view/SurfaceControl;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent;

    move-result-object v6

    .line 157
    .local v6, "evt":Landroid/view/DragEvent;
    :try_start_0
    iget-object v0, v4, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v0, v6}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    goto :goto_2

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to drag-end window "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "MiuiMirrorDragState"

    invoke-static {v8, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_2
    iget-object v0, v4, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v0, v0, Lcom/android/server/wm/Session;->mPid:I

    if-eq v2, v0, :cond_1

    .line 164
    invoke-virtual {v6}, Landroid/view/DragEvent;->recycle()V

    .line 166
    .end local v4    # "ws":Lcom/android/server/wm/WindowState;
    .end local v5    # "x":F
    .end local v6    # "evt":Landroid/view/DragEvent;
    .end local v17    # "y":F
    :cond_1
    goto :goto_0

    .line 167
    :cond_2
    iget-object v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mNotifiedWindows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 168
    iget-object v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mBroadcastDisplayIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 169
    return-void
.end method

.method private isValidDropTarget(Lcom/android/server/wm/WindowState;)Z
    .locals 3
    .param p1, "targetWin"    # Lcom/android/server/wm/WindowState;

    .line 200
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 201
    return v0

    .line 203
    :cond_0
    invoke-virtual {p1, v0}, Lcom/android/server/wm/WindowState;->isPotentialDragTarget(Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 204
    return v0

    .line 206
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMirrorDragState;->targetWindowSupportsGlobalDrag(Lcom/android/server/wm/WindowState;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 208
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mLocalWin:Landroid/os/IBinder;

    if-eqz v1, :cond_2

    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v2}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-eq v1, v2, :cond_3

    .line 209
    :cond_2
    return v0

    .line 213
    :cond_3
    iget-boolean v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCrossProfileCopyAllowed:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I

    .line 214
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningUid()I

    move-result v2

    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    if-ne v1, v2, :cond_5

    :cond_4
    const/4 v0, 0x1

    .line 213
    :cond_5
    return v0
.end method

.method private isWindowNotified(Lcom/android/server/wm/WindowState;)Z
    .locals 2
    .param p1, "newWin"    # Lcom/android/server/wm/WindowState;

    .line 239
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mNotifiedWindows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/WindowState;

    .line 240
    .local v1, "ws":Lcom/android/server/wm/WindowState;
    if-ne v1, p1, :cond_0

    .line 241
    const/4 v0, 0x1

    return v0

    .line 243
    .end local v1    # "ws":Lcom/android/server/wm/WindowState;
    :cond_0
    goto :goto_0

    .line 244
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private synthetic lambda$broadcastDragStartedLocked$0(FFLcom/android/server/wm/WindowState;)V
    .locals 1
    .param p1, "touchX"    # F
    .param p2, "touchY"    # F
    .param p3, "w"    # Lcom/android/server/wm/WindowState;

    .line 138
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDataDescription:Landroid/content/ClipDescription;

    invoke-direct {p0, p3, p1, p2, v0}, Lcom/android/server/wm/MiuiMirrorDragState;->sendDragStartedLocked(Lcom/android/server/wm/WindowState;FFLandroid/content/ClipDescription;)V

    .line 139
    return-void
.end method

.method private static obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent;
    .locals 16
    .param p0, "win"    # Lcom/android/server/wm/WindowState;
    .param p1, "action"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "localState"    # Ljava/lang/Object;
    .param p5, "description"    # Landroid/content/ClipDescription;
    .param p6, "data"    # Landroid/content/ClipData;
    .param p7, "dragAndDropPermissions"    # Lcom/android/internal/view/IDragAndDropPermissions;
    .param p8, "result"    # Z

    .line 371
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowState;->translateToWindowX(F)F

    move-result v13

    .line 372
    .local v13, "winX":F
    move/from16 v14, p3

    invoke-virtual {v0, v14}, Lcom/android/server/wm/WindowState;->translateToWindowY(F)F

    move-result v15

    .line 373
    .local v15, "winY":F
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x0

    move/from16 v2, p1

    move v3, v13

    move v4, v15

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v11, p7

    move/from16 v12, p8

    invoke-static/range {v2 .. v12}, Landroid/view/DragEvent;->obtain(IFFFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Landroid/view/SurfaceControl;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent;

    move-result-object v2

    return-object v2
.end method

.method private sendDragStartedLocked(Lcom/android/server/wm/WindowState;FFLandroid/content/ClipDescription;)V
    .locals 10
    .param p1, "newWin"    # Lcom/android/server/wm/WindowState;
    .param p2, "touchX"    # F
    .param p3, "touchY"    # F
    .param p4, "desc"    # Landroid/content/ClipDescription;

    .line 181
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMirrorDragState;->isValidDropTarget(Lcom/android/server/wm/WindowState;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v6, p4

    invoke-static/range {v1 .. v9}, Lcom/android/server/wm/MiuiMirrorDragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent;

    move-result-object v0

    .line 185
    .local v0, "event":Landroid/view/DragEvent;
    :try_start_0
    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v1, v0}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V

    .line 187
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mNotifiedWindows:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    if-eq v1, v2, :cond_1

    .line 193
    :goto_0
    invoke-virtual {v0}, Landroid/view/DragEvent;->recycle()V

    goto :goto_2

    .line 192
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 188
    :catch_0
    move-exception v1

    .line 189
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v2, "MiuiMirrorDragState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to drag-start window "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    nop

    .end local v1    # "e":Landroid/os/RemoteException;
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    if-eq v1, v2, :cond_1

    .line 193
    goto :goto_0

    .line 192
    :goto_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    iget-object v3, p1, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v3, v3, Lcom/android/server/wm/Session;->mPid:I

    if-eq v2, v3, :cond_0

    .line 193
    invoke-virtual {v0}, Landroid/view/DragEvent;->recycle()V

    .line 195
    :cond_0
    throw v1

    .line 197
    .end local v0    # "event":Landroid/view/DragEvent;
    :cond_1
    :goto_2
    return-void
.end method

.method private targetWindowSupportsGlobalDrag(Lcom/android/server/wm/WindowState;)Z
    .locals 2
    .param p1, "targetWin"    # Lcom/android/server/wm/WindowState;

    .line 220
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget v0, v0, Lcom/android/server/wm/ActivityRecord;->mTargetSdk:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method broadcastDragStartedLocked(IFF)V
    .locals 3
    .param p1, "displayId"    # I
    .param p2, "touchX"    # F
    .param p3, "touchY"    # F

    .line 130
    iput p1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayId:I

    .line 131
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 132
    iput p2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F

    .line 133
    iput p3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F

    .line 135
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mBroadcastDisplayIds:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mBroadcastDisplayIds:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayContent:Lcom/android/server/wm/DisplayContent;

    new-instance v1, Lcom/android/server/wm/MiuiMirrorDragState$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p2, p3}, Lcom/android/server/wm/MiuiMirrorDragState$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiMirrorDragState;FF)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/DisplayContent;->forAllWindows(Ljava/util/function/Consumer;Z)V

    .line 141
    :cond_0
    return-void
.end method

.method closeLocked()V
    .locals 1

    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked(Z)V

    .line 87
    return-void
.end method

.method closeLocked(Z)V
    .locals 3
    .param p1, "fromDelegate"    # Z

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mIsClosing:Z

    .line 92
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 93
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMirrorDragState;->broadcastDragEndedLocked()V

    .line 94
    if-nez p1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mMirrorServiceInternal:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z

    invoke-virtual {v0, v2}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->notifyDragResult(Z)V

    .line 97
    :cond_0
    iput-boolean v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z

    .line 100
    :cond_1
    iput v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mLocalWin:Landroid/os/IBinder;

    .line 102
    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mToken:Landroid/os/IBinder;

    .line 103
    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mData:Landroid/content/ClipData;

    .line 104
    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mNotifiedWindows:Ljava/util/ArrayList;

    .line 105
    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mBroadcastDisplayIds:Ljava/util/ArrayList;

    .line 108
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-virtual {v0, p0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->onDragStateClosedLocked(Lcom/android/server/wm/MiuiMirrorDragState;)V

    .line 109
    return-void
.end method

.method isClosing()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mIsClosing:Z

    return v0
.end method

.method isInProgress()Z
    .locals 1

    .line 363
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z

    return v0
.end method

.method notifyDropLocked(IFF)V
    .locals 21
    .param p1, "displayId"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F

    .line 296
    move-object/from16 v1, p0

    move/from16 v11, p2

    move/from16 v12, p3

    iget v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayId:I

    move/from16 v13, p1

    if-ne v0, v13, :cond_1

    iget-object v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayContent:Lcom/android/server/wm/DisplayContent;

    if-nez v0, :cond_0

    goto :goto_0

    .line 299
    :cond_0
    iput v11, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F

    .line 300
    iput v12, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F

    goto :goto_1

    .line 297
    :cond_1
    :goto_0
    invoke-virtual/range {p0 .. p3}, Lcom/android/server/wm/MiuiMirrorDragState;->broadcastDragStartedLocked(IFF)V

    .line 303
    :goto_1
    iget-object v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v0, v11, v12}, Lcom/android/server/wm/DisplayContent;->getTouchableWinAtPointLocked(FF)Lcom/android/server/wm/WindowState;

    move-result-object v14

    .line 305
    .local v14, "touchedWin":Lcom/android/server/wm/WindowState;
    invoke-direct {v1, v14}, Lcom/android/server/wm/MiuiMirrorDragState;->isWindowNotified(Lcom/android/server/wm/WindowState;)Z

    move-result v0

    const/4 v15, 0x0

    if-nez v0, :cond_2

    .line 308
    iput-boolean v15, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z

    .line 309
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    .line 310
    return-void

    .line 313
    :cond_2
    invoke-virtual {v14}, Lcom/android/server/wm/WindowState;->getOwningUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v10

    .line 316
    .local v10, "targetUserId":I
    iget v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I

    and-int/lit16 v2, v0, 0x100

    if-eqz v2, :cond_4

    and-int/lit8 v0, v0, 0x3

    if-eqz v0, :cond_4

    iget-object v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mData:Landroid/content/ClipData;

    if-eqz v0, :cond_4

    .line 318
    new-instance v0, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;

    iget-object v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v3, v2, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    iget-object v4, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mData:Landroid/content/ClipData;

    iget v5, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mUid:I

    .line 322
    invoke-virtual {v14}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v6

    iget v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I

    and-int/lit16 v7, v2, 0xc3

    iget v8, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I

    move-object v2, v0

    move v9, v10

    invoke-direct/range {v2 .. v9}, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;-><init>(Lcom/android/server/wm/WindowManagerGlobalLock;Landroid/content/ClipData;ILjava/lang/String;III)V

    .line 326
    .local v0, "dragAndDropPermissions":Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;
    iget v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mPid:I

    iget-object v3, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mMirrorServiceInternal:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    invoke-virtual {v3}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getDelegatePid()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 327
    iget-object v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mMirrorServiceInternal:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    .line 328
    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lcom/android/server/wm/MiuiMirrorDragState$$ExternalSyntheticLambda1;

    invoke-direct {v3, v2}, Lcom/android/server/wm/MiuiMirrorDragState$$ExternalSyntheticLambda1;-><init>(Lcom/xiaomi/mirror/service/MirrorServiceInternal;)V

    .line 327
    invoke-virtual {v0, v3}, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;->setOnPermissionReleaseListener(Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener;)V

    .line 333
    :cond_3
    move-object/from16 v16, v0

    goto :goto_2

    .line 331
    .end local v0    # "dragAndDropPermissions":Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;
    :cond_4
    const/4 v0, 0x0

    move-object/from16 v16, v0

    .line 333
    .local v16, "dragAndDropPermissions":Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;
    :goto_2
    iget v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I

    if-eq v0, v10, :cond_5

    .line 334
    iget-object v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mData:Landroid/content/ClipData;

    if-eqz v2, :cond_5

    .line 335
    invoke-virtual {v2, v0}, Landroid/content/ClipData;->fixUris(I)V

    .line 338
    :cond_5
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v9

    .line 339
    .local v9, "myPid":I
    iget-object v0, v14, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v0}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    move-result-object v8

    .line 340
    .local v8, "token":Landroid/os/IBinder;
    const/4 v3, 0x3

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mData:Landroid/content/ClipData;

    const/16 v17, 0x0

    move-object v2, v14

    move/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v18, v8

    .end local v8    # "token":Landroid/os/IBinder;
    .local v18, "token":Landroid/os/IBinder;
    move-object v8, v0

    move/from16 v19, v9

    .end local v9    # "myPid":I
    .local v19, "myPid":I
    move-object/from16 v9, v16

    move/from16 v20, v10

    .end local v10    # "targetUserId":I
    .local v20, "targetUserId":I
    move/from16 v10, v17

    invoke-static/range {v2 .. v10}, Lcom/android/server/wm/MiuiMirrorDragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent;

    move-result-object v2

    .line 343
    .local v2, "evt":Landroid/view/DragEvent;
    :try_start_0
    iget-object v0, v14, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v0, v2}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V

    .line 346
    iget-object v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragDropController:Lcom/android/server/wm/MiuiMirrorDragDropController;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object/from16 v3, v18

    .end local v18    # "token":Landroid/os/IBinder;
    .local v3, "token":Landroid/os/IBinder;
    :try_start_1
    invoke-virtual {v0, v15, v3}, Lcom/android/server/wm/MiuiMirrorDragDropController;->sendTimeoutMessage(ILjava/lang/Object;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351
    iget-object v0, v14, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v0, v0, Lcom/android/server/wm/Session;->mPid:I

    move/from16 v4, v19

    .end local v19    # "myPid":I
    .local v4, "myPid":I
    if-eq v4, v0, :cond_6

    .line 352
    :goto_3
    invoke-virtual {v2}, Landroid/view/DragEvent;->recycle()V

    goto :goto_5

    .line 351
    .end local v4    # "myPid":I
    .restart local v19    # "myPid":I
    :catchall_0
    move-exception v0

    move/from16 v4, v19

    .end local v19    # "myPid":I
    .restart local v4    # "myPid":I
    goto :goto_6

    .line 347
    .end local v4    # "myPid":I
    .restart local v19    # "myPid":I
    :catch_0
    move-exception v0

    move/from16 v4, v19

    .end local v19    # "myPid":I
    .restart local v4    # "myPid":I
    goto :goto_4

    .line 351
    .end local v3    # "token":Landroid/os/IBinder;
    .end local v4    # "myPid":I
    .restart local v18    # "token":Landroid/os/IBinder;
    .restart local v19    # "myPid":I
    :catchall_1
    move-exception v0

    move-object/from16 v3, v18

    move/from16 v4, v19

    .end local v18    # "token":Landroid/os/IBinder;
    .end local v19    # "myPid":I
    .restart local v3    # "token":Landroid/os/IBinder;
    .restart local v4    # "myPid":I
    goto :goto_6

    .line 347
    .end local v3    # "token":Landroid/os/IBinder;
    .end local v4    # "myPid":I
    .restart local v18    # "token":Landroid/os/IBinder;
    .restart local v19    # "myPid":I
    :catch_1
    move-exception v0

    move-object/from16 v3, v18

    move/from16 v4, v19

    .line 348
    .end local v18    # "token":Landroid/os/IBinder;
    .end local v19    # "myPid":I
    .local v0, "e":Landroid/os/RemoteException;
    .restart local v3    # "token":Landroid/os/IBinder;
    .restart local v4    # "myPid":I
    :goto_4
    :try_start_2
    const-string v5, "MiuiMirrorDragState"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "can\'t send drop notification to win "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 351
    .end local v0    # "e":Landroid/os/RemoteException;
    iget-object v0, v14, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v0, v0, Lcom/android/server/wm/Session;->mPid:I

    if-eq v4, v0, :cond_6

    .line 352
    goto :goto_3

    .line 355
    :cond_6
    :goto_5
    iput-object v3, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mToken:Landroid/os/IBinder;

    .line 356
    return-void

    .line 351
    :catchall_2
    move-exception v0

    :goto_6
    iget-object v5, v14, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v5, v5, Lcom/android/server/wm/Session;->mPid:I

    if-eq v4, v5, :cond_7

    .line 352
    invoke-virtual {v2}, Landroid/view/DragEvent;->recycle()V

    .line 354
    :cond_7
    throw v0
.end method

.method notifyMoveLocked(IFF)V
    .locals 11
    .param p1, "displayId"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F

    .line 248
    iget v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayId:I

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayContent:Lcom/android/server/wm/DisplayContent;

    if-nez v0, :cond_0

    goto :goto_0

    .line 251
    :cond_0
    iput p2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F

    .line 252
    iput p3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F

    goto :goto_1

    .line 249
    :cond_1
    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiMirrorDragState;->broadcastDragStartedLocked(IFF)V

    .line 256
    :goto_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayContent:Lcom/android/server/wm/DisplayContent;

    iget v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F

    iget v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F

    .line 257
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/DisplayContent;->getTouchableWinAtPointLocked(FF)Lcom/android/server/wm/WindowState;

    move-result-object v0

    .line 258
    .local v0, "touchedWin":Lcom/android/server/wm/WindowState;
    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiMirrorDragState;->isWindowNotified(Lcom/android/server/wm/WindowState;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 261
    const/4 v0, 0x0

    .line 265
    :cond_2
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    move v10, v1

    .line 268
    .local v10, "myPid":I
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    if-eq v0, v1, :cond_3

    if-eqz v1, :cond_3

    .line 270
    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/android/server/wm/MiuiMirrorDragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent;

    move-result-object v1

    .line 272
    .local v1, "evt":Landroid/view/DragEvent;
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v2, v1}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V

    .line 273
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    if-eq v10, v2, :cond_3

    .line 274
    invoke-virtual {v1}, Landroid/view/DragEvent;->recycle()V

    .line 277
    .end local v1    # "evt":Landroid/view/DragEvent;
    :cond_3
    if-eqz v0, :cond_4

    .line 278
    const/4 v2, 0x2

    iget v3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F

    iget v4, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v0

    invoke-static/range {v1 .. v9}, Lcom/android/server/wm/MiuiMirrorDragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent;

    move-result-object v1

    .line 280
    .restart local v1    # "evt":Landroid/view/DragEvent;
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v2, v1}, Landroid/view/IWindow;->dispatchDragEvent(Landroid/view/DragEvent;)V

    .line 281
    iget-object v2, v0, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    if-eq v10, v2, :cond_4

    .line 282
    invoke-virtual {v1}, Landroid/view/DragEvent;->recycle()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    .end local v1    # "evt":Landroid/view/DragEvent;
    .end local v10    # "myPid":I
    :cond_4
    goto :goto_2

    .line 285
    :catch_0
    move-exception v1

    .line 286
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "MiuiMirrorDragState"

    const-string v3, "can\'t send drag notification to windows"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_2
    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    .line 289
    return-void
.end method

.method prepareDragStartedLocked()V
    .locals 6

    .line 114
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mData:Landroid/content/ClipData;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDataDescription:Landroid/content/ClipDescription;

    .line 115
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mNotifiedWindows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 116
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mBroadcastDisplayIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z

    .line 119
    iget v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mUid:I

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I

    .line 121
    const-class v1, Lcom/android/server/pm/UserManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/UserManagerInternal;

    .line 122
    .local v1, "userManager":Lcom/android/server/pm/UserManagerInternal;
    iget v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I

    const-string v3, "no_cross_profile_copy_paste"

    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/UserManagerInternal;->getUserRestriction(ILjava/lang/String;)Z

    move-result v2

    xor-int/2addr v0, v2

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCrossProfileCopyAllowed:Z

    .line 124
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mMirrorServiceInternal:Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    iget v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mUid:I

    iget v3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mPid:I

    iget-object v4, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mData:Landroid/content/ClipData;

    iget v5, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->notifyDragStart(IILandroid/content/ClipData;I)V

    .line 125
    return-void
.end method

.method sendDragStartedIfNeededLocked(Lcom/android/server/wm/WindowState;)V
    .locals 3
    .param p1, "newWin"    # Lcom/android/server/wm/WindowState;

    .line 229
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z

    if-eqz v0, :cond_1

    .line 231
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMirrorDragState;->isWindowNotified(Lcom/android/server/wm/WindowState;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    return-void

    .line 234
    :cond_0
    iget v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F

    iget v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F

    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDataDescription:Landroid/content/ClipDescription;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/wm/MiuiMirrorDragState;->sendDragStartedLocked(Lcom/android/server/wm/WindowState;FFLandroid/content/ClipDescription;)V

    .line 236
    :cond_1
    return-void
.end method
