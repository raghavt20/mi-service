class com.android.server.wm.MiuiMultiWindowRecommendHelper$5 implements java.lang.Runnable {
	 /* .source "MiuiMultiWindowRecommendHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->displayConfigurationChange(Lcom/android/server/wm/DisplayContent;Landroid/content/res/Configuration;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendHelper this$0; //synthetic
final android.content.res.Configuration val$configuration; //synthetic
final com.android.server.wm.DisplayContent val$displayContent; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendHelper$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 577 */
this.this$0 = p1;
this.val$configuration = p2;
this.val$displayContent = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 580 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "displayConfigurationChange: configuration= "; // const-string v1, "displayConfigurationChange: configuration= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.val$configuration;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMultiWindowRecommendHelper"; // const-string v1, "MiuiMultiWindowRecommendHelper"
android.util.Slog .d ( v1,v0 );
/* .line 581 */
v0 = this.val$displayContent;
v0 = (( com.android.server.wm.DisplayContent ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 582 */
return;
/* .line 584 */
} // :cond_0
v0 = this.val$configuration;
v0 = android.util.MiuiMultiWindowUtils .isWideScreen ( v0 );
/* .line 585 */
/* .local v0, "isWideScreen":Z */
v2 = this.this$0;
v2 = com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fgetmLastIsWideScreen ( v2 );
/* if-eq v2, v0, :cond_1 */
/* .line 586 */
v2 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fputmLastIsWideScreen ( v2,v0 );
/* .line 587 */
/* if-nez v0, :cond_1 */
/* .line 588 */
v2 = this.this$0;
v2 = this.mMiuiMultiWindowRecommendController;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v2 ).removeSplitScreenRecommendView ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V
/* .line 591 */
} // :cond_1
v2 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fgetmLastConfiguration ( v2 );
v3 = this.val$configuration;
v2 = (( android.content.res.Configuration ) v2 ).updateFrom ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I
/* .line 592 */
/* .local v2, "changes":I */
/* and-int/lit16 v3, v2, 0x80 */
if ( v3 != null) { // if-eqz v3, :cond_2
int v3 = 1; // const/4 v3, 0x1
} // :cond_2
int v3 = 0; // const/4 v3, 0x0
/* .line 593 */
/* .local v3, "orientationChange":Z */
} // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 594 */
final String v4 = "orientationChange"; // const-string v4, "orientationChange"
android.util.Slog .d ( v1,v4 );
/* .line 595 */
v1 = this.this$0;
v1 = this.mMiuiMultiWindowRecommendController;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v1 ).removeRecommendView ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeRecommendView()V
/* .line 597 */
} // :cond_3
return;
} // .end method
