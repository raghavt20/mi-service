.class Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    .line 411
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 414
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 416
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v2, v2, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 417
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v2, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmLayoutParams(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Landroid/view/WindowManager$LayoutParams;)V

    .line 418
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v2, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/SplitScreenRecommendLayout;)V

    .line 419
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v2, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setSplitScreenRecommendState(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    goto :goto_0

    .line 420
    :catch_0
    move-exception v2

    .line 421
    .local v2, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v3, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmLayoutParams(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Landroid/view/WindowManager$LayoutParams;)V

    .line 422
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v3, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/SplitScreenRecommendLayout;)V

    .line 423
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setSplitScreenRecommendState(Z)V

    .line 424
    const-string v0, "MiuiMultiWindowRecommendController"

    const-string v1, " removeSplitScreenRecommendView fail"

    invoke-static {v0, v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 427
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method
