class com.android.server.wm.PolicyControl {
	 /* .source "PolicyControl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/PolicyControl$Filter; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
static final java.lang.String NAME_IMMERSIVE_FULL;
private static final java.lang.String NAME_IMMERSIVE_NAVIGATION;
private static final java.lang.String NAME_IMMERSIVE_PRECONFIRMATIONS;
private static final java.lang.String NAME_IMMERSIVE_STATUS;
private static final java.lang.String TAG;
private static com.android.server.wm.PolicyControl$Filter sImmersiveNavigationFilter;
private static com.android.server.wm.PolicyControl$Filter sImmersivePreconfirmationsFilter;
private static com.android.server.wm.PolicyControl$Filter sImmersiveStatusFilter;
private static java.lang.String sSettingValue;
/* # direct methods */
 com.android.server.wm.PolicyControl ( ) {
	 /* .locals 0 */
	 /* .line 52 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
static Integer adjustClearableFlags ( com.android.server.wm.WindowState p0, Integer p1 ) {
	 /* .locals 2 */
	 /* .param p0, "win" # Lcom/android/server/wm/WindowState; */
	 /* .param p1, "clearableFlags" # I */
	 /* .line 104 */
	 if ( p0 != null) { // if-eqz p0, :cond_0
		 (( com.android.server.wm.WindowState ) p0 ).getAttrs ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 105 */
	 /* .local v0, "attrs":Landroid/view/WindowManager$LayoutParams; */
} // :goto_0
v1 = com.android.server.wm.PolicyControl.sImmersiveStatusFilter;
if ( v1 != null) { // if-eqz v1, :cond_1
	 v1 = 	 (( com.android.server.wm.PolicyControl$Filter ) v1 ).matches ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/PolicyControl$Filter;->matches(Landroid/view/WindowManager$LayoutParams;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 106 */
		 /* and-int/lit8 p1, p1, -0x5 */
		 /* .line 108 */
	 } // :cond_1
} // .end method
static Boolean disableImmersiveConfirmation ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p0, "pkg" # Ljava/lang/String; */
	 /* .line 112 */
	 v0 = com.android.server.wm.PolicyControl.sImmersivePreconfirmationsFilter;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 113 */
		 v0 = 		 (( com.android.server.wm.PolicyControl$Filter ) v0 ).matches ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/wm/PolicyControl$Filter;->matches(Ljava/lang/String;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 114 */
	 } // :cond_0
	 v0 = 	 android.app.ActivityManager .isRunningInTestHarness ( );
	 if ( v0 != null) { // if-eqz v0, :cond_2
	 } // :cond_1
	 int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 112 */
} // :goto_0
} // .end method
private static void dump ( java.lang.String p0, com.android.server.wm.PolicyControl$Filter p1, java.lang.String p2, java.io.PrintWriter p3 ) {
/* .locals 1 */
/* .param p0, "name" # Ljava/lang/String; */
/* .param p1, "filter" # Lcom/android/server/wm/PolicyControl$Filter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 143 */
(( java.io.PrintWriter ) p3 ).print ( p2 ); // invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "PolicyControl."; // const-string v0, "PolicyControl."
(( java.io.PrintWriter ) p3 ).print ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
(( java.io.PrintWriter ) p3 ).print ( p0 ); // invoke-virtual {p3, p0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* const/16 v0, 0x3d */
(( java.io.PrintWriter ) p3 ).print ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(C)V
/* .line 144 */
/* if-nez p1, :cond_0 */
/* .line 145 */
final String v0 = "null"; // const-string v0, "null"
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 147 */
} // :cond_0
(( com.android.server.wm.PolicyControl$Filter ) p1 ).dump ( p3 ); // invoke-virtual {p1, p3}, Lcom/android/server/wm/PolicyControl$Filter;->dump(Ljava/io/PrintWriter;)V
(( java.io.PrintWriter ) p3 ).println ( ); // invoke-virtual {p3}, Ljava/io/PrintWriter;->println()V
/* .line 149 */
} // :goto_0
return;
} // .end method
static void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p0, "prefix" # Ljava/lang/String; */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 137 */
final String v0 = "sImmersiveStatusFilter"; // const-string v0, "sImmersiveStatusFilter"
v1 = com.android.server.wm.PolicyControl.sImmersiveStatusFilter;
com.android.server.wm.PolicyControl .dump ( v0,v1,p0,p1 );
/* .line 138 */
final String v0 = "sImmersiveNavigationFilter"; // const-string v0, "sImmersiveNavigationFilter"
v1 = com.android.server.wm.PolicyControl.sImmersiveNavigationFilter;
com.android.server.wm.PolicyControl .dump ( v0,v1,p0,p1 );
/* .line 139 */
final String v0 = "sImmersivePreconfirmationsFilter"; // const-string v0, "sImmersivePreconfirmationsFilter"
v1 = com.android.server.wm.PolicyControl.sImmersivePreconfirmationsFilter;
com.android.server.wm.PolicyControl .dump ( v0,v1,p0,p1 );
/* .line 140 */
return;
} // .end method
static Integer getSystemUiVisibility ( com.android.server.wm.WindowState p0, android.view.WindowManager$LayoutParams p1 ) {
/* .locals 2 */
/* .param p0, "win" # Lcom/android/server/wm/WindowState; */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 68 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* move-object v0, p1 */
} // :cond_0
(( com.android.server.wm.WindowState ) p0 ).getAttrs ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
} // :goto_0
/* move-object p1, v0 */
/* .line 69 */
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I */
/* iget v1, p1, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I */
/* or-int/2addr v0, v1 */
/* .line 70 */
/* .local v0, "vis":I */
v1 = com.android.server.wm.PolicyControl.sImmersiveStatusFilter;
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = (( com.android.server.wm.PolicyControl$Filter ) v1 ).matches ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/PolicyControl$Filter;->matches(Landroid/view/WindowManager$LayoutParams;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 71 */
/* or-int/lit16 v0, v0, 0x1004 */
/* .line 73 */
v1 = (( android.view.WindowManager$LayoutParams ) p1 ).isFullscreen ( ); // invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->isFullscreen()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 74 */
/* or-int/lit16 v0, v0, 0x400 */
/* .line 76 */
} // :cond_1
/* and-int/lit16 v0, v0, -0x101 */
/* .line 78 */
} // :cond_2
v1 = com.android.server.wm.PolicyControl.sImmersiveNavigationFilter;
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = (( com.android.server.wm.PolicyControl$Filter ) v1 ).matches ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/PolicyControl$Filter;->matches(Landroid/view/WindowManager$LayoutParams;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 79 */
/* or-int/lit16 v0, v0, 0x1002 */
/* .line 81 */
v1 = (( android.view.WindowManager$LayoutParams ) p1 ).isFullscreen ( ); // invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->isFullscreen()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 82 */
/* or-int/lit16 v0, v0, 0x200 */
/* .line 84 */
} // :cond_3
/* and-int/lit16 v0, v0, -0x101 */
/* .line 86 */
} // :cond_4
} // .end method
static Integer getWindowFlags ( com.android.server.wm.WindowState p0, android.view.WindowManager$LayoutParams p1 ) {
/* .locals 2 */
/* .param p0, "win" # Lcom/android/server/wm/WindowState; */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 90 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* move-object v0, p1 */
} // :cond_0
(( com.android.server.wm.WindowState ) p0 ).getAttrs ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
} // :goto_0
/* move-object p1, v0 */
/* .line 91 */
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 92 */
/* .local v0, "flags":I */
v1 = com.android.server.wm.PolicyControl.sImmersiveStatusFilter;
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = (( com.android.server.wm.PolicyControl$Filter ) v1 ).matches ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/PolicyControl$Filter;->matches(Landroid/view/WindowManager$LayoutParams;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 93 */
/* or-int/lit16 v0, v0, 0x400 */
/* .line 94 */
/* const v1, -0x4000801 */
/* and-int/2addr v0, v1 */
/* .line 97 */
} // :cond_1
v1 = com.android.server.wm.PolicyControl.sImmersiveNavigationFilter;
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = (( com.android.server.wm.PolicyControl$Filter ) v1 ).matches ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/PolicyControl$Filter;->matches(Landroid/view/WindowManager$LayoutParams;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 98 */
/* const v1, -0x8000001 */
/* and-int/2addr v0, v1 */
/* .line 100 */
} // :cond_2
} // .end method
static Boolean reloadFromSetting ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 119 */
int v0 = 0; // const/4 v0, 0x0
/* .line 121 */
/* .local v0, "value":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "policy_control"; // const-string v3, "policy_control"
int v4 = -2; // const/4 v4, -0x2
android.provider.Settings$Global .getStringForUser ( v2,v3,v4 );
/* move-object v0, v2 */
/* .line 124 */
v2 = com.android.server.wm.PolicyControl.sSettingValue;
/* if-eq v2, v0, :cond_1 */
if ( v2 != null) { // if-eqz v2, :cond_0
	 v2 = 	 (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 127 */
	 } // :cond_0
	 com.android.server.wm.PolicyControl .setFilters ( v0 );
	 /* .line 128 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 132 */
	 /* nop */
	 /* .line 133 */
	 int v1 = 1; // const/4 v1, 0x1
	 /* .line 125 */
} // :cond_1
} // :goto_0
/* .line 129 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 130 */
/* .local v2, "t":Ljava/lang/Throwable; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error loading policy control, value="; // const-string v4, "Error loading policy control, value="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "PolicyControl"; // const-string v4, "PolicyControl"
android.util.Slog .w ( v4,v3,v2 );
/* .line 131 */
} // .end method
static void setFilters ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p0, "value" # Ljava/lang/String; */
/* .line 154 */
int v0 = 0; // const/4 v0, 0x0
/* .line 155 */
/* .line 156 */
/* .line 157 */
if ( p0 != null) { // if-eqz p0, :cond_6
/* .line 158 */
final String v0 = ":"; // const-string v0, ":"
(( java.lang.String ) p0 ).split ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 159 */
/* .local v0, "nvps":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_6 */
/* aget-object v4, v0, v3 */
/* .line 160 */
/* .local v4, "nvp":Ljava/lang/String; */
/* const/16 v5, 0x3d */
v5 = (( java.lang.String ) v4 ).indexOf ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I
/* .line 161 */
/* .local v5, "i":I */
int v6 = -1; // const/4 v6, -0x1
/* if-ne v5, v6, :cond_0 */
/* .line 162 */
} // :cond_0
(( java.lang.String ) v4 ).substring ( v2, v5 ); // invoke-virtual {v4, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 163 */
/* .local v6, "n":Ljava/lang/String; */
/* add-int/lit8 v7, v5, 0x1 */
(( java.lang.String ) v4 ).substring ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 164 */
/* .local v7, "v":Ljava/lang/String; */
final String v8 = "immersive.full"; // const-string v8, "immersive.full"
v8 = (( java.lang.String ) v6 ).equals ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 165 */
com.android.server.wm.PolicyControl$Filter .parse ( v7 );
/* .line 166 */
/* .local v8, "f":Lcom/android/server/wm/PolicyControl$Filter; */
/* .line 167 */
v9 = com.android.server.wm.PolicyControl.sImmersivePreconfirmationsFilter;
/* if-nez v9, :cond_1 */
/* .line 168 */
/* .line 170 */
} // .end local v8 # "f":Lcom/android/server/wm/PolicyControl$Filter;
} // :cond_1
} // :cond_2
final String v8 = "immersive.status"; // const-string v8, "immersive.status"
v8 = (( java.lang.String ) v6 ).equals ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 171 */
com.android.server.wm.PolicyControl$Filter .parse ( v7 );
/* .line 172 */
/* .restart local v8 # "f":Lcom/android/server/wm/PolicyControl$Filter; */
/* .line 173 */
} // .end local v8 # "f":Lcom/android/server/wm/PolicyControl$Filter;
} // :cond_3
final String v8 = "immersive.navigation"; // const-string v8, "immersive.navigation"
v8 = (( java.lang.String ) v6 ).equals ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 174 */
com.android.server.wm.PolicyControl$Filter .parse ( v7 );
/* .line 175 */
/* .restart local v8 # "f":Lcom/android/server/wm/PolicyControl$Filter; */
/* .line 176 */
v9 = com.android.server.wm.PolicyControl.sImmersivePreconfirmationsFilter;
/* if-nez v9, :cond_5 */
/* .line 177 */
/* .line 179 */
} // .end local v8 # "f":Lcom/android/server/wm/PolicyControl$Filter;
} // :cond_4
final String v8 = "immersive.preconfirms"; // const-string v8, "immersive.preconfirms"
v8 = (( java.lang.String ) v6 ).equals ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 180 */
com.android.server.wm.PolicyControl$Filter .parse ( v7 );
/* .line 181 */
/* .restart local v8 # "f":Lcom/android/server/wm/PolicyControl$Filter; */
/* .line 179 */
} // .end local v8 # "f":Lcom/android/server/wm/PolicyControl$Filter;
} // :cond_5
} // :goto_1
/* nop */
/* .line 159 */
} // .end local v4 # "nvp":Ljava/lang/String;
} // .end local v5 # "i":I
} // .end local v6 # "n":Ljava/lang/String;
} // .end local v7 # "v":Ljava/lang/String;
} // :goto_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 190 */
} // .end local v0 # "nvps":[Ljava/lang/String;
} // :cond_6
return;
} // .end method
