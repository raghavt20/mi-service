public class com.android.server.wm.PassivePenAppWhiteListImpl {
	 /* .source "PassivePenAppWhiteListImpl.java" */
	 /* # static fields */
	 private static final Integer NOT_WHITE_LIST_TOP_APP;
	 private static final java.lang.String PASSIVE_PEN_APP_WHILTE_LIST;
	 private static final java.lang.Boolean PASSIVE_PEN_MODE_ENABLED;
	 private static final java.lang.String PASSIVE_PEN_MODULE_NAME;
	 public static final java.lang.String TAG;
	 private static final android.net.Uri URI_CLOUD_ALL_DATA_NOTIFY;
	 private static final Integer WHITE_LIST_TOP_APP;
	 private static com.android.server.wm.PassivePenAppWhiteListImpl mPassivePenImpl;
	 /* # instance fields */
	 private java.util.ArrayList codeWhiteList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.ArrayList defaultWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer lastWhiteListTopApp;
private miui.process.IForegroundInfoListener$Stub mAppObserver;
public com.android.server.wm.ActivityTaskManagerService mAtmService;
public android.content.Context mContext;
android.content.BroadcastReceiver mKeyguardStateReceiver;
protected Boolean splitMode;
private Integer whiteListTopApp;
/* # direct methods */
static Integer -$$Nest$fgetwhiteListTopApp ( com.android.server.wm.PassivePenAppWhiteListImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I */
} // .end method
static void -$$Nest$mreadLocalCloudControlData ( com.android.server.wm.PassivePenAppWhiteListImpl p0, android.content.ContentResolver p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->readLocalCloudControlData(Landroid/content/ContentResolver;Ljava/lang/String;)V */
return;
} // .end method
static com.android.server.wm.PassivePenAppWhiteListImpl ( ) {
/* .locals 2 */
/* .line 46 */
/* nop */
/* .line 47 */
final String v0 = "ro.miui.passive_pen.enabled"; // const-string v0, "ro.miui.passive_pen.enabled"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 46 */
java.lang.Boolean .valueOf ( v0 );
/* .line 48 */
/* nop */
/* .line 49 */
final String v0 = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"; // const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"
android.net.Uri .parse ( v0 );
/* .line 48 */
return;
} // .end method
private com.android.server.wm.PassivePenAppWhiteListImpl ( ) {
/* .locals 1 */
/* .line 56 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 40 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.defaultWhiteList = v0;
/* .line 41 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.codeWhiteList = v0;
/* .line 140 */
/* new-instance v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;-><init>(Lcom/android/server/wm/PassivePenAppWhiteListImpl;)V */
this.mKeyguardStateReceiver = v0;
/* .line 165 */
/* new-instance v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;-><init>(Lcom/android/server/wm/PassivePenAppWhiteListImpl;)V */
this.mAppObserver = v0;
/* .line 57 */
return;
} // .end method
public static com.android.server.wm.PassivePenAppWhiteListImpl getInstance ( ) {
/* .locals 1 */
/* .line 51 */
v0 = com.android.server.wm.PassivePenAppWhiteListImpl.mPassivePenImpl;
/* if-nez v0, :cond_0 */
/* .line 52 */
/* new-instance v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl; */
/* invoke-direct {v0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;-><init>()V */
/* .line 54 */
} // :cond_0
v0 = com.android.server.wm.PassivePenAppWhiteListImpl.mPassivePenImpl;
} // .end method
private void getPassivePenAppWhiteListFromXml ( ) {
/* .locals 3 */
/* .line 81 */
v0 = this.defaultWhiteList;
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x1103004c */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
(( java.util.ArrayList ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 83 */
return;
} // .end method
private void readLocalCloudControlData ( android.content.ContentResolver p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "contentResolver" # Landroid/content/ContentResolver; */
/* .param p2, "moduleName" # Ljava/lang/String; */
/* .line 189 */
v0 = android.text.TextUtils .isEmpty ( p2 );
final String v1 = "PassivePenAppWhiteListImpl"; // const-string v1, "PassivePenAppWhiteListImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 190 */
final String v0 = "moduleName can not be null"; // const-string v0, "moduleName can not be null"
android.util.Slog .e ( v1,v0 );
/* .line 191 */
return;
/* .line 194 */
} // :cond_0
try { // :try_start_0
/* const-string/jumbo v0, "whiteList" */
int v2 = 0; // const/4 v2, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( p1,p2,v0,v2 );
/* .line 195 */
/* .local v0, "data":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_2 */
/* .line 196 */
/* new-instance v2, Lorg/json/JSONArray; */
/* invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 197 */
/* .local v2, "apps":Lorg/json/JSONArray; */
v3 = this.codeWhiteList;
(( java.util.ArrayList ) v3 ).clear ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V
/* .line 198 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v4, :cond_1 */
/* .line 199 */
v4 = this.codeWhiteList;
(( org.json.JSONArray ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
(( java.util.ArrayList ) v4 ).add ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 198 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 201 */
} // .end local v3 # "i":I
} // :cond_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud data for listpassivepen "; // const-string v4, "cloud data for listpassivepen "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.codeWhiteList;
(( java.util.ArrayList ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 205 */
} // .end local v0 # "data":Ljava/lang/String;
} // .end local v2 # "apps":Lorg/json/JSONArray;
} // :cond_2
/* .line 203 */
/* :catch_0 */
/* move-exception v0 */
/* .line 204 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "Exception when readLocalCloudControlData :"; // const-string v2, "Exception when readLocalCloudControlData :"
android.util.Slog .e ( v1,v2,v0 );
/* .line 206 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void registerCloudControlObserver ( android.content.ContentResolver p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "contentResolver" # Landroid/content/ContentResolver; */
/* .param p2, "moduleName" # Ljava/lang/String; */
/* .line 214 */
v0 = com.android.server.wm.PassivePenAppWhiteListImpl.URI_CLOUD_ALL_DATA_NOTIFY;
/* new-instance v1, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2, p1, p2}, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;-><init>(Lcom/android/server/wm/PassivePenAppWhiteListImpl;Landroid/os/Handler;Landroid/content/ContentResolver;Ljava/lang/String;)V */
int v2 = 1; // const/4 v2, 0x1
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, v2, v1 ); // invoke-virtual {p1, v0, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 222 */
return;
} // .end method
private void registerKeyguardReceiver ( ) {
/* .locals 3 */
/* .line 73 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 74 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 75 */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 76 */
final String v1 = "android.intent.action.USER_PRESENT"; // const-string v1, "android.intent.action.USER_PRESENT"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 77 */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 78 */
v1 = this.mContext;
v2 = this.mKeyguardStateReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 79 */
return;
} // .end method
/* # virtual methods */
Boolean blackAppAndWhiteAppChanged ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 124 */
/* iget v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->lastWhiteListTopApp:I */
/* iget v1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I */
/* if-ne v0, v1, :cond_0 */
/* .line 125 */
int v0 = 0; // const/4 v0, 0x0
/* .line 127 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
Boolean dispatchTopAppWhiteState ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "value" # I */
/* .line 98 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
miui.util.ITouchFeature .getInstance ( );
/* .line 99 */
/* .local v1, "touchFeature":Lmiui/util/ITouchFeature; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 100 */
final String v2 = "PassivePenAppWhiteListImpl"; // const-string v2, "PassivePenAppWhiteListImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " dispatchTopAppWhiteState value = "; // const-string v4, " dispatchTopAppWhiteState value = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 101 */
/* const/16 v2, 0x17 */
v0 = (( miui.util.ITouchFeature ) v1 ).setTouchMode ( v0, v2, p1 ); // invoke-virtual {v1, v0, v2, p1}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 105 */
} // .end local v1 # "touchFeature":Lmiui/util/ITouchFeature;
} // :cond_0
/* .line 103 */
/* :catch_0 */
/* move-exception v1 */
/* .line 104 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 106 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
Boolean dispatchTopAppWhiteState ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "touchId" # I */
/* .param p2, "mode" # I */
/* .param p3, "value" # I */
/* .line 87 */
try { // :try_start_0
miui.util.ITouchFeature .getInstance ( );
/* .line 88 */
/* .local v0, "touchFeature":Lmiui/util/ITouchFeature; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 89 */
v1 = (( miui.util.ITouchFeature ) v0 ).setTouchMode ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 93 */
} // .end local v0 # "touchFeature":Lmiui/util/ITouchFeature;
} // :cond_0
/* .line 91 */
/* :catch_0 */
/* move-exception v0 */
/* .line 92 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 94 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
void init ( com.android.server.wm.ActivityTaskManagerService p0, android.content.Context p1 ) {
/* .locals 1 */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 59 */
v0 = com.android.server.wm.PassivePenAppWhiteListImpl.PASSIVE_PEN_MODE_ENABLED;
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 60 */
this.mAtmService = p1;
/* .line 61 */
this.mContext = p2;
/* .line 63 */
} // :cond_0
return;
} // .end method
void onSplitScreenExit ( ) {
/* .locals 2 */
/* .line 133 */
v0 = com.android.server.wm.PassivePenAppWhiteListImpl.PASSIVE_PEN_MODE_ENABLED;
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 134 */
/* iget v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 135 */
(( com.android.server.wm.PassivePenAppWhiteListImpl ) p0 ).dispatchTopAppWhiteState ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->dispatchTopAppWhiteState(I)Z
/* .line 137 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->splitMode:Z */
/* .line 139 */
} // :cond_1
return;
} // .end method
void onSystemReady ( ) {
/* .locals 2 */
/* .line 65 */
v0 = com.android.server.wm.PassivePenAppWhiteListImpl.PASSIVE_PEN_MODE_ENABLED;
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 66 */
v0 = this.mAppObserver;
miui.process.ProcessManager .registerForegroundInfoListener ( v0 );
/* .line 67 */
/* invoke-direct {p0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->getPassivePenAppWhiteListFromXml()V */
/* .line 68 */
/* invoke-direct {p0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->registerKeyguardReceiver()V */
/* .line 69 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "passivepen"; // const-string v1, "passivepen"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->registerCloudControlObserver(Landroid/content/ContentResolver;Ljava/lang/String;)V */
/* .line 71 */
} // :cond_0
return;
} // .end method
protected Boolean whiteListApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 109 */
/* iget v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I */
/* iput v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->lastWhiteListTopApp:I */
/* .line 110 */
v0 = this.defaultWhiteList;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = this.codeWhiteList;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 114 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I */
/* .line 115 */
/* .line 111 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I */
/* .line 112 */
} // .end method
