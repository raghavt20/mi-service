.class Lcom/android/server/wm/MiuiWindowMonitorImpl$6;
.super Ljava/lang/Object;
.source "MiuiWindowMonitorImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

.field final synthetic val$infoStr:Ljava/lang/String;

.field final synthetic val$report:I

.field final synthetic val$reportedPid:I

.field final synthetic val$reportedPkg:Ljava/lang/String;

.field final synthetic val$topTypeWindows:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiWindowMonitorImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 614
    iput-object p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    iput p2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$reportedPid:I

    iput-object p3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$reportedPkg:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$infoStr:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$topTypeWindows:Ljava/lang/String;

    iput p6, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$report:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 617
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    iget v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$reportedPid:I

    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$reportedPkg:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$infoStr:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$topTypeWindows:Ljava/lang/String;

    iget v5, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;->val$report:I

    invoke-static/range {v0 .. v5}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->-$$Nest$mreportWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 618
    return-void
.end method
