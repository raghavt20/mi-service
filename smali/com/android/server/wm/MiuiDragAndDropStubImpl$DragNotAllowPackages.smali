.class public Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;
.super Ljava/lang/Object;
.source "MiuiDragAndDropStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiDragAndDropStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DragNotAllowPackages"
.end annotation


# instance fields
.field private final packages:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private version:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;->packages:Landroid/util/ArraySet;

    return-void
.end method


# virtual methods
.method public getPackages()Landroid/util/ArraySet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;->packages:Landroid/util/ArraySet;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .line 79
    iget-wide v0, p0, Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;->version:J

    return-wide v0
.end method
