.class public Lcom/android/server/wm/MiuiFreezeImpl;
.super Ljava/lang/Object;
.source "MiuiFreezeImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiFreezeStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;,
        Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;,
        Lcom/android/server/wm/MiuiFreezeImpl$State;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final DELAY_REMOVE_SPLASH_PROP:Ljava/lang/String; = "persist.miui.freeze.time"

.field private static final DELAY_REMOVE_SPLASH_TIME:I

.field private static final DIALOG_DELAY_IMAGE_REC_TIME:I

.field private static final DIALOG_DELAY_SHOW_TIME_PROP:Ljava/lang/String; = "persist.sys.memfreeze.dialog.delay.time"

.field private static final FILE_PATH:Ljava/lang/String; = "/data/system/memgreezer/notify_splash_dismiss_switch"

.field private static final MEMFREEZE_ENABLE_DEBUG:Ljava/lang/String; = "persist.sys.memfreeze.debug"

.field private static final MEMFREEZE_IMAGAE_UNKNOWN:Ljava/lang/String; = "unknown"

.field private static final PUBG_PACKAGE_NAME:Ljava/lang/String; = "com.tencent.tmgp.pubgmhd"

.field private static final PUBG_SPLASH_CLASS_NAME:Ljava/lang/String; = "com.epicgames.ue4.SplashActivity"

.field private static final SPLASH_Z_ORDOR_LEVEL:I = -0x1

.field private static final TAG:Ljava/lang/String; = "MiuiFreezeImpl"

.field private static sInstance:Lcom/android/server/wm/MiuiFreezeImpl;


# instance fields
.field private atomicImageRecEnd:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private dialogShowListener:Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;

.field private isFreeForm:Z

.field private mAnimationHandler:Landroid/os/Handler;

.field private mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mBgHandler:Landroid/os/Handler;

.field private mCurPid:I

.field private mCurUid:I

.field private volatile mDialogShowing:Z

.field private mIoHandler:Landroid/os/Handler;

.field private mLoadingDialog:Lcom/android/server/wm/MiuiLoadingDialog;

.field private mPackageName:Ljava/lang/String;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRecogImageRunnable:Ljava/lang/Runnable;

.field private mRecogTimeoutRunnable:Ljava/lang/Runnable;

.field private mRemoveStartingRunnable:Ljava/lang/Runnable;

.field private mScene:Lcom/android/server/wm/MiuiRecognizeScene;

.field private final mShowDialogRunnable:Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;

.field private volatile mState:Lcom/android/server/wm/MiuiFreezeImpl$State;

.field private mUiContext:Landroid/content/Context;

.field private mUiHandler:Landroid/os/Handler;

.field private mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;


# direct methods
.method public static synthetic $r8$lambda$5mQuvweHICGoYvvJqfJLiwl_exs(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiFreezeImpl$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$writeFile$7(Lcom/android/server/wm/MiuiFreezeImpl$State;)V

    return-void
.end method

.method public static synthetic $r8$lambda$6S5YNi2her4Axe9EnomkzgvWiz0(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$new$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$AqzeD0GTjQr_4zPP1eUtxx6IEN8(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$unRegisterReceiver$6()V

    return-void
.end method

.method public static synthetic $r8$lambda$b13oYu0d3gv6oskzAWDgipcYBW0(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$dismissDialog$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$e8ohJtqrEFwHXfeuBirRcUVGSI4(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$registerReceiver$5()V

    return-void
.end method

.method public static synthetic $r8$lambda$lFvHWxzUph6kR3mgLj5ulIW_PuQ(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$new$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$oSF8cYjK2HfRevYalq1k0aG0I5o(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$new$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$rVFNB-XP3L9tBenDMv3Ix7nYqPQ(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$init$3()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetdialogShowListener(Lcom/android/server/wm/MiuiFreezeImpl;)Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->dialogShowListener:Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurUid(Lcom/android/server/wm/MiuiFreezeImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLoadingDialog(Lcom/android/server/wm/MiuiFreezeImpl;)Lcom/android/server/wm/MiuiLoadingDialog;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mLoadingDialog:Lcom/android/server/wm/MiuiLoadingDialog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageName(Lcom/android/server/wm/MiuiFreezeImpl;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mPackageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmReceiver(Lcom/android/server/wm/MiuiFreezeImpl;)Landroid/content/BroadcastReceiver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUiContext(Lcom/android/server/wm/MiuiFreezeImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDialogShowing(Lcom/android/server/wm/MiuiFreezeImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mDialogShowing:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLoadingDialog(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiLoadingDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mLoadingDialog:Lcom/android/server/wm/MiuiLoadingDialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$mdismissDialog(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->dismissDialog()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 68
    const-string v0, "persist.sys.memfreeze.dialog.delay.time"

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/wm/MiuiFreezeImpl;->DIALOG_DELAY_IMAGE_REC_TIME:I

    .line 70
    const-string v0, "persist.miui.freeze.time"

    const/16 v1, 0x1f40

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/wm/MiuiFreezeImpl;->DELAY_REMOVE_SPLASH_TIME:I

    .line 72
    const-string v0, "persist.sys.memfreeze.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiFreezeImpl;->DEBUG:Z

    .line 76
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/wm/MiuiFreezeImpl;->sInstance:Lcom/android/server/wm/MiuiFreezeImpl;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 86
    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 94
    new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;

    invoke-direct {v1, p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable-IA;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mShowDialogRunnable:Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;

    .line 96
    new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;

    invoke-direct {v1, p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener-IA;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->dialogShowListener:Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->isFreeForm:Z

    .line 110
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->atomicImageRecEnd:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 114
    new-instance v0, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRemoveStartingRunnable:Ljava/lang/Runnable;

    .line 119
    new-instance v0, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRecogImageRunnable:Ljava/lang/Runnable;

    .line 133
    new-instance v0, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda7;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRecogTimeoutRunnable:Ljava/lang/Runnable;

    .line 139
    const-string v0, "MiuiFreezeImpl"

    const-string v1, "MiuiFreezeImpl is Initialized!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    sget-object v0, Lcom/android/server/wm/MiuiFreezeImpl;->sInstance:Lcom/android/server/wm/MiuiFreezeImpl;

    if-nez v0, :cond_0

    .line 141
    sput-object p0, Lcom/android/server/wm/MiuiFreezeImpl;->sInstance:Lcom/android/server/wm/MiuiFreezeImpl;

    .line 143
    :cond_0
    return-void
.end method

.method private createFile(Ljava/io/File;)V
    .locals 6
    .param p1, "file"    # Ljava/io/File;

    .line 432
    const-string v0, "MiuiFreezeImpl"

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 433
    .local v1, "parent":Ljava/io/File;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 434
    const-string v2, "parent not exists"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 436
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    .line 437
    const/16 v2, 0x1fd

    .line 438
    .local v2, "perms":I
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, -0x1

    invoke-static {v3, v2, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 439
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 440
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/os/SELinux;->fileSelabelLookup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 441
    .local v3, "ctx":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 442
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to get SELinux context for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Landroid/os/SELinux;->setFileContext(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 445
    const-string v4, "Failed to set SELinux context"

    invoke-static {v0, v4}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    .end local v1    # "parent":Ljava/io/File;
    .end local v2    # "perms":I
    .end local v3    # "ctx":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 448
    :catch_0
    move-exception v1

    .line 449
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v2, "writeFile packageName: "

    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 451
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private dismissDialog()V
    .locals 2

    .line 335
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 350
    return-void
.end method

.method public static getImpl()Lcom/android/server/wm/MiuiFreezeImpl;
    .locals 1

    .line 146
    sget-object v0, Lcom/android/server/wm/MiuiFreezeImpl;->sInstance:Lcom/android/server/wm/MiuiFreezeImpl;

    return-object v0
.end method

.method private getTopFocusTaskPackageName()Ljava/lang/String;
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 228
    .local v0, "task":Lcom/android/server/wm/Task;
    if-nez v0, :cond_0

    const-string v1, ""

    return-object v1

    .line 229
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private hideDialogIfNeed(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 320
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->needShowLoading(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 321
    return-void

    .line 325
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->dismissDialog()V

    .line 328
    sget-object v0, Lcom/android/server/wm/MiuiFreezeImpl$State;->DISABLE:Lcom/android/server/wm/MiuiFreezeImpl$State;

    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->writeFile(Lcom/android/server/wm/MiuiFreezeImpl$State;)V

    .line 331
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->unRegisterReceiver()V

    .line 332
    return-void
.end method

.method private isPropEnable()Z
    .locals 1

    .line 150
    invoke-static {}, Lcom/android/server/am/MemoryFreezeStub;->getInstance()Lcom/android/server/am/MemoryFreezeStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/am/MemoryFreezeStub;->isEnable()Z

    move-result v0

    return v0
.end method

.method private isPubgSplash(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 4
    .param p1, "activity"    # Lcom/android/server/wm/ActivityRecord;

    .line 381
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-nez v1, :cond_0

    goto :goto_0

    .line 384
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 385
    .local v1, "packageName":Ljava/lang/String;
    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 386
    .local v2, "className":Ljava/lang/String;
    const-string v3, "com.tencent.tmgp.pubgmhd"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "com.epicgames.ue4.SplashActivity"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 382
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v2    # "className":Ljava/lang/String;
    :cond_2
    :goto_0
    return v0
.end method

.method private synthetic lambda$dismissDialog$4()V
    .locals 2

    .line 336
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mLoadingDialog:Lcom/android/server/wm/MiuiLoadingDialog;

    if-nez v0, :cond_0

    .line 337
    return-void

    .line 340
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mDialogShowing:Z

    if-eqz v0, :cond_1

    .line 341
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mLoadingDialog:Lcom/android/server/wm/MiuiLoadingDialog;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiLoadingDialog;->dismiss()V

    .line 342
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mLoadingDialog:Lcom/android/server/wm/MiuiLoadingDialog;

    .line 343
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mDialogShowing:Z

    .line 344
    const-string v0, "MiuiFreezeImpl"

    const-string/jumbo v1, "start dismiss dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 347
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->dialogShowListener:Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->setNeedDismissDialog(Z)V

    .line 349
    :goto_0
    return-void
.end method

.method private synthetic lambda$init$3()V
    .locals 2

    .line 168
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/memgreezer/notify_splash_dismiss_switch"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->createFile(Ljava/io/File;)V

    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 2

    .line 115
    const-string v0, "MiuiFreezeImpl"

    const-string v1, "mRemoveStartingRunnable timeout"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mPackageName:Ljava/lang/String;

    iget v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I

    invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/MiuiFreezeImpl;->finishAndRemoveSplashScreen(Ljava/lang/String;I)V

    .line 117
    return-void
.end method

.method private synthetic lambda$new$1()V
    .locals 4

    .line 120
    const-string/jumbo v0, "start image recognize "

    const-string v1, "MiuiFreezeImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const-string/jumbo v0, "unknown"

    .line 124
    .local v0, "result":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mScene:Lcom/android/server/wm/MiuiRecognizeScene;

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiRecognizeScene;->recognizeScene(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 125
    sget-boolean v2, Lcom/android/server/wm/MiuiFreezeImpl;->DEBUG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "image recog result is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :cond_0
    goto :goto_0

    .line 126
    :catch_0
    move-exception v2

    .line 127
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "image recog exception "

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->processRecgResult(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method private synthetic lambda$new$2()V
    .locals 1

    .line 135
    const-string/jumbo v0, "unknown"

    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->processRecgResult(Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method private synthetic lambda$registerReceiver$5()V
    .locals 8

    .line 391
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 392
    return-void

    .line 394
    :cond_0
    sget-boolean v0, Lcom/android/server/wm/MiuiFreezeImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 395
    const-string v0, "MiuiFreezeImpl"

    const-string/jumbo v1, "start register broadcast"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :cond_1
    new-instance v3, Lcom/android/server/wm/MiuiFreezeImpl$1;

    invoke-direct {v3, p0}, Lcom/android/server/wm/MiuiFreezeImpl$1;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    iput-object v3, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 406
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiContext:Landroid/content/Context;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v0, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v4, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    const/4 v7, 0x2

    invoke-virtual/range {v2 .. v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    .line 409
    return-void
.end method

.method private synthetic lambda$unRegisterReceiver$6()V
    .locals 4

    .line 414
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 415
    return-void

    .line 417
    :cond_0
    sget-boolean v0, Lcom/android/server/wm/MiuiFreezeImpl;->DEBUG:Z

    const-string v1, "MiuiFreezeImpl"

    if-eqz v0, :cond_1

    .line 418
    const-string/jumbo v0, "start unRegister broadcast"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    goto :goto_0

    .line 422
    :catch_0
    move-exception v0

    .line 424
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unregisterReceiver threw exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 427
    return-void
.end method

.method private synthetic lambda$writeFile$7(Lcom/android/server/wm/MiuiFreezeImpl$State;)V
    .locals 6
    .param p1, "state"    # Lcom/android/server/wm/MiuiFreezeImpl$State;

    .line 461
    const-string/jumbo v0, "writeFile close: "

    const-string v1, "MiuiFreezeImpl"

    new-instance v2, Ljava/io/File;

    const-string v3, "/data/system/memgreezer/notify_splash_dismiss_switch"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 462
    .local v2, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 464
    .local v3, "fileWriter":Ljava/io/FileWriter;
    :try_start_0
    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiFreezeImpl;->createFile(Ljava/io/File;)V

    .line 465
    new-instance v4, Ljava/io/FileWriter;

    invoke-direct {v4, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    move-object v3, v4

    .line 466
    sget-object v4, Lcom/android/server/wm/MiuiFreezeImpl$State;->ENABLE:Lcom/android/server/wm/MiuiFreezeImpl$State;

    if-ne p1, v4, :cond_0

    const-string v4, "1"

    goto :goto_0

    :cond_0
    const-string v4, "0"

    :goto_0
    invoke-virtual {v3, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 467
    invoke-virtual {v3}, Ljava/io/FileWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472
    nop

    .line 473
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 477
    :cond_1
    :goto_1
    goto :goto_2

    .line 475
    :catch_0
    move-exception v4

    .line 476
    .local v4, "e":Ljava/io/IOException;
    invoke-static {v1, v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 478
    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 471
    :catchall_0
    move-exception v4

    goto :goto_3

    .line 468
    :catch_1
    move-exception v4

    .line 469
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_2
    const-string/jumbo v5, "writeFile packageName: "

    invoke-static {v1, v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 472
    .end local v4    # "e":Ljava/io/IOException;
    if-eqz v3, :cond_1

    .line 473
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 479
    :goto_2
    return-void

    .line 472
    :goto_3
    if-eqz v3, :cond_2

    .line 473
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    .line 475
    :catch_2
    move-exception v5

    .line 476
    .local v5, "e":Ljava/io/IOException;
    invoke-static {v1, v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 477
    .end local v5    # "e":Ljava/io/IOException;
    :cond_2
    :goto_4
    nop

    .line 478
    :goto_5
    throw v4
.end method

.method private postRemoveSplashRunnable(Ljava/lang/String;I)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 253
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 254
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 255
    .local v1, "app":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->hasThread()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 258
    :cond_0
    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getActivities()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/ActivityRecord;

    .line 259
    .local v3, "record":Lcom/android/server/wm/ActivityRecord;
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAnimationHandler:Landroid/os/Handler;

    iget-object v5, v3, Lcom/android/server/wm/ActivityRecord;->mRemoveSplashRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 260
    nop

    .end local v3    # "record":Lcom/android/server/wm/ActivityRecord;
    goto :goto_0

    .line 261
    .end local v1    # "app":Lcom/android/server/wm/WindowProcessController;
    :cond_1
    monitor-exit v0

    .line 262
    return-void

    .line 256
    .restart local v1    # "app":Lcom/android/server/wm/WindowProcessController;
    :cond_2
    :goto_1
    monitor-exit v0

    return-void

    .line 261
    .end local v1    # "app":Lcom/android/server/wm/WindowProcessController;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private processRecgResult(Ljava/lang/String;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/String;

    .line 213
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->atomicImageRecEnd:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    return-void

    .line 217
    :cond_0
    const-string/jumbo v0, "unknown"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->getTopFocusTaskPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mPackageName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mShowDialogRunnable:Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 220
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mPackageName:Ljava/lang/String;

    iget v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I

    invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/MiuiFreezeImpl;->finishAndRemoveSplashScreen(Ljava/lang/String;I)V

    .line 224
    return-void
.end method

.method private registerReceiver()V
    .locals 2

    .line 390
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 410
    return-void
.end method

.method private removeTimeRunnable(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 240
    sget-boolean v0, Lcom/android/server/wm/MiuiRecognizeScene;->REC_ENABLE:Z

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRecogImageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 244
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRecogTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mShowDialogRunnable:Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 249
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAnimationHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRemoveStartingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 250
    return-void
.end method

.method private unRegisterReceiver()V
    .locals 2

    .line 413
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 428
    return-void
.end method

.method private writeFile(Lcom/android/server/wm/MiuiFreezeImpl$State;)V
    .locals 2
    .param p1, "state"    # Lcom/android/server/wm/MiuiFreezeImpl$State;

    .line 455
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mState:Lcom/android/server/wm/MiuiFreezeImpl$State;

    if-ne v0, p1, :cond_0

    .line 456
    return-void

    .line 458
    :cond_0
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mState:Lcom/android/server/wm/MiuiFreezeImpl$State;

    .line 459
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "write file "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mState:Lcom/android/server/wm/MiuiFreezeImpl$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreezeImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mIoHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiFreezeImpl$State;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 480
    return-void
.end method


# virtual methods
.method public checkFreeForm(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "taskId"    # I

    .line 233
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->isPropEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 234
    :cond_0
    invoke-static {}, Lcom/android/server/am/MemoryFreezeStub;->getInstance()Lcom/android/server/am/MemoryFreezeStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/am/MemoryFreezeStub;->isMemoryFreezeWhiteList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v0, :cond_2

    goto :goto_0

    .line 236
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->isFreeForm:Z

    .line 237
    return-void

    .line 235
    :cond_3
    :goto_0
    return-void
.end method

.method public finishAndRemoveSplashScreen(Ljava/lang/String;I)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 191
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->needShowLoading(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    return-void

    .line 194
    :cond_0
    iget v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I

    if-eq v0, p2, :cond_1

    .line 195
    return-void

    .line 197
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "finishAndRemoveSplashScreen packageName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreezeImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    const-string v0, "MF#finishAndRemoveSplashScreen"

    const-wide/16 v1, 0x20

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 201
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreezeImpl;->removeTimeRunnable(Ljava/lang/String;I)V

    .line 204
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->hideDialogIfNeed(Ljava/lang/String;)V

    .line 207
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreezeImpl;->postRemoveSplashRunnable(Ljava/lang/String;I)V

    .line 209
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 210
    return-void
.end method

.method public init(Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 2
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 159
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->isPropEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 160
    :cond_0
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 161
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/AnimationThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAnimationHandler:Landroid/os/Handler;

    .line 162
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/IoThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mIoHandler:Landroid/os/Handler;

    .line 163
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/UiThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    .line 164
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mBgHandler:Landroid/os/Handler;

    .line 165
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mUiContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiContext:Landroid/content/Context;

    .line 166
    new-instance v0, Lcom/android/server/wm/MiuiRecognizeScene;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/wm/MiuiRecognizeScene;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mScene:Lcom/android/server/wm/MiuiRecognizeScene;

    .line 168
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mIoHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 169
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService;

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 170
    return-void
.end method

.method public isSameProcess(I)Z
    .locals 3
    .param p1, "newPid"    # I

    .line 353
    iget v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurPid:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 354
    .local v0, "result":Z
    :goto_0
    if-nez v0, :cond_1

    .line 355
    const-string v1, "MiuiFreezeImpl"

    const-string v2, "isSameProcess process has changed"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    :cond_1
    return v0
.end method

.method public needShowLoading(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 179
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->isPropEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->isFreeForm:Z

    if-nez v0, :cond_0

    .line 180
    invoke-static {}, Lcom/android/server/am/MemoryFreezeStub;->getInstance()Lcom/android/server/am/MemoryFreezeStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/am/MemoryFreezeStub;->isMemoryFreezeWhiteList(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 179
    :goto_0
    return v0
.end method

.method public reparentPubgGame(Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 2
    .param p1, "from"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "isProcessExists"    # Z

    .line 369
    const-string v0, "MiuiFreezeImpl"

    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_1

    .line 373
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreezeImpl;->needShowLoading(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->isPubgSplash(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 377
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/android/server/wm/ActivityRecord;->needSkipAssignLayer:Z

    .line 378
    return-void

    .line 374
    :cond_2
    :goto_0
    const-string v1, "reparentPubgGame not need show or not from pubg splash, return"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    return-void

    .line 370
    :cond_3
    :goto_1
    const-string v1, "reparentPubgGame AR is null or process not exist, return"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    return-void
.end method

.method public reportAppDied(Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 310
    if-nez p1, :cond_0

    return-void

    .line 311
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mPackageName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 312
    :cond_1
    const-string v0, "MiuiFreezeImpl"

    const-string v1, "reportAppDied start hide dialog when process die"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mPackageName:Ljava/lang/String;

    iget v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I

    invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/MiuiFreezeImpl;->finishAndRemoveSplashScreen(Ljava/lang/String;I)V

    .line 314
    return-void
.end method

.method public showDialogIfNeed(Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;I)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "uid"    # I

    .line 271
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->needShowLoading(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    return-void

    .line 275
    :cond_0
    sget-object v0, Lcom/android/server/wm/MiuiFreezeImpl$State;->ENABLE:Lcom/android/server/wm/MiuiFreezeImpl$State;

    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->writeFile(Lcom/android/server/wm/MiuiFreezeImpl$State;)V

    .line 279
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->registerReceiver()V

    .line 282
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAnimationHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRemoveStartingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 283
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAnimationHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRemoveStartingRunnable:Ljava/lang/Runnable;

    sget v2, Lcom/android/server/wm/MiuiFreezeImpl;->DELAY_REMOVE_SPLASH_TIME:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 286
    sget-boolean v0, Lcom/android/server/wm/MiuiRecognizeScene;->REC_ENABLE:Z

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRecogImageRunnable:Ljava/lang/Runnable;

    sget v2, Lcom/android/server/wm/MiuiFreezeImpl;->DIALOG_DELAY_IMAGE_REC_TIME:I

    int-to-long v3, v2

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 289
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->atomicImageRecEnd:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 290
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mRecogTimeoutRunnable:Ljava/lang/Runnable;

    mul-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mUiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mShowDialogRunnable:Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;

    sget v2, Lcom/android/server/wm/MiuiFreezeImpl;->DIALOG_DELAY_IMAGE_REC_TIME:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 296
    :goto_0
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mPackageName:Ljava/lang/String;

    .line 298
    iput p3, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I

    .line 301
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 302
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1, p1, p3}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 303
    .local v1, "callerApp":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_2

    .line 304
    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v2

    iput v2, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurPid:I

    .line 306
    .end local v1    # "callerApp":Lcom/android/server/wm/WindowProcessController;
    :cond_2
    monitor-exit v0

    .line 307
    return-void

    .line 306
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
