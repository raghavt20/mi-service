.class public Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
.super Ljava/lang/Object;
.source "SplitScreenRecommendTaskInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SplitScreenRecommendTaskInfo"


# instance fields
.field private mPkgName:Ljava/lang/String;

.field private mSwitchTime:J

.field private mTask:Lcom/android/server/wm/Task;

.field private mTaskId:I


# direct methods
.method public constructor <init>(ILjava/lang/String;JLcom/android/server/wm/Task;)V
    .locals 0
    .param p1, "taskId"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "time"    # J
    .param p5, "task"    # Lcom/android/server/wm/Task;

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput p1, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mTaskId:I

    .line 28
    iput-object p2, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mPkgName:Ljava/lang/String;

    .line 29
    iput-wide p3, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mSwitchTime:J

    .line 30
    iput-object p5, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mTask:Lcom/android/server/wm/Task;

    .line 31
    return-void
.end method


# virtual methods
.method public getPkgName()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mPkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getSwitchTime()J
    .locals 2

    .line 15
    iget-wide v0, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mSwitchTime:J

    return-wide v0
.end method

.method public getTask()Lcom/android/server/wm/Task;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mTask:Lcom/android/server/wm/Task;

    return-object v0
.end method

.method public getTaskId()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mTaskId:I

    return v0
.end method
