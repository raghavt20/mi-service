class com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager {
	 /* .source "MiuiOrientationImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiOrientationImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "FullScreenPackageManager" */
} // .end annotation
/* # instance fields */
private android.content.Context mContext;
final java.util.function.Consumer mFullScreenComponentCallback;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
final java.util.function.Consumer mFullScreenPackagelistCallback;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.wm.ActivityTaskManagerServiceImpl mServiceImpl;
final com.android.server.wm.MiuiOrientationImpl this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$-2OoHhogRvqobL0i41I4WIUA9XU ( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->lambda$new$0(Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$H1FGS8fGsb2vvhwUgvQGTN1nM_8 ( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager p0, java.util.concurrent.ConcurrentHashMap p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->lambda$new$1(Ljava/util/concurrent/ConcurrentHashMap;)V */
return;
} // .end method
public static void $r8$lambda$rXRkxX0axJQNIQD9U3XtYaDq5lk ( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager p0, java.util.concurrent.ConcurrentHashMap p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->lambda$new$2(Ljava/util/concurrent/ConcurrentHashMap;)V */
return;
} // .end method
static void -$$Nest$mdump ( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager p0, java.io.PrintWriter p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V */
return;
} // .end method
public com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiOrientationImpl; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "serviceImpl" # Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
/* .line 1047 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1052 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;)V */
this.mFullScreenPackagelistCallback = v0;
/* .line 1059 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;)V */
this.mFullScreenComponentCallback = v0;
/* .line 1048 */
this.mContext = p2;
/* .line 1049 */
this.mServiceImpl = p3;
/* .line 1050 */
return;
} // .end method
private void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 1070 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1072 */
/* .local v0, "innerPrefix":Ljava/lang/String; */
v1 = this.this$0;
v1 = com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmComponentMapBySystem ( v1 );
final String v2 = "] "; // const-string v2, "] "
final String v3 = "["; // const-string v3, "["
/* if-nez v1, :cond_0 */
/* .line 1073 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "SmartRotationConfig(Activity)"; // const-string v4, "SmartRotationConfig(Activity)"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1074 */
v1 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmComponentMapBySystem ( v1 );
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 1075 */
/* .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1076 */
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
com.android.server.wm.MiuiOrientationImpl .fullScreenPolicyToString ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1075 */
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1077 */
} // .end local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
/* .line 1079 */
} // :cond_0
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 1081 */
v1 = this.this$0;
v1 = com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapBySystem ( v1 );
/* if-nez v1, :cond_1 */
/* .line 1082 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "SmartRotationConfig(Package)"; // const-string v4, "SmartRotationConfig(Package)"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1083 */
v1 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapBySystem ( v1 );
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 1084 */
/* .restart local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1085 */
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
com.android.server.wm.MiuiOrientationImpl .fullScreenPolicyToString ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1084 */
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1086 */
} // .end local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
/* .line 1088 */
} // :cond_1
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 1090 */
v1 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.this$0;
v1 = com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v1 );
/* if-nez v1, :cond_2 */
/* .line 1091 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "SmartRotationConfig(UserSetting)"; // const-string v4, "SmartRotationConfig(UserSetting)"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1092 */
v1 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v1 );
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 1093 */
/* .restart local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1094 */
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
com.android.server.wm.MiuiOrientationImpl .fullScreenPolicyToString ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1093 */
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1095 */
} // .end local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
/* .line 1097 */
} // :cond_2
return;
} // .end method
private void lambda$new$0 ( java.lang.String p0, java.lang.String p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .line 1055 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapBySystem ( v0 );
v1 = com.android.server.wm.MiuiOrientationImpl .parseFullScreenPolicy ( p2 );
java.lang.Integer .valueOf ( v1 );
/* .line 1056 */
return;
} // .end method
private void lambda$new$1 ( java.util.concurrent.ConcurrentHashMap p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "map" # Ljava/util/concurrent/ConcurrentHashMap; */
/* .line 1053 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapBySystem ( v0 );
/* .line 1054 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;)V */
(( java.util.concurrent.ConcurrentHashMap ) p1 ).forEach ( v0 ); // invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 1057 */
return;
} // .end method
private void lambda$new$2 ( java.util.concurrent.ConcurrentHashMap p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "map" # Ljava/util/concurrent/ConcurrentHashMap; */
/* .line 1060 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmComponentMapBySystem ( v0 );
/* .line 1061 */
/* new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;)V */
(( java.util.concurrent.ConcurrentHashMap ) p1 ).forEach ( v0 ); // invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 1067 */
return;
} // .end method
/* # virtual methods */
public void clearUserSettings ( ) {
/* .locals 1 */
/* .line 1150 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1151 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v0 );
/* .line 1153 */
} // :cond_0
return;
} // .end method
public Integer getOrientationMode ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1100 */
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) p0 ).getOrientationModeByUserSettings ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationModeByUserSettings(Ljava/lang/String;)I
/* .line 1101 */
/* .local v0, "modeByUserSettings":I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 1102 */
v1 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) p0 ).getOrientationModeBySystem ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationModeBySystem(Ljava/lang/String;)I
} // :cond_0
/* move v1, v0 */
/* .line 1101 */
} // :goto_0
} // .end method
public Integer getOrientationModeBySystem ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1112 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapBySystem ( v0 );
/* .line 1113 */
/* .local v0, "value":Ljava/lang/Object; */
/* instance-of v1, v0, Ljava/lang/Integer; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1114 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
/* .line 1113 */
} // :goto_0
} // .end method
public Integer getOrientationModeByUserSettings ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1118 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1119 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v0 );
/* .line 1120 */
/* .local v0, "value":Ljava/lang/Object; */
/* instance-of v1, v0, Ljava/lang/Integer; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1121 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1124 */
} // .end local v0 # "value":Ljava/lang/Object;
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
} // .end method
public Integer getOrientationPolicy ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1128 */
v0 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) p0 ).getOrientationPolicyByUserSettings ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByUserSettings(Ljava/lang/String;)I
/* .line 1129 */
/* .local v0, "modeByUserSettings":I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 1130 */
v1 = (( com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager ) p0 ).getOrientationPolicyBySystem ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyBySystem(Ljava/lang/String;)I
} // :cond_0
/* move v1, v0 */
/* .line 1129 */
} // :goto_0
} // .end method
public Integer getOrientationPolicyByComponent ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1106 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmComponentMapBySystem ( v0 );
/* .line 1107 */
/* .local v0, "value":Ljava/lang/Object; */
/* instance-of v1, v0, Ljava/lang/Integer; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1108 */
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
/* .line 1107 */
} // :goto_0
} // .end method
public Integer getOrientationPolicyBySystem ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1134 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapBySystem ( v0 );
/* .line 1135 */
/* .local v0, "value":Ljava/lang/Object; */
/* instance-of v1, v0, Ljava/lang/Integer; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1136 */
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
/* .line 1135 */
} // :goto_0
} // .end method
public Integer getOrientationPolicyByUserSettings ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1140 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1141 */
v0 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v0 );
/* .line 1142 */
/* .local v0, "value":Ljava/lang/Object; */
/* instance-of v1, v0, Ljava/lang/Integer; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1143 */
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 1146 */
} // .end local v0 # "value":Ljava/lang/Object;
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
} // .end method
public void setUserSettings ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .line 1156 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1157 */
/* .local v0, "policy":I */
v1 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v1, :cond_0 */
final String v1 = "--remove"; // const-string v1, "--remove"
v1 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1158 */
v1 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v1 );
/* .line 1159 */
} // :cond_0
v1 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v1, :cond_1 */
/* .line 1160 */
v0 = com.android.server.wm.MiuiOrientationImpl .parseFullScreenPolicy ( p2 );
/* .line 1161 */
v1 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v1 );
java.lang.Integer .valueOf ( v0 );
/* .line 1163 */
} // :cond_1
v1 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v1 );
java.lang.Integer .valueOf ( v0 );
/* .line 1165 */
} // :goto_0
v1 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmFileHandler ( v1 );
v2 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmWriteSettingsRunnable ( v2 );
(( android.os.Handler ) v1 ).removeCallbacks ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 1166 */
v1 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmFileHandler ( v1 );
v2 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmWriteSettingsRunnable ( v2 );
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1167 */
return;
} // .end method
public void setUserSettings ( java.lang.String[] p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "pkgNames" # [Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .line 1170 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1171 */
/* .local v0, "policy":I */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 1172 */
/* .local v1, "tmpPolicyDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 1173 */
/* .local v2, "tmpPolicyRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v3 = android.text.TextUtils .isEmpty ( p2 );
int v4 = 0; // const/4 v4, 0x0
/* if-nez v3, :cond_0 */
final String v3 = "--remove"; // const-string v3, "--remove"
v3 = (( java.lang.String ) p2 ).equals ( v3 ); // invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1174 */
/* array-length v3, p1 */
} // :goto_0
/* if-ge v4, v3, :cond_2 */
/* aget-object v5, p1, v4 */
/* .line 1175 */
/* .local v5, "packageName":Ljava/lang/String; */
/* .line 1174 */
} // .end local v5 # "packageName":Ljava/lang/String;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1178 */
} // :cond_0
v3 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v3, :cond_1 */
/* .line 1179 */
v0 = com.android.server.wm.MiuiOrientationImpl .parseFullScreenPolicy ( p2 );
/* .line 1181 */
} // :cond_1
/* array-length v3, p1 */
} // :goto_1
/* if-ge v4, v3, :cond_2 */
/* aget-object v5, p1, v4 */
/* .line 1182 */
/* .restart local v5 # "packageName":Ljava/lang/String; */
java.lang.Integer .valueOf ( v0 );
/* .line 1181 */
} // .end local v5 # "packageName":Ljava/lang/String;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1185 */
} // :cond_2
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Ljava/lang/String; */
/* .line 1186 */
/* .local v4, "name":Ljava/lang/String; */
v5 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v5 );
/* .line 1187 */
} // .end local v4 # "name":Ljava/lang/String;
/* .line 1188 */
} // :cond_3
v3 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmPackagesMapByUserSettings ( v3 );
/* .line 1189 */
v3 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmFileHandler ( v3 );
v4 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmWriteSettingsRunnable ( v4 );
(( android.os.Handler ) v3 ).removeCallbacks ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 1190 */
v3 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmFileHandler ( v3 );
v4 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmWriteSettingsRunnable ( v4 );
(( android.os.Handler ) v3 ).post ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1191 */
return;
} // .end method
