.class Lcom/android/server/wm/OrientationSensorJudgeImpl$TaskStackListenerImpl;
.super Landroid/app/TaskStackListener;
.source "OrientationSensorJudgeImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/OrientationSensorJudgeImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TaskStackListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/OrientationSensorJudgeImpl;

    .line 188
    iput-object p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$TaskStackListenerImpl;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    invoke-direct {p0}, Landroid/app/TaskStackListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTaskStackChanged()V
    .locals 2

    .line 191
    invoke-static {}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$sfgetLOG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const-string v0, "OrientationSensorJudgeImpl"

    const-string v1, "onTaskStackChanged!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$TaskStackListenerImpl;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    invoke-static {v0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$fgetmHandler(Lcom/android/server/wm/OrientationSensorJudgeImpl;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 195
    return-void
.end method
