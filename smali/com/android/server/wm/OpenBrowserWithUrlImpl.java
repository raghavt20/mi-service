public class com.android.server.wm.OpenBrowserWithUrlImpl implements com.android.server.wm.OpenBrowserWithUrlStub {
	 /* .source "OpenBrowserWithUrlImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String IS_SHORTCUT;
	 private static final java.lang.String KEY_CLOUD;
	 private static final java.lang.String KEY_ENABLE;
	 private static final java.lang.String MODULE_CLOUD;
	 private static final java.lang.String TAG;
	 private static java.lang.String mAppName;
	 private static java.util.HashMap mDeviceList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static java.lang.String mDocUrlActivity;
private static java.util.List mUrlActivityList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.lang.String mUrlRedirect;
private static java.lang.String mVersionCode;
/* # instance fields */
private Boolean isSwitch;
private android.content.Context mContext;
private android.os.HandlerThread mExecuteThread;
/* # direct methods */
static void -$$Nest$mupdateCloudData ( com.android.server.wm.OpenBrowserWithUrlImpl p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->updateCloudData(Landroid/content/Context;)V */
return;
} // .end method
static com.android.server.wm.OpenBrowserWithUrlImpl ( ) {
/* .locals 1 */
/* .line 49 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 50 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
return;
} // .end method
public com.android.server.wm.OpenBrowserWithUrlImpl ( ) {
/* .locals 1 */
/* .line 35 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 44 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
return;
} // .end method
private void checkSlow ( Long p0, Long p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "startTime" # J */
/* .param p3, "threshold" # J */
/* .param p5, "where" # Ljava/lang/String; */
/* .line 262 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p1 */
/* .line 263 */
/* .local v0, "took":J */
/* cmp-long v2, v0, p3 */
/* if-lez v2, :cond_0 */
/* .line 265 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Slow operation: "; // const-string v3, "Slow operation: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p5 ); // invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " took "; // const-string v3, " took "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms."; // const-string v3, "ms."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "KamiOpenBrowserWithUrl"; // const-string v3, "KamiOpenBrowserWithUrl"
android.util.Slog .w ( v3,v2 );
/* .line 267 */
} // :cond_0
return;
} // .end method
static void lambda$updateCloudData$0 ( java.lang.String p0 ) { //synthethic
/* .locals 0 */
/* .param p0, "v" # Ljava/lang/String; */
/* .line 127 */
int p0 = 0; // const/4 p0, 0x0
return;
} // .end method
private void updateCloudData ( android.content.Context p0 ) {
/* .locals 16 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 127 */
/* move-object/from16 v1, p0 */
v0 = com.android.server.wm.OpenBrowserWithUrlImpl.mAppName;
v2 = com.android.server.wm.OpenBrowserWithUrlImpl.mVersionCode;
v3 = com.android.server.wm.OpenBrowserWithUrlImpl.mUrlRedirect;
v4 = com.android.server.wm.OpenBrowserWithUrlImpl.mDocUrlActivity;
/* filled-new-array {v0, v2, v3, v4}, [Ljava/lang/String; */
java.util.stream.Stream .of ( v0 );
/* new-instance v2, Lcom/android/server/wm/OpenBrowserWithUrlImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v2}, Lcom/android/server/wm/OpenBrowserWithUrlImpl$$ExternalSyntheticLambda0;-><init>()V */
/* .line 128 */
v0 = com.android.server.wm.OpenBrowserWithUrlImpl.mUrlActivityList;
/* .line 129 */
v0 = com.android.server.wm.OpenBrowserWithUrlImpl.mDeviceList;
(( java.util.HashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
/* .line 130 */
v0 = this.mContext;
/* .line 131 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 130 */
final String v2 = "kamiCloudDataConfig"; // const-string v2, "kamiCloudDataConfig"
final String v3 = "openBrowserWithUrl"; // const-string v3, "openBrowserWithUrl"
final String v4 = ""; // const-string v4, ""
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v2,v3,v4 );
/* .line 132 */
/* .local v2, "data":Ljava/lang/String; */
final String v0 = "ro.product.name"; // const-string v0, "ro.product.name"
android.os.SystemProperties .get ( v0 );
/* .line 134 */
/* .local v3, "deviceName":Ljava/lang/String; */
v0 = android.text.TextUtils .isEmpty ( v2 );
final String v5 = "1"; // const-string v5, "1"
int v6 = 0; // const/4 v6, 0x0
final String v7 = "openbrowserwithurl_enable"; // const-string v7, "openbrowserwithurl_enable"
final String v8 = "0"; // const-string v8, "0"
final String v9 = "KamiOpenBrowserWithUrl"; // const-string v9, "KamiOpenBrowserWithUrl"
/* if-nez v0, :cond_6 */
/* .line 136 */
try { // :try_start_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 137 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
/* const-string/jumbo v10, "switchStatus" */
(( org.json.JSONObject ) v0 ).optString ( v10 ); // invoke-virtual {v0, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 138 */
/* .local v10, "switchStatus":Ljava/lang/String; */
v11 = (( java.lang.String ) v8 ).equals ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v11 != null) { // if-eqz v11, :cond_0
/* .line 139 */
/* iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
/* .line 140 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putString ( v4,v7,v8 );
/* .line 141 */
/* const-string/jumbo v4, "switchStatus is zero, not updating cloud data" */
android.util.Slog .i ( v9,v4 );
/* .line 142 */
return;
/* .line 146 */
} // :cond_0
v11 = this.mContext;
(( android.content.Context ) v11 ).getPackageManager ( ); // invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 147 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String; */
(( android.content.pm.PackageManager ) v11 ).getPackageInfo ( v12, v6 ); // invoke-virtual {v11, v12, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
(( android.content.pm.PackageInfo ) v11 ).getLongVersionCode ( ); // invoke-virtual {v11}, Landroid/content/pm/PackageInfo;->getLongVersionCode()J
/* move-result-wide v11 */
/* .line 146 */
java.lang.String .valueOf ( v11,v12 );
/* .line 148 */
/* .local v11, "localAppVersionCode":Ljava/lang/String; */
/* const-string/jumbo v12, "versionCode" */
(( org.json.JSONObject ) v0 ).optString ( v12 ); // invoke-virtual {v0, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 149 */
v12 = java.lang.Integer .parseInt ( v11 );
v13 = com.android.server.wm.OpenBrowserWithUrlImpl.mVersionCode;
v13 = java.lang.Integer .parseInt ( v13 );
/* if-lt v12, v13, :cond_1 */
/* .line 150 */
/* iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
/* .line 151 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putString ( v4,v7,v8 );
/* .line 152 */
final String v4 = "app versionCode is greater than cloudData, not updating cloud data"; // const-string v4, "app versionCode is greater than cloudData, not updating cloud data"
android.util.Slog .i ( v9,v4 );
/* .line 153 */
return;
/* .line 156 */
} // :cond_1
final String v12 = "device"; // const-string v12, "device"
(( org.json.JSONObject ) v0 ).optJSONObject ( v12 ); // invoke-virtual {v0, v12}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 157 */
/* .local v12, "deviceArray":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v12 ).keys ( ); // invoke-virtual {v12}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 158 */
/* .local v13, "keys_device":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v14 = } // :goto_0
if ( v14 != null) { // if-eqz v14, :cond_2
/* .line 159 */
/* check-cast v14, Ljava/lang/String; */
/* .line 160 */
/* .local v14, "key":Ljava/lang/String; */
(( org.json.JSONObject ) v12 ).getString ( v14 ); // invoke-virtual {v12, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 161 */
/* .local v15, "value":Ljava/lang/String; */
v4 = com.android.server.wm.OpenBrowserWithUrlImpl.mDeviceList;
(( java.util.HashMap ) v4 ).put ( v14, v15 ); // invoke-virtual {v4, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 162 */
/* nop */
} // .end local v14 # "key":Ljava/lang/String;
} // .end local v15 # "value":Ljava/lang/String;
/* .line 164 */
} // :cond_2
v4 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isTablet()Z */
if ( v4 != null) { // if-eqz v4, :cond_3
v4 = com.android.server.wm.OpenBrowserWithUrlImpl.mDeviceList;
(( java.util.HashMap ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
v4 = (( java.lang.String ) v8 ).equals ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 165 */
/* iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
/* .line 166 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putString ( v4,v7,v8 );
/* .line 167 */
/* const-string/jumbo v4, "the current device is in the blacklist, not updating cloud data" */
android.util.Slog .i ( v9,v4 );
/* .line 168 */
return;
/* .line 171 */
} // :cond_3
v4 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isTablet()Z */
/* if-nez v4, :cond_4 */
v4 = com.android.server.wm.OpenBrowserWithUrlImpl.mDeviceList;
(( java.util.HashMap ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
v4 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_4 */
/* .line 172 */
/* iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
/* .line 173 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putString ( v4,v7,v8 );
/* .line 174 */
/* const-string/jumbo v4, "the current device is not in the whitelist, not updating cloud data" */
android.util.Slog .i ( v9,v4 );
/* .line 175 */
return;
/* .line 177 */
} // :cond_4
int v4 = 1; // const/4 v4, 0x1
/* iput-boolean v4, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
/* .line 178 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putString ( v4,v7,v5 );
/* .line 179 */
final String v4 = "appName"; // const-string v4, "appName"
(( org.json.JSONObject ) v0 ).optString ( v4 ); // invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 180 */
/* const-string/jumbo v4, "urlRedirect" */
(( org.json.JSONObject ) v0 ).optString ( v4 ); // invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 181 */
final String v4 = "docUrlActivity"; // const-string v4, "docUrlActivity"
(( org.json.JSONObject ) v0 ).optString ( v4 ); // invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 182 */
/* const-string/jumbo v4, "urlActivity" */
(( org.json.JSONObject ) v0 ).optJSONObject ( v4 ); // invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 183 */
/* .local v4, "urlActivity":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v4 ).keys ( ); // invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
/* .line 184 */
/* .local v5, "keys_url":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 185 */
/* check-cast v6, Ljava/lang/String; */
/* .line 186 */
/* .local v6, "key":Ljava/lang/String; */
(( org.json.JSONObject ) v4 ).getString ( v6 ); // invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 187 */
/* .local v7, "value":Ljava/lang/String; */
v8 = com.android.server.wm.OpenBrowserWithUrlImpl.mUrlActivityList;
/* .line 188 */
/* nop */
} // .end local v6 # "key":Ljava/lang/String;
} // .end local v7 # "value":Ljava/lang/String;
/* .line 189 */
} // :cond_5
/* const-string/jumbo v6, "updateCloudData success" */
android.util.Slog .i ( v9,v6 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 192 */
/* nop */
} // .end local v0 # "jsonObject":Lorg/json/JSONObject;
} // .end local v4 # "urlActivity":Lorg/json/JSONObject;
} // .end local v5 # "keys_url":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // .end local v10 # "switchStatus":Ljava/lang/String;
} // .end local v11 # "localAppVersionCode":Ljava/lang/String;
} // .end local v12 # "deviceArray":Lorg/json/JSONObject;
} // .end local v13 # "keys_device":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
/* .line 190 */
/* :catch_0 */
/* move-exception v0 */
/* .line 191 */
/* .local v0, "e":Ljava/lang/Exception; */
/* const-string/jumbo v4, "updateCloudData error :" */
android.util.Slog .e ( v9,v4,v0 );
/* .line 192 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 195 */
} // :cond_6
try { // :try_start_1
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->initializeParam()V */
/* .line 196 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isTablet()Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 197 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
/* .line 198 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putString ( v0,v7,v5 );
/* .line 199 */
final String v0 = "cloud data is null and device is tablet, open by default"; // const-string v0, "cloud data is null and device is tablet, open by default"
android.util.Slog .i ( v9,v0 );
/* .line 201 */
} // :cond_7
/* iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
/* .line 202 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putString ( v0,v7,v8 );
/* .line 203 */
final String v0 = "cloud data is null and device is not tablet, off by default"; // const-string v0, "cloud data is null and device is not tablet, off by default"
android.util.Slog .i ( v9,v0 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 207 */
} // :goto_2
/* .line 205 */
/* :catch_1 */
/* move-exception v0 */
/* .line 206 */
/* .local v0, "ex":Ljava/lang/Exception; */
final String v4 = "reset switch error :"; // const-string v4, "reset switch error :"
android.util.Slog .e ( v9,v4,v0 );
/* .line 209 */
} // .end local v0 # "ex":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
/* # virtual methods */
public Boolean getSwitch ( ) {
/* .locals 1 */
/* .line 103 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 53 */
final String v0 = "KamiOpenBrowserWithUrl"; // const-string v0, "KamiOpenBrowserWithUrl"
/* if-nez p1, :cond_0 */
/* .line 54 */
return;
/* .line 57 */
} // :cond_0
try { // :try_start_0
(( com.android.server.wm.OpenBrowserWithUrlImpl ) p0 ).initializeParam ( ); // invoke-virtual {p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->initializeParam()V
/* .line 58 */
this.mContext = p1;
/* .line 59 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "kami_open_browser_thread"; // const-string v2, "kami_open_browser_thread"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mExecuteThread = v1;
/* .line 60 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 61 */
/* new-instance v1, Landroid/os/Handler; */
v2 = this.mExecuteThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 62 */
/* .local v1, "handler":Landroid/os/Handler; */
/* new-instance v2, Lcom/android/server/wm/OpenBrowserWithUrlImpl$1; */
/* invoke-direct {v2, p0, v1, p1}, Lcom/android/server/wm/OpenBrowserWithUrlImpl$1;-><init>(Lcom/android/server/wm/OpenBrowserWithUrlImpl;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 68 */
/* .local v2, "kamiCloudOberver":Landroid/database/ContentObserver; */
/* new-instance v3, Landroid/os/Handler; */
android.os.Looper .getMainLooper ( );
/* invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* new-instance v4, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2; */
/* invoke-direct {v4, p0, p1, v2, v1}, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;-><init>(Lcom/android/server/wm/OpenBrowserWithUrlImpl;Landroid/content/Context;Landroid/database/ContentObserver;Landroid/os/Handler;)V */
(( android.os.Handler ) v3 ).post ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 82 */
final String v3 = "init success"; // const-string v3, "init success"
android.util.Slog .i ( v0,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 85 */
/* nop */
} // .end local v1 # "handler":Landroid/os/Handler;
} // .end local v2 # "kamiCloudOberver":Landroid/database/ContentObserver;
/* .line 83 */
/* :catch_0 */
/* move-exception v1 */
/* .line 84 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "init error :"; // const-string v2, "init error :"
android.util.Slog .e ( v0,v2,v1 );
/* .line 86 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void initializeParam ( ) {
/* .locals 4 */
/* .line 89 */
final String v0 = "com.ss.android.lark.kami"; // const-string v0, "com.ss.android.lark.kami"
/* .line 90 */
final String v0 = "2147483647"; // const-string v0, "2147483647"
/* .line 91 */
final String v0 = "https://www.f.mioffice.cn/suite/passport/inbound/redirect"; // const-string v0, "https://www.f.mioffice.cn/suite/passport/inbound/redirect"
/* .line 92 */
v0 = com.android.server.wm.OpenBrowserWithUrlImpl.mUrlActivityList;
final String v1 = "com.ss.android.lark.kami/com.bytedance.ee.bear.document.DocActivity"; // const-string v1, "com.ss.android.lark.kami/com.bytedance.ee.bear.document.DocActivity"
final String v2 = "com.ss.android.lark.kami/com.bytedance.ee.bear.wikiv2.WikiActivity"; // const-string v2, "com.ss.android.lark.kami/com.bytedance.ee.bear.wikiv2.WikiActivity"
final String v3 = "com.ss.android.lark.kami/com.bytedance.lark.webview.container.impl.WebContainerMainProcessActivity"; // const-string v3, "com.ss.android.lark.kami/com.bytedance.lark.webview.container.impl.WebContainerMainProcessActivity"
/* filled-new-array {v3, v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* .line 97 */
final String v0 = "com.ss.android.lark.kami/com.bytedance.ee.bear.basesdk.DocRouteActivity"; // const-string v0, "com.ss.android.lark.kami/com.bytedance.ee.bear.basesdk.DocRouteActivity"
/* .line 98 */
final String v0 = "KamiOpenBrowserWithUrl"; // const-string v0, "KamiOpenBrowserWithUrl"
final String v1 = "initializeParam success"; // const-string v1, "initializeParam success"
android.util.Slog .i ( v0,v1 );
/* .line 99 */
return;
} // .end method
public Boolean isTablet ( ) {
/* .locals 2 */
/* .line 257 */
final String v0 = "ro.build.characteristics"; // const-string v0, "ro.build.characteristics"
android.os.SystemProperties .get ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 258 */
android.os.SystemProperties .get ( v0 );
/* const-string/jumbo v1, "tablet" */
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 257 */
} // :goto_0
} // .end method
public android.content.Intent openBrowserWithUrl ( android.content.Intent p0, java.lang.String p1 ) {
/* .locals 12 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .line 213 */
final String v0 = "doc_url"; // const-string v0, "doc_url"
/* const-string/jumbo v1, "url" */
final String v2 = "KamiOpenBrowserWithUrl"; // const-string v2, "KamiOpenBrowserWithUrl"
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* .line 214 */
/* .local v4, "start":J */
v3 = this.mContext;
(( android.content.Context ) v3 ).getPackageManager ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 215 */
int v6 = 0; // const/4 v6, 0x0
(( android.content.pm.PackageManager ) v3 ).getPackageInfo ( p2, v6 ); // invoke-virtual {v3, p2, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
(( android.content.pm.PackageInfo ) v3 ).getLongVersionCode ( ); // invoke-virtual {v3}, Landroid/content/pm/PackageInfo;->getLongVersionCode()J
/* move-result-wide v6 */
/* .line 214 */
java.lang.String .valueOf ( v6,v7 );
/* move-object v9, v3 */
/* .line 216 */
/* .local v9, "localAppVersionCode":Ljava/lang/String; */
/* iget-boolean v3, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z */
if ( v3 != null) { // if-eqz v3, :cond_7
v3 = this.mContext;
if ( v3 != null) { // if-eqz v3, :cond_7
v3 = com.android.server.wm.OpenBrowserWithUrlImpl.mAppName;
v3 = (( java.lang.String ) v3 ).equals ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 217 */
v3 = java.lang.Integer .parseInt ( v9 );
v6 = com.android.server.wm.OpenBrowserWithUrlImpl.mVersionCode;
v6 = java.lang.Integer .parseInt ( v6 );
/* if-lt v3, v6, :cond_0 */
/* goto/16 :goto_2 */
/* .line 221 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .line 222 */
/* .local v3, "url":Ljava/lang/String; */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v6 != null) { // if-eqz v6, :cond_6
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 223 */
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( android.os.Bundle ) v6 ).get ( v1 ); // invoke-virtual {v6, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;
if ( v6 != null) { // if-eqz v6, :cond_1
v6 = com.android.server.wm.OpenBrowserWithUrlImpl.mUrlActivityList;
/* .line 224 */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
v6 = (( android.content.ComponentName ) v7 ).flattenToShortString ( ); // invoke-virtual {v7}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 225 */
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( android.os.Bundle ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;
java.lang.String .valueOf ( v0 );
/* move-object v3, v0 */
/* .line 226 */
} // :cond_1
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( android.os.Bundle ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = com.android.server.wm.OpenBrowserWithUrlImpl.mDocUrlActivity;
/* .line 227 */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v6 ).flattenToShortString ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).equals ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 228 */
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( android.os.Bundle ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;
java.lang.String .valueOf ( v0 );
/* move-object v3, v0 */
/* .line 231 */
} // :cond_2
/* move-object v0, v3 */
} // .end local v3 # "url":Ljava/lang/String;
/* .local v0, "url":Ljava/lang/String; */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_5
v1 = com.android.server.wm.OpenBrowserWithUrlImpl.mUrlRedirect;
v1 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 234 */
} // :cond_3
/* new-instance v1, Landroid/content/Intent; */
final String v3 = "android.intent.action.VIEW"; // const-string v3, "android.intent.action.VIEW"
android.net.Uri .parse ( v0 );
/* invoke-direct {v1, v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V */
/* .line 235 */
/* .local v1, "browserIntent":Landroid/content/Intent; */
v3 = this.mContext;
(( android.content.Context ) v3 ).getPackageManager ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 236 */
/* const/high16 v6, 0x10000 */
(( android.content.pm.PackageManager ) v3 ).resolveActivity ( v1, v6 ); // invoke-virtual {v3, v1, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
/* move-object v10, v3 */
/* .line 237 */
/* .local v10, "resolveInfo":Landroid/content/pm/ResolveInfo; */
/* if-nez v10, :cond_4 */
/* .line 238 */
/* .line 241 */
} // :cond_4
/* const/high16 v3, 0x10000000 */
(( android.content.Intent ) v1 ).addFlags ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 242 */
v3 = this.activityInfo;
v3 = this.packageName;
(( android.content.Intent ) v1 ).setPackage ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 243 */
/* new-instance v3, Landroid/os/Bundle; */
/* invoke-direct {v3}, Landroid/os/Bundle;-><init>()V */
/* move-object v11, v3 */
/* .line 244 */
/* .local v11, "bundle":Landroid/os/Bundle; */
final String v3 = "isShortCut"; // const-string v3, "isShortCut"
int v6 = 1; // const/4 v6, 0x1
(( android.os.Bundle ) v11 ).putBoolean ( v3, v6 ); // invoke-virtual {v11, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 245 */
(( android.content.Intent ) v1 ).putExtras ( v11 ); // invoke-virtual {v1, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 246 */
/* const-wide/16 v6, 0x32 */
final String v8 = "check openBrowserWithUrl start time"; // const-string v8, "check openBrowserWithUrl start time"
/* move-object v3, p0 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->checkSlow(JJLjava/lang/String;)V */
/* .line 247 */
final String v3 = "openBrowserWithUrl success"; // const-string v3, "openBrowserWithUrl success"
android.util.Slog .i ( v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 248 */
/* .line 232 */
} // .end local v1 # "browserIntent":Landroid/content/Intent;
} // .end local v10 # "resolveInfo":Landroid/content/pm/ResolveInfo;
} // .end local v11 # "bundle":Landroid/os/Bundle;
} // :cond_5
} // :goto_1
/* .line 252 */
} // .end local v0 # "url":Ljava/lang/String;
} // .end local v4 # "start":J
} // .end local v9 # "localAppVersionCode":Ljava/lang/String;
} // :cond_6
/* .line 218 */
/* .restart local v4 # "start":J */
/* .restart local v9 # "localAppVersionCode":Ljava/lang/String; */
} // :cond_7
} // :goto_2
/* .line 250 */
} // .end local v4 # "start":J
} // .end local v9 # "localAppVersionCode":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 251 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "openBrowserWithUrl error :"; // const-string v1, "openBrowserWithUrl error :"
android.util.Slog .e ( v2,v1,v0 );
/* .line 253 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
} // .end method
