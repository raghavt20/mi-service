.class public Lcom/android/server/wm/PackageConfigurationController;
.super Ljava/lang/Thread;
.source "PackageConfigurationController.java"


# static fields
.field private static final COMMAND_OPTION_FORCE_UPDATE:Ljava/lang/String; = "ForceUpdate"

.field private static final COMMAND_OPTION_POLICY_RESET:Ljava/lang/String; = "PolicyReset"

.field private static final PACKAGE_CONFIGURATION_COMMAND:Ljava/lang/String; = "-packageconfiguration"

.field private static final PREFIX_ACTION_POLICY_UPDATED:Ljava/lang/String; = "sec.app.policy.UPDATE."

.field private static final SET_POLICY_DISABLED_COMMAND:Ljava/lang/String; = "-setPolicyDisabled"

.field private static final TAG:Ljava/lang/String; = "PackageConfigurationController"


# instance fields
.field final mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

.field private final mContext:Landroid/content/Context;

.field private final mLogs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPolicyDisabled:Z

.field private final mPolicyImplMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/wm/PolicyImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final mPolicyRequestQueue:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTmpPolicyRequestQueue:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$ILQ1kQlQ9cml7xWQF4no8p7i5hY(Lcom/android/server/wm/PackageConfigurationController;Ljava/lang/String;Lcom/android/server/wm/PolicyImpl;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/PackageConfigurationController;->lambda$initialize$0(Ljava/lang/String;Lcom/android/server/wm/PolicyImpl;)V

    return-void
.end method

.method public static synthetic $r8$lambda$dRw9l6JiR3mL2yOX_InG527pou4(Lcom/android/server/wm/PackageConfigurationController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/PackageConfigurationController;->lambda$scheduleUpdatePolicyItem$3()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 1
    .param p1, "atmService"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 41
    const-string v0, "PackageConfigurationUpdateThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mLogs:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyImplMap:Ljava/util/Map;

    .line 35
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyRequestQueue:Ljava/util/Set;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mTmpPolicyRequestQueue:Ljava/util/Set;

    .line 42
    iput-object p1, p0, Lcom/android/server/wm/PackageConfigurationController;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 43
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method

.method private initialize()V
    .locals 3

    .line 47
    const-string v0, "PackageConfigurationController"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyImplMap:Ljava/util/Map;

    new-instance v1, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/PackageConfigurationController;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 52
    const/4 v0, 0x0

    const-wide/32 v1, 0x493e0

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/wm/PackageConfigurationController;->scheduleUpdatePolicyItem(Ljava/lang/String;J)V

    .line 53
    return-void
.end method

.method static synthetic lambda$executeShellCommand$1(Ljava/io/PrintWriter;Ljava/lang/String;Lcom/android/server/wm/PolicyImpl;)V
    .locals 2
    .param p0, "pw"    # Ljava/io/PrintWriter;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "policy"    # Lcom/android/server/wm/PolicyImpl;

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/android/server/wm/PolicyImpl;->updatePolicyItem(Z)V

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " update forced."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method static synthetic lambda$executeShellCommand$2(Ljava/lang/String;Lcom/android/server/wm/PolicyImpl;)V
    .locals 0
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "policy"    # Lcom/android/server/wm/PolicyImpl;

    .line 87
    invoke-virtual {p1}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V

    .line 88
    return-void
.end method

.method private synthetic lambda$initialize$0(Ljava/lang/String;Lcom/android/server/wm/PolicyImpl;)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "policy"    # Lcom/android/server/wm/PolicyImpl;

    .line 49
    invoke-virtual {p2}, Lcom/android/server/wm/PolicyImpl;->init()V

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "sec.app.policy.UPDATE."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/wm/PackageConfigurationController;->scheduleUpdatePolicyItem(Ljava/lang/String;J)V

    .line 51
    return-void
.end method

.method private synthetic lambda$scheduleUpdatePolicyItem$3()V
    .locals 2

    .line 137
    monitor-enter p0

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyRequestQueue:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/wm/PackageConfigurationController;->mTmpPolicyRequestQueue:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 140
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mTmpPolicyRequestQueue:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 141
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    goto :goto_0

    .line 144
    :catchall_0
    move-exception v0

    goto :goto_1

    .line 142
    :catch_0
    move-exception v0

    .line 144
    :goto_0
    :try_start_1
    monitor-exit p0

    .line 145
    return-void

    .line 144
    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
    .locals 6
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 56
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 57
    const-string v0, "PackageConfigurationController"

    const-string v2, "MiuiAppSizeCompatMode not enabled"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    return v1

    .line 61
    :cond_0
    monitor-enter p0

    .line 62
    const/4 v0, 0x0

    .line 63
    .local v0, "number":I
    :try_start_0
    const-string v2, "-packageconfiguration"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    .line 64
    array-length v2, p2

    if-ne v2, v3, :cond_1

    .line 65
    invoke-virtual {p3}, Ljava/io/PrintWriter;->println()V

    .line 66
    const-string v2, "ForceUpdate"

    aget-object v1, p2, v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    const-string v1, "Started the update."

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 68
    iget-object v1, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyImplMap:Ljava/util/Map;

    new-instance v2, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda2;

    invoke-direct {v2, p3}, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda2;-><init>(Ljava/io/PrintWriter;)V

    invoke-interface {v1, v2}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 75
    :cond_1
    monitor-exit p0

    return v3

    .line 78
    :cond_2
    const-string v2, "-setPolicyDisabled"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 79
    array-length v2, p2

    if-ne v2, v3, :cond_4

    .line 80
    aget-object v2, p2, v1

    if-nez v2, :cond_3

    .line 81
    monitor-exit p0

    return v3

    .line 83
    :cond_3
    aget-object v1, p2, v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 84
    .local v1, "newPolicyDisabled":Z
    iget-boolean v2, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyDisabled:Z

    if-eq v2, v1, :cond_4

    .line 85
    iput-boolean v1, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyDisabled:Z

    .line 86
    iget-object v2, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyImplMap:Ljava/util/Map;

    new-instance v4, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda3;

    invoke-direct {v4}, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda3;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 92
    .end local v1    # "newPolicyDisabled":Z
    :cond_4
    monitor-exit p0

    return v3

    .line 95
    :cond_5
    iget-object v2, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyImplMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 96
    .local v4, "item":Ljava/lang/Object;
    move-object v5, v4

    check-cast v5, Lcom/android/server/wm/PolicyImpl;

    invoke-virtual {v5, p1, p2, p3}, Lcom/android/server/wm/PolicyImpl;->executeShellCommandLocked(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 97
    monitor-exit p0

    return v3

    .line 99
    .end local v4    # "item":Ljava/lang/Object;
    :cond_6
    goto :goto_0

    .line 100
    :cond_7
    monitor-exit p0

    return v1

    .line 101
    .end local v0    # "number":I
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method registerPolicy(Lcom/android/server/wm/PolicyImpl;)V
    .locals 2
    .param p1, "impl"    # Lcom/android/server/wm/PolicyImpl;

    .line 105
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyImplMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/server/wm/PolicyImpl;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mTmpPolicyRequestQueue:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/android/server/wm/PolicyImpl;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 107
    return-void
.end method

.method public run()V
    .locals 4

    .line 111
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 112
    invoke-direct {p0}, Lcom/android/server/wm/PackageConfigurationController;->initialize()V

    .line 113
    monitor-enter p0

    .line 116
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyRequestQueue:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 117
    .local v1, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyImplMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/PolicyImpl;

    .line 118
    .local v2, "policyImpl":Lcom/android/server/wm/PolicyImpl;
    if-eqz v2, :cond_0

    .line 119
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/server/wm/PolicyImpl;->updatePolicyItem(Z)V

    .line 121
    .end local v1    # "str":Ljava/lang/String;
    .end local v2    # "policyImpl":Lcom/android/server/wm/PolicyImpl;
    :cond_0
    goto :goto_1

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyRequestQueue:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 123
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 126
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_2
    goto :goto_0

    :goto_3
    nop

    .end local p0    # "this":Lcom/android/server/wm/PackageConfigurationController;
    :try_start_2
    throw v0

    .line 129
    .restart local p0    # "this":Lcom/android/server/wm/PackageConfigurationController;
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method scheduleUpdatePolicyItem(Ljava/lang/String;J)V
    .locals 2
    .param p1, "policyRequest"    # Ljava/lang/String;
    .param p2, "delayMillis"    # J

    .line 133
    if-eqz p1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mTmpPolicyRequestQueue:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mH:Lcom/android/server/wm/ActivityTaskManagerService$H;

    new-instance v1, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/PackageConfigurationController;)V

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/wm/ActivityTaskManagerService$H;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 146
    return-void
.end method

.method startThread()V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyImplMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    return-void

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/PackageConfigurationController;->start()V

    .line 153
    return-void
.end method
