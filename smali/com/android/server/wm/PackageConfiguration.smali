.class final Lcom/android/server/wm/PackageConfiguration;
.super Ljava/lang/Object;
.source "PackageConfiguration.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x2f0880a250L


# instance fields
.field final mName:Ljava/lang/String;

.field private final mPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private transient mTmpPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "configurationName"    # Ljava/lang/String;

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PackageConfiguration;->mPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 13
    iput-object p1, p0, Lcom/android/server/wm/PackageConfiguration;->mName:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method getPolicyDataMap()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/android/server/wm/PackageConfiguration;->mPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method updateCompleted()V
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/android/server/wm/PackageConfiguration;->mPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 22
    iget-object v0, p0, Lcom/android/server/wm/PackageConfiguration;->mPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Lcom/android/server/wm/PackageConfiguration;->mTmpPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putAll(Ljava/util/Map;)V

    .line 23
    return-void
.end method

.method updateFromScpm(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 26
    if-nez p2, :cond_0

    .line 27
    const-string p2, ""

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/PackageConfiguration;->mTmpPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method

.method updatePrepared()V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/android/server/wm/PackageConfiguration;->mTmpPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PackageConfiguration;->mTmpPolicyDataMap:Ljava/util/concurrent/ConcurrentHashMap;

    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 38
    :goto_0
    return-void
.end method
