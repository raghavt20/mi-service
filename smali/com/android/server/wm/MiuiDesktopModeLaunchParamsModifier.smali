.class public Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;
.super Ljava/lang/Object;
.source "MiuiDesktopModeLaunchParamsModifier.java"

# interfaces
.implements Lcom/android/server/wm/LaunchParamsController$LaunchParamsModifier;


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "MiuiDesktopModeLaunchParamsModifier"


# instance fields
.field private mLogBuilder:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private varargs appendLog(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .line 151
    iget-object v0, p0, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->mLogBuilder:Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    return-void
.end method

.method private calculate(Lcom/android/server/wm/Task;Landroid/content/pm/ActivityInfo$WindowLayout;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/app/ActivityOptions;Lcom/android/server/wm/ActivityStarter$Request;ILcom/android/server/wm/LaunchParamsController$LaunchParams;Lcom/android/server/wm/LaunchParamsController$LaunchParams;)I
    .locals 16
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "layout"    # Landroid/content/pm/ActivityInfo$WindowLayout;
    .param p3, "activity"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "source"    # Lcom/android/server/wm/ActivityRecord;
    .param p5, "options"    # Landroid/app/ActivityOptions;
    .param p6, "request"    # Lcom/android/server/wm/ActivityStarter$Request;
    .param p7, "phase"    # I
    .param p8, "currentParams"    # Lcom/android/server/wm/LaunchParamsController$LaunchParams;
    .param p9, "outParams"    # Lcom/android/server/wm/LaunchParamsController$LaunchParams;

    .line 68
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p5

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    const/4 v0, 0x0

    if-nez v2, :cond_0

    .line 69
    const-string/jumbo v6, "task null, skipping"

    new-array v7, v0, [Ljava/lang/Object;

    invoke-direct {v1, v6, v7}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    return v0

    .line 72
    :cond_0
    iget-object v6, v2, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v6, v6, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 73
    .local v6, "context":Landroid/content/Context;
    invoke-static {v6}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v7

    .line 74
    .local v7, "isDesktopModeActive":Z
    if-nez v7, :cond_1

    .line 75
    return v0

    .line 77
    :cond_1
    const/4 v8, 0x3

    move/from16 v9, p7

    if-eq v9, v8, :cond_2

    .line 78
    const-string v8, "not in bounds phase, skipping"

    new-array v10, v0, [Ljava/lang/Object;

    invoke-direct {v1, v8, v10}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    return v0

    .line 81
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->isActivityTypeStandard()Z

    move-result v8

    if-nez v8, :cond_3

    .line 82
    const-string v8, "not standard activity type, skipping"

    new-array v10, v0, [Ljava/lang/Object;

    invoke-direct {v1, v8, v10}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    return v0

    .line 85
    :cond_3
    iget-object v8, v4, Lcom/android/server/wm/LaunchParamsController$LaunchParams;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    .line 86
    const-string v8, "currentParams has bounds set, not overriding"

    new-array v10, v0, [Ljava/lang/Object;

    invoke-direct {v1, v8, v10}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    return v0

    .line 91
    :cond_4
    invoke-virtual {v5, v4}, Lcom/android/server/wm/LaunchParamsController$LaunchParams;->set(Lcom/android/server/wm/LaunchParamsController$LaunchParams;)V

    .line 107
    const/4 v8, 0x1

    if-eqz v7, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->isVisible()Z

    move-result v10

    if-nez v10, :cond_a

    .line 108
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v10

    const/4 v11, 0x5

    if-nez v10, :cond_5

    if-eqz v3, :cond_a

    .line 109
    invoke-virtual/range {p5 .. p5}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v10

    if-ne v10, v11, :cond_a

    .line 110
    :cond_5
    iget-object v10, v2, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    if-eqz v10, :cond_6

    iget-object v10, v2, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 111
    :cond_6
    iget-object v10, v2, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v10, :cond_7

    iget-object v10, v2, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 112
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v10

    if-eqz v10, :cond_8

    .line 113
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v10

    iget-object v10, v10, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_8
    const-string/jumbo v10, "unknown"

    :goto_0
    nop

    .line 114
    .local v10, "packageName":Ljava/lang/String;
    invoke-static {v6, v10, v8, v0, v7}, Landroid/util/MiuiMultiWindowUtils;->getActivityOptions(Landroid/content/Context;Ljava/lang/String;ZZZ)Landroid/app/ActivityOptions;

    move-result-object v12

    .line 117
    .local v12, "freeformLaunchOption":Landroid/app/ActivityOptions;
    if-eqz v12, :cond_a

    .line 118
    iget-object v13, v5, Lcom/android/server/wm/LaunchParamsController$LaunchParams;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v12}, Landroid/app/ActivityOptions;->getLaunchBounds()Landroid/graphics/Rect;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 119
    iput v11, v5, Lcom/android/server/wm/LaunchParamsController$LaunchParams;->mWindowingMode:I

    .line 121
    if-eqz v3, :cond_9

    .line 122
    :try_start_0
    const-string v13, "getActivityOptionsInjector"

    const/4 v14, 0x0

    invoke-static {v3, v13, v14}, Landroid/util/MiuiMultiWindowUtils;->isMethodExist(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v13

    .line 124
    .local v13, "method":Ljava/lang/reflect/Method;
    if-eqz v13, :cond_9

    .line 125
    new-array v14, v0, [Ljava/lang/Object;

    invoke-virtual {v13, v3, v14}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    const-string/jumbo v15, "setFreeformScale"

    new-array v11, v8, [Ljava/lang/Object;

    new-array v8, v0, [Ljava/lang/Object;

    .line 126
    invoke-virtual {v13, v12, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    const-string v0, "getFreeformScale"

    const/4 v2, 0x0

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v8, v0, v4}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v11, v2

    .line 125
    invoke-static {v14, v15, v11}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {v13, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v2, "setNormalFreeForm"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-array v11, v4, [Ljava/lang/Object;

    .line 128
    invoke-virtual {v13, v12, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    const-string v14, "isNormalFreeForm"

    new-array v15, v4, [Ljava/lang/Object;

    invoke-static {v11, v14, v15}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v11, 0x0

    aput-object v4, v8, v11

    .line 127
    invoke-static {v0, v2, v8}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    invoke-virtual {v12}, Landroid/app/ActivityOptions;->getLaunchBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/ActivityOptions;->setLaunchBounds(Landroid/graphics/Rect;)Landroid/app/ActivityOptions;

    .line 130
    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 133
    .end local v13    # "method":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    goto :goto_2

    .line 134
    :cond_9
    :goto_1
    nop

    .line 138
    .end local v10    # "packageName":Ljava/lang/String;
    .end local v12    # "freeformLaunchOption":Landroid/app/ActivityOptions;
    :cond_a
    :goto_2
    iget-object v0, v5, Lcom/android/server/wm/LaunchParamsController$LaunchParams;->mBounds:Landroid/graphics/Rect;

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v2, "setting desktop mode task bounds to %s"

    invoke-direct {v1, v2, v0}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    const/4 v2, 0x1

    return v2
.end method

.method private initLogBuilder(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;)V
    .locals 3
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "activity"    # Lcom/android/server/wm/ActivityRecord;

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MiuiDesktopModeLaunchParamsModifier: task="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " activity="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->mLogBuilder:Ljava/lang/StringBuilder;

    .line 148
    return-void
.end method

.method private outputLog()V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->mLogBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiDesktopModeLaunchParamsModifier"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return-void
.end method


# virtual methods
.method public onCalculate(Lcom/android/server/wm/Task;Landroid/content/pm/ActivityInfo$WindowLayout;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/app/ActivityOptions;Lcom/android/server/wm/ActivityStarter$Request;ILcom/android/server/wm/LaunchParamsController$LaunchParams;Lcom/android/server/wm/LaunchParamsController$LaunchParams;)I
    .locals 1
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "layout"    # Landroid/content/pm/ActivityInfo$WindowLayout;
    .param p3, "activity"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "source"    # Lcom/android/server/wm/ActivityRecord;
    .param p5, "options"    # Landroid/app/ActivityOptions;
    .param p6, "request"    # Lcom/android/server/wm/ActivityStarter$Request;
    .param p7, "phase"    # I
    .param p8, "currentParams"    # Lcom/android/server/wm/LaunchParamsController$LaunchParams;
    .param p9, "outParams"    # Lcom/android/server/wm/LaunchParamsController$LaunchParams;

    .line 55
    invoke-direct {p0, p1, p3}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->initLogBuilder(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;)V

    .line 56
    invoke-direct/range {p0 .. p9}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->calculate(Lcom/android/server/wm/Task;Landroid/content/pm/ActivityInfo$WindowLayout;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/app/ActivityOptions;Lcom/android/server/wm/ActivityStarter$Request;ILcom/android/server/wm/LaunchParamsController$LaunchParams;Lcom/android/server/wm/LaunchParamsController$LaunchParams;)I

    move-result v0

    .line 58
    .local v0, "result":I
    invoke-direct {p0}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->outputLog()V

    .line 59
    return v0
.end method
