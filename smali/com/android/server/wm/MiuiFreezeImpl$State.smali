.class final enum Lcom/android/server/wm/MiuiFreezeImpl$State;
.super Ljava/lang/Enum;
.source "MiuiFreezeImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiFreezeImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/wm/MiuiFreezeImpl$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/wm/MiuiFreezeImpl$State;

.field public static final enum DISABLE:Lcom/android/server/wm/MiuiFreezeImpl$State;

.field public static final enum ENABLE:Lcom/android/server/wm/MiuiFreezeImpl$State;


# direct methods
.method private static synthetic $values()[Lcom/android/server/wm/MiuiFreezeImpl$State;
    .locals 2

    .line 522
    sget-object v0, Lcom/android/server/wm/MiuiFreezeImpl$State;->DISABLE:Lcom/android/server/wm/MiuiFreezeImpl$State;

    sget-object v1, Lcom/android/server/wm/MiuiFreezeImpl$State;->ENABLE:Lcom/android/server/wm/MiuiFreezeImpl$State;

    filled-new-array {v0, v1}, [Lcom/android/server/wm/MiuiFreezeImpl$State;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 523
    new-instance v0, Lcom/android/server/wm/MiuiFreezeImpl$State;

    const-string v1, "DISABLE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/server/wm/MiuiFreezeImpl$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/wm/MiuiFreezeImpl$State;->DISABLE:Lcom/android/server/wm/MiuiFreezeImpl$State;

    .line 524
    new-instance v0, Lcom/android/server/wm/MiuiFreezeImpl$State;

    const-string v1, "ENABLE"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/android/server/wm/MiuiFreezeImpl$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/wm/MiuiFreezeImpl$State;->ENABLE:Lcom/android/server/wm/MiuiFreezeImpl$State;

    .line 522
    invoke-static {}, Lcom/android/server/wm/MiuiFreezeImpl$State;->$values()[Lcom/android/server/wm/MiuiFreezeImpl$State;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/MiuiFreezeImpl$State;->$VALUES:[Lcom/android/server/wm/MiuiFreezeImpl$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 522
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/wm/MiuiFreezeImpl$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 522
    const-class v0, Lcom/android/server/wm/MiuiFreezeImpl$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreezeImpl$State;

    return-object v0
.end method

.method public static values()[Lcom/android/server/wm/MiuiFreezeImpl$State;
    .locals 1

    .line 522
    sget-object v0, Lcom/android/server/wm/MiuiFreezeImpl$State;->$VALUES:[Lcom/android/server/wm/MiuiFreezeImpl$State;

    invoke-virtual {v0}, [Lcom/android/server/wm/MiuiFreezeImpl$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/wm/MiuiFreezeImpl$State;

    return-object v0
.end method
