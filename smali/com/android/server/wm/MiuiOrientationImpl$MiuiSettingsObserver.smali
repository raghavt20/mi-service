.class Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiOrientationImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiOrientationImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field resolver:Landroid/content/ContentResolver;

.field final synthetic this$0:Lcom/android/server/wm/MiuiOrientationImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 208
    iput-object p1, p0, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    .line 209
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 210
    invoke-static {p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiOrientationImpl;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->resolver:Landroid/content/ContentResolver;

    .line 211
    return-void
.end method


# virtual methods
.method observe()V
    .locals 5

    .line 214
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->resolver:Landroid/content/ContentResolver;

    const-string v1, "miui_smart_rotation"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 216
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 217
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 221
    const-string v0, "miui_smart_rotation"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->resolver:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$sfgetENABLED()Z

    move-result v2

    const/4 v3, -0x2

    invoke-static {v1, v0, v2, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    .line 224
    .local v0, "isEnabled":Z
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$sfgetENABLED()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$sfgetIS_MIUI_OPTIMIZATION()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fputmSmartRotationEnabled(Lcom/android/server/wm/MiuiOrientationImpl;Z)V

    .line 225
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v2}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmSmartRotationEnabled(Lcom/android/server/wm/MiuiOrientationImpl;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiOrientationImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    .end local v0    # "isEnabled":Z
    :cond_1
    return-void
.end method
