class com.android.server.wm.MultiSenceManagerInternalStubImpl$UpdateScreenStatus implements java.lang.Runnable {
	 /* .source "MultiSenceManagerInternalStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MultiSenceManagerInternalStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "UpdateScreenStatus" */
} // .end annotation
/* # instance fields */
private com.android.server.wm.WindowState mNewFocus;
final com.android.server.wm.MultiSenceManagerInternalStubImpl this$0; //synthetic
/* # direct methods */
public com.android.server.wm.MultiSenceManagerInternalStubImpl$UpdateScreenStatus ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MultiSenceManagerInternalStubImpl; */
/* .param p2, "newFocus" # Lcom/android/server/wm/WindowState; */
/* .line 76 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 77 */
this.mNewFocus = p2;
/* .line 78 */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 82 */
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$sfgetmMultiSenceMI ( );
/* if-nez v0, :cond_0 */
/* .line 83 */
v0 = this.this$0;
final String v1 = "not connect to multi-sence core service"; // const-string v1, "not connect to multi-sence core service"
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$mLOG_IF_DEBUG ( v0,v1 );
/* .line 84 */
return;
/* .line 87 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
/* if-nez v0, :cond_1 */
/* .line 88 */
v0 = this.this$0;
final String v1 = "func not enable due to Cloud Controller"; // const-string v1, "func not enable due to Cloud Controller"
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$mLOG_IF_DEBUG ( v0,v1 );
/* .line 89 */
return;
/* .line 92 */
} // :cond_1
v0 = this.mNewFocus;
/* if-nez v0, :cond_2 */
/* .line 93 */
v0 = this.this$0;
final String v1 = "new focus is null"; // const-string v1, "new focus is null"
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$mLOG_IF_DEBUG ( v0,v1 );
/* .line 94 */
return;
/* .line 97 */
} // :cond_2
v0 = this.this$0;
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$fgetmContext ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.this$0;
v0 = com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$fgetmBootComplete ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 98 */
v0 = this.this$0;
v1 = this.mNewFocus;
(( com.android.server.wm.MultiSenceManagerInternalStubImpl ) v0 ).sendFocusWindowsBroadcast ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->sendFocusWindowsBroadcast(Lcom/android/server/wm/WindowState;)V
/* .line 101 */
} // :cond_3
v0 = this.this$0;
v1 = this.mNewFocus;
v0 = com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$misWindowPackageChanged ( v0,v1 );
/* if-nez v0, :cond_4 */
/* .line 102 */
v0 = this.this$0;
v1 = this.mNewFocus;
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$fputmCurrentFocusWindow ( v0,v1 );
/* .line 103 */
return;
/* .line 105 */
} // :cond_4
v0 = this.this$0;
v1 = this.mNewFocus;
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$fputmCurrentFocusWindow ( v0,v1 );
/* .line 107 */
int v0 = 0; // const/4 v0, 0x0
/* .line 108 */
/* .local v0, "sWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
v1 = this.this$0;
v1 = com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$misConnectToWMS ( v1 );
/* .line 110 */
/* .local v1, "connection":Z */
/* if-nez v1, :cond_5 */
return;
/* .line 112 */
} // :cond_5
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$sfgetservice ( );
(( com.android.server.wm.WindowManagerService ) v2 ).getGlobalLock ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;
/* monitor-enter v2 */
/* .line 113 */
try { // :try_start_0
/* const-string/jumbo v3, "updateScreenStatusWithFoucs" */
/* const-wide/16 v4, 0x20 */
android.os.Trace .traceBegin ( v4,v5,v3 );
/* .line 114 */
v3 = this.this$0;
(( com.android.server.wm.MultiSenceManagerInternalStubImpl ) v3 ).getWindowsNeedToSched ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->getWindowsNeedToSched()Ljava/util/Map;
/* move-object v0, v3 */
/* .line 115 */
if ( v0 != null) { // if-eqz v0, :cond_6
v3 = this.mNewFocus;
v3 = (( com.android.server.wm.WindowState ) v3 ).getOwningPackage ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 116 */
v3 = this.mNewFocus;
(( com.android.server.wm.WindowState ) v3 ).getOwningPackage ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo; */
int v6 = 1; // const/4 v6, 0x1
(( com.miui.server.multisence.SingleWindowInfo ) v3 ).setFocused ( v6 ); // invoke-virtual {v3, v6}, Lcom/miui/server/multisence/SingleWindowInfo;->setFocused(Z)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 118 */
} // :cond_6
android.os.Trace .traceEnd ( v4,v5 );
/* .line 119 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 121 */
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$sfgetmMultiSenceMI ( );
/* monitor-enter v3 */
/* .line 122 */
try { // :try_start_1
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$sfgetmMultiSenceMI ( );
final String v4 = "focus"; // const-string v4, "focus"
/* .line 123 */
com.android.server.wm.MultiSenceManagerInternalStubImpl .-$$Nest$sfgetmMultiSenceMI ( );
/* .line 124 */
/* monitor-exit v3 */
/* .line 126 */
return;
/* .line 124 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* .line 119 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v3 */
} // .end method
