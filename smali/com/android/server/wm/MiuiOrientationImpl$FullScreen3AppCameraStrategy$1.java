class com.android.server.wm.MiuiOrientationImpl$FullScreen3AppCameraStrategy$1 extends android.os.Handler {
	 /* .source "MiuiOrientationImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiOrientationImpl$FullScreen3AppCameraStrategy this$1; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiOrientationImpl$FullScreen3AppCameraStrategy$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 1205 */
this.this$1 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1208 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "MiuiOrientationImpl"; // const-string v1, "MiuiOrientationImpl"
/* packed-switch v0, :pswitch_data_0 */
/* .line 1219 */
/* :pswitch_0 */
try { // :try_start_0
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "SET persist.vendor.device.orientation :"; // const-string v2, "SET persist.vendor.device.orientation :"
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.obj;
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v0 );
	 /* .line 1220 */
	 final String v0 = "persist.vendor.device.orientation"; // const-string v0, "persist.vendor.device.orientation"
	 v2 = this.obj;
	 java.lang.String .valueOf ( v2 );
	 android.os.SystemProperties .set ( v0,v2 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 1223 */
	 /* .line 1221 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 1222 */
	 /* .local v0, "e":Ljava/lang/RuntimeException; */
	 final String v2 = "Exception persist.vendor.device.orientation e: "; // const-string v2, "Exception persist.vendor.device.orientation e: "
	 android.util.Slog .e ( v1,v2,v0 );
	 /* .line 1224 */
} // .end local v0 # "e":Ljava/lang/RuntimeException;
/* .line 1211 */
/* :pswitch_1 */
try { // :try_start_1
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "SET persist.vendor.multiwin.3appcam :"; // const-string v2, "SET persist.vendor.multiwin.3appcam :"
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.obj;
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v0 );
	 /* .line 1212 */
	 final String v0 = "persist.vendor.multiwin.3appcam"; // const-string v0, "persist.vendor.multiwin.3appcam"
	 v2 = this.obj;
	 java.lang.String .valueOf ( v2 );
	 android.os.SystemProperties .set ( v0,v2 );
	 /* :try_end_1 */
	 /* .catch Ljava/lang/RuntimeException; {:try_start_1 ..:try_end_1} :catch_1 */
	 /* .line 1215 */
	 /* .line 1213 */
	 /* :catch_1 */
	 /* move-exception v0 */
	 /* .line 1214 */
	 /* .restart local v0 # "e":Ljava/lang/RuntimeException; */
	 final String v2 = "Exception persist.vendor.multiwin.3appcam e: "; // const-string v2, "Exception persist.vendor.multiwin.3appcam e: "
	 android.util.Slog .e ( v1,v2,v0 );
	 /* .line 1216 */
} // .end local v0 # "e":Ljava/lang/RuntimeException;
/* nop */
/* .line 1228 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x3e9 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
