.class Lcom/android/server/wm/MiuiPaperContrastOverlay$2;
.super Landroid/view/IDisplayWindowListener$Stub;
.source "MiuiPaperContrastOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiPaperContrastOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mLastBounds:Landroid/graphics/Rect;

.field private mNewBounds:Landroid/graphics/Rect;

.field private runnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;


# direct methods
.method public static synthetic $r8$lambda$Q-GGqp9ypaB816z7ZS9OiANlpjk(Lcom/android/server/wm/MiuiPaperContrastOverlay$2;Landroid/content/res/Configuration;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->lambda$onDisplayConfigurationChanged$0(Landroid/content/res/Configuration;I)V

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiPaperContrastOverlay;

    .line 210
    iput-object p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-direct {p0}, Landroid/view/IDisplayWindowListener$Stub;-><init>()V

    return-void
.end method

.method private synthetic lambda$onDisplayConfigurationChanged$0(Landroid/content/res/Configuration;I)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;
    .param p2, "displayId"    # I

    .line 233
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$mupdateDisplaySize(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V

    .line 234
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmLastConfiguration(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    move-result v0

    .line 235
    .local v0, "diff":I
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$misSizeChangeHappened(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Z

    move-result v1

    .line 237
    .local v1, "isChanged":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Config size change is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", from ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mLastBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v4}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmLastConfiguration(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    .line 238
    invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getRotation()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "] to ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mNewBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    .line 239
    invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getRotation()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "], "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    and-int/lit16 v4, v0, 0x400

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 237
    const-string v3, "MiuiPaperContrastOverlay"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    and-int/lit16 v2, v0, 0x80

    if-nez v2, :cond_0

    if-nez v1, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mLastBounds:Landroid/graphics/Rect;

    .line 250
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mNewBounds:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mLastBounds:Landroid/graphics/Rect;

    .line 251
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mNewBounds:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-eq v2, v3, :cond_2

    and-int/lit16 v2, v0, 0x400

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mNewBounds:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mLastBounds:Landroid/graphics/Rect;

    .line 253
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 255
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$mdestroySurface(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V

    .line 256
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmNeedShowPaperSurface(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 257
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$mcreateSurface(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V

    .line 258
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->showPaperModeSurface()V

    .line 262
    :cond_2
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmLastConfiguration(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    .line 263
    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 270
    return-void
.end method

.method public onDisplayConfigurationChanged(ILandroid/content/res/Configuration;)V
    .locals 6
    .param p1, "displayId"    # I
    .param p2, "newConfig"    # Landroid/content/res/Configuration;

    .line 218
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmLastConfiguration(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 219
    .local v0, "lastBounds":Landroid/graphics/Rect;
    iget-object v1, p2, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 221
    .local v1, "newBounds":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getDisplayId()I

    move-result v2

    if-eq v2, p1, :cond_0

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmFoldDeviceReady(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    .line 224
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fputmFoldDeviceReady(Lcom/android/server/wm/MiuiPaperContrastOverlay;Z)V

    .line 225
    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mLastBounds:Landroid/graphics/Rect;

    .line 226
    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->mNewBounds:Landroid/graphics/Rect;

    .line 228
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->runnable:Ljava/lang/Runnable;

    if-eqz v2, :cond_1

    .line 229
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmWms(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Lcom/android/server/wm/WindowManagerService;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/android/server/wm/WindowManagerService$H;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 232
    :cond_1
    new-instance v2, Lcom/android/server/wm/MiuiPaperContrastOverlay$2$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, p2, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlay$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay$2;Landroid/content/res/Configuration;I)V

    iput-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->runnable:Ljava/lang/Runnable;

    .line 264
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmWms(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Lcom/android/server/wm/WindowManagerService;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->runnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/wm/WindowManagerService$H;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 265
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 273
    return-void
.end method

.method public onFixedRotationFinished(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 279
    return-void
.end method

.method public onFixedRotationStarted(II)V
    .locals 0
    .param p1, "displayId"    # I
    .param p2, "newRotation"    # I

    .line 276
    return-void
.end method

.method public onKeepClearAreasChanged(ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1, "displayId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .line 283
    .local p2, "restricted":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    .local p3, "unrestricted":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    return-void
.end method
