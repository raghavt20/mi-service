.class final enum Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;
.super Ljava/lang/Enum;
.source "FindDeviceLockWindowImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/FindDeviceLockWindowImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "LockDeviceWindowPolicy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

.field public static final enum HIDE:Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

.field public static final enum SHOW:Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;


# direct methods
.method private static synthetic $values()[Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;
    .locals 2

    .line 120
    sget-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;->HIDE:Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    sget-object v1, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;->SHOW:Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    filled-new-array {v0, v1}, [Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 121
    new-instance v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    const-string v1, "HIDE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;->HIDE:Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    new-instance v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    const-string v1, "SHOW"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;->SHOW:Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    .line 120
    invoke-static {}, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;->$values()[Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;->$VALUES:[Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 120
    const-class v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    return-object v0
.end method

.method public static values()[Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;
    .locals 1

    .line 120
    sget-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;->$VALUES:[Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    invoke-virtual {v0}, [Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;

    return-object v0
.end method
