public class com.android.server.wm.AppRTWmsImpl implements com.android.server.wm.AppRTWmsStub {
	 /* .source "AppRTWmsImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.wm.AppRTWmsImpl ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private com.android.server.wm.AppResolutionTuner getTunerList ( ) {
		 /* .locals 1 */
		 /* .line 72 */
		 com.android.server.wm.AppResolutionTuner .getInstance ( );
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getTAG ( ) {
		 /* .locals 1 */
		 /* .line 63 */
		 final String v0 = "APP_RT"; // const-string v0, "APP_RT"
	 } // .end method
	 public Boolean isAppResolutionTunerSupport ( ) {
		 /* .locals 1 */
		 /* .line 21 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner; */
		 v0 = 		 (( com.android.server.wm.AppResolutionTuner ) v0 ).isAppRTEnable ( ); // invoke-virtual {v0}, Lcom/android/server/wm/AppResolutionTuner;->isAppRTEnable()Z
	 } // .end method
	 public Boolean isDebug ( ) {
		 /* .locals 1 */
		 /* .line 68 */
		 /* sget-boolean v0, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z */
	 } // .end method
	 public void setAppResolutionTunerSupport ( Boolean p0 ) {
		 /* .locals 1 */
		 /* .param p1, "support" # Z */
		 /* .line 16 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner; */
		 (( com.android.server.wm.AppResolutionTuner ) v0 ).setAppRTEnable ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/AppResolutionTuner;->setAppRTEnable(Z)V
		 /* .line 17 */
		 return;
	 } // .end method
	 public void setWindowScaleByWL ( com.android.server.wm.WindowState p0, android.view.DisplayInfo p1, android.view.WindowManager$LayoutParams p2, Integer p3, Integer p4 ) {
		 /* .locals 7 */
		 /* .param p1, "win" # Lcom/android/server/wm/WindowState; */
		 /* .param p2, "displayInfo" # Landroid/view/DisplayInfo; */
		 /* .param p3, "attrs" # Landroid/view/WindowManager$LayoutParams; */
		 /* .param p4, "requestedWidth" # I */
		 /* .param p5, "requestedHeight" # I */
		 /* .line 27 */
		 /* iget v0, p2, Landroid/view/DisplayInfo;->logicalWidth:I */
		 /* .line 28 */
		 /* .local v0, "width":I */
		 /* iget v1, p2, Landroid/view/DisplayInfo;->logicalHeight:I */
		 /* .line 29 */
		 /* .local v1, "height":I */
		 int v2 = 0; // const/4 v2, 0x0
		 if ( p3 != null) { // if-eqz p3, :cond_0
			 v3 = this.packageName;
		 } // :cond_0
		 /* move-object v3, v2 */
		 /* .line 30 */
		 /* .local v3, "packageName":Ljava/lang/String; */
	 } // :goto_0
	 if ( p3 != null) { // if-eqz p3, :cond_1
		 (( android.view.WindowManager$LayoutParams ) p3 ).getTitle ( ); // invoke-virtual {p3}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
		 if ( v4 != null) { // if-eqz v4, :cond_1
			 /* .line 31 */
			 (( android.view.WindowManager$LayoutParams ) p3 ).getTitle ( ); // invoke-virtual {p3}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
		 } // :cond_1
		 /* nop */
		 /* .line 32 */
		 /* .local v2, "windowName":Ljava/lang/String; */
	 } // :goto_1
	 if ( p3 != null) { // if-eqz p3, :cond_4
		 if ( v3 != null) { // if-eqz v3, :cond_4
			 if ( v2 != null) { // if-eqz v2, :cond_4
				 /* .line 33 */
				 final String v4 = "FastStarting"; // const-string v4, "FastStarting"
				 v4 = 				 (( java.lang.String ) v2 ).contains ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
				 /* if-nez v4, :cond_4 */
				 /* .line 35 */
				 final String v4 = "Splash Screen"; // const-string v4, "Splash Screen"
				 v4 = 				 (( java.lang.String ) v2 ).contains ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
				 /* if-nez v4, :cond_4 */
				 /* .line 37 */
				 final String v4 = "PopupWindow"; // const-string v4, "PopupWindow"
				 v4 = 				 (( java.lang.String ) v2 ).contains ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
				 /* if-nez v4, :cond_4 */
				 /* if-ne v1, p5, :cond_2 */
				 /* if-eq v0, p4, :cond_3 */
			 } // :cond_2
			 /* iget v4, p3, Landroid/view/WindowManager$LayoutParams;->width:I */
			 int v5 = -1; // const/4 v5, -0x1
			 /* if-ne v4, v5, :cond_4 */
			 /* iget v4, p3, Landroid/view/WindowManager$LayoutParams;->height:I */
			 /* if-ne v4, v5, :cond_4 */
			 /* iget v4, p3, Landroid/view/WindowManager$LayoutParams;->x:I */
			 /* if-nez v4, :cond_4 */
			 /* iget v4, p3, Landroid/view/WindowManager$LayoutParams;->y:I */
			 /* if-nez v4, :cond_4 */
			 /* .line 42 */
		 } // :cond_3
		 /* invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner; */
		 v4 = 		 (( com.android.server.wm.AppResolutionTuner ) v4 ).isScaledByWMS ( v3, v2 ); // invoke-virtual {v4, v3, v2}, Lcom/android/server/wm/AppResolutionTuner;->isScaledByWMS(Ljava/lang/String;Ljava/lang/String;)Z
		 if ( v4 != null) { // if-eqz v4, :cond_4
			 /* .line 43 */
			 /* invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner; */
			 v4 = 			 (( com.android.server.wm.AppResolutionTuner ) v4 ).getScaleValue ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/wm/AppResolutionTuner;->getScaleValue(Ljava/lang/String;)F
			 /* .line 45 */
			 /* .local v4, "scale":F */
			 /* const/high16 v5, 0x3f800000 # 1.0f */
			 /* cmpl-float v5, v4, v5 */
			 if ( v5 != null) { // if-eqz v5, :cond_4
				 /* .line 46 */
				 /* iput v4, p1, Lcom/android/server/wm/WindowState;->mHWScale:F */
				 /* .line 47 */
				 int v5 = 1; // const/4 v5, 0x1
				 /* iput-boolean v5, p1, Lcom/android/server/wm/WindowState;->mNeedHWResizer:Z */
				 /* .line 48 */
				 /* sget-boolean v5, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z */
				 if ( v5 != null) { // if-eqz v5, :cond_4
					 /* .line 49 */
					 /* new-instance v5, Ljava/lang/StringBuilder; */
					 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
					 /* const-string/jumbo v6, "setWindowScaleByWL, scale: " */
					 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
					 final String v6 = ", win: "; // const-string v6, ", win: "
					 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
					 final String v6 = ",attrs: "; // const-string v6, ",attrs: "
					 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 /* .line 50 */
					 (( android.view.WindowManager$LayoutParams ) p3 ).getTitle ( ); // invoke-virtual {p3}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
					 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
					 /* .line 49 */
					 final String v6 = "APP_RT"; // const-string v6, "APP_RT"
					 android.util.Slog .d ( v6,v5 );
					 /* .line 54 */
				 } // .end local v4 # "scale":F
			 } // :cond_4
			 return;
		 } // .end method
		 public Boolean updateResolutionTunerConfig ( java.lang.String p0 ) {
			 /* .locals 1 */
			 /* .param p1, "config" # Ljava/lang/String; */
			 /* .line 58 */
			 /* invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner; */
			 v0 = 			 (( com.android.server.wm.AppResolutionTuner ) v0 ).updateResolutionTunerConfig ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/AppResolutionTuner;->updateResolutionTunerConfig(Ljava/lang/String;)Z
		 } // .end method
