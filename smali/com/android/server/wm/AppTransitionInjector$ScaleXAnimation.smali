.class Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;
.super Landroid/view/animation/Animation;
.source "AppTransitionInjector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/AppTransitionInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScaleXAnimation"
.end annotation


# instance fields
.field private mFromX:F

.field private mPivotX:F

.field private mToX:F


# direct methods
.method public constructor <init>(FF)V
    .locals 1
    .param p1, "fromX"    # F
    .param p2, "toX"    # F

    .line 1361
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1362
    iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mFromX:F

    .line 1363
    iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mToX:F

    .line 1364
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mPivotX:F

    .line 1365
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "fromX"    # F
    .param p2, "toX"    # F
    .param p3, "pivotX"    # F

    .line 1367
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1368
    iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mFromX:F

    .line 1369
    iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mToX:F

    .line 1370
    iput p3, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mPivotX:F

    .line 1371
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 6
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .line 1375
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1376
    .local v0, "sx":F
    invoke-virtual {p0}, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->getScaleFactor()F

    move-result v1

    .line 1378
    .local v1, "scale":F
    iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mFromX:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v4, v2, v3

    if-nez v4, :cond_0

    iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mToX:F

    cmpl-float v4, v4, v3

    if-eqz v4, :cond_1

    .line 1379
    :cond_0
    iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mToX:F

    sub-float/2addr v4, v2

    mul-float/2addr v4, p1

    add-float v0, v2, v4

    .line 1382
    :cond_1
    iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mPivotX:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-nez v2, :cond_2

    .line 1383
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    goto :goto_0

    .line 1385
    :cond_2
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    iget v5, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mPivotX:F

    mul-float/2addr v5, v1

    invoke-virtual {v2, v0, v3, v5, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 1387
    :goto_0
    return-void
.end method
