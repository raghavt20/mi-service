public class com.android.server.wm.MiuiSizeCompatService extends android.sizecompat.IMiuiSizeCompat$Stub {
	 /* .source "MiuiSizeCompatService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiSizeCompatService$H;, */
	 /* Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;, */
	 /* Lcom/android/server/wm/MiuiSizeCompatService$LocalService;, */
	 /* Lcom/android/server/wm/MiuiSizeCompatService$Shell;, */
	 /* Lcom/android/server/wm/MiuiSizeCompatService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String APP_CONTINUITY_FIX_RATIO_KEY;
public static Boolean DEBUG;
public static final java.lang.String FAST_XML;
public static Float FLIP_DEFAULT_SCALE;
public static Float FLIP_UNSCALE;
private static final java.lang.String FORCE_RESIZABLE_ACTIVITIES;
private static final Integer NOTIFICATION_ID;
private static final java.lang.String PACKAGE_NAME;
public static final java.lang.String SERVICE_NAME;
private static final java.lang.String SETTING_CONFIG_BACKUP_FILE_NAME;
private static final java.lang.String SETTING_CONFIG_FILE_NAME;
public static final java.lang.String SYSTEM_USERS;
private static final java.lang.String TAG;
private static final java.lang.String XML_ATTRIBUTE_ASPECT_RATIO;
private static final java.lang.String XML_ATTRIBUTE_DISPLAY_NAME;
private static final java.lang.String XML_ATTRIBUTE_GRAVITY;
private static final java.lang.String XML_ATTRIBUTE_NAME;
private static final java.lang.String XML_ATTRIBUTE_SCALE_MODE;
private static final java.lang.String XML_ATTRIBUTE_SETTING_CONDIG;
private static final java.lang.String XML_ELEMENT_SETTING;
private static final java.util.List mCallerWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.List mRestartTaskActs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.List mRestartTaskApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private final java.util.List APPLIACTION_APP_BLACK_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.String GAME_JSON_KEY_GRAVITY;
private final java.lang.String GAME_JSON_KEY_NAME;
private final java.lang.String GAME_JSON_KEY_RATIO;
private final java.lang.String NOTIFICATION_ICON_KEY;
private final java.lang.String NOTIFICATION_PACKAGE_ICON;
private final java.lang.String NOTIFICATION_SERVICE_CLASS_NAME;
private final java.lang.String NOTIFICATION_SERVICE_PACKAGE;
private java.lang.String PERMISSION_ACTIVITY;
private final java.lang.String SIZE_COMPAT_CHANNEL_ID;
private final java.lang.String SIZE_COMPAT_CHANNEL_NAME;
private com.android.server.am.ActivityManagerService mAms;
private miui.process.IForegroundInfoListener$Stub mAppObserver;
private com.android.server.wm.ActivityTaskManagerService mAtms;
private java.io.File mBackupSettingFilename;
private final com.android.server.wm.MiuiSizeCompatService$H mBgHandler;
private final java.lang.String mCompatModeModuleName;
private final android.content.Context mContext;
private com.android.server.wm.ActivityRecord mCurFullAct;
private final android.os.Handler mFgHandler;
private android.view.IDisplayFoldListener$Stub mFoldObserver;
private final java.util.Map mGameSettingConfig;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Landroid/sizecompat/AspectRatioInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mLock;
private android.app.NotificationManager mNotificationManager;
private Boolean mNotificationShowing;
private com.miui.server.process.ProcessManagerInternal mPMS;
private miuix.appcompat.app.AlertDialog mRestartAppDialog;
private miui.security.SecurityManager mSecurityManager;
private Boolean mServiceReady;
private final java.util.Map mSettingConfigs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Landroid/sizecompat/AspectRatioInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.io.File mSettingFilename;
private final java.util.Map mStaticSettingConfigs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Landroid/sizecompat/AspectRatioInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.statusbar.StatusBarManagerInternal mStatusBarService;
private android.content.Context mUiContext;
private final com.android.server.wm.MiuiSizeCompatService$UiHandler mUiHandler;
/* # direct methods */
public static void $r8$lambda$EZGSahURCWm0emzACX_aU7BA7Mc ( com.android.server.wm.MiuiSizeCompatService p0, com.android.server.wm.ActivityRecord p1, android.content.DialogInterface p2, Integer p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiSizeCompatService;->lambda$showConfirmDialog$0(Lcom/android/server/wm/ActivityRecord;Landroid/content/DialogInterface;I)V */
return;
} // .end method
public static void $r8$lambda$G8QKMToB-TYoTyEXvBYYnuLQKNw ( com.android.server.wm.MiuiSizeCompatService p0, com.android.server.wm.ActivityRecord p1, Float p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiSizeCompatService;->restartProcessForRatioLocked(Lcom/android/server/wm/ActivityRecord;F)V */
return;
} // .end method
public static void $r8$lambda$poACLljgOu3uDdzrgGWnQ6sTMeE ( com.android.server.wm.MiuiSizeCompatService p0, com.android.server.wm.ActivityRecord p1, android.content.DialogInterface p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiSizeCompatService;->lambda$showConfirmDialog$3(Lcom/android/server/wm/ActivityRecord;Landroid/content/DialogInterface;)V */
return;
} // .end method
static java.lang.String -$$Nest$fgetPERMISSION_ACTIVITY ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.PERMISSION_ACTIVITY;
} // .end method
static com.android.server.wm.ActivityTaskManagerService -$$Nest$fgetmAtms ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAtms;
} // .end method
static com.android.server.wm.MiuiSizeCompatService$H -$$Nest$fgetmBgHandler ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBgHandler;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static com.android.server.wm.ActivityRecord -$$Nest$fgetmCurFullAct ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCurFullAct;
} // .end method
static android.os.Handler -$$Nest$fgetmFgHandler ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFgHandler;
} // .end method
static java.util.Map -$$Nest$fgetmGameSettingConfig ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mGameSettingConfig;
} // .end method
static Boolean -$$Nest$fgetmNotificationShowing ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationShowing:Z */
} // .end method
static miui.security.SecurityManager -$$Nest$fgetmSecurityManager ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSecurityManager;
} // .end method
static Boolean -$$Nest$fgetmServiceReady ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z */
} // .end method
static java.util.Map -$$Nest$fgetmSettingConfigs ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSettingConfigs;
} // .end method
static java.util.Map -$$Nest$fgetmStaticSettingConfigs ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mStaticSettingConfigs;
} // .end method
static Float -$$Nest$mgetAspectRatioByPackage ( com.android.server.wm.MiuiSizeCompatService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->getAspectRatioByPackage(Ljava/lang/String;)F */
} // .end method
static void -$$Nest$monSystemReady ( com.android.server.wm.MiuiSizeCompatService p0, com.android.server.wm.ActivityTaskManagerService p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->onSystemReady(Lcom/android/server/wm/ActivityTaskManagerService;)V */
return;
} // .end method
static void -$$Nest$mregisterDataObserver ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->registerDataObserver()V */
return;
} // .end method
static Boolean -$$Nest$msetMiuiSizeCompatScaleMode ( com.android.server.wm.MiuiSizeCompatService p0, java.lang.String p1, Integer p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiSizeCompatService;->setMiuiSizeCompatScaleMode(Ljava/lang/String;IZ)Z */
} // .end method
static void -$$Nest$mshowConfirmDialog ( com.android.server.wm.MiuiSizeCompatService p0, com.android.server.wm.ActivityRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->showConfirmDialog(Lcom/android/server/wm/ActivityRecord;)V */
return;
} // .end method
static void -$$Nest$mshowWarningNotification ( com.android.server.wm.MiuiSizeCompatService p0, com.android.server.wm.ActivityRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->showWarningNotification(Lcom/android/server/wm/ActivityRecord;)V */
return;
} // .end method
static void -$$Nest$mupdateSettingConfigsFromCloud ( com.android.server.wm.MiuiSizeCompatService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->updateSettingConfigsFromCloud()V */
return;
} // .end method
static com.android.server.wm.MiuiSizeCompatService ( ) {
/* .locals 4 */
/* .line 128 */
/* const v0, 0x3f4ccccd # 0.8f */
/* .line 130 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 149 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 150 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 152 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 153 */
int v3 = 0; // const/4 v3, 0x0
com.android.server.wm.MiuiSizeCompatService.DEBUG = (v3!= 0);
/* .line 192 */
final String v3 = "com.cntaiping.tpapp"; // const-string v3, "com.cntaiping.tpapp"
/* .line 193 */
final String v3 = "com.oda_cad"; // const-string v3, "com.oda_cad"
/* .line 194 */
final String v3 = "com.jime.encyclopediascanning"; // const-string v3, "com.jime.encyclopediascanning"
/* .line 195 */
final String v3 = "com.tmri.app.main"; // const-string v3, "com.tmri.app.main"
/* .line 197 */
final String v3 = "com.mahjong.nb"; // const-string v3, "com.mahjong.nb"
/* .line 198 */
final String v3 = "com.yusi.chongchong"; // const-string v3, "com.yusi.chongchong"
/* .line 199 */
final String v0 = "com.lianjia.common.vr.webview.VrWebviewActivity"; // const-string v0, "com.lianjia.common.vr.webview.VrWebviewActivity"
/* .line 200 */
final String v0 = "com.alipay.mobile.quinox.LauncherActivity"; // const-string v0, "com.alipay.mobile.quinox.LauncherActivity"
/* .line 202 */
final String v0 = "com.android.settings"; // const-string v0, "com.android.settings"
/* .line 203 */
final String v0 = "com.android.systemui"; // const-string v0, "com.android.systemui"
/* .line 204 */
final String v0 = "android"; // const-string v0, "android"
/* .line 205 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* .line 206 */
final String v0 = "miphone.android.compat"; // const-string v0, "miphone.android.compat"
/* .line 207 */
return;
} // .end method
public com.android.server.wm.MiuiSizeCompatService ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 223 */
/* invoke-direct {p0}, Landroid/sizecompat/IMiuiSizeCompat$Stub;-><init>()V */
/* .line 138 */
final String v0 = "MiuiSizeCompat"; // const-string v0, "MiuiSizeCompat"
this.SIZE_COMPAT_CHANNEL_NAME = v0;
/* .line 139 */
final String v0 = "WaringNotification"; // const-string v0, "WaringNotification"
this.SIZE_COMPAT_CHANNEL_ID = v0;
/* .line 140 */
final String v0 = "com.android.settings"; // const-string v0, "com.android.settings"
this.NOTIFICATION_PACKAGE_ICON = v0;
/* .line 141 */
final String v0 = "miui.appIcon"; // const-string v0, "miui.appIcon"
this.NOTIFICATION_ICON_KEY = v0;
/* .line 142 */
final String v0 = "android.sizecompat.SizeCompatChangeService"; // const-string v0, "android.sizecompat.SizeCompatChangeService"
this.NOTIFICATION_SERVICE_CLASS_NAME = v0;
/* .line 143 */
final String v0 = "android"; // const-string v0, "android"
this.NOTIFICATION_SERVICE_PACKAGE = v0;
/* .line 146 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mSettingConfigs = v0;
/* .line 147 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mStaticSettingConfigs = v0;
/* .line 165 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mGameSettingConfig = v0;
/* .line 166 */
final String v0 = "pkgName"; // const-string v0, "pkgName"
this.GAME_JSON_KEY_NAME = v0;
/* .line 167 */
final String v0 = "aspectRatio"; // const-string v0, "aspectRatio"
this.GAME_JSON_KEY_RATIO = v0;
/* .line 168 */
final String v0 = "gravity"; // const-string v0, "gravity"
this.GAME_JSON_KEY_GRAVITY = v0;
/* .line 171 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.APPLIACTION_APP_BLACK_LIST = v0;
/* .line 187 */
int v0 = 0; // const/4 v0, 0x0
this.mPMS = v0;
/* .line 188 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.mLock = v1;
/* .line 189 */
final String v1 = "com.lbe.security.miui/com.android.packageinstaller.permission.ui.GrantPermissionsActivity"; // const-string v1, "com.lbe.security.miui/com.android.packageinstaller.permission.ui.GrantPermissionsActivity"
this.PERMISSION_ACTIVITY = v1;
/* .line 517 */
/* new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiSizeCompatService$2;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;)V */
this.mAppObserver = v1;
/* .line 535 */
/* new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$3; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiSizeCompatService$3;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;)V */
this.mFoldObserver = v1;
/* .line 224 */
this.mContext = p1;
/* .line 225 */
/* new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$H; */
com.android.server.MiuiBgThread .getHandler ( );
(( android.os.Handler ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Landroid/os/Looper;)V */
this.mBgHandler = v1;
/* .line 226 */
/* new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$UiHandler; */
com.android.server.UiThread .getHandler ( );
(( android.os.Handler ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Landroid/os/Looper;)V */
this.mUiHandler = v1;
/* .line 227 */
com.android.server.MiuiFgThread .getHandler ( );
this.mFgHandler = v1;
/* .line 228 */
/* const-class v1, Lcom/android/server/wm/MiuiSizeCompatInternal; */
/* new-instance v2, Lcom/android/server/wm/MiuiSizeCompatService$LocalService; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/MiuiSizeCompatService$LocalService-IA;)V */
com.android.server.LocalServices .addService ( v1,v2 );
/* .line 229 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f0359 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
this.mCompatModeModuleName = v0;
/* .line 230 */
(( com.android.server.wm.MiuiSizeCompatService ) p0 ).initBlackAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->initBlackAppList()V
/* .line 231 */
return;
} // .end method
private Boolean checkInterfaceAccess ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "callingPid" # I */
/* .param p2, "callingUid" # I */
/* .line 902 */
v0 = android.os.Process .myPid ( );
int v1 = 1; // const/4 v1, 0x1
/* if-eq p1, v0, :cond_4 */
if ( p2 != null) { // if-eqz p2, :cond_4
/* const/16 v0, 0x7d0 */
/* if-ne p2, v0, :cond_0 */
/* .line 905 */
} // :cond_0
v0 = this.mAtms;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 906 */
try { // :try_start_0
v2 = this.mAtms;
v2 = this.mProcessMap;
(( com.android.server.wm.WindowProcessControllerMap ) v2 ).getProcess ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;
/* .line 907 */
/* .local v2, "wpc":Lcom/android/server/wm/WindowProcessController; */
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_3
v4 = this.mInfo;
if ( v4 != null) { // if-eqz v4, :cond_3
v4 = this.mInfo;
v4 = this.packageName;
v4 = android.text.TextUtils .isEmpty ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 910 */
} // :cond_1
v4 = com.android.server.wm.MiuiSizeCompatService.mCallerWhiteList;
v5 = this.mInfo;
v4 = v5 = this.packageName;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 911 */
/* monitor-exit v0 */
/* .line 913 */
} // .end local v2 # "wpc":Lcom/android/server/wm/WindowProcessController;
} // :cond_2
/* monitor-exit v0 */
/* .line 914 */
/* .line 908 */
/* .restart local v2 # "wpc":Lcom/android/server/wm/WindowProcessController; */
} // :cond_3
} // :goto_0
/* monitor-exit v0 */
/* .line 913 */
} // .end local v2 # "wpc":Lcom/android/server/wm/WindowProcessController;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 903 */
} // :cond_4
} // :goto_1
} // .end method
private void checkSlow ( Long p0, Long p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "startTime" # J */
/* .param p3, "threshold" # J */
/* .param p5, "where" # Ljava/lang/String; */
/* .line 1343 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p1 */
/* .line 1344 */
/* .local v0, "took":J */
/* cmp-long v2, v0, p3 */
/* if-lez v2, :cond_0 */
/* .line 1346 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Slow operation: "; // const-string v3, "Slow operation: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p5 ); // invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " took "; // const-string v3, " took "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms."; // const-string v3, "ms."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiSizeCompatService"; // const-string v3, "MiuiSizeCompatService"
android.util.Slog .w ( v3,v2 );
/* .line 1348 */
} // :cond_0
return;
} // .end method
private com.android.server.wm.ActivityRecord getActivityFromTokenLocked ( android.window.WindowContainerToken p0 ) {
/* .locals 6 */
/* .param p1, "token" # Landroid/window/WindowContainerToken; */
/* .line 1065 */
v0 = this.mAtms;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 1066 */
try { // :try_start_0
(( android.window.WindowContainerToken ) p1 ).asBinder ( ); // invoke-virtual {p1}, Landroid/window/WindowContainerToken;->asBinder()Landroid/os/IBinder;
com.android.server.wm.WindowContainer .fromBinder ( v1 );
/* .line 1067 */
/* .local v1, "wc":Lcom/android/server/wm/WindowContainer; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 1068 */
final String v3 = "MiuiSizeCompatService"; // const-string v3, "MiuiSizeCompatService"
final String v4 = "Could not resolve window from token"; // const-string v4, "Could not resolve window from token"
android.util.Slog .w ( v3,v4 );
/* .line 1069 */
/* monitor-exit v0 */
/* .line 1071 */
} // :cond_0
(( com.android.server.wm.WindowContainer ) v1 ).asTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
/* .line 1072 */
/* .local v3, "task":Lcom/android/server/wm/Task; */
/* if-nez v3, :cond_1 */
/* .line 1073 */
final String v4 = "MiuiSizeCompatService"; // const-string v4, "MiuiSizeCompatService"
final String v5 = "Could not resolve task from token"; // const-string v5, "Could not resolve task from token"
android.util.Slog .w ( v4,v5 );
/* .line 1074 */
/* monitor-exit v0 */
/* .line 1076 */
} // :cond_1
(( com.android.server.wm.Task ) v3 ).getTopNonFinishingActivity ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
/* monitor-exit v0 */
/* .line 1077 */
} // .end local v1 # "wc":Lcom/android/server/wm/WindowContainer;
} // .end local v3 # "task":Lcom/android/server/wm/Task;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Float getAspectRatioByPackage ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 750 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 751 */
/* const v0, 0x3fdc3e06 */
/* .line 753 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 755 */
/* .local v0, "info":Landroid/sizecompat/AspectRatioInfo; */
v1 = v1 = this.mGameSettingConfig;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 756 */
v1 = this.mGameSettingConfig;
/* move-object v0, v1 */
/* check-cast v0, Landroid/sizecompat/AspectRatioInfo; */
/* .line 757 */
} // :cond_1
v1 = v1 = this.mSettingConfigs;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 758 */
v1 = this.mSettingConfigs;
/* move-object v0, v1 */
/* check-cast v0, Landroid/sizecompat/AspectRatioInfo; */
/* .line 760 */
} // :cond_2
v1 = this.mStaticSettingConfigs;
/* move-object v0, v1 */
/* check-cast v0, Landroid/sizecompat/AspectRatioInfo; */
/* .line 762 */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v1, v0, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F */
} // :cond_3
/* const/high16 v1, -0x40800000 # -1.0f */
} // :goto_1
} // .end method
private android.graphics.Bitmap getBitmap ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1351 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1353 */
/* .local v0, "bm":Landroid/graphics/Bitmap; */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getApplicationContext ( ); // invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
/* .line 1354 */
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 1355 */
/* .local v1, "packageManager":Landroid/content/pm/PackageManager; */
int v2 = 0; // const/4 v2, 0x0
(( android.content.pm.PackageManager ) v1 ).getApplicationInfo ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 1358 */
/* .local v2, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1359 */
(( android.content.pm.PackageManager ) v1 ).getApplicationIcon ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
/* .line 1360 */
/* .local v3, "d":Landroid/graphics/drawable/Drawable; */
/* move-object v4, v3 */
/* check-cast v4, Landroid/graphics/drawable/BitmapDrawable; */
/* .line 1361 */
/* .local v4, "bd":Landroid/graphics/drawable/BitmapDrawable; */
(( android.graphics.drawable.BitmapDrawable ) v4 ).getBitmap ( ); // invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v5 */
/* .line 1365 */
} // .end local v1 # "packageManager":Landroid/content/pm/PackageManager;
} // .end local v2 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v3 # "d":Landroid/graphics/drawable/Drawable;
} // .end local v4 # "bd":Landroid/graphics/drawable/BitmapDrawable;
} // :cond_0
/* .line 1363 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1364 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Package "; // const-string v3, "Package "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " not found, Ignoring."; // const-string v3, " not found, Ignoring."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiSizeCompatService"; // const-string v3, "MiuiSizeCompatService"
android.util.Log .e ( v3,v2 );
/* .line 1367 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
} // .end method
private com.android.server.wm.ActivityRecord getRestartAct ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 619 */
v0 = this.mAtms;
v0 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v0 ).getTopResumedActivity ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getTopResumedActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 620 */
/* .local v0, "top":Lcom/android/server/wm/ActivityRecord; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 621 */
} // :cond_0
v2 = this.packageName;
v2 = android.text.TextUtils .equals ( v2,p1 );
final String v3 = "Real restart for "; // const-string v3, "Real restart for "
final String v4 = "MiuiSizeCompatService"; // const-string v4, "MiuiSizeCompatService"
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 622 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.shortComponentName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v1 );
/* .line 623 */
/* .line 625 */
} // :cond_1
v2 = this.PERMISSION_ACTIVITY;
v5 = this.shortComponentName;
v2 = (( java.lang.String ) v2 ).equals ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
(( com.android.server.wm.ActivityRecord ) v0 ).getTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 626 */
(( com.android.server.wm.ActivityRecord ) v0 ).getTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.Task ) v2 ).getActivityBelow ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/Task;->getActivityBelow(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;
/* .line 627 */
/* .local v2, "below":Lcom/android/server/wm/ActivityRecord; */
if ( v2 != null) { // if-eqz v2, :cond_2
v5 = this.packageName;
v5 = (( java.lang.String ) p1 ).equals ( v5 ); // invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 628 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.shortComponentName;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v1 );
/* .line 629 */
/* .line 632 */
} // .end local v2 # "below":Lcom/android/server/wm/ActivityRecord;
} // :cond_2
} // .end method
private void initStaticSettings ( ) {
/* .locals 13 */
/* .line 478 */
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 479 */
/* .local v1, "start":J */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x1103006f */
(( android.content.res.Resources ) v0 ).getStringArray ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* move-object v6, v0 */
/* .line 480 */
/* .local v6, "ratioPackages":[Ljava/lang/String; */
if ( v6 != null) { // if-eqz v6, :cond_3
/* array-length v0, v6 */
/* if-nez v0, :cond_0 */
/* .line 483 */
} // :cond_0
/* array-length v0, v6 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v0, :cond_2 */
/* aget-object v5, v6, v4 */
/* .line 484 */
/* .local v5, "ratioPackage":Ljava/lang/String; */
final String v7 = ","; // const-string v7, ","
(( java.lang.String ) v5 ).split ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 485 */
/* .local v7, "split":[Ljava/lang/String; */
/* array-length v8, v7 */
int v9 = 2; // const/4 v9, 0x2
/* if-ne v8, v9, :cond_1 */
int v8 = 1; // const/4 v8, 0x1
/* aget-object v8, v7, v8 */
} // :cond_1
final String v8 = ""; // const-string v8, ""
} // :goto_1
v8 = (( com.android.server.wm.MiuiSizeCompatService ) p0 ).parseRatioFromStr ( v8 ); // invoke-virtual {p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService;->parseRatioFromStr(Ljava/lang/String;)F
/* .line 486 */
/* .local v8, "ratio":F */
v9 = this.mStaticSettingConfigs;
/* aget-object v10, v7, v3 */
/* new-instance v11, Landroid/sizecompat/AspectRatioInfo; */
/* aget-object v12, v7, v3 */
/* invoke-direct {v11, v12, v8}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V */
/* .line 483 */
/* nop */
} // .end local v5 # "ratioPackage":Ljava/lang/String;
} // .end local v7 # "split":[Ljava/lang/String;
} // .end local v8 # "ratio":F
/* add-int/lit8 v4, v4, 0x1 */
/* .line 488 */
} // :cond_2
final String v0 = "MiuiSizeCompatService"; // const-string v0, "MiuiSizeCompatService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Static setting configs: "; // const-string v4, "Static setting configs: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = v4 = this.mStaticSettingConfigs;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 489 */
/* const-wide/16 v3, 0x64 */
final String v5 = "initStaticSettings"; // const-string v5, "initStaticSettings"
/* move-object v0, p0 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 492 */
} // .end local v1 # "start":J
} // .end local v6 # "ratioPackages":[Ljava/lang/String;
/* .line 481 */
/* .restart local v1 # "start":J */
/* .restart local v6 # "ratioPackages":[Ljava/lang/String; */
} // :cond_3
} // :goto_2
return;
/* .line 490 */
} // .end local v1 # "start":J
} // .end local v6 # "ratioPackages":[Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 491 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 493 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
private Boolean isInRestartTaskActs ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .line 661 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.mActivityComponent;
/* if-nez v0, :cond_0 */
/* .line 664 */
} // :cond_0
v0 = com.android.server.wm.MiuiSizeCompatService.mRestartTaskActs;
v1 = this.mActivityComponent;
v0 = (( android.content.ComponentName ) v1 ).getClassName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 662 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void lambda$showConfirmDialog$0 ( com.android.server.wm.ActivityRecord p0, android.content.DialogInterface p1, Integer p2 ) { //synthethic
/* .locals 5 */
/* .param p1, "activity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "dialog" # Landroid/content/DialogInterface; */
/* .param p3, "which" # I */
/* .line 1098 */
v0 = this.mRestartAppDialog;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( miuix.appcompat.app.AlertDialog ) v0 ).isChecked ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isChecked()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1099 */
v0 = this.mBgHandler;
v1 = this.mAtms;
v1 = (( com.android.server.wm.ActivityTaskManagerService ) v1 ).getCurrentUserId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I
v2 = this.packageName;
int v3 = 3; // const/4 v3, 0x3
int v4 = 0; // const/4 v4, 0x0
(( com.android.server.wm.MiuiSizeCompatService$H ) v0 ).obtainMessage ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
(( com.android.server.wm.MiuiSizeCompatService$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$H;->sendMessage(Landroid/os/Message;)Z
/* .line 1101 */
} // :cond_0
this.mCurFullAct = p1;
/* .line 1102 */
return;
} // .end method
static void lambda$showConfirmDialog$1 ( android.content.DialogInterface p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "dialog" # Landroid/content/DialogInterface; */
/* .param p1, "which" # I */
/* .line 1108 */
return;
} // .end method
static void lambda$showConfirmDialog$2 ( android.content.DialogInterface p0 ) { //synthethic
/* .locals 0 */
/* .param p0, "dialog" # Landroid/content/DialogInterface; */
/* .line 1113 */
return;
} // .end method
private void lambda$showConfirmDialog$3 ( com.android.server.wm.ActivityRecord p0, android.content.DialogInterface p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "activity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "dialog" # Landroid/content/DialogInterface; */
/* .line 1115 */
v0 = this.mCurFullAct;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.packageName;
v1 = this.packageName;
v0 = android.text.TextUtils .equals ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1116 */
/* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda0;-><init>()V */
/* .line 1117 */
int v1 = 0; // const/4 v1, 0x0
java.lang.Float .valueOf ( v1 );
/* .line 1116 */
com.android.internal.util.function.pooled.PooledLambda .obtainMessage ( v0,p0,p1,v1 );
/* .line 1118 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mFgHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1120 */
} // .end local v0 # "message":Landroid/os/Message;
} // :cond_0
return;
} // .end method
private android.app.Notification$Action makeSizeCompatAction ( Integer p0 ) {
/* .locals 12 */
/* .param p1, "type" # I */
/* .line 1328 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* .line 1329 */
/* .local v6, "start":J */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* move-object v8, v0 */
/* .line 1330 */
/* .local v8, "intent":Landroid/content/Intent; */
final String v0 = "android"; // const-string v0, "android"
final String v1 = "android.sizecompat.SizeCompatChangeService"; // const-string v1, "android.sizecompat.SizeCompatChangeService"
(( android.content.Intent ) v8 ).setClassName ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1331 */
final String v0 = "extra_type"; // const-string v0, "extra_type"
(( android.content.Intent ) v8 ).putExtra ( v0, p1 ); // invoke-virtual {v8, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1332 */
v0 = this.mContext;
/* const/high16 v1, 0x4000000 */
android.app.PendingIntent .getService ( v0,p1,v8,v1 );
/* .line 1333 */
/* .local v9, "pendingIntent":Landroid/app/PendingIntent; */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
/* const v0, 0x110f032b */
/* .line 1334 */
} // :cond_0
/* const v0, 0x110f032c */
} // :goto_0
/* move v10, v0 */
/* .line 1335 */
/* .local v10, "titleResId":I */
/* new-instance v0, Landroid/app/Notification$Action$Builder; */
v1 = this.mUiContext;
/* .line 1336 */
/* const v2, 0x108055a */
android.graphics.drawable.Icon .createWithResource ( v1,v2 );
v2 = this.mUiContext;
/* .line 1337 */
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v2 ).getString ( v10 ); // invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* invoke-direct {v0, v1, v2, v9}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V */
(( android.app.Notification$Action$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;
/* .line 1338 */
/* .local v11, "action":Landroid/app/Notification$Action; */
/* const-wide/16 v3, 0x32 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "makeSizeCompatAction type: "; // const-string v1, "makeSizeCompatAction type: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v0, p0 */
/* move-wide v1, v6 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 1339 */
} // .end method
private void onSystemReady ( com.android.server.wm.ActivityTaskManagerService p0 ) {
/* .locals 2 */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 234 */
this.mAtms = p1;
/* .line 235 */
v0 = this.mWindowManager;
v0 = this.mActivityManager;
/* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
this.mAms = v0;
/* .line 236 */
v0 = this.mContext;
/* const-string/jumbo v1, "security" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Lmiui/security/SecurityManager; */
this.mSecurityManager = v0;
/* .line 237 */
v0 = this.mContext;
final String v1 = "notification"; // const-string v1, "notification"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/NotificationManager; */
this.mNotificationManager = v0;
/* .line 238 */
/* const-class v0, Lcom/android/server/statusbar/StatusBarManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/statusbar/StatusBarManagerInternal; */
this.mStatusBarService = v0;
/* .line 239 */
v0 = this.mUiContext;
this.mUiContext = v0;
/* .line 240 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
this.mPMS = v0;
/* .line 241 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->initStaticSettings()V */
/* .line 242 */
(( com.android.server.wm.MiuiSizeCompatService ) p0 ).registerUserSwitchReceiver ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->registerUserSwitchReceiver()V
/* .line 243 */
return;
} // .end method
private Float parseAspectRatio ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .line 1032 */
/* instance-of v0, p1, Ljava/lang/Float; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1033 */
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 1034 */
} // :cond_0
/* instance-of v0, p1, Ljava/lang/Double; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1035 */
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Double; */
v0 = (( java.lang.Double ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F
/* .line 1036 */
} // :cond_1
/* instance-of v0, p1, Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1037 */
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->floatValue()F
/* .line 1038 */
} // :cond_2
/* instance-of v0, p1, Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1039 */
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/String; */
v0 = java.lang.Float .parseFloat ( v0 );
/* .line 1041 */
} // :cond_3
final String v0 = "MiuiSizeCompatService"; // const-string v0, "MiuiSizeCompatService"
final String v1 = "The game aspect ratio is error format"; // const-string v1, "The game aspect ratio is error format"
android.util.Slog .e ( v0,v1 );
/* .line 1042 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void registerDataObserver ( ) {
/* .locals 4 */
/* .line 432 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->updateSettingConfigsFromCloud()V */
/* .line 433 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 434 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/wm/MiuiSizeCompatService$1; */
v3 = this.mBgHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/wm/MiuiSizeCompatService$1;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Landroid/os/Handler;)V */
/* .line 433 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 442 */
return;
} // .end method
private void restartProcessForRatioLocked ( com.android.server.wm.ActivityRecord p0, Float p1 ) {
/* .locals 11 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "aspectRatio" # F */
/* .line 588 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* .line 589 */
/* .local v6, "start":J */
if ( p1 != null) { // if-eqz p1, :cond_7
v0 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_7
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_4 */
/* .line 592 */
} // :cond_0
v8 = this.packageName;
/* .line 593 */
/* .local v8, "packageName":Ljava/lang/String; */
v0 = this.mSettingConfigs;
/* move-object v9, v0 */
/* check-cast v9, Landroid/sizecompat/AspectRatioInfo; */
/* .line 594 */
/* .local v9, "info":Landroid/sizecompat/AspectRatioInfo; */
/* if-nez v9, :cond_1 */
/* .line 595 */
int p2 = 0; // const/4 p2, 0x0
/* move v10, p2 */
/* .line 594 */
} // :cond_1
/* move v10, p2 */
/* .line 597 */
} // .end local p2 # "aspectRatio":F
/* .local v10, "aspectRatio":F */
} // :goto_0
final String p2 = "MiuiSizeCompatService"; // const-string p2, "MiuiSizeCompatService"
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Original ar is "; // const-string v1, "Original ar is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.shortComponentName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( p2,v0 );
/* .line 598 */
int p2 = 1; // const/4 p2, 0x1
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.wm.MiuiSizeCompatService ) p0 ).setMiuiSizeCompatRatio ( v8, v10, p2, v0 ); // invoke-virtual {p0, v8, v10, p2, v0}, Lcom/android/server/wm/MiuiSizeCompatService;->setMiuiSizeCompatRatio(Ljava/lang/String;FZZ)Z
/* .line 599 */
com.android.server.app.GameManagerServiceStub .getInstance ( );
(( com.android.server.app.GameManagerServiceStub ) p2 ).addSizeCompatApps ( v8 ); // invoke-virtual {p2, v8}, Lcom/android/server/app/GameManagerServiceStub;->addSizeCompatApps(Ljava/lang/String;)V
/* .line 601 */
p2 = this.mAtms;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 602 */
try { // :try_start_0
p2 = (( com.android.server.wm.ActivityRecord ) p1 ).isTopRunningActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isTopRunningActivity()Z
if ( p2 != null) { // if-eqz p2, :cond_2
/* move-object p2, p1 */
} // :cond_2
/* invoke-direct {p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService;->getRestartAct(Ljava/lang/String;)Lcom/android/server/wm/ActivityRecord; */
/* .line 603 */
/* .local p2, "restart":Lcom/android/server/wm/ActivityRecord; */
} // :goto_1
if ( p2 != null) { // if-eqz p2, :cond_5
/* .line 604 */
v1 = v1 = com.android.server.wm.MiuiSizeCompatService.mRestartTaskApps;
/* if-nez v1, :cond_4 */
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->isInRestartTaskActs(Lcom/android/server/wm/ActivityRecord;)Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 608 */
} // :cond_3
(( com.android.server.wm.ActivityRecord ) p2 ).restartProcessIfVisible ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->restartProcessIfVisible()V
/* .line 605 */
} // :cond_4
} // :goto_2
v1 = (( com.android.server.wm.ActivityRecord ) p2 ).getRootTaskId ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
/* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->restartTask(I)V */
/* .line 606 */
final String v1 = "MiuiSizeCompatService"; // const-string v1, "MiuiSizeCompatService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " is in restart task list ,so restart task instead."; // const-string v3, " is in restart task list ,so restart task instead."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 611 */
} // :cond_5
} // :goto_3
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 612 */
/* if-nez p2, :cond_6 */
/* .line 613 */
(( com.android.server.wm.MiuiSizeCompatService ) p0 ).removeRunningApp ( v8 ); // invoke-virtual {p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService;->removeRunningApp(Ljava/lang/String;)V
/* .line 615 */
} // :cond_6
/* const-wide/16 v3, 0xc8 */
final String v5 = "restartProcessForRatioLocked"; // const-string v5, "restartProcessForRatioLocked"
/* move-object v0, p0 */
/* move-wide v1, v6 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 616 */
return;
/* .line 611 */
} // .end local p2 # "restart":Lcom/android/server/wm/ActivityRecord;
/* :catchall_0 */
/* move-exception p2 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw p2 */
/* .line 590 */
} // .end local v8 # "packageName":Ljava/lang/String;
} // .end local v9 # "info":Landroid/sizecompat/AspectRatioInfo;
} // .end local v10 # "aspectRatio":F
/* .local p2, "aspectRatio":F */
} // :cond_7
} // :goto_4
return;
} // .end method
private void restartTask ( Integer p0 ) {
/* .locals 30 */
/* .param p1, "taskId" # I */
/* .line 636 */
/* move-object/from16 v0, p0 */
com.android.server.wm.WindowManagerService .boostPriorityForLockedSection ( );
/* .line 637 */
v1 = this.mAtms;
v1 = this.mRootWindowContainer;
/* move/from16 v2, p1 */
(( com.android.server.wm.RootWindowContainer ) v1 ).anyTaskForId ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(I)Lcom/android/server/wm/Task;
/* .line 638 */
/* .local v1, "task":Lcom/android/server/wm/Task; */
/* if-nez v1, :cond_0 */
/* .line 639 */
final String v3 = "MiuiSizeCompatService"; // const-string v3, "MiuiSizeCompatService"
final String v4 = "Restart task fail, task is null."; // const-string v4, "Restart task fail, task is null."
android.util.Slog .d ( v3,v4 );
/* .line 640 */
return;
/* .line 642 */
} // :cond_0
v3 = this.intent;
/* .local v3, "intent":Landroid/content/Intent; */
/* move-object v11, v3 */
/* .line 643 */
/* iget v4, v1, Lcom/android/server/wm/Task;->mCallingUid:I */
/* .local v4, "callingUid":I */
/* move v6, v4 */
/* .line 644 */
v24 = android.os.Binder .getCallingUid ( );
/* .local v24, "realCallingUid":I */
/* move/from16 v8, v24 */
/* .line 645 */
v25 = android.os.Binder .getCallingPid ( );
/* .local v25, "realCallingPid":I */
/* move/from16 v7, v25 */
/* .line 646 */
v15 = this.mCallingPackage;
/* .local v15, "callingPackage":Ljava/lang/String; */
/* move-object v9, v15 */
/* .line 647 */
v14 = this.mCallingFeatureId;
/* .local v14, "callingFeatureId":Ljava/lang/String; */
/* move-object v10, v14 */
/* .line 648 */
/* iget v13, v1, Lcom/android/server/wm/Task;->mUserId:I */
/* .local v13, "userId":I */
/* move/from16 v18, v13 */
/* .line 649 */
/* const-string/jumbo v5, "sizecompat" */
(( com.android.server.wm.Task ) v1 ).removeImmediately ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/wm/Task;->removeImmediately(Ljava/lang/String;)V
/* .line 650 */
android.app.ActivityOptions .makeBasic ( );
/* .line 651 */
/* .local v12, "option":Landroid/app/ActivityOptions; */
int v5 = 1; // const/4 v5, 0x1
(( android.app.ActivityOptions ) v12 ).setLaunchWindowingMode ( v5 ); // invoke-virtual {v12, v5}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V
/* .line 652 */
/* new-instance v5, Lcom/android/server/wm/SafeActivityOptions; */
/* invoke-direct {v5, v12}, Lcom/android/server/wm/SafeActivityOptions;-><init>(Landroid/app/ActivityOptions;)V */
/* move-object/from16 v17, v5 */
/* .line 653 */
/* .local v17, "options":Lcom/android/server/wm/SafeActivityOptions; */
v5 = this.mAtms;
(( com.android.server.wm.ActivityTaskManagerService ) v5 ).getActivityStartController ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getActivityStartController()Lcom/android/server/wm/ActivityStartController;
/* const/16 v16, 0x0 */
/* move-object/from16 v26, v12 */
} // .end local v12 # "option":Landroid/app/ActivityOptions;
/* .local v26, "option":Landroid/app/ActivityOptions; */
/* move-object/from16 v12, v16 */
/* move/from16 v27, v13 */
} // .end local v13 # "userId":I
/* .local v27, "userId":I */
/* move-object/from16 v13, v16 */
/* move-object/from16 v28, v14 */
} // .end local v14 # "callingFeatureId":Ljava/lang/String;
/* .local v28, "callingFeatureId":Ljava/lang/String; */
/* move-object/from16 v14, v16 */
/* const/16 v16, 0x0 */
/* move-object/from16 v29, v15 */
} // .end local v15 # "callingPackage":Ljava/lang/String;
/* .local v29, "callingPackage":Ljava/lang/String; */
/* move/from16 v15, v16 */
/* const/16 v19, 0x0 */
/* const-string/jumbo v20, "sizecompat" */
/* const/16 v21, 0x0 */
/* const/16 v22, 0x0 */
v23 = android.app.BackgroundStartPrivileges.ALLOW_BAL;
/* invoke-virtual/range {v5 ..v23}, Lcom/android/server/wm/ActivityStartController;->startActivityInPackage(IIILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILcom/android/server/wm/SafeActivityOptions;ILcom/android/server/wm/Task;Ljava/lang/String;ZLcom/android/server/am/PendingIntentRecord;Landroid/app/BackgroundStartPrivileges;)I */
/* .line 657 */
com.android.server.wm.WindowManagerService .resetPriorityAfterLockedSection ( );
/* .line 658 */
return;
} // .end method
private Boolean setMiuiSizeCompatScaleMode ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 11 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "scaleMode" # I */
/* .param p3, "write" # Z */
/* .line 831 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiSizeCompatService"; // const-string v2, "MiuiSizeCompatService"
/* if-nez v0, :cond_0 */
/* .line 832 */
final String v0 = "Set miui size compat ratio fail : service is not ready."; // const-string v0, "Set miui size compat ratio fail : service is not ready."
android.util.Slog .d ( v2,v0 );
/* .line 833 */
/* .line 835 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v9 */
/* .line 836 */
/* .local v9, "start":J */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 837 */
final String v0 = "Set miui size compat scale mode fail : pkgName is null."; // const-string v0, "Set miui size compat scale mode fail : pkgName is null."
android.util.Slog .d ( v2,v0 );
/* .line 838 */
/* .line 840 */
} // :cond_1
v0 = this.mSettingConfigs;
/* check-cast v0, Landroid/sizecompat/AspectRatioInfo; */
/* .line 841 */
/* .local v0, "info":Landroid/sizecompat/AspectRatioInfo; */
/* sget-boolean v3, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 842 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setMiuiSizeCompatScaleMode " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " scaleMode = "; // const-string v4, " scaleMode = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " from pid "; // const-string v4, " from pid "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 843 */
v4 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 842 */
android.util.Slog .d ( v2,v3 );
/* .line 845 */
} // :cond_2
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 846 */
(( android.sizecompat.AspectRatioInfo ) v0 ).setScaleMode ( p2 ); // invoke-virtual {v0, p2}, Landroid/sizecompat/AspectRatioInfo;->setScaleMode(I)V
/* .line 851 */
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 852 */
v1 = this.mBgHandler;
int v2 = 4; // const/4 v2, 0x4
(( com.android.server.wm.MiuiSizeCompatService$H ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.wm.MiuiSizeCompatService$H ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;->sendMessage(Landroid/os/Message;)Z
/* .line 854 */
} // :cond_3
/* const-wide/16 v6, 0x64 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setMiuiSizeCompatScaleMode, write: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v3, p0 */
/* move-wide v4, v9 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 856 */
int v1 = 1; // const/4 v1, 0x1
/* .line 848 */
} // :cond_4
/* const-string/jumbo v3, "should set ratio first, skip" */
android.util.Slog .w ( v2,v3 );
/* .line 849 */
} // .end method
private void showConfirmDialog ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 21 */
/* .param p1, "activity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1081 */
/* move-object/from16 v6, p0 */
/* move-object/from16 v7, p1 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v8 */
/* .line 1082 */
/* .local v8, "start":J */
final String v10 = "MiuiSizeCompatService"; // const-string v10, "MiuiSizeCompatService"
if ( v7 != null) { // if-eqz v7, :cond_4
v0 = this.packageName;
/* if-nez v0, :cond_0 */
/* goto/16 :goto_1 */
/* .line 1086 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mCurFullAct = v0;
/* .line 1087 */
v0 = this.mRestartAppDialog;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( miuix.appcompat.app.AlertDialog ) v0 ).isShowing ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1088 */
v0 = this.mRestartAppDialog;
(( miuix.appcompat.app.AlertDialog ) v0 ).dismiss ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V
/* .line 1090 */
} // :cond_1
v0 = this.mUiContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1091 */
final String v1 = "AlertDialog.Theme.DayNight"; // const-string v1, "AlertDialog.Theme.DayNight"
/* const-string/jumbo v2, "style" */
final String v3 = "miuix.stub"; // const-string v3, "miuix.stub"
v0 = (( android.content.res.Resources ) v0 ).getIdentifier ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 1092 */
/* .local v0, "themeId":I */
/* if-nez v0, :cond_2 */
/* .line 1093 */
/* const v0, 0x10302d1 */
/* move v11, v0 */
/* .line 1092 */
} // :cond_2
/* move v11, v0 */
/* .line 1095 */
} // .end local v0 # "themeId":I
/* .local v11, "themeId":I */
} // :goto_0
v0 = this.mUiContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
v1 = this.packageName;
/* .line 1096 */
(( com.android.server.wm.MiuiSizeCompatService ) v6 ).getAppNameByPkg ( v1 ); // invoke-virtual {v6, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->getAppNameByPkg(Ljava/lang/String;)Ljava/lang/String;
/* filled-new-array {v1}, [Ljava/lang/Object; */
/* const v2, 0x110f030f */
(( android.content.res.Resources ) v0 ).getString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
/* .line 1097 */
/* .local v12, "title":Ljava/lang/String; */
/* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, v6, v7}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;)V */
/* move-object v13, v0 */
/* .line 1103 */
/* .local v13, "positiveListener":Landroid/content/DialogInterface$OnClickListener; */
/* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda2; */
/* invoke-direct {v0}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda2;-><init>()V */
/* move-object v14, v0 */
/* .line 1109 */
/* .local v14, "negativeListener":Landroid/content/DialogInterface$OnClickListener; */
/* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda3; */
/* invoke-direct {v0}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda3;-><init>()V */
/* move-object v15, v0 */
/* .line 1114 */
/* .local v15, "cancelListener":Landroid/content/DialogInterface$OnCancelListener; */
/* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda4; */
/* invoke-direct {v0, v6, v7}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;)V */
/* move-object v5, v0 */
/* .line 1121 */
/* .local v5, "dismissListener":Landroid/content/DialogInterface$OnDismissListener; */
/* new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder; */
v1 = this.mUiContext;
/* invoke-direct {v0, v1, v11}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V */
/* .line 1122 */
int v1 = 1; // const/4 v1, 0x1
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setCancelable ( v1 ); // invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1123 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setTitle ( v12 ); // invoke-virtual {v0, v12}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;
v2 = this.mUiContext;
/* .line 1124 */
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1125 */
/* const v3, 0x110f032e */
(( android.content.res.Resources ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1124 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setMessage ( v2 ); // invoke-virtual {v0, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;
v2 = this.mUiContext;
/* .line 1126 */
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1127 */
/* const v3, 0x110f030b */
(( android.content.res.Resources ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1126 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setCheckBox ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCheckBox(ZLjava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;
v2 = this.mUiContext;
/* .line 1128 */
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1129 */
/* const v3, 0x110f030d */
(( android.content.res.Resources ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1128 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setPositiveButton ( v2, v13 ); // invoke-virtual {v0, v2, v13}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
v2 = this.mUiContext;
/* .line 1130 */
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1131 */
/* const v3, 0x110f030c */
(( android.content.res.Resources ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1130 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setNegativeButton ( v2, v14 ); // invoke-virtual {v0, v2, v14}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1132 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setOnCancelListener ( v15 ); // invoke-virtual {v0, v15}, Lmiuix/appcompat/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1133 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setOnDismissListener ( v5 ); // invoke-virtual {v0, v5}, Lmiuix/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1134 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).create ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;
this.mRestartAppDialog = v0;
/* .line 1135 */
(( miuix.appcompat.app.AlertDialog ) v0 ).getWindow ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v0 ).getAttributes ( ); // invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
/* .line 1136 */
/* .local v3, "attrs":Landroid/view/WindowManager$LayoutParams; */
(( android.view.WindowManager$LayoutParams ) v3 ).setTitle ( v12 ); // invoke-virtual {v3, v12}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 1137 */
/* const/16 v0, 0x7d3 */
/* iput v0, v3, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 1138 */
/* iget v0, v3, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* const/high16 v2, 0x20000 */
/* or-int/2addr v0, v2 */
/* iput v0, v3, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 1139 */
/* const/16 v0, 0x51 */
/* iput v0, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 1140 */
/* iget v0, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* or-int/lit16 v0, v0, 0x110 */
/* iput v0, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* .line 1142 */
v0 = this.mRestartAppDialog;
(( miuix.appcompat.app.AlertDialog ) v0 ).getWindow ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v0 ).setAttributes ( v3 ); // invoke-virtual {v0, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
/* .line 1144 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
/* if-nez v0, :cond_3 */
/* .line 1145 */
v0 = this.mRestartAppDialog;
(( miuix.appcompat.app.AlertDialog ) v0 ).show ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->show()V
/* .line 1149 */
} // :cond_3
v0 = this.mRestartAppDialog;
(( miuix.appcompat.app.AlertDialog ) v0 ).getWindow ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v0 ).getAttributes ( ); // invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
/* .line 1150 */
/* .local v4, "attributes":Landroid/view/WindowManager$LayoutParams; */
/* iput v1, v4, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 1152 */
v0 = this.mRestartAppDialog;
(( miuix.appcompat.app.AlertDialog ) v0 ).getWindow ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v0 ).setAttributes ( v4 ); // invoke-virtual {v0, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
/* .line 1153 */
/* const-wide/16 v16, 0xc8 */
/* const-string/jumbo v18, "showConfirmDialog" */
/* move-object/from16 v0, p0 */
/* move-wide v1, v8 */
/* move-object/from16 v19, v3 */
/* move-object/from16 v20, v4 */
} // .end local v3 # "attrs":Landroid/view/WindowManager$LayoutParams;
} // .end local v4 # "attributes":Landroid/view/WindowManager$LayoutParams;
/* .local v19, "attrs":Landroid/view/WindowManager$LayoutParams; */
/* .local v20, "attributes":Landroid/view/WindowManager$LayoutParams; */
/* move-wide/from16 v3, v16 */
/* move-object/from16 v16, v5 */
} // .end local v5 # "dismissListener":Landroid/content/DialogInterface$OnDismissListener;
/* .local v16, "dismissListener":Landroid/content/DialogInterface$OnDismissListener; */
/* move-object/from16 v5, v18 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 1154 */
final String v0 = "Show confirm dialog in size compat mode."; // const-string v0, "Show confirm dialog in size compat mode."
android.util.Slog .d ( v10,v0 );
/* .line 1155 */
return;
/* .line 1083 */
} // .end local v11 # "themeId":I
} // .end local v12 # "title":Ljava/lang/String;
} // .end local v13 # "positiveListener":Landroid/content/DialogInterface$OnClickListener;
} // .end local v14 # "negativeListener":Landroid/content/DialogInterface$OnClickListener;
} // .end local v15 # "cancelListener":Landroid/content/DialogInterface$OnCancelListener;
} // .end local v16 # "dismissListener":Landroid/content/DialogInterface$OnDismissListener;
} // .end local v19 # "attrs":Landroid/view/WindowManager$LayoutParams;
} // .end local v20 # "attributes":Landroid/view/WindowManager$LayoutParams;
} // :cond_4
} // :goto_1
final String v0 = "Show confirm dialog fail, activity is null."; // const-string v0, "Show confirm dialog fail, activity is null."
android.util.Slog .d ( v10,v0 );
/* .line 1084 */
return;
} // .end method
private void showWarningNotification ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 10 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1299 */
final String v0 = "WaringNotification"; // const-string v0, "WaringNotification"
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 1300 */
/* .local v2, "start":J */
v1 = this.mNotificationManager;
/* new-instance v4, Landroid/app/NotificationChannel; */
final String v5 = "MiuiSizeCompat"; // const-string v5, "MiuiSizeCompat"
int v6 = 4; // const/4 v6, 0x4
/* invoke-direct {v4, v0, v5, v6}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V */
(( android.app.NotificationManager ) v1 ).createNotificationChannel ( v4 ); // invoke-virtual {v1, v4}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V
/* .line 1302 */
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
/* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap; */
/* move-object v7, v1 */
/* .line 1303 */
/* .local v7, "bitmap":Landroid/graphics/Bitmap; */
v1 = this.mUiContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110f032a */
(( android.content.res.Resources ) v1 ).getString ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* move-object v8, v1 */
/* .line 1304 */
/* .local v8, "content":Ljava/lang/String; */
v1 = this.mUiContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110f032d */
(( android.content.res.Resources ) v1 ).getString ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* move-object v9, v1 */
/* .line 1305 */
/* .local v9, "title":Ljava/lang/String; */
/* new-instance v1, Landroid/app/Notification$Builder; */
v4 = this.mContext;
/* invoke-direct {v1, v4, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 1306 */
/* const v0, 0x108055a */
(( android.app.Notification$Builder ) v1 ).setSmallIcon ( v0 ); // invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
/* .line 1307 */
(( android.app.Notification$Builder ) v0 ).setContentText ( v8 ); // invoke-virtual {v0, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 1308 */
(( android.app.Notification$Builder ) v0 ).setContentTitle ( v9 ); // invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 1309 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->makeSizeCompatAction(I)Landroid/app/Notification$Action; */
(( android.app.Notification$Builder ) v0 ).addAction ( v4 ); // invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;
/* .line 1310 */
int v4 = 2; // const/4 v4, 0x2
/* invoke-direct {p0, v4}, Lcom/android/server/wm/MiuiSizeCompatService;->makeSizeCompatAction(I)Landroid/app/Notification$Action; */
(( android.app.Notification$Builder ) v0 ).addAction ( v4 ); // invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;
/* .line 1311 */
int v4 = 0; // const/4 v4, 0x0
(( android.app.Notification$Builder ) v0 ).setOngoing ( v4 ); // invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;
/* .line 1312 */
(( android.app.Notification$Builder ) v0 ).setAutoCancel ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;
(( android.app.Notification$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
/* .line 1313 */
/* .local v0, "notification":Landroid/app/Notification; */
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 1314 */
v4 = this.extras;
final String v5 = "miui.appIcon"; // const-string v5, "miui.appIcon"
android.graphics.drawable.Icon .createWithBitmap ( v7 );
(( android.os.Bundle ) v4 ).putParcelable ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
/* .line 1316 */
} // :cond_0
v4 = this.mNotificationManager;
/* const/16 v5, 0x7d0 */
(( android.app.NotificationManager ) v4 ).notify ( v5, v0 ); // invoke-virtual {v4, v5, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
/* .line 1317 */
/* iput-boolean v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationShowing:Z */
/* .line 1318 */
v1 = this.mAppObserver;
miui.process.ProcessManager .registerForegroundInfoListener ( v1 );
/* .line 1319 */
v1 = this.mAtms;
v1 = this.mWindowManager;
v4 = this.mFoldObserver;
(( com.android.server.wm.WindowManagerService ) v1 ).registerDisplayFoldListener ( v4 ); // invoke-virtual {v1, v4}, Lcom/android/server/wm/WindowManagerService;->registerDisplayFoldListener(Landroid/view/IDisplayFoldListener;)V
/* .line 1320 */
final String v1 = "MiuiSizeCompatService"; // const-string v1, "MiuiSizeCompatService"
final String v4 = "Show warning notification in size compat mode."; // const-string v4, "Show warning notification in size compat mode."
android.util.Slog .d ( v1,v4 );
/* .line 1321 */
/* const-wide/16 v4, 0xc8 */
/* const-string/jumbo v6, "showWarningNotification" */
/* move-object v1, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1324 */
} // .end local v0 # "notification":Landroid/app/Notification;
} // .end local v2 # "start":J
} // .end local v7 # "bitmap":Landroid/graphics/Bitmap;
} // .end local v8 # "content":Ljava/lang/String;
} // .end local v9 # "title":Ljava/lang/String;
/* .line 1322 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1323 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1325 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void updateSettingConfigsFromCloud ( ) {
/* .locals 12 */
/* .line 445 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* .line 447 */
/* .local v6, "start":J */
try { // :try_start_0
v0 = this.mContext;
/* .line 448 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mCompatModeModuleName;
final String v2 = "app_continuity_o_fixratiolist"; // const-string v2, "app_continuity_o_fixratiolist"
/* .line 447 */
int v3 = 0; // const/4 v3, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v1,v2,v3 );
/* .line 449 */
/* .local v0, "data":Ljava/lang/String; */
final String v1 = "MiuiSizeCompatService"; // const-string v1, "MiuiSizeCompatService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getListFromCloud: data: "; // const-string v3, "getListFromCloud: data: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 450 */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 451 */
return;
/* .line 453 */
} // :cond_0
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 454 */
/* .local v1, "apps":Lorg/json/JSONArray; */
v2 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-gtz v2, :cond_1 */
/* .line 455 */
return;
/* .line 457 */
} // :cond_1
v2 = this.mStaticSettingConfigs;
/* .line 458 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-ge v2, v3, :cond_4 */
/* .line 459 */
(( org.json.JSONArray ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 460 */
/* .local v3, "app":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 461 */
/* .line 463 */
} // :cond_2
final String v4 = ","; // const-string v4, ","
(( java.lang.String ) v3 ).split ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 464 */
/* .local v4, "split":[Ljava/lang/String; */
/* array-length v5, v4 */
int v8 = 2; // const/4 v8, 0x2
/* if-ne v5, v8, :cond_3 */
int v5 = 1; // const/4 v5, 0x1
/* aget-object v5, v4, v5 */
} // :cond_3
final String v5 = ""; // const-string v5, ""
} // :goto_1
v5 = (( com.android.server.wm.MiuiSizeCompatService ) p0 ).parseRatioFromStr ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiSizeCompatService;->parseRatioFromStr(Ljava/lang/String;)F
/* .line 465 */
/* .local v5, "ratio":F */
v8 = this.mStaticSettingConfigs;
int v9 = 0; // const/4 v9, 0x0
/* aget-object v10, v4, v9 */
/* new-instance v11, Landroid/sizecompat/AspectRatioInfo; */
/* aget-object v9, v4, v9 */
/* invoke-direct {v11, v9, v5}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 458 */
} // .end local v3 # "app":Ljava/lang/String;
} // .end local v4 # "split":[Ljava/lang/String;
} // .end local v5 # "ratio":F
} // :goto_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 469 */
} // .end local v0 # "data":Ljava/lang/String;
} // .end local v1 # "apps":Lorg/json/JSONArray;
} // .end local v2 # "i":I
} // :cond_4
/* .line 467 */
/* :catch_0 */
/* move-exception v0 */
/* .line 468 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 470 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
/* const-wide/16 v3, 0x64 */
/* const-string/jumbo v5, "updateSettingConfigsFromCloud" */
/* move-object v0, p0 */
/* move-wide v1, v6 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 471 */
return;
} // .end method
/* # virtual methods */
public void cancelSizeCompatNotification ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 966 */
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 967 */
/* .local v1, "start":J */
v0 = this.mStatusBarService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 968 */
/* .line 970 */
} // :cond_0
v0 = this.mNotificationManager;
/* const/16 v3, 0x7d0 */
(( android.app.NotificationManager ) v0 ).cancel ( v3 ); // invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V
/* .line 971 */
v0 = this.mAppObserver;
miui.process.ProcessManager .unregisterForegroundInfoListener ( v0 );
/* .line 972 */
v0 = this.mAtms;
v0 = this.mWindowManager;
v3 = this.mFoldObserver;
(( com.android.server.wm.WindowManagerService ) v0 ).unregisterDisplayFoldListener ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/wm/WindowManagerService;->unregisterDisplayFoldListener(Landroid/view/IDisplayFoldListener;)V
/* .line 973 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationShowing:Z */
/* .line 974 */
int v0 = 0; // const/4 v0, 0x0
this.mCurFullAct = v0;
/* .line 975 */
/* const-wide/16 v3, 0x64 */
final String v5 = "cancelSizeCompatNotification"; // const-string v5, "cancelSizeCompatNotification"
/* move-object v0, p0 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 976 */
final String v0 = "MiuiSizeCompatService"; // const-string v0, "MiuiSizeCompatService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Cancel warning notification for "; // const-string v4, "Cancel warning notification for "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 979 */
/* nop */
} // .end local v1 # "start":J
/* .line 977 */
/* :catch_0 */
/* move-exception v0 */
/* .line 978 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 980 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public java.lang.String getAppNameByPkg ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1158 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 1160 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
int v1 = 1; // const/4 v1, 0x1
try { // :try_start_0
(( android.content.pm.PackageManager ) v0 ).getApplicationInfo ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 1161 */
/* .local v1, "info":Landroid/content/pm/ApplicationInfo; */
(( android.content.pm.ApplicationInfo ) v1 ).loadLabel ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1162 */
} // .end local v1 # "info":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v1 */
/* .line 1163 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " may not be installed."; // const-string v3, " may not be installed."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiSizeCompatService"; // const-string v3, "MiuiSizeCompatService"
android.util.Slog .d ( v3,v2 );
/* .line 1165 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // .end method
public Float getLastAspectRatioValue ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1173 */
v0 = this.mSettingConfigs;
/* check-cast v0, Landroid/sizecompat/AspectRatioInfo; */
/* .line 1174 */
/* .local v0, "info":Landroid/sizecompat/AspectRatioInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v1, v0, Landroid/sizecompat/AspectRatioInfo;->mLastAspectRatio:F */
} // :cond_0
/* const/high16 v1, -0x40800000 # -1.0f */
} // :goto_0
} // .end method
public java.util.Map getMiuiGameSizeCompatEnabledApps ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Landroid/sizecompat/AspectRatioInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1048 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* .line 1049 */
/* .local v6, "start":J */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* move-object v8, v0 */
/* .line 1050 */
/* .local v8, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;" */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z */
/* if-nez v0, :cond_0 */
/* .line 1051 */
final String v0 = "MiuiSizeCompatService"; // const-string v0, "MiuiSizeCompatService"
final String v1 = "Get miui game size compat enableds apps fail : service is not ready."; // const-string v1, "Get miui game size compat enableds apps fail : service is not ready."
android.util.Slog .d ( v0,v1 );
/* .line 1052 */
/* .line 1054 */
} // :cond_0
v0 = this.mGameSettingConfig;
/* new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$7; */
/* invoke-direct {v1, p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService$7;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/util/Map;)V */
/* .line 1060 */
/* const-wide/16 v3, 0xc8 */
final String v5 = "getMiuiGameSizeCompatList"; // const-string v5, "getMiuiGameSizeCompatList"
/* move-object v0, p0 */
/* move-wide v1, v6 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 1061 */
} // .end method
public Float getMiuiSizeCompatAppRatio ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1020 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* const/high16 v1, -0x40800000 # -1.0f */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1021 */
final String v0 = "MiuiSizeCompatService"; // const-string v0, "MiuiSizeCompatService"
final String v2 = "pkgName can not be empty"; // const-string v2, "pkgName can not be empty"
android.util.Slog .e ( v0,v2 );
/* .line 1022 */
/* .line 1024 */
} // :cond_0
v0 = this.mSettingConfigs;
/* check-cast v0, Landroid/sizecompat/AspectRatioInfo; */
/* .line 1025 */
/* .local v0, "info":Landroid/sizecompat/AspectRatioInfo; */
/* if-nez v0, :cond_1 */
/* .line 1026 */
v2 = this.mStaticSettingConfigs;
/* move-object v0, v2 */
/* check-cast v0, Landroid/sizecompat/AspectRatioInfo; */
/* .line 1028 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget v1, v0, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F */
} // :cond_2
} // .end method
public java.util.Map getMiuiSizeCompatEnabledApps ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Landroid/sizecompat/AspectRatioInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 727 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* .line 728 */
/* .local v6, "start":J */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* move-object v8, v0 */
/* .line 729 */
/* .local v8, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;" */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z */
/* if-nez v0, :cond_0 */
/* .line 730 */
final String v0 = "MiuiSizeCompatService"; // const-string v0, "MiuiSizeCompatService"
final String v1 = "Get miui size compat enableds apps fail : service is not ready."; // const-string v1, "Get miui size compat enableds apps fail : service is not ready."
android.util.Slog .d ( v0,v1 );
/* .line 731 */
/* .line 733 */
} // :cond_0
v0 = this.mStaticSettingConfigs;
/* new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$5; */
/* invoke-direct {v1, p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService$5;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/util/Map;)V */
/* .line 739 */
v0 = this.mSettingConfigs;
/* new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$6; */
/* invoke-direct {v1, p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService$6;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/util/Map;)V */
/* .line 745 */
/* const-wide/16 v3, 0xc8 */
final String v5 = "getMiuiSizeCompatEnabledApps"; // const-string v5, "getMiuiSizeCompatEnabledApps"
/* move-object v0, p0 */
/* move-wide v1, v6 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 746 */
} // .end method
public java.util.Map getMiuiSizeCompatInstalledApps ( ) {
/* .locals 11 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Landroid/sizecompat/AspectRatioInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 767 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* .line 768 */
/* .local v6, "start":J */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* move-object v8, v0 */
/* .line 769 */
/* .local v8, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;" */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z */
/* if-nez v0, :cond_0 */
/* .line 770 */
final String v0 = "MiuiSizeCompatService"; // const-string v0, "MiuiSizeCompatService"
final String v1 = "Get miui size compat installed apps fail : service is not ready."; // const-string v1, "Get miui size compat installed apps fail : service is not ready."
android.util.Slog .d ( v0,v1 );
/* .line 771 */
/* .line 773 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 774 */
/* .local v9, "packageManager":Landroid/content/pm/PackageManager; */
/* const/16 v0, 0x40 */
(( android.content.pm.PackageManager ) v9 ).getInstalledPackages ( v0 ); // invoke-virtual {v9, v0}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;
/* .line 775 */
/* .local v10, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_5
/* check-cast v1, Landroid/content/pm/PackageInfo; */
/* .line 777 */
/* .local v1, "object":Landroid/content/pm/PackageInfo; */
try { // :try_start_0
v2 = this.applicationInfo;
v2 = (( com.android.server.wm.MiuiSizeCompatService ) p0 ).isSystemApp ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService;->isSystemApp(Landroid/content/pm/ApplicationInfo;)Z
/* if-nez v2, :cond_4 */
v2 = this.packageName;
v2 = (( com.android.server.wm.MiuiSizeCompatService ) p0 ).pkgHasIcon ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService;->pkgHasIcon(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
v2 = this.APPLIACTION_APP_BLACK_LIST;
v3 = this.packageName;
v2 = /* .line 778 */
/* if-nez v2, :cond_4 */
/* .line 779 */
v2 = this.applicationInfo;
v2 = this.packageName;
/* .line 780 */
/* .local v2, "packageName":Ljava/lang/String; */
v3 = this.applicationInfo;
(( android.content.pm.ApplicationInfo ) v3 ).loadLabel ( v9 ); // invoke-virtual {v3, v9}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
/* check-cast v3, Ljava/lang/String; */
/* .line 782 */
/* .local v3, "label":Ljava/lang/String; */
v4 = v4 = this.mSettingConfigs;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 783 */
v4 = this.mSettingConfigs;
/* check-cast v4, Landroid/sizecompat/AspectRatioInfo; */
/* .local v4, "info":Landroid/sizecompat/AspectRatioInfo; */
/* .line 784 */
} // .end local v4 # "info":Landroid/sizecompat/AspectRatioInfo;
} // :cond_1
v4 = v4 = this.mStaticSettingConfigs;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 785 */
v4 = this.mStaticSettingConfigs;
/* check-cast v4, Landroid/sizecompat/AspectRatioInfo; */
/* .restart local v4 # "info":Landroid/sizecompat/AspectRatioInfo; */
/* .line 787 */
} // .end local v4 # "info":Landroid/sizecompat/AspectRatioInfo;
} // :cond_2
/* new-instance v4, Landroid/sizecompat/AspectRatioInfo; */
/* const/high16 v5, -0x40800000 # -1.0f */
/* invoke-direct {v4, v2, v5}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V */
/* .line 789 */
/* .restart local v4 # "info":Landroid/sizecompat/AspectRatioInfo; */
} // :goto_1
v5 = this.mApplicationName;
v5 = android.text.TextUtils .isEmpty ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 790 */
this.mApplicationName = v3;
/* .line 792 */
} // :cond_3
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 796 */
} // .end local v2 # "packageName":Ljava/lang/String;
} // .end local v3 # "label":Ljava/lang/String;
} // .end local v4 # "info":Landroid/sizecompat/AspectRatioInfo;
} // :cond_4
/* .line 794 */
/* :catch_0 */
/* move-exception v2 */
/* .line 795 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 797 */
} // .end local v1 # "object":Landroid/content/pm/PackageInfo;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
/* .line 798 */
} // :cond_5
/* const-wide/16 v3, 0x190 */
final String v5 = "getMiuiSizeCompatInstalledApps"; // const-string v5, "getMiuiSizeCompatInstalledApps"
/* move-object v0, p0 */
/* move-wide v1, v6 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 799 */
} // .end method
public void initBlackAppList ( ) {
/* .locals 2 */
/* .line 424 */
v0 = this.APPLIACTION_APP_BLACK_LIST;
final String v1 = "com.android.email"; // const-string v1, "com.android.email"
/* .line 425 */
v0 = this.APPLIACTION_APP_BLACK_LIST;
final String v1 = "com.android.soundrecorder"; // const-string v1, "com.android.soundrecorder"
/* .line 426 */
v0 = this.APPLIACTION_APP_BLACK_LIST;
final String v1 = "com.duokan.phone.remotecontroller"; // const-string v1, "com.duokan.phone.remotecontroller"
/* .line 427 */
v0 = this.APPLIACTION_APP_BLACK_LIST;
final String v1 = "com.mi.health"; // const-string v1, "com.mi.health"
/* .line 428 */
v0 = this.APPLIACTION_APP_BLACK_LIST;
final String v1 = "com.duokan.reader"; // const-string v1, "com.duokan.reader"
/* .line 429 */
return;
} // .end method
public void initSettingsDirForUser ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "userId" # I */
/* .line 1178 */
final String v0 = "MiuiSizeCompatService"; // const-string v0, "MiuiSizeCompatService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "system/users/" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1179 */
/* .local v1, "userSettingsDir":Ljava/lang/String; */
/* new-instance v2, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 1181 */
/* .local v2, "systemDir":Ljava/io/File; */
try { // :try_start_0
v3 = (( java.io.File ) v2 ).mkdirs ( ); // invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
/* if-nez v3, :cond_0 */
/* .line 1182 */
final String v3 = "Making dir failed"; // const-string v3, "Making dir failed"
android.util.Slog .e ( v0,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1186 */
} // :cond_0
/* .line 1184 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1185 */
/* .local v3, "e":Ljava/lang/SecurityException; */
final String v4 = "Exception throw while Making dir"; // const-string v4, "Exception throw while Making dir"
android.util.Slog .e ( v0,v4 );
/* .line 1188 */
} // .end local v3 # "e":Ljava/lang/SecurityException;
} // :goto_0
(( java.io.File ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;
/* const/16 v3, 0x1fd */
int v4 = -1; // const/4 v4, -0x1
android.os.FileUtils .setPermissions ( v0,v3,v4,v4 );
/* .line 1189 */
/* new-instance v0, Ljava/io/File; */
/* const-string/jumbo v3, "size_compat_setting_config.xml" */
/* invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mSettingFilename = v0;
/* .line 1190 */
/* new-instance v0, Ljava/io/File; */
/* const-string/jumbo v3, "size_compat_setting_config-backup.xml" */
/* invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mBackupSettingFilename = v0;
/* .line 1191 */
return;
} // .end method
Boolean isSizeCompatDisabled ( ) {
/* .locals 3 */
/* .line 1169 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "force_resizable_activities"; // const-string v1, "force_resizable_activities"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
} // .end method
public Boolean isSystemApp ( android.content.pm.ApplicationInfo p0 ) {
/* .locals 3 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .line 803 */
/* iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I */
int v1 = 1; // const/4 v1, 0x1
/* and-int/2addr v0, v1 */
/* if-gtz v0, :cond_1 */
/* iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* const/16 v2, 0x2710 */
/* if-lt v0, v2, :cond_1 */
v0 = this.packageName;
/* .line 804 */
final String v2 = "com.miui"; // const-string v2, "com.miui"
v0 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
v0 = this.packageName;
final String v2 = "com.xiaomi"; // const-string v2, "com.xiaomi"
v0 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
/* nop */
/* .line 803 */
} // :goto_1
} // .end method
public void onShellCommand ( java.io.FileDescriptor p0, java.io.FileDescriptor p1, java.io.FileDescriptor p2, java.lang.String[] p3, android.os.ShellCallback p4, android.os.ResultReceiver p5 ) {
/* .locals 8 */
/* .param p1, "in" # Ljava/io/FileDescriptor; */
/* .param p2, "out" # Ljava/io/FileDescriptor; */
/* .param p3, "err" # Ljava/io/FileDescriptor; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .param p5, "callback" # Landroid/os/ShellCallback; */
/* .param p6, "resultReceiver" # Landroid/os/ResultReceiver; */
/* .line 248 */
/* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$Shell; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/MiuiSizeCompatService$Shell-IA;)V */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move-object v6, p5 */
/* move-object v7, p6 */
/* invoke-virtual/range {v0 ..v7}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I */
/* .line 249 */
return;
} // .end method
public void onUserSwitch ( ) {
/* .locals 2 */
/* .line 715 */
v0 = this.mAtms;
v0 = (( com.android.server.wm.ActivityTaskManagerService ) v0 ).getCurrentUserId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I
(( com.android.server.wm.MiuiSizeCompatService ) p0 ).initSettingsDirForUser ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiSizeCompatService;->initSettingsDirForUser(I)V
/* .line 716 */
v0 = this.mSettingConfigs;
/* .line 717 */
(( com.android.server.wm.MiuiSizeCompatService ) p0 ).readSetting ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->readSetting()V
/* .line 718 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 719 */
int v1 = 1; // const/4 v1, 0x1
try { // :try_start_0
/* iput-boolean v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z */
/* .line 720 */
v1 = this.mLock;
(( java.lang.Object ) v1 ).notifyAll ( ); // invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
/* .line 721 */
/* monitor-exit v0 */
/* .line 722 */
return;
/* .line 721 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Float parseRatioFromStr ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "value" # Ljava/lang/String; */
/* .line 496 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 497 */
/* const v0, 0x3fdc3e06 */
/* .line 499 */
} // :cond_0
/* const v0, 0x3fe38e39 */
/* .line 500 */
/* .local v0, "ratio":F */
v1 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v1, :cond_3 */
/* .line 501 */
final String v1 = "-1"; // const-string v1, "-1"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 502 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* .line 503 */
} // :cond_1
final String v1 = "4:3"; // const-string v1, "4:3"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 504 */
/* const v0, 0x3faaaaaa */
/* .line 506 */
} // :cond_2
final String v1 = ":"; // const-string v1, ":"
(( java.lang.String ) p1 ).split ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 507 */
/* .local v1, "values":[Ljava/lang/String; */
/* array-length v2, v1 */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_3 */
/* .line 508 */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v2, v1, v2 */
v2 = java.lang.Float .parseFloat ( v2 );
/* .line 509 */
/* .local v2, "width":F */
int v3 = 1; // const/4 v3, 0x1
/* aget-object v3, v1, v3 */
v3 = java.lang.Float .parseFloat ( v3 );
/* .line 510 */
/* .local v3, "height":F */
/* div-float v0, v2, v3 */
/* .line 514 */
} // .end local v1 # "values":[Ljava/lang/String;
} // .end local v2 # "width":F
} // .end local v3 # "height":F
} // :cond_3
} // :goto_0
} // .end method
public Boolean pkgHasIcon ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 808 */
v0 = this.mContext;
final String v1 = "launcherapps"; // const-string v1, "launcherapps"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/content/pm/LauncherApps; */
/* .line 809 */
/* .local v0, "launcherApps":Landroid/content/pm/LauncherApps; */
v1 = android.os.UserHandle.SYSTEM;
(( android.content.pm.LauncherApps ) v0 ).getActivityList ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/pm/LauncherApps;->getActivityList(Ljava/lang/String;Landroid/os/UserHandle;)Ljava/util/List;
/* .line 810 */
v2 = /* .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LauncherActivityInfo;>;" */
/* xor-int/lit8 v2, v2, 0x1 */
} // .end method
public void readSetting ( ) {
/* .locals 14 */
/* .line 1371 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1372 */
/* .local v0, "settingsFileStream":Ljava/io/FileInputStream; */
v1 = this.mBackupSettingFilename;
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
final String v2 = "MiuiSizeCompatService"; // const-string v2, "MiuiSizeCompatService"
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1374 */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
v3 = this.mBackupSettingFilename;
/* invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v0, v1 */
/* .line 1375 */
v1 = this.mSettingFilename;
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1376 */
final String v1 = "Cleaning up size_compat_setting_config.xml"; // const-string v1, "Cleaning up size_compat_setting_config.xml"
android.util.Slog .v ( v2,v1 );
/* .line 1377 */
v1 = this.mSettingFilename;
(( java.io.File ) v1 ).delete ( ); // invoke-virtual {v1}, Ljava/io/File;->delete()Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1381 */
} // :cond_0
/* .line 1379 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1380 */
/* .local v1, "e":Ljava/io/IOException; */
/* const-string/jumbo v3, "size_compat_setting_config-backup.xml load config: " */
android.util.Slog .e ( v2,v3,v1 );
/* .line 1384 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_0
/* if-nez v0, :cond_3 */
/* .line 1385 */
v1 = this.mSettingFilename;
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_2 */
/* .line 1386 */
/* const-string/jumbo v1, "size_compat_setting_config.xml not found" */
android.util.Slog .v ( v2,v1 );
/* .line 1387 */
return;
/* .line 1390 */
} // :cond_2
try { // :try_start_1
/* new-instance v1, Ljava/io/FileInputStream; */
v3 = this.mSettingFilename;
/* invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object v0, v1 */
/* .line 1393 */
/* .line 1391 */
/* :catch_1 */
/* move-exception v1 */
/* .line 1392 */
/* .restart local v1 # "e":Ljava/io/IOException; */
/* const-string/jumbo v3, "size_compat_setting_config.xml load config: " */
android.util.Slog .e ( v2,v3,v1 );
/* .line 1397 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_3
} // :goto_1
/* const-string/jumbo v1, "size_compat_setting_config.xmlload config: " */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 1398 */
try { // :try_start_2
android.util.Xml .newPullParser ( );
/* .line 1399 */
/* .local v3, "xmlParser":Lorg/xmlpull/v1/XmlPullParser; */
int v4 = 0; // const/4 v4, 0x0
v5 = /* .line 1400 */
/* .line 1402 */
/* .local v5, "xmlEventType":I */
} // :goto_2
int v6 = 1; // const/4 v6, 0x1
/* if-ne v5, v6, :cond_4 */
/* .line 1403 */
/* goto/16 :goto_4 */
/* .line 1405 */
} // :cond_4
int v6 = 2; // const/4 v6, 0x2
/* if-ne v5, v6, :cond_6 */
/* const-string/jumbo v6, "setting" */
/* .line 1406 */
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 1407 */
final String v6 = "name"; // const-string v6, "name"
/* .line 1409 */
/* .local v6, "packageName":Ljava/lang/String; */
final String v7 = "displayName"; // const-string v7, "displayName"
/* .line 1411 */
/* .local v7, "applicationName":Ljava/lang/String; */
final String v8 = "aspectRatio"; // const-string v8, "aspectRatio"
/* .line 1413 */
/* .local v8, "aspectRatio":Ljava/lang/String; */
final String v9 = "gravity"; // const-string v9, "gravity"
/* .line 1415 */
/* .local v9, "gravity":Ljava/lang/String; */
final String v10 = "scaleMode"; // const-string v10, "scaleMode"
/* .line 1417 */
/* .local v10, "scaleMode":Ljava/lang/String; */
v11 = this.mSettingConfigs;
/* new-instance v12, Landroid/sizecompat/AspectRatioInfo$Builder; */
/* invoke-direct {v12}, Landroid/sizecompat/AspectRatioInfo$Builder;-><init>()V */
/* .line 1418 */
(( android.sizecompat.AspectRatioInfo$Builder ) v12 ).setPackageName ( v6 ); // invoke-virtual {v12, v6}, Landroid/sizecompat/AspectRatioInfo$Builder;->setPackageName(Ljava/lang/String;)Landroid/sizecompat/AspectRatioInfo$Builder;
/* .line 1419 */
(( android.sizecompat.AspectRatioInfo$Builder ) v12 ).setApplicationName ( v7 ); // invoke-virtual {v12, v7}, Landroid/sizecompat/AspectRatioInfo$Builder;->setApplicationName(Ljava/lang/String;)Landroid/sizecompat/AspectRatioInfo$Builder;
/* .line 1420 */
v13 = java.lang.Float .parseFloat ( v8 );
java.lang.Float .valueOf ( v13 );
(( android.sizecompat.AspectRatioInfo$Builder ) v12 ).setAspectRatio ( v13 ); // invoke-virtual {v12, v13}, Landroid/sizecompat/AspectRatioInfo$Builder;->setAspectRatio(Ljava/lang/Float;)Landroid/sizecompat/AspectRatioInfo$Builder;
/* .line 1421 */
v13 = java.lang.Integer .parseInt ( v9 );
(( android.sizecompat.AspectRatioInfo$Builder ) v12 ).setGravity ( v13 ); // invoke-virtual {v12, v13}, Landroid/sizecompat/AspectRatioInfo$Builder;->setGravity(I)Landroid/sizecompat/AspectRatioInfo$Builder;
/* .line 1422 */
v13 = android.sizecompat.AspectRatioInfo .isValidScaleModeStr ( v10 );
if ( v13 != null) { // if-eqz v13, :cond_5
/* .line 1423 */
v13 = java.lang.Integer .parseInt ( v10 );
} // :cond_5
int v13 = 0; // const/4 v13, 0x0
/* .line 1422 */
} // :goto_3
(( android.sizecompat.AspectRatioInfo$Builder ) v12 ).setScaleMode ( v13 ); // invoke-virtual {v12, v13}, Landroid/sizecompat/AspectRatioInfo$Builder;->setScaleMode(I)Landroid/sizecompat/AspectRatioInfo$Builder;
/* .line 1424 */
(( android.sizecompat.AspectRatioInfo$Builder ) v12 ).build ( ); // invoke-virtual {v12}, Landroid/sizecompat/AspectRatioInfo$Builder;->build()Landroid/sizecompat/AspectRatioInfo;
/* .line 1417 */
/* .line 1426 */
} // .end local v6 # "packageName":Ljava/lang/String;
} // .end local v7 # "applicationName":Ljava/lang/String;
} // .end local v8 # "aspectRatio":Ljava/lang/String;
} // .end local v9 # "gravity":Ljava/lang/String;
} // .end local v10 # "scaleMode":Ljava/lang/String;
v6 = } // :cond_6
/* move v5, v6 */
/* .line 1436 */
} // .end local v3 # "xmlParser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v5 # "xmlEventType":I
/* :catchall_0 */
/* move-exception v3 */
/* .line 1434 */
/* :catch_2 */
/* move-exception v3 */
/* .line 1432 */
/* :catch_3 */
/* move-exception v3 */
/* .line 1430 */
/* :catch_4 */
/* move-exception v3 */
/* .line 1429 */
} // :cond_7
} // :goto_4
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Setting configs: "; // const-string v4, "Setting configs: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = v4 = this.mSettingConfigs;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* :try_end_2 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_2 ..:try_end_2} :catch_4 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 ..:try_end_2} :catch_3 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1437 */
/* .local v3, "e":Ljava/lang/Throwable; */
} // :goto_5
android.util.Slog .e ( v2,v1,v3 );
/* .line 1435 */
/* .local v3, "e":Ljava/io/IOException; */
} // :goto_6
android.util.Slog .e ( v2,v1,v3 );
} // .end local v3 # "e":Ljava/io/IOException;
/* .line 1433 */
/* .local v3, "e":Lorg/xmlpull/v1/XmlPullParserException; */
} // :goto_7
android.util.Slog .e ( v2,v1,v3 );
} // .end local v3 # "e":Lorg/xmlpull/v1/XmlPullParserException;
/* .line 1431 */
/* .local v3, "e":Ljava/io/FileNotFoundException; */
} // :goto_8
android.util.Slog .e ( v2,v1,v3 );
/* .line 1438 */
} // .end local v3 # "e":Ljava/io/FileNotFoundException;
} // :goto_9
/* nop */
/* .line 1440 */
} // :goto_a
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 1442 */
try { // :try_start_3
(( java.io.FileInputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_5 */
/* .line 1446 */
/* .line 1443 */
/* :catch_5 */
/* move-exception v1 */
/* .line 1444 */
/* .restart local v1 # "e":Ljava/io/IOException; */
/* const-string/jumbo v3, "size_compat_setting_config.xmlload config:IO Exception while closing stream" */
android.util.Slog .e ( v2,v3,v1 );
/* .line 1448 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_8
} // :goto_b
return;
} // .end method
void registerUserSwitchReceiver ( ) {
/* .locals 4 */
/* .line 693 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 694 */
/* .local v0, "userSwitchFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.USER_SWITCHED"; // const-string v1, "android.intent.action.USER_SWITCHED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 695 */
final String v1 = "android.intent.action.USER_UNLOCKED"; // const-string v1, "android.intent.action.USER_UNLOCKED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 696 */
v1 = this.mContext;
/* new-instance v2, Lcom/android/server/wm/MiuiSizeCompatService$4; */
/* invoke-direct {v2, p0}, Lcom/android/server/wm/MiuiSizeCompatService$4;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;)V */
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 712 */
return;
} // .end method
public void removeRunningApp ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 918 */
v0 = this.mPMS;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 919 */
v1 = this.mAtms;
v1 = (( com.android.server.wm.ActivityTaskManagerService ) v1 ).getCurrentUserId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I
/* const-string/jumbo v2, "size compat change" */
(( com.miui.server.process.ProcessManagerInternal ) v0 ).forceStopPackage ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 921 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " Force stop "; // const-string v1, " Force stop "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " due to sizecompatRatio change"; // const-string v1, " due to sizecompatRatio change"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiSizeCompatService"; // const-string v1, "MiuiSizeCompatService"
android.util.Slog .i ( v1,v0 );
/* .line 922 */
return;
} // .end method
public void restorePrevRatio ( ) {
/* .locals 3 */
/* .line 956 */
v0 = this.mCurFullAct;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationShowing:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 957 */
/* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda0;-><init>()V */
v1 = this.mCurFullAct;
v2 = this.packageName;
/* .line 958 */
v2 = (( com.android.server.wm.MiuiSizeCompatService ) p0 ).getLastAspectRatioValue ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService;->getLastAspectRatioValue(Ljava/lang/String;)F
java.lang.Float .valueOf ( v2 );
/* .line 957 */
com.android.internal.util.function.pooled.PooledLambda .obtainMessage ( v0,p0,v1,v2 );
/* .line 959 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mFgHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 961 */
} // .end local v0 # "message":Landroid/os/Message;
} // :cond_0
return;
} // .end method
public Boolean setMiuiGameSizeCompatList ( java.lang.String p0, Boolean p1 ) {
/* .locals 11 */
/* .param p1, "json" # Ljava/lang/String; */
/* .param p2, "override" # Z */
/* .line 985 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "MiuiSizeCompatService"; // const-string v1, "MiuiSizeCompatService"
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 986 */
try { // :try_start_0
v2 = this.mGameSettingConfig;
/* .line 988 */
} // :cond_0
v2 = android.text.TextUtils .isEmpty ( p1 );
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 989 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "The game json list is empty, override is "; // const-string v4, "The game json list is empty, override is "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 990 */
/* .line 992 */
} // :cond_1
/* new-instance v2, Lorg/json/JSONArray; */
/* invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 993 */
/* .local v2, "jsonArray":Lorg/json/JSONArray; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v4, v5, :cond_3 */
/* .line 994 */
(( org.json.JSONArray ) v2 ).get ( v4 ); // invoke-virtual {v2, v4}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
/* check-cast v5, Lorg/json/JSONObject; */
/* .line 995 */
/* .local v5, "object":Lorg/json/JSONObject; */
final String v6 = "pkgName"; // const-string v6, "pkgName"
(( org.json.JSONObject ) v5 ).get ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/String; */
/* .line 996 */
/* .local v6, "pkgName":Ljava/lang/String; */
final String v7 = "gravity"; // const-string v7, "gravity"
(( org.json.JSONObject ) v5 ).get ( v7 ); // invoke-virtual {v5, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 997 */
/* .local v7, "gravity":I */
v8 = android.sizecompat.AspectRatioInfo .isGravityEffect ( v7 );
/* if-nez v8, :cond_2 */
/* .line 998 */
final String v8 = "The gravity is not effect, use default center gravity instead."; // const-string v8, "The gravity is not effect, use default center gravity instead."
android.util.Slog .e ( v1,v8 );
/* .line 999 */
/* const/16 v7, 0x11 */
/* .line 1001 */
} // :cond_2
final String v8 = "aspectRatio"; // const-string v8, "aspectRatio"
(( org.json.JSONObject ) v5 ).get ( v8 ); // invoke-virtual {v5, v8}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
v8 = /* invoke-direct {p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService;->parseAspectRatio(Ljava/lang/Object;)F */
/* .line 1002 */
/* .local v8, "ratio":F */
v9 = this.mGameSettingConfig;
/* new-instance v10, Landroid/sizecompat/AspectRatioInfo; */
/* invoke-direct {v10, v6, v8, v7}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;FI)V */
/* .line 993 */
/* nop */
} // .end local v5 # "object":Lorg/json/JSONObject;
} // .end local v6 # "pkgName":Ljava/lang/String;
} // .end local v7 # "gravity":I
} // .end local v8 # "ratio":F
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1004 */
} // .end local v4 # "i":I
} // :cond_3
/* sget-boolean v4, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 1005 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setMiuiGameSizeCompatList\uff1a" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v4 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1014 */
} // .end local v2 # "jsonArray":Lorg/json/JSONArray;
} // :cond_4
/* nop */
/* .line 1015 */
/* .line 1011 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1012 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1013 */
/* .line 1007 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v2 */
/* .line 1008 */
/* .local v2, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V
/* .line 1009 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "The game list json is error format.\n"; // const-string v4, "The game list json is error format.\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 1010 */
} // .end method
public Boolean setMiuiSizeCompatEnabled ( java.lang.String p0, android.sizecompat.AspectRatioInfo p1 ) {
/* .locals 11 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "info" # Landroid/sizecompat/AspectRatioInfo; */
/* .line 815 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiSizeCompatService"; // const-string v2, "MiuiSizeCompatService"
/* if-nez v0, :cond_0 */
/* .line 816 */
final String v0 = "Set miui size compat enabled fail: service is not ready."; // const-string v0, "Set miui size compat enabled fail: service is not ready."
android.util.Slog .d ( v2,v0 );
/* .line 817 */
/* .line 819 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v9 */
/* .line 820 */
/* .local v9, "start":J */
if ( p2 != null) { // if-eqz p2, :cond_2
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 824 */
} // :cond_1
v0 = this.mSettingConfigs;
/* .line 825 */
(( com.android.server.wm.MiuiSizeCompatService ) p0 ).writeSetting ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->writeSetting()V
/* .line 826 */
/* const-wide/16 v6, 0x12c */
/* const-string/jumbo v8, "setMiuiSizeCompatEnabled" */
/* move-object v3, p0 */
/* move-wide v4, v9 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 827 */
int v0 = 1; // const/4 v0, 0x1
/* .line 821 */
} // :cond_2
} // :goto_0
final String v0 = "Set miui size compat enabled fail: App pkgName or aspect info is null."; // const-string v0, "Set miui size compat enabled fail: App pkgName or aspect info is null."
android.util.Slog .d ( v2,v0 );
/* .line 822 */
} // .end method
public Boolean setMiuiSizeCompatRatio ( java.lang.String p0, Float p1, Boolean p2, Boolean p3 ) {
/* .locals 16 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "ratio" # F */
/* .param p3, "write" # Z */
/* .param p4, "kill" # Z */
/* .line 861 */
/* move-object/from16 v6, p0 */
/* move-object/from16 v7, p1 */
/* move/from16 v8, p2 */
/* move/from16 v9, p3 */
/* move/from16 v10, p4 */
/* iget-boolean v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiSizeCompatService"; // const-string v2, "MiuiSizeCompatService"
/* if-nez v0, :cond_0 */
/* .line 862 */
final String v0 = "Set miui size compat ratio fail : service is not ready."; // const-string v0, "Set miui size compat ratio fail : service is not ready."
android.util.Slog .d ( v2,v0 );
/* .line 863 */
/* .line 865 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v11 */
/* .line 866 */
/* .local v11, "start":J */
v13 = android.os.Binder .getCallingPid ( );
/* .line 867 */
/* .local v13, "callingPid":I */
v14 = android.os.Binder .getCallingUid ( );
/* .line 868 */
/* .local v14, "callingUid":I */
v0 = /* invoke-static/range {p1 ..p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 869 */
final String v0 = "Set miui size compat ratio fail : pkgName is null."; // const-string v0, "Set miui size compat ratio fail : pkgName is null."
android.util.Slog .d ( v2,v0 );
/* .line 870 */
/* .line 872 */
} // :cond_1
v0 = /* invoke-direct {v6, v13, v14}, Lcom/android/server/wm/MiuiSizeCompatService;->checkInterfaceAccess(II)Z */
/* if-nez v0, :cond_2 */
/* .line 873 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Do not allow pid= "; // const-string v3, "Do not allow pid= "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " uid= "; // const-string v3, " uid= "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " to change miui size compat ratio."; // const-string v3, " to change miui size compat ratio."
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v0 );
/* .line 875 */
/* .line 877 */
} // :cond_2
/* sget-boolean v0, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z */
/* if-nez v0, :cond_3 */
v0 = android.os.Process .myPid ( );
/* if-eq v13, v0, :cond_4 */
/* .line 878 */
} // :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setMiuiSizeCompatRatio " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " ratio = "; // const-string v1, " ratio = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = " from pid "; // const-string v1, " from pid "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 881 */
} // :cond_4
v0 = this.mSettingConfigs;
/* check-cast v0, Landroid/sizecompat/AspectRatioInfo; */
/* .line 882 */
/* .local v0, "info":Landroid/sizecompat/AspectRatioInfo; */
/* if-nez v0, :cond_6 */
/* .line 883 */
v1 = this.mStaticSettingConfigs;
/* check-cast v1, Landroid/sizecompat/AspectRatioInfo; */
/* .line 884 */
/* .local v1, "staticInfo":Landroid/sizecompat/AspectRatioInfo; */
if ( v1 != null) { // if-eqz v1, :cond_5
/* new-instance v2, Landroid/sizecompat/AspectRatioInfo; */
/* iget v3, v1, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F */
/* invoke-direct {v2, v7, v3}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V */
/* .line 885 */
} // :cond_5
/* new-instance v2, Landroid/sizecompat/AspectRatioInfo; */
/* const/high16 v3, -0x40800000 # -1.0f */
/* invoke-direct {v2, v7, v3}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V */
} // :goto_0
/* move-object v0, v2 */
/* .line 886 */
v2 = this.mSettingConfigs;
/* move-object v15, v0 */
/* .line 882 */
} // .end local v1 # "staticInfo":Landroid/sizecompat/AspectRatioInfo;
} // :cond_6
/* move-object v15, v0 */
/* .line 888 */
} // .end local v0 # "info":Landroid/sizecompat/AspectRatioInfo;
/* .local v15, "info":Landroid/sizecompat/AspectRatioInfo; */
} // :goto_1
(( android.sizecompat.AspectRatioInfo ) v15 ).setAspectRatio ( v8 ); // invoke-virtual {v15, v8}, Landroid/sizecompat/AspectRatioInfo;->setAspectRatio(F)V
/* .line 889 */
if ( v9 != null) { // if-eqz v9, :cond_7
/* .line 890 */
v0 = this.mBgHandler;
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.wm.MiuiSizeCompatService$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.wm.MiuiSizeCompatService$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$H;->sendMessage(Landroid/os/Message;)Z
/* .line 892 */
} // :cond_7
if ( v10 != null) { // if-eqz v10, :cond_8
/* .line 893 */
/* invoke-virtual/range {p0 ..p1}, Lcom/android/server/wm/MiuiSizeCompatService;->removeRunningApp(Ljava/lang/String;)V */
/* .line 894 */
com.android.server.app.GameManagerServiceStub .getInstance ( );
(( com.android.server.app.GameManagerServiceStub ) v0 ).toggleDownscaleForStopedApp ( v7 ); // invoke-virtual {v0, v7}, Lcom/android/server/app/GameManagerServiceStub;->toggleDownscaleForStopedApp(Ljava/lang/String;)V
/* .line 896 */
} // :cond_8
/* const-wide/16 v3, 0x64 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setMiuiSizeCompatRatio, write: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " , kill= "; // const-string v1, " , kill= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object/from16 v0, p0 */
/* move-wide v1, v11 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 898 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean switchToFullscreen ( android.window.WindowContainerToken p0 ) {
/* .locals 10 */
/* .param p1, "token" # Landroid/window/WindowContainerToken; */
/* .line 926 */
final String v0 = "restartTopActivityProcessIfVisible()"; // const-string v0, "restartTopActivityProcessIfVisible()"
com.android.server.wm.ActivityTaskManagerService .enforceTaskPermission ( v0 );
/* .line 928 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 929 */
/* .local v2, "start":J */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->getActivityFromTokenLocked(Landroid/window/WindowContainerToken;)Lcom/android/server/wm/ActivityRecord; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v7, v1 */
/* .line 930 */
/* .local v7, "activity":Lcom/android/server/wm/ActivityRecord; */
final String v1 = "MiuiSizeCompatService"; // const-string v1, "MiuiSizeCompatService"
if ( v7 != null) { // if-eqz v7, :cond_3
try { // :try_start_1
v4 = this.packageName;
/* if-nez v4, :cond_0 */
/* .line 934 */
} // :cond_0
v4 = this.mGameSettingConfig;
v5 = this.packageName;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 935 */
final String v4 = "Game app can not switch to fullscreen by systemui button."; // const-string v4, "Game app can not switch to fullscreen by systemui button."
android.util.Slog .e ( v1,v4 );
/* .line 936 */
/* .line 938 */
} // :cond_1
v1 = this.mSecurityManager;
v4 = this.packageName;
v1 = (( miui.security.SecurityManager ) v1 ).isScRelaunchNeedConfirm ( v4 ); // invoke-virtual {v1, v4}, Lmiui/security/SecurityManager;->isScRelaunchNeedConfirm(Ljava/lang/String;)Z
/* move v8, v1 */
/* .line 939 */
/* .local v8, "confirm":Z */
int v9 = 1; // const/4 v9, 0x1
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 940 */
v1 = this.mUiHandler;
(( com.android.server.wm.MiuiSizeCompatService$UiHandler ) v1 ).obtainMessage ( v9, v7 ); // invoke-virtual {v1, v9, v7}, Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( com.android.server.wm.MiuiSizeCompatService$UiHandler ) v1 ).sendMessage ( v4 ); // invoke-virtual {v1, v4}, Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 943 */
} // :cond_2
this.mCurFullAct = v7;
/* .line 944 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v7, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->restartProcessForRatioLocked(Lcom/android/server/wm/ActivityRecord;F)V */
/* .line 946 */
} // :goto_0
/* const-wide/16 v4, 0xc8 */
/* const-string/jumbo v6, "switchToFullscreen" */
/* move-object v1, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* .line 947 */
/* .line 931 */
} // .end local v8 # "confirm":Z
} // :cond_3
} // :goto_1
final String v4 = "Could not resolve activity from token"; // const-string v4, "Could not resolve activity from token"
android.util.Slog .e ( v1,v4 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 932 */
/* .line 948 */
} // .end local v2 # "start":J
} // .end local v7 # "activity":Lcom/android/server/wm/ActivityRecord;
/* :catch_0 */
/* move-exception v1 */
/* .line 949 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 950 */
} // .end method
public void writeSetting ( ) {
/* .locals 15 */
/* .line 1452 */
/* const-string/jumbo v0, "setting" */
/* const-string/jumbo v1, "setting_config" */
int v2 = 0; // const/4 v2, 0x0
/* .line 1453 */
/* .local v2, "settingsBufferedOutputStream":Ljava/io/BufferedOutputStream; */
int v3 = 0; // const/4 v3, 0x0
/* .line 1454 */
/* .local v3, "settingsFileStream":Ljava/io/FileOutputStream; */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v10 */
/* .line 1455 */
/* .local v10, "startTime":J */
v4 = this.mSettingFilename;
v4 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
final String v12 = "MiuiSizeCompatService"; // const-string v12, "MiuiSizeCompatService"
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1456 */
v4 = this.mBackupSettingFilename;
v4 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1457 */
v4 = this.mSettingFilename;
(( java.io.File ) v4 ).delete ( ); // invoke-virtual {v4}, Ljava/io/File;->delete()Z
/* .line 1458 */
/* const-string/jumbo v4, "size_compat_setting_config.xml delete old file" */
android.util.Slog .v ( v12,v4 );
/* .line 1459 */
} // :cond_0
v4 = this.mSettingFilename;
v5 = this.mBackupSettingFilename;
v4 = (( java.io.File ) v4 ).renameTo ( v5 ); // invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
/* if-nez v4, :cond_1 */
/* .line 1460 */
final String v0 = "Unable to backup size_compat_setting_config, current changes will be lost at reboot"; // const-string v0, "Unable to backup size_compat_setting_config, current changes will be lost at reboot"
android.util.Slog .e ( v12,v0 );
/* .line 1462 */
return;
/* .line 1467 */
} // :cond_1
} // :goto_0
try { // :try_start_0
/* new-instance v4, Ljava/io/FileOutputStream; */
v5 = this.mSettingFilename;
/* invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v3, v4 */
/* .line 1468 */
/* new-instance v4, Ljava/io/BufferedOutputStream; */
/* invoke-direct {v4, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V */
/* move-object v2, v4 */
/* .line 1469 */
/* new-instance v4, Lcom/android/internal/util/FastXmlSerializer; */
/* invoke-direct {v4}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V */
/* move-object v13, v4 */
/* .line 1470 */
/* .local v13, "serializer":Lcom/android/internal/util/FastXmlSerializer; */
v4 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v4 ).name ( ); // invoke-virtual {v4}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
(( com.android.internal.util.FastXmlSerializer ) v13 ).setOutput ( v2, v4 ); // invoke-virtual {v13, v2, v4}, Lcom/android/internal/util/FastXmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V
/* .line 1471 */
int v4 = 1; // const/4 v4, 0x1
java.lang.Boolean .valueOf ( v4 );
int v6 = 0; // const/4 v6, 0x0
(( com.android.internal.util.FastXmlSerializer ) v13 ).startDocument ( v6, v5 ); // invoke-virtual {v13, v6, v5}, Lcom/android/internal/util/FastXmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V
/* .line 1472 */
final String v5 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v5, "http://xmlpull.org/v1/doc/features.html#indent-output"
(( com.android.internal.util.FastXmlSerializer ) v13 ).setFeature ( v5, v4 ); // invoke-virtual {v13, v5, v4}, Lcom/android/internal/util/FastXmlSerializer;->setFeature(Ljava/lang/String;Z)V
/* .line 1473 */
(( com.android.internal.util.FastXmlSerializer ) v13 ).startTag ( v6, v1 ); // invoke-virtual {v13, v6, v1}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
/* .line 1474 */
v4 = this.mSettingConfigs;
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_4
/* check-cast v5, Ljava/lang/String; */
/* .line 1475 */
/* .local v5, "pkg":Ljava/lang/String; */
v7 = this.mSettingConfigs;
/* check-cast v7, Landroid/sizecompat/AspectRatioInfo; */
/* .line 1476 */
/* .local v7, "info":Landroid/sizecompat/AspectRatioInfo; */
/* if-nez v7, :cond_2 */
/* .line 1477 */
final String v8 = "Info is null when write settings."; // const-string v8, "Info is null when write settings."
android.util.Slog .e ( v12,v8 );
/* .line 1478 */
/* .line 1480 */
} // :cond_2
(( com.android.internal.util.FastXmlSerializer ) v13 ).startTag ( v6, v0 ); // invoke-virtual {v13, v6, v0}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
/* .line 1481 */
final String v8 = "name"; // const-string v8, "name"
(( com.android.internal.util.FastXmlSerializer ) v13 ).attribute ( v6, v8, v5 ); // invoke-virtual {v13, v6, v8, v5}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
/* .line 1482 */
v8 = this.mApplicationName;
/* if-nez v8, :cond_3 */
final String v8 = ""; // const-string v8, ""
} // :cond_3
v8 = this.mApplicationName;
/* .line 1483 */
/* .local v8, "applicationName":Ljava/lang/String; */
} // :goto_2
final String v9 = "displayName"; // const-string v9, "displayName"
(( com.android.internal.util.FastXmlSerializer ) v13 ).attribute ( v6, v9, v8 ); // invoke-virtual {v13, v6, v9, v8}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
/* .line 1484 */
final String v9 = "aspectRatio"; // const-string v9, "aspectRatio"
/* iget v14, v7, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F */
/* .line 1485 */
java.lang.String .valueOf ( v14 );
/* .line 1484 */
(( com.android.internal.util.FastXmlSerializer ) v13 ).attribute ( v6, v9, v14 ); // invoke-virtual {v13, v6, v9, v14}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
/* .line 1486 */
final String v9 = "gravity"; // const-string v9, "gravity"
/* iget v14, v7, Landroid/sizecompat/AspectRatioInfo;->mGravity:I */
/* .line 1487 */
java.lang.String .valueOf ( v14 );
/* .line 1486 */
(( com.android.internal.util.FastXmlSerializer ) v13 ).attribute ( v6, v9, v14 ); // invoke-virtual {v13, v6, v9, v14}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
/* .line 1488 */
final String v9 = "scaleMode"; // const-string v9, "scaleMode"
/* .line 1489 */
v14 = (( android.sizecompat.AspectRatioInfo ) v7 ).getScaleMode ( ); // invoke-virtual {v7}, Landroid/sizecompat/AspectRatioInfo;->getScaleMode()I
java.lang.String .valueOf ( v14 );
/* .line 1488 */
(( com.android.internal.util.FastXmlSerializer ) v13 ).attribute ( v6, v9, v14 ); // invoke-virtual {v13, v6, v9, v14}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
/* .line 1490 */
(( com.android.internal.util.FastXmlSerializer ) v13 ).endTag ( v6, v0 ); // invoke-virtual {v13, v6, v0}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
/* .line 1491 */
/* nop */
} // .end local v5 # "pkg":Ljava/lang/String;
} // .end local v7 # "info":Landroid/sizecompat/AspectRatioInfo;
} // .end local v8 # "applicationName":Ljava/lang/String;
/* .line 1493 */
} // :cond_4
(( com.android.internal.util.FastXmlSerializer ) v13 ).endTag ( v6, v1 ); // invoke-virtual {v13, v6, v1}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
/* .line 1494 */
(( com.android.internal.util.FastXmlSerializer ) v13 ).endDocument ( ); // invoke-virtual {v13}, Lcom/android/internal/util/FastXmlSerializer;->endDocument()V
/* .line 1495 */
(( java.io.BufferedOutputStream ) v2 ).flush ( ); // invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V
/* .line 1496 */
android.os.FileUtils .sync ( v3 );
/* .line 1497 */
v0 = this.mBackupSettingFilename;
(( java.io.File ) v0 ).delete ( ); // invoke-virtual {v0}, Ljava/io/File;->delete()Z
/* .line 1498 */
v0 = this.mSettingFilename;
(( java.io.File ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;
/* const/16 v1, 0x1b0 */
int v4 = -1; // const/4 v4, -0x1
android.os.FileUtils .setPermissions ( v0,v1,v4,v4 );
/* .line 1499 */
/* const-wide/16 v7, 0xc8 */
/* const-string/jumbo v9, "write size_compat_setting_config.xml" */
/* move-object v4, p0 */
/* move-wide v5, v10 */
/* invoke-direct/range {v4 ..v9}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1507 */
} // .end local v13 # "serializer":Lcom/android/internal/util/FastXmlSerializer;
/* :catchall_0 */
/* move-exception v0 */
/* .line 1500 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1501 */
/* .local v0, "e":Ljava/io/IOException; */
try { // :try_start_1
v1 = this.mSettingFilename;
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_5
v1 = this.mSettingFilename;
v1 = (( java.io.File ) v1 ).delete ( ); // invoke-virtual {v1}, Ljava/io/File;->delete()Z
/* if-nez v1, :cond_5 */
/* .line 1502 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to clean up mangled file: "; // const-string v4, "Failed to clean up mangled file: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mSettingFilename;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v12,v1 );
/* .line 1504 */
} // :cond_5
final String v1 = "Unable to write host recognize settings,current changes will be lost at reboot"; // const-string v1, "Unable to write host recognize settings,current changes will be lost at reboot"
android.util.Slog .e ( v12,v1,v0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1507 */
/* nop */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_3
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 1508 */
libcore.io.IoUtils .closeQuietly ( v2 );
/* .line 1509 */
/* nop */
/* .line 1510 */
return;
/* .line 1507 */
} // :goto_4
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 1508 */
libcore.io.IoUtils .closeQuietly ( v2 );
/* .line 1509 */
/* throw v0 */
} // .end method
