.class Lcom/android/server/wm/ActivityStarterInjector;
.super Ljava/lang/Object;
.source "ActivityStarterInjector.java"


# static fields
.field public static final FLAG_ASSOCIATED_SETTINGS_AV:I = 0x8000000

.field private static final TAG:Ljava/lang/String; = "ActivityStarter"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkFreeformSupport(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/app/ActivityOptions;)V
    .locals 2
    .param p0, "service"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p1, "options"    # Landroid/app/ActivityOptions;

    .line 44
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z

    if-nez v0, :cond_2

    if-eqz p1, :cond_0

    .line 46
    invoke-virtual {p1}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 48
    invoke-static {v0}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    invoke-interface {v0}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->showOpenMiuiOptimizationToast()V

    .line 58
    :cond_2
    return-void
.end method

.method public static getLastFrame(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 202
    const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 203
    const-string v0, "com.tencent.mm/.plugin.voip.ui.VideoActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 204
    const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.AVActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.AVLoadingDialogActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    const-string v0, "com.android.incallui/.InCallActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    const-string v0, "com.google.android.dialer/com.android.incallui.InCallActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 208
    const-string/jumbo v0, "voipcalling.VoipActivityV2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 211
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 209
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private static getLastFreeFormActivityRecord(Ljava/util/List;)Lcom/android/server/wm/ActivityRecord;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/wm/ActivityRecord;",
            ">;)",
            "Lcom/android/server/wm/ActivityRecord;"
        }
    .end annotation

    .line 241
    .local p0, "activities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 242
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 243
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityRecord;

    return-object v0

    .line 245
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private static isStartedInMiuiSetttingVirtualDispaly(Lcom/android/server/wm/RootWindowContainer;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Z
    .locals 4
    .param p0, "root"    # Lcom/android/server/wm/RootWindowContainer;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "aInfo"    # Landroid/content/pm/ActivityInfo;

    .line 231
    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 232
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/wm/RootWindowContainer;->findActivity(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Z)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 233
    .local v1, "result":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getMiuiFlags()I

    move-result v2

    const/high16 v3, 0x8000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    .line 234
    const/4 v0, 0x1

    return v0

    .line 237
    .end local v1    # "result":Lcom/android/server/wm/ActivityRecord;
    :cond_0
    return v0
.end method

.method public static modifyLaunchActivityOptionIfNeed(Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/RootWindowContainer;Ljava/lang/String;Landroid/app/ActivityOptions;Lcom/android/server/wm/WindowProcessController;Landroid/content/Intent;ILandroid/content/pm/ActivityInfo;Lcom/android/server/wm/ActivityRecord;)Landroid/app/ActivityOptions;
    .locals 14
    .param p0, "service"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p1, "root"    # Lcom/android/server/wm/RootWindowContainer;
    .param p2, "callingPackgae"    # Ljava/lang/String;
    .param p3, "options"    # Landroid/app/ActivityOptions;
    .param p4, "callerApp"    # Lcom/android/server/wm/WindowProcessController;
    .param p5, "intent"    # Landroid/content/Intent;
    .param p6, "userId"    # I
    .param p7, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p8, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 167
    move-object/from16 v8, p3

    move-object/from16 v9, p5

    move-object v10, p1

    move-object/from16 v11, p7

    invoke-static {p1, v9, v11}, Lcom/android/server/wm/ActivityStarterInjector;->isStartedInMiuiSetttingVirtualDispaly(Lcom/android/server/wm/RootWindowContainer;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-eqz v8, :cond_0

    .line 168
    invoke-virtual/range {p3 .. p3}, Landroid/app/ActivityOptions;->getLaunchDisplayId()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 169
    invoke-virtual {v8, v1}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;

    .line 171
    :cond_0
    const/4 v12, 0x0

    .line 172
    .local v12, "pcRun":Z
    if-nez v12, :cond_1

    if-eqz v9, :cond_1

    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/util/MiuiMultiWindowAdapter;->START_FROM_FREEFORM_BLACK_LIST_ACTIVITY:Ljava/util/List;

    .line 174
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177
    return-object v8

    .line 179
    :cond_1
    const/4 v0, 0x0

    .line 180
    .local v0, "ar":Lcom/android/server/wm/ActivityRecord;
    invoke-virtual {p1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 181
    invoke-virtual {p1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 182
    .local v2, "topTask":Lcom/android/server/wm/Task;
    if-eqz v2, :cond_2

    .line 183
    invoke-virtual {v2, v1, v3}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    move-object v13, v0

    goto :goto_0

    .line 186
    .end local v2    # "topTask":Lcom/android/server/wm/Task;
    :cond_2
    move-object v13, v0

    .end local v0    # "ar":Lcom/android/server/wm/ActivityRecord;
    .local v13, "ar":Lcom/android/server/wm/ActivityRecord;
    :goto_0
    if-eqz v8, :cond_3

    invoke-virtual/range {p3 .. p3}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    if-eqz v13, :cond_3

    iget-object v0, v13, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-eqz v0, :cond_3

    sget-object v0, Landroid/util/MiuiMultiWindowAdapter;->LIST_ABOUT_LOCK_MODE_ACTIVITY:Ljava/util/List;

    iget-object v2, v13, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 188
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 189
    invoke-virtual {v8, v1}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V

    .line 190
    return-object v8

    .line 192
    :cond_3
    if-eqz v9, :cond_4

    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v0

    .line 193
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/server/wm/MiuiFreeformUtilStub;->isFullScreenStrategyNeededInDesktopMode(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 194
    const-string v0, "ActivityStarter"

    const-string v1, "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed skip due to desktop Full-screen strategy"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    return-object v8

    .line 197
    :cond_4
    move-object v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move/from16 v4, p6

    move-object/from16 v5, p5

    move v6, v12

    move-object/from16 v7, p8

    invoke-static/range {v0 .. v7}, Lcom/android/server/wm/ActivityStarterInjector;->modifyLaunchActivityOptionIfNeed(Lcom/android/server/wm/RootWindowContainer;Ljava/lang/String;Landroid/app/ActivityOptions;Lcom/android/server/wm/WindowProcessController;ILandroid/content/Intent;ZLcom/android/server/wm/ActivityRecord;)Landroid/app/ActivityOptions;

    move-result-object v0

    return-object v0
.end method

.method private static modifyLaunchActivityOptionIfNeed(Lcom/android/server/wm/RootWindowContainer;Ljava/lang/String;Landroid/app/ActivityOptions;Lcom/android/server/wm/WindowProcessController;ILandroid/content/Intent;ZLcom/android/server/wm/ActivityRecord;)Landroid/app/ActivityOptions;
    .locals 17
    .param p0, "root"    # Lcom/android/server/wm/RootWindowContainer;
    .param p1, "callingPackgae"    # Ljava/lang/String;
    .param p2, "options"    # Landroid/app/ActivityOptions;
    .param p3, "callerApp"    # Lcom/android/server/wm/WindowProcessController;
    .param p4, "userId"    # I
    .param p5, "intent"    # Landroid/content/Intent;
    .param p6, "pcRun"    # Z
    .param p7, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 64
    move-object/from16 v1, p5

    const/4 v0, 0x0

    .line 65
    .local v0, "startPackageName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 66
    .local v2, "sourceFfas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    move-object/from16 v3, p7

    .line 67
    .local v3, "sourceFreeFormActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_3

    .line 68
    const-string v4, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 69
    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 71
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 73
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 74
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    .line 73
    :cond_2
    move-object v4, v0

    goto :goto_1

    .line 67
    :cond_3
    move-object v4, v0

    .line 77
    .end local v0    # "startPackageName":Ljava/lang/String;
    .local v4, "startPackageName":Ljava/lang/String;
    :goto_1
    if-nez v3, :cond_5

    if-eqz p3, :cond_5

    .line 78
    invoke-virtual/range {p3 .. p3}, Lcom/android/server/wm/WindowProcessController;->getActivities()Ljava/util/List;

    move-result-object v0

    .line 79
    .local v0, "callerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    invoke-virtual/range {p3 .. p3}, Lcom/android/server/wm/WindowProcessController;->getInactiveActivities()Ljava/util/List;

    move-result-object v5

    .line 80
    .local v5, "inactiveActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    invoke-static {v0}, Lcom/android/server/wm/ActivityStarterInjector;->getLastFreeFormActivityRecord(Ljava/util/List;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 81
    invoke-static {v0}, Lcom/android/server/wm/ActivityStarterInjector;->getLastFreeFormActivityRecord(Ljava/util/List;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v6

    goto :goto_2

    :cond_4
    invoke-static {v5}, Lcom/android/server/wm/ActivityStarterInjector;->getLastFreeFormActivityRecord(Ljava/util/List;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v6

    :goto_2
    move-object v3, v6

    .line 83
    .end local v0    # "callerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    .end local v5    # "inactiveActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    :cond_5
    if-eqz v3, :cond_6

    .line 84
    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    .line 85
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v5

    invoke-interface {v0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 87
    :cond_6
    if-eqz v1, :cond_8

    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 88
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getCanStartActivityFromFullscreenToFreefromList()Ljava/util/List;

    move-result-object v0

    .line 89
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 90
    move-object/from16 v5, p0

    iget-object v0, v5, Lcom/android/server/wm/RootWindowContainer;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    invoke-interface {v0}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->getSchemeLauncherTask()Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    goto :goto_3

    .line 89
    :cond_7
    move-object/from16 v5, p0

    goto :goto_3

    .line 87
    :cond_8
    move-object/from16 v5, p0

    .line 93
    :goto_3
    const-string v6, "ActivityStarter"

    const/4 v7, 0x1

    if-eqz v2, :cond_d

    if-eqz v4, :cond_d

    .line 94
    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    if-eqz v3, :cond_d

    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-eqz v0, :cond_d

    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 96
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    const-string v8, "com.miui.securitycore/com.miui.xspace.ui.activity.XSpaceResolveActivity"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_9
    if-eqz v3, :cond_d

    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-eqz v0, :cond_d

    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 98
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    const-string v8, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 99
    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, v4, v7}, Landroid/util/MiuiMultiWindowUtils;->getActivityOptions(Landroid/content/Context;Ljava/lang/String;Z)Landroid/app/ActivityOptions;

    move-result-object v8

    .line 101
    .end local p2    # "options":Landroid/app/ActivityOptions;
    .local v8, "options":Landroid/app/ActivityOptions;
    iget-object v9, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 102
    .local v9, "sourceTask":Lcom/android/server/wm/Task;
    if-eqz v9, :cond_b

    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, v9, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    .line 103
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 104
    iget-object v0, v9, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastPosition(Lcom/android/server/wm/Task;)Landroid/graphics/Rect;

    move-result-object v10

    .line 105
    .local v10, "launchBounds":Landroid/graphics/Rect;
    invoke-virtual {v8, v10}, Landroid/app/ActivityOptions;->setLaunchBounds(Landroid/graphics/Rect;)Landroid/app/ActivityOptions;

    .line 106
    iget-object v0, v9, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F

    move-result v11

    .line 107
    .local v11, "scale":F
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed:: modify bounds and scale in dkt  launchBounds: "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v12, " scale: "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :try_start_0
    const-string v0, "getActivityOptionsInjector"

    const/4 v12, 0x0

    new-array v13, v12, [Ljava/lang/Object;

    invoke-static {v8, v0, v13}, Landroid/util/MiuiMultiWindowUtils;->isMethodExist(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 113
    .local v0, "method":Ljava/lang/reflect/Method;
    if-eqz v0, :cond_a

    const/high16 v13, -0x40800000    # -1.0f

    cmpl-float v13, v11, v13

    if-eqz v13, :cond_a

    .line 114
    new-array v13, v12, [Ljava/lang/Object;

    invoke-virtual {v0, v8, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    const-string/jumbo v14, "setFreeformScale"

    new-array v15, v7, [Ljava/lang/Object;

    .line 115
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    aput-object v16, v15, v12

    .line 114
    invoke-static {v13, v14, v15}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    .end local v0    # "method":Ljava/lang/reflect/Method;
    :cond_a
    goto :goto_4

    .line 117
    :catch_0
    move-exception v0

    .line 120
    .end local v10    # "launchBounds":Landroid/graphics/Rect;
    .end local v11    # "scale":F
    :cond_b
    :goto_4
    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I

    move-result v0

    if-eqz v0, :cond_c

    .line 121
    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/app/ActivityOptions;->setLaunchFromTaskId(I)V

    .line 123
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed:: options = "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, " sourceFfas: "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, " sourceFreeFormActivity: "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 128
    .end local v8    # "options":Landroid/app/ActivityOptions;
    .end local v9    # "sourceTask":Lcom/android/server/wm/Task;
    .restart local p2    # "options":Landroid/app/ActivityOptions;
    :cond_d
    move-object/from16 v8, p2

    .end local p2    # "options":Landroid/app/ActivityOptions;
    .restart local v8    # "options":Landroid/app/ActivityOptions;
    :goto_5
    if-eqz v2, :cond_14

    if-eqz v4, :cond_14

    .line 129
    iget-object v0, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 130
    .local v0, "task":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_14

    iget-object v9, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v9, :cond_14

    sget-object v9, Landroid/util/MiuiMultiWindowAdapter;->NOT_AVOID_LAUNCH_OTHER_FREEFORM_LIST:Ljava/util/List;

    iget-object v10, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    .line 131
    invoke-virtual {v10}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 132
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    goto :goto_6

    :cond_e
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    .line 133
    .local v9, "currentRect":Landroid/graphics/Rect;
    :goto_6
    iget-object v10, v3, Lcom/android/server/wm/ActivityRecord;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v10, v10, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    .line 134
    const/4 v11, -0x1

    if-eqz v9, :cond_f

    iget v12, v9, Landroid/graphics/Rect;->left:I

    goto :goto_7

    :cond_f
    move v12, v11

    :goto_7
    if-eqz v9, :cond_10

    iget v11, v9, Landroid/graphics/Rect;->top:I

    .line 133
    :cond_10
    invoke-static {v10, v4, v7, v12, v11}, Landroid/util/MiuiMultiWindowUtils;->getActivityOptions(Landroid/content/Context;Ljava/lang/String;ZII)Landroid/app/ActivityOptions;

    move-result-object v7

    .line 135
    .local v7, "targetOptions":Landroid/app/ActivityOptions;
    if-eqz v7, :cond_14

    .line 136
    if-eqz v8, :cond_11

    invoke-virtual {v8}, Landroid/app/ActivityOptions;->getForceLaunchNewTask()Z

    move-result v10

    if-eqz v10, :cond_11

    .line 137
    invoke-virtual {v7}, Landroid/app/ActivityOptions;->setForceLaunchNewTask()V

    .line 139
    :cond_11
    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I

    move-result v10

    if-eqz v10, :cond_12

    .line 140
    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I

    move-result v10

    invoke-virtual {v7, v10}, Landroid/app/ActivityOptions;->setLaunchFromTaskId(I)V

    .line 142
    :cond_12
    if-eqz v8, :cond_13

    move-object v10, v7

    goto :goto_8

    :cond_13
    const/4 v10, 0x0

    :goto_8
    move-object v8, v10

    .line 143
    const-string v10, "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed::due to application confirm lock"

    invoke-static {v6, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    .end local v0    # "task":Lcom/android/server/wm/Task;
    .end local v7    # "targetOptions":Landroid/app/ActivityOptions;
    .end local v9    # "currentRect":Landroid/graphics/Rect;
    :cond_14
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 149
    if-nez v8, :cond_15

    .line 150
    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object v8

    .line 152
    :cond_15
    if-eqz v1, :cond_16

    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 153
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    const-string v6, "com.android.camera/.OneShotCamera"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 154
    invoke-virtual {v8}, Landroid/app/ActivityOptions;->setForceLaunchNewTask()V

    .line 157
    :cond_16
    return-object v8
.end method

.method public static startActivityUncheckedBefore(Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 0
    .param p0, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p1, "isFromHome"    # Z

    .line 226
    return-void
.end method
