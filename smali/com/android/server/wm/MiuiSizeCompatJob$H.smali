.class Lcom/android/server/wm/MiuiSizeCompatJob$H;
.super Landroid/os/Handler;
.source "MiuiSizeCompatJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiSizeCompatJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field private static final MSG_BIND_ONE_TRACK:I = 0x2

.field private static final MSG_TRACK_EVENT:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatJob;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MiuiSizeCompatJob;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 77
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    .line 78
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 79
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 83
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "MiuiSizeCompatJob"

    packed-switch v0, :pswitch_data_0

    .line 95
    const-string v0, "Error message what!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 92
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$mbindOneTrackService(Lcom/android/server/wm/MiuiSizeCompatJob;)V

    .line 93
    goto :goto_0

    .line 85
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Landroid/app/job/JobParameters;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/app/job/JobParameters;

    invoke-static {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$mtrackEvent(Lcom/android/server/wm/MiuiSizeCompatJob;Landroid/app/job/JobParameters;)V

    goto :goto_0

    .line 88
    :cond_0
    const-string v0, "Error obj ,not job params"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    nop

    .line 98
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
