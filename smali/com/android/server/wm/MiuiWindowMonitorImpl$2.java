class com.android.server.wm.MiuiWindowMonitorImpl$2 implements com.android.server.am.MiuiWarnings$WarningCallback {
	 /* .source "MiuiWindowMonitorImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiWindowMonitorImpl;->showKillAppDialog(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Ljava/lang/String;Z)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiWindowMonitorImpl this$0; //synthetic
final java.lang.String val$appLabel; //synthetic
final Boolean val$persistent; //synthetic
final Integer val$pid; //synthetic
final java.lang.String val$pkgName; //synthetic
final com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList val$processWindows; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiWindowMonitorImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiWindowMonitorImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 243 */
this.this$0 = p1;
this.val$processWindows = p2;
/* iput p3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I */
this.val$pkgName = p4;
this.val$appLabel = p5;
/* iput-boolean p6, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$persistent:Z */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCallback ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "positive" # Z */
/* .line 246 */
v0 = this.this$0;
/* monitor-enter v0 */
/* .line 247 */
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 248 */
try { // :try_start_0
	 v2 = this.val$processWindows;
	 int v3 = 1; // const/4 v3, 0x1
	 (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v2 ).setWillKill ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V
	 /* .line 250 */
} // :cond_0
v2 = this.val$processWindows;
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v2 ).setWillKill ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V
/* .line 253 */
} // :goto_0
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I */
v4 = this.val$processWindows;
com.android.server.wm.MiuiWindowMonitorImpl .-$$Nest$mreportIfNeeded ( v2,v3,v4,v1 );
/* .line 255 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 256 */
final String v2 = "MiuiWindowMonitorImpl"; // const-string v2, "MiuiWindowMonitorImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "kill to avoid window leak, pid="; // const-string v4, "kill to avoid window leak, pid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", package="; // const-string v4, ", package="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.val$pkgName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", label="; // const-string v4, ", label="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.val$appLabel;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", persistent="; // const-string v4, ", persistent="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$persistent:Z */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 258 */
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I */
com.android.server.wm.MiuiWindowMonitorImpl .-$$Nest$mkillApp ( v2,v3 );
/* .line 260 */
} // :cond_1
final String v2 = "MiuiWindowMonitorImpl"; // const-string v2, "MiuiWindowMonitorImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "application create too many windows, pid="; // const-string v4, "application create too many windows, pid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", package="; // const-string v4, ", package="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.val$pkgName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", label="; // const-string v4, ", label="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.val$appLabel;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", persistent="; // const-string v4, ", persistent="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$persistent:Z */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 262 */
v2 = this.val$processWindows;
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v2 ).setWillKill ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V
/* .line 264 */
} // :goto_1
v2 = this.val$processWindows;
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v2 ).setWarned ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V
/* .line 265 */
/* monitor-exit v0 */
/* .line 266 */
return;
/* .line 265 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
