.class Lcom/android/server/wm/MiuiMultiWindowRecommendController$1;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    .line 84
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$1;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .line 87
    const-string v0, "MiuiMultiWindowRecommendController"

    const-string v1, "click split screen recommend view"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$1;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V

    .line 89
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$1;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$menterSplitScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    .line 90
    return-void
.end method
