.class Lcom/android/server/wm/MiuiMultiWindowRecommendController$3;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    .line 105
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$3;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 109
    const-string v0, "MiuiMultiWindowRecommendController"

    :try_start_0
    const-string v1, "removeFreeFormRecomendRunnable"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$3;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    goto :goto_0

    .line 111
    :catch_0
    move-exception v1

    .line 112
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, " removeFreeFormRecomendRunnable error: "

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 114
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
