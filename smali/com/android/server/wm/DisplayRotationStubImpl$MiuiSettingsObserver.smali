.class Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "DisplayRotationStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/DisplayRotationStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/DisplayRotationStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/DisplayRotationStubImpl;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 432
    iput-object p1, p0, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;->this$0:Lcom/android/server/wm/DisplayRotationStubImpl;

    .line 433
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 434
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 437
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;->this$0:Lcom/android/server/wm/DisplayRotationStubImpl;

    invoke-static {v0}, Lcom/android/server/wm/DisplayRotationStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/wm/DisplayRotationStubImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 438
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "accelerometer_rotation_outer"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 441
    const-string/jumbo v1, "user_rotation_outer"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 444
    const-string v1, "accelerometer_rotation_inner"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 447
    const-string/jumbo v1, "user_rotation_inner"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 451
    iget-object v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;->this$0:Lcom/android/server/wm/DisplayRotationStubImpl;

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayRotationStubImpl;->updateRotationMode()V

    .line 452
    return-void
.end method

.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .line 456
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;->this$0:Lcom/android/server/wm/DisplayRotationStubImpl;

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayRotationStubImpl;->updateRotationMode()V

    .line 457
    return-void
.end method
