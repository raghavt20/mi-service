public class com.android.server.wm.SchedBoostGesturesEvent implements com.miui.server.input.gesture.MiuiGestureListener {
	 /* .source "SchedBoostGesturesEvent.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;, */
	 /* Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;, */
	 /* Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer MAX_FLING_TIME_MILLIS;
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
private Integer mDisplayId;
private android.view.GestureDetector mGestureDetector;
private com.android.server.wm.SchedBoostGesturesEvent$GesturesEventListener mGesturesEventListener;
private com.android.server.wm.SchedBoostGesturesEvent$MyHandler mHandler;
private Long mLastFlingTime;
private Boolean mScrollFired;
/* # direct methods */
public static void $r8$lambda$DcdU_oZ1m8deABLpUsOv2ceaqK0 ( com.android.server.wm.SchedBoostGesturesEvent p0, android.view.MotionEvent p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->lambda$onPointerEvent$1(Landroid/view/MotionEvent;)V */
	 return;
} // .end method
public static void $r8$lambda$FCKQg3Og1OhtnbQHGwZSF-gBwNM ( com.android.server.wm.SchedBoostGesturesEvent p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/SchedBoostGesturesEvent;->lambda$init$0()V */
	 return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.wm.SchedBoostGesturesEvent p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static com.android.server.wm.SchedBoostGesturesEvent$GesturesEventListener -$$Nest$fgetmGesturesEventListener ( com.android.server.wm.SchedBoostGesturesEvent p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mGesturesEventListener;
} // .end method
static Long -$$Nest$fgetmLastFlingTime ( com.android.server.wm.SchedBoostGesturesEvent p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mLastFlingTime:J */
	 /* return-wide v0 */
} // .end method
static Boolean -$$Nest$fgetmScrollFired ( com.android.server.wm.SchedBoostGesturesEvent p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z */
} // .end method
static void -$$Nest$fputmLastFlingTime ( com.android.server.wm.SchedBoostGesturesEvent p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mLastFlingTime:J */
	 return;
} // .end method
static void -$$Nest$fputmScrollFired ( com.android.server.wm.SchedBoostGesturesEvent p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z */
	 return;
} // .end method
public com.android.server.wm.SchedBoostGesturesEvent ( ) {
	 /* .locals 1 */
	 /* .param p1, "looper" # Landroid/os/Looper; */
	 /* .line 43 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 44 */
	 /* new-instance v0, Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler; */
	 /* invoke-direct {v0, p0, p1}, Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 45 */
	 return;
} // .end method
private void lambda$init$0 ( ) { //synthethic
	 /* .locals 6 */
	 /* .line 51 */
	 /* iget v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mDisplayId:I */
	 /* .line 52 */
	 /* .local v0, "displayId":I */
	 android.hardware.display.DisplayManagerGlobal .getInstance ( );
	 (( android.hardware.display.DisplayManagerGlobal ) v1 ).getDisplayInfo ( v0 ); // invoke-virtual {v1, v0}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayInfo(I)Landroid/view/DisplayInfo;
	 /* .line 53 */
	 /* .local v1, "info":Landroid/view/DisplayInfo; */
	 /* if-nez v1, :cond_0 */
	 /* .line 55 */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "Cannot create GestureDetector, display removed:"; // const-string v3, "Cannot create GestureDetector, display removed:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "SchedBoostGestures"; // const-string v3, "SchedBoostGestures"
	 android.util.Slog .w ( v3,v2 );
	 /* .line 56 */
	 return;
	 /* .line 58 */
} // :cond_0
/* new-instance v2, Lcom/android/server/wm/SchedBoostGesturesEvent$1; */
v3 = this.mContext;
/* new-instance v4, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector; */
/* invoke-direct {v4, p0}, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;)V */
v5 = this.mHandler;
/* invoke-direct {v2, p0, v3, v4, v5}, Lcom/android/server/wm/SchedBoostGesturesEvent$1;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V */
this.mGestureDetector = v2;
/* .line 60 */
return;
} // .end method
private void lambda$onPointerEvent$1 ( android.view.MotionEvent p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 71 */
(( com.android.server.wm.SchedBoostGesturesEvent ) p0 ).onSyncPointerEvent ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->onSyncPointerEvent(Landroid/view/MotionEvent;)V
return;
} // .end method
/* # virtual methods */
public void init ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 48 */
this.mContext = p1;
/* .line 49 */
v0 = (( android.content.Context ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Landroid/content/Context;->getDisplayId()I
/* iput v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mDisplayId:I */
/* .line 50 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/SchedBoostGesturesEvent$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/SchedBoostGesturesEvent$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;)V */
(( com.android.server.wm.SchedBoostGesturesEvent$MyHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 61 */
com.miui.server.input.gesture.MiuiGestureMonitor .getInstance ( p1 );
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v0 ).registerPointerEventListener ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 62 */
return;
} // .end method
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 70 */
(( android.view.MotionEvent ) p1 ).copy ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;
/* .line 71 */
/* .local v0, "motionEvent":Landroid/view/MotionEvent; */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/wm/SchedBoostGesturesEvent$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/wm/SchedBoostGesturesEvent$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/SchedBoostGesturesEvent;Landroid/view/MotionEvent;)V */
(( com.android.server.wm.SchedBoostGesturesEvent$MyHandler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/SchedBoostGesturesEvent$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 72 */
return;
} // .end method
public void onSyncPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 75 */
v0 = this.mGestureDetector;
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = 	 (( android.view.MotionEvent ) p1 ).isTouchEvent ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->isTouchEvent()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 76 */
		 v0 = this.mGestureDetector;
		 (( android.view.GestureDetector ) v0 ).onTouchEvent ( p1 ); // invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z
		 /* .line 78 */
	 } // :cond_0
	 v0 = 	 (( android.view.MotionEvent ) p1 ).getActionMasked ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
	 int v1 = 0; // const/4 v1, 0x0
	 /* packed-switch v0, :pswitch_data_0 */
	 /* .line 92 */
	 /* :pswitch_0 */
	 v0 = this.mGesturesEventListener;
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 93 */
		 /* .line 87 */
		 /* :pswitch_1 */
		 /* iget-boolean v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 v0 = this.mGesturesEventListener;
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 /* .line 88 */
				 /* .line 89 */
			 } // :cond_1
			 /* iput-boolean v1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z */
			 /* .line 90 */
			 /* .line 80 */
			 /* :pswitch_2 */
			 /* iput-boolean v1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent;->mScrollFired:Z */
			 /* .line 81 */
			 v0 = this.mGesturesEventListener;
			 if ( v0 != null) { // if-eqz v0, :cond_2
				 /* .line 82 */
				 /* .line 96 */
			 } // :cond_2
		 } // :goto_0
		 return;
		 /* :pswitch_data_0 */
		 /* .packed-switch 0x0 */
		 /* :pswitch_2 */
		 /* :pswitch_1 */
		 /* :pswitch_0 */
		 /* :pswitch_1 */
	 } // .end packed-switch
} // .end method
public void setGesturesEventListener ( com.android.server.wm.SchedBoostGesturesEvent$GesturesEventListener p0 ) {
	 /* .locals 0 */
	 /* .param p1, "listener" # Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener; */
	 /* .line 65 */
	 this.mGesturesEventListener = p1;
	 /* .line 66 */
	 return;
} // .end method
