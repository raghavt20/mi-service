.class Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;
.super Ljava/lang/Object;
.source "MiuiOrientationImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiOrientationImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FullScreenPackageManager"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final mFullScreenComponentCallback:Ljava/util/function/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Consumer<",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final mFullScreenPackagelistCallback:Ljava/util/function/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Consumer<",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

.field final synthetic this$0:Lcom/android/server/wm/MiuiOrientationImpl;


# direct methods
.method public static synthetic $r8$lambda$-2OoHhogRvqobL0i41I4WIUA9XU(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->lambda$new$0(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$H1FGS8fGsb2vvhwUgvQGTN1nM_8(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->lambda$new$1(Ljava/util/concurrent/ConcurrentHashMap;)V

    return-void
.end method

.method public static synthetic $r8$lambda$rXRkxX0axJQNIQD9U3XtYaDq5lk(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->lambda$new$2(Ljava/util/concurrent/ConcurrentHashMap;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdump(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiOrientationImpl;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "serviceImpl"    # Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    .line 1047
    iput-object p1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->mFullScreenPackagelistCallback:Ljava/util/function/Consumer;

    .line 1059
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->mFullScreenComponentCallback:Ljava/util/function/Consumer;

    .line 1048
    iput-object p2, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->mContext:Landroid/content/Context;

    .line 1049
    iput-object p3, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->mServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    .line 1050
    return-void
.end method

.method private dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 7
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 1070
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1072
    .local v0, "innerPrefix":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmComponentMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    const-string v2, "] "

    const-string v3, "["

    if-nez v1, :cond_0

    .line 1073
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "SmartRotationConfig(Activity)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1074
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmComponentMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1075
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1076
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/android/server/wm/MiuiOrientationImpl;->fullScreenPolicyToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1075
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1077
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 1079
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1081
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1082
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "SmartRotationConfig(Package)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1083
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1084
    .restart local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1085
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/android/server/wm/MiuiOrientationImpl;->fullScreenPolicyToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1084
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1086
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_1

    .line 1088
    :cond_1
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1090
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1091
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "SmartRotationConfig(UserSetting)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1092
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1093
    .restart local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1094
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/android/server/wm/MiuiOrientationImpl;->fullScreenPolicyToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1093
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1095
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_2

    .line 1097
    :cond_2
    return-void
.end method

.method private synthetic lambda$new$0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 1055
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p2}, Lcom/android/server/wm/MiuiOrientationImpl;->parseFullScreenPolicy(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1056
    return-void
.end method

.method private synthetic lambda$new$1(Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 1
    .param p1, "map"    # Ljava/util/concurrent/ConcurrentHashMap;

    .line 1053
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1054
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;)V

    invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 1057
    return-void
.end method

.method private synthetic lambda$new$2(Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 1
    .param p1, "map"    # Ljava/util/concurrent/ConcurrentHashMap;

    .line 1060
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmComponentMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1061
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager$1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;)V

    invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 1067
    return-void
.end method


# virtual methods
.method public clearUserSettings()V
    .locals 1

    .line 1150
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1151
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1153
    :cond_0
    return-void
.end method

.method public getOrientationMode(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1100
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationModeByUserSettings(Ljava/lang/String;)I

    move-result v0

    .line 1101
    .local v0, "modeByUserSettings":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1102
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationModeBySystem(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 1101
    :goto_0
    return v1
.end method

.method public getOrientationModeBySystem(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1112
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1113
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1114
    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    .line 1113
    :goto_0
    return v1
.end method

.method public getOrientationModeByUserSettings(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1118
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1119
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1120
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1121
    const/4 v1, 0x1

    return v1

    .line 1124
    .end local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public getOrientationPolicy(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1128
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByUserSettings(Ljava/lang/String;)I

    move-result v0

    .line 1129
    .local v0, "modeByUserSettings":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1130
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyBySystem(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 1129
    :goto_0
    return v1
.end method

.method public getOrientationPolicyByComponent(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1106
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmComponentMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1107
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1108
    move-object v1, v0

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    .line 1107
    :goto_0
    return v1
.end method

.method public getOrientationPolicyBySystem(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1134
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1135
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1136
    move-object v1, v0

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    .line 1135
    :goto_0
    return v1
.end method

.method public getOrientationPolicyByUserSettings(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1140
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1141
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1142
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1143
    move-object v1, v0

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    .line 1146
    .end local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public setUserSettings(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 1156
    const/4 v0, 0x0

    .line 1157
    .local v0, "policy":I
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "--remove"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1158
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1159
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1160
    invoke-static {p2}, Lcom/android/server/wm/MiuiOrientationImpl;->parseFullScreenPolicy(Ljava/lang/String;)I

    move-result v0

    .line 1161
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1163
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1165
    :goto_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmFileHandler(Lcom/android/server/wm/MiuiOrientationImpl;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v2}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmWriteSettingsRunnable(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1166
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmFileHandler(Lcom/android/server/wm/MiuiOrientationImpl;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v2}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmWriteSettingsRunnable(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1167
    return-void
.end method

.method public setUserSettings([Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "pkgNames"    # [Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 1170
    const/4 v0, 0x0

    .line 1171
    .local v0, "policy":I
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1172
    .local v1, "tmpPolicyDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1173
    .local v2, "tmpPolicyRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_0

    const-string v3, "--remove"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1174
    array-length v3, p1

    :goto_0
    if-ge v4, v3, :cond_2

    aget-object v5, p1, v4

    .line 1175
    .local v5, "packageName":Ljava/lang/String;
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1174
    .end local v5    # "packageName":Ljava/lang/String;
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1178
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1179
    invoke-static {p2}, Lcom/android/server/wm/MiuiOrientationImpl;->parseFullScreenPolicy(Ljava/lang/String;)I

    move-result v0

    .line 1181
    :cond_1
    array-length v3, p1

    :goto_1
    if-ge v4, v3, :cond_2

    aget-object v5, p1, v4

    .line 1182
    .restart local v5    # "packageName":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1181
    .end local v5    # "packageName":Ljava/lang/String;
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1185
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1186
    .local v4, "name":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v5}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1187
    .end local v4    # "name":Ljava/lang/String;
    goto :goto_2

    .line 1188
    :cond_3
    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v3}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1189
    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v3}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmFileHandler(Lcom/android/server/wm/MiuiOrientationImpl;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v4}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmWriteSettingsRunnable(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1190
    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v3}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmFileHandler(Lcom/android/server/wm/MiuiOrientationImpl;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v4}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmWriteSettingsRunnable(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1191
    return-void
.end method
