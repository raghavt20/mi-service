public class com.android.server.wm.DisplayRotationStubImpl implements com.android.server.wm.DisplayRotationStub {
	 /* .source "DisplayRotationStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer BOOT_SENSOR_ROTATION_INVALID;
private static final java.lang.String TAG;
/* # instance fields */
private android.view.SurfaceControl mBootBlackCoverLayer;
private android.content.Context mContext;
private com.android.server.wm.DisplayRotationStubImpl$MiuiSettingsObserver mMiuiSettingsObserver;
private com.android.server.wm.WindowManagerService mService;
private Integer mUserRotationInner;
private Integer mUserRotationModeInner;
private Integer mUserRotationModeOuter;
private Integer mUserRotationOuter;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.wm.DisplayRotationStubImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
public com.android.server.wm.DisplayRotationStubImpl ( ) {
	 /* .locals 1 */
	 /* .line 49 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 66 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I */
	 /* .line 69 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I */
	 /* .line 72 */
	 /* iput v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I */
	 /* .line 75 */
	 /* iput v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I */
	 return;
} // .end method
private void setUserRotation ( Integer p0, Integer p1 ) {
	 /* .locals 5 */
	 /* .param p1, "userRotationMode" # I */
	 /* .param p2, "userRotation" # I */
	 /* .line 197 */
	 v0 = this.mService;
	 v0 = this.mGlobalLock;
	 /* monitor-enter v0 */
	 /* .line 198 */
	 try { // :try_start_0
		 v1 = this.mContext;
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* .line 200 */
		 /* .local v1, "res":Landroid/content/ContentResolver; */
		 int v2 = 1; // const/4 v2, 0x1
		 /* if-ne p1, v2, :cond_0 */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .line 201 */
		 /* .local v2, "accelerometerRotation":I */
	 } // :cond_0
	 final String v3 = "accelerometer_rotation"; // const-string v3, "accelerometer_rotation"
	 int v4 = -2; // const/4 v4, -0x2
	 android.provider.Settings$System .putIntForUser ( v1,v3,v2,v4 );
	 /* .line 203 */
	 /* const-string/jumbo v3, "user_rotation" */
	 android.provider.Settings$System .putIntForUser ( v1,v3,p2,v4 );
	 /* .line 205 */
	 /* nop */
} // .end local v1 # "res":Landroid/content/ContentResolver;
} // .end local v2 # "accelerometerRotation":I
/* monitor-exit v0 */
/* .line 206 */
return;
/* .line 205 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void setUserRotationForFold ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "userRotationMode" # I */
/* .param p2, "userRotation" # I */
/* .param p3, "isFolded" # Z */
/* .line 220 */
v0 = com.android.server.wm.MiuiOrientationStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_FOLD:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 221 */
	 if ( p3 != null) { // if-eqz p3, :cond_0
		 /* .line 222 */
		 /* iput p1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I */
		 /* .line 223 */
		 /* iput p2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I */
		 /* .line 225 */
	 } // :cond_0
	 /* iput p1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I */
	 /* .line 226 */
	 /* iput p2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I */
	 /* .line 229 */
} // :cond_1
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void addBootBlackCoverLayer ( ) {
/* .locals 4 */
/* .line 385 */
v0 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).needBootBlackCoverLayer ( ); // invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->needBootBlackCoverLayer()Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).hasBootBlackCoverLayer ( ); // invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->hasBootBlackCoverLayer()Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 386 */
} // :cond_0
v0 = this.mService;
v0 = this.mTransactionFactory;
/* check-cast v0, Landroid/view/SurfaceControl$Transaction; */
/* .line 387 */
/* .local v0, "t":Landroid/view/SurfaceControl$Transaction; */
v1 = this.mService;
(( com.android.server.wm.WindowManagerService ) v1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
/* .line 388 */
/* .local v1, "displayContent":Lcom/android/server/wm/DisplayContent; */
(( com.android.server.wm.DisplayContent ) v1 ).makeOverlay ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;
/* .line 389 */
final String v3 = "BootBlackCoverLayerSurface"; // const-string v3, "BootBlackCoverLayerSurface"
(( android.view.SurfaceControl$Builder ) v2 ).setName ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 390 */
(( android.view.SurfaceControl$Builder ) v2 ).setColorLayer ( ); // invoke-virtual {v2}, Landroid/view/SurfaceControl$Builder;->setColorLayer()Landroid/view/SurfaceControl$Builder;
/* .line 391 */
final String v3 = "BootAnimation"; // const-string v3, "BootAnimation"
(( android.view.SurfaceControl$Builder ) v2 ).setCallsite ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 392 */
(( android.view.SurfaceControl$Builder ) v2 ).build ( ); // invoke-virtual {v2}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
this.mBootBlackCoverLayer = v2;
/* .line 393 */
/* const v3, 0x1eab91 */
(( android.view.SurfaceControl$Transaction ) v0 ).setLayer ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
/* .line 394 */
v2 = this.mBootBlackCoverLayer;
/* const/high16 v3, 0x3f800000 # 1.0f */
(( android.view.SurfaceControl$Transaction ) v0 ).setAlpha ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 395 */
v2 = this.mBootBlackCoverLayer;
(( android.view.SurfaceControl$Transaction ) v0 ).show ( v2 ); // invoke-virtual {v0, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 396 */
(( android.view.SurfaceControl$Transaction ) v0 ).apply ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 397 */
final String v2 = "DisplayRotationImpl"; // const-string v2, "DisplayRotationImpl"
final String v3 = "addBootBlackCoverLayerSurface"; // const-string v3, "addBootBlackCoverLayerSurface"
android.util.Slog .d ( v2,v3 );
/* .line 398 */
return;
/* .line 385 */
} // .end local v0 # "t":Landroid/view/SurfaceControl$Transaction;
} // .end local v1 # "displayContent":Lcom/android/server/wm/DisplayContent;
} // :cond_1
} // :goto_0
return;
} // .end method
public void cancelAppAnimationIfNeeded ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 5 */
/* .param p1, "mDisplayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 93 */
int v0 = 4; // const/4 v0, 0x4
int v1 = 1; // const/4 v1, 0x1
v0 = (( com.android.server.wm.DisplayContent ) p1 ).isAnimating ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lcom/android/server/wm/DisplayContent;->isAnimating(II)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 94 */
final String v0 = "prepareNormalRA apptransition still animating"; // const-string v0, "prepareNormalRA apptransition still animating"
final String v1 = "WindowManager"; // const-string v1, "WindowManager"
android.util.Slog .d ( v1,v0 );
/* .line 95 */
v0 = this.mAppTransition;
v0 = this.mLastOpeningAnimationTargets;
final String v2 = "prepareNormalRA cancelAnimation "; // const-string v2, "prepareNormalRA cancelAnimation "
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 97 */
v0 = this.mAppTransition;
v0 = this.mLastOpeningAnimationTargets;
(( android.util.ArraySet ) v0 ).iterator ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/wm/WindowContainer; */
/* .line 98 */
/* .local v3, "animatingContainer":Lcom/android/server/wm/WindowContainer; */
(( com.android.server.wm.WindowContainer ) v3 ).cancelAnimation ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowContainer;->cancelAnimation()V
/* .line 99 */
v4 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_ORIENTATION;
v4 = (( com.android.internal.protolog.ProtoLogGroup ) v4 ).isLogToLogcat ( ); // invoke-virtual {v4}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 100 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 102 */
} // .end local v3 # "animatingContainer":Lcom/android/server/wm/WindowContainer;
} // :cond_0
/* .line 104 */
} // :cond_1
v0 = this.mAppTransition;
v0 = this.mLastClosingAnimationTargets;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 106 */
v0 = this.mAppTransition;
v0 = this.mLastClosingAnimationTargets;
(( android.util.ArraySet ) v0 ).iterator ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/android/server/wm/WindowContainer; */
/* .line 107 */
/* .restart local v3 # "animatingContainer":Lcom/android/server/wm/WindowContainer; */
(( com.android.server.wm.WindowContainer ) v3 ).cancelAnimation ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowContainer;->cancelAnimation()V
/* .line 108 */
v4 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_ORIENTATION;
v4 = (( com.android.internal.protolog.ProtoLogGroup ) v4 ).isLogToLogcat ( ); // invoke-virtual {v4}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 109 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 111 */
} // .end local v3 # "animatingContainer":Lcom/android/server/wm/WindowContainer;
} // :cond_2
/* .line 114 */
} // :cond_3
return;
} // .end method
public void checkBootBlackCoverLayer ( ) {
/* .locals 5 */
/* .line 413 */
v0 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).needBootBlackCoverLayer ( ); // invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->needBootBlackCoverLayer()Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).hasBootBlackCoverLayer ( ); // invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->hasBootBlackCoverLayer()Z
/* if-nez v0, :cond_0 */
/* .line 414 */
} // :cond_0
v0 = this.mService;
(( com.android.server.wm.WindowManagerService ) v0 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
/* .line 415 */
/* .local v0, "displayContent":Lcom/android/server/wm/DisplayContent; */
v1 = (( com.android.server.wm.DisplayContent ) v0 ).getRotation ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getRotation()I
(( com.android.server.wm.DisplayContent ) v0 ).getWindowConfiguration ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getWindowConfiguration()Landroid/app/WindowConfiguration;
v2 = (( android.app.WindowConfiguration ) v2 ).getRotation ( ); // invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getRotation()I
/* if-ne v1, v2, :cond_1 */
/* .line 416 */
(( com.android.server.wm.DisplayRotationStubImpl ) p0 ).removeBootBlackCoverLayer ( ); // invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->removeBootBlackCoverLayer()V
/* .line 417 */
return;
/* .line 419 */
} // :cond_1
v1 = this.mService;
v1 = this.mH;
/* const/16 v2, 0x6d */
/* const-wide/16 v3, 0x7d0 */
(( com.android.server.wm.WindowManagerService$H ) v1 ).sendNewMessageDelayed ( v2, v0, v3, v4 ); // invoke-virtual {v1, v2, v0, v3, v4}, Lcom/android/server/wm/WindowManagerService$H;->sendNewMessageDelayed(ILjava/lang/Object;J)V
/* .line 421 */
return;
/* .line 413 */
} // .end local v0 # "displayContent":Lcom/android/server/wm/DisplayContent;
} // :cond_2
} // :goto_0
return;
} // .end method
public void dumpDoubleSwitch ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 279 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mUserRotationModeOuter="; // const-string v1, " mUserRotationModeOuter="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I */
/* .line 280 */
com.android.server.policy.WindowManagerPolicy .userRotationModeToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 279 */
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 281 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mUserRotationOuter="; // const-string v1, " mUserRotationOuter="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I */
android.view.Surface .rotationToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 282 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mUserRotationModeInner="; // const-string v1, " mUserRotationModeInner="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I */
/* .line 283 */
com.android.server.policy.WindowManagerPolicy .userRotationModeToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 282 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 284 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mUserRotationInner="; // const-string v1, " mUserRotationInner="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I */
android.view.Surface .rotationToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 285 */
return;
} // .end method
public Boolean forceEnableSeamless ( com.android.server.wm.WindowState p0 ) {
/* .locals 2 */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 84 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.mAttrs;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAttrs;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* const/high16 v1, 0x40000000 # 2.0f */
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 86 */
int v0 = 1; // const/4 v0, 0x1
/* .line 88 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getBootSensorRotation ( ) {
/* .locals 2 */
/* .line 361 */
final String v0 = "ro.boot.ori"; // const-string v0, "ro.boot.ori"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 362 */
/* .local v0, "bootSensorRotation":I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 373 */
/* .line 370 */
/* :pswitch_0 */
int v1 = 2; // const/4 v1, 0x2
/* .line 368 */
/* :pswitch_1 */
int v1 = 3; // const/4 v1, 0x3
/* .line 366 */
/* :pswitch_2 */
int v1 = 0; // const/4 v1, 0x0
/* .line 364 */
/* :pswitch_3 */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Integer getHoverModeRecommendRotation ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 2 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 127 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 128 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 129 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 130 */
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).getHoverModeRecommendRotation ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->getHoverModeRecommendRotation(Lcom/android/server/wm/DisplayContent;)I
/* .line 132 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
public Integer getUserRotationFromSettings ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "isFolded" # Z */
/* .line 253 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 254 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
int v1 = -2; // const/4 v1, -0x2
int v2 = 0; // const/4 v2, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 255 */
/* const-string/jumbo v3, "user_rotation_outer" */
v1 = android.provider.Settings$System .getIntForUser ( v0,v3,v2,v1 );
/* .line 258 */
} // :cond_0
/* const-string/jumbo v3, "user_rotation_inner" */
v1 = android.provider.Settings$System .getIntForUser ( v0,v3,v2,v1 );
} // .end method
public Integer getUserRotationModeFromSettings ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "isFolded" # Z */
/* .line 237 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 238 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
int v1 = -2; // const/4 v1, -0x2
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 240 */
/* nop */
/* .line 239 */
final String v4 = "accelerometer_rotation_outer"; // const-string v4, "accelerometer_rotation_outer"
v1 = android.provider.Settings$System .getIntForUser ( v0,v4,v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 241 */
/* .line 242 */
} // :cond_0
/* move v2, v3 */
/* .line 239 */
} // :goto_0
/* .line 246 */
} // :cond_1
/* nop */
/* .line 245 */
final String v4 = "accelerometer_rotation_inner"; // const-string v4, "accelerometer_rotation_inner"
v1 = android.provider.Settings$System .getIntForUser ( v0,v4,v3,v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 247 */
/* .line 248 */
} // :cond_2
/* move v2, v3 */
/* .line 245 */
} // :goto_1
} // .end method
public Boolean hasBootBlackCoverLayer ( ) {
/* .locals 1 */
/* .line 381 */
v0 = this.mBootBlackCoverLayer;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void immediatelyRemoveBootLayer ( ) {
/* .locals 3 */
/* .line 424 */
v0 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).needBootBlackCoverLayer ( ); // invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->needBootBlackCoverLayer()Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).hasBootBlackCoverLayer ( ); // invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->hasBootBlackCoverLayer()Z
/* if-nez v0, :cond_0 */
/* .line 425 */
} // :cond_0
(( com.android.server.wm.DisplayRotationStubImpl ) p0 ).removeBootBlackCoverLayer ( ); // invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->removeBootBlackCoverLayer()V
/* .line 426 */
v0 = this.mService;
v0 = this.mH;
v1 = this.mService;
/* .line 427 */
(( com.android.server.wm.WindowManagerService ) v1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
/* .line 426 */
/* const/16 v2, 0x6d */
(( com.android.server.wm.WindowManagerService$H ) v0 ).removeMessages ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(ILjava/lang/Object;)V
/* .line 428 */
return;
/* .line 424 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void init ( com.android.server.wm.WindowManagerService p0, android.content.Context p1 ) {
/* .locals 2 */
/* .param p1, "wms" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 60 */
this.mContext = p2;
/* .line 61 */
this.mService = p1;
/* .line 62 */
/* new-instance v0, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver; */
com.android.server.UiThread .getHandler ( );
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;-><init>(Lcom/android/server/wm/DisplayRotationStubImpl;Landroid/os/Handler;)V */
this.mMiuiSettingsObserver = v0;
/* .line 63 */
(( com.android.server.wm.DisplayRotationStubImpl$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;->observe()V
/* .line 64 */
return;
} // .end method
public Boolean isProvisioned ( ) {
/* .locals 3 */
/* .line 322 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 323 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
int v2 = 0; // const/4 v2, 0x0
v1 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
} // .end method
public Boolean isRotationLocked ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isFolded" # Z */
/* .line 232 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I */
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
} // .end method
public Boolean needBootBlackCoverLayer ( ) {
/* .locals 2 */
/* .line 377 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 378 */
final String v0 = "ro.boot.ori"; // const-string v0, "ro.boot.ori"
int v1 = -1; // const/4 v1, -0x1
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* if-eq v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 377 */
} // :goto_0
} // .end method
public void removeBootBlackCoverLayer ( ) {
/* .locals 3 */
/* .line 401 */
v0 = this.mBootBlackCoverLayer;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 402 */
v0 = this.mService;
v0 = this.mTransactionFactory;
/* check-cast v0, Landroid/view/SurfaceControl$Transaction; */
/* .line 403 */
/* .local v0, "t":Landroid/view/SurfaceControl$Transaction; */
v1 = this.mBootBlackCoverLayer;
v1 = (( android.view.SurfaceControl ) v1 ).isValid ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl;->isValid()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 404 */
v1 = this.mBootBlackCoverLayer;
(( android.view.SurfaceControl$Transaction ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 406 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
this.mBootBlackCoverLayer = v1;
/* .line 407 */
(( android.view.SurfaceControl$Transaction ) v0 ).apply ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 408 */
final String v1 = "DisplayRotationImpl"; // const-string v1, "DisplayRotationImpl"
final String v2 = "removeBootBlackCoverLayerSurface"; // const-string v2, "removeBootBlackCoverLayerSurface"
android.util.Slog .d ( v1,v2 );
/* .line 410 */
} // .end local v0 # "t":Landroid/view/SurfaceControl$Transaction;
} // :cond_1
return;
} // .end method
public void setUserRotation ( ) {
/* .locals 2 */
/* .line 189 */
v0 = this.mService;
v0 = v0 = this.mPolicy;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 190 */
/* iget v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I */
/* iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotation(II)V */
/* .line 192 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I */
/* iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotation(II)V */
/* .line 194 */
} // :goto_0
return;
} // .end method
public void setUserRotationForFold ( Integer p0, Boolean p1, Integer p2, Boolean p3 ) {
/* .locals 3 */
/* .param p1, "currRotation" # I */
/* .param p2, "enable" # Z */
/* .param p3, "rotation" # I */
/* .param p4, "isFolded" # Z */
/* .line 209 */
int v0 = -1; // const/4 v0, -0x1
/* if-ne p3, v0, :cond_0 */
/* move v0, p1 */
} // :cond_0
/* move v0, p3 */
} // :goto_0
/* move p3, v0 */
/* .line 210 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
if ( p2 != null) { // if-eqz p2, :cond_1
/* move v2, v1 */
/* .line 211 */
} // :cond_1
/* move v2, v0 */
} // :goto_1
/* nop */
/* .line 213 */
/* .local v2, "userRotationMode":I */
/* if-ne v2, v1, :cond_2 */
} // :cond_2
/* move v0, v1 */
/* .line 214 */
/* .local v0, "accelerometerRotation":I */
} // :goto_2
/* invoke-direct {p0, v2, p3, p4}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotationForFold(IIZ)V */
/* .line 215 */
(( com.android.server.wm.DisplayRotationStubImpl ) p0 ).setUserRotationLockedForSettings ( v0, p3, p4 ); // invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotationLockedForSettings(IIZ)V
/* .line 217 */
return;
} // .end method
public void setUserRotationLockedForSettings ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "userRotationMode" # I */
/* .param p2, "userRotation" # I */
/* .param p3, "isFolded" # Z */
/* .line 264 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 265 */
/* .local v0, "res":Landroid/content/ContentResolver; */
int v1 = -2; // const/4 v1, -0x2
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 266 */
final String v2 = "accelerometer_rotation_outer"; // const-string v2, "accelerometer_rotation_outer"
android.provider.Settings$System .putIntForUser ( v0,v2,p1,v1 );
/* .line 268 */
/* const-string/jumbo v2, "user_rotation_outer" */
android.provider.Settings$System .putIntForUser ( v0,v2,p2,v1 );
/* .line 271 */
} // :cond_0
final String v2 = "accelerometer_rotation_inner"; // const-string v2, "accelerometer_rotation_inner"
android.provider.Settings$System .putIntForUser ( v0,v2,p1,v1 );
/* .line 273 */
/* const-string/jumbo v2, "user_rotation_inner" */
android.provider.Settings$System .putIntForUser ( v0,v2,p2,v1 );
/* .line 276 */
} // :goto_0
return;
} // .end method
public Boolean shouldHoverModeEnableSensor ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 2 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 136 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 137 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 138 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 139 */
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).shouldHoverModeEnableSensor ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->shouldHoverModeEnableSensor(Lcom/android/server/wm/DisplayContent;)Z
/* .line 141 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void updateRotation ( com.android.server.wm.DisplayContent p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "dc" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "lastRotation" # I */
/* .line 119 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 120 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 121 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 122 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).updateLastRotation ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->updateLastRotation(Lcom/android/server/wm/DisplayContent;I)V
/* .line 124 */
} // :cond_0
return;
} // .end method
public void updateRotationMode ( ) {
/* .locals 9 */
/* .line 148 */
v0 = com.android.server.wm.MiuiOrientationStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_7
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_FOLD:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 149 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 151 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* const-string/jumbo v1, "user_rotation" */
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v3 );
/* .line 155 */
/* .local v1, "userRotation":I */
/* nop */
/* .line 154 */
final String v4 = "accelerometer_rotation"; // const-string v4, "accelerometer_rotation"
v3 = android.provider.Settings$System .getIntForUser ( v0,v4,v2,v3 );
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 156 */
/* move v3, v2 */
/* .line 157 */
} // :cond_0
/* move v3, v4 */
} // :goto_0
/* nop */
/* .line 159 */
/* .local v3, "userRotationMode":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 160 */
/* .local v5, "shouldUpdateRotationMode":Z */
v6 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).getUserRotationModeFromSettings ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/wm/DisplayRotationStubImpl;->getUserRotationModeFromSettings(Z)I
/* .line 161 */
/* .local v6, "userRotationModeOuter":I */
v4 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).getUserRotationFromSettings ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/wm/DisplayRotationStubImpl;->getUserRotationFromSettings(Z)I
/* .line 162 */
/* .local v4, "userOrientationOuter":I */
/* iget v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I */
/* if-ne v7, v6, :cond_1 */
/* iget v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I */
/* if-eq v7, v4, :cond_3 */
/* .line 164 */
} // :cond_1
/* iput v6, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I */
/* .line 165 */
/* iput v4, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I */
/* .line 166 */
v7 = this.mService;
v7 = v7 = this.mPolicy;
if ( v7 != null) { // if-eqz v7, :cond_3
/* iget v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I */
/* if-ne v7, v3, :cond_2 */
/* iget v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I */
/* if-eq v7, v1, :cond_3 */
/* .line 168 */
} // :cond_2
int v5 = 1; // const/4 v5, 0x1
/* .line 171 */
} // :cond_3
v7 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).getUserRotationModeFromSettings ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/DisplayRotationStubImpl;->getUserRotationModeFromSettings(Z)I
/* .line 172 */
/* .local v7, "userRotationModeInner":I */
v2 = (( com.android.server.wm.DisplayRotationStubImpl ) p0 ).getUserRotationFromSettings ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/DisplayRotationStubImpl;->getUserRotationFromSettings(Z)I
/* .line 173 */
/* .local v2, "userOrientationInner":I */
/* iget v8, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I */
/* if-ne v8, v7, :cond_4 */
/* iget v8, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I */
/* if-eq v8, v2, :cond_6 */
/* .line 175 */
} // :cond_4
/* iput v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I */
/* .line 176 */
/* iput v2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I */
/* .line 177 */
v8 = this.mService;
v8 = v8 = this.mPolicy;
/* if-nez v8, :cond_6 */
/* iget v8, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I */
/* if-ne v8, v3, :cond_5 */
/* iget v8, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I */
/* if-eq v8, v1, :cond_6 */
/* .line 179 */
} // :cond_5
int v5 = 1; // const/4 v5, 0x1
/* .line 182 */
} // :cond_6
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 183 */
(( com.android.server.wm.DisplayRotationStubImpl ) p0 ).setUserRotation ( ); // invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotation()V
/* .line 186 */
} // .end local v0 # "resolver":Landroid/content/ContentResolver;
} // .end local v1 # "userRotation":I
} // .end local v2 # "userOrientationInner":I
} // .end local v3 # "userRotationMode":I
} // .end local v4 # "userOrientationOuter":I
} // .end local v5 # "shouldUpdateRotationMode":Z
} // .end local v6 # "userRotationModeOuter":I
} // .end local v7 # "userRotationModeInner":I
} // :cond_7
return;
} // .end method
public void writeRotationChangeToProperties ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "rotation" # I */
/* .line 290 */
int v0 = -1; // const/4 v0, -0x1
/* .line 291 */
/* .local v0, "currRotation":I */
/* packed-switch p1, :pswitch_data_0 */
/* .line 302 */
/* :pswitch_0 */
/* const/16 v0, 0x10e */
/* .line 303 */
/* .line 299 */
/* :pswitch_1 */
/* const/16 v0, 0xb4 */
/* .line 300 */
/* .line 296 */
/* :pswitch_2 */
/* const/16 v0, 0x5a */
/* .line 297 */
/* .line 293 */
/* :pswitch_3 */
int v0 = 0; // const/4 v0, 0x0
/* .line 294 */
/* nop */
/* .line 307 */
} // :goto_0
/* if-gez v0, :cond_0 */
return;
/* .line 308 */
} // :cond_0
/* move v1, v0 */
/* .line 309 */
/* .local v1, "finalRotation":I */
v2 = android.os.AsyncTask.THREAD_POOL_EXECUTOR;
/* new-instance v3, Lcom/android/server/wm/DisplayRotationStubImpl$1; */
/* invoke-direct {v3, p0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl$1;-><init>(Lcom/android/server/wm/DisplayRotationStubImpl;I)V */
/* .line 319 */
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void writeRotationChangeToPropertiesForDisplay ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "rotation" # I */
/* .line 328 */
int v0 = -1; // const/4 v0, -0x1
/* .line 329 */
/* .local v0, "currRotation":I */
/* packed-switch p1, :pswitch_data_0 */
/* .line 340 */
/* :pswitch_0 */
/* const/16 v0, 0x10e */
/* .line 341 */
/* .line 337 */
/* :pswitch_1 */
/* const/16 v0, 0xb4 */
/* .line 338 */
/* .line 334 */
/* :pswitch_2 */
/* const/16 v0, 0x5a */
/* .line 335 */
/* .line 331 */
/* :pswitch_3 */
int v0 = 0; // const/4 v0, 0x0
/* .line 332 */
/* nop */
/* .line 345 */
} // :goto_0
/* if-gez v0, :cond_0 */
return;
/* .line 346 */
} // :cond_0
/* move v1, v0 */
/* .line 347 */
/* .local v1, "finalRotation":I */
v2 = android.os.AsyncTask.THREAD_POOL_EXECUTOR;
/* new-instance v3, Lcom/android/server/wm/DisplayRotationStubImpl$2; */
/* invoke-direct {v3, p0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl$2;-><init>(Lcom/android/server/wm/DisplayRotationStubImpl;I)V */
/* .line 357 */
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
