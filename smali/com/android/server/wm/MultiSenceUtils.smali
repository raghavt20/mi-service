.class public Lcom/android/server/wm/MultiSenceUtils;
.super Ljava/lang/Object;
.source "MultiSenceUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MultiSenceUtils$MultiSenceUtilsHolder;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final TAG:Ljava/lang/String; = "MultiSenceUtils"

.field private static mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

.field private static service:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 30
    const-string v0, "persist.multisence.debug.on"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MultiSenceUtils;->DEBUG:Z

    .line 32
    invoke-static {}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->getInstance()Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService;

    sput-object v0, Lcom/android/server/wm/MultiSenceUtils;->mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 34
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    sput-object v0, Lcom/android/server/wm/MultiSenceUtils;->service:Lcom/android/server/wm/WindowManagerService;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string v0, "MultiSenceUtils init"

    invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceUtils;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/wm/MultiSenceUtils-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MultiSenceUtils;-><init>()V

    return-void
.end method

.method private LOG_IF_DEBUG(Ljava/lang/String;)V
    .locals 1
    .param p1, "log"    # Ljava/lang/String;

    .line 192
    sget-boolean v0, Lcom/android/server/wm/MultiSenceUtils;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 193
    const-string v0, "MultiSenceUtils"

    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_0
    return-void
.end method

.method private getFreeformWindowForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
    .locals 1
    .param p1, "stack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 167
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_PIN:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    return-object v0

    .line 171
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM_MINI:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    return-object v0

    .line 175
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 176
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    return-object v0

    .line 179
    :cond_2
    sget-object v0, Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;->MUTIL_FREEDOM:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    return-object v0
.end method

.method public static getInstance()Lcom/android/server/wm/MultiSenceUtils;
    .locals 1

    .line 45
    sget-object v0, Lcom/android/server/wm/MultiSenceUtils;->mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

    if-nez v0, :cond_0

    .line 46
    invoke-static {}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->getInstance()Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService;

    sput-object v0, Lcom/android/server/wm/MultiSenceUtils;->mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 47
    :cond_0
    sget-object v0, Lcom/android/server/wm/MultiSenceUtils;->service:Lcom/android/server/wm/WindowManagerService;

    if-nez v0, :cond_1

    .line 48
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    sput-object v0, Lcom/android/server/wm/MultiSenceUtils;->service:Lcom/android/server/wm/WindowManagerService;

    .line 49
    :cond_1
    invoke-static {}, Lcom/android/server/wm/MultiSenceUtils$MultiSenceUtilsHolder;->-$$Nest$sfgetINSTANCE()Lcom/android/server/wm/MultiSenceUtils;

    move-result-object v0

    return-object v0
.end method

.method private getWindowMode(Lcom/android/server/wm/WindowState;)Ljava/lang/String;
    .locals 2
    .param p1, "newFocus"    # Lcom/android/server/wm/WindowState;

    .line 183
    if-nez p1, :cond_0

    .line 184
    const-string/jumbo v0, "unknown"

    return-object v0

    .line 186
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 187
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget-object v1, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    .line 188
    invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getWindowingMode()I

    move-result v1

    .line 187
    invoke-static {v1}, Landroid/app/WindowConfiguration;->windowingModeToString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static synthetic lambda$getWindowsNeedToSched$0(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p0, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 69
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecord;->finishing:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static synthetic lambda$getWindowsNeedToSched$1(Ljava/util/ArrayList;Lcom/android/server/wm/WindowState;)V
    .locals 6
    .param p0, "windowlist"    # Ljava/util/ArrayList;
    .param p1, "w"    # Lcom/android/server/wm/WindowState;

    .line 62
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTaskFragment()Lcom/android/server/wm/TaskFragment;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTaskFragment()Lcom/android/server/wm/TaskFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/TaskFragment;->shouldBeVisible(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 63
    const/4 v0, 0x1

    .line 67
    .local v0, "shouldAdd":Z
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.miui.securitycenter"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    const-string v4, "com.lbe.security.miui"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 68
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v2

    .line 69
    .local v2, "rootTask":Lcom/android/server/wm/Task;
    if-nez v2, :cond_1

    move-object v4, v1

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/android/server/wm/MultiSenceUtils$$ExternalSyntheticLambda0;

    invoke-direct {v4}, Lcom/android/server/wm/MultiSenceUtils$$ExternalSyntheticLambda0;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/android/server/wm/Task;->getActivity(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    .line 70
    .local v4, "baseActivity":Lcom/android/server/wm/ActivityRecord;
    :goto_0
    if-nez v4, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->getProcessName()Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "baseName":Ljava/lang/String;
    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 72
    const/4 v0, 0x0

    .line 75
    .end local v1    # "baseName":Ljava/lang/String;
    .end local v2    # "rootTask":Lcom/android/server/wm/Task;
    .end local v4    # "baseActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_3
    if-eqz v0, :cond_4

    .line 76
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    .end local v0    # "shouldAdd":Z
    :cond_4
    return-void
.end method


# virtual methods
.method public getWindowsNeedToSched()Ljava/util/Map;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;"
        }
    .end annotation

    .line 53
    move-object/from16 v1, p0

    const-string v2, "MultiSenceUtils"

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v3, v0

    .line 54
    .local v3, "sWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v4, v0

    .line 55
    .local v4, "windowlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/WindowState;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v5, v0

    .line 56
    .local v5, "mFreeFormLocation":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v6, v0

    .line 58
    .local v6, "mFloatWindowLocation":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;>;"
    sget-object v0, Lcom/android/server/wm/MultiSenceUtils;->service:Lcom/android/server/wm/WindowManagerService;

    const/4 v7, 0x0

    if-nez v0, :cond_0

    .line 59
    return-object v7

    .line 61
    :cond_0
    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    new-instance v8, Lcom/android/server/wm/MultiSenceUtils$$ExternalSyntheticLambda1;

    invoke-direct {v8, v4}, Lcom/android/server/wm/MultiSenceUtils$$ExternalSyntheticLambda1;-><init>(Ljava/util/ArrayList;)V

    const/4 v9, 0x1

    invoke-virtual {v0, v8, v9}, Lcom/android/server/wm/RootWindowContainer;->forAllWindows(Ljava/util/function/Consumer;Z)V

    .line 81
    sget-object v0, Lcom/android/server/wm/MultiSenceUtils;->mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

    if-nez v0, :cond_1

    .line 82
    return-object v7

    .line 84
    :cond_1
    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/Integer;

    .line 85
    .local v8, "taskid":Ljava/lang/Integer;
    const/4 v10, 0x0

    .line 86
    .local v10, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    const/4 v11, 0x0

    .line 88
    .local v11, "name":Ljava/lang/String;
    :try_start_0
    sget-object v0, Lcom/android/server/wm/MultiSenceUtils;->mffms:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-object v10, v0

    .line 89
    invoke-virtual {v10}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v11, v0

    .line 93
    nop

    .line 95
    if-eqz v10, :cond_3

    if-nez v11, :cond_2

    .line 96
    goto :goto_0

    .line 99
    :cond_2
    const/4 v12, -0x1

    .line 101
    .local v12, "pid":I
    :try_start_1
    iget-object v0, v10, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    const/4 v13, 0x0

    invoke-virtual {v0, v13, v9}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 102
    .local v0, "r":Lcom/android/server/wm/ActivityRecord;
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getPid()I

    move-result v13
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v12, v13

    .line 105
    .end local v0    # "r":Lcom/android/server/wm/ActivityRecord;
    goto :goto_1

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "get pid failed: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v10}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v0

    iget-object v0, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->bounds:Landroid/graphics/Rect;

    .line 108
    .local v0, "mBounds":Landroid/graphics/Rect;
    invoke-virtual {v10}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v13

    iget v13, v13, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->freeFormScale:F

    .line 109
    .local v13, "mFreeformScale":F
    iget v14, v0, Landroid/graphics/Rect;->left:I

    iget v15, v0, Landroid/graphics/Rect;->right:I

    iget v9, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v15, v9

    int-to-float v9, v15

    mul-float/2addr v9, v13

    float-to-int v9, v9

    add-int/2addr v14, v9

    .line 110
    .local v14, "right":I
    iget v9, v0, Landroid/graphics/Rect;->top:I

    iget v15, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v16, v7

    iget v7, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v15, v7

    int-to-float v7, v15

    mul-float/2addr v7, v13

    float-to-int v7, v7

    add-int/2addr v9, v7

    .line 111
    .local v9, "bottom":I
    new-instance v7, Landroid/graphics/Rect;

    iget v15, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v17, v8

    .end local v8    # "taskid":Ljava/lang/Integer;
    .local v17, "taskid":Ljava/lang/Integer;
    iget v8, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {v7, v15, v8, v14, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 112
    .local v7, "realBounds":Landroid/graphics/Rect;
    invoke-direct {v1, v10}, Lcom/android/server/wm/MultiSenceUtils;->getFreeformWindowForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    move-result-object v8

    .line 114
    .local v8, "realWindowForm":Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
    new-instance v15, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;

    invoke-direct {v15, v8, v7, v12}, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;-><init>(Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;Landroid/graphics/Rect;I)V

    .line 115
    .local v15, "detailInfo":Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;
    invoke-interface {v5, v11, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    move-object/from16 v18, v0

    .end local v0    # "mBounds":Landroid/graphics/Rect;
    .local v18, "mBounds":Landroid/graphics/Rect;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v19, v7

    .end local v7    # "realBounds":Landroid/graphics/Rect;
    .local v19, "realBounds":Landroid/graphics/Rect;
    const-string v7, "FreeForm: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", Pid: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/android/server/wm/MultiSenceUtils;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 120
    .end local v8    # "realWindowForm":Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
    .end local v9    # "bottom":I
    .end local v10    # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v11    # "name":Ljava/lang/String;
    .end local v12    # "pid":I
    .end local v13    # "mFreeformScale":F
    .end local v14    # "right":I
    .end local v15    # "detailInfo":Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;
    .end local v17    # "taskid":Ljava/lang/Integer;
    .end local v18    # "mBounds":Landroid/graphics/Rect;
    .end local v19    # "realBounds":Landroid/graphics/Rect;
    move-object/from16 v7, v16

    const/4 v9, 0x1

    goto/16 :goto_0

    .line 95
    .local v8, "taskid":Ljava/lang/Integer;
    .restart local v10    # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .restart local v11    # "name":Ljava/lang/String;
    :cond_3
    move-object/from16 v16, v7

    move-object/from16 v17, v8

    .end local v8    # "taskid":Ljava/lang/Integer;
    .restart local v17    # "taskid":Ljava/lang/Integer;
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 90
    .end local v17    # "taskid":Ljava/lang/Integer;
    .restart local v8    # "taskid":Ljava/lang/Integer;
    :catch_1
    move-exception v0

    move-object/from16 v16, v7

    move-object/from16 v17, v8

    .line 91
    .end local v8    # "taskid":Ljava/lang/Integer;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v17    # "taskid":Ljava/lang/Integer;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get freeform app name failed, error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    move-object/from16 v7, v16

    const/4 v9, 0x1

    goto/16 :goto_0

    .line 125
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v10    # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v11    # "name":Ljava/lang/String;
    .end local v17    # "taskid":Ljava/lang/Integer;
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_8

    .line 126
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/WindowState;

    .line 127
    .local v2, "windowInfo":Lcom/android/server/wm/WindowState;
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v7

    .line 129
    .local v7, "name":Ljava/lang/String;
    invoke-interface {v5, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 130
    new-instance v8, Lcom/miui/server/multisence/SingleWindowInfo;

    sget-object v9, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->COMMON:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    invoke-direct {v8, v7, v9}, Lcom/miui/server/multisence/SingleWindowInfo;-><init>(Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$AppType;)V

    .line 131
    const-string v9, "freeform"

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setWindowingModeString(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 132
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setVisiable(Z)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 133
    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;

    iget-object v9, v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;->mWindowForm:Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setgetWindowForm(Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 134
    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;

    iget v9, v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;->mPid:I

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setPid(I)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 135
    invoke-virtual {v8, v0}, Lcom/miui/server/multisence/SingleWindowInfo;->setLayerOrder(I)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 136
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getUid()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setUid(I)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 137
    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;

    iget-object v9, v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setRectValue(Landroid/graphics/Rect;)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    const/4 v9, 0x1

    .local v8, "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    goto :goto_3

    .line 138
    .end local v8    # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    :cond_5
    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 139
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "this is floating window: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Lcom/android/server/wm/MultiSenceUtils;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 140
    new-instance v8, Lcom/miui/server/multisence/SingleWindowInfo;

    sget-object v9, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->COMMON:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    invoke-direct {v8, v7, v9}, Lcom/miui/server/multisence/SingleWindowInfo;-><init>(Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$AppType;)V

    .line 141
    invoke-direct {v1, v2}, Lcom/android/server/wm/MultiSenceUtils;->getWindowMode(Lcom/android/server/wm/WindowState;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setWindowingModeString(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 142
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setVisiable(Z)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 143
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getPid()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setPid(I)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 144
    invoke-virtual {v8, v0}, Lcom/miui/server/multisence/SingleWindowInfo;->setLayerOrder(I)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 145
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getUid()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setUid(I)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    const/4 v9, 0x1

    .restart local v8    # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    goto :goto_3

    .line 147
    .end local v8    # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    :cond_6
    new-instance v8, Lcom/miui/server/multisence/SingleWindowInfo;

    sget-object v9, Lcom/miui/server/multisence/SingleWindowInfo$AppType;->COMMON:Lcom/miui/server/multisence/SingleWindowInfo$AppType;

    invoke-direct {v8, v7, v9}, Lcom/miui/server/multisence/SingleWindowInfo;-><init>(Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$AppType;)V

    .line 148
    invoke-direct {v1, v2}, Lcom/android/server/wm/MultiSenceUtils;->getWindowMode(Lcom/android/server/wm/WindowState;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setWindowingModeString(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 149
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setVisiable(Z)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 150
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getPid()I

    move-result v10

    invoke-virtual {v8, v10}, Lcom/miui/server/multisence/SingleWindowInfo;->setPid(I)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 151
    invoke-virtual {v8, v0}, Lcom/miui/server/multisence/SingleWindowInfo;->setLayerOrder(I)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 152
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getUid()I

    move-result v10

    invoke-virtual {v8, v10}, Lcom/miui/server/multisence/SingleWindowInfo;->setUid(I)Lcom/miui/server/multisence/SingleWindowInfo;

    move-result-object v8

    .line 155
    .restart local v8    # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    :goto_3
    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 156
    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/miui/server/multisence/SingleWindowInfo;

    .line 157
    .local v10, "swi":Lcom/miui/server/multisence/SingleWindowInfo;
    invoke-virtual {v10}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowCount()I

    move-result v11

    .line 158
    .local v11, "windowCount":I
    add-int/lit8 v12, v11, 0x1

    invoke-virtual {v10, v12}, Lcom/miui/server/multisence/SingleWindowInfo;->setWindowCount(I)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 159
    .end local v10    # "swi":Lcom/miui/server/multisence/SingleWindowInfo;
    .end local v11    # "windowCount":I
    goto :goto_4

    .line 160
    :cond_7
    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    .end local v2    # "windowInfo":Lcom/android/server/wm/WindowState;
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo;
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 163
    .end local v0    # "i":I
    :cond_8
    return-object v3
.end method
