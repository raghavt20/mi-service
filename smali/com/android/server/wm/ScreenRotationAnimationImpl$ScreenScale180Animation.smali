.class Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;
.super Landroid/view/animation/ScaleAnimation;
.source "ScreenRotationAnimationImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/ScreenRotationAnimationImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScreenScale180Animation"
.end annotation


# direct methods
.method public constructor <init>(FFFFIFIF)V
    .locals 0
    .param p1, "fromX"    # F
    .param p2, "toX"    # F
    .param p3, "fromY"    # F
    .param p4, "toY"    # F
    .param p5, "pivotXType"    # I
    .param p6, "pivotXValue"    # F
    .param p7, "pivotYType"    # I
    .param p8, "pivotYValue"    # F

    .line 834
    invoke-direct/range {p0 .. p8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 835
    return-void
.end method


# virtual methods
.method public getTransformation(JLandroid/view/animation/Transformation;)Z
    .locals 4
    .param p1, "currentTime"    # J
    .param p3, "outTransformation"    # Landroid/view/animation/Transformation;

    .line 839
    invoke-virtual {p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->getDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 840
    invoke-super {p0, p1, p2, p3}, Landroid/view/animation/ScaleAnimation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    move-result v0

    return v0

    .line 843
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
