class com.android.server.wm.MiuiMultiWindowRecommendHelper$3 extends android.database.ContentObserver {
	 /* .source "MiuiMultiWindowRecommendHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendHelper this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendHelper$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 151 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 154 */
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 155 */
v0 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 156 */
/* .local v0, "contentResolver":Landroid/content/ContentResolver; */
v1 = this.this$0;
int v2 = -1; // const/4 v2, -0x1
int v3 = -2; // const/4 v3, -0x2
final String v4 = "MiuiMultiWindowRecommendSwitch"; // const-string v4, "MiuiMultiWindowRecommendSwitch"
v2 = android.provider.Settings$System .getIntForUser ( v0,v4,v2,v3 );
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_0 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fputmMultiWindowRecommendSwitchEnabled ( v1,v3 );
/* .line 159 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " MultiWindowRecommendSwitch change,new mMultiWindowRecommendSwitchEnabled= "; // const-string v2, " MultiWindowRecommendSwitch change,new mMultiWindowRecommendSwitchEnabled= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fgetmMultiWindowRecommendSwitchEnabled ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiMultiWindowRecommendHelper"; // const-string v2, "MiuiMultiWindowRecommendHelper"
android.util.Slog .d ( v2,v1 );
/* .line 161 */
return;
} // .end method
