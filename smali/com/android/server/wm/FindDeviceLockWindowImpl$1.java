class com.android.server.wm.FindDeviceLockWindowImpl$1 implements com.android.internal.util.ToBooleanFunction {
	 /* .source "FindDeviceLockWindowImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/FindDeviceLockWindowImpl;->updateLockDeviceWindowLocked(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/DisplayContent;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Lcom/android/internal/util/ToBooleanFunction<", */
/* "Lcom/android/server/wm/WindowState;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.wm.FindDeviceLockWindowImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.FindDeviceLockWindowImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/FindDeviceLockWindowImpl; */
/* .line 21 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean apply ( com.android.server.wm.WindowState p0 ) {
/* .locals 6 */
/* .param p1, "win" # Lcom/android/server/wm/WindowState; */
/* .line 24 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_5
v1 = this.mAttrs;
/* if-nez v1, :cond_0 */
/* .line 26 */
} // :cond_0
v1 = this.mAttrs;
/* iget v1, v1, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 27 */
/* .local v1, "type":I */
v2 = this.mAttrs;
/* iget v2, v2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* .line 29 */
/* .local v2, "extraFlags":I */
/* and-int/lit16 v3, v2, 0x800 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 30 */
com.android.server.wm.FindDeviceLockWindowImpl .-$$Nest$sfputsTmpLockWindow ( p1 );
/* .line 31 */
} // :cond_1
int v3 = 1; // const/4 v3, 0x1
/* if-lt v1, v3, :cond_4 */
/* const/16 v4, 0x7d0 */
/* if-ge v1, v4, :cond_4 */
/* .line 33 */
(( com.android.server.wm.WindowState ) p1 ).getParentWindow ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParentWindow()Lcom/android/server/wm/WindowState;
/* if-nez v4, :cond_4 */
/* .line 34 */
com.android.server.wm.FindDeviceLockWindowImpl .-$$Nest$sfgetsTmpFirstAppWindow ( );
if ( v4 != null) { // if-eqz v4, :cond_3
com.android.server.wm.FindDeviceLockWindowImpl .-$$Nest$sfgetsTmpFirstAppWindow ( );
v4 = this.mActivityRecord;
if ( v4 != null) { // if-eqz v4, :cond_2
	 com.android.server.wm.FindDeviceLockWindowImpl .-$$Nest$sfgetsTmpFirstAppWindow ( );
	 v4 = this.mActivityRecord;
	 v5 = this.mActivityRecord;
	 /* .line 36 */
	 v4 = 	 (( java.lang.Object ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
	 if ( v4 != null) { // if-eqz v4, :cond_2
		 /* .line 38 */
	 } // :cond_2
	 com.android.server.wm.FindDeviceLockWindowImpl .-$$Nest$sfgetsTmpLockWindow ( );
	 if ( v4 != null) { // if-eqz v4, :cond_4
		 /* .line 39 */
		 /* .line 37 */
	 } // :cond_3
} // :goto_0
com.android.server.wm.FindDeviceLockWindowImpl .-$$Nest$sfputsTmpFirstAppWindow ( p1 );
/* .line 42 */
} // :cond_4
} // :goto_1
/* .line 24 */
} // .end local v1 # "type":I
} // .end local v2 # "extraFlags":I
} // :cond_5
} // :goto_2
} // .end method
public Boolean apply ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 21 */
/* check-cast p1, Lcom/android/server/wm/WindowState; */
p1 = (( com.android.server.wm.FindDeviceLockWindowImpl$1 ) p0 ).apply ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/FindDeviceLockWindowImpl$1;->apply(Lcom/android/server/wm/WindowState;)Z
} // .end method
