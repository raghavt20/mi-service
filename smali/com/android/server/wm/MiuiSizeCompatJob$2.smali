.class Lcom/android/server/wm/MiuiSizeCompatJob$2;
.super Ljava/lang/Object;
.source "MiuiSizeCompatJob.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiSizeCompatJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatJob;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiSizeCompatJob;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiSizeCompatJob;

    .line 126
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 5

    .line 130
    const-string v0, "MiuiSizeCompatJob"

    const-string v1, "DeathRecipient "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmTrackLock(Lcom/android/server/wm/MiuiSizeCompatJob;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 132
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiSizeCompatJob;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    if-nez v1, :cond_0

    .line 133
    monitor-exit v0

    return-void

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiSizeCompatJob;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    invoke-interface {v1}, Lcom/miui/analytics/ITrackBinder;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatJob$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v2}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmDeathRecipient(Lcom/android/server/wm/MiuiSizeCompatJob;)Landroid/os/IBinder$DeathRecipient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 136
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fputmITrackBinder(Lcom/android/server/wm/MiuiSizeCompatJob;Lcom/miui/analytics/ITrackBinder;)V

    .line 137
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmH(Lcom/android/server/wm/MiuiSizeCompatJob;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 138
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmH(Lcom/android/server/wm/MiuiSizeCompatJob;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v3, 0x12c

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 139
    monitor-exit v0

    .line 140
    return-void

    .line 139
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
