public class com.android.server.wm.SplitScreenRecommendTaskInfo {
	 /* .source "SplitScreenRecommendTaskInfo.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.lang.String mPkgName;
	 private Long mSwitchTime;
	 private com.android.server.wm.Task mTask;
	 private Integer mTaskId;
	 /* # direct methods */
	 public com.android.server.wm.SplitScreenRecommendTaskInfo ( ) {
		 /* .locals 0 */
		 /* .param p1, "taskId" # I */
		 /* .param p2, "packageName" # Ljava/lang/String; */
		 /* .param p3, "time" # J */
		 /* .param p5, "task" # Lcom/android/server/wm/Task; */
		 /* .line 26 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 27 */
		 /* iput p1, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mTaskId:I */
		 /* .line 28 */
		 this.mPkgName = p2;
		 /* .line 29 */
		 /* iput-wide p3, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mSwitchTime:J */
		 /* .line 30 */
		 this.mTask = p5;
		 /* .line 31 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getPkgName ( ) {
		 /* .locals 1 */
		 /* .line 11 */
		 v0 = this.mPkgName;
	 } // .end method
	 public Long getSwitchTime ( ) {
		 /* .locals 2 */
		 /* .line 15 */
		 /* iget-wide v0, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mSwitchTime:J */
		 /* return-wide v0 */
	 } // .end method
	 public com.android.server.wm.Task getTask ( ) {
		 /* .locals 1 */
		 /* .line 23 */
		 v0 = this.mTask;
	 } // .end method
	 public Integer getTaskId ( ) {
		 /* .locals 1 */
		 /* .line 19 */
		 /* iget v0, p0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->mTaskId:I */
	 } // .end method
