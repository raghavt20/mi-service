.class public Lcom/android/server/wm/WallpaperWindowTokenImpl;
.super Ljava/lang/Object;
.source "WallpaperWindowTokenImpl.java"

# interfaces
.implements Lcom/android/server/wm/WallpaperWindowTokenStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getWaitingRecentTransition(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/TransitionController;)Lcom/android/server/wm/Transition;
    .locals 5
    .param p1, "wallpaperTarget"    # Lcom/android/server/wm/WindowState;
    .param p2, "controller"    # Lcom/android/server/wm/TransitionController;

    .line 34
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-nez v1, :cond_0

    goto :goto_1

    .line 37
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p2, Lcom/android/server/wm/TransitionController;->mWaitingTransitions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 38
    iget-object v2, p2, Lcom/android/server/wm/TransitionController;->mWaitingTransitions:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/Transition;

    .line 39
    .local v2, "transition":Lcom/android/server/wm/Transition;
    iget v3, v2, Lcom/android/server/wm/Transition;->mParallelCollectType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 40
    invoke-virtual {v2, v3}, Lcom/android/server/wm/Transition;->isInTransition(Lcom/android/server/wm/WindowContainer;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 41
    return-object v2

    .line 37
    .end local v2    # "transition":Lcom/android/server/wm/Transition;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    .end local v1    # "i":I
    :cond_2
    return-object v0

    .line 35
    :cond_3
    :goto_1
    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;Lcom/android/server/wm/WallpaperWindowToken;)V
    .locals 3
    .param p1, "newParentConfig"    # Landroid/content/res/Configuration;
    .param p2, "wallpaperWindowToken"    # Lcom/android/server/wm/WallpaperWindowToken;

    .line 14
    iget-object v0, p2, Lcom/android/server/wm/WallpaperWindowToken;->mResolvedTmpConfig:Landroid/content/res/Configuration;

    invoke-virtual {p2}, Lcom/android/server/wm/WallpaperWindowToken;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/res/Configuration;->equals(Landroid/content/res/Configuration;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 15
    iget-object v0, p2, Lcom/android/server/wm/WallpaperWindowToken;->mChildren:Lcom/android/server/wm/WindowList;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 16
    iget-object v1, p2, Lcom/android/server/wm/WallpaperWindowToken;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mProcessMap:Lcom/android/server/wm/WindowProcessControllerMap;

    iget-object v2, p2, Lcom/android/server/wm/WallpaperWindowToken;->mChildren:Lcom/android/server/wm/WindowList;

    .line 17
    invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/WindowState;

    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 18
    .local v1, "app":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->hasThread()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 19
    invoke-virtual {p2}, Lcom/android/server/wm/WallpaperWindowToken;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowProcessController;->onRequestedOverrideConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 15
    .end local v1    # "app":Lcom/android/server/wm/WindowProcessController;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 24
    .end local v0    # "i":I
    :cond_1
    iget-object v0, p2, Lcom/android/server/wm/WallpaperWindowToken;->mChildren:Lcom/android/server/wm/WindowList;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .restart local v0    # "i":I
    :goto_1
    if-ltz v0, :cond_3

    .line 25
    iget-object v1, p2, Lcom/android/server/wm/WallpaperWindowToken;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mProcessMap:Lcom/android/server/wm/WindowProcessControllerMap;

    iget-object v2, p2, Lcom/android/server/wm/WallpaperWindowToken;->mChildren:Lcom/android/server/wm/WindowList;

    .line 26
    invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/WindowState;

    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 27
    .restart local v1    # "app":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->hasThread()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 28
    invoke-virtual {p2}, Lcom/android/server/wm/WallpaperWindowToken;->getMergedOverrideConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowProcessController;->onMergedOverrideConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 24
    .end local v1    # "app":Lcom/android/server/wm/WindowProcessController;
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 31
    .end local v0    # "i":I
    :cond_3
    return-void
.end method
