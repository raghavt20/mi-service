.class public Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;
.super Ljava/lang/Object;
.source "MultiSenceManagerInternalStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/MultiSenceManagerInternalStub;


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.wm.MultiSenceManagerInternalStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;
    }
.end annotation


# static fields
.field private static final ACTION_MULTISENCE_FOCUSED_CHANGE:Ljava/lang/String; = "com.xiaomi.multisence.action.FOCUSED_CHANGE"

.field private static final DEBUG:Z

.field private static final EXTRA_MULTISENCE_FOCUSED_PACKAGE:Ljava/lang/String; = "com.xiaomi.multisence.extra.FOCUSED_PACKAGE"

.field private static final TAG:Ljava/lang/String; = "MultiSenceManagerInternalStubImpl"

.field private static mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

.field private static service:Lcom/android/server/wm/WindowManagerService;


# instance fields
.field private mBootComplete:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentFocusWindow:Lcom/android/server/wm/WindowState;

.field public mHandler:Landroid/os/Handler;


# direct methods
.method static bridge synthetic -$$Nest$fgetmBootComplete(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mBootComplete:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrentFocusWindow(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Lcom/android/server/wm/WindowState;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mCurrentFocusWindow:Lcom/android/server/wm/WindowState;

    return-void
.end method

.method static bridge synthetic -$$Nest$mLOG_IF_DEBUG(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->LOG_IF_DEBUG(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$misConnectToWMS(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->isConnectToWMS()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misWindowPackageChanged(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Lcom/android/server/wm/WindowState;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->isWindowPackageChanged(Lcom/android/server/wm/WindowState;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetmMultiSenceMI()Lcom/miui/server/multisence/MultiSenceManagerInternal;
    .locals 1

    sget-object v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetservice()Lcom/android/server/wm/WindowManagerService;
    .locals 1

    sget-object v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->service:Lcom/android/server/wm/WindowManagerService;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 40
    const-string v0, "persist.multisence.debug.on"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->DEBUG:Z

    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    .line 47
    sput-object v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->service:Lcom/android/server/wm/WindowManagerService;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mCurrentFocusWindow:Lcom/android/server/wm/WindowState;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mBootComplete:Z

    return-void
.end method

.method private LOG_IF_DEBUG(Ljava/lang/String;)V
    .locals 1
    .param p1, "log"    # Ljava/lang/String;

    .line 193
    sget-boolean v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "MultiSenceManagerInternalStubImpl"

    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_0
    return-void
.end method

.method private isConnectToWMS()Z
    .locals 2

    .line 153
    sget-object v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->service:Lcom/android/server/wm/WindowManagerService;

    if-nez v0, :cond_0

    .line 154
    const-string v0, "MultiSenceManagerInternalStubImpl"

    const-string v1, "MultiSenceManagerInternalStubImpl does not connect to WMS"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const/4 v0, 0x0

    return v0

    .line 157
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private isWindowPackageChanged(Lcom/android/server/wm/WindowState;)Z
    .locals 3
    .param p1, "newFocus"    # Lcom/android/server/wm/WindowState;

    .line 165
    if-nez p1, :cond_0

    const/4 v0, 0x0

    return v0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mCurrentFocusWindow:Lcom/android/server/wm/WindowState;

    const/4 v1, 0x1

    if-nez v0, :cond_1

    return v1

    .line 168
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mCurrentFocusWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public bootComplete()V
    .locals 1

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mBootComplete:Z

    .line 55
    return-void
.end method

.method public checkFWSupport(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 173
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->FW_ENABLE:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    .line 174
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    const-string v0, "floating window is not supported by multisence config"

    invoke-static {v1, v0}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 175
    return v2

    .line 177
    :cond_2
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    goto :goto_1

    .line 181
    :cond_3
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/miui/server/multisence/MultiSenceConfig;->floatingWindowWhiteList:Ljava/util/Set;

    monitor-enter v0

    .line 182
    :try_start_0
    invoke-static {}, Lcom/miui/server/multisence/MultiSenceConfig;->getInstance()Lcom/miui/server/multisence/MultiSenceConfig;

    move-result-object v3

    iget-object v3, v3, Lcom/miui/server/multisence/MultiSenceConfig;->floatingWindowWhiteList:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 183
    sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z

    const-string v3, "floating window is supported"

    invoke-static {v2, v3}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 184
    monitor-exit v0

    return v1

    .line 186
    :cond_4
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z

    const-string v1, "floating window is not supported"

    invoke-static {v0, v1}, Lcom/miui/server/multisence/MultiSenceServiceUtils;->msLogD(ZLjava/lang/String;)V

    .line 188
    return v2

    .line 186
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 178
    :cond_5
    :goto_1
    return v2
.end method

.method public getWindowsNeedToSched()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;"
        }
    .end annotation

    .line 161
    invoke-static {}, Lcom/android/server/wm/MultiSenceUtils;->getInstance()Lcom/android/server/wm/MultiSenceUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/MultiSenceUtils;->getWindowsNeedToSched()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public init(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 59
    iput-object p1, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mContext:Landroid/content/Context;

    .line 60
    iput-object p2, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mHandler:Landroid/os/Handler;

    .line 61
    return-void
.end method

.method public sendFocusWindowsBroadcast(Lcom/android/server/wm/WindowState;)V
    .locals 6
    .param p1, "window"    # Lcom/android/server/wm/WindowState;

    .line 138
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.multisence.action.FOCUSED_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 139
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, ""

    .line 140
    .local v1, "focusPackage":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v1

    .line 141
    const-string v2, "com.xiaomi.multisence.extra.FOCUSED_PACKAGE"

    const-string v3, "MultiSenceManagerInternalStubImpl"

    if-nez v1, :cond_0

    .line 142
    const-string v4, "focusPackage\u4e3anull"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const-string v3, "NO_PACKAGE"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    iget-object v2, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 146
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "packageName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    iget-object v2, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 150
    :goto_0
    return-void
.end method

.method public systemReady()Z
    .locals 2

    .line 65
    const-string v0, "miui_multi_sence"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/miui/server/multisence/MultiSenceManagerInternal;

    sput-object v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    .line 66
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    sput-object v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->service:Lcom/android/server/wm/WindowManagerService;

    .line 67
    sget-object v1, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    .line 71
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 68
    :cond_1
    :goto_0
    const-string v0, "MultiSenceManagerInternalStubImpl"

    const-string v1, "MultiSenceManagerInternalStubImpl does not connect to miui_multi_sence or WMS"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public updateScreenStatusWithFoucs(Lcom/android/server/wm/WindowState;)V
    .locals 2
    .param p1, "newFocus"    # Lcom/android/server/wm/WindowState;

    .line 133
    new-instance v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;

    invoke-direct {v0, p0, p1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;-><init>(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Lcom/android/server/wm/WindowState;)V

    .line 134
    .local v0, "runnable":Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;
    iget-object v1, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 135
    return-void
.end method
