.class Lcom/android/server/wm/MiuiFreeFormGestureController$5;
.super Landroid/database/ContentObserver;
.source "MiuiFreeFormGestureController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiFreeFormGestureController;->registerFreeformCloudDataObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiFreeFormGestureController;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 214
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$5;->this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .line 217
    const-string v0, "MiuiFreeFormGestureController"

    const-string v1, "receive notification of data update from app"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$5;->this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/util/MiuiMultiWindowUtils;->updateListFromCloud(Landroid/content/Context;)V

    .line 219
    return-void
.end method
