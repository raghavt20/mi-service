.class Lcom/android/server/wm/MiuiMultiWindowRecommendController$12;
.super Landroid/view/ViewOutlineProvider;
.source "MiuiMultiWindowRecommendController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setRadius(Landroid/view/View;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

.field final synthetic val$radius:F


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;F)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    .line 458
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$12;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iput p2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$12;->val$radius:F

    invoke-direct {p0}, Landroid/view/ViewOutlineProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public getOutline(Landroid/view/View;Landroid/graphics/Outline;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "outline"    # Landroid/graphics/Outline;

    .line 461
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    iget v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$12;->val$radius:F

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Outline;->setRoundRect(IIIIF)V

    .line 462
    return-void
.end method
