.class public Lcom/android/server/wm/AccountHelper;
.super Ljava/lang/Object;
.source "AccountHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/AccountHelper$AccountCallback;,
        Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;
    }
.end annotation


# static fields
.field private static DEBUG:Z = false

.field private static final LISTEN_MODE_ACCOUNT:I = 0x1

.field private static final LISTEN_MODE_NONE:I = 0x0

.field private static final LISTEN_MODE_WIFI:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MiuiPermision"

.field private static final mAccountLoginActivity:Ljava/lang/String;

.field private static final mCTAActivity:Landroid/content/ComponentName;

.field private static mCallBack:Lcom/android/server/wm/AccountHelper$AccountCallback;

.field private static mContext:Landroid/content/Context;

.field private static final mGrantPermissionsActivity:Landroid/content/ComponentName;

.field private static mInIMEIWhiteList:Z

.field private static mListeningActivity:Z

.field private static final mNotificationActivity:Ljava/lang/String;

.field private static final mPermissionActivity:Ljava/lang/String;

.field private static final mWifiSettingActivity:Ljava/lang/String;

.field private static sAccessActivities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private static sAccessPackage:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sAccountHelper:Lcom/android/server/wm/AccountHelper;


# instance fields
.field mActivityStateObserver:Landroid/app/IMiuiActivityObserver;

.field private mListenMode:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmListenMode(Lcom/android/server/wm/AccountHelper;)I
    .locals 0

    iget p0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/AccountHelper;->DEBUG:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmCallBack()Lcom/android/server/wm/AccountHelper$AccountCallback;
    .locals 1

    sget-object v0, Lcom/android/server/wm/AccountHelper;->mCallBack:Lcom/android/server/wm/AccountHelper$AccountCallback;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmContext()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/android/server/wm/AccountHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetsAccessActivities()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/android/server/wm/AccountHelper;->sAccessActivities:Ljava/util/ArrayList;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetsAccessPackage()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/android/server/wm/AccountHelper;->sAccessPackage:Ljava/util/ArrayList;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 8

    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/wm/AccountHelper;->sAccountHelper:Lcom/android/server/wm/AccountHelper;

    .line 31
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/wm/AccountHelper;->mListeningActivity:Z

    .line 32
    sput-boolean v0, Lcom/android/server/wm/AccountHelper;->mInIMEIWhiteList:Z

    .line 33
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/wm/AccountHelper;->DEBUG:Z

    .line 35
    new-instance v0, Ljava/lang/String;

    const-string v1, "com.xiaomi.account"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/wm/AccountHelper;->mAccountLoginActivity:Ljava/lang/String;

    .line 36
    new-instance v1, Ljava/lang/String;

    const-string v2, "com.xiaomi.passport"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/android/server/wm/AccountHelper;->mNotificationActivity:Ljava/lang/String;

    .line 38
    new-instance v2, Ljava/lang/String;

    const-string v3, "com.android.settings"

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/android/server/wm/AccountHelper;->mWifiSettingActivity:Ljava/lang/String;

    .line 40
    new-instance v3, Ljava/lang/String;

    const-string v4, "com.google.android.permissioncontroller"

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/android/server/wm/AccountHelper;->mPermissionActivity:Ljava/lang/String;

    .line 43
    new-instance v4, Landroid/content/ComponentName;

    const-string v5, "com.miui.securitycenter"

    const-string v6, "com.miui.permcenter.permissions.SystemAppPermissionDialogActivity"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v4, Lcom/android/server/wm/AccountHelper;->mCTAActivity:Landroid/content/ComponentName;

    .line 46
    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.lbe.security.miui"

    const-string v7, "com.android.packageinstaller.permission.ui.GrantPermissionsActivity"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v5, Lcom/android/server/wm/AccountHelper;->mGrantPermissionsActivity:Landroid/content/ComponentName;

    .line 53
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    sput-object v6, Lcom/android/server/wm/AccountHelper;->sAccessPackage:Ljava/util/ArrayList;

    .line 54
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    sput-object v6, Lcom/android/server/wm/AccountHelper;->sAccessActivities:Ljava/util/ArrayList;

    .line 60
    sget-object v6, Lcom/android/server/wm/AccountHelper;->sAccessPackage:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v0, Lcom/android/server/wm/AccountHelper;->sAccessPackage:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    sget-object v0, Lcom/android/server/wm/AccountHelper;->sAccessPackage:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Lcom/android/server/wm/AccountHelper;->sAccessPackage:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v0, Lcom/android/server/wm/AccountHelper;->sAccessActivities:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/android/server/wm/AccountHelper;->sAccessActivities:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I

    .line 216
    new-instance v0, Lcom/android/server/wm/AccountHelper$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/AccountHelper$2;-><init>(Lcom/android/server/wm/AccountHelper;)V

    iput-object v0, p0, Lcom/android/server/wm/AccountHelper;->mActivityStateObserver:Landroid/app/IMiuiActivityObserver;

    .line 57
    return-void
.end method

.method public static getInstance()Lcom/android/server/wm/AccountHelper;
    .locals 2

    .line 69
    sget-object v0, Lcom/android/server/wm/AccountHelper;->sAccountHelper:Lcom/android/server/wm/AccountHelper;

    if-nez v0, :cond_1

    .line 70
    const-class v0, Lcom/android/server/wm/AccountHelper;

    monitor-enter v0

    .line 71
    :try_start_0
    sget-object v1, Lcom/android/server/wm/AccountHelper;->sAccountHelper:Lcom/android/server/wm/AccountHelper;

    if-nez v1, :cond_0

    .line 72
    new-instance v1, Lcom/android/server/wm/AccountHelper;

    invoke-direct {v1}, Lcom/android/server/wm/AccountHelper;-><init>()V

    sput-object v1, Lcom/android/server/wm/AccountHelper;->sAccountHelper:Lcom/android/server/wm/AccountHelper;

    .line 74
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 76
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/wm/AccountHelper;->sAccountHelper:Lcom/android/server/wm/AccountHelper;

    return-object v0
.end method


# virtual methods
.method public ListenAccount(I)V
    .locals 2
    .param p1, "mode"    # I

    .line 143
    invoke-virtual {p0}, Lcom/android/server/wm/AccountHelper;->registerAccountActivityObserver()V

    .line 144
    iget v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I

    .line 145
    sget-boolean v0, Lcom/android/server/wm/AccountHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ListenAccount mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mListenMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPermision"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    return-void
.end method

.method public UnListenAccount(I)V
    .locals 2
    .param p1, "mode"    # I

    .line 151
    iget v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I

    xor-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I

    .line 152
    if-nez v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/android/server/wm/AccountHelper;->unRegisterAccountActivityObserver()V

    .line 155
    :cond_0
    sget-boolean v0, Lcom/android/server/wm/AccountHelper;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UnListenAccount mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mListenMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPermision"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_1
    return-void
.end method

.method public addAccount(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .line 189
    sget-boolean v0, Lcom/android/server/wm/AccountHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 190
    const-string v0, "MiuiPermision"

    const-string v1, "addAccount"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 193
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.xiaomi"

    const-string v3, "passportapi"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/android/server/wm/AccountHelper$1;

    invoke-direct {v7, p0, p1}, Lcom/android/server/wm/AccountHelper$1;-><init>(Lcom/android/server/wm/AccountHelper;Landroid/content/Context;)V

    const/4 v8, 0x0

    .line 194
    invoke-virtual/range {v1 .. v8}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 214
    return-void
.end method

.method public getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 92
    .local v1, "am":Landroid/accounts/AccountManager;
    const-string v2, "com.xiaomi"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 93
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v3, v2

    if-lez v3, :cond_0

    .line 94
    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 96
    :cond_0
    if-nez v0, :cond_1

    .line 97
    const-string v3, "MiuiPermision"

    const-string/jumbo v4, "xiaomi account is null"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_1
    return-object v0
.end method

.method public onXiaomiAccountLogin(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;

    .line 105
    sget-object v0, Lcom/android/server/wm/AccountHelper;->mCallBack:Lcom/android/server/wm/AccountHelper$AccountCallback;

    invoke-interface {v0}, Lcom/android/server/wm/AccountHelper$AccountCallback;->onXiaomiAccountLogin()V

    .line 106
    return-void
.end method

.method public onXiaomiAccountLogout(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;

    .line 110
    sget-object v0, Lcom/android/server/wm/AccountHelper;->mCallBack:Lcom/android/server/wm/AccountHelper$AccountCallback;

    invoke-interface {v0}, Lcom/android/server/wm/AccountHelper$AccountCallback;->onXiaomiAccountLogout()V

    .line 111
    return-void
.end method

.method public registerAccountActivityObserver()V
    .locals 4

    .line 161
    sget-boolean v0, Lcom/android/server/wm/AccountHelper;->mListeningActivity:Z

    if-eqz v0, :cond_0

    .line 162
    return-void

    .line 163
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/wm/AccountHelper;->mListeningActivity:Z

    .line 164
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 165
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    .line 166
    .local v1, "activityManager":Landroid/app/IActivityManager;
    if-nez v1, :cond_1

    .line 167
    return-void

    .line 170
    :cond_1
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMiuiActivityController()Lcom/android/server/wm/MiuiActivityController;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/AccountHelper;->mActivityStateObserver:Landroid/app/IMiuiActivityObserver;

    .line 171
    invoke-interface {v2, v3, v0}, Lcom/android/server/wm/MiuiActivityController;->registerActivityObserver(Landroid/app/IMiuiActivityObserver;Landroid/content/Intent;)V

    .line 172
    return-void
.end method

.method public registerAccountListener(Landroid/content/Context;Lcom/android/server/wm/AccountHelper$AccountCallback;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callBack"    # Lcom/android/server/wm/AccountHelper$AccountCallback;

    .line 83
    sput-object p1, Lcom/android/server/wm/AccountHelper;->mContext:Landroid/content/Context;

    .line 84
    sput-object p2, Lcom/android/server/wm/AccountHelper;->mCallBack:Lcom/android/server/wm/AccountHelper$AccountCallback;

    .line 85
    new-instance v0, Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;-><init>(Lcom/android/server/wm/AccountHelper;Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver-IA;)V

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 87
    return-void
.end method

.method public unRegisterAccountActivityObserver()V
    .locals 4

    .line 175
    sget-boolean v0, Lcom/android/server/wm/AccountHelper;->mListeningActivity:Z

    if-nez v0, :cond_0

    .line 176
    return-void

    .line 177
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/wm/AccountHelper;->mListeningActivity:Z

    .line 178
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 179
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    .line 180
    .local v1, "activityManager":Landroid/app/IActivityManager;
    if-nez v1, :cond_1

    .line 181
    return-void

    .line 184
    :cond_1
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMiuiActivityController()Lcom/android/server/wm/MiuiActivityController;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/AccountHelper;->mActivityStateObserver:Landroid/app/IMiuiActivityObserver;

    .line 185
    invoke-interface {v2, v3}, Lcom/android/server/wm/MiuiActivityController;->unregisterActivityObserver(Landroid/app/IMiuiActivityObserver;)V

    .line 186
    return-void
.end method
