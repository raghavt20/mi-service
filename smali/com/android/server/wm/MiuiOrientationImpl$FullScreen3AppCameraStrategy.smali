.class Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;
.super Ljava/lang/Object;
.source "MiuiOrientationImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiOrientationImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FullScreen3AppCameraStrategy"
.end annotation


# static fields
.field private static final DEFAULT_CAMERA_ROTATION:I = 0x0

.field private static final EMBEDDED_PROP_3APP_CAMERA_ROTATION:Ljava/lang/String; = "persist.vendor.device.orientation"

.field private static final EMBEDDED_PROP_3APP_ROTATE_CAMERA:Ljava/lang/String; = "persist.vendor.multiwin.3appcam"

.field private static final MSG_SET_CAMERA_ROTATE:I = 0x3e9

.field private static final MSG_SET_CAMERA_ROTATION:I = 0x3ea


# instance fields
.field private final mBgHandler:Landroid/os/Handler;

.field private mLastInitRotation:I

.field private mLastSetCameraRotation:Z

.field private mLastSetRotateCamera:Z

.field final synthetic this$0:Lcom/android/server/wm/MiuiOrientationImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiOrientationImpl;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiOrientationImpl;

    .line 1194
    iput-object p1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1205
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy$1;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy$1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->mBgHandler:Landroid/os/Handler;

    return-void
.end method

.method private needRecalculateCameraRotation(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 1265
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedCameraRotate(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    .line 1266
    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedRotateWhenCameraResume(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 1267
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misFixedAspectRatio(Lcom/android/server/wm/MiuiOrientationImpl;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    .line 1268
    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedRotateWhenCameraResumeInPad(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1265
    :goto_0
    return v0
.end method


# virtual methods
.method public update3appCameraRotate(Lcom/android/server/wm/ActivityRecord;)V
    .locals 5
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 1232
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedCameraRotate(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    .line 1233
    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misDisplayFolded(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedCameraRotateAll(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    .line 1234
    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedCameraRotateInPad(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1235
    .local v0, "rotateCamera":Z
    :goto_0
    iget-boolean v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->mLastSetRotateCamera:Z

    if-ne v3, v0, :cond_3

    .line 1236
    return-void

    .line 1238
    :cond_3
    if-eqz v0, :cond_4

    move v1, v2

    .line 1239
    .local v1, "rotate":I
    :cond_4
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->mBgHandler:Landroid/os/Handler;

    const/16 v3, 0x3e9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 1240
    .local v2, "message":Landroid/os/Message;
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 1241
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->mLastSetRotateCamera:Z

    .line 1242
    return-void
.end method

.method public update3appCameraRotation(Lcom/android/server/wm/ActivityRecord;IZZ)V
    .locals 5
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "rotation"    # I
    .param p3, "isRotated"    # Z
    .param p4, "isFoldChanged"    # Z

    .line 1246
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedCameraRotate(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 1247
    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    .line 1248
    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedCameraRotateInPad(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1249
    .local v0, "shouldRotate":Z
    :goto_0
    if-nez v0, :cond_3

    if-eqz p4, :cond_a

    .line 1250
    :cond_3
    if-eqz v0, :cond_5

    if-nez p3, :cond_4

    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    .line 1251
    invoke-static {v3, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedRotateWhenCameraResume(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 1252
    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misFixedAspectRatio(Lcom/android/server/wm/MiuiOrientationImpl;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    .line 1253
    invoke-static {v3, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$misNeedRotateWhenCameraResumeInPad(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    goto :goto_1

    :cond_5
    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->mLastSetCameraRotation:Z

    .line 1254
    if-nez p3, :cond_6

    if-nez p4, :cond_6

    iput p2, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->mLastInitRotation:I

    .line 1255
    :cond_6
    if-eqz v1, :cond_7

    move v2, p2

    :cond_7
    move v1, v2

    .line 1256
    .local v1, "rotationToSet":I
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->needRecalculateCameraRotation(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v2

    if-eqz v2, :cond_9

    if-nez p3, :cond_8

    if-eqz p4, :cond_9

    .line 1257
    :cond_8
    iget v2, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->mLastInitRotation:I

    invoke-static {v2, v1}, Landroid/util/RotationUtils;->deltaRotation(II)I

    move-result v1

    .line 1259
    :cond_9
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->mBgHandler:Landroid/os/Handler;

    const/16 v3, 0x3ea

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 1260
    .local v2, "message":Landroid/os/Message;
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 1262
    .end local v1    # "rotationToSet":I
    .end local v2    # "message":Landroid/os/Message;
    :cond_a
    return-void
.end method
