class com.android.server.wm.OrientationSensorJudgeImpl$1 implements java.lang.Runnable {
	 /* .source "OrientationSensorJudgeImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/OrientationSensorJudgeImpl;->updateForegroundApp()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.OrientationSensorJudgeImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.OrientationSensorJudgeImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/OrientationSensorJudgeImpl; */
/* .line 81 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 85 */
try { // :try_start_0
	 v0 = this.this$0;
	 com.android.server.wm.OrientationSensorJudgeImpl .-$$Nest$fgetmActivityTaskManager ( v0 );
	 /* .line 86 */
	 /* .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
	 if ( v0 != null) { // if-eqz v0, :cond_7
		 v1 = this.topActivity;
		 /* if-nez v1, :cond_0 */
		 /* goto/16 :goto_1 */
		 /* .line 89 */
	 } // :cond_0
	 v1 = 	 (( android.app.ActivityTaskManager$RootTaskInfo ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 int v2 = 5; // const/4 v2, 0x5
	 final String v3 = "OrientationSensorJudgeImpl"; // const-string v3, "OrientationSensorJudgeImpl"
	 /* if-eq v1, v2, :cond_5 */
	 try { // :try_start_1
		 v1 = 		 (( android.app.ActivityTaskManager$RootTaskInfo ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
		 int v2 = 6; // const/4 v2, 0x6
		 /* if-ne v1, v2, :cond_1 */
		 /* .line 96 */
	 } // :cond_1
	 v1 = this.topActivity;
	 (( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
	 /* .line 97 */
	 /* .local v1, "packageName":Ljava/lang/String; */
	 if ( v1 != null) { // if-eqz v1, :cond_3
		 v2 = this.this$0;
		 com.android.server.wm.OrientationSensorJudgeImpl .-$$Nest$fgetmForegroundAppPackageName ( v2 );
		 /* .line 98 */
		 v2 = 		 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v2 != null) { // if-eqz v2, :cond_3
			 /* .line 99 */
			 v2 = 			 com.android.server.wm.OrientationSensorJudgeImpl .-$$Nest$sfgetLOG ( );
			 if ( v2 != null) { // if-eqz v2, :cond_2
				 /* .line 100 */
				 /* const-string/jumbo v2, "updateForegroundApp, app didn\'t change, nothing to do!" */
				 android.util.Slog .w ( v3,v2 );
				 /* .line 102 */
			 } // :cond_2
			 return;
			 /* .line 104 */
		 } // :cond_3
		 v2 = this.this$0;
		 com.android.server.wm.OrientationSensorJudgeImpl .-$$Nest$fputmPendingForegroundAppPackageName ( v2,v1 );
		 /* .line 105 */
		 v2 = 		 com.android.server.wm.OrientationSensorJudgeImpl .-$$Nest$sfgetLOG ( );
		 if ( v2 != null) { // if-eqz v2, :cond_4
			 /* .line 106 */
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 /* const-string/jumbo v4, "updateForegroundApp, packageName = " */
			 (( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .w ( v3,v2 );
			 /* .line 108 */
		 } // :cond_4
		 v2 = this.this$0;
		 com.android.server.wm.OrientationSensorJudgeImpl .-$$Nest$fgetmHandler ( v2 );
		 int v3 = 1; // const/4 v3, 0x1
		 (( android.os.Handler ) v2 ).sendEmptyMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
		 /* .line 111 */
		 /* nop */
	 } // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v1 # "packageName":Ljava/lang/String;
/* .line 91 */
/* .restart local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
} // :cond_5
} // :goto_0
v1 = com.android.server.wm.OrientationSensorJudgeImpl .-$$Nest$sfgetLOG ( );
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 92 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateForegroundApp, WindowingMode = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( android.app.ActivityTaskManager$RootTaskInfo ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v1 );
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 94 */
} // :cond_6
return;
/* .line 87 */
} // :cond_7
} // :goto_1
return;
/* .line 109 */
} // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
/* :catch_0 */
/* move-exception v0 */
/* .line 112 */
} // :goto_2
return;
} // .end method
