.class Lcom/android/server/wm/MiuiSizeCompatService$4;
.super Landroid/content/BroadcastReceiver;
.source "MiuiSizeCompatService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiSizeCompatService;->registerUserSwitchReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiSizeCompatService;

    .line 696
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService$4;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 699
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "android.intent.action.USER_UNLOCKED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 707
    const-string v0, "MiuiSizeCompatService"

    const-string v1, "Other action."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 704
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$4;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmBgHandler(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/MiuiSizeCompatService$H;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$4;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmBgHandler(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/MiuiSizeCompatService$H;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$H;->sendMessage(Landroid/os/Message;)Z

    .line 705
    goto :goto_2

    .line 701
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$4;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmBgHandler(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/MiuiSizeCompatService$H;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$4;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmBgHandler(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/MiuiSizeCompatService$H;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$H;->sendMessage(Landroid/os/Message;)Z

    .line 702
    nop

    .line 710
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x31af1c32 -> :sswitch_1
        0x392cb822 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
