.class public Lcom/android/server/wm/MiuiSizeCompatJob;
.super Landroid/app/job/JobService;
.source "MiuiSizeCompatJob.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiSizeCompatJob$H;
    }
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000000779"

.field private static final APP_PKG:Ljava/lang/String; = "android"

.field private static final EVENT_NAME:Ljava/lang/String; = "status"

.field private static final EVENT_STATUS_KEY:Ljava/lang/String; = "app_display_status"

.field private static final EVENT_STATUS_NAME_KEY:Ljava/lang/String; = "app_display_name"

.field private static final EVENT_STATUS_PACKAGE_KEY:Ljava/lang/String; = "app_package_name"

.field private static final EVENT_STATUS_STYLE_KEY:Ljava/lang/String; = "app_display_style"

.field private static final EVENT_STYLE_EMBEDED_VALUE:Ljava/lang/String; = "\u5e73\u884c\u7a97\u53e3"

.field private static final SERVER_CLASS_NAME:Ljava/lang/String; = "com.miui.analytics.onetrack.TrackService"

.field private static final SERVER_PKG_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final SIZE_COMPAT_JOB_ID:I = 0x82f5

.field private static final TAG:Ljava/lang/String; = "MiuiSizeCompatJob"

.field private static final mCName:Landroid/content/ComponentName;


# instance fields
.field private mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

.field private mH:Landroid/os/Handler;

.field private mITrackBinder:Lcom/miui/analytics/ITrackBinder;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mTrackLock:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDeathRecipient(Lcom/android/server/wm/MiuiSizeCompatJob;)Landroid/os/IBinder$DeathRecipient;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmH(Lcom/android/server/wm/MiuiSizeCompatJob;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mH:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiSizeCompatJob;)Lcom/miui/analytics/ITrackBinder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTrackLock(Lcom/android/server/wm/MiuiSizeCompatJob;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mTrackLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmITrackBinder(Lcom/android/server/wm/MiuiSizeCompatJob;Lcom/miui/analytics/ITrackBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    return-void
.end method

.method static bridge synthetic -$$Nest$mbindOneTrackService(Lcom/android/server/wm/MiuiSizeCompatJob;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatJob;->bindOneTrackService()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtrackEvent(Lcom/android/server/wm/MiuiSizeCompatJob;Landroid/app/job/JobParameters;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatJob;->trackEvent(Landroid/app/job/JobParameters;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 41
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android"

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/wm/MiuiSizeCompatJob;->mCName:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mTrackLock:Ljava/lang/Object;

    .line 101
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatJob$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiSizeCompatJob$1;-><init>(Lcom/android/server/wm/MiuiSizeCompatJob;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 126
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatJob$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiSizeCompatJob$2;-><init>(Lcom/android/server/wm/MiuiSizeCompatJob;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    return-void
.end method

.method private bindOneTrackService()V
    .locals 5

    .line 144
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 145
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v1, :cond_0

    .line 146
    const-string v1, "MiuiSizeCompatJob"

    const-string v2, "Already bound."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 150
    :cond_0
    :try_start_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 151
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.miui.analytics"

    const-string v3, "com.miui.analytics.onetrack.TrackService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mServiceConnection:Landroid/content/ServiceConnection;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const/4 v4, 0x1

    invoke-virtual {p0, v1, v2, v4, v3}, Lcom/android/server/wm/MiuiSizeCompatJob;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 153
    :catch_0
    move-exception v1

    .line 154
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 156
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v0

    .line 157
    return-void

    .line 156
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private getEmbeddedApps()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 246
    const/4 v0, 0x0

    .line 254
    .local v0, "embeddedApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;"
    return-object v0
.end method

.method public static sheduleJob(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 65
    const-string v0, "jobscheduler"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 66
    .local v0, "js":Landroid/app/job/JobScheduler;
    new-instance v1, Landroid/app/job/JobInfo$Builder;

    const v2, 0x82f5

    sget-object v3, Lcom/android/server/wm/MiuiSizeCompatJob;->mCName:Landroid/content/ComponentName;

    invoke-direct {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 67
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 68
    const-wide/32 v2, 0x5265c00

    invoke-virtual {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 69
    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    .line 70
    .local v1, "jobInfo":Landroid/app/job/JobInfo;
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 71
    return-void
.end method

.method private trackEvent(Landroid/app/job/JobParameters;)V
    .locals 13
    .param p1, "parameters"    # Landroid/app/job/JobParameters;

    .line 196
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 197
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 198
    .local v1, "start":J
    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v3, :cond_0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    return-void

    .line 200
    :cond_0
    const/4 v3, 0x0

    :try_start_1
    invoke-static {}, Landroid/sizecompat/MiuiSizeCompatManager;->getMiuiSizeCompatInstalledApps()Ljava/util/Map;

    move-result-object v4

    .line 201
    .local v4, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatJob;->getEmbeddedApps()Ljava/util/Map;

    move-result-object v5

    .line 202
    .local v5, "embeddedApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v6, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v6, :cond_2

    if-eqz v4, :cond_2

    .line 203
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 204
    .local v6, "object":Lorg/json/JSONObject;
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 205
    .local v7, "array":Lorg/json/JSONArray;
    new-instance v8, Lcom/android/server/wm/MiuiSizeCompatJob$3;

    invoke-direct {v8, p0, v5, v7}, Lcom/android/server/wm/MiuiSizeCompatJob$3;-><init>(Lcom/android/server/wm/MiuiSizeCompatJob;Ljava/util/Map;Lorg/json/JSONArray;)V

    invoke-interface {v4, v8}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 224
    const-string v8, "EVENT_NAME"

    const-string/jumbo v9, "status"

    invoke-virtual {v6, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 225
    const-string v8, "app_display_status"

    invoke-virtual {v6, v8, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 226
    iget-object v8, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    const-string v9, "31000000779"

    const-string v10, "android"

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v8, v9, v10, v11, v3}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 227
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v1

    .line 228
    .local v8, "took":J
    sget-boolean v10, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z

    if-eqz v10, :cond_1

    .line 229
    const-string v10, "MiuiSizeCompatJob"

    invoke-virtual {v7}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_1
    const-string v10, "MiuiSizeCompatJob"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Track "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " events took "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ms."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    .end local v4    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    .end local v5    # "embeddedApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v6    # "object":Lorg/json/JSONObject;
    .end local v7    # "array":Lorg/json/JSONArray;
    .end local v8    # "took":J
    :cond_2
    :try_start_2
    invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/MiuiSizeCompatJob;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 237
    const-string v3, "MiuiSizeCompatJob"

    const-string v4, "Job finished"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 236
    :catchall_0
    move-exception v4

    goto :goto_1

    .line 233
    :catch_0
    move-exception v4

    .line 234
    .local v4, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 236
    .end local v4    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/MiuiSizeCompatJob;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 237
    const-string v3, "MiuiSizeCompatJob"

    const-string v4, "Job finished"

    :goto_0
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    nop

    .line 239
    .end local v1    # "start":J
    monitor-exit v0

    .line 240
    return-void

    .line 236
    .restart local v1    # "start":J
    :goto_1
    invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/MiuiSizeCompatJob;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 237
    const-string v3, "MiuiSizeCompatJob"

    const-string v5, "Job finished"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    nop

    .end local p0    # "this":Lcom/android/server/wm/MiuiSizeCompatJob;
    .end local p1    # "parameters":Landroid/app/job/JobParameters;
    throw v4

    .line 239
    .end local v1    # "start":J
    .restart local p0    # "this":Lcom/android/server/wm/MiuiSizeCompatJob;
    .restart local p1    # "parameters":Landroid/app/job/JobParameters;
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .line 58
    const-string v0, "MiuiSizeCompatJob"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatJob$H;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiSizeCompatJob$H;-><init>(Lcom/android/server/wm/MiuiSizeCompatJob;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mH:Landroid/os/Handler;

    .line 60
    invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatJob;->bindOneTrackService()V

    .line 61
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 259
    invoke-super {p0}, Landroid/app/job/JobService;->onDestroy()V

    .line 260
    const-string v0, "MiuiSizeCompatJob"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatJob;->unBindOneTrackService()V

    .line 262
    return-void
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 5
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 177
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v0

    const v1, 0x82f5

    if-ne v0, v1, :cond_0

    .line 178
    const-string v0, "MiuiSizeCompatJob"

    const-string v1, "Start job!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mH:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 180
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mH:Landroid/os/Handler;

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 181
    return v1

    .line 183
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 2
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 188
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v0

    const v1, 0x82f5

    if-ne v0, v1, :cond_0

    .line 189
    const-string v0, "MiuiSizeCompatJob"

    const-string v1, "Stop job!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public unBindOneTrackService()V
    .locals 5

    .line 160
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 161
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 162
    invoke-interface {v1}, Lcom/miui/analytics/ITrackBinder;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 163
    iput-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    .line 164
    const-string v1, "MiuiSizeCompatJob"

    const-string/jumbo v3, "unbind OneTrack Service success."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 166
    :cond_0
    const-string v1, "MiuiSizeCompatJob"

    const-string/jumbo v3, "unbind OneTrack Service fail."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :goto_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_1

    .line 169
    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiSizeCompatJob;->unbindService(Landroid/content/ServiceConnection;)V

    .line 170
    iput-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatJob;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 172
    :cond_1
    monitor-exit v0

    .line 173
    return-void

    .line 172
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
