.class Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;
.super Ljava/lang/Object;
.source "MultiSenceManagerInternalStubImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateScreenStatus"
.end annotation


# instance fields
.field private mNewFocus:Lcom/android/server/wm/WindowState;

.field final synthetic this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Lcom/android/server/wm/WindowState;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;
    .param p2, "newFocus"    # Lcom/android/server/wm/WindowState;

    .line 76
    iput-object p1, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p2, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->mNewFocus:Lcom/android/server/wm/WindowState;

    .line 78
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 82
    invoke-static {}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$sfgetmMultiSenceMI()Lcom/miui/server/multisence/MultiSenceManagerInternal;

    move-result-object v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    const-string v1, "not connect to multi-sence core service"

    invoke-static {v0, v1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$mLOG_IF_DEBUG(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Ljava/lang/String;)V

    .line 84
    return-void

    .line 87
    :cond_0
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-nez v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    const-string v1, "func not enable due to Cloud Controller"

    invoke-static {v0, v1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$mLOG_IF_DEBUG(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Ljava/lang/String;)V

    .line 89
    return-void

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->mNewFocus:Lcom/android/server/wm/WindowState;

    if-nez v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    const-string v1, "new focus is null"

    invoke-static {v0, v1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$mLOG_IF_DEBUG(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Ljava/lang/String;)V

    .line 94
    return-void

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    invoke-static {v0}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    invoke-static {v0}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$fgetmBootComplete(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 98
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    iget-object v1, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->mNewFocus:Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->sendFocusWindowsBroadcast(Lcom/android/server/wm/WindowState;)V

    .line 101
    :cond_3
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    iget-object v1, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->mNewFocus:Lcom/android/server/wm/WindowState;

    invoke-static {v0, v1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$misWindowPackageChanged(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Lcom/android/server/wm/WindowState;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 102
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    iget-object v1, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->mNewFocus:Lcom/android/server/wm/WindowState;

    invoke-static {v0, v1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$fputmCurrentFocusWindow(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Lcom/android/server/wm/WindowState;)V

    .line 103
    return-void

    .line 105
    :cond_4
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    iget-object v1, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->mNewFocus:Lcom/android/server/wm/WindowState;

    invoke-static {v0, v1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$fputmCurrentFocusWindow(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Lcom/android/server/wm/WindowState;)V

    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "sWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    iget-object v1, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    invoke-static {v1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$misConnectToWMS(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;)Z

    move-result v1

    .line 110
    .local v1, "connection":Z
    if-nez v1, :cond_5

    return-void

    .line 112
    :cond_5
    invoke-static {}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$sfgetservice()Lcom/android/server/wm/WindowManagerService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;

    move-result-object v2

    monitor-enter v2

    .line 113
    :try_start_0
    const-string/jumbo v3, "updateScreenStatusWithFoucs"

    const-wide/16 v4, 0x20

    invoke-static {v4, v5, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 114
    iget-object v3, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->this$0:Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;

    invoke-virtual {v3}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->getWindowsNeedToSched()Ljava/util/Map;

    move-result-object v3

    move-object v0, v3

    .line 115
    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->mNewFocus:Lcom/android/server/wm/WindowState;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 116
    iget-object v3, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;->mNewFocus:Lcom/android/server/wm/WindowState;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/multisence/SingleWindowInfo;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/miui/server/multisence/SingleWindowInfo;->setFocused(Z)Lcom/miui/server/multisence/SingleWindowInfo;

    .line 118
    :cond_6
    invoke-static {v4, v5}, Landroid/os/Trace;->traceEnd(J)V

    .line 119
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 121
    invoke-static {}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$sfgetmMultiSenceMI()Lcom/miui/server/multisence/MultiSenceManagerInternal;

    move-result-object v3

    monitor-enter v3

    .line 122
    :try_start_1
    invoke-static {}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$sfgetmMultiSenceMI()Lcom/miui/server/multisence/MultiSenceManagerInternal;

    move-result-object v2

    const-string v4, "focus"

    invoke-interface {v2, v4}, Lcom/miui/server/multisence/MultiSenceManagerInternal;->setUpdateReason(Ljava/lang/String;)V

    .line 123
    invoke-static {}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->-$$Nest$sfgetmMultiSenceMI()Lcom/miui/server/multisence/MultiSenceManagerInternal;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/miui/server/multisence/MultiSenceManagerInternal;->updateStaticWindowsInfo(Ljava/util/Map;)V

    .line 124
    monitor-exit v3

    .line 126
    return-void

    .line 124
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 119
    :catchall_1
    move-exception v3

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3
.end method
