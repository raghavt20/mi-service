public class com.android.server.wm.MiuiMirrorDragDropController {
	 /* .source "MiuiMirrorDragDropController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long DRAG_TIMEOUT_MS;
static final Integer MSG_DRAG_END_TIMEOUT;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.wm.MiuiMirrorDragState mDragState;
private final android.os.Handler mHandler;
private com.android.server.wm.WindowManagerService mService;
/* # direct methods */
static com.android.server.wm.MiuiMirrorDragState -$$Nest$fgetmDragState ( com.android.server.wm.MiuiMirrorDragDropController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDragState;
} // .end method
public com.android.server.wm.MiuiMirrorDragDropController ( ) {
	 /* .locals 3 */
	 /* .line 47 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 48 */
	 /* const-string/jumbo v0, "window" */
	 android.os.ServiceManager .getService ( v0 );
	 /* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
	 this.mService = v0;
	 /* .line 49 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler; */
	 v1 = this.mService;
	 v2 = this.mH;
	 (( com.android.server.wm.WindowManagerService$H ) v2 ).getLooper ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService$H;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1, v2}, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;-><init>(Lcom/android/server/wm/MiuiMirrorDragDropController;Lcom/android/server/wm/WindowManagerService;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 50 */
	 return;
} // .end method
/* # virtual methods */
public void cancelDragAndDrop ( android.os.IBinder p0 ) {
	 /* .locals 3 */
	 /* .param p1, "dragToken" # Landroid/os/IBinder; */
	 /* .line 162 */
	 v0 = this.mService;
	 v0 = this.mGlobalLock;
	 /* monitor-enter v0 */
	 /* .line 163 */
	 try { // :try_start_0
		 v1 = this.mDragState;
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 168 */
			 v1 = this.mToken;
			 /* if-ne v1, p1, :cond_0 */
			 /* .line 175 */
			 v1 = this.mDragState;
			 int v2 = 0; // const/4 v2, 0x0
			 /* iput-boolean v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z */
			 /* .line 176 */
			 v1 = this.mDragState;
			 (( com.android.server.wm.MiuiMirrorDragState ) v1 ).closeLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
			 /* .line 177 */
			 /* monitor-exit v0 */
			 /* .line 178 */
			 return;
			 /* .line 169 */
		 } // :cond_0
		 final String v1 = "MiuiMirrorDragDrop"; // const-string v1, "MiuiMirrorDragDrop"
		 final String v2 = "cancelDragAndDrop() does not match prepareDrag()"; // const-string v2, "cancelDragAndDrop() does not match prepareDrag()"
		 android.util.Slog .w ( v1,v2 );
		 /* .line 171 */
		 /* new-instance v1, Ljava/lang/IllegalStateException; */
		 final String v2 = "cancelDragAndDrop() does not match prepareDrag()"; // const-string v2, "cancelDragAndDrop() does not match prepareDrag()"
		 /* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
	 } // .end local p0 # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
} // .end local p1 # "dragToken":Landroid/os/IBinder;
/* throw v1 */
/* .line 164 */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiMirrorDragDropController; */
/* .restart local p1 # "dragToken":Landroid/os/IBinder; */
} // :cond_1
final String v1 = "MiuiMirrorDragDrop"; // const-string v1, "MiuiMirrorDragDrop"
final String v2 = "cancelDragAndDrop() without prepareDrag()"; // const-string v2, "cancelDragAndDrop() without prepareDrag()"
android.util.Slog .w ( v1,v2 );
/* .line 165 */
/* new-instance v1, Ljava/lang/IllegalStateException; */
final String v2 = "cancelDragAndDrop() without prepareDrag()"; // const-string v2, "cancelDragAndDrop() without prepareDrag()"
/* invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
} // .end local p1 # "dragToken":Landroid/os/IBinder;
/* throw v1 */
/* .line 177 */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiMirrorDragDropController; */
/* .restart local p1 # "dragToken":Landroid/os/IBinder; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean dragDropActiveLocked ( ) {
/* .locals 1 */
/* .line 39 */
v0 = this.mDragState;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.wm.MiuiMirrorDragState ) v0 ).isClosing ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMirrorDragState;->isClosing()Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void injectDragEvent ( Integer p0, Integer p1, Float p2, Float p3 ) {
/* .locals 3 */
/* .param p1, "action" # I */
/* .param p2, "displayId" # I */
/* .param p3, "newX" # F */
/* .param p4, "newY" # F */
/* .line 181 */
v0 = this.mService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 182 */
try { // :try_start_0
v1 = (( com.android.server.wm.MiuiMirrorDragDropController ) p0 ).dragDropActiveLocked ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->dragDropActiveLocked()Z
/* if-nez v1, :cond_0 */
/* .line 183 */
/* monitor-exit v0 */
return;
/* .line 185 */
} // :cond_0
/* packed-switch p1, :pswitch_data_0 */
/* .line 196 */
/* :pswitch_0 */
v1 = this.mDragState;
/* const/high16 v2, -0x40800000 # -1.0f */
(( com.android.server.wm.MiuiMirrorDragState ) v1 ).notifyMoveLocked ( p2, v2, v2 ); // invoke-virtual {v1, p2, v2, v2}, Lcom/android/server/wm/MiuiMirrorDragState;->notifyMoveLocked(IFF)V
/* .line 197 */
/* .line 187 */
/* :pswitch_1 */
v1 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v1 ).broadcastDragStartedLocked ( p2, p3, p4 ); // invoke-virtual {v1, p2, p3, p4}, Lcom/android/server/wm/MiuiMirrorDragState;->broadcastDragStartedLocked(IFF)V
/* .line 188 */
/* .line 201 */
/* :pswitch_2 */
v1 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v1 ).closeLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
/* .line 202 */
/* .line 193 */
/* :pswitch_3 */
v1 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v1 ).notifyDropLocked ( p2, p3, p4 ); // invoke-virtual {v1, p2, p3, p4}, Lcom/android/server/wm/MiuiMirrorDragState;->notifyDropLocked(IFF)V
/* .line 194 */
/* .line 190 */
/* :pswitch_4 */
v1 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v1 ).notifyMoveLocked ( p2, p3, p4 ); // invoke-virtual {v1, p2, p3, p4}, Lcom/android/server/wm/MiuiMirrorDragState;->notifyMoveLocked(IFF)V
/* .line 191 */
/* .line 199 */
/* :pswitch_5 */
/* nop */
/* .line 206 */
} // :goto_0
/* monitor-exit v0 */
/* .line 207 */
return;
/* .line 206 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Integer lastTargetUid ( ) {
/* .locals 1 */
/* .line 43 */
v0 = this.mDragState;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mTargetWindow;
/* if-nez v0, :cond_0 */
/* .line 44 */
} // :cond_0
v0 = this.mDragState;
v0 = this.mTargetWindow;
v0 = (( com.android.server.wm.WindowState ) v0 ).getOwningUid ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getOwningUid()I
/* .line 43 */
} // :cond_1
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
} // .end method
void onDragStateClosedLocked ( com.android.server.wm.MiuiMirrorDragState p0 ) {
/* .locals 2 */
/* .param p1, "dragState" # Lcom/android/server/wm/MiuiMirrorDragState; */
/* .line 229 */
v0 = this.mDragState;
/* if-eq v0, p1, :cond_0 */
/* .line 230 */
final String v0 = "MiuiMirrorDragDrop"; // const-string v0, "MiuiMirrorDragDrop"
final String v1 = "Unknown drag state is closed"; // const-string v1, "Unknown drag state is closed"
android.util.Slog .wtf ( v0,v1 );
/* .line 231 */
return;
/* .line 233 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mDragState = v0;
/* .line 234 */
return;
} // .end method
public android.os.IBinder performDrag ( Integer p0, Integer p1, android.view.IWindow p2, Integer p3, Integer p4, android.content.ClipData p5 ) {
/* .locals 7 */
/* .param p1, "callerPid" # I */
/* .param p2, "callerUid" # I */
/* .param p3, "window" # Landroid/view/IWindow; */
/* .param p4, "flags" # I */
/* .param p5, "displayId" # I */
/* .param p6, "data" # Landroid/content/ClipData; */
/* .line 58 */
/* new-instance v0, Landroid/os/Binder; */
/* invoke-direct {v0}, Landroid/os/Binder;-><init>()V */
/* .line 59 */
/* .local v0, "dragToken":Landroid/os/IBinder; */
v1 = this.mService;
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 61 */
try { // :try_start_0
v2 = (( com.android.server.wm.MiuiMirrorDragDropController ) p0 ).dragDropActiveLocked ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->dragDropActiveLocked()Z
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 62 */
final String v2 = "MiuiMirrorDragDrop"; // const-string v2, "MiuiMirrorDragDrop"
final String v4 = "Mirror drag already in progress"; // const-string v4, "Mirror drag already in progress"
android.util.Slog .w ( v2,v4 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 63 */
/* nop */
/* .line 95 */
try { // :try_start_1
v2 = this.mDragState;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = (( com.android.server.wm.MiuiMirrorDragState ) v2 ).isInProgress ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z
/* if-nez v2, :cond_0 */
/* .line 96 */
v2 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v2 ).closeLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
} // :cond_0
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 63 */
/* .line 66 */
} // :cond_1
try { // :try_start_2
v2 = this.mService;
v2 = this.mDragDropController;
v2 = (( com.android.server.wm.DragDropController ) v2 ).dragDropActiveLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DragDropController;->dragDropActiveLocked()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 67 */
final String v2 = "MiuiMirrorDragDrop"; // const-string v2, "MiuiMirrorDragDrop"
final String v4 = "Normal drag in progress"; // const-string v4, "Normal drag in progress"
android.util.Slog .w ( v2,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 68 */
/* nop */
/* .line 95 */
try { // :try_start_3
v2 = this.mDragState;
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = (( com.android.server.wm.MiuiMirrorDragState ) v2 ).isInProgress ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z
/* if-nez v2, :cond_2 */
/* .line 96 */
v2 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v2 ).closeLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
} // :cond_2
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 68 */
/* .line 72 */
} // :cond_3
try { // :try_start_4
com.xiaomi.mirror.service.MirrorServiceInternal .getInstance ( );
v2 = (( com.xiaomi.mirror.service.MirrorServiceInternal ) v2 ).getDelegatePid ( ); // invoke-virtual {v2}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getDelegatePid()I
/* if-ne p1, v2, :cond_4 */
/* .line 73 */
final String v2 = "MiuiMirrorDragDrop"; // const-string v2, "MiuiMirrorDragDrop"
final String v3 = "drag from mirror"; // const-string v3, "drag from mirror"
android.util.Slog .d ( v2,v3 );
/* .line 74 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "winBinder":Landroid/os/IBinder; */
/* .line 76 */
} // .end local v2 # "winBinder":Landroid/os/IBinder;
} // :cond_4
v2 = this.mService;
int v4 = 0; // const/4 v4, 0x0
(( com.android.server.wm.WindowManagerService ) v2 ).windowForClientLocked ( v3, p3, v4 ); // invoke-virtual {v2, v3, p3, v4}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/view/IWindow;Z)Lcom/android/server/wm/WindowState;
/* .line 78 */
/* .local v2, "callingWin":Lcom/android/server/wm/WindowState; */
/* if-nez v2, :cond_6 */
/* .line 79 */
final String v4 = "MiuiMirrorDragDrop"; // const-string v4, "MiuiMirrorDragDrop"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Bad requesting window "; // const-string v6, "Bad requesting window "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p3 ); // invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 80 */
/* nop */
/* .line 95 */
try { // :try_start_5
v4 = this.mDragState;
if ( v4 != null) { // if-eqz v4, :cond_5
v4 = (( com.android.server.wm.MiuiMirrorDragState ) v4 ).isInProgress ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z
/* if-nez v4, :cond_5 */
/* .line 96 */
v4 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v4 ).closeLocked ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
} // :cond_5
/* monitor-exit v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 80 */
/* .line 83 */
} // :cond_6
try { // :try_start_6
/* move-object v2, v3 */
/* .line 86 */
/* .local v2, "winBinder":Landroid/os/IBinder; */
} // :goto_0
/* new-instance v3, Lcom/android/server/wm/MiuiMirrorDragState; */
v4 = this.mService;
/* invoke-direct {v3, v4, p0, p4, v2}, Lcom/android/server/wm/MiuiMirrorDragState;-><init>(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/MiuiMirrorDragDropController;ILandroid/os/IBinder;)V */
this.mDragState = v3;
/* .line 87 */
/* iput p1, v3, Lcom/android/server/wm/MiuiMirrorDragState;->mPid:I */
/* .line 88 */
v3 = this.mDragState;
/* iput p2, v3, Lcom/android/server/wm/MiuiMirrorDragState;->mUid:I */
/* .line 89 */
v3 = this.mDragState;
this.mToken = v0;
/* .line 90 */
v3 = this.mDragState;
/* iput p5, v3, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceDisplayId:I */
/* .line 92 */
v3 = this.mDragState;
this.mData = p6;
/* .line 93 */
v3 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v3 ).prepareDragStartedLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiMirrorDragState;->prepareDragStartedLocked()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 95 */
} // .end local v2 # "winBinder":Landroid/os/IBinder;
try { // :try_start_7
v2 = this.mDragState;
if ( v2 != null) { // if-eqz v2, :cond_7
v2 = (( com.android.server.wm.MiuiMirrorDragState ) v2 ).isInProgress ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z
/* if-nez v2, :cond_7 */
/* .line 96 */
v2 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v2 ).closeLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
/* .line 99 */
} // :cond_7
/* monitor-exit v1 */
/* .line 100 */
/* .line 95 */
/* :catchall_0 */
/* move-exception v2 */
v3 = this.mDragState;
if ( v3 != null) { // if-eqz v3, :cond_8
v3 = (( com.android.server.wm.MiuiMirrorDragState ) v3 ).isInProgress ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z
/* if-nez v3, :cond_8 */
/* .line 96 */
v3 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v3 ).closeLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
/* .line 98 */
} // :cond_8
/* nop */
} // .end local v0 # "dragToken":Landroid/os/IBinder;
} // .end local p0 # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
} // .end local p1 # "callerPid":I
} // .end local p2 # "callerUid":I
} // .end local p3 # "window":Landroid/view/IWindow;
} // .end local p4 # "flags":I
} // .end local p5 # "displayId":I
} // .end local p6 # "data":Landroid/content/ClipData;
/* throw v2 */
/* .line 99 */
/* .restart local v0 # "dragToken":Landroid/os/IBinder; */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiMirrorDragDropController; */
/* .restart local p1 # "callerPid":I */
/* .restart local p2 # "callerUid":I */
/* .restart local p3 # "window":Landroid/view/IWindow; */
/* .restart local p4 # "flags":I */
/* .restart local p5 # "displayId":I */
/* .restart local p6 # "data":Landroid/content/ClipData; */
/* :catchall_1 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* throw v2 */
} // .end method
public void reportDropResult ( Integer p0, android.view.IWindow p1, Boolean p2 ) {
/* .locals 8 */
/* .param p1, "pid" # I */
/* .param p2, "window" # Landroid/view/IWindow; */
/* .param p3, "consumed" # Z */
/* .line 104 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p2, :cond_0 */
/* move-object v1, v0 */
} // :cond_0
/* .line 105 */
/* .local v1, "token":Landroid/os/IBinder; */
} // :goto_0
com.xiaomi.mirror.service.MirrorServiceInternal .getInstance ( );
v2 = (( com.xiaomi.mirror.service.MirrorServiceInternal ) v2 ).getDelegatePid ( ); // invoke-virtual {v2}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getDelegatePid()I
int v3 = 0; // const/4 v3, 0x0
/* if-ne p1, v2, :cond_1 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_1
/* move v2, v3 */
/* .line 107 */
/* .local v2, "fromDelegate":Z */
} // :goto_1
v4 = this.mService;
v4 = this.mGlobalLock;
/* monitor-enter v4 */
/* .line 108 */
try { // :try_start_0
v5 = this.mDragState;
/* if-nez v5, :cond_2 */
/* .line 111 */
final String v0 = "MiuiMirrorDragDrop"; // const-string v0, "MiuiMirrorDragDrop"
final String v3 = "Drop result given but no drag in progress"; // const-string v3, "Drop result given but no drag in progress"
android.util.Slog .w ( v0,v3 );
/* .line 112 */
/* monitor-exit v4 */
return;
/* .line 115 */
} // :cond_2
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 116 */
v5 = this.mToken;
/* if-ne v5, v1, :cond_6 */
/* .line 125 */
v5 = this.mHandler;
(( android.os.Handler ) v5 ).removeMessages ( v3, v6 ); // invoke-virtual {v5, v3, v6}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V
/* .line 126 */
v5 = this.mService;
(( com.android.server.wm.WindowManagerService ) v5 ).windowForClientLocked ( v0, p2, v3 ); // invoke-virtual {v5, v0, p2, v3}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/view/IWindow;Z)Lcom/android/server/wm/WindowState;
/* .line 127 */
/* .local v0, "callingWin":Lcom/android/server/wm/WindowState; */
if ( v0 != null) { // if-eqz v0, :cond_5
(( com.android.server.wm.WindowState ) v0 ).getTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
/* if-nez v3, :cond_3 */
/* .line 133 */
} // :cond_3
/* if-nez p3, :cond_4 */
/* .line 136 */
com.xiaomi.mirror.service.MirrorServiceInternal .getInstance ( );
/* .line 137 */
(( com.android.server.wm.WindowState ) v0 ).getOwningPackage ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* .line 138 */
(( com.android.server.wm.WindowState ) v0 ).getTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
/* iget v6, v6, Lcom/android/server/wm/Task;->mTaskId:I */
v7 = this.mDragState;
v7 = this.mData;
/* .line 137 */
v3 = (( com.xiaomi.mirror.service.MirrorServiceInternal ) v3 ).tryToShareDrag ( v5, v6, v7 ); // invoke-virtual {v3, v5, v6, v7}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->tryToShareDrag(Ljava/lang/String;ILandroid/content/ClipData;)Z
/* move p3, v3 */
/* .line 140 */
} // .end local v0 # "callingWin":Lcom/android/server/wm/WindowState;
} // :cond_4
/* .line 128 */
/* .restart local v0 # "callingWin":Lcom/android/server/wm/WindowState; */
} // :cond_5
} // :goto_2
final String v3 = "MiuiMirrorDragDrop"; // const-string v3, "MiuiMirrorDragDrop"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Bad result-reporting window "; // const-string v6, "Bad result-reporting window "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v5 );
/* .line 129 */
v3 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v3 ).closeLocked ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked(Z)V
/* .line 130 */
/* monitor-exit v4 */
return;
/* .line 118 */
} // .end local v0 # "callingWin":Lcom/android/server/wm/WindowState;
} // :cond_6
final String v0 = "MiuiMirrorDragDrop"; // const-string v0, "MiuiMirrorDragDrop"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Invalid drop-result claim by "; // const-string v5, "Invalid drop-result claim by "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v3 );
/* .line 119 */
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v3 = "reportDropResult() by non-recipient"; // const-string v3, "reportDropResult() by non-recipient"
/* invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
} // .end local v1 # "token":Landroid/os/IBinder;
} // .end local v2 # "fromDelegate":Z
} // .end local p0 # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
} // .end local p1 # "pid":I
} // .end local p2 # "window":Landroid/view/IWindow;
} // .end local p3 # "consumed":Z
/* throw v0 */
/* .line 141 */
/* .restart local v1 # "token":Landroid/os/IBinder; */
/* .restart local v2 # "fromDelegate":Z */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiMirrorDragDropController; */
/* .restart local p1 # "pid":I */
/* .restart local p2 # "window":Landroid/view/IWindow; */
/* .restart local p3 # "consumed":Z */
} // :cond_7
/* if-nez v2, :cond_8 */
/* .line 142 */
final String v0 = "MiuiMirrorDragDrop"; // const-string v0, "MiuiMirrorDragDrop"
final String v3 = "Try to report a result without a delegate"; // const-string v3, "Try to report a result without a delegate"
android.util.Slog .w ( v0,v3 );
/* .line 143 */
/* monitor-exit v4 */
return;
/* .line 147 */
} // :cond_8
} // :goto_3
v0 = this.mDragState;
/* iput-boolean p3, v0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z */
/* .line 148 */
v0 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v0 ).closeLocked ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked(Z)V
/* .line 149 */
/* monitor-exit v4 */
/* .line 150 */
return;
/* .line 149 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void sendDragStartedIfNeededLocked ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "window" # Ljava/lang/Object; */
/* .line 53 */
v0 = this.mDragState;
/* move-object v1, p1 */
/* check-cast v1, Lcom/android/server/wm/WindowState; */
(( com.android.server.wm.MiuiMirrorDragState ) v0 ).sendDragStartedIfNeededLocked ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMirrorDragState;->sendDragStartedIfNeededLocked(Lcom/android/server/wm/WindowState;)V
/* .line 54 */
return;
} // .end method
void sendHandlerMessage ( Integer p0, java.lang.Object p1 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg" # Ljava/lang/Object; */
/* .line 213 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 214 */
return;
} // .end method
void sendTimeoutMessage ( Integer p0, java.lang.Object p1 ) {
/* .locals 4 */
/* .param p1, "what" # I */
/* .param p2, "arg" # Ljava/lang/Object; */
/* .line 220 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).removeMessages ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V
/* .line 221 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 222 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/16 v2, 0x1388 */
(( android.os.Handler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 223 */
return;
} // .end method
public void shutdownDragAndDropIfNeeded ( ) {
/* .locals 3 */
/* .line 153 */
v0 = this.mService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 154 */
try { // :try_start_0
v1 = this.mDragState;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( com.android.server.wm.MiuiMirrorDragState ) v1 ).isInProgress ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 155 */
v1 = this.mDragState;
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z */
/* .line 156 */
v1 = this.mDragState;
(( com.android.server.wm.MiuiMirrorDragState ) v1 ).closeLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
/* .line 158 */
} // :cond_0
/* monitor-exit v0 */
/* .line 159 */
return;
/* .line 158 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
