.class public final Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;
.super Ljava/lang/Object;
.source "ActivityRecordImpl.java"

# interfaces
.implements Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/ActivityRecordImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MutableActivityRecordImpl"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;
    }
.end annotation


# instance fields
.field mActivityRecord:Lcom/android/server/wm/ActivityRecord;

.field private mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mForcePortraitActivity:Z

.field private mIsCirculatedToVirtual:Z

.field private mIsColdStart:Z

.field private mIsKeyguardEditor:Z

.field private mIsSecSplitAct:Z

.field private mIsSplitMode:Z

.field private mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

.field private mLetterboxUiController:Lcom/android/server/wm/LetterboxUiController;

.field private mNavigationBarColor:Ljava/lang/Integer;

.field private mNavigationBarColorChanged:Z

.field private mNotRelaunchCirculateApp:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public adaptCirculateConfigChanged(Lcom/android/server/wm/Task;II)I
    .locals 1
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "changes"    # I
    .param p3, "configChanged"    # I

    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsCirculatedToVirtual:Z

    .line 182
    or-int v0, p3, p2

    return v0
.end method

.method public appCirculate(ILcom/android/server/wm/Task;)V
    .locals 2
    .param p1, "lastReportedDisplayId"    # I
    .param p2, "task"    # Lcom/android/server/wm/Task;

    .line 187
    if-lez p1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsCirculatedToVirtual:Z

    if-eqz v0, :cond_0

    .line 188
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mNotRelaunchCirculateApp:Z

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsCirculatedToVirtual:Z

    .line 191
    :cond_0
    if-eqz p2, :cond_1

    .line 192
    iget-object v0, p2, Lcom/android/server/wm/Task;->mTaskStub:Lcom/android/server/wm/TaskStub$MutableTaskStub;

    iget-boolean v1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsCirculatedToVirtual:Z

    invoke-interface {v0, v1}, Lcom/android/server/wm/TaskStub$MutableTaskStub;->setIsCirculatedToVirtual(Z)V

    .line 193
    iget-object v0, p2, Lcom/android/server/wm/Task;->mTaskStub:Lcom/android/server/wm/TaskStub$MutableTaskStub;

    invoke-interface {v0, p1}, Lcom/android/server/wm/TaskStub$MutableTaskStub;->setLastDisplayId(I)V

    .line 195
    :cond_1
    return-void
.end method

.method public getAppOrientation(I)I
    .locals 1
    .param p1, "type"    # I

    .line 359
    packed-switch p1, :pswitch_data_0

    .line 369
    const/4 v0, -0x1

    return v0

    .line 367
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iget v0, v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mFlipScreenOrientationOuter:I

    return v0

    .line 365
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iget v0, v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationPad:I

    return v0

    .line 363
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iget v0, v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationOuter:I

    return v0

    .line 361
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iget v0, v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationInner:I

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getInitTaskAffinity(Landroid/content/pm/ActivityInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "info"    # Landroid/content/pm/ActivityInfo;
    .param p2, "callerName"    # Ljava/lang/String;

    .line 286
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->isSplitMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p1, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    iget-object v1, p1, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 288
    :cond_0
    iget-object v0, p1, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, p2

    .line 289
    .local v0, "taskAffinity":Ljava/lang/String;
    :goto_1
    iget v1, p1, Landroid/content/pm/ActivityInfo;->launchMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 290
    const/4 v1, 0x0

    iput v1, p1, Landroid/content/pm/ActivityInfo;->launchMode:I

    goto :goto_2

    .line 293
    .end local v0    # "taskAffinity":Ljava/lang/String;
    :cond_2
    iget-object v0, p1, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    .line 295
    .restart local v0    # "taskAffinity":Ljava/lang/String;
    :cond_3
    :goto_2
    return-object v0
.end method

.method public getIsCirculatedToVirtual()Z
    .locals 1

    .line 161
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsCirculatedToVirtual:Z

    return v0
.end method

.method public getIsColdStart()Z
    .locals 1

    .line 151
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsColdStart:Z

    return v0
.end method

.method public getNavigationBarColor()I
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mNavigationBarColor:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getNotRelaunchCirculateApp()Z
    .locals 1

    .line 166
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mNotRelaunchCirculateApp:Z

    return v0
.end method

.method public init(Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/LetterboxUiController;)V
    .locals 2
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "_service"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p4, "letterboxUiController"    # Lcom/android/server/wm/LetterboxUiController;

    .line 200
    iput-object p1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 201
    invoke-static {}, Lcom/android/server/wm/ActivityRecordStub;->get()Lcom/android/server/wm/ActivityRecordStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-interface {v0, v1, p2}, Lcom/android/server/wm/ActivityRecordStub;->initSplitMode(Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsSplitMode:Z

    .line 202
    invoke-static {}, Lcom/android/server/wm/ActivityRecordStub;->get()Lcom/android/server/wm/ActivityRecordStub;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsSplitMode:Z

    invoke-interface {v0, v1, p2}, Lcom/android/server/wm/ActivityRecordStub;->isSecSplit(ZLandroid/content/Intent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsSecSplitAct:Z

    .line 203
    iput-object p3, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 204
    invoke-virtual {p2}, Landroid/content/Intent;->getMiuiFlags()I

    move-result v0

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsKeyguardEditor:Z

    .line 205
    iput-object p4, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLetterboxUiController:Lcom/android/server/wm/LetterboxUiController;

    .line 206
    new-instance v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;-><init>(Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo-IA;)V

    iput-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    .line 207
    return-void
.end method

.method public isAboveSplitMode()Z
    .locals 2

    .line 267
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    iget-object v1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/Task;->getActivityAbove(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    iget-object v1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 268
    invoke-virtual {v0, v1}, Lcom/android/server/wm/Task;->getActivityAbove(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isSplitMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 267
    :goto_0
    return v0
.end method

.method public isDummySecSplitAct()Z
    .locals 2

    .line 261
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    iget-object v1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/Task;->getActivityBelow(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 262
    .local v0, "below":Lcom/android/server/wm/ActivityRecord;
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v1}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isSecSplitAct()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/android/server/wm/ActivityRecord;->finishing:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public isForcePortraitActivity()Z
    .locals 1

    .line 303
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mForcePortraitActivity:Z

    return v0
.end method

.method public isInitialColor()Z
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mNavigationBarColor:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isKeyguardEditor()Z
    .locals 1

    .line 215
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsKeyguardEditor:Z

    return v0
.end method

.method public isNeedUpdateAppOrientation(I)Z
    .locals 4
    .param p1, "type"    # I

    .line 375
    const/4 v0, 0x1

    const/4 v1, -0x2

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    .line 385
    return v2

    .line 383
    :pswitch_0
    iget-object v3, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iget v3, v3, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mFlipScreenOrientationOuter:I

    if-eq v3, v1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    return v0

    .line 381
    :pswitch_1
    iget-object v3, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iget v3, v3, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationPad:I

    if-eq v3, v1, :cond_1

    goto :goto_1

    :cond_1
    move v0, v2

    :goto_1
    return v0

    .line 379
    :pswitch_2
    iget-object v3, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iget v3, v3, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationOuter:I

    if-eq v3, v1, :cond_2

    goto :goto_2

    :cond_2
    move v0, v2

    :goto_2
    return v0

    .line 377
    :pswitch_3
    iget-object v3, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iget v3, v3, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationInner:I

    if-eq v3, v1, :cond_3

    goto :goto_3

    :cond_3
    move v0, v2

    :goto_3
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isSecSplitAct()Z
    .locals 1

    .line 220
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsSecSplitAct:Z

    return v0
.end method

.method public isSecondStateActivity()Z
    .locals 1

    .line 280
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->isSecSplitAct()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->isDummySecSplitAct()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isSplitBaseActivity()Z
    .locals 3

    .line 273
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mLastResumedActivity:Lcom/android/server/wm/ActivityRecord;

    .line 274
    .local v0, "ar":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v1}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isSplitMode()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v2, v1, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    .line 275
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 274
    :goto_0
    return v1
.end method

.method public isSplitMode()Z
    .locals 1

    .line 211
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsSplitMode:Z

    return v0
.end method

.method public navigationBarColorChanged()Z
    .locals 1

    .line 324
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mNavigationBarColorChanged:Z

    return v0
.end method

.method public needSplitAnimation()Z
    .locals 3

    .line 225
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->mTaskStub:Lcom/android/server/wm/TaskStub$MutableTaskStub;

    invoke-interface {v0}, Lcom/android/server/wm/TaskStub$MutableTaskStub;->isSplitMode()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 226
    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 228
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->isSplitMode()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/wm/ActivityRecordProxy;->mOccludesParent:Lcom/xiaomi/reflect/RefObject;

    iget-object v2, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0, v2}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1

    .line 227
    :cond_3
    :goto_0
    return v1
.end method

.method public setAppOrientation(II)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "orientation"    # I

    .line 339
    packed-switch p1, :pswitch_data_0

    .line 353
    return-void

    .line 350
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iput p2, v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mFlipScreenOrientationOuter:I

    .line 351
    return-void

    .line 347
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iput p2, v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationPad:I

    .line 348
    return-void

    .line 344
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iput p2, v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationOuter:I

    .line 345
    return-void

    .line 341
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLastAppOrientationInfo:Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;

    iput p2, v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationInner:I

    .line 342
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setForcePortraitActivity(Z)V
    .locals 0
    .param p1, "forcePortraitActivity"    # Z

    .line 299
    iput-boolean p1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mForcePortraitActivity:Z

    .line 300
    return-void
.end method

.method public setIsCirculatedToVirtual(Z)V
    .locals 0
    .param p1, "isCirculatedToVirtual"    # Z

    .line 171
    iput-boolean p1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsCirculatedToVirtual:Z

    .line 172
    return-void
.end method

.method public setIsColdStart(Z)V
    .locals 0
    .param p1, "isColdStart"    # Z

    .line 156
    iput-boolean p1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mIsColdStart:Z

    .line 157
    return-void
.end method

.method public setNavigationBarColor(I)V
    .locals 1
    .param p1, "color"    # I

    .line 318
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mNavigationBarColorChanged:Z

    .line 319
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mNavigationBarColor:Ljava/lang/Integer;

    .line 320
    return-void
.end method

.method public setNotRelaunchCirculateApp(Z)V
    .locals 0
    .param p1, "notRelaunchCirculateApp"    # Z

    .line 176
    iput-boolean p1, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mNotRelaunchCirculateApp:Z

    .line 177
    return-void
.end method

.method public shouldMakeVisible(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 4
    .param p1, "top"    # Lcom/android/server/wm/ActivityRecord;

    .line 246
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->isSplitMode()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-ne p1, v0, :cond_0

    goto :goto_2

    .line 249
    :cond_0
    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    .line 250
    return v2

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    iget-object v3, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0, v3}, Lcom/android/server/wm/Task;->getActivityAbove(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 253
    .local v0, "above":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_4

    iget-boolean v3, v0, Lcom/android/server/wm/ActivityRecord;->finishing:Z

    if-eqz v3, :cond_2

    goto :goto_1

    .line 256
    :cond_2
    iget-object v3, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v3}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isSplitMode()Z

    move-result v3

    if-nez v3, :cond_3

    sget-object v3, Lcom/android/server/wm/ActivityRecordProxy;->mOccludesParent:Lcom/xiaomi/reflect/RefObject;

    invoke-virtual {v3, v0}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    :cond_3
    move v1, v2

    :goto_0
    return v1

    .line 254
    :cond_4
    :goto_1
    return v1

    .line 247
    .end local v0    # "above":Lcom/android/server/wm/ActivityRecord;
    :cond_5
    :goto_2
    return v1
.end method

.method public shouldSplitBeInvisible(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 3
    .param p1, "top"    # Lcom/android/server/wm/ActivityRecord;

    .line 233
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->isSplitMode()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eq p1, v0, :cond_2

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    if-nez v0, :cond_0

    goto :goto_0

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->task:Lcom/android/server/wm/Task;

    iget-object v2, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0, v2}, Lcom/android/server/wm/Task;->getActivityAbove(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 236
    .local v0, "above":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v2}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isSplitMode()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lcom/android/server/wm/ActivityRecord;->finishing:Z

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 234
    .end local v0    # "above":Lcom/android/server/wm/ActivityRecord;
    :cond_2
    :goto_0
    return v1
.end method

.method public shouldSplitBevisible()Z
    .locals 2

    .line 241
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->isSplitMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;

    move-result-object v0

    sget-object v1, Lcom/android/server/wm/ActivityRecord$State;->STOPPED:Lcom/android/server/wm/ActivityRecord$State;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityRecord$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public updateLetterboxSurface(Lcom/android/server/wm/WindowState;)V
    .locals 1
    .param p1, "state"    # Lcom/android/server/wm/WindowState;

    .line 334
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mLetterboxUiController:Lcom/android/server/wm/LetterboxUiController;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/LetterboxUiController;->updateLetterboxSurface(Lcom/android/server/wm/WindowState;)V

    .line 335
    return-void
.end method

.method public updateNavigationBarColorFinish()V
    .locals 1

    .line 329
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;->mNavigationBarColorChanged:Z

    .line 330
    return-void
.end method
