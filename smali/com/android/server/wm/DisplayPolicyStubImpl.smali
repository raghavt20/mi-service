.class public Lcom/android/server/wm/DisplayPolicyStubImpl;
.super Ljava/lang/Object;
.source "DisplayPolicyStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/DisplayPolicyStub;


# static fields
.field private static final CARWITH_PACKAGE_NAME:Ljava/lang/String; = "com.miui.carlink"


# instance fields
.field private mInsetsFrameProvider:Landroid/view/InsetsFrameProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getDisplayName(Lcom/android/server/wm/DisplayContent;)Ljava/lang/String;
    .locals 1
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 98
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget-object v0, v0, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public createCarwithInsetsFrameProvider(Lcom/android/server/wm/DisplayContent;Landroid/view/WindowManager$LayoutParams;)Landroid/view/InsetsFrameProvider;
    .locals 5
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 86
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    .line 87
    .local v0, "mInsetsSourceOwner":Landroid/os/Binder;
    new-instance v1, Landroid/view/InsetsFrameProvider;

    invoke-static {}, Landroid/view/WindowInsets$Type;->navigationBars()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v1, v0, v3, v2}, Landroid/view/InsetsFrameProvider;-><init>(Ljava/lang/Object;II)V

    iput-object v1, p0, Lcom/android/server/wm/DisplayPolicyStubImpl;->mInsetsFrameProvider:Landroid/view/InsetsFrameProvider;

    .line 88
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v1

    .line 89
    .local v1, "di":Landroid/view/DisplayInfo;
    iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    iget v4, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    if-le v2, v4, :cond_0

    .line 90
    iget-object v2, p0, Lcom/android/server/wm/DisplayPolicyStubImpl;->mInsetsFrameProvider:Landroid/view/InsetsFrameProvider;

    iget v4, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    invoke-static {v4, v3, v3, v3}, Landroid/graphics/Insets;->of(IIII)Landroid/graphics/Insets;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/InsetsFrameProvider;->setInsetsSize(Landroid/graphics/Insets;)Landroid/view/InsetsFrameProvider;

    goto :goto_0

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/DisplayPolicyStubImpl;->mInsetsFrameProvider:Landroid/view/InsetsFrameProvider;

    iget v4, p2, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-static {v3, v3, v3, v4}, Landroid/graphics/Insets;->of(IIII)Landroid/graphics/Insets;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/InsetsFrameProvider;->setInsetsSize(Landroid/graphics/Insets;)Landroid/view/InsetsFrameProvider;

    .line 94
    :goto_0
    iget-object v2, p0, Lcom/android/server/wm/DisplayPolicyStubImpl;->mInsetsFrameProvider:Landroid/view/InsetsFrameProvider;

    return-object v2
.end method

.method public getExtraNavigationBarAppearance(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;)I
    .locals 3
    .param p1, "winCandidate"    # Lcom/android/server/wm/WindowState;
    .param p2, "navColorWin"    # Lcom/android/server/wm/WindowState;

    .line 40
    const/4 v0, 0x0

    .line 41
    .local v0, "appearance":I
    if-eqz p2, :cond_0

    .line 42
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    const/high16 v2, 0x100000

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_0

    .line 44
    or-int/lit8 v0, v0, 0x10

    .line 47
    :cond_0
    if-eqz p1, :cond_1

    .line 48
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    const v2, 0x8000

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_1

    .line 50
    or-int/lit16 v0, v0, 0x800

    .line 52
    :cond_1
    return v0
.end method

.method public isCarWithDisplay(Lcom/android/server/wm/DisplayContent;)Z
    .locals 2
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 68
    const-string v0, "com.miui.carlink"

    invoke-direct {p0, p1}, Lcom/android/server/wm/DisplayPolicyStubImpl;->getDisplayName(Lcom/android/server/wm/DisplayContent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMiuiVersion()Z
    .locals 1

    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public notifyOnScroll(Z)V
    .locals 0
    .param p1, "start"    # Z

    .line 62
    invoke-static {p1}, Lcom/miui/whetstone/client/WhetstoneClientManager;->notifyOnScroll(Z)V

    .line 63
    return-void
.end method

.method public setCarWithWindowContainer(Lcom/android/server/wm/DisplayContent;Lcom/android/server/wm/WindowState;Lcom/android/server/wm/DisplayPolicyStub$InsertActionInterface;)V
    .locals 1
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "win"    # Lcom/android/server/wm/WindowState;
    .param p3, "insertActionInterface"    # Lcom/android/server/wm/DisplayPolicyStub$InsertActionInterface;

    .line 74
    iget-object v0, p0, Lcom/android/server/wm/DisplayPolicyStubImpl;->mInsetsFrameProvider:Landroid/view/InsetsFrameProvider;

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p2, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/DisplayPolicyStubImpl;->createCarwithInsetsFrameProvider(Lcom/android/server/wm/DisplayContent;Landroid/view/WindowManager$LayoutParams;)Landroid/view/InsetsFrameProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/DisplayPolicyStubImpl;->mInsetsFrameProvider:Landroid/view/InsetsFrameProvider;

    .line 78
    :cond_0
    if-eqz p3, :cond_1

    .line 79
    iget-object v0, p0, Lcom/android/server/wm/DisplayPolicyStubImpl;->mInsetsFrameProvider:Landroid/view/InsetsFrameProvider;

    invoke-virtual {v0}, Landroid/view/InsetsFrameProvider;->getId()I

    move-result v0

    invoke-interface {p3, v0}, Lcom/android/server/wm/DisplayPolicyStub$InsertActionInterface;->insertAction(I)V

    .line 81
    :cond_1
    return-void
.end method
