public class com.android.server.wm.MultiSplitBlockListController implements com.android.server.wm.IController {
	 /* .source "MultiSplitBlockListController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MultiSplitBlockListController$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final com.android.server.wm.ActivityTaskManagerService mAtm;
private final java.util.ArrayList mBlocklistPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Set mDeferredBlocklistPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.wm.WindowManagerGlobalLock mGlobalLock;
private com.android.server.wm.MultiSplitBlockListController$H mH;
private final java.lang.Object mLock;
final java.util.function.Consumer mMultiSplitBlocklistChangedCallback;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$3aE-aTTybfIUeoQB3eAdaRvh1PM ( com.android.server.wm.MultiSplitBlockListController p0, java.util.concurrent.ConcurrentHashMap p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MultiSplitBlockListController;->lambda$new$0(Ljava/util/concurrent/ConcurrentHashMap;)V */
return;
} // .end method
static com.android.server.wm.WindowManagerGlobalLock -$$Nest$fgetmGlobalLock ( com.android.server.wm.MultiSplitBlockListController p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mGlobalLock;
} // .end method
static void -$$Nest$mupdateDeferredBlockListLocked ( com.android.server.wm.MultiSplitBlockListController p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MultiSplitBlockListController;->updateDeferredBlockListLocked()V */
return;
} // .end method
public com.android.server.wm.MultiSplitBlockListController ( ) {
/* .locals 1 */
/* .param p1, "atm" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 46 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 23 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mBlocklistPackages = v0;
/* .line 24 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mDeferredBlocklistPackages = v0;
/* .line 27 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
/* .line 29 */
/* new-instance v0, Lcom/android/server/wm/MultiSplitBlockListController$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MultiSplitBlockListController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MultiSplitBlockListController;)V */
this.mMultiSplitBlocklistChangedCallback = v0;
/* .line 47 */
this.mAtm = p1;
/* .line 48 */
v0 = this.mGlobalLock;
this.mGlobalLock = v0;
/* .line 49 */
(( com.android.server.wm.MultiSplitBlockListController ) p0 ).initialize ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MultiSplitBlockListController;->initialize()V
/* .line 50 */
return;
} // .end method
private java.util.ArrayList getFinalBlockListLocked ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 53 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 54 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
v2 = this.mBlocklistPackages;
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 55 */
/* .local v1, "finalBlocklistPackages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v2 = this.mDeferredBlocklistPackages;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/lang/String; */
/* .line 56 */
/* .local v3, "pkgName":Ljava/lang/String; */
v4 = (( java.util.ArrayList ) v1 ).contains ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 57 */
(( java.util.ArrayList ) v1 ).remove ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 58 */
/* .line 60 */
} // :cond_0
(( java.util.ArrayList ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 61 */
/* nop */
} // .end local v3 # "pkgName":Ljava/lang/String;
/* .line 62 */
} // :cond_1
/* monitor-exit v0 */
/* .line 63 */
} // .end local v1 # "finalBlocklistPackages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$new$0 ( java.util.concurrent.ConcurrentHashMap p0 ) { //synthethic
/* .locals 6 */
/* .param p1, "map" # Ljava/util/concurrent/ConcurrentHashMap; */
/* .line 30 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 31 */
try { // :try_start_0
v1 = this.mBlocklistPackages;
(( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
/* .line 32 */
(( java.util.concurrent.ConcurrentHashMap ) p1 ).entrySet ( ); // invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;
/* .line 33 */
/* .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 34 */
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 35 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
/* check-cast v3, Ljava/lang/String; */
/* .line 36 */
/* .local v3, "key":Ljava/lang/String; */
/* check-cast v4, Ljava/lang/String; */
/* .line 37 */
/* .local v4, "value":Ljava/lang/String; */
final String v5 = "b"; // const-string v5, "b"
v5 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 38 */
v5 = this.mBlocklistPackages;
(( java.util.ArrayList ) v5 ).add ( v3 ); // invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 40 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v3 # "key":Ljava/lang/String;
} // .end local v4 # "value":Ljava/lang/String;
} // :cond_0
/* .line 41 */
} // :cond_1
v2 = this.mH;
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.wm.MultiSplitBlockListController$H ) v2 ).removeMessages ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/MultiSplitBlockListController$H;->removeMessages(I)V
/* .line 42 */
v2 = this.mH;
(( com.android.server.wm.MultiSplitBlockListController$H ) v2 ).sendEmptyMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/MultiSplitBlockListController$H;->sendEmptyMessage(I)Z
/* .line 43 */
/* nop */
} // .end local v1 # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
/* monitor-exit v0 */
/* .line 44 */
return;
/* .line 43 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void updateDeferredBlockListLocked ( ) {
/* .locals 4 */
/* .line 67 */
v0 = this.mDeferredBlocklistPackages;
/* .line 68 */
v0 = this.mAtm;
v0 = this.mTaskSupervisor;
v0 = this.mRecentTasks;
(( com.android.server.wm.RecentTasks ) v0 ).getRawTasks ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/wm/Task; */
/* .line 72 */
/* .local v1, "task":Lcom/android/server/wm/Task; */
v2 = this.realActivity;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mDeferredBlocklistPackages;
v3 = this.realActivity;
/* .line 73 */
v2 = (( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* if-nez v2, :cond_0 */
/* .line 74 */
v2 = this.mDeferredBlocklistPackages;
v3 = this.realActivity;
(( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 76 */
} // .end local v1 # "task":Lcom/android/server/wm/Task;
} // :cond_0
/* .line 77 */
} // :cond_1
v0 = v0 = this.mDeferredBlocklistPackages;
/* if-nez v0, :cond_2 */
/* .line 78 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateDeferredBlockListLocked: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDeferredBlocklistPackages;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MultiSplitBlockListController"; // const-string v1, "MultiSplitBlockListController"
android.util.Slog .d ( v1,v0 );
/* .line 80 */
} // :cond_2
return;
} // .end method
/* # virtual methods */
public void dumpLocked ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 84 */
final String v0 = "[MultiSplitBlockListController]"; // const-string v0, "[MultiSplitBlockListController]"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 85 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 86 */
try { // :try_start_0
v1 = this.mBlocklistPackages;
v1 = (( java.util.ArrayList ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z
/* if-nez v1, :cond_2 */
/* .line 87 */
final String v1 = "(mBlocklistPackages)"; // const-string v1, "(mBlocklistPackages)"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 88 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 89 */
int v1 = 0; // const/4 v1, 0x0
/* .line 90 */
/* .local v1, "numPrint":I */
v2 = this.mBlocklistPackages;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
/* .line 91 */
/* .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 92 */
/* check-cast v3, Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).print ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 93 */
/* add-int/lit8 v1, v1, 0x1 */
/* rem-int/lit8 v3, v1, 0x5 */
/* if-nez v3, :cond_0 */
/* .line 94 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 95 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 97 */
} // :cond_0
final String v3 = " "; // const-string v3, " "
(( java.io.PrintWriter ) p1 ).print ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 99 */
} // :cond_1
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 101 */
} // .end local v1 # "numPrint":I
} // .end local v2 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // :cond_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 103 */
v0 = v0 = this.mDeferredBlocklistPackages;
/* if-nez v0, :cond_5 */
/* .line 104 */
final String v0 = "(mDeferredBlocklistPackages)"; // const-string v0, "(mDeferredBlocklistPackages)"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 105 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 106 */
int v0 = 0; // const/4 v0, 0x0
/* .line 107 */
/* .local v0, "numPrint":I */
v1 = this.mDeferredBlocklistPackages;
/* .line 108 */
/* .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 109 */
/* check-cast v2, Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 110 */
/* add-int/lit8 v0, v0, 0x1 */
/* rem-int/lit8 v2, v0, 0x5 */
/* if-nez v2, :cond_3 */
/* .line 111 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 112 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 114 */
} // :cond_3
final String v2 = " "; // const-string v2, " "
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 116 */
} // :cond_4
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 118 */
} // .end local v0 # "numPrint":I
} // .end local v1 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // :cond_5
return;
/* .line 101 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
java.util.ArrayList getBlocklistAppList ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 121 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 122 */
try { // :try_start_0
com.android.server.wm.WindowManagerService .boostPriorityForLockedSection ( );
/* .line 123 */
/* invoke-direct {p0}, Lcom/android/server/wm/MultiSplitBlockListController;->getFinalBlockListLocked()Ljava/util/ArrayList; */
/* .line 124 */
/* .local v1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
com.android.server.wm.WindowManagerService .resetPriorityAfterLockedSection ( );
/* .line 125 */
/* monitor-exit v0 */
/* .line 126 */
} // .end local v1 # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
java.util.function.Consumer getMultiSplitBlocklistChangedCallbackLocked ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 137 */
v0 = this.mMultiSplitBlocklistChangedCallback;
} // .end method
java.util.ArrayList getOriBlocklistAppList ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 130 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 131 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
v2 = this.mBlocklistPackages;
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 132 */
/* .local v1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* monitor-exit v0 */
/* .line 133 */
} // .end local v1 # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void initialize ( ) {
/* .locals 2 */
/* .line 141 */
/* new-instance v0, Lcom/android/server/wm/MultiSplitBlockListController$H; */
com.android.server.DisplayThread .get ( );
(( com.android.server.DisplayThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/DisplayThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MultiSplitBlockListController$H;-><init>(Lcom/android/server/wm/MultiSplitBlockListController;Landroid/os/Looper;)V */
this.mH = v0;
/* .line 142 */
return;
} // .end method
Boolean isBlocklistApp ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 145 */
/* if-nez p1, :cond_0 */
/* .line 146 */
int v0 = 0; // const/4 v0, 0x0
/* .line 148 */
} // :cond_0
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 149 */
try { // :try_start_0
com.android.server.wm.WindowManagerService .boostPriorityForLockedSection ( );
/* .line 150 */
/* invoke-direct {p0}, Lcom/android/server/wm/MultiSplitBlockListController;->getFinalBlockListLocked()Ljava/util/ArrayList; */
v1 = (( java.util.ArrayList ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* .line 151 */
/* .local v1, "isBlockApp":Z */
com.android.server.wm.WindowManagerService .resetPriorityAfterLockedSection ( );
/* .line 152 */
/* monitor-exit v0 */
/* .line 153 */
} // .end local v1 # "isBlockApp":Z
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Boolean isMultiSplitBlocklistPackageLocked ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 157 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 158 */
try { // :try_start_0
v1 = this.mBlocklistPackages;
v1 = (( java.util.ArrayList ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 159 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void removeFromDeferredBlocklistIfNeeedLocked ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 163 */
v0 = this.realActivity;
/* if-nez v0, :cond_0 */
/* .line 164 */
return;
/* .line 167 */
} // :cond_0
v0 = this.realActivity;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 168 */
/* .local v0, "pkgName":Ljava/lang/String; */
v1 = this.mDeferredBlocklistPackages;
/* .line 171 */
return;
} // .end method
public void setWindowManager ( com.android.server.wm.WindowManagerService p0 ) {
/* .locals 0 */
/* .param p1, "wm" # Lcom/android/server/wm/WindowManagerService; */
/* .line 175 */
return;
} // .end method
