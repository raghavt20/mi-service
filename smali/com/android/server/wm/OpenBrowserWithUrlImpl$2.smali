.class Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;
.super Ljava/lang/Object;
.source "OpenBrowserWithUrlImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/OpenBrowserWithUrlImpl;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/OpenBrowserWithUrlImpl;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$handler:Landroid/os/Handler;

.field final synthetic val$kamiCloudOberver:Landroid/database/ContentObserver;


# direct methods
.method constructor <init>(Lcom/android/server/wm/OpenBrowserWithUrlImpl;Landroid/content/Context;Landroid/database/ContentObserver;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/OpenBrowserWithUrlImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 68
    iput-object p1, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;->this$0:Lcom/android/server/wm/OpenBrowserWithUrlImpl;

    iput-object p2, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;->val$kamiCloudOberver:Landroid/database/ContentObserver;

    iput-object p4, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 71
    iget-object v0, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 72
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;->val$kamiCloudOberver:Landroid/database/ContentObserver;

    .line 71
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 74
    iget-object v0, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;->val$handler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2$1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2$1;-><init>(Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 80
    return-void
.end method
