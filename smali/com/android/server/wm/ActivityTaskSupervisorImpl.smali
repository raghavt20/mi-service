.class public Lcom/android/server/wm/ActivityTaskSupervisorImpl;
.super Lcom/android/server/wm/ActivityTaskSupervisorStub;
.source "ActivityTaskSupervisorImpl.java"


# static fields
.field private static final ACTIVITY_RESUME_TIMEOUT:I = 0x1388

.field public static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "android.intent.extra.PACKAGE_NAME"

.field private static final INCALL_PACKAGE_NAME:Ljava/lang/String; = "com.android.incallui"

.field private static final INCALL_UI_NAME:Ljava/lang/String; = "com.android.incallui.InCallActivity"

.field private static final MAX_SWITCH_INTERVAL:I = 0x3e8

.field public static final MIUI_APP_LOCK_ACTION:Ljava/lang/String; = "miui.intent.action.CHECK_ACCESS_CONTROL"

.field public static final MIUI_APP_LOCK_ACTIVITY_NAME:Ljava/lang/String; = "com.miui.applicationlock.ConfirmAccessControl"

.field public static final MIUI_APP_LOCK_PACKAGE_NAME:Ljava/lang/String; = "com.miui.securitycenter"

.field public static final MIUI_APP_LOCK_REQUEST_CODE:I = -0x3e9

.field private static final TAG:Ljava/lang/String; = "ActivityTaskSupervisor"

.field private static mLastIncallUiLaunchTime:J

.field private static sActivityRequestId:I

.field static final sSupportsMultiTaskInDockList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mContext:Landroid/content/Context;

.field private volatile mFromKeyguard:Z

.field private volatile mTimestamp:J


# direct methods
.method public static synthetic $r8$lambda$zZ0PRlQIOg8CB3-j4YEjE0qsDWw(Lcom/android/server/wm/ActivityTaskSupervisorImpl;Landroid/content/pm/ActivityInfo;JZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->lambda$activityIdle$0(Landroid/content/pm/ActivityInfo;JZ)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->sSupportsMultiTaskInDockList:Ljava/util/ArrayList;

    .line 168
    const-string v1, "com.miui.hybrid"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mLastIncallUiLaunchTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 52
    invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskSupervisorStub;-><init>()V

    .line 68
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mFromKeyguard:Z

    return-void
.end method

.method public static exitfreeformIfNeeded(Lcom/android/server/wm/Task;IILcom/android/server/wm/ActivityTaskSupervisor;)Lcom/android/server/wm/Task;
    .locals 5
    .param p0, "task"    # Lcom/android/server/wm/Task;
    .param p1, "taskId"    # I
    .param p2, "windowMode"    # I
    .param p3, "supervisor"    # Lcom/android/server/wm/ActivityTaskSupervisor;

    .line 243
    move-object v0, p0

    .line 244
    .local v0, "tTask":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_0

    .line 245
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    if-eq p2, v2, :cond_0

    .line 246
    const/4 v1, 0x0

    .line 247
    .local v1, "op":Landroid/app/ActivityOptions;
    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object v1

    .line 248
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V

    .line 249
    iget-object v3, p3, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    const/4 v4, 0x2

    invoke-virtual {v3, p1, v4, v1, v2}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(IILandroid/app/ActivityOptions;Z)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 254
    .end local v1    # "op":Landroid/app/ActivityOptions;
    :cond_0
    return-object v0
.end method

.method private static getNextRequestIdLocked()I
    .locals 2

    .line 221
    sget v0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->sActivityRequestId:I

    const v1, 0x7fffffff

    if-lt v0, v1, :cond_0

    .line 222
    const/4 v0, 0x0

    sput v0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->sActivityRequestId:I

    .line 224
    :cond_0
    sget v0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->sActivityRequestId:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->sActivityRequestId:I

    .line 225
    return v0
.end method

.method static isAllowedAppSwitch(Lcom/android/server/wm/Task;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)Z
    .locals 7
    .param p0, "stack"    # Lcom/android/server/wm/Task;
    .param p1, "callingPackageName"    # Ljava/lang/String;
    .param p2, "aInfo"    # Landroid/content/pm/ActivityInfo;

    .line 191
    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 192
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/server/wm/Task;->topRunningNonDelayedActivityLocked(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 193
    .local v1, "topr":Lcom/android/server/wm/ActivityRecord;
    const-string v2, "com.android.incallui.InCallActivity"

    if-eqz v1, :cond_1

    iget-object v3, v1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    if-eqz v3, :cond_1

    iget-object v3, v1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 194
    const-string v3, "com.android.incallui"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz p2, :cond_1

    iget-object v3, p2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 195
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-wide v3, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mLastIncallUiLaunchTime:J

    const-wide/16 v5, 0x3e8

    add-long/2addr v3, v5

    .line 196
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    .line 197
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app switch:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stopped for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms.Try later."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ActivityManager"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    return v0

    .line 201
    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mLastIncallUiLaunchTime:J

    .line 204
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method static isAllowedAppSwitch(Lcom/android/server/wm/Task;Ljava/lang/String;Landroid/content/pm/ActivityInfo;J)Z
    .locals 1
    .param p0, "stack"    # Lcom/android/server/wm/Task;
    .param p1, "callingPackageName"    # Ljava/lang/String;
    .param p2, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p3, "lastTime"    # J

    .line 186
    invoke-static {p0, p1, p2}, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->isAllowedAppSwitch(Lcom/android/server/wm/Task;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)Z

    move-result v0

    return v0
.end method

.method private synthetic lambda$activityIdle$0(Landroid/content/pm/ActivityInfo;JZ)V
    .locals 0
    .param p1, "info"    # Landroid/content/pm/ActivityInfo;
    .param p2, "durationMillis"    # J
    .param p4, "fromKeyguard"    # Z

    .line 137
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->trackActivityResumeTimeout(Landroid/content/pm/ActivityInfo;JZ)V

    return-void
.end method

.method public static notPauseAtFreeformMode(Lcom/android/server/wm/Task;Lcom/android/server/wm/Task;)Z
    .locals 4
    .param p0, "focusStack"    # Lcom/android/server/wm/Task;
    .param p1, "curStack"    # Lcom/android/server/wm/Task;

    .line 229
    invoke-static {}, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->supportsFreeform()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 230
    return v1

    .line 232
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v0

    const/4 v2, 0x5

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 233
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v0

    if-ne v0, v2, :cond_3

    :cond_2
    move v1, v3

    .line 232
    :cond_3
    return v1
.end method

.method private static resolveIntent(Landroid/content/Intent;Ljava/lang/String;I)Landroid/content/pm/ResolveInfo;
    .locals 6
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "resolvedType"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 213
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    const-wide/32 v3, 0x10400

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    invoke-interface/range {v0 .. v5}, Landroid/content/pm/IPackageManager;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 215
    :catch_0
    move-exception v0

    .line 217
    const/4 v0, 0x0

    return-object v0
.end method

.method public static supportsFreeform()Z
    .locals 3

    .line 237
    nop

    .line 238
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "persist.sys.miui_optimization"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    invoke-static {}, Landroid/util/MiuiMultiWindowUtils;->isForceResizeable()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 237
    :goto_0
    return v1
.end method

.method public static supportsMultiTaskInDock(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 208
    sget-object v0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->sSupportsMultiTaskInDockList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private trackActivityResumeTimeout(Landroid/content/pm/ActivityInfo;JZ)V
    .locals 4
    .param p1, "info"    # Landroid/content/pm/ActivityInfo;
    .param p2, "durationMillis"    # J
    .param p4, "fromKeyguard"    # Z

    .line 143
    :try_start_0
    new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V

    .line 144
    .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    const/16 v1, 0x1b1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V

    .line 145
    iget-object v1, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V

    .line 146
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V

    .line 147
    const-string v1, "activity resume timeout"

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "activity="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " duration="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms fromKeyguard="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V

    .line 149
    long-to-int v1, p2

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V

    .line 150
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z

    .line 152
    new-instance v1, Lcom/miui/misight/MiEvent;

    const v2, 0x35b436fc

    invoke-direct {v1, v2}, Lcom/miui/misight/MiEvent;-><init>(I)V

    .line 153
    .local v1, "miEvent":Lcom/miui/misight/MiEvent;
    const-string v2, "PackageName"

    iget-object v3, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    .line 154
    const-string v2, "ActivityName"

    iget-object v3, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    .line 155
    const-string v2, "Duration"

    long-to-int v3, p2

    invoke-virtual {v1, v2, v3}, Lcom/miui/misight/MiEvent;->addInt(Ljava/lang/String;I)Lcom/miui/misight/MiEvent;

    .line 156
    const-string v2, "FromKeyguard"

    invoke-virtual {v1, v2, p4}, Lcom/miui/misight/MiEvent;->addBool(Ljava/lang/String;Z)Lcom/miui/misight/MiEvent;

    .line 157
    invoke-static {v1}, Lcom/miui/misight/MiSight;->sendEvent(Lcom/miui/misight/MiEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    .end local v0    # "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    .end local v1    # "miEvent":Lcom/miui/misight/MiEvent;
    goto :goto_0

    .line 158
    :catch_0
    move-exception v0

    .line 161
    :goto_0
    return-void
.end method

.method public static updateApplicationConfiguration(Lcom/android/server/wm/ActivityTaskSupervisor;Landroid/content/res/Configuration;Ljava/lang/String;)V
    .locals 6
    .param p0, "stackSupervisor"    # Lcom/android/server/wm/ActivityTaskSupervisor;
    .param p1, "globalConfiguration"    # Landroid/content/res/Configuration;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 261
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskSupervisor;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 262
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 263
    .local v1, "topStack":Lcom/android/server/wm/Task;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 264
    if-eqz v1, :cond_1

    .line 266
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskSupervisor;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 267
    :try_start_1
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 268
    .local v2, "topActivity":Lcom/android/server/wm/ActivityRecord;
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_1

    iget-object v0, v2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 272
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 274
    .local v0, "rect":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    if-le v4, v5, :cond_0

    .line 275
    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x2

    :goto_0
    iput v4, p1, Landroid/content/res/Configuration;->orientation:I

    .line 276
    iget-object v4, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v4, v3}, Landroid/app/WindowConfiguration;->setWindowingMode(I)V

    .line 278
    iget-object v3, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    .line 279
    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    .line 278
    invoke-virtual {v3, v4}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 268
    .end local v0    # "rect":Landroid/graphics/Rect;
    .end local v2    # "topActivity":Lcom/android/server/wm/ActivityRecord;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 282
    :cond_1
    :goto_1
    return-void

    .line 263
    .end local v1    # "topStack":Lcom/android/server/wm/Task;
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method static updateInfoBeforeRealStartActivity(Lcom/android/server/wm/Task;Landroid/app/IApplicationThread;ILjava/lang/String;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Landroid/os/IBinder;II)V
    .locals 1
    .param p0, "stack"    # Lcom/android/server/wm/Task;
    .param p1, "caller"    # Landroid/app/IApplicationThread;
    .param p2, "callingUid"    # I
    .param p3, "callingPackage"    # Ljava/lang/String;
    .param p4, "intent"    # Landroid/content/Intent;
    .param p5, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p6, "resultTo"    # Landroid/os/IBinder;
    .param p7, "requestCode"    # I
    .param p8, "userId"    # I

    .line 174
    invoke-static {}, Lcom/android/server/wm/MiuiMultiTaskManagerStub;->get()Lcom/android/server/wm/MiuiMultiTaskManagerStub;

    move-result-object v0

    invoke-interface {v0, p0, p5, p4}, Lcom/android/server/wm/MiuiMultiTaskManagerStub;->updateMultiTaskInfoIfNeed(Lcom/android/server/wm/Task;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;)V

    .line 175
    return-void
.end method


# virtual methods
.method acquireLaunchWakelock()V
    .locals 0

    .line 115
    return-void
.end method

.method activityIdle(Landroid/content/pm/ActivityInfo;)V
    .locals 13
    .param p1, "info"    # Landroid/content/pm/ActivityInfo;

    .line 130
    iget-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    return-void

    .line 131
    :cond_0
    iget-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J

    .line 132
    .local v0, "timestamp":J
    iget-boolean v10, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mFromKeyguard:Z

    .line 133
    .local v10, "fromKeyguard":Z
    iput-wide v2, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J

    .line 134
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mFromKeyguard:Z

    .line 135
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    .line 136
    .local v2, "durationMillis":J
    const-wide/16 v4, 0x1388

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 137
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v11

    new-instance v12, Lcom/android/server/wm/ActivityTaskSupervisorImpl$$ExternalSyntheticLambda0;

    move-object v4, v12

    move-object v5, p0

    move-object v6, p1

    move-wide v7, v2

    move v9, v10

    invoke-direct/range {v4 .. v9}, Lcom/android/server/wm/ActivityTaskSupervisorImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityTaskSupervisorImpl;Landroid/content/pm/ActivityInfo;JZ)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 139
    :cond_1
    return-void
.end method

.method addSender(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/content/Intent;II)V
    .locals 3
    .param p1, "service"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "callingPid"    # I
    .param p4, "filterCallingUid"    # I

    .line 285
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 286
    :try_start_0
    invoke-virtual {p1, p3, p4}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(II)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 287
    .local v1, "wpc":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_0

    .line 288
    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p2, v2}, Landroid/content/Intent;->setSender(Ljava/lang/String;)V

    goto :goto_0

    .line 290
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Landroid/content/Intent;->setSender(Ljava/lang/String;)V

    .line 292
    .end local v1    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :goto_0
    monitor-exit v0

    .line 293
    return-void

    .line 292
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method init(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/os/Looper;)V
    .locals 1
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 76
    iput-object p1, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 77
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mContext:Landroid/content/Context;

    .line 78
    return-void
.end method

.method isAppLockActivity(Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;I)I
    .locals 2
    .param p1, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p4, "requestCode"    # I

    .line 108
    nop

    .line 103
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-ne p4, v0, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 106
    invoke-virtual {p2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "miui.intent.action.CHECK_ACCESS_CONTROL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 108
    const-string v1, "com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const/16 v0, -0x3e9

    goto :goto_0

    :cond_0
    move v0, p4

    .line 103
    :goto_0
    return v0
.end method

.method keyguardGoingAway(I)V
    .locals 2
    .param p1, "flags"    # I

    .line 124
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mFromKeyguard:Z

    .line 126
    return-void
.end method

.method startActivityFromRecentsFlag(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .line 82
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "targetPkg":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 84
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 85
    .local v1, "component":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 86
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 89
    .end local v1    # "component":Landroid/content/ComponentName;
    :cond_0
    if-nez v0, :cond_1

    .line 90
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 91
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 92
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v2, :cond_1

    .line 93
    iget-object v0, v2, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    .line 96
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    .end local v2    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 97
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/am/PendingIntentRecordImpl;->exemptTemporarily(Ljava/lang/String;Z)V

    .line 99
    :cond_2
    return-void
.end method

.method startPausingResumedActivity()V
    .locals 2

    .line 119
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J

    .line 120
    return-void
.end method
