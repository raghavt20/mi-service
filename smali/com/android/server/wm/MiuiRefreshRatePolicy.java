public class com.android.server.wm.MiuiRefreshRatePolicy implements com.android.server.wm.RefreshRatePolicyStub {
	 /* .source "MiuiRefreshRatePolicy.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate; */
	 /* } */
} // .end annotation
/* # instance fields */
private final com.android.server.wm.MiuiRefreshRatePolicy$PackageRefreshRate mRefreshRatePackages;
private com.android.server.wm.WindowManagerInternal mWindowManagerInternal;
/* # direct methods */
public com.android.server.wm.MiuiRefreshRatePolicy ( ) {
	 /* .locals 1 */
	 /* .line 11 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 14 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;-><init>(Lcom/android/server/wm/MiuiRefreshRatePolicy;)V */
	 this.mRefreshRatePackages = v0;
	 return;
} // .end method
public static com.android.server.wm.MiuiRefreshRatePolicy getInstance ( ) {
	 /* .locals 1 */
	 /* .line 17 */
	 /* const-class v0, Lcom/android/server/wm/RefreshRatePolicyStub; */
	 com.miui.base.MiuiStubUtil .getImpl ( v0 );
	 /* check-cast v0, Lcom/android/server/wm/MiuiRefreshRatePolicy; */
} // .end method
/* # virtual methods */
public void addSceneRefreshRateForPackage ( java.lang.String p0, Float p1 ) {
	 /* .locals 2 */
	 /* .param p1, "packageName" # Ljava/lang/String; */
	 /* .param p2, "preferredMaxRefreshRate" # F */
	 /* .line 52 */
	 v0 = this.mRefreshRatePackages;
	 /* monitor-enter v0 */
	 /* .line 53 */
	 try { // :try_start_0
		 v1 = this.mRefreshRatePackages;
		 (( com.android.server.wm.MiuiRefreshRatePolicy$PackageRefreshRate ) v1 ).add ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;->add(Ljava/lang/String;F)V
		 /* .line 54 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 55 */
		 v0 = this.mWindowManagerInternal;
		 (( com.android.server.wm.WindowManagerInternal ) v0 ).requestTraversalFromDisplayManager ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerInternal;->requestTraversalFromDisplayManager()V
		 /* .line 56 */
		 return;
		 /* .line 54 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 try { // :try_start_1
			 /* monitor-exit v0 */
			 /* :try_end_1 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* throw v1 */
		 } // .end method
		 public void dump ( java.io.PrintWriter p0 ) {
			 /* .locals 2 */
			 /* .param p1, "pw" # Ljava/io/PrintWriter; */
			 /* .line 69 */
			 final String v0 = " RefreshRatePlicy"; // const-string v0, " RefreshRatePlicy"
			 (( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
			 /* .line 70 */
			 /* new-instance v0, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v1 = " mRefreshRatePackages: "; // const-string v1, " mRefreshRatePackages: "
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v1 = this.mRefreshRatePackages;
			 com.android.server.wm.MiuiRefreshRatePolicy$PackageRefreshRate .-$$Nest$fgetmPackages ( v1 );
			 (( java.util.HashMap ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->toString()Ljava/lang/String;
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 (( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
			 /* .line 71 */
			 return;
		 } // .end method
		 public Float getSceneRefreshRate ( com.android.server.wm.WindowState p0 ) {
			 /* .locals 3 */
			 /* .param p1, "w" # Lcom/android/server/wm/WindowState; */
			 /* .line 43 */
			 v0 = 			 (( com.android.server.wm.WindowState ) p1 ).isFocused ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->isFocused()Z
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 /* .line 44 */
				 (( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
				 /* .line 45 */
				 /* .local v0, "pkg":Ljava/lang/String; */
				 v1 = this.mRefreshRatePackages;
				 (( com.android.server.wm.MiuiRefreshRatePolicy$PackageRefreshRate ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;->get(Ljava/lang/String;)Ljava/lang/Float;
				 /* .line 46 */
				 /* .local v1, "preferredMaxRefreshRate":Ljava/lang/Float; */
				 if ( v1 != null) { // if-eqz v1, :cond_0
					 v2 = 					 (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
				 } // :cond_0
				 int v2 = 0; // const/4 v2, 0x0
			 } // :goto_0
			 /* .line 48 */
		 } // .end local v0 # "pkg":Ljava/lang/String;
	 } // .end local v1 # "preferredMaxRefreshRate":Ljava/lang/Float;
} // :cond_1
/* const/high16 v0, -0x40800000 # -1.0f */
} // .end method
public void onSystemReady ( ) {
/* .locals 1 */
/* .line 22 */
/* const-class v0, Lcom/android/server/wm/WindowManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerInternal; */
this.mWindowManagerInternal = v0;
/* .line 23 */
return;
} // .end method
public void removeRefreshRateRangeForPackage ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 59 */
v0 = this.mRefreshRatePackages;
/* monitor-enter v0 */
/* .line 60 */
try { // :try_start_0
	 v1 = this.mRefreshRatePackages;
	 (( com.android.server.wm.MiuiRefreshRatePolicy$PackageRefreshRate ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;->remove(Ljava/lang/String;)V
	 /* .line 61 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 62 */
	 v0 = this.mWindowManagerInternal;
	 (( com.android.server.wm.WindowManagerInternal ) v0 ).requestTraversalFromDisplayManager ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerInternal;->requestTraversalFromDisplayManager()V
	 /* .line 63 */
	 return;
	 /* .line 61 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 try { // :try_start_1
		 /* monitor-exit v0 */
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* throw v1 */
	 } // .end method
