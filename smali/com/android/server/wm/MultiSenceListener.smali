.class public Lcom/android/server/wm/MultiSenceListener;
.super Ljava/lang/Object;
.source "MultiSenceListener.java"

# interfaces
.implements Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MultiSenceListener$H;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final EVENT_MULTI_TASK_ACTION_END:I = 0x2

.field private static final EVENT_MULTI_TASK_ACTION_START:I = 0x1

.field private static final MSG_DISPATCH_MULTI_TASK:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MultiSenceListener"

.field private static service:Lcom/android/server/wm/WindowManagerService;


# instance fields
.field private isSystemReady:Z

.field private final mHandler:Lcom/android/server/wm/MultiSenceListener$H;

.field private mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;


# direct methods
.method static bridge synthetic -$$Nest$mLOG_IF_DEBUG(Lcom/android/server/wm/MultiSenceListener;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDynamicSenceInfo(Lcom/android/server/wm/MultiSenceListener;Lmiui/smartpower/MultiTaskActionManager$ActionInfo;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MultiSenceListener;->updateDynamicSenceInfo(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;Z)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 37
    const-string v0, "persist.multisence.debug.on"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MultiSenceListener;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 1
    .param p1, "looper"    # Landroid/os/Looper;

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MultiSenceListener;->isSystemReady:Z

    .line 52
    const-string v0, "MultiSenceListener init"

    invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 54
    new-instance v0, Lcom/android/server/wm/MultiSenceListener$H;

    invoke-direct {v0, p0, p1}, Lcom/android/server/wm/MultiSenceListener$H;-><init>(Lcom/android/server/wm/MultiSenceListener;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MultiSenceListener;->mHandler:Lcom/android/server/wm/MultiSenceListener$H;

    .line 55
    return-void
.end method

.method private LOG_IF_DEBUG(Ljava/lang/String;)V
    .locals 1
    .param p1, "log"    # Ljava/lang/String;

    .line 228
    sget-boolean v0, Lcom/android/server/wm/MultiSenceListener;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 229
    const-string v0, "MultiSenceListener"

    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_0
    return-void
.end method

.method private getWindowsNeedToSched()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/multisence/SingleWindowInfo;",
            ">;"
        }
    .end annotation

    .line 224
    invoke-static {}, Lcom/android/server/wm/MultiSenceUtils;->getInstance()Lcom/android/server/wm/MultiSenceUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/MultiSenceUtils;->getWindowsNeedToSched()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private isConnectToWMS()Z
    .locals 2

    .line 216
    sget-object v0, Lcom/android/server/wm/MultiSenceListener;->service:Lcom/android/server/wm/WindowManagerService;

    if-nez v0, :cond_0

    .line 217
    const-string v0, "MultiSenceListener"

    const-string v1, "MultiSence Listener does not connect to WMS"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    const/4 v0, 0x0

    return v0

    .line 220
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 4
    .param p1, "event"    # I
    .param p2, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 126
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceListener;->mHandler:Lcom/android/server/wm/MultiSenceListener$H;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MultiSenceListener$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 127
    .local v0, "message":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 128
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 129
    invoke-virtual {p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I

    move-result v1

    const/16 v2, 0x1001

    if-ne v1, v2, :cond_0

    .line 130
    iget-object v1, p0, Lcom/android/server/wm/MultiSenceListener;->mHandler:Lcom/android/server/wm/MultiSenceListener$H;

    const-wide/16 v2, 0x320

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/wm/MultiSenceListener$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 132
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MultiSenceListener;->mHandler:Lcom/android/server/wm/MultiSenceListener$H;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/MultiSenceListener$H;->sendMessage(Landroid/os/Message;)Z

    .line 134
    :goto_0
    return-void
.end method

.method private readyToWork()Z
    .locals 2

    .line 69
    iget-boolean v0, p0, Lcom/android/server/wm/MultiSenceListener;->isSystemReady:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 70
    return v1

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceListener;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    if-nez v0, :cond_1

    .line 74
    const-string v0, "not connect to multi-sence core service"

    invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 75
    return v1

    .line 78
    :cond_1
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-nez v0, :cond_2

    .line 79
    const-string v0, "func not enable due to Cloud Contrller"

    invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 80
    return v1

    .line 83
    :cond_2
    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->MULTITASK_ANIM_SCHED_ENABLED:Z

    if-nez v0, :cond_3

    .line 84
    const-string v0, "multi-task animation not enable by config"

    invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 85
    return v1

    .line 88
    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method private updateDynamicSenceInfo(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;Z)V
    .locals 7
    .param p1, "taskInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;
    .param p2, "isStarted"    # Z

    .line 167
    const-string v0, "_"

    .line 168
    .local v0, "tids_s":Ljava/lang/String;
    invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getDrawnTids()[I

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget v4, v1, v3

    .line 169
    .local v4, "value":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 168
    .end local v4    # "value":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 171
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Task uid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Task tid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V

    .line 172
    invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I

    move-result v1

    .line 173
    .local v1, "senceId":I
    invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getUid()I

    move-result v2

    .line 174
    .local v2, "uid":I
    invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getDrawnTids()[I

    move-result-object v3

    .line 175
    .local v3, "tids":[I
    iget-object v4, p0, Lcom/android/server/wm/MultiSenceListener;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    invoke-interface {v4, v1, v2, v3, p2}, Lcom/miui/server/multisence/MultiSenceManagerInternal;->updateDynamicWindowsInfo(II[IZ)V

    .line 176
    return-void
.end method


# virtual methods
.method public onMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 3
    .param p1, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 110
    invoke-direct {p0}, Lcom/android/server/wm/MultiSenceListener;->readyToWork()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    return-void

    .line 114
    :cond_0
    if-nez p1, :cond_1

    .line 115
    const-string v0, "MultiSenceListener"

    const-string v1, "action info must not be null"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    return-void

    .line 119
    :cond_1
    const-string v0, "MultisenceListener.onMultiTaskActionEnd"

    const-wide/16 v1, 0x20

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 120
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MultiSenceListener;->onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 121
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 122
    return-void
.end method

.method public onMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 3
    .param p1, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 93
    invoke-direct {p0}, Lcom/android/server/wm/MultiSenceListener;->readyToWork()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    return-void

    .line 97
    :cond_0
    if-nez p1, :cond_1

    .line 98
    const-string v0, "MultiSenceListener"

    const-string v1, "action info must not be null"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    return-void

    .line 102
    :cond_1
    const-string v0, "MultisenceListener.onMultiTaskActionStart"

    const-wide/16 v1, 0x20

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 103
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MultiSenceListener;->onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V

    .line 104
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 105
    return-void
.end method

.method public systemReady()Z
    .locals 2

    .line 58
    const-string v0, "miui_multi_sence"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/miui/server/multisence/MultiSenceManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/MultiSenceListener;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    .line 59
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    sput-object v0, Lcom/android/server/wm/MultiSenceListener;->service:Lcom/android/server/wm/WindowManagerService;

    .line 60
    iget-object v1, p0, Lcom/android/server/wm/MultiSenceListener;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    .line 64
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MultiSenceListener;->isSystemReady:Z

    .line 65
    return v0

    .line 61
    :cond_1
    :goto_0
    const-string v0, "MultiSenceListener"

    const-string v1, "MultiSence Listener does not connect to miui_multi_sence or WMS"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public updateScreenStatus()V
    .locals 9

    .line 186
    iget-object v0, p0, Lcom/android/server/wm/MultiSenceListener;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 192
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/MultiSenceListener;->isConnectToWMS()Z

    move-result v0

    .line 194
    .local v0, "connection":Z
    if-nez v0, :cond_1

    return-void

    .line 196
    :cond_1
    sget-object v1, Lcom/android/server/wm/MultiSenceListener;->service:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;

    move-result-object v1

    monitor-enter v1

    .line 197
    :try_start_0
    const-string v2, "getWindowsNeedToSched from multi-stack sence"

    const-wide/16 v3, 0x20

    invoke-static {v3, v4, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 198
    invoke-direct {p0}, Lcom/android/server/wm/MultiSenceListener;->getWindowsNeedToSched()Ljava/util/Map;

    move-result-object v2

    .line 199
    .local v2, "sWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    invoke-static {v3, v4}, Landroid/os/Trace;->traceEnd(J)V

    .line 200
    sget-object v3, Lcom/android/server/wm/MultiSenceListener;->service:Lcom/android/server/wm/WindowManagerService;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v3}, Lcom/android/server/wm/RootWindowContainer;->getTopFocusedDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v3

    .line 201
    .local v3, "displayContent":Lcom/android/server/wm/DisplayContent;
    iget-object v4, v3, Lcom/android/server/wm/DisplayContent;->mCurrentFocus:Lcom/android/server/wm/WindowState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 203
    .local v4, "foucsWindow":Lcom/android/server/wm/WindowState;
    :try_start_1
    invoke-virtual {v4}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/miui/server/multisence/SingleWindowInfo;->setFocused(Z)Lcom/miui/server/multisence/SingleWindowInfo;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 207
    nop

    .line 208
    .end local v3    # "displayContent":Lcom/android/server/wm/DisplayContent;
    .end local v4    # "foucsWindow":Lcom/android/server/wm/WindowState;
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209
    iget-object v3, p0, Lcom/android/server/wm/MultiSenceListener;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    monitor-enter v3

    .line 210
    :try_start_3
    iget-object v1, p0, Lcom/android/server/wm/MultiSenceListener;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    const-string v4, "multi-task"

    invoke-interface {v1, v4}, Lcom/miui/server/multisence/MultiSenceManagerInternal;->setUpdateReason(Ljava/lang/String;)V

    .line 211
    iget-object v1, p0, Lcom/android/server/wm/MultiSenceListener;->mMultiSenceMI:Lcom/miui/server/multisence/MultiSenceManagerInternal;

    invoke-interface {v1, v2}, Lcom/miui/server/multisence/MultiSenceManagerInternal;->updateStaticWindowsInfo(Ljava/util/Map;)V

    .line 212
    monitor-exit v3

    .line 213
    return-void

    .line 212
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 204
    .restart local v3    # "displayContent":Lcom/android/server/wm/DisplayContent;
    .restart local v4    # "foucsWindow":Lcom/android/server/wm/WindowState;
    :catch_0
    move-exception v5

    .line 205
    .local v5, "npe":Ljava/lang/NullPointerException;
    :try_start_4
    const-string v6, "MultiSenceListener"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setFocus error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    monitor-exit v1

    return-void

    .line 208
    .end local v2    # "sWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
    .end local v3    # "displayContent":Lcom/android/server/wm/DisplayContent;
    .end local v4    # "foucsWindow":Lcom/android/server/wm/WindowState;
    .end local v5    # "npe":Ljava/lang/NullPointerException;
    :catchall_1
    move-exception v2

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    .line 187
    .end local v0    # "connection":Z
    :cond_2
    :goto_0
    const-string v0, "MultiSenceListener"

    const-string v1, "return updateScreenStatus"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    return-void
.end method
