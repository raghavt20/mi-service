public class com.android.server.wm.OneTrackRotationHelper {
	 /* .source "OneTrackRotationHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;, */
	 /* Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID;
private static Boolean DEBUG;
private static final java.lang.String DEVICE_TYPE;
private static final Boolean ENABLE_TRACK;
private static final java.lang.String EVENT_NAME;
private static final Integer FLAG_NON_ANONYMOUS;
private static final Boolean IS_FLIP;
private static final Boolean IS_FOLD;
private static final Boolean IS_TABLET;
private static final java.lang.String ONETRACK_ACTION;
private static final Long ONE_TRACE_INTERVAL;
private static final Long ONE_TRACE_INTERVAL_DEBUG;
private static final Integer ON_DEVICE_FOLD_CHANGED;
private static final Integer ON_FOREGROUND_WINDOW_CHANGED;
private static final Integer ON_NEXT_SENDING_COMING;
private static final Integer ON_ROTATION_CHANGED;
private static final Integer ON_SCREEN_STATE_CHANGED;
private static final Integer ON_SHUT_DOWN;
private static final Integer ON_TODAY_IS_OVER;
private static final java.lang.String PACKAGE;
private static final java.lang.String PACKAGE_NAME;
private static final Integer SCREEN_OFF;
private static final Integer SCREEN_ON;
private static final java.lang.String SCREEN_ON_NOTIFICATION;
private static final java.lang.String SERVICE_PACKAGE_NAME;
private static final java.lang.String TAG;
private static final java.lang.String TIP;
private static final java.lang.String TIP_FOR_SCREENDATA;
private static Boolean mIsScreenOnNotification;
private static Long mReportInterval;
private static volatile com.android.server.wm.OneTrackRotationHelper sInstance;
/* # instance fields */
private final Float MIN_TIME;
private Long currentTimeMillis;
private java.lang.String lastForegroundPkg;
android.app.ActivityManager mAm;
android.content.Context mContext;
android.os.Handler mHandler;
private Boolean mIsInit;
android.app.KeyguardManager mKm;
android.os.PowerManager mPm;
private com.android.server.wm.OneTrackRotationHelper$RotationStateMachine mRotationStateMachine;
private android.content.BroadcastReceiver mScreenStateReceiver;
android.os.HandlerThread mThread;
android.view.WindowManager mWm;
/* # direct methods */
public static void $r8$lambda$f15T8AsVfpLDSHfz7B6-J2-Lg9M ( com.android.server.wm.OneTrackRotationHelper p0, Integer p1, java.lang.String p2 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->lambda$trackScreenData$0(ILjava/lang/String;)V */
	 return;
} // .end method
public static void $r8$lambda$xga9131XNdXrqjZSygbQkPhkf0A ( com.android.server.wm.OneTrackRotationHelper p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->initAllListeners()V */
	 return;
} // .end method
static Long -$$Nest$fgetcurrentTimeMillis ( com.android.server.wm.OneTrackRotationHelper p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J */
	 /* return-wide v0 */
} // .end method
static Boolean -$$Nest$fgetmIsInit ( com.android.server.wm.OneTrackRotationHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
} // .end method
static com.android.server.wm.OneTrackRotationHelper$RotationStateMachine -$$Nest$fgetmRotationStateMachine ( com.android.server.wm.OneTrackRotationHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mRotationStateMachine;
} // .end method
static void -$$Nest$fputcurrentTimeMillis ( com.android.server.wm.OneTrackRotationHelper p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J */
	 return;
} // .end method
static void -$$Nest$mprepareFinalSendingInToday ( com.android.server.wm.OneTrackRotationHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->prepareFinalSendingInToday()V */
	 return;
} // .end method
static void -$$Nest$mprepareNextSending ( com.android.server.wm.OneTrackRotationHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->prepareNextSending()V */
	 return;
} // .end method
static void -$$Nest$mrecordTimeForFlip ( com.android.server.wm.OneTrackRotationHelper p0, Float p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/wm/OneTrackRotationHelper;->recordTimeForFlip(F)V */
	 return;
} // .end method
static void -$$Nest$mreportOneTrack ( com.android.server.wm.OneTrackRotationHelper p0, java.util.ArrayList p1, Integer p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->reportOneTrack(Ljava/util/ArrayList;I)V */
	 return;
} // .end method
static void -$$Nest$mreportOneTrackForScreenData ( com.android.server.wm.OneTrackRotationHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->reportOneTrackForScreenData()V */
	 return;
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z */
} // .end method
static Boolean -$$Nest$sfgetIS_FLIP ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z */
} // .end method
static Boolean -$$Nest$sfgetIS_FOLD ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z */
} // .end method
static Boolean -$$Nest$sfgetmIsScreenOnNotification ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsScreenOnNotification:Z */
} // .end method
static void -$$Nest$sfputDEBUG ( Boolean p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.android.server.wm.OneTrackRotationHelper.DEBUG = (p0!= 0);
	 return;
} // .end method
static void -$$Nest$sfputmIsScreenOnNotification ( Boolean p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.android.server.wm.OneTrackRotationHelper.mIsScreenOnNotification = (p0!= 0);
	 return;
} // .end method
static void -$$Nest$sfputmReportInterval ( Long p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* sput-wide p0, Lcom/android/server/wm/OneTrackRotationHelper;->mReportInterval:J */
	 return;
} // .end method
static com.android.server.wm.OneTrackRotationHelper ( ) {
	 /* .locals 6 */
	 /* .line 51 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.android.server.wm.OneTrackRotationHelper.DEBUG = (v0!= 0);
	 /* .line 92 */
	 com.android.server.wm.OneTrackRotationHelper.mIsScreenOnNotification = (v0!= 0);
	 /* .line 96 */
	 /* const-wide/32 v1, 0x6ddd00 */
	 /* sput-wide v1, Lcom/android/server/wm/OneTrackRotationHelper;->mReportInterval:J */
	 /* .line 98 */
	 final String v1 = "ro.build.characteristics"; // const-string v1, "ro.build.characteristics"
	 final String v2 = ""; // const-string v2, ""
	 android.os.SystemProperties .get ( v1,v2 );
	 /* const-string/jumbo v2, "tablet" */
	 v1 = 	 (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 int v3 = 1; // const/4 v3, 0x1
	 /* if-nez v1, :cond_1 */
	 /* .line 99 */
	 final String v1 = "ro.config.tablet"; // const-string v1, "ro.config.tablet"
	 v1 = 	 android.os.SystemProperties .getBoolean ( v1,v0 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
	 } // :cond_0
	 /* move v1, v0 */
} // :cond_1
} // :goto_0
/* move v1, v3 */
} // :goto_1
com.android.server.wm.OneTrackRotationHelper.IS_TABLET = (v1!= 0);
/* .line 100 */
v4 = miui.util.MiuiMultiDisplayTypeInfo .isFoldDevice ( );
com.android.server.wm.OneTrackRotationHelper.IS_FOLD = (v4!= 0);
/* .line 101 */
v5 = miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
com.android.server.wm.OneTrackRotationHelper.IS_FLIP = (v5!= 0);
/* .line 102 */
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_2
if ( v4 != null) { // if-eqz v4, :cond_3
final String v2 = "fold"; // const-string v2, "fold"
} // :cond_3
if ( v5 != null) { // if-eqz v5, :cond_4
final String v2 = "flip"; // const-string v2, "flip"
} // :cond_4
final String v2 = "normal"; // const-string v2, "normal"
} // :goto_2
/* .line 103 */
/* if-nez v4, :cond_5 */
/* if-nez v1, :cond_5 */
if ( v5 != null) { // if-eqz v5, :cond_6
} // :cond_5
/* move v0, v3 */
} // :cond_6
com.android.server.wm.OneTrackRotationHelper.ENABLE_TRACK = (v0!= 0);
return;
} // .end method
private com.android.server.wm.OneTrackRotationHelper ( ) {
/* .locals 2 */
/* .line 122 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 107 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
/* .line 113 */
/* const v0, 0x3e4ccccd # 0.2f */
/* iput v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->MIN_TIME:F */
/* .line 123 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onetrack-rotation: enable = "; // const-string v1, "onetrack-rotation: enable = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "OneTrackRotationHelper"; // const-string v1, "OneTrackRotationHelper"
android.util.Slog .i ( v1,v0 );
/* .line 124 */
return;
} // .end method
private java.lang.String castToSting ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "direction" # I */
/* .line 552 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 562 */
/* const-string/jumbo v0, "unknown" */
/* .line 560 */
/* :pswitch_0 */
final String v0 = "right"; // const-string v0, "right"
/* .line 558 */
/* :pswitch_1 */
final String v0 = "down"; // const-string v0, "down"
/* .line 556 */
/* :pswitch_2 */
final String v0 = "left"; // const-string v0, "left"
/* .line 554 */
/* :pswitch_3 */
/* const-string/jumbo v0, "up" */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Integer getDisplayRotation ( ) {
/* .locals 1 */
/* .line 193 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
/* if-nez v0, :cond_0 */
/* .line 194 */
int v0 = 0; // const/4 v0, 0x0
/* .line 197 */
} // :cond_0
v0 = this.mWm;
v0 = (( android.view.Display ) v0 ).getRotation ( ); // invoke-virtual {v0}, Landroid/view/Display;->getRotation()I
} // .end method
public static synchronized com.android.server.wm.OneTrackRotationHelper getInstance ( ) {
/* .locals 2 */
/* const-class v0, Lcom/android/server/wm/OneTrackRotationHelper; */
/* monitor-enter v0 */
/* .line 116 */
try { // :try_start_0
v1 = com.android.server.wm.OneTrackRotationHelper.sInstance;
/* if-nez v1, :cond_0 */
/* .line 117 */
/* new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper; */
/* invoke-direct {v1}, Lcom/android/server/wm/OneTrackRotationHelper;-><init>()V */
/* .line 119 */
} // :cond_0
v1 = com.android.server.wm.OneTrackRotationHelper.sInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 115 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* throw v1 */
} // .end method
private Boolean getKeyguardLocked ( ) {
/* .locals 1 */
/* .line 209 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
/* if-nez v0, :cond_0 */
/* .line 210 */
int v0 = 0; // const/4 v0, 0x0
/* .line 213 */
} // :cond_0
v0 = this.mKm;
v0 = (( android.app.KeyguardManager ) v0 ).isKeyguardLocked ( ); // invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z
} // .end method
private Boolean getScreenState ( ) {
/* .locals 1 */
/* .line 201 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
/* if-nez v0, :cond_0 */
/* .line 202 */
int v0 = 0; // const/4 v0, 0x0
/* .line 205 */
} // :cond_0
v0 = this.mPm;
v0 = (( android.os.PowerManager ) v0 ).isScreenOn ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z
} // .end method
private java.lang.String getTopAppName ( ) {
/* .locals 4 */
/* .line 225 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
final String v1 = ""; // const-string v1, ""
/* if-nez v0, :cond_0 */
/* .line 226 */
/* .line 230 */
} // :cond_0
try { // :try_start_0
v0 = this.mAm;
int v2 = 1; // const/4 v2, 0x1
(( android.app.ActivityManager ) v0 ).getRunningTasks ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
/* .line 231 */
v2 = /* .local v0, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;" */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 232 */
int v2 = 0; // const/4 v2, 0x0
/* check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo; */
v2 = this.topActivity;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 234 */
} // :cond_1
/* .line 236 */
} // .end local v0 # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
/* :catch_0 */
/* move-exception v0 */
/* .line 237 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getTopAppName e= "; // const-string v3, "getTopAppName e= "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "OneTrackRotationHelper"; // const-string v3, "OneTrackRotationHelper"
android.util.Slog .e ( v3,v2 );
/* .line 238 */
} // .end method
private void initAllListeners ( ) {
/* .locals 6 */
/* .line 275 */
v0 = this.mContext;
final String v1 = "OneTrackRotationHelper"; // const-string v1, "OneTrackRotationHelper"
/* if-nez v0, :cond_0 */
/* .line 276 */
final String v0 = "initAllListeners mContext = null"; // const-string v0, "initAllListeners mContext = null"
android.util.Slog .e ( v1,v0 );
/* .line 277 */
return;
/* .line 281 */
} // :cond_0
/* sget-boolean v2, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z */
/* if-nez v2, :cond_1 */
/* sget-boolean v2, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 282 */
} // :cond_1
/* const-class v2, Landroid/hardware/devicestate/DeviceStateManager; */
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/devicestate/DeviceStateManager; */
/* .line 283 */
/* .local v0, "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 284 */
/* new-instance v2, Landroid/os/HandlerExecutor; */
v3 = this.mHandler;
/* invoke-direct {v2, v3}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V */
/* new-instance v3, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener; */
v4 = this.mContext;
/* new-instance v5, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda2; */
/* invoke-direct {v5, p0}, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;)V */
/* invoke-direct {v3, v4, v5}, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;-><init>(Landroid/content/Context;Ljava/util/function/Consumer;)V */
(( android.hardware.devicestate.DeviceStateManager ) v0 ).registerCallback ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V
/* .line 287 */
} // :cond_2
final String v2 = "deviceStateManager == null"; // const-string v2, "deviceStateManager == null"
android.util.Slog .v ( v1,v2 );
/* .line 292 */
} // .end local v0 # "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
} // :cond_3
} // :goto_0
/* new-instance v0, Lcom/android/server/wm/OneTrackRotationHelper$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/OneTrackRotationHelper$2;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;)V */
this.mScreenStateReceiver = v0;
/* .line 314 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 315 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.SCREEN_OFF"; // const-string v2, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 316 */
final String v2 = "android.intent.action.USER_PRESENT"; // const-string v2, "android.intent.action.USER_PRESENT"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 317 */
final String v2 = "android.intent.action.ACTION_SHUTDOWN"; // const-string v2, "android.intent.action.ACTION_SHUTDOWN"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 318 */
final String v2 = "android.intent.action.REBOOT"; // const-string v2, "android.intent.action.REBOOT"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 320 */
try { // :try_start_0
v2 = this.mContext;
v3 = this.mScreenStateReceiver;
(( android.content.Context ) v2 ).registerReceiver ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 324 */
/* nop */
/* .line 326 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
/* .line 327 */
final String v2 = "OneTrackRotationHelper init successfully"; // const-string v2, "OneTrackRotationHelper init successfully"
android.util.Slog .i ( v1,v2 );
/* .line 328 */
return;
/* .line 321 */
/* :catch_0 */
/* move-exception v2 */
/* .line 322 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "initAllListeners e = "; // const-string v4, "initAllListeners e = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 323 */
return;
} // .end method
private void initOneTrackRotationThread ( ) {
/* .locals 2 */
/* .line 243 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "OneTrackRotationThread"; // const-string v1, "OneTrackRotationThread"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mThread = v0;
/* .line 244 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 245 */
/* new-instance v0, Lcom/android/server/wm/OneTrackRotationHelper$1; */
v1 = this.mThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/OneTrackRotationHelper$1;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 272 */
return;
} // .end method
private void initializeData ( ) {
/* .locals 8 */
/* .line 162 */
final String v0 = ""; // const-string v0, ""
/* .line 163 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = this.mAm;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 165 */
try { // :try_start_0
(( android.app.ActivityManager ) v1 ).getRunningTasks ( v3 ); // invoke-virtual {v1, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
/* .line 166 */
v4 = /* .local v1, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;" */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 167 */
/* check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo; */
v4 = this.topActivity;
(( android.content.ComponentName ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v4 */
/* .line 171 */
} // .end local v1 # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
} // :cond_0
/* .line 169 */
/* :catch_0 */
/* move-exception v1 */
/* .line 170 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "initializeData packageName e= "; // const-string v5, "initializeData packageName e= "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "OneTrackRotationHelper"; // const-string v5, "OneTrackRotationHelper"
android.util.Slog .e ( v5,v4 );
/* .line 174 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* .line 175 */
/* .local v1, "screenState":Z */
v4 = this.mPm;
if ( v4 != null) { // if-eqz v4, :cond_3
v5 = this.mKm;
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 176 */
v4 = (( android.os.PowerManager ) v4 ).isScreenOn ( ); // invoke-virtual {v4}, Landroid/os/PowerManager;->isScreenOn()Z
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = this.mKm;
v4 = (( android.app.KeyguardManager ) v4 ).isKeyguardLocked ( ); // invoke-virtual {v4}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z
/* if-nez v4, :cond_2 */
/* move v4, v3 */
} // :cond_2
/* move v4, v2 */
} // :goto_1
/* move v1, v4 */
/* .line 179 */
} // :cond_3
int v4 = 0; // const/4 v4, 0x0
/* .line 180 */
/* .local v4, "rotation":I */
v5 = this.mWm;
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 181 */
(( android.view.Display ) v5 ).getRotation ( ); // invoke-virtual {v5}, Landroid/view/Display;->getRotation()I
/* .line 184 */
} // :cond_4
int v5 = 0; // const/4 v5, 0x0
/* .line 185 */
/* .local v5, "folded":Z */
/* sget-boolean v6, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z */
/* if-nez v6, :cond_5 */
/* sget-boolean v6, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z */
if ( v6 != null) { // if-eqz v6, :cond_7
} // :cond_5
v6 = this.mContext;
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 186 */
(( android.content.Context ) v6 ).getContentResolver ( ); // invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v7 = "device_posture"; // const-string v7, "device_posture"
v6 = android.provider.Settings$Global .getInt ( v6,v7,v2 );
/* if-ne v6, v3, :cond_6 */
/* move v2, v3 */
} // :cond_6
/* move v5, v2 */
/* .line 189 */
} // :cond_7
v2 = this.mRotationStateMachine;
(( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine ) v2 ).init ( v0, v1, v5, v4 ); // invoke-virtual {v2, v0, v1, v5, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->init(Ljava/lang/String;ZZI)V
/* .line 190 */
return;
} // .end method
private Boolean isReportXiaomiServer ( ) {
/* .locals 3 */
/* .line 435 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 436 */
/* .local v0, "region":Ljava/lang/String; */
/* sget-boolean v1, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 437 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "the region is :" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "OneTrackRotationHelper"; // const-string v2, "OneTrackRotationHelper"
android.util.Slog .i ( v2,v1 );
/* .line 439 */
} // :cond_0
final String v1 = "CN"; // const-string v1, "CN"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_2 */
final String v1 = "RU"; // const-string v1, "RU"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :cond_2
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
} // .end method
private void lambda$trackScreenData$0 ( Integer p0, java.lang.String p1 ) { //synthethic
/* .locals 0 */
/* .param p1, "wakefulness" # I */
/* .param p2, "details" # Ljava/lang/String; */
/* .line 447 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->sendOneTrackForScreenData(ILjava/lang/String;)V */
/* .line 448 */
return;
} // .end method
private void prepareFinalSendingInToday ( ) {
/* .locals 10 */
/* .line 374 */
/* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 375 */
final String v0 = "OneTrackRotationHelper"; // const-string v0, "OneTrackRotationHelper"
final String v1 = "prepareFinalSendingInToday"; // const-string v1, "prepareFinalSendingInToday"
android.util.Slog .i ( v0,v1 );
/* .line 379 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 381 */
/* .local v0, "now":J */
java.time.LocalDate .now ( );
/* .line 382 */
/* .local v2, "localDate":Ljava/time/LocalDate; */
/* const-wide/16 v3, 0x1 */
(( java.time.LocalDate ) v2 ).plusDays ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/time/LocalDate;->plusDays(J)Ljava/time/LocalDate;
/* .line 383 */
v3 = (( java.time.LocalDate ) v2 ).getYear ( ); // invoke-virtual {v2}, Ljava/time/LocalDate;->getYear()I
(( java.time.LocalDate ) v2 ).getMonth ( ); // invoke-virtual {v2}, Ljava/time/LocalDate;->getMonth()Ljava/time/Month;
v5 = (( java.time.LocalDate ) v2 ).getDayOfMonth ( ); // invoke-virtual {v2}, Ljava/time/LocalDate;->getDayOfMonth()I
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* invoke-static/range {v3 ..v8}, Ljava/time/LocalDateTime;->of(ILjava/time/Month;IIII)Ljava/time/LocalDateTime; */
/* .line 384 */
/* .local v3, "dateTime":Ljava/time/LocalDateTime; */
java.time.ZoneId .systemDefault ( );
(( java.time.LocalDateTime ) v3 ).atZone ( v4 ); // invoke-virtual {v3, v4}, Ljava/time/LocalDateTime;->atZone(Ljava/time/ZoneId;)Ljava/time/ZonedDateTime;
(( java.time.ZonedDateTime ) v4 ).toInstant ( ); // invoke-virtual {v4}, Ljava/time/ZonedDateTime;->toInstant()Ljava/time/Instant;
(( java.time.Instant ) v4 ).toEpochMilli ( ); // invoke-virtual {v4}, Ljava/time/Instant;->toEpochMilli()J
/* move-result-wide v4 */
/* .line 386 */
/* .local v4, "nextDay":J */
v6 = this.mRotationStateMachine;
com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$msetToday ( v6,v0,v1 );
/* .line 387 */
v6 = this.mHandler;
int v7 = 6; // const/4 v7, 0x6
(( android.os.Handler ) v6 ).removeMessages ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V
/* .line 388 */
v6 = this.mHandler;
/* sub-long v8, v4, v0 */
(( android.os.Handler ) v6 ).sendEmptyMessageDelayed ( v7, v8, v9 ); // invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 389 */
return;
} // .end method
private void prepareNextSending ( ) {
/* .locals 4 */
/* .line 362 */
/* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 363 */
final String v0 = "OneTrackRotationHelper"; // const-string v0, "OneTrackRotationHelper"
final String v1 = "prepareNextSending"; // const-string v1, "prepareNextSending"
android.util.Slog .i ( v0,v1 );
/* .line 366 */
} // :cond_0
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 367 */
v0 = this.mHandler;
/* sget-wide v2, Lcom/android/server/wm/OneTrackRotationHelper;->mReportInterval:J */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 368 */
return;
} // .end method
private void recordTimeForFlip ( Float p0 ) {
/* .locals 4 */
/* .param p1, "duration" # F */
/* .line 467 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "screen_direction_"; // const-string v1, "screen_direction_"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRotationStateMachine;
v1 = com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetmDisplayRotation ( v1 );
/* invoke-direct {p0, v1}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 468 */
/* .local v0, "key":Ljava/lang/String; */
v1 = this.mRotationStateMachine;
com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetfilpUsageDate ( v1 );
int v2 = 0; // const/4 v2, 0x0
java.lang.Float .valueOf ( v2 );
(( java.util.HashMap ) v1 ).getOrDefault ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* .line 469 */
/* .local v1, "existingDuration":F */
v2 = this.mRotationStateMachine;
com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetfilpUsageDate ( v2 );
/* add-float v3, v1, p1 */
java.lang.Float .valueOf ( v3 );
(( java.util.HashMap ) v2 ).put ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 470 */
return;
} // .end method
private void reportOneTrack ( java.util.ArrayList p0, Integer p1 ) {
/* .locals 18 */
/* .param p2, "reportDate" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 393 */
/* .local p1, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move/from16 v3, p2 */
final String v4 = "OneTrackRotationHelper"; // const-string v4, "OneTrackRotationHelper"
try { // :try_start_0
v0 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/wm/OneTrackRotationHelper;->isReportXiaomiServer()Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_3 */
final String v5 = "report_date"; // const-string v5, "report_date"
final String v6 = "model_type"; // const-string v6, "model_type"
final String v7 = "all_app_usage_time"; // const-string v7, "all_app_usage_time"
final String v8 = "866.2.1.1.28680"; // const-string v8, "866.2.1.1.28680"
/* const-string/jumbo v9, "tip" */
final String v10 = "android"; // const-string v10, "android"
final String v11 = "PACKAGE"; // const-string v11, "PACKAGE"
/* const-string/jumbo v12, "screen_use_duration" */
final String v13 = "EVENT_NAME"; // const-string v13, "EVENT_NAME"
final String v14 = "31000000779"; // const-string v14, "31000000779"
final String v15 = "APP_ID"; // const-string v15, "APP_ID"
/* move-object/from16 v16, v4 */
final String v4 = "com.miui.analytics"; // const-string v4, "com.miui.analytics"
final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 394 */
try { // :try_start_1
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 395 */
/* .local v0, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v0 ).setPackage ( v4 ); // invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 396 */
(( android.content.Intent ) v0 ).putExtra ( v15, v14 ); // invoke-virtual {v0, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 397 */
(( android.content.Intent ) v0 ).putExtra ( v13, v12 ); // invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 398 */
(( android.content.Intent ) v0 ).putExtra ( v11, v10 ); // invoke-virtual {v0, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 399 */
(( android.content.Intent ) v0 ).putExtra ( v9, v8 ); // invoke-virtual {v0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 400 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 401 */
/* .local v1, "params":Landroid/os/Bundle; */
(( android.os.Bundle ) v1 ).putStringArrayList ( v7, v2 ); // invoke-virtual {v1, v7, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
/* .line 402 */
(( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 403 */
v4 = com.android.server.wm.OneTrackRotationHelper.DEVICE_TYPE;
(( android.content.Intent ) v0 ).putExtra ( v6, v4 ); // invoke-virtual {v0, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 404 */
(( android.content.Intent ) v0 ).putExtra ( v5, v3 ); // invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
/* .line 405 */
/* move-object/from16 v4, p0 */
try { // :try_start_2
v5 = this.mContext;
v6 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v5 ).startServiceAsUser ( v0, v6 ); // invoke-virtual {v5, v0, v6}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 406 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v1 # "params":Landroid/os/Bundle;
/* .line 429 */
/* :catch_0 */
/* move-exception v0 */
/* .line 408 */
} // :cond_0
/* move-object v0, v1 */
/* move-object/from16 v1, p0 */
try { // :try_start_3
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 409 */
/* .restart local v0 # "intent":Landroid/content/Intent; */
(( android.content.Intent ) v0 ).setPackage ( v4 ); // invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 410 */
(( android.content.Intent ) v0 ).putExtra ( v15, v14 ); // invoke-virtual {v0, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 411 */
(( android.content.Intent ) v0 ).putExtra ( v13, v12 ); // invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 412 */
(( android.content.Intent ) v0 ).putExtra ( v11, v10 ); // invoke-virtual {v0, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 413 */
final String v1 = "PROJECT_ID"; // const-string v1, "PROJECT_ID"
/* const-string/jumbo v4, "thirdappadaptation" */
(( android.content.Intent ) v0 ).putExtra ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 414 */
final String v1 = "TOPIC"; // const-string v1, "TOPIC"
/* const-string/jumbo v4, "topic_ods_pubsub_event_di_31000000779" */
(( android.content.Intent ) v0 ).putExtra ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 415 */
final String v1 = "PRIVATE_KEY_ID"; // const-string v1, "PRIVATE_KEY_ID"
final String v4 = "9f3945ec5765512b0ca43029da3f62aa93613c93"; // const-string v4, "9f3945ec5765512b0ca43029da3f62aa93613c93"
(( android.content.Intent ) v0 ).putExtra ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 416 */
(( android.content.Intent ) v0 ).putExtra ( v9, v8 ); // invoke-virtual {v0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 417 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 418 */
/* .restart local v1 # "params":Landroid/os/Bundle; */
(( android.os.Bundle ) v1 ).putStringArrayList ( v7, v2 ); // invoke-virtual {v1, v7, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
/* .line 419 */
(( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 420 */
v4 = com.android.server.wm.OneTrackRotationHelper.DEVICE_TYPE;
(( android.content.Intent ) v0 ).putExtra ( v6, v4 ); // invoke-virtual {v0, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 421 */
(( android.content.Intent ) v0 ).putExtra ( v5, v3 ); // invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 422 */
int v4 = 2; // const/4 v4, 0x2
(( android.content.Intent ) v0 ).setFlags ( v4 ); // invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 423 */
/* move-object/from16 v4, p0 */
try { // :try_start_4
v5 = this.mContext;
v6 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v5 ).startServiceAsUser ( v0, v6 ); // invoke-virtual {v5, v0, v6}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* .line 426 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v1 # "params":Landroid/os/Bundle;
} // :goto_0
/* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 427 */
final String v0 = "reportOneTrack"; // const-string v0, "reportOneTrack"
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* move-object/from16 v1, v16 */
try { // :try_start_5
android.util.Slog .i ( v1,v0 );
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_1 */
/* .line 429 */
/* :catch_1 */
/* move-exception v0 */
/* .line 431 */
} // :cond_1
} // :goto_1
/* .line 429 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v4, p0 */
} // :goto_2
/* move-object/from16 v1, v16 */
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v17, v4 */
/* move-object v4, v1 */
/* move-object/from16 v1, v17 */
/* .line 430 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_3
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "reportOneTrack e = "; // const-string v6, "reportOneTrack e = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v5 );
/* .line 432 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
return;
} // .end method
private void reportOneTrackForScreenData ( ) {
/* .locals 8 */
/* .line 473 */
/* iget-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iget-wide v6, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J */
/* sub-long/2addr v4, v6 */
/* long-to-float v0, v4 */
/* const/high16 v4, 0x447a0000 # 1000.0f */
/* div-float/2addr v0, v4 */
} // :cond_0
/* move v0, v1 */
/* .line 474 */
/* .local v0, "duration":F */
} // :goto_0
/* cmpl-float v1, v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* const v1, 0x3e4ccccd # 0.2f */
/* cmpl-float v1, v0, v1 */
/* if-lez v1, :cond_2 */
/* .line 475 */
/* sget-boolean v1, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
v4 = this.mRotationStateMachine;
v4 = com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetmFolded ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 476 */
v4 = this.mRotationStateMachine;
com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetoneTrackRotationHelper ( v4 );
/* invoke-direct {v4, v0}, Lcom/android/server/wm/OneTrackRotationHelper;->recordTimeForFlip(F)V */
/* .line 478 */
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/android/server/wm/OneTrackRotationHelper;->reportOneTrackForScreenData(F)V */
/* .line 479 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 480 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->resetFilpUsageDate()V */
/* .line 483 */
} // :cond_2
/* iput-wide v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J */
/* .line 484 */
return;
} // .end method
private void reportOneTrackForScreenData ( Float p0 ) {
/* .locals 20 */
/* .param p1, "duration" # F */
/* .line 488 */
/* move-object/from16 v1, p0 */
try { // :try_start_0
v0 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/wm/OneTrackRotationHelper;->isReportXiaomiServer()Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
final String v2 = "_time"; // const-string v2, "_time"
/* const-string/jumbo v4, "use_duration" */
/* const-string/jumbo v5, "\u5916\u5c4f" */
/* const-string/jumbo v6, "\u5185\u5c4f" */
final String v7 = "model_type"; // const-string v7, "model_type"
final String v8 = "866.2.1.1.32924"; // const-string v8, "866.2.1.1.32924"
/* const-string/jumbo v9, "tip" */
final String v10 = "android"; // const-string v10, "android"
final String v11 = "PACKAGE"; // const-string v11, "PACKAGE"
/* const-string/jumbo v12, "screen_use_duration" */
final String v13 = "EVENT_NAME"; // const-string v13, "EVENT_NAME"
final String v14 = "31000000779"; // const-string v14, "31000000779"
final String v15 = "APP_ID"; // const-string v15, "APP_ID"
final String v3 = "com.miui.analytics"; // const-string v3, "com.miui.analytics"
/* move-object/from16 v16, v4 */
final String v4 = "onetrack.action.TRACK_EVENT"; // const-string v4, "onetrack.action.TRACK_EVENT"
/* move-object/from16 v17, v2 */
final String v2 = "screen_direction_"; // const-string v2, "screen_direction_"
/* move-object/from16 v18, v2 */
/* const-string/jumbo v2, "screen_type" */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 489 */
try { // :try_start_1
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 490 */
/* .local v0, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v0 ).setPackage ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 491 */
(( android.content.Intent ) v0 ).putExtra ( v15, v14 ); // invoke-virtual {v0, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 492 */
(( android.content.Intent ) v0 ).putExtra ( v13, v12 ); // invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 493 */
(( android.content.Intent ) v0 ).putExtra ( v11, v10 ); // invoke-virtual {v0, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 494 */
(( android.content.Intent ) v0 ).putExtra ( v9, v8 ); // invoke-virtual {v0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 495 */
v3 = com.android.server.wm.OneTrackRotationHelper.DEVICE_TYPE;
(( android.content.Intent ) v0 ).putExtra ( v7, v3 ); // invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 496 */
/* sget-boolean v3, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z */
/* if-nez v3, :cond_0 */
/* sget-boolean v3, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 497 */
} // :cond_0
v3 = this.mRotationStateMachine;
v3 = com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetmFolded ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 498 */
(( android.content.Intent ) v0 ).putExtra ( v2, v5 ); // invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 500 */
} // :cond_1
(( android.content.Intent ) v0 ).putExtra ( v2, v6 ); // invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 503 */
} // :cond_2
} // :goto_0
/* sget-boolean v2, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = this.mRotationStateMachine;
v2 = com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetmFolded ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 504 */
int v2 = 3; // const/4 v2, 0x3
int v3 = 2; // const/4 v3, 0x2
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
/* filled-new-array {v5, v4, v3, v2}, [I */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 505 */
/* .local v2, "directions":[I */
int v3 = 0; // const/4 v3, 0x0
/* .line 506 */
} // .end local p1 # "duration":F
/* .local v3, "duration":F */
try { // :try_start_2
/* array-length v4, v2 */
int v5 = 0; // const/4 v5, 0x0
} // :goto_1
/* if-ge v5, v4, :cond_4 */
/* aget v6, v2, v5 */
/* .line 507 */
/* .local v6, "direction":I */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v8, v18 */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {v1, v6}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 508 */
/* .local v7, "key":Ljava/lang/String; */
v9 = this.mRotationStateMachine;
com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetfilpUsageDate ( v9 );
(( java.util.HashMap ) v9 ).get ( v7 ); // invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v9, Ljava/lang/Float; */
v9 = (( java.lang.Float ) v9 ).floatValue ( ); // invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F
/* .line 509 */
/* .local v9, "time":F */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {v1, v6}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v11, v17 */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 510 */
/* .local v10, "timeKey":Ljava/lang/String; */
(( android.content.Intent ) v0 ).putExtra ( v10, v9 ); // invoke-virtual {v0, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 511 */
/* add-float/2addr v3, v9 */
/* .line 506 */
} // .end local v6 # "direction":I
} // .end local v7 # "key":Ljava/lang/String;
} // .end local v9 # "time":F
} // .end local v10 # "timeKey":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* move-object/from16 v18, v8 */
/* move-object/from16 v17, v11 */
/* .line 514 */
} // .end local v2 # "directions":[I
} // .end local v3 # "duration":F
/* .restart local p1 # "duration":F */
} // :cond_3
/* move/from16 v3, p1 */
} // .end local p1 # "duration":F
/* .restart local v3 # "duration":F */
} // :cond_4
/* move-object/from16 v2, v16 */
(( android.content.Intent ) v0 ).putExtra ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 515 */
v2 = this.mContext;
v4 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v2 ).startServiceAsUser ( v0, v4 ); // invoke-virtual {v2, v0, v4}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 516 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
/* goto/16 :goto_4 */
/* .line 517 */
} // .end local v3 # "duration":F
/* .restart local p1 # "duration":F */
} // :cond_5
/* move-object/from16 v19, v16 */
/* move-object/from16 v0, v18 */
try { // :try_start_3
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 518 */
/* .restart local v0 # "intent":Landroid/content/Intent; */
(( android.content.Intent ) v0 ).setPackage ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 519 */
(( android.content.Intent ) v0 ).putExtra ( v15, v14 ); // invoke-virtual {v0, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 520 */
(( android.content.Intent ) v0 ).putExtra ( v13, v12 ); // invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 521 */
(( android.content.Intent ) v0 ).putExtra ( v11, v10 ); // invoke-virtual {v0, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 522 */
(( android.content.Intent ) v0 ).putExtra ( v9, v8 ); // invoke-virtual {v0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 523 */
v3 = com.android.server.wm.OneTrackRotationHelper.DEVICE_TYPE;
(( android.content.Intent ) v0 ).putExtra ( v7, v3 ); // invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 524 */
/* sget-boolean v3, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z */
/* if-nez v3, :cond_6 */
/* sget-boolean v3, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z */
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 525 */
} // :cond_6
v3 = this.mRotationStateMachine;
v3 = com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetmFolded ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 526 */
(( android.content.Intent ) v0 ).putExtra ( v2, v5 ); // invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 528 */
} // :cond_7
(( android.content.Intent ) v0 ).putExtra ( v2, v6 ); // invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 531 */
} // :cond_8
} // :goto_2
/* sget-boolean v2, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z */
if ( v2 != null) { // if-eqz v2, :cond_9
v2 = this.mRotationStateMachine;
v2 = com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetmFolded ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 532 */
int v2 = 3; // const/4 v2, 0x3
int v3 = 2; // const/4 v3, 0x2
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
/* filled-new-array {v5, v4, v3, v2}, [I */
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 533 */
/* .restart local v2 # "directions":[I */
int v3 = 0; // const/4 v3, 0x0
/* .line 534 */
} // .end local p1 # "duration":F
/* .restart local v3 # "duration":F */
try { // :try_start_4
/* array-length v4, v2 */
} // :goto_3
/* if-ge v5, v4, :cond_a */
/* aget v6, v2, v5 */
/* .line 535 */
/* .restart local v6 # "direction":I */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v8, v18 */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {v1, v6}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 536 */
/* .restart local v7 # "key":Ljava/lang/String; */
v9 = this.mRotationStateMachine;
com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetfilpUsageDate ( v9 );
(( java.util.HashMap ) v9 ).get ( v7 ); // invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v9, Ljava/lang/Float; */
v9 = (( java.lang.Float ) v9 ).floatValue ( ); // invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F
/* .line 537 */
/* .restart local v9 # "time":F */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {v1, v6}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v11, v17 */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 538 */
/* .restart local v10 # "timeKey":Ljava/lang/String; */
(( android.content.Intent ) v0 ).putExtra ( v10, v9 ); // invoke-virtual {v0, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 539 */
/* add-float/2addr v3, v9 */
/* .line 534 */
} // .end local v6 # "direction":I
} // .end local v7 # "key":Ljava/lang/String;
} // .end local v9 # "time":F
} // .end local v10 # "timeKey":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* move-object/from16 v18, v8 */
/* move-object/from16 v17, v11 */
/* .line 542 */
} // .end local v2 # "directions":[I
} // .end local v3 # "duration":F
/* .restart local p1 # "duration":F */
} // :cond_9
/* move/from16 v3, p1 */
} // .end local p1 # "duration":F
/* .restart local v3 # "duration":F */
} // :cond_a
/* move-object/from16 v2, v19 */
(( android.content.Intent ) v0 ).putExtra ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 543 */
int v2 = 2; // const/4 v2, 0x2
(( android.content.Intent ) v0 ).setFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 544 */
v2 = this.mContext;
v4 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v2 ).startServiceAsUser ( v0, v4 ); // invoke-virtual {v2, v0, v4}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 548 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :goto_4
/* .line 546 */
/* :catch_0 */
/* move-exception v0 */
} // .end local v3 # "duration":F
/* .restart local p1 # "duration":F */
/* :catch_1 */
/* move-exception v0 */
/* move/from16 v3, p1 */
/* .line 547 */
} // .end local p1 # "duration":F
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v3 # "duration":F */
} // :goto_5
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "reportOneTrackForScreenData e = "; // const-string v4, "reportOneTrackForScreenData e = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "OneTrackRotationHelper"; // const-string v4, "OneTrackRotationHelper"
android.util.Slog .e ( v4,v2 );
/* .line 549 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_6
return;
} // .end method
private void resetFilpUsageDate ( ) {
/* .locals 4 */
/* .line 567 */
v0 = this.mRotationStateMachine;
com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetfilpUsageDate ( v0 );
(( java.util.HashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/lang/String; */
/* .line 568 */
/* .local v1, "key":Ljava/lang/String; */
v2 = this.mRotationStateMachine;
com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$fgetfilpUsageDate ( v2 );
int v3 = 0; // const/4 v3, 0x0
java.lang.Float .valueOf ( v3 );
(( java.util.HashMap ) v2 ).put ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 569 */
} // .end local v1 # "key":Ljava/lang/String;
/* .line 570 */
} // :cond_0
return;
} // .end method
private void sendOneTrackForScreenData ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "wakefulness" # I */
/* .param p2, "details" # Ljava/lang/String; */
/* .line 453 */
int v0 = 1; // const/4 v0, 0x1
if ( p2 != null) { // if-eqz p2, :cond_0
final String v1 = "com.android.systemui:NOTIFICATION"; // const-string v1, "com.android.systemui:NOTIFICATION"
v1 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 454 */
com.android.server.wm.OneTrackRotationHelper.mIsScreenOnNotification = (v0!= 0);
/* .line 455 */
return;
/* .line 457 */
} // :cond_0
/* if-ne p1, v0, :cond_1 */
/* .line 458 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J */
/* .line 460 */
} // :cond_1
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_2 */
/* .line 461 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.wm.OneTrackRotationHelper.mIsScreenOnNotification = (v0!= 0);
/* .line 462 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->reportOneTrackForScreenData()V */
/* .line 464 */
} // :cond_2
return;
} // .end method
/* # virtual methods */
public void init ( ) {
/* .locals 4 */
/* .line 127 */
/* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z */
/* if-nez v0, :cond_0 */
/* .line 128 */
return;
/* .line 131 */
} // :cond_0
final String v0 = "onetrack-rotation init"; // const-string v0, "onetrack-rotation init"
final String v1 = "OneTrackRotationHelper"; // const-string v1, "OneTrackRotationHelper"
android.util.Slog .i ( v1,v0 );
/* .line 133 */
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v0 ).getApplication ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;
this.mContext = v0;
/* .line 134 */
/* if-nez v0, :cond_1 */
/* .line 135 */
final String v0 = "init OneTrackRotationHelper mContext = null"; // const-string v0, "init OneTrackRotationHelper mContext = null"
android.util.Slog .e ( v1,v0 );
/* .line 136 */
return;
/* .line 139 */
} // :cond_1
/* new-instance v0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;)V */
this.mRotationStateMachine = v0;
/* .line 141 */
v0 = this.mContext;
final String v2 = "activity"; // const-string v2, "activity"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
this.mAm = v0;
/* .line 142 */
v0 = this.mContext;
/* const-string/jumbo v2, "window" */
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/WindowManager; */
this.mWm = v0;
/* .line 143 */
v0 = this.mContext;
final String v2 = "power"; // const-string v2, "power"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
this.mPm = v0;
/* .line 144 */
v0 = this.mContext;
final String v2 = "keyguard"; // const-string v2, "keyguard"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/KeyguardManager; */
this.mKm = v0;
/* .line 145 */
v2 = this.mAm;
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = this.mWm;
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = this.mPm;
if ( v2 != null) { // if-eqz v2, :cond_3
/* if-nez v0, :cond_2 */
/* .line 154 */
} // :cond_2
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->initOneTrackRotationThread()V */
/* .line 156 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->initializeData()V */
/* .line 158 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 159 */
return;
/* .line 146 */
} // :cond_3
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getSystemService failed service:"; // const-string v2, "getSystemService failed service:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 147 */
v2 = this.mAm;
final String v3 = ""; // const-string v3, ""
/* if-nez v2, :cond_4 */
final String v2 = " AM"; // const-string v2, " AM"
} // :cond_4
/* move-object v2, v3 */
} // :goto_1
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 148 */
v2 = this.mWm;
/* if-nez v2, :cond_5 */
final String v2 = " WM"; // const-string v2, " WM"
} // :cond_5
/* move-object v2, v3 */
} // :goto_2
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 149 */
v2 = this.mPm;
/* if-nez v2, :cond_6 */
final String v2 = " PM"; // const-string v2, " PM"
} // :cond_6
/* move-object v2, v3 */
} // :goto_3
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 150 */
v2 = this.mKm;
/* if-nez v2, :cond_7 */
final String v3 = " KM"; // const-string v3, " KM"
} // :cond_7
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 146 */
android.util.Slog .e ( v1,v0 );
/* .line 151 */
return;
} // .end method
public Boolean isScreenRealUnlocked ( ) {
/* .locals 2 */
/* .line 217 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 218 */
/* .line 221 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->getScreenState()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->getKeyguardLocked()Z */
/* if-nez v0, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
public void reportDeviceFolded ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "folded" # Z */
/* .line 353 */
/* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
/* if-nez v0, :cond_0 */
/* .line 356 */
} // :cond_0
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
android.os.Message .obtain ( v0,v1 );
/* .line 357 */
/* .local v0, "message":Landroid/os/Message; */
/* new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
java.lang.Boolean .valueOf ( p1 );
/* invoke-direct {v1, v2}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>(Ljava/lang/Object;)V */
this.obj = v1;
/* .line 358 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 359 */
return;
/* .line 354 */
} // .end local v0 # "message":Landroid/os/Message;
} // :cond_1
} // :goto_0
return;
} // .end method
public void reportPackageForeground ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 342 */
/* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
/* if-nez v0, :cond_0 */
/* .line 344 */
} // :cond_0
v0 = this.lastForegroundPkg;
v0 = android.text.TextUtils .equals ( p1,v0 );
/* if-nez v0, :cond_1 */
/* .line 345 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
android.os.Message .obtain ( v0,v1 );
/* .line 346 */
/* .local v0, "message":Landroid/os/Message; */
/* new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
/* invoke-direct {v1, p1}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>(Ljava/lang/Object;)V */
this.obj = v1;
/* .line 347 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 348 */
this.lastForegroundPkg = p1;
/* .line 350 */
} // .end local v0 # "message":Landroid/os/Message;
} // :cond_1
return;
/* .line 343 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void reportRotationChanged ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .param p2, "rotation" # I */
/* .line 331 */
/* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 334 */
} // :cond_0
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
android.os.Message .obtain ( v0,v1 );
/* .line 335 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 336 */
/* iput p2, v0, Landroid/os/Message;->arg2:I */
/* .line 337 */
/* new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
/* invoke-direct {v1}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>()V */
this.obj = v1;
/* .line 338 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 339 */
return;
/* .line 332 */
} // .end local v0 # "message":Landroid/os/Message;
} // :cond_1
} // :goto_0
return;
} // .end method
public void trackScreenData ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "wakefulness" # I */
/* .param p2, "details" # Ljava/lang/String; */
/* .line 443 */
/* sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z */
/* if-nez v0, :cond_0 */
/* .line 446 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;ILjava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 449 */
return;
/* .line 444 */
} // :cond_1
} // :goto_0
return;
} // .end method
