class com.android.server.wm.MiuiMultiWindowRecommendController$7 implements java.lang.Runnable {
	 /* .source "MiuiMultiWindowRecommendController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->addFreeFormRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendController this$0; //synthetic
final com.android.server.wm.RecommendDataEntry val$recommendDataEntry; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendController$7 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendController; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 242 */
this.this$0 = p1;
this.val$recommendDataEntry = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 246 */
final String v0 = "MiuiMultiWindowRecommendController"; // const-string v0, "MiuiMultiWindowRecommendController"
try { // :try_start_0
v1 = this.this$0;
v2 = this.val$recommendDataEntry;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmFreeFormRecommendDataEntry ( v1,v2 );
/* .line 247 */
v1 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmContext ( v1 );
android.view.LayoutInflater .from ( v2 );
/* const v3, 0x110c000f */
int v4 = 0; // const/4 v4, 0x0
(( android.view.LayoutInflater ) v2 ).inflate ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
/* check-cast v2, Lcom/android/server/wm/FreeFormRecommendLayout; */
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmFreeFormRecommendLayout ( v1,v2 );
/* .line 249 */
v1 = this.this$0;
v1 = this.mService;
/* .line 250 */
(( com.android.server.wm.WindowManagerService ) v1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v1 ).getInsetsStateController ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getInsetsStateController()Lcom/android/server/wm/InsetsStateController;
/* .line 249 */
int v2 = 0; // const/4 v2, 0x0
v1 = com.android.server.wm.MiuiMultiWindowRecommendController .getStatusBarHeight ( v1,v2 );
v3 = this.this$0;
v3 = this.mService;
/* .line 251 */
(( com.android.server.wm.WindowManagerService ) v3 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
v3 = this.mDisplayFrames;
v3 = com.android.server.wm.MiuiMultiWindowRecommendController .getDisplayCutoutHeight ( v3 );
/* .line 249 */
v1 = java.lang.Math .max ( v1,v3 );
/* .line 252 */
/* .local v1, "statusBarHeight":I */
v3 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v3 );
(( com.android.server.wm.FreeFormRecommendLayout ) v4 ).createLayoutParams ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/server/wm/FreeFormRecommendLayout;->createLayoutParams(I)Landroid/view/WindowManager$LayoutParams;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmLayoutParams ( v3,v4 );
/* .line 253 */
v3 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v3 );
(( com.android.server.wm.FreeFormRecommendLayout ) v4 ).getFreeFormIconContainer ( ); // invoke-virtual {v4}, Lcom/android/server/wm/FreeFormRecommendLayout;->getFreeFormIconContainer()Landroid/widget/RelativeLayout;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmFreeFormRecommendIconContainer ( v3,v4 );
/* .line 254 */
v3 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v3 );
v4 = this.val$recommendDataEntry;
(( com.android.server.wm.RecommendDataEntry ) v4 ).getFreeformPackageName ( ); // invoke-virtual {v4}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;
v5 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmContext ( v5 );
v6 = this.val$recommendDataEntry;
/* .line 255 */
v6 = (( com.android.server.wm.RecommendDataEntry ) v6 ).getFreeformUserId ( ); // invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformUserId()I
/* .line 254 */
com.android.server.wm.MiuiMultiWindowRecommendController .loadDrawableByPackageName ( v4,v5,v6 );
(( com.android.server.wm.FreeFormRecommendLayout ) v3 ).setFreeFormIcon ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/FreeFormRecommendLayout;->setFreeFormIcon(Landroid/graphics/drawable/Drawable;)V
/* .line 256 */
v3 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v3 );
v4 = this.this$0;
v4 = this.freeFormClickListener;
(( com.android.server.wm.FreeFormRecommendLayout ) v3 ).setOnClickListener ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/FreeFormRecommendLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V
/* .line 257 */
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Landroid/view/View; */
v5 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v5 );
/* aput-object v5, v4, v2 */
miuix.animation.Folme .useAt ( v4 );
v5 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v5 );
/* new-array v2, v2, [Lmiuix/animation/base/AnimConfig; */
/* .line 258 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v4 = 2; // const/4 v4, 0x2
/* if-ge v2, v4, :cond_2 */
/* .line 259 */
v4 = this.this$0;
v4 = this.mWindowManager;
v5 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v5 );
v6 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmLayoutParams ( v6 );
/* .line 260 */
android.view.inspector.WindowInspector .getGlobalWindowViews ( );
v5 = this.this$0;
v4 = com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v5 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 261 */
v4 = this.this$0;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v4 ).setFreeFormRecommendState ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setFreeFormRecommendState(Z)V
/* .line 262 */
v4 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v4 );
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v4 ).startMultiWindowRecommendAnimation ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->startMultiWindowRecommendAnimation(Landroid/view/View;Z)V
/* .line 263 */
v3 = this.this$0;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v3 ).removeFreeFormRecommendViewForTimer ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendViewForTimer()V
/* .line 264 */
v3 = this.this$0;
v3 = this.mRecommendHelper;
v3 = this.mFreeFormManagerService;
v3 = this.mFreeFormGestureController;
v3 = this.mTrackManager;
if ( v3 != null) { // if-eqz v3, :cond_0
	 /* .line 265 */
	 v3 = this.this$0;
	 v3 = this.mRecommendHelper;
	 v3 = this.mFreeFormManagerService;
	 v3 = this.mFreeFormGestureController;
	 v3 = this.mTrackManager;
	 v4 = this.val$recommendDataEntry;
	 /* .line 266 */
	 (( com.android.server.wm.RecommendDataEntry ) v4 ).getFreeformPackageName ( ); // invoke-virtual {v4}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;
	 v5 = this.this$0;
	 v6 = this.val$recommendDataEntry;
	 /* .line 267 */
	 (( com.android.server.wm.RecommendDataEntry ) v6 ).getFreeformPackageName ( ); // invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;
	 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v5 ).getApplicationName ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;
	 /* .line 266 */
	 (( com.android.server.wm.MiuiFreeformTrackManager ) v3 ).trackFreeFormRecommendExposeEvent ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/wm/MiuiFreeformTrackManager;->trackFreeFormRecommendExposeEvent(Ljava/lang/String;Ljava/lang/String;)V
	 /* .line 269 */
} // :cond_0
return;
/* .line 258 */
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 272 */
} // .end local v2 # "i":I
} // :cond_2
final String v2 = " addFreeFormRecommendView twice fail"; // const-string v2, " addFreeFormRecommendView twice fail"
android.util.Slog .d ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 276 */
/* nop */
} // .end local v1 # "statusBarHeight":I
/* .line 273 */
/* :catch_0 */
/* move-exception v1 */
/* .line 274 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = this.this$0;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v2 ).removeFreeFormRecommendViewForTimer ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendViewForTimer()V
/* .line 275 */
final String v2 = " addFreeFormRecommendView fail"; // const-string v2, " addFreeFormRecommendView fail"
android.util.Slog .d ( v0,v2,v1 );
/* .line 277 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
