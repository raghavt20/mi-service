.class final Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiuiFreeFormGestureController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiFreeFormGestureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FreeFormReceiver"
.end annotation


# instance fields
.field mFilter:Landroid/content/IntentFilter;

.field final synthetic this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MiuiFreeFormGestureController;)V
    .locals 1

    .line 138
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;->this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 136
    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;->mFilter:Landroid/content/IntentFilter;

    .line 139
    const-string v0, "android.intent.action.USER_SWITCHED"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    iget-object p1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;->mFilter:Landroid/content/IntentFilter;

    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 141
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 146
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;->this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->updateDataFromSetting()V

    .line 149
    const-string v1, "MiuiFreeFormGestureController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "update data from Setting for user switch new userId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "android.intent.extra.user_handle"

    .line 150
    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 149
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :cond_0
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 153
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;->this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    if-eqz v1, :cond_1

    .line 154
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;->this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->bindOneTrackService()V

    goto :goto_0

    .line 156
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;->this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;

    new-instance v2, Lcom/android/server/wm/MiuiFreeformTrackManager;

    iget-object v3, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;->this$0:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-direct {v2, v3, v4}, Lcom/android/server/wm/MiuiFreeformTrackManager;-><init>(Landroid/content/Context;Lcom/android/server/wm/MiuiFreeFormGestureController;)V

    iput-object v2, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v0    # "action":Ljava/lang/String;
    :cond_2
    :goto_0
    goto :goto_1

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 162
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
