public class com.android.server.wm.AccountHelper {
	 /* .source "AccountHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/AccountHelper$AccountCallback;, */
	 /* Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static Boolean DEBUG;
private static final Integer LISTEN_MODE_ACCOUNT;
private static final Integer LISTEN_MODE_NONE;
private static final Integer LISTEN_MODE_WIFI;
private static final java.lang.String TAG;
private static final java.lang.String mAccountLoginActivity;
private static final android.content.ComponentName mCTAActivity;
private static com.android.server.wm.AccountHelper$AccountCallback mCallBack;
private static android.content.Context mContext;
private static final android.content.ComponentName mGrantPermissionsActivity;
private static Boolean mInIMEIWhiteList;
private static Boolean mListeningActivity;
private static final java.lang.String mNotificationActivity;
private static final java.lang.String mPermissionActivity;
private static final java.lang.String mWifiSettingActivity;
private static java.util.ArrayList sAccessActivities;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Landroid/content/ComponentName;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.ArrayList sAccessPackage;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static volatile com.android.server.wm.AccountHelper sAccountHelper;
/* # instance fields */
android.app.IMiuiActivityObserver mActivityStateObserver;
private Integer mListenMode;
/* # direct methods */
static Integer -$$Nest$fgetmListenMode ( com.android.server.wm.AccountHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I */
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/wm/AccountHelper;->DEBUG:Z */
} // .end method
static com.android.server.wm.AccountHelper$AccountCallback -$$Nest$sfgetmCallBack ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.AccountHelper.mCallBack;
} // .end method
static android.content.Context -$$Nest$sfgetmContext ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.AccountHelper.mContext;
} // .end method
static java.util.ArrayList -$$Nest$sfgetsAccessActivities ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.AccountHelper.sAccessActivities;
} // .end method
static java.util.ArrayList -$$Nest$sfgetsAccessPackage ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.AccountHelper.sAccessPackage;
} // .end method
static com.android.server.wm.AccountHelper ( ) {
/* .locals 8 */
/* .line 29 */
int v0 = 0; // const/4 v0, 0x0
/* .line 31 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.wm.AccountHelper.mListeningActivity = (v0!= 0);
/* .line 32 */
com.android.server.wm.AccountHelper.mInIMEIWhiteList = (v0!= 0);
/* .line 33 */
int v0 = 1; // const/4 v0, 0x1
com.android.server.wm.AccountHelper.DEBUG = (v0!= 0);
/* .line 35 */
/* new-instance v0, Ljava/lang/String; */
final String v1 = "com.xiaomi.account"; // const-string v1, "com.xiaomi.account"
/* invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V */
/* .line 36 */
/* new-instance v1, Ljava/lang/String; */
final String v2 = "com.xiaomi.passport"; // const-string v2, "com.xiaomi.passport"
/* invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V */
/* .line 38 */
/* new-instance v2, Ljava/lang/String; */
final String v3 = "com.android.settings"; // const-string v3, "com.android.settings"
/* invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V */
/* .line 40 */
/* new-instance v3, Ljava/lang/String; */
final String v4 = "com.google.android.permissioncontroller"; // const-string v4, "com.google.android.permissioncontroller"
/* invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V */
/* .line 43 */
/* new-instance v4, Landroid/content/ComponentName; */
final String v5 = "com.miui.securitycenter"; // const-string v5, "com.miui.securitycenter"
final String v6 = "com.miui.permcenter.permissions.SystemAppPermissionDialogActivity"; // const-string v6, "com.miui.permcenter.permissions.SystemAppPermissionDialogActivity"
/* invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 46 */
/* new-instance v5, Landroid/content/ComponentName; */
final String v6 = "com.lbe.security.miui"; // const-string v6, "com.lbe.security.miui"
final String v7 = "com.android.packageinstaller.permission.ui.GrantPermissionsActivity"; // const-string v7, "com.android.packageinstaller.permission.ui.GrantPermissionsActivity"
/* invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 53 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
/* .line 54 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
/* .line 60 */
v6 = com.android.server.wm.AccountHelper.sAccessPackage;
(( java.util.ArrayList ) v6 ).add ( v0 ); // invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 61 */
v0 = com.android.server.wm.AccountHelper.sAccessPackage;
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 62 */
v0 = com.android.server.wm.AccountHelper.sAccessPackage;
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 63 */
v0 = com.android.server.wm.AccountHelper.sAccessPackage;
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 64 */
v0 = com.android.server.wm.AccountHelper.sAccessActivities;
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 65 */
v0 = com.android.server.wm.AccountHelper.sAccessActivities;
(( java.util.ArrayList ) v0 ).add ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 66 */
return;
} // .end method
private com.android.server.wm.AccountHelper ( ) {
/* .locals 1 */
/* .line 56 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 48 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I */
/* .line 216 */
/* new-instance v0, Lcom/android/server/wm/AccountHelper$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/AccountHelper$2;-><init>(Lcom/android/server/wm/AccountHelper;)V */
this.mActivityStateObserver = v0;
/* .line 57 */
return;
} // .end method
public static com.android.server.wm.AccountHelper getInstance ( ) {
/* .locals 2 */
/* .line 69 */
v0 = com.android.server.wm.AccountHelper.sAccountHelper;
/* if-nez v0, :cond_1 */
/* .line 70 */
/* const-class v0, Lcom/android/server/wm/AccountHelper; */
/* monitor-enter v0 */
/* .line 71 */
try { // :try_start_0
v1 = com.android.server.wm.AccountHelper.sAccountHelper;
/* if-nez v1, :cond_0 */
/* .line 72 */
/* new-instance v1, Lcom/android/server/wm/AccountHelper; */
/* invoke-direct {v1}, Lcom/android/server/wm/AccountHelper;-><init>()V */
/* .line 74 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 76 */
} // :cond_1
} // :goto_0
v0 = com.android.server.wm.AccountHelper.sAccountHelper;
} // .end method
/* # virtual methods */
public void ListenAccount ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .line 143 */
(( com.android.server.wm.AccountHelper ) p0 ).registerAccountActivityObserver ( ); // invoke-virtual {p0}, Lcom/android/server/wm/AccountHelper;->registerAccountActivityObserver()V
/* .line 144 */
/* iget v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I */
/* or-int/2addr v0, p1 */
/* iput v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I */
/* .line 145 */
/* sget-boolean v0, Lcom/android/server/wm/AccountHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 146 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ListenAccount mode: "; // const-string v1, "ListenAccount mode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " mListenMode: "; // const-string v1, " mListenMode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPermision"; // const-string v1, "MiuiPermision"
android.util.Log .i ( v1,v0 );
/* .line 148 */
} // :cond_0
return;
} // .end method
public void UnListenAccount ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .line 151 */
/* iget v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I */
/* xor-int/2addr v0, p1 */
/* iput v0, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I */
/* .line 152 */
/* if-nez v0, :cond_0 */
/* .line 153 */
(( com.android.server.wm.AccountHelper ) p0 ).unRegisterAccountActivityObserver ( ); // invoke-virtual {p0}, Lcom/android/server/wm/AccountHelper;->unRegisterAccountActivityObserver()V
/* .line 155 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/wm/AccountHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 156 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "UnListenAccount mode: "; // const-string v1, "UnListenAccount mode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " mListenMode: "; // const-string v1, " mListenMode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/AccountHelper;->mListenMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPermision"; // const-string v1, "MiuiPermision"
android.util.Log .i ( v1,v0 );
/* .line 158 */
} // :cond_1
return;
} // .end method
public void addAccount ( android.content.Context p0 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 189 */
/* sget-boolean v0, Lcom/android/server/wm/AccountHelper;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 190 */
final String v0 = "MiuiPermision"; // const-string v0, "MiuiPermision"
final String v1 = "addAccount"; // const-string v1, "addAccount"
android.util.Log .i ( v0,v1 );
/* .line 192 */
} // :cond_0
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 193 */
/* .local v0, "bundle":Landroid/os/Bundle; */
android.accounts.AccountManager .get ( p1 );
final String v2 = "com.xiaomi"; // const-string v2, "com.xiaomi"
final String v3 = "passportapi"; // const-string v3, "passportapi"
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
/* new-instance v7, Lcom/android/server/wm/AccountHelper$1; */
/* invoke-direct {v7, p0, p1}, Lcom/android/server/wm/AccountHelper$1;-><init>(Lcom/android/server/wm/AccountHelper;Landroid/content/Context;)V */
int v8 = 0; // const/4 v8, 0x0
/* .line 194 */
/* invoke-virtual/range {v1 ..v8}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture; */
/* .line 214 */
return;
} // .end method
public android.accounts.Account getXiaomiAccount ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 90 */
int v0 = 0; // const/4 v0, 0x0
/* .line 91 */
/* .local v0, "account":Landroid/accounts/Account; */
android.accounts.AccountManager .get ( p1 );
/* .line 92 */
/* .local v1, "am":Landroid/accounts/AccountManager; */
final String v2 = "com.xiaomi"; // const-string v2, "com.xiaomi"
(( android.accounts.AccountManager ) v1 ).getAccountsByType ( v2 ); // invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;
/* .line 93 */
/* .local v2, "accounts":[Landroid/accounts/Account; */
/* array-length v3, v2 */
/* if-lez v3, :cond_0 */
/* .line 94 */
int v3 = 0; // const/4 v3, 0x0
/* aget-object v0, v2, v3 */
/* .line 96 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 97 */
final String v3 = "MiuiPermision"; // const-string v3, "MiuiPermision"
/* const-string/jumbo v4, "xiaomi account is null" */
android.util.Log .i ( v3,v4 );
/* .line 100 */
} // :cond_1
} // .end method
public void onXiaomiAccountLogin ( android.content.Context p0, android.accounts.Account p1 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "account" # Landroid/accounts/Account; */
/* .line 105 */
v0 = com.android.server.wm.AccountHelper.mCallBack;
/* .line 106 */
return;
} // .end method
public void onXiaomiAccountLogout ( android.content.Context p0, android.accounts.Account p1 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "account" # Landroid/accounts/Account; */
/* .line 110 */
v0 = com.android.server.wm.AccountHelper.mCallBack;
/* .line 111 */
return;
} // .end method
public void registerAccountActivityObserver ( ) {
/* .locals 4 */
/* .line 161 */
/* sget-boolean v0, Lcom/android/server/wm/AccountHelper;->mListeningActivity:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 162 */
return;
/* .line 163 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
com.android.server.wm.AccountHelper.mListeningActivity = (v0!= 0);
/* .line 164 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 165 */
/* .local v0, "intent":Landroid/content/Intent; */
android.app.ActivityManager .getService ( );
/* .line 166 */
/* .local v1, "activityManager":Landroid/app/IActivityManager; */
/* if-nez v1, :cond_1 */
/* .line 167 */
return;
/* .line 170 */
} // :cond_1
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v2 ).getMiuiActivityController ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMiuiActivityController()Lcom/android/server/wm/MiuiActivityController;
v3 = this.mActivityStateObserver;
/* .line 171 */
/* .line 172 */
return;
} // .end method
public void registerAccountListener ( android.content.Context p0, com.android.server.wm.AccountHelper$AccountCallback p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "callBack" # Lcom/android/server/wm/AccountHelper$AccountCallback; */
/* .line 83 */
/* .line 84 */
/* .line 85 */
/* new-instance v0, Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;-><init>(Lcom/android/server/wm/AccountHelper;Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver-IA;)V */
/* new-instance v1, Landroid/content/IntentFilter; */
final String v2 = "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"; // const-string v2, "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"
/* invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) p1 ).registerReceiver ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 87 */
return;
} // .end method
public void unRegisterAccountActivityObserver ( ) {
/* .locals 4 */
/* .line 175 */
/* sget-boolean v0, Lcom/android/server/wm/AccountHelper;->mListeningActivity:Z */
/* if-nez v0, :cond_0 */
/* .line 176 */
return;
/* .line 177 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
com.android.server.wm.AccountHelper.mListeningActivity = (v0!= 0);
/* .line 178 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 179 */
/* .local v0, "intent":Landroid/content/Intent; */
android.app.ActivityManager .getService ( );
/* .line 180 */
/* .local v1, "activityManager":Landroid/app/IActivityManager; */
/* if-nez v1, :cond_1 */
/* .line 181 */
return;
/* .line 184 */
} // :cond_1
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v2 ).getMiuiActivityController ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMiuiActivityController()Lcom/android/server/wm/MiuiActivityController;
v3 = this.mActivityStateObserver;
/* .line 185 */
/* .line 186 */
return;
} // .end method
