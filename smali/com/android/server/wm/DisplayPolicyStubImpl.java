public class com.android.server.wm.DisplayPolicyStubImpl implements com.android.server.wm.DisplayPolicyStub {
	 /* .source "DisplayPolicyStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String CARWITH_PACKAGE_NAME;
	 /* # instance fields */
	 private android.view.InsetsFrameProvider mInsetsFrameProvider;
	 /* # direct methods */
	 public com.android.server.wm.DisplayPolicyStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 31 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private java.lang.String getDisplayName ( com.android.server.wm.DisplayContent p0 ) {
		 /* .locals 1 */
		 /* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
		 /* .line 98 */
		 v0 = this.mDisplayInfo;
		 v0 = this.name;
	 } // .end method
	 /* # virtual methods */
	 public android.view.InsetsFrameProvider createCarwithInsetsFrameProvider ( com.android.server.wm.DisplayContent p0, android.view.WindowManager$LayoutParams p1 ) {
		 /* .locals 5 */
		 /* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
		 /* .param p2, "attrs" # Landroid/view/WindowManager$LayoutParams; */
		 /* .line 86 */
		 /* new-instance v0, Landroid/os/Binder; */
		 /* invoke-direct {v0}, Landroid/os/Binder;-><init>()V */
		 /* .line 87 */
		 /* .local v0, "mInsetsSourceOwner":Landroid/os/Binder; */
		 /* new-instance v1, Landroid/view/InsetsFrameProvider; */
		 v2 = 		 android.view.WindowInsets$Type .navigationBars ( );
		 int v3 = 0; // const/4 v3, 0x0
		 /* invoke-direct {v1, v0, v3, v2}, Landroid/view/InsetsFrameProvider;-><init>(Ljava/lang/Object;II)V */
		 this.mInsetsFrameProvider = v1;
		 /* .line 88 */
		 (( com.android.server.wm.DisplayContent ) p1 ).getDisplayInfo ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;
		 /* .line 89 */
		 /* .local v1, "di":Landroid/view/DisplayInfo; */
		 /* iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I */
		 /* iget v4, v1, Landroid/view/DisplayInfo;->logicalHeight:I */
		 /* if-le v2, v4, :cond_0 */
		 /* .line 90 */
		 v2 = this.mInsetsFrameProvider;
		 /* iget v4, p2, Landroid/view/WindowManager$LayoutParams;->width:I */
		 android.graphics.Insets .of ( v4,v3,v3,v3 );
		 (( android.view.InsetsFrameProvider ) v2 ).setInsetsSize ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/InsetsFrameProvider;->setInsetsSize(Landroid/graphics/Insets;)Landroid/view/InsetsFrameProvider;
		 /* .line 92 */
	 } // :cond_0
	 v2 = this.mInsetsFrameProvider;
	 /* iget v4, p2, Landroid/view/WindowManager$LayoutParams;->height:I */
	 android.graphics.Insets .of ( v3,v3,v3,v4 );
	 (( android.view.InsetsFrameProvider ) v2 ).setInsetsSize ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/InsetsFrameProvider;->setInsetsSize(Landroid/graphics/Insets;)Landroid/view/InsetsFrameProvider;
	 /* .line 94 */
} // :goto_0
v2 = this.mInsetsFrameProvider;
} // .end method
public Integer getExtraNavigationBarAppearance ( com.android.server.wm.WindowState p0, com.android.server.wm.WindowState p1 ) {
/* .locals 3 */
/* .param p1, "winCandidate" # Lcom/android/server/wm/WindowState; */
/* .param p2, "navColorWin" # Lcom/android/server/wm/WindowState; */
/* .line 40 */
int v0 = 0; // const/4 v0, 0x0
/* .line 41 */
/* .local v0, "appearance":I */
if ( p2 != null) { // if-eqz p2, :cond_0
	 /* .line 42 */
	 (( com.android.server.wm.WindowState ) p2 ).getAttrs ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
	 /* iget v1, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
	 /* const/high16 v2, 0x100000 */
	 /* and-int/2addr v1, v2 */
	 /* if-ne v1, v2, :cond_0 */
	 /* .line 44 */
	 /* or-int/lit8 v0, v0, 0x10 */
	 /* .line 47 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
	 /* .line 48 */
	 (( com.android.server.wm.WindowState ) p1 ).getAttrs ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
	 /* iget v1, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
	 /* const v2, 0x8000 */
	 /* and-int/2addr v1, v2 */
	 /* if-ne v1, v2, :cond_1 */
	 /* .line 50 */
	 /* or-int/lit16 v0, v0, 0x800 */
	 /* .line 52 */
} // :cond_1
} // .end method
public Boolean isCarWithDisplay ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 2 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 68 */
final String v0 = "com.miui.carlink"; // const-string v0, "com.miui.carlink"
/* invoke-direct {p0, p1}, Lcom/android/server/wm/DisplayPolicyStubImpl;->getDisplayName(Lcom/android/server/wm/DisplayContent;)Ljava/lang/String; */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isMiuiVersion ( ) {
/* .locals 1 */
/* .line 57 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void notifyOnScroll ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "start" # Z */
/* .line 62 */
com.miui.whetstone.client.WhetstoneClientManager .notifyOnScroll ( p1 );
/* .line 63 */
return;
} // .end method
public void setCarWithWindowContainer ( com.android.server.wm.DisplayContent p0, com.android.server.wm.WindowState p1, com.android.server.wm.DisplayPolicyStub$InsertActionInterface p2 ) {
/* .locals 1 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "win" # Lcom/android/server/wm/WindowState; */
/* .param p3, "insertActionInterface" # Lcom/android/server/wm/DisplayPolicyStub$InsertActionInterface; */
/* .line 74 */
v0 = this.mInsetsFrameProvider;
/* if-nez v0, :cond_0 */
/* .line 75 */
v0 = this.mAttrs;
(( com.android.server.wm.DisplayPolicyStubImpl ) p0 ).createCarwithInsetsFrameProvider ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/DisplayPolicyStubImpl;->createCarwithInsetsFrameProvider(Lcom/android/server/wm/DisplayContent;Landroid/view/WindowManager$LayoutParams;)Landroid/view/InsetsFrameProvider;
this.mInsetsFrameProvider = v0;
/* .line 78 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 79 */
v0 = this.mInsetsFrameProvider;
v0 = (( android.view.InsetsFrameProvider ) v0 ).getId ( ); // invoke-virtual {v0}, Landroid/view/InsetsFrameProvider;->getId()I
/* .line 81 */
} // :cond_1
return;
} // .end method
