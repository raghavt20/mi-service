public class com.android.server.wm.BoundsCompatController extends com.android.server.wm.BoundsCompatControllerStub {
	 /* .source "BoundsCompatController.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.wm.BoundsCompatControllerStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/wm/BoundsCompatController$AspectRatioPolicy; */
/* } */
} // .end annotation
/* # instance fields */
private android.graphics.Rect mBounds;
private Boolean mBoundsCompatEnabled;
private Float mCurrentScale;
private Boolean mDispatchNeeded;
private android.graphics.Rect mDispatchedBounds;
private Integer mDispatchedState;
private Float mFixedAspectRatio;
private Boolean mFixedAspectRatioAvailable;
private Float mGlobalScale;
private Integer mGravity;
private com.android.server.wm.ActivityRecord mOwner;
private Integer mState;
/* # direct methods */
public com.android.server.wm.BoundsCompatController ( ) {
/* .locals 1 */
/* .line 47 */
/* invoke-direct {p0}, Lcom/android/server/wm/BoundsCompatControllerStub;-><init>()V */
/* .line 56 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F */
/* .line 66 */
/* iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F */
/* .line 67 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
return;
} // .end method
private void applyPolicyIfNeeded ( android.content.pm.ActivityInfo p0 ) {
/* .locals 4 */
/* .param p1, "info" # Landroid/content/pm/ActivityInfo; */
/* .line 98 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z */
/* .line 99 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* .line 100 */
/* .local v0, "fixedAspectRatio":F */
v1 = this.mOwner;
v1 = this.mAtmService;
v1 = this.mWindowManager;
v1 = v1 = this.mPolicy;
/* if-nez v1, :cond_0 */
/* .line 101 */
v1 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 102 */
} // :cond_0
v1 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v1 != null) { // if-eqz v1, :cond_4
	 /* .line 103 */
} // :cond_1
v1 = this.mOwner;
(( com.android.server.wm.ActivityRecord ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* .line 104 */
/* .local v1, "task":Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_4
	 v2 = this.realActivity;
	 if ( v2 != null) { // if-eqz v2, :cond_4
		 v2 = 		 (( com.android.server.wm.Task ) v1 ).inMultiWindowMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z
		 /* if-nez v2, :cond_4 */
		 /* .line 105 */
		 v2 = 		 android.util.MiuiAppSizeCompatModeStub .get ( );
		 if ( v2 != null) { // if-eqz v2, :cond_3
			 /* .line 106 */
			 v2 = 			 /* invoke-direct {p0}, Lcom/android/server/wm/BoundsCompatController;->getActivityFlipCompatMode()I */
			 /* if-nez v2, :cond_2 */
			 /* .line 107 */
			 int v0 = 0; // const/4 v0, 0x0
			 /* .line 109 */
		 } // :cond_2
		 /* const v0, 0x3fdc3e06 */
		 /* .line 112 */
	 } // :cond_3
	 com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
	 v3 = this.packageName;
	 v0 = 	 (( com.android.server.wm.ActivityTaskManagerServiceStub ) v2 ).getAspectRatio ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getAspectRatio(Ljava/lang/String;)F
	 /* .line 114 */
} // :goto_0
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v3 = this.packageName;
v2 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v2 ).getAspectGravity ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getAspectGravity(Ljava/lang/String;)I
/* iput v2, p0, Lcom/android/server/wm/BoundsCompatController;->mGravity:I */
/* .line 118 */
} // .end local v1 # "task":Lcom/android/server/wm/Task;
} // :cond_4
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F */
v1 = java.lang.Float .compare ( v1,v0 );
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 119 */
/* iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F */
/* .line 122 */
} // :cond_5
/* const/high16 v1, -0x40800000 # -1.0f */
/* cmpl-float v1, v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 123 */
v1 = com.android.server.wm.ActivityTaskManagerServiceStub .hasDefinedAspectRatio ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 124 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z */
/* .line 126 */
} // :cond_6
return;
/* .line 128 */
} // :cond_7
return;
} // .end method
private java.lang.String aspectRatioPolicyToString ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "policy" # I */
/* .line 187 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 193 */
java.lang.Integer .toString ( p1 );
/* .line 191 */
/* :pswitch_0 */
final String v0 = "FullScreen"; // const-string v0, "FullScreen"
/* .line 189 */
/* :pswitch_1 */
final String v0 = "Default"; // const-string v0, "Default"
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean canUseFixedAspectRatio ( android.content.res.Configuration p0 ) {
/* .locals 2 */
/* .param p1, "parentConfiguration" # Landroid/content/res/Configuration; */
/* .line 543 */
/* iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 544 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
/* if-nez v0, :cond_0 */
v0 = this.mOwner;
/* .line 545 */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).getUid ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getUid()I
/* const/16 v1, 0x2710 */
/* if-lt v0, v1, :cond_3 */
/* .line 546 */
} // :cond_0
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 547 */
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v1 = this.mOwner;
v0 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v0 ).shouldNotApplyAspectRatio ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->shouldNotApplyAspectRatio(Lcom/android/server/wm/ActivityRecord;)Z
/* if-nez v0, :cond_3 */
/* .line 548 */
} // :cond_1
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 549 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/BoundsCompatController;->getActivityFlipCompatMode()I */
/* if-nez v0, :cond_2 */
/* .line 553 */
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
/* .line 551 */
} // :cond_3
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Integer getActivityFlipCompatMode ( ) {
/* .locals 4 */
/* .line 569 */
com.android.server.wm.BoundsCompatUtils .getInstance ( );
v1 = this.mOwner;
v0 = (( com.android.server.wm.BoundsCompatUtils ) v0 ).getFlipCompatModeByActivity ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByActivity(Lcom/android/server/wm/ActivityRecord;)I
/* .line 570 */
/* .local v0, "activityMode":I */
com.android.server.wm.BoundsCompatUtils .getInstance ( );
v2 = this.mOwner;
v2 = this.mAtmService;
v3 = this.mOwner;
v3 = this.packageName;
/* .line 571 */
v1 = (( com.android.server.wm.BoundsCompatUtils ) v1 ).getFlipCompatModeByApp ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I
/* .line 573 */
/* .local v1, "appMode":I */
int v2 = -1; // const/4 v2, -0x1
/* if-eq v0, v2, :cond_0 */
/* .line 574 */
/* .line 575 */
} // :cond_0
/* if-eq v1, v2, :cond_1 */
/* .line 576 */
/* .line 578 */
} // :cond_1
} // .end method
private Integer getRequestedConfigurationOrientation ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 8 */
/* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .line 482 */
/* iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I */
/* iget v1, p1, Lcom/android/server/wm/ActivityRecord;->mOriginScreenOrientation:I */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* if-eq v0, v1, :cond_1 */
/* iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I */
int v1 = -2; // const/4 v1, -0x2
/* if-eq v0, v1, :cond_0 */
} // :cond_0
/* move v0, v2 */
} // :cond_1
} // :goto_0
/* move v0, v3 */
/* .line 486 */
/* .local v0, "useOriginRequestOrientation":Z */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 487 */
/* iget v1, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I */
/* .line 488 */
} // :cond_2
/* iget v1, p1, Lcom/android/server/wm/ActivityRecord;->mOriginScreenOrientation:I */
} // :goto_2
/* nop */
/* .line 490 */
/* .local v1, "requestedOrientation":I */
int v4 = 5; // const/4 v4, 0x5
/* if-ne v1, v4, :cond_3 */
/* .line 492 */
v3 = this.mDisplayContent;
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 493 */
v2 = this.mDisplayContent;
v2 = (( com.android.server.wm.DisplayContent ) v2 ).getNaturalOrientation ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getNaturalOrientation()I
/* .line 495 */
} // :cond_3
/* const/16 v4, 0xe */
/* if-ne v1, v4, :cond_4 */
/* .line 497 */
(( com.android.server.wm.ActivityRecord ) p1 ).getConfiguration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getConfiguration()Landroid/content/res/Configuration;
/* iget v2, v2, Landroid/content/res/Configuration;->orientation:I */
/* .line 498 */
} // :cond_4
v4 = android.content.pm.ActivityInfo .isFixedOrientationLandscape ( v1 );
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 499 */
int v2 = 2; // const/4 v2, 0x2
/* .line 500 */
} // :cond_5
v4 = android.content.pm.ActivityInfo .isFixedOrientationPortrait ( v1 );
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 501 */
/* .line 502 */
} // :cond_6
int v3 = 4; // const/4 v3, 0x4
/* if-ne v1, v3, :cond_9 */
v3 = this.mDisplayContent;
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 505 */
v3 = this.mDisplayContent;
(( com.android.server.wm.DisplayContent ) v3 ).getDisplayRotation ( ); // invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplayRotation()Lcom/android/server/wm/DisplayRotation;
/* .line 507 */
/* .local v3, "displayRotation":Lcom/android/server/wm/DisplayRotation; */
if ( v3 != null) { // if-eqz v3, :cond_7
(( com.android.server.wm.DisplayRotation ) v3 ).getOrientationListener ( ); // invoke-virtual {v3}, Lcom/android/server/wm/DisplayRotation;->getOrientationListener()Lcom/android/server/wm/WindowOrientationListener;
} // :cond_7
int v4 = 0; // const/4 v4, 0x0
/* .line 509 */
/* .local v4, "listener":Lcom/android/server/wm/WindowOrientationListener; */
} // :goto_3
int v5 = -1; // const/4 v5, -0x1
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 510 */
v6 = (( com.android.server.wm.WindowOrientationListener ) v4 ).getProposedRotation ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowOrientationListener;->getProposedRotation()I
/* .line 511 */
} // :cond_8
/* move v6, v5 */
} // :goto_4
/* nop */
/* .line 513 */
/* .local v6, "sensorRotation":I */
v7 = /* invoke-direct {p0, v6}, Lcom/android/server/wm/BoundsCompatController;->rotationToOrientation(I)I */
/* .line 514 */
/* .local v7, "sensorOrientation":I */
/* if-eq v7, v5, :cond_9 */
/* .line 515 */
/* .line 518 */
} // .end local v3 # "displayRotation":Lcom/android/server/wm/DisplayRotation;
} // .end local v4 # "listener":Lcom/android/server/wm/WindowOrientationListener;
} // .end local v6 # "sensorRotation":I
} // .end local v7 # "sensorOrientation":I
} // :cond_9
} // .end method
static void lambda$clearSizeCompatMode$0 ( com.android.server.wm.ActivityRecord p0, Boolean p1, com.android.server.wm.ActivityRecord p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "excluded" # Lcom/android/server/wm/ActivityRecord; */
/* .param p1, "isClearConfig" # Z */
/* .param p2, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .line 78 */
/* if-ne p2, p0, :cond_0 */
/* .line 79 */
return;
/* .line 81 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 82 */
(( com.android.server.wm.ActivityRecord ) p2 ).clearCompatDisplayInsets ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->clearCompatDisplayInsets()V
/* .line 83 */
return;
/* .line 85 */
} // :cond_1
(( com.android.server.wm.ActivityRecord ) p2 ).clearSizeCompatMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->clearSizeCompatMode()V
/* .line 86 */
v0 = (( com.android.server.wm.ActivityRecord ) p2 ).attachedToProcess ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->attachedToProcess()Z
/* if-nez v0, :cond_2 */
/* .line 87 */
return;
/* .line 90 */
} // :cond_2
try { // :try_start_0
v0 = this.mBoundsCompatControllerStub;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.BoundsCompatControllerStub ) v0 ).interceptScheduleTransactionIfNeeded ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/BoundsCompatControllerStub;->interceptScheduleTransactionIfNeeded(Landroid/app/servertransaction/ClientTransactionItem;)Z
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 92 */
/* .line 91 */
/* :catch_0 */
/* move-exception v0 */
/* .line 93 */
} // :goto_0
return;
} // .end method
private Integer rotationToOrientation ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "rotation" # I */
/* .line 528 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 538 */
int v0 = -1; // const/4 v0, -0x1
/* .line 535 */
/* :pswitch_0 */
int v0 = 2; // const/4 v0, 0x2
/* .line 531 */
/* :pswitch_1 */
int v0 = 1; // const/4 v0, 0x1
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean taskContainsActivityRecord ( com.android.server.wm.Task p0 ) {
/* .locals 6 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 325 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "j":I */
} // :goto_0
v1 = (( com.android.server.wm.Task ) p1 ).getChildCount ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I
int v2 = 0; // const/4 v2, 0x0
/* if-ge v0, v1, :cond_6 */
/* .line 326 */
v1 = (( com.android.server.wm.Task ) p1 ).isLeafTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isLeafTask()Z
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 327 */
(( com.android.server.wm.Task ) p1 ).getChildAt ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v1 ).asActivityRecord ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
/* .line 328 */
/* .local v1, "ar":Lcom/android/server/wm/ActivityRecord; */
if ( v1 != null) { // if-eqz v1, :cond_2
v4 = (( com.android.server.wm.ActivityRecord ) v1 ).isVisible ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->isVisible()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 330 */
v4 = this.mOwner;
/* if-ne v1, v4, :cond_0 */
/* .line 331 */
/* .line 335 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) v4 ).getResolvedOverrideConfiguration ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;
/* iget v4, v4, Landroid/content/res/Configuration;->orientation:I */
/* .line 336 */
/* .local v4, "currentOrientation":I */
(( com.android.server.wm.ActivityRecord ) v1 ).getResolvedOverrideConfiguration ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;
/* iget v5, v5, Landroid/content/res/Configuration;->orientation:I */
/* .line 338 */
/* .local v5, "bottomOrientation":I */
if ( v4 != null) { // if-eqz v4, :cond_1
if ( v5 != null) { // if-eqz v5, :cond_1
/* if-eq v4, v5, :cond_1 */
/* .line 341 */
/* .line 344 */
} // :cond_1
/* .line 346 */
} // .end local v1 # "ar":Lcom/android/server/wm/ActivityRecord;
} // .end local v4 # "currentOrientation":I
} // .end local v5 # "bottomOrientation":I
} // :cond_2
/* .line 347 */
} // :cond_3
v1 = (( com.android.server.wm.Task ) p1 ).getChildCount ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I
/* sub-int/2addr v1, v3 */
/* .local v1, "i":I */
} // :goto_1
/* if-ltz v1, :cond_5 */
/* .line 348 */
(( com.android.server.wm.Task ) p1 ).getChildAt ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v2 ).asTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
/* .line 349 */
/* .local v2, "t":Lcom/android/server/wm/Task; */
if ( v2 != null) { // if-eqz v2, :cond_4
v3 = (( com.android.server.wm.Task ) p1 ).isVisible ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isVisible()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 350 */
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/wm/BoundsCompatController;->taskContainsActivityRecord(Lcom/android/server/wm/Task;)Z */
/* .line 347 */
} // .end local v2 # "t":Lcom/android/server/wm/Task;
} // :cond_4
/* add-int/lit8 v1, v1, -0x1 */
/* .line 325 */
} // .end local v1 # "i":I
} // :cond_5
} // :goto_2
/* add-int/lit8 v0, v0, 0x1 */
/* .line 355 */
} // .end local v0 # "j":I
} // :cond_6
} // .end method
/* # virtual methods */
Boolean adaptCompatConfiguration ( android.content.res.Configuration p0, android.content.res.Configuration p1, com.android.server.wm.DisplayContent p2 ) {
/* .locals 8 */
/* .param p1, "parentConfiguration" # Landroid/content/res/Configuration; */
/* .param p2, "resolvedConfig" # Landroid/content/res/Configuration; */
/* .param p3, "dc" # Lcom/android/server/wm/DisplayContent; */
/* .line 414 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/BoundsCompatController;->canUseFixedAspectRatio(Landroid/content/res/Configuration;)Z */
int v1 = 0; // const/4 v1, 0x0
/* const/high16 v2, 0x3f800000 # 1.0f */
/* const/high16 v3, -0x40800000 # -1.0f */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 415 */
v0 = this.mOwner;
(( com.android.server.wm.ActivityRecord ) v0 ).clearSizeCompatModeWithoutConfigChange ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->clearSizeCompatModeWithoutConfigChange()V
/* .line 417 */
v0 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v0 ).getAppBounds ( ); // invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getAppBounds()Landroid/graphics/Rect;
/* .line 419 */
/* .local v0, "parentAppBounds":Landroid/graphics/Rect; */
if ( p3 != null) { // if-eqz p3, :cond_7
v4 = this.mDisplayInfo;
/* if-nez v4, :cond_0 */
/* goto/16 :goto_1 */
/* .line 424 */
} // :cond_0
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F */
/* cmpl-float v1, v1, v3 */
/* if-nez v1, :cond_1 */
v1 = this.mDisplayInfo;
v1 = this.uniqueId;
v4 = this.mCurrentUniqueDisplayId;
/* .line 425 */
v1 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 426 */
com.android.server.wm.BoundsCompatUtils .getInstance ( );
v4 = this.mOwner;
v4 = this.info;
v4 = this.packageName;
/* .line 428 */
v5 = /* invoke-direct {p0}, Lcom/android/server/wm/BoundsCompatController;->getActivityFlipCompatMode()I */
v6 = this.windowConfiguration;
/* .line 429 */
(( android.app.WindowConfiguration ) v6 ).getBounds ( ); // invoke-virtual {v6}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* .line 427 */
v1 = (( com.android.server.wm.BoundsCompatUtils ) v1 ).getGlobalScaleByName ( v4, v5, v6 ); // invoke-virtual {v1, v4, v5, v6}, Lcom/android/server/wm/BoundsCompatUtils;->getGlobalScaleByName(Ljava/lang/String;ILandroid/graphics/Rect;)F
/* iput v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F */
/* .line 432 */
} // :cond_1
/* iget v1, p1, Landroid/content/res/Configuration;->orientation:I */
int v4 = 2; // const/4 v4, 0x2
/* if-eq v1, v4, :cond_2 */
/* .line 433 */
v1 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v1 != null) { // if-eqz v1, :cond_3
} // :cond_2
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F */
/* cmpl-float v3, v1, v3 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 435 */
/* iput v1, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
/* .line 437 */
} // :cond_3
/* iput v2, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
/* .line 440 */
} // :goto_0
v1 = this.windowConfiguration;
/* const/16 v2, 0x11 */
v1 = com.android.server.wm.BoundsCompatUtils .getCompatGravity ( v1,v2 );
/* iput v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGravity:I */
/* .line 443 */
android.app.servertransaction.BoundsCompat .getInstance ( );
/* iget v3, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F */
v1 = this.mOwner;
/* .line 445 */
v5 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/BoundsCompatController;->getRequestedConfigurationOrientation(Lcom/android/server/wm/ActivityRecord;)I */
/* iget v6, p0, Lcom/android/server/wm/BoundsCompatController;->mGravity:I */
/* iget v7, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
/* .line 443 */
/* move-object v4, p1 */
/* invoke-virtual/range {v2 ..v7}, Landroid/app/servertransaction/BoundsCompat;->computeCompatBounds(FLandroid/content/res/Configuration;IIF)Landroid/graphics/Rect; */
/* .line 447 */
/* .local v1, "compatBounds":Landroid/graphics/Rect; */
/* iget v2, p2, Landroid/content/res/Configuration;->densityDpi:I */
/* if-nez v2, :cond_4 */
/* .line 448 */
/* iget v2, p1, Landroid/content/res/Configuration;->densityDpi:I */
/* iput v2, p2, Landroid/content/res/Configuration;->densityDpi:I */
/* .line 450 */
} // :cond_4
v2 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v2 ).setBounds ( v1 ); // invoke-virtual {v2, v1}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V
/* .line 451 */
v2 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v2 ).setAppBounds ( v1 ); // invoke-virtual {v2, v1}, Landroid/app/WindowConfiguration;->setAppBounds(Landroid/graphics/Rect;)V
/* .line 454 */
android.util.MiuiAppSizeCompatModeImpl .getInstance ( );
v3 = this.mOwner;
(( com.android.server.wm.ActivityRecord ) v3 ).getName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getName()Ljava/lang/String;
v2 = (( android.util.MiuiAppSizeCompatModeImpl ) v2 ).shouldUseMaxBoundsFullscreen ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/MiuiAppSizeCompatModeImpl;->shouldUseMaxBoundsFullscreen(Ljava/lang/String;)Z
/* .line 456 */
/* .local v2, "shouldUseMaxBoundsFullscreen":Z */
/* if-nez v2, :cond_5 */
/* .line 457 */
v3 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v3 ).setMaxBounds ( v1 ); // invoke-virtual {v3, v1}, Landroid/app/WindowConfiguration;->setMaxBounds(Landroid/graphics/Rect;)V
/* .line 460 */
} // :cond_5
com.android.server.wm.BoundsCompatUtils .getInstance ( );
(( com.android.server.wm.BoundsCompatUtils ) v3 ).adaptCompatBounds ( p2, p3 ); // invoke-virtual {v3, p2, p3}, Lcom/android/server/wm/BoundsCompatUtils;->adaptCompatBounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayContent;)V
/* .line 461 */
/* iget-boolean v3, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z */
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 462 */
/* iget v3, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
/* or-int/lit8 v3, v3, 0x8 */
/* iput v3, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
/* .line 465 */
} // :cond_6
int v3 = 1; // const/4 v3, 0x1
/* .line 420 */
} // .end local v1 # "compatBounds":Landroid/graphics/Rect;
} // .end local v2 # "shouldUseMaxBoundsFullscreen":Z
} // :cond_7
} // :goto_1
/* .line 469 */
} // .end local v0 # "parentAppBounds":Landroid/graphics/Rect;
} // :cond_8
/* iput v3, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F */
/* .line 470 */
/* iput v2, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
/* .line 472 */
} // .end method
void addCallbackIfNeeded ( android.app.servertransaction.ClientTransaction p0 ) {
/* .locals 3 */
/* .param p1, "transaction" # Landroid/app/servertransaction/ClientTransaction; */
/* .line 136 */
/* iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 137 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z */
/* .line 138 */
/* iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I */
v1 = this.mDispatchedBounds;
/* iget v2, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
android.app.servertransaction.BoundsCompatInfoChangeItem .obtain ( v0,v1,v2 );
(( android.app.servertransaction.ClientTransaction ) p1 ).addCallback ( v0 ); // invoke-virtual {p1, v0}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 140 */
} // :cond_0
return;
} // .end method
void adjustWindowParamsIfNeededLocked ( com.android.server.wm.WindowState p0, android.view.WindowManager$LayoutParams p1 ) {
/* .locals 2 */
/* .param p1, "win" # Lcom/android/server/wm/WindowState; */
/* .param p2, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 143 */
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mActivityRecord;
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).inMiuiSizeCompatMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mAttrs;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p2, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* if-nez v0, :cond_1 */
/* .line 146 */
} // :cond_0
v0 = this.mAttrs;
int v1 = 1; // const/4 v1, 0x1
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 147 */
/* iput v1, p2, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 148 */
/* iget v0, p2, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* or-int/lit16 v0, v0, 0x100 */
/* iput v0, p2, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 150 */
} // :cond_1
return;
} // .end method
Boolean areBoundsLetterboxed ( ) {
/* .locals 1 */
/* .line 311 */
v0 = (( com.android.server.wm.BoundsCompatController ) p0 ).inMiuiSizeCompatMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/BoundsCompatController;->inMiuiSizeCompatMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mOwner;
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).areBoundsLetterboxed ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->areBoundsLetterboxed()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
void clearAppBoundsIfNeeded ( android.content.res.Configuration p0 ) {
/* .locals 3 */
/* .param p1, "resolvedConfig" # Landroid/content/res/Configuration; */
/* .line 153 */
v0 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v0 ).getAppBounds ( ); // invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getAppBounds()Landroid/graphics/Rect;
/* .line 154 */
/* .local v0, "resolvedAppBounds":Landroid/graphics/Rect; */
if ( v0 != null) { // if-eqz v0, :cond_0
android.app.servertransaction.BoundsCompatStub .get ( );
/* iget v2, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
v1 = (( android.app.servertransaction.BoundsCompatStub ) v1 ).isFixedAspectRatioModeEnabled ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/servertransaction/BoundsCompatStub;->isFixedAspectRatioModeEnabled(I)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 155 */
(( android.graphics.Rect ) v0 ).setEmpty ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V
/* .line 157 */
} // :cond_0
return;
} // .end method
public void clearSizeCompatMode ( com.android.server.wm.Task p0, Boolean p1, com.android.server.wm.ActivityRecord p2 ) {
/* .locals 1 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "isClearConfig" # Z */
/* .param p3, "excluded" # Lcom/android/server/wm/ActivityRecord; */
/* .line 76 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z */
/* .line 77 */
/* new-instance v0, Lcom/android/server/wm/BoundsCompatController$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p3, p2}, Lcom/android/server/wm/BoundsCompatController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityRecord;Z)V */
(( com.android.server.wm.Task ) p1 ).forAllActivities ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->forAllActivities(Ljava/util/function/Consumer;)V
/* .line 95 */
return;
} // .end method
void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 160 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "MiuiSizeCompat info:"; // const-string v1, "MiuiSizeCompat info:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 162 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mState=0x"; // const-string v1, " mState=0x"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
java.lang.Integer .toHexString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 163 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCompatBounds="; // const-string v1, " mCompatBounds="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mBounds;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 164 */
/* iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 165 */
final String v0 = " mDispatchNeeded=true"; // const-string v0, " mDispatchNeeded=true"
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 168 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I */
/* if-ne v0, v1, :cond_1 */
v0 = this.mBounds;
v1 = this.mDispatchedBounds;
v0 = (( android.graphics.Rect ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
/* .line 169 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDispatchedState="; // const-string v1, " mDispatchedState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I */
java.lang.Integer .toHexString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 170 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDispatchedBounds="; // const-string v1, " mDispatchedBounds="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDispatchedBounds;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 173 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F */
/* const/high16 v1, -0x40800000 # -1.0f */
/* cmpl-float v0, v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 174 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mFixedAspectRatio="; // const-string v2, " mFixedAspectRatio="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 177 */
} // :cond_3
/* iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F */
/* cmpl-float v0, v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 178 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 179 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mGlobalScale="; // const-string v1, " mGlobalScale="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 180 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentScale="; // const-string v1, " mCurrentScale="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 183 */
} // :cond_4
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 184 */
return;
} // .end method
void dumpBounds ( java.io.PrintWriter p0, java.lang.String p1, java.lang.String p2, android.app.WindowConfiguration p3 ) {
/* .locals 8 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .param p3, "title" # Ljava/lang/String; */
/* .param p4, "winConfig" # Landroid/app/WindowConfiguration; */
/* .line 198 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 199 */
final String v0 = " Bounds="; // const-string v0, " Bounds="
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 200 */
(( android.app.WindowConfiguration ) p4 ).getBounds ( ); // invoke-virtual {p4}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* .line 201 */
/* .local v0, "bounds":Landroid/graphics/Rect; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "("; // const-string v2, "("
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, v0, Landroid/graphics/Rect;->left:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ","; // const-string v3, ","
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, v0, Landroid/graphics/Rect;->top:I */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ")"; // const-string v4, ")"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 202 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v5, "x" */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( android.graphics.Rect ) v0 ).height ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 203 */
(( android.app.WindowConfiguration ) p4 ).getAppBounds ( ); // invoke-virtual {p4}, Landroid/app/WindowConfiguration;->getAppBounds()Landroid/graphics/Rect;
/* .line 204 */
/* .local v1, "appBounds":Landroid/graphics/Rect; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 205 */
final String v6 = " AppBounds="; // const-string v6, " AppBounds="
(( java.io.PrintWriter ) p1 ).print ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 206 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v1, Landroid/graphics/Rect;->left:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v1, Landroid/graphics/Rect;->top:I */
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 207 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( android.graphics.Rect ) v1 ).width ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->width()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( android.graphics.Rect ) v1 ).height ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->height()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 210 */
} // :cond_0
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 211 */
return;
} // .end method
Float getCurrentMiuiSizeCompatScale ( Float p0 ) {
/* .locals 1 */
/* .param p1, "currentScale" # F */
/* .line 288 */
/* iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
} // .end method
public android.graphics.Rect getDispatchedBounds ( ) {
/* .locals 1 */
/* .line 392 */
v0 = this.mDispatchedBounds;
} // .end method
public android.graphics.Rect getScaledBounds ( android.graphics.Rect p0 ) {
/* .locals 3 */
/* .param p1, "bounds" # Landroid/graphics/Rect; */
/* .line 561 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V */
/* .line 562 */
/* .local v0, "realFrame":Landroid/graphics/Rect; */
(( android.graphics.Rect ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 563 */
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
(( android.graphics.Rect ) v0 ).scale ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Rect;->scale(F)V
/* .line 564 */
/* iget v1, p1, Landroid/graphics/Rect;->left:I */
/* iget v2, p1, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v0 ).offsetTo ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 565 */
} // .end method
Boolean inMiuiSizeCompatMode ( ) {
/* .locals 1 */
/* .line 284 */
v0 = this.mDispatchedBounds;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( android.graphics.Rect ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void initBoundsCompatController ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 0 */
/* .param p1, "owner" # Lcom/android/server/wm/ActivityRecord; */
/* .line 71 */
this.mOwner = p1;
/* .line 72 */
return;
} // .end method
public Boolean interceptScheduleTransactionIfNeeded ( android.app.servertransaction.ClientTransactionItem p0 ) {
/* .locals 4 */
/* .param p1, "originalTransactionItem" # Landroid/app/servertransaction/ClientTransactionItem; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 215 */
/* iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 216 */
/* iput-boolean v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z */
/* .line 217 */
v0 = this.mOwner;
v0 = this.app;
(( com.android.server.wm.WindowProcessController ) v0 ).getThread ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
v1 = this.mOwner;
v1 = this.token;
android.app.servertransaction.ClientTransaction .obtain ( v0,v1 );
/* .line 218 */
/* .local v0, "transaction":Landroid/app/servertransaction/ClientTransaction; */
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I */
v2 = this.mDispatchedBounds;
/* iget v3, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F */
android.app.servertransaction.BoundsCompatInfoChangeItem .obtain ( v1,v2,v3 );
(( android.app.servertransaction.ClientTransaction ) v0 ).addCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 219 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 220 */
(( android.app.servertransaction.ClientTransaction ) v0 ).addCallback ( p1 ); // invoke-virtual {v0, p1}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 222 */
} // :cond_0
v1 = this.mOwner;
v1 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getLifecycleManager ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;
(( com.android.server.wm.ClientLifecycleManager ) v1 ).scheduleTransaction ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V
/* .line 223 */
int v1 = 1; // const/4 v1, 0x1
/* .line 225 */
} // .end local v0 # "transaction":Landroid/app/servertransaction/ClientTransaction;
} // :cond_1
} // .end method
Boolean isBoundsCompatEnabled ( ) {
/* .locals 1 */
/* .line 229 */
/* iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mBoundsCompatEnabled:Z */
} // .end method
public Boolean isDebugEnable ( ) {
/* .locals 1 */
/* .line 557 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z */
} // .end method
Boolean isDisplayCompatAvailable ( com.android.server.wm.Task p0 ) {
/* .locals 4 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 233 */
v0 = this.mAtmService;
v0 = this.mWindowManager;
v0 = v0 = this.mPolicy;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 234 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 235 */
} // :cond_0
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
/* if-nez v0, :cond_7 */
/* .line 236 */
} // :cond_1
v0 = (( com.android.server.wm.Task ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I
/* if-nez v0, :cond_6 */
v0 = (( com.android.server.wm.Task ) p1 ).isActivityTypeStandard ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isActivityTypeStandard()Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 237 */
v0 = (( com.android.server.wm.Task ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z
/* if-nez v0, :cond_6 */
/* .line 238 */
/* iget v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatPolicy:I */
v0 = com.android.server.wm.ActivityTaskManagerServiceStub .isForcedResizeableDisplayCompatPolicy ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 239 */
/* .line 242 */
} // :cond_2
/* iget v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatPolicy:I */
v0 = com.android.server.wm.ActivityTaskManagerServiceStub .isForcedUnresizeableDisplayCompatPolicy ( v0 );
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 243 */
/* .line 246 */
} // :cond_3
/* iget v0, p1, Lcom/android/server/wm/Task;->mResizeMode:I */
int v3 = 2; // const/4 v3, 0x2
/* if-eq v0, v3, :cond_5 */
/* iget v0, p1, Lcom/android/server/wm/Task;->mResizeMode:I */
/* if-ne v0, v2, :cond_4 */
/* .line 250 */
} // :cond_4
/* .line 248 */
} // :cond_5
} // :goto_0
/* .line 252 */
} // :cond_6
/* .line 255 */
} // :cond_7
/* iget-boolean v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
v0 = (( com.android.server.wm.Task ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 256 */
/* .line 258 */
} // :cond_8
/* iget-boolean v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z */
} // .end method
public Boolean isFixedAspectRatioAvailable ( ) {
/* .locals 1 */
/* .line 263 */
/* iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z */
} // .end method
public Boolean isFixedAspectRatioEnabled ( ) {
/* .locals 2 */
/* .line 268 */
android.app.servertransaction.BoundsCompat .getInstance ( );
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
v0 = (( android.app.servertransaction.BoundsCompat ) v0 ).isFixedAspectRatioModeEnabled ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/servertransaction/BoundsCompat;->isFixedAspectRatioModeEnabled(I)Z
} // .end method
public void prepareBoundsCompat ( ) {
/* .locals 2 */
/* .line 273 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
/* .line 274 */
v0 = this.mOwner;
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).isActivityTypeStandardOrUndefined ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isActivityTypeStandardOrUndefined()Z
/* if-nez v0, :cond_0 */
/* .line 275 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mOwner;
/* .line 276 */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).getActivityType ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getActivityType()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
/* .line 277 */
} // :cond_0
v0 = this.mOwner;
v0 = this.info;
/* invoke-direct {p0, v0}, Lcom/android/server/wm/BoundsCompatController;->applyPolicyIfNeeded(Landroid/content/pm/ActivityInfo;)V */
/* .line 279 */
} // :cond_1
return;
} // .end method
void resolveBoundsCompat ( android.content.res.Configuration p0, android.content.res.Configuration p1, android.graphics.Rect p2, Float p3 ) {
/* .locals 5 */
/* .param p1, "resolvedConfig" # Landroid/content/res/Configuration; */
/* .param p2, "parentConfig" # Landroid/content/res/Configuration; */
/* .param p3, "sizeCompatBounds" # Landroid/graphics/Rect; */
/* .param p4, "sizeCompatScale" # F */
/* .line 359 */
com.android.server.wm.BoundsCompatUtils .getInstance ( );
v1 = this.mOwner;
v0 = (( com.android.server.wm.BoundsCompatUtils ) v0 ).getFlipCompatModeByActivity ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByActivity(Lcom/android/server/wm/ActivityRecord;)I
/* .line 360 */
/* .local v0, "activityMode":I */
com.android.server.wm.BoundsCompatUtils .getInstance ( );
v2 = this.mOwner;
v2 = this.mAtmService;
v3 = this.mOwner;
v3 = this.packageName;
/* .line 361 */
v1 = (( com.android.server.wm.BoundsCompatUtils ) v1 ).getFlipCompatModeByApp ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I
/* .line 362 */
/* .local v1, "appMode":I */
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* if-nez v0, :cond_0 */
/* .line 364 */
/* iput-boolean v2, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z */
/* .line 366 */
} // :cond_0
android.app.servertransaction.BoundsCompat .getInstance ( );
/* iget v4, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
v3 = (( android.app.servertransaction.BoundsCompat ) v3 ).isFixedAspectRatioModeEnabled ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/servertransaction/BoundsCompat;->isFixedAspectRatioModeEnabled(I)Z
/* iput-boolean v3, p0, Lcom/android/server/wm/BoundsCompatController;->mBoundsCompatEnabled:Z */
/* .line 367 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 368 */
v3 = this.mBounds;
/* if-nez v3, :cond_1 */
/* .line 369 */
/* new-instance v3, Landroid/graphics/Rect; */
/* invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V */
this.mBounds = v3;
/* .line 371 */
} // :cond_1
v3 = this.mBounds;
v4 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v4 ).getBounds ( ); // invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
(( android.graphics.Rect ) v3 ).set ( v4 ); // invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 372 */
v3 = this.mDispatchedBounds;
/* if-nez v3, :cond_2 */
/* .line 373 */
/* new-instance v3, Landroid/graphics/Rect; */
/* invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V */
this.mDispatchedBounds = v3;
/* .line 375 */
} // :cond_2
v3 = this.mDispatchedBounds;
v4 = this.mBounds;
v3 = (( android.graphics.Rect ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_4 */
/* .line 376 */
v3 = this.mDispatchedBounds;
v4 = this.mBounds;
(( android.graphics.Rect ) v3 ).set ( v4 ); // invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 377 */
/* iput-boolean v2, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z */
/* .line 379 */
} // :cond_3
android.app.servertransaction.BoundsCompat .getInstance ( );
/* iget v4, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I */
v3 = (( android.app.servertransaction.BoundsCompat ) v3 ).isFixedAspectRatioModeEnabled ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/servertransaction/BoundsCompat;->isFixedAspectRatioModeEnabled(I)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 380 */
v3 = this.mBounds;
(( android.graphics.Rect ) v3 ).setEmpty ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V
/* .line 381 */
v3 = this.mDispatchedBounds;
(( android.graphics.Rect ) v3 ).setEmpty ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V
/* .line 382 */
/* iput-boolean v2, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z */
/* .line 385 */
} // :cond_4
} // :goto_0
/* iget v3, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
/* iget v4, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I */
/* if-eq v3, v4, :cond_5 */
/* .line 386 */
/* iput v3, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I */
/* .line 387 */
/* iput-boolean v2, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z */
/* .line 389 */
} // :cond_5
return;
} // .end method
Boolean shouldNotCreateCompatDisplayInsets ( ) {
/* .locals 2 */
/* .line 303 */
v0 = this.mOwner;
v0 = this.mAtmService;
/* iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mForceResizableActivities:Z */
/* if-nez v0, :cond_0 */
/* .line 304 */
android.app.servertransaction.BoundsCompatStub .get ( );
/* iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I */
v0 = (( android.app.servertransaction.BoundsCompatStub ) v0 ).isFixedAspectRatioModeEnabled ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/servertransaction/BoundsCompatStub;->isFixedAspectRatioModeEnabled(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 306 */
/* .local v0, "inMiuiSizeCompatMode":Z */
} // :goto_0
} // .end method
Integer shouldRelaunchInMiuiSizeCompatMode ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "changes" # I */
/* .param p2, "configChanged" # I */
/* .param p3, "lastReportedDisplayId" # I */
/* .line 294 */
v0 = this.mOwner;
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getDisplayId()I
/* if-ne v0, p3, :cond_0 */
/* and-int/lit16 v0, p1, 0x80 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 296 */
/* or-int/lit16 p2, p2, 0xd00 */
/* .line 299 */
} // :cond_0
} // .end method
Boolean shouldShowLetterboxUi ( ) {
/* .locals 2 */
/* .line 315 */
v0 = this.mOwner;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).inMiuiSizeCompatMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 316 */
v0 = this.mOwner;
(( com.android.server.wm.ActivityRecord ) v0 ).getTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* .line 317 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 318 */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/BoundsCompatController;->taskContainsActivityRecord(Lcom/android/server/wm/Task;)Z */
/* .line 321 */
} // .end local v0 # "task":Lcom/android/server/wm/Task;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void updateDisplayCompatModeAvailableIfNeeded ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 396 */
v0 = this.realActivity;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 397 */
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v1 = this.realActivity;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v0 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v0 ).getPolicy ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getPolicy(Ljava/lang/String;)I
/* .line 398 */
/* .local v0, "policy":I */
/* iget v1, p1, Lcom/android/server/wm/Task;->mDisplayCompatPolicy:I */
/* if-eq v1, v0, :cond_0 */
/* .line 399 */
/* iput v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatPolicy:I */
/* .line 403 */
} // .end local v0 # "policy":I
} // :cond_0
v0 = (( com.android.server.wm.BoundsCompatController ) p0 ).isDisplayCompatAvailable ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/BoundsCompatController;->isDisplayCompatAvailable(Lcom/android/server/wm/Task;)Z
/* .line 404 */
/* .local v0, "isAvailable":Z */
/* iget-boolean v1, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z */
/* if-eq v1, v0, :cond_1 */
/* .line 405 */
/* iput-boolean v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z */
/* .line 406 */
/* if-nez v0, :cond_1 */
/* .line 407 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.BoundsCompatController ) p0 ).clearSizeCompatMode ( p1, v1 ); // invoke-virtual {p0, p1, v1}, Lcom/android/server/wm/BoundsCompatController;->clearSizeCompatMode(Lcom/android/server/wm/Task;Z)V
/* .line 410 */
} // :cond_1
return;
} // .end method
