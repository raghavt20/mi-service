.class public Lcom/android/server/wm/MiuiFreeformTrackManager;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiFreeformTrackManager$CommonTrackConstants;,
        Lcom/android/server/wm/MiuiFreeformTrackManager$MultiWindowRecommendTrackConstants;,
        Lcom/android/server/wm/MiuiFreeformTrackManager$MultiFreeformTrackConstants;,
        Lcom/android/server/wm/MiuiFreeformTrackManager$SmallWindowTrackConstants;,
        Lcom/android/server/wm/MiuiFreeformTrackManager$MiniWindowTrackConstants;
    }
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000000538"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final SERVER_CLASS_NAME:Ljava/lang/String; = "com.miui.analytics.onetrack.TrackService"

.field private static final SERVER_PKG_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final TAG:Ljava/lang/String; = "MiuiFreeformOneTrackManager"


# instance fields
.field private mBindOneTrackService:Ljava/lang/Runnable;

.field private mContext:Landroid/content/Context;

.field public final mFreeFormTrackLock:Ljava/lang/Object;

.field private mHandler:Landroid/os/Handler;

.field private mITrackBinder:Lcom/miui/analytics/ITrackBinder;

.field private mMiuiFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

.field private mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static bridge synthetic -$$Nest$fgetmBindOneTrackService(Lcom/android/server/wm/MiuiFreeformTrackManager;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mBindOneTrackService:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/MiuiFreeformTrackManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/wm/MiuiFreeformTrackManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;Lcom/miui/analytics/ITrackBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    return-void
.end method

.method static bridge synthetic -$$Nest$mputCommomParam(Lcom/android/server/wm/MiuiFreeformTrackManager;Lorg/json/JSONObject;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->putCommomParam(Lorg/json/JSONObject;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/server/wm/MiuiFreeFormGestureController;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "controller"    # Lcom/android/server/wm/MiuiFreeFormGestureController;

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mFreeFormTrackLock:Ljava/lang/Object;

    .line 224
    new-instance v0, Lcom/android/server/wm/MiuiFreeformTrackManager$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeformTrackManager$2;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mBindOneTrackService:Ljava/lang/Runnable;

    .line 176
    iput-object p2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mMiuiFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    .line 177
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mContext:Landroid/content/Context;

    .line 178
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    .line 179
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformTrackManager;->bindOneTrackService()V

    .line 180
    return-void
.end method

.method private static getRegion()Ljava/lang/String;
    .locals 10

    .line 758
    const-string v0, ""

    :try_start_0
    const-string v1, "ro.miui.region"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 759
    .local v1, "region":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 760
    const-string v2, "ro.product.locale.region"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 762
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 764
    const-string v2, "android.os.LocaleList"

    .line 765
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getDefault"

    const/4 v4, 0x0

    new-array v5, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 766
    .local v2, "localeList":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v5, "size"

    new-array v6, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 767
    .local v3, "size":Ljava/lang/Object;
    instance-of v5, v3, Ljava/lang/Integer;

    if-eqz v5, :cond_1

    move-object v5, v3

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-lez v5, :cond_1

    .line 768
    nop

    .line 769
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "get"

    const/4 v7, 0x1

    new-array v8, v7, [Ljava/lang/Class;

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v8, v4

    invoke-virtual {v5, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {v5, v2, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 770
    .local v5, "locale":Ljava/lang/Object;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "getCountry"

    new-array v8, v4, [Ljava/lang/Class;

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v6, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 771
    .local v4, "country":Ljava/lang/Object;
    instance-of v6, v4, Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 772
    move-object v6, v4

    check-cast v6, Ljava/lang/String;

    move-object v1, v6

    .line 776
    .end local v2    # "localeList":Ljava/lang/Object;
    .end local v3    # "size":Ljava/lang/Object;
    .end local v4    # "country":Ljava/lang/Object;
    .end local v5    # "locale":Ljava/lang/Object;
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 777
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 780
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 781
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 785
    .end local v1    # "region":Ljava/lang/String;
    :cond_3
    goto :goto_0

    .line 783
    :catch_0
    move-exception v1

    .line 784
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "MiuiFreeformOneTrackManager"

    const-string v3, "getRegion Exception: "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 786
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v0
.end method

.method public static isCanOneTrack()Z
    .locals 2

    .line 753
    const-string v0, "CN"

    invoke-static {}, Lcom/android/server/wm/MiuiFreeformTrackManager;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private putCommomParam(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "jsonData"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 738
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/util/MiuiMultiWindowUtils;->isPadScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 739
    const-string v0, "pad"

    goto :goto_0

    .line 740
    :cond_0
    const-string/jumbo v0, "\u624b\u673a"

    .line 738
    :goto_0
    const-string v1, "model_type"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 741
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mMiuiFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-boolean v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsPortrait:Z

    if-eqz v0, :cond_1

    .line 742
    const-string/jumbo v0, "\u7ad6\u5c4f"

    goto :goto_1

    .line 743
    :cond_1
    const-string/jumbo v0, "\u6a2a\u5c4f"

    .line 741
    :goto_1
    const-string v1, "screen_orientation"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 744
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/util/MiuiMultiWindowUtils;->isFoldInnerScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 745
    const-string/jumbo v0, "\u5185\u5c4f"

    goto :goto_2

    .line 746
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/util/MiuiMultiWindowUtils;->isFoldOuterScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 747
    const-string/jumbo v0, "\u5916\u5c4f"

    goto :goto_2

    .line 748
    :cond_3
    const-string v0, "nothing"

    .line 744
    :goto_2
    const-string/jumbo v1, "screen_type"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 749
    const-string v0, "data_version"

    const-string v1, "22053100"

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 750
    return-void
.end method


# virtual methods
.method public bindOneTrackService()V
    .locals 4

    .line 186
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 187
    const-string v0, "MiuiFreeformOneTrackManager"

    const-string v1, "Context == null"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mFreeFormTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 191
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v1, :cond_1

    .line 192
    const-string v1, "MiuiFreeformOneTrackManager"

    const-string v2, "aready bound"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    monitor-exit v0

    return-void

    .line 195
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 198
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    const-string v2, "com.miui.analytics.onetrack.TrackService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeformTrackManager$1;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 216
    const-string v1, "MiuiFreeformOneTrackManager"

    const-string v2, "Start Bind OneTrack Service"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 221
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 220
    const-string v1, "MiuiFreeformOneTrackManager"

    const-string v2, "Bind OneTrack Service Exception"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 195
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public closeOneTrackService(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceConnection"    # Landroid/content/ServiceConnection;

    .line 231
    if-nez p1, :cond_0

    .line 232
    const-string v0, "MiuiFreeformOneTrackManager"

    const-string/jumbo v1, "unBindOneTrackService: mContext == null "

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mFreeFormTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 236
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    .line 237
    invoke-virtual {p1, p2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 238
    const-string v1, "MiuiFreeformOneTrackManager"

    const-string/jumbo v2, "unBindOneTrackService success"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 240
    :cond_1
    const-string v1, "MiuiFreeformOneTrackManager"

    const-string/jumbo v2, "unBindOneTrackService failed: mServiceConnection == null"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    .line 243
    monitor-exit v0

    .line 244
    return-void

    .line 243
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public trackClickSmallWindowEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;

    .line 431
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$10;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$10;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 451
    return-void
.end method

.method public trackFreeFormRecommendClickEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;

    .line 710
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$21;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$21;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 735
    return-void
.end method

.method public trackFreeFormRecommendExposeEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;

    .line 654
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$19;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$19;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 679
    return-void
.end method

.method public trackMiniWindowEnterWayEvent(Ljava/lang/String;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p1, "enterWay"    # Ljava/lang/String;
    .param p2, "leftTopPosition"    # Landroid/graphics/Point;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "applicationName"    # Ljava/lang/String;
    .param p5, "freeformAppCount"    # I

    .line 248
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v8, Lcom/android/server/wm/MiuiFreeformTrackManager$3;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/android/server/wm/MiuiFreeformTrackManager$3;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 272
    return-void
.end method

.method public trackMiniWindowMoveEvent(Landroid/graphics/Point;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "startPosition"    # Landroid/graphics/Point;
    .param p2, "endPosition"    # Landroid/graphics/Point;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "applicationName"    # Ljava/lang/String;

    .line 276
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$4;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$4;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Landroid/graphics/Point;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 300
    return-void
.end method

.method public trackMiniWindowPinedEvent(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;
    .param p3, "posX"    # I
    .param p4, "posY"    # I

    .line 481
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$12;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$12;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 503
    return-void
.end method

.method public trackMiniWindowPinedMoveEvent(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;
    .param p3, "posX"    # I
    .param p4, "posY"    # I

    .line 531
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$14;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$14;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 553
    return-void
.end method

.method public trackMiniWindowPinedQuitEvent(Ljava/lang/String;Ljava/lang/String;F)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;
    .param p3, "pinedDuration"    # F

    .line 580
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$16;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeformTrackManager$16;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;F)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 601
    return-void
.end method

.method public trackMiniWindowQuitEvent(Ljava/lang/String;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "quitWay"    # Ljava/lang/String;
    .param p2, "leftTopPosition"    # Landroid/graphics/Point;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "applicationName"    # Ljava/lang/String;

    .line 304
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$5;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$5;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 327
    return-void
.end method

.method public trackMultiFreeformEnterEvent(I)V
    .locals 2
    .param p1, "nums"    # I

    .line 604
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$17;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeformTrackManager$17;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 623
    return-void
.end method

.method public trackSmallWindowEnterWayEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p1, "enterWay"    # Ljava/lang/String;
    .param p2, "smallWindowRatio"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "applicationName"    # Ljava/lang/String;
    .param p5, "freeformAppCount"    # I

    .line 331
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v8, Lcom/android/server/wm/MiuiFreeformTrackManager$6;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/android/server/wm/MiuiFreeformTrackManager$6;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 354
    return-void
.end method

.method public trackSmallWindowMoveEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;

    .line 357
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$7;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$7;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 377
    return-void
.end method

.method public trackSmallWindowPinedEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;
    .param p3, "pinedWay"    # Ljava/lang/String;
    .param p4, "posX"    # I
    .param p5, "posY"    # I

    .line 455
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v8, Lcom/android/server/wm/MiuiFreeformTrackManager$11;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/android/server/wm/MiuiFreeformTrackManager$11;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 478
    return-void
.end method

.method public trackSmallWindowPinedMoveEvent(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;
    .param p3, "posX"    # I
    .param p4, "posY"    # I

    .line 506
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$13;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$13;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 528
    return-void
.end method

.method public trackSmallWindowPinedQuitEvent(Ljava/lang/String;Ljava/lang/String;F)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;
    .param p3, "pinedDuration"    # F

    .line 556
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$15;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeformTrackManager$15;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;F)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 577
    return-void
.end method

.method public trackSmallWindowQuitEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 8
    .param p1, "quitWay"    # Ljava/lang/String;
    .param p2, "smallWindowRatio"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "applicationName"    # Ljava/lang/String;
    .param p5, "useDuration"    # J

    .line 407
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$9;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$9;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 428
    return-void
.end method

.method public trackSmallWindowResizeEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 8
    .param p1, "smallWindowFromRatio"    # Ljava/lang/String;
    .param p2, "smallWindowToRatio"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "applicationName"    # Ljava/lang/String;
    .param p5, "useDuration"    # J

    .line 381
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$8;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$8;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 403
    return-void
.end method

.method public trackSplitScreenRecommendClickEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;

    .line 682
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$20;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$20;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 707
    return-void
.end method

.method public trackSplitScreenRecommendExposeEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "applicationName"    # Ljava/lang/String;

    .line 626
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$18;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$18;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 651
    return-void
.end method
