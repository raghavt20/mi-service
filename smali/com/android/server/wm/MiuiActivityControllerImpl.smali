.class public Lcom/android/server/wm/MiuiActivityControllerImpl;
.super Ljava/lang/Object;
.source "MiuiActivityControllerImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiActivityController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiActivityControllerImpl$H;
    }
.end annotation


# static fields
.field private static final APP_DC_DISPLAYID:Ljava/lang/String; = "app_dc_displayid"

.field private static final DEBUG_MESSAGES:Z

.field static final INSTANCE:Lcom/android/server/wm/MiuiActivityControllerImpl;

.field private static final MIUI_ACTIVITY_EMBEDDING_ENABLED:Z

.field private static final PREFIX_TAG:Ljava/lang/String; = "MiuiLog-ActivityObserver:"

.field private static final SUB_SETTINGS_ACT:Ljava/lang/String; = "com.android.settings/.SubSettings"

.field private static final SUB_SETTINGS_FRAGMENT_NAME:Ljava/lang/String; = ":settings:show_fragment"

.field private static final TAG:Ljava/lang/String; = "MiuiActivityController"


# instance fields
.field private final mActivityObservers:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Landroid/app/IMiuiActivityObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final mH:Lcom/android/server/wm/MiuiActivityControllerImpl$H;

.field private final mSendIntent:Landroid/content/Intent;


# direct methods
.method static bridge synthetic -$$Nest$fgetmActivityObservers(Lcom/android/server/wm/MiuiActivityControllerImpl;)Landroid/os/RemoteCallbackList;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mActivityObservers:Landroid/os/RemoteCallbackList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSendIntent(Lcom/android/server/wm/MiuiActivityControllerImpl;)Landroid/content/Intent;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mSendIntent:Landroid/content/Intent;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleSpecialExtras(Lcom/android/server/wm/MiuiActivityControllerImpl;Landroid/content/Intent;Lcom/android/server/wm/ActivityRecord;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiActivityControllerImpl;->handleSpecialExtras(Landroid/content/Intent;Lcom/android/server/wm/ActivityRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG_MESSAGES()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->DEBUG_MESSAGES:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetMIUI_ACTIVITY_EMBEDDING_ENABLED()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->MIUI_ACTIVITY_EMBEDDING_ENABLED:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 27
    const-string v0, "debug.miui.activity.log"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->DEBUG_MESSAGES:Z

    .line 29
    const-string v0, "ro.config.miui_activity_embedding_enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->MIUI_ACTIVITY_EMBEDDING_ENABLED:Z

    .line 34
    new-instance v0, Lcom/android/server/wm/MiuiActivityControllerImpl;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiActivityControllerImpl;-><init>()V

    sput-object v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->INSTANCE:Lcom/android/server/wm/MiuiActivityControllerImpl;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mSendIntent:Landroid/content/Intent;

    .line 194
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mActivityObservers:Landroid/os/RemoteCallbackList;

    .line 195
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MiuiActivityController"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 196
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 197
    new-instance v1, Lcom/android/server/wm/MiuiActivityControllerImpl$H;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;-><init>(Lcom/android/server/wm/MiuiActivityControllerImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mH:Lcom/android/server/wm/MiuiActivityControllerImpl$H;

    .line 198
    return-void
.end method

.method private handleSpecialExtras(Landroid/content/Intent;Lcom/android/server/wm/ActivityRecord;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "isSubSettings":Z
    const-string v1, ":settings:show_fragment"

    if-eqz p2, :cond_0

    :try_start_0
    const-string v2, "com.android.settings/.SubSettings"

    iget-object v3, p2, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_0

    .line 205
    iget-object v2, p2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 206
    .local v2, "fragment":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 207
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    const/4 v0, 0x1

    goto :goto_0

    .line 214
    .end local v0    # "isSubSettings":Z
    .end local v2    # "fragment":Ljava/lang/String;
    :catch_0
    move-exception v0

    goto :goto_1

    .line 211
    .restart local v0    # "isSubSettings":Z
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 212
    invoke-virtual {p1, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 215
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 216
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    nop

    .line 218
    :goto_3
    if-eqz p2, :cond_2

    iget-object v0, p2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_2

    .line 219
    iget-object v0, p2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getSender()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setSender(Ljava/lang/String;)V

    .line 221
    :cond_2
    return-void
.end method

.method public static logMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "prefix"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .line 290
    sget-boolean v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->DEBUG_MESSAGES:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiActivityController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    :cond_0
    return-void
.end method

.method private sendMessage(ILjava/lang/Object;)V
    .locals 6
    .param p1, "what"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .line 259
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;IIZ)V

    .line 260
    return-void
.end method

.method private sendMessage(ILjava/lang/Object;I)V
    .locals 6
    .param p1, "what"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "arg1"    # I

    .line 263
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;IIZ)V

    .line 264
    return-void
.end method

.method private sendMessage(ILjava/lang/Object;II)V
    .locals 6
    .param p1, "what"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I

    .line 267
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;IIZ)V

    .line 268
    return-void
.end method

.method private sendMessage(ILjava/lang/Object;IIZ)V
    .locals 3
    .param p1, "what"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "async"    # Z

    .line 271
    iget-object v0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mActivityObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v0

    .line 272
    .local v0, "size":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SendMessage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mH:Lcom/android/server/wm/MiuiActivityControllerImpl$H;

    .line 273
    invoke-virtual {v2, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->codeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " observer size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 272
    const-string v2, "MiuiLog-ActivityObserver:"

    invoke-static {v2, v1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->logMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    if-gtz v0, :cond_0

    .line 276
    return-void

    .line 278
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 279
    .local v1, "msg":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->what:I

    .line 280
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 281
    iput p3, v1, Landroid/os/Message;->arg1:I

    .line 282
    iput p4, v1, Landroid/os/Message;->arg2:I

    .line 283
    if-eqz p5, :cond_1

    .line 284
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Message;->setAsynchronous(Z)V

    .line 286
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mH:Lcom/android/server/wm/MiuiActivityControllerImpl$H;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->sendMessage(Landroid/os/Message;)Z

    .line 287
    return-void
.end method


# virtual methods
.method public activityDestroyed(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 255
    const/4 v0, 0x5

    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V

    .line 256
    return-void
.end method

.method public activityIdle(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 235
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V

    .line 236
    return-void
.end method

.method public activityPaused(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 245
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V

    .line 246
    return-void
.end method

.method public activityResumed(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 240
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V

    .line 241
    return-void
.end method

.method public activityStopped(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 250
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V

    .line 251
    return-void
.end method

.method public registerActivityObserver(Landroid/app/IMiuiActivityObserver;Landroid/content/Intent;)V
    .locals 1
    .param p1, "observer"    # Landroid/app/IMiuiActivityObserver;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 225
    iget-object v0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mActivityObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1, p2}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 226
    return-void
.end method

.method public unregisterActivityObserver(Landroid/app/IMiuiActivityObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/app/IMiuiActivityObserver;

    .line 230
    iget-object v0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl;->mActivityObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 231
    return-void
.end method
