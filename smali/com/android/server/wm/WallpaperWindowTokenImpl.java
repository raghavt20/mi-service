public class com.android.server.wm.WallpaperWindowTokenImpl implements com.android.server.wm.WallpaperWindowTokenStub {
	 /* .source "WallpaperWindowTokenImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.wm.WallpaperWindowTokenImpl ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public com.android.server.wm.Transition getWaitingRecentTransition ( com.android.server.wm.WindowState p0, com.android.server.wm.TransitionController p1 ) {
		 /* .locals 5 */
		 /* .param p1, "wallpaperTarget" # Lcom/android/server/wm/WindowState; */
		 /* .param p2, "controller" # Lcom/android/server/wm/TransitionController; */
		 /* .line 34 */
		 int v0 = 0; // const/4 v0, 0x0
		 if ( p1 != null) { // if-eqz p1, :cond_3
			 v1 = this.mActivityRecord;
			 /* if-nez v1, :cond_0 */
			 /* .line 37 */
		 } // :cond_0
		 int v1 = 0; // const/4 v1, 0x0
		 /* .local v1, "i":I */
	 } // :goto_0
	 v2 = this.mWaitingTransitions;
	 v2 = 	 (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
	 /* if-ge v1, v2, :cond_2 */
	 /* .line 38 */
	 v2 = this.mWaitingTransitions;
	 (( java.util.ArrayList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
	 /* check-cast v2, Lcom/android/server/wm/Transition; */
	 /* .line 39 */
	 /* .local v2, "transition":Lcom/android/server/wm/Transition; */
	 /* iget v3, v2, Lcom/android/server/wm/Transition;->mParallelCollectType:I */
	 int v4 = 2; // const/4 v4, 0x2
	 /* if-ne v3, v4, :cond_1 */
	 v3 = this.mActivityRecord;
	 /* .line 40 */
	 v3 = 	 (( com.android.server.wm.Transition ) v2 ).isInTransition ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/Transition;->isInTransition(Lcom/android/server/wm/WindowContainer;)Z
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 41 */
		 /* .line 37 */
	 } // .end local v2 # "transition":Lcom/android/server/wm/Transition;
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 44 */
} // .end local v1 # "i":I
} // :cond_2
/* .line 35 */
} // :cond_3
} // :goto_1
} // .end method
public void onConfigurationChanged ( android.content.res.Configuration p0, com.android.server.wm.WallpaperWindowToken p1 ) {
/* .locals 3 */
/* .param p1, "newParentConfig" # Landroid/content/res/Configuration; */
/* .param p2, "wallpaperWindowToken" # Lcom/android/server/wm/WallpaperWindowToken; */
/* .line 14 */
v0 = this.mResolvedTmpConfig;
(( com.android.server.wm.WallpaperWindowToken ) p2 ).getResolvedOverrideConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WallpaperWindowToken;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;
v0 = (( android.content.res.Configuration ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Configuration;->equals(Landroid/content/res/Configuration;)Z
/* if-nez v0, :cond_1 */
/* .line 15 */
v0 = this.mChildren;
v0 = (( com.android.server.wm.WindowList ) v0 ).size ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowList;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 16 */
v1 = this.mWmService;
v1 = this.mAtmService;
v1 = this.mProcessMap;
v2 = this.mChildren;
/* .line 17 */
(( com.android.server.wm.WindowList ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/WindowState; */
v2 = this.mSession;
/* iget v2, v2, Lcom/android/server/wm/Session;->mPid:I */
(( com.android.server.wm.WindowProcessControllerMap ) v1 ).getProcess ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;
/* .line 18 */
/* .local v1, "app":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = (( com.android.server.wm.WindowProcessController ) v1 ).hasThread ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->hasThread()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 19 */
(( com.android.server.wm.WallpaperWindowToken ) p2 ).getResolvedOverrideConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WallpaperWindowToken;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;
(( com.android.server.wm.WindowProcessController ) v1 ).onRequestedOverrideConfigurationChanged ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowProcessController;->onRequestedOverrideConfigurationChanged(Landroid/content/res/Configuration;)V
/* .line 15 */
} // .end local v1 # "app":Lcom/android/server/wm/WindowProcessController;
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 24 */
} // .end local v0 # "i":I
} // :cond_1
v0 = this.mChildren;
v0 = (( com.android.server.wm.WindowList ) v0 ).size ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowList;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .restart local v0 # "i":I */
} // :goto_1
/* if-ltz v0, :cond_3 */
/* .line 25 */
v1 = this.mWmService;
v1 = this.mAtmService;
v1 = this.mProcessMap;
v2 = this.mChildren;
/* .line 26 */
(( com.android.server.wm.WindowList ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/WindowState; */
v2 = this.mSession;
/* iget v2, v2, Lcom/android/server/wm/Session;->mPid:I */
(( com.android.server.wm.WindowProcessControllerMap ) v1 ).getProcess ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;
/* .line 27 */
/* .restart local v1 # "app":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = (( com.android.server.wm.WindowProcessController ) v1 ).hasThread ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->hasThread()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 28 */
(( com.android.server.wm.WallpaperWindowToken ) p2 ).getMergedOverrideConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WallpaperWindowToken;->getMergedOverrideConfiguration()Landroid/content/res/Configuration;
(( com.android.server.wm.WindowProcessController ) v1 ).onMergedOverrideConfigurationChanged ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowProcessController;->onMergedOverrideConfigurationChanged(Landroid/content/res/Configuration;)V
/* .line 24 */
} // .end local v1 # "app":Lcom/android/server/wm/WindowProcessController;
} // :cond_2
/* add-int/lit8 v0, v0, -0x1 */
/* .line 31 */
} // .end local v0 # "i":I
} // :cond_3
return;
} // .end method
