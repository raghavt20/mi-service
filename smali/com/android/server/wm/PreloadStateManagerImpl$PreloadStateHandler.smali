.class final Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;
.super Landroid/os/Handler;
.source "PreloadStateManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/PreloadStateManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "PreloadStateHandler"
.end annotation


# static fields
.field static final ENABLE_AUDIO_MSG:I = 0x4

.field static final FREEZE_PROCESS_MSG:I = 0x1

.field static final KILL_PROCESS_MSG:I = 0x2

.field static final ON_KILL_ENABLE_AUDIO_MSG:I = 0x3


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 2
    .param p1, "looper"    # Landroid/os/Looper;

    .line 636
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 637
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .line 641
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x0

    const-string v2, "PreloadStateManagerImpl"

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 685
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->-$$Nest$smdisableAudio(ZI)V

    goto/16 :goto_1

    .line 678
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 679
    .local v0, "uid":I
    invoke-static {}, Lcom/android/server/wm/PreloadStateManagerImpl;->-$$Nest$sfgetsPreloadUidInfos()Ljava/util/Map;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/server/wm/PreloadStateManagerImpl;->-$$Nest$sfgetsPreloadUidInfos()Ljava/util/Map;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    iget v1, v1, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    if-gez v1, :cond_0

    .line 680
    invoke-static {v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->-$$Nest$smonPreloadAppKilled(I)V

    .line 683
    .end local v0    # "uid":I
    :cond_0
    goto/16 :goto_1

    .line 659
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    .line 660
    .local v0, "info":Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;
    if-nez v0, :cond_1

    .line 661
    return-void

    .line 663
    :cond_1
    invoke-static {}, Lcom/android/server/wm/PreloadStateManagerStub;->get()Lcom/android/server/wm/PreloadStateManagerStub;

    move-result-object v3

    iget v4, v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    invoke-interface {v3, v4}, Lcom/android/server/wm/PreloadStateManagerStub;->alreadyPreload(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 664
    invoke-static {}, Lcom/android/server/wm/PreloadStateManagerImpl;->-$$Nest$sfgetsPreloadUidInfos()Ljava/util/Map;

    move-result-object v3

    iget v4, v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    invoke-direct {v5}, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;-><init>()V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 665
    iget v3, v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    invoke-static {v3}, Lcom/android/server/am/PreloadAppControllerImpl;->getPackageName(I)Ljava/lang/String;

    move-result-object v3

    .line 666
    .local v3, "packageName":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 667
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "timeout kill preloadApp "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    const-class v2, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/process/ProcessManagerInternal;

    .line 669
    const-string/jumbo v4, "timeout_kill_preloadApp"

    invoke-virtual {v2, v3, v1, v4}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 672
    :cond_2
    const-string v1, "preloadApp kill fail because packagename is null"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    .end local v0    # "info":Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_3
    :goto_0
    goto :goto_1

    .line 643
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    .line 644
    .restart local v0    # "info":Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;
    if-nez v0, :cond_4

    .line 645
    return-void

    .line 647
    :cond_4
    invoke-static {}, Lcom/android/server/wm/PreloadStateManagerStub;->get()Lcom/android/server/wm/PreloadStateManagerStub;

    move-result-object v1

    iget v3, v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    invoke-interface {v1, v3}, Lcom/android/server/wm/PreloadStateManagerStub;->alreadyPreload(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 648
    sget-boolean v1, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v1, :cond_5

    .line 649
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "freeze preloadApp uid "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    :cond_5
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v3

    iget v1, v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    filled-new-array {v1}, [I

    move-result-object v4

    const-wide/16 v5, 0x0

    sget v7, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_PRELOAD:I

    const-string v8, "freeze preloadApp"

    const/4 v9, 0x0

    invoke-virtual/range {v3 .. v9}, Lcom/miui/server/greeze/GreezeManagerInternal;->freezeUids([IJILjava/lang/String;Z)Ljava/util/List;

    .line 654
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v1

    iget v2, v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerInternal;->queryBinderState(I)V

    .line 657
    .end local v0    # "info":Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;
    :cond_6
    nop

    .line 689
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
