.class public Lcom/android/server/wm/DimmerImpl;
.super Ljava/lang/Object;
.source "DimmerImpl.java"

# interfaces
.implements Lcom/android/server/wm/DimmerStub;


# instance fields
.field private mAnimateEnter:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/DimmerImpl;->mAnimateEnter:Z

    return-void
.end method


# virtual methods
.method public isAnimateEnter()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/android/server/wm/DimmerImpl;->mAnimateEnter:Z

    return v0
.end method

.method public setAnimateEnter(Z)V
    .locals 0
    .param p1, "animateEnter"    # Z

    .line 14
    iput-boolean p1, p0, Lcom/android/server/wm/DimmerImpl;->mAnimateEnter:Z

    .line 15
    return-void
.end method
