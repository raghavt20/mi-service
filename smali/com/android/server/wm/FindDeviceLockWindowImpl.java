public class com.android.server.wm.FindDeviceLockWindowImpl implements com.android.server.wm.FindDeviceLockWindowStub {
	 /* .source "FindDeviceLockWindowImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static com.android.server.wm.WindowState sTmpFirstAppWindow;
private static com.android.server.wm.WindowState sTmpLockWindow;
/* # direct methods */
static com.android.server.wm.WindowState -$$Nest$sfgetsTmpFirstAppWindow ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpFirstAppWindow;
} // .end method
static com.android.server.wm.WindowState -$$Nest$sfgetsTmpLockWindow ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpLockWindow;
} // .end method
static void -$$Nest$sfputsTmpFirstAppWindow ( com.android.server.wm.WindowState p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 return;
} // .end method
static void -$$Nest$sfputsTmpLockWindow ( com.android.server.wm.WindowState p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 return;
} // .end method
public com.android.server.wm.FindDeviceLockWindowImpl ( ) {
	 /* .locals 0 */
	 /* .line 12 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
private static Boolean isObscuringFullScreen ( com.android.server.wm.WindowState p0, android.view.WindowManager$LayoutParams p1 ) {
	 /* .locals 3 */
	 /* .param p0, "win" # Lcom/android/server/wm/WindowState; */
	 /* .param p1, "params" # Landroid/view/WindowManager$LayoutParams; */
	 /* .line 110 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* if-nez p0, :cond_0 */
	 /* .line 111 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 113 */
} // :cond_1
v1 = (( com.android.server.wm.WindowState ) p0 ).isObscuringDisplay ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isObscuringDisplay()Z
/* if-nez v1, :cond_2 */
/* .line 114 */
} // :cond_2
/* iget v1, p1, Landroid/view/WindowManager$LayoutParams;->x:I */
/* if-nez v1, :cond_3 */
/* iget v1, p1, Landroid/view/WindowManager$LayoutParams;->y:I */
/* if-nez v1, :cond_3 */
/* iget v1, p1, Landroid/view/WindowManager$LayoutParams;->width:I */
int v2 = -1; // const/4 v2, -0x1
/* if-ne v1, v2, :cond_3 */
/* iget v1, p1, Landroid/view/WindowManager$LayoutParams;->height:I */
/* if-ne v1, v2, :cond_3 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_3
} // .end method
/* # virtual methods */
public void updateLockDeviceWindowLocked ( com.android.server.wm.WindowManagerService p0, com.android.server.wm.DisplayContent p1 ) {
/* .locals 7 */
/* .param p1, "wms" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "dc" # Lcom/android/server/wm/DisplayContent; */
/* .line 18 */
if ( p1 != null) { // if-eqz p1, :cond_6
/* if-nez p2, :cond_0 */
/* goto/16 :goto_2 */
/* .line 21 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* new-instance v1, Lcom/android/server/wm/FindDeviceLockWindowImpl$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/FindDeviceLockWindowImpl$1;-><init>(Lcom/android/server/wm/FindDeviceLockWindowImpl;)V */
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.wm.DisplayContent ) p2 ).forAllWindows ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Lcom/android/server/wm/DisplayContent;->forAllWindows(Lcom/android/internal/util/ToBooleanFunction;Z)Z
/* .line 46 */
v1 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpLockWindow;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v1, :cond_1 */
/* .line 103 */
/* .line 104 */
/* .line 47 */
return;
/* .line 50 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 51 */
/* .local v1, "hideLockWindow":Z */
try { // :try_start_1
v3 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpFirstAppWindow;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 52 */
v3 = this.mAttrs;
/* iget v3, v3, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* and-int/lit16 v3, v3, 0x1000 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 54 */
v3 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpFirstAppWindow;
v3 = (( com.android.server.wm.WindowState ) v3 ).isVisible ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->isVisible()Z
if ( v3 != null) { // if-eqz v3, :cond_3
	 v3 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpFirstAppWindow;
	 v3 = 	 (( com.android.server.wm.WindowState ) v3 ).isDrawn ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->isDrawn()Z
	 if ( v3 != null) { // if-eqz v3, :cond_3
		 /* .line 55 */
		 v3 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpFirstAppWindow;
		 v4 = this.mAttrs;
		 v3 = 		 com.android.server.wm.FindDeviceLockWindowImpl .isObscuringFullScreen ( v3,v4 );
		 if ( v3 != null) { // if-eqz v3, :cond_2
			 /* .line 56 */
			 int v1 = 1; // const/4 v1, 0x1
			 /* .line 57 */
		 } // :cond_2
		 v3 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpFirstAppWindow;
		 v3 = 		 (( com.android.server.wm.WindowState ) v3 ).inFreeformWindowingMode ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->inFreeformWindowingMode()Z
		 if ( v3 != null) { // if-eqz v3, :cond_3
			 /* .line 58 */
			 v3 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpFirstAppWindow;
			 (( com.android.server.wm.WindowState ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
			 /* .line 59 */
			 /* .local v3, "task":Lcom/android/server/wm/Task; */
			 if ( v3 != null) { // if-eqz v3, :cond_3
				 /* .line 60 */
				 v4 = this.mAtmService;
				 v4 = this.mMiuiFreeFormManagerService;
				 /* iget v5, v3, Lcom/android/server/wm/Task;->mTaskId:I */
				 /* .line 61 */
				 /* .line 62 */
				 /* .local v4, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStackStub; */
				 if ( v4 != null) { // if-eqz v4, :cond_3
					 /* .line 63 */
					 v5 = this.mAtmService;
					 v5 = this.mMiuiFreeFormManagerService;
					 /* iget v6, v3, Lcom/android/server/wm/Task;->mTaskId:I */
					 /* .line 85 */
				 } // .end local v3 # "task":Lcom/android/server/wm/Task;
			 } // .end local v4 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
		 } // :cond_3
	 } // :goto_0
	 int v3 = 0; // const/4 v3, 0x0
	 if ( v1 != null) { // if-eqz v1, :cond_4
		 /* .line 86 */
		 v4 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpLockWindow;
		 v3 = 		 (( com.android.server.wm.WindowState ) v4 ).hide ( v3, v3 ); // invoke-virtual {v4, v3, v3}, Lcom/android/server/wm/WindowState;->hide(ZZ)Z
		 /* .local v3, "change":Z */
		 /* .line 88 */
	 } // .end local v3 # "change":Z
} // :cond_4
v4 = com.android.server.wm.FindDeviceLockWindowImpl.sTmpLockWindow;
v3 = (( com.android.server.wm.WindowState ) v4 ).show ( v3, v3 ); // invoke-virtual {v4, v3, v3}, Lcom/android/server/wm/WindowState;->show(ZZ)Z
/* .line 91 */
/* .restart local v3 # "change":Z */
} // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 92 */
/* iput-boolean v2, p1, Lcom/android/server/wm/WindowManagerService;->mFocusMayChange:Z */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 103 */
} // .end local v1 # "hideLockWindow":Z
} // .end local v3 # "change":Z
} // :cond_5
/* .line 104 */
/* .line 105 */
/* nop */
/* .line 106 */
return;
/* .line 103 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 104 */
/* .line 105 */
/* throw v1 */
/* .line 18 */
} // :cond_6
} // :goto_2
return;
} // .end method
