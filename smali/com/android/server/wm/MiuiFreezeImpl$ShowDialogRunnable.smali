.class Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;
.super Ljava/lang/Object;
.source "MiuiFreezeImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiFreezeImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowDialogRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiFreezeImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    .line 482
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 486
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fgetmLoadingDialog(Lcom/android/server/wm/MiuiFreezeImpl;)Lcom/android/server/wm/MiuiLoadingDialog;

    move-result-object v0

    const-string v1, "MiuiFreezeImpl"

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fgetmReceiver(Lcom/android/server/wm/MiuiFreezeImpl;)Landroid/content/BroadcastReceiver;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    new-instance v2, Lcom/android/server/wm/MiuiLoadingDialog;

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-static {v3}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fgetmUiContext(Lcom/android/server/wm/MiuiFreezeImpl;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/server/wm/MiuiLoadingDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v2}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fputmLoadingDialog(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiLoadingDialog;)V

    .line 491
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fgetmLoadingDialog(Lcom/android/server/wm/MiuiFreezeImpl;)Lcom/android/server/wm/MiuiLoadingDialog;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-static {v2}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fgetdialogShowListener(Lcom/android/server/wm/MiuiFreezeImpl;)Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiLoadingDialog;->setOnShowAnimListener(Lmiuix/appcompat/app/AlertDialog$OnDialogShowAnimListener;)V

    .line 492
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fgetmLoadingDialog(Lcom/android/server/wm/MiuiFreezeImpl;)Lcom/android/server/wm/MiuiLoadingDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiLoadingDialog;->show()V

    .line 493
    const-string/jumbo v0, "start show dialog"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    return-void

    .line 487
    :cond_1
    :goto_0
    const-string v0, "dialog is showing or mReceiver is null "

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    return-void
.end method
