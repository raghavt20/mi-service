class com.android.server.wm.AppTransitionInjector$BezierXAnimation extends android.view.animation.Animation {
	 /* .source "AppTransitionInjector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AppTransitionInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "BezierXAnimation" */
} // .end annotation
/* # instance fields */
private Float mEndX;
private Float mFromX;
private Float mInertiaX;
private Float mPivotX;
private Float mStartX;
private Float mToX;
private Integer mWidth;
/* # direct methods */
public com.android.server.wm.AppTransitionInjector$BezierXAnimation ( ) {
/* .locals 6 */
/* .param p1, "fromX" # F */
/* .param p2, "toX" # F */
/* .param p3, "pivotX" # F */
/* .param p4, "startX" # F */
/* .param p5, "endX" # F */
/* .param p6, "inertia" # Landroid/graphics/Point; */
/* .param p7, "width" # I */
/* .line 1439 */
/* invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V */
/* .line 1440 */
/* iput p4, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mStartX:F */
/* .line 1441 */
/* iput p5, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mEndX:F */
/* .line 1442 */
/* float-to-double v0, p4 */
/* iget v2, p6, Landroid/graphics/Point;->x:I */
/* int-to-double v2, v2 */
/* const-wide v4, 0x3ff3333333333333L # 1.2 */
/* mul-double/2addr v2, v4 */
/* add-double/2addr v0, v2 */
/* double-to-float v0, v0 */
/* iput v0, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mInertiaX:F */
/* .line 1443 */
/* iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mFromX:F */
/* .line 1444 */
/* iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mToX:F */
/* .line 1445 */
/* iput p3, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mPivotX:F */
/* .line 1446 */
/* iput p7, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mWidth:I */
/* .line 1447 */
return;
} // .end method
public com.android.server.wm.AppTransitionInjector$BezierXAnimation ( ) {
/* .locals 8 */
/* .param p1, "fromX" # F */
/* .param p2, "toX" # F */
/* .param p3, "startX" # F */
/* .param p4, "endX" # F */
/* .param p5, "inertia" # Landroid/graphics/Point; */
/* .param p6, "width" # I */
/* .line 1435 */
int v3 = 0; // const/4 v3, 0x0
/* move-object v0, p0 */
/* move v1, p1 */
/* move v2, p2 */
/* move v4, p3 */
/* move v5, p4 */
/* move-object v6, p5 */
/* move v7, p6 */
/* invoke-direct/range {v0 ..v7}, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;-><init>(FFFFFLandroid/graphics/Point;I)V */
/* .line 1436 */
return;
} // .end method
/* # virtual methods */
protected void applyTransformation ( Float p0, android.view.animation.Transformation p1 ) {
/* .locals 11 */
/* .param p1, "interpolatedTime" # F */
/* .param p2, "t" # Landroid/view/animation/Transformation; */
/* .line 1451 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 1452 */
/* .local v0, "sx":F */
v1 = (( com.android.server.wm.AppTransitionInjector$BezierXAnimation ) p0 ).getScaleFactor ( ); // invoke-virtual {p0}, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->getScaleFactor()F
/* .line 1454 */
/* .local v1, "scale":F */
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mFromX:F */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* cmpl-float v4, v2, v3 */
/* if-nez v4, :cond_0 */
/* iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mToX:F */
/* cmpl-float v4, v4, v3 */
if ( v4 != null) { // if-eqz v4, :cond_1
	 /* .line 1455 */
} // :cond_0
/* iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mToX:F */
/* sub-float/2addr v4, v2 */
/* mul-float/2addr v4, p1 */
/* add-float v0, v2, v4 */
/* .line 1458 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mPivotX:F */
int v4 = 0; // const/4 v4, 0x0
/* cmpl-float v2, v2, v4 */
/* if-nez v2, :cond_2 */
/* .line 1459 */
(( android.view.animation.Transformation ) p2 ).getMatrix ( ); // invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
(( android.graphics.Matrix ) v2 ).setScale ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Landroid/graphics/Matrix;->setScale(FF)V
/* .line 1461 */
} // :cond_2
(( android.view.animation.Transformation ) p2 ).getMatrix ( ); // invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
/* iget v5, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mPivotX:F */
/* mul-float/2addr v5, v1 */
(( android.graphics.Matrix ) v2 ).setScale ( v0, v3, v5, v4 ); // invoke-virtual {v2, v0, v3, v5, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V
/* .line 1464 */
} // :goto_0
/* sub-float v2, v3, p1 */
/* float-to-double v5, v2 */
/* const-wide/high16 v7, 0x4000000000000000L # 2.0 */
java.lang.Math .pow ( v5,v6,v7,v8 );
/* move-result-wide v5 */
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mStartX:F */
/* float-to-double v9, v2 */
/* mul-double/2addr v5, v9 */
/* float-to-double v9, p1 */
/* .line 1465 */
java.lang.Math .pow ( v9,v10,v7,v8 );
/* move-result-wide v7 */
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mEndX:F */
/* float-to-double v9, v2 */
/* mul-double/2addr v7, v9 */
/* add-double/2addr v5, v7 */
/* const/high16 v2, 0x40000000 # 2.0f */
/* mul-float v7, p1, v2 */
/* sub-float/2addr v3, p1 */
/* mul-float/2addr v7, v3 */
/* iget v3, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mInertiaX:F */
/* mul-float/2addr v7, v3 */
/* float-to-double v7, v7 */
/* add-double/2addr v5, v7 */
/* .line 1467 */
/* .local v5, "targetX":D */
(( android.view.animation.Transformation ) p2 ).getMatrix ( ); // invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
/* double-to-float v7, v5 */
/* iget v8, p0, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;->mWidth:I */
/* int-to-float v8, v8 */
/* mul-float/2addr v8, v0 */
/* div-float/2addr v8, v2 */
/* sub-float/2addr v7, v8 */
(( android.graphics.Matrix ) v3 ).postTranslate ( v7, v4 ); // invoke-virtual {v3, v7, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z
/* .line 1468 */
return;
} // .end method
