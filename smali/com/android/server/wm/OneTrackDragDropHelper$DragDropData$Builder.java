public class com.android.server.wm.OneTrackDragDropHelper$DragDropData$Builder {
	 /* .source "OneTrackDragDropHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Builder" */
} // .end annotation
/* # instance fields */
private final android.content.ClipData mClipData;
private final android.content.ContentResolver mContentResolver;
private final com.android.server.wm.WindowState mDragWindow;
private final com.android.server.wm.WindowState mDropWindow;
private final Boolean mResult;
/* # direct methods */
 com.android.server.wm.OneTrackDragDropHelper$DragDropData$Builder ( ) {
/* .locals 0 */
/* .param p1, "dragWindow" # Lcom/android/server/wm/WindowState; */
/* .param p2, "dropWindow" # Lcom/android/server/wm/WindowState; */
/* .param p3, "result" # Z */
/* .param p4, "clipData" # Landroid/content/ClipData; */
/* .param p5, "contentResolver" # Landroid/content/ContentResolver; */
/* .line 126 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 127 */
this.mDragWindow = p1;
/* .line 128 */
this.mDropWindow = p2;
/* .line 129 */
/* iput-boolean p3, p0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mResult:Z */
/* .line 130 */
this.mClipData = p4;
/* .line 131 */
this.mContentResolver = p5;
/* .line 132 */
return;
} // .end method
/* # virtual methods */
public com.android.server.wm.OneTrackDragDropHelper$DragDropData build ( ) {
/* .locals 16 */
/* .line 135 */
/* move-object/from16 v0, p0 */
v1 = this.mDragWindow;
(( com.android.server.wm.WindowState ) v1 ).getOwningPackage ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
/* if-nez v1, :cond_0 */
/* move-object v1, v2 */
} // :cond_0
v1 = this.mDragWindow;
(( com.android.server.wm.WindowState ) v1 ).getOwningPackage ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* .line 136 */
/* .local v1, "dragPackage":Ljava/lang/String; */
} // :goto_0
v3 = this.mDropWindow;
/* if-nez v3, :cond_1 */
} // :goto_1
} // :cond_1
(( com.android.server.wm.WindowState ) v3 ).getOwningPackage ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* if-nez v3, :cond_2 */
} // :cond_2
v2 = this.mDropWindow;
(( com.android.server.wm.WindowState ) v2 ).getOwningPackage ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* .line 138 */
/* .local v2, "dropPackage":Ljava/lang/String; */
} // :goto_2
/* iget-boolean v3, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mResult:Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* const-string/jumbo v3, "\u6210\u529f" */
} // :cond_3
/* const-string/jumbo v3, "\u5931\u8d25" */
/* .line 140 */
/* .local v3, "result":Ljava/lang/String; */
} // :goto_3
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "hasText":Z */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "hasImage":Z */
int v6 = 0; // const/4 v6, 0x0
/* .line 141 */
/* .local v6, "hasFile":Z */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
v8 = this.mClipData;
/* if-nez v8, :cond_4 */
int v8 = 0; // const/4 v8, 0x0
} // :cond_4
v8 = (( android.content.ClipData ) v8 ).getItemCount ( ); // invoke-virtual {v8}, Landroid/content/ClipData;->getItemCount()I
/* .local v8, "count":I */
} // :goto_4
final String v10 = "image/*"; // const-string v10, "image/*"
/* if-ge v7, v8, :cond_c */
/* .line 142 */
if ( v4 != null) { // if-eqz v4, :cond_5
if ( v5 != null) { // if-eqz v5, :cond_5
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 143 */
/* .line 145 */
} // :cond_5
v11 = this.mClipData;
(( android.content.ClipData ) v11 ).getItemAt ( v7 ); // invoke-virtual {v11, v7}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;
/* .line 146 */
/* .local v11, "item":Landroid/content/ClipData$Item; */
(( android.content.ClipData$Item ) v11 ).getText ( ); // invoke-virtual {v11}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;
/* if-nez v12, :cond_6 */
(( android.content.ClipData$Item ) v11 ).getHtmlText ( ); // invoke-virtual {v11}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;
if ( v12 != null) { // if-eqz v12, :cond_7
/* .line 147 */
} // :cond_6
int v4 = 1; // const/4 v4, 0x1
/* .line 149 */
} // :cond_7
(( android.content.ClipData$Item ) v11 ).getUri ( ); // invoke-virtual {v11}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;
/* .line 150 */
/* .local v12, "uri":Landroid/net/Uri; */
if ( v12 != null) { // if-eqz v12, :cond_b
/* .line 151 */
int v13 = 0; // const/4 v13, 0x0
/* .line 152 */
/* .local v13, "mimeType":Ljava/lang/String; */
final String v14 = "content"; // const-string v14, "content"
(( android.net.Uri ) v12 ).getScheme ( ); // invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
v14 = (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_8
/* .line 153 */
v14 = this.mContentResolver;
(( android.content.ContentResolver ) v14 ).getType ( v12 ); // invoke-virtual {v14, v12}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;
/* .line 154 */
} // :cond_8
final String v14 = "file"; // const-string v14, "file"
(( android.net.Uri ) v12 ).getScheme ( ); // invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
v14 = (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_9
/* .line 155 */
android.webkit.MimeTypeMap .getSingleton ( );
/* new-instance v15, Ljava/io/File; */
/* .line 157 */
(( android.net.Uri ) v12 ).getPath ( ); // invoke-virtual {v12}, Landroid/net/Uri;->getPath()Ljava/lang/String;
/* invoke-direct {v15, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 156 */
android.net.Uri .fromFile ( v15 );
/* .line 157 */
(( android.net.Uri ) v9 ).toString ( ); // invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;
/* .line 156 */
android.webkit.MimeTypeMap .getFileExtensionFromUrl ( v9 );
/* .line 155 */
(( android.webkit.MimeTypeMap ) v14 ).getMimeTypeFromExtension ( v9 ); // invoke-virtual {v14, v9}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
/* .line 159 */
} // :cond_9
} // :goto_5
if ( v13 != null) { // if-eqz v13, :cond_b
/* .line 160 */
v9 = android.content.ClipDescription .compareMimeTypes ( v13,v10 );
if ( v9 != null) { // if-eqz v9, :cond_a
/* .line 161 */
int v5 = 1; // const/4 v5, 0x1
/* .line 163 */
} // :cond_a
int v6 = 1; // const/4 v6, 0x1
/* .line 141 */
} // .end local v11 # "item":Landroid/content/ClipData$Item;
} // .end local v12 # "uri":Landroid/net/Uri;
} // .end local v13 # "mimeType":Ljava/lang/String;
} // :cond_b
} // :goto_6
/* add-int/lit8 v7, v7, 0x1 */
/* .line 168 */
} // .end local v7 # "i":I
} // .end local v8 # "count":I
} // :cond_c
} // :goto_7
int v7 = 0; // const/4 v7, 0x0
/* .restart local v7 # "i":I */
v8 = this.mClipData;
/* if-nez v8, :cond_d */
int v9 = 0; // const/4 v9, 0x0
} // :cond_d
(( android.content.ClipData ) v8 ).getDescription ( ); // invoke-virtual {v8}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;
v9 = (( android.content.ClipDescription ) v8 ).getMimeTypeCount ( ); // invoke-virtual {v8}, Landroid/content/ClipDescription;->getMimeTypeCount()I
} // :goto_8
/* move v8, v9 */
/* .restart local v8 # "count":I */
} // :goto_9
/* if-ge v7, v8, :cond_11 */
/* .line 169 */
if ( v4 != null) { // if-eqz v4, :cond_e
if ( v5 != null) { // if-eqz v5, :cond_e
/* .line 170 */
/* .line 172 */
} // :cond_e
v9 = this.mClipData;
(( android.content.ClipData ) v9 ).getDescription ( ); // invoke-virtual {v9}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;
(( android.content.ClipDescription ) v9 ).getMimeType ( v7 ); // invoke-virtual {v9, v7}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;
/* .line 173 */
/* .local v9, "mimeType":Ljava/lang/String; */
/* const-string/jumbo v11, "text/*" */
v11 = android.content.ClipDescription .compareMimeTypes ( v9,v11 );
if ( v11 != null) { // if-eqz v11, :cond_f
/* .line 174 */
int v4 = 1; // const/4 v4, 0x1
/* .line 175 */
} // :cond_f
v11 = android.content.ClipDescription .compareMimeTypes ( v9,v10 );
if ( v11 != null) { // if-eqz v11, :cond_10
/* .line 176 */
int v5 = 1; // const/4 v5, 0x1
/* .line 168 */
} // .end local v9 # "mimeType":Ljava/lang/String;
} // :cond_10
} // :goto_a
/* add-int/lit8 v7, v7, 0x1 */
/* .line 179 */
} // .end local v7 # "i":I
} // .end local v8 # "count":I
} // :cond_11
} // :goto_b
/* new-instance v7, Ljava/util/ArrayList; */
/* invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V */
/* .line 180 */
/* .local v7, "contentStyles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
if ( v4 != null) { // if-eqz v4, :cond_12
/* const-string/jumbo v8, "\u6587\u5b57" */
(( java.util.ArrayList ) v7 ).add ( v8 ); // invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 181 */
} // :cond_12
if ( v5 != null) { // if-eqz v5, :cond_13
/* const-string/jumbo v8, "\u56fe\u7247" */
(( java.util.ArrayList ) v7 ).add ( v8 ); // invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 182 */
} // :cond_13
if ( v6 != null) { // if-eqz v6, :cond_14
/* const-string/jumbo v8, "\u6587\u4ef6" */
(( java.util.ArrayList ) v7 ).add ( v8 ); // invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 183 */
} // :cond_14
final String v8 = ","; // const-string v8, ","
java.lang.String .join ( v8,v7 );
/* .line 185 */
/* .local v8, "contentStyle":Ljava/lang/String; */
/* new-instance v9, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData; */
/* invoke-direct {v9, v1, v2, v3, v8}, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
} // .end method
