class com.android.server.wm.MiuiMultiWindowRecommendController$10 implements java.lang.Runnable {
	 /* .source "MiuiMultiWindowRecommendController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendController this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendController$10 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendController; */
/* .line 411 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 414 */
v0 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 416 */
	 int v0 = 0; // const/4 v0, 0x0
	 int v1 = 0; // const/4 v1, 0x0
	 try { // :try_start_0
		 v2 = this.this$0;
		 v2 = this.mWindowManager;
		 v3 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v3 );
		 /* .line 417 */
		 v2 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmLayoutParams ( v2,v1 );
		 /* .line 418 */
		 v2 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmSplitScreenRecommendLayout ( v2,v1 );
		 /* .line 419 */
		 v2 = this.this$0;
		 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v2 ).setSplitScreenRecommendState ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setSplitScreenRecommendState(Z)V
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 425 */
		 /* .line 420 */
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 421 */
		 /* .local v2, "e":Ljava/lang/Exception; */
		 v3 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmLayoutParams ( v3,v1 );
		 /* .line 422 */
		 v3 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmSplitScreenRecommendLayout ( v3,v1 );
		 /* .line 423 */
		 v1 = this.this$0;
		 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v1 ).setSplitScreenRecommendState ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setSplitScreenRecommendState(Z)V
		 /* .line 424 */
		 final String v0 = "MiuiMultiWindowRecommendController"; // const-string v0, "MiuiMultiWindowRecommendController"
		 final String v1 = " removeSplitScreenRecommendView fail"; // const-string v1, " removeSplitScreenRecommendView fail"
		 android.util.Slog .d ( v0,v1,v2 );
		 /* .line 427 */
	 } // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
