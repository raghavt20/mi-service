.class Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;
.super Landroid/view/animation/Animation;
.source "ScreenRotationAnimationImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/ScreenRotationAnimationImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShotClipAnimation"
.end annotation


# instance fields
.field private mCurHeight:I

.field private mCurWidth:I

.field protected final mFromRect:Landroid/graphics/Rect;

.field private mFromScale:F

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mPivotX:F

.field private mPivotXType:I

.field private mPivotXValue:F

.field private mPivotY:F

.field private mPivotYType:I

.field private mPivotYValue:F

.field private mShotBottom:I

.field private mShotLeft:I

.field private mShotRight:I

.field private mShotScale:F

.field private mShotTop:I

.field protected final mToRect:Landroid/graphics/Rect;

.field private mToScale:F


# direct methods
.method public constructor <init>(IIFFIFIFIIIIIIII)V
    .locals 16
    .param p1, "curWidth"    # I
    .param p2, "curHeight"    # I
    .param p3, "fromScale"    # F
    .param p4, "toScale"    # F
    .param p5, "pivotXType"    # I
    .param p6, "pivotXValue"    # F
    .param p7, "pivotYType"    # I
    .param p8, "pivotYValue"    # F
    .param p9, "fromL"    # I
    .param p10, "fromT"    # I
    .param p11, "fromR"    # I
    .param p12, "fromB"    # I
    .param p13, "toL"    # I
    .param p14, "toT"    # I
    .param p15, "toR"    # I
    .param p16, "toB"    # I

    .line 562
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Landroid/view/animation/Animation;-><init>()V

    .line 543
    const/4 v1, 0x0

    iput v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotXType:I

    .line 544
    iput v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotYType:I

    .line 545
    const/4 v1, 0x0

    iput v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotXValue:F

    .line 546
    iput v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotYValue:F

    .line 563
    new-instance v1, Landroid/graphics/Rect;

    move/from16 v2, p9

    move/from16 v3, p10

    move/from16 v4, p11

    move/from16 v5, p12

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mFromRect:Landroid/graphics/Rect;

    .line 564
    new-instance v1, Landroid/graphics/Rect;

    move/from16 v6, p13

    move/from16 v7, p14

    move/from16 v8, p15

    move/from16 v9, p16

    invoke-direct {v1, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToRect:Landroid/graphics/Rect;

    .line 565
    move/from16 v1, p3

    iput v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mFromScale:F

    .line 566
    move/from16 v10, p4

    iput v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToScale:F

    .line 567
    move/from16 v11, p6

    iput v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotXValue:F

    .line 568
    move/from16 v12, p5

    iput v12, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotXType:I

    .line 569
    move/from16 v13, p8

    iput v13, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotYValue:F

    .line 570
    move/from16 v14, p7

    iput v14, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotYType:I

    .line 571
    move/from16 v15, p1

    iput v15, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mCurWidth:I

    .line 572
    move/from16 v1, p2

    iput v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mCurHeight:I

    .line 573
    invoke-direct/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->initializePivotPoint()V

    .line 574
    return-void
.end method

.method private initializePivotPoint()V
    .locals 1

    .line 577
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotXType:I

    if-nez v0, :cond_0

    .line 578
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotXValue:F

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotX:F

    .line 580
    :cond_0
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotYType:I

    if-nez v0, :cond_1

    .line 581
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotYValue:F

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotY:F

    .line 583
    :cond_1
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5
    .param p1, "interpolatedTime"    # F
    .param p2, "tr"    # Landroid/view/animation/Transformation;

    .line 649
    invoke-virtual {p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->getScaleFactor()F

    move-result v0

    .line 650
    .local v0, "scale":F
    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotLeft:I

    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotTop:I

    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotRight:I

    iget v4, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotBottom:I

    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/view/animation/Transformation;->setClipRect(IIII)V

    .line 651
    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotX:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotY:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 652
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotScale:F

    invoke-virtual {v1, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    goto :goto_0

    .line 654
    :cond_0
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotScale:F

    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotX:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotY:F

    mul-float/2addr v4, v0

    invoke-virtual {v1, v2, v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 656
    :goto_0
    return-void
.end method

.method public getTransformation(JLandroid/view/animation/Transformation;)Z
    .locals 25
    .param p1, "currentTime"    # J
    .param p3, "outTransformation"    # Landroid/view/animation/Transformation;

    .line 592
    move-object/from16 v0, p0

    const/4 v1, 0x0

    .line 594
    .local v1, "more":Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->getStartOffset()J

    move-result-wide v2

    .line 595
    .local v2, "startOffset":J
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->getDuration()J

    move-result-wide v4

    .line 596
    .local v4, "duration":J
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->getStartTime()J

    move-result-wide v6

    .line 598
    .local v6, "startTime":J
    const/4 v8, 0x0

    .line 599
    .local v8, "interpolatedTime":F
    const-wide/16 v9, 0x0

    cmp-long v9, v4, v9

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    if-eqz v9, :cond_0

    .line 600
    add-long v12, v6, v2

    sub-long v12, p1, v12

    long-to-float v9, v12

    long-to-float v12, v4

    div-float/2addr v9, v12

    .local v9, "normalizedTime":F
    goto :goto_0

    .line 603
    .end local v9    # "normalizedTime":F
    :cond_0
    cmp-long v9, p1, v6

    if-gez v9, :cond_1

    move v9, v10

    goto :goto_0

    :cond_1
    move v9, v11

    .line 606
    .restart local v9    # "normalizedTime":F
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->getFillAfter()Z

    move-result v12

    .line 607
    .local v12, "fillAfter":Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->getFillBefore()Z

    move-result v13

    .line 608
    .local v13, "fillBefore":Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->isFillEnabled()Z

    move-result v14

    .line 610
    .local v14, "fillEnabled":Z
    if-nez v14, :cond_2

    invoke-static {v9, v11}, Ljava/lang/Math;->min(FF)F

    move-result v15

    invoke-static {v15, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 612
    :cond_2
    cmpl-float v15, v9, v10

    if-gez v15, :cond_3

    if-eqz v13, :cond_4

    :cond_3
    cmpg-float v15, v9, v11

    if-lez v15, :cond_5

    if-eqz v12, :cond_4

    goto :goto_1

    .line 643
    :cond_4
    move/from16 v17, v1

    move-wide/from16 v18, v2

    move-wide/from16 v20, v4

    goto/16 :goto_3

    .line 613
    :cond_5
    :goto_1
    if-eqz v14, :cond_6

    .line 614
    invoke-static {v9, v11}, Ljava/lang/Math;->min(FF)F

    move-result v15

    invoke-static {v15, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 615
    :cond_6
    iget-object v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v10, v9}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v8

    .line 617
    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mCurWidth:I

    iget v15, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mCurHeight:I

    invoke-static {v10, v15}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 618
    .local v10, "shortLength":I
    iget v15, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToScale:F

    iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mFromScale:F

    sub-float/2addr v11, v15

    mul-float/2addr v11, v8

    add-float/2addr v15, v11

    .line 619
    .local v15, "appScale":F
    iget-object v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    move/from16 v17, v1

    .end local v1    # "more":Z
    .local v17, "more":Z
    iget-object v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mFromRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    move-wide/from16 v18, v2

    .end local v2    # "startOffset":J
    .local v18, "startOffset":J
    iget-object v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, v8

    float-to-int v1, v1

    add-int/2addr v11, v1

    .line 621
    .local v11, "appLeft":I
    iget-object v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mFromRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, v8

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 623
    .local v1, "appTop":I
    iget-object v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mFromRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    move-wide/from16 v20, v4

    .end local v4    # "duration":J
    .local v20, "duration":J
    iget-object v4, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v8

    float-to-int v3, v3

    add-int/2addr v2, v3

    .line 625
    .local v2, "appRight":I
    iget-object v3, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mFromRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mToRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    mul-float/2addr v4, v8

    float-to-int v4, v4

    add-int/2addr v3, v4

    .line 627
    .local v3, "appBottom":I
    iget v4, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mCurWidth:I

    iget v5, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mCurHeight:I

    if-le v4, v5, :cond_7

    .line 628
    sub-int v22, v2, v11

    goto :goto_2

    :cond_7
    sub-int v22, v3, v1

    :goto_2
    move/from16 v23, v22

    .line 629
    .local v23, "appLength":I
    move/from16 v22, v1

    move/from16 v1, v23

    move/from16 v23, v2

    .end local v2    # "appRight":I
    .local v1, "appLength":I
    .local v22, "appTop":I
    .local v23, "appRight":I
    int-to-float v2, v1

    mul-float/2addr v2, v15

    move/from16 v24, v1

    .end local v1    # "appLength":I
    .local v24, "appLength":I
    int-to-float v1, v10

    const/high16 v16, 0x3f800000    # 1.0f

    mul-float v1, v1, v16

    div-float/2addr v2, v1

    iput v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotScale:F

    .line 630
    int-to-float v1, v10

    mul-float/2addr v1, v15

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 631
    .local v1, "shotClipLength":I
    if-le v4, v5, :cond_8

    .line 632
    sub-int v16, v4, v1

    div-int/lit8 v2, v16, 0x2

    iput v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotLeft:I

    .line 633
    const/4 v2, 0x0

    iput v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotTop:I

    .line 634
    add-int/2addr v4, v1

    div-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotRight:I

    .line 635
    iput v5, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotBottom:I

    goto :goto_3

    .line 637
    :cond_8
    const/4 v2, 0x0

    iput v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotLeft:I

    .line 638
    sub-int v2, v5, v1

    div-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotTop:I

    .line 639
    iput v4, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotRight:I

    .line 640
    add-int/2addr v5, v1

    div-int/lit8 v5, v5, 0x2

    iput v5, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mShotBottom:I

    .line 643
    .end local v1    # "shotClipLength":I
    .end local v3    # "appBottom":I
    .end local v10    # "shortLength":I
    .end local v11    # "appLeft":I
    .end local v15    # "appScale":F
    .end local v22    # "appTop":I
    .end local v23    # "appRight":I
    .end local v24    # "appLength":I
    :goto_3
    invoke-super/range {p0 .. p3}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    move-result v1

    .line 644
    .end local v17    # "more":Z
    .local v1, "more":Z
    return v1
.end method

.method public initialize(IIII)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "parentWidth"    # I
    .param p4, "parentHeight"    # I

    .line 660
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 661
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotXType:I

    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotXValue:F

    invoke-virtual {p0, v0, v1, p1, p3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->resolveSize(IFII)F

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotX:F

    .line 662
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotYType:I

    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotYValue:F

    invoke-virtual {p0, v0, v1, p2, p4}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->resolveSize(IFII)F

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mPivotY:F

    .line 663
    return-void
.end method

.method public setInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 0
    .param p1, "interpolator"    # Landroid/view/animation/Interpolator;

    .line 587
    iput-object p1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 588
    return-void
.end method
