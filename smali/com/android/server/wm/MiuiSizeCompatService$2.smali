.class Lcom/android/server/wm/MiuiSizeCompatService$2;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "MiuiSizeCompatService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiSizeCompatService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiSizeCompatService;

    .line 517
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 4
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 520
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    const-string v1, "exit foregroud"

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmNotificationShowing(Lcom/android/server/wm/MiuiSizeCompatService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->cancelSizeCompatNotification(Ljava/lang/String;)V

    .line 523
    :cond_1
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget-object v2, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 524
    sget-boolean v0, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Last is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v2}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " and current is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "MiuiSizeCompatService"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmAtms(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityTaskManagerService;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getTopResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 528
    .local v0, "top":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v2}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetPERMISSION_ACTIVITY(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 529
    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService$2;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->cancelSizeCompatNotification(Ljava/lang/String;)V

    .line 532
    .end local v0    # "top":Lcom/android/server/wm/ActivityRecord;
    :cond_3
    return-void
.end method
