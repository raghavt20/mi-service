public class com.android.server.wm.WindowContainerImpl implements com.android.server.wm.WindowContainerStub {
	 /* .source "WindowContainerImpl.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private android.view.animation.Animation mSplitDimmer;
	 private com.android.server.wm.WindowContainer wc;
	 /* # direct methods */
	 public com.android.server.wm.WindowContainerImpl ( ) {
		 /* .locals 0 */
		 /* .line 11 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void clearTransitionDimmer ( ) {
		 /* .locals 3 */
		 /* .line 24 */
		 v0 = this.mSplitDimmer;
		 /* if-nez v0, :cond_0 */
		 /* .line 25 */
		 return;
		 /* .line 27 */
	 } // :cond_0
	 com.android.server.wm.AppTransitionStub .get ( );
	 v1 = this.mSplitDimmer;
	 v2 = this.wc;
	 /* .line 28 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mSplitDimmer = v0;
	 /* .line 29 */
	 return;
} // .end method
public android.view.animation.Animation getSplitDimmer ( ) {
	 /* .locals 1 */
	 /* .line 55 */
	 v0 = this.mSplitDimmer;
} // .end method
public void init ( com.android.server.wm.WindowContainer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "container" # Lcom/android/server/wm/WindowContainer; */
	 /* .line 18 */
	 this.wc = p1;
	 /* .line 19 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mSplitDimmer = v0;
	 /* .line 20 */
	 return;
} // .end method
public void setSplitDimmer ( android.view.animation.Animation p0 ) {
	 /* .locals 5 */
	 /* .param p1, "a" # Landroid/view/animation/Animation; */
	 /* .line 33 */
	 (( com.android.server.wm.WindowContainerImpl ) p0 ).clearTransitionDimmer ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowContainerImpl;->clearTransitionDimmer()V
	 /* .line 34 */
	 /* instance-of v0, p1, Landroid/view/animation/AnimationSet; */
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 35 */
		 /* move-object v0, p1 */
		 /* check-cast v0, Landroid/view/animation/AnimationSet; */
		 (( android.view.animation.AnimationSet ) v0 ).getAnimations ( ); // invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;
		 /* .line 36 */
		 /* .local v0, "animations":Ljava/util/List;, "Ljava/util/List<Landroid/view/animation/Animation;>;" */
			 v1 = 		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* if-lez v1, :cond_1 */
			 /* .line 37 */
			 int v1 = 0; // const/4 v1, 0x0
			 /* .local v1, "i":I */
		 v2 = 		 } // :goto_0
		 /* if-ge v1, v2, :cond_1 */
		 /* .line 38 */
		 /* check-cast v2, Landroid/view/animation/Animation; */
		 /* .line 39 */
		 /* .local v2, "animation":Landroid/view/animation/Animation; */
		 com.android.server.wm.AppTransitionStub .get ( );
		 v4 = this.wc;
		 /* .line 40 */
		 /* .line 41 */
		 /* .local v3, "splitDimmer":Landroid/view/animation/Animation; */
		 this.mSplitDimmer = v3;
		 /* .line 42 */
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 /* .line 43 */
			 /* .line 37 */
		 } // .end local v2 # "animation":Landroid/view/animation/Animation;
	 } // .end local v3 # "splitDimmer":Landroid/view/animation/Animation;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 47 */
} // .end local v0 # "animations":Ljava/util/List;, "Ljava/util/List<Landroid/view/animation/Animation;>;"
} // .end local v1 # "i":I
} // :cond_1
} // :goto_1
/* .line 48 */
} // :cond_2
com.android.server.wm.AppTransitionStub .get ( );
v1 = this.wc;
/* .line 49 */
/* .local v0, "splitDimmer":Landroid/view/animation/Animation; */
this.mSplitDimmer = v0;
/* .line 51 */
} // .end local v0 # "splitDimmer":Landroid/view/animation/Animation;
} // :goto_2
return;
} // .end method
