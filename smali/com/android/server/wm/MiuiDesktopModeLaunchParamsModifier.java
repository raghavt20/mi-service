public class com.android.server.wm.MiuiDesktopModeLaunchParamsModifier implements com.android.server.wm.LaunchParamsController$LaunchParamsModifier {
	 /* .source "MiuiDesktopModeLaunchParamsModifier.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.lang.StringBuilder mLogBuilder;
	 /* # direct methods */
	 public com.android.server.wm.MiuiDesktopModeLaunchParamsModifier ( ) {
		 /* .locals 0 */
		 /* .line 41 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private void appendLog ( java.lang.String p0, java.lang.Object...p1 ) {
		 /* .locals 2 */
		 /* .param p1, "format" # Ljava/lang/String; */
		 /* .param p2, "args" # [Ljava/lang/Object; */
		 /* .line 151 */
		 v0 = this.mLogBuilder;
		 final String v1 = " "; // const-string v1, " "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 java.lang.String .format ( p1,p2 );
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 152 */
		 return;
	 } // .end method
	 private Integer calculate ( com.android.server.wm.Task p0, android.content.pm.ActivityInfo$WindowLayout p1, com.android.server.wm.ActivityRecord p2, com.android.server.wm.ActivityRecord p3, android.app.ActivityOptions p4, com.android.server.wm.ActivityStarter$Request p5, Integer p6, com.android.server.wm.LaunchParamsController$LaunchParams p7, com.android.server.wm.LaunchParamsController$LaunchParams p8 ) {
		 /* .locals 16 */
		 /* .param p1, "task" # Lcom/android/server/wm/Task; */
		 /* .param p2, "layout" # Landroid/content/pm/ActivityInfo$WindowLayout; */
		 /* .param p3, "activity" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p4, "source" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p5, "options" # Landroid/app/ActivityOptions; */
		 /* .param p6, "request" # Lcom/android/server/wm/ActivityStarter$Request; */
		 /* .param p7, "phase" # I */
		 /* .param p8, "currentParams" # Lcom/android/server/wm/LaunchParamsController$LaunchParams; */
		 /* .param p9, "outParams" # Lcom/android/server/wm/LaunchParamsController$LaunchParams; */
		 /* .line 68 */
		 /* move-object/from16 v1, p0 */
		 /* move-object/from16 v2, p1 */
		 /* move-object/from16 v3, p5 */
		 /* move-object/from16 v4, p8 */
		 /* move-object/from16 v5, p9 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* if-nez v2, :cond_0 */
		 /* .line 69 */
		 /* const-string/jumbo v6, "task null, skipping" */
		 /* new-array v7, v0, [Ljava/lang/Object; */
		 /* invoke-direct {v1, v6, v7}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V */
		 /* .line 70 */
		 /* .line 72 */
	 } // :cond_0
	 v6 = this.mAtmService;
	 v6 = this.mContext;
	 /* .line 73 */
	 /* .local v6, "context":Landroid/content/Context; */
	 v7 = 	 com.android.server.wm.MiuiDesktopModeUtils .isActive ( v6 );
	 /* .line 74 */
	 /* .local v7, "isDesktopModeActive":Z */
	 /* if-nez v7, :cond_1 */
	 /* .line 75 */
	 /* .line 77 */
} // :cond_1
int v8 = 3; // const/4 v8, 0x3
/* move/from16 v9, p7 */
/* if-eq v9, v8, :cond_2 */
/* .line 78 */
final String v8 = "not in bounds phase, skipping"; // const-string v8, "not in bounds phase, skipping"
/* new-array v10, v0, [Ljava/lang/Object; */
/* invoke-direct {v1, v8, v10}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V */
/* .line 79 */
/* .line 81 */
} // :cond_2
v8 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->isActivityTypeStandard()Z */
/* if-nez v8, :cond_3 */
/* .line 82 */
final String v8 = "not standard activity type, skipping"; // const-string v8, "not standard activity type, skipping"
/* new-array v10, v0, [Ljava/lang/Object; */
/* invoke-direct {v1, v8, v10}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V */
/* .line 83 */
/* .line 85 */
} // :cond_3
v8 = this.mBounds;
v8 = (( android.graphics.Rect ) v8 ).isEmpty ( ); // invoke-virtual {v8}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v8, :cond_4 */
/* .line 86 */
final String v8 = "currentParams has bounds set, not overriding"; // const-string v8, "currentParams has bounds set, not overriding"
/* new-array v10, v0, [Ljava/lang/Object; */
/* invoke-direct {v1, v8, v10}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V */
/* .line 87 */
/* .line 91 */
} // :cond_4
(( com.android.server.wm.LaunchParamsController$LaunchParams ) v5 ).set ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/wm/LaunchParamsController$LaunchParams;->set(Lcom/android/server/wm/LaunchParamsController$LaunchParams;)V
/* .line 107 */
int v8 = 1; // const/4 v8, 0x1
if ( v7 != null) { // if-eqz v7, :cond_a
v10 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->isVisible()Z */
/* if-nez v10, :cond_a */
/* .line 108 */
v10 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z */
int v11 = 5; // const/4 v11, 0x5
/* if-nez v10, :cond_5 */
if ( v3 != null) { // if-eqz v3, :cond_a
/* .line 109 */
v10 = /* invoke-virtual/range {p5 ..p5}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I */
/* if-ne v10, v11, :cond_a */
/* .line 110 */
} // :cond_5
v10 = this.origActivity;
if ( v10 != null) { // if-eqz v10, :cond_6
v10 = this.origActivity;
(( android.content.ComponentName ) v10 ).getPackageName ( ); // invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 111 */
} // :cond_6
v10 = this.realActivity;
if ( v10 != null) { // if-eqz v10, :cond_7
v10 = this.realActivity;
(( android.content.ComponentName ) v10 ).getPackageName ( ); // invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 112 */
} // :cond_7
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord; */
if ( v10 != null) { // if-eqz v10, :cond_8
/* .line 113 */
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord; */
v10 = this.packageName;
} // :cond_8
/* const-string/jumbo v10, "unknown" */
} // :goto_0
/* nop */
/* .line 114 */
/* .local v10, "packageName":Ljava/lang/String; */
android.util.MiuiMultiWindowUtils .getActivityOptions ( v6,v10,v8,v0,v7 );
/* .line 117 */
/* .local v12, "freeformLaunchOption":Landroid/app/ActivityOptions; */
if ( v12 != null) { // if-eqz v12, :cond_a
/* .line 118 */
v13 = this.mBounds;
(( android.app.ActivityOptions ) v12 ).getLaunchBounds ( ); // invoke-virtual {v12}, Landroid/app/ActivityOptions;->getLaunchBounds()Landroid/graphics/Rect;
(( android.graphics.Rect ) v13 ).set ( v14 ); // invoke-virtual {v13, v14}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 119 */
/* iput v11, v5, Lcom/android/server/wm/LaunchParamsController$LaunchParams;->mWindowingMode:I */
/* .line 121 */
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 122 */
try { // :try_start_0
final String v13 = "getActivityOptionsInjector"; // const-string v13, "getActivityOptionsInjector"
int v14 = 0; // const/4 v14, 0x0
android.util.MiuiMultiWindowUtils .isMethodExist ( v3,v13,v14 );
/* .line 124 */
/* .local v13, "method":Ljava/lang/reflect/Method; */
if ( v13 != null) { // if-eqz v13, :cond_9
	 /* .line 125 */
	 /* new-array v14, v0, [Ljava/lang/Object; */
	 (( java.lang.reflect.Method ) v13 ).invoke ( v3, v14 ); // invoke-virtual {v13, v3, v14}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
	 /* const-string/jumbo v15, "setFreeformScale" */
	 /* new-array v11, v8, [Ljava/lang/Object; */
	 /* new-array v8, v0, [Ljava/lang/Object; */
	 /* .line 126 */
	 (( java.lang.reflect.Method ) v13 ).invoke ( v12, v8 ); // invoke-virtual {v13, v12, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
	 final String v0 = "getFreeformScale"; // const-string v0, "getFreeformScale"
	 int v2 = 0; // const/4 v2, 0x0
	 /* new-array v4, v2, [Ljava/lang/Object; */
	 android.util.MiuiMultiWindowUtils .invoke ( v8,v0,v4 );
	 /* check-cast v0, Ljava/lang/Float; */
	 v0 = 	 (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
	 java.lang.Float .valueOf ( v0 );
	 int v2 = 0; // const/4 v2, 0x0
	 /* aput-object v0, v11, v2 */
	 /* .line 125 */
	 android.util.MiuiMultiWindowUtils .invoke ( v14,v15,v11 );
	 /* .line 127 */
	 /* new-array v0, v2, [Ljava/lang/Object; */
	 (( java.lang.reflect.Method ) v13 ).invoke ( v3, v0 ); // invoke-virtual {v13, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
	 /* const-string/jumbo v2, "setNormalFreeForm" */
	 int v4 = 1; // const/4 v4, 0x1
	 /* new-array v8, v4, [Ljava/lang/Object; */
	 int v4 = 0; // const/4 v4, 0x0
	 /* new-array v11, v4, [Ljava/lang/Object; */
	 /* .line 128 */
	 (( java.lang.reflect.Method ) v13 ).invoke ( v12, v11 ); // invoke-virtual {v13, v12, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
	 final String v14 = "isNormalFreeForm"; // const-string v14, "isNormalFreeForm"
	 /* new-array v15, v4, [Ljava/lang/Object; */
	 android.util.MiuiMultiWindowUtils .invoke ( v11,v14,v15 );
	 /* check-cast v4, Ljava/lang/Boolean; */
	 v4 = 	 (( java.lang.Boolean ) v4 ).booleanValue ( ); // invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
	 java.lang.Boolean .valueOf ( v4 );
	 int v11 = 0; // const/4 v11, 0x0
	 /* aput-object v4, v8, v11 */
	 /* .line 127 */
	 android.util.MiuiMultiWindowUtils .invoke ( v0,v2,v8 );
	 /* .line 129 */
	 (( android.app.ActivityOptions ) v12 ).getLaunchBounds ( ); // invoke-virtual {v12}, Landroid/app/ActivityOptions;->getLaunchBounds()Landroid/graphics/Rect;
	 (( android.app.ActivityOptions ) v3 ).setLaunchBounds ( v0 ); // invoke-virtual {v3, v0}, Landroid/app/ActivityOptions;->setLaunchBounds(Landroid/graphics/Rect;)Landroid/app/ActivityOptions;
	 /* .line 130 */
	 int v0 = 5; // const/4 v0, 0x5
	 (( android.app.ActivityOptions ) v3 ).setLaunchWindowingMode ( v0 ); // invoke-virtual {v3, v0}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 133 */
} // .end local v13 # "method":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v0 */
/* .line 134 */
} // :cond_9
} // :goto_1
/* nop */
/* .line 138 */
} // .end local v10 # "packageName":Ljava/lang/String;
} // .end local v12 # "freeformLaunchOption":Landroid/app/ActivityOptions;
} // :cond_a
} // :goto_2
v0 = this.mBounds;
/* filled-new-array {v0}, [Ljava/lang/Object; */
/* const-string/jumbo v2, "setting desktop mode task bounds to %s" */
/* invoke-direct {v1, v2, v0}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->appendLog(Ljava/lang/String;[Ljava/lang/Object;)V */
/* .line 140 */
int v2 = 1; // const/4 v2, 0x1
} // .end method
private void initLogBuilder ( com.android.server.wm.Task p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 3 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "activity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 145 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "MiuiDesktopModeLaunchParamsModifier: task="; // const-string v2, "MiuiDesktopModeLaunchParamsModifier: task="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " activity="; // const-string v2, " activity="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
this.mLogBuilder = v0;
/* .line 148 */
return;
} // .end method
private void outputLog ( ) {
/* .locals 2 */
/* .line 155 */
v0 = this.mLogBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiDesktopModeLaunchParamsModifier"; // const-string v1, "MiuiDesktopModeLaunchParamsModifier"
android.util.Slog .d ( v1,v0 );
/* .line 156 */
return;
} // .end method
/* # virtual methods */
public Integer onCalculate ( com.android.server.wm.Task p0, android.content.pm.ActivityInfo$WindowLayout p1, com.android.server.wm.ActivityRecord p2, com.android.server.wm.ActivityRecord p3, android.app.ActivityOptions p4, com.android.server.wm.ActivityStarter$Request p5, Integer p6, com.android.server.wm.LaunchParamsController$LaunchParams p7, com.android.server.wm.LaunchParamsController$LaunchParams p8 ) {
/* .locals 1 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "layout" # Landroid/content/pm/ActivityInfo$WindowLayout; */
/* .param p3, "activity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "source" # Lcom/android/server/wm/ActivityRecord; */
/* .param p5, "options" # Landroid/app/ActivityOptions; */
/* .param p6, "request" # Lcom/android/server/wm/ActivityStarter$Request; */
/* .param p7, "phase" # I */
/* .param p8, "currentParams" # Lcom/android/server/wm/LaunchParamsController$LaunchParams; */
/* .param p9, "outParams" # Lcom/android/server/wm/LaunchParamsController$LaunchParams; */
/* .line 55 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->initLogBuilder(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;)V */
/* .line 56 */
v0 = /* invoke-direct/range {p0 ..p9}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->calculate(Lcom/android/server/wm/Task;Landroid/content/pm/ActivityInfo$WindowLayout;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/app/ActivityOptions;Lcom/android/server/wm/ActivityStarter$Request;ILcom/android/server/wm/LaunchParamsController$LaunchParams;Lcom/android/server/wm/LaunchParamsController$LaunchParams;)I */
/* .line 58 */
/* .local v0, "result":I */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;->outputLog()V */
/* .line 59 */
} // .end method
