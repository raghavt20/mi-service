.class public Lcom/android/server/wm/MiuiContrastOverlayStubImpl;
.super Ljava/lang/Object;
.source "MiuiContrastOverlayStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiContrastOverlayStub;


# static fields
.field private static final TYPE_LAYER_MULTIPLIER:I = 0x2710

.field private static surfaceControlFeature:Landroid/view/SurfaceControlFeatureImpl;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDh:I

.field private mDisplay:Landroid/view/Display;

.field private mDw:I

.field private mSurfaceControl:Landroid/view/SurfaceControl;

.field private mTransaction:Landroid/view/SurfaceControl$Transaction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Landroid/view/SurfaceControlFeatureImpl;

    invoke-direct {v0}, Landroid/view/SurfaceControlFeatureImpl;-><init>()V

    sput-object v0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->surfaceControlFeature:Landroid/view/SurfaceControlFeatureImpl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "MiuiContrastOverlayStubImpl"

    iput-object v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->TAG:Ljava/lang/String;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I

    .line 22
    iput v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I

    return-void
.end method


# virtual methods
.method public hideContrastOverlay()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 75
    iget-object v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->release()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 78
    :cond_0
    return-void
.end method

.method public init(Lcom/android/server/wm/DisplayContent;Landroid/util/DisplayMetrics;Landroid/content/Context;)V
    .locals 7
    .param p1, "dc"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "dm"    # Landroid/util/DisplayMetrics;
    .param p3, "mcontext"    # Landroid/content/Context;

    .line 29
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDisplay:Landroid/view/Display;

    .line 30
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v0

    .line 31
    .local v0, "defaultInfo":Landroid/view/DisplayInfo;
    iget v1, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    iput v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I

    .line 32
    iget v1, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    iput v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I

    .line 33
    const/4 v1, 0x0

    .line 34
    .local v1, "control":Landroid/view/SurfaceControl;
    iget v2, p2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v2, v2

    const/high16 v3, 0x43200000    # 160.0f

    div-float/2addr v2, v3

    .line 37
    .local v2, "constNum":F
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    const-string v4, "MiuiContrastOverlay"

    .line 38
    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 39
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setOpaque(Z)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 40
    invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->setColorLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 41
    invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v3

    move-object v1, v3

    .line 42
    invoke-static {}, Landroid/view/SurfaceControl;->getGlobalTransaction()Landroid/view/SurfaceControl$Transaction;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    .line 43
    sget-object v3, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->surfaceControlFeature:Landroid/view/SurfaceControlFeatureImpl;

    iget-object v4, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDisplay:Landroid/view/Display;

    invoke-virtual {v4}, Landroid/view/Display;->getLayerStack()I

    move-result v4

    invoke-virtual {v3, v1, v4}, Landroid/view/SurfaceControlFeatureImpl;->setLayerStack(Landroid/view/SurfaceControl;I)V

    .line 44
    iget-object v3, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    const v4, 0xf423f

    invoke-virtual {v3, v1, v4}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    .line 45
    iget-object v3, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4, v4}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 46
    iget-object v3, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v3, v1}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    :try_end_0
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    goto :goto_0

    .line 47
    :catch_0
    move-exception v3

    .line 48
    .local v3, "e":Landroid/view/Surface$OutOfResourcesException;
    iget-object v4, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createSurface e "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    .end local v3    # "e":Landroid/view/Surface$OutOfResourcesException;
    :goto_0
    iput-object v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 51
    return-void
.end method

.method public positionSurface(II)V
    .locals 6
    .param p1, "dw"    # I
    .param p2, "dh"    # I

    .line 54
    iget v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I

    if-eq p2, v0, :cond_1

    .line 55
    :cond_0
    iput p1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I

    .line 56
    iput p2, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I

    .line 57
    iget-object v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    iget-object v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mSurfaceControl:Landroid/view/SurfaceControl;

    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I

    iget v4, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I

    const/4 v5, 0x0

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;

    .line 59
    :cond_1
    return-void
.end method

.method public setAlpha(F)V
    .locals 2
    .param p1, "alpha"    # F

    .line 69
    iget-object v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    iget-object v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v1, p1}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 70
    return-void
.end method

.method public showContrastOverlay(F)V
    .locals 6
    .param p1, "alpha"    # F

    .line 62
    sget-object v0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->surfaceControlFeature:Landroid/view/SurfaceControlFeatureImpl;

    iget-object v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/4 v2, 0x3

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-virtual {v0, v1, v2}, Landroid/view/SurfaceControlFeatureImpl;->setColor(Landroid/view/SurfaceControl;[F)V

    .line 63
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->setAlpha(F)V

    .line 64
    iget-object v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    iget-object v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mSurfaceControl:Landroid/view/SurfaceControl;

    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I

    iget v4, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I

    const/4 v5, 0x0

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;

    .line 65
    iget-object v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    iget-object v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 66
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method
