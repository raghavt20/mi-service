.class public Lcom/android/server/wm/MiuiRotationAnimationUtils;
.super Ljava/lang/Object;
.source "MiuiRotationAnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseSineInOutInterpolator;,
        Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseQuartOutInterpolator;,
        Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseOutInterpolater;,
        Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseInInterpolater;
    }
.end annotation


# static fields
.field private static final DEVICE_HIGH_END:I = 0x3

.field private static final DEVICE_LOW_END:I = 0x1

.field private static final DEVICE_MIDDLE_END:I = 0x2

.field private static final HIGH_ROTATION_ANIMATION_DURATION:I = 0x1f4

.field private static final LOW_ROTATION_ANIMATION_DURATION:I = 0x12c

.field private static final MIDDLE_ROTATION_ANIMATION_DURATION:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "MiuiRotationAnimationUtil"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getRotationDuration()I
    .locals 3

    .line 18
    sget-boolean v0, Lmiui/os/Build;->IS_MIUI_LITE_VERSION:Z

    const/16 v1, 0x12c

    if-eqz v0, :cond_0

    .line 19
    return v1

    .line 21
    :cond_0
    invoke-static {}, Lmiui/os/Build;->getDeviceLevelForAnimation()I

    move-result v0

    .line 22
    .local v0, "devicelevel":I
    const/16 v2, 0x1f4

    packed-switch v0, :pswitch_data_0

    .line 30
    return v2

    .line 28
    :pswitch_0
    return v2

    .line 26
    :pswitch_1
    return v2

    .line 24
    :pswitch_2
    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
