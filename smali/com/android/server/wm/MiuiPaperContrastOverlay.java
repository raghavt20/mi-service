public class com.android.server.wm.MiuiPaperContrastOverlay {
	 /* .source "MiuiPaperContrastOverlay.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
public static final Boolean IS_FOLDABLE_DEVICE;
private static final java.lang.String TAG;
private static volatile com.android.server.wm.MiuiPaperContrastOverlay mContrastOverlay;
/* # instance fields */
private android.view.SurfaceControl mBLASTSurfaceControl;
private android.graphics.BLASTBufferQueue mBlastBufferQueue;
private android.content.Context mContext;
private Boolean mCreatedResources;
private final android.hardware.display.DisplayManagerInternal mDisplayManagerInternal;
private android.view.IDisplayWindowListener mDisplayWindowListener;
private android.opengl.EGLConfig mEglConfig;
private android.opengl.EGLContext mEglContext;
private android.opengl.EGLDisplay mEglDisplay;
private android.opengl.EGLSurface mEglSurface;
private Boolean mFoldDeviceReady;
private java.nio.ShortBuffer mIndexBuffer;
private final android.content.res.Configuration mLastConfiguration;
private Integer mLastDisplayHeight;
private Integer mLastDisplayWidth;
private Integer mLastLevel;
private Integer mLastRandSeed;
private Boolean mNeedShowPaperSurface;
private Float mPaperAlpha;
private Boolean mPaperMode;
private Integer mPaperNoiseLevel;
private Boolean mPrepared;
private Integer mProgram;
private Integer mRandSeed;
private java.nio.IntBuffer mRandomTexBuffer;
private android.view.Surface mSurface;
private android.view.SurfaceControl mSurfaceControl;
private Integer mSurfaceHeight;
private com.android.server.wm.MiuiPaperContrastOverlay$PaperSurfaceLayout mSurfaceLayout;
private android.view.SurfaceSession mSurfaceSession;
private Integer mSurfaceWidth;
private java.nio.FloatBuffer mTextureBuffer;
private mTextureId;
private java.nio.FloatBuffer mVertexBuffer;
private com.android.server.wm.WindowManagerService mWms;
private Integer samplerLocA;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.wm.MiuiPaperContrastOverlay p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmFoldDeviceReady ( com.android.server.wm.MiuiPaperContrastOverlay p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z */
} // .end method
static android.content.res.Configuration -$$Nest$fgetmLastConfiguration ( com.android.server.wm.MiuiPaperContrastOverlay p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mLastConfiguration;
} // .end method
static Boolean -$$Nest$fgetmNeedShowPaperSurface ( com.android.server.wm.MiuiPaperContrastOverlay p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mNeedShowPaperSurface:Z */
} // .end method
static com.android.server.wm.WindowManagerService -$$Nest$fgetmWms ( com.android.server.wm.MiuiPaperContrastOverlay p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mWms;
} // .end method
static void -$$Nest$fputmFoldDeviceReady ( com.android.server.wm.MiuiPaperContrastOverlay p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z */
	 return;
} // .end method
static void -$$Nest$mcreateSurface ( com.android.server.wm.MiuiPaperContrastOverlay p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createSurface()V */
	 return;
} // .end method
static void -$$Nest$mdestroySurface ( com.android.server.wm.MiuiPaperContrastOverlay p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroySurface()V */
	 return;
} // .end method
static Boolean -$$Nest$misSizeChangeHappened ( com.android.server.wm.MiuiPaperContrastOverlay p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->isSizeChangeHappened()Z */
} // .end method
static void -$$Nest$mupdateDisplaySize ( com.android.server.wm.MiuiPaperContrastOverlay p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateDisplaySize()V */
	 return;
} // .end method
static com.android.server.wm.MiuiPaperContrastOverlay ( ) {
	 /* .locals 3 */
	 /* .line 50 */
	 /* nop */
	 /* .line 51 */
	 final String v0 = "persist.sys.muiltdisplay_type"; // const-string v0, "persist.sys.muiltdisplay_type"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getInt ( v0,v1 );
	 int v2 = 2; // const/4 v2, 0x2
	 /* if-eq v0, v2, :cond_0 */
	 /* .line 52 */
	 v0 = 	 miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
	 if ( v0 != null) { // if-eqz v0, :cond_1
	 } // :cond_0
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_1
com.android.server.wm.MiuiPaperContrastOverlay.IS_FOLDABLE_DEVICE = (v1!= 0);
/* .line 50 */
return;
} // .end method
private com.android.server.wm.MiuiPaperContrastOverlay ( ) {
/* .locals 3 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 109 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 78 */
int v0 = 1; // const/4 v0, 0x1
/* new-array v0, v0, [I */
this.mTextureId = v0;
/* .line 83 */
/* const v0, 0x3d888889 */
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperAlpha:F */
/* .line 84 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z */
/* .line 85 */
/* const/16 v1, 0x11 */
/* iput v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperNoiseLevel:I */
/* .line 210 */
/* new-instance v1, Lcom/android/server/wm/MiuiPaperContrastOverlay$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V */
this.mDisplayWindowListener = v1;
/* .line 110 */
this.mWms = p1;
/* .line 111 */
this.mContext = p2;
/* .line 113 */
/* const-class v1, Landroid/hardware/display/DisplayManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Landroid/hardware/display/DisplayManagerInternal; */
this.mDisplayManagerInternal = v1;
/* .line 115 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateDisplaySize()V */
/* .line 117 */
/* new-instance v1, Landroid/content/res/Configuration; */
v2 = this.mContext;
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v2 ).getConfiguration ( ); // invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* invoke-direct {v1, v2}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V */
this.mLastConfiguration = v1;
/* .line 118 */
v1 = this.mWms;
v2 = this.mDisplayWindowListener;
(( com.android.server.wm.WindowManagerService ) v1 ).registerDisplayWindowListener ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->registerDisplayWindowListener(Landroid/view/IDisplayWindowListener;)[I
/* .line 120 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z */
/* .line 122 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->IS_FOLDABLE_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 123 */
	 v0 = this.mContext;
	 /* const-class v1, Landroid/hardware/devicestate/DeviceStateManager; */
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/hardware/devicestate/DeviceStateManager; */
	 v1 = this.mContext;
	 /* .line 124 */
	 (( android.content.Context ) v1 ).getMainExecutor ( ); // invoke-virtual {v1}, Landroid/content/Context;->getMainExecutor()Ljava/util/concurrent/Executor;
	 /* new-instance v2, Lcom/android/server/wm/MiuiPaperContrastOverlay$1; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V */
	 (( android.hardware.devicestate.DeviceStateManager ) v0 ).registerCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V
	 /* .line 146 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createSurface()V */
/* .line 147 */
return;
} // .end method
private Boolean attachEglContext ( ) {
/* .locals 4 */
/* .line 861 */
v0 = this.mEglSurface;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
	 v2 = this.mEglContext;
	 /* if-nez v2, :cond_0 */
	 /* .line 864 */
} // :cond_0
v3 = this.mEglDisplay;
v0 = android.opengl.EGL14 .eglMakeCurrent ( v3,v0,v0,v2 );
/* if-nez v0, :cond_1 */
/* .line 865 */
final String v0 = "eglMakeCurrent"; // const-string v0, "eglMakeCurrent"
com.android.server.wm.MiuiPaperContrastOverlay .logEglError ( v0 );
/* .line 866 */
/* .line 868 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 862 */
} // :cond_2
} // :goto_0
} // .end method
private static Boolean checkGlErrors ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "func" # Ljava/lang/String; */
/* .line 883 */
int v0 = 1; // const/4 v0, 0x1
v0 = com.android.server.wm.MiuiPaperContrastOverlay .checkGlErrors ( p0,v0 );
} // .end method
private static Boolean checkGlErrors ( java.lang.String p0, Boolean p1 ) {
/* .locals 5 */
/* .param p0, "func" # Ljava/lang/String; */
/* .param p1, "log" # Z */
/* .line 887 */
int v0 = 0; // const/4 v0, 0x0
/* .line 889 */
/* .local v0, "hadError":Z */
} // :goto_0
v1 = android.opengl.GLES20 .glGetError ( );
/* move v2, v1 */
/* .local v2, "error":I */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 890 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 891 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " failed: error "; // const-string v3, " failed: error "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-instance v3, Ljava/lang/Throwable; */
/* invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V */
final String v4 = "MiuiPaperContrastOverlay"; // const-string v4, "MiuiPaperContrastOverlay"
android.util.Slog .e ( v4,v1,v3 );
/* .line 893 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 895 */
} // :cond_1
} // .end method
private void clearEglAndSurface ( ) {
/* .locals 3 */
/* .line 187 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "clearEglAndSurface, egl display is "; // const-string v1, "clearEglAndSurface, egl display is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mEglDisplay;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", egl context is "; // const-string v1, ", egl context is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mEglContext;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
android.util.Slog .i ( v1,v0 );
/* .line 190 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V */
/* .line 191 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyRandomTexture()V */
/* .line 192 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyGLShaders()V */
/* .line 193 */
v0 = this.mEglContext;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 194 */
v2 = this.mEglDisplay;
android.opengl.EGL14 .eglDestroyContext ( v2,v0 );
/* .line 195 */
this.mEglContext = v1;
/* .line 197 */
} // :cond_0
v0 = this.mEglDisplay;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 198 */
android.opengl.EGL14 .eglTerminate ( v0 );
/* .line 199 */
this.mEglDisplay = v1;
/* .line 201 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V */
/* .line 202 */
return;
} // .end method
private Boolean createEglContext ( ) {
/* .locals 14 */
/* .line 768 */
v0 = this.mEglDisplay;
int v1 = 0; // const/4 v1, 0x0
int v2 = 2; // const/4 v2, 0x2
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* if-nez v0, :cond_1 */
/* .line 769 */
android.opengl.EGL14 .eglGetDisplay ( v4 );
this.mEglDisplay = v0;
/* .line 770 */
v5 = android.opengl.EGL14.EGL_NO_DISPLAY;
/* if-ne v0, v5, :cond_0 */
/* .line 771 */
final String v0 = "eglGetDisplay"; // const-string v0, "eglGetDisplay"
com.android.server.wm.MiuiPaperContrastOverlay .logEglError ( v0 );
/* .line 772 */
/* .line 775 */
} // :cond_0
/* new-array v0, v2, [I */
/* .line 776 */
/* .local v0, "version":[I */
v5 = this.mEglDisplay;
v5 = android.opengl.EGL14 .eglInitialize ( v5,v0,v4,v0,v3 );
/* if-nez v5, :cond_1 */
/* .line 777 */
this.mEglDisplay = v1;
/* .line 778 */
final String v1 = "eglInitialize"; // const-string v1, "eglInitialize"
com.android.server.wm.MiuiPaperContrastOverlay .logEglError ( v1 );
/* .line 779 */
/* .line 783 */
} // .end local v0 # "version":[I
} // :cond_1
v0 = this.mEglConfig;
/* if-nez v0, :cond_4 */
/* .line 784 */
/* const/16 v0, 0xb */
/* new-array v6, v0, [I */
/* fill-array-data v6, :array_0 */
/* .line 793 */
/* .local v6, "eglConfigAttribList":[I */
/* new-array v0, v3, [I */
/* .line 794 */
/* .local v0, "numEglConfigs":[I */
/* new-array v13, v3, [Landroid/opengl/EGLConfig; */
/* .line 795 */
/* .local v13, "eglConfigs":[Landroid/opengl/EGLConfig; */
v5 = this.mEglDisplay;
int v7 = 0; // const/4 v7, 0x0
int v9 = 0; // const/4 v9, 0x0
/* array-length v10, v13 */
int v12 = 0; // const/4 v12, 0x0
/* move-object v8, v13 */
/* move-object v11, v0 */
v5 = /* invoke-static/range {v5 ..v12}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z */
/* if-nez v5, :cond_2 */
/* .line 797 */
final String v1 = "eglChooseConfig"; // const-string v1, "eglChooseConfig"
com.android.server.wm.MiuiPaperContrastOverlay .logEglError ( v1 );
/* .line 798 */
/* .line 800 */
} // :cond_2
/* aget v5, v0, v4 */
/* if-gtz v5, :cond_3 */
/* .line 801 */
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
final String v2 = "no valid config found"; // const-string v2, "no valid config found"
android.util.Slog .e ( v1,v2 );
/* .line 802 */
/* .line 805 */
} // :cond_3
/* aget-object v5, v13, v4 */
this.mEglConfig = v5;
/* .line 810 */
} // .end local v0 # "numEglConfigs":[I
} // .end local v6 # "eglConfigAttribList":[I
} // .end local v13 # "eglConfigs":[Landroid/opengl/EGLConfig;
} // :cond_4
v0 = this.mEglContext;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 811 */
v5 = this.mEglDisplay;
android.opengl.EGL14 .eglDestroyContext ( v5,v0 );
/* .line 812 */
this.mEglContext = v1;
/* .line 815 */
} // :cond_5
v0 = this.mEglContext;
/* if-nez v0, :cond_6 */
/* .line 816 */
/* const/16 v0, 0x3098 */
/* const/16 v1, 0x3038 */
/* filled-new-array {v0, v2, v1}, [I */
/* .line 820 */
/* .local v0, "eglContextAttribList":[I */
v1 = this.mEglDisplay;
v2 = this.mEglConfig;
v5 = android.opengl.EGL14.EGL_NO_CONTEXT;
android.opengl.EGL14 .eglCreateContext ( v1,v2,v5,v0,v4 );
this.mEglContext = v1;
/* .line 822 */
/* if-nez v1, :cond_6 */
/* .line 823 */
final String v1 = "eglCreateContext"; // const-string v1, "eglCreateContext"
com.android.server.wm.MiuiPaperContrastOverlay .logEglError ( v1 );
/* .line 824 */
/* .line 827 */
} // .end local v0 # "eglContextAttribList":[I
} // :cond_6
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x3040 */
/* 0x4 */
/* 0x3024 */
/* 0x8 */
/* 0x3023 */
/* 0x8 */
/* 0x3022 */
/* 0x8 */
/* 0x3021 */
/* 0x8 */
/* 0x3038 */
} // .end array-data
} // .end method
private Boolean createEglSurface ( ) {
/* .locals 5 */
/* .line 832 */
v0 = this.mEglSurface;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 833 */
v1 = this.mEglDisplay;
android.opengl.EGL14 .eglDestroySurface ( v1,v0 );
/* .line 834 */
int v0 = 0; // const/4 v0, 0x0
this.mEglSurface = v0;
/* .line 836 */
} // :cond_0
v0 = this.mEglSurface;
/* if-nez v0, :cond_1 */
/* .line 837 */
/* const/16 v0, 0x3038 */
/* filled-new-array {v0}, [I */
/* .line 841 */
/* .local v0, "eglSurfaceAttribList":[I */
v1 = this.mEglDisplay;
v2 = this.mEglConfig;
v3 = this.mSurface;
int v4 = 0; // const/4 v4, 0x0
android.opengl.EGL14 .eglCreateWindowSurface ( v1,v2,v3,v0,v4 );
this.mEglSurface = v1;
/* .line 843 */
/* if-nez v1, :cond_1 */
/* .line 844 */
final String v1 = "eglCreateWindowSurface"; // const-string v1, "eglCreateWindowSurface"
com.android.server.wm.MiuiPaperContrastOverlay .logEglError ( v1 );
/* .line 845 */
/* .line 848 */
} // .end local v0 # "eglSurfaceAttribList":[I
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void createRandTexture ( ) {
/* .locals 13 */
/* .line 626 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "createRandTexture randSeed: "; // const-string v1, "createRandTexture randSeed: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", lastRandSeed: "; // const-string v1, ", lastRandSeed: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastRandSeed:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
android.util.Slog .d ( v1,v0 );
/* .line 628 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 629 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* .line 630 */
/* .local v3, "begin":J */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "createRandTexture begin, size ("; // const-string v5, "createRandTexture begin, size ("
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ","; // const-string v5, ","
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = "), pixels: "; // const-string v5, "), pixels: "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mRandomTexBuffer;
/* .line 631 */
v5 = (( java.nio.IntBuffer ) v5 ).limit ( ); // invoke-virtual {v5}, Ljava/nio/IntBuffer;->limit()I
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 630 */
android.util.Slog .i ( v1,v0 );
/* .line 635 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
/* mul-int/2addr v0, v5 */
v5 = this.mRandomTexBuffer;
v5 = (( java.nio.IntBuffer ) v5 ).limit ( ); // invoke-virtual {v5}, Ljava/nio/IntBuffer;->limit()I
/* if-le v0, v5, :cond_0 */
/* .line 636 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->requestRandomBuffer()V */
/* .line 643 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
/* mul-int/2addr v0, v5 */
int v5 = 2; // const/4 v5, 0x2
/* div-int/2addr v0, v5 */
/* .line 644 */
/* .local v0, "halfPixels":I */
java.util.concurrent.ThreadLocalRandom .current ( );
/* int-to-long v7, v0 */
/* iget v9, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I */
(( java.util.concurrent.ThreadLocalRandom ) v6 ).ints ( v7, v8, v2, v9 ); // invoke-virtual {v6, v7, v8, v2, v9}, Ljava/util/concurrent/ThreadLocalRandom;->ints(JII)Ljava/util/stream/IntStream;
/* .line 645 */
/* .local v6, "randomStream":Ljava/util/stream/IntStream; */
/* .line 646 */
/* .local v7, "random":[I */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "j":I */
} // :goto_0
/* if-ge v8, v5, :cond_2 */
/* .line 647 */
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "i":I */
} // :goto_1
/* array-length v10, v7 */
/* if-ge v9, v10, :cond_1 */
/* .line 648 */
v10 = this.mRandomTexBuffer;
/* mul-int v11, v8, v0 */
/* add-int/2addr v11, v9 */
/* aget v12, v7, v9 */
(( java.nio.IntBuffer ) v10 ).put ( v11, v12 ); // invoke-virtual {v10, v11, v12}, Ljava/nio/IntBuffer;->put(II)Ljava/nio/IntBuffer;
/* .line 647 */
/* add-int/lit8 v9, v9, 0x1 */
/* .line 646 */
} // .end local v9 # "i":I
} // :cond_1
/* add-int/lit8 v8, v8, 0x1 */
/* .line 652 */
} // .end local v8 # "j":I
} // :cond_2
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
/* .line 653 */
/* .local v8, "end":J */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "createRandTexture end, duration: "; // const-string v10, "createRandTexture end, duration: "
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sub-long v10, v8, v3 */
(( java.lang.StringBuilder ) v5 ).append ( v10, v11 ); // invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v5 );
} // .end local v0 # "halfPixels":I
} // .end local v3 # "begin":J
} // .end local v6 # "randomStream":Ljava/util/stream/IntStream;
} // .end local v7 # "random":[I
} // .end local v8 # "end":J
/* .line 654 */
} // :cond_3
/* if-nez v0, :cond_4 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastRandSeed:I */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 655 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_2
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
/* mul-int/2addr v1, v3 */
/* if-ge v0, v1, :cond_5 */
/* .line 656 */
v1 = this.mRandomTexBuffer;
(( java.nio.IntBuffer ) v1 ).put ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/nio/IntBuffer;->put(II)Ljava/nio/IntBuffer;
/* .line 655 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 654 */
} // .end local v0 # "i":I
} // :cond_4
} // :goto_3
/* nop */
/* .line 659 */
} // :cond_5
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateDisplaySize()V */
/* .line 660 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I */
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastRandSeed:I */
/* .line 661 */
v0 = this.mTextureId;
/* aget v0, v0, v2 */
/* const/16 v1, 0xde1 */
android.opengl.GLES20 .glBindTexture ( v1,v0 );
/* .line 662 */
/* const/16 v2, 0xde1 */
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x1908 */
/* iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
int v7 = 0; // const/4 v7, 0x0
/* const/16 v8, 0x1908 */
/* const/16 v9, 0x1401 */
v10 = this.mRandomTexBuffer;
/* invoke-static/range {v2 ..v10}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V */
/* .line 664 */
final String v0 = "createRandTexture"; // const-string v0, "createRandTexture"
com.android.server.wm.MiuiPaperContrastOverlay .checkGlErrors ( v0 );
/* .line 665 */
return;
} // .end method
private void createSurface ( ) {
/* .locals 9 */
/* .line 379 */
v0 = this.mSurfaceSession;
/* if-nez v0, :cond_0 */
/* .line 380 */
/* new-instance v0, Landroid/view/SurfaceSession; */
/* invoke-direct {v0}, Landroid/view/SurfaceSession;-><init>()V */
this.mSurfaceSession = v0;
/* .line 382 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Create surface size: "; // const-string v1, "Create surface size: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSurfaceControl;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
android.util.Slog .d ( v1,v0 );
/* .line 384 */
v0 = this.mSurfaceControl;
/* if-nez v0, :cond_5 */
/* .line 386 */
try { // :try_start_0
/* new-instance v0, Landroid/view/SurfaceControl$Builder; */
v2 = this.mSurfaceSession;
/* invoke-direct {v0, v2}, Landroid/view/SurfaceControl$Builder;-><init>(Landroid/view/SurfaceSession;)V */
/* .line 387 */
(( android.view.SurfaceControl$Builder ) v0 ).setName ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 415 */
/* .local v0, "builder":Landroid/view/SurfaceControl$Builder; */
int v2 = 0; // const/4 v2, 0x0
/* .line 416 */
/* .local v2, "parentCtrl":Landroid/view/SurfaceControl; */
v3 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v3 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
v3 = this.mChildren;
/* .line 417 */
/* .local v3, "mChildren":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<Lcom/android/server/wm/DisplayArea;>;" */
v4 = (( com.android.server.wm.WindowList ) v3 ).size ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowList;->size()I
int v5 = 1; // const/4 v5, 0x1
/* sub-int/2addr v4, v5 */
/* .local v4, "i":I */
} // :goto_0
/* if-ltz v4, :cond_3 */
/* .line 418 */
(( com.android.server.wm.WindowList ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wm/DisplayArea; */
/* .line 419 */
/* .local v6, "childArea":Lcom/android/server/wm/DisplayArea;, "Lcom/android/server/wm/DisplayArea<*>;" */
/* if-nez v6, :cond_1 */
/* .line 420 */
/* .line 423 */
} // :cond_1
(( com.android.server.wm.DisplayArea ) v6 ).getName ( ); // invoke-virtual {v6}, Lcom/android/server/wm/DisplayArea;->getName()Ljava/lang/String;
final String v8 = "WindowedMagnification"; // const-string v8, "WindowedMagnification"
v7 = (( java.lang.String ) v7 ).contains ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 424 */
(( com.android.server.wm.DisplayArea ) v6 ).getSurfaceControl ( ); // invoke-virtual {v6}, Lcom/android/server/wm/DisplayArea;->getSurfaceControl()Landroid/view/SurfaceControl;
/* move-object v2, v7 */
/* .line 417 */
} // .end local v6 # "childArea":Lcom/android/server/wm/DisplayArea;, "Lcom/android/server/wm/DisplayArea<*>;"
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, -0x1 */
/* .line 427 */
} // .end local v4 # "i":I
} // :cond_3
/* if-nez v2, :cond_4 */
/* .line 428 */
v4 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v4 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v4 ).getSurfaceControl ( ); // invoke-virtual {v4}, Lcom/android/server/wm/DisplayContent;->getSurfaceControl()Landroid/view/SurfaceControl;
/* move-object v2, v4 */
/* .line 430 */
} // :cond_4
(( android.view.SurfaceControl$Builder ) v0 ).setName ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 431 */
(( android.view.SurfaceControl$Builder ) v4 ).setFormat ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setFormat(I)Landroid/view/SurfaceControl$Builder;
/* iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
/* .line 432 */
(( android.view.SurfaceControl$Builder ) v4 ).setBufferSize ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/view/SurfaceControl$Builder;->setBufferSize(II)Landroid/view/SurfaceControl$Builder;
/* .line 433 */
(( android.view.SurfaceControl$Builder ) v4 ).setContainerLayer ( ); // invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->setContainerLayer()Landroid/view/SurfaceControl$Builder;
/* .line 434 */
(( android.view.SurfaceControl$Builder ) v4 ).setParent ( v2 ); // invoke-virtual {v4, v2}, Landroid/view/SurfaceControl$Builder;->setParent(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;
/* .line 435 */
(( android.view.SurfaceControl$Builder ) v4 ).build ( ); // invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
/* .line 436 */
(( android.view.SurfaceControl$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
this.mSurfaceControl = v4;
/* :try_end_0 */
/* .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 439 */
} // .end local v0 # "builder":Landroid/view/SurfaceControl$Builder;
} // .end local v2 # "parentCtrl":Landroid/view/SurfaceControl;
} // .end local v3 # "mChildren":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<Lcom/android/server/wm/DisplayArea;>;"
/* .line 437 */
/* :catch_0 */
/* move-exception v0 */
/* .line 438 */
/* .local v0, "ex":Landroid/view/Surface$OutOfResourcesException; */
final String v2 = "Unable to create surface."; // const-string v2, "Unable to create surface."
android.util.Slog .e ( v1,v2,v0 );
/* .line 441 */
} // .end local v0 # "ex":Landroid/view/Surface$OutOfResourcesException;
} // :goto_2
v0 = this.mWms;
v0 = this.mTransactionFactory;
/* check-cast v0, Landroid/view/SurfaceControl$Transaction; */
/* .line 442 */
/* .local v0, "t":Landroid/view/SurfaceControl$Transaction; */
v2 = this.mSurfaceControl;
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
(( android.view.SurfaceControl$Transaction ) v0 ).setWindowCrop ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;
/* .line 443 */
v2 = this.mSurfaceControl;
/* const v3, 0xf4240 */
(( android.view.SurfaceControl$Transaction ) v0 ).setLayer ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
/* .line 444 */
v2 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v0 ).show ( v2 ); // invoke-virtual {v0, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 446 */
/* new-instance v2, Landroid/view/SurfaceControl$Builder; */
/* invoke-direct {v2}, Landroid/view/SurfaceControl$Builder;-><init>()V */
/* .line 447 */
(( android.view.SurfaceControl$Builder ) v2 ).setName ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
v2 = this.mSurfaceControl;
/* .line 448 */
(( android.view.SurfaceControl$Builder ) v1 ).setParent ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/SurfaceControl$Builder;->setParent(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;
/* .line 449 */
int v2 = 0; // const/4 v2, 0x0
(( android.view.SurfaceControl$Builder ) v1 ).setHidden ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/SurfaceControl$Builder;->setHidden(Z)Landroid/view/SurfaceControl$Builder;
/* .line 450 */
(( android.view.SurfaceControl$Builder ) v1 ).setBLASTLayer ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;
/* .line 451 */
/* .local v1, "b":Landroid/view/SurfaceControl$Builder; */
(( android.view.SurfaceControl$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
this.mBLASTSurfaceControl = v2;
/* .line 452 */
/* new-instance v2, Landroid/graphics/BLASTBufferQueue; */
final String v4 = "MiuiPaperContrastOverlay"; // const-string v4, "MiuiPaperContrastOverlay"
v5 = this.mBLASTSurfaceControl;
/* iget v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v7, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
int v8 = -3; // const/4 v8, -0x3
/* move-object v3, v2 */
/* invoke-direct/range {v3 ..v8}, Landroid/graphics/BLASTBufferQueue;-><init>(Ljava/lang/String;Landroid/view/SurfaceControl;III)V */
this.mBlastBufferQueue = v2;
/* .line 454 */
(( android.graphics.BLASTBufferQueue ) v2 ).createSurface ( ); // invoke-virtual {v2}, Landroid/graphics/BLASTBufferQueue;->createSurface()Landroid/view/Surface;
this.mSurface = v2;
/* .line 456 */
/* new-instance v2, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout; */
v3 = this.mDisplayManagerInternal;
v4 = this.mSurfaceControl;
/* invoke-direct {v2, p0, v3, v4}, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay;Landroid/hardware/display/DisplayManagerInternal;Landroid/view/SurfaceControl;)V */
this.mSurfaceLayout = v2;
/* .line 457 */
(( com.android.server.wm.MiuiPaperContrastOverlay$PaperSurfaceLayout ) v2 ).onDisplayTransaction ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->onDisplayTransaction(Landroid/view/SurfaceControl$Transaction;)V
/* .line 458 */
(( android.view.SurfaceControl$Transaction ) v0 ).apply ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 461 */
} // .end local v0 # "t":Landroid/view/SurfaceControl$Transaction;
} // .end local v1 # "b":Landroid/view/SurfaceControl$Builder;
} // :cond_5
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateDisplaySize()V */
/* .line 462 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->initEgl()V */
/* .line 463 */
return;
} // .end method
private void destroyEglSurface ( ) {
/* .locals 2 */
/* .line 852 */
v0 = this.mEglSurface;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 853 */
v1 = this.mEglDisplay;
v0 = android.opengl.EGL14 .eglDestroySurface ( v1,v0 );
/* if-nez v0, :cond_0 */
/* .line 854 */
final String v0 = "eglDestroySurface"; // const-string v0, "eglDestroySurface"
com.android.server.wm.MiuiPaperContrastOverlay .logEglError ( v0 );
/* .line 856 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mEglSurface = v0;
/* .line 858 */
} // :cond_1
return;
} // .end method
private void destroyGLShaders ( ) {
/* .locals 1 */
/* .line 683 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I */
android.opengl.GLES20 .glDeleteProgram ( v0 );
/* .line 684 */
final String v0 = "glDeleteProgram"; // const-string v0, "glDeleteProgram"
com.android.server.wm.MiuiPaperContrastOverlay .checkGlErrors ( v0 );
/* .line 685 */
return;
} // .end method
private void destroyRandomTexture ( ) {
/* .locals 3 */
/* .line 616 */
v0 = this.mTextureId;
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
android.opengl.GLES20 .glDeleteTextures ( v2,v0,v1 );
/* .line 617 */
int v0 = 0; // const/4 v0, 0x0
this.mTextureBuffer = v0;
/* .line 618 */
this.mVertexBuffer = v0;
/* .line 619 */
this.mIndexBuffer = v0;
/* .line 620 */
this.mRandomTexBuffer = v0;
/* .line 621 */
final String v0 = "glDeleteTextures"; // const-string v0, "glDeleteTextures"
com.android.server.wm.MiuiPaperContrastOverlay .checkGlErrors ( v0 );
/* .line 622 */
return;
} // .end method
private void destroySurface ( ) {
/* .locals 4 */
/* .line 720 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "destroySurface surfaceControl: "; // const-string v1, "destroySurface surfaceControl: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSurfaceControl;
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* move v1, v2 */
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
android.util.Slog .d ( v1,v0 );
/* .line 723 */
v0 = this.mSurfaceControl;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 724 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "screen_paper_layer_show"; // const-string v1, "screen_paper_layer_show"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .putIntForUser ( v0,v1,v2,v3 );
/* .line 727 */
/* iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I */
/* .line 728 */
/* iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I */
/* .line 729 */
v0 = this.mSurfaceLayout;
(( com.android.server.wm.MiuiPaperContrastOverlay$PaperSurfaceLayout ) v0 ).dispose ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->dispose()V
/* .line 730 */
int v0 = 0; // const/4 v0, 0x0
this.mSurfaceLayout = v0;
/* .line 731 */
v1 = this.mWms;
v1 = this.mTransactionFactory;
/* check-cast v1, Landroid/view/SurfaceControl$Transaction; */
v2 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
(( android.view.SurfaceControl$Transaction ) v1 ).apply ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 732 */
v1 = this.mSurface;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 733 */
(( android.view.Surface ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/view/Surface;->release()V
/* .line 734 */
this.mSurface = v0;
/* .line 736 */
} // :cond_1
v1 = this.mBLASTSurfaceControl;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 737 */
(( android.view.SurfaceControl ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl;->release()V
/* .line 738 */
this.mBLASTSurfaceControl = v0;
/* .line 739 */
v1 = this.mBlastBufferQueue;
(( android.graphics.BLASTBufferQueue ) v1 ).destroy ( ); // invoke-virtual {v1}, Landroid/graphics/BLASTBufferQueue;->destroy()V
/* .line 740 */
this.mBlastBufferQueue = v0;
/* .line 742 */
} // :cond_2
this.mSurfaceControl = v0;
/* .line 744 */
} // :cond_3
return;
} // .end method
private void detachEglContext ( ) {
/* .locals 4 */
/* .line 872 */
v0 = this.mEglDisplay;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 873 */
v1 = android.opengl.EGL14.EGL_NO_SURFACE;
v2 = android.opengl.EGL14.EGL_NO_SURFACE;
v3 = android.opengl.EGL14.EGL_NO_CONTEXT;
android.opengl.EGL14 .eglMakeCurrent ( v0,v1,v2,v3 );
/* .line 876 */
} // :cond_0
return;
} // .end method
private void dismiss ( ) {
/* .locals 3 */
/* .line 707 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z */
/* .line 709 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "dismiss "; // const-string v2, "dismiss "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPaperContrastOverlay"; // const-string v2, "MiuiPaperContrastOverlay"
android.util.Slog .d ( v2,v1 );
/* .line 711 */
/* iget-boolean v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 712 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismissResources()V */
/* .line 713 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroySurface()V */
/* .line 714 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z */
/* .line 716 */
} // :cond_0
return;
} // .end method
private void dismissResources ( ) {
/* .locals 2 */
/* .line 748 */
final String v0 = "MiuiPaperContrastOverlay"; // const-string v0, "MiuiPaperContrastOverlay"
final String v1 = "dismissResources"; // const-string v1, "dismissResources"
android.util.Slog .d ( v0,v1 );
/* .line 751 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mCreatedResources:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 752 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->attachEglContext()Z */
/* .line 754 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyRandomTexture()V */
/* .line 755 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyGLShaders()V */
/* .line 756 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyEglSurface()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 758 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V */
/* .line 759 */
/* nop */
/* .line 762 */
android.opengl.GLES20 .glFlush ( );
/* .line 763 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mCreatedResources:Z */
/* .line 758 */
/* :catchall_0 */
/* move-exception v0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V */
/* .line 759 */
/* throw v0 */
/* .line 765 */
} // :cond_0
} // :goto_0
return;
} // .end method
private void drawFrame ( Float p0 ) {
/* .locals 17 */
/* .param p1, "alpha" # F */
/* .line 688 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p1 */
int v2 = 0; // const/4 v2, 0x0
android.opengl.GLES20 .glClearColor ( v2,v2,v2,v1 );
/* .line 689 */
/* iget v2, v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v3, v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
int v4 = 0; // const/4 v4, 0x0
android.opengl.GLES20 .glViewport ( v4,v4,v2,v3 );
/* .line 690 */
/* iget v2, v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->samplerLocA:I */
android.opengl.GLES20 .glVertexAttrib1f ( v2,v1 );
/* .line 693 */
int v5 = 0; // const/4 v5, 0x0
int v6 = 3; // const/4 v6, 0x3
/* const/16 v7, 0x1406 */
int v8 = 0; // const/4 v8, 0x0
/* const/16 v9, 0xc */
v10 = this.mVertexBuffer;
/* invoke-static/range {v5 ..v10}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V */
/* .line 696 */
int v11 = 1; // const/4 v11, 0x1
int v12 = 2; // const/4 v12, 0x2
/* const/16 v13, 0x1406 */
int v14 = 0; // const/4 v14, 0x0
/* const/16 v15, 0x8 */
v2 = this.mTextureBuffer;
/* move-object/from16 v16, v2 */
/* invoke-static/range {v11 ..v16}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V */
/* .line 699 */
android.opengl.GLES20 .glEnableVertexAttribArray ( v4 );
/* .line 700 */
int v2 = 1; // const/4 v2, 0x1
android.opengl.GLES20 .glEnableVertexAttribArray ( v2 );
/* .line 702 */
/* const/16 v2, 0x4100 */
android.opengl.GLES20 .glClear ( v2 );
/* .line 703 */
/* const/16 v2, 0x1403 */
v3 = this.mIndexBuffer;
int v4 = 4; // const/4 v4, 0x4
int v5 = 6; // const/4 v5, 0x6
android.opengl.GLES20 .glDrawElements ( v4,v5,v2,v3 );
/* .line 704 */
return;
} // .end method
private void drawSurface ( Float p0 ) {
/* .locals 2 */
/* .param p1, "alpha" # F */
/* .line 360 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Draw surface, alpha is "; // const-string v1, "Draw surface, alpha is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = " and randSeed is "; // const-string v1, " and randSeed is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
android.util.Slog .d ( v1,v0 );
/* .line 361 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateGLShaders(F)V */
/* .line 362 */
/* const-string/jumbo v0, "updateGLShaders" */
v0 = com.android.server.wm.MiuiPaperContrastOverlay .checkGlErrors ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 363 */
final String v0 = "Draw surface updateGLShaders failed"; // const-string v0, "Draw surface updateGLShaders failed"
android.util.Slog .d ( v1,v0 );
/* .line 364 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V */
/* .line 365 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V */
/* .line 366 */
return;
/* .line 368 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->drawFrame(F)V */
/* .line 369 */
final String v0 = "drawFrame"; // const-string v0, "drawFrame"
v0 = com.android.server.wm.MiuiPaperContrastOverlay .checkGlErrors ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 370 */
final String v0 = "Draw frame failed"; // const-string v0, "Draw frame failed"
android.util.Slog .d ( v1,v0 );
/* .line 371 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V */
/* .line 372 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V */
/* .line 373 */
return;
/* .line 375 */
} // :cond_1
v0 = this.mEglDisplay;
v1 = this.mEglSurface;
android.opengl.EGL14 .eglSwapBuffers ( v0,v1 );
/* .line 376 */
return;
} // .end method
public static com.android.server.wm.MiuiPaperContrastOverlay getInstance ( com.android.server.wm.WindowManagerService p0, android.content.Context p1 ) {
/* .locals 2 */
/* .param p0, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 99 */
v0 = com.android.server.wm.MiuiPaperContrastOverlay.mContrastOverlay;
/* if-nez v0, :cond_1 */
/* .line 100 */
/* const-class v0, Lcom/android/server/wm/MiuiPaperContrastOverlay; */
/* monitor-enter v0 */
/* .line 101 */
try { // :try_start_0
v1 = com.android.server.wm.MiuiPaperContrastOverlay.mContrastOverlay;
/* if-nez v1, :cond_0 */
/* .line 102 */
/* new-instance v1, Lcom/android/server/wm/MiuiPaperContrastOverlay; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;-><init>(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V */
/* .line 104 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 106 */
} // :cond_1
} // :goto_0
v0 = com.android.server.wm.MiuiPaperContrastOverlay.mContrastOverlay;
} // .end method
private void initEgl ( ) {
/* .locals 2 */
/* .line 467 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createEglContext()Z */
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createEglSurface()Z */
/* if-nez v0, :cond_0 */
/* .line 476 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->attachEglContext()Z */
/* if-nez v0, :cond_1 */
/* .line 477 */
final String v0 = "attachEglContext failed"; // const-string v0, "attachEglContext failed"
android.util.Slog .d ( v1,v0 );
/* .line 478 */
return;
/* .line 481 */
} // :cond_1
try { // :try_start_0
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->initGLShaders()Z */
if ( v0 != null) { // if-eqz v0, :cond_3
final String v0 = "initEgl"; // const-string v0, "initEgl"
v0 = com.android.server.wm.MiuiPaperContrastOverlay .checkGlErrors ( v0 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 487 */
} // :cond_2
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V */
/* .line 488 */
/* nop */
/* .line 491 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mCreatedResources:Z */
/* .line 492 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z */
/* .line 493 */
return;
/* .line 482 */
} // :cond_3
} // :goto_0
try { // :try_start_1
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V */
/* .line 483 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 487 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V */
/* .line 484 */
return;
/* .line 487 */
/* :catchall_0 */
/* move-exception v0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V */
/* .line 488 */
/* throw v0 */
/* .line 468 */
} // :cond_4
} // :goto_1
final String v0 = "createEglContext failed"; // const-string v0, "createEglContext failed"
android.util.Slog .d ( v1,v0 );
/* .line 473 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V */
/* .line 474 */
return;
} // .end method
private Boolean initGLShaders ( ) {
/* .locals 10 */
/* .line 545 */
/* const v0, 0x8b31 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->loadShader(I)I */
/* .line 546 */
/* .local v0, "vshader":I */
/* const v1, 0x8b30 */
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->loadShader(I)I */
/* .line 547 */
/* .local v1, "fshader":I */
android.opengl.GLES20 .glReleaseShaderCompiler ( );
/* .line 548 */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez v1, :cond_0 */
/* goto/16 :goto_0 */
/* .line 550 */
} // :cond_0
v3 = android.opengl.GLES20 .glCreateProgram ( );
/* iput v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I */
/* .line 552 */
android.opengl.GLES20 .glAttachShader ( v3,v0 );
/* .line 553 */
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I */
android.opengl.GLES20 .glAttachShader ( v3,v1 );
/* .line 554 */
android.opengl.GLES20 .glDeleteShader ( v0 );
/* .line 555 */
android.opengl.GLES20 .glDeleteShader ( v1 );
/* .line 557 */
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I */
android.opengl.GLES20 .glLinkProgram ( v3 );
/* .line 559 */
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I */
android.opengl.GLES20 .glUseProgram ( v3 );
/* .line 560 */
final String v3 = "glUseProgram"; // const-string v3, "glUseProgram"
com.android.server.wm.MiuiPaperContrastOverlay .checkGlErrors ( v3 );
/* .line 561 */
final String v3 = "MiuiPaperContrastOverlay"; // const-string v3, "MiuiPaperContrastOverlay"
final String v4 = "initGLShader start"; // const-string v4, "initGLShader start"
android.util.Slog .d ( v3,v4 );
/* .line 562 */
/* const/16 v3, 0xc */
/* new-array v3, v3, [F */
/* fill-array-data v3, :array_0 */
/* .line 567 */
/* .local v3, "vCoord":[F */
/* const/16 v4, 0x8 */
/* new-array v4, v4, [F */
/* fill-array-data v4, :array_1 */
/* .line 572 */
/* .local v4, "tCoord":[F */
int v5 = 6; // const/4 v5, 0x6
/* new-array v5, v5, [S */
/* fill-array-data v5, :array_2 */
/* .line 573 */
/* .local v5, "indices":[S */
/* array-length v6, v3 */
/* mul-int/lit8 v6, v6, 0x4 */
java.nio.ByteBuffer .allocateDirect ( v6 );
/* .line 574 */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v6 ).order ( v7 ); // invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 575 */
(( java.nio.ByteBuffer ) v6 ).asFloatBuffer ( ); // invoke-virtual {v6}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;
/* .line 576 */
(( java.nio.FloatBuffer ) v6 ).put ( v3 ); // invoke-virtual {v6, v3}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;
this.mVertexBuffer = v6;
/* .line 577 */
(( java.nio.FloatBuffer ) v6 ).position ( v2 ); // invoke-virtual {v6, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;
/* .line 578 */
/* array-length v6, v4 */
/* mul-int/lit8 v6, v6, 0x4 */
java.nio.ByteBuffer .allocateDirect ( v6 );
/* .line 579 */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v6 ).order ( v7 ); // invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 580 */
(( java.nio.ByteBuffer ) v6 ).asFloatBuffer ( ); // invoke-virtual {v6}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;
/* .line 581 */
(( java.nio.FloatBuffer ) v6 ).put ( v4 ); // invoke-virtual {v6, v4}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;
this.mTextureBuffer = v6;
/* .line 582 */
(( java.nio.FloatBuffer ) v6 ).position ( v2 ); // invoke-virtual {v6, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;
/* .line 583 */
/* array-length v6, v5 */
/* mul-int/lit8 v6, v6, 0x4 */
java.nio.ByteBuffer .allocateDirect ( v6 );
/* .line 584 */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v6 ).order ( v7 ); // invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 585 */
(( java.nio.ByteBuffer ) v6 ).asShortBuffer ( ); // invoke-virtual {v6}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;
/* .line 586 */
(( java.nio.ShortBuffer ) v6 ).put ( v5 ); // invoke-virtual {v6, v5}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;
this.mIndexBuffer = v6;
/* .line 587 */
(( java.nio.ShortBuffer ) v6 ).position ( v2 ); // invoke-virtual {v6, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;
/* .line 589 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->requestRandomBuffer()V */
/* .line 591 */
v6 = this.mTextureId;
int v7 = 1; // const/4 v7, 0x1
android.opengl.GLES20 .glGenTextures ( v7,v6,v2 );
/* .line 592 */
v6 = this.mTextureId;
/* aget v6, v6, v2 */
/* const/16 v8, 0xde1 */
android.opengl.GLES20 .glBindTexture ( v8,v6 );
/* .line 593 */
/* const/16 v6, 0xcf5 */
android.opengl.GLES20 .glPixelStorei ( v6,v7 );
/* .line 594 */
/* const/16 v6, 0x2801 */
/* const/16 v9, 0x2600 */
android.opengl.GLES20 .glTexParameteri ( v8,v6,v9 );
/* .line 595 */
/* const/16 v6, 0x2800 */
android.opengl.GLES20 .glTexParameteri ( v8,v6,v9 );
/* .line 596 */
/* const/16 v6, 0x2802 */
/* const/16 v9, 0x2901 */
android.opengl.GLES20 .glTexParameteri ( v8,v6,v9 );
/* .line 597 */
/* const/16 v6, 0x2803 */
android.opengl.GLES20 .glTexParameteri ( v8,v6,v9 );
/* .line 598 */
/* iget v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v8, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
android.opengl.GLES20 .glViewport ( v2,v2,v6,v8 );
/* .line 599 */
/* .line 548 */
} // .end local v3 # "vCoord":[F
} // .end local v4 # "tCoord":[F
} // .end local v5 # "indices":[S
} // :cond_1
} // :goto_0
/* :array_0 */
/* .array-data 4 */
/* -0x40800000 # -1.0f */
/* 0x3f800000 # 1.0f */
/* 0x0 */
/* -0x40800000 # -1.0f */
/* -0x40800000 # -1.0f */
/* 0x0 */
/* 0x3f800000 # 1.0f */
/* -0x40800000 # -1.0f */
/* 0x0 */
/* 0x3f800000 # 1.0f */
/* 0x3f800000 # 1.0f */
/* 0x0 */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x3f800000 # 1.0f */
/* 0x3f800000 # 1.0f */
/* 0x3f800000 # 1.0f */
/* 0x3f800000 # 1.0f */
/* 0x0 */
} // .end array-data
/* :array_2 */
/* .array-data 2 */
/* 0x0s */
/* 0x1s */
/* 0x2s */
/* 0x0s */
/* 0x2s */
/* 0x3s */
} // .end array-data
} // .end method
private Boolean isSizeChangeHappened ( ) {
/* .locals 3 */
/* .line 206 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayHeight:I */
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
/* if-ne v0, v1, :cond_0 */
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayWidth:I */
/* iget v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* if-eq v1, v2, :cond_1 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Integer loadShader ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .line 496 */
final String v0 = ""; // const-string v0, ""
/* .line 497 */
/* .local v0, "source":Ljava/lang/String; */
/* const v1, 0x8b31 */
/* if-ne p1, v1, :cond_0 */
/* .line 498 */
final String v0 = "#version 300 es \nlayout(location = 0) in vec4 a_position; \nlayout(location = 1) in vec2 a_texCoord; \nout vec2 v_texCoord; \nvoid main() \n{ \n gl_Position = a_position; \n v_texCoord = a_texCoord; \n} \n"; // const-string v0, "#version 300 es \nlayout(location = 0) in vec4 a_position; \nlayout(location = 1) in vec2 a_texCoord; \nout vec2 v_texCoord; \nvoid main() \n{ \n gl_Position = a_position; \n v_texCoord = a_texCoord; \n} \n"
/* .line 509 */
} // :cond_0
final String v0 = "#version 300 es \nprecision mediump float; \nin vec2 v_texCoord; \nuniform float s_Alpha; \nlayout(location = 0) out vec4 outColor; \nuniform sampler2D s_RandTex; \nvoid main() \n{ \n outColor.r = texture(s_RandTex, v_texCoord).x; \n outColor.g = texture(s_RandTex, v_texCoord).x; \n outColor.b = 1.1f * texture(s_RandTex, v_texCoord).x; \n outColor.a = s_Alpha; \n} \n"; // const-string v0, "#version 300 es \nprecision mediump float; \nin vec2 v_texCoord; \nuniform float s_Alpha; \nlayout(location = 0) out vec4 outColor; \nuniform sampler2D s_RandTex; \nvoid main() \n{ \n outColor.r = texture(s_RandTex, v_texCoord).x; \n outColor.g = texture(s_RandTex, v_texCoord).x; \n outColor.b = 1.1f * texture(s_RandTex, v_texCoord).x; \n outColor.a = s_Alpha; \n} \n"
/* .line 525 */
} // :goto_0
v1 = android.opengl.GLES20 .glCreateShader ( p1 );
/* .line 527 */
/* .local v1, "shader":I */
android.opengl.GLES20 .glShaderSource ( v1,v0 );
/* .line 528 */
android.opengl.GLES20 .glCompileShader ( v1 );
/* .line 530 */
int v2 = 1; // const/4 v2, 0x1
/* new-array v2, v2, [I */
/* .line 531 */
/* .local v2, "compiled":[I */
/* const v3, 0x8b81 */
int v4 = 0; // const/4 v4, 0x0
android.opengl.GLES20 .glGetShaderiv ( v1,v3,v2,v4 );
/* .line 532 */
/* aget v3, v2, v4 */
/* if-nez v3, :cond_1 */
/* .line 533 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Could not compile shader: "; // const-string v4, "Could not compile shader: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", "; // const-string v4, ", "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ":"; // const-string v4, ":"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiPaperContrastOverlay"; // const-string v4, "MiuiPaperContrastOverlay"
android.util.Slog .e ( v4,v3 );
/* .line 534 */
android.opengl.GLES20 .glGetShaderSource ( v1 );
android.util.Slog .e ( v4,v3 );
/* .line 535 */
android.opengl.GLES20 .glGetShaderInfoLog ( v1 );
android.util.Slog .e ( v4,v3 );
/* .line 536 */
android.opengl.GLES20 .glDeleteShader ( v1 );
/* .line 537 */
int v1 = 0; // const/4 v1, 0x0
/* .line 539 */
} // :cond_1
final String v3 = "loadShader"; // const-string v3, "loadShader"
com.android.server.wm.MiuiPaperContrastOverlay .checkGlErrors ( v3 );
/* .line 541 */
} // .end method
private static void logEglError ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p0, "func" # Ljava/lang/String; */
/* .line 879 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " failed: error "; // const-string v1, " failed: error "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.opengl.EGL14 .eglGetError ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-instance v1, Ljava/lang/Throwable; */
/* invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V */
final String v2 = "MiuiPaperContrastOverlay"; // const-string v2, "MiuiPaperContrastOverlay"
android.util.Slog .e ( v2,v0,v1 );
/* .line 880 */
return;
} // .end method
private void requestRandomBuffer ( ) {
/* .locals 2 */
/* .line 603 */
int v0 = 0; // const/4 v0, 0x0
this.mRandomTexBuffer = v0;
/* .line 604 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
/* mul-int/2addr v0, v1 */
/* mul-int/lit8 v0, v0, 0x4 */
java.nio.ByteBuffer .allocateDirect ( v0 );
/* .line 605 */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 606 */
(( java.nio.ByteBuffer ) v0 ).asIntBuffer ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;
this.mRandomTexBuffer = v0;
/* .line 608 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Request random buffer ("; // const-string v1, "Request random buffer ("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "), pixels: "; // const-string v1, "), pixels: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRandomTexBuffer;
/* .line 610 */
v1 = (( java.nio.IntBuffer ) v1 ).limit ( ); // invoke-virtual {v1}, Ljava/nio/IntBuffer;->limit()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 608 */
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
android.util.Slog .d ( v1,v0 );
/* .line 612 */
v0 = this.mRandomTexBuffer;
int v1 = 0; // const/4 v1, 0x0
(( java.nio.IntBuffer ) v0 ).position ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;
/* .line 613 */
return;
} // .end method
private void showSurface ( ) {
/* .locals 5 */
/* .line 328 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z */
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
/* if-nez v0, :cond_0 */
/* .line 329 */
final String v0 = "Surface is not prepared ready."; // const-string v0, "Surface is not prepared ready."
android.util.Slog .d ( v1,v0 );
/* .line 330 */
return;
/* .line 333 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->attachEglContext()Z */
/* if-nez v0, :cond_1 */
/* .line 334 */
final String v0 = "attachEglContext failed"; // const-string v0, "attachEglContext failed"
android.util.Slog .d ( v1,v0 );
/* .line 335 */
return;
/* .line 338 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 339 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperNoiseLevel:I */
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I */
/* .line 340 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperAlpha:F */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->drawSurface(F)V */
/* .line 342 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I */
/* .line 343 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->drawSurface(F)V */
/* .line 346 */
} // :goto_0
v0 = this.mWms;
v0 = this.mTransactionFactory;
/* check-cast v0, Landroid/view/SurfaceControl$Transaction; */
/* .line 347 */
/* .local v0, "t":Landroid/view/SurfaceControl$Transaction; */
v2 = this.mSurfaceControl;
v3 = this.mWms;
/* .line 348 */
(( com.android.server.wm.WindowManagerService ) v3 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
v3 = (( com.android.server.wm.DisplayContent ) v3 ).getDisplayId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
/* .line 347 */
com.android.server.wm.InputMonitor .setTrustedOverlayInputInfo ( v2,v0,v3,v1 );
/* .line 349 */
v2 = this.mBLASTSurfaceControl;
v3 = this.mWms;
/* .line 350 */
(( com.android.server.wm.WindowManagerService ) v3 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
v3 = (( com.android.server.wm.DisplayContent ) v3 ).getDisplayId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
/* .line 349 */
com.android.server.wm.InputMonitor .setTrustedOverlayInputInfo ( v2,v0,v3,v1 );
/* .line 351 */
v2 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v0 ).show ( v2 ); // invoke-virtual {v0, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
(( android.view.SurfaceControl$Transaction ) v2 ).apply ( ); // invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 352 */
final String v2 = "Apply to show surface."; // const-string v2, "Apply to show surface."
android.util.Slog .d ( v1,v2 );
/* .line 354 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = 1; // const/4 v2, 0x1
int v3 = -2; // const/4 v3, -0x2
final String v4 = "screen_paper_layer_show"; // const-string v4, "screen_paper_layer_show"
android.provider.Settings$System .putIntForUser ( v1,v4,v2,v3 );
/* .line 357 */
return;
} // .end method
private void updateDisplaySize ( ) {
/* .locals 4 */
/* .line 158 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayWidth:I */
/* .line 159 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayHeight:I */
/* .line 161 */
/* new-instance v0, Landroid/graphics/Point; */
/* invoke-direct {v0}, Landroid/graphics/Point;-><init>()V */
/* .line 162 */
/* .local v0, "displaySize":Landroid/graphics/Point; */
v1 = this.mWms;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.WindowManagerService ) v1 ).getBaseDisplaySize ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/WindowManagerService;->getBaseDisplaySize(ILandroid/graphics/Point;)V
/* .line 164 */
/* iget v1, v0, Landroid/graphics/Point;->x:I */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v1, v0, Landroid/graphics/Point;->y:I */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 165 */
/* iget v1, v0, Landroid/graphics/Point;->x:I */
/* iput v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* .line 166 */
/* iget v1, v0, Landroid/graphics/Point;->y:I */
/* iput v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
/* .line 168 */
} // :cond_0
v1 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v1 ).getDisplayInfo ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;
/* .line 169 */
/* .local v1, "defaultInfo":Landroid/view/DisplayInfo; */
/* iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I */
/* iget v3, v1, Landroid/view/DisplayInfo;->logicalHeight:I */
/* if-le v2, v3, :cond_1 */
/* .line 170 */
/* iget v2, v1, Landroid/view/DisplayInfo;->logicalHeight:I */
/* .line 171 */
} // :cond_1
/* iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I */
} // :goto_0
/* iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
/* .line 172 */
/* iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I */
/* iget v3, v1, Landroid/view/DisplayInfo;->logicalHeight:I */
/* if-le v2, v3, :cond_2 */
/* .line 173 */
/* iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I */
/* .line 174 */
} // :cond_2
/* iget v2, v1, Landroid/view/DisplayInfo;->logicalHeight:I */
} // :goto_1
/* iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
/* .line 178 */
} // .end local v1 # "defaultInfo":Landroid/view/DisplayInfo;
} // :goto_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Update surface ("; // const-string v2, "Update surface ("
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, v0, Landroid/graphics/Point;->x:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, v0, Landroid/graphics/Point;->y:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ") from ("; // const-string v3, ") from ("
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayWidth:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayHeight:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ") to ("; // const-string v3, ") to ("
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ","; // const-string v3, ","
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ") on thread: "; // const-string v3, ") on thread: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 181 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v3 ).getName ( ); // invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 178 */
final String v2 = "MiuiPaperContrastOverlay"; // const-string v2, "MiuiPaperContrastOverlay"
android.util.Slog .d ( v2,v1 );
/* .line 183 */
return;
} // .end method
private void updateGLShaders ( Float p0 ) {
/* .locals 4 */
/* .param p1, "alpha" # F */
/* .line 668 */
final String v0 = "MiuiPaperContrastOverlay"; // const-string v0, "MiuiPaperContrastOverlay"
/* const-string/jumbo v1, "updateGLShaders start" */
android.util.Slog .d ( v0,v1 );
/* .line 670 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createRandTexture()V */
/* .line 672 */
/* const v0, 0x84c0 */
android.opengl.GLES20 .glActiveTexture ( v0 );
/* .line 673 */
v0 = this.mTextureId;
int v1 = 0; // const/4 v1, 0x0
/* aget v0, v0, v1 */
/* const/16 v2, 0xde1 */
android.opengl.GLES20 .glBindTexture ( v2,v0 );
/* .line 675 */
/* iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I */
final String v2 = "s_RandTex"; // const-string v2, "s_RandTex"
v0 = android.opengl.GLES20 .glGetUniformLocation ( v0,v2 );
/* .line 676 */
/* .local v0, "samplerLocR":I */
/* iget v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I */
final String v3 = "s_Alpha"; // const-string v3, "s_Alpha"
v2 = android.opengl.GLES20 .glGetUniformLocation ( v2,v3 );
/* iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->samplerLocA:I */
/* .line 678 */
android.opengl.GLES20 .glUniform1i ( v0,v1 );
/* .line 679 */
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->samplerLocA:I */
android.opengl.GLES20 .glUniform1f ( v1,p1 );
/* .line 680 */
return;
} // .end method
/* # virtual methods */
public void changeDeviceReady ( ) {
/* .locals 1 */
/* .line 154 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z */
/* .line 155 */
return;
} // .end method
public void changeShowLayerStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "needShow" # Z */
/* .line 150 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mNeedShowPaperSurface:Z */
/* .line 151 */
return;
} // .end method
public android.view.SurfaceControl getSurfaceControl ( ) {
/* .locals 1 */
/* .line 900 */
v0 = this.mSurfaceControl;
} // .end method
public void hidePaperModeSurface ( ) {
/* .locals 4 */
/* .line 318 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z */
/* .line 319 */
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I */
/* .line 320 */
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I */
/* .line 321 */
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
final String v2 = "Hide paper-mode surface.Dark-mode is disable."; // const-string v2, "Hide paper-mode surface.Dark-mode is disable."
android.util.Slog .d ( v1,v2 );
/* .line 322 */
v1 = this.mWms;
v1 = this.mTransactionFactory;
/* check-cast v1, Landroid/view/SurfaceControl$Transaction; */
v2 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v1 ).hide ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
(( android.view.SurfaceControl$Transaction ) v1 ).apply ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 323 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "screen_paper_layer_show"; // const-string v2, "screen_paper_layer_show"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .putIntForUser ( v1,v2,v0,v3 );
/* .line 325 */
return;
} // .end method
public void showPaperModeSurface ( ) {
/* .locals 5 */
/* .line 289 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->IS_FOLDABLE_DEVICE:Z */
final String v1 = "MiuiPaperContrastOverlay"; // const-string v1, "MiuiPaperContrastOverlay"
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z */
/* if-nez v0, :cond_0 */
/* .line 290 */
final String v0 = "The fold device is not ready, paper-mode return."; // const-string v0, "The fold device is not ready, paper-mode return."
android.util.Slog .i ( v1,v0 );
/* .line 291 */
return;
/* .line 295 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->isSizeChangeHappened()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 296 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroySurface()V */
/* .line 298 */
} // :cond_1
v0 = this.mSurfaceControl;
/* if-nez v0, :cond_2 */
/* .line 299 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createSurface()V */
/* .line 301 */
} // :cond_2
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = -2; // const/4 v3, -0x2
/* const-string/jumbo v4, "screen_texture_eyecare_level" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v4,v2,v3 );
/* .line 304 */
/* .local v0, "level":I */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z */
/* .line 305 */
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperNoiseLevel:I */
/* .line 306 */
/* int-to-float v2, v0 */
/* const/high16 v3, 0x437f0000 # 255.0f */
/* div-float/2addr v2, v3 */
/* iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperAlpha:F */
/* .line 308 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Paper-mode surface params.Alpha: "; // const-string v3, "Paper-mode surface params.Alpha: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperAlpha:F */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", Level from "; // const-string v3, ", Level from "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " to "; // const-string v3, " to "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 311 */
/* iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I */
/* if-eq v0, v1, :cond_3 */
/* .line 312 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->showSurface()V */
/* .line 314 */
} // :cond_3
/* iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I */
/* .line 315 */
return;
} // .end method
