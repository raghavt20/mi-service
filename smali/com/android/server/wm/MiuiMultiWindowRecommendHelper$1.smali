.class Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "MiuiMultiWindowRecommendHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    .line 92
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 10
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 95
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$misDeviceSupportSplitScreenRecommend(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V

    .line 99
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->getFocusedTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 100
    .local v0, "focusedTask":Lcom/android/server/wm/Task;
    const/4 v1, 0x0

    .line 101
    .local v1, "focusedTaskPackageName":Ljava/lang/String;
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v2, :cond_1

    .line 102
    iget-object v2, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move-object v7, v1

    goto :goto_0

    .line 104
    :cond_1
    move-object v7, v1

    .end local v1    # "focusedTaskPackageName":Ljava/lang/String;
    .local v7, "focusedTaskPackageName":Ljava/lang/String;
    :goto_0
    if-eqz v7, :cond_2

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 106
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v1, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$mcheckPreConditionsForSplitScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Lcom/android/server/wm/Task;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 107
    iget-object v8, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    new-instance v9, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    iget v2, v0, Lcom/android/server/wm/Task;->mTaskId:I

    .line 108
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v1, v9

    move-object v3, v7

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;-><init>(ILjava/lang/String;JLcom/android/server/wm/Task;)V

    .line 107
    invoke-static {v8, v9}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$mpredictSplitScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V

    .line 109
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " focused task id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " packageName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiMultiWindowRecommendHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_2
    return-void

    .line 96
    .end local v0    # "focusedTask":Lcom/android/server/wm/Task;
    .end local v7    # "focusedTaskPackageName":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void
.end method
