.class Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;
.super Lcom/android/server/wm/MiuiPaperContrastOverlayStub;
.source "MiuiPaperContrastOverlayStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.MiuiPaperContrastOverlayStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$UserSwitchReceivere;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiPaperContrastOverlayStubImpl"


# instance fields
.field mContext:Landroid/content/Context;

.field private mFoldDeviceReady:Z

.field mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;

.field private final mPaperModeEnableUri:Landroid/net/Uri;

.field private final mPaperModeTypeUri:Landroid/net/Uri;

.field private final mSecurityModeGbUri:Landroid/net/Uri;

.field private final mSecurityModeVtbUri:Landroid/net/Uri;

.field private final mTextureEyeCareLevelUri:Landroid/net/Uri;

.field mWmService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public static synthetic $r8$lambda$S1KpKV9QLNHuolHZsT0VuWwW2ZM(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->lambda$updateTextureEyeCareLevel$0(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmPaperModeEnableUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mPaperModeEnableUri:Landroid/net/Uri;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPaperModeTypeUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mPaperModeTypeUri:Landroid/net/Uri;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSecurityModeGbUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mSecurityModeGbUri:Landroid/net/Uri;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSecurityModeVtbUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mSecurityModeVtbUri:Landroid/net/Uri;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTextureEyeCareLevelUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mTextureEyeCareLevelUri:Landroid/net/Uri;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmFoldDeviceReady(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mFoldDeviceReady:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetSecurityCenterStatus(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->getSecurityCenterStatus()Z

    move-result p0

    return p0
.end method

.method constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStub;-><init>()V

    .line 33
    nop

    .line 34
    const-string/jumbo v0, "screen_texture_eyecare_level"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mTextureEyeCareLevelUri:Landroid/net/Uri;

    .line 35
    nop

    .line 36
    const-string v0, "screen_mode_type"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mPaperModeTypeUri:Landroid/net/Uri;

    .line 37
    nop

    .line 38
    const-string/jumbo v0, "screen_paper_mode_enabled"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mPaperModeEnableUri:Landroid/net/Uri;

    .line 39
    const-string/jumbo v0, "vtb_boosting"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mSecurityModeVtbUri:Landroid/net/Uri;

    .line 40
    const-string v0, "gb_boosting"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mSecurityModeGbUri:Landroid/net/Uri;

    return-void
.end method

.method private getSecurityCenterStatus()Z
    .locals 4

    .line 130
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gb_boosting"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mContext:Landroid/content/Context;

    .line 132
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "vtb_boosting"

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    nop

    .line 130
    :goto_0
    return v2
.end method

.method private synthetic lambda$updateTextureEyeCareLevel$0(Z)V
    .locals 7
    .param p1, "eyecareEnable"    # Z

    .line 59
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    monitor-enter v0

    .line 60
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 63
    :try_start_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_mode_type"

    const/4 v3, -0x2

    const/4 v4, 0x0

    invoke-static {v1, v2, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 65
    .local v1, "paperModeType":I
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mContext:Landroid/content/Context;

    .line 66
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v5, "screen_paper_mode_enabled"

    .line 65
    invoke-static {v2, v5, v4, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v2

    .line 69
    .local v2, "paperModeEnable":Z
    const-string v3, "MiuiPaperContrastOverlayStubImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Update paper-mode, param is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    if-eqz p1, :cond_3

    if-eqz v2, :cond_3

    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    const/4 v5, 0x3

    if-ne v1, v5, :cond_3

    .line 72
    :cond_0
    iget-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    if-nez v4, :cond_1

    .line 73
    iget-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mContext:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->getInstance(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)Lcom/android/server/wm/MiuiPaperContrastOverlay;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    .line 75
    :cond_1
    sget-boolean v4, Lcom/android/server/wm/MiuiPaperContrastOverlay;->IS_FOLDABLE_DEVICE:Z

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mFoldDeviceReady:Z

    if-eqz v4, :cond_2

    .line 76
    iget-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-virtual {v4}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->changeDeviceReady()V

    .line 78
    :cond_2
    iget-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-virtual {v4, v3}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->changeShowLayerStatus(Z)V

    .line 79
    const-string v3, "MiuiPaperContrastOverlayStubImpl"

    const-string v4, "Show paper-mode surface."

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->showPaperModeSurface()V

    goto :goto_0

    .line 82
    :cond_3
    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    if-eqz v3, :cond_4

    .line 83
    invoke-virtual {v3, v4}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->changeShowLayerStatus(Z)V

    .line 84
    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->hidePaperModeSurface()V

    .line 86
    :cond_4
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    .end local v1    # "paperModeType":I
    .end local v2    # "paperModeEnable":Z
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    const-string/jumbo v2, "updateTextureEyeCareLevel"

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 90
    nop

    .line 91
    monitor-exit v0

    .line 92
    return-void

    .line 89
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    const-string/jumbo v3, "updateTextureEyeCareLevel"

    invoke-virtual {v2, v3}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 90
    nop

    .end local p0    # "this":Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;
    .end local p1    # "eyecareEnable":Z
    throw v1

    .line 91
    .restart local p0    # "this":Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;
    .restart local p1    # "eyecareEnable":Z
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method


# virtual methods
.method public getMiuiPaperSurfaceControl()Landroid/view/SurfaceControl;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mMiuiPaperSurface:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v0

    return-object v0

    .line 148
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public init(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V
    .locals 1
    .param p1, "wms"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mContext:Landroid/content/Context;

    .line 45
    iput-object p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    .line 46
    invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->registerObserver(Landroid/content/Context;)V

    .line 48
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->getSecurityCenterStatus()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->updateTextureEyeCareLevel(Z)V

    .line 49
    return-void
.end method

.method public registerObserver(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 96
    new-instance v0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Landroid/os/Handler;)V

    .line 112
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mTextureEyeCareLevelUri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mPaperModeTypeUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 116
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mPaperModeEnableUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mSecurityModeVtbUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mSecurityModeGbUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 123
    invoke-virtual {v0, v4}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 125
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$UserSwitchReceivere;

    invoke-direct {v3, p0, v1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$UserSwitchReceivere;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$UserSwitchReceivere-IA;)V

    new-instance v1, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.USER_SWITCHED"

    invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 127
    return-void
.end method

.method public updateTextureEyeCareLevel(Z)V
    .locals 2
    .param p1, "eyecareEnable"    # Z

    .line 58
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    new-instance v1, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Z)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 93
    return-void
.end method

.method public updateTextureEyeCareWhenAodShow(Z)V
    .locals 1
    .param p1, "aodShowing"    # Z

    .line 53
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->getSecurityCenterStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->updateTextureEyeCareLevel(Z)V

    .line 54
    return-void
.end method
