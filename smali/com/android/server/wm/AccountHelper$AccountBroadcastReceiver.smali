.class Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AccountHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/AccountHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/AccountHelper;


# direct methods
.method private constructor <init>(Lcom/android/server/wm/AccountHelper;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;->this$0:Lcom/android/server/wm/AccountHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/wm/AccountHelper;Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;-><init>(Lcom/android/server/wm/AccountHelper;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 116
    if-nez p2, :cond_0

    .line 117
    return-void

    .line 119
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 121
    const-string v1, "extra_update_type"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 122
    .local v1, "type":I
    const-string v2, "extra_account"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    .line 123
    .local v2, "account":Landroid/accounts/Account;
    const-string v3, "MiuiPermision"

    if-eqz v2, :cond_4

    iget-object v4, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v5, "com.xiaomi"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_0

    .line 128
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 129
    .local v4, "appContext":Landroid/content/Context;
    const/4 v5, 0x1

    if-ne v1, v5, :cond_2

    .line 131
    iget-object v3, p0, Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;->this$0:Lcom/android/server/wm/AccountHelper;

    invoke-virtual {v3, v4, v2}, Lcom/android/server/wm/AccountHelper;->onXiaomiAccountLogout(Landroid/content/Context;Landroid/accounts/Account;)V

    goto :goto_1

    .line 132
    :cond_2
    const/4 v5, 0x2

    if-ne v1, v5, :cond_3

    .line 134
    iget-object v3, p0, Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;->this$0:Lcom/android/server/wm/AccountHelper;

    invoke-virtual {v3, v4, v2}, Lcom/android/server/wm/AccountHelper;->onXiaomiAccountLogin(Landroid/content/Context;Landroid/accounts/Account;)V

    goto :goto_1

    .line 136
    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    filled-new-array {v5}, [Ljava/lang/Object;

    move-result-object v5

    const-string v6, "Xiaomi account changed, but unknown type: %s."

    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 124
    .end local v4    # "appContext":Landroid/content/Context;
    :cond_4
    :goto_0
    const-string v4, "It isn\'t a xiaomi account changed."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    return-void

    .line 139
    .end local v1    # "type":I
    .end local v2    # "account":Landroid/accounts/Account;
    :cond_5
    :goto_1
    return-void
.end method
