.class public final Lcom/android/server/wm/MiuiFreeformTrackManager$CommonTrackConstants;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiFreeformTrackManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CommonTrackConstants"
.end annotation


# static fields
.field public static final DATA_VERSION:Ljava/lang/String; = "22053100"

.field public static final DEVICE_TYPE_PAD:Ljava/lang/String; = "pad"

.field public static final DEVICE_TYPE_PHONE:Ljava/lang/String; = "\u624b\u673a"

.field public static final SCREEN_ORIENTATION_LANDSCAPE:Ljava/lang/String; = "\u6a2a\u5c4f"

.field public static final SCREEN_ORIENTATION_PORTRAIT:Ljava/lang/String; = "\u7ad6\u5c4f"

.field public static final SCREEN_TYPE_INNER:Ljava/lang/String; = "\u5185\u5c4f"

.field public static final SCREEN_TYPE_NOTHING:Ljava/lang/String; = "nothing"

.field public static final SCREEN_TYPE_OUTTER:Ljava/lang/String; = "\u5916\u5c4f"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
