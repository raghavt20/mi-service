.class Lcom/android/server/wm/TalkbackWatermark;
.super Ljava/lang/Object;
.source "TalkbackWatermark.java"

# interfaces
.implements Lcom/android/server/wm/TalkbackWatermarkStub;


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "TalkbackWatermark"


# instance fields
.field private mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

.field private mBroadcast:Landroid/content/BroadcastReceiver;

.field private final mDetDp:F

.field private mDetPx:I

.field private mDrawNeeded:Z

.field private mHasDrawn:Z

.field private mLastDH:I

.field private mLastDW:I

.field private final mPaddingDp:F

.field private mPaddingPx:I

.field private final mShadowDx:F

.field private final mShadowDy:F

.field private final mShadowRadius:F

.field private mString1:Ljava/lang/String;

.field private mString2:Ljava/lang/String;

.field private mSurface:Landroid/view/Surface;

.field private mSurfaceControl:Landroid/view/SurfaceControl;

.field private final mTextSizeDp:F

.field private mTextSizePx:I

.field private final mTitleSizeDp:F

.field private mTitleSizePx:F

.field private mTransaction:Landroid/view/SurfaceControl$Transaction;

.field private mWms:Lcom/android/server/wm/WindowManagerService;

.field private final mXProportion:F

.field private final mYProportionTop:F


# direct methods
.method public static synthetic $r8$lambda$37iju6o-pbGYJtR2DbIm9LM8X74(Lcom/android/server/wm/TalkbackWatermark;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->lambda$show$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$EP-Zo72UAMbTgk_9DhzaaAQRevs(Lcom/android/server/wm/TalkbackWatermark;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->lambda$updateWaterMark$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$eVfletGoOtHMriGPp_gr3ZUCg7U(Lcom/android/server/wm/TalkbackWatermark;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->dismissInternal()V

    return-void
.end method

.method public static synthetic $r8$lambda$lCnD30CJjjaPm4mWDDhqHPVtkdA(Lcom/android/server/wm/TalkbackWatermark;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->lambda$dismiss$2()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrefresh(Lcom/android/server/wm/TalkbackWatermark;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->refresh()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateWaterMark(Lcom/android/server/wm/TalkbackWatermark;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->updateWaterMark()V

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const v0, 0x41cb999a    # 25.45f

    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mTitleSizeDp:F

    .line 70
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mTextSizeDp:F

    .line 71
    const v0, 0x41a2f5c3    # 20.37f

    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mDetDp:F

    .line 72
    const v0, 0x4145c28f    # 12.36f

    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mPaddingDp:F

    .line 73
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mShadowRadius:F

    .line 74
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mShadowDx:F

    .line 75
    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mShadowDy:F

    .line 80
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mYProportionTop:F

    .line 81
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mXProportion:F

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 86
    iput-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    .line 87
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I

    .line 88
    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I

    .line 89
    iput-boolean v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z

    .line 90
    iput-boolean v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mHasDrawn:Z

    return-void
.end method

.method private declared-synchronized dismissInternal()V
    .locals 3

    monitor-enter p0

    .line 352
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_0

    .line 353
    monitor-exit p0

    return-void

    .line 355
    :cond_0
    :try_start_1
    const-string v0, "TalkbackWatermark"

    const-string/jumbo v1, "talkback-test dismissInternal"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->hideInternal()V

    .line 357
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->destroy()V

    .line 358
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    invoke-virtual {v0}, Landroid/graphics/BLASTBufferQueue;->destroy()V

    .line 359
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 362
    :try_start_2
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl;->reparent(Landroid/view/SurfaceControl;)V

    .line 363
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->release()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 366
    :try_start_3
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    const-string/jumbo v2, "updateTalkbackWatermark"

    invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 367
    nop

    .line 368
    iput-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    .line 369
    iput-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 370
    iput-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurface:Landroid/view/Surface;

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mHasDrawn:Z

    .line 372
    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I

    .line 373
    iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 374
    monitor-exit p0

    return-void

    .line 366
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :catchall_0
    move-exception v0

    :try_start_4
    iget-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    const-string/jumbo v2, "updateTalkbackWatermark"

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 367
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 351
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized doCreateSurface(Lcom/android/server/wm/WindowManagerService;)V
    .locals 10
    .param p1, "wms"    # Lcom/android/server/wm/WindowManagerService;

    monitor-enter p0

    .line 154
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 155
    .local v0, "dc":Lcom/android/server/wm/DisplayContent;
    iget-object v1, v0, Lcom/android/server/wm/DisplayContent;->mRealDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    const/high16 v2, 0x43200000    # 160.0f

    div-float/2addr v1, v2

    .line 156
    .local v1, "constNum":F
    const/high16 v2, 0x41a00000    # 20.0f

    mul-float/2addr v2, v1

    float-to-int v2, v2

    iput v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mTextSizePx:I

    .line 157
    const v2, 0x41a2f5c3    # 20.37f

    mul-float/2addr v2, v1

    float-to-int v2, v2

    iput v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mDetPx:I

    .line 158
    const v2, 0x4145c28f    # 12.36f

    mul-float/2addr v2, v1

    float-to-int v2, v2

    iput v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mPaddingPx:I

    .line 159
    const v2, 0x41cb999a    # 25.45f

    mul-float/2addr v2, v1

    float-to-int v2, v2

    int-to-float v2, v2

    iput v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mTitleSizePx:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    const/4 v2, 0x0

    .line 163
    .local v2, "ctrl":Landroid/view/SurfaceControl;
    :try_start_1
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    const-string v4, "TalkbackWatermarkSurface"

    .line 164
    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 165
    invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 166
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v4}, Landroid/view/SurfaceControl$Builder;->setBufferSize(II)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 167
    const/4 v4, -0x3

    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setFormat(I)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    const-string v4, "TalkbackWatermarkSurface"

    .line 168
    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v3

    .line 169
    invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v3

    move-object v2, v3

    .line 170
    iget-object v3, p1, Lcom/android/server/wm/WindowManagerService;->mTransactionFactory:Ljava/util/function/Supplier;

    invoke-interface {v3}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceControl$Transaction;

    iput-object v3, p0, Lcom/android/server/wm/TalkbackWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    .line 173
    const v4, 0xf4240

    invoke-virtual {v3, v2, v4}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    .line 174
    iget-object v3, p0, Lcom/android/server/wm/TalkbackWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4, v4}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 175
    iget-object v3, p0, Lcom/android/server/wm/TalkbackWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v3, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 176
    iget-object v3, p0, Lcom/android/server/wm/TalkbackWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v4

    const-string v5, "TalkbackWatermarkSurface"

    invoke-static {v2, v3, v4, v5}, Lcom/android/server/wm/InputMonitor;->setTrustedOverlayInputInfo(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;ILjava/lang/String;)V

    .line 177
    iget-object v3, p0, Lcom/android/server/wm/TalkbackWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v3}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 178
    iput-object v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 179
    new-instance v3, Landroid/graphics/BLASTBufferQueue;

    const-string v5, "TalkbackWatermarkSurface"

    iget-object v6, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object v4, v3

    invoke-direct/range {v4 .. v9}, Landroid/graphics/BLASTBufferQueue;-><init>(Ljava/lang/String;Landroid/view/SurfaceControl;III)V

    iput-object v3, p0, Lcom/android/server/wm/TalkbackWatermark;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    .line 181
    invoke-virtual {v3}, Landroid/graphics/BLASTBufferQueue;->createSurface()Landroid/view/Surface;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurface:Landroid/view/Surface;
    :try_end_1
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    goto :goto_0

    .line 182
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :catch_0
    move-exception v3

    .line 183
    .local v3, "e":Landroid/view/Surface$OutOfResourcesException;
    :try_start_2
    const-string v4, "TalkbackWatermark"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createrSurface e"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 185
    .end local v3    # "e":Landroid/view/Surface$OutOfResourcesException;
    :goto_0
    monitor-exit p0

    return-void

    .line 153
    .end local v0    # "dc":Lcom/android/server/wm/DisplayContent;
    .end local v1    # "constNum":F
    .end local v2    # "ctrl":Landroid/view/SurfaceControl;
    .end local p1    # "wms":Lcom/android/server/wm/WindowManagerService;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private drawIfNeeded()V
    .locals 36

    .line 204
    move-object/from16 v1, p0

    iget-boolean v0, v1, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z

    if-eqz v0, :cond_4

    .line 205
    iget v2, v1, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I

    .line 206
    .local v2, "dw":I
    iget v3, v1, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I

    .line 208
    .local v3, "dh":I
    iget-object v0, v1, Lcom/android/server/wm/TalkbackWatermark;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    iget-object v4, v1, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v2, v3, v5}, Landroid/graphics/BLASTBufferQueue;->update(Landroid/view/SurfaceControl;III)V

    .line 210
    new-instance v0, Landroid/graphics/Rect;

    const/4 v4, 0x0

    invoke-direct {v0, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v6, v0

    .line 211
    .local v6, "dirty":Landroid/graphics/Rect;
    const/4 v7, 0x0

    .line 213
    .local v7, "c":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v0, v1, Lcom/android/server/wm/TalkbackWatermark;->mSurface:Landroid/view/Surface;

    invoke-virtual {v0, v6}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v7, v0

    .line 216
    goto :goto_0

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v8, "TalkbackWatermark"

    const-string v9, "Failed to lock canvas"

    invoke-static {v8, v9, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 218
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :goto_0
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    if-ne v0, v2, :cond_3

    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    if-eq v0, v3, :cond_0

    goto/16 :goto_3

    .line 222
    :cond_0
    iput-boolean v4, v1, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z

    .line 224
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v7, v4, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 226
    const/16 v0, 0x3c

    .line 227
    .local v0, "offset":I
    const/high16 v8, 0x3f000000    # 0.5f

    int-to-float v9, v2

    mul-float/2addr v9, v8

    float-to-int v8, v9

    .line 228
    .local v8, "x":I
    const v9, 0x3ecccccd    # 0.4f

    int-to-float v10, v3

    mul-float/2addr v10, v9

    float-to-int v9, v10

    add-int/lit8 v9, v9, 0x3c

    .line 230
    .local v9, "y":I
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10, v5}, Landroid/graphics/Paint;-><init>(I)V

    move-object v15, v10

    .line 231
    .local v15, "paint":Landroid/graphics/Paint;
    iget v10, v1, Lcom/android/server/wm/TalkbackWatermark;->mTitleSizePx:F

    invoke-virtual {v15, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 232
    sget-object v10, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v10, v4}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 233
    const v4, -0x4c4c4d

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 234
    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 235
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v10, -0x1000000

    const/high16 v11, 0x40000000    # 2.0f

    invoke-virtual {v15, v4, v11, v11, v10}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 236
    iget-object v4, v1, Lcom/android/server/wm/TalkbackWatermark;->mString1:Ljava/lang/String;

    int-to-float v10, v8

    int-to-float v11, v9

    invoke-virtual {v7, v4, v10, v11, v15}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 238
    iget v4, v1, Lcom/android/server/wm/TalkbackWatermark;->mTextSizePx:I

    int-to-float v4, v4

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 239
    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4, v15}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    move-object/from16 v20, v4

    .line 241
    .local v20, "textPaint":Landroid/text/TextPaint;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v4

    .line 242
    .local v4, "dir":I
    if-ne v4, v5, :cond_2

    .line 243
    const/4 v10, 0x1

    .line 244
    .local v10, "line":I
    const/4 v11, 0x0

    move/from16 v27, v10

    move v14, v11

    .end local v10    # "line":I
    .local v14, "i":I
    .local v27, "line":I
    :goto_1
    iget-object v10, v1, Lcom/android/server/wm/TalkbackWatermark;->mString2:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v14, v10, :cond_1

    .line 245
    iget-object v10, v1, Lcom/android/server/wm/TalkbackWatermark;->mString2:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v23

    const/16 v24, 0x1

    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    move-result v11

    iget v12, v1, Lcom/android/server/wm/TalkbackWatermark;->mPaddingPx:I

    sub-int/2addr v11, v12

    int-to-float v11, v11

    const/16 v26, 0x0

    move-object/from16 v21, v10

    move/from16 v22, v14

    move/from16 v25, v11

    invoke-virtual/range {v20 .. v26}, Landroid/text/TextPaint;->breakText(Ljava/lang/CharSequence;IIZF[F)I

    move-result v21

    .line 246
    .local v21, "len":I
    iget-object v10, v1, Lcom/android/server/wm/TalkbackWatermark;->mString2:Ljava/lang/String;

    add-int v11, v14, v21

    invoke-virtual {v10, v14, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    const/4 v12, 0x0

    const/16 v16, 0x0

    int-to-float v13, v8

    iget v10, v1, Lcom/android/server/wm/TalkbackWatermark;->mDetPx:I

    mul-int v10, v10, v27

    add-int/2addr v10, v9

    int-to-float v10, v10

    const/16 v18, 0x1

    move/from16 v17, v10

    move-object v10, v7

    move/from16 v19, v13

    move/from16 v13, v21

    .end local v14    # "i":I
    .local v22, "i":I
    move/from16 v14, v16

    move-object/from16 v23, v15

    .end local v15    # "paint":Landroid/graphics/Paint;
    .local v23, "paint":Landroid/graphics/Paint;
    move/from16 v15, v21

    move/from16 v16, v19

    move-object/from16 v19, v20

    invoke-virtual/range {v10 .. v19}, Landroid/graphics/Canvas;->drawTextRun([CIIIIFFZLandroid/graphics/Paint;)V

    .line 247
    add-int v14, v22, v21

    .line 248
    .end local v22    # "i":I
    .restart local v14    # "i":I
    nop

    .end local v21    # "len":I
    add-int/lit8 v27, v27, 0x1

    .line 249
    move-object/from16 v15, v23

    goto :goto_1

    .line 244
    .end local v23    # "paint":Landroid/graphics/Paint;
    .restart local v15    # "paint":Landroid/graphics/Paint;
    :cond_1
    move/from16 v22, v14

    move-object/from16 v23, v15

    .line 250
    .end local v14    # "i":I
    .end local v15    # "paint":Landroid/graphics/Paint;
    .end local v27    # "line":I
    .restart local v23    # "paint":Landroid/graphics/Paint;
    goto :goto_2

    .line 251
    .end local v23    # "paint":Landroid/graphics/Paint;
    .restart local v15    # "paint":Landroid/graphics/Paint;
    :cond_2
    move-object/from16 v23, v15

    .end local v15    # "paint":Landroid/graphics/Paint;
    .restart local v23    # "paint":Landroid/graphics/Paint;
    new-instance v10, Landroid/text/StaticLayout;

    iget-object v11, v1, Lcom/android/server/wm/TalkbackWatermark;->mString2:Ljava/lang/String;

    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    move-result v12

    iget v13, v1, Lcom/android/server/wm/TalkbackWatermark;->mPaddingPx:I

    sub-int v31, v12, v13

    sget-object v32, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v33, 0x3f800000    # 1.0f

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v28, v10

    move-object/from16 v29, v11

    move-object/from16 v30, v20

    invoke-direct/range {v28 .. v35}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 252
    .local v10, "staticLayout":Landroid/text/StaticLayout;
    invoke-virtual {v7}, Landroid/graphics/Canvas;->save()I

    .line 253
    int-to-float v11, v8

    iget v12, v1, Lcom/android/server/wm/TalkbackWatermark;->mDetPx:I

    add-int/2addr v12, v9

    int-to-float v12, v12

    invoke-virtual {v7, v11, v12}, Landroid/graphics/Canvas;->translate(FF)V

    .line 254
    invoke-virtual {v10, v7}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 255
    invoke-virtual {v7}, Landroid/graphics/Canvas;->restore()V

    .line 258
    .end local v10    # "staticLayout":Landroid/text/StaticLayout;
    :goto_2
    iget-object v10, v1, Lcom/android/server/wm/TalkbackWatermark;->mSurface:Landroid/view/Surface;

    invoke-virtual {v10, v7}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 259
    iput-boolean v5, v1, Lcom/android/server/wm/TalkbackWatermark;->mHasDrawn:Z

    goto :goto_4

    .line 219
    .end local v0    # "offset":I
    .end local v4    # "dir":I
    .end local v8    # "x":I
    .end local v9    # "y":I
    .end local v20    # "textPaint":Landroid/text/TextPaint;
    .end local v23    # "paint":Landroid/graphics/Paint;
    :cond_3
    :goto_3
    return-void

    .line 261
    .end local v2    # "dw":I
    .end local v3    # "dh":I
    .end local v6    # "dirty":Landroid/graphics/Rect;
    .end local v7    # "c":Landroid/graphics/Canvas;
    :cond_4
    :goto_4
    return-void
.end method

.method private declared-synchronized hideInternal()V
    .locals 3

    monitor-enter p0

    .line 337
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-nez v0, :cond_0

    .line 338
    monitor-exit p0

    return-void

    .line 340
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 342
    :try_start_2
    invoke-static {}, Landroid/view/SurfaceControl;->getGlobalTransaction()Landroid/view/SurfaceControl$Transaction;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 343
    .local v0, "transaction":Landroid/view/SurfaceControl$Transaction;
    if-eqz v0, :cond_1

    .line 344
    :try_start_3
    iget-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 347
    .end local v0    # "transaction":Landroid/view/SurfaceControl$Transaction;
    :catchall_0
    move-exception v0

    goto :goto_1

    :cond_1
    :goto_0
    :try_start_4
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    const-string/jumbo v1, "updateTalkbackWatermark"

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 348
    nop

    .line 349
    monitor-exit p0

    return-void

    .line 347
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :catchall_1
    move-exception v0

    :goto_1
    :try_start_5
    iget-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    const-string/jumbo v2, "updateTalkbackWatermark"

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 348
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 336
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private synthetic lambda$dismiss$2()V
    .locals 3

    .line 284
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mBroadcast:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    goto :goto_0

    .line 285
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "TalkbackWatermark"

    const-string v2, "mBroadcast is not registered"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 288
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :goto_0
    return-void
.end method

.method private synthetic lambda$show$1()V
    .locals 4

    .line 267
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mBroadcast:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 268
    return-void
.end method

.method private synthetic lambda$updateWaterMark$0()V
    .locals 5

    .line 135
    const-string/jumbo v0, "updateTalkbackWatermark"

    iget-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, -0x2

    const-string/jumbo v3, "talkback_watermark_enable"

    const/4 v4, 0x1

    invoke-static {v1, v3, v4, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    move v1, v4

    .line 137
    .local v1, "enabled":Z
    iget-object v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "talkback-test enabled ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "TalkbackWatermark"

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    if-eqz v1, :cond_1

    .line 142
    iget-object v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-direct {p0, v2}, Lcom/android/server/wm/TalkbackWatermark;->doCreateSurface(Lcom/android/server/wm/WindowManagerService;)V

    .line 143
    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->showInternal()V

    goto :goto_1

    .line 145
    :cond_1
    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->dismissInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :goto_1
    iget-object v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 149
    nop

    .line 150
    return-void

    .line 148
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v3, v0}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 149
    throw v2
.end method

.method private declared-synchronized refresh()V
    .locals 2

    monitor-enter p0

    .line 378
    :try_start_0
    const-string v0, "TalkbackWatermark"

    const-string/jumbo v1, "talkback-test refresh"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 380
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z

    .line 381
    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->updateWaterMark()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    monitor-exit p0

    return-void

    .line 377
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setupBroadcast()V
    .locals 1

    monitor-enter p0

    .line 118
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mBroadcast:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Lcom/android/server/wm/TalkbackWatermark$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/TalkbackWatermark$2;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V

    iput-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mBroadcast:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :cond_0
    monitor-exit p0

    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized showInternal()V
    .locals 6

    monitor-enter p0

    .line 309
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-nez v0, :cond_0

    .line 310
    monitor-exit p0

    return-void

    .line 312
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V

    .line 313
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 314
    const v1, 0x110f03b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mString1:Ljava/lang/String;

    .line 315
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 316
    const v1, 0x110f03b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mString2:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 319
    :try_start_2
    invoke-static {}, Landroid/view/SurfaceControl;->getGlobalTransaction()Landroid/view/SurfaceControl$Transaction;

    move-result-object v0

    .line 320
    .local v0, "transaction":Landroid/view/SurfaceControl$Transaction;
    iget-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    .line 321
    .local v1, "defaultDc":Lcom/android/server/wm/DisplayContent;
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v2

    .line 322
    .local v2, "defaultInfo":Landroid/view/DisplayInfo;
    iget v3, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    .line 323
    .local v3, "defaultDw":I
    iget v4, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    .line 324
    .local v4, "defaultDh":I
    iget v5, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v5, :cond_1

    :try_start_3
    iget v5, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v5, :cond_2

    goto :goto_0

    .line 332
    .end local v0    # "transaction":Landroid/view/SurfaceControl$Transaction;
    .end local v1    # "defaultDc":Lcom/android/server/wm/DisplayContent;
    .end local v2    # "defaultInfo":Landroid/view/DisplayInfo;
    .end local v3    # "defaultDw":I
    .end local v4    # "defaultDh":I
    :catchall_0
    move-exception v0

    goto :goto_1

    .line 325
    .restart local v0    # "transaction":Landroid/view/SurfaceControl$Transaction;
    .restart local v1    # "defaultDc":Lcom/android/server/wm/DisplayContent;
    .restart local v2    # "defaultInfo":Landroid/view/DisplayInfo;
    .restart local v3    # "defaultDw":I
    .restart local v4    # "defaultDh":I
    :cond_1
    :goto_0
    :try_start_4
    invoke-virtual {p0, v3, v4}, Lcom/android/server/wm/TalkbackWatermark;->positionSurface(II)V

    .line 327
    :cond_2
    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->drawIfNeeded()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 328
    if-eqz v0, :cond_3

    .line 329
    :try_start_5
    iget-object v5, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v5}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 332
    .end local v0    # "transaction":Landroid/view/SurfaceControl$Transaction;
    .end local v1    # "defaultDc":Lcom/android/server/wm/DisplayContent;
    .end local v2    # "defaultInfo":Landroid/view/DisplayInfo;
    .end local v3    # "defaultDw":I
    .end local v4    # "defaultDh":I
    :cond_3
    :try_start_6
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    const-string/jumbo v1, "updateTalkbackWatermark"

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 333
    nop

    .line 334
    monitor-exit p0

    return-void

    .line 332
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :catchall_1
    move-exception v0

    :goto_1
    :try_start_7
    iget-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    const-string/jumbo v2, "updateTalkbackWatermark"

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 333
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 308
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateWaterMark()V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 151
    return-void
.end method


# virtual methods
.method public declared-synchronized dismiss()V
    .locals 2

    monitor-enter p0

    .line 282
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 289
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 290
    monitor-exit p0

    return-void

    .line 292
    :cond_0
    :try_start_1
    const-string v0, "TalkbackWatermark"

    const-string/jumbo v1, "talkback-test dismiss"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294
    monitor-exit p0

    return-void

    .line 281
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public init(Lcom/android/server/wm/WindowManagerService;)V
    .locals 5
    .param p1, "wms"    # Lcom/android/server/wm/WindowManagerService;

    .line 101
    nop

    .line 102
    const-string/jumbo v0, "talkback_watermark_enable"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 103
    .local v0, "talkbackWatermarkEnableUri":Landroid/net/Uri;
    iget-object v1, p1, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/TalkbackWatermark$1;

    new-instance v3, Landroid/os/Handler;

    .line 105
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v2, p0, v3, v0}, Lcom/android/server/wm/TalkbackWatermark$1;-><init>(Lcom/android/server/wm/TalkbackWatermark;Landroid/os/Handler;Landroid/net/Uri;)V

    .line 103
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v1, v0, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 113
    iput-object p1, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    .line 114
    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->setupBroadcast()V

    .line 115
    return-void
.end method

.method public declared-synchronized positionSurface(II)V
    .locals 2
    .param p1, "dw"    # I
    .param p2, "dh"    # I

    monitor-enter p0

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 190
    monitor-exit p0

    return-void

    .line 192
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I

    if-ne v0, p1, :cond_1

    iget v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I

    if-eq v0, p2, :cond_3

    .line 193
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :cond_1
    iput p1, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I

    .line 194
    iput p2, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I

    .line 195
    invoke-static {}, Landroid/view/SurfaceControl;->getGlobalTransaction()Landroid/view/SurfaceControl$Transaction;

    move-result-object v0

    .line 196
    .local v0, "transaction":Landroid/view/SurfaceControl$Transaction;
    if-eqz v0, :cond_2

    .line 197
    iget-object v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/SurfaceControl$Transaction;->setBufferSize(Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;

    .line 199
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    .end local v0    # "transaction":Landroid/view/SurfaceControl$Transaction;
    :cond_3
    monitor-exit p0

    return-void

    .line 188
    .end local p1    # "dw":I
    .end local p2    # "dh":I
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    monitor-enter p0

    .line 273
    if-eqz p1, :cond_0

    .line 274
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->showInternal()V

    goto :goto_0

    .line 276
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->hideInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    :goto_0
    monitor-exit p0

    return-void

    .line 272
    .end local p1    # "visible":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized show()V
    .locals 2

    monitor-enter p0

    .line 265
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->updateWaterMark()V

    .line 266
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    monitor-exit p0

    return-void

    .line 264
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized updateTalkbackMode(ZLandroid/content/ComponentName;)V
    .locals 2
    .param p1, "enable"    # Z
    .param p2, "mComponentName"    # Landroid/content/ComponentName;

    monitor-enter p0

    .line 298
    :try_start_0
    invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TalkBackService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.marvin.talkback"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    if-eqz p1, :cond_0

    .line 301
    invoke-virtual {p0}, Lcom/android/server/wm/TalkbackWatermark;->show()V

    goto :goto_0

    .line 303
    .end local p0    # "this":Lcom/android/server/wm/TalkbackWatermark;
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/TalkbackWatermark;->dismiss()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 297
    .end local p1    # "enable":Z
    .end local p2    # "mComponentName":Landroid/content/ComponentName;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
