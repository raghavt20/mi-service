.class public Lcom/android/server/wm/MiuiSizeCompatService;
.super Landroid/sizecompat/IMiuiSizeCompat$Stub;
.source "MiuiSizeCompatService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiSizeCompatService$H;,
        Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;,
        Lcom/android/server/wm/MiuiSizeCompatService$LocalService;,
        Lcom/android/server/wm/MiuiSizeCompatService$Shell;,
        Lcom/android/server/wm/MiuiSizeCompatService$Lifecycle;
    }
.end annotation


# static fields
.field public static final APP_CONTINUITY_FIX_RATIO_KEY:Ljava/lang/String; = "app_continuity_o_fixratiolist"

.field public static DEBUG:Z = false

.field public static final FAST_XML:Ljava/lang/String; = "http://xmlpull.org/v1/doc/features.html#indent-output"

.field public static FLIP_DEFAULT_SCALE:F = 0.0f

.field public static FLIP_UNSCALE:F = 0.0f

.field private static final FORCE_RESIZABLE_ACTIVITIES:Ljava/lang/String; = "force_resizable_activities"

.field private static final NOTIFICATION_ID:I = 0x7d0

.field private static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field public static final SERVICE_NAME:Ljava/lang/String; = "MiuiSizeCompat"

.field private static final SETTING_CONFIG_BACKUP_FILE_NAME:Ljava/lang/String; = "size_compat_setting_config-backup.xml"

.field private static final SETTING_CONFIG_FILE_NAME:Ljava/lang/String; = "size_compat_setting_config.xml"

.field public static final SYSTEM_USERS:Ljava/lang/String; = "system/users/"

.field private static final TAG:Ljava/lang/String; = "MiuiSizeCompatService"

.field private static final XML_ATTRIBUTE_ASPECT_RATIO:Ljava/lang/String; = "aspectRatio"

.field private static final XML_ATTRIBUTE_DISPLAY_NAME:Ljava/lang/String; = "displayName"

.field private static final XML_ATTRIBUTE_GRAVITY:Ljava/lang/String; = "gravity"

.field private static final XML_ATTRIBUTE_NAME:Ljava/lang/String; = "name"

.field private static final XML_ATTRIBUTE_SCALE_MODE:Ljava/lang/String; = "scaleMode"

.field private static final XML_ATTRIBUTE_SETTING_CONDIG:Ljava/lang/String; = "setting_config"

.field private static final XML_ELEMENT_SETTING:Ljava/lang/String; = "setting"

.field private static final mCallerWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final mRestartTaskActs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final mRestartTaskApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final APPLIACTION_APP_BLACK_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final GAME_JSON_KEY_GRAVITY:Ljava/lang/String;

.field private final GAME_JSON_KEY_NAME:Ljava/lang/String;

.field private final GAME_JSON_KEY_RATIO:Ljava/lang/String;

.field private final NOTIFICATION_ICON_KEY:Ljava/lang/String;

.field private final NOTIFICATION_PACKAGE_ICON:Ljava/lang/String;

.field private final NOTIFICATION_SERVICE_CLASS_NAME:Ljava/lang/String;

.field private final NOTIFICATION_SERVICE_PACKAGE:Ljava/lang/String;

.field private PERMISSION_ACTIVITY:Ljava/lang/String;

.field private final SIZE_COMPAT_CHANNEL_ID:Ljava/lang/String;

.field private final SIZE_COMPAT_CHANNEL_NAME:Ljava/lang/String;

.field private mAms:Lcom/android/server/am/ActivityManagerService;

.field private mAppObserver:Lmiui/process/IForegroundInfoListener$Stub;

.field private mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mBackupSettingFilename:Ljava/io/File;

.field private final mBgHandler:Lcom/android/server/wm/MiuiSizeCompatService$H;

.field private final mCompatModeModuleName:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mCurFullAct:Lcom/android/server/wm/ActivityRecord;

.field private final mFgHandler:Landroid/os/Handler;

.field private mFoldObserver:Landroid/view/IDisplayFoldListener$Stub;

.field private final mGameSettingConfig:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mNotificationShowing:Z

.field private mPMS:Lcom/miui/server/process/ProcessManagerInternal;

.field private mRestartAppDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mSecurityManager:Lmiui/security/SecurityManager;

.field private mServiceReady:Z

.field private final mSettingConfigs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSettingFilename:Ljava/io/File;

.field private final mStaticSettingConfigs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mStatusBarService:Lcom/android/server/statusbar/StatusBarManagerInternal;

.field private mUiContext:Landroid/content/Context;

.field private final mUiHandler:Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;


# direct methods
.method public static synthetic $r8$lambda$EZGSahURCWm0emzACX_aU7BA7Mc(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiSizeCompatService;->lambda$showConfirmDialog$0(Lcom/android/server/wm/ActivityRecord;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$G8QKMToB-TYoTyEXvBYYnuLQKNw(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;F)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiSizeCompatService;->restartProcessForRatioLocked(Lcom/android/server/wm/ActivityRecord;F)V

    return-void
.end method

.method public static synthetic $r8$lambda$poACLljgOu3uDdzrgGWnQ6sTMeE(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiSizeCompatService;->lambda$showConfirmDialog$3(Lcom/android/server/wm/ActivityRecord;Landroid/content/DialogInterface;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetPERMISSION_ACTIVITY(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->PERMISSION_ACTIVITY:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAtms(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityTaskManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBgHandler(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/MiuiSizeCompatService$H;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBgHandler:Lcom/android/server/wm/MiuiSizeCompatService$H;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/MiuiSizeCompatService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mCurFullAct:Lcom/android/server/wm/ActivityRecord;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFgHandler(Lcom/android/server/wm/MiuiSizeCompatService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mFgHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mGameSettingConfig:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNotificationShowing(Lcom/android/server/wm/MiuiSizeCompatService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationShowing:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSecurityManager(Lcom/android/server/wm/MiuiSizeCompatService;)Lmiui/security/SecurityManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSecurityManager:Lmiui/security/SecurityManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmServiceReady(Lcom/android/server/wm/MiuiSizeCompatService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStaticSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetAspectRatioByPackage(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/lang/String;)F
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->getAspectRatioByPackage(Ljava/lang/String;)F

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$monSystemReady(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->onSystemReady(Lcom/android/server/wm/ActivityTaskManagerService;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterDataObserver(Lcom/android/server/wm/MiuiSizeCompatService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->registerDataObserver()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetMiuiSizeCompatScaleMode(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/lang/String;IZ)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiSizeCompatService;->setMiuiSizeCompatScaleMode(Ljava/lang/String;IZ)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mshowConfirmDialog(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->showConfirmDialog(Lcom/android/server/wm/ActivityRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowWarningNotification(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->showWarningNotification(Lcom/android/server/wm/ActivityRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSettingConfigsFromCloud(Lcom/android/server/wm/MiuiSizeCompatService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->updateSettingConfigsFromCloud()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 128
    const v0, 0x3f4ccccd    # 0.8f

    sput v0, Lcom/android/server/wm/MiuiSizeCompatService;->FLIP_DEFAULT_SCALE:F

    .line 130
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/android/server/wm/MiuiSizeCompatService;->FLIP_UNSCALE:F

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartTaskApps:Ljava/util/List;

    .line 150
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartTaskActs:Ljava/util/List;

    .line 152
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/android/server/wm/MiuiSizeCompatService;->mCallerWhiteList:Ljava/util/List;

    .line 153
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z

    .line 192
    const-string v3, "com.cntaiping.tpapp"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    const-string v3, "com.oda_cad"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    const-string v3, "com.jime.encyclopediascanning"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    const-string v3, "com.tmri.app.main"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    const-string v3, "com.mahjong.nb"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    const-string v3, "com.yusi.chongchong"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    const-string v0, "com.lianjia.common.vr.webview.VrWebviewActivity"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    const-string v0, "com.alipay.mobile.quinox.LauncherActivity"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    const-string v0, "com.android.settings"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    const-string v0, "com.android.systemui"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    const-string v0, "android"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    const-string v0, "com.miui.securitycenter"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    const-string v0, "miphone.android.compat"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 223
    invoke-direct {p0}, Landroid/sizecompat/IMiuiSizeCompat$Stub;-><init>()V

    .line 138
    const-string v0, "MiuiSizeCompat"

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->SIZE_COMPAT_CHANNEL_NAME:Ljava/lang/String;

    .line 139
    const-string v0, "WaringNotification"

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->SIZE_COMPAT_CHANNEL_ID:Ljava/lang/String;

    .line 140
    const-string v0, "com.android.settings"

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->NOTIFICATION_PACKAGE_ICON:Ljava/lang/String;

    .line 141
    const-string v0, "miui.appIcon"

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->NOTIFICATION_ICON_KEY:Ljava/lang/String;

    .line 142
    const-string v0, "android.sizecompat.SizeCompatChangeService"

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->NOTIFICATION_SERVICE_CLASS_NAME:Ljava/lang/String;

    .line 143
    const-string v0, "android"

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->NOTIFICATION_SERVICE_PACKAGE:Ljava/lang/String;

    .line 146
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    .line 147
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    .line 165
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mGameSettingConfig:Ljava/util/Map;

    .line 166
    const-string v0, "pkgName"

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->GAME_JSON_KEY_NAME:Ljava/lang/String;

    .line 167
    const-string v0, "aspectRatio"

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->GAME_JSON_KEY_RATIO:Ljava/lang/String;

    .line 168
    const-string v0, "gravity"

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->GAME_JSON_KEY_GRAVITY:Ljava/lang/String;

    .line 171
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->APPLIACTION_APP_BLACK_LIST:Ljava/util/List;

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    .line 188
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mLock:Ljava/lang/Object;

    .line 189
    const-string v1, "com.lbe.security.miui/com.android.packageinstaller.permission.ui.GrantPermissionsActivity"

    iput-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->PERMISSION_ACTIVITY:Ljava/lang/String;

    .line 517
    new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$2;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiSizeCompatService$2;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAppObserver:Lmiui/process/IForegroundInfoListener$Stub;

    .line 535
    new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$3;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiSizeCompatService$3;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mFoldObserver:Landroid/view/IDisplayFoldListener$Stub;

    .line 224
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    .line 225
    new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$H;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBgHandler:Lcom/android/server/wm/MiuiSizeCompatService$H;

    .line 226
    new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;

    invoke-static {}, Lcom/android/server/UiThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mUiHandler:Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;

    .line 227
    invoke-static {}, Lcom/android/server/MiuiFgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mFgHandler:Landroid/os/Handler;

    .line 228
    const-class v1, Lcom/android/server/wm/MiuiSizeCompatInternal;

    new-instance v2, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;

    invoke-direct {v2, p0, v0}, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/MiuiSizeCompatService$LocalService-IA;)V

    invoke-static {v1, v2}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 229
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f0359

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mCompatModeModuleName:Ljava/lang/String;

    .line 230
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->initBlackAppList()V

    .line 231
    return-void
.end method

.method private checkInterfaceAccess(II)Z
    .locals 6
    .param p1, "callingPid"    # I
    .param p2, "callingUid"    # I

    .line 902
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    const/4 v1, 0x1

    if-eq p1, v0, :cond_4

    if-eqz p2, :cond_4

    const/16 v0, 0x7d0

    if-ne p2, v0, :cond_0

    goto :goto_1

    .line 905
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 906
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mProcessMap:Lcom/android/server/wm/WindowProcessControllerMap;

    invoke-virtual {v2, p1}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v2

    .line 907
    .local v2, "wpc":Lcom/android/server/wm/WindowProcessController;
    const/4 v3, 0x0

    if-eqz v2, :cond_3

    iget-object v4, v2, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v4, :cond_3

    iget-object v4, v2, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    .line 910
    :cond_1
    sget-object v4, Lcom/android/server/wm/MiuiSizeCompatService;->mCallerWhiteList:Ljava/util/List;

    iget-object v5, v2, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 911
    monitor-exit v0

    return v1

    .line 913
    .end local v2    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :cond_2
    monitor-exit v0

    .line 914
    return v3

    .line 908
    .restart local v2    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :cond_3
    :goto_0
    monitor-exit v0

    return v3

    .line 913
    .end local v2    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 903
    :cond_4
    :goto_1
    return v1
.end method

.method private checkSlow(JJLjava/lang/String;)V
    .locals 4
    .param p1, "startTime"    # J
    .param p3, "threshold"    # J
    .param p5, "where"    # Ljava/lang/String;

    .line 1343
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 1344
    .local v0, "took":J
    cmp-long v2, v0, p3

    if-lez v2, :cond_0

    .line 1346
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Slow operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " took "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiSizeCompatService"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1348
    :cond_0
    return-void
.end method

.method private getActivityFromTokenLocked(Landroid/window/WindowContainerToken;)Lcom/android/server/wm/ActivityRecord;
    .locals 6
    .param p1, "token"    # Landroid/window/WindowContainerToken;

    .line 1065
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 1066
    :try_start_0
    invoke-virtual {p1}, Landroid/window/WindowContainerToken;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/wm/WindowContainer;->fromBinder(Landroid/os/IBinder;)Lcom/android/server/wm/WindowContainer;

    move-result-object v1

    .line 1067
    .local v1, "wc":Lcom/android/server/wm/WindowContainer;
    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 1068
    const-string v3, "MiuiSizeCompatService"

    const-string v4, "Could not resolve window from token"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    monitor-exit v0

    return-object v2

    .line 1071
    :cond_0
    invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v3

    .line 1072
    .local v3, "task":Lcom/android/server/wm/Task;
    if-nez v3, :cond_1

    .line 1073
    const-string v4, "MiuiSizeCompatService"

    const-string v5, "Could not resolve task from token"

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1074
    monitor-exit v0

    return-object v2

    .line 1076
    :cond_1
    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    monitor-exit v0

    return-object v2

    .line 1077
    .end local v1    # "wc":Lcom/android/server/wm/WindowContainer;
    .end local v3    # "task":Lcom/android/server/wm/Task;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getAspectRatioByPackage(Ljava/lang/String;)F
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 750
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 751
    const v0, 0x3fdc3e06

    return v0

    .line 753
    :cond_0
    const/4 v0, 0x0

    .line 755
    .local v0, "info":Landroid/sizecompat/AspectRatioInfo;
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mGameSettingConfig:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 756
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mGameSettingConfig:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    goto :goto_0

    .line 757
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 758
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    goto :goto_0

    .line 760
    :cond_2
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    .line 762
    :goto_0
    if-eqz v0, :cond_3

    iget v1, v0, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    goto :goto_1

    :cond_3
    const/high16 v1, -0x40800000    # -1.0f

    :goto_1
    return v1
.end method

.method private getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1351
    const/4 v0, 0x0

    .line 1353
    .local v0, "bm":Landroid/graphics/Bitmap;
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1354
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1355
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 1358
    .local v2, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v2, :cond_0

    .line 1359
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1360
    .local v3, "d":Landroid/graphics/drawable/Drawable;
    move-object v4, v3

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    .line 1361
    .local v4, "bd":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v5

    .line 1365
    .end local v1    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v2    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "d":Landroid/graphics/drawable/Drawable;
    .end local v4    # "bd":Landroid/graphics/drawable/BitmapDrawable;
    :cond_0
    goto :goto_0

    .line 1363
    :catch_0
    move-exception v1

    .line 1364
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found, Ignoring."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiSizeCompatService"

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    return-object v0
.end method

.method private getRestartAct(Ljava/lang/String;)Lcom/android/server/wm/ActivityRecord;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .line 619
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getTopResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 620
    .local v0, "top":Lcom/android/server/wm/ActivityRecord;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 621
    :cond_0
    iget-object v2, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    const-string v3, "Real restart for "

    const-string v4, "MiuiSizeCompatService"

    if-eqz v2, :cond_1

    .line 622
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    return-object v0

    .line 625
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService;->PERMISSION_ACTIVITY:Ljava/lang/String;

    iget-object v5, v0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 626
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/server/wm/Task;->getActivityBelow(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 627
    .local v2, "below":Lcom/android/server/wm/ActivityRecord;
    if-eqz v2, :cond_2

    iget-object v5, v2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 628
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v2, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    return-object v2

    .line 632
    .end local v2    # "below":Lcom/android/server/wm/ActivityRecord;
    :cond_2
    return-object v1
.end method

.method private initStaticSettings()V
    .locals 13

    .line 478
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 479
    .local v1, "start":J
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x1103006f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 480
    .local v6, "ratioPackages":[Ljava/lang/String;
    if-eqz v6, :cond_3

    array-length v0, v6

    if-nez v0, :cond_0

    goto :goto_2

    .line 483
    :cond_0
    array-length v0, v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v0, :cond_2

    aget-object v5, v6, v4

    .line 484
    .local v5, "ratioPackage":Ljava/lang/String;
    const-string v7, ","

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 485
    .local v7, "split":[Ljava/lang/String;
    array-length v8, v7

    const/4 v9, 0x2

    if-ne v8, v9, :cond_1

    const/4 v8, 0x1

    aget-object v8, v7, v8

    goto :goto_1

    :cond_1
    const-string v8, ""

    :goto_1
    invoke-virtual {p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService;->parseRatioFromStr(Ljava/lang/String;)F

    move-result v8

    .line 486
    .local v8, "ratio":F
    iget-object v9, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    aget-object v10, v7, v3

    new-instance v11, Landroid/sizecompat/AspectRatioInfo;

    aget-object v12, v7, v3

    invoke-direct {v11, v12, v8}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    nop

    .end local v5    # "ratioPackage":Ljava/lang/String;
    .end local v7    # "split":[Ljava/lang/String;
    .end local v8    # "ratio":F
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 488
    :cond_2
    const-string v0, "MiuiSizeCompatService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Static setting configs: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    const-wide/16 v3, 0x64

    const-string v5, "initStaticSettings"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 492
    .end local v1    # "start":J
    .end local v6    # "ratioPackages":[Ljava/lang/String;
    goto :goto_3

    .line 481
    .restart local v1    # "start":J
    .restart local v6    # "ratioPackages":[Ljava/lang/String;
    :cond_3
    :goto_2
    return-void

    .line 490
    .end local v1    # "start":J
    .end local v6    # "ratioPackages":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 491
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 493
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    return-void
.end method

.method private isInRestartTaskActs(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 661
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    goto :goto_0

    .line 664
    :cond_0
    sget-object v0, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartTaskActs:Ljava/util/List;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 662
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private synthetic lambda$showConfirmDialog$0(Lcom/android/server/wm/ActivityRecord;Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "activity"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "dialog"    # Landroid/content/DialogInterface;
    .param p3, "which"    # I

    .line 1098
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartAppDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1099
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBgHandler:Lcom/android/server/wm/MiuiSizeCompatService$H;

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I

    move-result v1

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$H;->sendMessage(Landroid/os/Message;)Z

    .line 1101
    :cond_0
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mCurFullAct:Lcom/android/server/wm/ActivityRecord;

    .line 1102
    return-void
.end method

.method static synthetic lambda$showConfirmDialog$1(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "which"    # I

    .line 1108
    return-void
.end method

.method static synthetic lambda$showConfirmDialog$2(Landroid/content/DialogInterface;)V
    .locals 0
    .param p0, "dialog"    # Landroid/content/DialogInterface;

    .line 1113
    return-void
.end method

.method private synthetic lambda$showConfirmDialog$3(Lcom/android/server/wm/ActivityRecord;Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "activity"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "dialog"    # Landroid/content/DialogInterface;

    .line 1115
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mCurFullAct:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1116
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda0;-><init>()V

    .line 1117
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 1116
    invoke-static {v0, p0, p1, v1}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainMessage(Lcom/android/internal/util/function/TriConsumer;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1118
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mFgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1120
    .end local v0    # "message":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private makeSizeCompatAction(I)Landroid/app/Notification$Action;
    .locals 12
    .param p1, "type"    # I

    .line 1328
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 1329
    .local v6, "start":J
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    move-object v8, v0

    .line 1330
    .local v8, "intent":Landroid/content/Intent;
    const-string v0, "android"

    const-string v1, "android.sizecompat.SizeCompatChangeService"

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1331
    const-string v0, "extra_type"

    invoke-virtual {v8, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1332
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    const/high16 v1, 0x4000000

    invoke-static {v0, p1, v8, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 1333
    .local v9, "pendingIntent":Landroid/app/PendingIntent;
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const v0, 0x110f032b

    goto :goto_0

    .line 1334
    :cond_0
    const v0, 0x110f032c

    :goto_0
    move v10, v0

    .line 1335
    .local v10, "titleResId":I
    new-instance v0, Landroid/app/Notification$Action$Builder;

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    .line 1336
    const v2, 0x108055a

    invoke-static {v1, v2}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    .line 1337
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v9}, Landroid/app/Notification$Action$Builder;-><init>(Landroid/graphics/drawable/Icon;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v0}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v11

    .line 1338
    .local v11, "action":Landroid/app/Notification$Action;
    const-wide/16 v3, 0x32

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "makeSizeCompatAction type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-wide v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 1339
    return-object v11
.end method

.method private onSystemReady(Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 2
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 234
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 235
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mActivityManager:Landroid/app/IActivityManager;

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAms:Lcom/android/server/am/ActivityManagerService;

    .line 236
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "security"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManager;

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSecurityManager:Lmiui/security/SecurityManager;

    .line 237
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 238
    const-class v0, Lcom/android/server/statusbar/StatusBarManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/statusbar/StatusBarManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStatusBarService:Lcom/android/server/statusbar/StatusBarManagerInternal;

    .line 239
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mUiContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    .line 240
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    .line 241
    invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->initStaticSettings()V

    .line 242
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->registerUserSwitchReceiver()V

    .line 243
    return-void
.end method

.method private parseAspectRatio(Ljava/lang/Object;)F
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .line 1032
    instance-of v0, p1, Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 1033
    move-object v0, p1

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0

    .line 1034
    :cond_0
    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 1035
    move-object v0, p1

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v0

    return v0

    .line 1036
    :cond_1
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1037
    move-object v0, p1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->floatValue()F

    move-result v0

    return v0

    .line 1038
    :cond_2
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1039
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0

    .line 1041
    :cond_3
    const-string v0, "MiuiSizeCompatService"

    const-string v1, "The game aspect ratio is error format"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    const/4 v0, 0x0

    return v0
.end method

.method private registerDataObserver()V
    .locals 4

    .line 432
    invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->updateSettingConfigsFromCloud()V

    .line 433
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 434
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/MiuiSizeCompatService$1;

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBgHandler:Lcom/android/server/wm/MiuiSizeCompatService$H;

    invoke-direct {v2, p0, v3}, Lcom/android/server/wm/MiuiSizeCompatService$1;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Landroid/os/Handler;)V

    .line 433
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 442
    return-void
.end method

.method private restartProcessForRatioLocked(Lcom/android/server/wm/ActivityRecord;F)V
    .locals 11
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "aspectRatio"    # F

    .line 588
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 589
    .local v6, "start":J
    if-eqz p1, :cond_7

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 592
    :cond_0
    iget-object v8, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 593
    .local v8, "packageName":Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/sizecompat/AspectRatioInfo;

    .line 594
    .local v9, "info":Landroid/sizecompat/AspectRatioInfo;
    if-nez v9, :cond_1

    .line 595
    const/4 p2, 0x0

    move v10, p2

    goto :goto_0

    .line 594
    :cond_1
    move v10, p2

    .line 597
    .end local p2    # "aspectRatio":F
    .local v10, "aspectRatio":F
    :goto_0
    const-string p2, "MiuiSizeCompatService"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Original ar is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, v8, v10, p2, v0}, Lcom/android/server/wm/MiuiSizeCompatService;->setMiuiSizeCompatRatio(Ljava/lang/String;FZZ)Z

    .line 599
    invoke-static {}, Lcom/android/server/app/GameManagerServiceStub;->getInstance()Lcom/android/server/app/GameManagerServiceStub;

    move-result-object p2

    invoke-virtual {p2, v8}, Lcom/android/server/app/GameManagerServiceStub;->addSizeCompatApps(Ljava/lang/String;)V

    .line 601
    iget-object p2, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, p2, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 602
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isTopRunningActivity()Z

    move-result p2

    if-eqz p2, :cond_2

    move-object p2, p1

    goto :goto_1

    :cond_2
    invoke-direct {p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService;->getRestartAct(Ljava/lang/String;)Lcom/android/server/wm/ActivityRecord;

    move-result-object p2

    .line 603
    .local p2, "restart":Lcom/android/server/wm/ActivityRecord;
    :goto_1
    if-eqz p2, :cond_5

    .line 604
    sget-object v1, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartTaskApps:Ljava/util/List;

    invoke-interface {v1, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->isInRestartTaskActs(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_2

    .line 608
    :cond_3
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->restartProcessIfVisible()V

    goto :goto_3

    .line 605
    :cond_4
    :goto_2
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->restartTask(I)V

    .line 606
    const-string v1, "MiuiSizeCompatService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is in restart task list ,so restart task instead."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    :cond_5
    :goto_3
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 612
    if-nez p2, :cond_6

    .line 613
    invoke-virtual {p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService;->removeRunningApp(Ljava/lang/String;)V

    .line 615
    :cond_6
    const-wide/16 v3, 0xc8

    const-string v5, "restartProcessForRatioLocked"

    move-object v0, p0

    move-wide v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 616
    return-void

    .line 611
    .end local p2    # "restart":Lcom/android/server/wm/ActivityRecord;
    :catchall_0
    move-exception p2

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p2

    .line 590
    .end local v8    # "packageName":Ljava/lang/String;
    .end local v9    # "info":Landroid/sizecompat/AspectRatioInfo;
    .end local v10    # "aspectRatio":F
    .local p2, "aspectRatio":F
    :cond_7
    :goto_4
    return-void
.end method

.method private restartTask(I)V
    .locals 30
    .param p1, "taskId"    # I

    .line 636
    move-object/from16 v0, p0

    invoke-static {}, Lcom/android/server/wm/WindowManagerService;->boostPriorityForLockedSection()V

    .line 637
    iget-object v1, v0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    move/from16 v2, p1

    invoke-virtual {v1, v2}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(I)Lcom/android/server/wm/Task;

    move-result-object v1

    .line 638
    .local v1, "task":Lcom/android/server/wm/Task;
    if-nez v1, :cond_0

    .line 639
    const-string v3, "MiuiSizeCompatService"

    const-string v4, "Restart task fail, task is null."

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    return-void

    .line 642
    :cond_0
    iget-object v3, v1, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .local v3, "intent":Landroid/content/Intent;
    move-object v11, v3

    .line 643
    iget v4, v1, Lcom/android/server/wm/Task;->mCallingUid:I

    .local v4, "callingUid":I
    move v6, v4

    .line 644
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v24

    .local v24, "realCallingUid":I
    move/from16 v8, v24

    .line 645
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v25

    .local v25, "realCallingPid":I
    move/from16 v7, v25

    .line 646
    iget-object v15, v1, Lcom/android/server/wm/Task;->mCallingPackage:Ljava/lang/String;

    .local v15, "callingPackage":Ljava/lang/String;
    move-object v9, v15

    .line 647
    iget-object v14, v1, Lcom/android/server/wm/Task;->mCallingFeatureId:Ljava/lang/String;

    .local v14, "callingFeatureId":Ljava/lang/String;
    move-object v10, v14

    .line 648
    iget v13, v1, Lcom/android/server/wm/Task;->mUserId:I

    .local v13, "userId":I
    move/from16 v18, v13

    .line 649
    const-string/jumbo v5, "sizecompat"

    invoke-virtual {v1, v5}, Lcom/android/server/wm/Task;->removeImmediately(Ljava/lang/String;)V

    .line 650
    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object v12

    .line 651
    .local v12, "option":Landroid/app/ActivityOptions;
    const/4 v5, 0x1

    invoke-virtual {v12, v5}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V

    .line 652
    new-instance v5, Lcom/android/server/wm/SafeActivityOptions;

    invoke-direct {v5, v12}, Lcom/android/server/wm/SafeActivityOptions;-><init>(Landroid/app/ActivityOptions;)V

    move-object/from16 v17, v5

    .line 653
    .local v17, "options":Lcom/android/server/wm/SafeActivityOptions;
    iget-object v5, v0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getActivityStartController()Lcom/android/server/wm/ActivityStartController;

    move-result-object v5

    const/16 v16, 0x0

    move-object/from16 v26, v12

    .end local v12    # "option":Landroid/app/ActivityOptions;
    .local v26, "option":Landroid/app/ActivityOptions;
    move-object/from16 v12, v16

    move/from16 v27, v13

    .end local v13    # "userId":I
    .local v27, "userId":I
    move-object/from16 v13, v16

    move-object/from16 v28, v14

    .end local v14    # "callingFeatureId":Ljava/lang/String;
    .local v28, "callingFeatureId":Ljava/lang/String;
    move-object/from16 v14, v16

    const/16 v16, 0x0

    move-object/from16 v29, v15

    .end local v15    # "callingPackage":Ljava/lang/String;
    .local v29, "callingPackage":Ljava/lang/String;
    move/from16 v15, v16

    const/16 v19, 0x0

    const-string/jumbo v20, "sizecompat"

    const/16 v21, 0x0

    const/16 v22, 0x0

    sget-object v23, Landroid/app/BackgroundStartPrivileges;->ALLOW_BAL:Landroid/app/BackgroundStartPrivileges;

    invoke-virtual/range {v5 .. v23}, Lcom/android/server/wm/ActivityStartController;->startActivityInPackage(IIILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILcom/android/server/wm/SafeActivityOptions;ILcom/android/server/wm/Task;Ljava/lang/String;ZLcom/android/server/am/PendingIntentRecord;Landroid/app/BackgroundStartPrivileges;)I

    .line 657
    invoke-static {}, Lcom/android/server/wm/WindowManagerService;->resetPriorityAfterLockedSection()V

    .line 658
    return-void
.end method

.method private setMiuiSizeCompatScaleMode(Ljava/lang/String;IZ)Z
    .locals 11
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "scaleMode"    # I
    .param p3, "write"    # Z

    .line 831
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z

    const/4 v1, 0x0

    const-string v2, "MiuiSizeCompatService"

    if-nez v0, :cond_0

    .line 832
    const-string v0, "Set miui size compat ratio fail : service is not ready."

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    return v1

    .line 835
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    .line 836
    .local v9, "start":J
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 837
    const-string v0, "Set miui size compat scale mode fail : pkgName is null."

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    return v1

    .line 840
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    .line 841
    .local v0, "info":Landroid/sizecompat/AspectRatioInfo;
    sget-boolean v3, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z

    if-eqz v3, :cond_2

    .line 842
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setMiuiSizeCompatScaleMode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " scaleMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from pid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 843
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 842
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    :cond_2
    if-eqz v0, :cond_4

    .line 846
    invoke-virtual {v0, p2}, Landroid/sizecompat/AspectRatioInfo;->setScaleMode(I)V

    .line 851
    if-eqz p3, :cond_3

    .line 852
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBgHandler:Lcom/android/server/wm/MiuiSizeCompatService$H;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiSizeCompatService$H;->sendMessage(Landroid/os/Message;)Z

    .line 854
    :cond_3
    const-wide/16 v6, 0x64

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMiuiSizeCompatScaleMode, write: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v3, p0

    move-wide v4, v9

    invoke-direct/range {v3 .. v8}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 856
    const/4 v1, 0x1

    return v1

    .line 848
    :cond_4
    const-string/jumbo v3, "should set ratio first, skip"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    return v1
.end method

.method private showConfirmDialog(Lcom/android/server/wm/ActivityRecord;)V
    .locals 21
    .param p1, "activity"    # Lcom/android/server/wm/ActivityRecord;

    .line 1081
    move-object/from16 v6, p0

    move-object/from16 v7, p1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 1082
    .local v8, "start":J
    const-string v10, "MiuiSizeCompatService"

    if-eqz v7, :cond_4

    iget-object v0, v7, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 1086
    :cond_0
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mCurFullAct:Lcom/android/server/wm/ActivityRecord;

    .line 1087
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartAppDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1088
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartAppDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V

    .line 1090
    :cond_1
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1091
    const-string v1, "AlertDialog.Theme.DayNight"

    const-string/jumbo v2, "style"

    const-string v3, "miuix.stub"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1092
    .local v0, "themeId":I
    if-nez v0, :cond_2

    .line 1093
    const v0, 0x10302d1

    move v11, v0

    goto :goto_0

    .line 1092
    :cond_2
    move v11, v0

    .line 1095
    .end local v0    # "themeId":I
    .local v11, "themeId":I
    :goto_0
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, v7, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 1096
    invoke-virtual {v6, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->getAppNameByPkg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const v2, 0x110f030f

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 1097
    .local v12, "title":Ljava/lang/String;
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda1;

    invoke-direct {v0, v6, v7}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;)V

    move-object v13, v0

    .line 1103
    .local v13, "positiveListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda2;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda2;-><init>()V

    move-object v14, v0

    .line 1109
    .local v14, "negativeListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda3;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda3;-><init>()V

    move-object v15, v0

    .line 1114
    .local v15, "cancelListener":Landroid/content/DialogInterface$OnCancelListener;
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda4;

    invoke-direct {v0, v6, v7}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;)V

    move-object v5, v0

    .line 1121
    .local v5, "dismissListener":Landroid/content/DialogInterface$OnDismissListener;
    new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder;

    iget-object v1, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    invoke-direct {v0, v1, v11}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1122
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 1123
    invoke-virtual {v0, v12}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    .line 1124
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1125
    const v3, 0x110f032e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1124
    invoke-virtual {v0, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    .line 1126
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1127
    const v3, 0x110f030b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1126
    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCheckBox(ZLjava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    .line 1128
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1129
    const v3, 0x110f030d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1128
    invoke-virtual {v0, v2, v13}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    .line 1130
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1131
    const v3, 0x110f030c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1130
    invoke-virtual {v0, v2, v14}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 1132
    invoke-virtual {v0, v15}, Lmiuix/appcompat/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 1133
    invoke-virtual {v0, v5}, Lmiuix/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 1134
    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v0

    iput-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartAppDialog:Lmiuix/appcompat/app/AlertDialog;

    .line 1135
    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 1136
    .local v3, "attrs":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v3, v12}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 1137
    const/16 v0, 0x7d3

    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 1138
    iget v0, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, 0x20000

    or-int/2addr v0, v2

    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1139
    const/16 v0, 0x51

    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1140
    iget v0, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v0, v0, 0x110

    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 1142
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartAppDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1144
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1145
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartAppDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->show()V

    .line 1149
    :cond_3
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartAppDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 1150
    .local v4, "attributes":Landroid/view/WindowManager$LayoutParams;
    iput v1, v4, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 1152
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mRestartAppDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1153
    const-wide/16 v16, 0xc8

    const-string/jumbo v18, "showConfirmDialog"

    move-object/from16 v0, p0

    move-wide v1, v8

    move-object/from16 v19, v3

    move-object/from16 v20, v4

    .end local v3    # "attrs":Landroid/view/WindowManager$LayoutParams;
    .end local v4    # "attributes":Landroid/view/WindowManager$LayoutParams;
    .local v19, "attrs":Landroid/view/WindowManager$LayoutParams;
    .local v20, "attributes":Landroid/view/WindowManager$LayoutParams;
    move-wide/from16 v3, v16

    move-object/from16 v16, v5

    .end local v5    # "dismissListener":Landroid/content/DialogInterface$OnDismissListener;
    .local v16, "dismissListener":Landroid/content/DialogInterface$OnDismissListener;
    move-object/from16 v5, v18

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 1154
    const-string v0, "Show confirm dialog in size compat mode."

    invoke-static {v10, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    return-void

    .line 1083
    .end local v11    # "themeId":I
    .end local v12    # "title":Ljava/lang/String;
    .end local v13    # "positiveListener":Landroid/content/DialogInterface$OnClickListener;
    .end local v14    # "negativeListener":Landroid/content/DialogInterface$OnClickListener;
    .end local v15    # "cancelListener":Landroid/content/DialogInterface$OnCancelListener;
    .end local v16    # "dismissListener":Landroid/content/DialogInterface$OnDismissListener;
    .end local v19    # "attrs":Landroid/view/WindowManager$LayoutParams;
    .end local v20    # "attributes":Landroid/view/WindowManager$LayoutParams;
    :cond_4
    :goto_1
    const-string v0, "Show confirm dialog fail, activity is null."

    invoke-static {v10, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    return-void
.end method

.method private showWarningNotification(Lcom/android/server/wm/ActivityRecord;)V
    .locals 10
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 1299
    const-string v0, "WaringNotification"

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 1300
    .local v2, "start":J
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationManager:Landroid/app/NotificationManager;

    new-instance v4, Landroid/app/NotificationChannel;

    const-string v5, "MiuiSizeCompat"

    const/4 v6, 0x4

    invoke-direct {v4, v0, v5, v6}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-virtual {v1, v4}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 1302
    const-string v1, "com.android.settings"

    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v7, v1

    .line 1303
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x110f032a

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v8, v1

    .line 1304
    .local v8, "content":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mUiContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x110f032d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v9, v1

    .line 1305
    .local v9, "title":Ljava/lang/String;
    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1306
    const v0, 0x108055a

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1307
    invoke-virtual {v0, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1308
    invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1309
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->makeSizeCompatAction(I)Landroid/app/Notification$Action;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1310
    const/4 v4, 0x2

    invoke-direct {p0, v4}, Lcom/android/server/wm/MiuiSizeCompatService;->makeSizeCompatAction(I)Landroid/app/Notification$Action;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1311
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 1312
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 1313
    .local v0, "notification":Landroid/app/Notification;
    if-eqz v7, :cond_0

    .line 1314
    iget-object v4, v0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string v5, "miui.appIcon"

    invoke-static {v7}, Landroid/graphics/drawable/Icon;->createWithBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Icon;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1316
    :cond_0
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v5, 0x7d0

    invoke-virtual {v4, v5, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1317
    iput-boolean v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationShowing:Z

    .line 1318
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAppObserver:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v1}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 1319
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mFoldObserver:Landroid/view/IDisplayFoldListener$Stub;

    invoke-virtual {v1, v4}, Lcom/android/server/wm/WindowManagerService;->registerDisplayFoldListener(Landroid/view/IDisplayFoldListener;)V

    .line 1320
    const-string v1, "MiuiSizeCompatService"

    const-string v4, "Show warning notification in size compat mode."

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1321
    const-wide/16 v4, 0xc8

    const-string/jumbo v6, "showWarningNotification"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1324
    .end local v0    # "notification":Landroid/app/Notification;
    .end local v2    # "start":J
    .end local v7    # "bitmap":Landroid/graphics/Bitmap;
    .end local v8    # "content":Ljava/lang/String;
    .end local v9    # "title":Ljava/lang/String;
    goto :goto_0

    .line 1322
    :catch_0
    move-exception v0

    .line 1323
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1325
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private updateSettingConfigsFromCloud()V
    .locals 12

    .line 445
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 447
    .local v6, "start":J
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    .line 448
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mCompatModeModuleName:Ljava/lang/String;

    const-string v2, "app_continuity_o_fixratiolist"

    .line 447
    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 449
    .local v0, "data":Ljava/lang/String;
    const-string v1, "MiuiSizeCompatService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getListFromCloud: data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 451
    return-void

    .line 453
    :cond_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 454
    .local v1, "apps":Lorg/json/JSONArray;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 455
    return-void

    .line 457
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 458
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 459
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 460
    .local v3, "app":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 461
    goto :goto_2

    .line 463
    :cond_2
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 464
    .local v4, "split":[Ljava/lang/String;
    array-length v5, v4

    const/4 v8, 0x2

    if-ne v5, v8, :cond_3

    const/4 v5, 0x1

    aget-object v5, v4, v5

    goto :goto_1

    :cond_3
    const-string v5, ""

    :goto_1
    invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiSizeCompatService;->parseRatioFromStr(Ljava/lang/String;)F

    move-result v5

    .line 465
    .local v5, "ratio":F
    iget-object v8, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    const/4 v9, 0x0

    aget-object v10, v4, v9

    new-instance v11, Landroid/sizecompat/AspectRatioInfo;

    aget-object v9, v4, v9

    invoke-direct {v11, v9, v5}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V

    invoke-interface {v8, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 458
    .end local v3    # "app":Ljava/lang/String;
    .end local v4    # "split":[Ljava/lang/String;
    .end local v5    # "ratio":F
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 469
    .end local v0    # "data":Ljava/lang/String;
    .end local v1    # "apps":Lorg/json/JSONArray;
    .end local v2    # "i":I
    :cond_4
    goto :goto_3

    .line 467
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 470
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    const-wide/16 v3, 0x64

    const-string/jumbo v5, "updateSettingConfigsFromCloud"

    move-object v0, p0

    move-wide v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 471
    return-void
.end method


# virtual methods
.method public cancelSizeCompatNotification(Ljava/lang/String;)V
    .locals 6
    .param p1, "reason"    # Ljava/lang/String;

    .line 966
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 967
    .local v1, "start":J
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStatusBarService:Lcom/android/server/statusbar/StatusBarManagerInternal;

    if-eqz v0, :cond_0

    .line 968
    invoke-interface {v0}, Lcom/android/server/statusbar/StatusBarManagerInternal;->collapsePanels()V

    .line 970
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v3, 0x7d0

    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 971
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAppObserver:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 972
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mFoldObserver:Landroid/view/IDisplayFoldListener$Stub;

    invoke-virtual {v0, v3}, Lcom/android/server/wm/WindowManagerService;->unregisterDisplayFoldListener(Landroid/view/IDisplayFoldListener;)V

    .line 973
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationShowing:Z

    .line 974
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mCurFullAct:Lcom/android/server/wm/ActivityRecord;

    .line 975
    const-wide/16 v3, 0x64

    const-string v5, "cancelSizeCompatNotification"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 976
    const-string v0, "MiuiSizeCompatService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cancel warning notification for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 979
    nop

    .end local v1    # "start":J
    goto :goto_0

    .line 977
    :catch_0
    move-exception v0

    .line 978
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 980
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public getAppNameByPkg(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1158
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1160
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 1161
    .local v1, "info":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v1, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 1162
    .end local v1    # "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v1

    .line 1163
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " may not be installed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiSizeCompatService"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    return-object p1
.end method

.method public getLastAspectRatioValue(Ljava/lang/String;)F
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1173
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    .line 1174
    .local v0, "info":Landroid/sizecompat/AspectRatioInfo;
    if-eqz v0, :cond_0

    iget v1, v0, Landroid/sizecompat/AspectRatioInfo;->mLastAspectRatio:F

    goto :goto_0

    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    :goto_0
    return v1
.end method

.method public getMiuiGameSizeCompatEnabledApps()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation

    .line 1048
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 1049
    .local v6, "start":J
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v8, v0

    .line 1050
    .local v8, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z

    if-nez v0, :cond_0

    .line 1051
    const-string v0, "MiuiSizeCompatService"

    const-string v1, "Get miui game size compat enableds apps fail : service is not ready."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    return-object v8

    .line 1054
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mGameSettingConfig:Ljava/util/Map;

    new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$7;

    invoke-direct {v1, p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService$7;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/util/Map;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 1060
    const-wide/16 v3, 0xc8

    const-string v5, "getMiuiGameSizeCompatList"

    move-object v0, p0

    move-wide v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 1061
    return-object v8
.end method

.method public getMiuiSizeCompatAppRatio(Ljava/lang/String;)F
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1020
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/high16 v1, -0x40800000    # -1.0f

    if-eqz v0, :cond_0

    .line 1021
    const-string v0, "MiuiSizeCompatService"

    const-string v2, "pkgName can not be empty"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    return v1

    .line 1024
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    .line 1025
    .local v0, "info":Landroid/sizecompat/AspectRatioInfo;
    if-nez v0, :cond_1

    .line 1026
    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    .line 1028
    :cond_1
    if-eqz v0, :cond_2

    iget v1, v0, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    :cond_2
    return v1
.end method

.method public getMiuiSizeCompatEnabledApps()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation

    .line 727
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 728
    .local v6, "start":J
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v8, v0

    .line 729
    .local v8, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z

    if-nez v0, :cond_0

    .line 730
    const-string v0, "MiuiSizeCompatService"

    const-string v1, "Get miui size compat enableds apps fail : service is not ready."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    return-object v8

    .line 733
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$5;

    invoke-direct {v1, p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService$5;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/util/Map;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 739
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$6;

    invoke-direct {v1, p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService$6;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/util/Map;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 745
    const-wide/16 v3, 0xc8

    const-string v5, "getMiuiSizeCompatEnabledApps"

    move-object v0, p0

    move-wide v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 746
    return-object v8
.end method

.method public getMiuiSizeCompatInstalledApps()Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation

    .line 767
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 768
    .local v6, "start":J
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v8, v0

    .line 769
    .local v8, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z

    if-nez v0, :cond_0

    .line 770
    const-string v0, "MiuiSizeCompatService"

    const-string v1, "Get miui size compat installed apps fail : service is not ready."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    return-object v8

    .line 773
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 774
    .local v9, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v0, 0x40

    invoke-virtual {v9, v0}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v10

    .line 775
    .local v10, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 777
    .local v1, "object":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService;->isSystemApp(Landroid/content/pm/ApplicationInfo;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService;->pkgHasIcon(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService;->APPLIACTION_APP_BLACK_LIST:Ljava/util/List;

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 778
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 779
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 780
    .local v2, "packageName":Ljava/lang/String;
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v3, v9}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 782
    .local v3, "label":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 783
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/sizecompat/AspectRatioInfo;

    .local v4, "info":Landroid/sizecompat/AspectRatioInfo;
    goto :goto_1

    .line 784
    .end local v4    # "info":Landroid/sizecompat/AspectRatioInfo;
    :cond_1
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 785
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/sizecompat/AspectRatioInfo;

    .restart local v4    # "info":Landroid/sizecompat/AspectRatioInfo;
    goto :goto_1

    .line 787
    .end local v4    # "info":Landroid/sizecompat/AspectRatioInfo;
    :cond_2
    new-instance v4, Landroid/sizecompat/AspectRatioInfo;

    const/high16 v5, -0x40800000    # -1.0f

    invoke-direct {v4, v2, v5}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V

    .line 789
    .restart local v4    # "info":Landroid/sizecompat/AspectRatioInfo;
    :goto_1
    iget-object v5, v4, Landroid/sizecompat/AspectRatioInfo;->mApplicationName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 790
    iput-object v3, v4, Landroid/sizecompat/AspectRatioInfo;->mApplicationName:Ljava/lang/String;

    .line 792
    :cond_3
    invoke-interface {v8, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 796
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "label":Ljava/lang/String;
    .end local v4    # "info":Landroid/sizecompat/AspectRatioInfo;
    :cond_4
    goto :goto_2

    .line 794
    :catch_0
    move-exception v2

    .line 795
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 797
    .end local v1    # "object":Landroid/content/pm/PackageInfo;
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    goto :goto_0

    .line 798
    :cond_5
    const-wide/16 v3, 0x190

    const-string v5, "getMiuiSizeCompatInstalledApps"

    move-object v0, p0

    move-wide v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 799
    return-object v8
.end method

.method public initBlackAppList()V
    .locals 2

    .line 424
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->APPLIACTION_APP_BLACK_LIST:Ljava/util/List;

    const-string v1, "com.android.email"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 425
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->APPLIACTION_APP_BLACK_LIST:Ljava/util/List;

    const-string v1, "com.android.soundrecorder"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->APPLIACTION_APP_BLACK_LIST:Ljava/util/List;

    const-string v1, "com.duokan.phone.remotecontroller"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->APPLIACTION_APP_BLACK_LIST:Ljava/util/List;

    const-string v1, "com.mi.health"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 428
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->APPLIACTION_APP_BLACK_LIST:Ljava/util/List;

    const-string v1, "com.duokan.reader"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    return-void
.end method

.method public initSettingsDirForUser(I)V
    .locals 5
    .param p1, "userId"    # I

    .line 1178
    const-string v0, "MiuiSizeCompatService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "system/users/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1179
    .local v1, "userSettingsDir":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1181
    .local v2, "systemDir":Ljava/io/File;
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1182
    const-string v3, "Making dir failed"

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1186
    :cond_0
    goto :goto_0

    .line 1184
    :catch_0
    move-exception v3

    .line 1185
    .local v3, "e":Ljava/lang/SecurityException;
    const-string v4, "Exception throw while Making dir"

    invoke-static {v0, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    .end local v3    # "e":Ljava/lang/SecurityException;
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1fd

    const/4 v4, -0x1

    invoke-static {v0, v3, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 1189
    new-instance v0, Ljava/io/File;

    const-string/jumbo v3, "size_compat_setting_config.xml"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    .line 1190
    new-instance v0, Ljava/io/File;

    const-string/jumbo v3, "size_compat_setting_config-backup.xml"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBackupSettingFilename:Ljava/io/File;

    .line 1191
    return-void
.end method

.method isSizeCompatDisabled()Z
    .locals 3

    .line 1169
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "force_resizable_activities"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public isSystemApp(Landroid/content/pm/ApplicationInfo;)Z
    .locals 3
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;

    .line 803
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-gtz v0, :cond_1

    iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v2, 0x2710

    if-lt v0, v2, :cond_1

    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 804
    const-string v2, "com.miui"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v2, "com.xiaomi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    nop

    .line 803
    :goto_1
    return v1
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;

    .line 248
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/MiuiSizeCompatService$Shell-IA;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 249
    return-void
.end method

.method public onUserSwitch()V
    .locals 2

    .line 715
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiSizeCompatService;->initSettingsDirForUser(I)V

    .line 716
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 717
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->readSetting()V

    .line 718
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 719
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z

    .line 720
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 721
    monitor-exit v0

    .line 722
    return-void

    .line 721
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method parseRatioFromStr(Ljava/lang/String;)F
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .line 496
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    const v0, 0x3fdc3e06

    return v0

    .line 499
    :cond_0
    const v0, 0x3fe38e39

    .line 500
    .local v0, "ratio":F
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 501
    const-string v1, "-1"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 502
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0

    .line 503
    :cond_1
    const-string v1, "4:3"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 504
    const v0, 0x3faaaaaa

    goto :goto_0

    .line 506
    :cond_2
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 507
    .local v1, "values":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 508
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 509
    .local v2, "width":F
    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    .line 510
    .local v3, "height":F
    div-float v0, v2, v3

    .line 514
    .end local v1    # "values":[Ljava/lang/String;
    .end local v2    # "width":F
    .end local v3    # "height":F
    :cond_3
    :goto_0
    return v0
.end method

.method public pkgHasIcon(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 808
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    const-string v1, "launcherapps"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/LauncherApps;

    .line 809
    .local v0, "launcherApps":Landroid/content/pm/LauncherApps;
    sget-object v1, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/LauncherApps;->getActivityList(Ljava/lang/String;Landroid/os/UserHandle;)Ljava/util/List;

    move-result-object v1

    .line 810
    .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LauncherActivityInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    return v2
.end method

.method public readSetting()V
    .locals 14

    .line 1371
    const/4 v0, 0x0

    .line 1372
    .local v0, "settingsFileStream":Ljava/io/FileInputStream;
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBackupSettingFilename:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    const-string v2, "MiuiSizeCompatService"

    if-eqz v1, :cond_1

    .line 1374
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBackupSettingFilename:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v0, v1

    .line 1375
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1376
    const-string v1, "Cleaning up size_compat_setting_config.xml"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1377
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1381
    :cond_0
    goto :goto_0

    .line 1379
    :catch_0
    move-exception v1

    .line 1380
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v3, "size_compat_setting_config-backup.xml load config: "

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1384
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :goto_0
    if-nez v0, :cond_3

    .line 1385
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1386
    const-string/jumbo v1, "size_compat_setting_config.xml not found"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1387
    return-void

    .line 1390
    :cond_2
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 1393
    goto :goto_1

    .line 1391
    :catch_1
    move-exception v1

    .line 1392
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "size_compat_setting_config.xml load config: "

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1397
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    :goto_1
    const-string/jumbo v1, "size_compat_setting_config.xmlload config: "

    if-eqz v0, :cond_7

    .line 1398
    :try_start_2
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 1399
    .local v3, "xmlParser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 1400
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    .line 1402
    .local v5, "xmlEventType":I
    :goto_2
    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    .line 1403
    goto/16 :goto_4

    .line 1405
    :cond_4
    const/4 v6, 0x2

    if-ne v5, v6, :cond_6

    const-string/jumbo v6, "setting"

    .line 1406
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1407
    const-string v6, "name"

    invoke-interface {v3, v4, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1409
    .local v6, "packageName":Ljava/lang/String;
    const-string v7, "displayName"

    invoke-interface {v3, v4, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1411
    .local v7, "applicationName":Ljava/lang/String;
    const-string v8, "aspectRatio"

    invoke-interface {v3, v4, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1413
    .local v8, "aspectRatio":Ljava/lang/String;
    const-string v9, "gravity"

    invoke-interface {v3, v4, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1415
    .local v9, "gravity":Ljava/lang/String;
    const-string v10, "scaleMode"

    invoke-interface {v3, v4, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1417
    .local v10, "scaleMode":Ljava/lang/String;
    iget-object v11, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    new-instance v12, Landroid/sizecompat/AspectRatioInfo$Builder;

    invoke-direct {v12}, Landroid/sizecompat/AspectRatioInfo$Builder;-><init>()V

    .line 1418
    invoke-virtual {v12, v6}, Landroid/sizecompat/AspectRatioInfo$Builder;->setPackageName(Ljava/lang/String;)Landroid/sizecompat/AspectRatioInfo$Builder;

    move-result-object v12

    .line 1419
    invoke-virtual {v12, v7}, Landroid/sizecompat/AspectRatioInfo$Builder;->setApplicationName(Ljava/lang/String;)Landroid/sizecompat/AspectRatioInfo$Builder;

    move-result-object v12

    .line 1420
    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/sizecompat/AspectRatioInfo$Builder;->setAspectRatio(Ljava/lang/Float;)Landroid/sizecompat/AspectRatioInfo$Builder;

    move-result-object v12

    .line 1421
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/sizecompat/AspectRatioInfo$Builder;->setGravity(I)Landroid/sizecompat/AspectRatioInfo$Builder;

    move-result-object v12

    .line 1422
    invoke-static {v10}, Landroid/sizecompat/AspectRatioInfo;->isValidScaleModeStr(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1423
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    goto :goto_3

    :cond_5
    const/4 v13, 0x0

    .line 1422
    :goto_3
    invoke-virtual {v12, v13}, Landroid/sizecompat/AspectRatioInfo$Builder;->setScaleMode(I)Landroid/sizecompat/AspectRatioInfo$Builder;

    move-result-object v12

    .line 1424
    invoke-virtual {v12}, Landroid/sizecompat/AspectRatioInfo$Builder;->build()Landroid/sizecompat/AspectRatioInfo;

    move-result-object v12

    .line 1417
    invoke-interface {v11, v6, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1426
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "applicationName":Ljava/lang/String;
    .end local v8    # "aspectRatio":Ljava/lang/String;
    .end local v9    # "gravity":Ljava/lang/String;
    .end local v10    # "scaleMode":Ljava/lang/String;
    :cond_6
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    move v5, v6

    goto :goto_2

    .line 1436
    .end local v3    # "xmlParser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v5    # "xmlEventType":I
    :catchall_0
    move-exception v3

    goto :goto_5

    .line 1434
    :catch_2
    move-exception v3

    goto :goto_6

    .line 1432
    :catch_3
    move-exception v3

    goto :goto_7

    .line 1430
    :catch_4
    move-exception v3

    goto :goto_8

    .line 1429
    :cond_7
    :goto_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Setting configs: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_9

    .line 1437
    .local v3, "e":Ljava/lang/Throwable;
    :goto_5
    invoke-static {v2, v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    .line 1435
    .local v3, "e":Ljava/io/IOException;
    :goto_6
    invoke-static {v2, v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_9

    .line 1433
    .local v3, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_7
    invoke-static {v2, v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v3    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    goto :goto_9

    .line 1431
    .local v3, "e":Ljava/io/FileNotFoundException;
    :goto_8
    invoke-static {v2, v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1438
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :goto_9
    nop

    .line 1440
    :goto_a
    if-eqz v0, :cond_8

    .line 1442
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 1446
    goto :goto_b

    .line 1443
    :catch_5
    move-exception v1

    .line 1444
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "size_compat_setting_config.xmlload config:IO Exception while closing stream"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1448
    .end local v1    # "e":Ljava/io/IOException;
    :cond_8
    :goto_b
    return-void
.end method

.method registerUserSwitchReceiver()V
    .locals 4

    .line 693
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 694
    .local v0, "userSwitchFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 695
    const-string v1, "android.intent.action.USER_UNLOCKED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 696
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/server/wm/MiuiSizeCompatService$4;

    invoke-direct {v2, p0}, Lcom/android/server/wm/MiuiSizeCompatService$4;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;)V

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 712
    return-void
.end method

.method public removeRunningApp(Ljava/lang/String;)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 918
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mPMS:Lcom/miui/server/process/ProcessManagerInternal;

    if-eqz v0, :cond_0

    .line 919
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I

    move-result v1

    const-string/jumbo v2, "size compat change"

    invoke-virtual {v0, p1, v1, v2}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 921
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " Force stop "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " due to sizecompatRatio change"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiSizeCompatService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 922
    return-void
.end method

.method public restorePrevRatio()V
    .locals 3

    .line 956
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mCurFullAct:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mNotificationShowing:Z

    if-eqz v0, :cond_0

    .line 957
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiSizeCompatService$$ExternalSyntheticLambda0;-><init>()V

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mCurFullAct:Lcom/android/server/wm/ActivityRecord;

    iget-object v2, v1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 958
    invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiSizeCompatService;->getLastAspectRatioValue(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 957
    invoke-static {v0, p0, v1, v2}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainMessage(Lcom/android/internal/util/function/TriConsumer;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 959
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mFgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 961
    .end local v0    # "message":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public setMiuiGameSizeCompatList(Ljava/lang/String;Z)Z
    .locals 11
    .param p1, "json"    # Ljava/lang/String;
    .param p2, "override"    # Z

    .line 985
    const/4 v0, 0x0

    const-string v1, "MiuiSizeCompatService"

    if-eqz p2, :cond_0

    .line 986
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mGameSettingConfig:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 988
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    .line 989
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The game json list is empty, override is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    return v3

    .line 992
    :cond_1
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 993
    .local v2, "jsonArray":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 994
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/json/JSONObject;

    .line 995
    .local v5, "object":Lorg/json/JSONObject;
    const-string v6, "pkgName"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 996
    .local v6, "pkgName":Ljava/lang/String;
    const-string v7, "gravity"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 997
    .local v7, "gravity":I
    invoke-static {v7}, Landroid/sizecompat/AspectRatioInfo;->isGravityEffect(I)Z

    move-result v8

    if-nez v8, :cond_2

    .line 998
    const-string v8, "The gravity is not effect, use default center gravity instead."

    invoke-static {v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    const/16 v7, 0x11

    .line 1001
    :cond_2
    const-string v8, "aspectRatio"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/android/server/wm/MiuiSizeCompatService;->parseAspectRatio(Ljava/lang/Object;)F

    move-result v8

    .line 1002
    .local v8, "ratio":F
    iget-object v9, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mGameSettingConfig:Ljava/util/Map;

    new-instance v10, Landroid/sizecompat/AspectRatioInfo;

    invoke-direct {v10, v6, v8, v7}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;FI)V

    invoke-interface {v9, v6, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 993
    nop

    .end local v5    # "object":Lorg/json/JSONObject;
    .end local v6    # "pkgName":Ljava/lang/String;
    .end local v7    # "gravity":I
    .end local v8    # "ratio":F
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1004
    .end local v4    # "i":I
    :cond_3
    sget-boolean v4, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z

    if-eqz v4, :cond_4

    .line 1005
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setMiuiGameSizeCompatList\uff1a"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1014
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    :cond_4
    nop

    .line 1015
    return v3

    .line 1011
    :catch_0
    move-exception v1

    .line 1012
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1013
    return v0

    .line 1007
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 1008
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 1009
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The game list json is error format.\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    return v0
.end method

.method public setMiuiSizeCompatEnabled(Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;)Z
    .locals 11
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "info"    # Landroid/sizecompat/AspectRatioInfo;

    .line 815
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z

    const/4 v1, 0x0

    const-string v2, "MiuiSizeCompatService"

    if-nez v0, :cond_0

    .line 816
    const-string v0, "Set miui size compat enabled fail: service is not ready."

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    return v1

    .line 819
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    .line 820
    .local v9, "start":J
    if-eqz p2, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 824
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 825
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService;->writeSetting()V

    .line 826
    const-wide/16 v6, 0x12c

    const-string/jumbo v8, "setMiuiSizeCompatEnabled"

    move-object v3, p0

    move-wide v4, v9

    invoke-direct/range {v3 .. v8}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 827
    const/4 v0, 0x1

    return v0

    .line 821
    :cond_2
    :goto_0
    const-string v0, "Set miui size compat enabled fail: App pkgName or aspect info is null."

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    return v1
.end method

.method public setMiuiSizeCompatRatio(Ljava/lang/String;FZZ)Z
    .locals 16
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "ratio"    # F
    .param p3, "write"    # Z
    .param p4, "kill"    # Z

    .line 861
    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move/from16 v8, p2

    move/from16 v9, p3

    move/from16 v10, p4

    iget-boolean v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mServiceReady:Z

    const/4 v1, 0x0

    const-string v2, "MiuiSizeCompatService"

    if-nez v0, :cond_0

    .line 862
    const-string v0, "Set miui size compat ratio fail : service is not ready."

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    return v1

    .line 865
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    .line 866
    .local v11, "start":J
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v13

    .line 867
    .local v13, "callingPid":I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v14

    .line 868
    .local v14, "callingUid":I
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 869
    const-string v0, "Set miui size compat ratio fail : pkgName is null."

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    return v1

    .line 872
    :cond_1
    invoke-direct {v6, v13, v14}, Lcom/android/server/wm/MiuiSizeCompatService;->checkInterfaceAccess(II)Z

    move-result v0

    if-nez v0, :cond_2

    .line 873
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Do not allow pid= "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " uid= "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to change miui size compat ratio."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    return v1

    .line 877
    :cond_2
    sget-boolean v0, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z

    if-nez v0, :cond_3

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    if-eq v13, v0, :cond_4

    .line 878
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMiuiSizeCompatRatio "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ratio = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from pid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    :cond_4
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    .line 882
    .local v0, "info":Landroid/sizecompat/AspectRatioInfo;
    if-nez v0, :cond_6

    .line 883
    iget-object v1, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mStaticSettingConfigs:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/sizecompat/AspectRatioInfo;

    .line 884
    .local v1, "staticInfo":Landroid/sizecompat/AspectRatioInfo;
    if-eqz v1, :cond_5

    new-instance v2, Landroid/sizecompat/AspectRatioInfo;

    iget v3, v1, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    invoke-direct {v2, v7, v3}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V

    goto :goto_0

    .line 885
    :cond_5
    new-instance v2, Landroid/sizecompat/AspectRatioInfo;

    const/high16 v3, -0x40800000    # -1.0f

    invoke-direct {v2, v7, v3}, Landroid/sizecompat/AspectRatioInfo;-><init>(Ljava/lang/String;F)V

    :goto_0
    move-object v0, v2

    .line 886
    iget-object v2, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v2, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v15, v0

    goto :goto_1

    .line 882
    .end local v1    # "staticInfo":Landroid/sizecompat/AspectRatioInfo;
    :cond_6
    move-object v15, v0

    .line 888
    .end local v0    # "info":Landroid/sizecompat/AspectRatioInfo;
    .local v15, "info":Landroid/sizecompat/AspectRatioInfo;
    :goto_1
    invoke-virtual {v15, v8}, Landroid/sizecompat/AspectRatioInfo;->setAspectRatio(F)V

    .line 889
    if-eqz v9, :cond_7

    .line 890
    iget-object v0, v6, Lcom/android/server/wm/MiuiSizeCompatService;->mBgHandler:Lcom/android/server/wm/MiuiSizeCompatService$H;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$H;->sendMessage(Landroid/os/Message;)Z

    .line 892
    :cond_7
    if-eqz v10, :cond_8

    .line 893
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/wm/MiuiSizeCompatService;->removeRunningApp(Ljava/lang/String;)V

    .line 894
    invoke-static {}, Lcom/android/server/app/GameManagerServiceStub;->getInstance()Lcom/android/server/app/GameManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/android/server/app/GameManagerServiceStub;->toggleDownscaleForStopedApp(Ljava/lang/String;)V

    .line 896
    :cond_8
    const-wide/16 v3, 0x64

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMiuiSizeCompatRatio, write: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , kill= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-wide v1, v11

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 898
    const/4 v0, 0x1

    return v0
.end method

.method public switchToFullscreen(Landroid/window/WindowContainerToken;)Z
    .locals 10
    .param p1, "token"    # Landroid/window/WindowContainerToken;

    .line 926
    const-string v0, "restartTopActivityProcessIfVisible()"

    invoke-static {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->enforceTaskPermission(Ljava/lang/String;)V

    .line 928
    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 929
    .local v2, "start":J
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->getActivityFromTokenLocked(Landroid/window/WindowContainerToken;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v7, v1

    .line 930
    .local v7, "activity":Lcom/android/server/wm/ActivityRecord;
    const-string v1, "MiuiSizeCompatService"

    if-eqz v7, :cond_3

    :try_start_1
    iget-object v4, v7, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    if-nez v4, :cond_0

    goto :goto_1

    .line 934
    :cond_0
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mGameSettingConfig:Ljava/util/Map;

    iget-object v5, v7, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 935
    const-string v4, "Game app can not switch to fullscreen by systemui button."

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    return v0

    .line 938
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSecurityManager:Lmiui/security/SecurityManager;

    iget-object v4, v7, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lmiui/security/SecurityManager;->isScRelaunchNeedConfirm(Ljava/lang/String;)Z

    move-result v1

    move v8, v1

    .line 939
    .local v8, "confirm":Z
    const/4 v9, 0x1

    if-eqz v8, :cond_2

    .line 940
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mUiHandler:Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;

    invoke-virtual {v1, v9, v7}, Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/server/wm/MiuiSizeCompatService$UiHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 943
    :cond_2
    iput-object v7, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mCurFullAct:Lcom/android/server/wm/ActivityRecord;

    .line 944
    const/4 v1, 0x0

    invoke-direct {p0, v7, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->restartProcessForRatioLocked(Lcom/android/server/wm/ActivityRecord;F)V

    .line 946
    :goto_0
    const-wide/16 v4, 0xc8

    const-string/jumbo v6, "switchToFullscreen"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V

    .line 947
    return v9

    .line 931
    .end local v8    # "confirm":Z
    :cond_3
    :goto_1
    const-string v4, "Could not resolve activity from token"

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 932
    return v0

    .line 948
    .end local v2    # "start":J
    .end local v7    # "activity":Lcom/android/server/wm/ActivityRecord;
    :catch_0
    move-exception v1

    .line 949
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 950
    return v0
.end method

.method public writeSetting()V
    .locals 15

    .line 1452
    const-string/jumbo v0, "setting"

    const-string/jumbo v1, "setting_config"

    const/4 v2, 0x0

    .line 1453
    .local v2, "settingsBufferedOutputStream":Ljava/io/BufferedOutputStream;
    const/4 v3, 0x0

    .line 1454
    .local v3, "settingsFileStream":Ljava/io/FileOutputStream;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 1455
    .local v10, "startTime":J
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    const-string v12, "MiuiSizeCompatService"

    if-eqz v4, :cond_1

    .line 1456
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBackupSettingFilename:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1457
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 1458
    const-string/jumbo v4, "size_compat_setting_config.xml delete old file"

    invoke-static {v12, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1459
    :cond_0
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    iget-object v5, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBackupSettingFilename:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1460
    const-string v0, "Unable to backup size_compat_setting_config, current changes will be lost at reboot"

    invoke-static {v12, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    return-void

    .line 1467
    :cond_1
    :goto_0
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v3, v4

    .line 1468
    new-instance v4, Ljava/io/BufferedOutputStream;

    invoke-direct {v4, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v2, v4

    .line 1469
    new-instance v4, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v4}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    move-object v13, v4

    .line 1470
    .local v13, "serializer":Lcom/android/internal/util/FastXmlSerializer;
    sget-object v4, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v4}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v2, v4}, Lcom/android/internal/util/FastXmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1471
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v13, v6, v5}, Lcom/android/internal/util/FastXmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1472
    const-string v5, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-virtual {v13, v5, v4}, Lcom/android/internal/util/FastXmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 1473
    invoke-virtual {v13, v6, v1}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1474
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1475
    .local v5, "pkg":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingConfigs:Ljava/util/Map;

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/sizecompat/AspectRatioInfo;

    .line 1476
    .local v7, "info":Landroid/sizecompat/AspectRatioInfo;
    if-nez v7, :cond_2

    .line 1477
    const-string v8, "Info is null when write settings."

    invoke-static {v12, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1478
    goto :goto_1

    .line 1480
    :cond_2
    invoke-virtual {v13, v6, v0}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1481
    const-string v8, "name"

    invoke-virtual {v13, v6, v8, v5}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1482
    iget-object v8, v7, Landroid/sizecompat/AspectRatioInfo;->mApplicationName:Ljava/lang/String;

    if-nez v8, :cond_3

    const-string v8, ""

    goto :goto_2

    :cond_3
    iget-object v8, v7, Landroid/sizecompat/AspectRatioInfo;->mApplicationName:Ljava/lang/String;

    .line 1483
    .local v8, "applicationName":Ljava/lang/String;
    :goto_2
    const-string v9, "displayName"

    invoke-virtual {v13, v6, v9, v8}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1484
    const-string v9, "aspectRatio"

    iget v14, v7, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    .line 1485
    invoke-static {v14}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v14

    .line 1484
    invoke-virtual {v13, v6, v9, v14}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1486
    const-string v9, "gravity"

    iget v14, v7, Landroid/sizecompat/AspectRatioInfo;->mGravity:I

    .line 1487
    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    .line 1486
    invoke-virtual {v13, v6, v9, v14}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1488
    const-string v9, "scaleMode"

    .line 1489
    invoke-virtual {v7}, Landroid/sizecompat/AspectRatioInfo;->getScaleMode()I

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    .line 1488
    invoke-virtual {v13, v6, v9, v14}, Lcom/android/internal/util/FastXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1490
    invoke-virtual {v13, v6, v0}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1491
    nop

    .end local v5    # "pkg":Ljava/lang/String;
    .end local v7    # "info":Landroid/sizecompat/AspectRatioInfo;
    .end local v8    # "applicationName":Ljava/lang/String;
    goto :goto_1

    .line 1493
    :cond_4
    invoke-virtual {v13, v6, v1}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1494
    invoke-virtual {v13}, Lcom/android/internal/util/FastXmlSerializer;->endDocument()V

    .line 1495
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V

    .line 1496
    invoke-static {v3}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    .line 1497
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mBackupSettingFilename:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1498
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x1b0

    const/4 v4, -0x1

    invoke-static {v0, v1, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 1499
    const-wide/16 v7, 0xc8

    const-string/jumbo v9, "write size_compat_setting_config.xml"

    move-object v4, p0

    move-wide v5, v10

    invoke-direct/range {v4 .. v9}, Lcom/android/server/wm/MiuiSizeCompatService;->checkSlow(JJLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1507
    .end local v13    # "serializer":Lcom/android/internal/util/FastXmlSerializer;
    goto :goto_3

    :catchall_0
    move-exception v0

    goto :goto_4

    .line 1500
    :catch_0
    move-exception v0

    .line 1501
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1502
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to clean up mangled file: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService;->mSettingFilename:Ljava/io/File;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1504
    :cond_5
    const-string v1, "Unable to write host recognize settings,current changes will be lost at reboot"

    invoke-static {v12, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1507
    nop

    .end local v0    # "e":Ljava/io/IOException;
    :goto_3
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1508
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1509
    nop

    .line 1510
    return-void

    .line 1507
    :goto_4
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1508
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1509
    throw v0
.end method
