.class Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;
.super Lmiuix/animation/listener/TransitionListener;
.source "MiuiMultiWindowRecommendController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->initFolmeConfig()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    .line 305
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-direct {p0}, Lmiuix/animation/listener/TransitionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onBegin(Ljava/lang/Object;)V
    .locals 2
    .param p1, "toTag"    # Ljava/lang/Object;

    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mShowHideAnimConfig onBegin: toTag= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiWindowRecommendController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    const-string v0, "freeFormRecommendShow"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setCornerRadiusAndShadow(Landroid/view/View;)V

    goto :goto_0

    .line 311
    :cond_0
    const-string/jumbo v0, "splitScreenRecommendShow"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setCornerRadiusAndShadow(Landroid/view/View;)V

    goto :goto_0

    .line 313
    :cond_1
    const-string v0, "freeFormRecommendHide"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->resetShadow(Landroid/view/View;)V

    goto :goto_0

    .line 315
    :cond_2
    const-string/jumbo v0, "splitScreenRecommendHide"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 316
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->resetShadow(Landroid/view/View;)V

    .line 319
    :cond_3
    :goto_0
    return-void
.end method

.method public onComplete(Ljava/lang/Object;)V
    .locals 2
    .param p1, "toTag"    # Ljava/lang/Object;

    .line 323
    invoke-super {p0, p1}, Lmiuix/animation/listener/TransitionListener;->onComplete(Ljava/lang/Object;)V

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mShowHideAnimConfig onComplete: toTag= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiWindowRecommendController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    const-string v0, "freeFormRecommendHide"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/FreeFormRecommendLayout;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V

    goto :goto_0

    .line 328
    :cond_0
    const-string/jumbo v0, "splitScreenRecommendHide"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->setVisibility(I)V

    .line 330
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V

    .line 332
    :cond_1
    :goto_0
    return-void
.end method
