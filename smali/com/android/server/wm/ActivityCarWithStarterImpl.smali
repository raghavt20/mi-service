.class public Lcom/android/server/wm/ActivityCarWithStarterImpl;
.super Ljava/lang/Object;
.source "ActivityCarWithStarterImpl.java"


# static fields
.field private static final MIUI_CARLINK_PERMISSION:Ljava/lang/String; = "miui.car.permission.MI_CARLINK_STATUS"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAddress:Ljava/lang/String;

.field private mAltitude:D

.field private mCoordinateType:Ljava/lang/String;

.field private mLatitude:D

.field private mLongitude:D


# direct methods
.method public static synthetic $r8$lambda$LY34ctSSQxgjrTOp40N0BwryIgY(Lcom/android/server/wm/ActivityCarWithStarterImpl;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->lambda$recordCarWithIntent$0(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "ActivityCarWithStarterImpl"

    iput-object v0, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->TAG:Ljava/lang/String;

    .line 20
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D

    .line 21
    iput-wide v0, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D

    .line 22
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAltitude:D

    .line 23
    const-string v0, "NA"

    iput-object v0, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAddress:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mCoordinateType:Ljava/lang/String;

    .line 27
    return-void
.end method

.method private synthetic lambda$recordCarWithIntent$0(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mCarWithIntent"    # Landroid/content/Intent;

    .line 79
    const-string v0, "ActivityCarWithStarterImpl"

    const-string v1, "Send BroadCast To carlink"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const-string v0, "miui.car.permission.MI_CARLINK_STATUS"

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method private recordBaiduCoorType(Ljava/lang/String;)V
    .locals 4
    .param p1, "dat"    # Ljava/lang/String;

    .line 138
    const-string v0, "coord_type"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const-string v0, "coord_type=(\\w+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 140
    .local v0, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 141
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 142
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mCoordinateType:Ljava/lang/String;

    .line 143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCoordinateType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mCoordinateType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ActivityCarWithStarterImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    .end local v0    # "pattern":Ljava/util/regex/Pattern;
    .end local v1    # "matcher":Ljava/util/regex/Matcher;
    :cond_0
    return-void
.end method

.method private recordBaiduMap(Ljava/lang/String;)V
    .locals 8
    .param p1, "dat"    # Ljava/lang/String;

    .line 112
    const-string v0, ""

    .line 114
    .local v0, "mUriRegex":Ljava/lang/String;
    const-string v1, "destination=latlng"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eqz v1, :cond_0

    .line 115
    const-string v0, "destination=latlng:([\\d\\.]+),([\\d\\.]+)\\|name:([^&]+)"

    .line 116
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 117
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 118
    .local v5, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 119
    invoke-virtual {v5, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D

    .line 120
    invoke-virtual {v5, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D

    .line 121
    invoke-virtual {v5, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAddress:Ljava/lang/String;

    goto :goto_0

    .line 123
    .end local v1    # "pattern":Ljava/util/regex/Pattern;
    .end local v5    # "matcher":Ljava/util/regex/Matcher;
    :cond_0
    const-string v1, "destination=name"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    const-string v0, "destination=name:([^|]+)\\|latlng:([\\d\\.]+),([\\d\\.]+)"

    .line 125
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 126
    .restart local v1    # "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 127
    .restart local v5    # "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 128
    invoke-virtual {v5, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAddress:Ljava/lang/String;

    .line 129
    invoke-virtual {v5, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D

    .line 130
    invoke-virtual {v5, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D

    goto :goto_0

    .line 133
    .end local v1    # "pattern":Ljava/util/regex/Pattern;
    .end local v5    # "matcher":Ljava/util/regex/Matcher;
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordDefaultMap(Ljava/lang/String;)V

    .line 135
    :cond_2
    :goto_0
    return-void
.end method

.method private recordDefaultMap(Ljava/lang/String;)V
    .locals 10
    .param p1, "dat"    # Ljava/lang/String;

    .line 169
    const/4 v0, 0x0

    .line 170
    .local v0, "latIndex":I
    const/4 v1, 0x0

    .line 171
    .local v1, "lonIndex":I
    const-string v2, "^-?((0|[1-9]\\d?|1[0-7]\\d)(\\.\\d+)?|180(\\.0+)?)$"

    .line 172
    .local v2, "regexLongitude":Ljava/lang/String;
    const-string v3, "^-?([1-8]\\d?|([1-9]0))(\\.\\d+)?|90(\\.0+)?$"

    .line 173
    .local v3, "regexLatitude":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 174
    .local v4, "latitudes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 175
    .local v5, "longitudes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    const-string v6, "(\\d+\\.\\d+)"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 176
    .local v6, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v6, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 177
    .local v7, "matcher":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 178
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v8

    .line 179
    .local v8, "value":Ljava/lang/String;
    invoke-virtual {v8, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 180
    invoke-static {v8}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 182
    :cond_0
    invoke-virtual {v8, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 183
    invoke-static {v8}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    add-int/lit8 v1, v1, 0x1

    .line 186
    .end local v8    # "value":Ljava/lang/String;
    :cond_1
    :goto_1
    goto :goto_0

    .line 187
    :cond_2
    if-eqz v0, :cond_3

    if-ne v0, v1, :cond_3

    .line 188
    add-int/lit8 v0, v0, -0x1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Double;

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iput-wide v8, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D

    .line 189
    add-int/lit8 v1, v1, -0x1

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Double;

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iput-wide v8, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D

    .line 190
    const-string v8, "NA"

    iput-object v8, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAddress:Ljava/lang/String;

    .line 192
    :cond_3
    return-void
.end method

.method private recordGaoDeMap(Ljava/lang/String;)V
    .locals 6
    .param p1, "dat"    # Ljava/lang/String;

    .line 89
    const-string v0, "dlat"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    const-string v0, "dlat=(\\d+\\.\\d+)&dlon=(\\d+\\.\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 91
    .local v0, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 92
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 93
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D

    .line 94
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D

    .line 96
    :cond_0
    const-string v2, "dname=([^&]+)"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 97
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 98
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 99
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAddress:Ljava/lang/String;

    .line 100
    .end local v0    # "pattern":Ljava/util/regex/Pattern;
    .end local v1    # "matcher":Ljava/util/regex/Matcher;
    :cond_1
    goto :goto_0

    :cond_2
    const-string v0, "denstination"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 101
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordBaiduMap(Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_3
    const-string/jumbo v0, "tocoord"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordTencentMap(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_4
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordDefaultMap(Ljava/lang/String;)V

    .line 107
    :goto_0
    return-void
.end method

.method private recordTencentMap(Ljava/lang/String;)V
    .locals 6
    .param p1, "dat"    # Ljava/lang/String;

    .line 150
    const-string/jumbo v0, "tocoord"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    const-string/jumbo v0, "tocoord=(\\d+\\.\\d+),(\\d+\\.\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 152
    .local v0, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 153
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 154
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D

    .line 155
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D

    .line 157
    :cond_0
    const-string/jumbo v2, "to=([^&]+)"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 158
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 160
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAddress:Ljava/lang/String;

    .line 162
    .end local v0    # "pattern":Ljava/util/regex/Pattern;
    .end local v1    # "matcher":Ljava/util/regex/Matcher;
    :cond_1
    goto :goto_0

    .line 163
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordDefaultMap(Ljava/lang/String;)V

    .line 165
    :goto_0
    return-void
.end method


# virtual methods
.method public recordCarWithIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callingPackage"    # Ljava/lang/String;
    .param p3, "pkg"    # Ljava/lang/String;
    .param p4, "dat"    # Ljava/lang/String;

    .line 32
    move-object/from16 v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    const-string v4, "com.baidu.BaiduMap"

    .line 33
    .local v4, "BAIDUMAP":Ljava/lang/String;
    const-string v5, "com.autonavi.minimap"

    .line 34
    .local v5, "GAODEMAP":Ljava/lang/String;
    const-string v6, "com.tencent.map"

    .line 36
    .local v6, "TENCENTMAP":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "com.autonavi.minimap"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v0, "com.baidu.BaiduMap"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v0, "com.tencent.map"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 48
    invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordDefaultMap(Ljava/lang/String;)V

    goto :goto_2

    .line 45
    :pswitch_0
    invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordTencentMap(Ljava/lang/String;)V

    .line 46
    goto :goto_2

    .line 41
    :pswitch_1
    invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordBaiduCoorType(Ljava/lang/String;)V

    .line 42
    invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordBaiduMap(Ljava/lang/String;)V

    .line 43
    goto :goto_2

    .line 38
    :pswitch_2
    invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordGaoDeMap(Ljava/lang/String;)V

    .line 39
    nop

    .line 51
    :goto_2
    iget-wide v7, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D

    const-wide/high16 v9, -0x4010000000000000L    # -1.0

    cmpl-double v0, v7, v9

    if-nez v0, :cond_1

    iget-wide v9, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D

    cmpl-double v0, v7, v9

    if-nez v0, :cond_1

    const-string v0, "NA"

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    return-void

    .line 55
    :cond_1
    const-string v7, "com.miui.carlink.map.CarMapReceiver"

    .line 56
    .local v7, "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    const-string v8, "com.miui.carlink"

    .line 57
    .local v8, "POIADDRESS_PACKAGE_NAME":Ljava/lang/String;
    const-string v9, "poiaddress_poidetailInfo_type"

    .line 58
    .local v9, "POIADDRESS_POIFETAILINFO_TYPE":Ljava/lang/String;
    const-string v10, "poiaddress_poidetailInfo_latitude"

    .line 59
    .local v10, "POIADDRESS_POIFETAILINFO_LATITUDE":Ljava/lang/String;
    const-string v11, "poiaddress_poidetailInfo_longitude"

    .line 60
    .local v11, "POIADDRESS_POIFETAILINFO_LONGITUDE":Ljava/lang/String;
    const-string v12, "poiaddress_poidetailInfo_altitude"

    .line 61
    .local v12, "POIADDRESS_POIFETAILINFO_ALTITUDE":Ljava/lang/String;
    const-string v13, "poiaddress_address"

    .line 62
    .local v13, "POIADDRESS_ADDRESS":Ljava/lang/String;
    const-string v14, "poiaddress_src_package_name"

    .line 63
    .local v14, "POIADDRESS_SRC_PACKAGE_NAME":Ljava/lang/String;
    const-string v15, "poiaddress_dest_package_name"

    .line 66
    .local v15, "POIADDRESS_DEST_PACKAGE_NAME":Ljava/lang/String;
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 67
    .local v0, "mCarWithIntent":Landroid/content/Intent;
    new-instance v16, Landroid/os/Bundle;

    invoke-direct/range {v16 .. v16}, Landroid/os/Bundle;-><init>()V

    move-object/from16 v17, v16

    .line 68
    .local v17, "mCarWithBundle":Landroid/os/Bundle;
    const-string v3, "com.miui.carlink.map.CarMapReceiver"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v3, "poiaddress_poidetailInfo_type"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    move-object/from16 v16, v4

    .end local v4    # "BAIDUMAP":Ljava/lang/String;
    .local v16, "BAIDUMAP":Ljava/lang/String;
    :try_start_1
    iget-object v4, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mCoordinateType:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-object/from16 v18, v5

    move-object/from16 v5, v17

    .end local v17    # "mCarWithBundle":Landroid/os/Bundle;
    .local v5, "mCarWithBundle":Landroid/os/Bundle;
    .local v18, "GAODEMAP":Ljava/lang/String;
    :try_start_2
    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "poiaddress_poidetailInfo_latitude"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move-object v4, v6

    move-object/from16 v17, v7

    .end local v6    # "TENCENTMAP":Ljava/lang/String;
    .end local v7    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    .local v4, "TENCENTMAP":Ljava/lang/String;
    .local v17, "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    :try_start_3
    iget-wide v6, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D

    invoke-virtual {v5, v3, v6, v7}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 71
    const-string v3, "poiaddress_poidetailInfo_longitude"

    iget-wide v6, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D

    invoke-virtual {v5, v3, v6, v7}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 72
    const-string v3, "poiaddress_poidetailInfo_altitude"

    iget-wide v6, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAltitude:D

    invoke-virtual {v5, v3, v6, v7}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 73
    const-string v3, "poiaddress_address"

    iget-object v6, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAddress:Ljava/lang/String;

    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "poiaddress_src_package_name"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-object/from16 v6, p2

    :try_start_4
    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "poiaddress_dest_package_name"

    invoke-virtual {v5, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-virtual {v0, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 77
    const-string v3, "com.miui.carlink"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    new-instance v7, Lcom/android/server/wm/ActivityCarWithStarterImpl$$ExternalSyntheticLambda0;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-object/from16 v2, p1

    :try_start_5
    invoke-direct {v7, v1, v2, v0}, Lcom/android/server/wm/ActivityCarWithStarterImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityCarWithStarterImpl;Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v3, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 84
    nop

    .end local v0    # "mCarWithIntent":Landroid/content/Intent;
    .end local v5    # "mCarWithBundle":Landroid/os/Bundle;
    goto :goto_4

    .line 82
    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object/from16 v2, p1

    goto :goto_3

    :catch_2
    move-exception v0

    move-object/from16 v2, p1

    move-object/from16 v6, p2

    goto :goto_3

    .end local v4    # "TENCENTMAP":Ljava/lang/String;
    .end local v17    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    .restart local v6    # "TENCENTMAP":Ljava/lang/String;
    .restart local v7    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    :catch_3
    move-exception v0

    move-object/from16 v2, p1

    move-object v4, v6

    move-object/from16 v17, v7

    move-object/from16 v6, p2

    .end local v6    # "TENCENTMAP":Ljava/lang/String;
    .end local v7    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    .restart local v4    # "TENCENTMAP":Ljava/lang/String;
    .restart local v17    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    goto :goto_3

    .end local v4    # "TENCENTMAP":Ljava/lang/String;
    .end local v17    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    .end local v18    # "GAODEMAP":Ljava/lang/String;
    .local v5, "GAODEMAP":Ljava/lang/String;
    .restart local v6    # "TENCENTMAP":Ljava/lang/String;
    .restart local v7    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    :catch_4
    move-exception v0

    move-object/from16 v2, p1

    move-object/from16 v18, v5

    move-object v4, v6

    move-object/from16 v17, v7

    move-object/from16 v6, p2

    .end local v5    # "GAODEMAP":Ljava/lang/String;
    .end local v6    # "TENCENTMAP":Ljava/lang/String;
    .end local v7    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    .restart local v4    # "TENCENTMAP":Ljava/lang/String;
    .restart local v17    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    .restart local v18    # "GAODEMAP":Ljava/lang/String;
    goto :goto_3

    .end local v16    # "BAIDUMAP":Ljava/lang/String;
    .end local v17    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    .end local v18    # "GAODEMAP":Ljava/lang/String;
    .local v4, "BAIDUMAP":Ljava/lang/String;
    .restart local v5    # "GAODEMAP":Ljava/lang/String;
    .restart local v6    # "TENCENTMAP":Ljava/lang/String;
    .restart local v7    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    :catch_5
    move-exception v0

    move-object/from16 v2, p1

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    move-object v4, v6

    move-object/from16 v17, v7

    move-object/from16 v6, p2

    .line 83
    .end local v5    # "GAODEMAP":Ljava/lang/String;
    .end local v6    # "TENCENTMAP":Ljava/lang/String;
    .end local v7    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    .local v0, "e":Ljava/lang/Exception;
    .local v4, "TENCENTMAP":Ljava/lang/String;
    .restart local v16    # "BAIDUMAP":Ljava/lang/String;
    .restart local v17    # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
    .restart local v18    # "GAODEMAP":Ljava/lang/String;
    :goto_3
    const-string v3, "ActivityCarWithStarterImpl"

    const-string v5, "Send BroadCast To CarWithFailed"

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x62ba9ba -> :sswitch_2
        0x2c649fe1 -> :sswitch_1
        0x4ac75759 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
