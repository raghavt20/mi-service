.class public Lcom/android/server/wm/PreloadLifecycle;
.super Ljava/lang/Object;
.source "PreloadLifecycle.java"


# static fields
.field public static final NO_PRELOAD:I = 0x0

.field public static final ON_FREEZE:I = 0x6

.field public static final ON_STOP:I = 0x4

.field public static final PRELOAD_SOON:I = 0x2

.field public static final PRE_FREEZE:I = 0x5

.field public static final PRE_KILL:I = 0x7

.field public static final PRE_STOP:I = 0x3

.field public static final START_PRELOAD:I = 0x1


# instance fields
.field protected mAlreadyPreloaded:Z

.field protected mConfig:Lmiui/process/LifecycleConfig;

.field private mDisplayId:I

.field protected mFreezeTimeout:J

.field protected mIgnoreMemory:Z

.field protected mKillTimeout:J

.field protected mPackageName:Ljava/lang/String;

.field protected mStopTimeout:J

.field protected mUid:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/android/server/wm/PreloadLifecycle;-><init>(ZLjava/lang/String;Lmiui/process/LifecycleConfig;)V

    .line 32
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Lmiui/process/LifecycleConfig;)V
    .locals 0
    .param p1, "ignoreMemory"    # Z
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "config"    # Lmiui/process/LifecycleConfig;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-boolean p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mIgnoreMemory:Z

    .line 37
    iput-object p2, p0, Lcom/android/server/wm/PreloadLifecycle;->mPackageName:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/android/server/wm/PreloadLifecycle;->mConfig:Lmiui/process/LifecycleConfig;

    .line 39
    invoke-virtual {p0, p3}, Lcom/android/server/wm/PreloadLifecycle;->initTimeout(Lmiui/process/LifecycleConfig;)V

    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .line 138
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 139
    return v0

    .line 142
    :cond_0
    move-object v1, p1

    check-cast v1, Lcom/android/server/wm/PreloadLifecycle;

    .line 143
    .local v1, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    iget v2, p0, Lcom/android/server/wm/PreloadLifecycle;->mUid:I

    iget v3, v1, Lcom/android/server/wm/PreloadLifecycle;->mUid:I

    if-eq v2, v3, :cond_1

    .line 144
    return v0

    .line 147
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/PreloadLifecycle;->mPackageName:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 148
    return v0

    .line 151
    :cond_2
    iget-object v3, v1, Lcom/android/server/wm/PreloadLifecycle;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 152
    return v0

    .line 155
    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method public getConfig()Lmiui/process/LifecycleConfig;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mConfig:Lmiui/process/LifecycleConfig;

    return-object v0
.end method

.method public getDisplayId()I
    .locals 1

    .line 129
    iget v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mDisplayId:I

    return v0
.end method

.method public getFreezeTimeout()J
    .locals 2

    .line 61
    iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mFreezeTimeout:J

    return-wide v0
.end method

.method public getKillTimeout()J
    .locals 2

    .line 69
    iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mKillTimeout:J

    return-wide v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPreloadNextTimeout()J
    .locals 4

    .line 49
    iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mFreezeTimeout:J

    const-wide/16 v2, 0xbb8

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPreloadSchedAffinity()[I
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mConfig:Lmiui/process/LifecycleConfig;

    invoke-virtual {v0}, Lmiui/process/LifecycleConfig;->getSchedAffinity()[I

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mConfig:Lmiui/process/LifecycleConfig;

    .line 94
    invoke-virtual {v0}, Lmiui/process/LifecycleConfig;->getSchedAffinity()[I

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mConfig:Lmiui/process/LifecycleConfig;

    invoke-virtual {v0}, Lmiui/process/LifecycleConfig;->getSchedAffinity()[I

    move-result-object v0

    return-object v0

    .line 95
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sPreloadSchedAffinity:[I

    return-object v0
.end method

.method public getPreloadType()I
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mConfig:Lmiui/process/LifecycleConfig;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lmiui/process/LifecycleConfig;->getType()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getStopTimeout()J
    .locals 2

    .line 53
    iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J

    return-wide v0
.end method

.method public getUid()I
    .locals 1

    .line 77
    iget v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mUid:I

    return v0
.end method

.method public initTimeout(Lmiui/process/LifecycleConfig;)V
    .locals 4
    .param p1, "config"    # Lmiui/process/LifecycleConfig;

    .line 43
    invoke-virtual {p1}, Lmiui/process/LifecycleConfig;->getStopTimeout()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J

    .line 44
    invoke-virtual {p1}, Lmiui/process/LifecycleConfig;->getKillTimeout()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mKillTimeout:J

    .line 45
    iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J

    const-wide/16 v2, 0x1f4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mFreezeTimeout:J

    .line 46
    return-void
.end method

.method public isAlreadyPreloaded()Z
    .locals 1

    .line 109
    iget-boolean v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mAlreadyPreloaded:Z

    return v0
.end method

.method public isIgnoreMemory()Z
    .locals 1

    .line 101
    iget-boolean v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mIgnoreMemory:Z

    return v0
.end method

.method public setAlreadyPreloaded(Z)V
    .locals 0
    .param p1, "alreadyPreloaded"    # Z

    .line 113
    iput-boolean p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mAlreadyPreloaded:Z

    .line 114
    return-void
.end method

.method public setConfig(Lmiui/process/LifecycleConfig;)V
    .locals 0
    .param p1, "mConfig"    # Lmiui/process/LifecycleConfig;

    .line 121
    iput-object p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mConfig:Lmiui/process/LifecycleConfig;

    .line 122
    return-void
.end method

.method public setDisplayId(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 133
    iput p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mDisplayId:I

    .line 134
    return-void
.end method

.method public setFreezeTimeout(J)V
    .locals 0
    .param p1, "mFreezeTimeout"    # J

    .line 65
    iput-wide p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mFreezeTimeout:J

    .line 66
    return-void
.end method

.method public setIgnoreMemory(Z)V
    .locals 0
    .param p1, "ignoreMemory"    # Z

    .line 105
    iput-boolean p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mIgnoreMemory:Z

    .line 106
    return-void
.end method

.method public setKillTimeout(J)V
    .locals 0
    .param p1, "mKillTimeout"    # J

    .line 73
    iput-wide p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mKillTimeout:J

    .line 74
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPackageName"    # Ljava/lang/String;

    .line 89
    iput-object p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mPackageName:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setStopTimeout(J)V
    .locals 0
    .param p1, "mStopTimeout"    # J

    .line 57
    iput-wide p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J

    .line 58
    return-void
.end method

.method public setUid(I)V
    .locals 0
    .param p1, "mUid"    # I

    .line 81
    iput p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mUid:I

    .line 82
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 160
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 161
    .local v0, "sb":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPackageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/PreloadLifecycle;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 162
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",mStopTimeout="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",mKillTimeout="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/wm/PreloadLifecycle;->mKillTimeout:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 165
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
