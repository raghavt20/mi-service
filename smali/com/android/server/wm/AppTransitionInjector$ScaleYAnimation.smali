.class Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;
.super Landroid/view/animation/Animation;
.source "AppTransitionInjector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/AppTransitionInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScaleYAnimation"
.end annotation


# instance fields
.field private mFromY:F

.field private mPivotY:F

.field private mToY:F


# direct methods
.method public constructor <init>(FF)V
    .locals 1
    .param p1, "fromY"    # F
    .param p2, "toY"    # F

    .line 1395
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1396
    iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mFromY:F

    .line 1397
    iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mToY:F

    .line 1398
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mPivotY:F

    .line 1399
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "fromY"    # F
    .param p2, "toY"    # F
    .param p3, "pivotY"    # F

    .line 1401
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1402
    iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mFromY:F

    .line 1403
    iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mToY:F

    .line 1404
    iput p3, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mPivotY:F

    .line 1405
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 6
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .line 1409
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1410
    .local v0, "sy":F
    invoke-virtual {p0}, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->getScaleFactor()F

    move-result v1

    .line 1412
    .local v1, "scale":F
    iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mFromY:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v4, v2, v3

    if-nez v4, :cond_0

    iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mToY:F

    cmpl-float v4, v4, v3

    if-eqz v4, :cond_1

    .line 1413
    :cond_0
    iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mToY:F

    sub-float/2addr v4, v2

    mul-float/2addr v4, p1

    add-float v0, v2, v4

    .line 1416
    :cond_1
    iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mPivotY:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-nez v2, :cond_2

    .line 1417
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    goto :goto_0

    .line 1419
    :cond_2
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    iget v5, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mPivotY:F

    mul-float/2addr v5, v1

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 1421
    :goto_0
    return-void
.end method
