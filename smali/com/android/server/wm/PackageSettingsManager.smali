.class public Lcom/android/server/wm/PackageSettingsManager;
.super Ljava/lang/Object;
.source "PackageSettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;,
        Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPolicy;,
        Lcom/android/server/wm/PackageSettingsManager$TaskStarter;,
        Lcom/android/server/wm/PackageSettingsManager$RequestPackageSettings;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PackageSettingsManager"


# instance fields
.field private final mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

.field private final mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

.field public final mDisplayCompatPackages:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

.field private mFullScreenLaunchOption:Lcom/android/server/wm/SafeActivityOptions;

.field mHasDisplayCutout:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmAtmService(Lcom/android/server/wm/PackageSettingsManager;)Lcom/android/server/wm/ActivityTaskManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/PackageSettingsManager;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAtmServiceImpl(Lcom/android/server/wm/PackageSettingsManager;)Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/PackageSettingsManager;->mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFullScreenLaunchOption(Lcom/android/server/wm/PackageSettingsManager;)Lcom/android/server/wm/SafeActivityOptions;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/PackageSettingsManager;->mFullScreenLaunchOption:Lcom/android/server/wm/SafeActivityOptions;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmFullScreenLaunchOption(Lcom/android/server/wm/PackageSettingsManager;Lcom/android/server/wm/SafeActivityOptions;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/PackageSettingsManager;->mFullScreenLaunchOption:Lcom/android/server/wm/SafeActivityOptions;

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V
    .locals 1
    .param p1, "atmServiceImpl"    # Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/android/server/wm/PackageSettingsManager;->mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    .line 30
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iput-object v0, p0, Lcom/android/server/wm/PackageSettingsManager;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 31
    new-instance v0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-direct {v0, p0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;-><init>(Lcom/android/server/wm/PackageSettingsManager;)V

    iput-object v0, p0, Lcom/android/server/wm/PackageSettingsManager;->mDisplayCompatPackages:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    .line 32
    return-void
.end method

.method static displayCompatPolicyToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "policy"    # I

    .line 35
    if-nez p0, :cond_0

    .line 36
    const-string v0, "NONE"

    return-object v0

    .line 39
    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 40
    const-string v0, "FORCED_RESIZEABLE_BY_USER_SETTING"

    return-object v0

    .line 43
    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    .line 44
    const-string v0, "FORCED_UNRESIZEABLE_BY_USER_SETTING"

    return-object v0

    .line 47
    :cond_2
    const/4 v0, 0x3

    if-ne p0, v0, :cond_3

    .line 48
    const-string v0, "FORCED_RESIZEABLE_BY_ALLOW_LIST"

    return-object v0

    .line 51
    :cond_3
    const/4 v0, 0x4

    if-ne p0, v0, :cond_4

    .line 52
    const-string v0, "FORCED_UNRESIZEABLE_BY_BLOCK_LIST"

    return-object v0

    .line 55
    :cond_4
    const/4 v0, 0x7

    if-ne p0, v0, :cond_5

    .line 56
    const-string v0, "CONTINUITY_RESTART_BY_BLOCK_LIST"

    return-object v0

    .line 59
    :cond_5
    const/16 v0, 0x8

    if-ne p0, v0, :cond_6

    .line 60
    const-string v0, "CONTINUITY_INTERCEPT_BY_BLOCK_LIST"

    return-object v0

    .line 63
    :cond_6
    const/16 v0, 0x9

    if-ne p0, v0, :cond_7

    .line 64
    const-string v0, "CONTINUITY_INTERCEPT_COMPONENT_BY_BLOCK_LIST"

    return-object v0

    .line 67
    :cond_7
    const/16 v0, 0xa

    if-ne p0, v0, :cond_8

    .line 68
    const-string v0, "CONTINUITY_INTERCEPT_BY_ALLOW_LIST"

    return-object v0

    .line 71
    :cond_8
    const/16 v0, 0xb

    if-ne p0, v0, :cond_9

    .line 72
    const-string v0, "CONTINUITY_INTERCEPT_COMPONENT_BY_ALLOW_LIST"

    return-object v0

    .line 75
    :cond_9
    const/4 v0, 0x6

    if-ne p0, v0, :cond_a

    .line 76
    const-string v0, "CONTINUITY_RELAUNCH_BY_BLOCK_LIST"

    return-object v0

    .line 79
    :cond_a
    const/4 v0, 0x5

    if-ne p0, v0, :cond_b

    .line 80
    const-string v0, "EXCLUDE_BY_META_DATA"

    return-object v0

    .line 83
    :cond_b
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isForcedResizeableDisplayCompatPolicy(I)Z
    .locals 2
    .param p0, "policy"    # I

    .line 87
    const/4 v0, 0x1

    .line 88
    .local v0, "isForceResize":Z
    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x1

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 93
    :cond_0
    return v0
.end method

.method public static isForcedUnresizeableDisplayCompatPolicy(I)Z
    .locals 1
    .param p0, "policy"    # I

    .line 97
    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x7

    if-eq p0, v0, :cond_1

    const/16 v0, 0x8

    if-eq p0, v0, :cond_1

    const/16 v0, 0x9

    if-eq p0, v0, :cond_1

    const/16 v0, 0xa

    if-eq p0, v0, :cond_1

    const/16 v0, 0xb

    if-eq p0, v0, :cond_1

    const/4 v0, 0x6

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method static synthetic lambda$killAndRestartTask$0(ILjava/lang/String;Lcom/android/server/wm/Task;)Z
    .locals 3
    .param p0, "userId"    # I
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "t"    # Lcom/android/server/wm/Task;

    .line 136
    iget v0, p2, Lcom/android/server/wm/Task;->mUserId:I

    const/4 v1, 0x0

    if-eq v0, p0, :cond_0

    .line 137
    return v1

    .line 139
    :cond_0
    iget-object v0, p2, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-nez v0, :cond_1

    .line 140
    return v1

    .line 142
    :cond_1
    iget-object v0, p2, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "pkgName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 144
    return v1

    .line 146
    :cond_2
    const/4 v1, 0x1

    return v1
.end method

.method static synthetic lambda$killAndRestartTask$1(Lcom/android/server/wm/PackageSettingsManager$TaskStarter;)V
    .locals 0
    .param p0, "taskStarter"    # Lcom/android/server/wm/PackageSettingsManager$TaskStarter;

    .line 154
    invoke-static {p0}, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->-$$Nest$mrestartTask(Lcom/android/server/wm/PackageSettingsManager$TaskStarter;)V

    .line 155
    return-void
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 108
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    const-string v0, "MiuiAppSizeCompatMode not enabled"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 110
    return-void

    .line 113
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "innerPrefix":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "PACKAGE SETTINGS MANAGER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 116
    iget-boolean v1, p0, Lcom/android/server/wm/PackageSettingsManager;->mHasDisplayCutout:Z

    if-eqz v1, :cond_1

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mHasDisplayCutout=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 119
    :cond_1
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 120
    iget-object v1, p0, Lcom/android/server/wm/PackageSettingsManager;->mDisplayCompatPackages:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-static {v1, p1, v0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->-$$Nest$mdump(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 122
    return-void
.end method

.method killAndRestartTask(ILjava/lang/String;Landroid/app/ActivityOptions;ILjava/lang/String;)V
    .locals 9
    .param p1, "restartTaskId"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "activityOptions"    # Landroid/app/ActivityOptions;
    .param p4, "userId"    # I
    .param p5, "reason"    # Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 128
    :try_start_0
    invoke-static {}, Lcom/android/server/wm/WindowManagerService;->boostPriorityForLockedSection()V

    .line 129
    const/4 v1, -0x1

    const/4 v2, 0x1

    if-eq p1, v1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/android/server/wm/PackageSettingsManager;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v1, p1, v2}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;

    move-result-object v5

    .line 131
    .local v5, "targetTask":Lcom/android/server/wm/Task;
    new-instance v1, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;

    const/4 v8, 0x0

    move-object v3, v1

    move-object v4, p0

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v3 .. v8}, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;-><init>(Lcom/android/server/wm/PackageSettingsManager;Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;Ljava/lang/String;Lcom/android/server/wm/PackageSettingsManager$TaskStarter-IA;)V

    .line 132
    .local v1, "taskStarter":Lcom/android/server/wm/PackageSettingsManager$TaskStarter;
    move-object v3, v5

    .line 133
    .end local v5    # "targetTask":Lcom/android/server/wm/Task;
    .local v3, "task":Lcom/android/server/wm/Task;
    goto :goto_0

    .line 134
    .end local v1    # "taskStarter":Lcom/android/server/wm/PackageSettingsManager$TaskStarter;
    .end local v3    # "task":Lcom/android/server/wm/Task;
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/PackageSettingsManager;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 135
    .local v1, "rootWindowContainer":Lcom/android/server/wm/RootWindowContainer;
    new-instance v3, Lcom/android/server/wm/PackageSettingsManager$$ExternalSyntheticLambda0;

    invoke-direct {v3, p4, p2}, Lcom/android/server/wm/PackageSettingsManager$$ExternalSyntheticLambda0;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/android/server/wm/RootWindowContainer;->getTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;

    move-result-object v3

    .line 148
    .restart local v3    # "task":Lcom/android/server/wm/Task;
    const/4 v4, 0x0

    move-object v1, v4

    .line 150
    .local v1, "taskStarter":Lcom/android/server/wm/PackageSettingsManager$TaskStarter;
    :goto_0
    if-eqz v3, :cond_1

    .line 151
    iget-object v4, p0, Lcom/android/server/wm/PackageSettingsManager;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v2, v5, p5}, Lcom/android/server/wm/ActivityTaskSupervisor;->removeTask(Lcom/android/server/wm/Task;ZZLjava/lang/String;)V

    .line 152
    if-eqz v1, :cond_1

    .line 153
    iget-object v2, p0, Lcom/android/server/wm/PackageSettingsManager;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mH:Lcom/android/server/wm/ActivityTaskManagerService$H;

    new-instance v4, Lcom/android/server/wm/PackageSettingsManager$$ExternalSyntheticLambda1;

    invoke-direct {v4, v1}, Lcom/android/server/wm/PackageSettingsManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/PackageSettingsManager$TaskStarter;)V

    invoke-virtual {v2, v4}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 158
    :cond_1
    invoke-static {}, Lcom/android/server/wm/WindowManagerService;->resetPriorityAfterLockedSection()V

    .line 159
    .end local v1    # "taskStarter":Lcom/android/server/wm/PackageSettingsManager$TaskStarter;
    .end local v3    # "task":Lcom/android/server/wm/Task;
    monitor-exit v0

    .line 160
    return-void

    .line 159
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
