.class public Lcom/android/server/wm/MiuiFreeFormManagerService;
.super Lmiui/app/IMiuiFreeFormManager$Stub;
.source "MiuiFreeFormManagerService.java"

# interfaces
.implements Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiFreeFormManagerService"


# instance fields
.field mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mAppBehindHome:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mApplicationUsedInFreeform:Ljava/lang/String;

.field private mAutoLayoutModeOn:Z

.field private final mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lmiui/app/IFreeformCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mCorrectScaleList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field final mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/wm/MiuiFreeFormActivityStack;",
            ">;"
        }
    .end annotation
.end field

.field mFreeFormCameraStrategy:Lcom/android/server/wm/MiuiFreeFormCameraStrategy;

.field mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

.field mFreeFormStackDisplayStrategy:Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;

.field private mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

.field private final mFreeformModeControlDeath:Landroid/os/IBinder$DeathRecipient;

.field mHandler:Landroid/os/Handler;

.field private mImeHeight:I

.field private mIsImeShowing:Z


# direct methods
.method public static synthetic $r8$lambda$7izAiEB8PoL1-S1Oje3FKpXFrk0(Lcom/android/server/wm/MiuiFreeFormManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$new$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$Bqquvlh8rG7WxT8sZTzwMZRDNm0(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZLcom/android/server/wm/Task;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getBottomFreeformTask$3(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZLcom/android/server/wm/Task;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$EJbMKE5acRWEMzCj_UhtQ1OkWJ8(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$dispatchFreeFormStackModeChanged$6(Lcom/android/server/wm/MiuiFreeFormActivityStack;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$Kljm1N4FZGtUch9ixmNtvRwLNZ8(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$fromFreefromToMini$10(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$S2tFuE_mJ0W3BRmaMIvImHHlQkk(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$fullscreenFreeformTask$15(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$d-_PaSwNB2ZaD72GwvymMr8JoPs(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZLcom/android/server/wm/Task;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getLastFreeformTask$1(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZLcom/android/server/wm/Task;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$fpM-xuREdLTfKdDwCA3Lccpjlxg(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$unPinFloatingWindowForActive$7(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$mm_AKnhjp-wY25TNSTGT0CXmvVk(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;Lcom/android/server/wm/Task;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getBottomGameFreeFormActivityStack$4(Lcom/android/server/wm/MiuiFreeFormActivityStack;Lcom/android/server/wm/Task;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$oQY6W05var7hgiZQH-5tB1XVpfM(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/Task;Lcom/android/server/wm/Task;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getLastVisibleFreeformTask$2(Lcom/android/server/wm/Task;Lcom/android/server/wm/Task;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$pLSudP9ZOkzthgsGU9NkQfYNPic(Lcom/android/server/wm/MiuiFreeFormManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$freeformKillAll$17()V

    return-void
.end method

.method public static synthetic $r8$lambda$rPHaFgGcc3Zh5DqOMcZ3HDywkvQ(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$freeformFullscreenTask$16(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$sJj-lpr-1P4fBYw5-P2lZ7CzPxc(Lcom/android/server/wm/MiuiFreeFormManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$allFreeformFormFreefromToMini$9()V

    return-void
.end method

.method public static synthetic $r8$lambda$tDwk7sHxedwGV75PTko6X-Jm7Uo(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/Task;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$setRequestedOrientation$12(Lcom/android/server/wm/Task;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$tskTCCcQhUpP3JTTcbpkrCJa26c(Lcom/android/server/wm/MiuiFreeFormManagerService;ILcom/android/server/wm/Task;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getFreeFormStackToAvoid$13(ILcom/android/server/wm/Task;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$vNiR9-28tAymv8LuB53QQIzleZk(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$onExitFreeform$11(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$xvAbAMPU7COXn2aoWqImy5aHpHk(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$removeFreeformParamsForAutoLayout$14(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$zvzJM_7O52NQQydOXhEo7h74glE(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$restoreMiniToFreeformMode$8(I)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 104
    invoke-direct {p0}, Lmiui/app/IMiuiFreeFormManager$Stub;-><init>()V

    .line 106
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    .line 107
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAutoLayoutModeOn:Z

    .line 118
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    .line 119
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    .line 120
    new-instance v0, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControlDeath:Landroid/os/IBinder$DeathRecipient;

    return-void
.end method

.method private applockMatched(Lcom/android/server/wm/Task;Ljava/lang/String;)Z
    .locals 7
    .param p1, "lastFreeformTask"    # Lcom/android/server/wm/Task;
    .param p2, "pkg"    # Ljava/lang/String;

    .line 777
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 778
    const/4 v1, 0x0

    .line 779
    .local v1, "lastFreeformActivity":Lcom/android/server/wm/ActivityRecord;
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v2

    .line 780
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {p1, v3, v3}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    move-object v1, v4

    .line 781
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 782
    if-eqz v1, :cond_0

    .line 783
    iget-object v2, v1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 784
    .local v2, "lastFreeformComponentName":Landroid/content/ComponentName;
    :goto_0
    if-eqz v2, :cond_1

    const-string v4, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    .line 785
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v5

    .line 784
    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/android/server/wm/Task;->behindAppLockPkg:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/android/server/wm/Task;->behindAppLockPkg:Ljava/lang/String;

    .line 786
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 787
    const-string v4, "MiuiFreeFormManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "unlock: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to addingTaskPkg:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v4, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 788
    return v3

    .line 781
    .end local v2    # "lastFreeformComponentName":Landroid/content/ComponentName;
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 791
    .end local v1    # "lastFreeformActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_1
    return v0
.end method

.method private centerRect(Landroid/graphics/Rect;Landroid/graphics/Rect;ZI)V
    .locals 3
    .param p1, "outBounds"    # Landroid/graphics/Rect;
    .param p2, "accessibleArea"    # Landroid/graphics/Rect;
    .param p3, "isDisplayLandscape"    # Z
    .param p4, "startPoint"    # I

    .line 1663
    const/4 v0, 0x0

    if-eqz p3, :cond_0

    .line 1664
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 1665
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, p4, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    goto :goto_0

    .line 1667
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 1668
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1, v0, p4}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1670
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "centerRect to ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreeFormManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1671
    return-void
.end method

.method private detachFreeformModeControl()V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    if-nez v0, :cond_0

    return-void

    .line 148
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    .line 149
    return-void
.end method

.method private getBottomFreeformTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)Lcom/android/server/wm/Task;
    .locals 4
    .param p1, "addingTask"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .param p2, "findFreeformMode"    # Z
    .param p3, "findMini"    # Z

    .line 835
    const-string v0, "MiuiFreeFormManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getBottomFreeformTask addingTask = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " findFreeformMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " findMini: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 838
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 839
    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v1

    .line 840
    .local v1, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda12;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda12;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 854
    .local v2, "replacedTask":Lcom/android/server/wm/Task;
    monitor-exit v0

    return-object v2

    .line 855
    .end local v1    # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    .end local v2    # "replacedTask":Lcom/android/server/wm/Task;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getFreeformTaskHasAppLock(Lcom/android/server/wm/Task;Ljava/lang/String;)Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .locals 5
    .param p1, "addingTask"    # Lcom/android/server/wm/Task;
    .param p2, "pkg"    # Ljava/lang/String;

    .line 756
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 757
    return-object v0

    .line 759
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getLastVisibleFreeformTask(Lcom/android/server/wm/Task;)Lcom/android/server/wm/Task;

    move-result-object v1

    .line 760
    .local v1, "lastVisibleFreeformTask":Lcom/android/server/wm/Task;
    if-eqz v1, :cond_1

    invoke-direct {p0, v1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->applockMatched(Lcom/android/server/wm/Task;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 761
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 762
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get freeformTask with applock: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "MiuiFreeFormManagerService"

    invoke-static {v3, v4, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 763
    return-object v0

    .line 765
    .end local v0    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_1
    return-object v0
.end method

.method private getLastFreeformTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)Lcom/android/server/wm/Task;
    .locals 4
    .param p1, "addingTask"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .param p2, "findFreeformMode"    # Z
    .param p3, "findMini"    # Z

    .line 796
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 797
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 798
    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v1

    .line 799
    .local v1, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda6;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 810
    .local v2, "lastTask":Lcom/android/server/wm/Task;
    monitor-exit v0

    return-object v2

    .line 811
    .end local v1    # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    .end local v2    # "lastTask":Lcom/android/server/wm/Task;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getLastVisibleFreeformTask(Lcom/android/server/wm/Task;)Lcom/android/server/wm/Task;
    .locals 4
    .param p1, "addingTask"    # Lcom/android/server/wm/Task;

    .line 815
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 816
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 817
    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v1

    .line 818
    .local v1, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda9;

    invoke-direct {v2, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda9;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/Task;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 829
    .local v2, "lastVisibleTask":Lcom/android/server/wm/Task;
    monitor-exit v0

    return-object v2

    .line 830
    .end local v1    # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    .end local v2    # "lastVisibleTask":Lcom/android/server/wm/Task;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getReplacePinModeTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/Task;
    .locals 7
    .param p1, "addingTask"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 859
    const/4 v0, 0x0

    .line 860
    .local v0, "replacedPinTask":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 861
    .local v2, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eq v2, p1, :cond_1

    .line 862
    if-eqz v0, :cond_0

    iget-wide v3, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->pinActiveTime:J

    iget-wide v5, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->pinActiveTime:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    .line 863
    :cond_0
    move-object v0, v2

    .line 866
    .end local v2    # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_1
    goto :goto_0

    .line 867
    :cond_2
    if-nez v0, :cond_3

    const/4 v1, 0x0

    return-object v1

    .line 868
    :cond_3
    iget-object v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    return-object v1
.end method

.method private isCandidateForAutoLayout(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z
    .locals 1
    .param p1, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1655
    if-eqz p1, :cond_0

    .line 1657
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 1658
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1659
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1655
    :goto_0
    return v0
.end method

.method private isHomeTopTask()Z
    .locals 3

    .line 442
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v0

    .line 443
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 444
    .local v0, "currentFullRootTask":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 445
    return v1

    .line 447
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method private isLaunchForSplitMode(Lcom/android/server/wm/Task;)Z
    .locals 5
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 1939
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1940
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 1941
    .local v1, "rootTask":Lcom/android/server/wm/Task;
    if-nez v1, :cond_1

    return v0

    .line 1944
    :cond_1
    iget-boolean v2, v1, Lcom/android/server/wm/Task;->mSoScRoot:Z

    const/4 v3, 0x1

    if-nez v2, :cond_3

    iget-boolean v2, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v2, :cond_2

    .line 1945
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 1946
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    .line 1947
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v2

    const/4 v4, 0x6

    if-ne v2, v4, :cond_2

    goto :goto_0

    .line 1950
    :cond_2
    return v0

    .line 1948
    :cond_3
    :goto_0
    return v3
.end method

.method private synthetic lambda$allFreeformFormFreefromToMini$9()V
    .locals 1

    .line 1169
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0}, Lmiui/app/IMiuiFreeformModeControl;->allFreeformFormFreefromToMini()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1172
    goto :goto_0

    .line 1170
    :catch_0
    move-exception v0

    .line 1171
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1173
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method static synthetic lambda$dispatchFreeFormStackModeChanged$5(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;Lcom/android/server/wm/WindowState;)V
    .locals 0
    .param p0, "action"    # I
    .param p1, "freeFormASInfo"    # Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    .param p2, "win"    # Lcom/android/server/wm/WindowState;

    .line 1081
    invoke-virtual {p2, p0, p1}, Lcom/android/server/wm/WindowState;->dispatchFreeFormStackModeChanged(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V

    .line 1082
    return-void
.end method

.method private synthetic lambda$dispatchFreeFormStackModeChanged$6(Lcom/android/server/wm/MiuiFreeFormActivityStack;I)V
    .locals 7
    .param p1, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .param p2, "action"    # I

    .line 1060
    if-nez p1, :cond_0

    return-void

    .line 1061
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v0

    .line 1062
    .local v0, "freeFormASInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    const-string v1, "MiuiFreeFormManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dispatchFreeFormStackModeChanged freeFormASInfo = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3, v1, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 1063
    iget-wide v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->timestamp:J

    const-wide/16 v4, 0x0

    cmp-long v1, v1, v4

    if-nez v1, :cond_1

    .line 1064
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setFreeformTimestamp(J)V

    .line 1066
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    monitor-enter v1

    .line 1067
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 1068
    .local v2, "callbacksCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_2

    .line 1069
    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lmiui/app/IFreeformCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1071
    .local v5, "freeformCallback":Lmiui/app/IFreeformCallback;
    :try_start_1
    invoke-interface {v5, p2, v0}, Lmiui/app/IFreeformCallback;->dispatchFreeFormStackModeChanged(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1074
    goto :goto_1

    .line 1072
    :catch_0
    move-exception v6

    .line 1073
    .local v6, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 1068
    .end local v5    # "freeformCallback":Lmiui/app/IFreeformCallback;
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1076
    .end local v4    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1077
    .end local v2    # "callbacksCount":I
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1078
    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v1, :cond_3

    .line 1079
    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v1, v1, Lcom/android/server/wm/Task;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 1080
    :try_start_3
    iget-object v2, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    new-instance v4, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda11;

    invoke-direct {v4, p2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda11;-><init>(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V

    invoke-virtual {v2, v4, v3}, Lcom/android/server/wm/Task;->forAllWindows(Ljava/util/function/Consumer;Z)V

    .line 1083
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1084
    if-nez p2, :cond_3

    .line 1085
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverDestroyItemToAlipay(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    goto :goto_2

    .line 1083
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 1088
    :cond_3
    :goto_2
    return-void

    .line 1077
    :catchall_1
    move-exception v2

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v2
.end method

.method private synthetic lambda$freeformFullscreenTask$16(I)V
    .locals 1
    .param p1, "taskId"    # I

    .line 1835
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->freeformFullscreenTask(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1838
    goto :goto_0

    .line 1836
    :catch_0
    move-exception v0

    .line 1837
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1839
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$freeformKillAll$17()V
    .locals 1

    .line 1846
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0}, Lmiui/app/IMiuiFreeformModeControl;->freeformKillAll()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1849
    goto :goto_0

    .line 1847
    :catch_0
    move-exception v0

    .line 1848
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1850
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$fromFreefromToMini$10(I)V
    .locals 1
    .param p1, "taskId"    # I

    .line 1180
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->fromFreefromToMini(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1183
    goto :goto_0

    .line 1181
    :catch_0
    move-exception v0

    .line 1182
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1184
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$fullscreenFreeformTask$15(I)V
    .locals 1
    .param p1, "taskId"    # I

    .line 1824
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->fullscreenFreeformTask(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1827
    goto :goto_0

    .line 1825
    :catch_0
    move-exception v0

    .line 1826
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1828
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$getBottomFreeformTask$3(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZLcom/android/server/wm/Task;)Z
    .locals 3
    .param p1, "addingTask"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .param p2, "findFreeformMode"    # Z
    .param p3, "findMini"    # Z
    .param p4, "t"    # Lcom/android/server/wm/Task;

    .line 841
    invoke-virtual {p4}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 842
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p4}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 843
    .local v0, "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getBottomFreeformTask formActivityStack = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFreeFormManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    if-eqz v0, :cond_2

    if-eq v0, p1, :cond_2

    .line 845
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    .line 846
    invoke-virtual {p4}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 847
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p2, :cond_1

    .line 848
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p3, :cond_2

    .line 849
    :cond_1
    const/4 v1, 0x1

    return v1

    .line 852
    .end local v0    # "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private synthetic lambda$getBottomGameFreeFormActivityStack$4(Lcom/android/server/wm/MiuiFreeFormActivityStack;Lcom/android/server/wm/Task;)Z
    .locals 4
    .param p1, "addingStack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .param p2, "t"    # Lcom/android/server/wm/Task;

    .line 893
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 894
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 895
    .local v0, "curFreeFormActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    .line 896
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/util/MiuiMultiWindowAdapter;->isInTopGameList(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 897
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    .line 898
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 899
    const/4 v1, 0x1

    return v1

    .line 901
    :cond_0
    return v1

    .line 903
    .end local v0    # "curFreeFormActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_1
    return v1
.end method

.method private synthetic lambda$getFreeFormStackToAvoid$13(ILcom/android/server/wm/Task;)Z
    .locals 5
    .param p1, "displayId"    # I
    .param p2, "t"    # Lcom/android/server/wm/Task;

    .line 1566
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 1567
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1568
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v3, v3, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v3, :cond_0

    sget-object v3, Landroid/util/MiuiMultiWindowAdapter;->NOT_AVOID_LAUNCH_OTHER_FREEFORM_LIST:Ljava/util/List;

    iget-object v4, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v4, v4, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    .line 1571
    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    .line 1570
    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v3, v3, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    .line 1573
    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    .line 1572
    const-string v4, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    move v3, v1

    .line 1574
    .local v3, "notAvoid":Z
    :goto_0
    if-eqz v0, :cond_2

    const/4 v4, -0x1

    if-eq v4, p1, :cond_1

    iget-object v4, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v4}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v4

    if-ne v4, p1, :cond_2

    .line 1576
    :cond_1
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v4

    if-nez v4, :cond_2

    if-nez v3, :cond_2

    .line 1577
    return v2

    .line 1579
    .end local v0    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v3    # "notAvoid":Z
    :cond_2
    return v1
.end method

.method private synthetic lambda$getLastFreeformTask$1(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZLcom/android/server/wm/Task;)Z
    .locals 2
    .param p1, "addingTask"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .param p2, "findFreeformMode"    # Z
    .param p3, "findMini"    # Z
    .param p4, "t"    # Lcom/android/server/wm/Task;

    .line 800
    invoke-virtual {p4}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 801
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p4}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 802
    .local v0, "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_2

    if-eq v0, p1, :cond_2

    .line 803
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p2, :cond_1

    .line 804
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p3, :cond_2

    .line 805
    :cond_1
    const/4 v1, 0x1

    return v1

    .line 808
    .end local v0    # "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private synthetic lambda$getLastVisibleFreeformTask$2(Lcom/android/server/wm/Task;Lcom/android/server/wm/Task;)Z
    .locals 3
    .param p1, "addingTask"    # Lcom/android/server/wm/Task;
    .param p2, "t"    # Lcom/android/server/wm/Task;

    .line 819
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 820
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 821
    .local v0, "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_0

    .line 822
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isCandidateForAutoLayout(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 823
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I

    if-eq v1, v2, :cond_0

    .line 824
    const/4 v1, 0x1

    return v1

    .line 827
    .end local v0    # "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private synthetic lambda$new$0()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 122
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->detachFreeformModeControl()V

    .line 124
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->clearAllFreeFormForProcessReboot()V

    .line 125
    monitor-exit v0

    .line 126
    return-void

    .line 125
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private synthetic lambda$onExitFreeform$11(I)V
    .locals 1
    .param p1, "taskId"    # I

    .line 1191
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->onExitFreeform(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1194
    goto :goto_0

    .line 1192
    :catch_0
    move-exception v0

    .line 1193
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1195
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$removeFreeformParamsForAutoLayout$14(I)V
    .locals 1
    .param p1, "taskId"    # I

    .line 1677
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->removeFreeformParamsForAutoLayout(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1680
    goto :goto_0

    .line 1678
    :catch_0
    move-exception v0

    .line 1679
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1681
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$restoreMiniToFreeformMode$8(I)V
    .locals 1
    .param p1, "taskId"    # I

    .line 1158
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->restoreMiniToFreeformMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1161
    goto :goto_0

    .line 1159
    :catch_0
    move-exception v0

    .line 1160
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1162
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$setRequestedOrientation$12(Lcom/android/server/wm/Task;I)V
    .locals 2
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "requestedOrientation"    # I

    .line 1208
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    invoke-interface {v0, v1, p2}, Lmiui/app/IMiuiFreeformModeControl;->setRequestedOrientation(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1211
    goto :goto_0

    .line 1209
    :catch_0
    move-exception v0

    .line 1210
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1212
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$unPinFloatingWindowForActive$7(I)V
    .locals 1
    .param p1, "taskId"    # I

    .line 1147
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->unPinFloatingWindowForActive(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150
    goto :goto_0

    .line 1148
    :catch_0
    move-exception v0

    .line 1149
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1151
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method static synthetic lambda$unSupportedFreeformInDesktop$18(ILcom/android/server/wm/Task;)Z
    .locals 1
    .param p0, "taskId"    # I
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 2193
    iget v0, p1, Lcom/android/server/wm/Task;->mTaskId:I

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static logd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "string"    # Ljava/lang/String;

    .line 1361
    const/4 v0, 0x1

    invoke-static {v0, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 1362
    return-void
.end method

.method static logd(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "enable"    # Z
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "string"    # Ljava/lang/String;

    .line 1365
    sget-boolean v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->DEBUG:Z

    if-nez v0, :cond_0

    if-eqz p0, :cond_1

    .line 1366
    :cond_0
    invoke-static {p1, p2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    :cond_1
    return-void
.end method

.method private notifyImeVisibilityChanged(ZIZ)V
    .locals 3
    .param p1, "imeVisible"    # Z
    .param p2, "imeHeight"    # I
    .param p3, "adjustedForRotation"    # Z

    .line 1216
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    if-eqz v0, :cond_0

    .line 1218
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Lmiui/app/IMiuiFreeformModeControl;->onImeVisibilityChanged(ZIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221
    goto :goto_0

    .line 1219
    :catch_0
    move-exception v0

    .line 1220
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MiuiFreeFormManagerService"

    const-string v2, "Error delivering bounds changed event."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1223
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method

.method private onMiuiFreeFormStasckAdded(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 3
    .param p1, "stack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 577
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormStackDisplayStrategy:Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v0, v1, v2, p1}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->onMiuiFreeFormStasckAdded(Ljava/util/concurrent/ConcurrentHashMap;Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 580
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setCameraRotationIfNeeded()V

    .line 581
    return-void
.end method

.method private removeFreeformParamsForAutoLayout(I)V
    .locals 2
    .param p1, "taskId"    # I

    .line 1674
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1675
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda16;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda16;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1682
    return-void
.end method

.method private scaleDownIfNeeded(FLandroid/graphics/Rect;Landroid/graphics/Rect;)F
    .locals 6
    .param p1, "scale"    # F
    .param p2, "bounds"    # Landroid/graphics/Rect;
    .param p3, "stableBounds"    # Landroid/graphics/Rect;

    .line 1685
    move v0, p1

    .line 1686
    .local v0, "finalScale":F
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    .line 1687
    .local v1, "currentVisualWidth":F
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    .line 1688
    .local v2, "currentVisualHeight":F
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v3, v1, v3

    if-gtz v3, :cond_0

    .line 1689
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    .line 1690
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v1

    .line 1691
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v2

    .line 1690
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 1692
    .local v3, "scaleDown":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "scaleDownIfNeeded scaleDown: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " currentVisualWidth: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " currentVisualHeight: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " stableBounds: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " scale: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiFreeFormManagerService"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1695
    mul-float v0, p1, v3

    .line 1697
    .end local v3    # "scaleDown":F
    :cond_1
    return v0
.end method

.method private showFreeformIfNeeded(I)V
    .locals 3
    .param p1, "taskId"    # I

    .line 451
    const-string v0, "MiuiFreeFormManagerService"

    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 452
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    if-eqz v1, :cond_1

    .line 454
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "show hidden task  ...."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isHomeTopTask()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 456
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v1, p1}, Lmiui/app/IMiuiFreeformModeControl;->showFreeform(I)V

    goto :goto_0

    .line 458
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v1, p1}, Lmiui/app/IMiuiFreeformModeControl;->fromFulltoFreeform(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 462
    :goto_0
    goto :goto_1

    .line 460
    :catch_0
    move-exception v1

    .line 461
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Error showFreeformIfNeeded."

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 465
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_1
    return-void
.end method

.method private showPropotionalFreeformToast(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 5
    .param p1, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 547
    if-eqz p1, :cond_2

    iget-boolean v0, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 548
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mApplicationUsedInFreeform:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 549
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 550
    .local v0, "applications":[Ljava/lang/String;
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 551
    .local v2, "applicationsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 552
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mApplicationUsedInFreeform:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mApplicationUsedInFreeform:Ljava/lang/String;

    .line 553
    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->showPropotionalFreeformToast(Ljava/lang/String;)V

    .line 555
    .end local v0    # "applications":[Ljava/lang/String;
    .end local v2    # "applicationsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    goto :goto_0

    .line 556
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mApplicationUsedInFreeform:Ljava/lang/String;

    .line 557
    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->showPropotionalFreeformToast(Ljava/lang/String;)V

    .line 560
    :cond_2
    :goto_0
    return-void
.end method

.method private unlockingAppLock(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z
    .locals 2
    .param p1, "addingTask"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 769
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getLastFreeformTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 770
    .local v0, "lastFreeformTask":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 773
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->applockMatched(Lcom/android/server/wm/Task;Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 771
    :cond_1
    :goto_0
    return v1
.end method


# virtual methods
.method public activeFreeformTask(I)V
    .locals 3
    .param p1, "taskId"    # I

    .line 1807
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1808
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_1

    .line 1810
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 1811
    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V

    .line 1812
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->unPinFloatingWindowForActive(I)V

    goto :goto_0

    .line 1813
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1814
    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V

    .line 1815
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->restoreMiniToFreeformMode(I)V

    .line 1818
    :cond_1
    :goto_0
    return-void
.end method

.method public activeFreeformTaskIfNeed(ILandroid/app/ActivityOptions;Lcom/android/server/wm/RootWindowContainer;)Z
    .locals 8
    .param p1, "taskId"    # I
    .param p2, "activityOptions"    # Landroid/app/ActivityOptions;
    .param p3, "rootWindowContainer"    # Lcom/android/server/wm/RootWindowContainer;

    .line 1893
    const/4 v0, 0x0

    .line 1894
    .local v0, "activeFreeformTask":Z
    const/4 v1, 0x0

    .line 1895
    .local v1, "startForFullscreen":Z
    const/4 v2, 0x0

    invoke-virtual {p3, p1, v2}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 1896
    .local v2, "preFindTask":Lcom/android/server/wm/Task;
    const/4 v3, 0x0

    .line 1897
    .local v3, "startForSosc":Z
    if-eqz p2, :cond_0

    .line 1898
    invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchRootTask()Landroid/window/WindowContainerToken;

    move-result-object v4

    invoke-static {v4}, Lcom/android/server/wm/Task;->fromWindowContainerToken(Landroid/window/WindowContainerToken;)Lcom/android/server/wm/Task;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isLaunchForSplitMode(Lcom/android/server/wm/Task;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1899
    const/4 v3, 0x1

    .line 1901
    :cond_0
    const/4 v4, 0x1

    if-eqz p2, :cond_1

    .line 1902
    invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v5

    if-ne v5, v4, :cond_1

    .line 1903
    const/4 v1, 0x1

    .line 1906
    :cond_1
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v5

    if-eqz v5, :cond_4

    if-nez v3, :cond_4

    .line 1907
    const-string v5, "MiuiFreeFormManagerService"

    if-eqz v1, :cond_2

    .line 1908
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fullscreenFreeformTask(I)V

    .line 1909
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fullscreenFreeformTask "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from freeform to fullscreen for options.getLaunchWindowingMode() == WINDOWING_MODE_FULLSCREEN"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1911
    return v4

    .line 1913
    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v4

    .line 1914
    .local v4, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1915
    :cond_3
    const/4 v0, 0x1

    .line 1916
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->activeFreeformTask(I)V

    .line 1917
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "activeFreeformTaskIfNeed taskId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " activityOptions: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " startForSosc: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1921
    .end local v4    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_4
    return v0
.end method

.method public activeOrFullscreenFreeformTaskIfNeed(Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 7
    .param p1, "reusedTask"    # Lcom/android/server/wm/Task;
    .param p2, "options"    # Landroid/app/ActivityOptions;
    .param p3, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "startingAr"    # Lcom/android/server/wm/ActivityRecord;

    .line 1855
    const/4 v0, 0x0

    .line 1856
    .local v0, "startForSosc":Z
    const/4 v1, 0x0

    .line 1857
    .local v1, "startForFullscreen":Z
    const/4 v2, 0x0

    .line 1858
    .local v2, "startDefaultAr":Z
    if-eqz p2, :cond_0

    .line 1859
    invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchRootTask()Landroid/window/WindowContainerToken;

    move-result-object v3

    invoke-static {v3}, Lcom/android/server/wm/Task;->fromWindowContainerToken(Landroid/window/WindowContainerToken;)Lcom/android/server/wm/Task;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isLaunchForSplitMode(Lcom/android/server/wm/Task;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1860
    const/4 v0, 0x1

    .line 1862
    :cond_0
    const/4 v3, 0x1

    if-eqz p2, :cond_1

    .line 1863
    invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v4

    if-ne v4, v3, :cond_1

    .line 1864
    const/4 v1, 0x1

    .line 1867
    :cond_1
    const-string v4, "MiuiFreeFormManagerService"

    if-eqz p4, :cond_2

    iget-object v5, p4, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v5, :cond_2

    .line 1868
    iget-object v5, p4, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-static {v5}, Lcom/android/server/wm/ActivityRecord;->isMainIntent(Landroid/content/Intent;)Z

    move-result v2

    .line 1869
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "activeOrFullscreenFreeformTaskIfNeed startDefaultAr:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1872
    :cond_2
    if-nez v0, :cond_6

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v5

    if-eqz v5, :cond_6

    if-eqz p3, :cond_3

    .line 1873
    invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v5

    if-nez v5, :cond_6

    .line 1874
    :cond_3
    const/high16 v5, 0x100000

    iget v6, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v5, v6}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(II)V

    .line 1875
    iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->inPinMode(I)Z

    move-result v5

    if-nez v5, :cond_4

    iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isInMiniFreeFormMode(I)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    if-nez v1, :cond_5

    if-eqz v2, :cond_5

    .line 1877
    iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->activeFreeformTask(I)V

    .line 1878
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "activFreeformTask "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is in pinmode or minifreeform, so just active it!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1880
    return v3

    .line 1881
    :cond_5
    if-eqz v1, :cond_6

    .line 1882
    iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fullscreenFreeformTask(I)V

    .line 1883
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fullscreenFreeformTask "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from freeform to fullscreen for options.getLaunchWindowingMode() == WINDOWING_MODE_FULLSCREEN"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1885
    return v3

    .line 1888
    :cond_6
    const/4 v3, 0x0

    return v3
.end method

.method public addFreeFormActivityStack(Lcom/android/server/wm/Task;)V
    .locals 2
    .param p1, "rootTask"    # Lcom/android/server/wm/Task;

    .line 380
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;ILjava/lang/String;)V

    .line 381
    return-void
.end method

.method public addFreeFormActivityStack(Lcom/android/server/wm/Task;I)V
    .locals 1
    .param p1, "rootTask"    # Lcom/android/server/wm/Task;
    .param p2, "miuiFreeFromWindowMode"    # I

    .line 388
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;ILjava/lang/String;)V

    .line 389
    return-void
.end method

.method public addFreeFormActivityStack(Lcom/android/server/wm/Task;ILjava/lang/String;)V
    .locals 9
    .param p1, "rootTask"    # Lcom/android/server/wm/Task;
    .param p2, "miuiFreeFromWindowMode"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 393
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isActivityTypeHome()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 394
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "addFreeFormActivityStack reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreeFormManagerService"

    invoke-static {v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, ", miuiFreeFromWindowMode="

    const-string v3, " mffas.isInFreeFormMode()="

    const-string v4, "addFreeFormActivityStack as = "

    if-eqz v0, :cond_5

    .line 396
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 397
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    iget v6, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 398
    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    iget v6, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    iput v5, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    .line 399
    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    iget v6, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    :cond_1
    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormStackDisplayStrategy:Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->getMaxMiuiFreeFormStackCount(Ljava/lang/String;Lcom/android/server/wm/MiuiFreeFormActivityStack;)I

    move-result v5

    .line 402
    .local v5, "maxStackCount":I
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/util/MiuiMultiWindowAdapter;->isInTopGameList(Ljava/lang/String;)Z

    move-result v6

    .line 403
    .local v6, "isAddingTopGame":Z
    iget-object v7, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v7

    const-string v8, ", isAddingTopGame="

    if-gt v7, v5, :cond_2

    if-nez v6, :cond_2

    .line 404
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",maxStackCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mFreeFormActivityStacks.size()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    .line 406
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 404
    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 408
    :cond_2
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->hasActivity()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 409
    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFrontFreeformNum(Lcom/android/server/wm/MiuiFreeFormActivityStack;)I

    move-result v2

    .line 410
    .local v2, "frontSize":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",frontSize="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v1

    if-eqz v1, :cond_4

    if-ge v2, v5, :cond_3

    if-eqz v6, :cond_4

    .line 414
    :cond_3
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormStackDisplayStrategy:Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v1, v3, v4, v0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->onMiuiFreeFormStasckAdded(Ljava/util/concurrent/ConcurrentHashMap;Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 417
    .end local v2    # "frontSize":I
    :cond_4
    :goto_0
    return-void

    .line 420
    .end local v0    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v5    # "maxStackCount":I
    .end local v6    # "isAddingTopGame":Z
    :cond_5
    new-instance v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    invoke-direct {v0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;-><init>(Lcom/android/server/wm/Task;I)V

    .line 421
    .restart local v0    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    iget v3, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 424
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addFreeFormActivityStack  mCorrectScale  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    iget v4, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    .line 426
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    :cond_6
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->onMiuiFreeFormStasckAdded(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 430
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_8

    .line 431
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 432
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V

    goto :goto_1

    .line 433
    :cond_7
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 434
    invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V

    goto :goto_1

    .line 437
    :cond_8
    iput-boolean v2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z

    .line 439
    :cond_9
    :goto_1
    return-void
.end method

.method public addFreeFormActivityStack(Lcom/android/server/wm/Task;Ljava/lang/String;)V
    .locals 1
    .param p1, "rootTask"    # Lcom/android/server/wm/Task;
    .param p2, "reason"    # Ljava/lang/String;

    .line 384
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;ILjava/lang/String;)V

    .line 385
    return-void
.end method

.method public addFreeFormActivityStackFromStartSmallFreeform(Lcom/android/server/wm/Task;ILjava/lang/String;Landroid/graphics/Rect;)V
    .locals 7
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "cornerPosition"    # I
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "fromRect"    # Landroid/graphics/Rect;

    .line 1489
    const-string v0, "FromStartSmallFreeform"

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;ILjava/lang/String;)V

    .line 1490
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1491
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setCornerPosition(I)V

    .line 1492
    invoke-virtual {v0, p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setEnterMiniFreeformReason(Ljava/lang/String;)V

    .line 1493
    invoke-virtual {v0, p4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setEnterMiniFreeformRect(Landroid/graphics/Rect;)V

    .line 1495
    iget-object v2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1496
    iget-object v2, p1, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iget-boolean v3, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    iget-boolean v4, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    iget-object v5, p1, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 1498
    invoke-static {v5}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v5

    .line 1499
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v6

    .line 1496
    invoke-static {v2, v3, v4, v5, v6}, Landroid/util/MiuiMultiWindowUtils;->getOriFreeformScale(Landroid/content/Context;ZZZLjava/lang/String;)F

    move-result v2

    iput v2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    goto :goto_0

    .line 1501
    :cond_0
    iget-object v2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F

    move-result v2

    iput v2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    .line 1503
    :goto_0
    invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->setAlwaysOnTop(Z)V

    .line 1504
    return-void
.end method

.method public addFullScreenTasksBehindHome(I)V
    .locals 2
    .param p1, "taskId"    # I

    .line 2012
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 2013
    return-void
.end method

.method public adjustBoundsAndScaleIfNeeded(I)Z
    .locals 1
    .param p1, "taskId"    # I

    .line 2136
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->adjustBoundsAndScaleIfNeeded(I)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 2137
    :catch_0
    move-exception v0

    .line 2138
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2140
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return v0
.end method

.method public adjustFreeformTouchRegion(Landroid/graphics/Rect;I)V
    .locals 6
    .param p1, "inOutRect"    # Landroid/graphics/Rect;
    .param p2, "taskId"    # I

    .line 1777
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/android/server/wm/MiuiFreeformUtilStub;->isPadScreen(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 1778
    invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeformScale(I)F

    move-result v0

    .line 1779
    .local v0, "freefromScale":F
    invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeFormWindowMode(I)I

    move-result v3

    if-nez v3, :cond_0

    .line 1780
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    mul-int/lit8 v1, v1, -0x1

    .line 1781
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    mul-int/lit8 v2, v2, -0x1

    .line 1782
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v3

    div-float/2addr v3, v0

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    mul-int/lit8 v3, v3, -0x1

    .line 1783
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceBottomOffsetPad()F

    move-result v4

    div-float/2addr v4, v0

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    mul-int/lit8 v4, v4, -0x1

    .line 1780
    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Rect;->inset(IIII)V

    goto :goto_0

    .line 1784
    :cond_0
    invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeFormWindowMode(I)I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 1785
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    mul-int/lit8 v2, v2, -0x1

    .line 1787
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v3

    div-float/2addr v3, v0

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    mul-int/lit8 v3, v3, -0x1

    .line 1788
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceBottomOffsetPad()F

    move-result v4

    div-float/2addr v4, v0

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    mul-int/lit8 v4, v4, -0x1

    .line 1785
    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/graphics/Rect;->inset(IIII)V

    .line 1790
    .end local v0    # "freefromScale":F
    :cond_1
    :goto_0
    goto/16 :goto_1

    .line 1791
    :cond_2
    invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeformScale(I)F

    move-result v0

    .line 1792
    .restart local v0    # "freefromScale":F
    invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeFormWindowMode(I)I

    move-result v3

    if-nez v3, :cond_3

    .line 1793
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    mul-int/lit8 v1, v1, -0x1

    .line 1794
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    mul-int/lit8 v2, v2, -0x1

    .line 1793
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    goto :goto_1

    .line 1795
    :cond_3
    invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeFormWindowMode(I)I

    move-result v3

    if-ne v3, v2, :cond_4

    .line 1796
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v2

    .line 1797
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getMiniFreeformPaddingStroke()F

    move-result v3

    add-float/2addr v2, v3

    div-float/2addr v2, v0

    float-to-double v2, v2

    .line 1796
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    mul-int/lit8 v2, v2, -0x1

    .line 1799
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v3

    .line 1800
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getMiniFreeformPaddingStroke()F

    move-result v4

    add-float/2addr v3, v4

    div-float/2addr v3, v0

    float-to-double v3, v3

    .line 1799
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    mul-int/lit8 v3, v3, -0x1

    .line 1801
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getHotSpaceOffset()F

    move-result v4

    div-float/2addr v4, v0

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    mul-int/lit8 v4, v4, -0x1

    .line 1796
    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/graphics/Rect;->inset(IIII)V

    .line 1804
    .end local v0    # "freefromScale":F
    :cond_4
    :goto_1
    return-void
.end method

.method public adjustMovedToTopIfNeed(ZLcom/android/server/wm/Task;ZLcom/android/server/wm/Task;)Z
    .locals 2
    .param p1, "alreadyResumed"    # Z
    .param p2, "topFocusableTask"    # Lcom/android/server/wm/Task;
    .param p3, "movedToTop"    # Z
    .param p4, "focusTask"    # Lcom/android/server/wm/Task;

    .line 205
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    .line 206
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->isAlwaysOnTop()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    invoke-virtual {p4}, Lcom/android/server/wm/Task;->isAlwaysOnTop()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "adjustMovedToTop false topFocusableTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreeFormManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const/4 p3, 0x0

    .line 212
    :cond_0
    return p3
.end method

.method public adjustedReuseableFreeformTask(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/Task;
    .locals 8
    .param p1, "startActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 1269
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1270
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1271
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1272
    .local v3, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v4, :cond_8

    .line 1273
    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    .line 1274
    .local v4, "cls":Landroid/content/ComponentName;
    iget v5, p1, Lcom/android/server/wm/ActivityRecord;->mAnimationType:I

    iget-object v6, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 1275
    invoke-virtual {v6}, Lcom/android/server/wm/Task;->getActivityType()I

    move-result v6

    .line 1274
    invoke-static {v5, v6}, Lcom/android/server/wm/ConfigurationContainer;->isCompatibleActivityType(II)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1276
    goto/16 :goto_1

    .line 1279
    :cond_0
    iget-object v5, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v5, v5, Lcom/android/server/wm/Task;->mUserId:I

    iget v6, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    if-eq v5, v6, :cond_1

    .line 1280
    goto/16 :goto_1

    .line 1283
    :cond_1
    iget-object v5, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity(Z)Lcom/android/server/wm/ActivityRecord;

    move-result-object v5

    .line 1285
    .local v5, "r":Lcom/android/server/wm/ActivityRecord;
    if-eqz v5, :cond_9

    iget-boolean v6, v5, Lcom/android/server/wm/ActivityRecord;->finishing:Z

    if-nez v6, :cond_9

    iget v6, v5, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    iget v7, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    if-ne v6, v7, :cond_9

    iget v6, v5, Lcom/android/server/wm/ActivityRecord;->launchMode:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_2

    .line 1287
    goto/16 :goto_1

    .line 1290
    :cond_2
    invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord;->getActivityType()I

    move-result v6

    iget v7, p1, Lcom/android/server/wm/ActivityRecord;->mAnimationType:I

    invoke-static {v6, v7}, Lcom/android/server/wm/ConfigurationContainer;->isCompatibleActivityType(II)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1292
    goto :goto_1

    .line 1295
    :cond_3
    iget-object v6, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v6, v6, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v6, :cond_4

    iget-object v6, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v6, v6, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v6, v4}, Landroid/content/ComponentName;->compareTo(Landroid/content/ComponentName;)I

    move-result v6

    if-eqz v6, :cond_6

    :cond_4
    iget-object v6, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v6, v6, Lcom/android/server/wm/Task;->affinityIntent:Landroid/content/Intent;

    if-eqz v6, :cond_5

    iget-object v6, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v6, v6, Lcom/android/server/wm/Task;->affinityIntent:Landroid/content/Intent;

    .line 1296
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v6, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v6, v6, Lcom/android/server/wm/Task;->affinityIntent:Landroid/content/Intent;

    .line 1297
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/ComponentName;->compareTo(Landroid/content/ComponentName;)I

    move-result v6

    if-eqz v6, :cond_6

    :cond_5
    iget-object v6, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v6, v6, Lcom/android/server/wm/Task;->rootAffinity:Ljava/lang/String;

    if-eqz v6, :cond_8

    iget-object v6, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v6, v6, Lcom/android/server/wm/Task;->rootAffinity:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/server/wm/ActivityRecord;->taskAffinity:Ljava/lang/String;

    .line 1299
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1300
    :cond_6
    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-static {v6, v4}, Landroid/util/MiuiMultiWindowAdapter;->isAppLockActivity(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1301
    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    const-string v7, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1302
    .local v6, "pkgBehindIntentActivity":Ljava/lang/String;
    if-eqz v6, :cond_7

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 1303
    goto/16 :goto_0

    .line 1305
    .end local v6    # "pkgBehindIntentActivity":Ljava/lang/String;
    :cond_7
    iget-object v6, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1308
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    .end local v3    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v4    # "cls":Landroid/content/ComponentName;
    .end local v5    # "r":Lcom/android/server/wm/ActivityRecord;
    :cond_8
    goto/16 :goto_0

    .line 1309
    :cond_9
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x0

    goto :goto_2

    :cond_a
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/Task;

    :goto_2
    return-object v1
.end method

.method public allFreeformFormFreefromToMini()V
    .locals 2

    .line 1166
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1167
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1174
    return-void
.end method

.method public appLockAlreadyInFront(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 3
    .param p1, "targetRootTask"    # Lcom/android/server/wm/Task;
    .param p2, "startActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 2223
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    goto :goto_1

    .line 2226
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isFrontFreeFormStackInfo(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 2227
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    nop

    .line 2226
    :goto_0
    return v0

    .line 2224
    :cond_2
    :goto_1
    return v0
.end method

.method public autoLayoutFreeFormStackIfNeed(Landroid/graphics/Rect;FLandroid/graphics/Rect;Lcom/android/server/wm/Task;Ljava/lang/String;Lcom/android/server/wm/MiuiFreeFormActivityStack;)I
    .locals 17
    .param p1, "outBounds"    # Landroid/graphics/Rect;
    .param p2, "scale"    # F
    .param p3, "accessibleArea"    # Landroid/graphics/Rect;
    .param p4, "excludedTask"    # Lcom/android/server/wm/Task;
    .param p5, "pkg"    # Ljava/lang/String;
    .param p6, "freeformTaskHasAppLock"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1593
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p6

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v6

    const/4 v8, 0x1

    if-le v5, v6, :cond_0

    move v5, v8

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    .line 1594
    .local v5, "isDisplayLandscape":Z
    :goto_0
    if-eqz v5, :cond_1

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v6

    goto :goto_1

    :cond_1
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v6

    .line 1596
    .local v6, "remainingSpace":I
    :goto_1
    const/high16 v9, 0x41a00000    # 20.0f

    invoke-static {v9}, Landroid/util/MiuiMultiWindowUtils;->applyDip2Px(F)F

    move-result v9

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v9, v10

    float-to-int v9, v9

    .line 1597
    .local v9, "gap":I
    const/4 v11, 0x0

    .line 1598
    .local v11, "frontNum":I
    iget-object v12, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v12}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    const-string v14, "MiuiFreeFormManagerService"

    if-eqz v13, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    .line 1599
    .local v13, "taskId":Ljava/lang/Integer;
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v15

    iget v7, v3, Lcom/android/server/wm/Task;->mTaskId:I

    if-ne v15, v7, :cond_2

    goto :goto_2

    .line 1600
    :cond_2
    iget-object v7, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v13}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1601
    .local v7, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-direct {v0, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isCandidateForAutoLayout(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1603
    iget-object v15, v7, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v15}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v15

    iget v10, v7, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    invoke-static {v5, v15, v10}, Landroid/util/MiuiMultiWindowUtils;->getVisualWidthOrHeight(ZLandroid/graphics/Rect;F)I

    move-result v10

    sub-int/2addr v6, v10

    .line 1604
    sub-int/2addr v6, v9

    .line 1605
    add-int/lit8 v11, v11, 0x1

    .line 1606
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "auto layout: CandidateForAutoLayout: "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, " remainingSpace: "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v14, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1608
    .end local v7    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v13    # "taskId":Ljava/lang/Integer;
    :cond_3
    const/high16 v10, 0x3f000000    # 0.5f

    goto :goto_2

    .line 1609
    :cond_4
    iget v7, v3, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v0, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFrontFreeformNum(I)I

    move-result v7

    .line 1610
    .local v7, "freeformNum":I
    const/4 v10, 0x4

    if-lt v7, v10, :cond_5

    move/from16 v16, v8

    goto :goto_3

    :cond_5
    const/16 v16, 0x0

    :goto_3
    move/from16 v10, v16

    .line 1611
    .local v10, "replaceFreeForm":Z
    if-ne v11, v8, :cond_6

    if-eqz v10, :cond_6

    .line 1612
    const-string v12, "auto layout: There is only one freeform at the front desk and it will be replaced"

    invoke-static {v14, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1613
    add-int/lit8 v11, v11, -0x1

    .line 1615
    :cond_6
    if-eqz v11, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAutoLayoutModeOn()Z

    move-result v12

    if-eqz v12, :cond_7

    goto :goto_4

    :cond_7
    move/from16 v12, p2

    goto/16 :goto_8

    .line 1616
    :cond_8
    :goto_4
    if-eqz v4, :cond_9

    .line 1617
    add-int/2addr v6, v9

    .line 1618
    iget-object v12, v4, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v12}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v12

    iget v13, v4, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    invoke-static {v5, v12, v13}, Landroid/util/MiuiMultiWindowUtils;->getVisualWidthOrHeight(ZLandroid/graphics/Rect;F)I

    move-result v12

    add-int/2addr v6, v12

    .line 1621
    iget-object v12, v4, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v12}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v12

    invoke-direct {v0, v12}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFreeformParamsForAutoLayout(I)V

    .line 1622
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "auto layout: found app lock is going to exit freeform: increase left space to "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v14, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 1623
    :cond_9
    if-eqz v10, :cond_a

    .line 1624
    iget v12, v3, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v0, v12}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v12

    .line 1625
    .local v12, "excludedMffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-virtual {v0, v12}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getReplaceFreeForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v13

    .line 1626
    .local v13, "subMffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-direct {v0, v13}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isCandidateForAutoLayout(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1628
    add-int/2addr v6, v9

    .line 1629
    iget-object v15, v13, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v15}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v15

    iget v8, v13, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    invoke-static {v5, v15, v8}, Landroid/util/MiuiMultiWindowUtils;->getVisualWidthOrHeight(ZLandroid/graphics/Rect;F)I

    move-result v8

    add-int/2addr v6, v8

    .line 1632
    iget-object v8, v13, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v8}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v8

    invoke-direct {v0, v8}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFreeformParamsForAutoLayout(I)V

    .line 1633
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "auto layout: found one is going to exit freeform: increase left space to "

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v14, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1637
    .end local v12    # "excludedMffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v13    # "subMffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_a
    :goto_5
    if-eqz v5, :cond_b

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v8, v6

    goto :goto_6

    .line 1638
    :cond_b
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v8

    sub-int/2addr v8, v6

    :goto_6
    nop

    .line 1639
    .local v8, "freeformPlusGapSpace":I
    move/from16 v12, p2

    invoke-static {v5, v1, v12}, Landroid/util/MiuiMultiWindowUtils;->getVisualWidthOrHeight(ZLandroid/graphics/Rect;F)I

    move-result v13

    sub-int/2addr v6, v13

    .line 1640
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "auto layout: left space = "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, " freeformPlusGapSpace: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, " outBounds: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v14, v13}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1641
    if-ltz v6, :cond_d

    .line 1642
    div-int/lit8 v13, v6, 0x2

    int-to-float v13, v13

    const/high16 v15, 0x3f000000    # 0.5f

    add-float/2addr v13, v15

    float-to-int v13, v13

    .line 1643
    .local v13, "margin":I
    if-eqz v5, :cond_c

    iget v15, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v15, v13

    goto :goto_7

    .line 1644
    :cond_c
    iget v15, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v15, v13

    :goto_7
    nop

    .line 1645
    .local v15, "startPoint":I
    add-int/2addr v15, v8

    .line 1646
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->updateAutoLayoutModeStatus(Z)V

    .line 1647
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "auto layout: left start point = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1648
    return v15

    .line 1651
    .end local v8    # "freeformPlusGapSpace":I
    .end local v13    # "margin":I
    .end local v15    # "startPoint":I
    :cond_d
    :goto_8
    const/4 v0, -0x1

    return v0
.end method

.method public autoLayoutOthersIfNeed(I)V
    .locals 1
    .param p1, "taskId"    # I

    .line 2232
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->autoLayoutOthersIfNeed(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2235
    goto :goto_0

    .line 2233
    :catch_0
    move-exception v0

    .line 2234
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2236
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public clearFullScreenTasksBehindHome()V
    .locals 1

    .line 2018
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->clear()V

    .line 2020
    return-void
.end method

.method public deliverResultForFinishActivity(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;)V
    .locals 1
    .param p1, "resultTo"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "resultFrom"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "intent"    # Landroid/content/Intent;

    .line 2050
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    if-eqz v0, :cond_0

    .line 2051
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverResultForFinishActivity(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;)V

    .line 2053
    :cond_0
    return-void
.end method

.method public deliverResultForResumeActivityInFreeform(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "resultTo"    # Lcom/android/server/wm/ActivityRecord;

    .line 2056
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    if-eqz v0, :cond_0

    .line 2057
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverResultForResumeActivityInFreeform(Lcom/android/server/wm/ActivityRecord;)V

    .line 2059
    :cond_0
    return-void
.end method

.method public disableIgnoreOrientationRequest(Lcom/android/server/wm/TransitionController;)Z
    .locals 2
    .param p1, "transitionController"    # Lcom/android/server/wm/TransitionController;

    .line 2130
    invoke-virtual {p1}, Lcom/android/server/wm/TransitionController;->getCollectingTransitionType()I

    move-result v0

    const v1, 0x7fffff97

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public disableSplashScreenForWpsInFreeForm(Landroid/content/ComponentName;I)Z
    .locals 3
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "userId"    # I

    .line 1478
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 1479
    return v0

    .line 1481
    :cond_0
    const-string v1, "cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity"

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1482
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(Ljava/lang/String;I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1483
    const/4 v0, 0x1

    return v0

    .line 1485
    :cond_1
    return v0
.end method

.method public dispatchFreeFormStackModeChanged(II)V
    .locals 4
    .param p1, "action"    # I
    .param p2, "taskId"    # I

    .line 1047
    invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1048
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    const/4 v1, 0x0

    .line 1049
    .local v1, "i":I
    :goto_0
    const/16 v2, 0x15

    if-gt v1, v2, :cond_1

    .line 1050
    const/4 v2, 0x1

    shl-int/2addr v2, v1

    .line 1051
    .local v2, "mask":I
    and-int v3, p1, v2

    if-eqz v3, :cond_0

    .line 1052
    invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 1049
    .end local v2    # "mask":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1055
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 2
    .param p1, "action"    # I
    .param p2, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1058
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1059
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda8;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda8;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1089
    return-void
.end method

.method public displayConfigurationChange(Lcom/android/server/wm/DisplayContent;Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "configuration"    # Landroid/content/res/Configuration;

    .line 1769
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    if-eqz v0, :cond_0

    .line 1770
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->displayConfigurationChange(Lcom/android/server/wm/DisplayContent;Landroid/content/res/Configuration;)V

    .line 1772
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->displayConfigurationChange(Lcom/android/server/wm/DisplayContent;Landroid/content/res/Configuration;)V

    .line 1774
    :cond_0
    return-void
.end method

.method public exitAllFreeform()V
    .locals 4

    .line 2206
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getAllMiuiFreeFormActivityStack()Ljava/util/List;

    move-result-object v0

    .line 2207
    .local v0, "miuiFreeFormActivityStackList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 2208
    .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v2, :cond_0

    .line 2209
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v3, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 2211
    .end local v2    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto :goto_0

    .line 2212
    :cond_1
    return-void
.end method

.method public exitFreeformIfEnterSplitScreen(Lcom/android/server/wm/Task;Lcom/android/server/wm/Task;)V
    .locals 3
    .param p1, "candidateTask"    # Lcom/android/server/wm/Task;
    .param p2, "candidateRoot"    # Lcom/android/server/wm/Task;

    .line 1925
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1926
    invoke-direct {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isLaunchForSplitMode(Lcom/android/server/wm/Task;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1927
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exitFreeformEnterSplitScreen candidateTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " candidateRoot: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreeFormManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1929
    new-instance v0, Landroid/content/res/Configuration;

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRequestedOverrideConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 1930
    .local v0, "c":Landroid/content/res/Configuration;
    iget-object v1, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/WindowConfiguration;->setAlwaysOnTop(Z)V

    .line 1931
    iget-object v1, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v1, v2}, Landroid/app/WindowConfiguration;->setWindowingMode(I)V

    .line 1932
    iget-object v1, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V

    .line 1933
    invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->onRequestedOverrideConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1935
    .end local v0    # "c":Landroid/content/res/Configuration;
    :cond_0
    return-void
.end method

.method public freeformFullscreenTask(I)V
    .locals 2
    .param p1, "taskId"    # I

    .line 1832
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1833
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda17;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda17;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1840
    return-void
.end method

.method public freeformKillAll()V
    .locals 2

    .line 1843
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1844
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1851
    return-void
.end method

.method public freeformToFullscreenByBottomCaption(I)V
    .locals 5
    .param p1, "taskId"    # I

    .line 2069
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 2070
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    if-eqz v1, :cond_0

    .line 2071
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 2072
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    const-string v2, "MiuiFreeFormManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " freeformToFullscreenByBottomCaption: taskId= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2073
    if-eqz v1, :cond_0

    .line 2074
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverResultForExitFreeform(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 2077
    .end local v1    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    monitor-exit v0

    .line 2078
    return-void

    .line 2077
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public fromFreefromToMini(I)V
    .locals 2
    .param p1, "taskId"    # I

    .line 1177
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1178
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1185
    return-void
.end method

.method public fromSoscToFreeform(I)V
    .locals 3
    .param p1, "taskId"    # I

    .line 1237
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    if-eqz v0, :cond_0

    .line 1239
    :try_start_0
    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->fromSoscToFreeform(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1242
    goto :goto_0

    .line 1240
    :catch_0
    move-exception v0

    .line 1241
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MiuiFreeFormManagerService"

    const-string v2, "fromSoscToFreeform."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1244
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method

.method public fullscreenFreeformTask(I)V
    .locals 2
    .param p1, "taskId"    # I

    .line 1821
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1822
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1829
    return-void
.end method

.method public getAllFreeFormStackInfosOnDisplay(I)Ljava/util/List;
    .locals 8
    .param p1, "displayId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;",
            ">;"
        }
    .end annotation

    .line 912
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 914
    .local v0, "ident":J
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 915
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
    const/4 v3, -0x1

    const-string v4, "MiuiFreeFormManagerService"

    if-ne v3, p1, :cond_2

    .line 916
    :try_start_1
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 917
    .local v5, "taskId":Ljava/lang/Integer;
    iget-object v6, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 918
    .local v6, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v6, :cond_0

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Lmiui/app/MiuiFreeFormManager;->isFrontFreeFormStackInfo(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 919
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Lmiui/app/MiuiFreeFormManager;->isHideStackFromFullScreen(I)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    .line 920
    invoke-virtual {v7, v5}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 922
    invoke-virtual {v6}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 924
    .end local v5    # "taskId":Ljava/lang/Integer;
    .end local v6    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto :goto_0

    .line 925
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAllFreeFormStackInfosOnDisplay INVALID_DISPLAY list="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 926
    nop

    .line 941
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 926
    return-object v2

    .line 928
    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 929
    .restart local v5    # "taskId":Ljava/lang/Integer;
    iget-object v6, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 930
    .restart local v6    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v6, :cond_3

    iget-object v7, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v7}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v7

    if-ne v7, p1, :cond_3

    .line 931
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Lmiui/app/MiuiFreeFormManager;->isFrontFreeFormStackInfo(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 932
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Lmiui/app/MiuiFreeFormManager;->isHideStackFromFullScreen(I)Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    .line 933
    invoke-virtual {v7, v5}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 935
    invoke-virtual {v6}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 937
    .end local v5    # "taskId":Ljava/lang/Integer;
    .end local v6    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_3
    goto :goto_1

    .line 938
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAllFreeFormStackInfosOnDisplay list="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 939
    nop

    .line 941
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 939
    return-object v2

    .line 941
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 942
    throw v2
.end method

.method public getAllFrontFreeFormStackInfosOnDesktopMode(I)Ljava/util/List;
    .locals 7
    .param p1, "displayId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;",
            ">;"
        }
    .end annotation

    .line 946
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 948
    .local v0, "ident":J
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 949
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
    const/4 v3, -0x1

    if-ne v3, p1, :cond_2

    .line 950
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 951
    .local v4, "taskId":Ljava/lang/Integer;
    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 952
    .local v5, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v5, :cond_0

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lmiui/app/MiuiFreeFormManager;->isFrontFreeFormStackInfo(I)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lmiui/app/MiuiFreeFormManager;->isHideStackFromFullScreen(I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 953
    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 955
    .end local v4    # "taskId":Ljava/lang/Integer;
    .end local v5    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto :goto_0

    .line 956
    :cond_1
    nop

    .line 967
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 956
    return-object v2

    .line 958
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 959
    .restart local v4    # "taskId":Ljava/lang/Integer;
    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 960
    .restart local v5    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v5, :cond_3

    iget-object v6, v5, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v6}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v6

    if-ne v6, p1, :cond_3

    .line 961
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lmiui/app/MiuiFreeFormManager;->isFrontFreeFormStackInfo(I)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lmiui/app/MiuiFreeFormManager;->isHideStackFromFullScreen(I)Z

    move-result v6

    if-nez v6, :cond_3

    .line 962
    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 964
    .end local v4    # "taskId":Ljava/lang/Integer;
    .end local v5    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_3
    goto :goto_1

    .line 965
    :cond_4
    nop

    .line 967
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 965
    return-object v2

    .line 967
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 968
    throw v2
.end method

.method public getAllMiuiFreeFormActivityStack()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/server/wm/MiuiFreeFormActivityStack;",
            ">;"
        }
    .end annotation

    .line 652
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 653
    .local v0, "stackList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 654
    .local v2, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 655
    .end local v2    # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    goto :goto_0

    .line 656
    :cond_0
    return-object v0
.end method

.method public getAllPinedFreeFormStackInfosOnDisplay(I)Ljava/util/List;
    .locals 7
    .param p1, "displayId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;",
            ">;"
        }
    .end annotation

    .line 971
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 973
    .local v0, "ident":J
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 974
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
    const/4 v3, -0x1

    if-ne v3, p1, :cond_2

    .line 975
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 976
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 977
    .local v5, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 978
    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 980
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    .end local v5    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto :goto_0

    .line 981
    :cond_1
    nop

    .line 991
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 981
    return-object v2

    .line 983
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 984
    .restart local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 985
    .restart local v5    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v5, :cond_3

    iget-object v6, v5, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v6}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v6

    if-ne v6, p1, :cond_3

    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 986
    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 988
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    .end local v5    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_3
    goto :goto_1

    .line 989
    :cond_4
    nop

    .line 991
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 989
    return-object v2

    .line 991
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 992
    throw v2
.end method

.method public getApplicationUsedInFreeform(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 1104
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "application_used_freeform"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBottomGameFreeFormActivityStack(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .locals 4
    .param p1, "addingStack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 887
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 888
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 889
    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v1

    .line 892
    .local v1, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda10;

    invoke-direct {v2, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda10;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 905
    .local v2, "bottomTask":Lcom/android/server/wm/Task;
    if-nez v2, :cond_0

    .line 906
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    .line 908
    :cond_0
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v3

    monitor-exit v0

    return-object v3

    .line 909
    .end local v1    # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    .end local v2    # "bottomTask":Lcom/android/server/wm/Task;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getCurrentMiuiFreeFormNum()I
    .locals 1

    .line 376
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    return v0
.end method

.method public getFreeFormStackInfoByActivity(Landroid/os/IBinder;)Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    .locals 6
    .param p1, "token"    # Landroid/os/IBinder;

    .line 996
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 998
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 999
    :try_start_1
    invoke-static {p1}, Lcom/android/server/wm/ActivityRecord;->isInRootTaskLocked(Landroid/os/IBinder;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 1000
    .local v3, "r":Lcom/android/server/wm/ActivityRecord;
    if-eqz v3, :cond_0

    .line 1001
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v4

    .line 1002
    .local v4, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v4, :cond_0

    .line 1003
    invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v5

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1009
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1003
    return-object v5

    .line 1006
    .end local v4    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1009
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1006
    const/4 v2, 0x0

    return-object v2

    .line 1007
    .end local v3    # "r":Lcom/android/server/wm/ActivityRecord;
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "ident":J
    .end local p0    # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
    .end local p1    # "token":Landroid/os/IBinder;
    :try_start_4
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1009
    .restart local v0    # "ident":J
    .restart local p0    # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
    .restart local p1    # "token":Landroid/os/IBinder;
    :catchall_1
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1010
    throw v2
.end method

.method public getFreeFormStackInfoByStackId(I)Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    .locals 5
    .param p1, "stackId"    # I

    .line 1032
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1034
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1035
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v3

    .line 1036
    .local v3, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v3, :cond_0

    .line 1037
    invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v4

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1042
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1037
    return-object v4

    .line 1039
    :cond_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1042
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1039
    const/4 v2, 0x0

    return-object v2

    .line 1040
    .end local v3    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "ident":J
    .end local p0    # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
    .end local p1    # "stackId":I
    :try_start_4
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1042
    .restart local v0    # "ident":J
    .restart local p0    # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
    .restart local p1    # "stackId":I
    :catchall_1
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1043
    throw v2
.end method

.method public getFreeFormStackInfoByWindow(Landroid/os/IBinder;)Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    .locals 6
    .param p1, "wtoken"    # Landroid/os/IBinder;

    .line 1014
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1016
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1017
    :try_start_1
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v5, p1, v4}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/os/IBinder;Z)Lcom/android/server/wm/WindowState;

    move-result-object v3

    .line 1018
    .local v3, "win":Lcom/android/server/wm/WindowState;
    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v4, :cond_0

    .line 1019
    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v4

    .line 1020
    .local v4, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v4, :cond_0

    .line 1021
    invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v5

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1027
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1021
    return-object v5

    .line 1024
    .end local v4    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1027
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1024
    return-object v5

    .line 1025
    .end local v3    # "win":Lcom/android/server/wm/WindowState;
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "ident":J
    .end local p0    # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
    .end local p1    # "wtoken":Landroid/os/IBinder;
    :try_start_4
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1027
    .restart local v0    # "ident":J
    .restart local p0    # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
    .restart local p1    # "wtoken":Landroid/os/IBinder;
    :catchall_1
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1028
    throw v2
.end method

.method public getFreeFormStackToAvoid(ILjava/lang/String;)Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    .locals 7
    .param p1, "displayId"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 1554
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1556
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 1557
    invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    .line 1559
    .local v2, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1560
    .local v4, "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v6, :cond_0

    goto :goto_1

    .line 1563
    .end local v4    # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto :goto_0

    .line 1561
    .restart local v4    # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_1
    :goto_1
    nop

    .line 1586
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1561
    return-object v5

    .line 1564
    .end local v4    # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1565
    :try_start_2
    new-instance v4, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda0;

    invoke-direct {v4, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V

    const/4 v6, 0x1

    invoke-virtual {v2, v4, v6}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;

    move-result-object v4

    .line 1580
    .local v4, "topTaskToAvoid":Lcom/android/server/wm/Task;
    if-nez v4, :cond_3

    .line 1581
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1586
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1581
    return-object v5

    .line 1583
    :cond_3
    :try_start_3
    invoke-virtual {v4}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v5

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1586
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1583
    return-object v5

    .line 1584
    .end local v4    # "topTaskToAvoid":Lcom/android/server/wm/Task;
    :catchall_0
    move-exception v4

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .end local v0    # "ident":J
    .end local p0    # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
    .end local p1    # "displayId":I
    .end local p2    # "packageName":Ljava/lang/String;
    :try_start_5
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1586
    .end local v2    # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    .restart local v0    # "ident":J
    .restart local p0    # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
    .restart local p1    # "displayId":I
    .restart local p2    # "packageName":Ljava/lang/String;
    :catchall_1
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1587
    throw v2
.end method

.method public getFreeFormWindowMode(I)I
    .locals 2
    .param p1, "rootStackId"    # I

    .line 671
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 672
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_0

    .line 673
    const/4 v1, -0x1

    return v1

    .line 675
    :cond_0
    iget v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    return v1
.end method

.method public getFreeformLastPosition(I)Landroid/graphics/Rect;
    .locals 2
    .param p1, "taskId"    # I

    .line 1959
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 1960
    .local v0, "task":Lcom/android/server/wm/Task;
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastPosition(Lcom/android/server/wm/Task;)Landroid/graphics/Rect;

    move-result-object v1

    return-object v1
.end method

.method public getFreeformLastScale(I)F
    .locals 2
    .param p1, "taskId"    # I

    .line 1954
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 1955
    .local v0, "task":Lcom/android/server/wm/Task;
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F

    move-result v1

    return v1
.end method

.method public getFreeformRectDesktop(Landroid/graphics/Rect;FIZ)F
    .locals 19
    .param p1, "outBounds"    # Landroid/graphics/Rect;
    .param p2, "scale"    # F
    .param p3, "taskId"    # I
    .param p4, "isDefaultPosition"    # Z

    .line 1701
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move/from16 v0, p2

    move/from16 v9, p3

    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v1

    const-string v10, "getFreeformRectDesktop taskId="

    const-string v11, "MiuiFreeFormManagerService"

    if-eqz v1, :cond_10

    .line 1702
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-lez v1, :cond_f

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    goto/16 :goto_3

    .line 1706
    :cond_0
    iget-object v1, v7, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    const/4 v12, 0x0

    invoke-virtual {v1, v9, v12}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;

    move-result-object v13

    .line 1707
    .local v13, "task":Lcom/android/server/wm/Task;
    if-nez v13, :cond_1

    return v0

    .line 1708
    :cond_1
    iget-object v1, v7, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v2

    invoke-static {v1, v2}, Landroid/util/MiuiMultiWindowUtils;->getFreeFormAccessibleArea(Landroid/content/Context;Z)Landroid/graphics/Rect;

    move-result-object v14

    .line 1709
    .local v14, "accessibleArea":Landroid/graphics/Rect;
    invoke-static/range {p1 .. p2}, Landroid/util/MiuiMultiWindowUtils;->getVisualBounds(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object v1

    .line 1710
    .local v1, "visibleBounds":Landroid/graphics/Rect;
    iget-object v2, v13, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    if-eqz v2, :cond_2

    iget-object v2, v13, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1711
    :cond_2
    iget-object v2, v13, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v2, :cond_3

    iget-object v2, v13, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1712
    :cond_3
    invoke-virtual {v13}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1713
    invoke-virtual {v13}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string/jumbo v2, "unknown"

    :goto_0
    move-object v15, v2

    .line 1714
    .local v15, "packageName":Ljava/lang/String;
    const/4 v6, 0x1

    if-eqz p4, :cond_5

    .line 1715
    iget-object v2, v7, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-static {v2, v15, v6, v12, v6}, Landroid/util/MiuiMultiWindowUtils;->getActivityOptions(Landroid/content/Context;Ljava/lang/String;ZZZ)Landroid/app/ActivityOptions;

    move-result-object v2

    .line 1717
    invoke-virtual {v2}, Landroid/app/ActivityOptions;->getLaunchBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 1718
    .local v2, "launchBounds":Landroid/graphics/Rect;
    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1720
    .end local v2    # "launchBounds":Landroid/graphics/Rect;
    :cond_5
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-gt v2, v3, :cond_6

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-le v2, v3, :cond_7

    .line 1721
    :cond_6
    invoke-direct {v7, v0, v8, v14}, Lcom/android/server/wm/MiuiFreeFormManagerService;->scaleDownIfNeeded(FLandroid/graphics/Rect;Landroid/graphics/Rect;)F

    move-result v2

    .line 1722
    .local v2, "newscale":F
    cmpl-float v3, v2, v0

    if-eqz v3, :cond_7

    .line 1723
    move v0, v2

    .line 1724
    .end local p2    # "scale":F
    .local v0, "scale":F
    invoke-static {v8, v0}, Landroid/util/MiuiMultiWindowUtils;->getVisualBounds(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object v1

    .line 1725
    iget-object v3, v7, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCorrectScaleList:Ljava/util/Map;

    iget v4, v13, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1726
    invoke-virtual {v13, v2}, Lcom/android/server/wm/Task;->setMiuiFreeformScale(F)V

    .line 1727
    invoke-virtual {v13, v8}, Lcom/android/server/wm/Task;->setLastNonFullscreenBounds(Landroid/graphics/Rect;)V

    .line 1728
    iget-object v3, v7, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/android/server/wm/LaunchParamsController;->saveFreeformDesktopMemory(Lcom/android/server/wm/Task;)V

    move v5, v0

    move-object v4, v1

    goto :goto_1

    .line 1732
    .end local v0    # "scale":F
    .end local v2    # "newscale":F
    .restart local p2    # "scale":F
    :cond_7
    move v5, v0

    move-object v4, v1

    .end local v1    # "visibleBounds":Landroid/graphics/Rect;
    .end local p2    # "scale":F
    .local v4, "visibleBounds":Landroid/graphics/Rect;
    .local v5, "scale":F
    :goto_1
    invoke-direct {v7, v13, v15}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeformTaskHasAppLock(Lcom/android/server/wm/Task;Ljava/lang/String;)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v3

    .line 1733
    .local v3, "freeformTaskHasAppLock":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v3, :cond_8

    .line 1734
    iget-object v0, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1735
    .local v0, "posBeforeUnlocking":Landroid/graphics/Rect;
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v8, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1736
    iget v1, v8, Landroid/graphics/Rect;->left:I

    iget v2, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1737
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unlock app lock and stay at"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1740
    .end local v0    # "posBeforeUnlocking":Landroid/graphics/Rect;
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v5

    move-object/from16 v16, v3

    .end local v3    # "freeformTaskHasAppLock":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .local v16, "freeformTaskHasAppLock":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    move-object v3, v14

    move-object v12, v4

    .end local v4    # "visibleBounds":Landroid/graphics/Rect;
    .local v12, "visibleBounds":Landroid/graphics/Rect;
    move-object v4, v13

    move-object/from16 v17, v13

    move v13, v5

    .end local v5    # "scale":F
    .local v13, "scale":F
    .local v17, "task":Lcom/android/server/wm/Task;
    move-object v5, v15

    move/from16 v18, v6

    move-object/from16 v6, v16

    invoke-virtual/range {v0 .. v6}, Lcom/android/server/wm/MiuiFreeFormManagerService;->autoLayoutFreeFormStackIfNeed(Landroid/graphics/Rect;FLandroid/graphics/Rect;Lcom/android/server/wm/Task;Ljava/lang/String;Lcom/android/server/wm/MiuiFreeFormActivityStack;)I

    move-result v0

    .line 1743
    .local v0, "startPoint":I
    if-lez v0, :cond_a

    .line 1744
    invoke-static {v8, v13}, Landroid/util/MiuiMultiWindowUtils;->scaleBounds(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object v1

    .line 1745
    .local v1, "tempScaledBounds":Landroid/graphics/Rect;
    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-le v2, v3, :cond_9

    move/from16 v2, v18

    goto :goto_2

    :cond_9
    const/4 v2, 0x0

    :goto_2
    invoke-direct {v7, v1, v14, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->centerRect(Landroid/graphics/Rect;Landroid/graphics/Rect;ZI)V

    .line 1746
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v12, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1747
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " auto layout move to"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1750
    .end local v1    # "tempScaledBounds":Landroid/graphics/Rect;
    :cond_a
    iget v1, v12, Landroid/graphics/Rect;->left:I

    iget v2, v14, Landroid/graphics/Rect;->left:I

    if-ge v1, v2, :cond_b

    .line 1751
    iget v1, v14, Landroid/graphics/Rect;->left:I

    iget v2, v12, Landroid/graphics/Rect;->top:I

    invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1753
    :cond_b
    iget v1, v12, Landroid/graphics/Rect;->top:I

    iget v2, v14, Landroid/graphics/Rect;->top:I

    if-ge v1, v2, :cond_c

    .line 1754
    iget v1, v12, Landroid/graphics/Rect;->left:I

    iget v2, v14, Landroid/graphics/Rect;->top:I

    invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1756
    :cond_c
    iget v1, v12, Landroid/graphics/Rect;->left:I

    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v14, Landroid/graphics/Rect;->right:I

    if-le v1, v2, :cond_d

    .line 1757
    iget v1, v14, Landroid/graphics/Rect;->right:I

    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, v12, Landroid/graphics/Rect;->top:I

    invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1759
    :cond_d
    iget v1, v12, Landroid/graphics/Rect;->top:I

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v14, Landroid/graphics/Rect;->bottom:I

    if-le v1, v2, :cond_e

    .line 1760
    iget v1, v12, Landroid/graphics/Rect;->left:I

    iget v2, v14, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1762
    :cond_e
    iget v1, v12, Landroid/graphics/Rect;->left:I

    iget v2, v12, Landroid/graphics/Rect;->top:I

    invoke-virtual {v8, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    goto :goto_4

    .line 1703
    .end local v0    # "startPoint":I
    .end local v12    # "visibleBounds":Landroid/graphics/Rect;
    .end local v13    # "scale":F
    .end local v14    # "accessibleArea":Landroid/graphics/Rect;
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v16    # "freeformTaskHasAppLock":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v17    # "task":Lcom/android/server/wm/Task;
    .restart local p2    # "scale":F
    :cond_f
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFreeformRectDesktop failed! taskId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " scale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1704
    return v0

    .line 1701
    :cond_10
    move v13, v0

    .line 1764
    .end local p2    # "scale":F
    .restart local v13    # "scale":F
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ,scale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", outBounds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1765
    return v13
.end method

.method public getFreeformScale(I)F
    .locals 2
    .param p1, "taskId"    # I

    .line 326
    const/high16 v0, 0x3f800000    # 1.0f

    .line 327
    .local v0, "freeformScale":F
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v1

    .line 328
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_0

    .line 329
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormScale()F

    move-result v0

    .line 331
    :cond_0
    return v0
.end method

.method public getFreeformStackBounds(Lcom/android/server/wm/Task;IILandroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 18
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "oldRotation"    # I
    .param p3, "newRotation"    # I
    .param p4, "preBounds"    # Landroid/graphics/Rect;
    .param p5, "newBounds"    # Landroid/graphics/Rect;

    .line 2144
    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 2145
    .local v1, "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    const/4 v2, 0x0

    if-eqz v1, :cond_2

    iget-object v3, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-nez v3, :cond_0

    move/from16 v7, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    goto/16 :goto_1

    .line 2146
    :cond_0
    iget-object v3, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v3}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v3

    .line 2147
    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;

    move-result-object v3

    .line 2148
    .local v3, "topRootTask":Lcom/android/server/wm/Task;
    iget-object v4, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v5, v4, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-boolean v9, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    .line 2149
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    iget v10, v3, Lcom/android/server/wm/Task;->mTaskId:I

    .line 2150
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v13

    if-eq v10, v13, :cond_1

    move v13, v4

    goto :goto_0

    :cond_1
    move v13, v2

    :goto_0
    const/4 v14, 0x0

    const/4 v15, 0x0

    iget-boolean v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    iget-object v10, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v10, v10, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 2151
    invoke-static {v10}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v17

    .line 2148
    move-object/from16 v10, p5

    move/from16 v16, v2

    invoke-static/range {v5 .. v17}, Landroid/util/MiuiMultiWindowUtils;->getFreeformRect(Landroid/content/Context;ZZZZLandroid/graphics/Rect;Ljava/lang/String;ZZIIZZ)Landroid/graphics/Rect;

    .line 2152
    sget v2, Landroid/util/MiuiMultiWindowUtils;->sScale:F

    invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setFreeformScale(F)V

    .line 2153
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " preBounds= "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v5, p4

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " newBounds= "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v6, p5

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " newRotation= "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v7, p3

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " scale= "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v8, Landroid/util/MiuiMultiWindowUtils;->sScale:F

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v8, "MiuiFreeFormManagerService"

    invoke-static {v8, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 2155
    return v4

    .line 2145
    .end local v3    # "topRootTask":Lcom/android/server/wm/Task;
    :cond_2
    move/from16 v7, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    :goto_1
    return v2
.end method

.method public getFrontFreeformNum(I)I
    .locals 5
    .param p1, "addingTaskId"    # I

    .line 737
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 738
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    return v0

    .line 740
    :cond_0
    const/4 v0, 0x0

    .line 741
    .local v0, "frontSize":I
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 742
    .local v2, "taskId":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 743
    .local v3, "mffas_temp":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 744
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, p1, :cond_1

    .line 745
    add-int/lit8 v0, v0, 0x1

    .line 747
    .end local v2    # "taskId":Ljava/lang/Integer;
    .end local v3    # "mffas_temp":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_1
    goto :goto_0

    .line 748
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current FrontFreeformNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", addingTaskId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFreeFormManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    return v0
.end method

.method public getFrontFreeformNum(Lcom/android/server/wm/MiuiFreeFormActivityStack;)I
    .locals 3
    .param p1, "addingStack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 727
    const/4 v0, -0x1

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 728
    .local v1, "addingTaskId":I
    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 729
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isHideStackFromFullScreen()Z

    move-result v2

    if-nez v2, :cond_1

    .line 730
    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 731
    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFrontFreeformNum(I)I

    move-result v0

    return v0

    .line 733
    :cond_1
    return v0
.end method

.method public getGameFreeFormCount(Lcom/android/server/wm/MiuiFreeFormActivityStack;)I
    .locals 6
    .param p1, "addingStack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 872
    const/4 v0, 0x0

    .line 873
    .local v0, "existingGameFreeform":I
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 874
    .local v2, "taskId":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 875
    .local v3, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v5, v5, Lcom/android/server/wm/Task;->mTaskId:I

    if-eq v4, v5, :cond_0

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/util/MiuiMultiWindowAdapter;->isInTopGameList(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-boolean v4, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z

    if-eqz v4, :cond_0

    .line 877
    invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    .line 878
    invoke-virtual {v4, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 879
    add-int/lit8 v0, v0, 0x1

    .line 881
    .end local v2    # "taskId":Ljava/lang/Integer;
    .end local v3    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto :goto_0

    .line 882
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "existingGameFreeform="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFreeFormManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    return v0
.end method

.method public getMiniFreeformBounds(ILandroid/graphics/Rect;Z)Landroid/graphics/Rect;
    .locals 8
    .param p1, "taskId"    # I
    .param p2, "stableBounds"    # Landroid/graphics/Rect;
    .param p3, "rotated"    # Z

    .line 1511
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1512
    .local v0, "miniFreeformBounds":Landroid/graphics/Rect;
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v1

    .line 1513
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_2

    .line 1514
    invoke-static {}, Lcom/xiaomi/freeform/MiuiFreeformStub;->getInstance()Lcom/xiaomi/freeform/MiuiFreeformStub;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    xor-int/lit8 v4, p3, 0x1

    iget-boolean v5, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    .line 1516
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v6

    iget-boolean v7, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    .line 1514
    invoke-virtual/range {v2 .. v7}, Lcom/xiaomi/freeform/MiuiFreeformStub;->getPossibleBounds(Landroid/content/Context;ZZLjava/lang/String;Z)Landroid/graphics/RectF;

    move-result-object v2

    .line 1518
    .local v2, "possibleBounds":Landroid/graphics/RectF;
    iget v3, v2, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget v4, v2, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    iget v5, v2, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    iget v6, v2, Landroid/graphics/RectF;->bottom:F

    float-to-int v6, v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 1520
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 1521
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isLandcapeFreeform()Z

    move-result v4

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v5

    .line 1520
    invoke-static {v3, v4, v0, v5}, Landroid/util/MiuiMultiWindowUtils;->getMiniFreeformScale(Landroid/content/Context;ZLandroid/graphics/Rect;Ljava/lang/String;)F

    move-result v3

    .line 1522
    .local v3, "miniScale":F
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/util/MiuiMultiWindowUtils;->isPadScreen(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1523
    sget v4, Landroid/util/MiuiMultiWindowUtils;->ACCESSABLE_MARGIN_DIP_PAD:F

    goto :goto_0

    :cond_0
    sget v4, Landroid/util/MiuiMultiWindowUtils;->ACCESSABLE_MARGIN_DIP:F

    :goto_0
    float-to-int v4, v4

    .line 1524
    .local v4, "margin":I
    add-int/lit8 v5, v4, 0x14

    const/16 v6, 0x14

    invoke-virtual {p2, v5, v6}, Landroid/graphics/Rect;->inset(II)V

    .line 1526
    iget v5, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mCornerPosition:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 1527
    iget v5, p2, Landroid/graphics/Rect;->left:I

    .line 1528
    .local v5, "toPosX":I
    iget v6, p2, Landroid/graphics/Rect;->top:I

    .local v6, "toPosY":I
    goto :goto_1

    .line 1530
    .end local v5    # "toPosX":I
    .end local v6    # "toPosY":I
    :cond_1
    iget v5, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v3

    float-to-int v6, v6

    sub-int/2addr v5, v6

    .line 1531
    .restart local v5    # "toPosX":I
    iget v6, p2, Landroid/graphics/Rect;->top:I

    .line 1533
    .restart local v6    # "toPosY":I
    :goto_1
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1535
    .end local v2    # "possibleBounds":Landroid/graphics/RectF;
    .end local v3    # "miniScale":F
    .end local v4    # "margin":I
    .end local v5    # "toPosX":I
    .end local v6    # "toPosY":I
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " getMiniFreeformBounds taskId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stableBounds: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rotated: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " miniFreeformBounds: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFreeFormManagerService"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1537
    return-object v0
.end method

.method public getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    .locals 2
    .param p1, "taskId"    # I

    .line 633
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    return-object v0
.end method

.method public getMiuiFreeFormActivityStack(Ljava/lang/String;I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 641
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 642
    .local v1, "taskId":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 643
    .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 644
    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v3, v3, Lcom/android/server/wm/Task;->mUserId:I

    if-ne v3, p2, :cond_0

    .line 645
    return-object v2

    .line 647
    .end local v1    # "taskId":Ljava/lang/Integer;
    .end local v2    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto :goto_0

    .line 648
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .locals 2
    .param p1, "taskId"    # I

    .line 637
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    return-object v0
.end method

.method public getMiuiFreeFormStackInfo(I)Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    .locals 2
    .param p1, "taskId"    # I

    .line 196
    const/4 v0, 0x0

    .line 197
    .local v0, "res":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v1

    .line 198
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_0

    .line 199
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    move-result-object v0

    .line 201
    :cond_0
    return-object v0
.end method

.method public getMiuiFreeformBounds(ILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 3
    .param p1, "taskId"    # I
    .param p2, "originalBounds"    # Landroid/graphics/Rect;

    .line 2110
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 2111
    .local v0, "bounds":Landroid/graphics/Rect;
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeformScale(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->scale(F)V

    .line 2112
    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 2113
    return-object v0
.end method

.method public getMiuiFreeformCornerRadius(I)F
    .locals 3
    .param p1, "taskId"    # I

    .line 2117
    const/4 v0, 0x0

    .line 2118
    .local v0, "cornerRadius":F
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v1

    .line 2119
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_1

    .line 2120
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2121
    sget v0, Landroid/util/MiuiMultiWindowUtils;->FREEFORM_ROUND_CORNER_DIP_MIUI15:F

    goto :goto_0

    .line 2122
    :cond_0
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2123
    sget v0, Landroid/util/MiuiMultiWindowUtils;->MINI_FREEFORM_ROUND_CORNER_DIP_MIUI15:F

    .line 2126
    :cond_1
    :goto_0
    return v0
.end method

.method public getMiuiFreeformIsForegroundPin(I)Z
    .locals 2
    .param p1, "taskId"    # I

    .line 320
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 321
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    return v1

    .line 322
    :cond_0
    iget-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z

    return v1
.end method

.method public getMiuiFreeformMode(I)I
    .locals 2
    .param p1, "taskId"    # I

    .line 342
    const/4 v0, -0x1

    .line 343
    .local v0, "freeformMode":I
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v1

    .line 344
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_0

    .line 345
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFromWindowMode()I

    move-result v0

    .line 347
    :cond_0
    return v0
.end method

.method public getReplaceFreeForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .locals 8
    .param p1, "addingTask"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 683
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->unlockingAppLock(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 684
    return-object v1

    .line 686
    :cond_0
    const/4 v0, 0x0

    .line 687
    .local v0, "hasFreeformModeTask":Z
    const/4 v2, 0x0

    .line 688
    .local v2, "hasPinModeTask":Z
    const/4 v3, 0x0

    .line 690
    .local v3, "hasMiniFreeformModeTask":Z
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 691
    .local v5, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eq v5, p1, :cond_1

    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFromWindowMode()I

    move-result v7

    if-nez v7, :cond_1

    .line 692
    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, v5, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v7, v7, Lcom/android/server/wm/Task;->mTaskId:I

    .line 693
    invoke-virtual {p0, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 694
    or-int/lit8 v0, v0, 0x1

    .line 696
    :cond_1
    if-eq v5, p1, :cond_2

    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 697
    or-int/lit8 v2, v2, 0x1

    .line 699
    :cond_2
    if-eq v5, p1, :cond_3

    invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFromWindowMode()I

    move-result v7

    if-ne v7, v6, :cond_3

    .line 700
    or-int/lit8 v3, v3, 0x1

    .line 702
    .end local v5    # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_3
    goto :goto_0

    .line 703
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getReplaceFreeForm stacks = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " addingTask: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " hasFreeformModeTask: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " hasPinModeTask: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " hasMiniFreeformModeTask: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiFreeFormManagerService"

    invoke-static {v5, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    const/4 v4, 0x0

    .line 709
    .local v4, "replacedTask":Lcom/android/server/wm/Task;
    const/4 v7, 0x0

    if-eqz v0, :cond_5

    .line 710
    invoke-direct {p0, p1, v6, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getBottomFreeformTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)Lcom/android/server/wm/Task;

    move-result-object v4

    .line 711
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getReplaceFreeForm getBottomFreeformTask  replacedTask = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 712
    :cond_5
    if-eqz v2, :cond_6

    .line 713
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getReplacePinModeTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/Task;

    move-result-object v4

    .line 714
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getReplaceFreeForm getReplacePinModeTask  replacedTask = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 715
    :cond_6
    if-eqz v3, :cond_7

    .line 716
    invoke-direct {p0, p1, v7, v6}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getBottomFreeformTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)Lcom/android/server/wm/Task;

    move-result-object v4

    .line 718
    :cond_7
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getReplaceFreeForm replacedTask = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", hasFreeformModeTask="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", hasPinModeTask="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", hasMiniFreeformModeTask="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    if-nez v4, :cond_8

    .line 721
    return-object v1

    .line 723
    :cond_8
    invoke-virtual {v4}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v1

    return-object v1
.end method

.method public getSchemeLauncherTask()Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .locals 5

    .line 1313
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1314
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1315
    .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v3, v3, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v3, v3, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 1316
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v3, v3, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 1317
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v3

    .line 1318
    const-string v4, "com.alipay.mobile.quinox.SchemeLauncherActivity"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1319
    return-object v2

    .line 1321
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    .end local v2    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto :goto_0

    .line 1322
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getSchemeLauncherTask()Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getSchemeLauncherTask()Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    return-object v0
.end method

.method public getStackPackageName(Lcom/android/server/wm/Task;)Ljava/lang/String;
    .locals 4
    .param p1, "targetTask"    # Lcom/android/server/wm/Task;

    .line 1326
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 1327
    return-object v0

    .line 1329
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    if-eqz v1, :cond_1

    .line 1330
    iget-object v0, p1, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1332
    :cond_1
    iget-object v1, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v1, :cond_2

    .line 1333
    iget-object v0, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1335
    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1336
    invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    return-object v0

    .line 1338
    :cond_3
    return-object v0
.end method

.method public hasFreeformDesktopMemory(I)Z
    .locals 2
    .param p1, "taskId"    # I

    .line 1964
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 1965
    .local v0, "task":Lcom/android/server/wm/Task;
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z

    move-result v1

    return v1
.end method

.method public hasMiuiFreeformOnShellFeature()Z
    .locals 1

    .line 372
    const/4 v0, 0x1

    return v0
.end method

.method public hideCallingTaskIfAddSpecificChild(Lcom/android/server/wm/WindowContainer;)V
    .locals 4
    .param p1, "child"    # Lcom/android/server/wm/WindowContainer;

    .line 1375
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1376
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/util/MiuiMultiWindowAdapter;->SHOW_HIDDEN_TASK_IF_FINISHED_WHITE_LIST_ACTIVITY:Ljava/util/List;

    .line 1378
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1379
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1380
    .local v1, "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 1381
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 1382
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 1383
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/wm/Task;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 1384
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 1385
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v2, Landroid/util/MiuiMultiWindowAdapter;->HIDE_SELF_IF_NEW_FREEFORM_TASK_WHITE_LIST_ACTIVITY:Ljava/util/List;

    iget-object v3, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 1387
    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1388
    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getSyncTransaction()Landroid/view/SurfaceControl$Transaction;

    move-result-object v2

    iget-object v3, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 1390
    .end local v1    # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto/16 :goto_0

    .line 1392
    :cond_1
    return-void
.end method

.method public ignoreDeliverResultForFreeForm(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p1, "resultTo"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "resultFrom"    # Lcom/android/server/wm/ActivityRecord;

    .line 2062
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    if-eqz v0, :cond_0

    .line 2063
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->ignoreDeliverResultForFreeForm(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    return v0

    .line 2065
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public inPinMode(I)Z
    .locals 2
    .param p1, "taskId"    # I

    .line 216
    const/4 v0, 0x0

    .line 217
    .local v0, "res":Z
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v1

    .line 218
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_0

    .line 219
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v0

    .line 221
    :cond_0
    return v0
.end method

.method public init(Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 4
    .param p1, "ams"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 185
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 186
    new-instance v0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormStackDisplayStrategy:Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;

    .line 187
    new-instance v0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormCameraStrategy:Lcom/android/server/wm/MiuiFreeFormCameraStrategy;

    .line 188
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MiuiFreeFormManagerService"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 189
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 190
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    .line 191
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, p0, v3}, Lcom/android/server/wm/MiuiFreeFormGestureController;-><init>(Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/MiuiFreeFormManagerService;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    .line 193
    return-void
.end method

.method public isAppBehindHome(I)Z
    .locals 2
    .param p1, "rootStackId"    # I

    .line 678
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isAutoLayoutModeOn()Z
    .locals 1

    .line 155
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAutoLayoutModeOn:Z

    return v0
.end method

.method public isFrontFreeFormStackInfo(I)Z
    .locals 2
    .param p1, "taskId"    # I

    .line 2004
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 2005
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_0

    .line 2006
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v1

    return v1

    .line 2008
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isFullScreenStrategyNeededInDesktopMode(I)Z
    .locals 6
    .param p1, "taskId"    # I

    .line 2022
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 2023
    return v1

    .line 2024
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 2025
    .local v0, "task":Lcom/android/server/wm/Task;
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 2026
    .local v2, "ar":Lcom/android/server/wm/ActivityRecord;
    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v4

    iget-object v5, v2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2027
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v4

    iget-object v5, v2, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2028
    :cond_1
    return v3

    .line 2030
    :cond_2
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-interface {v4, v2, v1, v5}, Lcom/android/server/wm/MiuiFreeformUtilStub;->isAppSetAutoUiInDesktopMode(Lcom/android/server/wm/ActivityRecord;ILandroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2031
    return v3

    .line 2033
    :cond_3
    return v1
.end method

.method public isHideStackFromFullScreen(I)Z
    .locals 2
    .param p1, "taskId"    # I

    .line 1987
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1988
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_0

    .line 1989
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isHideStackFromFullScreen()Z

    move-result v1

    return v1

    .line 1991
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isInFreeForm(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 660
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 661
    .local v1, "taskId":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 662
    .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 663
    invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 664
    const/4 v0, 0x1

    return v0

    .line 666
    .end local v1    # "taskId":Ljava/lang/Integer;
    .end local v2    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto :goto_0

    .line 667
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isInInfiniteDragTaskResizeAnim(I)Z
    .locals 1
    .param p1, "taskId"    # I

    .line 2215
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0, p1}, Lmiui/app/IMiuiFreeformModeControl;->isInInfiniteDragTaskResizeAnim(I)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 2216
    :catch_0
    move-exception v0

    .line 2217
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2219
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return v0
.end method

.method public isInMiniFreeFormMode(I)Z
    .locals 2
    .param p1, "taskId"    # I

    .line 351
    const/4 v0, 0x0

    .line 352
    .local v0, "res":Z
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v1

    .line 353
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_0

    .line 354
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v0

    .line 356
    :cond_0
    return v0
.end method

.method public isInVideoOrGameScene()Z
    .locals 1

    .line 2043
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    if-eqz v0, :cond_0

    .line 2044
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z

    move-result v0

    return v0

    .line 2046
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isNormalFreeForm(Lcom/android/server/wm/Task;Ljava/lang/String;)Z
    .locals 5
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "pkg"    # Ljava/lang/String;

    .line 2170
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 2171
    return v0

    .line 2174
    :cond_0
    invoke-static {p2}, Landroid/util/MiuiMultiWindowAdapter;->inFreeformWhiteList(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Task "

    const-string v3, "MiuiFreeFormManagerService"

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v1, :cond_1

    .line 2175
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformResizeableWhiteList()Ljava/util/List;

    move-result-object v1

    iget-object v4, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    .line 2176
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 2182
    :cond_1
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformBlackList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is Abnormal freeform because pkg is in freeform black list"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2185
    return v0

    .line 2188
    :cond_2
    iget v0, p1, Lcom/android/server/wm/Task;->mResizeMode:I

    invoke-static {v0}, Landroid/content/pm/ActivityInfo;->isResizeableMode(I)Z

    move-result v0

    return v0

    .line 2177
    :cond_3
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is Normal freeform because pkg or activity is in freeform white list"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2179
    const/4 v0, 0x1

    return v0
.end method

.method public isSplitRootTask(I)Z
    .locals 2
    .param p1, "taskId"    # I

    .line 2036
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 2037
    .local v0, "task":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_0

    .line 2038
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->isSplitScreenRootTask()Z

    move-result v1

    return v1

    .line 2040
    :cond_0
    return v1
.end method

.method public isSupportPin()Z
    .locals 1

    .line 368
    const/4 v0, 0x1

    return v0
.end method

.method public isWpsPreStartActivity(Ljava/lang/String;)Z
    .locals 1
    .param p1, "shortComponentName"    # Ljava/lang/String;

    .line 1470
    const-string v0, "cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isWpsSameActivity(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z
    .locals 1
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "otherComponentName"    # Landroid/content/ComponentName;

    .line 1474
    invoke-static {p1, p2}, Landroid/util/MiuiMultiWindowAdapter;->isWpsSameActivity(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method

.method public launchFullscreenInDesktopModeIfNeeded(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/Task;)V
    .locals 4
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "reusedTask"    # Lcom/android/server/wm/Task;

    .line 2093
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_1

    .line 2096
    :cond_0
    invoke-static {}, Landroid/util/MiuiMultiWindowUtils;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v0

    .line 2097
    .local v0, "launchFullscreenAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2098
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2099
    iget v2, p2, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v2

    .line 2100
    .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    if-eqz v2, :cond_1

    .line 2101
    iget v3, p2, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fullscreenFreeformTask(I)V

    .line 2102
    return-void

    .line 2097
    .end local v2    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2106
    .end local v1    # "i":I
    :cond_2
    return-void

    .line 2094
    .end local v0    # "launchFullscreenAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    :goto_1
    return-void
.end method

.method public miniFreeformToFullscreenByBottomCaption(I)V
    .locals 5
    .param p1, "taskId"    # I

    .line 2081
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 2082
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    if-eqz v1, :cond_0

    .line 2083
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 2084
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    const-string v2, "MiuiFreeFormManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " miniFreeformToFullscreenByBottomCaption: taskId= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2085
    if-eqz v1, :cond_0

    .line 2086
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverResultForExitFreeform(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 2089
    .end local v1    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    monitor-exit v0

    .line 2090
    return-void

    .line 2089
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public moveTaskToBack(Lcom/android/server/wm/Task;)V
    .locals 2
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 2198
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2199
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 2200
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2201
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 2203
    :cond_1
    return-void

    .line 2198
    .end local v0    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_2
    :goto_0
    return-void
.end method

.method public moveTaskToFront(Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;)V
    .locals 3
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "options"    # Landroid/app/ActivityOptions;

    .line 1123
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1124
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1125
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_3

    .line 1126
    iget v1, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFullScreenTasksBehindHome(I)V

    .line 1128
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p2, :cond_2

    .line 1129
    invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 1130
    .local v1, "bounds":Landroid/graphics/Rect;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1131
    invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->setBounds(Landroid/graphics/Rect;)I

    .line 1134
    .end local v1    # "bounds":Landroid/graphics/Rect;
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setIsFrontFreeFormStackInfo(Z)V

    .line 1135
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isHideStackFromFullScreen()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1136
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->showFreeformIfNeeded(I)V

    .line 1141
    :cond_3
    return-void

    .line 1123
    .end local v0    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_4
    :goto_0
    return-void
.end method

.method public moveToFront(Lcom/android/server/wm/Task;Ljava/lang/String;)V
    .locals 4
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "reason"    # Ljava/lang/String;

    .line 2159
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 2160
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    if-eqz v1, :cond_1

    .line 2162
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v2

    invoke-interface {v1, v2, p2}, Lmiui/app/IMiuiFreeformModeControl;->moveToFront(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2165
    goto :goto_0

    .line 2163
    :catch_0
    move-exception v1

    .line 2164
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "MiuiFreeFormManagerService"

    const-string v3, "Error moveToFront."

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2167
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    return-void
.end method

.method public notifyCameraStateChanged(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "cameraState"    # I

    .line 1351
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormCameraStrategy:Lcom/android/server/wm/MiuiFreeFormCameraStrategy;

    .line 1352
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    goto :goto_0

    .line 1353
    :cond_0
    const/4 v1, 0x0

    .line 1351
    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->onCameraStateChanged(Ljava/lang/String;I)V

    .line 1354
    return-void
.end method

.method public onATMSSystemReady()V
    .locals 1

    .line 1507
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->onATMSSystemReady()V

    .line 1508
    return-void
.end method

.method public onActivityStackConfigurationChanged(Lcom/android/server/wm/Task;II)V
    .locals 1
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "prevWindowingMode"    # I
    .param p3, "overrideWindowingMode"    # I

    .line 623
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624
    const-string v0, "onActivityStackConfigurationChanged"

    invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;Ljava/lang/String;)V

    goto :goto_0

    .line 626
    :cond_0
    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    if-eq p3, v0, :cond_1

    .line 627
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFreeFormActivityStack(Lcom/android/server/wm/Task;Z)V

    .line 630
    :cond_1
    :goto_0
    return-void
.end method

.method public onActivityStackFirstActivityRecordAdded(I)V
    .locals 4
    .param p1, "rootId"    # I

    .line 1541
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1542
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_0

    return-void

    .line 1543
    :cond_0
    iget-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z

    if-eqz v1, :cond_2

    .line 1544
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z

    .line 1545
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1546
    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    .line 1545
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 1548
    :cond_2
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    if-eqz v1, :cond_3

    .line 1549
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormStackDisplayStrategy:Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v3, v1, v0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->onMiuiFreeFormStasckAdded(Ljava/util/concurrent/ConcurrentHashMap;Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 1551
    :cond_3
    return-void
.end method

.method public onActivityStackWindowModeSet(Lcom/android/server/wm/Task;I)V
    .locals 1
    .param p1, "rootTask"    # Lcom/android/server/wm/Task;
    .param p2, "mode"    # I

    .line 615
    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    .line 616
    const-string v0, "onActivityStackWindowModeSet"

    invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;Ljava/lang/String;)V

    goto :goto_0

    .line 618
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFreeFormActivityStack(Lcom/android/server/wm/Task;Z)V

    .line 620
    :goto_0
    return-void
.end method

.method public onExitFreeform(I)V
    .locals 2
    .param p1, "taskId"    # I

    .line 1188
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1189
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda18;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda18;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1196
    return-void
.end method

.method public onFirstWindowDrawn(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 539
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->onFirstWindowDrawn(Lcom/android/server/wm/ActivityRecord;)V

    .line 540
    return-void
.end method

.method public onShowToastIfNeeded(Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;)V
    .locals 2
    .param p1, "as"    # Lcom/android/server/wm/Task;
    .param p2, "options"    # Landroid/app/ActivityOptions;

    .line 530
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isActivityTypeHome()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 531
    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z

    if-eqz v0, :cond_2

    .line 533
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p1, Lcom/android/server/wm/Task;->mTaskId:I

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 534
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->showPropotionalFreeformToast(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 536
    .end local v0    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_2
    return-void
.end method

.method public onStartActivity(Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;Landroid/content/Intent;)V
    .locals 17
    .param p1, "as"    # Lcom/android/server/wm/Task;
    .param p2, "options"    # Landroid/app/ActivityOptions;
    .param p3, "intent"    # Landroid/content/Intent;

    .line 468
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->isActivityTypeHome()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 469
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStartActivityInner as = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " op = "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v7, "MiuiFreeFormManagerService"

    invoke-static {v7, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    if-eqz v4, :cond_1

    const-string v0, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    move-object v8, v0

    .line 471
    .local v8, "oriPkg":Ljava/lang/String;
    const/4 v9, 0x0

    if-eqz v4, :cond_2

    const-string v0, "originating_uid"

    invoke-virtual {v4, v0, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_1

    :cond_2
    move v0, v9

    :goto_1
    move v10, v0

    .line 472
    .local v10, "oriUid":I
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    if-eqz v2, :cond_4

    .line 473
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 474
    iput-object v8, v2, Lcom/android/server/wm/Task;->behindAppLockPkg:Ljava/lang/String;

    .line 475
    iput v10, v2, Lcom/android/server/wm/Task;->originatingUid:I

    goto :goto_2

    .line 477
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 478
    .local v0, "task":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_4

    .line 479
    iput-object v8, v0, Lcom/android/server/wm/Task;->behindAppLockPkg:Ljava/lang/String;

    .line 480
    iput v10, v0, Lcom/android/server/wm/Task;->originatingUid:I

    .line 484
    .end local v0    # "task":Lcom/android/server/wm/Task;
    :cond_4
    :goto_2
    if-eqz v3, :cond_8

    invoke-virtual/range {p2 .. p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v0

    const/4 v11, 0x5

    if-ne v0, v11, :cond_8

    if-eqz v2, :cond_8

    iget-object v0, v2, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z

    if-eqz v0, :cond_8

    .line 486
    const-string v0, "getActivityOptionsInjector"

    new-array v11, v9, [Ljava/lang/Object;

    invoke-static {v3, v0, v11}, Landroid/util/MiuiMultiWindowUtils;->isMethodExist(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v11

    .line 487
    .local v11, "method":Ljava/lang/reflect/Method;
    if-eqz v11, :cond_8

    .line 489
    :try_start_0
    new-array v0, v9, [Ljava/lang/Object;

    .line 490
    invoke-virtual {v11, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v12, "getFreeformAnimation"

    new-array v13, v9, [Ljava/lang/Object;

    .line 489
    invoke-static {v0, v12, v13}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 496
    .local v0, "needAnimation":Z
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z

    move-result v12

    if-eqz v12, :cond_5

    move-object v12, v2

    goto :goto_3

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v12

    :goto_3
    const-string v13, "onStartActivity"

    invoke-virtual {v1, v12, v13}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;Ljava/lang/String;)V

    .line 497
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v12

    invoke-virtual {v1, v12}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v12

    .line 498
    .local v12, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    new-array v13, v9, [Ljava/lang/Object;

    invoke-virtual {v11, v3, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    const-string v14, "isNormalFreeForm"

    new-array v15, v9, [Ljava/lang/Object;

    invoke-static {v13, v14, v15}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    iput-boolean v13, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    .line 499
    invoke-virtual {v12, v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setNeedAnimation(Z)V

    .line 500
    new-array v13, v9, [Ljava/lang/Object;

    invoke-virtual {v11, v3, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    const-string v14, "getFreeformScale"

    new-array v15, v9, [Ljava/lang/Object;

    invoke-static {v13, v14, v15}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Float;

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v13

    .line 501
    .local v13, "scaleInOptions":F
    const/high16 v14, -0x40800000    # -1.0f

    cmpl-float v14, v13, v14

    if-eqz v14, :cond_6

    move/from16 v16, v0

    move v0, v13

    goto :goto_4

    .line 502
    :cond_6
    iget-object v14, v1, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v14, v14, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iget-boolean v15, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    iget-boolean v9, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    move/from16 v16, v0

    .end local v0    # "needAnimation":Z
    .local v16, "needAnimation":Z
    iget-object v0, v1, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {v12}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v14, v15, v9, v0, v4}, Landroid/util/MiuiMultiWindowUtils;->getOriFreeformScale(Landroid/content/Context;ZZZLjava/lang/String;)F

    move-result v0

    :goto_4
    iput v0, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    .line 503
    invoke-virtual/range {p2 .. p2}, Landroid/app/ActivityOptions;->getLaunchFromTaskId()I

    move-result v0

    if-eqz v0, :cond_7

    .line 504
    invoke-virtual/range {p2 .. p2}, Landroid/app/ActivityOptions;->getLaunchFromTaskId()I

    move-result v0

    iput v0, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormLaunchFromTaskId:I

    .line 508
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " options.getLaunchFromTaskId()= "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 509
    invoke-virtual/range {p2 .. p2}, Landroid/app/ActivityOptions;->getLaunchFromTaskId()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 508
    invoke-static {v7, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 511
    .end local v12    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v13    # "scaleInOptions":F
    .end local v16    # "needAnimation":Z
    goto :goto_5

    .line 510
    :catch_0
    move-exception v0

    .line 514
    .end local v11    # "method":Ljava/lang/reflect/Method;
    :cond_8
    :goto_5
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 515
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 516
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_c

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v4

    if-nez v4, :cond_c

    .line 517
    :cond_9
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_c

    iget-object v4, v2, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    .line 518
    invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 519
    iget-object v4, v2, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v4

    iget-object v5, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v4, v5}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F

    move-result v4

    .line 520
    .local v4, "lastScale":F
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v5

    invoke-virtual {v1, v5, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setMiuiFreeformScale(IF)V

    .line 521
    iget-object v5, v2, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v5

    iget-object v6, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v5, v6}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastPosition(Lcom/android/server/wm/Task;)Landroid/graphics/Rect;

    move-result-object v5

    .line 522
    .local v5, "lastBounds":Landroid/graphics/Rect;
    if-eqz v5, :cond_b

    .line 523
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v7

    if-le v6, v7, :cond_a

    const/4 v9, 0x1

    goto :goto_6

    :cond_a
    const/4 v9, 0x0

    :goto_6
    iput-boolean v9, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    .line 524
    :cond_b
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getStackPackageName(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isNormalFreeForm(Lcom/android/server/wm/Task;Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    .line 527
    .end local v0    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v4    # "lastScale":F
    .end local v5    # "lastBounds":Landroid/graphics/Rect;
    :cond_c
    return-void
.end method

.method public openCameraInFreeForm(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1357
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormCameraStrategy:Lcom/android/server/wm/MiuiFreeFormCameraStrategy;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->openCameraInFreeForm(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public registerFreeformCallback(Lmiui/app/IFreeformCallback;)V
    .locals 2
    .param p1, "freeformCallback"    # Lmiui/app/IFreeformCallback;

    .line 1092
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    monitor-enter v0

    .line 1093
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 1094
    monitor-exit v0

    .line 1095
    return-void

    .line 1094
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerMiuiFreeformModeControl(Lmiui/app/IMiuiFreeformModeControl;)V
    .locals 3
    .param p1, "freeformModeControl"    # Lmiui/app/IMiuiFreeformModeControl;

    .line 131
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 132
    invoke-interface {v0}, Lmiui/app/IMiuiFreeformModeControl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    invoke-interface {v0}, Lmiui/app/IMiuiFreeformModeControl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControlDeath:Landroid/os/IBinder$DeathRecipient;

    invoke-interface {v0, v2, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 135
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->detachFreeformModeControl()V

    .line 137
    :cond_1
    invoke-interface {p1}, Lmiui/app/IMiuiFreeformModeControl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 138
    invoke-interface {p1}, Lmiui/app/IMiuiFreeformModeControl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControlDeath:Landroid/os/IBinder$DeathRecipient;

    invoke-interface {v0, v2, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 140
    :cond_2
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    nop

    .line 144
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to set transition player"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public removeFreeFormActivityStack(Lcom/android/server/wm/Task;Z)V
    .locals 4
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "stackRemoved"    # Z

    .line 588
    if-eqz p1, :cond_7

    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 591
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 592
    .local v0, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_1

    return-void

    .line 593
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeFreeFormActivityStack as = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFreeFormManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->setAlwaysOnTop(Z)V

    .line 595
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 596
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    const/high16 v3, -0x80000000

    invoke-virtual {v2, v3, p1, v1}, Lcom/android/server/wm/TaskDisplayArea;->positionChildAt(ILcom/android/server/wm/WindowContainer;Z)V

    .line 598
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFromWindowMode()I

    move-result v1

    .line 599
    .local v1, "windowMode":I
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V

    .line 600
    const/4 v2, 0x3

    if-nez v1, :cond_3

    .line 601
    invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V

    goto :goto_0

    .line 602
    :cond_3
    const/4 v3, 0x1

    if-ne v1, v3, :cond_4

    .line 603
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V

    goto :goto_0

    .line 604
    :cond_4
    const/4 v3, 0x2

    if-ne v1, v3, :cond_5

    .line 605
    const/16 v2, 0x10

    invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V

    goto :goto_0

    .line 606
    :cond_5
    if-ne v1, v2, :cond_6

    .line 607
    const/16 v2, 0x11

    invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 609
    :cond_6
    :goto_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setCameraRotationIfNeeded()V

    .line 612
    return-void

    .line 589
    .end local v0    # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v1    # "windowMode":I
    :cond_7
    :goto_1
    return-void
.end method

.method public removeFullScreenTasksBehindHome(I)V
    .locals 2
    .param p1, "taskId"    # I

    .line 2015
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAppBehindHome:Landroid/util/ArraySet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    .line 2016
    return-void
.end method

.method public restoreMiniToFreeformMode(I)V
    .locals 2
    .param p1, "taskId"    # I

    .line 1155
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1156
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda15;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda15;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1163
    return-void
.end method

.method public setAdjustedForIme(ZIZ)V
    .locals 2
    .param p1, "adjustedForIme"    # Z
    .param p2, "imeHeight"    # I
    .param p3, "adjustedForRotation"    # Z

    .line 173
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 174
    .local v1, "imeShowing":Z
    :goto_0
    if-eqz v1, :cond_1

    move v0, p2

    :cond_1
    move p2, v0

    .line 175
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mIsImeShowing:Z

    if-ne v1, v0, :cond_2

    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mImeHeight:I

    if-ne p2, v0, :cond_2

    .line 176
    return-void

    .line 179
    :cond_2
    iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mIsImeShowing:Z

    .line 180
    iput p2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mImeHeight:I

    .line 181
    invoke-direct {p0, v1, p2, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->notifyImeVisibilityChanged(ZIZ)V

    .line 182
    return-void
.end method

.method public setApplicationUsedInFreeform(Ljava/lang/String;)V
    .locals 3
    .param p1, "applicationUsedInFreeform"    # Ljava/lang/String;

    .line 1109
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1111
    .local v0, "origId":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p1, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setApplicationUsedInFreeform(Ljava/lang/String;Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1113
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1114
    nop

    .line 1115
    return-void

    .line 1113
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1114
    throw v2
.end method

.method public setApplicationUsedInFreeform(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p1, "applicationUsedInFreeform"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .line 1118
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "application_used_freeform"

    const/4 v2, -0x2

    invoke-static {v0, v1, p1, v2}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 1120
    return-void
.end method

.method public setCameraOrientation(I)V
    .locals 1
    .param p1, "newRotation"    # I

    .line 1371
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormCameraStrategy:Lcom/android/server/wm/MiuiFreeFormCameraStrategy;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V

    .line 1372
    return-void
.end method

.method public setCameraRotationIfNeeded()V
    .locals 1

    .line 584
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormCameraStrategy:Lcom/android/server/wm/MiuiFreeFormCameraStrategy;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->rotateCameraIfNeeded()V

    .line 585
    return-void
.end method

.method public setFreeformForceHideIfNeed(Ljava/util/List;ILjava/util/Map$Entry;Lcom/android/server/wm/WindowContainer;)V
    .locals 8
    .param p2, "hopSize"    # I
    .param p4, "wc"    # Lcom/android/server/wm/WindowContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/window/WindowContainerTransaction$HierarchyOp;",
            ">;I",
            "Ljava/util/Map$Entry<",
            "Landroid/os/IBinder;",
            "Landroid/window/WindowContainerTransaction$Change;",
            ">;",
            "Lcom/android/server/wm/WindowContainer;",
            ")V"
        }
    .end annotation

    .line 226
    .local p1, "hops":Ljava/util/List;, "Ljava/util/List<Landroid/window/WindowContainerTransaction$HierarchyOp;>;"
    .local p3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/os/IBinder;Landroid/window/WindowContainerTransaction$Change;>;"
    const/4 v0, 0x0

    .line 227
    .local v0, "reorderToBack":Z
    const/4 v1, 0x0

    .line 229
    .local v1, "unsetAlwaysOnTop":Z
    invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->getTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 230
    invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->getTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/TaskDisplayArea;->inFreeformWindowingMode()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 231
    .local v2, "isFreeformDisplayArea":Z
    :goto_0
    invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->inFreeformWindowingMode()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 232
    invoke-interface {p3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/window/WindowContainerTransaction$Change;

    invoke-virtual {v4}, Landroid/window/WindowContainerTransaction$Change;->getWindowingMode()I

    move-result v4

    if-nez v4, :cond_6

    if-nez v2, :cond_6

    .line 234
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, p2, :cond_3

    .line 235
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/window/WindowContainerTransaction$HierarchyOp;

    .line 236
    .local v5, "hop":Landroid/window/WindowContainerTransaction$HierarchyOp;
    invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getType()I

    move-result v6

    if-eq v6, v3, :cond_1

    goto :goto_2

    .line 237
    :cond_1
    nop

    .line 238
    invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getContainer()Landroid/os/IBinder;

    move-result-object v6

    .line 237
    invoke-static {v6}, Lcom/android/server/wm/WindowContainer;->fromBinder(Landroid/os/IBinder;)Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    .line 239
    .local v6, "hopWc":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {p4, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    goto :goto_2

    .line 240
    :cond_2
    invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getToTop()Z

    move-result v7

    xor-int/2addr v7, v3

    move v0, v7

    .line 234
    .end local v5    # "hop":Landroid/window/WindowContainerTransaction$HierarchyOp;
    .end local v6    # "hopWc":Lcom/android/server/wm/WindowContainer;
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 242
    .end local v4    # "i":I
    :cond_3
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_3
    if-ge v4, p2, :cond_6

    .line 243
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/window/WindowContainerTransaction$HierarchyOp;

    .line 244
    .restart local v5    # "hop":Landroid/window/WindowContainerTransaction$HierarchyOp;
    invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getType()I

    move-result v6

    const/16 v7, 0xc

    if-eq v6, v7, :cond_4

    goto :goto_4

    .line 245
    :cond_4
    nop

    .line 246
    invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getContainer()Landroid/os/IBinder;

    move-result-object v6

    .line 245
    invoke-static {v6}, Lcom/android/server/wm/WindowContainer;->fromBinder(Landroid/os/IBinder;)Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    .line 247
    .restart local v6    # "hopWc":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {p4, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    goto :goto_4

    .line 248
    :cond_5
    invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->isAlwaysOnTop()Z

    move-result v7

    xor-int/2addr v7, v3

    move v1, v7

    .line 242
    .end local v5    # "hop":Landroid/window/WindowContainerTransaction$HierarchyOp;
    .end local v6    # "hopWc":Lcom/android/server/wm/WindowContainer;
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 251
    .end local v4    # "i":I
    :cond_6
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    .line 252
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setforceHiden wc: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " hops: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiFreeFormManagerService"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v4

    const/high16 v5, 0x10000

    invoke-virtual {v4, v5, v3}, Lcom/android/server/wm/Task;->setForceHidden(IZ)Z

    .line 255
    :cond_7
    return-void
.end method

.method public setFrontFreeFormStackInfo(IZ)V
    .locals 3
    .param p1, "taskId"    # I
    .param p2, "isFront"    # Z

    .line 1996
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1997
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_0

    .line 1998
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " setFrontFreeFormStackInfo taskId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isFront="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mffas="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFreeFormManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1999
    invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setIsFrontFreeFormStackInfo(Z)V

    .line 2001
    :cond_0
    return-void
.end method

.method public setHideStackFromFullScreen(IZ)V
    .locals 6
    .param p1, "taskId"    # I
    .param p2, "hidden"    # Z

    .line 1969
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1970
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_1

    .line 1971
    const-string v1, "MiuiFreeFormManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " setHideStackFromFullScreen taskId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", hiden="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mffas="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1972
    invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setHideStackFromFullScreen(Z)V

    .line 1973
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 1975
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 1976
    .local v2, "curentTask":Lcom/android/server/wm/Task;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p2, :cond_0

    .line 1977
    invoke-virtual {v2, v3}, Lcom/android/server/wm/Task;->getNextFocusableTask(Z)Lcom/android/server/wm/Task;

    move-result-object v3

    .line 1978
    .local v3, "nextFocusdTask":Lcom/android/server/wm/Task;
    if-eqz v3, :cond_0

    .line 1979
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->setFocusedRootTask(I)V

    .line 1982
    .end local v2    # "curentTask":Lcom/android/server/wm/Task;
    .end local v3    # "nextFocusdTask":Lcom/android/server/wm/Task;
    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1984
    :cond_1
    :goto_0
    return-void
.end method

.method public setMiuiFreeFormTouchExcludeRegion(Landroid/graphics/Region;Landroid/graphics/Region;)V
    .locals 3
    .param p1, "region"    # Landroid/graphics/Region;
    .param p2, "region2"    # Landroid/graphics/Region;

    .line 1227
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeformModeControl:Lmiui/app/IMiuiFreeformModeControl;

    if-eqz v0, :cond_0

    .line 1229
    :try_start_0
    invoke-interface {v0, p1, p2}, Lmiui/app/IMiuiFreeformModeControl;->setMiuiFreeFormTouchExcludeRegion(Landroid/graphics/Region;Landroid/graphics/Region;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1232
    goto :goto_0

    .line 1230
    :catch_0
    move-exception v0

    .line 1231
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MiuiFreeFormManagerService"

    const-string/jumbo v2, "setMiuiFreeFormTouchExcludeRegion."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1234
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method

.method public setMiuiFreeformIsForegroundPin(IZ)V
    .locals 1
    .param p1, "taskId"    # I
    .param p2, "isForegroundPin"    # Z

    .line 308
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 309
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_0

    return-void

    .line 310
    :cond_0
    iput-boolean p2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z

    .line 311
    return-void
.end method

.method public setMiuiFreeformIsNeedAnimation(IZ)V
    .locals 1
    .param p1, "taskId"    # I
    .param p2, "needAnimation"    # Z

    .line 314
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 315
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_0

    return-void

    .line 316
    :cond_0
    invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setNeedAnimation(Z)V

    .line 317
    return-void
.end method

.method public setMiuiFreeformMode(II)V
    .locals 1
    .param p1, "taskId"    # I
    .param p2, "miuiFreeformMode"    # I

    .line 335
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 336
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V

    .line 339
    :cond_0
    return-void
.end method

.method public setMiuiFreeformOrientation(IZ)V
    .locals 1
    .param p1, "taskId"    # I
    .param p2, "isLandscape"    # Z

    .line 290
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 291
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_0

    return-void

    .line 292
    :cond_0
    iput-boolean p2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    .line 293
    return-void
.end method

.method public setMiuiFreeformPinPos(ILandroid/graphics/Rect;)V
    .locals 2
    .param p1, "taskId"    # I
    .param p2, "pinPos"    # Landroid/graphics/Rect;

    .line 296
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 297
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_0

    return-void

    .line 298
    :cond_0
    iget-object v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mPinFloatingWindowPos:Landroid/graphics/Rect;

    invoke-virtual {v1, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 299
    return-void
.end method

.method public setMiuiFreeformPinedActiveTime(IJ)V
    .locals 1
    .param p1, "taskId"    # I
    .param p2, "activeTime"    # J

    .line 302
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 303
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_0

    return-void

    .line 304
    :cond_0
    iput-wide p2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->pinActiveTime:J

    .line 305
    return-void
.end method

.method public setMiuiFreeformScale(IF)V
    .locals 3
    .param p1, "taskId"    # I
    .param p2, "miuiFreeformScale"    # F

    .line 283
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 284
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-nez v0, :cond_0

    return-void

    .line 285
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormScale()F

    move-result v1

    sub-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3a83126f    # 0.001f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    return-void

    .line 286
    :cond_1
    invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setFreeformScale(F)V

    .line 287
    return-void
.end method

.method public setRequestedOrientation(ILcom/android/server/wm/Task;Z)V
    .locals 3
    .param p1, "requestedOrientation"    # I
    .param p2, "task"    # Lcom/android/server/wm/Task;
    .param p3, "noAnimation"    # Z

    .line 1199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setRequestedOrientation task: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " requestedOrientation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreeFormManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    .line 1202
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1203
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_1

    .line 1204
    invoke-static {p1}, Landroid/util/MiuiMultiWindowUtils;->isOrientationLandscape(I)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    .line 1206
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda14;

    invoke-direct {v2, p0, p2, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda14;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/Task;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1213
    return-void

    .line 1201
    .end local v0    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_2
    :goto_0
    return-void
.end method

.method public shouldAddingToTask(Ljava/lang/String;)Z
    .locals 1
    .param p1, "shortComponentName"    # Ljava/lang/String;

    .line 364
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getLaunchInTaskList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public shouldAdjustFreeformLayer(ILandroid/window/TransitionInfo;)Z
    .locals 6
    .param p1, "type"    # I
    .param p2, "info"    # Landroid/window/TransitionInfo;

    .line 2239
    const/4 v0, 0x0

    .line 2240
    .local v0, "shouldAdjustLayer":Z
    const/4 v1, 0x1

    if-eqz p2, :cond_1

    .line 2241
    invoke-virtual {p2}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 2242
    invoke-virtual {p2}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/window/TransitionInfo$Change;

    .line 2243
    .local v3, "c":Landroid/window/TransitionInfo$Change;
    invoke-virtual {v3}, Landroid/window/TransitionInfo$Change;->getContainer()Landroid/window/WindowContainerToken;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2244
    invoke-virtual {v3}, Landroid/window/TransitionInfo$Change;->getContainer()Landroid/window/WindowContainerToken;

    move-result-object v4

    invoke-virtual {v4}, Landroid/window/WindowContainerToken;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/android/server/wm/WindowContainer;->fromBinder(Landroid/os/IBinder;)Lcom/android/server/wm/WindowContainer;

    move-result-object v4

    .line 2245
    .local v4, "wc":Lcom/android/server/wm/WindowContainer;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->inFreeformWindowingMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2246
    const/4 v0, 0x1

    .line 2247
    goto :goto_1

    .line 2241
    .end local v3    # "c":Landroid/window/TransitionInfo$Change;
    .end local v4    # "wc":Lcom/android/server/wm/WindowContainer;
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 2253
    .end local v2    # "i":I
    :cond_1
    :goto_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_2
    if-eq p1, v1, :cond_3

    const/4 v2, 0x3

    if-ne p1, v2, :cond_5

    :cond_3
    if-eqz v0, :cond_5

    .line 2256
    :cond_4
    return v1

    .line 2258
    :cond_5
    const/4 v1, 0x0

    return v1
.end method

.method public shouldForegroundPin(I)Z
    .locals 2
    .param p1, "taskId"    # I

    .line 1342
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v0

    .line 1343
    .local v0, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-nez v1, :cond_0

    goto :goto_0

    .line 1346
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->needForegroundPin(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z

    .line 1347
    iget-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z

    return v1

    .line 1344
    :cond_1
    :goto_0
    const/4 v1, 0x0

    return v1
.end method

.method public showHiddenTaskIfRemoveSpecificChild(Lcom/android/server/wm/WindowContainer;)V
    .locals 4
    .param p1, "child"    # Lcom/android/server/wm/WindowContainer;

    .line 1395
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1396
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/util/MiuiMultiWindowAdapter;->SHOW_HIDDEN_TASK_IF_FINISHED_WHITE_LIST_ACTIVITY:Ljava/util/List;

    .line 1398
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1399
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1400
    .local v1, "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 1401
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 1402
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 1403
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/wm/Task;->mCallingPackage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 1404
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 1405
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v2, Landroid/util/MiuiMultiWindowAdapter;->HIDE_SELF_IF_NEW_FREEFORM_TASK_WHITE_LIST_ACTIVITY:Ljava/util/List;

    iget-object v3, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 1407
    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1408
    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getSyncTransaction()Landroid/view/SurfaceControl$Transaction;

    move-result-object v2

    iget-object v3, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 1410
    .end local v1    # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_0
    goto/16 :goto_0

    .line 1412
    :cond_1
    return-void
.end method

.method public showOpenMiuiOptimizationToast()V
    .locals 2

    .line 1415
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$2;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeFormManagerService$2;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1424
    return-void
.end method

.method public showPropotionalFreeformToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "applicationUsedInFreeform"    # Ljava/lang/String;

    .line 563
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$1;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$1;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 574
    return-void
.end method

.method public skipStartActivity(Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/TaskDisplayArea;)Z
    .locals 5
    .param p1, "reusedTask"    # Lcom/android/server/wm/Task;
    .param p2, "activityOption"    # Landroid/app/ActivityOptions;
    .param p3, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p5, "taskDisplayArea"    # Lcom/android/server/wm/TaskDisplayArea;

    .line 1249
    const/4 v0, 0x1

    const-string v1, "MiuiFreeFormManagerService"

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/server/wm/MiuiSoScManagerStub;->isInSoScMode(Lcom/android/server/wm/WindowContainer;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p2, :cond_0

    .line 1250
    invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    .line 1251
    iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fromSoscToFreeform(I)V

    .line 1252
    const-string v2, "Skip start activity from sosc to freeform."

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    return v0

    .line 1256
    :cond_0
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 1257
    .local v2, "sourceTask":Lcom/android/server/wm/Task;
    :goto_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isMiuiFreeformExiting()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz p2, :cond_2

    .line 1259
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v3

    invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v4

    invoke-virtual {v3, v4, p3, v2, p5}, Lcom/android/server/wm/MiuiSoScManagerStub;->validateWindowingModeForSoSc(ILcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/Task;Lcom/android/server/wm/TaskDisplayArea;)I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_2

    .line 1261
    const-string v3, "Skip start activity via freeform is exiting"

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    return v0

    .line 1265
    :cond_2
    invoke-virtual {p0, p1, p2, p4, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->activeOrFullscreenFreeformTaskIfNeed(Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    return v0
.end method

.method public startSmallFreeformFromNotification()I
    .locals 1

    .line 543
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->startSmallFreeformFromNotification()I

    move-result v0

    return v0
.end method

.method public traverseTask(Lcom/android/server/wm/Task;Ljava/util/function/Consumer;)V
    .locals 6
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/wm/Task;",
            "Ljava/util/function/Consumer<",
            "Lcom/android/server/wm/ActivityRecord;",
            ">;)V"
        }
    .end annotation

    .line 1427
    .local p2, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Lcom/android/server/wm/ActivityRecord;>;"
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 1428
    if-nez p1, :cond_0

    .line 1429
    :try_start_0
    monitor-exit v0

    return-void

    .line 1432
    :cond_0
    const/4 v1, -0x2

    .line 1434
    .local v1, "candidate":I
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "activityIndex":I
    :goto_0
    if-ltz v2, :cond_6

    .line 1435
    invoke-virtual {p1, v2}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 1436
    .local v3, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    if-nez v3, :cond_1

    .line 1437
    goto :goto_1

    .line 1439
    :cond_1
    invoke-virtual {v3, v1}, Lcom/android/server/wm/ActivityRecord;->getOrientation(I)I

    move-result v4

    .line 1440
    .local v4, "orientation":I
    move v1, v4

    .line 1446
    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    const/16 v5, 0xd

    if-eq v4, v5, :cond_5

    const/16 v5, 0xa

    if-eq v4, v5, :cond_5

    const/4 v5, 0x4

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_5

    const/16 v5, 0xe

    if-ne v4, v5, :cond_2

    goto :goto_2

    .line 1455
    :cond_2
    const/4 v5, 0x3

    if-eq v4, v5, :cond_4

    const/4 v5, -0x2

    if-ne v4, v5, :cond_3

    .line 1457
    goto :goto_1

    .line 1463
    :cond_3
    invoke-interface {p2, v3}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    .line 1464
    monitor-exit v0

    return-void

    .line 1434
    .end local v3    # "activityRecord":Lcom/android/server/wm/ActivityRecord;
    .end local v4    # "orientation":I
    :cond_4
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1452
    .restart local v3    # "activityRecord":Lcom/android/server/wm/ActivityRecord;
    .restart local v4    # "orientation":I
    :cond_5
    :goto_2
    monitor-exit v0

    return-void

    .line 1466
    .end local v1    # "candidate":I
    .end local v2    # "activityIndex":I
    .end local v3    # "activityRecord":Lcom/android/server/wm/ActivityRecord;
    .end local v4    # "orientation":I
    :cond_6
    monitor-exit v0

    .line 1467
    return-void

    .line 1466
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unPinFloatingWindowForActive(I)V
    .locals 2
    .param p1, "taskId"    # I

    .line 1144
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 1145
    :cond_0
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda13;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda13;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1152
    return-void
.end method

.method public unSupportedFreeformInDesktop(I)Z
    .locals 2
    .param p1, "taskId"    # I

    .line 2192
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v0

    new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda7;

    invoke-direct {v1, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda7;-><init>(I)V

    .line 2193
    invoke-virtual {v0, v1}, Lcom/android/server/wm/TaskDisplayArea;->getRootTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 2194
    .local v0, "target":Lcom/android/server/wm/Task;
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformUtilStub;->getInstance()Lcom/android/server/wm/MiuiFreeformUtilStub;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/android/server/wm/MiuiFreeformUtilStub;->isSupportFreeFormInDesktopMode(Lcom/android/server/wm/Task;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    return v1
.end method

.method public unregisterFreeformCallback(Lmiui/app/IFreeformCallback;)V
    .locals 2
    .param p1, "freeformCallback"    # Lmiui/app/IFreeformCallback;

    .line 1098
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    monitor-enter v0

    .line 1099
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 1100
    monitor-exit v0

    .line 1101
    return-void

    .line 1100
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unsetFreeformForceHideIfNeed(Landroid/window/WindowContainerTransaction$HierarchyOp;Lcom/android/server/wm/WindowContainer;)V
    .locals 3
    .param p1, "hop"    # Landroid/window/WindowContainerTransaction$HierarchyOp;
    .param p2, "container"    # Lcom/android/server/wm/WindowContainer;

    .line 259
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->isForceHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/window/WindowContainerTransaction$HierarchyOp;->isAlwaysOnTop()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unsetforceHiden wc: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreeFormManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v0

    const/high16 v1, 0x10000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/Task;->setForceHidden(IZ)Z

    .line 263
    :cond_0
    return-void
.end method

.method public updateAutoLayoutModeStatus(Z)V
    .locals 0
    .param p1, "isOn"    # Z

    .line 162
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAutoLayoutModeOn:Z

    .line 163
    return-void
.end method

.method updateDataFromSetting()V
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getApplicationUsedInFreeform(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mApplicationUsedInFreeform:Ljava/lang/String;

    .line 361
    return-void
.end method
