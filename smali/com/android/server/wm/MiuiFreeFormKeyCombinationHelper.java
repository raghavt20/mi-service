public class com.android.server.wm.MiuiFreeFormKeyCombinationHelper {
	 /* .source "MiuiFreeFormKeyCombinationHelper.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.wm.MiuiFreeFormGestureController mGestureController;
	 private Boolean mIsHandleKeyCombination;
	 /* # direct methods */
	 com.android.server.wm.MiuiFreeFormKeyCombinationHelper ( ) {
		 /* .locals 1 */
		 /* .param p1, "gestureController" # Lcom/android/server/wm/MiuiFreeFormGestureController; */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 18 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z */
		 /* .line 21 */
		 this.mGestureController = p1;
		 /* .line 22 */
		 return;
	 } // .end method
	 private java.lang.String getActivityName ( android.content.Context p0, com.android.server.wm.ActivityRecord p1 ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 91 */
		 (( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
		 /* .line 92 */
		 /* .local v0, "pm":Landroid/content/pm/PackageManager; */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 93 */
			 v1 = this.info;
			 v1 = this.applicationInfo;
			 (( android.content.pm.ApplicationInfo ) v1 ).loadLabel ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
			 /* .line 95 */
		 } // :cond_0
		 final String v1 = ""; // const-string v1, ""
	 } // .end method
	 static Boolean lambda$freeFormAndFullScreenToggleByKeyCombination$0 ( com.android.server.wm.ActivityRecord p0 ) { //synthethic
		 /* .locals 1 */
		 /* .param p0, "app" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 40 */
		 int v0 = 3; // const/4 v0, 0x3
		 v0 = 		 (( com.android.server.wm.ActivityRecord ) p0 ).isAnimating ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/ActivityRecord;->isAnimating(I)Z
	 } // .end method
	 static void lambda$showNotSupportToast$1 ( android.content.Context p0, java.lang.String p1 ) { //synthethic
		 /* .locals 3 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "activityName" # Ljava/lang/String; */
		 /* .line 101 */
		 (( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
		 /* const v1, 0x110f029e */
		 /* filled-new-array {p1}, [Ljava/lang/Object; */
		 (( android.content.res.Resources ) v0 ).getString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
		 /* .line 102 */
		 /* .local v0, "tips":Ljava/lang/String; */
		 int v1 = 0; // const/4 v1, 0x0
		 android.widget.Toast .makeText ( p0,v0,v1 );
		 (( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
		 /* .line 103 */
		 return;
	 } // .end method
	 private void showNotSupportToast ( android.content.Context p0, java.lang.String p1 ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "activityName" # Ljava/lang/String; */
		 /* .line 99 */
		 v0 = this.mGestureController;
		 v0 = this.mHandler;
		 /* new-instance v1, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper$$ExternalSyntheticLambda1; */
		 /* invoke-direct {v1, p1, p2}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper$$ExternalSyntheticLambda1;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
		 (( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
		 /* .line 104 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void exitFreeFormByKeyCombination ( com.android.server.wm.Task p0 ) {
		 /* .locals 5 */
		 /* .param p1, "task" # Lcom/android/server/wm/Task; */
		 /* .line 107 */
		 v0 = this.mGestureController;
		 v0 = this.mMiuiFreeFormManagerService;
		 /* .line 108 */
		 v1 = 		 (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
		 (( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).getMiuiFreeFormActivityStack ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
		 /* check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
		 /* .line 109 */
		 /* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 v1 = 			 (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInMiniFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 110 */
				 return;
				 /* .line 112 */
			 } // :cond_0
			 int v1 = 1; // const/4 v1, 0x1
			 (( com.android.server.wm.Task ) p1 ).getTopActivity ( v1, v1 ); // invoke-virtual {p1, v1, v1}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
			 /* .line 113 */
			 /* .local v2, "ar":Lcom/android/server/wm/ActivityRecord; */
			 int v3 = 0; // const/4 v3, 0x0
			 if ( v2 != null) { // if-eqz v2, :cond_1
				 /* .line 114 */
				 (( com.android.server.wm.Task ) p1 ).setAlwaysOnTop ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/server/wm/Task;->setAlwaysOnTop(Z)V
				 /* .line 115 */
				 android.app.ActivityClient .getInstance ( );
				 v4 = this.token;
				 (( android.app.ActivityClient ) v3 ).moveActivityTaskToBack ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Landroid/app/ActivityClient;->moveActivityTaskToBack(Landroid/os/IBinder;Z)Z
				 /* .line 117 */
			 } // :cond_1
			 (( com.android.server.wm.Task ) p1 ).setAlwaysOnTop ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/server/wm/Task;->setAlwaysOnTop(Z)V
			 /* .line 118 */
			 (( com.android.server.wm.Task ) p1 ).moveTaskToBack ( p1 ); // invoke-virtual {p1, p1}, Lcom/android/server/wm/Task;->moveTaskToBack(Lcom/android/server/wm/Task;)Z
			 /* .line 120 */
		 } // :goto_0
		 v1 = 		 com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
		 if ( v1 != null) { // if-eqz v1, :cond_2
			 /* .line 121 */
			 v1 = this.mGestureController;
			 v1 = this.mMiuiFreeFormManagerService;
			 v3 = 			 (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
			 (( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).autoLayoutOthersIfNeed ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->autoLayoutOthersIfNeed(I)V
			 /* .line 123 */
		 } // :cond_2
		 return;
	 } // .end method
	 public void freeFormAndFullScreenToggleByKeyCombination ( Boolean p0 ) {
		 /* .locals 6 */
		 /* .param p1, "isStartFreeForm" # Z */
		 /* .line 25 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 26 */
		 /* .local v0, "focusedTask":Lcom/android/server/wm/Task; */
		 v1 = this.mGestureController;
		 v1 = this.mDisplayContent;
		 v1 = this.mFocusedApp;
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 27 */
			 v1 = this.mGestureController;
			 v1 = this.mDisplayContent;
			 v1 = this.mFocusedApp;
			 (( com.android.server.wm.ActivityRecord ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
			 /* .line 30 */
		 } // :cond_0
		 final String v1 = "MiuiFreeFormKeyCombinationHelper"; // const-string v1, "MiuiFreeFormKeyCombinationHelper"
		 /* if-nez v0, :cond_1 */
		 /* .line 31 */
		 final String v2 = "no task focused, do noting"; // const-string v2, "no task focused, do noting"
		 android.util.Slog .d ( v1,v2 );
		 /* .line 32 */
		 return;
		 /* .line 35 */
	 } // :cond_1
	 v2 = 	 (( com.android.server.wm.Task ) v0 ).isActivityTypeHomeOrRecents ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z
	 if ( v2 != null) { // if-eqz v2, :cond_2
		 /* .line 36 */
		 /* const-string/jumbo v2, "task isActivityTypeHomeOrRecents" */
		 android.util.Slog .d ( v1,v2 );
		 /* .line 37 */
		 return;
		 /* .line 40 */
	 } // :cond_2
	 /* new-instance v2, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper$$ExternalSyntheticLambda0;-><init>()V */
	 (( com.android.server.wm.Task ) v0 ).getActivity ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/Task;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;
	 if ( v2 != null) { // if-eqz v2, :cond_3
		 /* .line 41 */
		 /* const-string/jumbo v2, "topFocusedStack is in animating" */
		 android.util.Slog .d ( v1,v2 );
		 /* .line 42 */
		 return;
		 /* .line 45 */
	 } // :cond_3
	 v2 = 	 (( com.android.server.wm.Task ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getWindowingMode()I
	 /* .line 46 */
	 /* .local v2, "windowingMode":I */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v4, "topFocusedStack.getRootTaskId()= " */
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v4 = 	 (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v4 = " islaunchFreeform= "; // const-string v4, " islaunchFreeform= "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v4 = " windowingMode= "; // const-string v4, " windowingMode= "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v4 = " mIsHandleKeyCombination= "; // const-string v4, " mIsHandleKeyCombination= "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z */
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v3 );
	 /* .line 49 */
	 /* iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z */
	 if ( v1 != null) { // if-eqz v1, :cond_4
		 /* .line 50 */
		 return;
		 /* .line 52 */
	 } // :cond_4
	 int v1 = 1; // const/4 v1, 0x1
	 /* iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z */
	 /* .line 54 */
	 int v3 = 0; // const/4 v3, 0x0
	 if ( p1 != null) { // if-eqz p1, :cond_7
		 /* if-ne v2, v1, :cond_7 */
		 /* .line 55 */
		 try { // :try_start_0
			 v4 = this.mGestureController;
			 v4 = this.mService;
			 v5 = 			 (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
			 v4 = 			 (( com.android.server.wm.ActivityTaskManagerService ) v4 ).getTaskResizeableForFreeform ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getTaskResizeableForFreeform(I)Z
			 /* if-nez v4, :cond_6 */
			 /* .line 56 */
			 (( com.android.server.wm.Task ) v0 ).getTopActivity ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
			 /* .line 57 */
			 /* .local v1, "ar":Lcom/android/server/wm/ActivityRecord; */
			 if ( v1 != null) { // if-eqz v1, :cond_5
				 /* .line 58 */
				 v4 = this.mGestureController;
				 v4 = this.mService;
				 v4 = this.mContext;
				 v5 = this.mGestureController;
				 v5 = this.mService;
				 v5 = this.mContext;
				 /* .line 59 */
				 /* invoke-direct {p0, v5, v1}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->getActivityName(Landroid/content/Context;Lcom/android/server/wm/ActivityRecord;)Ljava/lang/String; */
				 /* .line 58 */
				 /* invoke-direct {p0, v4, v5}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->showNotSupportToast(Landroid/content/Context;Ljava/lang/String;)V */
				 /* .line 61 */
			 } // .end local v1 # "ar":Lcom/android/server/wm/ActivityRecord;
		 } // :cond_5
		 /* goto/16 :goto_4 */
		 /* .line 62 */
	 } // :cond_6
	 v1 = this.mGestureController;
	 v1 = this.mMiuiFreeFormManagerService;
	 v4 = 	 (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
	 (( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).freeformFullscreenTask ( v4 ); // invoke-virtual {v1, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->freeformFullscreenTask(I)V
	 /* goto/16 :goto_4 */
	 /* .line 85 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* goto/16 :goto_2 */
	 /* .line 82 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* goto/16 :goto_1 */
	 /* .line 64 */
} // :cond_7
/* if-nez p1, :cond_8 */
int v4 = 5; // const/4 v4, 0x5
/* if-ne v2, v4, :cond_8 */
/* .line 65 */
v1 = this.mGestureController;
v1 = this.mMiuiFreeFormManagerService;
/* .line 66 */
v4 = (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).getMiuiFreeFormActivityStack ( v4 ); // invoke-virtual {v1, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
/* check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 67 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_b
	 v4 = 	 (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).isInFreeFormMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
	 if ( v4 != null) { // if-eqz v4, :cond_b
		 /* .line 68 */
		 v4 = this.mGestureController;
		 v4 = this.mMiuiFreeFormManagerService;
		 v5 = 		 (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
		 (( com.android.server.wm.MiuiFreeFormManagerService ) v4 ).fullscreenFreeformTask ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fullscreenFreeformTask(I)V
		 /* .line 70 */
	 } // .end local v1 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_8
if ( p1 != null) { // if-eqz p1, :cond_b
	 int v4 = 6; // const/4 v4, 0x6
	 /* if-ne v2, v4, :cond_b */
	 /* .line 71 */
	 v4 = this.mGestureController;
	 v4 = this.mService;
	 v5 = 	 (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
	 v4 = 	 (( com.android.server.wm.ActivityTaskManagerService ) v4 ).getTaskResizeableForFreeform ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getTaskResizeableForFreeform(I)Z
	 /* if-nez v4, :cond_9 */
	 /* .line 72 */
	 (( com.android.server.wm.Task ) v0 ).getTopActivity ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
	 /* .line 73 */
	 /* .local v1, "ar":Lcom/android/server/wm/ActivityRecord; */
	 if ( v1 != null) { // if-eqz v1, :cond_a
		 /* .line 74 */
		 v4 = this.mGestureController;
		 v4 = this.mService;
		 v4 = this.mContext;
		 v5 = this.mGestureController;
		 v5 = this.mService;
		 v5 = this.mContext;
		 /* .line 75 */
		 /* invoke-direct {p0, v5, v1}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->getActivityName(Landroid/content/Context;Lcom/android/server/wm/ActivityRecord;)Ljava/lang/String; */
		 /* .line 74 */
		 /* invoke-direct {p0, v4, v5}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->showNotSupportToast(Landroid/content/Context;Ljava/lang/String;)V */
		 /* .line 77 */
	 } // .end local v1 # "ar":Lcom/android/server/wm/ActivityRecord;
} // :cond_9
v1 = this.mTransitionController;
v1 = (( com.android.server.wm.TransitionController ) v1 ).isCollecting ( ); // invoke-virtual {v1}, Lcom/android/server/wm/TransitionController;->isCollecting()Z
/* if-nez v1, :cond_a */
v1 = this.mTransitionController;
/* .line 78 */
v1 = (( com.android.server.wm.TransitionController ) v1 ).isPlaying ( ); // invoke-virtual {v1}, Lcom/android/server/wm/TransitionController;->isPlaying()Z
/* if-nez v1, :cond_c */
/* .line 79 */
v1 = this.mGestureController;
v1 = this.mMiuiFreeFormManagerService;
/* iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).fromSoscToFreeform ( v4 ); // invoke-virtual {v1, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fromSoscToFreeform(I)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 77 */
} // :cond_a
} // :goto_0
/* .line 83 */
/* .local v1, "e":Ljava/lang/Exception; */
} // :goto_1
try { // :try_start_1
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* .line 85 */
} // :goto_2
/* iput-boolean v3, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z */
/* .line 86 */
/* throw v1 */
/* .line 70 */
} // :cond_b
} // :goto_3
/* nop */
/* .line 85 */
} // :cond_c
} // :goto_4
/* iput-boolean v3, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z */
/* .line 86 */
/* nop */
/* .line 87 */
return;
} // .end method
