.class public Lcom/android/server/wm/MiuiPaperContrastOverlay;
.super Ljava/lang/Object;
.source "MiuiPaperContrastOverlay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field public static final IS_FOLDABLE_DEVICE:Z

.field private static final TAG:Ljava/lang/String; = "MiuiPaperContrastOverlay"

.field private static volatile mContrastOverlay:Lcom/android/server/wm/MiuiPaperContrastOverlay;


# instance fields
.field private mBLASTSurfaceControl:Landroid/view/SurfaceControl;

.field private mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

.field private mContext:Landroid/content/Context;

.field private mCreatedResources:Z

.field private final mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

.field private mDisplayWindowListener:Landroid/view/IDisplayWindowListener;

.field private mEglConfig:Landroid/opengl/EGLConfig;

.field private mEglContext:Landroid/opengl/EGLContext;

.field private mEglDisplay:Landroid/opengl/EGLDisplay;

.field private mEglSurface:Landroid/opengl/EGLSurface;

.field private mFoldDeviceReady:Z

.field private mIndexBuffer:Ljava/nio/ShortBuffer;

.field private final mLastConfiguration:Landroid/content/res/Configuration;

.field private mLastDisplayHeight:I

.field private mLastDisplayWidth:I

.field private mLastLevel:I

.field private mLastRandSeed:I

.field private mNeedShowPaperSurface:Z

.field private mPaperAlpha:F

.field private mPaperMode:Z

.field private mPaperNoiseLevel:I

.field private mPrepared:Z

.field private mProgram:I

.field private mRandSeed:I

.field private mRandomTexBuffer:Ljava/nio/IntBuffer;

.field private mSurface:Landroid/view/Surface;

.field private mSurfaceControl:Landroid/view/SurfaceControl;

.field private mSurfaceHeight:I

.field private mSurfaceLayout:Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;

.field private mSurfaceSession:Landroid/view/SurfaceSession;

.field private mSurfaceWidth:I

.field private mTextureBuffer:Ljava/nio/FloatBuffer;

.field private mTextureId:[I

.field private mVertexBuffer:Ljava/nio/FloatBuffer;

.field private mWms:Lcom/android/server/wm/WindowManagerService;

.field private samplerLocA:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFoldDeviceReady(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastConfiguration(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Landroid/content/res/Configuration;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastConfiguration:Landroid/content/res/Configuration;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNeedShowPaperSurface(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mNeedShowPaperSurface:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmWms(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Lcom/android/server/wm/WindowManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmFoldDeviceReady(Lcom/android/server/wm/MiuiPaperContrastOverlay;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcreateSurface(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createSurface()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdestroySurface(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroySurface()V

    return-void
.end method

.method static bridge synthetic -$$Nest$misSizeChangeHappened(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->isSizeChangeHappened()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateDisplaySize(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateDisplaySize()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 50
    nop

    .line 51
    const-string v0, "persist.sys.muiltdisplay_type"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 52
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    sput-boolean v1, Lcom/android/server/wm/MiuiPaperContrastOverlay;->IS_FOLDABLE_DEVICE:Z

    .line 50
    return-void
.end method

.method private constructor <init>(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V
    .locals 3
    .param p1, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mTextureId:[I

    .line 83
    const v0, 0x3d888889

    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperAlpha:F

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z

    .line 85
    const/16 v1, 0x11

    iput v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperNoiseLevel:I

    .line 210
    new-instance v1, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mDisplayWindowListener:Landroid/view/IDisplayWindowListener;

    .line 110
    iput-object p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    .line 111
    iput-object p2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContext:Landroid/content/Context;

    .line 113
    const-class v1, Landroid/hardware/display/DisplayManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManagerInternal;

    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    .line 115
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateDisplaySize()V

    .line 117
    new-instance v1, Landroid/content/res/Configuration;

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastConfiguration:Landroid/content/res/Configuration;

    .line 118
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mDisplayWindowListener:Landroid/view/IDisplayWindowListener;

    invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->registerDisplayWindowListener(Landroid/view/IDisplayWindowListener;)[I

    .line 120
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z

    .line 122
    sget-boolean v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->IS_FOLDABLE_DEVICE:Z

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContext:Landroid/content/Context;

    const-class v1, Landroid/hardware/devicestate/DeviceStateManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/devicestate/DeviceStateManager;

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContext:Landroid/content/Context;

    .line 124
    invoke-virtual {v1}, Landroid/content/Context;->getMainExecutor()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;

    invoke-direct {v2, p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V

    invoke-virtual {v0, v1, v2}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V

    .line 146
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createSurface()V

    .line 147
    return-void
.end method

.method private attachEglContext()Z
    .locals 4

    .line 861
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglSurface:Landroid/opengl/EGLSurface;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglContext:Landroid/opengl/EGLContext;

    if-nez v2, :cond_0

    goto :goto_0

    .line 864
    :cond_0
    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v3, v0, v0, v2}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 865
    const-string v0, "eglMakeCurrent"

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->logEglError(Ljava/lang/String;)V

    .line 866
    return v1

    .line 868
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 862
    :cond_2
    :goto_0
    return v1
.end method

.method private static checkGlErrors(Ljava/lang/String;)Z
    .locals 1
    .param p0, "func"    # Ljava/lang/String;

    .line 883
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->checkGlErrors(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static checkGlErrors(Ljava/lang/String;Z)Z
    .locals 5
    .param p0, "func"    # Ljava/lang/String;
    .param p1, "log"    # Z

    .line 887
    const/4 v0, 0x0

    .line 889
    .local v0, "hadError":Z
    :goto_0
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v1

    move v2, v1

    .local v2, "error":I
    if-eqz v1, :cond_1

    .line 890
    if-eqz p1, :cond_0

    .line 891
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " failed: error "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    const-string v4, "MiuiPaperContrastOverlay"

    invoke-static {v4, v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 893
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 895
    :cond_1
    return v0
.end method

.method private clearEglAndSurface()V
    .locals 3

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "clearEglAndSurface, egl display is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", egl context is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglContext:Landroid/opengl/EGLContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPaperContrastOverlay"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V

    .line 191
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyRandomTexture()V

    .line 192
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyGLShaders()V

    .line 193
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglContext:Landroid/opengl/EGLContext;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 194
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v2, v0}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 195
    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglContext:Landroid/opengl/EGLContext;

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    if-eqz v0, :cond_1

    .line 198
    invoke-static {v0}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 199
    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    .line 201
    :cond_1
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V

    .line 202
    return-void
.end method

.method private createEglContext()Z
    .locals 14

    .line 768
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v0, :cond_1

    .line 769
    invoke-static {v4}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    .line 770
    sget-object v5, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-ne v0, v5, :cond_0

    .line 771
    const-string v0, "eglGetDisplay"

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->logEglError(Ljava/lang/String;)V

    .line 772
    return v4

    .line 775
    :cond_0
    new-array v0, v2, [I

    .line 776
    .local v0, "version":[I
    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v5, v0, v4, v0, v3}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v5

    if-nez v5, :cond_1

    .line 777
    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    .line 778
    const-string v1, "eglInitialize"

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->logEglError(Ljava/lang/String;)V

    .line 779
    return v4

    .line 783
    .end local v0    # "version":[I
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglConfig:Landroid/opengl/EGLConfig;

    if-nez v0, :cond_4

    .line 784
    const/16 v0, 0xb

    new-array v6, v0, [I

    fill-array-data v6, :array_0

    .line 793
    .local v6, "eglConfigAttribList":[I
    new-array v0, v3, [I

    .line 794
    .local v0, "numEglConfigs":[I
    new-array v13, v3, [Landroid/opengl/EGLConfig;

    .line 795
    .local v13, "eglConfigs":[Landroid/opengl/EGLConfig;
    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    const/4 v7, 0x0

    const/4 v9, 0x0

    array-length v10, v13

    const/4 v12, 0x0

    move-object v8, v13

    move-object v11, v0

    invoke-static/range {v5 .. v12}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v5

    if-nez v5, :cond_2

    .line 797
    const-string v1, "eglChooseConfig"

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->logEglError(Ljava/lang/String;)V

    .line 798
    return v4

    .line 800
    :cond_2
    aget v5, v0, v4

    if-gtz v5, :cond_3

    .line 801
    const-string v1, "MiuiPaperContrastOverlay"

    const-string v2, "no valid config found"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    return v4

    .line 805
    :cond_3
    aget-object v5, v13, v4

    iput-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglConfig:Landroid/opengl/EGLConfig;

    .line 810
    .end local v0    # "numEglConfigs":[I
    .end local v6    # "eglConfigAttribList":[I
    .end local v13    # "eglConfigs":[Landroid/opengl/EGLConfig;
    :cond_4
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglContext:Landroid/opengl/EGLContext;

    if-eqz v0, :cond_5

    .line 811
    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v5, v0}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 812
    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglContext:Landroid/opengl/EGLContext;

    .line 815
    :cond_5
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglContext:Landroid/opengl/EGLContext;

    if-nez v0, :cond_6

    .line 816
    const/16 v0, 0x3098

    const/16 v1, 0x3038

    filled-new-array {v0, v2, v1}, [I

    move-result-object v0

    .line 820
    .local v0, "eglContextAttribList":[I
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglConfig:Landroid/opengl/EGLConfig;

    sget-object v5, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v1, v2, v5, v0, v4}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglContext:Landroid/opengl/EGLContext;

    .line 822
    if-nez v1, :cond_6

    .line 823
    const-string v1, "eglCreateContext"

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->logEglError(Ljava/lang/String;)V

    .line 824
    return v4

    .line 827
    .end local v0    # "eglContextAttribList":[I
    :cond_6
    return v3

    nop

    :array_0
    .array-data 4
        0x3040
        0x4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3038
    .end array-data
.end method

.method private createEglSurface()Z
    .locals 5

    .line 832
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglSurface:Landroid/opengl/EGLSurface;

    if-eqz v0, :cond_0

    .line 833
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v1, v0}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 834
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglSurface:Landroid/opengl/EGLSurface;

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglSurface:Landroid/opengl/EGLSurface;

    if-nez v0, :cond_1

    .line 837
    const/16 v0, 0x3038

    filled-new-array {v0}, [I

    move-result-object v0

    .line 841
    .local v0, "eglSurfaceAttribList":[I
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglConfig:Landroid/opengl/EGLConfig;

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurface:Landroid/view/Surface;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v0, v4}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglSurface:Landroid/opengl/EGLSurface;

    .line 843
    if-nez v1, :cond_1

    .line 844
    const-string v1, "eglCreateWindowSurface"

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->logEglError(Ljava/lang/String;)V

    .line 845
    return v4

    .line 848
    .end local v0    # "eglSurfaceAttribList":[I
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private createRandTexture()V
    .locals 13

    .line 626
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "createRandTexture randSeed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastRandSeed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastRandSeed:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPaperContrastOverlay"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    .line 629
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 630
    .local v3, "begin":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createRandTexture begin, size ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "), pixels: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    .line 631
    invoke-virtual {v5}, Ljava/nio/IntBuffer;->limit()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 630
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    mul-int/2addr v0, v5

    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v5}, Ljava/nio/IntBuffer;->limit()I

    move-result v5

    if-le v0, v5, :cond_0

    .line 636
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->requestRandomBuffer()V

    .line 643
    :cond_0
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    mul-int/2addr v0, v5

    const/4 v5, 0x2

    div-int/2addr v0, v5

    .line 644
    .local v0, "halfPixels":I
    invoke-static {}, Ljava/util/concurrent/ThreadLocalRandom;->current()Ljava/util/concurrent/ThreadLocalRandom;

    move-result-object v6

    int-to-long v7, v0

    iget v9, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I

    invoke-virtual {v6, v7, v8, v2, v9}, Ljava/util/concurrent/ThreadLocalRandom;->ints(JII)Ljava/util/stream/IntStream;

    move-result-object v6

    .line 645
    .local v6, "randomStream":Ljava/util/stream/IntStream;
    invoke-interface {v6}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v7

    .line 646
    .local v7, "random":[I
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_0
    if-ge v8, v5, :cond_2

    .line 647
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    array-length v10, v7

    if-ge v9, v10, :cond_1

    .line 648
    iget-object v10, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    mul-int v11, v8, v0

    add-int/2addr v11, v9

    aget v12, v7, v9

    invoke-virtual {v10, v11, v12}, Ljava/nio/IntBuffer;->put(II)Ljava/nio/IntBuffer;

    .line 647
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 646
    .end local v9    # "i":I
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 652
    .end local v8    # "j":I
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 653
    .local v8, "end":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createRandTexture end, duration: "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v10, v8, v3

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "halfPixels":I
    .end local v3    # "begin":J
    .end local v6    # "randomStream":Ljava/util/stream/IntStream;
    .end local v7    # "random":[I
    .end local v8    # "end":J
    goto :goto_3

    .line 654
    :cond_3
    if-nez v0, :cond_4

    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastRandSeed:I

    if-eqz v0, :cond_4

    .line 655
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    mul-int/2addr v1, v3

    if-ge v0, v1, :cond_5

    .line 656
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v0, v2}, Ljava/nio/IntBuffer;->put(II)Ljava/nio/IntBuffer;

    .line 655
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 654
    .end local v0    # "i":I
    :cond_4
    :goto_3
    nop

    .line 659
    :cond_5
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateDisplaySize()V

    .line 660
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I

    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastRandSeed:I

    .line 661
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mTextureId:[I

    aget v0, v0, v2

    const/16 v1, 0xde1

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 662
    const/16 v2, 0xde1

    const/4 v3, 0x0

    const/16 v4, 0x1908

    iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    const/4 v7, 0x0

    const/16 v8, 0x1908

    const/16 v9, 0x1401

    iget-object v10, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    invoke-static/range {v2 .. v10}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 664
    const-string v0, "createRandTexture"

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->checkGlErrors(Ljava/lang/String;)Z

    .line 665
    return-void
.end method

.method private createSurface()V
    .locals 9

    .line 379
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceSession:Landroid/view/SurfaceSession;

    if-nez v0, :cond_0

    .line 380
    new-instance v0, Landroid/view/SurfaceSession;

    invoke-direct {v0}, Landroid/view/SurfaceSession;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceSession:Landroid/view/SurfaceSession;

    .line 382
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Create surface size: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPaperContrastOverlay"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_5

    .line 386
    :try_start_0
    new-instance v0, Landroid/view/SurfaceControl$Builder;

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceSession:Landroid/view/SurfaceSession;

    invoke-direct {v0, v2}, Landroid/view/SurfaceControl$Builder;-><init>(Landroid/view/SurfaceSession;)V

    .line 387
    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    .line 415
    .local v0, "builder":Landroid/view/SurfaceControl$Builder;
    const/4 v2, 0x0

    .line 416
    .local v2, "parentCtrl":Landroid/view/SurfaceControl;
    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/wm/DisplayContent;->mChildren:Lcom/android/server/wm/WindowList;

    .line 417
    .local v3, "mChildren":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<Lcom/android/server/wm/DisplayArea;>;"
    invoke-virtual {v3}, Lcom/android/server/wm/WindowList;->size()I

    move-result v4

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    .local v4, "i":I
    :goto_0
    if-ltz v4, :cond_3

    .line 418
    invoke-virtual {v3, v4}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wm/DisplayArea;

    .line 419
    .local v6, "childArea":Lcom/android/server/wm/DisplayArea;, "Lcom/android/server/wm/DisplayArea<*>;"
    if-nez v6, :cond_1

    .line 420
    goto :goto_1

    .line 423
    :cond_1
    invoke-virtual {v6}, Lcom/android/server/wm/DisplayArea;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "WindowedMagnification"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 424
    invoke-virtual {v6}, Lcom/android/server/wm/DisplayArea;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v7

    move-object v2, v7

    .line 417
    .end local v6    # "childArea":Lcom/android/server/wm/DisplayArea;, "Lcom/android/server/wm/DisplayArea<*>;"
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 427
    .end local v4    # "i":I
    :cond_3
    if-nez v2, :cond_4

    .line 428
    iget-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v4}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/wm/DisplayContent;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v4

    move-object v2, v4

    .line 430
    :cond_4
    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 431
    invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setFormat(I)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    .line 432
    invoke-virtual {v4, v5, v6}, Landroid/view/SurfaceControl$Builder;->setBufferSize(II)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 433
    invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->setContainerLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 434
    invoke-virtual {v4, v2}, Landroid/view/SurfaceControl$Builder;->setParent(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 435
    invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    .line 436
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;
    :try_end_0
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    .end local v0    # "builder":Landroid/view/SurfaceControl$Builder;
    .end local v2    # "parentCtrl":Landroid/view/SurfaceControl;
    .end local v3    # "mChildren":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<Lcom/android/server/wm/DisplayArea;>;"
    goto :goto_2

    .line 437
    :catch_0
    move-exception v0

    .line 438
    .local v0, "ex":Landroid/view/Surface$OutOfResourcesException;
    const-string v2, "Unable to create surface."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 441
    .end local v0    # "ex":Landroid/view/Surface$OutOfResourcesException;
    :goto_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mTransactionFactory:Ljava/util/function/Supplier;

    invoke-interface {v0}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceControl$Transaction;

    .line 442
    .local v0, "t":Landroid/view/SurfaceControl$Transaction;
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;

    .line 443
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    const v3, 0xf4240

    invoke-virtual {v0, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    .line 444
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 446
    new-instance v2, Landroid/view/SurfaceControl$Builder;

    invoke-direct {v2}, Landroid/view/SurfaceControl$Builder;-><init>()V

    .line 447
    invoke-virtual {v2, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 448
    invoke-virtual {v1, v2}, Landroid/view/SurfaceControl$Builder;->setParent(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;

    move-result-object v1

    .line 449
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/SurfaceControl$Builder;->setHidden(Z)Landroid/view/SurfaceControl$Builder;

    move-result-object v1

    .line 450
    invoke-virtual {v1}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v1

    .line 451
    .local v1, "b":Landroid/view/SurfaceControl$Builder;
    invoke-virtual {v1}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mBLASTSurfaceControl:Landroid/view/SurfaceControl;

    .line 452
    new-instance v2, Landroid/graphics/BLASTBufferQueue;

    const-string v4, "MiuiPaperContrastOverlay"

    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mBLASTSurfaceControl:Landroid/view/SurfaceControl;

    iget v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v7, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    const/4 v8, -0x3

    move-object v3, v2

    invoke-direct/range {v3 .. v8}, Landroid/graphics/BLASTBufferQueue;-><init>(Ljava/lang/String;Landroid/view/SurfaceControl;III)V

    iput-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    .line 454
    invoke-virtual {v2}, Landroid/graphics/BLASTBufferQueue;->createSurface()Landroid/view/Surface;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurface:Landroid/view/Surface;

    .line 456
    new-instance v2, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    iget-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-direct {v2, p0, v3, v4}, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay;Landroid/hardware/display/DisplayManagerInternal;Landroid/view/SurfaceControl;)V

    iput-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceLayout:Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;

    .line 457
    invoke-virtual {v2, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->onDisplayTransaction(Landroid/view/SurfaceControl$Transaction;)V

    .line 458
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 461
    .end local v0    # "t":Landroid/view/SurfaceControl$Transaction;
    .end local v1    # "b":Landroid/view/SurfaceControl$Builder;
    :cond_5
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateDisplaySize()V

    .line 462
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->initEgl()V

    .line 463
    return-void
.end method

.method private destroyEglSurface()V
    .locals 2

    .line 852
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglSurface:Landroid/opengl/EGLSurface;

    if-eqz v0, :cond_1

    .line 853
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v1, v0}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 854
    const-string v0, "eglDestroySurface"

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->logEglError(Ljava/lang/String;)V

    .line 856
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglSurface:Landroid/opengl/EGLSurface;

    .line 858
    :cond_1
    return-void
.end method

.method private destroyGLShaders()V
    .locals 1

    .line 683
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 684
    const-string v0, "glDeleteProgram"

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->checkGlErrors(Ljava/lang/String;)Z

    .line 685
    return-void
.end method

.method private destroyRandomTexture()V
    .locals 3

    .line 616
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mTextureId:[I

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 617
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mTextureBuffer:Ljava/nio/FloatBuffer;

    .line 618
    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mVertexBuffer:Ljava/nio/FloatBuffer;

    .line 619
    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mIndexBuffer:Ljava/nio/ShortBuffer;

    .line 620
    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    .line 621
    const-string v0, "glDeleteTextures"

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->checkGlErrors(Ljava/lang/String;)Z

    .line 622
    return-void
.end method

.method private destroySurface()V
    .locals 4

    .line 720
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "destroySurface surfaceControl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPaperContrastOverlay"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_3

    .line 724
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_paper_layer_show"

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 727
    iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I

    .line 728
    iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I

    .line 729
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceLayout:Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->dispose()V

    .line 730
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceLayout:Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;

    .line 731
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mTransactionFactory:Ljava/util/function/Supplier;

    invoke-interface {v1}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceControl$Transaction;

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v1, v2}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 732
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurface:Landroid/view/Surface;

    if-eqz v1, :cond_1

    .line 733
    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 734
    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurface:Landroid/view/Surface;

    .line 736
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mBLASTSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v1, :cond_2

    .line 737
    invoke-virtual {v1}, Landroid/view/SurfaceControl;->release()V

    .line 738
    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mBLASTSurfaceControl:Landroid/view/SurfaceControl;

    .line 739
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    invoke-virtual {v1}, Landroid/graphics/BLASTBufferQueue;->destroy()V

    .line 740
    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    .line 742
    :cond_2
    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 744
    :cond_3
    return-void
.end method

.method private detachEglContext()V
    .locals 4

    .line 872
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    if-eqz v0, :cond_0

    .line 873
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 876
    :cond_0
    return-void
.end method

.method private dismiss()V
    .locals 3

    .line 707
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z

    .line 709
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dismiss "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPaperContrastOverlay"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    iget-boolean v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z

    if-eqz v1, :cond_0

    .line 712
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismissResources()V

    .line 713
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroySurface()V

    .line 714
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z

    .line 716
    :cond_0
    return-void
.end method

.method private dismissResources()V
    .locals 2

    .line 748
    const-string v0, "MiuiPaperContrastOverlay"

    const-string v1, "dismissResources"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mCreatedResources:Z

    if-eqz v0, :cond_0

    .line 752
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->attachEglContext()Z

    .line 754
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyRandomTexture()V

    .line 755
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyGLShaders()V

    .line 756
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroyEglSurface()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 758
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V

    .line 759
    nop

    .line 762
    invoke-static {}, Landroid/opengl/GLES20;->glFlush()V

    .line 763
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mCreatedResources:Z

    goto :goto_0

    .line 758
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V

    .line 759
    throw v0

    .line 765
    :cond_0
    :goto_0
    return-void
.end method

.method private drawFrame(F)V
    .locals 17
    .param p1, "alpha"    # F

    .line 688
    move-object/from16 v0, p0

    move/from16 v1, p1

    const/4 v2, 0x0

    invoke-static {v2, v2, v2, v1}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 689
    iget v2, v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v3, v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    const/4 v4, 0x0

    invoke-static {v4, v4, v2, v3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 690
    iget v2, v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->samplerLocA:I

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glVertexAttrib1f(IF)V

    .line 693
    const/4 v5, 0x0

    const/4 v6, 0x3

    const/16 v7, 0x1406

    const/4 v8, 0x0

    const/16 v9, 0xc

    iget-object v10, v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-static/range {v5 .. v10}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 696
    const/4 v11, 0x1

    const/4 v12, 0x2

    const/16 v13, 0x1406

    const/4 v14, 0x0

    const/16 v15, 0x8

    iget-object v2, v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mTextureBuffer:Ljava/nio/FloatBuffer;

    move-object/from16 v16, v2

    invoke-static/range {v11 .. v16}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 699
    invoke-static {v4}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 700
    const/4 v2, 0x1

    invoke-static {v2}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 702
    const/16 v2, 0x4100

    invoke-static {v2}, Landroid/opengl/GLES20;->glClear(I)V

    .line 703
    const/16 v2, 0x1403

    iget-object v3, v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mIndexBuffer:Ljava/nio/ShortBuffer;

    const/4 v4, 0x4

    const/4 v5, 0x6

    invoke-static {v4, v5, v2, v3}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    .line 704
    return-void
.end method

.method private drawSurface(F)V
    .locals 2
    .param p1, "alpha"    # F

    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Draw surface, alpha is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and randSeed is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPaperContrastOverlay"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->updateGLShaders(F)V

    .line 362
    const-string/jumbo v0, "updateGLShaders"

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->checkGlErrors(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    const-string v0, "Draw surface updateGLShaders failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V

    .line 365
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V

    .line 366
    return-void

    .line 368
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->drawFrame(F)V

    .line 369
    const-string v0, "drawFrame"

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->checkGlErrors(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370
    const-string v0, "Draw frame failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V

    .line 372
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V

    .line 373
    return-void

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mEglSurface:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 376
    return-void
.end method

.method public static getInstance(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)Lcom/android/server/wm/MiuiPaperContrastOverlay;
    .locals 2
    .param p0, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p1, "context"    # Landroid/content/Context;

    .line 99
    sget-object v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContrastOverlay:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    if-nez v0, :cond_1

    .line 100
    const-class v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;

    monitor-enter v0

    .line 101
    :try_start_0
    sget-object v1, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContrastOverlay:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    if-nez v1, :cond_0

    .line 102
    new-instance v1, Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;-><init>(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContrastOverlay:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    .line 104
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 106
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContrastOverlay:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    return-object v0
.end method

.method private initEgl()V
    .locals 2

    .line 467
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createEglContext()Z

    move-result v0

    const-string v1, "MiuiPaperContrastOverlay"

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createEglSurface()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 476
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->attachEglContext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 477
    const-string v0, "attachEglContext failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    return-void

    .line 481
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->initGLShaders()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "initEgl"

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->checkGlErrors(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 487
    :cond_2
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V

    .line 488
    nop

    .line 491
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mCreatedResources:Z

    .line 492
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z

    .line 493
    return-void

    .line 482
    :cond_3
    :goto_0
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V

    .line 483
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V

    .line 484
    return-void

    .line 487
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->detachEglContext()V

    .line 488
    throw v0

    .line 468
    :cond_4
    :goto_1
    const-string v0, "createEglContext failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->dismiss()V

    .line 474
    return-void
.end method

.method private initGLShaders()Z
    .locals 10

    .line 545
    const v0, 0x8b31

    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->loadShader(I)I

    move-result v0

    .line 546
    .local v0, "vshader":I
    const v1, 0x8b30

    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->loadShader(I)I

    move-result v1

    .line 547
    .local v1, "fshader":I
    invoke-static {}, Landroid/opengl/GLES20;->glReleaseShaderCompiler()V

    .line 548
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    goto/16 :goto_0

    .line 550
    :cond_0
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v3

    iput v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I

    .line 552
    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 553
    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I

    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 554
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 555
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 557
    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I

    invoke-static {v3}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 559
    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I

    invoke-static {v3}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 560
    const-string v3, "glUseProgram"

    invoke-static {v3}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->checkGlErrors(Ljava/lang/String;)Z

    .line 561
    const-string v3, "MiuiPaperContrastOverlay"

    const-string v4, "initGLShader start"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    const/16 v3, 0xc

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    .line 567
    .local v3, "vCoord":[F
    const/16 v4, 0x8

    new-array v4, v4, [F

    fill-array-data v4, :array_1

    .line 572
    .local v4, "tCoord":[F
    const/4 v5, 0x6

    new-array v5, v5, [S

    fill-array-data v5, :array_2

    .line 573
    .local v5, "indices":[S
    array-length v6, v3

    mul-int/lit8 v6, v6, 0x4

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 574
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 575
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v6

    .line 576
    invoke-virtual {v6, v3}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v6

    iput-object v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mVertexBuffer:Ljava/nio/FloatBuffer;

    .line 577
    invoke-virtual {v6, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 578
    array-length v6, v4

    mul-int/lit8 v6, v6, 0x4

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 579
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 580
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v6

    .line 581
    invoke-virtual {v6, v4}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v6

    iput-object v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mTextureBuffer:Ljava/nio/FloatBuffer;

    .line 582
    invoke-virtual {v6, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 583
    array-length v6, v5

    mul-int/lit8 v6, v6, 0x4

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 584
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 585
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v6

    .line 586
    invoke-virtual {v6, v5}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    move-result-object v6

    iput-object v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mIndexBuffer:Ljava/nio/ShortBuffer;

    .line 587
    invoke-virtual {v6, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 589
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->requestRandomBuffer()V

    .line 591
    iget-object v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mTextureId:[I

    const/4 v7, 0x1

    invoke-static {v7, v6, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 592
    iget-object v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mTextureId:[I

    aget v6, v6, v2

    const/16 v8, 0xde1

    invoke-static {v8, v6}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 593
    const/16 v6, 0xcf5

    invoke-static {v6, v7}, Landroid/opengl/GLES20;->glPixelStorei(II)V

    .line 594
    const/16 v6, 0x2801

    const/16 v9, 0x2600

    invoke-static {v8, v6, v9}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 595
    const/16 v6, 0x2800

    invoke-static {v8, v6, v9}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 596
    const/16 v6, 0x2802

    const/16 v9, 0x2901

    invoke-static {v8, v6, v9}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 597
    const/16 v6, 0x2803

    invoke-static {v8, v6, v9}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 598
    iget v6, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v8, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    invoke-static {v2, v2, v6, v8}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 599
    return v7

    .line 548
    .end local v3    # "vCoord":[F
    .end local v4    # "tCoord":[F
    .end local v5    # "indices":[S
    :cond_1
    :goto_0
    return v2

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_2
    .array-data 2
        0x0s
        0x1s
        0x2s
        0x0s
        0x2s
        0x3s
    .end array-data
.end method

.method private isSizeChangeHappened()Z
    .locals 3

    .line 206
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayHeight:I

    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    if-ne v0, v1, :cond_0

    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayWidth:I

    iget v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    if-eq v1, v2, :cond_1

    :cond_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private loadShader(I)I
    .locals 5
    .param p1, "type"    # I

    .line 496
    const-string v0, ""

    .line 497
    .local v0, "source":Ljava/lang/String;
    const v1, 0x8b31

    if-ne p1, v1, :cond_0

    .line 498
    const-string v0, "#version 300 es                                              \nlayout(location = 0) in vec4 a_position;                   \nlayout(location = 1) in vec2 a_texCoord;                   \nout vec2 v_texCoord;                                       \nvoid main()                                                \n{                                                          \n    gl_Position = a_position;                              \n    v_texCoord = a_texCoord;                               \n}                                                          \n"

    goto :goto_0

    .line 509
    :cond_0
    const-string v0, "#version 300 es                                              \nprecision mediump float;                                   \nin vec2 v_texCoord;                                        \nuniform float s_Alpha;                                     \nlayout(location = 0) out vec4 outColor;                    \nuniform sampler2D s_RandTex;                               \nvoid main()                                                \n{                                                          \n    outColor.r =  texture(s_RandTex, v_texCoord).x;        \n    outColor.g =  texture(s_RandTex, v_texCoord).x;        \n    outColor.b =  1.1f * texture(s_RandTex, v_texCoord).x; \n    outColor.a =  s_Alpha; \n}                                                          \n"

    .line 525
    :goto_0
    invoke-static {p1}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    .line 527
    .local v1, "shader":I
    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 528
    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 530
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 531
    .local v2, "compiled":[I
    const v3, 0x8b81

    const/4 v4, 0x0

    invoke-static {v1, v3, v2, v4}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 532
    aget v3, v2, v4

    if-nez v3, :cond_1

    .line 533
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not compile shader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiPaperContrastOverlay"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderSource(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 537
    const/4 v1, 0x0

    .line 539
    :cond_1
    const-string v3, "loadShader"

    invoke-static {v3}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->checkGlErrors(Ljava/lang/String;)Z

    .line 541
    return v1
.end method

.method private static logEglError(Ljava/lang/String;)V
    .locals 3
    .param p0, "func"    # Ljava/lang/String;

    .line 879
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failed: error "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    const-string v2, "MiuiPaperContrastOverlay"

    invoke-static {v2, v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 880
    return-void
.end method

.method private requestRandomBuffer()V
    .locals 2

    .line 603
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    .line 604
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 605
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 606
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Request random buffer ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), pixels: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    .line 610
    invoke-virtual {v1}, Ljava/nio/IntBuffer;->limit()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 608
    const-string v1, "MiuiPaperContrastOverlay"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandomTexBuffer:Ljava/nio/IntBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 613
    return-void
.end method

.method private showSurface()V
    .locals 5

    .line 328
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPrepared:Z

    const-string v1, "MiuiPaperContrastOverlay"

    if-nez v0, :cond_0

    .line 329
    const-string v0, "Surface is not prepared ready."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    return-void

    .line 333
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->attachEglContext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 334
    const-string v0, "attachEglContext failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return-void

    .line 338
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z

    if-eqz v0, :cond_2

    .line 339
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperNoiseLevel:I

    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I

    .line 340
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperAlpha:F

    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->drawSurface(F)V

    goto :goto_0

    .line 342
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I

    .line 343
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->drawSurface(F)V

    .line 346
    :goto_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mTransactionFactory:Ljava/util/function/Supplier;

    invoke-interface {v0}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceControl$Transaction;

    .line 347
    .local v0, "t":Landroid/view/SurfaceControl$Transaction;
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    .line 348
    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v3

    .line 347
    invoke-static {v2, v0, v3, v1}, Lcom/android/server/wm/InputMonitor;->setTrustedOverlayInputInfo(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;ILjava/lang/String;)V

    .line 349
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mBLASTSurfaceControl:Landroid/view/SurfaceControl;

    iget-object v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    .line 350
    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v3

    .line 349
    invoke-static {v2, v0, v3, v1}, Lcom/android/server/wm/InputMonitor;->setTrustedOverlayInputInfo(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;ILjava/lang/String;)V

    .line 351
    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 352
    const-string v2, "Apply to show surface."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, -0x2

    const-string v4, "screen_paper_layer_show"

    invoke-static {v1, v4, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 357
    return-void
.end method

.method private updateDisplaySize()V
    .locals 4

    .line 158
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayWidth:I

    .line 159
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayHeight:I

    .line 161
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 162
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/WindowManagerService;->getBaseDisplaySize(ILandroid/graphics/Point;)V

    .line 164
    iget v1, v0, Landroid/graphics/Point;->x:I

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/graphics/Point;->y:I

    if-eqz v1, :cond_0

    .line 165
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    .line 166
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    goto :goto_2

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v1

    .line 169
    .local v1, "defaultInfo":Landroid/view/DisplayInfo;
    iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    iget v3, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    if-le v2, v3, :cond_1

    .line 170
    iget v2, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    goto :goto_0

    .line 171
    :cond_1
    iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    :goto_0
    iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    .line 172
    iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    iget v3, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    if-le v2, v3, :cond_2

    .line 173
    iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    goto :goto_1

    .line 174
    :cond_2
    iget v2, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    :goto_1
    iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    .line 178
    .end local v1    # "defaultInfo":Landroid/view/DisplayInfo;
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update surface ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") from ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayWidth:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastDisplayHeight:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") to ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceWidth:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceHeight:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") on thread: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 181
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 178
    const-string v2, "MiuiPaperContrastOverlay"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return-void
.end method

.method private updateGLShaders(F)V
    .locals 4
    .param p1, "alpha"    # F

    .line 668
    const-string v0, "MiuiPaperContrastOverlay"

    const-string/jumbo v1, "updateGLShaders start"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createRandTexture()V

    .line 672
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 673
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mTextureId:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const/16 v2, 0xde1

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 675
    iget v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I

    const-string v2, "s_RandTex"

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 676
    .local v0, "samplerLocR":I
    iget v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mProgram:I

    const-string v3, "s_Alpha"

    invoke-static {v2, v3}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->samplerLocA:I

    .line 678
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 679
    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->samplerLocA:I

    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 680
    return-void
.end method


# virtual methods
.method public changeDeviceReady()V
    .locals 1

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z

    .line 155
    return-void
.end method

.method public changeShowLayerStatus(Z)V
    .locals 0
    .param p1, "needShow"    # Z

    .line 150
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mNeedShowPaperSurface:Z

    .line 151
    return-void
.end method

.method public getSurfaceControl()Landroid/view/SurfaceControl;
    .locals 1

    .line 900
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    return-object v0
.end method

.method public hidePaperModeSurface()V
    .locals 4

    .line 318
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z

    .line 319
    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I

    .line 320
    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mRandSeed:I

    .line 321
    const-string v1, "MiuiPaperContrastOverlay"

    const-string v2, "Hide paper-mode surface. Dark-mode is disable."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mTransactionFactory:Ljava/util/function/Supplier;

    invoke-interface {v1}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceControl$Transaction;

    iget-object v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v1, v2}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 323
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_paper_layer_show"

    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 325
    return-void
.end method

.method public showPaperModeSurface()V
    .locals 5

    .line 289
    sget-boolean v0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->IS_FOLDABLE_DEVICE:Z

    const-string v1, "MiuiPaperContrastOverlay"

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mFoldDeviceReady:Z

    if-nez v0, :cond_0

    .line 290
    const-string v0, "The fold device is not ready, paper-mode return."

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    return-void

    .line 295
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->isSizeChangeHappened()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->destroySurface()V

    .line 298
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_2

    .line 299
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->createSurface()V

    .line 301
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget v2, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_TEXTURE_EYECARE_LEVEL:I

    const/4 v3, -0x2

    const-string/jumbo v4, "screen_texture_eyecare_level"

    invoke-static {v0, v4, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 304
    .local v0, "level":I
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperMode:Z

    .line 305
    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperNoiseLevel:I

    .line 306
    int-to-float v2, v0

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperAlpha:F

    .line 308
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Paper-mode surface params. Alpha: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mPaperAlpha:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Level from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    iget v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I

    if-eq v0, v1, :cond_3

    .line 312
    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->showSurface()V

    .line 314
    :cond_3
    iput v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay;->mLastLevel:I

    .line 315
    return-void
.end method
