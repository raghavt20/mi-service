.class public Lcom/android/server/wm/MiuiMirrorDragEventImpl;
.super Ljava/lang/Object;
.source "MiuiMirrorDragEventImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiMirrorDragEventStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDragEvent(Lcom/android/server/wm/WindowState;FFLandroid/content/ClipData;Lcom/android/server/wm/DragState;Landroid/view/DragEvent;)Landroid/view/DragEvent;
    .locals 12
    .param p1, "newWin"    # Lcom/android/server/wm/WindowState;
    .param p2, "touchX"    # F
    .param p3, "touchY"    # F
    .param p4, "data"    # Landroid/content/ClipData;
    .param p5, "dragState"    # Lcom/android/server/wm/DragState;
    .param p6, "event"    # Landroid/view/DragEvent;

    .line 17
    move-object/from16 v0, p5

    const-string v1, "com.xiaomi.mirror"

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningUid()I

    move-result v1

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    .line 20
    .local v1, "targetUserId":I
    sget-object v2, Lcom/android/server/wm/GetDragEventProxy;->mFlags:Lcom/xiaomi/reflect/RefInt;

    invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I

    move-result v10

    .line 21
    .local v10, "flags":I
    and-int/lit16 v2, v10, 0x100

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/server/wm/GetDragEventProxy;->DRAG_FLAGS_URI_ACCESS:Lcom/xiaomi/reflect/RefInt;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I

    move-result v2

    and-int/2addr v2, v10

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/server/wm/GetDragEventProxy;->mData:Lcom/xiaomi/reflect/RefObject;

    .line 22
    invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 23
    new-instance v11, Lcom/android/server/wm/DragAndDropPermissionsHandler;

    sget-object v2, Lcom/android/server/wm/GetDragEventProxy;->mService:Lcom/xiaomi/reflect/RefObject;

    .line 24
    invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/WindowManagerService;

    iget-object v4, v2, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    sget-object v2, Lcom/android/server/wm/GetDragEventProxy;->mData:Lcom/xiaomi/reflect/RefObject;

    .line 25
    invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Landroid/content/ClipData;

    sget-object v2, Lcom/android/server/wm/GetDragEventProxy;->mUid:Lcom/xiaomi/reflect/RefInt;

    .line 26
    invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I

    move-result v6

    .line 27
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v7

    sget-object v2, Lcom/android/server/wm/GetDragEventProxy;->DRAG_FLAGS_URI_PERMISSIONS:Lcom/xiaomi/reflect/RefInt;

    .line 28
    invoke-virtual {v2, v3}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I

    move-result v2

    and-int v8, v10, v2

    sget-object v2, Lcom/android/server/wm/GetDragEventProxy;->mSourceUserId:Lcom/xiaomi/reflect/RefInt;

    .line 29
    invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I

    move-result v9

    move-object v2, v11

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-object v6, v7

    move v7, v8

    move v8, v9

    move v9, v1

    invoke-direct/range {v2 .. v9}, Lcom/android/server/wm/DragAndDropPermissionsHandler;-><init>(Lcom/android/server/wm/WindowManagerGlobalLock;Landroid/content/ClipData;ILjava/lang/String;III)V

    .local v2, "dragAndDropPermissions":Lcom/android/server/wm/DragAndDropPermissionsHandler;
    goto :goto_0

    .line 32
    .end local v2    # "dragAndDropPermissions":Lcom/android/server/wm/DragAndDropPermissionsHandler;
    :cond_0
    const/4 v2, 0x0

    .line 34
    .restart local v2    # "dragAndDropPermissions":Lcom/android/server/wm/DragAndDropPermissionsHandler;
    :goto_0
    sget-object v9, Lcom/android/server/wm/GetDragEventProxy;->obtainDragEvent:Lcom/xiaomi/reflect/RefMethod;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    sget-object v6, Lcom/android/server/wm/GetDragEventProxy;->mData:Lcom/xiaomi/reflect/RefObject;

    .line 35
    invoke-virtual {v6, v0}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object v8, v2

    filled-new-array/range {v3 .. v8}, [Ljava/lang/Object;

    move-result-object v3

    .line 34
    invoke-virtual {v9, v0, v3}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/DragEvent;

    .end local p6    # "event":Landroid/view/DragEvent;
    .local v3, "event":Landroid/view/DragEvent;
    goto :goto_1

    .line 17
    .end local v1    # "targetUserId":I
    .end local v2    # "dragAndDropPermissions":Lcom/android/server/wm/DragAndDropPermissionsHandler;
    .end local v3    # "event":Landroid/view/DragEvent;
    .end local v10    # "flags":I
    .restart local p6    # "event":Landroid/view/DragEvent;
    :cond_1
    move-object/from16 v3, p6

    .line 37
    .end local p6    # "event":Landroid/view/DragEvent;
    .restart local v3    # "event":Landroid/view/DragEvent;
    :goto_1
    return-object v3
.end method
