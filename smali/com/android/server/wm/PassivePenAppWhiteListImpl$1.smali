.class Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;
.super Landroid/content/BroadcastReceiver;
.source "PassivePenAppWhiteListImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/PassivePenAppWhiteListImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/PassivePenAppWhiteListImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    .line 140
    iput-object p1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 143
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "mAction":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    iget-object v1, v1, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/server/wm/ActivityTaskManagerService;->isKeyguardLocked(I)Z

    move-result v1

    .line 145
    .local v1, "isLocked":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onReceive= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "PassivePenAppWhiteListImpl"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/4 v4, 0x1

    sparse-switch v3, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v3, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    goto :goto_1

    :sswitch_1
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x3

    goto :goto_1

    :sswitch_2
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v2

    goto :goto_1

    :sswitch_3
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x2

    goto :goto_1

    :goto_0
    const/4 v3, -0x1

    :goto_1
    packed-switch v3, :pswitch_data_0

    goto :goto_2

    .line 159
    :pswitch_0
    iget-object v2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "passivepen"

    invoke-static {v2, v3, v4}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->-$$Nest$mreadLocalCloudControlData(Lcom/android/server/wm/PassivePenAppWhiteListImpl;Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 160
    goto :goto_2

    .line 154
    :pswitch_1
    iget-object v3, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-static {v3}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->-$$Nest$fgetwhiteListTopApp(Lcom/android/server/wm/PassivePenAppWhiteListImpl;)I

    move-result v3

    if-ne v3, v4, :cond_1

    .line 155
    iget-object v3, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-virtual {v3, v2}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->dispatchTopAppWhiteState(I)Z

    goto :goto_2

    .line 149
    :pswitch_2
    iget-object v2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-static {v2}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->-$$Nest$fgetwhiteListTopApp(Lcom/android/server/wm/PassivePenAppWhiteListImpl;)I

    move-result v2

    if-ne v2, v4, :cond_1

    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    iget-boolean v2, v2, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->splitMode:Z

    if-nez v2, :cond_1

    .line 150
    iget-object v2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-virtual {v2, v4}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->dispatchTopAppWhiteState(I)Z

    .line 163
    :cond_1
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ed8ea7f -> :sswitch_3
        -0x56ac2893 -> :sswitch_2
        0x2f94f923 -> :sswitch_1
        0x311a1d6c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
