class com.android.server.wm.MiuiSizeCompatService$UiHandler extends android.os.Handler {
	 /* .source "MiuiSizeCompatService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiSizeCompatService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "UiHandler" */
} // .end annotation
/* # static fields */
public static final Integer MSG_SHOW_APP_RESTART_DIALOG;
/* # instance fields */
final com.android.server.wm.MiuiSizeCompatService this$0; //synthetic
/* # direct methods */
public com.android.server.wm.MiuiSizeCompatService$UiHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 670 */
this.this$0 = p1;
/* .line 671 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 672 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 676 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 687 */
/* new-instance v0, Ljava/lang/IllegalStateException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Unexpected value: "; // const-string v2, "Unexpected value: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 678 */
/* :pswitch_0 */
v0 = this.obj;
/* instance-of v0, v0, Lcom/android/server/wm/ActivityRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 680 */
	 try { // :try_start_0
		 v0 = this.this$0;
		 v1 = this.obj;
		 /* check-cast v1, Lcom/android/server/wm/ActivityRecord; */
		 com.android.server.wm.MiuiSizeCompatService .-$$Nest$mshowConfirmDialog ( v0,v1 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 681 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 682 */
		 /* .local v0, "e":Ljava/lang/Exception; */
		 (( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
		 /* .line 683 */
	 } // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* nop */
/* .line 689 */
} // :cond_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
