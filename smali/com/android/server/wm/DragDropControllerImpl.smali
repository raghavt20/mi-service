.class public Lcom/android/server/wm/DragDropControllerImpl;
.super Ljava/lang/Object;
.source "DragDropControllerImpl.java"

# interfaces
.implements Lcom/android/server/wm/DragDropControllerStub;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDragDropController:Lcom/android/server/wm/DragDropController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    const-class v0, Lcom/android/server/wm/DragDropControllerImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/DragDropControllerImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelCurrentDrag()Z
    .locals 4

    .line 54
    iget-object v0, p0, Lcom/android/server/wm/DragDropControllerImpl;->mDragDropController:Lcom/android/server/wm/DragDropController;

    invoke-virtual {v0}, Lcom/android/server/wm/DragDropController;->getDragState()Lcom/android/server/wm/DragState;

    move-result-object v0

    .line 55
    .local v0, "dragState":Lcom/android/server/wm/DragState;
    if-nez v0, :cond_0

    .line 56
    const/4 v1, 0x0

    return v1

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/DragDropControllerImpl;->mDragDropController:Lcom/android/server/wm/DragDropController;

    iget-object v2, v0, Lcom/android/server/wm/DragState;->mToken:Landroid/os/IBinder;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/DragDropController;->cancelDragAndDrop(Landroid/os/IBinder;Z)V

    .line 60
    return v3
.end method

.method public initDragDropController(Lcom/android/server/wm/DragDropController;)V
    .locals 0
    .param p1, "dragDropController"    # Lcom/android/server/wm/DragDropController;

    .line 18
    iput-object p1, p0, Lcom/android/server/wm/DragDropControllerImpl;->mDragDropController:Lcom/android/server/wm/DragDropController;

    .line 19
    return-void
.end method

.method public notifyDragFinish(Z)V
    .locals 3
    .param p1, "dragResult"    # Z

    .line 26
    iget-object v0, p0, Lcom/android/server/wm/DragDropControllerImpl;->mDragDropController:Lcom/android/server/wm/DragDropController;

    invoke-virtual {v0}, Lcom/android/server/wm/DragDropController;->getDragState()Lcom/android/server/wm/DragState;

    move-result-object v0

    .line 27
    .local v0, "dragState":Lcom/android/server/wm/DragState;
    const-string v1, ""

    .line 28
    .local v1, "packageName":Ljava/lang/String;
    iget-object v2, v0, Lcom/android/server/wm/DragState;->mDropWindow:Lcom/android/server/wm/WindowState;

    if-eqz v2, :cond_0

    .line 29
    iget-object v2, v0, Lcom/android/server/wm/DragState;->mDropWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v1

    .line 31
    :cond_0
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getInstance()Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    move-result-object v2

    invoke-virtual {v2, v1, p1}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->notifyDragFinish(Ljava/lang/String;Z)V

    .line 32
    return-void
.end method

.method public notifyDragStart(Landroid/content/ClipData;III)V
    .locals 1
    .param p1, "data"    # Landroid/content/ClipData;
    .param p2, "uid"    # I
    .param p3, "pid"    # I
    .param p4, "flag"    # I

    .line 22
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getInstance()Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->notifyDragStart(Landroid/content/ClipData;III)V

    .line 23
    return-void
.end method

.method public setDragSurfaceVisible(Z)Z
    .locals 5
    .param p1, "visible"    # Z

    .line 35
    iget-object v0, p0, Lcom/android/server/wm/DragDropControllerImpl;->mDragDropController:Lcom/android/server/wm/DragDropController;

    invoke-virtual {v0}, Lcom/android/server/wm/DragDropController;->getDragState()Lcom/android/server/wm/DragState;

    move-result-object v0

    .line 36
    .local v0, "dragState":Lcom/android/server/wm/DragState;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 37
    return v1

    .line 39
    :cond_0
    :try_start_0
    new-instance v2, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v2}, Landroid/view/SurfaceControl$Transaction;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .local v2, "transaction":Landroid/view/SurfaceControl$Transaction;
    if-eqz p1, :cond_1

    .line 41
    :try_start_1
    iget-object v3, v0, Lcom/android/server/wm/DragState;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    goto :goto_0

    .line 43
    :cond_1
    iget-object v3, v0, Lcom/android/server/wm/DragState;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 45
    :goto_0
    invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 46
    nop

    .line 47
    :try_start_2
    invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 46
    const/4 v1, 0x1

    return v1

    .line 39
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "dragState":Lcom/android/server/wm/DragState;
    .end local p0    # "this":Lcom/android/server/wm/DragDropControllerImpl;
    .end local p1    # "visible":Z
    :goto_1
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 47
    .end local v2    # "transaction":Landroid/view/SurfaceControl$Transaction;
    .restart local v0    # "dragState":Lcom/android/server/wm/DragState;
    .restart local p0    # "this":Lcom/android/server/wm/DragDropControllerImpl;
    .restart local p1    # "visible":Z
    :catch_0
    move-exception v2

    .line 48
    .local v2, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/android/server/wm/DragDropControllerImpl;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    .end local v2    # "e":Ljava/lang/Exception;
    return v1
.end method
