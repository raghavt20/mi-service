.class Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;
.super Ljava/lang/Object;
.source "ScreenRotationAnimationImpl.java"

# interfaces
.implements Lcom/android/server/wm/LocalAnimationAdapter$AnimationSpec;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/ScreenRotationAnimationImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScreenRotationSpec"
.end annotation


# instance fields
.field private mAnimation:Landroid/view/animation/Animation;

.field private mHeight:I

.field private final mThreadLocalTmps:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;",
            ">;"
        }
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/view/animation/Animation;II)V
    .locals 1
    .param p1, "animation"    # Landroid/view/animation/Animation;
    .param p2, "originalWidth"    # I
    .param p3, "originalHeight"    # I

    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364
    new-instance v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec$$ExternalSyntheticLambda0;-><init>()V

    .line 365
    invoke-static {v0}, Ljava/lang/ThreadLocal;->withInitial(Ljava/util/function/Supplier;)Ljava/lang/ThreadLocal;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mThreadLocalTmps:Ljava/lang/ThreadLocal;

    .line 370
    iput-object p1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mAnimation:Landroid/view/animation/Animation;

    .line 371
    iput p2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mWidth:I

    .line 372
    iput p3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mHeight:I

    .line 373
    return-void
.end method


# virtual methods
.method public apply(Landroid/view/SurfaceControl$Transaction;Landroid/view/SurfaceControl;J)V
    .locals 6
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "leash"    # Landroid/view/SurfaceControl;
    .param p3, "currentPlayTime"    # J

    .line 383
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mThreadLocalTmps:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;

    .line 384
    .local v0, "tmp":Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;
    iget-object v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->transformation:Landroid/view/animation/Transformation;

    invoke-virtual {v1}, Landroid/view/animation/Transformation;->clear()V

    .line 385
    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mAnimation:Landroid/view/animation/Animation;

    iget-object v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->transformation:Landroid/view/animation/Transformation;

    invoke-virtual {v1, p3, p4, v2}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    .line 386
    iget-object v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->transformation:Landroid/view/animation/Transformation;

    invoke-virtual {v1}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    iget-object v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->floats:[F

    invoke-virtual {p1, p2, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;Landroid/graphics/Matrix;[F)Landroid/view/SurfaceControl$Transaction;

    .line 387
    iget-object v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->transformation:Landroid/view/animation/Transformation;

    invoke-virtual {v1}, Landroid/view/animation/Transformation;->getAlpha()F

    move-result v1

    invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 388
    iget-object v1, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->transformation:Landroid/view/animation/Transformation;

    invoke-virtual {v1}, Landroid/view/animation/Transformation;->hasClipRect()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 389
    sget v1, Lcom/android/server/wm/AppTransitionInjector;->DISPLAY_ROUND_CORNER_RADIUS:I

    int-to-float v1, v1

    invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setCornerRadius(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 390
    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->transformation:Landroid/view/animation/Transformation;

    invoke-virtual {v2}, Landroid/view/animation/Transformation;->getClipRect()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->transformation:Landroid/view/animation/Transformation;

    .line 391
    invoke-virtual {v3}, Landroid/view/animation/Transformation;->getClipRect()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->transformation:Landroid/view/animation/Transformation;

    .line 392
    invoke-virtual {v4}, Landroid/view/animation/Transformation;->getClipRect()Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;->transformation:Landroid/view/animation/Transformation;

    .line 393
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getClipRect()Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 390
    invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;

    goto :goto_0

    .line 395
    :cond_0
    sget v1, Lcom/android/server/wm/AppTransitionInjector;->DISPLAY_ROUND_CORNER_RADIUS:I

    int-to-float v1, v1

    invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setCornerRadius(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 396
    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mWidth:I

    iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mHeight:I

    const/4 v4, 0x0

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;

    .line 398
    :goto_0
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 402
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 403
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 404
    return-void
.end method

.method public dumpDebugInner(Landroid/util/proto/ProtoOutputStream;)V
    .locals 5
    .param p1, "proto"    # Landroid/util/proto/ProtoOutputStream;

    .line 408
    const-wide v0, 0x10b00000001L

    invoke-virtual {p1, v0, v1}, Landroid/util/proto/ProtoOutputStream;->start(J)J

    move-result-wide v0

    .line 409
    .local v0, "token":J
    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide v3, 0x10900000001L

    invoke-virtual {p1, v3, v4, v2}, Landroid/util/proto/ProtoOutputStream;->write(JLjava/lang/String;)V

    .line 410
    invoke-virtual {p1, v0, v1}, Landroid/util/proto/ProtoOutputStream;->end(J)V

    .line 411
    return-void
.end method

.method public getDuration()J
    .locals 2

    .line 377
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->computeDurationHint()J

    move-result-wide v0

    return-wide v0
.end method
