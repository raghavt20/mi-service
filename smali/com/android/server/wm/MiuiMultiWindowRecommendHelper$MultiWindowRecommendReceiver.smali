.class final Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiuiMultiWindowRecommendHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MultiWindowRecommendReceiver"
.end annotation


# static fields
.field private static final ACTION_FREE_FORM_RECOMMEND:Ljava/lang/String; = "com.miui.freeform_recommend"

.field private static final RECOMMEND_SCENE:Ljava/lang/String; = "recommendScene"

.field private static final RECOMMEND_TRANSACTION_TYPE:Ljava/lang/String; = "recommendTransactionType"

.field private static final SENDER_PACKAGENAME:Ljava/lang/String; = "senderPackageName"


# instance fields
.field mFilter:Landroid/content/IntentFilter;

.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V
    .locals 1

    .line 195
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 193
    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;->mFilter:Landroid/content/IntentFilter;

    .line 196
    const-string v0, "com.miui.freeform_recommend"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 197
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 201
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$misDeviceSupportFreeFormRecommend(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    return-void

    .line 204
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.miui.freeform_recommend"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 206
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 207
    .local v1, "bundle":Landroid/os/Bundle;
    if-nez v1, :cond_1

    return-void

    .line 208
    :cond_1
    const-string/jumbo v2, "senderPackageName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 209
    .local v2, "senderPackageName":Ljava/lang/String;
    const-string v3, "recommendTransactionType"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 210
    .local v3, "recommendTransactionType":I
    const-string v4, "recommendScene"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 211
    .local v4, "recommendScene":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive: senderPackageName= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " recommendTransactionType= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " recommendScene= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MiuiMultiWindowRecommendHelper"

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v5, v2, v3, v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$mFreeFormRecommendIfNeeded(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Ljava/lang/String;II)V

    .line 216
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "senderPackageName":Ljava/lang/String;
    .end local v3    # "recommendTransactionType":I
    .end local v4    # "recommendScene":I
    :cond_2
    return-void
.end method
