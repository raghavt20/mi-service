class com.android.server.wm.OneTrackRotationHelper$1 extends android.os.Handler {
	 /* .source "OneTrackRotationHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/OneTrackRotationHelper;->initOneTrackRotationThread()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.OneTrackRotationHelper this$0; //synthetic
/* # direct methods */
 com.android.server.wm.OneTrackRotationHelper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/OneTrackRotationHelper; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 245 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 1 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 248 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 259 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetmRotationStateMachine ( v0 );
(( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine ) v0 ).onDeviceFoldChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onDeviceFoldChanged(Landroid/os/Message;)V
/* .line 260 */
/* .line 268 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetmRotationStateMachine ( v0 );
com.android.server.wm.OneTrackRotationHelper$RotationStateMachine .-$$Nest$monTodayIsOver ( v0 );
/* .line 265 */
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetmRotationStateMachine ( v0 );
(( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine ) v0 ).onNextSending ( ); // invoke-virtual {v0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onNextSending()V
/* .line 266 */
/* .line 262 */
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetmRotationStateMachine ( v0 );
(( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine ) v0 ).onShutDown ( ); // invoke-virtual {v0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onShutDown()V
/* .line 263 */
/* .line 256 */
/* :pswitch_4 */
v0 = this.this$0;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetmRotationStateMachine ( v0 );
(( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine ) v0 ).onScreenStateChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onScreenStateChanged(Landroid/os/Message;)V
/* .line 257 */
/* .line 250 */
/* :pswitch_5 */
v0 = this.this$0;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetmRotationStateMachine ( v0 );
(( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine ) v0 ).onForegroundAppChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onForegroundAppChanged(Landroid/os/Message;)V
/* .line 251 */
/* .line 253 */
/* :pswitch_6 */
v0 = this.this$0;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetmRotationStateMachine ( v0 );
(( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine ) v0 ).onRotationChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onRotationChanged(Landroid/os/Message;)V
/* .line 254 */
/* nop */
/* .line 270 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
