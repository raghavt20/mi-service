.class final Lcom/android/server/wm/PolicyItem;
.super Ljava/lang/Object;
.source "PolicyItem.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x2f0880a250L


# instance fields
.field private mCurrentVersion:Ljava/lang/String;

.field private final mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/wm/PackageConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalVersion:I

.field private mScpmVersion:I


# direct methods
.method public static synthetic $r8$lambda$vsQWkpaZ_b52oQ5l4QCucZTJRuM(Lcom/android/server/wm/PolicyItem;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/PolicyItem;->lambda$new$0(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 16
    .local p1, "policySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PolicyItem;->mList:Ljava/util/List;

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/PolicyItem;->mScpmVersion:I

    .line 13
    const-string v1, ""

    iput-object v1, p0, Lcom/android/server/wm/PolicyItem;->mCurrentVersion:Ljava/lang/String;

    .line 14
    iput v0, p0, Lcom/android/server/wm/PolicyItem;->mLocalVersion:I

    .line 17
    new-instance v0, Lcom/android/server/wm/PolicyItem$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/wm/PolicyItem$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/PolicyItem;)V

    invoke-interface {p1, v0}, Ljava/util/Set;->forEach(Ljava/util/function/Consumer;)V

    .line 21
    return-void
.end method

.method private synthetic lambda$new$0(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .line 18
    new-instance v0, Lcom/android/server/wm/PackageConfiguration;

    invoke-direct {v0, p1}, Lcom/android/server/wm/PackageConfiguration;-><init>(Ljava/lang/String;)V

    .line 19
    .local v0, "pkgConfig":Lcom/android/server/wm/PackageConfiguration;
    iget-object v1, p0, Lcom/android/server/wm/PolicyItem;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    return-void
.end method


# virtual methods
.method getCurrentVersion()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/android/server/wm/PolicyItem;->mCurrentVersion:Ljava/lang/String;

    return-object v0
.end method

.method getLocalVersion()I
    .locals 1

    .line 44
    iget v0, p0, Lcom/android/server/wm/PolicyItem;->mLocalVersion:I

    return v0
.end method

.method getPackageConfigurationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/server/wm/PackageConfiguration;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/android/server/wm/PolicyItem;->mList:Ljava/util/List;

    return-object v0
.end method

.method getScpmVersion()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/android/server/wm/PolicyItem;->mScpmVersion:I

    return v0
.end method

.method isMismatch(Ljava/util/Set;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 52
    .local p1, "policySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/wm/PolicyItem;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/PolicyItem;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    if-eq v0, v2, :cond_0

    goto :goto_0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/PolicyItem;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 57
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PackageConfiguration;>;"
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 58
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/PackageConfiguration;

    iget-object v2, v2, Lcom/android/server/wm/PackageConfiguration;->mName:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 59
    return v1

    .line 62
    :cond_2
    const/4 v1, 0x0

    return v1

    .line 53
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PackageConfiguration;>;"
    :cond_3
    :goto_0
    return v1
.end method

.method setCurrentVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentVersion"    # Ljava/lang/String;

    .line 40
    iput-object p1, p0, Lcom/android/server/wm/PolicyItem;->mCurrentVersion:Ljava/lang/String;

    .line 41
    return-void
.end method

.method setLocalVersion(I)V
    .locals 0
    .param p1, "localVersion"    # I

    .line 48
    iput p1, p0, Lcom/android/server/wm/PolicyItem;->mLocalVersion:I

    .line 49
    return-void
.end method

.method setScpmVersion(I)V
    .locals 0
    .param p1, "scpmVersion"    # I

    .line 32
    iput p1, p0, Lcom/android/server/wm/PolicyItem;->mScpmVersion:I

    .line 33
    return-void
.end method
