.class public Lcom/android/server/wm/SplitScreenRecommendLayout;
.super Landroid/widget/FrameLayout;
.source "SplitScreenRecommendLayout.java"


# static fields
.field private static final RECOMMEND_VIEW_TOP_MARGIN:F = 37.0f

.field private static final RECOMMEND_VIEW_TOP_MARGIN_OFFSET:F = 10.0f

.field private static final TAG:Ljava/lang/String; = "SplitScreenRecommendLayout"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPrimaryImageView:Landroid/widget/ImageView;

.field private mSecondaryImageView:Landroid/widget/ImageView;

.field private mSplitScreenIconContainer:Landroid/widget/RelativeLayout;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 42
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-virtual {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->init(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    invoke-virtual {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->init(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    invoke-virtual {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->init(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 58
    invoke-virtual {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->init(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method private dipToPx(F)F
    .locals 2
    .param p1, "dip"    # F

    .line 138
    nop

    .line 139
    invoke-virtual {p0}, Lcom/android/server/wm/SplitScreenRecommendLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 138
    const/4 v1, 0x1

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method private setRadius(Landroid/view/View;F)V
    .locals 1
    .param p1, "content"    # Landroid/view/View;
    .param p2, "radius"    # F

    .line 143
    new-instance v0, Lcom/android/server/wm/SplitScreenRecommendLayout$1;

    invoke-direct {v0, p0, p2}, Lcom/android/server/wm/SplitScreenRecommendLayout$1;-><init>(Lcom/android/server/wm/SplitScreenRecommendLayout;F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 149
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClipToOutline(Z)V

    .line 150
    return-void
.end method


# virtual methods
.method public createLayoutParams(I)Landroid/view/WindowManager$LayoutParams;
    .locals 9
    .param p1, "topMargin"    # I

    .line 94
    const/4 v6, -0x2

    .line 95
    .local v6, "width":I
    const/4 v7, -0x2

    .line 96
    .local v7, "height":I
    new-instance v8, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7f6

    const v4, 0x1000528

    const/4 v5, 0x1

    move-object v0, v8

    move v1, v6

    move v2, v7

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 106
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 107
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x40

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 108
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    const/high16 v2, 0x20000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 109
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setFitInsetsTypes(I)V

    .line 110
    const/4 v2, 0x1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 111
    const/4 v2, -0x1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 112
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->rotationAnimation:I

    .line 113
    const/16 v2, 0x31

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 114
    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {p0, v2}, Lcom/android/server/wm/SplitScreenRecommendLayout;->dipToPx(F)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v2, p1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 115
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 116
    const-string v1, "SplitScreen-RecommendView"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 117
    return-object v0
.end method

.method public getPrimaryImageView()Landroid/widget/ImageView;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mPrimaryImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getSecondaryImageView()Landroid/widget/ImageView;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mSecondaryImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getSplitScreenIconContainer()Landroid/widget/RelativeLayout;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mSplitScreenIconContainer:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 62
    const-string v0, "SplitScreenRecommendLayout"

    const-string v1, "init splitScreenRecommendLayout "

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iput-object p1, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mContext:Landroid/content/Context;

    .line 64
    const-string/jumbo v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mWindowManager:Landroid/view/WindowManager;

    .line 65
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .line 83
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 84
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConfigurationChanged newConfig.orientation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SplitScreenRecommendLayout"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 89
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 90
    const-string v0, "SplitScreenRecommendLayout"

    const-string v1, "onDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 74
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 75
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/SplitScreenRecommendLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mSplitScreenIconContainer:Landroid/widget/RelativeLayout;

    .line 76
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mPrimaryImageView:Landroid/widget/ImageView;

    .line 77
    iget-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mSplitScreenIconContainer:Landroid/widget/RelativeLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mSecondaryImageView:Landroid/widget/ImageView;

    .line 78
    const-string v0, "SplitScreenRecommendLayout"

    const-string v1, "onFinishInflate"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return-void
.end method

.method public setSplitScreenIcon(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "primaryDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "secondaryDrawble"    # Landroid/graphics/drawable/Drawable;

    .line 133
    iget-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mPrimaryImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 134
    iget-object v0, p0, Lcom/android/server/wm/SplitScreenRecommendLayout;->mSecondaryImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 135
    return-void
.end method
