class com.android.server.wm.TaskSnapshotControllerInjectorImpl extends com.android.server.wm.TaskSnapshotControllerInjectorStub {
	 /* .source "TaskSnapshotControllerInjectorImpl.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public static void $r8$lambda$QavdEXcV9xH1ZFJZ3ISxlrC3VSY ( com.android.server.wm.TaskSnapshotControllerInjectorImpl p0, com.android.server.wm.ActivityRecord p1, Boolean p2, com.android.server.wm.TaskSnapshotController p3, com.android.server.wm.TaskSnapshotPersister p4, Boolean p5 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct/range {p0 ..p5}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->lambda$doSnapshotQS$0(Lcom/android/server/wm/ActivityRecord;ZLcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Z)V */
		 return;
	 } // .end method
	 com.android.server.wm.TaskSnapshotControllerInjectorImpl ( ) {
		 /* .locals 0 */
		 /* .line 23 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/TaskSnapshotControllerInjectorStub;-><init>()V */
		 return;
	 } // .end method
	 private void doSnapshotQSInternal ( com.android.server.wm.TaskSnapshotController p0, com.android.server.wm.TaskSnapshotPersister p1, com.android.server.wm.ActivityRecord p2, Boolean p3, Boolean p4 ) {
		 /* .locals 7 */
		 /* .param p1, "controller" # Lcom/android/server/wm/TaskSnapshotController; */
		 /* .param p2, "persister" # Lcom/android/server/wm/TaskSnapshotPersister; */
		 /* .param p3, "r" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p4, "visible" # Z */
		 /* .param p5, "forceUpdate" # Z */
		 /* .line 78 */
		 (( com.android.server.wm.ActivityRecord ) p3 ).getTask ( ); // invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
		 /* .line 79 */
		 /* .local v0, "task":Lcom/android/server/wm/Task; */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 v1 = 			 (( com.android.server.wm.Task ) v0 ).isVisible ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->isVisible()Z
			 /* if-nez v1, :cond_1 */
		 } // :cond_0
		 /* if-nez p5, :cond_1 */
		 /* .line 80 */
		 return;
		 /* .line 82 */
	 } // :cond_1
	 v1 = 	 (( com.android.server.wm.Task ) v0 ).isActivityTypeHome ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->isActivityTypeHome()Z
	 final String v2 = "TaskSnapshot_CInjector"; // const-string v2, "TaskSnapshot_CInjector"
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 83 */
		 final String v1 = "doSnapshotQSInternal()...return as isActivityTypeHome!"; // const-string v1, "doSnapshotQSInternal()...return as isActivityTypeHome!"
		 android.util.Slog .w ( v2,v1 );
		 /* .line 84 */
		 return;
		 /* .line 88 */
	 } // :cond_2
	 try { // :try_start_0
		 v1 = this.mDisplayContent;
		 v1 = this.mCurrentFocus;
		 (( com.android.server.wm.WindowState ) v1 ).getWindowInfo ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->getWindowInfo()Landroid/view/WindowInfo;
		 /* iget v1, v1, Landroid/view/WindowInfo;->type:I */
		 /* .line 89 */
		 /* .local v1, "windowType":I */
		 int v3 = 1; // const/4 v3, 0x1
		 /* if-eq v1, v3, :cond_3 */
		 /* .line 90 */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "doSnapshotQSInternal()...return as window type is incorrect, type="; // const-string v4, "doSnapshotQSInternal()...return as window type is incorrect, type="
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .w ( v2,v3 );
		 /* .line 91 */
		 return;
		 /* .line 94 */
	 } // :cond_3
	 com.android.server.wm.TaskSnapshotPersisterInjectorStub .get ( );
	 /* new-instance v5, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v5}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$$ExternalSyntheticLambda0;-><init>()V */
	 v4 = 	 (( com.android.server.wm.TaskSnapshotPersisterInjectorStub ) v4 ).couldPersist ( v5, p3 ); // invoke-virtual {v4, v5, p3}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorStub;->couldPersist(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;Lcom/android/server/wm/ActivityRecord;)Z
	 /* if-nez v4, :cond_4 */
	 /* .line 95 */
	 final String v3 = "couldPersist()...false."; // const-string v3, "couldPersist()...false."
	 android.util.Slog .w ( v2,v3 );
	 /* .line 96 */
	 return;
	 /* .line 99 */
} // :cond_4
v4 = (( com.android.server.wm.TaskSnapshotController ) p1 ).getSnapshotMode ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/TaskSnapshotController;->getSnapshotMode(Lcom/android/server/wm/WindowContainer;)I
/* packed-switch v4, :pswitch_data_0 */
/* .line 106 */
/* .line 101 */
/* :pswitch_0 */
(( com.android.server.wm.TaskSnapshotController ) p1 ).snapshot ( v0, v3 ); // invoke-virtual {p1, v0, v3}, Lcom/android/server/wm/TaskSnapshotController;->snapshot(Lcom/android/server/wm/WindowContainer;Z)Landroid/window/TaskSnapshot;
/* .line 102 */
/* .local v3, "snapshot":Landroid/window/TaskSnapshot; */
/* .line 106 */
} // .end local v3 # "snapshot":Landroid/window/TaskSnapshot;
} // :goto_0
final String v3 = "doSnapshotQSInternal()...snapshot mode NOT ok!"; // const-string v3, "doSnapshotQSInternal()...snapshot mode NOT ok!"
android.util.Slog .w ( v2,v3 );
/* .line 107 */
int v3 = 0; // const/4 v3, 0x0
/* .line 111 */
/* .restart local v3 # "snapshot":Landroid/window/TaskSnapshot; */
} // :goto_1
/* if-nez v3, :cond_6 */
if ( p5 != null) { // if-eqz p5, :cond_6
/* .line 112 */
v4 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_STARTING_WINDOW;
v4 = (( com.android.internal.protolog.ProtoLogGroup ) v4 ).isLogToLogcat ( ); // invoke-virtual {v4}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 113 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "snapshot is null and use snapshot if needed.current state " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.ActivityRecord ) p3 ).getState ( ); // invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;
(( com.android.server.wm.ActivityRecord$State ) v5 ).name ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord$State;->name()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* .line 115 */
} // :cond_5
v4 = com.android.server.wm.ActivityRecord$State.RESUMED;
v4 = (( com.android.server.wm.ActivityRecord ) p3 ).isState ( v4 ); // invoke-virtual {p3, v4}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z
/* if-nez v4, :cond_6 */
/* .line 116 */
/* iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I */
/* iget v5, v0, Lcom/android/server/wm/Task;->mUserId:I */
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.wm.TaskSnapshotController ) p1 ).getSnapshot ( v4, v5, v6, v6 ); // invoke-virtual {p1, v4, v5, v6, v6}, Lcom/android/server/wm/TaskSnapshotController;->getSnapshot(IIZZ)Landroid/window/TaskSnapshot;
/* move-object v3, v4 */
/* .line 119 */
} // :cond_6
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 120 */
v4 = android.app.TaskSnapshotHelperImpl.QUICK_START_NAME_WITH_ACTIVITY_LIST;
v4 = v5 = this.packageName;
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 121 */
v4 = this.intent;
(( android.content.Intent ) v4 ).getComponent ( ); // invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v4 ).getClassName ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
(( android.window.TaskSnapshot ) v3 ).setClassNameQS ( v4 ); // invoke-virtual {v3, v4}, Landroid/window/TaskSnapshot;->setClassNameQS(Ljava/lang/String;)V
/* .line 125 */
} // :cond_7
/* invoke-direct {p0, v3, v0, p2}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->saveSnapshot(Landroid/window/TaskSnapshot;Lcom/android/server/wm/Task;Lcom/android/server/wm/TaskSnapshotPersister;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 128 */
} // .end local v1 # "windowType":I
} // .end local v3 # "snapshot":Landroid/window/TaskSnapshot;
/* .line 126 */
/* :catch_0 */
/* move-exception v1 */
/* .line 127 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "exception:"; // const-string v4, "exception:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 129 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void lambda$doSnapshotQS$0 ( com.android.server.wm.ActivityRecord p0, Boolean p1, com.android.server.wm.TaskSnapshotController p2, com.android.server.wm.TaskSnapshotPersister p3, Boolean p4 ) { //synthethic
/* .locals 7 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "forceUpdate" # Z */
/* .param p3, "controller" # Lcom/android/server/wm/TaskSnapshotController; */
/* .param p4, "persister" # Lcom/android/server/wm/TaskSnapshotPersister; */
/* .param p5, "visible" # Z */
/* .line 54 */
final String v0 = "TaskSnapshot_CInjector"; // const-string v0, "TaskSnapshot_CInjector"
try { // :try_start_0
v1 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_STARTING_WINDOW;
v1 = (( com.android.internal.protolog.ProtoLogGroup ) v1 ).isLogToLogcat ( ); // invoke-virtual {v1}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 55 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "doSnapshotQS()...later! state="; // const-string v2, "doSnapshotQS()...later! state="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.ActivityRecord ) p1 ).getState ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;
com.android.server.wm.TaskSnapshotControllerInjectorImpl .toState ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 58 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) p1 ).getState ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;
v2 = com.android.server.wm.ActivityRecord$State.RESUMED;
/* if-ne v1, v2, :cond_1 */
/* .line 59 */
v1 = (( com.android.server.wm.TaskSnapshotControllerInjectorImpl ) p0 ).canTakeSnapshot ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->canTakeSnapshot(Lcom/android/server/wm/ActivityRecord;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 60 */
v1 = (( com.android.server.wm.TaskSnapshotControllerInjectorImpl ) p0 ).isTop ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->isTop(Lcom/android/server/wm/ActivityRecord;)Z
/* if-nez v1, :cond_2 */
} // :cond_1
/* if-nez p2, :cond_2 */
/* .line 61 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "doSnapshotQS()...launchedBy="; // const-string v2, "doSnapshotQS()...launchedBy="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.launchedFromPackage;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 62 */
return;
/* .line 66 */
} // :cond_2
/* move-object v1, p0 */
/* move-object v2, p3 */
/* move-object v3, p4 */
/* move-object v4, p1 */
/* move v5, p5 */
/* move v6, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->doSnapshotQSInternal(Lcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Lcom/android/server/wm/ActivityRecord;ZZ)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 69 */
/* .line 67 */
/* :catch_0 */
/* move-exception v1 */
/* .line 68 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "exception:"; // const-string v2, "exception:"
android.util.Slog .e ( v0,v2,v1 );
/* .line 70 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void saveSnapshot ( android.window.TaskSnapshot p0, com.android.server.wm.Task p1, com.android.server.wm.TaskSnapshotPersister p2 ) {
/* .locals 4 */
/* .param p1, "snapshot" # Landroid/window/TaskSnapshot; */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .param p3, "persister" # Lcom/android/server/wm/TaskSnapshotPersister; */
/* .line 131 */
final String v0 = "TaskSnapshot_CInjector"; // const-string v0, "TaskSnapshot_CInjector"
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 132 */
(( android.window.TaskSnapshot ) p1 ).getSnapshot ( ); // invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getSnapshot()Landroid/graphics/GraphicBuffer;
/* .line 133 */
/* .local v1, "buffer":Landroid/graphics/GraphicBuffer; */
v2 = (( android.graphics.GraphicBuffer ) v1 ).getWidth ( ); // invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->getWidth()I
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = (( android.graphics.GraphicBuffer ) v1 ).getHeight ( ); // invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->getHeight()I
/* if-nez v2, :cond_0 */
/* .line 138 */
} // :cond_0
/* iget v0, p2, Lcom/android/server/wm/Task;->mTaskId:I */
/* iget v2, p2, Lcom/android/server/wm/Task;->mUserId:I */
(( com.android.server.wm.TaskSnapshotPersister ) p3 ).persistSnapshotQS ( v0, v2, p1 ); // invoke-virtual {p3, v0, v2, p1}, Lcom/android/server/wm/TaskSnapshotPersister;->persistSnapshotQS(IILandroid/window/TaskSnapshot;)V
/* .line 134 */
} // :cond_1
} // :goto_0
(( android.graphics.GraphicBuffer ) v1 ).destroy ( ); // invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->destroy()V
/* .line 135 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Invalid task snapshot dimensions "; // const-string v3, "Invalid task snapshot dimensions "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( android.graphics.GraphicBuffer ) v1 ).getWidth ( ); // invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->getWidth()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "*"; // const-string v3, "*"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 136 */
v3 = (( android.graphics.GraphicBuffer ) v1 ).getHeight ( ); // invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->getHeight()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 135 */
android.util.Slog .e ( v0,v2 );
/* .line 140 */
} // .end local v1 # "buffer":Landroid/graphics/GraphicBuffer;
} // :goto_1
/* .line 141 */
} // :cond_2
final String v1 = "doSnapshotQSInternal()...snapshot is null!"; // const-string v1, "doSnapshotQSInternal()...snapshot is null!"
android.util.Slog .w ( v0,v1 );
/* .line 143 */
} // :goto_2
return;
} // .end method
public static java.lang.String toState ( com.android.server.wm.ActivityRecord$State p0 ) {
/* .locals 1 */
/* .param p0, "state" # Lcom/android/server/wm/ActivityRecord$State; */
/* .line 160 */
v0 = com.android.server.wm.ActivityRecord$State.DESTROYED;
/* if-ne p0, v0, :cond_0 */
/* .line 161 */
final String v0 = "DESTROYED"; // const-string v0, "DESTROYED"
/* .line 163 */
} // :cond_0
v0 = com.android.server.wm.ActivityRecord$State.DESTROYING;
/* if-ne p0, v0, :cond_1 */
/* .line 164 */
final String v0 = "DESTROYING"; // const-string v0, "DESTROYING"
/* .line 166 */
} // :cond_1
v0 = com.android.server.wm.ActivityRecord$State.FINISHING;
/* if-ne p0, v0, :cond_2 */
/* .line 167 */
final String v0 = "FINISHING"; // const-string v0, "FINISHING"
/* .line 169 */
} // :cond_2
v0 = com.android.server.wm.ActivityRecord$State.INITIALIZING;
/* if-ne p0, v0, :cond_3 */
/* .line 170 */
final String v0 = "INITIALIZING"; // const-string v0, "INITIALIZING"
/* .line 172 */
} // :cond_3
v0 = com.android.server.wm.ActivityRecord$State.PAUSED;
/* if-ne p0, v0, :cond_4 */
/* .line 173 */
final String v0 = "PAUSED"; // const-string v0, "PAUSED"
/* .line 175 */
} // :cond_4
v0 = com.android.server.wm.ActivityRecord$State.PAUSING;
/* if-ne p0, v0, :cond_5 */
/* .line 176 */
final String v0 = "PAUSING"; // const-string v0, "PAUSING"
/* .line 178 */
} // :cond_5
v0 = com.android.server.wm.ActivityRecord$State.RESTARTING_PROCESS;
/* if-ne p0, v0, :cond_6 */
/* .line 179 */
final String v0 = "RESTARTING_PROCESS"; // const-string v0, "RESTARTING_PROCESS"
/* .line 181 */
} // :cond_6
v0 = com.android.server.wm.ActivityRecord$State.RESUMED;
/* if-ne p0, v0, :cond_7 */
/* .line 182 */
final String v0 = "RESUMED"; // const-string v0, "RESUMED"
/* .line 184 */
} // :cond_7
v0 = com.android.server.wm.ActivityRecord$State.STARTED;
/* if-ne p0, v0, :cond_8 */
/* .line 185 */
final String v0 = "STARTED"; // const-string v0, "STARTED"
/* .line 187 */
} // :cond_8
v0 = com.android.server.wm.ActivityRecord$State.STOPPED;
/* if-ne p0, v0, :cond_9 */
/* .line 188 */
final String v0 = "STOPPED"; // const-string v0, "STOPPED"
/* .line 190 */
} // :cond_9
v0 = com.android.server.wm.ActivityRecord$State.STOPPING;
/* if-ne p0, v0, :cond_a */
/* .line 191 */
final String v0 = "STOPPING"; // const-string v0, "STOPPING"
/* .line 193 */
} // :cond_a
final String v0 = "Unknown"; // const-string v0, "Unknown"
} // .end method
/* # virtual methods */
public void adaptLetterboxInsets ( com.android.server.wm.ActivityRecord p0, android.graphics.Rect p1 ) {
/* .locals 2 */
/* .param p1, "activity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "letterboxInsets" # Landroid/graphics/Rect; */
/* .line 203 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 204 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 205 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 206 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).adaptLetterboxInsets ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->adaptLetterboxInsets(Lcom/android/server/wm/ActivityRecord;Landroid/graphics/Rect;)V
/* .line 208 */
} // :cond_0
return;
} // .end method
public Boolean canTakeSnapshot ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 197 */
v0 = android.app.TaskSnapshotHelperImpl.QUICK_START_HOME_PACKAGE_NAME_LIST;
v0 = v1 = this.launchedFromPackage;
/* if-nez v0, :cond_1 */
v0 = android.app.TaskSnapshotHelperImpl.QUICK_START_SPECIAL_PACKAGE_NAME_LIST;
v1 = this.packageName;
v0 = /* .line 198 */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 197 */
} // :goto_1
} // .end method
public void doSnapshotQS ( com.android.server.wm.TaskSnapshotController p0, com.android.server.wm.TaskSnapshotPersister p1, android.os.Handler p2, com.android.server.wm.ActivityRecord p3, Boolean p4 ) {
/* .locals 7 */
/* .param p1, "controller" # Lcom/android/server/wm/TaskSnapshotController; */
/* .param p2, "persister" # Lcom/android/server/wm/TaskSnapshotPersister; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .param p4, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p5, "visible" # Z */
/* .line 32 */
int v6 = 1; // const/4 v6, 0x1
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* move-object v4, p4 */
/* move v5, p5 */
/* invoke-virtual/range {v0 ..v6}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->doSnapshotQS(Lcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Landroid/os/Handler;Lcom/android/server/wm/ActivityRecord;ZZ)V */
/* .line 33 */
return;
} // .end method
public void doSnapshotQS ( com.android.server.wm.TaskSnapshotController p0, com.android.server.wm.TaskSnapshotPersister p1, android.os.Handler p2, com.android.server.wm.ActivityRecord p3, Boolean p4, Boolean p5 ) {
/* .locals 9 */
/* .param p1, "controller" # Lcom/android/server/wm/TaskSnapshotController; */
/* .param p2, "persister" # Lcom/android/server/wm/TaskSnapshotPersister; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .param p4, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p5, "visible" # Z */
/* .param p6, "forceUpdate" # Z */
/* .line 41 */
if ( p1 != null) { // if-eqz p1, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
if ( p3 != null) { // if-eqz p3, :cond_2
/* if-nez p4, :cond_0 */
/* .line 45 */
} // :cond_0
v0 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_STARTING_WINDOW;
v0 = (( com.android.internal.protolog.ProtoLogGroup ) v0 ).isLogToLogcat ( ); // invoke-virtual {v0}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 46 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "doSnapshotQS()...ago! activity="; // const-string v1, "doSnapshotQS()...ago! activity="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mActivityComponent;
/* .line 47 */
(( android.content.ComponentName ) v1 ).flattenToShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", state="; // const-string v1, ", state="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 48 */
(( com.android.server.wm.ActivityRecord ) p4 ).getState ( ); // invoke-virtual {p4}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;
com.android.server.wm.TaskSnapshotControllerInjectorImpl .toState ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", lastVisibleTime="; // const-string v1, ", lastVisibleTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p4, Lcom/android/server/wm/ActivityRecord;->lastVisibleTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 46 */
final String v1 = "TaskSnapshot_CInjector"; // const-string v1, "TaskSnapshot_CInjector"
android.util.Slog .w ( v1,v0 );
/* .line 52 */
} // :cond_1
/* new-instance v0, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$$ExternalSyntheticLambda1; */
/* move-object v2, v0 */
/* move-object v3, p0 */
/* move-object v4, p4 */
/* move v5, p6 */
/* move-object v6, p1 */
/* move-object v7, p2 */
/* move v8, p5 */
/* invoke-direct/range {v2 ..v8}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;Lcom/android/server/wm/ActivityRecord;ZLcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Z)V */
/* .line 70 */
android.app.TaskSnapshotHelperStub .get ( );
v2 = this.packageName;
/* move-result-wide v1 */
/* .line 52 */
(( android.os.Handler ) p3 ).postDelayed ( v0, v1, v2 ); // invoke-virtual {p3, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 71 */
return;
/* .line 42 */
} // :cond_2
} // :goto_0
return;
} // .end method
public Boolean isTop ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 5 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 146 */
(( com.android.server.wm.ActivityRecord ) p1 ).getTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* .line 147 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 148 */
v1 = this.mActivityComponent;
(( android.content.ComponentName ) v1 ).flattenToShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* .line 149 */
/* .local v1, "current":Ljava/lang/String; */
(( com.android.server.wm.Task ) v0 ).topRunningActivityLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
v2 = this.mActivityComponent;
(( android.content.ComponentName ) v2 ).flattenToShortString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* .line 151 */
/* .local v2, "top":Ljava/lang/String; */
v3 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_STARTING_WINDOW;
v3 = (( com.android.internal.protolog.ProtoLogGroup ) v3 ).isLogToLogcat ( ); // invoke-virtual {v3}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 152 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "isTop()...current="; // const-string v4, "isTop()...current="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", topActivity="; // const-string v4, ", topActivity="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "TaskSnapshot_CInjector"; // const-string v4, "TaskSnapshot_CInjector"
android.util.Slog .w ( v4,v3 );
/* .line 154 */
} // :cond_0
v3 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 156 */
} // .end local v1 # "current":Ljava/lang/String;
} // .end local v2 # "top":Ljava/lang/String;
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
