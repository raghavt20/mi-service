.class Lcom/android/server/wm/MiuiFreeformTrackManager$3;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiFreeformTrackManager;->trackMiniWindowEnterWayEvent(Ljava/lang/String;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

.field final synthetic val$applicationName:Ljava/lang/String;

.field final synthetic val$enterWay:Ljava/lang/String;

.field final synthetic val$freeformAppCount:I

.field final synthetic val$leftTopPosition:Landroid/graphics/Point;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiFreeformTrackManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 248
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iput-object p2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$enterWay:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$leftTopPosition:Landroid/graphics/Point;

    iput-object p4, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$packageName:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$applicationName:Ljava/lang/String;

    iput p6, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$freeformAppCount:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 250
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mFreeFormTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 252
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 253
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 254
    .local v1, "jsonData":Lorg/json/JSONObject;
    const-string/jumbo v2, "tip"

    const-string v3, "621.2.0.1.14007"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 255
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v2, v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$mputCommomParam(Lcom/android/server/wm/MiuiFreeformTrackManager;Lorg/json/JSONObject;)V

    .line 256
    const-string v2, "EVENT_NAME"

    const-string v3, "enter"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 257
    const-string v2, "mini_window_enter_way"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$enterWay:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 258
    const-string v2, "pixel_x_location"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$leftTopPosition:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 259
    const-string v2, "pixel_y_location"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$leftTopPosition:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 260
    const-string v2, "app_package_name"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$packageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 261
    const-string v2, "app_display_name"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$applicationName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 262
    const-string/jumbo v2, "window_num"

    iget v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->val$freeformAppCount:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 263
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$3;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v2}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v2

    const-string v3, "31000000538"

    const-string v4, "android"

    .line 264
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    .line 263
    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    .end local v1    # "jsonData":Lorg/json/JSONObject;
    :cond_0
    goto :goto_0

    .line 269
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 266
    :catch_0
    move-exception v1

    .line 267
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 269
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v0

    .line 270
    return-void

    .line 269
    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
