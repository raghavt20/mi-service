class com.android.server.wm.AnimationDimmer extends android.view.animation.Animation {
	 /* .source "AnimationDimmer.java" */
	 /* # instance fields */
	 Float alpha;
	 Boolean isVisible;
	 private android.graphics.Rect mClipRect;
	 Float mCornerRadius;
	 android.view.SurfaceControl mDimLayer;
	 private Float mFromAlpha;
	 private com.android.server.wm.WindowContainer mHost;
	 private Float mToAlpha;
	 /* # direct methods */
	 public com.android.server.wm.AnimationDimmer ( ) {
		 /* .locals 0 */
		 /* .param p1, "fromAlpha" # F */
		 /* .param p2, "toAlpha" # F */
		 /* .param p3, "rect" # Landroid/graphics/Rect; */
		 /* .param p4, "host" # Lcom/android/server/wm/WindowContainer; */
		 /* .line 30 */
		 /* invoke-direct {p0, p1, p2, p4}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLcom/android/server/wm/WindowContainer;)V */
		 /* .line 31 */
		 this.mClipRect = p3;
		 /* .line 32 */
		 return;
	 } // .end method
	 public com.android.server.wm.AnimationDimmer ( ) {
		 /* .locals 0 */
		 /* .param p1, "fromAlpha" # F */
		 /* .param p2, "toAlpha" # F */
		 /* .param p3, "rect" # Landroid/graphics/Rect; */
		 /* .param p4, "host" # Lcom/android/server/wm/WindowContainer; */
		 /* .param p5, "cornerRadius" # F */
		 /* .line 25 */
		 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V */
		 /* .line 26 */
		 /* iput p5, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F */
		 /* .line 27 */
		 return;
	 } // .end method
	 public com.android.server.wm.AnimationDimmer ( ) {
		 /* .locals 1 */
		 /* .param p1, "fromAlpha" # F */
		 /* .param p2, "toAlpha" # F */
		 /* .param p3, "host" # Lcom/android/server/wm/WindowContainer; */
		 /* .line 34 */
		 /* invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V */
		 /* .line 11 */
		 /* const v0, 0x3dcccccd # 0.1f */
		 /* iput v0, p0, Lcom/android/server/wm/AnimationDimmer;->alpha:F */
		 /* .line 15 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F */
		 /* .line 35 */
		 /* iput p1, p0, Lcom/android/server/wm/AnimationDimmer;->mFromAlpha:F */
		 /* .line 36 */
		 /* iput p2, p0, Lcom/android/server/wm/AnimationDimmer;->mToAlpha:F */
		 /* .line 37 */
		 this.mHost = p3;
		 /* .line 38 */
		 return;
	 } // .end method
	 private void dim ( android.view.SurfaceControl$Transaction p0, com.android.server.wm.WindowContainer p1, Integer p2, Float p3 ) {
		 /* .locals 4 */
		 /* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
		 /* .param p2, "container" # Lcom/android/server/wm/WindowContainer; */
		 /* .param p3, "relativeLayer" # I */
		 /* .param p4, "alpha" # F */
		 /* .line 53 */
		 /* monitor-enter p0 */
		 /* .line 54 */
		 try { // :try_start_0
			 v0 = this.mDimLayer;
			 /* if-nez v0, :cond_0 */
			 /* .line 55 */
			 /* invoke-direct {p0}, Lcom/android/server/wm/AnimationDimmer;->makeDimLayer()Landroid/view/SurfaceControl; */
			 this.mDimLayer = v0;
			 /* .line 57 */
		 } // :cond_0
		 v0 = this.mDimLayer;
		 /* if-nez v0, :cond_1 */
		 /* .line 58 */
		 /* monitor-exit p0 */
		 return;
		 /* .line 61 */
	 } // :cond_1
	 if ( p2 != null) { // if-eqz p2, :cond_2
		 /* .line 65 */
		 (( com.android.server.wm.WindowContainer ) p2 ).getSurfaceControl ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->getSurfaceControl()Landroid/view/SurfaceControl;
		 (( android.view.SurfaceControl$Transaction ) p1 ).setRelativeLayer ( v0, v1, p3 ); // invoke-virtual {p1, v0, v1, p3}, Landroid/view/SurfaceControl$Transaction;->setRelativeLayer(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
		 /* .line 67 */
	 } // :cond_2
	 /* const v1, 0x7fffffff */
	 (( android.view.SurfaceControl$Transaction ) p1 ).setLayer ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
	 /* .line 69 */
} // :goto_0
/* iget v0, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F */
v0 = java.lang.Math .abs ( v0 );
/* float-to-double v0, v0 */
/* const-wide v2, 0x3eb0c6f7a0b5ed8dL # 1.0E-6 */
/* cmpl-double v0, v0, v2 */
/* if-lez v0, :cond_3 */
/* .line 70 */
v0 = this.mDimLayer;
/* iget v1, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F */
(( android.view.SurfaceControl$Transaction ) p1 ).setCornerRadius ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/view/SurfaceControl$Transaction;->setCornerRadius(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 73 */
} // :cond_3
v0 = this.mDimLayer;
(( android.view.SurfaceControl$Transaction ) p1 ).setAlpha ( v0, p4 ); // invoke-virtual {p1, v0, p4}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 74 */
/* iput p4, p0, Lcom/android/server/wm/AnimationDimmer;->alpha:F */
/* .line 75 */
v0 = this.mDimLayer;
(( android.view.SurfaceControl$Transaction ) p1 ).show ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 76 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 77 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/AnimationDimmer;->isVisible:Z */
/* .line 78 */
return;
/* .line 76 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit p0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
} // .end method
private android.view.SurfaceControl makeDimLayer ( ) {
/* .locals 3 */
/* .line 41 */
v0 = this.mHost;
/* instance-of v1, v0, Lcom/android/server/wm/ActivityRecord; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_1 */
/* instance-of v1, v0, Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 48 */
} // :cond_0
/* .line 42 */
} // :cond_1
} // :goto_0
(( com.android.server.wm.WindowContainer ) v0 ).makeChildSurface ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowContainer;->makeChildSurface(Lcom/android/server/wm/WindowContainer;)Landroid/view/SurfaceControl$Builder;
v1 = this.mHost;
/* .line 43 */
(( com.android.server.wm.WindowContainer ) v1 ).getSurfaceControl ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->getSurfaceControl()Landroid/view/SurfaceControl;
(( android.view.SurfaceControl$Builder ) v0 ).setParent ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setParent(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;
/* .line 44 */
(( android.view.SurfaceControl$Builder ) v0 ).setColorLayer ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->setColorLayer()Landroid/view/SurfaceControl$Builder;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Transition Dim Layer for - "; // const-string v2, "Transition Dim Layer for - "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mHost;
/* .line 45 */
(( com.android.server.wm.WindowContainer ) v2 ).getName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.view.SurfaceControl$Builder ) v0 ).setName ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 46 */
(( android.view.SurfaceControl$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
/* .line 42 */
} // .end method
/* # virtual methods */
protected void applyTransformation ( Float p0, android.view.animation.Transformation p1 ) {
/* .locals 2 */
/* .param p1, "interpolatedTime" # F */
/* .param p2, "t" # Landroid/view/animation/Transformation; */
/* .line 116 */
/* iget v0, p0, Lcom/android/server/wm/AnimationDimmer;->mFromAlpha:F */
/* .line 117 */
/* .local v0, "tmpAlpha":F */
/* iget v1, p0, Lcom/android/server/wm/AnimationDimmer;->mToAlpha:F */
/* sub-float/2addr v1, v0 */
/* mul-float/2addr v1, p1 */
/* add-float/2addr v1, v0 */
/* iput v1, p0, Lcom/android/server/wm/AnimationDimmer;->alpha:F */
/* .line 118 */
return;
} // .end method
void dimAbove ( android.view.SurfaceControl$Transaction p0, com.android.server.wm.WindowContainer p1 ) {
/* .locals 2 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "container" # Lcom/android/server/wm/WindowContainer; */
/* .line 95 */
int v0 = 1; // const/4 v0, 0x1
/* iget v1, p0, Lcom/android/server/wm/AnimationDimmer;->mFromAlpha:F */
/* invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/server/wm/AnimationDimmer;->dim(Landroid/view/SurfaceControl$Transaction;Lcom/android/server/wm/WindowContainer;IF)V */
/* .line 96 */
return;
} // .end method
void dimBelow ( android.view.SurfaceControl$Transaction p0, com.android.server.wm.WindowContainer p1 ) {
/* .locals 2 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "container" # Lcom/android/server/wm/WindowContainer; */
/* .line 125 */
int v0 = -1; // const/4 v0, -0x1
/* iget v1, p0, Lcom/android/server/wm/AnimationDimmer;->mFromAlpha:F */
/* invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/server/wm/AnimationDimmer;->dim(Landroid/view/SurfaceControl$Transaction;Lcom/android/server/wm/WindowContainer;IF)V */
/* .line 126 */
return;
} // .end method
public android.graphics.Rect getClipRect ( ) {
/* .locals 1 */
/* .line 133 */
v0 = this.mClipRect;
} // .end method
public android.view.SurfaceControl getDimmer ( ) {
/* .locals 1 */
/* .line 129 */
v0 = this.mDimLayer;
} // .end method
void setAlpha ( android.view.SurfaceControl$Transaction p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "alpha" # F */
/* .line 99 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AnimationDimmer;->isVisible:Z */
/* if-nez v0, :cond_0 */
/* .line 100 */
return;
/* .line 102 */
} // :cond_0
/* monitor-enter p0 */
/* .line 103 */
try { // :try_start_0
v0 = this.mDimLayer;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = (( android.view.SurfaceControl ) v0 ).isValid ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z
/* if-nez v0, :cond_1 */
/* .line 106 */
} // :cond_1
v0 = this.mDimLayer;
(( android.view.SurfaceControl$Transaction ) p1 ).setAlpha ( v0, p2 ); // invoke-virtual {p1, v0, p2}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 107 */
/* monitor-exit p0 */
/* .line 108 */
return;
/* .line 104 */
} // :cond_2
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 107 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
void stepTransitionDim ( android.view.SurfaceControl$Transaction p0 ) {
/* .locals 1 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .line 121 */
/* iget v0, p0, Lcom/android/server/wm/AnimationDimmer;->alpha:F */
(( com.android.server.wm.AnimationDimmer ) p0 ).setAlpha ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/AnimationDimmer;->setAlpha(Landroid/view/SurfaceControl$Transaction;F)V
/* .line 122 */
return;
} // .end method
void stopDim ( android.view.SurfaceControl$Transaction p0 ) {
/* .locals 2 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .line 81 */
/* monitor-enter p0 */
/* .line 82 */
try { // :try_start_0
v0 = this.mDimLayer;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( android.view.SurfaceControl ) v0 ).isValid ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 83 */
v0 = this.mDimLayer;
(( android.view.SurfaceControl$Transaction ) p1 ).hide ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 84 */
v0 = this.mDimLayer;
(( android.view.SurfaceControl$Transaction ) p1 ).remove ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 85 */
this.mDimLayer = v1;
/* .line 87 */
} // :cond_0
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 88 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/AnimationDimmer;->isVisible:Z */
/* .line 89 */
this.mClipRect = v1;
/* .line 90 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F */
/* .line 91 */
return;
/* .line 87 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit p0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
} // .end method
