.class final Lcom/android/server/wm/MiuiLoadingDialog;
.super Lmiuix/appcompat/app/ProgressDialog;
.source "MiuiLoadingDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiLoadingDialog"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$mcloseDialog(Lcom/android/server/wm/MiuiLoadingDialog;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiLoadingDialog;->closeDialog()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 27
    const v0, 0x66110006

    invoke-direct {p0, p1, v0}, Lcom/android/server/wm/MiuiLoadingDialog;-><init>(Landroid/content/Context;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .line 31
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiLoadingDialog;->mHandler:Landroid/os/Handler;

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiLoadingDialog;->setCancelable(Z)V

    .line 34
    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiLoadingDialog;->setProgressStyle(I)V

    .line 36
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiLoadingDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7d3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    .line 37
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiLoadingDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x20000

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 39
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiLoadingDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 41
    .local v0, "attrs":Landroid/view/WindowManager$LayoutParams;
    invoke-static {p1}, Landroid/util/MiuiMultiWindowUtils;->isPadScreen(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x11

    goto :goto_0

    :cond_0
    const/16 v1, 0x50

    :goto_0
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 42
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiLoadingDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 45
    const v2, 0x110f028b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiLoadingDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 47
    return-void
.end method

.method private closeDialog()V
    .locals 0

    .line 84
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiLoadingDialog;->dismiss()V

    .line 85
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 7

    .line 51
    invoke-super {p0}, Lmiuix/appcompat/app/ProgressDialog;->onStart()V

    .line 52
    iget-object v0, p0, Lcom/android/server/wm/MiuiLoadingDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/android/server/wm/MiuiLoadingDialog$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiLoadingDialog$1;-><init>(Lcom/android/server/wm/MiuiLoadingDialog;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiLoadingDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 61
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiLoadingDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiLoadingDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v0, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/server/wm/MiuiLoadingDialog;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x2

    invoke-virtual/range {v1 .. v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    .line 64
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 3

    .line 68
    invoke-super {p0}, Lmiuix/appcompat/app/ProgressDialog;->onStop()V

    .line 69
    iget-object v0, p0, Lcom/android/server/wm/MiuiLoadingDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 71
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiLoadingDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiLoadingDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterReceiver threw exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiLoadingDialog"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiLoadingDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 78
    :cond_0
    return-void
.end method
