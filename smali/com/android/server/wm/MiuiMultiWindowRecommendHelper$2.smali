.class Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;
.super Landroid/app/TaskStackListener;
.source "MiuiMultiWindowRecommendHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    .line 116
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-direct {p0}, Landroid/app/TaskStackListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTaskRemoved(I)V
    .locals 3
    .param p1, "taskId"    # I

    .line 119
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$misDeviceSupportSplitScreenRecommend(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$minSplitScreenRecommendState(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z

    move-result v0

    const-string v1, "MiuiMultiWindowRecommendHelper"

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fgetmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fgetmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryTaskId()I

    move-result v0

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fgetmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryTaskId()I

    move-result v0

    if-ne v0, p1, :cond_2

    .line 126
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onTaskRemoved: remove SplitScreenRecommendView, taskId= "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V

    .line 130
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$minFreeFormRecommendState(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fgetmFreeFormRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fgetmFreeFormRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getFreeFormTaskId()I

    move-result v0

    if-ne v0, p1, :cond_3

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onTaskRemoved: remove FreeFormRecommendView, taskId= "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V

    .line 135
    :cond_3
    return-void
.end method
