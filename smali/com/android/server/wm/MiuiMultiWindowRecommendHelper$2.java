class com.android.server.wm.MiuiMultiWindowRecommendHelper$2 extends android.app.TaskStackListener {
	 /* .source "MiuiMultiWindowRecommendHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendHelper this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendHelper$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
/* .line 116 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/TaskStackListener;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onTaskRemoved ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .line 119 */
v0 = this.this$0;
v0 = com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$misDeviceSupportSplitScreenRecommend ( v0 );
/* if-nez v0, :cond_0 */
/* .line 120 */
return;
/* .line 123 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$minSplitScreenRecommendState ( v0 );
final String v1 = "MiuiMultiWindowRecommendHelper"; // const-string v1, "MiuiMultiWindowRecommendHelper"
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fgetmSpiltScreenRecommendDataEntry ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 v0 = this.this$0;
	 com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fgetmSpiltScreenRecommendDataEntry ( v0 );
	 /* .line 124 */
	 v0 = 	 (( com.android.server.wm.RecommendDataEntry ) v0 ).getPrimaryTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryTaskId()I
	 /* if-eq v0, p1, :cond_1 */
	 v0 = this.this$0;
	 com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fgetmSpiltScreenRecommendDataEntry ( v0 );
	 /* .line 125 */
	 v0 = 	 (( com.android.server.wm.RecommendDataEntry ) v0 ).getSecondaryTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryTaskId()I
	 /* if-ne v0, p1, :cond_2 */
	 /* .line 126 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " onTaskRemoved: remove SplitScreenRecommendView, taskId= "; // const-string v2, " onTaskRemoved: remove SplitScreenRecommendView, taskId= "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 127 */
v0 = this.this$0;
v0 = this.mMiuiMultiWindowRecommendController;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).removeSplitScreenRecommendView ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V
/* .line 130 */
} // :cond_2
v0 = this.this$0;
v0 = com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$minFreeFormRecommendState ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fgetmFreeFormRecommendDataEntry ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
	 v0 = this.this$0;
	 com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$fgetmFreeFormRecommendDataEntry ( v0 );
	 /* .line 131 */
	 v0 = 	 (( com.android.server.wm.RecommendDataEntry ) v0 ).getFreeFormTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getFreeFormTaskId()I
	 /* if-ne v0, p1, :cond_3 */
	 /* .line 132 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = " onTaskRemoved: remove FreeFormRecommendView, taskId= "; // const-string v2, " onTaskRemoved: remove FreeFormRecommendView, taskId= "
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v0 );
	 /* .line 133 */
	 v0 = this.this$0;
	 v0 = this.mMiuiMultiWindowRecommendController;
	 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).removeFreeFormRecommendView ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V
	 /* .line 135 */
} // :cond_3
return;
} // .end method
