.class public final Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;
.super Ljava/lang/Object;
.source "TaskStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/TaskStub$MutableTaskStub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/TaskStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MutableTaskStubImpl"
.end annotation


# instance fields
.field private mIsCirculatedToVirtual:Z

.field private mIsSplitMode:Z

.field private mLastDisplayId:I

.field mTask:Lcom/android/server/wm/Task;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIsCirculatedToVirtual()Z
    .locals 1

    .line 76
    iget-boolean v0, p0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;->mIsCirculatedToVirtual:Z

    return v0
.end method

.method public getLastDisplayId()I
    .locals 1

    .line 66
    iget v0, p0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;->mLastDisplayId:I

    return v0
.end method

.method public init(Lcom/android/server/wm/Task;)V
    .locals 1
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 87
    iput-object p1, p0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;->mTask:Lcom/android/server/wm/Task;

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;->mIsSplitMode:Z

    .line 89
    return-void
.end method

.method public isSplitMode()Z
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;->mIsSplitMode:Z

    return v0
.end method

.method public setIsCirculatedToVirtual(Z)V
    .locals 0
    .param p1, "isCirculatedToVirtual"    # Z

    .line 81
    iput-boolean p1, p0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;->mIsCirculatedToVirtual:Z

    .line 82
    return-void
.end method

.method public setLastDisplayId(I)V
    .locals 0
    .param p1, "lastDisplayId"    # I

    .line 71
    iput p1, p0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;->mLastDisplayId:I

    .line 72
    return-void
.end method

.method public setSplitMode(Z)V
    .locals 0
    .param p1, "isSplitMode"    # Z

    .line 98
    iput-boolean p1, p0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;->mIsSplitMode:Z

    .line 99
    return-void
.end method
