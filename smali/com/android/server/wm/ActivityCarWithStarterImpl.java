public class com.android.server.wm.ActivityCarWithStarterImpl {
	 /* .source "ActivityCarWithStarterImpl.java" */
	 /* # static fields */
	 private static final java.lang.String MIUI_CARLINK_PERMISSION;
	 /* # instance fields */
	 private final java.lang.String TAG;
	 private java.lang.String mAddress;
	 private Double mAltitude;
	 private java.lang.String mCoordinateType;
	 private Double mLatitude;
	 private Double mLongitude;
	 /* # direct methods */
	 public static void $r8$lambda$LY34ctSSQxgjrTOp40N0BwryIgY ( com.android.server.wm.ActivityCarWithStarterImpl p0, android.content.Context p1, android.content.Intent p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->lambda$recordCarWithIntent$0(Landroid/content/Context;Landroid/content/Intent;)V */
		 return;
	 } // .end method
	 public com.android.server.wm.ActivityCarWithStarterImpl ( ) {
		 /* .locals 2 */
		 /* .line 26 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 19 */
		 final String v0 = "ActivityCarWithStarterImpl"; // const-string v0, "ActivityCarWithStarterImpl"
		 this.TAG = v0;
		 /* .line 20 */
		 /* const-wide/high16 v0, -0x4010000000000000L # -1.0 */
		 /* iput-wide v0, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D */
		 /* .line 21 */
		 /* iput-wide v0, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D */
		 /* .line 22 */
		 /* const-wide/16 v0, 0x0 */
		 /* iput-wide v0, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAltitude:D */
		 /* .line 23 */
		 final String v0 = "NA"; // const-string v0, "NA"
		 this.mAddress = v0;
		 /* .line 24 */
		 this.mCoordinateType = v0;
		 /* .line 27 */
		 return;
	 } // .end method
	 private void lambda$recordCarWithIntent$0 ( android.content.Context p0, android.content.Intent p1 ) { //synthethic
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "mCarWithIntent" # Landroid/content/Intent; */
		 /* .line 79 */
		 final String v0 = "ActivityCarWithStarterImpl"; // const-string v0, "ActivityCarWithStarterImpl"
		 final String v1 = "Send BroadCast To carlink"; // const-string v1, "Send BroadCast To carlink"
		 android.util.Slog .d ( v0,v1 );
		 /* .line 80 */
		 final String v0 = "miui.car.permission.MI_CARLINK_STATUS"; // const-string v0, "miui.car.permission.MI_CARLINK_STATUS"
		 (( android.content.Context ) p1 ).sendBroadcast ( p2, v0 ); // invoke-virtual {p1, p2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
		 /* .line 81 */
		 return;
	 } // .end method
	 private void recordBaiduCoorType ( java.lang.String p0 ) {
		 /* .locals 4 */
		 /* .param p1, "dat" # Ljava/lang/String; */
		 /* .line 138 */
		 final String v0 = "coord_type"; // const-string v0, "coord_type"
		 v0 = 		 (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 139 */
			 final String v0 = "coord_type=(\\w+)"; // const-string v0, "coord_type=(\\w+)"
			 java.util.regex.Pattern .compile ( v0 );
			 /* .line 140 */
			 /* .local v0, "pattern":Ljava/util/regex/Pattern; */
			 (( java.util.regex.Pattern ) v0 ).matcher ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
			 /* .line 141 */
			 /* .local v1, "matcher":Ljava/util/regex/Matcher; */
			 v2 = 			 (( java.util.regex.Matcher ) v1 ).find ( ); // invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
			 if ( v2 != null) { // if-eqz v2, :cond_0
				 /* .line 142 */
				 int v2 = 1; // const/4 v2, 0x1
				 (( java.util.regex.Matcher ) v1 ).group ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
				 this.mCoordinateType = v2;
				 /* .line 143 */
				 /* new-instance v2, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v3 = "mCoordinateType: "; // const-string v3, "mCoordinateType: "
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 v3 = this.mCoordinateType;
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 final String v3 = "ActivityCarWithStarterImpl"; // const-string v3, "ActivityCarWithStarterImpl"
				 android.util.Slog .d ( v3,v2 );
				 /* .line 146 */
			 } // .end local v0 # "pattern":Ljava/util/regex/Pattern;
		 } // .end local v1 # "matcher":Ljava/util/regex/Matcher;
	 } // :cond_0
	 return;
} // .end method
private void recordBaiduMap ( java.lang.String p0 ) {
	 /* .locals 8 */
	 /* .param p1, "dat" # Ljava/lang/String; */
	 /* .line 112 */
	 final String v0 = ""; // const-string v0, ""
	 /* .line 114 */
	 /* .local v0, "mUriRegex":Ljava/lang/String; */
	 final String v1 = "destination=latlng"; // const-string v1, "destination=latlng"
	 v1 = 	 (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 int v2 = 3; // const/4 v2, 0x3
	 int v3 = 2; // const/4 v3, 0x2
	 int v4 = 1; // const/4 v4, 0x1
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 115 */
		 final String v0 = "destination=latlng:([\\d\\.]+),([\\d\\.]+)\\|name:([^&]+)"; // const-string v0, "destination=latlng:([\\d\\.]+),([\\d\\.]+)\\|name:([^&]+)"
		 /* .line 116 */
		 java.util.regex.Pattern .compile ( v0 );
		 /* .line 117 */
		 /* .local v1, "pattern":Ljava/util/regex/Pattern; */
		 (( java.util.regex.Pattern ) v1 ).matcher ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
		 /* .line 118 */
		 /* .local v5, "matcher":Ljava/util/regex/Matcher; */
		 v6 = 		 (( java.util.regex.Matcher ) v5 ).find ( ); // invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z
		 if ( v6 != null) { // if-eqz v6, :cond_2
			 /* .line 119 */
			 (( java.util.regex.Matcher ) v5 ).group ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
			 java.lang.Double .valueOf ( v4 );
			 (( java.lang.Double ) v4 ).doubleValue ( ); // invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D
			 /* move-result-wide v6 */
			 /* iput-wide v6, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D */
			 /* .line 120 */
			 (( java.util.regex.Matcher ) v5 ).group ( v3 ); // invoke-virtual {v5, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
			 java.lang.Double .valueOf ( v3 );
			 (( java.lang.Double ) v3 ).doubleValue ( ); // invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D
			 /* move-result-wide v3 */
			 /* iput-wide v3, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D */
			 /* .line 121 */
			 (( java.util.regex.Matcher ) v5 ).group ( v2 ); // invoke-virtual {v5, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
			 this.mAddress = v2;
			 /* .line 123 */
		 } // .end local v1 # "pattern":Ljava/util/regex/Pattern;
	 } // .end local v5 # "matcher":Ljava/util/regex/Matcher;
} // :cond_0
final String v1 = "destination=name"; // const-string v1, "destination=name"
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 124 */
	 final String v0 = "destination=name:([^|]+)\\|latlng:([\\d\\.]+),([\\d\\.]+)"; // const-string v0, "destination=name:([^|]+)\\|latlng:([\\d\\.]+),([\\d\\.]+)"
	 /* .line 125 */
	 java.util.regex.Pattern .compile ( v0 );
	 /* .line 126 */
	 /* .restart local v1 # "pattern":Ljava/util/regex/Pattern; */
	 (( java.util.regex.Pattern ) v1 ).matcher ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
	 /* .line 127 */
	 /* .restart local v5 # "matcher":Ljava/util/regex/Matcher; */
	 v6 = 	 (( java.util.regex.Matcher ) v5 ).find ( ); // invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z
	 if ( v6 != null) { // if-eqz v6, :cond_2
		 /* .line 128 */
		 (( java.util.regex.Matcher ) v5 ).group ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
		 this.mAddress = v4;
		 /* .line 129 */
		 (( java.util.regex.Matcher ) v5 ).group ( v3 ); // invoke-virtual {v5, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
		 java.lang.Double .valueOf ( v3 );
		 (( java.lang.Double ) v3 ).doubleValue ( ); // invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D
		 /* move-result-wide v3 */
		 /* iput-wide v3, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D */
		 /* .line 130 */
		 (( java.util.regex.Matcher ) v5 ).group ( v2 ); // invoke-virtual {v5, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
		 java.lang.Double .valueOf ( v2 );
		 (( java.lang.Double ) v2 ).doubleValue ( ); // invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
		 /* move-result-wide v2 */
		 /* iput-wide v2, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D */
		 /* .line 133 */
	 } // .end local v1 # "pattern":Ljava/util/regex/Pattern;
} // .end local v5 # "matcher":Ljava/util/regex/Matcher;
} // :cond_1
/* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordDefaultMap(Ljava/lang/String;)V */
/* .line 135 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void recordDefaultMap ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p1, "dat" # Ljava/lang/String; */
/* .line 169 */
int v0 = 0; // const/4 v0, 0x0
/* .line 170 */
/* .local v0, "latIndex":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 171 */
/* .local v1, "lonIndex":I */
final String v2 = "^-?((0|[1-9]\\d?|1[0-7]\\d)(\\.\\d+)?|180(\\.0+)?)$"; // const-string v2, "^-?((0|[1-9]\\d?|1[0-7]\\d)(\\.\\d+)?|180(\\.0+)?)$"
/* .line 172 */
/* .local v2, "regexLongitude":Ljava/lang/String; */
final String v3 = "^-?([1-8]\\d?|([1-9]0))(\\.\\d+)?|90(\\.0+)?$"; // const-string v3, "^-?([1-8]\\d?|([1-9]0))(\\.\\d+)?|90(\\.0+)?$"
/* .line 173 */
/* .local v3, "regexLatitude":Ljava/lang/String; */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 174 */
/* .local v4, "latitudes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;" */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* .line 175 */
/* .local v5, "longitudes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;" */
final String v6 = "(\\d+\\.\\d+)"; // const-string v6, "(\\d+\\.\\d+)"
java.util.regex.Pattern .compile ( v6 );
/* .line 176 */
/* .local v6, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v6 ).matcher ( p1 ); // invoke-virtual {v6, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 177 */
/* .local v7, "matcher":Ljava/util/regex/Matcher; */
} // :goto_0
v8 = (( java.util.regex.Matcher ) v7 ).find ( ); // invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 178 */
(( java.util.regex.Matcher ) v7 ).group ( ); // invoke-virtual {v7}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;
/* .line 179 */
/* .local v8, "value":Ljava/lang/String; */
v9 = (( java.lang.String ) v8 ).matches ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 180 */
java.lang.Double .valueOf ( v8 );
/* .line 181 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 182 */
} // :cond_0
v9 = (( java.lang.String ) v8 ).matches ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_1
/* .line 183 */
java.lang.Double .valueOf ( v8 );
/* .line 184 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 186 */
} // .end local v8 # "value":Ljava/lang/String;
} // :cond_1
} // :goto_1
/* .line 187 */
} // :cond_2
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-ne v0, v1, :cond_3 */
/* .line 188 */
/* add-int/lit8 v0, v0, -0x1 */
/* check-cast v8, Ljava/lang/Double; */
(( java.lang.Double ) v8 ).doubleValue ( ); // invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v8 */
/* iput-wide v8, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D */
/* .line 189 */
/* add-int/lit8 v1, v1, -0x1 */
/* check-cast v8, Ljava/lang/Double; */
(( java.lang.Double ) v8 ).doubleValue ( ); // invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v8 */
/* iput-wide v8, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D */
/* .line 190 */
final String v8 = "NA"; // const-string v8, "NA"
this.mAddress = v8;
/* .line 192 */
} // :cond_3
return;
} // .end method
private void recordGaoDeMap ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "dat" # Ljava/lang/String; */
/* .line 89 */
final String v0 = "dlat"; // const-string v0, "dlat"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 90 */
final String v0 = "dlat=(\\d+\\.\\d+)&dlon=(\\d+\\.\\d+)"; // const-string v0, "dlat=(\\d+\\.\\d+)&dlon=(\\d+\\.\\d+)"
java.util.regex.Pattern .compile ( v0 );
/* .line 91 */
/* .local v0, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v0 ).matcher ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 92 */
/* .local v1, "matcher":Ljava/util/regex/Matcher; */
v2 = (( java.util.regex.Matcher ) v1 ).find ( ); // invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 93 */
(( java.util.regex.Matcher ) v1 ).group ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
java.lang.Double .valueOf ( v2 );
(( java.lang.Double ) v2 ).doubleValue ( ); // invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D */
/* .line 94 */
int v2 = 2; // const/4 v2, 0x2
(( java.util.regex.Matcher ) v1 ).group ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
java.lang.Double .valueOf ( v2 );
(( java.lang.Double ) v2 ).doubleValue ( ); // invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D */
/* .line 96 */
} // :cond_0
final String v2 = "dname=([^&]+)"; // const-string v2, "dname=([^&]+)"
java.util.regex.Pattern .compile ( v2 );
/* .line 97 */
(( java.util.regex.Pattern ) v0 ).matcher ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 98 */
v2 = (( java.util.regex.Matcher ) v1 ).find ( ); // invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 99 */
(( java.util.regex.Matcher ) v1 ).group ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
this.mAddress = v2;
/* .line 100 */
} // .end local v0 # "pattern":Ljava/util/regex/Pattern;
} // .end local v1 # "matcher":Ljava/util/regex/Matcher;
} // :cond_1
} // :cond_2
final String v0 = "denstination"; // const-string v0, "denstination"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 101 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordBaiduMap(Ljava/lang/String;)V */
/* .line 102 */
} // :cond_3
/* const-string/jumbo v0, "tocoord" */
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 103 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordTencentMap(Ljava/lang/String;)V */
/* .line 105 */
} // :cond_4
/* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordDefaultMap(Ljava/lang/String;)V */
/* .line 107 */
} // :goto_0
return;
} // .end method
private void recordTencentMap ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "dat" # Ljava/lang/String; */
/* .line 150 */
/* const-string/jumbo v0, "tocoord" */
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 151 */
/* const-string/jumbo v0, "tocoord=(\\d+\\.\\d+),(\\d+\\.\\d+)" */
java.util.regex.Pattern .compile ( v0 );
/* .line 152 */
/* .local v0, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v0 ).matcher ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 153 */
/* .local v1, "matcher":Ljava/util/regex/Matcher; */
v2 = (( java.util.regex.Matcher ) v1 ).find ( ); // invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 154 */
(( java.util.regex.Matcher ) v1 ).group ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
java.lang.Double .valueOf ( v2 );
(( java.lang.Double ) v2 ).doubleValue ( ); // invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D */
/* .line 155 */
int v2 = 2; // const/4 v2, 0x2
(( java.util.regex.Matcher ) v1 ).group ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
java.lang.Double .valueOf ( v2 );
(( java.lang.Double ) v2 ).doubleValue ( ); // invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D */
/* .line 157 */
} // :cond_0
/* const-string/jumbo v2, "to=([^&]+)" */
java.util.regex.Pattern .compile ( v2 );
/* .line 158 */
(( java.util.regex.Pattern ) v0 ).matcher ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 159 */
v2 = (( java.util.regex.Matcher ) v1 ).find ( ); // invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 160 */
(( java.util.regex.Matcher ) v1 ).group ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
this.mAddress = v2;
/* .line 162 */
} // .end local v0 # "pattern":Ljava/util/regex/Pattern;
} // .end local v1 # "matcher":Ljava/util/regex/Matcher;
} // :cond_1
/* .line 163 */
} // :cond_2
/* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordDefaultMap(Ljava/lang/String;)V */
/* .line 165 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void recordCarWithIntent ( android.content.Context p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 19 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .param p4, "dat" # Ljava/lang/String; */
/* .line 32 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p3 */
/* move-object/from16 v3, p4 */
final String v4 = "com.baidu.BaiduMap"; // const-string v4, "com.baidu.BaiduMap"
/* .line 33 */
/* .local v4, "BAIDUMAP":Ljava/lang/String; */
final String v5 = "com.autonavi.minimap"; // const-string v5, "com.autonavi.minimap"
/* .line 34 */
/* .local v5, "GAODEMAP":Ljava/lang/String; */
final String v6 = "com.tencent.map"; // const-string v6, "com.tencent.map"
/* .line 36 */
/* .local v6, "TENCENTMAP":Ljava/lang/String; */
v0 = /* invoke-virtual/range {p3 ..p3}, Ljava/lang/String;->hashCode()I */
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v0 = "com.autonavi.minimap"; // const-string v0, "com.autonavi.minimap"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_1 */
final String v0 = "com.baidu.BaiduMap"; // const-string v0, "com.baidu.BaiduMap"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_2 */
final String v0 = "com.tencent.map"; // const-string v0, "com.tencent.map"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 48 */
/* invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordDefaultMap(Ljava/lang/String;)V */
/* .line 45 */
/* :pswitch_0 */
/* invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordTencentMap(Ljava/lang/String;)V */
/* .line 46 */
/* .line 41 */
/* :pswitch_1 */
/* invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordBaiduCoorType(Ljava/lang/String;)V */
/* .line 42 */
/* invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordBaiduMap(Ljava/lang/String;)V */
/* .line 43 */
/* .line 38 */
/* :pswitch_2 */
/* invoke-direct {v1, v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordGaoDeMap(Ljava/lang/String;)V */
/* .line 39 */
/* nop */
/* .line 51 */
} // :goto_2
/* iget-wide v7, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D */
/* const-wide/high16 v9, -0x4010000000000000L # -1.0 */
/* cmpl-double v0, v7, v9 */
/* if-nez v0, :cond_1 */
/* iget-wide v9, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D */
/* cmpl-double v0, v7, v9 */
/* if-nez v0, :cond_1 */
final String v0 = "NA"; // const-string v0, "NA"
v0 = android.text.TextUtils .equals ( v3,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 52 */
return;
/* .line 55 */
} // :cond_1
final String v7 = "com.miui.carlink.map.CarMapReceiver"; // const-string v7, "com.miui.carlink.map.CarMapReceiver"
/* .line 56 */
/* .local v7, "POIADDRESS_ACTION_NAME":Ljava/lang/String; */
final String v8 = "com.miui.carlink"; // const-string v8, "com.miui.carlink"
/* .line 57 */
/* .local v8, "POIADDRESS_PACKAGE_NAME":Ljava/lang/String; */
final String v9 = "poiaddress_poidetailInfo_type"; // const-string v9, "poiaddress_poidetailInfo_type"
/* .line 58 */
/* .local v9, "POIADDRESS_POIFETAILINFO_TYPE":Ljava/lang/String; */
final String v10 = "poiaddress_poidetailInfo_latitude"; // const-string v10, "poiaddress_poidetailInfo_latitude"
/* .line 59 */
/* .local v10, "POIADDRESS_POIFETAILINFO_LATITUDE":Ljava/lang/String; */
final String v11 = "poiaddress_poidetailInfo_longitude"; // const-string v11, "poiaddress_poidetailInfo_longitude"
/* .line 60 */
/* .local v11, "POIADDRESS_POIFETAILINFO_LONGITUDE":Ljava/lang/String; */
final String v12 = "poiaddress_poidetailInfo_altitude"; // const-string v12, "poiaddress_poidetailInfo_altitude"
/* .line 61 */
/* .local v12, "POIADDRESS_POIFETAILINFO_ALTITUDE":Ljava/lang/String; */
final String v13 = "poiaddress_address"; // const-string v13, "poiaddress_address"
/* .line 62 */
/* .local v13, "POIADDRESS_ADDRESS":Ljava/lang/String; */
final String v14 = "poiaddress_src_package_name"; // const-string v14, "poiaddress_src_package_name"
/* .line 63 */
/* .local v14, "POIADDRESS_SRC_PACKAGE_NAME":Ljava/lang/String; */
final String v15 = "poiaddress_dest_package_name"; // const-string v15, "poiaddress_dest_package_name"
/* .line 66 */
/* .local v15, "POIADDRESS_DEST_PACKAGE_NAME":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 67 */
/* .local v0, "mCarWithIntent":Landroid/content/Intent; */
/* new-instance v16, Landroid/os/Bundle; */
/* invoke-direct/range {v16 ..v16}, Landroid/os/Bundle;-><init>()V */
/* move-object/from16 v17, v16 */
/* .line 68 */
/* .local v17, "mCarWithBundle":Landroid/os/Bundle; */
final String v3 = "com.miui.carlink.map.CarMapReceiver"; // const-string v3, "com.miui.carlink.map.CarMapReceiver"
(( android.content.Intent ) v0 ).setAction ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 69 */
final String v3 = "poiaddress_poidetailInfo_type"; // const-string v3, "poiaddress_poidetailInfo_type"
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_5 */
/* move-object/from16 v16, v4 */
} // .end local v4 # "BAIDUMAP":Ljava/lang/String;
/* .local v16, "BAIDUMAP":Ljava/lang/String; */
try { // :try_start_1
v4 = this.mCoordinateType;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_4 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v5, v17 */
} // .end local v17 # "mCarWithBundle":Landroid/os/Bundle;
/* .local v5, "mCarWithBundle":Landroid/os/Bundle; */
/* .local v18, "GAODEMAP":Ljava/lang/String; */
try { // :try_start_2
(( android.os.Bundle ) v5 ).putString ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 70 */
final String v3 = "poiaddress_poidetailInfo_latitude"; // const-string v3, "poiaddress_poidetailInfo_latitude"
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_3 */
/* move-object v4, v6 */
/* move-object/from16 v17, v7 */
} // .end local v6 # "TENCENTMAP":Ljava/lang/String;
} // .end local v7 # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
/* .local v4, "TENCENTMAP":Ljava/lang/String; */
/* .local v17, "POIADDRESS_ACTION_NAME":Ljava/lang/String; */
try { // :try_start_3
/* iget-wide v6, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLatitude:D */
(( android.os.Bundle ) v5 ).putDouble ( v3, v6, v7 ); // invoke-virtual {v5, v3, v6, v7}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V
/* .line 71 */
final String v3 = "poiaddress_poidetailInfo_longitude"; // const-string v3, "poiaddress_poidetailInfo_longitude"
/* iget-wide v6, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mLongitude:D */
(( android.os.Bundle ) v5 ).putDouble ( v3, v6, v7 ); // invoke-virtual {v5, v3, v6, v7}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V
/* .line 72 */
final String v3 = "poiaddress_poidetailInfo_altitude"; // const-string v3, "poiaddress_poidetailInfo_altitude"
/* iget-wide v6, v1, Lcom/android/server/wm/ActivityCarWithStarterImpl;->mAltitude:D */
(( android.os.Bundle ) v5 ).putDouble ( v3, v6, v7 ); // invoke-virtual {v5, v3, v6, v7}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V
/* .line 73 */
final String v3 = "poiaddress_address"; // const-string v3, "poiaddress_address"
v6 = this.mAddress;
(( android.os.Bundle ) v5 ).putString ( v3, v6 ); // invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 74 */
final String v3 = "poiaddress_src_package_name"; // const-string v3, "poiaddress_src_package_name"
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_2 */
/* move-object/from16 v6, p2 */
try { // :try_start_4
(( android.os.Bundle ) v5 ).putString ( v3, v6 ); // invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 75 */
final String v3 = "poiaddress_dest_package_name"; // const-string v3, "poiaddress_dest_package_name"
(( android.os.Bundle ) v5 ).putString ( v3, v2 ); // invoke-virtual {v5, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 76 */
(( android.content.Intent ) v0 ).putExtras ( v5 ); // invoke-virtual {v0, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 77 */
final String v3 = "com.miui.carlink"; // const-string v3, "com.miui.carlink"
(( android.content.Intent ) v0 ).setPackage ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 78 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v7, Lcom/android/server/wm/ActivityCarWithStarterImpl$$ExternalSyntheticLambda0; */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_1 */
/* move-object/from16 v2, p1 */
try { // :try_start_5
/* invoke-direct {v7, v1, v2, v0}, Lcom/android/server/wm/ActivityCarWithStarterImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityCarWithStarterImpl;Landroid/content/Context;Landroid/content/Intent;)V */
(( android.os.Handler ) v3 ).post ( v7 ); // invoke-virtual {v3, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 84 */
/* nop */
} // .end local v0 # "mCarWithIntent":Landroid/content/Intent;
} // .end local v5 # "mCarWithBundle":Landroid/os/Bundle;
/* .line 82 */
/* :catch_0 */
/* move-exception v0 */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v2, p1 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v6, p2 */
} // .end local v4 # "TENCENTMAP":Ljava/lang/String;
} // .end local v17 # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
/* .restart local v6 # "TENCENTMAP":Ljava/lang/String; */
/* .restart local v7 # "POIADDRESS_ACTION_NAME":Ljava/lang/String; */
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v2, p1 */
/* move-object v4, v6 */
/* move-object/from16 v17, v7 */
/* move-object/from16 v6, p2 */
} // .end local v6 # "TENCENTMAP":Ljava/lang/String;
} // .end local v7 # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
/* .restart local v4 # "TENCENTMAP":Ljava/lang/String; */
/* .restart local v17 # "POIADDRESS_ACTION_NAME":Ljava/lang/String; */
} // .end local v4 # "TENCENTMAP":Ljava/lang/String;
} // .end local v17 # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
} // .end local v18 # "GAODEMAP":Ljava/lang/String;
/* .local v5, "GAODEMAP":Ljava/lang/String; */
/* .restart local v6 # "TENCENTMAP":Ljava/lang/String; */
/* .restart local v7 # "POIADDRESS_ACTION_NAME":Ljava/lang/String; */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v18, v5 */
/* move-object v4, v6 */
/* move-object/from16 v17, v7 */
/* move-object/from16 v6, p2 */
} // .end local v5 # "GAODEMAP":Ljava/lang/String;
} // .end local v6 # "TENCENTMAP":Ljava/lang/String;
} // .end local v7 # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
/* .restart local v4 # "TENCENTMAP":Ljava/lang/String; */
/* .restart local v17 # "POIADDRESS_ACTION_NAME":Ljava/lang/String; */
/* .restart local v18 # "GAODEMAP":Ljava/lang/String; */
} // .end local v16 # "BAIDUMAP":Ljava/lang/String;
} // .end local v17 # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
} // .end local v18 # "GAODEMAP":Ljava/lang/String;
/* .local v4, "BAIDUMAP":Ljava/lang/String; */
/* .restart local v5 # "GAODEMAP":Ljava/lang/String; */
/* .restart local v6 # "TENCENTMAP":Ljava/lang/String; */
/* .restart local v7 # "POIADDRESS_ACTION_NAME":Ljava/lang/String; */
/* :catch_5 */
/* move-exception v0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* move-object v4, v6 */
/* move-object/from16 v17, v7 */
/* move-object/from16 v6, p2 */
/* .line 83 */
} // .end local v5 # "GAODEMAP":Ljava/lang/String;
} // .end local v6 # "TENCENTMAP":Ljava/lang/String;
} // .end local v7 # "POIADDRESS_ACTION_NAME":Ljava/lang/String;
/* .local v0, "e":Ljava/lang/Exception; */
/* .local v4, "TENCENTMAP":Ljava/lang/String; */
/* .restart local v16 # "BAIDUMAP":Ljava/lang/String; */
/* .restart local v17 # "POIADDRESS_ACTION_NAME":Ljava/lang/String; */
/* .restart local v18 # "GAODEMAP":Ljava/lang/String; */
} // :goto_3
final String v3 = "ActivityCarWithStarterImpl"; // const-string v3, "ActivityCarWithStarterImpl"
final String v5 = "Send BroadCast To CarWithFailed"; // const-string v5, "Send BroadCast To CarWithFailed"
android.util.Slog .e ( v3,v5 );
/* .line 85 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x62ba9ba -> :sswitch_2 */
/* 0x2c649fe1 -> :sswitch_1 */
/* 0x4ac75759 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
