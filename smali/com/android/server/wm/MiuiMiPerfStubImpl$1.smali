.class Lcom/android/server/wm/MiuiMiPerfStubImpl$1;
.super Ljava/util/TimerTask;
.source "MiuiMiPerfStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiMiPerfStubImpl;->onAfterActivityResumed(Lcom/android/server/wm/ActivityRecord;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMiPerfStubImpl;

.field final synthetic val$activityName:Ljava/lang/String;

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$pid:I


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMiPerfStubImpl;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMiPerfStubImpl;

    .line 169
    iput-object p1, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->this$0:Lcom/android/server/wm/MiuiMiPerfStubImpl;

    iput p2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->val$pid:I

    iput-object p3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->val$packageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->val$activityName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 173
    const-string v0, "MiuiMiPerfStubImpl"

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 174
    .local v1, "startMiperfTime":J
    iget-object v3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->this$0:Lcom/android/server/wm/MiuiMiPerfStubImpl;

    iget v4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->val$pid:I

    iget-object v5, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->val$packageName:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;->val$activityName:Ljava/lang/String;

    const-string v7, "onAfterActivityResumed"

    invoke-static {v3, v4, v5, v6, v7}, Lcom/android/server/wm/MiuiMiPerfStubImpl;->-$$Nest$mmiPerfSystemBoostNotify(Lcom/android/server/wm/MiuiMiPerfStubImpl;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v1

    .line 176
    .local v3, "durationMiperf":J
    const-wide/16 v5, 0x32

    cmp-long v5, v3, v5

    if-lez v5, :cond_0

    .line 177
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Call miPerfSystemBoostNotify is timeout, took "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    .end local v1    # "startMiperfTime":J
    .end local v3    # "durationMiperf":J
    :cond_0
    goto :goto_0

    .line 179
    :catch_0
    move-exception v1

    .line 180
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "miPerfSystemBoostNotify, IOException e:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
