public class com.android.server.wm.WindowStateStubImpl extends com.android.server.wm.WindowStateStub {
	 /* .source "WindowStateStubImpl.java" */
	 /* # static fields */
	 private static final Float DEFAULT_COMPAT_SCALE_VALUE;
	 private static Float FLIP_COMPAT_SCALE_SCALE_VALUE;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean mIsWallpaperOffsetUpdateCompleted;
	 /* # direct methods */
	 static com.android.server.wm.WindowStateStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 47 */
		 /* const-class v0, Lcom/android/server/wm/WindowStateStubImpl; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 /* .line 54 */
		 /* const v0, 0x3f4ccccd # 0.8f */
		 return;
	 } // .end method
	 public com.android.server.wm.WindowStateStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 44 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/WindowStateStub;-><init>()V */
		 /* .line 52 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* iput-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z */
		 return;
	 } // .end method
	 public static void adjuestFrameForChild ( com.android.server.wm.WindowState p0 ) {
		 /* .locals 12 */
		 /* .param p0, "win" # Lcom/android/server/wm/WindowState; */
		 /* .line 98 */
		 v0 = 		 (( com.android.server.wm.WindowState ) p0 ).isChildWindow ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isChildWindow()Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 99 */
			 return;
			 /* .line 102 */
		 } // :cond_0
		 (( com.android.server.wm.WindowState ) p0 ).getTask ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
		 /* .line 103 */
		 /* .local v0, "task":Lcom/android/server/wm/Task; */
		 if ( v0 != null) { // if-eqz v0, :cond_4
			 /* .line 105 */
			 (( com.android.server.wm.Task ) v0 ).getBounds ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
			 /* .line 106 */
			 /* .local v1, "rect":Landroid/graphics/Rect; */
			 /* iget v2, v1, Landroid/graphics/Rect;->left:I */
			 v3 = this.mWindowFrames;
			 v3 = this.mFrame;
			 /* iget v3, v3, Landroid/graphics/Rect;->left:I */
			 /* const/high16 v4, 0x40000000 # 2.0f */
			 /* const/high16 v5, 0x3f800000 # 1.0f */
			 /* if-ge v2, v3, :cond_2 */
			 /* .line 107 */
			 v2 = this.mWindowFrames;
			 v2 = this.mFrame;
			 /* iget v2, v2, Landroid/graphics/Rect;->left:I */
			 /* iget v3, v1, Landroid/graphics/Rect;->left:I */
			 /* sub-int/2addr v2, v3 */
			 /* .line 108 */
			 /* .local v2, "relativeLeft":I */
			 /* int-to-float v3, v2 */
			 /* iget v6, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
			 /* mul-float/2addr v3, v6 */
			 /* float-to-int v3, v3 */
			 /* sub-int v3, v2, v3 */
			 /* .line 110 */
			 /* .local v3, "weightOffset":I */
			 v6 = 			 (( com.android.server.wm.WindowState ) p0 ).inFreeformWindowingMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->inFreeformWindowingMode()Z
			 if ( v6 != null) { // if-eqz v6, :cond_1
				 /* iget v6, p0, Lcom/android/server/wm/WindowState;->mDownscaleScale:F */
				 /* cmpl-float v6, v6, v5 */
				 /* if-lez v6, :cond_1 */
				 /* .line 111 */
				 (( com.android.server.wm.WindowState ) p0 ).getWindowConfiguration ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;
				 /* .line 112 */
				 /* .local v6, "windowConfiguration":Landroid/app/WindowConfiguration; */
				 if ( v6 != null) { // if-eqz v6, :cond_1
					 (( android.app.WindowConfiguration ) v6 ).getBounds ( ); // invoke-virtual {v6}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
					 if ( v7 != null) { // if-eqz v7, :cond_1
						 /* .line 113 */
						 (( android.app.WindowConfiguration ) v6 ).getBounds ( ); // invoke-virtual {v6}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
						 /* .line 115 */
						 /* .local v7, "bounds":Landroid/graphics/Rect; */
						 /* iget v8, v7, Landroid/graphics/Rect;->left:I */
						 /* int-to-float v8, v8 */
						 /* .line 116 */
						 v9 = 						 (( android.graphics.Rect ) v7 ).width ( ); // invoke-virtual {v7}, Landroid/graphics/Rect;->width()I
						 /* int-to-float v9, v9 */
						 /* iget v10, p0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I */
						 /* int-to-float v10, v10 */
						 /* iget v11, p0, Lcom/android/server/wm/WindowState;->mDownscaleScale:F */
						 /* mul-float/2addr v10, v11 */
						 /* sub-float/2addr v9, v10 */
						 /* iget v10, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
						 /* mul-float/2addr v9, v10 */
						 /* div-float/2addr v9, v4 */
						 /* add-float/2addr v8, v9 */
						 /* float-to-int v8, v8 */
						 /* .line 117 */
						 /* .local v8, "targetLeft":I */
						 v9 = this.mWindowFrames;
						 v9 = this.mFrame;
						 /* iget v9, v9, Landroid/graphics/Rect;->left:I */
						 /* sub-int v3, v9, v8 */
						 /* .line 121 */
					 } // .end local v6 # "windowConfiguration":Landroid/app/WindowConfiguration;
				 } // .end local v7 # "bounds":Landroid/graphics/Rect;
			 } // .end local v8 # "targetLeft":I
		 } // :cond_1
		 v6 = this.mWindowFrames;
		 v6 = this.mFrame;
		 /* iget v7, v6, Landroid/graphics/Rect;->left:I */
		 /* sub-int/2addr v7, v3 */
		 /* iput v7, v6, Landroid/graphics/Rect;->left:I */
		 /* .line 122 */
		 v6 = this.mWindowFrames;
		 v6 = this.mFrame;
		 /* iget v7, v6, Landroid/graphics/Rect;->right:I */
		 /* sub-int/2addr v7, v3 */
		 /* iput v7, v6, Landroid/graphics/Rect;->right:I */
		 /* .line 125 */
	 } // .end local v2 # "relativeLeft":I
} // .end local v3 # "weightOffset":I
} // :cond_2
/* iget v2, v1, Landroid/graphics/Rect;->top:I */
v3 = this.mWindowFrames;
v3 = this.mFrame;
/* iget v3, v3, Landroid/graphics/Rect;->top:I */
/* if-ge v2, v3, :cond_4 */
/* .line 126 */
v2 = this.mWindowFrames;
v2 = this.mFrame;
/* iget v2, v2, Landroid/graphics/Rect;->top:I */
/* iget v3, v1, Landroid/graphics/Rect;->top:I */
/* sub-int/2addr v2, v3 */
/* .line 127 */
/* .local v2, "relativeTop":I */
/* int-to-float v3, v2 */
/* iget v6, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* mul-float/2addr v3, v6 */
/* float-to-int v3, v3 */
/* sub-int v3, v2, v3 */
/* .line 129 */
/* .local v3, "heightOffset":I */
v6 = (( com.android.server.wm.WindowState ) p0 ).inFreeformWindowingMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->inFreeformWindowingMode()Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* iget v6, p0, Lcom/android/server/wm/WindowState;->mDownscaleScale:F */
/* cmpl-float v5, v6, v5 */
/* if-lez v5, :cond_3 */
/* .line 130 */
(( com.android.server.wm.WindowState ) p0 ).getWindowConfiguration ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;
/* .line 131 */
/* .local v5, "windowConfiguration":Landroid/app/WindowConfiguration; */
if ( v5 != null) { // if-eqz v5, :cond_3
	 (( android.app.WindowConfiguration ) v5 ).getBounds ( ); // invoke-virtual {v5}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
	 if ( v6 != null) { // if-eqz v6, :cond_3
		 /* .line 132 */
		 (( android.app.WindowConfiguration ) v5 ).getBounds ( ); // invoke-virtual {v5}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
		 /* .line 134 */
		 /* .local v6, "bounds":Landroid/graphics/Rect; */
		 /* iget v7, v6, Landroid/graphics/Rect;->top:I */
		 /* int-to-float v7, v7 */
		 /* .line 135 */
		 v8 = 		 (( android.graphics.Rect ) v6 ).height ( ); // invoke-virtual {v6}, Landroid/graphics/Rect;->height()I
		 /* int-to-float v8, v8 */
		 /* iget v9, p0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I */
		 /* int-to-float v9, v9 */
		 /* iget v10, p0, Lcom/android/server/wm/WindowState;->mDownscaleScale:F */
		 /* mul-float/2addr v9, v10 */
		 /* sub-float/2addr v8, v9 */
		 /* iget v9, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
		 /* mul-float/2addr v8, v9 */
		 /* div-float/2addr v8, v4 */
		 /* add-float/2addr v7, v8 */
		 /* float-to-int v4, v7 */
		 /* .line 136 */
		 /* .local v4, "targetTop":I */
		 v7 = this.mWindowFrames;
		 v7 = this.mFrame;
		 /* iget v7, v7, Landroid/graphics/Rect;->top:I */
		 /* sub-int v3, v7, v4 */
		 /* .line 140 */
	 } // .end local v4 # "targetTop":I
} // .end local v5 # "windowConfiguration":Landroid/app/WindowConfiguration;
} // .end local v6 # "bounds":Landroid/graphics/Rect;
} // :cond_3
v4 = this.mWindowFrames;
v4 = this.mFrame;
/* iget v5, v4, Landroid/graphics/Rect;->top:I */
/* sub-int/2addr v5, v3 */
/* iput v5, v4, Landroid/graphics/Rect;->top:I */
/* .line 141 */
v4 = this.mWindowFrames;
v4 = this.mFrame;
/* iget v5, v4, Landroid/graphics/Rect;->bottom:I */
/* sub-int/2addr v5, v3 */
/* iput v5, v4, Landroid/graphics/Rect;->bottom:I */
/* .line 144 */
} // .end local v1 # "rect":Landroid/graphics/Rect;
} // .end local v2 # "relativeTop":I
} // .end local v3 # "heightOffset":I
} // :cond_4
return;
} // .end method
public static void adjuestFreeFormTouchRegion ( com.android.server.wm.WindowState p0, android.graphics.Region p1 ) {
/* .locals 0 */
/* .param p0, "win" # Lcom/android/server/wm/WindowState; */
/* .param p1, "outRegion" # Landroid/graphics/Region; */
/* .line 147 */
return;
} // .end method
public static void adjuestScaleAndFrame ( com.android.server.wm.WindowState p0, com.android.server.wm.Task p1 ) {
/* .locals 0 */
/* .param p0, "win" # Lcom/android/server/wm/WindowState; */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 74 */
return;
} // .end method
private Integer getNavBarHeight ( com.android.server.wm.WindowState p0 ) {
/* .locals 4 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 206 */
(( com.android.server.wm.WindowState ) p1 ).getInsetsState ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getInsetsState()Landroid/view/InsetsState;
/* .line 207 */
(( com.android.server.wm.WindowState ) p1 ).getInsetsState ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getInsetsState()Landroid/view/InsetsState;
(( android.view.InsetsState ) v1 ).getDisplayFrame ( ); // invoke-virtual {v1}, Landroid/view/InsetsState;->getDisplayFrame()Landroid/graphics/Rect;
/* .line 208 */
v2 = android.view.WindowInsets$Type .navigationBars ( );
/* .line 207 */
int v3 = 1; // const/4 v3, 0x1
(( android.view.InsetsState ) v0 ).calculateInsets ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/view/InsetsState;->calculateInsets(Landroid/graphics/Rect;IZ)Landroid/graphics/Insets;
/* iget v0, v0, Landroid/graphics/Insets;->bottom:I */
/* .line 206 */
} // .end method
public static com.android.server.wm.WindowState getWinStateFromInputWinMap ( com.android.server.wm.WindowManagerService p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p0, "windowManagerService" # Lcom/android/server/wm/WindowManagerService; */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 154 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 155 */
try { // :try_start_0
v1 = this.mInputToWindowMap;
(( java.util.HashMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/wm/WindowState; */
/* monitor-exit v0 */
/* .line 156 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private static Integer getmGravity ( android.app.WindowConfiguration p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "windowconfiguration" # Landroid/app/WindowConfiguration; */
/* .param p1, "mGravity" # I */
/* .line 368 */
v0 = (( android.app.WindowConfiguration ) p0 ).getDisplayRotation ( ); // invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I
/* if-nez v0, :cond_0 */
/* .line 369 */
int p1 = 5; // const/4 p1, 0x5
/* .line 370 */
} // :cond_0
v0 = (( android.app.WindowConfiguration ) p0 ).getDisplayRotation ( ); // invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
/* .line 371 */
int p1 = 3; // const/4 p1, 0x3
/* .line 373 */
} // :cond_1
} // :goto_0
} // .end method
private Boolean isFixedOrizationScaleEnable ( com.android.server.wm.WindowState p0 ) {
/* .locals 1 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 232 */
(( com.android.server.wm.WindowState ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 233 */
(( com.android.server.wm.WindowState ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
v0 = (( com.android.server.wm.DisplayContent ) v0 ).getIgnoreOrientationRequest ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getIgnoreOrientationRequest()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 232 */
} // :goto_0
} // .end method
public static Boolean isFlipScale ( com.android.server.wm.WindowState p0 ) {
/* .locals 2 */
/* .param p0, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 395 */
/* iget v0, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* cmpl-float v0, v0, v1 */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isImageWallpaper ( com.android.server.wm.WindowState p0 ) {
/* .locals 2 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 307 */
(( com.android.server.wm.WindowState ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getName()Ljava/lang/String;
final String v1 = "ImageWallpaper"; // const-string v1, "ImageWallpaper"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 308 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isinMiuiSizeCompatModeEnable ( com.android.server.wm.WindowState p0 ) {
/* .locals 1 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 238 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mActivityRecord;
/* .line 239 */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).inMiuiSizeCompatMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 238 */
} // :goto_0
} // .end method
private Boolean needNavBarOffset ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "gravity" # I */
/* .line 253 */
/* and-int/lit8 v0, p1, 0x70 */
/* const/16 v1, 0x50 */
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean needUpdateWindowEnable ( com.android.server.wm.WindowState p0 ) {
/* .locals 3 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 214 */
v0 = this.mActivityRecord;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v0, p1, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* const/high16 v2, 0x3f800000 # 1.0f */
/* cmpl-float v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 219 */
} // :cond_0
(( com.android.server.wm.WindowState ) p1 ).getAttrs ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/WindowStateStubImpl;->switchWindowType(Landroid/view/WindowManager$LayoutParams;)Z */
/* if-nez v0, :cond_1 */
/* .line 220 */
/* .line 223 */
} // :cond_1
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->isinMiuiSizeCompatModeEnable(Lcom/android/server/wm/WindowState;)Z */
/* if-nez v0, :cond_2 */
/* .line 224 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->isFixedOrizationScaleEnable(Lcom/android/server/wm/WindowState;)Z */
/* if-nez v0, :cond_2 */
/* .line 225 */
/* .line 228 */
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
/* .line 216 */
} // :cond_3
} // :goto_0
} // .end method
private Boolean needUpdateWindowEnableForFlip ( com.android.server.wm.WindowState p0 ) {
/* .locals 3 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 379 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 380 */
/* .line 382 */
} // :cond_0
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v2 = this.mAttrs;
v2 = this.packageName;
/* .line 383 */
v0 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v0 ).shouldNotApplyAspectRatio ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->shouldNotApplyAspectRatio(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 384 */
/* .line 386 */
} // :cond_1
v0 = (( com.android.server.wm.WindowState ) p1 ).getWindowType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* sparse-switch v0, :sswitch_data_0 */
/* .line 391 */
/* .line 389 */
/* :sswitch_0 */
int v0 = 1; // const/4 v0, 0x1
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x7d5 -> :sswitch_0 */
/* 0x7ef -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private Boolean switchWindowType ( android.view.WindowManager$LayoutParams p0 ) {
/* .locals 1 */
/* .param p1, "Attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 243 */
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I */
/* sparse-switch v0, :sswitch_data_0 */
/* .line 248 */
int v0 = 0; // const/4 v0, 0x0
/* .line 246 */
/* :sswitch_0 */
int v0 = 1; // const/4 v0, 0x1
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2 -> :sswitch_0 */
/* 0x3e8 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
/* # virtual methods */
public void adjustFrameForDownscale ( com.android.server.wm.WindowState p0, android.graphics.Rect p1, android.app.WindowConfiguration p2 ) {
/* .locals 6 */
/* .param p1, "ws" # Lcom/android/server/wm/WindowState; */
/* .param p2, "frame" # Landroid/graphics/Rect; */
/* .param p3, "config" # Landroid/app/WindowConfiguration; */
/* .line 258 */
v0 = (( com.android.server.wm.WindowState ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->inFreeformWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p1, Lcom/android/server/wm/WindowState;->mDownscaleScale:F */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* cmpl-float v0, v0, v1 */
/* if-lez v0, :cond_0 */
/* .line 259 */
(( android.app.WindowConfiguration ) p3 ).getBounds ( ); // invoke-virtual {p3}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* .line 260 */
/* .local v0, "bounds":Landroid/graphics/Rect; */
v2 = (( android.graphics.Rect ) v0 ).height ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
/* int-to-float v2, v2 */
/* iget v3, p1, Lcom/android/server/wm/WindowState;->mDownscaleScale:F */
/* div-float v3, v1, v3 */
/* mul-float/2addr v2, v3 */
/* const/high16 v3, 0x3f000000 # 0.5f */
/* add-float/2addr v2, v3 */
/* float-to-int v2, v2 */
/* .line 261 */
/* .local v2, "height":I */
v4 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
/* int-to-float v4, v4 */
/* iget v5, p1, Lcom/android/server/wm/WindowState;->mDownscaleScale:F */
/* div-float/2addr v1, v5 */
/* mul-float/2addr v4, v1 */
/* add-float/2addr v4, v3 */
/* float-to-int v1, v4 */
/* .line 262 */
/* .local v1, "width":I */
/* iget v3, p2, Landroid/graphics/Rect;->left:I */
/* add-int/2addr v3, v1 */
/* iput v3, p2, Landroid/graphics/Rect;->right:I */
/* .line 263 */
/* iget v3, p2, Landroid/graphics/Rect;->top:I */
/* add-int/2addr v3, v2 */
/* iput v3, p2, Landroid/graphics/Rect;->bottom:I */
/* .line 265 */
} // .end local v0 # "bounds":Landroid/graphics/Rect;
} // .end local v1 # "width":I
} // .end local v2 # "height":I
} // :cond_0
return;
} // .end method
public android.view.InsetsState adjustInsetsForFlipFolded ( com.android.server.wm.WindowState p0, android.view.InsetsState p1 ) {
/* .locals 2 */
/* .param p1, "ws" # Lcom/android/server/wm/WindowState; */
/* .param p2, "insets" # Landroid/view/InsetsState; */
/* .line 400 */
if ( p1 != null) { // if-eqz p1, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_3
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 403 */
v0 = android.view.DisplayCutoutStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 404 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 405 */
v0 = (( com.android.server.wm.WindowState ) p1 ).getActivityType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getActivityType()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 408 */
} // :cond_0
v0 = this.mActivityRecord;
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).inMiuiSizeCompatMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z
/* if-nez v0, :cond_1 */
v0 = this.mAttrs;
v0 = this.accessibilityTitle;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mAttrs;
v0 = this.accessibilityTitle;
/* .line 409 */
final String v1 = "MiuixImmersionDialog"; // const-string v1, "MiuixImmersionDialog"
v0 = (( java.lang.Object ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 410 */
} // :cond_1
v0 = android.view.DisplayCutout.NO_CUTOUT;
(( android.view.InsetsState ) p2 ).setDisplayCutout ( v0 ); // invoke-virtual {p2, v0}, Landroid/view/InsetsState;->setDisplayCutout(Landroid/view/DisplayCutout;)V
/* .line 412 */
} // :cond_2
/* .line 406 */
} // :cond_3
} // :goto_0
} // .end method
public android.view.InsetsState adjustInsetsForSplit ( com.android.server.wm.WindowState p0, android.view.InsetsState p1 ) {
/* .locals 4 */
/* .param p1, "ws" # Lcom/android/server/wm/WindowState; */
/* .param p2, "insets" # Landroid/view/InsetsState; */
/* .line 166 */
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
/* .line 167 */
if ( p1 != null) { // if-eqz p1, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 168 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).isVerticalSplit ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isVerticalSplit()Z
/* if-nez v0, :cond_0 */
/* .line 171 */
} // :cond_0
v0 = (( com.android.server.wm.WindowState ) p1 ).inSplitScreenWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->inSplitScreenWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mActivityRecord;
/* iget-boolean v0, v0, Lcom/android/server/wm/ActivityRecord;->mImeInsetsFrozenUntilStartInput:Z */
/* if-nez v0, :cond_2 */
/* .line 173 */
(( com.android.server.wm.WindowState ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
/* .line 174 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.DisplayContent ) v0 ).getImeTarget ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;
/* .line 175 */
/* .local v0, "imeTarget":Lcom/android/server/wm/InsetsControlTarget; */
if ( v0 != null) { // if-eqz v0, :cond_1
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 176 */
(( com.android.server.wm.WindowState ) p1 ).getTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.WindowState ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
/* if-eq v2, v3, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* nop */
/* .line 177 */
/* .local v1, "removeIme":Z */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
(( android.view.InsetsState ) p2 ).removeSource ( v2 ); // invoke-virtual {p2, v2}, Landroid/view/InsetsState;->removeSource(I)V
/* .line 179 */
} // .end local v0 # "imeTarget":Lcom/android/server/wm/InsetsControlTarget;
} // .end local v1 # "removeIme":Z
} // :cond_2
/* .line 169 */
} // :cond_3
} // :goto_1
} // .end method
public com.android.server.wm.IMiuiWindowStateEx getMiuiWindowStateEx ( com.android.server.wm.WindowManagerService p0, java.lang.Object p1 ) {
/* .locals 1 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "w" # Ljava/lang/Object; */
/* .line 161 */
/* new-instance v0, Lcom/android/server/wm/MiuiWindowStateEx; */
/* invoke-direct {v0, p1, p2}, Lcom/android/server/wm/MiuiWindowStateEx;-><init>(Lcom/android/server/wm/WindowManagerService;Ljava/lang/Object;)V */
} // .end method
public Float getScaleCurrentDuration ( com.android.server.wm.DisplayContent p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "windowAnimationScale" # F */
/* .line 312 */
/* if-nez p1, :cond_0 */
/* .line 313 */
/* .line 315 */
} // :cond_0
v0 = v0 = this.mDisplayContentStub;
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
/* move v0, p2 */
} // :goto_0
} // .end method
public Boolean isMiuiLayoutInCutoutAlways ( android.view.WindowManager$LayoutParams p0 ) {
/* .locals 1 */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 58 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isStatusBarForceBlackWindow ( android.view.WindowManager$LayoutParams p0 ) {
/* .locals 2 */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 68 */
final String v0 = "ForceBlack"; // const-string v0, "ForceBlack"
(( android.view.WindowManager$LayoutParams ) p1 ).getTitle ( ); // invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "com.android.systemui"; // const-string v0, "com.android.systemui"
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isWallpaperOffsetUpdateCompleted ( com.android.server.wm.WindowState p0 ) {
/* .locals 1 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 302 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->isImageWallpaper(Lcom/android/server/wm/WindowState;)Z */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 303 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z */
} // .end method
public Boolean notifyNonAppSurfaceVisibilityChanged ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "type" # I */
/* .line 64 */
final String v0 = "com.miui.home"; // const-string v0, "com.miui.home"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* if-ne p2, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void updateClientHoverState ( com.android.server.wm.WindowState p0, Boolean p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .param p2, "hoverMode" # Z */
/* .param p3, "supportTouch" # Z */
/* .line 271 */
try { // :try_start_0
v0 = this.mClient;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 274 */
/* .line 272 */
/* :catch_0 */
/* move-exception v0 */
/* .line 273 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 275 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void updateHoverGuidePanel ( com.android.server.wm.WindowState p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .param p2, "add" # Z */
/* .line 278 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 279 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 280 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 281 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).updateHoverGuidePanel ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->updateHoverGuidePanel(Lcom/android/server/wm/WindowState;Z)V
/* .line 283 */
} // :cond_0
return;
} // .end method
public Boolean updateSizeCompatModeWindowInsetsPosition ( com.android.server.wm.WindowState p0, android.graphics.Point p1, android.graphics.Rect p2 ) {
/* .locals 1 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .param p2, "outPos" # Landroid/graphics/Point; */
/* .param p3, "surfaceInsets" # Landroid/graphics/Rect; */
/* .line 197 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->needUpdateWindowEnable(Lcom/android/server/wm/WindowState;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 198 */
/* iget v0, p3, Landroid/graphics/Rect;->left:I */
/* iput v0, p2, Landroid/graphics/Point;->x:I */
/* .line 199 */
/* iget v0, p3, Landroid/graphics/Rect;->top:I */
/* iput v0, p2, Landroid/graphics/Point;->y:I */
/* .line 200 */
int v0 = 1; // const/4 v0, 0x1
/* .line 202 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void updateSizeCompatModeWindowPosition ( android.graphics.Point p0, com.android.server.wm.WindowState p1 ) {
/* .locals 3 */
/* .param p1, "mSurfacePosition" # Landroid/graphics/Point; */
/* .param p2, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 184 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/wm/WindowStateStubImpl;->needUpdateWindowEnable(Lcom/android/server/wm/WindowState;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 185 */
/* iget v0, p1, Landroid/graphics/Point;->x:I */
/* int-to-float v0, v0 */
/* iget v1, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* mul-float/2addr v0, v1 */
/* const/high16 v1, 0x3f000000 # 0.5f */
/* add-float/2addr v0, v1 */
/* float-to-int v0, v0 */
/* iput v0, p1, Landroid/graphics/Point;->x:I */
/* .line 186 */
(( com.android.server.wm.WindowState ) p2 ).getAttrs ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/WindowStateStubImpl;->needNavBarOffset(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 187 */
/* iget v0, p1, Landroid/graphics/Point;->y:I */
/* int-to-float v0, v0 */
/* iget v2, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* mul-float/2addr v0, v2 */
/* add-float/2addr v0, v1 */
/* float-to-int v0, v0 */
/* .line 188 */
v1 = /* invoke-direct {p0, p2}, Lcom/android/server/wm/WindowStateStubImpl;->getNavBarHeight(Lcom/android/server/wm/WindowState;)I */
/* sub-int/2addr v0, v1 */
/* iput v0, p1, Landroid/graphics/Point;->y:I */
/* .line 190 */
} // :cond_0
/* iget v0, p1, Landroid/graphics/Point;->y:I */
/* int-to-float v0, v0 */
/* iget v2, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* mul-float/2addr v0, v2 */
/* add-float/2addr v0, v1 */
/* float-to-int v0, v0 */
/* iput v0, p1, Landroid/graphics/Point;->y:I */
/* .line 193 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void updateSizeCompatModeWindowPositionForFlip ( android.graphics.Point p0, com.android.server.wm.WindowState p1 ) {
/* .locals 9 */
/* .param p1, "mSurfacePosition" # Landroid/graphics/Point; */
/* .param p2, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 330 */
v0 = this.mToken;
/* .line 332 */
/* .local v0, "token":Lcom/android/server/wm/WindowToken; */
v1 = /* invoke-direct {p0, p2}, Lcom/android/server/wm/WindowStateStubImpl;->needUpdateWindowEnableForFlip(Lcom/android/server/wm/WindowState;)Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 334 */
v1 = (( com.android.server.wm.WindowState ) p2 ).getWindowType ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* const/16 v2, 0x7d5 */
/* if-eq v1, v2, :cond_0 */
/* .line 335 */
/* iput v1, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* .line 338 */
} // :cond_0
v1 = this.mDisplayContent;
v1 = (( com.android.server.wm.DisplayContent ) v1 ).inTransition ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->inTransition()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 339 */
v1 = (( com.android.server.wm.WindowState ) p2 ).getWindowType ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* if-ne v1, v2, :cond_1 */
/* .line 340 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.WindowState ) p2 ).hide ( v1, v1 ); // invoke-virtual {p2, v1, v1}, Lcom/android/server/wm/WindowState;->hide(ZZ)Z
/* .line 343 */
} // :cond_1
android.app.servertransaction.BoundsCompat .getInstance ( );
/* const v3, 0x3fdc3e06 */
/* new-instance v4, Landroid/graphics/Point; */
/* .line 345 */
(( com.android.server.wm.WindowToken ) v0 ).getBounds ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowToken;->getBounds()Landroid/graphics/Rect;
v1 = (( android.graphics.Rect ) v1 ).width ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->width()I
(( com.android.server.wm.WindowToken ) v0 ).getBounds ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowToken;->getBounds()Landroid/graphics/Rect;
v5 = (( android.graphics.Rect ) v5 ).height ( ); // invoke-virtual {v5}, Landroid/graphics/Rect;->height()I
/* invoke-direct {v4, v1, v5}, Landroid/graphics/Point;-><init>(II)V */
/* .line 346 */
(( com.android.server.wm.WindowState ) p2 ).getWindowConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;
/* const/16 v5, 0x11 */
v5 = com.android.server.wm.WindowStateStubImpl .getmGravity ( v1,v5 );
/* .line 347 */
(( com.android.server.wm.WindowState ) p2 ).getWindowConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;
v6 = (( android.app.WindowConfiguration ) v1 ).getRotation ( ); // invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getRotation()I
/* .line 348 */
(( com.android.server.wm.WindowState ) p2 ).getWindowConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;
v7 = (( android.app.WindowConfiguration ) v1 ).getRotation ( ); // invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getRotation()I
/* iget v8, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* .line 343 */
/* invoke-virtual/range {v2 ..v8}, Landroid/app/servertransaction/BoundsCompat;->computeCompatBounds(FLandroid/graphics/Point;IIIF)Landroid/graphics/Rect; */
/* .line 350 */
/* .local v1, "rect":Landroid/graphics/Rect; */
(( com.android.server.wm.WindowState ) p2 ).setBounds ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/wm/WindowState;->setBounds(Landroid/graphics/Rect;)I
/* .line 352 */
(( com.android.server.wm.WindowState ) p2 ).getWindowConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;
v2 = (( android.app.WindowConfiguration ) v2 ).getDisplayRotation ( ); // invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getDisplayRotation()I
int v3 = 2; // const/4 v3, 0x2
/* const-wide/high16 v4, 0x3fe0000000000000L # 0.5 */
/* if-ne v2, v3, :cond_2 */
/* .line 353 */
/* iget v2, p1, Landroid/graphics/Point;->x:I */
/* int-to-float v2, v2 */
/* iget v3, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* mul-float/2addr v2, v3 */
/* float-to-double v2, v2 */
/* add-double/2addr v2, v4 */
/* double-to-int v2, v2 */
/* iput v2, p1, Landroid/graphics/Point;->x:I */
/* .line 355 */
} // :cond_2
/* iget v2, p1, Landroid/graphics/Point;->y:I */
/* int-to-float v2, v2 */
/* iget v3, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* mul-float/2addr v2, v3 */
/* float-to-double v2, v2 */
/* add-double/2addr v2, v4 */
/* double-to-int v2, v2 */
/* iput v2, p1, Landroid/graphics/Point;->y:I */
/* .line 356 */
} // .end local v1 # "rect":Landroid/graphics/Rect;
/* .line 357 */
} // :cond_3
v1 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 358 */
v1 = android.util.MiuiAppSizeCompatModeStub .get ( );
/* if-nez v1, :cond_4 */
/* .line 359 */
v1 = (( com.android.server.wm.WindowState ) p2 ).getWindowType ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* const/16 v2, 0x7ef */
/* if-ne v1, v2, :cond_4 */
/* .line 360 */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* iput v1, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F */
/* .line 361 */
(( com.android.server.wm.WindowToken ) v0 ).getBounds ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowToken;->getBounds()Landroid/graphics/Rect;
(( com.android.server.wm.WindowState ) p2 ).setBounds ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/wm/WindowState;->setBounds(Landroid/graphics/Rect;)I
/* .line 365 */
} // :cond_4
} // :goto_0
return;
} // .end method
public Boolean updateSyncStateReady ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "viewVisibility" # I */
/* .param p2, "syncState" # I */
/* .line 321 */
/* const/16 v0, 0x8 */
/* if-ne p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p2, v0, :cond_0 */
/* .line 322 */
/* .line 324 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void updateWallpaperOffsetUpdateState ( ) {
/* .locals 1 */
/* .line 288 */
/* iget-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 289 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z */
/* .line 291 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z */
/* .line 293 */
} // :goto_0
return;
} // .end method
public void updateWallpaperOffsetUpdateState ( com.android.server.wm.WindowState p0 ) {
/* .locals 1 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 296 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->isImageWallpaper(Lcom/android/server/wm/WindowState;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z */
/* if-nez v0, :cond_0 */
/* .line 297 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z */
/* .line 299 */
} // :cond_0
return;
} // .end method
