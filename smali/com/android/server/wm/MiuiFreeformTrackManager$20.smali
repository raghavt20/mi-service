.class Lcom/android/server/wm/MiuiFreeformTrackManager$20;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiFreeformTrackManager;->trackSplitScreenRecommendClickEvent(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

.field final synthetic val$applicationName:Ljava/lang/String;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiFreeformTrackManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 682
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iput-object p2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->val$applicationName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 684
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mFreeFormTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 686
    :try_start_0
    invoke-static {}, Lcom/android/server/wm/MiuiFreeformTrackManager;->isCanOneTrack()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 687
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 689
    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    if-nez v1, :cond_1

    .line 690
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->bindOneTrackService()V

    .line 692
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 693
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 694
    .local v1, "jsonData":Lorg/json/JSONObject;
    const-string/jumbo v2, "tip"

    const-string v3, "621.10.1.1.29120"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 695
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v2, v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$mputCommomParam(Lcom/android/server/wm/MiuiFreeformTrackManager;Lorg/json/JSONObject;)V

    .line 696
    const-string v2, "EVENT_NAME"

    const-string v3, "recommendation_click"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 697
    const-string v2, "app_package_combination"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->val$packageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 698
    const-string v2, "app_display_name_combination"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->val$applicationName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 699
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$20;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v2}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v2

    const-string v3, "31000000538"

    const-string v4, "android"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 703
    .end local v1    # "jsonData":Lorg/json/JSONObject;
    :cond_2
    goto :goto_0

    .line 704
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 701
    :catch_0
    move-exception v1

    .line 702
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 704
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v0

    .line 705
    return-void

    .line 704
    :goto_1
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method
