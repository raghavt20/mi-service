.class Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;
.super Ljava/lang/Object;
.source "ActivityRecordImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppOrientationInfo"
.end annotation


# instance fields
.field mFlipScreenOrientationOuter:I

.field mScreenOrientationInner:I

.field mScreenOrientationOuter:I

.field mScreenOrientationPad:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390
    const/4 v0, -0x2

    iput v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationInner:I

    .line 391
    iput v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationOuter:I

    .line 392
    iput v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationPad:I

    .line 393
    iput v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mFlipScreenOrientationOuter:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;-><init>()V

    return-void
.end method
