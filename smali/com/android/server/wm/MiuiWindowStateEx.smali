.class public final Lcom/android/server/wm/MiuiWindowStateEx;
.super Ljava/lang/Object;
.source "MiuiWindowStateEx.java"

# interfaces
.implements Lcom/android/server/wm/IMiuiWindowStateEx;


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiWindowStateEx"


# instance fields
.field private mDimAssist:Z

.field private mIsDimWindow:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private mScreenWakeLock:Landroid/os/PowerManager$WakeLock;

.field final mService:Lcom/android/server/wm/WindowManagerService;

.field private final mWinState:Lcom/android/server/wm/WindowState;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/WindowManagerService;Ljava/lang/Object;)V
    .locals 3
    .param p1, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "w"    # Ljava/lang/Object;

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 29
    move-object v0, p2

    check-cast v0, Lcom/android/server/wm/WindowState;

    iput-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mWinState:Lcom/android/server/wm/WindowState;

    .line 30
    iget-object v0, p1, Lcom/android/server/wm/WindowManagerService;->mPowerManager:Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mPowerManager:Landroid/os/PowerManager;

    .line 31
    const v1, 0x3000000a

    const-string v2, "WAKEUP-FROM-CLOUD-DIM"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 35
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 36
    return-void
.end method


# virtual methods
.method public getScreenOffTime()J
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getScreenOffTimeout()J

    move-result-wide v0

    return-wide v0
.end method

.method public isAssistDim()Z
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mDimAssist:Z

    return v0
.end method

.method public isCloudDimWindowingMode()Z
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mWinState:Lcom/android/server/wm/WindowState;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mWinState:Lcom/android/server/wm/WindowState;

    .line 40
    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mWinState:Lcom/android/server/wm/WindowState;

    .line 41
    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 43
    const/4 v0, 0x1

    return v0

    .line 45
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isDimWindow()Z
    .locals 1

    .line 73
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mIsDimWindow:Z

    return v0
.end method

.method public isKeepScreenOnFlag()Z
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mWinState:Lcom/android/server/wm/WindowState;

    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 50
    .local v0, "attrFlags":I
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 51
    const/4 v1, 0x1

    return v1

    .line 53
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public resetVsyncRate()V
    .locals 3

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mWinState:Lcom/android/server/wm/WindowState;

    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v0}, Landroid/view/IWindow;->notifyResetVsyncRate()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MiuiWindowStateEx.resetVsyncRate catch RemoteException"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiWindowStateEx"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public setDimAssist(Z)V
    .locals 0
    .param p1, "mDimAssist"    # Z

    .line 65
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mDimAssist:Z

    .line 66
    return-void
.end method

.method public setIsDimWindow(Z)V
    .locals 0
    .param p1, "mIsDimWindow"    # Z

    .line 61
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mIsDimWindow:Z

    .line 62
    return-void
.end method

.method public setMiuiUserActivityTimeOut(I)V
    .locals 3
    .param p1, "miuiUserActivityTimeOut"    # I

    .line 69
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mWinState:Lcom/android/server/wm/WindowState;

    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    int-to-long v1, p1

    iput-wide v1, v0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    .line 70
    return-void
.end method

.method public setVsyncRate(I)V
    .locals 3
    .param p1, "rate"    # I

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowStateEx;->mWinState:Lcom/android/server/wm/WindowState;

    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v0, p1}, Landroid/view/IWindow;->notifySetVsyncRate(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MiuiWindowStateEx.setVsyncRate catch RemoteException"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiWindowStateEx"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
