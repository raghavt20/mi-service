.class public Lcom/android/server/wm/LaunchParamsControllerImpl;
.super Ljava/lang/Object;
.source "LaunchParamsControllerImpl.java"

# interfaces
.implements Lcom/android/server/wm/LaunchParamsControllerStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public registerDefaultModifiers(Lcom/android/server/wm/LaunchParamsController;)V
    .locals 1
    .param p1, "controller"    # Lcom/android/server/wm/LaunchParamsController;

    .line 10
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    new-instance v0, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiDesktopModeLaunchParamsModifier;-><init>()V

    invoke-virtual {p1, v0}, Lcom/android/server/wm/LaunchParamsController;->registerModifier(Lcom/android/server/wm/LaunchParamsController$LaunchParamsModifier;)V

    .line 14
    :cond_0
    return-void
.end method
