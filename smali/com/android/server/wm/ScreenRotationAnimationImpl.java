public class com.android.server.wm.ScreenRotationAnimationImpl implements com.android.server.wm.ScreenRotationAnimationStub {
	 /* .source "ScreenRotationAnimationImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;, */
	 /* Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;, */
	 /* Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;, */
	 /* Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;, */
	 /* Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer BLACK_SURFACE_INVALID_POSITION;
public static final Integer COVER_EGE;
public static final Integer COVER_OFFSET;
private static java.lang.String OPAQUE_APPNAME_LIST;
private static final Float SCALE_FACTOR;
private static final java.lang.String TAG;
public static final Integer TYPE_APP;
public static final Integer TYPE_BACKGROUND;
public static final Integer TYPE_SCREEN_SHOT;
public static sTmpFloats;
/* # instance fields */
private Integer mAlphaDuration;
private android.view.animation.Interpolator mAlphaInterpolator;
private android.content.Context mContext;
private Boolean mEnableRotationAnimation;
private Integer mFirstPhaseDuration;
private android.view.animation.Interpolator mFirstPhaseInterpolator;
private Integer mLongDuration;
private android.view.animation.Interpolator mLongEaseInterpolator;
private Integer mMiddleDuration;
private android.view.animation.Interpolator mMiddleEaseInterpolater;
private Integer mMiuiScreenRotationMode;
private final android.view.animation.Interpolator mQuartEaseOutInterpolator;
private Integer mScaleDelayTime;
private Integer mScaleDuration;
private Float mScaleFactor;
private Integer mScreenRotationDuration;
private Integer mShortAlphaDuration;
private android.view.animation.Interpolator mShortAlphaInterpolator;
private Integer mShortDuration;
private android.view.animation.Interpolator mShortEaseInterpolater;
private final android.view.animation.Interpolator mSinEaseInOutInterpolator;
private android.view.animation.Interpolator mSinEaseOutInterpolator;
android.view.SurfaceControl mSurfaceControlCoverBt;
android.view.SurfaceControl mSurfaceControlCoverLt;
android.view.SurfaceControl mSurfaceControlCoverRt;
android.view.SurfaceControl mSurfaceControlCoverTp;
private com.android.server.wm.WindowManagerService mWms;
/* # direct methods */
static com.android.server.wm.ScreenRotationAnimationImpl ( ) {
	 /* .locals 1 */
	 /* .line 47 */
	 /* const/16 v0, 0x9 */
	 /* new-array v0, v0, [F */
	 return;
} // .end method
public com.android.server.wm.ScreenRotationAnimationImpl ( ) {
	 /* .locals 1 */
	 /* .line 36 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 53 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseOutInterpolater; */
	 /* invoke-direct {v0}, Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseOutInterpolater;-><init>()V */
	 this.mSinEaseOutInterpolator = v0;
	 /* .line 55 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseQuartOutInterpolator; */
	 /* invoke-direct {v0}, Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseQuartOutInterpolator;-><init>()V */
	 this.mQuartEaseOutInterpolator = v0;
	 /* .line 57 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseSineInOutInterpolator; */
	 /* invoke-direct {v0}, Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseSineInOutInterpolator;-><init>()V */
	 this.mSinEaseInOutInterpolator = v0;
	 return;
} // .end method
/* # virtual methods */
public android.view.animation.Animation createRotation180Enter ( ) {
	 /* .locals 18 */
	 /* .line 785 */
	 /* move-object/from16 v0, p0 */
	 /* new-instance v1, Landroid/view/animation/AnimationSet; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
	 /* .line 787 */
	 /* .local v1, "set":Landroid/view/animation/AnimationSet; */
	 v3 = 	 com.android.server.wm.MiuiRotationAnimationUtils .getRotationDuration ( );
	 /* .line 788 */
	 /* .local v3, "screenrotationduration":I */
	 /* new-instance v11, Landroid/view/animation/RotateAnimation; */
	 /* const/high16 v5, 0x43340000 # 180.0f */
	 int v6 = 0; // const/4 v6, 0x0
	 int v7 = 1; // const/4 v7, 0x1
	 /* const/high16 v8, 0x3f000000 # 0.5f */
	 int v9 = 1; // const/4 v9, 0x1
	 /* const/high16 v10, 0x3f000000 # 0.5f */
	 /* move-object v4, v11 */
	 /* invoke-direct/range {v4 ..v10}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V */
	 /* .line 791 */
	 /* .local v4, "rotateAnimation":Landroid/view/animation/RotateAnimation; */
	 v5 = this.mQuartEaseOutInterpolator;
	 (( android.view.animation.RotateAnimation ) v4 ).setInterpolator ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
	 /* .line 792 */
	 /* int-to-long v5, v3 */
	 (( android.view.animation.RotateAnimation ) v4 ).setDuration ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/view/animation/RotateAnimation;->setDuration(J)V
	 /* .line 793 */
	 int v5 = 1; // const/4 v5, 0x1
	 (( android.view.animation.RotateAnimation ) v4 ).setFillAfter ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V
	 /* .line 794 */
	 (( android.view.animation.RotateAnimation ) v4 ).setFillBefore ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillBefore(Z)V
	 /* .line 795 */
	 (( android.view.animation.RotateAnimation ) v4 ).setFillEnabled ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V
	 /* .line 796 */
	 (( android.view.animation.AnimationSet ) v1 ).addAnimation ( v4 ); // invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
	 /* .line 798 */
	 /* new-instance v15, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation; */
	 /* const/high16 v7, 0x3f800000 # 1.0f */
	 /* const v8, 0x3f70a3d7 # 0.94f */
	 /* const/high16 v9, 0x3f800000 # 1.0f */
	 /* const v10, 0x3f70a3d7 # 0.94f */
	 int v11 = 1; // const/4 v11, 0x1
	 /* const/high16 v12, 0x3f000000 # 0.5f */
	 int v13 = 1; // const/4 v13, 0x1
	 /* const/high16 v14, 0x3f000000 # 0.5f */
	 /* move-object v6, v15 */
	 /* invoke-direct/range {v6 ..v14}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;-><init>(FFFFIFIF)V */
	 /* .line 803 */
	 /* .local v6, "scalephase1":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation; */
	 v7 = this.mQuartEaseOutInterpolator;
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v6 ).setInterpolator ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
	 /* .line 804 */
	 /* div-int/lit8 v7, v3, 0x3 */
	 /* int-to-long v7, v7 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v6 ).setDuration ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setDuration(J)V
	 /* .line 805 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v6 ).setFillAfter ( v2 ); // invoke-virtual {v6, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillAfter(Z)V
	 /* .line 806 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v6 ).setFillBefore ( v2 ); // invoke-virtual {v6, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillBefore(Z)V
	 /* .line 807 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v6 ).setFillEnabled ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillEnabled(Z)V
	 /* .line 808 */
	 (( android.view.animation.AnimationSet ) v1 ).addAnimation ( v6 ); // invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
	 /* .line 810 */
	 /* new-instance v7, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation; */
	 /* const/high16 v11, 0x3f800000 # 1.0f */
	 /* const v12, 0x3f70a3d7 # 0.94f */
	 /* const/high16 v13, 0x3f800000 # 1.0f */
	 int v14 = 1; // const/4 v14, 0x1
	 /* const/high16 v15, 0x3f000000 # 0.5f */
	 /* const/16 v16, 0x1 */
	 /* const/high16 v17, 0x3f000000 # 0.5f */
	 /* move-object v9, v7 */
	 /* invoke-direct/range {v9 ..v17}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;-><init>(FFFFIFIF)V */
	 /* .line 814 */
	 /* .local v7, "scalephase2":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation; */
	 v8 = this.mSinEaseInOutInterpolator;
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setInterpolator ( v8 ); // invoke-virtual {v7, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
	 /* .line 815 */
	 /* div-int/lit8 v8, v3, 0x3 */
	 /* int-to-long v8, v8 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setStartOffset ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setStartOffset(J)V
	 /* .line 816 */
	 /* mul-int/lit8 v8, v3, 0x2 */
	 /* div-int/lit8 v8, v8, 0x3 */
	 /* int-to-long v8, v8 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setDuration ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setDuration(J)V
	 /* .line 817 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setFillAfter ( v5 ); // invoke-virtual {v7, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillAfter(Z)V
	 /* .line 818 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setFillBefore ( v2 ); // invoke-virtual {v7, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillBefore(Z)V
	 /* .line 819 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setFillEnabled ( v5 ); // invoke-virtual {v7, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillEnabled(Z)V
	 /* .line 820 */
	 (( android.view.animation.AnimationSet ) v1 ).addAnimation ( v7 ); // invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
	 /* .line 822 */
} // .end method
public android.view.animation.Animation createRotation180Exit ( ) {
	 /* .locals 19 */
	 /* .line 730 */
	 /* move-object/from16 v0, p0 */
	 /* new-instance v1, Landroid/view/animation/AnimationSet; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
	 /* .line 732 */
	 /* .local v1, "set":Landroid/view/animation/AnimationSet; */
	 v3 = 	 com.android.server.wm.MiuiRotationAnimationUtils .getRotationDuration ( );
	 /* .line 733 */
	 /* .local v3, "screenrotationduration":I */
	 /* new-instance v11, Landroid/view/animation/RotateAnimation; */
	 int v5 = 0; // const/4 v5, 0x0
	 /* const/high16 v6, -0x3ccc0000 # -180.0f */
	 int v7 = 1; // const/4 v7, 0x1
	 /* const/high16 v8, 0x3f000000 # 0.5f */
	 int v9 = 1; // const/4 v9, 0x1
	 /* const/high16 v10, 0x3f000000 # 0.5f */
	 /* move-object v4, v11 */
	 /* invoke-direct/range {v4 ..v10}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V */
	 /* .line 736 */
	 /* .local v4, "rotateAnimation":Landroid/view/animation/RotateAnimation; */
	 v5 = this.mQuartEaseOutInterpolator;
	 (( android.view.animation.RotateAnimation ) v4 ).setInterpolator ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
	 /* .line 737 */
	 /* int-to-long v5, v3 */
	 (( android.view.animation.RotateAnimation ) v4 ).setDuration ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/view/animation/RotateAnimation;->setDuration(J)V
	 /* .line 738 */
	 int v5 = 1; // const/4 v5, 0x1
	 (( android.view.animation.RotateAnimation ) v4 ).setFillAfter ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V
	 /* .line 739 */
	 (( android.view.animation.RotateAnimation ) v4 ).setFillBefore ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillBefore(Z)V
	 /* .line 740 */
	 (( android.view.animation.RotateAnimation ) v4 ).setFillEnabled ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V
	 /* .line 741 */
	 (( android.view.animation.AnimationSet ) v1 ).addAnimation ( v4 ); // invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
	 /* .line 743 */
	 /* new-instance v6, Landroid/view/animation/AlphaAnimation; */
	 /* const/high16 v7, 0x3f800000 # 1.0f */
	 int v8 = 0; // const/4 v8, 0x0
	 /* invoke-direct {v6, v7, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
	 /* .line 744 */
	 /* .local v6, "alphaAnimation":Landroid/view/animation/AlphaAnimation; */
	 v7 = this.mQuartEaseOutInterpolator;
	 (( android.view.animation.AlphaAnimation ) v6 ).setInterpolator ( v7 ); // invoke-virtual {v6, v7}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
	 /* .line 745 */
	 /* div-int/lit8 v7, v3, 0x2 */
	 /* int-to-long v7, v7 */
	 (( android.view.animation.AlphaAnimation ) v6 ).setDuration ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V
	 /* .line 746 */
	 (( android.view.animation.AlphaAnimation ) v6 ).setFillAfter ( v5 ); // invoke-virtual {v6, v5}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V
	 /* .line 747 */
	 (( android.view.animation.AlphaAnimation ) v6 ).setFillBefore ( v5 ); // invoke-virtual {v6, v5}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V
	 /* .line 748 */
	 (( android.view.animation.AlphaAnimation ) v6 ).setFillEnabled ( v5 ); // invoke-virtual {v6, v5}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V
	 /* .line 749 */
	 (( android.view.animation.AnimationSet ) v1 ).addAnimation ( v6 ); // invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
	 /* .line 751 */
	 /* new-instance v7, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation; */
	 /* const/high16 v10, 0x3f800000 # 1.0f */
	 /* const v11, 0x3f70a3d7 # 0.94f */
	 /* const/high16 v12, 0x3f800000 # 1.0f */
	 /* const v13, 0x3f70a3d7 # 0.94f */
	 int v14 = 1; // const/4 v14, 0x1
	 /* const/high16 v15, 0x3f000000 # 0.5f */
	 /* const/16 v16, 0x1 */
	 /* const/high16 v17, 0x3f000000 # 0.5f */
	 /* move-object v9, v7 */
	 /* invoke-direct/range {v9 ..v17}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;-><init>(FFFFIFIF)V */
	 /* .line 756 */
	 /* .local v7, "scalephase1":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation; */
	 v8 = this.mQuartEaseOutInterpolator;
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setInterpolator ( v8 ); // invoke-virtual {v7, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
	 /* .line 757 */
	 /* div-int/lit8 v8, v3, 0x3 */
	 /* int-to-long v8, v8 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setDuration ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setDuration(J)V
	 /* .line 758 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setFillAfter ( v2 ); // invoke-virtual {v7, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillAfter(Z)V
	 /* .line 759 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setFillBefore ( v2 ); // invoke-virtual {v7, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillBefore(Z)V
	 /* .line 760 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v7 ).setFillEnabled ( v5 ); // invoke-virtual {v7, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillEnabled(Z)V
	 /* .line 761 */
	 (( android.view.animation.AnimationSet ) v1 ).addAnimation ( v7 ); // invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
	 /* .line 763 */
	 /* new-instance v8, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation; */
	 /* const/high16 v14, 0x3f800000 # 1.0f */
	 int v15 = 1; // const/4 v15, 0x1
	 /* const/high16 v16, 0x3f000000 # 0.5f */
	 /* const/16 v17, 0x1 */
	 /* const/high16 v18, 0x3f000000 # 0.5f */
	 /* move-object v10, v8 */
	 /* invoke-direct/range {v10 ..v18}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;-><init>(FFFFIFIF)V */
	 /* .line 768 */
	 /* .local v8, "scalephase2":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation; */
	 v9 = this.mSinEaseInOutInterpolator;
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v8 ).setInterpolator ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
	 /* .line 769 */
	 /* div-int/lit8 v9, v3, 0x3 */
	 /* int-to-long v9, v9 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v8 ).setStartOffset ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setStartOffset(J)V
	 /* .line 770 */
	 /* mul-int/lit8 v9, v3, 0x2 */
	 /* div-int/lit8 v9, v9, 0x3 */
	 /* int-to-long v9, v9 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v8 ).setDuration ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setDuration(J)V
	 /* .line 771 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v8 ).setFillAfter ( v5 ); // invoke-virtual {v8, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillAfter(Z)V
	 /* .line 772 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v8 ).setFillBefore ( v2 ); // invoke-virtual {v8, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillBefore(Z)V
	 /* .line 773 */
	 (( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) v8 ).setFillEnabled ( v5 ); // invoke-virtual {v8, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillEnabled(Z)V
	 /* .line 774 */
	 (( android.view.animation.AnimationSet ) v1 ).addAnimation ( v8 ); // invoke-virtual {v1, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
	 /* .line 776 */
} // .end method
public android.view.animation.Animation createRotationEnterAnimation ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
	 /* .locals 25 */
	 /* .param p1, "width" # I */
	 /* .param p2, "height" # I */
	 /* .param p3, "originRotation" # I */
	 /* .param p4, "curRotation" # I */
	 /* .line 260 */
	 /* move-object/from16 v0, p0 */
	 /* move/from16 v1, p4 */
	 /* new-instance v2, Landroid/view/animation/AnimationSet; */
	 int v3 = 0; // const/4 v3, 0x0
	 /* invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
	 /* .line 262 */
	 /* .local v2, "set":Landroid/view/animation/AnimationSet; */
	 /* move/from16 v4, p2 */
	 /* int-to-float v5, v4 */
	 /* move/from16 v6, p1 */
	 /* int-to-float v7, v6 */
	 /* div-float/2addr v5, v7 */
	 /* .line 263 */
	 /* .local v5, "scale":F */
	 v7 = 	 /* invoke-static/range {p3 ..p4}, Landroid/util/RotationUtils;->deltaRotation(II)I */
	 /* .line 265 */
	 /* .local v7, "delta":I */
	 int v8 = 3; // const/4 v8, 0x3
	 int v15 = 1; // const/4 v15, 0x1
	 /* if-eq v1, v15, :cond_1 */
	 /* if-ne v1, v8, :cond_0 */
	 /* .line 270 */
} // :cond_0
/* move/from16 v9, p1 */
/* .line 271 */
/* .local v9, "curWidth":I */
/* move/from16 v10, p2 */
/* move v14, v9 */
/* move v13, v10 */
/* .local v10, "curHeight":I */
/* .line 267 */
} // .end local v9 # "curWidth":I
} // .end local v10 # "curHeight":I
} // :cond_1
} // :goto_0
/* move/from16 v9, p2 */
/* .line 268 */
/* .restart local v9 # "curWidth":I */
/* move/from16 v10, p1 */
/* move v14, v9 */
/* move v13, v10 */
/* .line 274 */
} // .end local v9 # "curWidth":I
/* .local v13, "curHeight":I */
/* .local v14, "curWidth":I */
} // :goto_1
/* new-instance v9, Landroid/view/animation/RotateAnimation; */
/* .line 275 */
/* if-ne v7, v8, :cond_2 */
/* const/high16 v8, 0x42b40000 # 90.0f */
} // :cond_2
/* const/high16 v8, -0x3d4c0000 # -90.0f */
} // :goto_2
/* move/from16 v17, v8 */
/* const/16 v18, 0x0 */
/* const/16 v19, 0x1 */
/* const/high16 v20, 0x3f000000 # 0.5f */
/* const/16 v21, 0x1 */
/* const/high16 v22, 0x3f000000 # 0.5f */
/* move-object/from16 v16, v9 */
/* invoke-direct/range {v16 ..v22}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V */
/* move-object v12, v9 */
/* .line 278 */
/* .local v12, "rotateAnimation":Landroid/view/animation/RotateAnimation; */
v8 = this.mMiddleEaseInterpolater;
(( android.view.animation.RotateAnimation ) v12 ).setInterpolator ( v8 ); // invoke-virtual {v12, v8}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 279 */
/* iget v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiddleDuration:I */
/* int-to-long v8, v8 */
(( android.view.animation.RotateAnimation ) v12 ).setDuration ( v8, v9 ); // invoke-virtual {v12, v8, v9}, Landroid/view/animation/RotateAnimation;->setDuration(J)V
/* .line 280 */
(( android.view.animation.RotateAnimation ) v12 ).setFillAfter ( v15 ); // invoke-virtual {v12, v15}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V
/* .line 281 */
(( android.view.animation.RotateAnimation ) v12 ).setFillBefore ( v15 ); // invoke-virtual {v12, v15}, Landroid/view/animation/RotateAnimation;->setFillBefore(Z)V
/* .line 282 */
(( android.view.animation.RotateAnimation ) v12 ).setFillEnabled ( v15 ); // invoke-virtual {v12, v15}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V
/* .line 283 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v12 ); // invoke-virtual {v2, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 285 */
/* new-instance v8, Landroid/view/animation/ClipRectAnimation; */
/* int-to-float v9, v14 */
/* int-to-float v10, v13 */
/* div-float/2addr v10, v5 */
/* sub-float/2addr v9, v10 */
/* float-to-int v9, v9 */
/* div-int/lit8 v17, v9, 0x2 */
/* int-to-float v9, v13 */
/* int-to-float v10, v14 */
/* div-float/2addr v10, v5 */
/* sub-float/2addr v9, v10 */
/* float-to-int v9, v9 */
/* div-int/lit8 v18, v9, 0x2 */
/* int-to-float v9, v14 */
/* int-to-float v10, v13 */
/* div-float/2addr v10, v5 */
/* add-float/2addr v9, v10 */
/* float-to-int v9, v9 */
/* div-int/lit8 v19, v9, 0x2 */
/* int-to-float v9, v13 */
/* int-to-float v10, v14 */
/* div-float/2addr v10, v5 */
/* add-float/2addr v9, v10 */
/* float-to-int v9, v9 */
/* div-int/lit8 v20, v9, 0x2 */
/* const/16 v21, 0x0 */
/* const/16 v22, 0x0 */
/* move-object/from16 v16, v8 */
/* move/from16 v23, v14 */
/* move/from16 v24, v13 */
/* invoke-direct/range {v16 ..v24}, Landroid/view/animation/ClipRectAnimation;-><init>(IIIIIIII)V */
/* move-object v11, v8 */
/* .line 291 */
/* .local v11, "clipRectAnimation":Landroid/view/animation/ClipRectAnimation; */
v8 = this.mShortEaseInterpolater;
(( android.view.animation.ClipRectAnimation ) v11 ).setInterpolator ( v8 ); // invoke-virtual {v11, v8}, Landroid/view/animation/ClipRectAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 292 */
/* iget v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I */
/* int-to-long v8, v8 */
(( android.view.animation.ClipRectAnimation ) v11 ).setDuration ( v8, v9 ); // invoke-virtual {v11, v8, v9}, Landroid/view/animation/ClipRectAnimation;->setDuration(J)V
/* .line 293 */
(( android.view.animation.ClipRectAnimation ) v11 ).setFillAfter ( v15 ); // invoke-virtual {v11, v15}, Landroid/view/animation/ClipRectAnimation;->setFillAfter(Z)V
/* .line 294 */
(( android.view.animation.ClipRectAnimation ) v11 ).setFillBefore ( v15 ); // invoke-virtual {v11, v15}, Landroid/view/animation/ClipRectAnimation;->setFillBefore(Z)V
/* .line 295 */
(( android.view.animation.ClipRectAnimation ) v11 ).setFillEnabled ( v15 ); // invoke-virtual {v11, v15}, Landroid/view/animation/ClipRectAnimation;->setFillEnabled(Z)V
/* .line 296 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v11 ); // invoke-virtual {v2, v11}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 298 */
/* new-instance v17, Landroid/view/animation/ScaleAnimation; */
/* const/high16 v10, 0x3f800000 # 1.0f */
/* const/high16 v16, 0x3f800000 # 1.0f */
/* const/16 v18, 0x1 */
/* const/high16 v19, 0x3f000000 # 0.5f */
/* const/16 v20, 0x1 */
/* const/high16 v21, 0x3f000000 # 0.5f */
/* move-object/from16 v8, v17 */
/* move v9, v5 */
/* move-object/from16 v22, v11 */
} // .end local v11 # "clipRectAnimation":Landroid/view/animation/ClipRectAnimation;
/* .local v22, "clipRectAnimation":Landroid/view/animation/ClipRectAnimation; */
/* move v11, v5 */
/* move-object/from16 v23, v12 */
} // .end local v12 # "rotateAnimation":Landroid/view/animation/RotateAnimation;
/* .local v23, "rotateAnimation":Landroid/view/animation/RotateAnimation; */
/* move/from16 v12, v16 */
} // .end local v13 # "curHeight":I
/* .local v24, "curHeight":I */
/* move/from16 v13, v18 */
/* move/from16 v18, v14 */
} // .end local v14 # "curWidth":I
/* .local v18, "curWidth":I */
/* move/from16 v14, v19 */
/* move/from16 v15, v20 */
/* move/from16 v16, v21 */
/* invoke-direct/range {v8 ..v16}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V */
/* .line 303 */
/* .local v8, "scaleAnimation":Landroid/view/animation/ScaleAnimation; */
v9 = this.mShortEaseInterpolater;
(( android.view.animation.ScaleAnimation ) v8 ).setInterpolator ( v9 ); // invoke-virtual {v8, v9}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 304 */
/* iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I */
/* int-to-long v9, v9 */
(( android.view.animation.ScaleAnimation ) v8 ).setDuration ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V
/* .line 305 */
(( android.view.animation.ScaleAnimation ) v8 ).setFillAfter ( v3 ); // invoke-virtual {v8, v3}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V
/* .line 306 */
(( android.view.animation.ScaleAnimation ) v8 ).setFillBefore ( v3 ); // invoke-virtual {v8, v3}, Landroid/view/animation/ScaleAnimation;->setFillBefore(Z)V
/* .line 307 */
int v3 = 1; // const/4 v3, 0x1
(( android.view.animation.ScaleAnimation ) v8 ).setFillEnabled ( v3 ); // invoke-virtual {v8, v3}, Landroid/view/animation/ScaleAnimation;->setFillEnabled(Z)V
/* .line 308 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v8 ); // invoke-virtual {v2, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 310 */
/* new-instance v16, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation; */
/* iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleFactor:F */
/* const/high16 v11, 0x3f800000 # 1.0f */
int v12 = 1; // const/4 v12, 0x1
/* const/high16 v13, 0x3f000000 # 0.5f */
int v14 = 1; // const/4 v14, 0x1
/* const/high16 v15, 0x3f000000 # 0.5f */
/* move-object/from16 v9, v16 */
/* invoke-direct/range {v9 ..v15}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;-><init>(FFIFIF)V */
/* .line 314 */
/* .local v9, "screenScaleAnimation":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation; */
/* iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I */
/* int-to-long v10, v10 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v9 ).setDuration ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setDuration(J)V
/* .line 315 */
/* iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mFirstPhaseDuration:I */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v9 ).setFirstPhaseDuration ( v10 ); // invoke-virtual {v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFirstPhaseDuration(I)V
/* .line 316 */
/* iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongDuration:I */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v9 ).setSecPhaseDuration ( v10 ); // invoke-virtual {v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setSecPhaseDuration(I)V
/* .line 317 */
v10 = this.mLongEaseInterpolator;
v11 = this.mFirstPhaseInterpolator;
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v9 ).setAnimationInterpolator ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setAnimationInterpolator(Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V
/* .line 319 */
/* iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I */
/* int-to-float v10, v10 */
/* const/high16 v11, 0x3f800000 # 1.0f */
/* mul-float/2addr v10, v11 */
/* iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I */
/* int-to-float v11, v11 */
/* div-float/2addr v10, v11 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v9 ).setScaleBreakOffset ( v10 ); // invoke-virtual {v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setScaleBreakOffset(F)V
/* .line 321 */
/* iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v9 ).setScaleDelayTime ( v10 ); // invoke-virtual {v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setScaleDelayTime(I)V
/* .line 322 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v9 ).setFillAfter ( v3 ); // invoke-virtual {v9, v3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillAfter(Z)V
/* .line 323 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v9 ).setFillBefore ( v3 ); // invoke-virtual {v9, v3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillBefore(Z)V
/* .line 324 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v9 ).setFillEnabled ( v3 ); // invoke-virtual {v9, v3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillEnabled(Z)V
/* .line 325 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v9 ); // invoke-virtual {v2, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 327 */
} // .end method
public android.view.animation.Animation createRotationExitAnimation ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 29 */
/* .param p1, "width" # I */
/* .param p2, "height" # I */
/* .param p3, "originRotation" # I */
/* .param p4, "curRotation" # I */
/* .line 173 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p4 */
/* new-instance v2, Landroid/view/animation/AnimationSet; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 175 */
/* .local v2, "set":Landroid/view/animation/AnimationSet; */
/* move/from16 v4, p2 */
/* int-to-float v5, v4 */
/* move/from16 v6, p1 */
/* int-to-float v7, v6 */
/* div-float/2addr v5, v7 */
/* .line 176 */
/* .local v5, "scale":F */
v7 = /* invoke-static/range {p3 ..p4}, Landroid/util/RotationUtils;->deltaRotation(II)I */
/* .line 178 */
/* .local v7, "delta":I */
int v9 = 2; // const/4 v9, 0x2
int v12 = 1; // const/4 v12, 0x1
/* if-eq v1, v12, :cond_1 */
int v8 = 3; // const/4 v8, 0x3
/* if-ne v1, v8, :cond_0 */
/* .line 186 */
} // :cond_0
/* move/from16 v8, p1 */
/* .line 187 */
/* .local v8, "curWidth":I */
/* move/from16 v10, p2 */
/* .line 188 */
/* .local v10, "curHeight":I */
/* iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I */
/* if-ne v11, v9, :cond_2 */
/* .line 189 */
v11 = this.mShortAlphaInterpolator;
this.mAlphaInterpolator = v11;
/* .line 190 */
/* iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortAlphaDuration:I */
/* iput v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaDuration:I */
/* .line 180 */
} // .end local v8 # "curWidth":I
} // .end local v10 # "curHeight":I
} // :cond_1
} // :goto_0
/* move/from16 v8, p2 */
/* .line 181 */
/* .restart local v8 # "curWidth":I */
/* move/from16 v10, p1 */
/* .line 182 */
/* .restart local v10 # "curHeight":I */
/* iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I */
/* if-ne v11, v9, :cond_2 */
/* .line 183 */
/* iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDuration:I */
/* iput v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaDuration:I */
/* .line 194 */
} // :cond_2
} // :goto_1
/* move v15, v10 */
/* move v10, v8 */
} // .end local v8 # "curWidth":I
/* .local v10, "curWidth":I */
/* .local v15, "curHeight":I */
/* new-instance v8, Landroid/view/animation/RotateAnimation; */
/* const/16 v17, 0x0 */
/* .line 195 */
/* if-ne v7, v12, :cond_3 */
/* const/high16 v11, 0x42b40000 # 90.0f */
} // :cond_3
/* const/high16 v11, -0x3d4c0000 # -90.0f */
} // :goto_2
/* move/from16 v18, v11 */
/* const/16 v19, 0x1 */
/* const/high16 v20, 0x3f000000 # 0.5f */
/* const/16 v21, 0x1 */
/* const/high16 v22, 0x3f000000 # 0.5f */
/* move-object/from16 v16, v8 */
/* invoke-direct/range {v16 ..v22}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V */
/* move-object v14, v8 */
/* .line 198 */
/* .local v14, "rotateAnimation":Landroid/view/animation/RotateAnimation; */
v8 = this.mMiddleEaseInterpolater;
(( android.view.animation.RotateAnimation ) v14 ).setInterpolator ( v8 ); // invoke-virtual {v14, v8}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 199 */
/* iget v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiddleDuration:I */
/* int-to-long v3, v8 */
(( android.view.animation.RotateAnimation ) v14 ).setDuration ( v3, v4 ); // invoke-virtual {v14, v3, v4}, Landroid/view/animation/RotateAnimation;->setDuration(J)V
/* .line 200 */
(( android.view.animation.RotateAnimation ) v14 ).setFillAfter ( v12 ); // invoke-virtual {v14, v12}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V
/* .line 201 */
(( android.view.animation.RotateAnimation ) v14 ).setFillBefore ( v12 ); // invoke-virtual {v14, v12}, Landroid/view/animation/RotateAnimation;->setFillBefore(Z)V
/* .line 202 */
(( android.view.animation.RotateAnimation ) v14 ).setFillEnabled ( v12 ); // invoke-virtual {v14, v12}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V
/* .line 203 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v14 ); // invoke-virtual {v2, v14}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 205 */
/* new-instance v3, Landroid/view/animation/AlphaAnimation; */
int v4 = 0; // const/4 v4, 0x0
/* const/high16 v13, 0x3f800000 # 1.0f */
/* invoke-direct {v3, v13, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 206 */
/* .local v3, "alphaAnimation":Landroid/view/animation/AlphaAnimation; */
v4 = this.mAlphaInterpolator;
(( android.view.animation.AlphaAnimation ) v3 ).setInterpolator ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 207 */
/* iget v4, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaDuration:I */
/* move-object/from16 v16, v14 */
} // .end local v14 # "rotateAnimation":Landroid/view/animation/RotateAnimation;
/* .local v16, "rotateAnimation":Landroid/view/animation/RotateAnimation; */
/* int-to-long v13, v4 */
(( android.view.animation.AlphaAnimation ) v3 ).setDuration ( v13, v14 ); // invoke-virtual {v3, v13, v14}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V
/* .line 208 */
(( android.view.animation.AlphaAnimation ) v3 ).setFillAfter ( v12 ); // invoke-virtual {v3, v12}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V
/* .line 209 */
(( android.view.animation.AlphaAnimation ) v3 ).setFillBefore ( v12 ); // invoke-virtual {v3, v12}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V
/* .line 210 */
(( android.view.animation.AlphaAnimation ) v3 ).setFillEnabled ( v12 ); // invoke-virtual {v3, v12}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V
/* .line 211 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 213 */
/* new-instance v4, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation; */
/* move-object v8, v4 */
/* const/high16 v11, 0x3f800000 # 1.0f */
int v13 = 1; // const/4 v13, 0x1
/* const/high16 v25, 0x3f800000 # 1.0f */
/* const/high16 v14, 0x3f000000 # 0.5f */
/* move-object/from16 v26, v16 */
} // .end local v16 # "rotateAnimation":Landroid/view/animation/RotateAnimation;
/* .local v26, "rotateAnimation":Landroid/view/animation/RotateAnimation; */
/* const/16 v16, 0x1 */
/* move v11, v15 */
} // .end local v15 # "curHeight":I
/* .local v11, "curHeight":I */
/* move/from16 v15, v16 */
/* const/high16 v16, 0x3f000000 # 0.5f */
/* const/16 v17, 0x0 */
/* const/16 v18, 0x0 */
/* int-to-float v12, v10 */
/* int-to-float v13, v11 */
/* div-float/2addr v13, v5 */
/* sub-float/2addr v12, v13 */
/* float-to-int v12, v12 */
/* div-int/lit8 v21, v12, 0x2 */
/* int-to-float v12, v11 */
/* int-to-float v13, v10 */
/* div-float/2addr v13, v5 */
/* sub-float/2addr v12, v13 */
/* float-to-int v12, v12 */
/* div-int/lit8 v22, v12, 0x2 */
/* int-to-float v12, v10 */
/* int-to-float v13, v11 */
/* div-float/2addr v13, v5 */
/* add-float/2addr v12, v13 */
/* float-to-int v12, v12 */
/* div-int/lit8 v23, v12, 0x2 */
/* int-to-float v12, v11 */
/* int-to-float v13, v10 */
/* div-float/2addr v13, v5 */
/* add-float/2addr v12, v13 */
/* float-to-int v12, v12 */
/* div-int/lit8 v24, v12, 0x2 */
/* move v9, v10 */
/* move/from16 v28, v10 */
} // .end local v10 # "curWidth":I
/* .local v28, "curWidth":I */
/* move v10, v11 */
int v13 = 1; // const/4 v13, 0x1
/* move v12, v5 */
/* move/from16 v19, v28 */
/* move/from16 v20, v11 */
/* move/from16 v27, v11 */
/* move v1, v13 */
/* const/high16 v11, 0x3f800000 # 1.0f */
int v13 = 1; // const/4 v13, 0x1
} // .end local v11 # "curHeight":I
/* .local v27, "curHeight":I */
/* invoke-direct/range {v8 ..v24}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;-><init>(IIFFIFIFIIIIIIII)V */
/* .line 222 */
/* .local v4, "shotClipAnimation":Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation; */
/* iget v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I */
/* int-to-long v8, v8 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ShotClipAnimation ) v4 ).setDuration ( v8, v9 ); // invoke-virtual {v4, v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setDuration(J)V
/* .line 223 */
v8 = this.mShortEaseInterpolater;
(( com.android.server.wm.ScreenRotationAnimationImpl$ShotClipAnimation ) v4 ).setInterpolator ( v8 ); // invoke-virtual {v4, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 224 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ShotClipAnimation ) v4 ).setFillAfter ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setFillAfter(Z)V
/* .line 225 */
int v8 = 0; // const/4 v8, 0x0
(( com.android.server.wm.ScreenRotationAnimationImpl$ShotClipAnimation ) v4 ).setFillBefore ( v8 ); // invoke-virtual {v4, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setFillBefore(Z)V
/* .line 226 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ShotClipAnimation ) v4 ).setFillEnabled ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setFillEnabled(Z)V
/* .line 227 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v4 ); // invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 229 */
/* new-instance v8, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation; */
/* iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleFactor:F */
/* const/high16 v11, 0x3f800000 # 1.0f */
int v12 = 1; // const/4 v12, 0x1
/* const/high16 v13, 0x3f000000 # 0.5f */
int v14 = 1; // const/4 v14, 0x1
/* const/high16 v15, 0x3f000000 # 0.5f */
/* move-object v9, v8 */
/* invoke-direct/range {v9 ..v15}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;-><init>(FFIFIF)V */
/* .line 233 */
/* .local v8, "screenScaleAnimation":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation; */
/* iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I */
/* int-to-long v9, v9 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v8 ).setDuration ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setDuration(J)V
/* .line 234 */
/* iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mFirstPhaseDuration:I */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v8 ).setFirstPhaseDuration ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFirstPhaseDuration(I)V
/* .line 235 */
/* iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongDuration:I */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v8 ).setSecPhaseDuration ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setSecPhaseDuration(I)V
/* .line 236 */
v9 = this.mLongEaseInterpolator;
v10 = this.mFirstPhaseInterpolator;
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v8 ).setAnimationInterpolator ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setAnimationInterpolator(Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V
/* .line 238 */
/* iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I */
/* int-to-float v9, v9 */
/* mul-float v9, v9, v25 */
/* iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I */
/* int-to-float v10, v10 */
/* div-float/2addr v9, v10 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v8 ).setScaleBreakOffset ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setScaleBreakOffset(F)V
/* .line 240 */
/* iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v8 ).setScaleDelayTime ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setScaleDelayTime(I)V
/* .line 241 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v8 ).setFillAfter ( v1 ); // invoke-virtual {v8, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillAfter(Z)V
/* .line 242 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v8 ).setFillBefore ( v1 ); // invoke-virtual {v8, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillBefore(Z)V
/* .line 243 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScaleAnimation ) v8 ).setFillEnabled ( v1 ); // invoke-virtual {v8, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillEnabled(Z)V
/* .line 244 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v8 ); // invoke-virtual {v2, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 246 */
} // .end method
public com.android.server.wm.LocalAnimationAdapter$AnimationSpec createScreenRotationSpec ( android.view.animation.Animation p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "animation" # Landroid/view/animation/Animation; */
/* .param p2, "originalWidth" # I */
/* .param p3, "originalHeight" # I */
/* .line 332 */
/* new-instance v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec; */
/* invoke-direct {v0, p1, p2, p3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;-><init>(Landroid/view/animation/Animation;II)V */
} // .end method
public Integer getDisplayRoundCornerRadius ( ) {
/* .locals 1 */
/* .line 672 */
} // .end method
public Boolean getIsCancelRotationAnimation ( ) {
/* .locals 1 */
/* .line 141 */
/* iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mEnableRotationAnimation:Z */
/* if-nez v0, :cond_0 */
v0 = this.mWms;
v0 = (( com.android.server.wm.WindowManagerService ) v0 ).isKeyguardShowingAndNotOccluded ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->isKeyguardShowingAndNotOccluded()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Integer getScreenRotationAnimationMode ( ) {
/* .locals 1 */
/* .line 137 */
/* iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I */
} // .end method
public void init ( android.content.Context p0, com.android.server.wm.WindowManagerService p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "wms" # Lcom/android/server/wm/WindowManagerService; */
/* .line 84 */
this.mContext = p1;
/* .line 85 */
this.mWms = p2;
/* .line 86 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 87 */
/* const v1, 0x10e000d */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I */
/* .line 88 */
/* new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator; */
v1 = this.mContext;
/* .line 89 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x10500ad */
v1 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
v3 = this.mContext;
/* .line 90 */
(( android.content.Context ) v3 ).getResources ( ); // invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x10500ae */
v3 = (( android.content.res.Resources ) v3 ).getFloat ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getFloat(I)F
/* invoke-direct {v0, v1, v3}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V */
this.mLongEaseInterpolator = v0;
/* .line 91 */
/* new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator; */
v1 = this.mContext;
/* .line 92 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
v1 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
v3 = this.mContext;
/* .line 93 */
(( android.content.Context ) v3 ).getResources ( ); // invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x10500af */
v3 = (( android.content.res.Resources ) v3 ).getFloat ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getFloat(I)F
/* invoke-direct {v0, v1, v3}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V */
this.mMiddleEaseInterpolater = v0;
/* .line 94 */
/* new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator; */
v1 = this.mContext;
/* .line 95 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
v1 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
v2 = this.mContext;
/* .line 96 */
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x10500b0 */
v2 = (( android.content.res.Resources ) v2 ).getFloat ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getFloat(I)F
/* invoke-direct {v0, v1, v2}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V */
this.mShortEaseInterpolater = v0;
/* .line 97 */
v0 = this.mSinEaseOutInterpolator;
this.mAlphaInterpolator = v0;
/* .line 98 */
/* new-instance v0, Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseInInterpolater; */
/* invoke-direct {v0}, Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseInInterpolater;-><init>()V */
this.mShortAlphaInterpolator = v0;
/* .line 99 */
/* iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 100 */
v0 = this.mSinEaseOutInterpolator;
} // :cond_0
v0 = this.mShortEaseInterpolater;
} // :goto_0
this.mFirstPhaseInterpolator = v0;
/* .line 101 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 102 */
/* const v2, 0x10e000a */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongDuration:I */
/* .line 103 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 104 */
/* const v2, 0x10e000c */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiddleDuration:I */
/* .line 105 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 106 */
/* const v2, 0x10e0011 */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I */
/* .line 107 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 108 */
/* const v2, 0x10e000e */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDuration:I */
/* .line 109 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 110 */
/* const v2, 0x10e000f */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I */
/* .line 111 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 112 */
/* const v2, 0x10e0009 */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaDuration:I */
/* .line 113 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 114 */
/* const v2, 0x10e0010 */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortAlphaDuration:I */
/* .line 115 */
/* iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I */
/* if-ne v0, v1, :cond_1 */
/* .line 116 */
/* iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDuration:I */
} // :cond_1
/* iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I */
} // :goto_1
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mFirstPhaseDuration:I */
/* .line 117 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 118 */
/* const v1, 0x10500b1 */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleFactor:F */
/* .line 119 */
/* iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I */
/* iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongDuration:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I */
/* .line 120 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 121 */
/* const v1, 0x1110158 */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mEnableRotationAnimation:Z */
/* .line 122 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
final String v1 = "com.youku.phone"; // const-string v1, "com.youku.phone"
final String v2 = "com.miui.mediaviewer"; // const-string v2, "com.miui.mediaviewer"
final String v3 = "com.miui.video"; // const-string v3, "com.miui.video"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 123 */
/* filled-new-array {v3, v2, v1}, [Ljava/lang/String; */
/* .line 128 */
} // :cond_2
/* const-string/jumbo v0, "tv.danmaku.bili" */
/* filled-new-array {v3, v2, v0, v1}, [Ljava/lang/String; */
/* .line 134 */
} // :goto_2
return;
} // .end method
public Boolean isOpaqueHasVideo ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 1 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 719 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void kill ( android.view.SurfaceControl$Transaction p0 ) {
/* .locals 2 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .line 336 */
v0 = this.mSurfaceControlCoverLt;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 337 */
v0 = (( android.view.SurfaceControl ) v0 ).isValid ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 338 */
v0 = this.mSurfaceControlCoverLt;
(( android.view.SurfaceControl$Transaction ) p1 ).remove ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 340 */
} // :cond_0
this.mSurfaceControlCoverLt = v1;
/* .line 342 */
} // :cond_1
v0 = this.mSurfaceControlCoverTp;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 343 */
v0 = (( android.view.SurfaceControl ) v0 ).isValid ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 344 */
v0 = this.mSurfaceControlCoverTp;
(( android.view.SurfaceControl$Transaction ) p1 ).remove ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 346 */
} // :cond_2
this.mSurfaceControlCoverTp = v1;
/* .line 348 */
} // :cond_3
v0 = this.mSurfaceControlCoverRt;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 349 */
v0 = (( android.view.SurfaceControl ) v0 ).isValid ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 350 */
v0 = this.mSurfaceControlCoverRt;
(( android.view.SurfaceControl$Transaction ) p1 ).remove ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 352 */
} // :cond_4
this.mSurfaceControlCoverRt = v1;
/* .line 354 */
} // :cond_5
v0 = this.mSurfaceControlCoverBt;
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 355 */
v0 = (( android.view.SurfaceControl ) v0 ).isValid ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 356 */
v0 = this.mSurfaceControlCoverBt;
(( android.view.SurfaceControl$Transaction ) p1 ).remove ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 358 */
} // :cond_6
this.mSurfaceControlCoverBt = v1;
/* .line 360 */
} // :cond_7
return;
} // .end method
public android.view.animation.Animation loadEnterAnimation ( Integer p0, Integer p1, Integer p2, Integer p3, android.view.SurfaceControl p4, android.view.SurfaceControl$Transaction p5 ) {
/* .locals 1 */
/* .param p1, "width" # I */
/* .param p2, "height" # I */
/* .param p3, "originRotation" # I */
/* .param p4, "curRotation" # I */
/* .param p5, "surfaceControlBg" # Landroid/view/SurfaceControl; */
/* .param p6, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .line 150 */
(( android.view.SurfaceControl$Transaction ) p6 ).hide ( p5 ); // invoke-virtual {p6, p5}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 151 */
(( com.android.server.wm.ScreenRotationAnimationImpl ) p0 ).createRotationEnterAnimation ( p1, p2, p3, p4 ); // invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/server/wm/ScreenRotationAnimationImpl;->createRotationEnterAnimation(IIII)Landroid/view/animation/Animation;
} // .end method
public android.view.animation.Animation loadExitAnimation ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "width" # I */
/* .param p2, "height" # I */
/* .param p3, "originRotation" # I */
/* .param p4, "curRotation" # I */
/* .line 145 */
(( com.android.server.wm.ScreenRotationAnimationImpl ) p0 ).createRotationExitAnimation ( p1, p2, p3, p4 ); // invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/server/wm/ScreenRotationAnimationImpl;->createRotationExitAnimation(IIII)Landroid/view/animation/Animation;
} // .end method
public android.view.animation.Animation loadRotation180Enter ( ) {
/* .locals 1 */
/* .line 159 */
(( com.android.server.wm.ScreenRotationAnimationImpl ) p0 ).createRotation180Enter ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl;->createRotation180Enter()Landroid/view/animation/Animation;
} // .end method
public android.view.animation.Animation loadRotation180Exit ( ) {
/* .locals 1 */
/* .line 155 */
(( com.android.server.wm.ScreenRotationAnimationImpl ) p0 ).createRotation180Exit ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl;->createRotation180Exit()Landroid/view/animation/Animation;
} // .end method
