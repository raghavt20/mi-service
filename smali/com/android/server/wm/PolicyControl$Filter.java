class com.android.server.wm.PolicyControl$Filter {
	 /* .source "PolicyControl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/PolicyControl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Filter" */
} // .end annotation
/* # static fields */
private static final java.lang.String ALL;
private static final java.lang.String APPS;
/* # instance fields */
private final android.util.ArraySet mBlacklist;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArraySet mWhitelist;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
private com.android.server.wm.PolicyControl$Filter ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 199 */
/* .local p1, "whitelist":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
/* .local p2, "blacklist":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 200 */
this.mWhitelist = p1;
/* .line 201 */
this.mBlacklist = p2;
/* .line 202 */
return;
} // .end method
private void dump ( java.lang.String p0, android.util.ArraySet p1, java.io.PrintWriter p2 ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/io/PrintWriter;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 233 */
/* .local p2, "set":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
(( java.io.PrintWriter ) p3 ).print ( p1 ); // invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v0 = "=("; // const-string v0, "=("
(( java.io.PrintWriter ) p3 ).print ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 234 */
v0 = (( android.util.ArraySet ) p2 ).size ( ); // invoke-virtual {p2}, Landroid/util/ArraySet;->size()I
/* .line 235 */
/* .local v0, "n":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 236 */
/* if-lez v1, :cond_0 */
/* const/16 v2, 0x2c */
(( java.io.PrintWriter ) p3 ).print ( v2 ); // invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(C)V
/* .line 237 */
} // :cond_0
(( android.util.ArraySet ) p2 ).valueAt ( v1 ); // invoke-virtual {p2, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
(( java.io.PrintWriter ) p3 ).print ( v2 ); // invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 235 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 239 */
} // .end local v1 # "i":I
} // :cond_1
/* const/16 v1, 0x29 */
(( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(C)V
/* .line 240 */
return;
} // .end method
private Boolean onBlacklist ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 219 */
v0 = this.mBlacklist;
v0 = (( android.util.ArraySet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = this.mBlacklist;
final String v1 = "*"; // const-string v1, "*"
v0 = (( android.util.ArraySet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean onWhitelist ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 223 */
v0 = this.mWhitelist;
final String v1 = "*"; // const-string v1, "*"
v0 = (( android.util.ArraySet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = this.mWhitelist;
v0 = (( android.util.ArraySet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
static com.android.server.wm.PolicyControl$Filter parse ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p0, "value" # Ljava/lang/String; */
/* .line 252 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 253 */
} // :cond_0
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 254 */
/* .local v0, "whitelist":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
/* new-instance v1, Landroid/util/ArraySet; */
/* invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V */
/* .line 255 */
/* .local v1, "blacklist":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) p0 ).split ( v2 ); // invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* array-length v3, v2 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_2 */
/* aget-object v5, v2, v4 */
/* .line 256 */
/* .local v5, "token":Ljava/lang/String; */
(( java.lang.String ) v5 ).trim ( ); // invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 257 */
final String v6 = "-"; // const-string v6, "-"
v6 = (( java.lang.String ) v5 ).startsWith ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
v6 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
int v7 = 1; // const/4 v7, 0x1
/* if-le v6, v7, :cond_1 */
/* .line 258 */
(( java.lang.String ) v5 ).substring ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 259 */
(( android.util.ArraySet ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 261 */
} // :cond_1
(( android.util.ArraySet ) v0 ).add ( v5 ); // invoke-virtual {v0, v5}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 255 */
} // .end local v5 # "token":Ljava/lang/String;
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 264 */
} // :cond_2
/* new-instance v2, Lcom/android/server/wm/PolicyControl$Filter; */
/* invoke-direct {v2, v0, v1}, Lcom/android/server/wm/PolicyControl$Filter;-><init>(Landroid/util/ArraySet;Landroid/util/ArraySet;)V */
} // .end method
/* # virtual methods */
void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 227 */
final String v0 = "Filter["; // const-string v0, "Filter["
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 228 */
/* const-string/jumbo v0, "whitelist" */
v1 = this.mWhitelist;
/* invoke-direct {p0, v0, v1, p1}, Lcom/android/server/wm/PolicyControl$Filter;->dump(Ljava/lang/String;Landroid/util/ArraySet;Ljava/io/PrintWriter;)V */
/* const/16 v0, 0x2c */
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V
/* .line 229 */
final String v0 = "blacklist"; // const-string v0, "blacklist"
v1 = this.mBlacklist;
/* invoke-direct {p0, v0, v1, p1}, Lcom/android/server/wm/PolicyControl$Filter;->dump(Ljava/lang/String;Landroid/util/ArraySet;Ljava/io/PrintWriter;)V */
/* const/16 v0, 0x5d */
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V
/* .line 230 */
return;
} // .end method
Boolean matches ( android.view.WindowManager$LayoutParams p0 ) {
/* .locals 5 */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 205 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 206 */
} // :cond_0
/* iget v1, p1, Landroid/view/WindowManager$LayoutParams;->type:I */
int v2 = 1; // const/4 v2, 0x1
/* if-lt v1, v2, :cond_1 */
/* iget v1, p1, Landroid/view/WindowManager$LayoutParams;->type:I */
/* const/16 v3, 0x63 */
/* if-gt v1, v3, :cond_1 */
/* move v1, v2 */
} // :cond_1
/* move v1, v0 */
/* .line 208 */
/* .local v1, "isApp":Z */
} // :goto_0
final String v3 = "apps"; // const-string v3, "apps"
if ( v1 != null) { // if-eqz v1, :cond_2
v4 = this.mBlacklist;
v4 = (( android.util.ArraySet ) v4 ).contains ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 209 */
} // :cond_2
v4 = this.packageName;
v4 = /* invoke-direct {p0, v4}, Lcom/android/server/wm/PolicyControl$Filter;->onBlacklist(Ljava/lang/String;)Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 210 */
} // :cond_3
if ( v1 != null) { // if-eqz v1, :cond_4
v0 = this.mWhitelist;
v0 = (( android.util.ArraySet ) v0 ).contains ( v3 ); // invoke-virtual {v0, v3}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 211 */
} // :cond_4
v0 = this.packageName;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/PolicyControl$Filter;->onWhitelist(Ljava/lang/String;)Z */
} // .end method
Boolean matches ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 215 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/PolicyControl$Filter;->onBlacklist(Ljava/lang/String;)Z */
/* if-nez v0, :cond_0 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/PolicyControl$Filter;->onWhitelist(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 244 */
/* new-instance v0, Ljava/io/StringWriter; */
/* invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V */
/* .line 245 */
/* .local v0, "sw":Ljava/io/StringWriter; */
/* new-instance v1, Ljava/io/PrintWriter; */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {v1, v0, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;Z)V */
(( com.android.server.wm.PolicyControl$Filter ) p0 ).dump ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/PolicyControl$Filter;->dump(Ljava/io/PrintWriter;)V
/* .line 246 */
(( java.io.StringWriter ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
} // .end method
