.class Lcom/android/server/wm/PolicyImpl$1;
.super Landroid/database/ContentObserver;
.source "PolicyImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/PolicyImpl;->registerDataObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/PolicyImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/PolicyImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/PolicyImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 123
    iput-object p1, p0, Lcom/android/server/wm/PolicyImpl$1;->this$0:Lcom/android/server/wm/PolicyImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .line 126
    const-string v0, "PolicyImpl"

    const-string v1, "AppContinuity policyDataMapFromCloud onChange--"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/android/server/wm/PolicyImpl$1;->this$0:Lcom/android/server/wm/PolicyImpl;

    invoke-virtual {v0}, Lcom/android/server/wm/PolicyImpl;->updateDataMapFromCloud()V

    .line 128
    return-void
.end method
