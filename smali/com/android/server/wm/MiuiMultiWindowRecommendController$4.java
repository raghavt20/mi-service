class com.android.server.wm.MiuiMultiWindowRecommendController$4 implements java.lang.Runnable {
	 /* .source "MiuiMultiWindowRecommendController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendController this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendController$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendController; */
/* .line 148 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 152 */
final String v0 = "MiuiMultiWindowRecommendController"; // const-string v0, "MiuiMultiWindowRecommendController"
try { // :try_start_0
	 v1 = this.this$0;
	 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 153 */
		 final String v1 = "dismissSplitScreenRecommendViewRunnable "; // const-string v1, "dismissSplitScreenRecommendViewRunnable "
		 android.util.Slog .d ( v0,v1 );
		 /* .line 154 */
		 v1 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v1 );
		 int v3 = 0; // const/4 v3, 0x0
		 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v1 ).startMultiWindowRecommendAnimation ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->startMultiWindowRecommendAnimation(Landroid/view/View;Z)V
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 159 */
	 } // :cond_0
	 /* .line 156 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 157 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 final String v2 = " dismissSplitScreenRecommendViewRunnable error: "; // const-string v2, " dismissSplitScreenRecommendViewRunnable error: "
	 android.util.Slog .d ( v0,v2,v1 );
	 /* .line 158 */
	 (( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
	 /* .line 160 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
