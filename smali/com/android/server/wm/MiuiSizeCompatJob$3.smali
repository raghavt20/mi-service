.class Lcom/android/server/wm/MiuiSizeCompatJob$3;
.super Ljava/lang/Object;
.source "MiuiSizeCompatJob.java"

# interfaces
.implements Ljava/util/function/BiConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiSizeCompatJob;->trackEvent(Landroid/app/job/JobParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/function/BiConsumer<",
        "Ljava/lang/String;",
        "Landroid/sizecompat/AspectRatioInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

.field final synthetic val$array:Lorg/json/JSONArray;

.field final synthetic val$embeddedApps:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiSizeCompatJob;Ljava/util/Map;Lorg/json/JSONArray;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiSizeCompatJob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 205
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$3;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    iput-object p2, p0, Lcom/android/server/wm/MiuiSizeCompatJob$3;->val$embeddedApps:Ljava/util/Map;

    iput-object p3, p0, Lcom/android/server/wm/MiuiSizeCompatJob$3;->val$array:Lorg/json/JSONArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 205
    check-cast p1, Ljava/lang/String;

    check-cast p2, Landroid/sizecompat/AspectRatioInfo;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/MiuiSizeCompatJob$3;->accept(Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;)V

    return-void
.end method

.method public accept(Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "aspectRatioInfo"    # Landroid/sizecompat/AspectRatioInfo;

    .line 208
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 210
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "app_package_name"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 211
    const-string v1, "app_display_name"

    iget-object v2, p2, Landroid/sizecompat/AspectRatioInfo;->mApplicationName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 212
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$3;->val$embeddedApps:Ljava/util/Map;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "app_display_style"

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p2, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-nez v1, :cond_0

    .line 214
    const-string/jumbo v1, "\u5e73\u884c\u7a97\u53e3"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 216
    :cond_0
    invoke-virtual {p2}, Landroid/sizecompat/AspectRatioInfo;->getRatioStr()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 218
    :goto_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$3;->val$array:Lorg/json/JSONArray;

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 221
    goto :goto_1

    .line 219
    :catch_0
    move-exception v1

    .line 220
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 222
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_1
    return-void
.end method
