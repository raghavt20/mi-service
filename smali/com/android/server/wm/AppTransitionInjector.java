public class com.android.server.wm.AppTransitionInjector {
	 /* .source "AppTransitionInjector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;, */
	 /* Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;, */
	 /* Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;, */
	 /* Lcom/android/server/wm/AppTransitionInjector$BezierYAnimation;, */
	 /* Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;, */
	 /* Lcom/android/server/wm/AppTransitionInjector$CubicEaseOutInterpolator;, */
	 /* Lcom/android/server/wm/AppTransitionInjector$QuartEaseOutInterpolator;, */
	 /* Lcom/android/server/wm/AppTransitionInjector$QuintEaseOutInterpolator;, */
	 /* Lcom/android/server/wm/AppTransitionInjector$ScaleWallPaperAnimation;, */
	 /* Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation; */
	 /* } */
} // .end annotation
/* # static fields */
static final Integer APP_TRANSITION_SPECS_PENDING_TIMEOUT;
static final Integer BEZIER_ALLPAPER_OPEN_DURATION;
private static final java.util.ArrayList BLACK_LIST_NOT_ALLOWED_SNAPSHOT;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.HashSet BLACK_LIST_NOT_ALLOWED_SNAPSHOT_COMPONENT;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final android.view.animation.Interpolator CUBIC_EASE_OUT_INTERPOLATOR;
private static final Integer DEFAULT_ACTIVITY_SCALE_DOWN_ALPHA_DELAY;
private static final Integer DEFAULT_ACTIVITY_SCALE_DOWN_ALPHA_DURATION;
private static final Integer DEFAULT_ACTIVITY_SCALE_DOWN_DURATION;
private static final Integer DEFAULT_ACTIVITY_SCALE_UP_ALPHA_DURATION;
private static final Integer DEFAULT_ACTIVITY_SCALE_UP_DURATION;
private static final Integer DEFAULT_ACTIVITY_TRANSITION_DURATION;
private static final Integer DEFAULT_ALPHA_DURATION;
private static final Integer DEFAULT_ALPHA_OFFSET;
private static final Integer DEFAULT_ANIMATION_DURATION;
static final Integer DEFAULT_APP_TRANSITION_ROUND_CORNER_RADIUS;
private static final Float DEFAULT_BACK_TO_SCREEN_CENTER_SCALE;
private static final Float DEFAULT_ENTER_ACTIVITY_END_ALPHA;
private static final Float DEFAULT_ENTER_ACTIVITY_START_ALPHA;
private static final Float DEFAULT_EXIT_ACTIVITY_END_ALPHA;
private static final Float DEFAULT_EXIT_ACTIVITY_START_ALPHA;
private static final Integer DEFAULT_LAUNCH_FORM_HOME_DURATION;
private static final Float DEFAULT_REENTER_ACTIVITY_END_ALPHA;
private static final Float DEFAULT_REENTER_ACTIVITY_START_ALPHA;
private static final Float DEFAULT_RETURN_ACTIVITY_END_ALPHA;
private static final Float DEFAULT_RETURN_ACTIVITY_START_ALPHA;
private static final Integer DEFAULT_TASK_TRANSITION_DURATION;
private static final Float DEFAULT_WALLPAPER_EXIT_SCALE_X;
private static final Float DEFAULT_WALLPAPER_EXIT_SCALE_Y;
static final Integer DEFAULT_WALLPAPER_OPEN_DURATION;
private static final Integer DEFAULT_WALLPAPER_TRANSITION_DURATION;
private static final Integer DEFAULT_WALLPAPER_TRANSITION_R_CTS_DURATION;
static Integer DISPLAY_ROUND_CORNER_RADIUS;
private static final java.util.ArrayList IGNORE_LAUNCHED_FROM_SYSTEM_SURFACE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
static final Boolean IS_E10;
private static final Float LAUNCHER_DEFAULT_ALPHA;
private static final Float LAUNCHER_DEFAULT_SCALE;
private static final Float LAUNCHER_TRANSITION_ALPHA;
private static final Float LAUNCHER_TRANSITION_SCALE;
static final Integer NEXT_TRANSIT_TYPE_BACK_HOME;
static final Integer NEXT_TRANSIT_TYPE_BACK_WITH_SCALED_THUMB;
static final Integer NEXT_TRANSIT_TYPE_LAUNCH_BACK_ROUNDED_VIEW;
static final Integer NEXT_TRANSIT_TYPE_LAUNCH_FROM_HOME;
static final Integer NEXT_TRANSIT_TYPE_LAUNCH_FROM_ROUNDED_VIEW;
static final Integer NEXT_TRANSIT_TYPE_LAUNCH_WITH_SCALED_THUMB;
static final Integer PENDING_EXECUTE_APP_TRANSITION_TIMEOUT;
private static final android.view.animation.Interpolator QUART_EASE_OUT_INTERPOLATOR;
private static final android.view.animation.Interpolator QUINT_EASE_OUT_INTERPOLATOR;
private static final android.view.animation.Interpolator SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR;
private static final android.view.animation.Interpolator SCALE_UP_PHYSIC_BASED_INTERPOLATOR;
private static final java.lang.String TAG;
static final Integer THUMBNAIL_ANIMATION_TIMEOUT_DURATION;
private static final java.util.ArrayList WHITE_LIST_ALLOW_CUSTOM_ANIMATION;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
static final java.util.ArrayList WHITE_LIST_ALLOW_CUSTOM_APPLICATION_TRANSITION;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static android.view.animation.Interpolator sActivityTransitionInterpolator;
private static android.graphics.Rect sMiuiAnimSupportInset;
/* # direct methods */
static void -$$Nest$smdoAnimationCallback ( android.os.IRemoteCallback p0 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.wm.AppTransitionInjector .doAnimationCallback ( p0 );
return;
} // .end method
static com.android.server.wm.AppTransitionInjector ( ) {
/* .locals 4 */
/* .line 75 */
/* const/16 v0, 0x3c */
/* .line 76 */
final String v0 = "beryllium"; // const-string v0, "beryllium"
v1 = android.os.Build.PRODUCT;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.android.server.wm.AppTransitionInjector.IS_E10 = (v0!= 0);
/* .line 81 */
final String v0 = "cactus"; // const-string v0, "cactus"
v1 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x320 */
} // :cond_0
/* const/16 v0, 0x64 */
} // :goto_0
/* .line 114 */
/* new-instance v0, Lcom/android/server/wm/AppTransitionInjector$CubicEaseOutInterpolator; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/android/server/wm/AppTransitionInjector$CubicEaseOutInterpolator;-><init>(Lcom/android/server/wm/AppTransitionInjector$CubicEaseOutInterpolator-IA;)V */
/* .line 115 */
/* new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator; */
/* const v2, 0x3f4ccccd # 0.8f */
/* const v3, 0x3f733333 # 0.95f */
/* invoke-direct {v0, v3, v2}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V */
/* .line 117 */
/* new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator; */
/* const v2, 0x3f47ae14 # 0.78f */
/* invoke-direct {v0, v3, v2}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V */
/* .line 119 */
/* new-instance v0, Lcom/android/server/wm/AppTransitionInjector$QuartEaseOutInterpolator; */
/* invoke-direct {v0, v1}, Lcom/android/server/wm/AppTransitionInjector$QuartEaseOutInterpolator;-><init>(Lcom/android/server/wm/AppTransitionInjector$QuartEaseOutInterpolator-IA;)V */
/* .line 120 */
/* new-instance v0, Lcom/android/server/wm/AppTransitionInjector$QuintEaseOutInterpolator; */
/* invoke-direct {v0, v1}, Lcom/android/server/wm/AppTransitionInjector$QuintEaseOutInterpolator;-><init>(Lcom/android/server/wm/AppTransitionInjector$QuintEaseOutInterpolator-IA;)V */
/* .line 121 */
/* .line 123 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V */
/* .line 125 */
/* new-instance v0, Lcom/android/server/wm/AppTransitionInjector$1; */
/* invoke-direct {v0}, Lcom/android/server/wm/AppTransitionInjector$1;-><init>()V */
/* .line 134 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 138 */
final String v1 = "com.tencent.mm/.plugin.offline.ui.WalletOfflineCoinPurseUI"; // const-string v1, "com.tencent.mm/.plugin.offline.ui.WalletOfflineCoinPurseUI"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 142 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 145 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 149 */
final String v2 = "com.android.camera"; // const-string v2, "com.android.camera"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 150 */
final String v3 = "com.android.browser"; // const-string v3, "com.android.browser"
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 151 */
final String v3 = "com.miui.securitycenter"; // const-string v3, "com.miui.securitycenter"
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 152 */
final String v3 = "com.mi.globalbrowser"; // const-string v3, "com.mi.globalbrowser"
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 153 */
final String v3 = "com.miui.home"; // const-string v3, "com.miui.home"
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 154 */
final String v3 = "com.mi.android.globallauncher"; // const-string v3, "com.mi.android.globallauncher"
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 155 */
final String v3 = "com.android.contacts"; // const-string v3, "com.android.contacts"
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 156 */
final String v3 = "com.android.quicksearchbox"; // const-string v3, "com.android.quicksearchbox"
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 157 */
final String v0 = "com.mfashiongallery.emag"; // const-string v0, "com.mfashiongallery.emag"
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 158 */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 414 */
/* new-instance v0, Lcom/android/server/wm/AppTransitionInjector$3; */
/* invoke-direct {v0}, Lcom/android/server/wm/AppTransitionInjector$3;-><init>()V */
return;
} // .end method
public com.android.server.wm.AppTransitionInjector ( ) {
/* .locals 0 */
/* .line 64 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static void addAnimationListener ( android.view.animation.Animation p0, android.os.Handler p1, android.os.IRemoteCallback p2, android.os.IRemoteCallback p3 ) {
/* .locals 3 */
/* .param p0, "a" # Landroid/view/animation/Animation; */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "mAnimationStartCallback" # Landroid/os/IRemoteCallback; */
/* .param p3, "mAnimationFinishCallback" # Landroid/os/IRemoteCallback; */
/* .line 383 */
/* move-object v0, p2 */
/* .line 384 */
/* .local v0, "exitStartCallback":Landroid/os/IRemoteCallback; */
/* move-object v1, p3 */
/* .line 385 */
/* .local v1, "exitFinishCallback":Landroid/os/IRemoteCallback; */
/* new-instance v2, Lcom/android/server/wm/AppTransitionInjector$2; */
/* invoke-direct {v2, v0, p1, v1}, Lcom/android/server/wm/AppTransitionInjector$2;-><init>(Landroid/os/IRemoteCallback;Landroid/os/Handler;Landroid/os/IRemoteCallback;)V */
(( android.view.animation.Animation ) p0 ).setAnimationListener ( v2 ); // invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
/* .line 405 */
return;
} // .end method
static Boolean allowCustomAnimation ( android.util.ArraySet p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/ArraySet<", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 1082 */
/* .local p0, "closingApps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/ActivityRecord;>;" */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 1083 */
/* .line 1086 */
} // :cond_0
v1 = (( android.util.ArraySet ) p0 ).size ( ); // invoke-virtual {p0}, Landroid/util/ArraySet;->size()I
/* .line 1087 */
/* .local v1, "size":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_2 */
/* .line 1088 */
(( android.util.ArraySet ) p0 ).valueAt ( v2 ); // invoke-virtual {p0, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/ActivityRecord; */
/* .line 1089 */
/* .local v3, "atoken":Lcom/android/server/wm/ActivityRecord; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1090 */
(( com.android.server.wm.ActivityRecord ) v3 ).findMainWindow ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->findMainWindow()Lcom/android/server/wm/WindowState;
/* .line 1091 */
/* .local v4, "win":Lcom/android/server/wm/WindowState; */
if ( v4 != null) { // if-eqz v4, :cond_1
v5 = com.android.server.wm.AppTransitionInjector.WHITE_LIST_ALLOW_CUSTOM_ANIMATION;
v6 = this.mAttrs;
v6 = this.packageName;
/* .line 1092 */
v5 = (( java.util.ArrayList ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 1093 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1087 */
} // .end local v3 # "atoken":Lcom/android/server/wm/ActivityRecord;
} // .end local v4 # "win":Lcom/android/server/wm/WindowState;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1097 */
} // .end local v2 # "i":I
} // :cond_2
} // .end method
static void calculateGestureThumbnailSpec ( android.graphics.Rect p0, android.graphics.Rect p1, android.graphics.Matrix p2, Float p3, android.view.SurfaceControl$Transaction p4, android.view.SurfaceControl p5 ) {
/* .locals 20 */
/* .param p0, "appRect" # Landroid/graphics/Rect; */
/* .param p1, "thumbnailRect" # Landroid/graphics/Rect; */
/* .param p2, "curSpec" # Landroid/graphics/Matrix; */
/* .param p3, "alpha" # F */
/* .param p4, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p5, "leash" # Landroid/view/SurfaceControl; */
/* .line 1039 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p2 */
/* move-object/from16 v8, p4 */
/* move-object/from16 v9, p5 */
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
if ( v1 != null) { // if-eqz v1, :cond_1
if ( v9 != null) { // if-eqz v9, :cond_1
/* if-nez v8, :cond_0 */
/* move/from16 v2, p3 */
/* goto/16 :goto_0 */
/* .line 1044 */
} // :cond_0
/* new-instance v2, Landroid/graphics/Matrix; */
/* invoke-direct {v2, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V */
/* move-object v10, v2 */
/* .line 1045 */
/* .local v10, "tmpMatrix":Landroid/graphics/Matrix; */
/* iget v2, v0, Landroid/graphics/Rect;->left:I */
/* int-to-float v2, v2 */
/* iget v3, v0, Landroid/graphics/Rect;->top:I */
/* int-to-float v3, v3 */
(( android.graphics.Matrix ) v10 ).postTranslate ( v2, v3 ); // invoke-virtual {v10, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z
/* .line 1046 */
/* const/16 v2, 0x9 */
/* new-array v11, v2, [F */
/* .line 1047 */
/* .local v11, "tmp":[F */
(( android.graphics.Matrix ) v10 ).getValues ( v11 ); // invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->getValues([F)V
/* .line 1049 */
int v2 = 0; // const/4 v2, 0x0
/* aget v12, v11, v2 */
/* .line 1050 */
/* .local v12, "curScaleX":F */
int v2 = 4; // const/4 v2, 0x4
/* aget v13, v11, v2 */
/* .line 1051 */
/* .local v13, "curScaleY":F */
v2 = /* invoke-virtual/range {p0 ..p0}, Landroid/graphics/Rect;->width()I */
/* int-to-float v2, v2 */
/* mul-float v14, v2, v12 */
/* .line 1052 */
/* .local v14, "curWidth":F */
v2 = /* invoke-virtual/range {p0 ..p0}, Landroid/graphics/Rect;->height()I */
/* int-to-float v2, v2 */
/* mul-float v15, v2, v13 */
/* .line 1053 */
/* .local v15, "curHeight":F */
v2 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->width()I */
v3 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v3, v3, Landroid/graphics/Rect;->left:I */
/* sub-int/2addr v2, v3 */
v3 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v3, v3, Landroid/graphics/Rect;->right:I */
/* sub-int/2addr v2, v3 */
/* int-to-float v2, v2 */
/* div-float v16, v14, v2 */
/* .line 1055 */
/* .local v16, "newScaleX":F */
v2 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
v3 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v3, v3, Landroid/graphics/Rect;->top:I */
/* sub-int/2addr v2, v3 */
v3 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v3, v3, Landroid/graphics/Rect;->bottom:I */
/* sub-int/2addr v2, v3 */
/* int-to-float v2, v2 */
/* div-float v17, v15, v2 */
/* .line 1057 */
/* .local v17, "newScaleY":F */
v2 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
v3 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v3, v3, Landroid/graphics/Rect;->top:I */
/* sub-int/2addr v2, v3 */
v3 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v3, v3, Landroid/graphics/Rect;->bottom:I */
/* sub-int/2addr v2, v3 */
/* int-to-float v2, v2 */
/* mul-float v18, v16, v2 */
/* .line 1060 */
/* .local v18, "curThumbnailHeight":F */
int v2 = 2; // const/4 v2, 0x2
/* aget v2, v11, v2 */
v3 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v3, v3, Landroid/graphics/Rect;->left:I */
/* int-to-float v3, v3 */
/* mul-float v3, v3, v16 */
/* sub-float v7, v2, v3 */
/* .line 1062 */
/* .local v7, "curTranslateX":F */
int v2 = 5; // const/4 v2, 0x5
/* aget v2, v11, v2 */
v3 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v3, v3, Landroid/graphics/Rect;->top:I */
/* int-to-float v3, v3 */
/* mul-float v3, v3, v17 */
/* sub-float/2addr v2, v3 */
/* sub-float v3, v15, v18 */
/* const/high16 v4, 0x40000000 # 2.0f */
/* div-float/2addr v3, v4 */
/* add-float v6, v2, v3 */
/* .line 1065 */
/* .local v6, "curTranslateY":F */
int v2 = 3; // const/4 v2, 0x3
/* aget v5, v11, v2 */
int v2 = 1; // const/4 v2, 0x1
/* aget v19, v11, v2 */
/* move-object/from16 v2, p4 */
/* move-object/from16 v3, p5 */
/* move/from16 v4, v16 */
/* move v0, v6 */
} // .end local v6 # "curTranslateY":F
/* .local v0, "curTranslateY":F */
/* move/from16 v6, v19 */
/* move v1, v7 */
} // .end local v7 # "curTranslateX":F
/* .local v1, "curTranslateX":F */
/* move/from16 v7, v16 */
/* invoke-virtual/range {v2 ..v7}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction; */
/* .line 1066 */
(( android.view.SurfaceControl$Transaction ) v8 ).setPosition ( v9, v1, v0 ); // invoke-virtual {v8, v9, v1, v0}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;
/* .line 1067 */
/* move/from16 v2, p3 */
(( android.view.SurfaceControl$Transaction ) v8 ).setAlpha ( v9, v2 ); // invoke-virtual {v8, v9, v2}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 1068 */
return;
/* .line 1039 */
} // .end local v0 # "curTranslateY":F
} // .end local v1 # "curTranslateX":F
} // .end local v10 # "tmpMatrix":Landroid/graphics/Matrix;
} // .end local v11 # "tmp":[F
} // .end local v12 # "curScaleX":F
} // .end local v13 # "curScaleY":F
} // .end local v14 # "curWidth":F
} // .end local v15 # "curHeight":F
} // .end local v16 # "newScaleX":F
} // .end local v17 # "newScaleY":F
} // .end local v18 # "curThumbnailHeight":F
} // :cond_1
/* move/from16 v2, p3 */
/* .line 1041 */
} // :goto_0
return;
} // .end method
static void calculateMiuiActivityThumbnailSpec ( android.graphics.Rect p0, android.graphics.Rect p1, android.graphics.Matrix p2, Float p3, Float p4, android.view.SurfaceControl$Transaction p5, android.view.SurfaceControl p6 ) {
/* .locals 14 */
/* .param p0, "appRect" # Landroid/graphics/Rect; */
/* .param p1, "thumbnailRect" # Landroid/graphics/Rect; */
/* .param p2, "curSpec" # Landroid/graphics/Matrix; */
/* .param p3, "alpha" # F */
/* .param p4, "radius" # F */
/* .param p5, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p6, "leash" # Landroid/view/SurfaceControl; */
/* .line 1008 */
/* move-object/from16 v0, p2 */
/* move-object/from16 v1, p5 */
/* move-object/from16 v2, p6 */
if ( p0 != null) { // if-eqz p0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_1
if ( v2 != null) { // if-eqz v2, :cond_1
/* if-nez v1, :cond_0 */
/* move/from16 v6, p3 */
/* .line 1012 */
} // :cond_0
/* const/16 v3, 0x9 */
/* new-array v3, v3, [F */
/* .line 1013 */
/* .local v3, "tmp":[F */
(( android.graphics.Matrix ) v0 ).getValues ( v3 ); // invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->getValues([F)V
/* .line 1014 */
int v4 = 2; // const/4 v4, 0x2
/* aget v4, v3, v4 */
/* .line 1015 */
/* .local v4, "curTranslateX":F */
int v5 = 5; // const/4 v5, 0x5
/* aget v5, v3, v5 */
/* .line 1016 */
/* .local v5, "curTranslateY":F */
int v6 = 0; // const/4 v6, 0x0
/* aget v7, v3, v6 */
/* .line 1017 */
/* .local v7, "curScaleX":F */
int v8 = 4; // const/4 v8, 0x4
/* aget v8, v3, v8 */
/* .line 1018 */
/* .local v8, "curScaleY":F */
v9 = (( android.graphics.Rect ) p0 ).width ( ); // invoke-virtual {p0}, Landroid/graphics/Rect;->width()I
/* int-to-float v9, v9 */
/* mul-float/2addr v9, v7 */
/* .line 1019 */
/* .local v9, "curWidth":F */
v10 = (( android.graphics.Rect ) p0 ).height ( ); // invoke-virtual {p0}, Landroid/graphics/Rect;->height()I
/* int-to-float v10, v10 */
/* mul-float/2addr v10, v8 */
/* .line 1020 */
/* .local v10, "curHeight":F */
/* new-instance v11, Landroid/graphics/Rect; */
/* float-to-int v12, v9 */
/* float-to-int v13, v10 */
/* invoke-direct {v11, v6, v6, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V */
(( android.view.SurfaceControl$Transaction ) v1 ).setWindowCrop ( v2, v11 ); // invoke-virtual {v1, v2, v11}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
/* .line 1021 */
(( android.view.SurfaceControl$Transaction ) v1 ).setPosition ( v2, v4, v5 ); // invoke-virtual {v1, v2, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;
/* .line 1022 */
/* move/from16 v6, p3 */
(( android.view.SurfaceControl$Transaction ) v1 ).setAlpha ( v2, v6 ); // invoke-virtual {v1, v2, v6}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 1023 */
return;
/* .line 1008 */
} // .end local v3 # "tmp":[F
} // .end local v4 # "curTranslateX":F
} // .end local v5 # "curTranslateY":F
} // .end local v7 # "curScaleX":F
} // .end local v8 # "curScaleY":F
} // .end local v9 # "curWidth":F
} // .end local v10 # "curHeight":F
} // :cond_1
/* move/from16 v6, p3 */
/* .line 1010 */
} // :goto_0
return;
} // .end method
static void calculateMiuiThumbnailSpec ( android.graphics.Rect p0, android.graphics.Rect p1, android.graphics.Matrix p2, Float p3, android.view.SurfaceControl$Transaction p4, android.view.SurfaceControl p5 ) {
/* .locals 17 */
/* .param p0, "appRect" # Landroid/graphics/Rect; */
/* .param p1, "thumbnailRect" # Landroid/graphics/Rect; */
/* .param p2, "curSpec" # Landroid/graphics/Matrix; */
/* .param p3, "alpha" # F */
/* .param p4, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p5, "leash" # Landroid/view/SurfaceControl; */
/* .line 978 */
/* move-object/from16 v0, p2 */
/* move-object/from16 v7, p4 */
/* move-object/from16 v8, p5 */
if ( p0 != null) { // if-eqz p0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_1
if ( v8 != null) { // if-eqz v8, :cond_1
/* if-nez v7, :cond_0 */
/* move/from16 v1, p3 */
/* goto/16 :goto_0 */
/* .line 983 */
} // :cond_0
/* const/16 v1, 0x9 */
/* new-array v9, v1, [F */
/* .line 984 */
/* .local v9, "tmp":[F */
(( android.graphics.Matrix ) v0 ).getValues ( v9 ); // invoke-virtual {v0, v9}, Landroid/graphics/Matrix;->getValues([F)V
/* .line 986 */
int v1 = 0; // const/4 v1, 0x0
/* aget v10, v9, v1 */
/* .line 987 */
/* .local v10, "curScaleX":F */
int v1 = 4; // const/4 v1, 0x4
/* aget v11, v9, v1 */
/* .line 988 */
/* .local v11, "curScaleY":F */
v1 = /* invoke-virtual/range {p0 ..p0}, Landroid/graphics/Rect;->width()I */
/* int-to-float v1, v1 */
/* mul-float v12, v1, v10 */
/* .line 989 */
/* .local v12, "curWidth":F */
v1 = /* invoke-virtual/range {p0 ..p0}, Landroid/graphics/Rect;->height()I */
/* int-to-float v1, v1 */
/* mul-float v13, v1, v11 */
/* .line 990 */
/* .local v13, "curHeight":F */
v1 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->width()I */
v2 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v2, v2, Landroid/graphics/Rect;->left:I */
/* sub-int/2addr v1, v2 */
v2 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v2, v2, Landroid/graphics/Rect;->right:I */
/* sub-int/2addr v1, v2 */
/* int-to-float v1, v1 */
/* div-float v14, v12, v1 */
/* .line 992 */
/* .local v14, "newScaleX":F */
v1 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
v2 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v2, v2, Landroid/graphics/Rect;->top:I */
/* sub-int/2addr v1, v2 */
v2 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v2, v2, Landroid/graphics/Rect;->bottom:I */
/* sub-int/2addr v1, v2 */
/* int-to-float v1, v1 */
/* div-float v15, v13, v1 */
/* .line 995 */
/* .local v15, "newScaleY":F */
int v1 = 2; // const/4 v1, 0x2
/* aget v1, v9, v1 */
v2 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v2, v2, Landroid/graphics/Rect;->left:I */
/* int-to-float v2, v2 */
/* mul-float/2addr v2, v14 */
/* sub-float v6, v1, v2 */
/* .line 997 */
/* .local v6, "curTranslateX":F */
int v1 = 5; // const/4 v1, 0x5
/* aget v1, v9, v1 */
v2 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v2, v2, Landroid/graphics/Rect;->top:I */
/* int-to-float v2, v2 */
/* mul-float/2addr v2, v15 */
/* sub-float v5, v1, v2 */
/* .line 1000 */
/* .local v5, "curTranslateY":F */
int v1 = 3; // const/4 v1, 0x3
/* aget v4, v9, v1 */
int v1 = 1; // const/4 v1, 0x1
/* aget v16, v9, v1 */
/* move-object/from16 v1, p4 */
/* move-object/from16 v2, p5 */
/* move v3, v14 */
/* move v0, v5 */
} // .end local v5 # "curTranslateY":F
/* .local v0, "curTranslateY":F */
/* move/from16 v5, v16 */
/* move-object/from16 v16, v9 */
/* move v9, v6 */
} // .end local v6 # "curTranslateX":F
/* .local v9, "curTranslateX":F */
/* .local v16, "tmp":[F */
/* move v6, v15 */
/* invoke-virtual/range {v1 ..v6}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction; */
/* .line 1001 */
(( android.view.SurfaceControl$Transaction ) v7 ).setPosition ( v8, v9, v0 ); // invoke-virtual {v7, v8, v9, v0}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;
/* .line 1002 */
/* move/from16 v1, p3 */
(( android.view.SurfaceControl$Transaction ) v7 ).setAlpha ( v8, v1 ); // invoke-virtual {v7, v8, v1}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 1003 */
return;
/* .line 978 */
} // .end local v0 # "curTranslateY":F
} // .end local v9 # "curTranslateX":F
} // .end local v10 # "curScaleX":F
} // .end local v11 # "curScaleY":F
} // .end local v12 # "curWidth":F
} // .end local v13 # "curHeight":F
} // .end local v14 # "newScaleX":F
} // .end local v15 # "newScaleY":F
} // .end local v16 # "tmp":[F
} // :cond_1
/* move/from16 v1, p3 */
/* .line 980 */
} // :goto_0
return;
} // .end method
static void calculateScaleUpDownThumbnailSpec ( android.graphics.Rect p0, android.graphics.Rect p1, android.graphics.Matrix p2, android.view.SurfaceControl$Transaction p3, android.view.SurfaceControl p4 ) {
/* .locals 2 */
/* .param p0, "appClipRect" # Landroid/graphics/Rect; */
/* .param p1, "thumbnailRect" # Landroid/graphics/Rect; */
/* .param p2, "curSpec" # Landroid/graphics/Matrix; */
/* .param p3, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p4, "leash" # Landroid/view/SurfaceControl; */
/* .line 1027 */
if ( p0 != null) { // if-eqz p0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
if ( p2 != null) { // if-eqz p2, :cond_1
if ( p4 != null) { // if-eqz p4, :cond_1
/* if-nez p3, :cond_0 */
/* .line 1031 */
} // :cond_0
/* const/16 v0, 0x9 */
/* new-array v0, v0, [F */
/* .line 1032 */
/* .local v0, "tmp":[F */
(( android.view.SurfaceControl$Transaction ) p3 ).setMatrix ( p4, p2, v0 ); // invoke-virtual {p3, p4, p2, v0}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;Landroid/graphics/Matrix;[F)Landroid/view/SurfaceControl$Transaction;
/* .line 1033 */
(( android.view.SurfaceControl$Transaction ) p3 ).setWindowCrop ( p4, p0 ); // invoke-virtual {p3, p4, p0}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
/* .line 1034 */
/* const/high16 v1, 0x3f800000 # 1.0f */
(( android.view.SurfaceControl$Transaction ) p3 ).setAlpha ( p4, v1 ); // invoke-virtual {p3, p4, v1}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 1035 */
return;
/* .line 1029 */
} // .end local v0 # "tmp":[F
} // :cond_1
} // :goto_0
return;
} // .end method
static android.view.animation.Animation createActivityOpenCloseTransition ( Boolean p0, android.graphics.Rect p1, Boolean p2, Float p3, com.android.server.wm.WindowContainer p4 ) {
/* .locals 30 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "isOpenOrClose" # Z */
/* .param p3, "freeformScale" # F */
/* .param p4, "container" # Lcom/android/server/wm/WindowContainer; */
/* .line 780 */
/* move/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move/from16 v2, p2 */
/* move-object/from16 v12, p4 */
/* new-instance v3, Landroid/view/animation/AnimationSet; */
int v13 = 1; // const/4 v13, 0x1
/* invoke-direct {v3, v13}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* move-object v14, v3 */
/* .line 781 */
/* .local v14, "set":Landroid/view/animation/AnimationSet; */
int v15 = 0; // const/4 v15, 0x0
/* .line 782 */
/* .local v15, "translateAnimation":Landroid/view/animation/Animation; */
/* const/16 v16, 0x0 */
/* .line 783 */
/* .local v16, "alphaAnimation":Landroid/view/animation/Animation; */
/* const/16 v17, 0x0 */
/* .line 784 */
/* .local v17, "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
int v11 = 0; // const/4 v11, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const/high16 v4, 0x3f000000 # 0.5f */
/* const/high16 v5, 0x3f800000 # 1.0f */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 785 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 786 */
/* new-instance v18, Landroid/view/animation/TranslateAnimation; */
int v4 = 1; // const/4 v4, 0x1
int v6 = 1; // const/4 v6, 0x1
int v7 = 0; // const/4 v7, 0x0
int v8 = 1; // const/4 v8, 0x1
int v9 = 0; // const/4 v9, 0x0
int v10 = 1; // const/4 v10, 0x1
/* const/16 v19, 0x0 */
/* move-object/from16 v3, v18 */
/* move/from16 v5, p3 */
/* move/from16 v11, v19 */
/* invoke-direct/range {v3 ..v11}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* .line 790 */
} // .end local v15 # "translateAnimation":Landroid/view/animation/Animation;
/* .local v3, "translateAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v14 ).setZAdjustment ( v13 ); // invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* goto/16 :goto_0 */
/* .line 794 */
} // .end local v3 # "translateAnimation":Landroid/view/animation/Animation;
/* .restart local v15 # "translateAnimation":Landroid/view/animation/Animation; */
} // :cond_0
/* new-instance v6, Landroid/view/animation/TranslateAnimation; */
/* const/16 v22, 0x1 */
/* const/16 v23, 0x0 */
/* const/16 v24, 0x1 */
/* const/high16 v25, -0x41800000 # -0.25f */
/* const/16 v26, 0x1 */
/* const/16 v27, 0x0 */
/* const/16 v28, 0x1 */
/* const/16 v29, 0x0 */
/* move-object/from16 v21, v6 */
/* invoke-direct/range {v21 ..v29}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* .line 798 */
} // .end local v15 # "translateAnimation":Landroid/view/animation/Animation;
/* .local v6, "translateAnimation":Landroid/view/animation/Animation; */
/* cmpg-float v7, p3, v5 */
/* if-gez v7, :cond_1 */
/* .line 799 */
/* new-instance v3, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v3, v5, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 800 */
} // .end local v16 # "alphaAnimation":Landroid/view/animation/Animation;
/* .local v3, "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v14 ).addAnimation ( v3 ); // invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* move-object/from16 v16, v3 */
/* move-object v3, v6 */
/* goto/16 :goto_0 */
/* .line 801 */
} // .end local v3 # "alphaAnimation":Landroid/view/animation/Animation;
/* .restart local v16 # "alphaAnimation":Landroid/view/animation/Animation; */
} // :cond_1
/* if-nez v12, :cond_2 */
/* .line 802 */
/* new-instance v3, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 803 */
} // .end local v16 # "alphaAnimation":Landroid/view/animation/Animation;
/* .restart local v3 # "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v14 ).addAnimation ( v3 ); // invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* move-object/from16 v16, v3 */
/* move-object v3, v6 */
/* goto/16 :goto_0 */
/* .line 804 */
} // .end local v3 # "alphaAnimation":Landroid/view/animation/Animation;
/* .restart local v16 # "alphaAnimation":Landroid/view/animation/Animation; */
} // :cond_2
/* iget-boolean v5, v12, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 805 */
/* new-instance v5, Lcom/android/server/wm/AnimationDimmer; */
/* invoke-direct {v5, v3, v4, v1, v12}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V */
/* move-object v3, v5 */
/* .line 806 */
} // .end local v17 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
/* .local v3, "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
(( android.view.animation.AnimationSet ) v14 ).addAnimation ( v3 ); // invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 807 */
(( android.view.animation.AnimationSet ) v14 ).setTaskAnimationLevel ( v13 ); // invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setTaskAnimationLevel(Z)V
/* .line 808 */
int v11 = 0; // const/4 v11, 0x0
/* iput-boolean v11, v12, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z */
/* move-object/from16 v17, v3 */
/* move-object v3, v6 */
/* goto/16 :goto_0 */
/* .line 810 */
} // .end local v3 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
/* .restart local v17 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
} // :cond_3
int v11 = 0; // const/4 v11, 0x0
v5 = /* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/WindowContainer;->fillsParent()Z */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 811 */
/* new-instance v5, Lcom/android/server/wm/AnimationDimmer; */
/* invoke-direct {v5, v3, v4, v1, v12}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V */
/* move-object v3, v5 */
/* .line 812 */
} // .end local v17 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
/* .restart local v3 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
(( android.view.animation.AnimationSet ) v14 ).addAnimation ( v3 ); // invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* move-object/from16 v17, v3 */
/* move-object v3, v6 */
/* goto/16 :goto_0 */
/* .line 810 */
} // .end local v3 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
/* .restart local v17 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
} // :cond_4
/* move-object v3, v6 */
/* goto/16 :goto_0 */
/* .line 817 */
} // .end local v6 # "translateAnimation":Landroid/view/animation/Animation;
/* .restart local v15 # "translateAnimation":Landroid/view/animation/Animation; */
} // :cond_5
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 818 */
/* new-instance v6, Landroid/view/animation/TranslateAnimation; */
/* const/16 v19, 0x1 */
/* const/high16 v20, -0x41800000 # -0.25f */
/* const/16 v21, 0x1 */
/* const/16 v22, 0x0 */
/* const/16 v23, 0x1 */
/* const/16 v24, 0x0 */
/* const/16 v25, 0x1 */
/* const/16 v26, 0x0 */
/* move-object/from16 v18, v6 */
/* invoke-direct/range {v18 ..v26}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* .line 822 */
} // .end local v15 # "translateAnimation":Landroid/view/animation/Animation;
/* .restart local v6 # "translateAnimation":Landroid/view/animation/Animation; */
/* cmpg-float v7, p3, v5 */
/* if-gez v7, :cond_6 */
/* .line 823 */
/* new-instance v3, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v3, v5, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 824 */
} // .end local v16 # "alphaAnimation":Landroid/view/animation/Animation;
/* .local v3, "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v14 ).addAnimation ( v3 ); // invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* move-object/from16 v16, v3 */
/* move-object v3, v6 */
/* .line 825 */
} // .end local v3 # "alphaAnimation":Landroid/view/animation/Animation;
/* .restart local v16 # "alphaAnimation":Landroid/view/animation/Animation; */
} // :cond_6
/* if-nez v12, :cond_7 */
/* .line 826 */
/* new-instance v3, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 827 */
} // .end local v16 # "alphaAnimation":Landroid/view/animation/Animation;
/* .restart local v3 # "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v14 ).addAnimation ( v3 ); // invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* move-object/from16 v16, v3 */
/* move-object v3, v6 */
/* .line 828 */
} // .end local v3 # "alphaAnimation":Landroid/view/animation/Animation;
/* .restart local v16 # "alphaAnimation":Landroid/view/animation/Animation; */
} // :cond_7
/* iget-boolean v5, v12, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z */
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 829 */
/* new-instance v5, Lcom/android/server/wm/AnimationDimmer; */
/* invoke-direct {v5, v4, v3, v1, v12}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V */
/* move-object v3, v5 */
/* .line 830 */
} // .end local v17 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
/* .local v3, "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
(( android.view.animation.AnimationSet ) v14 ).addAnimation ( v3 ); // invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 831 */
(( android.view.animation.AnimationSet ) v14 ).setTaskAnimationLevel ( v13 ); // invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setTaskAnimationLevel(Z)V
/* .line 832 */
/* iput-boolean v11, v12, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z */
/* move-object/from16 v17, v3 */
/* move-object v3, v6 */
/* .line 834 */
} // .end local v3 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
/* .restart local v17 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
} // :cond_8
v5 = /* invoke-virtual/range {p4 ..p4}, Lcom/android/server/wm/WindowContainer;->fillsParent()Z */
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 835 */
/* new-instance v5, Lcom/android/server/wm/AnimationDimmer; */
/* invoke-direct {v5, v4, v3, v1, v12}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V */
/* move-object v3, v5 */
/* .line 836 */
} // .end local v17 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
/* .restart local v3 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
(( android.view.animation.AnimationSet ) v14 ).addAnimation ( v3 ); // invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* move-object/from16 v17, v3 */
/* move-object v3, v6 */
/* .line 834 */
} // .end local v3 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
/* .restart local v17 # "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
} // :cond_9
/* move-object v3, v6 */
/* .line 840 */
} // .end local v6 # "translateAnimation":Landroid/view/animation/Animation;
/* .restart local v15 # "translateAnimation":Landroid/view/animation/Animation; */
} // :cond_a
/* new-instance v18, Landroid/view/animation/TranslateAnimation; */
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
int v6 = 1; // const/4 v6, 0x1
int v8 = 1; // const/4 v8, 0x1
int v9 = 0; // const/4 v9, 0x0
int v10 = 1; // const/4 v10, 0x1
/* const/16 v19, 0x0 */
/* move-object/from16 v3, v18 */
/* move/from16 v7, p3 */
/* move/from16 v11, v19 */
/* invoke-direct/range {v3 ..v11}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* .line 844 */
} // .end local v15 # "translateAnimation":Landroid/view/animation/Animation;
/* .local v3, "translateAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v14 ).setZAdjustment ( v13 ); // invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 847 */
} // :goto_0
/* new-instance v4, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator; */
/* const v5, 0x3f4ccccd # 0.8f */
/* const v6, 0x3f733333 # 0.95f */
/* invoke-direct {v4, v5, v6}, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;-><init>(FF)V */
/* .line 848 */
(( android.view.animation.AnimationSet ) v14 ).addAnimation ( v3 ); // invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 849 */
v4 = com.android.server.wm.AppTransitionInjector.sActivityTransitionInterpolator;
(( android.view.animation.AnimationSet ) v14 ).setInterpolator ( v4 ); // invoke-virtual {v14, v4}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 850 */
/* const-wide/16 v4, 0x1f4 */
(( android.view.animation.AnimationSet ) v14 ).setDuration ( v4, v5 ); // invoke-virtual {v14, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V
/* .line 851 */
(( android.view.animation.AnimationSet ) v14 ).setHasRoundedCorners ( v13 ); // invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V
/* .line 852 */
/* if-eq v2, v0, :cond_b */
/* .line 853 */
int v4 = 0; // const/4 v4, 0x0
(( android.view.animation.AnimationSet ) v14 ).setHasRoundedCorners ( v4 ); // invoke-virtual {v14, v4}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V
/* .line 855 */
} // :cond_b
} // .end method
static android.view.animation.Animation createActivityOpenCloseTransitionForDimmer ( Boolean p0, android.graphics.Rect p1, Boolean p2, com.android.server.wm.WindowContainer p3, com.android.server.wm.WindowManagerService p4 ) {
/* .locals 18 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "isOpenOrClose" # Z */
/* .param p3, "container" # Lcom/android/server/wm/WindowContainer; */
/* .param p4, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .line 861 */
/* move-object/from16 v0, p1 */
/* move-object/from16 v1, p3 */
/* new-instance v2, Landroid/view/animation/AnimationSet; */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 862 */
/* .local v2, "set":Landroid/view/animation/AnimationSet; */
int v4 = 0; // const/4 v4, 0x0
/* .line 863 */
/* .local v4, "translateAnimation":Landroid/view/animation/TranslateAnimation; */
int v5 = 0; // const/4 v5, 0x0
/* .line 864 */
/* .local v5, "animationDimmer":Lcom/android/server/wm/AnimationDimmer; */
v6 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->width()I */
v7 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
int v8 = 0; // const/4 v8, 0x0
(( android.graphics.Rect ) v0 ).set ( v8, v8, v6, v7 ); // invoke-virtual {v0, v8, v8, v6, v7}, Landroid/graphics/Rect;->set(IIII)V
/* .line 865 */
int v6 = 0; // const/4 v6, 0x0
/* const/high16 v7, 0x3f000000 # 0.5f */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 866 */
if ( p0 != null) { // if-eqz p0, :cond_5
/* .line 867 */
/* new-instance v17, Landroid/view/animation/TranslateAnimation; */
int v9 = 1; // const/4 v9, 0x1
/* const/high16 v10, 0x3f800000 # 1.0f */
int v11 = 1; // const/4 v11, 0x1
int v12 = 0; // const/4 v12, 0x0
int v13 = 1; // const/4 v13, 0x1
int v14 = 0; // const/4 v14, 0x0
int v15 = 1; // const/4 v15, 0x1
/* const/16 v16, 0x0 */
/* move-object/from16 v8, v17 */
/* invoke-direct/range {v8 ..v16}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* move-object/from16 v4, v17 */
/* .line 871 */
/* instance-of v8, v1, Lcom/android/server/wm/ActivityRecord; */
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 872 */
/* new-instance v8, Lcom/android/server/wm/AnimationDimmer; */
/* move-object v9, v1 */
/* check-cast v9, Lcom/android/server/wm/ActivityRecord; */
/* .line 873 */
(( com.android.server.wm.ActivityRecord ) v9 ).getTask ( ); // invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* invoke-direct {v8, v6, v7, v0, v9}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V */
/* move-object v5, v8 */
/* .line 875 */
} // :cond_0
/* instance-of v8, v1, Lcom/android/server/wm/Task; */
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 876 */
/* new-instance v8, Lcom/android/server/wm/AnimationDimmer; */
/* invoke-direct {v8, v6, v7, v0, v1}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V */
/* move-object v5, v8 */
/* .line 879 */
} // :cond_1
v6 = (( android.view.animation.TranslateAnimation ) v4 ).getZAdjustment ( ); // invoke-virtual {v4}, Landroid/view/animation/TranslateAnimation;->getZAdjustment()I
/* sub-int/2addr v6, v3 */
(( com.android.server.wm.AnimationDimmer ) v5 ).setZAdjustment ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/AnimationDimmer;->setZAdjustment(I)V
/* .line 880 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v4 ); // invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 881 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v5 ); // invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 884 */
} // :cond_2
/* if-nez p0, :cond_5 */
/* .line 885 */
/* new-instance v17, Landroid/view/animation/TranslateAnimation; */
int v9 = 1; // const/4 v9, 0x1
int v10 = 0; // const/4 v10, 0x0
int v11 = 1; // const/4 v11, 0x1
/* const/high16 v12, 0x3f800000 # 1.0f */
int v13 = 1; // const/4 v13, 0x1
int v14 = 0; // const/4 v14, 0x0
int v15 = 1; // const/4 v15, 0x1
/* const/16 v16, 0x0 */
/* move-object/from16 v8, v17 */
/* invoke-direct/range {v8 ..v16}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* move-object/from16 v4, v17 */
/* .line 889 */
/* instance-of v8, v1, Lcom/android/server/wm/ActivityRecord; */
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 890 */
/* new-instance v8, Lcom/android/server/wm/AnimationDimmer; */
/* move-object v9, v1 */
/* check-cast v9, Lcom/android/server/wm/ActivityRecord; */
/* .line 891 */
(( com.android.server.wm.ActivityRecord ) v9 ).getTask ( ); // invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* invoke-direct {v8, v7, v6, v0, v9}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V */
/* move-object v5, v8 */
/* .line 893 */
} // :cond_3
/* instance-of v8, v1, Lcom/android/server/wm/Task; */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 894 */
/* new-instance v8, Lcom/android/server/wm/AnimationDimmer; */
/* invoke-direct {v8, v7, v6, v0, v1}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V */
/* move-object v5, v8 */
/* .line 897 */
} // :cond_4
v6 = (( android.view.animation.TranslateAnimation ) v4 ).getZAdjustment ( ); // invoke-virtual {v4}, Landroid/view/animation/TranslateAnimation;->getZAdjustment()I
/* sub-int/2addr v6, v3 */
(( com.android.server.wm.AnimationDimmer ) v5 ).setZAdjustment ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/AnimationDimmer;->setZAdjustment(I)V
/* .line 898 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v4 ); // invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 899 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v5 ); // invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 902 */
} // :cond_5
} // :goto_0
/* new-instance v6, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator; */
/* const v7, 0x3f4ccccd # 0.8f */
/* const v8, 0x3f733333 # 0.95f */
/* invoke-direct {v6, v7, v8}, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;-><init>(FF)V */
/* .line 903 */
(( android.view.animation.AnimationSet ) v2 ).setInterpolator ( v6 ); // invoke-virtual {v2, v6}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 904 */
/* const-wide/16 v6, 0x1f4 */
(( android.view.animation.AnimationSet ) v2 ).setDuration ( v6, v7 ); // invoke-virtual {v2, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V
/* .line 905 */
(( android.view.animation.AnimationSet ) v2 ).setHasRoundedCorners ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V
/* .line 906 */
} // .end method
static android.view.animation.Animation createBackActivityFromRoundedViewAnimation ( Boolean p0, android.graphics.Rect p1, android.graphics.Rect p2, Float p3 ) {
/* .locals 12 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "targetPositionRect" # Landroid/graphics/Rect; */
/* .param p3, "radius" # F */
/* .line 294 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V */
/* .line 296 */
/* .local v0, "positionRect":Landroid/graphics/Rect; */
/* const-wide/16 v1, 0xc8 */
/* const/high16 v3, 0x3f800000 # 1.0f */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 297 */
/* new-instance v4, Landroid/view/animation/AnimationSet; */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v4, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 298 */
/* .local v4, "set":Landroid/view/animation/AnimationSet; */
/* new-instance v5, Landroid/view/animation/AlphaAnimation; */
/* const v6, 0x3f4ccccd # 0.8f */
/* invoke-direct {v5, v6, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v3, v5 */
/* .line 300 */
/* .local v3, "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v4 ).addAnimation ( v3 ); // invoke-virtual {v4, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 301 */
v5 = com.android.server.wm.AppTransitionInjector.SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.Animation ) v3 ).setInterpolator ( v5 ); // invoke-virtual {v3, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 302 */
int v5 = -1; // const/4 v5, -0x1
(( android.view.animation.AnimationSet ) v4 ).setZAdjustment ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 303 */
(( android.view.animation.Animation ) v3 ).setDuration ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 304 */
/* move-object v1, v4 */
/* .line 305 */
} // .end local v3 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v4 # "set":Landroid/view/animation/AnimationSet;
/* .local v1, "anim":Landroid/view/animation/Animation; */
/* .line 306 */
} // .end local v1 # "anim":Landroid/view/animation/Animation;
} // :cond_0
/* new-instance v4, Landroid/view/animation/AlphaAnimation; */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v4, v3, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 308 */
/* .local v4, "alphaAnimation":Landroid/view/animation/Animation; */
/* const-wide/16 v5, 0x64 */
(( android.view.animation.Animation ) v4 ).setStartOffset ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/view/animation/Animation;->setStartOffset(J)V
/* .line 309 */
/* new-instance v5, Lcom/android/server/wm/RadiusAnimation; */
/* int-to-float v6, v6 */
/* invoke-direct {v5, v6, p3}, Lcom/android/server/wm/RadiusAnimation;-><init>(FF)V */
/* .line 310 */
/* .local v5, "radiusAnimation":Landroid/view/animation/Animation; */
v6 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
/* int-to-float v6, v6 */
v7 = (( android.graphics.Rect ) p1 ).width ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->width()I
/* int-to-float v7, v7 */
/* div-float/2addr v6, v7 */
/* .line 311 */
/* .local v6, "scaleX":F */
/* new-instance v7, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation; */
/* iget v8, v0, Landroid/graphics/Rect;->left:I */
/* int-to-float v8, v8 */
/* sub-float v9, v3, v6 */
/* div-float/2addr v8, v9 */
/* invoke-direct {v7, v3, v6, v8}, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;-><init>(FFF)V */
/* .line 313 */
/* .local v7, "scaleXAnimation":Landroid/view/animation/Animation; */
v8 = (( android.graphics.Rect ) v0 ).height ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
/* int-to-float v8, v8 */
v9 = (( android.graphics.Rect ) p1 ).height ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->height()I
/* int-to-float v9, v9 */
/* div-float/2addr v8, v9 */
/* .line 314 */
/* .local v8, "scaleY":F */
/* new-instance v9, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation; */
/* iget v10, v0, Landroid/graphics/Rect;->top:I */
/* int-to-float v10, v10 */
/* sub-float v11, v3, v8 */
/* div-float/2addr v10, v11 */
/* invoke-direct {v9, v3, v8, v10}, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;-><init>(FFF)V */
/* move-object v3, v9 */
/* .line 316 */
/* .local v3, "scaleYAnimation":Landroid/view/animation/Animation; */
/* new-instance v9, Landroid/view/animation/AnimationSet; */
int v10 = 1; // const/4 v10, 0x1
/* invoke-direct {v9, v10}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 317 */
/* .local v9, "set":Landroid/view/animation/AnimationSet; */
(( android.view.animation.AnimationSet ) v9 ).addAnimation ( v4 ); // invoke-virtual {v9, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 318 */
(( android.view.animation.AnimationSet ) v9 ).addAnimation ( v5 ); // invoke-virtual {v9, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 319 */
(( android.view.animation.AnimationSet ) v9 ).addAnimation ( v7 ); // invoke-virtual {v9, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 320 */
(( android.view.animation.AnimationSet ) v9 ).addAnimation ( v3 ); // invoke-virtual {v9, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 322 */
v11 = com.android.server.wm.AppTransitionInjector.QUART_EASE_OUT_INTERPOLATOR;
(( android.view.animation.Animation ) v4 ).setInterpolator ( v11 ); // invoke-virtual {v4, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 323 */
v11 = com.android.server.wm.AppTransitionInjector.SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.Animation ) v5 ).setInterpolator ( v11 ); // invoke-virtual {v5, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 324 */
(( android.view.animation.Animation ) v7 ).setInterpolator ( v11 ); // invoke-virtual {v7, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 325 */
(( android.view.animation.Animation ) v3 ).setInterpolator ( v11 ); // invoke-virtual {v3, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 327 */
(( android.view.animation.AnimationSet ) v9 ).setZAdjustment ( v10 ); // invoke-virtual {v9, v10}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 329 */
(( android.view.animation.Animation ) v4 ).setDuration ( v1, v2 ); // invoke-virtual {v4, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 330 */
/* const-wide/16 v1, 0x1c2 */
(( android.view.animation.Animation ) v5 ).setDuration ( v1, v2 ); // invoke-virtual {v5, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 331 */
(( android.view.animation.Animation ) v7 ).setDuration ( v1, v2 ); // invoke-virtual {v7, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 332 */
(( android.view.animation.Animation ) v3 ).setDuration ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 334 */
/* move-object v1, v9 */
/* .line 336 */
} // .end local v3 # "scaleYAnimation":Landroid/view/animation/Animation;
} // .end local v4 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v5 # "radiusAnimation":Landroid/view/animation/Animation;
} // .end local v6 # "scaleX":F
} // .end local v7 # "scaleXAnimation":Landroid/view/animation/Animation;
} // .end local v8 # "scaleY":F
} // .end local v9 # "set":Landroid/view/animation/AnimationSet;
/* .restart local v1 # "anim":Landroid/view/animation/Animation; */
} // :goto_0
} // .end method
static android.view.animation.Animation createBackActivityScaledToScreenCenter ( Boolean p0, android.graphics.Rect p1 ) {
/* .locals 14 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .line 341 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V */
/* .line 342 */
/* .local v0, "positionRect":Landroid/graphics/Rect; */
v1 = (( android.graphics.Rect ) p1 ).width ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->width()I
/* int-to-float v1, v1 */
/* const v2, 0x3f19999a # 0.6f */
/* mul-float/2addr v1, v2 */
/* float-to-int v1, v1 */
/* .line 343 */
/* .local v1, "scaleWith":I */
v3 = (( android.graphics.Rect ) p1 ).height ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->height()I
/* int-to-float v3, v3 */
/* mul-float/2addr v3, v2 */
/* float-to-int v2, v3 */
/* .line 344 */
/* .local v2, "scaleHeight":I */
/* iget v3, p1, Landroid/graphics/Rect;->left:I */
v4 = (( android.graphics.Rect ) p1 ).width ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->width()I
/* sub-int/2addr v4, v1 */
/* div-int/lit8 v4, v4, 0x2 */
/* add-int/2addr v3, v4 */
/* iput v3, v0, Landroid/graphics/Rect;->left:I */
/* .line 345 */
/* iget v3, p1, Landroid/graphics/Rect;->top:I */
v4 = (( android.graphics.Rect ) p1 ).height ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->height()I
/* sub-int/2addr v4, v2 */
/* div-int/lit8 v4, v4, 0x2 */
/* add-int/2addr v3, v4 */
/* iput v3, v0, Landroid/graphics/Rect;->top:I */
/* .line 346 */
/* iget v3, p1, Landroid/graphics/Rect;->right:I */
v4 = (( android.graphics.Rect ) p1 ).width ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->width()I
/* sub-int/2addr v4, v1 */
/* div-int/lit8 v4, v4, 0x2 */
/* sub-int/2addr v3, v4 */
/* iput v3, v0, Landroid/graphics/Rect;->right:I */
/* .line 347 */
/* iget v3, p1, Landroid/graphics/Rect;->bottom:I */
v4 = (( android.graphics.Rect ) p1 ).height ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->height()I
/* sub-int/2addr v4, v2 */
/* div-int/lit8 v4, v4, 0x2 */
/* sub-int/2addr v3, v4 */
/* iput v3, v0, Landroid/graphics/Rect;->bottom:I */
/* .line 349 */
/* const-wide/16 v3, 0x1c2 */
int v5 = 1; // const/4 v5, 0x1
/* const/high16 v6, 0x3f800000 # 1.0f */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 350 */
/* new-instance v7, Landroid/view/animation/AnimationSet; */
/* invoke-direct {v7, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* move-object v5, v7 */
/* .line 351 */
/* .local v5, "set":Landroid/view/animation/AnimationSet; */
/* new-instance v7, Landroid/view/animation/AlphaAnimation; */
/* const v8, 0x3f4ccccd # 0.8f */
/* invoke-direct {v7, v8, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v6, v7 */
/* .line 353 */
/* .local v6, "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v5 ).addAnimation ( v6 ); // invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 354 */
v7 = com.android.server.wm.AppTransitionInjector.SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.AnimationSet ) v5 ).setInterpolator ( v7 ); // invoke-virtual {v5, v7}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 355 */
int v7 = -1; // const/4 v7, -0x1
(( android.view.animation.AnimationSet ) v5 ).setZAdjustment ( v7 ); // invoke-virtual {v5, v7}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 356 */
(( android.view.animation.AnimationSet ) v5 ).setDuration ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Landroid/view/animation/AnimationSet;->setDuration(J)V
/* .line 357 */
/* move-object v3, v5 */
/* .line 358 */
} // .end local v5 # "set":Landroid/view/animation/AnimationSet;
} // .end local v6 # "alphaAnimation":Landroid/view/animation/Animation;
/* .local v3, "anim":Landroid/view/animation/Animation; */
/* .line 359 */
} // .end local v3 # "anim":Landroid/view/animation/Animation;
} // :cond_0
/* new-instance v7, Landroid/view/animation/AlphaAnimation; */
int v8 = 0; // const/4 v8, 0x0
/* invoke-direct {v7, v6, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 361 */
/* .local v7, "alphaAnimation":Landroid/view/animation/Animation; */
v8 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
/* int-to-float v8, v8 */
v9 = (( android.graphics.Rect ) p1 ).width ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->width()I
/* int-to-float v9, v9 */
/* div-float/2addr v8, v9 */
/* .line 362 */
/* .local v8, "scaleX":F */
/* new-instance v9, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation; */
/* iget v10, v0, Landroid/graphics/Rect;->left:I */
/* int-to-float v10, v10 */
/* sub-float v11, v6, v8 */
/* div-float/2addr v10, v11 */
/* invoke-direct {v9, v6, v8, v10}, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;-><init>(FFF)V */
/* .line 364 */
/* .local v9, "scaleXAnimation":Landroid/view/animation/Animation; */
v10 = (( android.graphics.Rect ) v0 ).height ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
/* int-to-float v10, v10 */
v11 = (( android.graphics.Rect ) p1 ).height ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->height()I
/* int-to-float v11, v11 */
/* div-float/2addr v10, v11 */
/* .line 365 */
/* .local v10, "scaleY":F */
/* new-instance v11, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation; */
/* iget v12, v0, Landroid/graphics/Rect;->top:I */
/* int-to-float v12, v12 */
/* sub-float v13, v6, v10 */
/* div-float/2addr v12, v13 */
/* invoke-direct {v11, v6, v10, v12}, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;-><init>(FFF)V */
/* move-object v6, v11 */
/* .line 367 */
/* .local v6, "scaleYAnimation":Landroid/view/animation/Animation; */
/* new-instance v11, Landroid/view/animation/AnimationSet; */
/* invoke-direct {v11, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 368 */
/* .local v11, "set":Landroid/view/animation/AnimationSet; */
(( android.view.animation.AnimationSet ) v11 ).addAnimation ( v7 ); // invoke-virtual {v11, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 369 */
(( android.view.animation.AnimationSet ) v11 ).addAnimation ( v9 ); // invoke-virtual {v11, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 370 */
(( android.view.animation.AnimationSet ) v11 ).addAnimation ( v6 ); // invoke-virtual {v11, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 371 */
v12 = com.android.server.wm.AppTransitionInjector.SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.AnimationSet ) v11 ).setInterpolator ( v12 ); // invoke-virtual {v11, v12}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 372 */
(( android.view.animation.AnimationSet ) v11 ).setZAdjustment ( v5 ); // invoke-virtual {v11, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 373 */
(( android.view.animation.AnimationSet ) v11 ).setDuration ( v3, v4 ); // invoke-virtual {v11, v3, v4}, Landroid/view/animation/AnimationSet;->setDuration(J)V
/* .line 374 */
/* move-object v3, v11 */
/* .line 376 */
} // .end local v6 # "scaleYAnimation":Landroid/view/animation/Animation;
} // .end local v7 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v8 # "scaleX":F
} // .end local v9 # "scaleXAnimation":Landroid/view/animation/Animation;
} // .end local v10 # "scaleY":F
} // .end local v11 # "set":Landroid/view/animation/AnimationSet;
/* .restart local v3 # "anim":Landroid/view/animation/Animation; */
} // :goto_0
} // .end method
static android.view.animation.Animation createDummyAnimation ( Float p0 ) {
/* .locals 3 */
/* .param p0, "alpha" # F */
/* .line 718 */
/* new-instance v0, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v0, p0, p0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 719 */
/* .local v0, "dummyAnimation":Landroid/view/animation/Animation; */
/* const-wide/16 v1, 0x12c */
(( android.view.animation.Animation ) v0 ).setDuration ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 720 */
} // .end method
static android.view.animation.Animation createFloatWindowShowHideAnimation ( Boolean p0, android.graphics.Rect p1, com.android.server.wm.MiuiFreeFormActivityStack p2 ) {
/* .locals 25 */
/* .param p0, "enter" # Z */
/* .param p1, "frame" # Landroid/graphics/Rect; */
/* .param p2, "freemAs" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 527 */
/* move/from16 v0, p0 */
/* move-object/from16 v1, p2 */
int v2 = 0; // const/4 v2, 0x0
/* .line 528 */
/* .local v2, "set":Landroid/view/animation/AnimationSet; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "createFloatWindowShowHideAnimation enter = "; // const-string v4, "createFloatWindowShowHideAnimation enter = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "AppTransitionInjector"; // const-string v4, "AppTransitionInjector"
android.util.Slog .e ( v4,v3 );
/* .line 529 */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* .line 530 */
/* .local v3, "freeFormScale":F */
int v4 = 0; // const/4 v4, 0x0
/* .line 531 */
/* .local v4, "needSetRoundedCorners":Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 532 */
int v4 = 1; // const/4 v4, 0x1
/* .line 533 */
/* iget v3, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* .line 535 */
} // :cond_0
/* new-instance v5, Landroid/view/animation/AnimationSet; */
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct {v5, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* move-object v2, v5 */
/* .line 536 */
int v5 = 1; // const/4 v5, 0x1
/* const/high16 v6, 0x40000000 # 2.0f */
/* const/high16 v7, 0x42a00000 # 80.0f */
int v8 = 0; // const/4 v8, 0x0
/* const/high16 v9, 0x3f800000 # 1.0f */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 537 */
/* new-instance v10, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v10, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 538 */
/* .local v10, "alphaAnimation":Landroid/view/animation/Animation; */
/* const-wide/16 v11, 0x15e */
(( android.view.animation.Animation ) v10 ).setDuration ( v11, v12 ); // invoke-virtual {v10, v11, v12}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 539 */
/* new-instance v13, Landroid/view/animation/DecelerateInterpolator; */
/* invoke-direct {v13, v9}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V */
(( android.view.animation.Animation ) v10 ).setInterpolator ( v13 ); // invoke-virtual {v10, v13}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 540 */
v13 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
/* int-to-float v13, v13 */
/* mul-float/2addr v13, v3 */
/* .line 541 */
/* .local v13, "windowHeight":F */
v14 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->width()I */
/* int-to-float v14, v14 */
/* mul-float/2addr v14, v3 */
/* .line 542 */
/* .local v14, "windowWidth":F */
/* cmpl-float v15, v13, v14 */
/* if-lez v15, :cond_1 */
/* move v15, v13 */
} // :cond_1
/* move v15, v14 */
/* .line 543 */
/* .local v15, "maxSide":F */
} // :goto_0
/* cmpl-float v16, v15, v7 */
/* if-lez v16, :cond_2 */
/* sub-float v7, v15, v7 */
/* div-float v8, v7, v15 */
} // :cond_2
/* move/from16 v17, v8 */
/* .line 544 */
/* .local v17, "scaleEnd":F */
/* new-instance v7, Landroid/view/animation/ScaleAnimation; */
/* const/high16 v18, 0x3f800000 # 1.0f */
/* const/high16 v20, 0x3f800000 # 1.0f */
/* const/16 v21, 0x0 */
/* div-float v22, v14, v6 */
/* const/16 v23, 0x0 */
/* div-float v24, v13, v6 */
/* move-object/from16 v16, v7 */
/* move/from16 v19, v17 */
/* invoke-direct/range {v16 ..v24}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V */
/* move-object v6, v7 */
/* .line 546 */
/* .local v6, "scaleAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.Animation ) v6 ).setDuration ( v11, v12 ); // invoke-virtual {v6, v11, v12}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 547 */
/* new-instance v7, Landroid/view/animation/DecelerateInterpolator; */
/* invoke-direct {v7, v9}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V */
(( android.view.animation.Animation ) v6 ).setInterpolator ( v7 ); // invoke-virtual {v6, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 548 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v10 ); // invoke-virtual {v2, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 549 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v6 ); // invoke-virtual {v2, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 550 */
(( android.view.animation.AnimationSet ) v2 ).setZAdjustment ( v5 ); // invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 551 */
if ( v4 != null) { // if-eqz v4, :cond_3
(( android.view.animation.AnimationSet ) v2 ).setHasRoundedCorners ( v5 ); // invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V
/* .line 552 */
} // .end local v6 # "scaleAnimation":Landroid/view/animation/Animation;
} // .end local v10 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v13 # "windowHeight":F
} // .end local v14 # "windowWidth":F
} // .end local v15 # "maxSide":F
} // .end local v17 # "scaleEnd":F
} // :cond_3
/* .line 553 */
} // :cond_4
/* new-instance v10, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v10, v9, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v9, v10 */
/* .line 554 */
/* .local v9, "alphaAnimation":Landroid/view/animation/Animation; */
/* const-wide/16 v10, 0xc8 */
(( android.view.animation.Animation ) v9 ).setDuration ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 555 */
/* new-instance v12, Landroid/view/animation/DecelerateInterpolator; */
/* const/high16 v13, 0x3fc00000 # 1.5f */
/* invoke-direct {v12, v13}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V */
(( android.view.animation.Animation ) v9 ).setInterpolator ( v12 ); // invoke-virtual {v9, v12}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 556 */
v12 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
/* int-to-float v12, v12 */
/* mul-float/2addr v12, v3 */
/* .line 557 */
/* .local v12, "windowHeight":F */
v14 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->width()I */
/* int-to-float v14, v14 */
/* mul-float/2addr v14, v3 */
/* .line 558 */
/* .restart local v14 # "windowWidth":F */
/* cmpl-float v15, v12, v14 */
/* if-lez v15, :cond_5 */
/* move v15, v12 */
} // :cond_5
/* move v15, v14 */
/* .line 559 */
/* .restart local v15 # "maxSide":F */
} // :goto_1
/* cmpl-float v16, v15, v7 */
/* if-lez v16, :cond_6 */
/* sub-float v7, v15, v7 */
/* div-float v8, v7, v15 */
} // :cond_6
/* move/from16 v18, v8 */
/* .line 560 */
/* .local v18, "scaleEnd":F */
/* new-instance v7, Landroid/view/animation/ScaleAnimation; */
/* const/high16 v17, 0x3f800000 # 1.0f */
/* const/high16 v19, 0x3f800000 # 1.0f */
/* const/16 v21, 0x0 */
/* div-float v22, v14, v6 */
/* const/16 v23, 0x0 */
/* div-float v24, v12, v6 */
/* move-object/from16 v16, v7 */
/* move/from16 v20, v18 */
/* invoke-direct/range {v16 ..v24}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V */
/* move-object v6, v7 */
/* .line 562 */
/* .restart local v6 # "scaleAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.Animation ) v6 ).setDuration ( v10, v11 ); // invoke-virtual {v6, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 563 */
/* new-instance v7, Landroid/view/animation/DecelerateInterpolator; */
/* invoke-direct {v7, v13}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V */
(( android.view.animation.Animation ) v6 ).setInterpolator ( v7 ); // invoke-virtual {v6, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 564 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v9 ); // invoke-virtual {v2, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 565 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v6 ); // invoke-virtual {v2, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 566 */
(( android.view.animation.AnimationSet ) v2 ).setZAdjustment ( v5 ); // invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 567 */
if ( v4 != null) { // if-eqz v4, :cond_7
(( android.view.animation.AnimationSet ) v2 ).setHasRoundedCorners ( v5 ); // invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V
/* .line 569 */
} // .end local v6 # "scaleAnimation":Landroid/view/animation/Animation;
} // .end local v9 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v12 # "windowHeight":F
} // .end local v14 # "windowWidth":F
} // .end local v15 # "maxSide":F
} // .end local v18 # "scaleEnd":F
} // :cond_7
} // :goto_2
} // .end method
static android.view.animation.Animation createFreeFormActivityOpenAndExitAnimation ( Boolean p0 ) {
/* .locals 4 */
/* .param p0, "enter" # Z */
/* .line 470 */
int v0 = 0; // const/4 v0, 0x0
/* .line 471 */
/* .local v0, "translateAnimation":Landroid/view/animation/Animation; */
int v1 = 0; // const/4 v1, 0x0
/* const/high16 v2, 0x3f800000 # 1.0f */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 472 */
/* new-instance v3, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v3, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v0, v3 */
/* .line 474 */
} // :cond_0
/* new-instance v3, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v3, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v0, v3 */
/* .line 476 */
} // :goto_0
/* const-wide/16 v1, 0x1f4 */
(( android.view.animation.Animation ) v0 ).setDuration ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 477 */
} // .end method
static android.view.animation.Animation createFreeFormAppOpenAndExitAnimation ( com.android.server.wm.WindowManagerService p0, Boolean p1, android.graphics.Rect p2, com.android.server.wm.MiuiFreeFormActivityStack p3 ) {
/* .locals 21 */
/* .param p0, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p1, "enter" # Z */
/* .param p2, "frame" # Landroid/graphics/Rect; */
/* .param p3, "freemAs" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 481 */
/* move-object/from16 v0, p3 */
int v1 = 0; // const/4 v1, 0x0
/* .line 482 */
/* .local v1, "set":Landroid/view/animation/AnimationSet; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 483 */
/* new-instance v2, Landroid/view/animation/AnimationSet; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* move-object v1, v2 */
/* .line 484 */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
/* const/high16 v4, 0x3f800000 # 1.0f */
/* const/high16 v5, 0x40000000 # 2.0f */
/* const v6, 0x3f333333 # 0.7f */
/* const v7, 0x3f59999a # 0.85f */
/* const-wide/16 v8, 0x1f4 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 485 */
/* new-instance v10, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v10, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v3, v10 */
/* .line 486 */
/* .local v3, "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.Animation ) v3 ).setDuration ( v8, v9 ); // invoke-virtual {v3, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 487 */
/* new-instance v4, Lcom/android/server/wm/PhysicBasedInterpolator; */
/* invoke-direct {v4, v7, v6}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V */
(( android.view.animation.Animation ) v3 ).setInterpolator ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 488 */
v4 = /* invoke-virtual/range {p2 ..p2}, Landroid/graphics/Rect;->height()I */
/* int-to-float v4, v4 */
/* iget v10, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* mul-float/2addr v4, v10 */
/* .line 489 */
/* .local v4, "windowHeight":F */
v10 = /* invoke-virtual/range {p2 ..p2}, Landroid/graphics/Rect;->width()I */
/* int-to-float v10, v10 */
/* iget v11, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* mul-float/2addr v10, v11 */
/* .line 490 */
/* .local v10, "windowWidth":F */
/* new-instance v20, Landroid/view/animation/ScaleAnimation; */
/* const v12, 0x3f19999a # 0.6f */
/* const/high16 v13, 0x3f800000 # 1.0f */
/* const v14, 0x3f19999a # 0.6f */
/* const/high16 v15, 0x3f800000 # 1.0f */
/* const/16 v16, 0x0 */
/* div-float v17, v10, v5 */
/* const/16 v18, 0x0 */
/* div-float v19, v4, v5 */
/* move-object/from16 v11, v20 */
/* invoke-direct/range {v11 ..v19}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V */
/* move-object/from16 v5, v20 */
/* .line 492 */
/* .local v5, "scaleAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.Animation ) v5 ).setDuration ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 493 */
/* new-instance v8, Lcom/android/server/wm/PhysicBasedInterpolator; */
/* invoke-direct {v8, v7, v6}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V */
(( android.view.animation.Animation ) v5 ).setInterpolator ( v8 ); // invoke-virtual {v5, v8}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 494 */
(( android.view.animation.AnimationSet ) v1 ).addAnimation ( v3 ); // invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 495 */
(( android.view.animation.AnimationSet ) v1 ).addAnimation ( v5 ); // invoke-virtual {v1, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 496 */
(( android.view.animation.AnimationSet ) v1 ).setZAdjustment ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 497 */
} // .end local v3 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v4 # "windowHeight":F
} // .end local v5 # "scaleAnimation":Landroid/view/animation/Animation;
} // .end local v10 # "windowWidth":F
/* .line 498 */
} // :cond_0
/* new-instance v10, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v10, v4, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v3, v10 */
/* .line 499 */
/* .restart local v3 # "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.Animation ) v3 ).setDuration ( v8, v9 ); // invoke-virtual {v3, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 500 */
/* new-instance v4, Lcom/android/server/wm/PhysicBasedInterpolator; */
/* invoke-direct {v4, v7, v6}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V */
(( android.view.animation.Animation ) v3 ).setInterpolator ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 501 */
v4 = /* invoke-virtual/range {p2 ..p2}, Landroid/graphics/Rect;->height()I */
/* int-to-float v4, v4 */
/* iget v10, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* mul-float/2addr v4, v10 */
/* .line 502 */
/* .restart local v4 # "windowHeight":F */
v10 = /* invoke-virtual/range {p2 ..p2}, Landroid/graphics/Rect;->width()I */
/* int-to-float v10, v10 */
/* iget v11, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* mul-float/2addr v10, v11 */
/* .line 503 */
/* .restart local v10 # "windowWidth":F */
/* new-instance v20, Landroid/view/animation/ScaleAnimation; */
/* const/high16 v12, 0x3f800000 # 1.0f */
/* const v13, 0x3f19999a # 0.6f */
/* const/high16 v14, 0x3f800000 # 1.0f */
/* const v15, 0x3f19999a # 0.6f */
/* const/16 v16, 0x0 */
/* div-float v17, v10, v5 */
/* const/16 v18, 0x0 */
/* div-float v19, v4, v5 */
/* move-object/from16 v11, v20 */
/* invoke-direct/range {v11 ..v19}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V */
/* move-object/from16 v5, v20 */
/* .line 505 */
/* .restart local v5 # "scaleAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.Animation ) v5 ).setDuration ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 506 */
/* new-instance v8, Lcom/android/server/wm/PhysicBasedInterpolator; */
/* invoke-direct {v8, v7, v6}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V */
(( android.view.animation.Animation ) v5 ).setInterpolator ( v8 ); // invoke-virtual {v5, v8}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 507 */
(( android.view.animation.AnimationSet ) v1 ).addAnimation ( v3 ); // invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 508 */
(( android.view.animation.AnimationSet ) v1 ).addAnimation ( v5 ); // invoke-virtual {v1, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 509 */
(( android.view.animation.AnimationSet ) v1 ).setZAdjustment ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 512 */
} // .end local v3 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v4 # "windowHeight":F
} // .end local v5 # "scaleAnimation":Landroid/view/animation/Animation;
} // .end local v10 # "windowWidth":F
} // :cond_1
} // :goto_0
} // .end method
static android.view.animation.Animation createLaunchActivityFromRoundedViewAnimation ( Integer p0, Boolean p1, android.graphics.Rect p2, android.graphics.Rect p3, Float p4 ) {
/* .locals 23 */
/* .param p0, "transit" # I */
/* .param p1, "enter" # Z */
/* .param p2, "appFrame" # Landroid/graphics/Rect; */
/* .param p3, "positionRect" # Landroid/graphics/Rect; */
/* .param p4, "radius" # F */
/* .line 244 */
/* move-object/from16 v0, p3 */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/graphics/Rect;->width()I */
/* .line 245 */
/* .local v1, "appWidth":I */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/graphics/Rect;->height()I */
/* .line 246 */
/* .local v2, "appHeight":I */
/* iget v3, v0, Landroid/graphics/Rect;->left:I */
/* .line 247 */
/* .local v3, "startX":I */
/* iget v4, v0, Landroid/graphics/Rect;->top:I */
/* .line 248 */
/* .local v4, "startY":I */
v5 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->width()I */
/* .line 249 */
/* .local v5, "startWidth":I */
v6 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->height()I */
/* .line 250 */
/* .local v6, "startHeight":I */
int v8 = 0; // const/4 v8, 0x0
/* const/high16 v11, 0x3f800000 # 1.0f */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 251 */
/* new-instance v12, Landroid/view/animation/AlphaAnimation; */
int v13 = 0; // const/4 v13, 0x0
/* invoke-direct {v12, v13, v11}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 253 */
/* .local v12, "alphaAnimation":Landroid/view/animation/Animation; */
/* new-instance v13, Lcom/android/server/wm/RadiusAnimation; */
/* int-to-float v14, v14 */
/* move/from16 v15, p4 */
/* invoke-direct {v13, v15, v14}, Lcom/android/server/wm/RadiusAnimation;-><init>(FF)V */
/* .line 254 */
/* .local v13, "radiusAnimation":Landroid/view/animation/Animation; */
/* int-to-float v14, v5 */
/* int-to-float v9, v1 */
/* div-float/2addr v14, v9 */
/* .line 255 */
/* .local v14, "scaleX":F */
/* int-to-float v9, v6 */
/* int-to-float v10, v2 */
/* div-float/2addr v9, v10 */
/* .line 256 */
/* .local v9, "scaleY":F */
/* new-instance v10, Landroid/view/animation/ScaleAnimation; */
/* const/high16 v18, 0x3f800000 # 1.0f */
/* const/high16 v20, 0x3f800000 # 1.0f */
/* int-to-float v7, v3 */
/* sub-float v16, v11, v14 */
/* div-float v21, v7, v16 */
/* int-to-float v7, v4 */
/* sub-float/2addr v11, v9 */
/* div-float v22, v7, v11 */
/* move-object/from16 v16, v10 */
/* move/from16 v17, v14 */
/* move/from16 v19, v9 */
/* invoke-direct/range {v16 ..v22}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V */
/* move-object v7, v10 */
/* .line 260 */
/* .local v7, "scaleAnimation":Landroid/view/animation/ScaleAnimation; */
(( android.view.animation.ScaleAnimation ) v7 ).initialize ( v1, v2, v1, v2 ); // invoke-virtual {v7, v1, v2, v1, v2}, Landroid/view/animation/ScaleAnimation;->initialize(IIII)V
/* .line 261 */
/* new-instance v10, Landroid/view/animation/AnimationSet; */
/* invoke-direct {v10, v8}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* move-object v8, v10 */
/* .line 262 */
/* .local v8, "set":Landroid/view/animation/AnimationSet; */
(( android.view.animation.AnimationSet ) v8 ).addAnimation ( v12 ); // invoke-virtual {v8, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 263 */
(( android.view.animation.AnimationSet ) v8 ).addAnimation ( v13 ); // invoke-virtual {v8, v13}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 264 */
(( android.view.animation.AnimationSet ) v8 ).addAnimation ( v7 ); // invoke-virtual {v8, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 266 */
v10 = com.android.server.wm.AppTransitionInjector.QUART_EASE_OUT_INTERPOLATOR;
(( android.view.animation.Animation ) v12 ).setInterpolator ( v10 ); // invoke-virtual {v12, v10}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 267 */
v10 = com.android.server.wm.AppTransitionInjector.SCALE_UP_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.Animation ) v13 ).setInterpolator ( v10 ); // invoke-virtual {v13, v10}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 268 */
(( android.view.animation.ScaleAnimation ) v7 ).setInterpolator ( v10 ); // invoke-virtual {v7, v10}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 270 */
int v10 = 1; // const/4 v10, 0x1
(( android.view.animation.AnimationSet ) v8 ).setZAdjustment ( v10 ); // invoke-virtual {v8, v10}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 272 */
/* const-wide/16 v10, 0x64 */
(( android.view.animation.Animation ) v12 ).setDuration ( v10, v11 ); // invoke-virtual {v12, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 273 */
/* const-wide/16 v10, 0x1f4 */
(( android.view.animation.Animation ) v13 ).setDuration ( v10, v11 ); // invoke-virtual {v13, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 274 */
(( android.view.animation.ScaleAnimation ) v7 ).setDuration ( v10, v11 ); // invoke-virtual {v7, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V
/* .line 277 */
/* move-object v7, v8 */
/* .line 278 */
} // .end local v8 # "set":Landroid/view/animation/AnimationSet;
} // .end local v9 # "scaleY":F
} // .end local v12 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v13 # "radiusAnimation":Landroid/view/animation/Animation;
} // .end local v14 # "scaleX":F
/* .local v7, "anim":Landroid/view/animation/Animation; */
/* .line 279 */
} // .end local v7 # "anim":Landroid/view/animation/Animation;
} // :cond_0
/* move/from16 v15, p4 */
/* new-instance v7, Landroid/view/animation/AnimationSet; */
/* invoke-direct {v7, v8}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 280 */
/* .local v7, "set":Landroid/view/animation/AnimationSet; */
/* new-instance v8, Landroid/view/animation/AlphaAnimation; */
/* const v9, 0x3f4ccccd # 0.8f */
/* invoke-direct {v8, v11, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 282 */
/* .local v8, "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v7 ).addAnimation ( v8 ); // invoke-virtual {v7, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 283 */
v9 = com.android.server.wm.AppTransitionInjector.SCALE_UP_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.Animation ) v8 ).setInterpolator ( v9 ); // invoke-virtual {v8, v9}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 284 */
int v9 = -1; // const/4 v9, -0x1
(( android.view.animation.AnimationSet ) v7 ).setZAdjustment ( v9 ); // invoke-virtual {v7, v9}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 285 */
/* const-wide/16 v9, 0x1f4 */
(( android.view.animation.Animation ) v8 ).setDuration ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 286 */
/* move-object v9, v7 */
/* .line 287 */
/* .local v9, "anim":Landroid/view/animation/Animation; */
int v10 = 1; // const/4 v10, 0x1
(( android.view.animation.Animation ) v9 ).setFillAfter ( v10 ); // invoke-virtual {v9, v10}, Landroid/view/animation/Animation;->setFillAfter(Z)V
/* .line 289 */
} // .end local v8 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v9 # "anim":Landroid/view/animation/Animation;
/* .local v7, "anim":Landroid/view/animation/Animation; */
} // :goto_0
} // .end method
static android.view.animation.Animation createScaleDownAnimation ( Boolean p0, android.graphics.Rect p1, android.graphics.Rect p2, Float p3 ) {
/* .locals 8 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "targetPositionRect" # Landroid/graphics/Rect; */
/* .param p3, "radius" # F */
/* .line 206 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* const-wide/16 v1, 0x1c2 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 207 */
/* new-instance v3, Landroid/view/animation/AnimationSet; */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v3, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 208 */
/* .local v3, "set":Landroid/view/animation/AnimationSet; */
/* new-instance v4, Landroid/view/animation/AlphaAnimation; */
/* const v5, 0x3f4ccccd # 0.8f */
/* invoke-direct {v4, v5, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v0, v4 */
/* .line 210 */
/* .local v0, "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v3 ).addAnimation ( v0 ); // invoke-virtual {v3, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 211 */
v4 = com.android.server.wm.AppTransitionInjector.SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.Animation ) v0 ).setInterpolator ( v4 ); // invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 212 */
int v4 = -1; // const/4 v4, -0x1
(( android.view.animation.AnimationSet ) v3 ).setZAdjustment ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 213 */
(( android.view.animation.Animation ) v0 ).setDuration ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 214 */
/* move-object v0, v3 */
/* .line 215 */
} // .end local v3 # "set":Landroid/view/animation/AnimationSet;
/* .local v0, "anim":Landroid/view/animation/Animation; */
/* .line 216 */
} // .end local v0 # "anim":Landroid/view/animation/Animation;
} // :cond_0
/* new-instance v3, Landroid/view/animation/AlphaAnimation; */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v3, v0, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v0, v3 */
/* .line 218 */
/* .local v0, "alphaAnimation":Landroid/view/animation/Animation; */
/* const-wide/16 v3, 0x64 */
(( android.view.animation.Animation ) v0 ).setStartOffset ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setStartOffset(J)V
/* .line 219 */
/* new-instance v3, Lcom/android/server/wm/RadiusAnimation; */
/* int-to-float v4, v4 */
/* invoke-direct {v3, v4, p3}, Lcom/android/server/wm/RadiusAnimation;-><init>(FF)V */
/* .line 220 */
/* .local v3, "radiusAnimation":Landroid/view/animation/Animation; */
/* new-instance v4, Lcom/android/server/wm/ScaleWithClipAnimation; */
/* invoke-direct {v4, p1, p2}, Lcom/android/server/wm/ScaleWithClipAnimation;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;)V */
/* .line 221 */
/* .local v4, "scaleWithClipAnimation":Lcom/android/server/wm/ScaleWithClipAnimation; */
/* new-instance v5, Landroid/view/animation/AnimationSet; */
int v6 = 1; // const/4 v6, 0x1
/* invoke-direct {v5, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 222 */
/* .local v5, "set":Landroid/view/animation/AnimationSet; */
(( android.view.animation.AnimationSet ) v5 ).addAnimation ( v0 ); // invoke-virtual {v5, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 223 */
(( android.view.animation.AnimationSet ) v5 ).addAnimation ( v3 ); // invoke-virtual {v5, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 224 */
(( android.view.animation.AnimationSet ) v5 ).addAnimation ( v4 ); // invoke-virtual {v5, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 226 */
v7 = com.android.server.wm.AppTransitionInjector.QUART_EASE_OUT_INTERPOLATOR;
(( android.view.animation.Animation ) v0 ).setInterpolator ( v7 ); // invoke-virtual {v0, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 227 */
v7 = com.android.server.wm.AppTransitionInjector.SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.Animation ) v3 ).setInterpolator ( v7 ); // invoke-virtual {v3, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 228 */
(( com.android.server.wm.ScaleWithClipAnimation ) v4 ).setInterpolator ( v7 ); // invoke-virtual {v4, v7}, Lcom/android/server/wm/ScaleWithClipAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 230 */
(( android.view.animation.AnimationSet ) v5 ).setZAdjustment ( v6 ); // invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 232 */
/* const-wide/16 v6, 0xc8 */
(( android.view.animation.Animation ) v0 ).setDuration ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 233 */
(( android.view.animation.Animation ) v3 ).setDuration ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 234 */
(( com.android.server.wm.ScaleWithClipAnimation ) v4 ).setDuration ( v1, v2 ); // invoke-virtual {v4, v1, v2}, Lcom/android/server/wm/ScaleWithClipAnimation;->setDuration(J)V
/* .line 236 */
/* move-object v1, v5 */
/* move-object v0, v1 */
/* .line 238 */
} // .end local v3 # "radiusAnimation":Landroid/view/animation/Animation;
} // .end local v4 # "scaleWithClipAnimation":Lcom/android/server/wm/ScaleWithClipAnimation;
} // .end local v5 # "set":Landroid/view/animation/AnimationSet;
/* .local v0, "anim":Landroid/view/animation/Animation; */
} // :goto_0
} // .end method
static android.view.animation.Animation createScaleUpAnimation ( Boolean p0, android.graphics.Rect p1, android.graphics.Rect p2, Float p3 ) {
/* .locals 9 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "positionRect" # Landroid/graphics/Rect; */
/* .param p3, "radius" # F */
/* .line 165 */
int v0 = 1; // const/4 v0, 0x1
/* const/high16 v1, 0x3f800000 # 1.0f */
int v2 = 0; // const/4 v2, 0x0
/* const-wide/16 v3, 0x1f4 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 166 */
/* new-instance v5, Landroid/view/animation/AlphaAnimation; */
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct {v5, v6, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v1, v5 */
/* .line 168 */
/* .local v1, "alphaAnimation":Landroid/view/animation/Animation; */
/* new-instance v5, Lcom/android/server/wm/RadiusAnimation; */
/* int-to-float v6, v6 */
/* invoke-direct {v5, p3, v6}, Lcom/android/server/wm/RadiusAnimation;-><init>(FF)V */
/* .line 170 */
/* .local v5, "radiusAnimation":Landroid/view/animation/Animation; */
/* new-instance v6, Lcom/android/server/wm/ScaleWithClipAnimation; */
/* invoke-direct {v6, p2, p1}, Lcom/android/server/wm/ScaleWithClipAnimation;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;)V */
/* .line 172 */
/* .local v6, "scaleWithClipAnimation":Lcom/android/server/wm/ScaleWithClipAnimation; */
/* new-instance v7, Landroid/view/animation/AnimationSet; */
/* invoke-direct {v7, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* move-object v2, v7 */
/* .line 173 */
/* .local v2, "set":Landroid/view/animation/AnimationSet; */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 174 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v5 ); // invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 175 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v6 ); // invoke-virtual {v2, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 177 */
v7 = com.android.server.wm.AppTransitionInjector.QUART_EASE_OUT_INTERPOLATOR;
(( android.view.animation.Animation ) v1 ).setInterpolator ( v7 ); // invoke-virtual {v1, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 178 */
v7 = com.android.server.wm.AppTransitionInjector.SCALE_UP_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.Animation ) v5 ).setInterpolator ( v7 ); // invoke-virtual {v5, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 179 */
(( com.android.server.wm.ScaleWithClipAnimation ) v6 ).setInterpolator ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/wm/ScaleWithClipAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 181 */
(( android.view.animation.AnimationSet ) v2 ).setZAdjustment ( v0 ); // invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 183 */
/* const-wide/16 v7, 0x64 */
(( android.view.animation.Animation ) v1 ).setDuration ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 184 */
(( android.view.animation.Animation ) v5 ).setDuration ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 185 */
(( com.android.server.wm.ScaleWithClipAnimation ) v6 ).setDuration ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Lcom/android/server/wm/ScaleWithClipAnimation;->setDuration(J)V
/* .line 188 */
/* move-object v0, v2 */
/* .line 189 */
} // .end local v1 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v2 # "set":Landroid/view/animation/AnimationSet;
} // .end local v5 # "radiusAnimation":Landroid/view/animation/Animation;
} // .end local v6 # "scaleWithClipAnimation":Lcom/android/server/wm/ScaleWithClipAnimation;
/* .local v0, "anim":Landroid/view/animation/Animation; */
/* .line 190 */
} // .end local v0 # "anim":Landroid/view/animation/Animation;
} // :cond_0
/* new-instance v5, Landroid/view/animation/AnimationSet; */
/* invoke-direct {v5, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* move-object v2, v5 */
/* .line 191 */
/* .restart local v2 # "set":Landroid/view/animation/AnimationSet; */
/* new-instance v5, Landroid/view/animation/AlphaAnimation; */
/* const v6, 0x3f4ccccd # 0.8f */
/* invoke-direct {v5, v1, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v1, v5 */
/* .line 193 */
/* .restart local v1 # "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 194 */
v5 = com.android.server.wm.AppTransitionInjector.SCALE_UP_PHYSIC_BASED_INTERPOLATOR;
(( android.view.animation.Animation ) v1 ).setInterpolator ( v5 ); // invoke-virtual {v1, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 195 */
int v5 = -1; // const/4 v5, -0x1
(( android.view.animation.AnimationSet ) v2 ).setZAdjustment ( v5 ); // invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 196 */
(( android.view.animation.Animation ) v1 ).setDuration ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 197 */
/* move-object v3, v2 */
/* .line 198 */
/* .local v3, "anim":Landroid/view/animation/Animation; */
(( android.view.animation.Animation ) v3 ).setFillAfter ( v0 ); // invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setFillAfter(Z)V
/* move-object v0, v3 */
/* .line 200 */
} // .end local v1 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v2 # "set":Landroid/view/animation/AnimationSet;
} // .end local v3 # "anim":Landroid/view/animation/Animation;
/* .restart local v0 # "anim":Landroid/view/animation/Animation; */
} // :goto_0
} // .end method
static android.view.animation.Animation createTaskOpenCloseTransition ( Boolean p0, android.graphics.Rect p1, Boolean p2, com.android.server.wm.WindowManagerService p3 ) {
/* .locals 19 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "isOpenOrClose" # Z */
/* .param p3, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .line 724 */
/* move-object/from16 v0, p3 */
/* new-instance v1, Landroid/view/animation/AnimationSet; */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 725 */
/* .local v1, "set":Landroid/view/animation/AnimationSet; */
int v3 = 0; // const/4 v3, 0x0
/* .line 726 */
/* .local v3, "translateAnimation":Landroid/view/animation/Animation; */
int v4 = 0; // const/4 v4, 0x0
/* .line 727 */
/* .local v4, "scaleAnimation":Landroid/view/animation/Animation; */
v5 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->width()I */
/* .line 728 */
/* .local v5, "width":I */
v6 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
/* .line 729 */
/* .local v6, "heigth":I */
/* const-wide/16 v7, 0x96 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 730 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 731 */
/* new-instance v18, Landroid/view/animation/TranslateAnimation; */
int v10 = 1; // const/4 v10, 0x1
/* const/high16 v11, 0x3f800000 # 1.0f */
int v12 = 1; // const/4 v12, 0x1
int v13 = 0; // const/4 v13, 0x0
int v14 = 1; // const/4 v14, 0x1
int v15 = 0; // const/4 v15, 0x0
/* const/16 v16, 0x1 */
/* const/16 v17, 0x0 */
/* move-object/from16 v9, v18 */
/* invoke-direct/range {v9 ..v17}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* move-object/from16 v3, v18 */
/* .line 736 */
/* new-instance v16, Landroid/view/animation/ScaleAnimation; */
/* const v10, 0x3f666666 # 0.9f */
/* const v12, 0x3f666666 # 0.9f */
/* const/high16 v13, 0x3f800000 # 1.0f */
/* div-int/lit8 v9, v5, 0x2 */
/* int-to-float v14, v9 */
/* div-int/lit8 v9, v6, 0x2 */
/* int-to-float v15, v9 */
/* move-object/from16 v9, v16 */
/* invoke-direct/range {v9 ..v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V */
/* move-object/from16 v4, v16 */
/* .line 737 */
(( android.view.animation.Animation ) v4 ).setStartOffset ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Landroid/view/animation/Animation;->setStartOffset(J)V
/* goto/16 :goto_0 */
/* .line 739 */
} // :cond_0
/* new-instance v7, Landroid/view/animation/ScaleAnimation; */
/* const/high16 v10, 0x3f800000 # 1.0f */
/* const v11, 0x3f666666 # 0.9f */
/* const/high16 v12, 0x3f800000 # 1.0f */
/* const v13, 0x3f666666 # 0.9f */
/* div-int/lit8 v8, v5, 0x2 */
/* int-to-float v14, v8 */
/* div-int/lit8 v8, v6, 0x2 */
/* int-to-float v15, v8 */
/* move-object v9, v7 */
/* invoke-direct/range {v9 ..v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V */
/* move-object v4, v7 */
/* .line 740 */
/* new-instance v16, Landroid/view/animation/TranslateAnimation; */
int v8 = 1; // const/4 v8, 0x1
int v9 = 0; // const/4 v9, 0x0
int v10 = 1; // const/4 v10, 0x1
/* const/high16 v11, -0x40800000 # -1.0f */
int v12 = 1; // const/4 v12, 0x1
int v13 = 0; // const/4 v13, 0x0
int v14 = 1; // const/4 v14, 0x1
int v15 = 0; // const/4 v15, 0x0
/* move-object/from16 v7, v16 */
/* invoke-direct/range {v7 ..v15}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* move-object/from16 v3, v16 */
/* goto/16 :goto_0 */
/* .line 746 */
} // :cond_1
if ( p0 != null) { // if-eqz p0, :cond_2
/* .line 747 */
/* new-instance v18, Landroid/view/animation/TranslateAnimation; */
int v10 = 1; // const/4 v10, 0x1
/* const/high16 v11, -0x40800000 # -1.0f */
int v12 = 1; // const/4 v12, 0x1
int v13 = 0; // const/4 v13, 0x0
int v14 = 1; // const/4 v14, 0x1
int v15 = 0; // const/4 v15, 0x0
/* const/16 v16, 0x1 */
/* const/16 v17, 0x0 */
/* move-object/from16 v9, v18 */
/* invoke-direct/range {v9 ..v17}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* move-object/from16 v3, v18 */
/* .line 751 */
/* new-instance v16, Landroid/view/animation/ScaleAnimation; */
/* const v10, 0x3f666666 # 0.9f */
/* const/high16 v11, 0x3f800000 # 1.0f */
/* const v12, 0x3f666666 # 0.9f */
/* const/high16 v13, 0x3f800000 # 1.0f */
/* div-int/lit8 v9, v5, 0x2 */
/* int-to-float v14, v9 */
/* div-int/lit8 v9, v6, 0x2 */
/* int-to-float v15, v9 */
/* move-object/from16 v9, v16 */
/* invoke-direct/range {v9 ..v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V */
/* move-object/from16 v4, v16 */
/* .line 752 */
(( android.view.animation.Animation ) v4 ).setStartOffset ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Landroid/view/animation/Animation;->setStartOffset(J)V
/* .line 754 */
} // :cond_2
v7 = this.mContext;
v7 = com.android.server.wm.AppTransitionInjector .getNavigationBarMode ( v7 );
/* if-nez v7, :cond_3 */
v7 = this.mContext;
/* .line 755 */
(( android.content.Context ) v7 ).getResources ( ); // invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v7 ).getConfiguration ( ); // invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* iget v7, v7, Landroid/content/res/Configuration;->orientation:I */
int v8 = 2; // const/4 v8, 0x2
/* if-ne v7, v8, :cond_3 */
/* .line 756 */
/* new-instance v7, Landroid/view/animation/ScaleAnimation; */
/* const/high16 v10, 0x3f800000 # 1.0f */
/* const v11, 0x3f666666 # 0.9f */
/* const/high16 v12, 0x3f800000 # 1.0f */
/* const v13, 0x3f666666 # 0.9f */
/* div-int/lit8 v8, v5, 0x2 */
/* int-to-float v14, v8 */
/* div-int/lit8 v8, v6, 0x2 */
/* int-to-float v15, v8 */
/* move-object v9, v7 */
/* invoke-direct/range {v9 ..v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V */
/* move-object v4, v7 */
/* .line 757 */
/* new-instance v16, Landroid/view/animation/TranslateAnimation; */
int v8 = 1; // const/4 v8, 0x1
int v9 = 0; // const/4 v9, 0x0
int v10 = 1; // const/4 v10, 0x1
/* const v11, 0x3f8ccccd # 1.1f */
int v12 = 1; // const/4 v12, 0x1
int v13 = 0; // const/4 v13, 0x0
int v14 = 1; // const/4 v14, 0x1
int v15 = 0; // const/4 v15, 0x0
/* move-object/from16 v7, v16 */
/* invoke-direct/range {v7 ..v15}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* move-object/from16 v3, v16 */
/* .line 762 */
} // :cond_3
/* new-instance v14, Landroid/view/animation/ScaleAnimation; */
/* const/high16 v8, 0x3f800000 # 1.0f */
/* const v9, 0x3f666666 # 0.9f */
/* const/high16 v10, 0x3f800000 # 1.0f */
/* const v11, 0x3f666666 # 0.9f */
/* div-int/lit8 v7, v5, 0x2 */
/* int-to-float v12, v7 */
/* div-int/lit8 v7, v6, 0x2 */
/* int-to-float v13, v7 */
/* move-object v7, v14 */
/* invoke-direct/range {v7 ..v13}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V */
/* move-object v4, v14 */
/* .line 763 */
/* new-instance v16, Landroid/view/animation/TranslateAnimation; */
int v8 = 1; // const/4 v8, 0x1
int v9 = 0; // const/4 v9, 0x0
int v10 = 1; // const/4 v10, 0x1
/* const/high16 v11, 0x3f800000 # 1.0f */
int v12 = 1; // const/4 v12, 0x1
int v13 = 0; // const/4 v13, 0x0
int v14 = 1; // const/4 v14, 0x1
int v15 = 0; // const/4 v15, 0x0
/* move-object/from16 v7, v16 */
/* invoke-direct/range {v7 ..v15}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V */
/* move-object/from16 v3, v16 */
/* .line 770 */
} // :goto_0
/* new-instance v7, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator; */
/* const v8, 0x3f19999a # 0.6f */
/* const v9, 0x3f7d70a4 # 0.99f */
/* invoke-direct {v7, v8, v9}, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;-><init>(FF)V */
/* .line 771 */
(( android.view.animation.AnimationSet ) v1 ).addAnimation ( v4 ); // invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 772 */
(( android.view.animation.AnimationSet ) v1 ).addAnimation ( v3 ); // invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 773 */
v7 = com.android.server.wm.AppTransitionInjector.sActivityTransitionInterpolator;
(( android.view.animation.AnimationSet ) v1 ).setInterpolator ( v7 ); // invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 774 */
/* const-wide/16 v7, 0x28a */
(( android.view.animation.AnimationSet ) v1 ).setDuration ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Landroid/view/animation/AnimationSet;->setDuration(J)V
/* .line 775 */
(( android.view.animation.AnimationSet ) v1 ).setHasRoundedCorners ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V
/* .line 776 */
} // .end method
static android.view.animation.Animation createTransitionAnimation ( Boolean p0, android.graphics.Rect p1, android.graphics.Rect p2, android.graphics.Rect p3, Integer p4, android.graphics.Point p5 ) {
/* .locals 40 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "targetPositionRect" # Landroid/graphics/Rect; */
/* .param p3, "startRect" # Landroid/graphics/Rect; */
/* .param p4, "orientation" # I */
/* .param p5, "inertia" # Landroid/graphics/Point; */
/* .line 594 */
/* move-object/from16 v0, p1 */
/* move-object/from16 v1, p3 */
/* new-instance v2, Landroid/graphics/Rect; */
/* move-object/from16 v3, p2 */
/* invoke-direct {v2, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V */
/* .line 595 */
/* .local v2, "positionRect":Landroid/graphics/Rect; */
int v5 = 1; // const/4 v5, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
v6 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->isEmpty()Z */
/* if-nez v6, :cond_0 */
/* move v6, v5 */
} // :cond_0
int v6 = 0; // const/4 v6, 0x0
/* .line 596 */
/* .local v6, "hasStartRect":Z */
} // :goto_0
/* move/from16 v7, p4 */
/* if-ne v7, v5, :cond_1 */
/* move v8, v5 */
} // :cond_1
int v8 = 0; // const/4 v8, 0x0
/* .line 597 */
/* .local v8, "isPortrait":Z */
} // :goto_1
v15 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->width()I */
/* .line 598 */
/* .local v15, "appWidth":I */
v14 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
/* .line 600 */
/* .local v14, "appHeight":I */
v9 = (( android.graphics.Rect ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z
/* xor-int/2addr v9, v5 */
/* move/from16 v23, v9 */
/* .line 601 */
/* .local v23, "canFindPosition":Z */
v9 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v13, v9, Landroid/graphics/Rect;->left:I */
/* .line 602 */
/* .local v13, "insetLeft":I */
v9 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v12, v9, Landroid/graphics/Rect;->top:I */
/* .line 603 */
/* .local v12, "insetTop":I */
v9 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v11, v9, Landroid/graphics/Rect;->right:I */
/* .line 604 */
/* .local v11, "insetRight":I */
v9 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
/* iget v10, v9, Landroid/graphics/Rect;->bottom:I */
/* .line 606 */
/* .local v10, "insetBottom":I */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 607 */
/* iget v9, v0, Landroid/graphics/Rect;->left:I */
/* neg-int v9, v9 */
/* iget v4, v0, Landroid/graphics/Rect;->top:I */
/* neg-int v4, v4 */
(( android.graphics.Rect ) v1 ).offset ( v9, v4 ); // invoke-virtual {v1, v9, v4}, Landroid/graphics/Rect;->offset(II)V
/* .line 610 */
} // :cond_2
if ( v23 != null) { // if-eqz v23, :cond_3
/* .line 611 */
/* iget v4, v0, Landroid/graphics/Rect;->left:I */
/* neg-int v4, v4 */
/* iget v9, v0, Landroid/graphics/Rect;->top:I */
/* neg-int v9, v9 */
(( android.graphics.Rect ) v2 ).offset ( v4, v9 ); // invoke-virtual {v2, v4, v9}, Landroid/graphics/Rect;->offset(II)V
/* .line 614 */
} // :cond_3
/* const v4, 0x3ecccccd # 0.4f */
if ( v23 != null) { // if-eqz v23, :cond_4
v9 = (( android.graphics.Rect ) v2 ).width ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->width()I
/* add-int v17, v13, v11 */
/* sub-int v9, v9, v17 */
/* .line 615 */
} // :cond_4
/* int-to-float v9, v15 */
/* mul-float/2addr v9, v4 */
/* float-to-int v9, v9 */
} // :goto_2
/* nop */
/* .line 616 */
/* .local v9, "targetWidth":I */
if ( v23 != null) { // if-eqz v23, :cond_5
v17 = (( android.graphics.Rect ) v2 ).height ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->height()I
/* add-int v18, v12, v10 */
/* sub-int v17, v17, v18 */
/* .line 617 */
} // :cond_5
/* int-to-float v5, v14 */
/* mul-float/2addr v5, v4 */
/* float-to-int v5, v5 */
/* move/from16 v17, v5 */
} // :goto_3
/* move/from16 v5, v17 */
/* .line 619 */
/* .local v5, "targetHeight":I */
if ( v6 != null) { // if-eqz v6, :cond_6
/* iget v4, v1, Landroid/graphics/Rect;->left:I */
} // :cond_6
int v4 = 0; // const/4 v4, 0x0
/* .line 620 */
/* .local v4, "startX":I */
} // :goto_4
if ( v6 != null) { // if-eqz v6, :cond_7
/* iget v3, v1, Landroid/graphics/Rect;->top:I */
} // :cond_7
int v3 = 0; // const/4 v3, 0x0
/* .line 621 */
/* .local v3, "startY":I */
} // :goto_5
if ( v6 != null) { // if-eqz v6, :cond_8
v18 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->width()I */
/* div-int/lit8 v18, v18, 0x2 */
/* add-int v18, v4, v18 */
/* .line 622 */
} // :cond_8
/* iget v1, v0, Landroid/graphics/Rect;->left:I */
v18 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->width()I */
/* div-int/lit8 v18, v18, 0x2 */
/* add-int v18, v1, v18 */
} // :goto_6
/* move/from16 v1, v18 */
/* .line 623 */
/* .local v1, "startCenterX":I */
if ( v6 != null) { // if-eqz v6, :cond_9
v18 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->height()I */
/* div-int/lit8 v18, v18, 0x2 */
/* add-int v18, v3, v18 */
/* .line 624 */
} // :cond_9
/* iget v7, v0, Landroid/graphics/Rect;->top:I */
v18 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
/* div-int/lit8 v18, v18, 0x2 */
/* add-int v18, v7, v18 */
} // :goto_7
/* move/from16 v7, v18 */
/* .line 625 */
/* .local v7, "startCenterY":I */
/* const v18, 0x3f19999a # 0.6f */
/* const/high16 v19, 0x40000000 # 2.0f */
if ( v23 != null) { // if-eqz v23, :cond_a
/* move/from16 v20, v10 */
} // .end local v10 # "insetBottom":I
/* .local v20, "insetBottom":I */
/* iget v10, v2, Landroid/graphics/Rect;->left:I */
/* add-int/2addr v10, v13 */
/* .line 626 */
} // .end local v20 # "insetBottom":I
/* .restart local v10 # "insetBottom":I */
} // :cond_a
/* move/from16 v20, v10 */
} // .end local v10 # "insetBottom":I
/* .restart local v20 # "insetBottom":I */
/* int-to-float v10, v15 */
/* mul-float v10, v10, v18 */
/* div-float v10, v10, v19 */
/* float-to-int v10, v10 */
} // :goto_8
/* nop */
/* .line 627 */
/* .local v10, "targetX":I */
if ( v23 != null) { // if-eqz v23, :cond_b
/* move/from16 v21, v11 */
} // .end local v11 # "insetRight":I
/* .local v21, "insetRight":I */
/* iget v11, v2, Landroid/graphics/Rect;->top:I */
/* add-int/2addr v11, v12 */
/* .line 628 */
} // .end local v21 # "insetRight":I
/* .restart local v11 # "insetRight":I */
} // :cond_b
/* move/from16 v21, v11 */
} // .end local v11 # "insetRight":I
/* .restart local v21 # "insetRight":I */
/* int-to-float v11, v14 */
/* mul-float v11, v11, v18 */
/* div-float v11, v11, v19 */
/* float-to-int v11, v11 */
} // :goto_9
/* nop */
/* .line 630 */
/* .local v11, "targetY":I */
/* move/from16 v24, v7 */
} // .end local v7 # "startCenterY":I
/* .local v24, "startCenterY":I */
if ( v6 != null) { // if-eqz v6, :cond_c
/* .line 631 */
v7 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->width()I */
/* int-to-float v7, v7 */
/* move/from16 v25, v1 */
} // .end local v1 # "startCenterX":I
/* .local v25, "startCenterX":I */
/* int-to-float v1, v15 */
/* div-float v1, v7, v1 */
} // .end local v25 # "startCenterX":I
/* .restart local v1 # "startCenterX":I */
} // :cond_c
/* move/from16 v25, v1 */
} // .end local v1 # "startCenterX":I
/* .restart local v25 # "startCenterX":I */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* .line 632 */
/* .local v1, "startScaleX":F */
} // :goto_a
if ( v6 != null) { // if-eqz v6, :cond_d
/* .line 633 */
v7 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->height()I */
/* int-to-float v7, v7 */
/* move/from16 v26, v6 */
} // .end local v6 # "hasStartRect":Z
/* .local v26, "hasStartRect":Z */
/* int-to-float v6, v14 */
/* div-float v6, v7, v6 */
} // .end local v26 # "hasStartRect":Z
/* .restart local v6 # "hasStartRect":Z */
} // :cond_d
/* move/from16 v26, v6 */
} // .end local v6 # "hasStartRect":Z
/* .restart local v26 # "hasStartRect":Z */
/* const/high16 v6, 0x3f800000 # 1.0f */
/* .line 634 */
/* .local v6, "startScaleY":F */
} // :goto_b
/* int-to-float v7, v9 */
/* move/from16 v22, v12 */
} // .end local v12 # "insetTop":I
/* .local v22, "insetTop":I */
/* int-to-float v12, v15 */
/* div-float/2addr v7, v12 */
/* .line 635 */
/* .local v7, "scaleX":F */
/* int-to-float v12, v5 */
/* move/from16 v27, v5 */
} // .end local v5 # "targetHeight":I
/* .local v27, "targetHeight":I */
/* int-to-float v5, v14 */
/* div-float v5, v12, v5 */
/* .line 638 */
/* .local v5, "scaleY":F */
/* move/from16 v28, v13 */
} // .end local v13 # "insetLeft":I
/* .local v28, "insetLeft":I */
if ( p0 != null) { // if-eqz p0, :cond_f
/* .line 639 */
/* new-instance v12, Landroid/view/animation/AnimationSet; */
int v13 = 1; // const/4 v13, 0x1
/* invoke-direct {v12, v13}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 640 */
/* .local v12, "set":Landroid/view/animation/AnimationSet; */
/* new-instance v13, Landroid/view/animation/ScaleAnimation; */
/* const v32, 0x3f4ccccd # 0.8f */
/* const/high16 v33, 0x3f800000 # 1.0f */
/* const v34, 0x3f4ccccd # 0.8f */
/* const/high16 v35, 0x3f800000 # 1.0f */
/* move/from16 v38, v9 */
} // .end local v9 # "targetWidth":I
/* .local v38, "targetWidth":I */
/* int-to-float v9, v15 */
/* div-float v36, v9, v19 */
/* int-to-float v9, v14 */
/* div-float v37, v9, v19 */
/* move-object/from16 v31, v13 */
/* invoke-direct/range {v31 ..v37}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V */
/* move-object v9, v13 */
/* .line 644 */
/* .local v9, "scaleAnimation":Landroid/view/animation/Animation; */
/* move/from16 v19, v14 */
/* const-wide/16 v13, 0x12c */
} // .end local v14 # "appHeight":I
/* .local v19, "appHeight":I */
(( android.view.animation.Animation ) v9 ).setDuration ( v13, v14 ); // invoke-virtual {v9, v13, v14}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 645 */
/* sget-boolean v13, Lcom/android/server/wm/AppTransitionInjector;->IS_E10:Z */
/* if-nez v13, :cond_e */
/* .line 646 */
(( android.view.animation.AnimationSet ) v12 ).addAnimation ( v9 ); // invoke-virtual {v12, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 648 */
} // :cond_e
v13 = com.android.server.wm.AppTransitionInjector.CUBIC_EASE_OUT_INTERPOLATOR;
(( android.view.animation.AnimationSet ) v12 ).setInterpolator ( v13 ); // invoke-virtual {v12, v13}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 649 */
int v13 = -1; // const/4 v13, -0x1
(( android.view.animation.AnimationSet ) v12 ).setZAdjustment ( v13 ); // invoke-virtual {v12, v13}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 650 */
/* move-object v9, v12 */
/* .line 651 */
} // .end local v12 # "set":Landroid/view/animation/AnimationSet;
/* .local v9, "anim":Landroid/view/animation/Animation; */
/* move/from16 v17, v1 */
/* move-object/from16 v37, v2 */
/* move v12, v4 */
/* move/from16 v39, v7 */
/* move v7, v10 */
/* move v0, v11 */
/* move/from16 v33, v20 */
/* move/from16 v34, v21 */
/* move/from16 v35, v22 */
/* move/from16 v10, v24 */
/* move/from16 v36, v25 */
/* move/from16 v25, v38 */
/* move/from16 v38, v15 */
/* move v15, v3 */
/* move/from16 v3, v19 */
/* goto/16 :goto_f */
} // .end local v19 # "appHeight":I
} // .end local v38 # "targetWidth":I
/* .local v9, "targetWidth":I */
/* .restart local v14 # "appHeight":I */
} // :cond_f
/* move/from16 v38, v9 */
/* move/from16 v19, v14 */
} // .end local v9 # "targetWidth":I
} // .end local v14 # "appHeight":I
/* .restart local v19 # "appHeight":I */
/* .restart local v38 # "targetWidth":I */
int v9 = 0; // const/4 v9, 0x0
if ( v23 != null) { // if-eqz v23, :cond_13
/* .line 652 */
/* new-instance v12, Landroid/view/animation/AnimationSet; */
int v13 = 0; // const/4 v13, 0x0
/* invoke-direct {v12, v13}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* move-object v14, v12 */
/* .line 653 */
/* .local v14, "set":Landroid/view/animation/AnimationSet; */
/* new-instance v12, Landroid/view/animation/AlphaAnimation; */
/* const/high16 v13, 0x3f800000 # 1.0f */
/* invoke-direct {v12, v13, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v13, v12 */
/* .line 654 */
/* .local v13, "alphaAnimation":Landroid/view/animation/Animation; */
/* move-object/from16 v17, v14 */
/* move/from16 v16, v15 */
} // .end local v14 # "set":Landroid/view/animation/AnimationSet;
} // .end local v15 # "appWidth":I
/* .local v16, "appWidth":I */
/* .local v17, "set":Landroid/view/animation/AnimationSet; */
/* const-wide/16 v14, 0x28 */
(( android.view.animation.Animation ) v13 ).setStartOffset ( v14, v15 ); // invoke-virtual {v13, v14, v15}, Landroid/view/animation/Animation;->setStartOffset(J)V
/* .line 655 */
/* const-wide/16 v14, 0xd2 */
(( android.view.animation.Animation ) v13 ).setDuration ( v14, v15 ); // invoke-virtual {v13, v14, v15}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 656 */
v9 = com.android.server.wm.AppTransitionInjector.CUBIC_EASE_OUT_INTERPOLATOR;
(( android.view.animation.Animation ) v13 ).setInterpolator ( v9 ); // invoke-virtual {v13, v9}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 660 */
/* if-nez p5, :cond_11 */
/* .line 661 */
/* new-instance v12, Landroid/view/animation/TranslateXAnimation; */
/* int-to-float v14, v4 */
/* iget v15, v0, Landroid/graphics/Rect;->left:I */
/* add-int/2addr v15, v10 */
/* int-to-float v15, v15 */
/* invoke-direct {v12, v14, v15}, Landroid/view/animation/TranslateXAnimation;-><init>(FF)V */
/* .line 662 */
/* .local v12, "translateXAnimation":Landroid/view/animation/Animation; */
/* new-instance v14, Landroid/view/animation/TranslateYAnimation; */
/* int-to-float v15, v3 */
/* move-object/from16 v18, v9 */
/* iget v9, v0, Landroid/graphics/Rect;->top:I */
/* add-int/2addr v9, v11 */
/* int-to-float v9, v9 */
/* invoke-direct {v14, v15, v9}, Landroid/view/animation/TranslateYAnimation;-><init>(FF)V */
/* move-object v9, v14 */
/* .line 664 */
/* .local v9, "translateYAnimation":Landroid/view/animation/Animation; */
/* new-instance v14, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation; */
/* invoke-direct {v14, v1, v7}, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;-><init>(FF)V */
/* .line 665 */
/* .local v14, "scaleXAnimation":Landroid/view/animation/Animation; */
if ( v8 != null) { // if-eqz v8, :cond_10
/* move-object/from16 v15, v18 */
/* .line 666 */
} // :cond_10
v15 = com.android.server.wm.AppTransitionInjector.QUINT_EASE_OUT_INTERPOLATOR;
/* .line 665 */
} // :goto_c
(( android.view.animation.Animation ) v14 ).setInterpolator ( v15 ); // invoke-virtual {v14, v15}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 667 */
/* move v15, v10 */
/* move/from16 v18, v11 */
/* const-wide/16 v10, 0x12c */
} // .end local v10 # "targetX":I
} // .end local v11 # "targetY":I
/* .local v15, "targetX":I */
/* .local v18, "targetY":I */
(( android.view.animation.Animation ) v14 ).setDuration ( v10, v11 ); // invoke-virtual {v14, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 669 */
/* new-instance v10, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation; */
/* invoke-direct {v10, v6, v5}, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;-><init>(FF)V */
/* .line 670 */
/* .local v10, "scaleYAnimation":Landroid/view/animation/Animation; */
v11 = com.android.server.wm.AppTransitionInjector.QUART_EASE_OUT_INTERPOLATOR;
(( android.view.animation.Animation ) v10 ).setInterpolator ( v11 ); // invoke-virtual {v10, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 671 */
/* move/from16 v32, v3 */
/* move/from16 v31, v4 */
/* const-wide/16 v3, 0x12c */
} // .end local v3 # "startY":I
} // .end local v4 # "startX":I
/* .local v31, "startX":I */
/* .local v32, "startY":I */
(( android.view.animation.Animation ) v10 ).setDuration ( v3, v4 ); // invoke-virtual {v10, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 672 */
(( android.view.animation.Animation ) v12 ).setDuration ( v3, v4 ); // invoke-virtual {v12, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 673 */
(( android.view.animation.Animation ) v9 ).setDuration ( v3, v4 ); // invoke-virtual {v9, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 674 */
/* move-object/from16 v3, v17 */
} // .end local v17 # "set":Landroid/view/animation/AnimationSet;
/* .local v3, "set":Landroid/view/animation/AnimationSet; */
(( android.view.animation.AnimationSet ) v3 ).addAnimation ( v14 ); // invoke-virtual {v3, v14}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 675 */
(( android.view.animation.AnimationSet ) v3 ).addAnimation ( v10 ); // invoke-virtual {v3, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 676 */
} // .end local v10 # "scaleYAnimation":Landroid/view/animation/Animation;
} // .end local v14 # "scaleXAnimation":Landroid/view/animation/Animation;
/* move-object/from16 v37, v2 */
/* move-object v4, v3 */
/* move/from16 v39, v7 */
/* move-object v2, v13 */
/* move v7, v15 */
/* move/from16 v0, v18 */
/* move/from16 v3, v19 */
/* move/from16 v33, v20 */
/* move/from16 v34, v21 */
/* move/from16 v35, v22 */
/* move/from16 v10, v24 */
/* move/from16 v36, v25 */
/* move/from16 v25, v38 */
/* move/from16 v38, v16 */
/* goto/16 :goto_d */
/* .line 677 */
} // .end local v9 # "translateYAnimation":Landroid/view/animation/Animation;
} // .end local v12 # "translateXAnimation":Landroid/view/animation/Animation;
} // .end local v15 # "targetX":I
} // .end local v18 # "targetY":I
} // .end local v31 # "startX":I
} // .end local v32 # "startY":I
/* .local v3, "startY":I */
/* .restart local v4 # "startX":I */
/* .local v10, "targetX":I */
/* .restart local v11 # "targetY":I */
/* .restart local v17 # "set":Landroid/view/animation/AnimationSet; */
} // :cond_11
/* move/from16 v32, v3 */
/* move/from16 v31, v4 */
/* move v15, v10 */
/* move/from16 v18, v11 */
/* move-object/from16 v3, v17 */
} // .end local v4 # "startX":I
} // .end local v10 # "targetX":I
} // .end local v11 # "targetY":I
} // .end local v17 # "set":Landroid/view/animation/AnimationSet;
/* .local v3, "set":Landroid/view/animation/AnimationSet; */
/* .restart local v15 # "targetX":I */
/* .restart local v18 # "targetY":I */
/* .restart local v31 # "startX":I */
/* .restart local v32 # "startY":I */
/* iget v4, v2, Landroid/graphics/Rect;->left:I */
/* add-int v4, v4, v28 */
/* div-int/lit8 v9, v38, 0x2 */
/* add-int/2addr v4, v9 */
/* int-to-float v4, v4 */
/* .line 678 */
/* .local v4, "endCenterX":F */
/* iget v9, v2, Landroid/graphics/Rect;->top:I */
/* add-int v9, v9, v22 */
/* div-int/lit8 v10, v27, 0x2 */
/* add-int/2addr v9, v10 */
/* int-to-float v14, v9 */
/* .line 679 */
/* .local v14, "endCenterY":F */
/* new-instance v17, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation; */
/* move/from16 v12, v25 */
} // .end local v25 # "startCenterX":I
/* .local v12, "startCenterX":I */
/* int-to-float v11, v12 */
/* move/from16 v25, v38 */
} // .end local v38 # "targetWidth":I
/* .local v25, "targetWidth":I */
/* move-object/from16 v9, v17 */
/* move/from16 v33, v20 */
} // .end local v20 # "insetBottom":I
/* .local v33, "insetBottom":I */
/* move v10, v1 */
/* move/from16 v0, v18 */
/* move/from16 v34, v21 */
/* move/from16 v18, v11 */
} // .end local v18 # "targetY":I
} // .end local v21 # "insetRight":I
/* .local v0, "targetY":I */
/* .local v34, "insetRight":I */
/* move v11, v7 */
/* move/from16 v36, v12 */
/* move/from16 v35, v22 */
} // .end local v12 # "startCenterX":I
} // .end local v22 # "insetTop":I
/* .local v35, "insetTop":I */
/* .local v36, "startCenterX":I */
/* move/from16 v12, v18 */
/* move-object/from16 v37, v2 */
/* move-object v2, v13 */
} // .end local v13 # "alphaAnimation":Landroid/view/animation/Animation;
/* .local v2, "alphaAnimation":Landroid/view/animation/Animation; */
/* .local v37, "positionRect":Landroid/graphics/Rect; */
/* move v13, v4 */
/* move/from16 v29, v4 */
/* move/from16 v30, v14 */
/* move-object v4, v3 */
/* move/from16 v3, v19 */
} // .end local v14 # "endCenterY":F
} // .end local v19 # "appHeight":I
/* .local v3, "appHeight":I */
/* .local v4, "set":Landroid/view/animation/AnimationSet; */
/* .local v29, "endCenterX":F */
/* .local v30, "endCenterY":F */
/* move-object/from16 v14, p5 */
/* move/from16 v39, v7 */
/* move v7, v15 */
/* move/from16 v38, v16 */
} // .end local v15 # "targetX":I
} // .end local v16 # "appWidth":I
/* .local v7, "targetX":I */
/* .local v38, "appWidth":I */
/* .local v39, "scaleX":F */
/* move/from16 v15, v38 */
/* invoke-direct/range {v9 ..v15}, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;-><init>(FFFFLandroid/graphics/Point;I)V */
/* move-object/from16 v12, v17 */
/* .line 681 */
/* .local v12, "translateXAnimation":Landroid/view/animation/Animation; */
/* new-instance v9, Lcom/android/server/wm/AppTransitionInjector$BezierYAnimation; */
/* move/from16 v10, v24 */
} // .end local v24 # "startCenterY":I
/* .local v10, "startCenterY":I */
/* int-to-float v11, v10 */
/* move-object/from16 v16, v9 */
/* move/from16 v17, v6 */
/* move/from16 v18, v5 */
/* move/from16 v19, v11 */
/* move/from16 v20, v30 */
/* move-object/from16 v21, p5 */
/* move/from16 v22, v3 */
/* invoke-direct/range {v16 ..v22}, Lcom/android/server/wm/AppTransitionInjector$BezierYAnimation;-><init>(FFFFLandroid/graphics/Point;I)V */
/* .line 683 */
/* .restart local v9 # "translateYAnimation":Landroid/view/animation/Animation; */
/* const-wide/16 v13, 0x190 */
(( android.view.animation.Animation ) v12 ).setDuration ( v13, v14 ); // invoke-virtual {v12, v13, v14}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 684 */
(( android.view.animation.Animation ) v9 ).setDuration ( v13, v14 ); // invoke-virtual {v9, v13, v14}, Landroid/view/animation/Animation;->setDuration(J)V
/* .line 687 */
} // .end local v29 # "endCenterX":F
} // .end local v30 # "endCenterY":F
} // :goto_d
if ( v8 != null) { // if-eqz v8, :cond_12
v11 = com.android.server.wm.AppTransitionInjector.QUART_EASE_OUT_INTERPOLATOR;
/* .line 688 */
} // :cond_12
v11 = com.android.server.wm.AppTransitionInjector.QUINT_EASE_OUT_INTERPOLATOR;
/* .line 687 */
} // :goto_e
(( android.view.animation.Animation ) v12 ).setInterpolator ( v11 ); // invoke-virtual {v12, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 689 */
v11 = com.android.server.wm.AppTransitionInjector.QUART_EASE_OUT_INTERPOLATOR;
(( android.view.animation.Animation ) v9 ).setInterpolator ( v11 ); // invoke-virtual {v9, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 691 */
(( android.view.animation.AnimationSet ) v4 ).addAnimation ( v2 ); // invoke-virtual {v4, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 692 */
(( android.view.animation.AnimationSet ) v4 ).addAnimation ( v12 ); // invoke-virtual {v4, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 693 */
(( android.view.animation.AnimationSet ) v4 ).addAnimation ( v9 ); // invoke-virtual {v4, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 694 */
int v11 = 1; // const/4 v11, 0x1
(( android.view.animation.AnimationSet ) v4 ).setZAdjustment ( v11 ); // invoke-virtual {v4, v11}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 695 */
/* move-object v9, v4 */
/* .line 696 */
} // .end local v2 # "alphaAnimation":Landroid/view/animation/Animation;
} // .end local v4 # "set":Landroid/view/animation/AnimationSet;
} // .end local v12 # "translateXAnimation":Landroid/view/animation/Animation;
/* .local v9, "anim":Landroid/view/animation/Animation; */
/* move/from16 v17, v1 */
/* move/from16 v12, v31 */
/* move/from16 v15, v32 */
/* .line 697 */
} // .end local v0 # "targetY":I
} // .end local v9 # "anim":Landroid/view/animation/Animation;
} // .end local v31 # "startX":I
} // .end local v32 # "startY":I
} // .end local v33 # "insetBottom":I
} // .end local v34 # "insetRight":I
} // .end local v35 # "insetTop":I
} // .end local v36 # "startCenterX":I
} // .end local v37 # "positionRect":Landroid/graphics/Rect;
} // .end local v39 # "scaleX":F
/* .local v2, "positionRect":Landroid/graphics/Rect; */
/* .local v3, "startY":I */
/* .local v4, "startX":I */
/* .local v7, "scaleX":F */
/* .local v10, "targetX":I */
/* .restart local v11 # "targetY":I */
/* .local v15, "appWidth":I */
/* .restart local v19 # "appHeight":I */
/* .restart local v20 # "insetBottom":I */
/* .restart local v21 # "insetRight":I */
/* .restart local v22 # "insetTop":I */
/* .restart local v24 # "startCenterY":I */
/* .local v25, "startCenterX":I */
/* .local v38, "targetWidth":I */
} // :cond_13
/* move-object/from16 v37, v2 */
/* move/from16 v32, v3 */
/* move/from16 v31, v4 */
/* move/from16 v39, v7 */
/* move v7, v10 */
/* move v0, v11 */
/* move/from16 v3, v19 */
/* move/from16 v33, v20 */
/* move/from16 v34, v21 */
/* move/from16 v35, v22 */
/* move/from16 v10, v24 */
/* move/from16 v36, v25 */
/* move/from16 v25, v38 */
int v11 = 1; // const/4 v11, 0x1
/* move/from16 v38, v15 */
} // .end local v2 # "positionRect":Landroid/graphics/Rect;
} // .end local v4 # "startX":I
} // .end local v11 # "targetY":I
} // .end local v15 # "appWidth":I
} // .end local v19 # "appHeight":I
} // .end local v20 # "insetBottom":I
} // .end local v21 # "insetRight":I
} // .end local v22 # "insetTop":I
} // .end local v24 # "startCenterY":I
/* .restart local v0 # "targetY":I */
/* .local v3, "appHeight":I */
/* .local v7, "targetX":I */
/* .local v10, "startCenterY":I */
/* .local v25, "targetWidth":I */
/* .restart local v31 # "startX":I */
/* .restart local v32 # "startY":I */
/* .restart local v33 # "insetBottom":I */
/* .restart local v34 # "insetRight":I */
/* .restart local v35 # "insetTop":I */
/* .restart local v36 # "startCenterX":I */
/* .restart local v37 # "positionRect":Landroid/graphics/Rect; */
/* .local v38, "appWidth":I */
/* .restart local v39 # "scaleX":F */
/* new-instance v2, Landroid/view/animation/AnimationSet; */
/* invoke-direct {v2, v11}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 698 */
/* .local v2, "set":Landroid/view/animation/AnimationSet; */
/* new-instance v4, Landroid/view/animation/ScaleAnimation; */
/* const v11, 0x3ecccccd # 0.4f */
/* invoke-direct {v4, v1, v11, v6, v11}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V */
/* .line 701 */
/* .local v4, "scaleAnimation":Landroid/view/animation/Animation; */
/* new-instance v11, Landroid/view/animation/TranslateAnimation; */
/* move/from16 v12, v31 */
} // .end local v31 # "startX":I
/* .local v12, "startX":I */
/* int-to-float v13, v12 */
/* int-to-float v14, v7 */
/* move/from16 v15, v32 */
} // .end local v32 # "startY":I
/* .local v15, "startY":I */
/* int-to-float v9, v15 */
/* move/from16 v17, v1 */
} // .end local v1 # "startScaleX":F
/* .local v17, "startScaleX":F */
/* int-to-float v1, v0 */
/* invoke-direct {v11, v13, v14, v9, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V */
/* move-object v1, v11 */
/* .line 702 */
/* .local v1, "translateAnimation":Landroid/view/animation/Animation; */
/* new-instance v9, Landroid/view/animation/AlphaAnimation; */
int v11 = 0; // const/4 v11, 0x0
/* const/high16 v13, 0x3f800000 # 1.0f */
/* invoke-direct {v9, v13, v11}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* .line 704 */
/* .local v9, "alphaAnimation":Landroid/view/animation/Animation; */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v4 ); // invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 705 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 706 */
(( android.view.animation.AnimationSet ) v2 ).addAnimation ( v9 ); // invoke-virtual {v2, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 707 */
/* const-wide/16 v13, 0x12c */
(( android.view.animation.AnimationSet ) v2 ).setDuration ( v13, v14 ); // invoke-virtual {v2, v13, v14}, Landroid/view/animation/AnimationSet;->setDuration(J)V
/* .line 708 */
v11 = com.android.server.wm.AppTransitionInjector.CUBIC_EASE_OUT_INTERPOLATOR;
(( android.view.animation.AnimationSet ) v2 ).setInterpolator ( v11 ); // invoke-virtual {v2, v11}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 709 */
/* sget-boolean v11, Lcom/android/server/wm/AppTransitionInjector;->IS_E10:Z */
if ( v11 != null) { // if-eqz v11, :cond_14
/* .line 710 */
int v11 = 1; // const/4 v11, 0x1
(( android.view.animation.AnimationSet ) v2 ).setZAdjustment ( v11 ); // invoke-virtual {v2, v11}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V
/* .line 712 */
} // :cond_14
/* move-object v11, v2 */
/* move-object v9, v11 */
/* .line 714 */
} // .end local v1 # "translateAnimation":Landroid/view/animation/Animation;
} // .end local v2 # "set":Landroid/view/animation/AnimationSet;
} // .end local v4 # "scaleAnimation":Landroid/view/animation/Animation;
/* .local v9, "anim":Landroid/view/animation/Animation; */
} // :goto_f
} // .end method
static android.view.animation.Animation createWallPaperOpenAnimation ( Boolean p0, android.graphics.Rect p1, android.graphics.Rect p2 ) {
/* .locals 1 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "positionRect" # Landroid/graphics/Rect; */
/* .line 573 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.wm.AppTransitionInjector .createWallPaperOpenAnimation ( p0,p1,p2,v0 );
} // .end method
static android.view.animation.Animation createWallPaperOpenAnimation ( Boolean p0, android.graphics.Rect p1, android.graphics.Rect p2, Integer p3 ) {
/* .locals 1 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "positionRect" # Landroid/graphics/Rect; */
/* .param p3, "orientation" # I */
/* .line 584 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.wm.AppTransitionInjector .createWallPaperOpenAnimation ( p0,p1,p2,v0,p3 );
} // .end method
static android.view.animation.Animation createWallPaperOpenAnimation ( Boolean p0, android.graphics.Rect p1, android.graphics.Rect p2, android.graphics.Rect p3 ) {
/* .locals 1 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "positionRect" # Landroid/graphics/Rect; */
/* .param p3, "startRect" # Landroid/graphics/Rect; */
/* .line 578 */
int v0 = 1; // const/4 v0, 0x1
com.android.server.wm.AppTransitionInjector .createWallPaperOpenAnimation ( p0,p1,p2,p3,v0 );
} // .end method
static android.view.animation.Animation createWallPaperOpenAnimation ( Boolean p0, android.graphics.Rect p1, android.graphics.Rect p2, android.graphics.Rect p3, Integer p4 ) {
/* .locals 6 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "positionRect" # Landroid/graphics/Rect; */
/* .param p3, "startRect" # Landroid/graphics/Rect; */
/* .param p4, "orientation" # I */
/* .line 589 */
int v3 = 0; // const/4 v3, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move v4, p4 */
/* invoke-static/range {v0 ..v5}, Lcom/android/server/wm/AppTransitionInjector;->createTransitionAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ILandroid/graphics/Point;)Landroid/view/animation/Animation; */
} // .end method
static android.view.animation.Animation createWallerOpenCloseTransitionAnimation ( Boolean p0, android.graphics.Rect p1, Boolean p2 ) {
/* .locals 18 */
/* .param p0, "enter" # Z */
/* .param p1, "appFrame" # Landroid/graphics/Rect; */
/* .param p2, "isOpenOrClose" # Z */
/* .line 913 */
/* new-instance v0, Landroid/view/animation/AnimationSet; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V */
/* .line 916 */
/* .local v0, "set":Landroid/view/animation/AnimationSet; */
int v2 = 0; // const/4 v2, 0x0
/* .line 917 */
/* .local v2, "scaleAnimation":Landroid/view/animation/Animation; */
int v3 = 0; // const/4 v3, 0x0
/* .line 918 */
/* .local v3, "alphaAnimation":Landroid/view/animation/Animation; */
v4 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->width()I */
/* .line 919 */
/* .local v4, "width":I */
v5 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Rect;->height()I */
/* .line 920 */
/* .local v5, "heigth":I */
int v6 = 0; // const/4 v6, 0x0
/* const/high16 v7, 0x3f800000 # 1.0f */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 938 */
/* new-instance v17, Landroid/view/animation/ScaleAnimation; */
/* const/high16 v9, 0x3f000000 # 0.5f */
/* const/high16 v10, 0x3f800000 # 1.0f */
/* const/high16 v11, 0x3f000000 # 0.5f */
/* const/high16 v12, 0x3f800000 # 1.0f */
int v13 = 1; // const/4 v13, 0x1
/* const/high16 v14, 0x3f000000 # 0.5f */
int v15 = 1; // const/4 v15, 0x1
/* const/high16 v16, 0x3f000000 # 0.5f */
/* move-object/from16 v8, v17 */
/* invoke-direct/range {v8 ..v16}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V */
/* move-object/from16 v2, v17 */
/* .line 939 */
/* new-instance v8, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v8, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v3, v8 */
/* .line 940 */
(( android.view.animation.AnimationSet ) v0 ).addAnimation ( v3 ); // invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 941 */
(( android.view.animation.AnimationSet ) v0 ).addAnimation ( v2 ); // invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 945 */
} // :cond_0
/* new-instance v8, Landroid/view/animation/AlphaAnimation; */
/* invoke-direct {v8, v7, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V */
/* move-object v3, v8 */
/* .line 946 */
(( android.view.animation.AnimationSet ) v0 ).addAnimation ( v3 ); // invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V
/* .line 949 */
} // :goto_0
/* new-instance v6, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator; */
/* const v7, 0x3f266666 # 0.65f */
/* const v8, 0x3f733333 # 0.95f */
/* invoke-direct {v6, v7, v8}, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;-><init>(FF)V */
/* .line 951 */
(( android.view.animation.AnimationSet ) v0 ).setInterpolator ( v6 ); // invoke-virtual {v0, v6}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V
/* .line 952 */
v6 = com.android.server.wm.AppTransitionInjector .isCTS ( );
if ( v6 != null) { // if-eqz v6, :cond_1
/* const-wide/16 v6, 0x190 */
} // :cond_1
/* const-wide/16 v6, 0x226 */
} // :goto_1
(( android.view.animation.AnimationSet ) v0 ).setDuration ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V
/* .line 954 */
(( android.view.animation.AnimationSet ) v0 ).setHasRoundedCorners ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V
/* .line 955 */
} // .end method
static Boolean disableSnapshot ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 1525 */
v0 = com.android.server.wm.AppTransitionInjector.BLACK_LIST_NOT_ALLOWED_SNAPSHOT;
v0 = (( java.util.ArrayList ) v0 ).contains ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1526 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1528 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
static Boolean disableSnapshotByComponent ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p0, "mActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1532 */
v0 = com.android.server.wm.AppTransitionInjector.BLACK_LIST_NOT_ALLOWED_SNAPSHOT_COMPONENT;
v1 = this.shortComponentName;
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1533 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1535 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
static Boolean disableSnapshotForApplock ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .param p1, "uid" # I */
/* .line 1553 */
/* const-string/jumbo v0, "security" */
android.os.ServiceManager .getService ( v0 );
/* .line 1554 */
/* .local v0, "b":Landroid/os/IBinder; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1555 */
/* .local v1, "result":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1557 */
try { // :try_start_0
v2 = android.os.UserHandle .getUserId ( p1 );
/* .line 1558 */
/* .local v2, "userId":I */
miui.security.ISecurityManager$Stub .asInterface ( v0 );
/* .line 1559 */
v4 = /* .local v3, "service":Lmiui/security/ISecurityManager; */
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = /* .line 1560 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1561 */
v4 = int v4 = 0; // const/4 v4, 0x0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v4, :cond_0 */
int v4 = 1; // const/4 v4, 0x1
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* move v1, v4 */
/* .line 1564 */
} // .end local v2 # "userId":I
} // .end local v3 # "service":Lmiui/security/ISecurityManager;
/* .line 1562 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1563 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "AppTransitionInjector"; // const-string v3, "AppTransitionInjector"
/* const-string/jumbo v4, "start window checkAccessControlPass error: " */
android.util.Slog .e ( v3,v4,v2 );
/* .line 1566 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_1
} // .end method
private static void doAnimationCallback ( android.os.IRemoteCallback p0 ) {
/* .locals 1 */
/* .param p0, "callback" # Landroid/os/IRemoteCallback; */
/* .line 409 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 411 */
/* .line 410 */
/* :catch_0 */
/* move-exception v0 */
/* .line 412 */
} // :goto_0
return;
} // .end method
public static android.graphics.Rect getMiuiAnimSupportInset ( ) {
/* .locals 1 */
/* .line 1521 */
v0 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
} // .end method
public static Integer getNavigationBarMode ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 1630 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1631 */
v1 = android.os.UserHandle .myUserId ( );
/* .line 1630 */
final String v2 = "navigation_mode"; // const-string v2, "navigation_mode"
int v3 = 2; // const/4 v3, 0x2
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
} // .end method
private static Float getRoundedCornerRadius ( android.view.RoundedCorner p0 ) {
/* .locals 1 */
/* .param p0, "roundedCorner" # Landroid/view/RoundedCorner; */
/* .line 1608 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1609 */
} // :cond_0
v0 = (( android.view.RoundedCorner ) p0 ).getRadius ( ); // invoke-virtual {p0}, Landroid/view/RoundedCorner;->getRadius()I
/* int-to-float v0, v0 */
} // .end method
public static android.util.ArraySet getUnHierarchicalAnimationTargets ( android.util.ArraySet p0, android.util.ArraySet p1, Boolean p2 ) {
/* .locals 5 */
/* .param p2, "visible" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/ArraySet<", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* ">;", */
/* "Landroid/util/ArraySet<", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* ">;Z)", */
/* "Landroid/util/ArraySet<", */
/* "Lcom/android/server/wm/WindowContainer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1618 */
/* .local p0, "openingApps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/ActivityRecord;>;" */
/* .local p1, "closingApps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/ActivityRecord;>;" */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
/* .line 1619 */
/* .local v0, "candidates":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/server/wm/WindowContainer;>;" */
if ( p2 != null) { // if-eqz p2, :cond_0
/* move-object v1, p0 */
} // :cond_0
/* move-object v1, p1 */
/* .line 1620 */
/* .local v1, "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/ActivityRecord;>;" */
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
v3 = (( android.util.ArraySet ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 1621 */
(( android.util.ArraySet ) v1 ).valueAt ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/ActivityRecord; */
/* .line 1622 */
/* .local v3, "app":Lcom/android/server/wm/ActivityRecord; */
v4 = (( com.android.server.wm.ActivityRecord ) v3 ).shouldApplyAnimation ( p2 ); // invoke-virtual {v3, p2}, Lcom/android/server/wm/ActivityRecord;->shouldApplyAnimation(Z)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1623 */
(( java.util.LinkedList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 1620 */
} // .end local v3 # "app":Lcom/android/server/wm/ActivityRecord;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1626 */
} // .end local v2 # "i":I
} // :cond_2
/* new-instance v2, Landroid/util/ArraySet; */
/* invoke-direct {v2, v0}, Landroid/util/ArraySet;-><init>(Ljava/util/Collection;)V */
} // .end method
static Boolean ignoreLaunchedFromSystemSurface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 1539 */
v0 = com.android.server.wm.AppTransitionInjector.IGNORE_LAUNCHED_FROM_SYSTEM_SURFACE;
v0 = (( java.util.ArrayList ) v0 ).contains ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1540 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1542 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
static void initDisplayRoundCorner ( android.content.Context p0 ) {
/* .locals 8 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 1578 */
/* const/16 v0, 0x3c */
/* .line 1579 */
/* .local v0, "result":I */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 1580 */
(( android.content.Context ) p0 ).getDisplayNoVerify ( ); // invoke-virtual {p0}, Landroid/content/Context;->getDisplayNoVerify()Landroid/view/Display;
/* .line 1581 */
/* .local v1, "display":Landroid/view/Display; */
if ( v1 != null) { // if-eqz v1, :cond_0
int v2 = 0; // const/4 v2, 0x0
(( android.view.Display ) v1 ).getRoundedCorner ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1582 */
/* nop */
/* .line 1583 */
(( android.view.Display ) v1 ).getRoundedCorner ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;
/* .line 1582 */
v2 = com.android.server.wm.AppTransitionInjector .getRoundedCornerRadius ( v2 );
/* .line 1584 */
/* .local v2, "topLeftRadius":F */
/* nop */
/* .line 1585 */
int v3 = 1; // const/4 v3, 0x1
(( android.view.Display ) v1 ).getRoundedCorner ( v3 ); // invoke-virtual {v1, v3}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;
/* .line 1584 */
v3 = com.android.server.wm.AppTransitionInjector .getRoundedCornerRadius ( v3 );
/* .line 1586 */
/* .local v3, "topRightRadius":F */
/* nop */
/* .line 1587 */
int v4 = 2; // const/4 v4, 0x2
(( android.view.Display ) v1 ).getRoundedCorner ( v4 ); // invoke-virtual {v1, v4}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;
/* .line 1586 */
v4 = com.android.server.wm.AppTransitionInjector .getRoundedCornerRadius ( v4 );
/* .line 1588 */
/* .local v4, "bottomRightRadius":F */
/* nop */
/* .line 1589 */
int v5 = 3; // const/4 v5, 0x3
(( android.view.Display ) v1 ).getRoundedCorner ( v5 ); // invoke-virtual {v1, v5}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;
/* .line 1588 */
v5 = com.android.server.wm.AppTransitionInjector .getRoundedCornerRadius ( v5 );
/* .line 1592 */
/* .local v5, "bottomLeftRadius":F */
v6 = com.android.server.wm.AppTransitionInjector .minRadius ( v2,v3 );
/* .line 1593 */
v7 = com.android.server.wm.AppTransitionInjector .minRadius ( v5,v4 );
/* .line 1592 */
v6 = com.android.server.wm.AppTransitionInjector .minRadius ( v6,v7 );
/* float-to-int v0, v6 */
/* .line 1594 */
/* int-to-float v6, v0 */
int v7 = 0; // const/4 v7, 0x0
/* cmpg-float v6, v6, v7 */
/* if-gtz v6, :cond_0 */
/* .line 1595 */
/* const/16 v0, 0x3c */
/* .line 1598 */
} // .end local v1 # "display":Landroid/view/Display;
} // .end local v2 # "topLeftRadius":F
} // .end local v3 # "topRightRadius":F
} // .end local v4 # "bottomRightRadius":F
} // .end local v5 # "bottomLeftRadius":F
} // :cond_0
/* .line 1599 */
return;
} // .end method
public static Boolean isCTS ( ) {
/* .locals 2 */
/* .line 1613 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/lit8 v0, v0, 0x1 */
final String v1 = "persist.sys.miui_optimization"; // const-string v1, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
static Boolean isUseFloatingAnimation ( android.view.animation.Animation p0 ) {
/* .locals 3 */
/* .param p0, "a" # Landroid/view/animation/Animation; */
/* .line 516 */
/* instance-of v0, p0, Landroid/view/animation/AnimationSet; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 517 */
/* move-object v0, p0 */
/* check-cast v0, Landroid/view/animation/AnimationSet; */
(( android.view.animation.AnimationSet ) v0 ).getAnimations ( ); // invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Landroid/view/animation/Animation; */
/* .line 518 */
/* .local v1, "animation":Landroid/view/animation/Animation; */
/* instance-of v2, v1, Landroid/view/animation/TranslateWithClipAnimation; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 519 */
int v0 = 1; // const/4 v0, 0x1
/* .line 521 */
} // .end local v1 # "animation":Landroid/view/animation/Animation;
} // :cond_0
/* .line 523 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
static Boolean isUseFreeFormAnimation ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "transit" # I */
/* .line 422 */
/* const/16 v0, 0x18 */
/* if-eq p0, v0, :cond_1 */
int v0 = 6; // const/4 v0, 0x6
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0x19 */
/* if-eq p0, v0, :cond_1 */
int v0 = 7; // const/4 v0, 0x7
/* if-ne p0, v0, :cond_0 */
/* .line 428 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 426 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
static android.view.animation.Animation loadAnimationNotCheckForDimmer ( Integer p0, Boolean p1, android.graphics.Rect p2, com.android.server.wm.WindowContainer p3, com.android.server.wm.WindowManagerService p4 ) {
/* .locals 2 */
/* .param p0, "transit" # I */
/* .param p1, "enter" # Z */
/* .param p2, "frame" # Landroid/graphics/Rect; */
/* .param p3, "container" # Lcom/android/server/wm/WindowContainer; */
/* .param p4, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .line 960 */
int v0 = 0; // const/4 v0, 0x0
/* .line 961 */
/* .local v0, "defaultAnimation":Landroid/view/animation/Animation; */
/* packed-switch p0, :pswitch_data_0 */
/* .line 971 */
int v0 = 0; // const/4 v0, 0x0
/* .line 967 */
/* :pswitch_0 */
int v1 = 0; // const/4 v1, 0x0
com.android.server.wm.AppTransitionInjector .createActivityOpenCloseTransitionForDimmer ( p1,p2,v1,p3,p4 );
/* .line 969 */
/* .line 963 */
/* :pswitch_1 */
int v1 = 1; // const/4 v1, 0x1
com.android.server.wm.AppTransitionInjector .createActivityOpenCloseTransitionForDimmer ( p1,p2,v1,p3,p4 );
/* .line 965 */
/* nop */
/* .line 973 */
} // :goto_0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x6 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static android.view.animation.Animation loadAnimationSafely ( android.content.Context p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "resId" # I */
/* .line 1102 */
try { // :try_start_0
android.view.animation.AnimationUtils .loadAnimation ( p0,p1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1103 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1104 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "AppTransitionInjector"; // const-string v1, "AppTransitionInjector"
final String v2 = "Unable to load animation resource"; // const-string v2, "Unable to load animation resource"
android.util.Slog .w ( v1,v2,v0 );
/* .line 1105 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
static android.view.animation.Animation loadDefaultAnimation ( android.view.WindowManager$LayoutParams p0, Integer p1, Boolean p2, android.graphics.Rect p3 ) {
/* .locals 8 */
/* .param p0, "lp" # Landroid/view/WindowManager$LayoutParams; */
/* .param p1, "transit" # I */
/* .param p2, "enter" # Z */
/* .param p3, "frame" # Landroid/graphics/Rect; */
/* .line 1120 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1121 */
/* .local v0, "defaultAnimation":Landroid/view/animation/Animation; */
v1 = com.android.server.wm.AppTransitionInjector .useDefaultAnimationAttr ( p0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1122 */
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move-object v2, p0 */
/* move v3, p1 */
/* move v4, p2 */
/* move-object v5, p3 */
/* invoke-static/range {v2 ..v7}, Lcom/android/server/wm/AppTransitionInjector;->loadDefaultAnimationNotCheck(Landroid/view/WindowManager$LayoutParams;IZLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;Lcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation; */
/* .line 1124 */
} // :cond_0
} // .end method
static android.view.animation.Animation loadDefaultAnimationNotCheck ( android.view.WindowManager$LayoutParams p0, Integer p1, Boolean p2, android.graphics.Rect p3, com.android.server.wm.WindowContainer p4, com.android.server.wm.WindowManagerService p5 ) {
/* .locals 7 */
/* .param p0, "lp" # Landroid/view/WindowManager$LayoutParams; */
/* .param p1, "transit" # I */
/* .param p2, "enter" # Z */
/* .param p3, "frame" # Landroid/graphics/Rect; */
/* .param p4, "container" # Lcom/android/server/wm/WindowContainer; */
/* .param p5, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .line 1128 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1129 */
/* .local v0, "defaultAnimation":Landroid/view/animation/Animation; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1130 */
/* .local v1, "mffasStub":Lcom/android/server/wm/MiuiFreeFormActivityStackStub; */
int v2 = 0; // const/4 v2, 0x0
/* .line 1131 */
/* .local v2, "freemAs":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* .line 1132 */
/* .local v3, "freeformScale":F */
/* instance-of v4, p4, Lcom/android/server/wm/ActivityRecord; */
if ( v4 != null) { // if-eqz v4, :cond_0
if ( p4 != null) { // if-eqz p4, :cond_0
if ( p5 != null) { // if-eqz p5, :cond_0
/* .line 1133 */
/* move-object v4, p4 */
/* check-cast v4, Lcom/android/server/wm/ActivityRecord; */
v4 = (( com.android.server.wm.ActivityRecord ) v4 ).getRootTaskId ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
/* .line 1134 */
/* .local v4, "rootTaskId":I */
v5 = this.mAtmService;
v5 = this.mMiuiFreeFormManagerService;
/* .line 1135 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1136 */
/* instance-of v5, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 1137 */
/* move-object v2, v1 */
/* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1138 */
/* iget v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* .line 1141 */
} // .end local v4 # "rootTaskId":I
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
if ( p4 != null) { // if-eqz p4, :cond_1
/* .line 1142 */
/* iget-boolean v5, p4, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z */
} // :cond_1
/* move v5, v4 */
/* .line 1143 */
/* .local v5, "isSwitchAnimationType":Z */
} // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 1144 */
/* const/16 v6, 0x8 */
/* if-eq p1, v6, :cond_4 */
/* const/16 v6, 0xa */
/* if-ne p1, v6, :cond_2 */
/* .line 1146 */
} // :cond_2
/* const/16 v6, 0x9 */
/* if-eq p1, v6, :cond_3 */
/* const/16 v6, 0xb */
/* if-ne p1, v6, :cond_5 */
/* .line 1147 */
} // :cond_3
int p1 = 7; // const/4 p1, 0x7
/* .line 1145 */
} // :cond_4
} // :goto_1
int p1 = 6; // const/4 p1, 0x6
/* .line 1150 */
} // :cond_5
} // :goto_2
int v6 = 1; // const/4 v6, 0x1
/* sparse-switch p1, :sswitch_data_0 */
/* .line 1196 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1165 */
/* :sswitch_0 */
/* if-nez v2, :cond_6 */
/* .line 1166 */
/* .line 1168 */
} // :cond_6
if ( p5 != null) { // if-eqz p5, :cond_8
/* .line 1169 */
v4 = this.mContext;
v6 = com.android.server.wm.AppTransitionInjector .updateToTranslucentAnimIfNeeded ( p1 );
com.android.server.wm.AppTransitionInjector .loadAnimationSafely ( v4,v6 );
/* .line 1152 */
/* :sswitch_1 */
/* if-nez v2, :cond_7 */
/* .line 1153 */
/* .line 1155 */
} // :cond_7
if ( p5 != null) { // if-eqz p5, :cond_8
/* .line 1156 */
v4 = this.mContext;
v6 = com.android.server.wm.AppTransitionInjector .updateToTranslucentAnimIfNeeded ( p1 );
com.android.server.wm.AppTransitionInjector .loadAnimationSafely ( v4,v6 );
/* .line 1191 */
/* :sswitch_2 */
if ( p2 != null) { // if-eqz p2, :cond_8
/* .line 1192 */
com.android.server.wm.AppTransitionInjector .createWallerOpenCloseTransitionAnimation ( p2,p3,v4 );
/* .line 1183 */
/* :sswitch_3 */
com.android.server.wm.AppTransitionInjector .createTaskOpenCloseTransition ( p2,p3,v4,p5 );
/* .line 1184 */
/* .line 1179 */
/* :sswitch_4 */
com.android.server.wm.AppTransitionInjector .createTaskOpenCloseTransition ( p2,p3,v6,p5 );
/* .line 1180 */
/* .line 1174 */
/* :sswitch_5 */
com.android.server.wm.AppTransitionInjector .createActivityOpenCloseTransition ( p2,p3,v4,v3,p4 );
/* .line 1176 */
/* .line 1161 */
/* :sswitch_6 */
com.android.server.wm.AppTransitionInjector .createActivityOpenCloseTransition ( p2,p3,v6,v3,p4 );
/* .line 1163 */
/* nop */
/* .line 1198 */
} // :cond_8
} // :goto_3
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x6 -> :sswitch_6 */
/* 0x7 -> :sswitch_5 */
/* 0x8 -> :sswitch_4 */
/* 0x9 -> :sswitch_3 */
/* 0xa -> :sswitch_4 */
/* 0xb -> :sswitch_3 */
/* 0xc -> :sswitch_2 */
/* 0x18 -> :sswitch_1 */
/* 0x19 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
static android.view.animation.Animation loadFreeFormAnimation ( com.android.server.wm.WindowManagerService p0, Integer p1, Boolean p2, android.graphics.Rect p3, com.android.server.wm.WindowContainer p4 ) {
/* .locals 5 */
/* .param p0, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p1, "transit" # I */
/* .param p2, "enter" # Z */
/* .param p3, "frame" # Landroid/graphics/Rect; */
/* .param p4, "container" # Lcom/android/server/wm/WindowContainer; */
/* .line 433 */
int v0 = 0; // const/4 v0, 0x0
/* .line 434 */
/* .local v0, "a":Landroid/view/animation/Animation; */
/* instance-of v1, p4, Lcom/android/server/wm/WindowState; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 435 */
/* move-object v1, p4 */
/* check-cast v1, Lcom/android/server/wm/WindowState; */
(( com.android.server.wm.WindowState ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
/* .line 437 */
} // :cond_0
/* instance-of v1, p4, Lcom/android/server/wm/ActivityRecord; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 438 */
/* move-object v1, p4 */
/* check-cast v1, Lcom/android/server/wm/ActivityRecord; */
(( com.android.server.wm.ActivityRecord ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* .line 440 */
} // :cond_1
if ( p4 != null) { // if-eqz p4, :cond_5
/* instance-of v1, p4, Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 441 */
/* move-object v1, p4 */
/* check-cast v1, Lcom/android/server/wm/Task; */
v1 = (( com.android.server.wm.Task ) v1 ).getRootTaskId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* .line 442 */
/* .local v1, "rootTaskId":I */
v2 = this.mAtmService;
v2 = this.mMiuiFreeFormManagerService;
/* .line 443 */
/* .local v2, "freemAsstub":Lcom/android/server/wm/MiuiFreeFormActivityStackStub; */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 444 */
/* instance-of v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 445 */
/* move-object v3, v2 */
/* check-cast v3, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 446 */
/* .local v3, "freemAs":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* const/16 v4, 0x8 */
/* if-eq p1, v4, :cond_3 */
/* const/16 v4, 0xa */
/* if-eq p1, v4, :cond_3 */
/* const/16 v4, 0x9 */
/* if-eq p1, v4, :cond_3 */
/* const/16 v4, 0xb */
/* if-eq p1, v4, :cond_3 */
/* if-nez p1, :cond_2 */
/* .line 456 */
} // :cond_2
/* .line 451 */
} // :cond_3
} // :goto_0
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v3 ).isNeedAnimation ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNeedAnimation()Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 452 */
com.android.server.wm.AppTransitionInjector .createFreeFormAppOpenAndExitAnimation ( p0,p2,p3,v3 );
/* .line 454 */
} // :cond_4
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.wm.MiuiFreeFormActivityStack ) v3 ).setNeedAnimation ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setNeedAnimation(Z)V
/* .line 466 */
} // .end local v1 # "rootTaskId":I
} // .end local v2 # "freemAsstub":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
} // .end local v3 # "freemAs":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_5
} // :goto_1
} // .end method
private static Float minRadius ( Float p0, Float p1 ) {
/* .locals 2 */
/* .param p0, "aRadius" # F */
/* .param p1, "bRadius" # F */
/* .line 1602 */
int v0 = 0; // const/4 v0, 0x0
/* cmpl-float v1, p0, v0 */
/* if-nez v1, :cond_0 */
/* .line 1603 */
} // :cond_0
/* cmpl-float v0, p1, v0 */
/* if-nez v0, :cond_1 */
/* .line 1604 */
} // :cond_1
v0 = java.lang.Math .min ( p0,p1 );
} // .end method
static Long recalculateClipRevealTranslateYDuration ( Long p0 ) {
/* .locals 2 */
/* .param p0, "duration" # J */
/* .line 1245 */
/* const-wide/16 v0, 0x32 */
/* sub-long v0, p0, v0 */
/* return-wide v0 */
} // .end method
static void setMiuiAnimSupportInset ( android.graphics.Rect p0 ) {
/* .locals 1 */
/* .param p0, "inset" # Landroid/graphics/Rect; */
/* .line 1071 */
/* if-nez p0, :cond_0 */
/* .line 1072 */
v0 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
(( android.graphics.Rect ) v0 ).setEmpty ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V
/* .line 1074 */
} // :cond_0
v0 = com.android.server.wm.AppTransitionInjector.sMiuiAnimSupportInset;
(( android.graphics.Rect ) v0 ).set ( p0 ); // invoke-virtual {v0, p0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 1076 */
} // :goto_0
return;
} // .end method
private static Integer updateToTranslucentAnimIfNeeded ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "transit" # I */
/* .line 1110 */
/* const/16 v0, 0x18 */
/* if-ne p0, v0, :cond_0 */
/* .line 1111 */
/* const v0, 0x10a0012 */
/* .line 1113 */
} // :cond_0
/* const/16 v0, 0x19 */
/* if-ne p0, v0, :cond_1 */
/* .line 1114 */
/* const v0, 0x10a0011 */
/* .line 1116 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
static Boolean useDefaultAnimationAttr ( android.view.WindowManager$LayoutParams p0 ) {
/* .locals 2 */
/* .param p0, "lp" # Landroid/view/WindowManager$LayoutParams; */
/* .line 1201 */
/* if-nez p0, :cond_0 */
/* .line 1202 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1205 */
} // :cond_0
v0 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget v0, p0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I */
/* const/high16 v1, -0x1000000 */
/* and-int/2addr v0, v1 */
/* const/high16 v1, 0x1000000 */
/* if-ne v0, v1, :cond_1 */
/* .line 1208 */
} // :cond_1
final String v0 = "android"; // const-string v0, "android"
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 1206 */
} // :cond_2
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
static Boolean useDefaultAnimationAttr ( android.view.WindowManager$LayoutParams p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "lp" # Landroid/view/WindowManager$LayoutParams; */
/* .param p1, "resId" # I */
/* .line 1213 */
/* if-nez p0, :cond_0 */
/* .line 1214 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1217 */
} // :cond_0
v0 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/high16 v0, -0x1000000 */
/* and-int/2addr v0, p1 */
/* const/high16 v1, 0x1000000 */
/* if-ne v0, v1, :cond_1 */
/* .line 1220 */
} // :cond_1
final String v0 = "android"; // const-string v0, "android"
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 1218 */
} // :cond_2
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
static Boolean useDefaultAnimationAttr ( android.view.WindowManager$LayoutParams p0, Integer p1, Integer p2, Boolean p3, Boolean p4 ) {
/* .locals 3 */
/* .param p0, "lp" # Landroid/view/WindowManager$LayoutParams; */
/* .param p1, "resId" # I */
/* .param p2, "transit" # I */
/* .param p3, "enter" # Z */
/* .param p4, "isFreeForm" # Z */
/* .line 1224 */
int v0 = 1; // const/4 v0, 0x1
if ( p4 != null) { // if-eqz p4, :cond_1
int v1 = 6; // const/4 v1, 0x6
/* if-eq p2, v1, :cond_0 */
/* const/16 v1, 0x18 */
/* if-eq p2, v1, :cond_0 */
int v1 = 7; // const/4 v1, 0x7
/* if-eq p2, v1, :cond_0 */
/* const/16 v1, 0x19 */
/* if-ne p2, v1, :cond_1 */
/* .line 1231 */
} // :cond_0
/* .line 1233 */
} // :cond_1
/* if-nez p0, :cond_2 */
/* .line 1234 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1237 */
} // :cond_2
v1 = this.packageName;
if ( v1 != null) { // if-eqz v1, :cond_4
/* const/high16 v1, -0x1000000 */
/* and-int/2addr v1, p1 */
/* const/high16 v2, 0x1000000 */
/* if-eq v1, v2, :cond_4 */
if ( p3 != null) { // if-eqz p3, :cond_3
/* const/16 v1, 0xc */
/* if-ne p2, v1, :cond_3 */
/* if-nez p1, :cond_3 */
/* .line 1241 */
} // :cond_3
final String v0 = "android"; // const-string v0, "android"
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 1239 */
} // :cond_4
} // :goto_0
} // .end method
