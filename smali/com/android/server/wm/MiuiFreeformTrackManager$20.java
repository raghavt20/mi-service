class com.android.server.wm.MiuiFreeformTrackManager$20 implements java.lang.Runnable {
	 /* .source "MiuiFreeformTrackManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiFreeformTrackManager;->trackSplitScreenRecommendClickEvent(Ljava/lang/String;Ljava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiFreeformTrackManager this$0; //synthetic
final java.lang.String val$applicationName; //synthetic
final java.lang.String val$packageName; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiFreeformTrackManager$20 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiFreeformTrackManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 682 */
this.this$0 = p1;
this.val$packageName = p2;
this.val$applicationName = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 684 */
v0 = this.this$0;
v0 = this.mFreeFormTrackLock;
/* monitor-enter v0 */
/* .line 686 */
try { // :try_start_0
v1 = com.android.server.wm.MiuiFreeformTrackManager .isCanOneTrack ( );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v1, :cond_0 */
/* .line 687 */
try { // :try_start_1
	 /* monitor-exit v0 */
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 return;
	 /* .line 689 */
} // :cond_0
try { // :try_start_2
	 v1 = this.this$0;
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmITrackBinder ( v1 );
	 /* if-nez v1, :cond_1 */
	 /* .line 690 */
	 v1 = this.this$0;
	 (( com.android.server.wm.MiuiFreeformTrackManager ) v1 ).bindOneTrackService ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->bindOneTrackService()V
	 /* .line 692 */
} // :cond_1
v1 = this.this$0;
com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmITrackBinder ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 693 */
	 /* new-instance v1, Lorg/json/JSONObject; */
	 /* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
	 /* .line 694 */
	 /* .local v1, "jsonData":Lorg/json/JSONObject; */
	 /* const-string/jumbo v2, "tip" */
	 final String v3 = "621.10.1.1.29120"; // const-string v3, "621.10.1.1.29120"
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 695 */
	 v2 = this.this$0;
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$mputCommomParam ( v2,v1 );
	 /* .line 696 */
	 final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
	 final String v3 = "recommendation_click"; // const-string v3, "recommendation_click"
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 697 */
	 final String v2 = "app_package_combination"; // const-string v2, "app_package_combination"
	 v3 = this.val$packageName;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 698 */
	 final String v2 = "app_display_name_combination"; // const-string v2, "app_display_name_combination"
	 v3 = this.val$applicationName;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 699 */
	 v2 = this.this$0;
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmITrackBinder ( v2 );
	 final String v3 = "31000000538"; // const-string v3, "31000000538"
	 final String v4 = "android"; // const-string v4, "android"
	 (( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
	 int v6 = 0; // const/4 v6, 0x0
	 /* :try_end_2 */
	 /* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
	 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
	 /* .line 703 */
} // .end local v1 # "jsonData":Lorg/json/JSONObject;
} // :cond_2
/* .line 704 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 701 */
/* :catch_0 */
/* move-exception v1 */
/* .line 702 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_3
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 704 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit v0 */
/* .line 705 */
return;
/* .line 704 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v1 */
} // .end method
