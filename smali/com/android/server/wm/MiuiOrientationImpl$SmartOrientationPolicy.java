class com.android.server.wm.MiuiOrientationImpl$SmartOrientationPolicy {
	 /* .source "MiuiOrientationImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiOrientationImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "SmartOrientationPolicy" */
} // .end annotation
/* # static fields */
private static final java.lang.String MIUI_SMART_ORIEANTATION_ACTIVITY_KEY;
private static final java.lang.String MIUI_SMART_ORIENTATION_KEY;
private static final java.lang.String MOULD_NAME;
private static final java.lang.String TAG;
/* # instance fields */
private final java.util.List mActivityList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.wm.ActivityTaskManagerServiceImpl mAtmServiceImpl;
private final android.content.Context mContext;
private final java.util.List mPackageList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.res.Resources mResources;
final com.android.server.wm.MiuiOrientationImpl this$0; //synthetic
/* # direct methods */
static void -$$Nest$mupdateListFromCloud ( com.android.server.wm.MiuiOrientationImpl$SmartOrientationPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->updateListFromCloud()V */
return;
} // .end method
 com.android.server.wm.MiuiOrientationImpl$SmartOrientationPolicy ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiOrientationImpl; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "atmServiceImpl" # Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
/* .line 1312 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1313 */
this.mAtmServiceImpl = p3;
/* .line 1314 */
this.mContext = p2;
/* .line 1315 */
(( android.content.Context ) p2 ).getResources ( ); // invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
this.mResources = v0;
/* .line 1317 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mPackageList = v0;
/* .line 1318 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mActivityList = v0;
/* .line 1319 */
return;
} // .end method
private void clearList ( ) {
/* .locals 1 */
/* .line 1360 */
v0 = this.mPackageList;
/* .line 1361 */
v0 = this.mActivityList;
/* .line 1362 */
return;
} // .end method
private void registerDataObserver ( ) {
/* .locals 4 */
/* .line 1326 */
v0 = this.this$0;
v0 = (( com.android.server.wm.MiuiOrientationImpl ) v0 ).isSmartOrientEnable ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 1327 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->updatePolicyListFromLocal()V */
/* .line 1328 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1329 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy$1; */
/* .line 1330 */
com.android.server.MiuiBgThread .getHandler ( );
/* invoke-direct {v2, p0, v3}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy$1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;Landroid/os/Handler;)V */
/* .line 1328 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1337 */
return;
} // .end method
private void updateListFromCloud ( ) {
/* .locals 3 */
/* .line 1354 */
v0 = this.mContext;
/* const-string/jumbo v1, "smart_orientation_list" */
final String v2 = "miuiSmartOrientation"; // const-string v2, "miuiSmartOrientation"
/* invoke-direct {p0, v0, v2, v1}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->updateListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1355 */
v0 = this.mContext;
/* const-string/jumbo v1, "smart_orientation_activity_list" */
/* invoke-direct {p0, v0, v2, v1}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->updateListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1357 */
return;
} // .end method
private void updateListFromCloud ( android.content.Context p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "moduleName" # Ljava/lang/String; */
/* .param p3, "key" # Ljava/lang/String; */
/* .line 1366 */
final String v0 = "SmartOrientationPolicy"; // const-string v0, "SmartOrientationPolicy"
/* .line 1367 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1366 */
int v2 = 0; // const/4 v2, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v1,p2,p3,v2 );
/* .line 1368 */
/* .local v1, "data":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateListFromCloud: data: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " moduleName="; // const-string v3, " moduleName="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " key="; // const-string v3, " key="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 1370 */
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1371 */
return;
/* .line 1373 */
} // :cond_0
/* new-instance v2, Lorg/json/JSONArray; */
/* invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 1374 */
/* .local v2, "apps":Lorg/json/JSONArray; */
v3 = (( java.lang.String ) p3 ).hashCode ( ); // invoke-virtual {p3}, Ljava/lang/String;->hashCode()I
/* sparse-switch v3, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
/* const-string/jumbo v3, "smart_orientation_activity_list" */
v3 = (( java.lang.String ) p3 ).equals ( v3 ); // invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
int v3 = 1; // const/4 v3, 0x1
/* :sswitch_1 */
/* const-string/jumbo v3, "smart_orientation_list" */
v3 = (( java.lang.String ) p3 ).equals ( v3 ); // invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v3 != null) { // if-eqz v3, :cond_1
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
int v3 = -1; // const/4 v3, -0x1
} // :goto_1
/* const-string/jumbo v4, "updateListFromCloud: mCloudList: " */
/* packed-switch v3, :pswitch_data_0 */
/* .line 1383 */
/* :pswitch_0 */
try { // :try_start_1
v3 = this.mActivityList;
/* .line 1384 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_2
v5 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v5, :cond_2 */
/* .line 1385 */
v5 = this.mActivityList;
(( org.json.JSONArray ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 1384 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1387 */
} // .end local v3 # "i":I
} // :cond_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mActivityList;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 1388 */
/* .line 1376 */
/* :pswitch_1 */
v3 = this.mPackageList;
/* .line 1377 */
int v3 = 0; // const/4 v3, 0x0
/* .restart local v3 # "i":I */
} // :goto_3
v5 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v5, :cond_3 */
/* .line 1378 */
v5 = this.mPackageList;
(( org.json.JSONArray ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 1377 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1380 */
} // .end local v3 # "i":I
} // :cond_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mPackageList;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1381 */
/* nop */
/* .line 1395 */
} // .end local v1 # "data":Ljava/lang/String;
} // .end local v2 # "apps":Lorg/json/JSONArray;
} // :goto_4
/* .line 1393 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1394 */
/* .local v1, "e":Lorg/json/JSONException; */
final String v2 = "exception when updateListFromCloud: "; // const-string v2, "exception when updateListFromCloud: "
android.util.Slog .e ( v0,v2,v1 );
/* .line 1396 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_5
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x78bb729d -> :sswitch_1 */
/* -0x47fc4757 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void updatePolicyListFromLocal ( ) {
/* .locals 5 */
/* .line 1340 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->clearList()V */
/* .line 1341 */
v0 = this.mResources;
/* .line 1342 */
/* const v1, 0x110300c6 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 1343 */
/* .local v0, "packages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_0 */
/* .line 1344 */
v2 = this.mPackageList;
/* aget-object v3, v0, v1 */
/* .line 1343 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1346 */
} // .end local v1 # "i":I
} // :cond_0
v1 = this.mResources;
/* .line 1347 */
/* const v2, 0x110300c5 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 1348 */
/* .local v1, "activities":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
/* array-length v3, v1 */
/* if-ge v2, v3, :cond_1 */
/* .line 1349 */
v3 = this.mActivityList;
/* aget-object v4, v1, v2 */
/* .line 1348 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1351 */
} // .end local v2 # "i":I
} // :cond_1
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 1413 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1414 */
/* .local v0, "innerPrefix":Ljava/lang/String; */
v1 = v1 = this.mPackageList;
final String v2 = "] "; // const-string v2, "] "
final String v3 = "["; // const-string v3, "["
/* if-nez v1, :cond_0 */
/* .line 1415 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "SmartOrientationPackageList(Package)"; // const-string v4, "SmartOrientationPackageList(Package)"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1416 */
v1 = this.mPackageList;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Ljava/lang/String; */
/* .line 1417 */
/* .local v4, "packages":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1418 */
} // .end local v4 # "packages":Ljava/lang/String;
/* .line 1420 */
} // :cond_0
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 1421 */
v1 = v1 = this.mActivityList;
/* if-nez v1, :cond_1 */
/* .line 1422 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "SmartOrientationActivityList(Activity)"; // const-string v4, "SmartOrientationActivityList(Activity)"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1423 */
v1 = this.mActivityList;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/lang/String; */
/* .line 1424 */
/* .local v4, "activities":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1425 */
} // .end local v4 # "activities":Ljava/lang/String;
/* .line 1427 */
} // :cond_1
return;
} // .end method
void init ( ) {
/* .locals 0 */
/* .line 1322 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->registerDataObserver()V */
/* .line 1323 */
return;
} // .end method
public Boolean isNeedAccSensor ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "activityName" # Ljava/lang/String; */
/* .line 1406 */
v0 = this.mActivityList;
v0 = if ( v0 != null) { // if-eqz v0, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1407 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1409 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isSupportSmartOrientation ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1399 */
v0 = this.mPackageList;
v0 = if ( v0 != null) { // if-eqz v0, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1400 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1402 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
