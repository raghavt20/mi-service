public class com.android.server.wm.MiuiDragAndDropStubImpl$DragNotAllowPackages {
	 /* .source "MiuiDragAndDropStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiDragAndDropStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "DragNotAllowPackages" */
} // .end annotation
/* # instance fields */
private final android.util.ArraySet packages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long version;
/* # direct methods */
public com.android.server.wm.MiuiDragAndDropStubImpl$DragNotAllowPackages ( ) {
/* .locals 1 */
/* .line 74 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 76 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.packages = v0;
return;
} // .end method
/* # virtual methods */
public android.util.ArraySet getPackages ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 83 */
v0 = this.packages;
} // .end method
public Long getVersion ( ) {
/* .locals 2 */
/* .line 79 */
/* iget-wide v0, p0, Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;->version:J */
/* return-wide v0 */
} // .end method
