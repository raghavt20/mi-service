.class public Lcom/android/server/wm/MiuiMirrorDragDropController;
.super Ljava/lang/Object;
.source "MiuiMirrorDragDropController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;
    }
.end annotation


# static fields
.field private static final DRAG_TIMEOUT_MS:J = 0x1388L

.field static final MSG_DRAG_END_TIMEOUT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MiuiMirrorDragDrop"


# instance fields
.field private mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

.field private final mHandler:Landroid/os/Handler;

.field private mService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDragState(Lcom/android/server/wm/MiuiMirrorDragDropController;)Lcom/android/server/wm/MiuiMirrorDragState;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 3

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 49
    new-instance v0, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v1, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService$H;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;-><init>(Lcom/android/server/wm/MiuiMirrorDragDropController;Lcom/android/server/wm/WindowManagerService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mHandler:Landroid/os/Handler;

    .line 50
    return-void
.end method


# virtual methods
.method public cancelDragAndDrop(Landroid/os/IBinder;)V
    .locals 3
    .param p1, "dragToken"    # Landroid/os/IBinder;

    .line 162
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 163
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eqz v1, :cond_1

    .line 168
    iget-object v1, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mToken:Landroid/os/IBinder;

    if-ne v1, p1, :cond_0

    .line 175
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z

    .line 176
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    .line 177
    monitor-exit v0

    .line 178
    return-void

    .line 169
    :cond_0
    const-string v1, "MiuiMirrorDragDrop"

    const-string v2, "cancelDragAndDrop() does not match prepareDrag()"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "cancelDragAndDrop() does not match prepareDrag()"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
    .end local p1    # "dragToken":Landroid/os/IBinder;
    throw v1

    .line 164
    .restart local p0    # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
    .restart local p1    # "dragToken":Landroid/os/IBinder;
    :cond_1
    const-string v1, "MiuiMirrorDragDrop"

    const-string v2, "cancelDragAndDrop() without prepareDrag()"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "cancelDragAndDrop() without prepareDrag()"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
    .end local p1    # "dragToken":Landroid/os/IBinder;
    throw v1

    .line 177
    .restart local p0    # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
    .restart local p1    # "dragToken":Landroid/os/IBinder;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public dragDropActiveLocked()Z
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMirrorDragState;->isClosing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public injectDragEvent(IIFF)V
    .locals 3
    .param p1, "action"    # I
    .param p2, "displayId"    # I
    .param p3, "newX"    # F
    .param p4, "newY"    # F

    .line 181
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 182
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->dragDropActiveLocked()Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    monitor-exit v0

    return-void

    .line 185
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 196
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v1, p2, v2, v2}, Lcom/android/server/wm/MiuiMirrorDragState;->notifyMoveLocked(IFF)V

    .line 197
    goto :goto_0

    .line 187
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v1, p2, p3, p4}, Lcom/android/server/wm/MiuiMirrorDragState;->broadcastDragStartedLocked(IFF)V

    .line 188
    goto :goto_0

    .line 201
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    .line 202
    goto :goto_0

    .line 193
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v1, p2, p3, p4}, Lcom/android/server/wm/MiuiMirrorDragState;->notifyDropLocked(IFF)V

    .line 194
    goto :goto_0

    .line 190
    :pswitch_4
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v1, p2, p3, p4}, Lcom/android/server/wm/MiuiMirrorDragState;->notifyMoveLocked(IFF)V

    .line 191
    goto :goto_0

    .line 199
    :pswitch_5
    nop

    .line 206
    :goto_0
    monitor-exit v0

    .line 207
    return-void

    .line 206
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public lastTargetUid()I
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/android/server/wm/MiuiMirrorDragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    if-nez v0, :cond_0

    goto :goto_0

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMirrorDragState;->mTargetWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getOwningUid()I

    move-result v0

    goto :goto_1

    .line 43
    :cond_1
    :goto_0
    const/4 v0, -0x1

    :goto_1
    return v0
.end method

.method onDragStateClosedLocked(Lcom/android/server/wm/MiuiMirrorDragState;)V
    .locals 2
    .param p1, "dragState"    # Lcom/android/server/wm/MiuiMirrorDragState;

    .line 229
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eq v0, p1, :cond_0

    .line 230
    const-string v0, "MiuiMirrorDragDrop"

    const-string v1, "Unknown drag state is closed"

    invoke-static {v0, v1}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    return-void

    .line 233
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    .line 234
    return-void
.end method

.method public performDrag(IILandroid/view/IWindow;IILandroid/content/ClipData;)Landroid/os/IBinder;
    .locals 7
    .param p1, "callerPid"    # I
    .param p2, "callerUid"    # I
    .param p3, "window"    # Landroid/view/IWindow;
    .param p4, "flags"    # I
    .param p5, "displayId"    # I
    .param p6, "data"    # Landroid/content/ClipData;

    .line 58
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    .line 59
    .local v0, "dragToken":Landroid/os/IBinder;
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 61
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->dragDropActiveLocked()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 62
    const-string v2, "MiuiMirrorDragDrop"

    const-string v4, "Mirror drag already in progress"

    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    nop

    .line 95
    :try_start_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z

    move-result v2

    if-nez v2, :cond_0

    .line 96
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 63
    return-object v3

    .line 66
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mDragDropController:Lcom/android/server/wm/DragDropController;

    invoke-virtual {v2}, Lcom/android/server/wm/DragDropController;->dragDropActiveLocked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 67
    const-string v2, "MiuiMirrorDragDrop"

    const-string v4, "Normal drag in progress"

    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 68
    nop

    .line 95
    :try_start_3
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z

    move-result v2

    if-nez v2, :cond_2

    .line 96
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    :cond_2
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 68
    return-object v3

    .line 72
    :cond_3
    :try_start_4
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getInstance()Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getDelegatePid()I

    move-result v2

    if-ne p1, v2, :cond_4

    .line 73
    const-string v2, "MiuiMirrorDragDrop"

    const-string v3, "drag from mirror"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/4 v2, 0x0

    .local v2, "winBinder":Landroid/os/IBinder;
    goto :goto_0

    .line 76
    .end local v2    # "winBinder":Landroid/os/IBinder;
    :cond_4
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/view/IWindow;Z)Lcom/android/server/wm/WindowState;

    move-result-object v2

    .line 78
    .local v2, "callingWin":Lcom/android/server/wm/WindowState;
    if-nez v2, :cond_6

    .line 79
    const-string v4, "MiuiMirrorDragDrop"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad requesting window "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 80
    nop

    .line 95
    :try_start_5
    iget-object v4, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z

    move-result v4

    if-nez v4, :cond_5

    .line 96
    iget-object v4, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v4}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    :cond_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 80
    return-object v3

    .line 83
    :cond_6
    :try_start_6
    invoke-interface {p3}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    move-object v2, v3

    .line 86
    .local v2, "winBinder":Landroid/os/IBinder;
    :goto_0
    new-instance v3, Lcom/android/server/wm/MiuiMirrorDragState;

    iget-object v4, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    invoke-direct {v3, v4, p0, p4, v2}, Lcom/android/server/wm/MiuiMirrorDragState;-><init>(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/MiuiMirrorDragDropController;ILandroid/os/IBinder;)V

    iput-object v3, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    .line 87
    iput p1, v3, Lcom/android/server/wm/MiuiMirrorDragState;->mPid:I

    .line 88
    iget-object v3, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    iput p2, v3, Lcom/android/server/wm/MiuiMirrorDragState;->mUid:I

    .line 89
    iget-object v3, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    iput-object v0, v3, Lcom/android/server/wm/MiuiMirrorDragState;->mToken:Landroid/os/IBinder;

    .line 90
    iget-object v3, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    iput p5, v3, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceDisplayId:I

    .line 92
    iget-object v3, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    iput-object p6, v3, Lcom/android/server/wm/MiuiMirrorDragState;->mData:Landroid/content/ClipData;

    .line 93
    iget-object v3, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiMirrorDragState;->prepareDragStartedLocked()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 95
    .end local v2    # "winBinder":Landroid/os/IBinder;
    :try_start_7
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z

    move-result v2

    if-nez v2, :cond_7

    .line 96
    iget-object v2, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    .line 99
    :cond_7
    monitor-exit v1

    .line 100
    return-object v0

    .line 95
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z

    move-result v3

    if-nez v3, :cond_8

    .line 96
    iget-object v3, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    .line 98
    :cond_8
    nop

    .end local v0    # "dragToken":Landroid/os/IBinder;
    .end local p0    # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
    .end local p1    # "callerPid":I
    .end local p2    # "callerUid":I
    .end local p3    # "window":Landroid/view/IWindow;
    .end local p4    # "flags":I
    .end local p5    # "displayId":I
    .end local p6    # "data":Landroid/content/ClipData;
    throw v2

    .line 99
    .restart local v0    # "dragToken":Landroid/os/IBinder;
    .restart local p0    # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
    .restart local p1    # "callerPid":I
    .restart local p2    # "callerUid":I
    .restart local p3    # "window":Landroid/view/IWindow;
    .restart local p4    # "flags":I
    .restart local p5    # "displayId":I
    .restart local p6    # "data":Landroid/content/ClipData;
    :catchall_1
    move-exception v2

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v2
.end method

.method public reportDropResult(ILandroid/view/IWindow;Z)V
    .locals 8
    .param p1, "pid"    # I
    .param p2, "window"    # Landroid/view/IWindow;
    .param p3, "consumed"    # Z

    .line 104
    const/4 v0, 0x0

    if-nez p2, :cond_0

    move-object v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 105
    .local v1, "token":Landroid/os/IBinder;
    :goto_0
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getInstance()Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getDelegatePid()I

    move-result v2

    const/4 v3, 0x0

    if-ne p1, v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    move v2, v3

    .line 107
    .local v2, "fromDelegate":Z
    :goto_1
    iget-object v4, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v4

    .line 108
    :try_start_0
    iget-object v5, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-nez v5, :cond_2

    .line 111
    const-string v0, "MiuiMirrorDragDrop"

    const-string v3, "Drop result given but no drag in progress"

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    monitor-exit v4

    return-void

    .line 115
    :cond_2
    if-eqz v1, :cond_7

    .line 116
    iget-object v5, v5, Lcom/android/server/wm/MiuiMirrorDragState;->mToken:Landroid/os/IBinder;

    if-ne v5, v1, :cond_6

    .line 125
    iget-object v5, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mHandler:Landroid/os/Handler;

    invoke-interface {p2}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 126
    iget-object v5, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v5, v0, p2, v3}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/view/IWindow;Z)Lcom/android/server/wm/WindowState;

    move-result-object v0

    .line 127
    .local v0, "callingWin":Lcom/android/server/wm/WindowState;
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    if-nez v3, :cond_3

    goto :goto_2

    .line 133
    :cond_3
    if-nez p3, :cond_4

    .line 136
    invoke-static {}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getInstance()Lcom/xiaomi/mirror/service/MirrorServiceInternal;

    move-result-object v3

    .line 137
    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v5

    .line 138
    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v6

    iget v6, v6, Lcom/android/server/wm/Task;->mTaskId:I

    iget-object v7, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    iget-object v7, v7, Lcom/android/server/wm/MiuiMirrorDragState;->mData:Landroid/content/ClipData;

    .line 137
    invoke-virtual {v3, v5, v6, v7}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->tryToShareDrag(Ljava/lang/String;ILandroid/content/ClipData;)Z

    move-result v3

    move p3, v3

    .line 140
    .end local v0    # "callingWin":Lcom/android/server/wm/WindowState;
    :cond_4
    goto :goto_3

    .line 128
    .restart local v0    # "callingWin":Lcom/android/server/wm/WindowState;
    :cond_5
    :goto_2
    const-string v3, "MiuiMirrorDragDrop"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad result-reporting window "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v3, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v3, v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked(Z)V

    .line 130
    monitor-exit v4

    return-void

    .line 118
    .end local v0    # "callingWin":Lcom/android/server/wm/WindowState;
    :cond_6
    const-string v0, "MiuiMirrorDragDrop"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid drop-result claim by "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "reportDropResult() by non-recipient"

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .end local v1    # "token":Landroid/os/IBinder;
    .end local v2    # "fromDelegate":Z
    .end local p0    # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
    .end local p1    # "pid":I
    .end local p2    # "window":Landroid/view/IWindow;
    .end local p3    # "consumed":Z
    throw v0

    .line 141
    .restart local v1    # "token":Landroid/os/IBinder;
    .restart local v2    # "fromDelegate":Z
    .restart local p0    # "this":Lcom/android/server/wm/MiuiMirrorDragDropController;
    .restart local p1    # "pid":I
    .restart local p2    # "window":Landroid/view/IWindow;
    .restart local p3    # "consumed":Z
    :cond_7
    if-nez v2, :cond_8

    .line 142
    const-string v0, "MiuiMirrorDragDrop"

    const-string v3, "Try to report a result without a delegate"

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    monitor-exit v4

    return-void

    .line 147
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    iput-boolean p3, v0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z

    .line 148
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked(Z)V

    .line 149
    monitor-exit v4

    .line 150
    return-void

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendDragStartedIfNeededLocked(Ljava/lang/Object;)V
    .locals 2
    .param p1, "window"    # Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    move-object v1, p1

    check-cast v1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMirrorDragState;->sendDragStartedIfNeededLocked(Lcom/android/server/wm/WindowState;)V

    .line 54
    return-void
.end method

.method sendHandlerMessage(ILjava/lang/Object;)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg"    # Ljava/lang/Object;

    .line 213
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 214
    return-void
.end method

.method sendTimeoutMessage(ILjava/lang/Object;)V
    .locals 4
    .param p1, "what"    # I
    .param p2, "arg"    # Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 221
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 222
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 223
    return-void
.end method

.method public shutdownDragAndDropIfNeeded()V
    .locals 3

    .line 153
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 154
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->isInProgress()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z

    .line 156
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController;->mDragState:Lcom/android/server/wm/MiuiMirrorDragState;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    .line 158
    :cond_0
    monitor-exit v0

    .line 159
    return-void

    .line 158
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
