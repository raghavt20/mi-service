.class public final Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl$Provider;
.super Ljava/lang/Object;
.source "TaskStubImpl$MutableTaskStubImpl$Provider.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl$Provider$SINGLETON;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
        "Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideNewInstance()Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;
    .locals 1

    .line 17
    new-instance v0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;

    invoke-direct {v0}, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;-><init>()V

    return-object v0
.end method

.method public bridge synthetic provideNewInstance()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl$Provider;->provideNewInstance()Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;

    move-result-object v0

    return-object v0
.end method

.method public provideSingleton()Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;
    .locals 1

    .line 13
    sget-object v0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl$Provider$SINGLETON;->INSTANCE:Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;

    return-object v0
.end method

.method public bridge synthetic provideSingleton()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl$Provider;->provideSingleton()Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;

    move-result-object v0

    return-object v0
.end method
