.class Lcom/android/server/wm/AppResurrectionTrackManager$1;
.super Ljava/lang/Object;
.source "AppResurrectionTrackManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/AppResurrectionTrackManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/AppResurrectionTrackManager;


# direct methods
.method constructor <init>(Lcom/android/server/wm/AppResurrectionTrackManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/AppResurrectionTrackManager;

    .line 36
    iput-object p1, p0, Lcom/android/server/wm/AppResurrectionTrackManager$1;->this$0:Lcom/android/server/wm/AppResurrectionTrackManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 39
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager$1;->this$0:Lcom/android/server/wm/AppResurrectionTrackManager;

    invoke-static {p2}, Lcom/miui/analytics/ITrackBinder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/wm/AppResurrectionTrackManager;->-$$Nest$fputmITrackBinder(Lcom/android/server/wm/AppResurrectionTrackManager;Lcom/miui/analytics/ITrackBinder;)V

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onServiceConnected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionTrackManager$1;->this$0:Lcom/android/server/wm/AppResurrectionTrackManager;

    invoke-static {v1}, Lcom/android/server/wm/AppResurrectionTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/AppResurrectionTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppResurrectionTrackManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 45
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager$1;->this$0:Lcom/android/server/wm/AppResurrectionTrackManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/wm/AppResurrectionTrackManager;->-$$Nest$fputmITrackBinder(Lcom/android/server/wm/AppResurrectionTrackManager;Lcom/miui/analytics/ITrackBinder;)V

    .line 46
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager$1;->this$0:Lcom/android/server/wm/AppResurrectionTrackManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/wm/AppResurrectionTrackManager;->-$$Nest$fputmIsBind(Lcom/android/server/wm/AppResurrectionTrackManager;Z)V

    .line 47
    const-string v0, "AppResurrectionTrackManager"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    return-void
.end method
