class com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx extends com.android.server.wm.Letterbox$LetterboxSurface {
	 /* .source "LetterboxImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/LetterboxImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "LetterboxSurfaceEx" */
} // .end annotation
/* # instance fields */
private android.graphics.Bitmap mBackgroundBitmap;
private android.view.Surface mBackgroundSurface;
android.graphics.Paint mPaint;
final com.android.server.wm.LetterboxImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/wm/LetterboxImpl; */
/* .param p2, "type" # Ljava/lang/String; */
/* .param p3, "impl" # Lcom/android/server/wm/LetterboxImpl; */
/* .line 208 */
this.this$0 = p1;
/* .line 209 */
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmLetterbox ( p3 );
java.util.Objects .requireNonNull ( v0 );
/* invoke-direct {p0, v0, p2}, Lcom/android/server/wm/Letterbox$LetterboxSurface;-><init>(Lcom/android/server/wm/Letterbox;Ljava/lang/String;)V */
/* .line 206 */
/* new-instance v0, Landroid/graphics/Paint; */
/* invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V */
this.mPaint = v0;
/* .line 210 */
return;
} // .end method
private Boolean applyLetterboxSurfaceChangesForActivityEmbedding ( android.graphics.Rect p0, android.graphics.Rect p1 ) {
/* .locals 3 */
/* .param p1, "mSurfaceFrameRelative" # Landroid/graphics/Rect; */
/* .param p2, "mLayoutFrameRelative" # Landroid/graphics/Rect; */
/* .line 397 */
v0 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v0 );
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).isEmbedded ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 398 */
	 v0 = 	 (( android.graphics.Rect ) p1 ).height ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->height()I
	 v2 = 	 (( android.graphics.Rect ) p2 ).height ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->height()I
	 /* if-eq v0, v2, :cond_0 */
	 /* .line 399 */
	 v0 = 	 (( android.graphics.Rect ) p1 ).width ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->width()I
	 v2 = 	 (( android.graphics.Rect ) p2 ).width ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->width()I
	 /* if-eq v0, v2, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 398 */
} // :goto_0
/* .line 401 */
} // :cond_1
} // .end method
private void drawBackgroundBitmapLocked ( android.graphics.Bitmap p0, android.graphics.Rect p1 ) {
/* .locals 12 */
/* .param p1, "bgBitmap" # Landroid/graphics/Bitmap; */
/* .param p2, "rect" # Landroid/graphics/Rect; */
/* .line 245 */
if ( p1 != null) { // if-eqz p1, :cond_5
v0 = this.mBackgroundSurface;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 246 */
int v1 = 0; // const/4 v1, 0x0
/* .line 248 */
/* .local v1, "canvas":Landroid/graphics/Canvas; */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
	 (( android.view.Surface ) v0 ).lockCanvas ( v2 ); // invoke-virtual {v0, v2}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* move-object v1, v0 */
	 /* .line 251 */
	 /* .line 249 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 250 */
	 /* .local v0, "e":Ljava/lang/RuntimeException; */
	 final String v3 = "LetterboxImpl"; // const-string v3, "LetterboxImpl"
	 final String v4 = "Failed to lock canvas"; // const-string v4, "Failed to lock canvas"
	 android.util.Slog .w ( v3,v4,v0 );
	 /* .line 252 */
} // .end local v0 # "e":Ljava/lang/RuntimeException;
} // :goto_0
/* if-nez v1, :cond_0 */
/* .line 253 */
return;
/* .line 255 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 256 */
/* .local v0, "left":I */
v3 = (( android.graphics.Bitmap ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I
/* add-int/2addr v3, v0 */
/* .line 257 */
/* .local v3, "right":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 258 */
/* .local v4, "top":I */
v5 = (( android.graphics.Bitmap ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
/* .line 259 */
/* .local v5, "bottom":I */
v6 = (( android.graphics.Bitmap ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I
v7 = (( android.graphics.Rect ) p2 ).width ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->width()I
/* if-lt v6, v7, :cond_1 */
v6 = (( android.graphics.Bitmap ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
v7 = (( android.graphics.Rect ) p2 ).height ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->height()I
/* if-lt v6, v7, :cond_1 */
/* .line 260 */
v6 = (( android.graphics.Bitmap ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I
/* .line 261 */
/* .local v6, "inWidth":I */
v7 = (( android.graphics.Rect ) p2 ).width ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->width()I
/* .line 262 */
/* .local v7, "outWidth":I */
v8 = (( android.graphics.Bitmap ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
/* .line 263 */
/* .local v8, "inHeight":I */
v9 = (( android.graphics.Rect ) p2 ).height ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->height()I
/* .line 264 */
/* .local v9, "outHeight":I */
v7 = java.lang.Math .min ( v6,v7 );
/* .line 265 */
v9 = java.lang.Math .min ( v8,v9 );
/* .line 266 */
/* sub-int v10, v6, v7 */
/* int-to-float v10, v10 */
/* const/high16 v11, 0x3f000000 # 0.5f */
/* mul-float/2addr v10, v11 */
/* float-to-int v0, v10 */
/* .line 267 */
/* add-int v3, v0, v7 */
/* .line 268 */
/* sub-int v10, v8, v9 */
/* int-to-float v10, v10 */
/* mul-float/2addr v10, v11 */
/* float-to-int v4, v10 */
/* .line 269 */
/* add-int v5, v4, v9 */
/* .line 271 */
} // .end local v6 # "inWidth":I
} // .end local v7 # "outWidth":I
} // .end local v8 # "inHeight":I
} // .end local v9 # "outHeight":I
} // :cond_1
/* new-instance v6, Landroid/graphics/Rect; */
/* invoke-direct {v6, v0, v4, v3, v5}, Landroid/graphics/Rect;-><init>(IIII)V */
(( android.graphics.Canvas ) v1 ).drawBitmap ( p1, v6, p2, v2 ); // invoke-virtual {v1, p1, v6, p2, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
/* .line 272 */
v2 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v2 );
v2 = (( com.android.server.wm.ActivityRecord ) v2 ).isEmbedded ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 273 */
v2 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v2 );
(( com.android.server.wm.ActivityRecord ) v2 ).getTaskFragment ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getTaskFragment()Lcom/android/server/wm/TaskFragment;
/* .line 274 */
/* .local v2, "taskFragment":Lcom/android/server/wm/TaskFragment; */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 275 */
(( com.android.server.wm.TaskFragment ) v2 ).getAdjacentTaskFragment ( ); // invoke-virtual {v2}, Lcom/android/server/wm/TaskFragment;->getAdjacentTaskFragment()Lcom/android/server/wm/TaskFragment;
/* .line 276 */
/* .local v6, "adjacentTaskFragment":Lcom/android/server/wm/TaskFragment; */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 277 */
v7 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v7 );
v7 = this.mAtmService;
v7 = this.mContext;
/* .line 278 */
/* .local v7, "context":Landroid/content/Context; */
(( android.content.Context ) v7 ).getResources ( ); // invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v8 ).getConfiguration ( ); // invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* iget v8, v8, Landroid/content/res/Configuration;->uiMode:I */
/* and-int/lit8 v8, v8, 0x30 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_2 */
int v8 = 1; // const/4 v8, 0x1
} // :cond_2
int v8 = 0; // const/4 v8, 0x0
/* .line 280 */
/* .local v8, "isDarkMode":Z */
} // :goto_1
v9 = this.mPaint;
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 281 */
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
v11 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v11 );
v10 = v11 = this.packageName;
/* .line 282 */
} // :cond_3
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
v11 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v11 );
v10 = v11 = this.packageName;
/* .line 280 */
} // :goto_2
(( android.graphics.Paint ) v9 ).setColor ( v10 ); // invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V
/* .line 283 */
/* new-instance v9, Landroid/graphics/Rect; */
/* invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V */
/* .line 284 */
/* .local v9, "splitLineRect":Landroid/graphics/Rect; */
/* invoke-direct {p0, v2, v6, p2, v9}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getSplitLineRect(Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/TaskFragment;Landroid/graphics/Rect;Landroid/graphics/Rect;)V */
/* .line 285 */
v10 = (( android.graphics.Rect ) v9 ).isEmpty ( ); // invoke-virtual {v9}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v10, :cond_4 */
/* .line 286 */
v10 = this.mPaint;
(( android.graphics.Canvas ) v1 ).drawRect ( v9, v10 ); // invoke-virtual {v1, v9, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V
/* .line 291 */
} // .end local v2 # "taskFragment":Lcom/android/server/wm/TaskFragment;
} // .end local v6 # "adjacentTaskFragment":Lcom/android/server/wm/TaskFragment;
} // .end local v7 # "context":Landroid/content/Context;
} // .end local v8 # "isDarkMode":Z
} // .end local v9 # "splitLineRect":Landroid/graphics/Rect;
} // :cond_4
v2 = this.mBackgroundSurface;
(( android.view.Surface ) v2 ).unlockCanvasAndPost ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
/* .line 293 */
} // .end local v0 # "left":I
} // .end local v1 # "canvas":Landroid/graphics/Canvas;
} // .end local v3 # "right":I
} // .end local v4 # "top":I
} // .end local v5 # "bottom":I
} // :cond_5
return;
} // .end method
private void getBitmapSurfaceParams ( android.graphics.Rect p0 ) {
/* .locals 5 */
/* .param p1, "rect" # Landroid/graphics/Rect; */
/* .line 308 */
v0 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v0 );
/* if-nez v0, :cond_0 */
/* .line 309 */
return;
/* .line 310 */
} // :cond_0
v0 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v0 );
(( com.android.server.wm.ActivityRecord ) v0 ).getDisplayContent ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
/* .line 311 */
/* .local v0, "dc":Lcom/android/server/wm/DisplayContent; */
v1 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v1 );
v1 = (( com.android.server.wm.DisplayContent ) v0 ).isFixedRotationLaunchingApp ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayContent;->isFixedRotationLaunchingApp(Lcom/android/server/wm/ActivityRecord;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 312 */
v1 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v1 );
(( com.android.server.wm.ActivityRecord ) v1 ).getFixedRotationTransformDisplayInfo ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getFixedRotationTransformDisplayInfo()Landroid/view/DisplayInfo;
} // :cond_1
(( com.android.server.wm.DisplayContent ) v0 ).getDisplayInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;
/* .line 313 */
/* .local v1, "displayInfo":Landroid/view/DisplayInfo; */
} // :goto_0
/* if-nez v1, :cond_2 */
/* .line 314 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "skip getBitmapSurfaceParams, displayInfo == null ,mActivityRecord = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " ,isFixedRotationLaunchingApp = "; // const-string v3, " ,isFixedRotationLaunchingApp = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v3 );
/* .line 316 */
v3 = (( com.android.server.wm.DisplayContent ) v0 ).isFixedRotationLaunchingApp ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/wm/DisplayContent;->isFixedRotationLaunchingApp(Lcom/android/server/wm/ActivityRecord;)Z
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 314 */
final String v3 = "LetterboxImpl"; // const-string v3, "LetterboxImpl"
android.util.Slog .w ( v3,v2 );
/* .line 318 */
return;
/* .line 320 */
} // :cond_2
/* iget v2, v1, Landroid/view/DisplayInfo;->logicalWidth:I */
/* iget v3, v1, Landroid/view/DisplayInfo;->logicalHeight:I */
int v4 = 0; // const/4 v4, 0x0
(( android.graphics.Rect ) p1 ).set ( v4, v4, v2, v3 ); // invoke-virtual {p1, v4, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V
/* .line 334 */
return;
} // .end method
private void getSplitLineRect ( com.android.server.wm.TaskFragment p0, com.android.server.wm.TaskFragment p1, android.graphics.Rect p2, android.graphics.Rect p3 ) {
/* .locals 6 */
/* .param p1, "tf1" # Lcom/android/server/wm/TaskFragment; */
/* .param p2, "tf2" # Lcom/android/server/wm/TaskFragment; */
/* .param p3, "bitmapRect" # Landroid/graphics/Rect; */
/* .param p4, "rect" # Landroid/graphics/Rect; */
/* .line 296 */
if ( p1 != null) { // if-eqz p1, :cond_1
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 297 */
(( com.android.server.wm.TaskFragment ) p1 ).getBounds ( ); // invoke-virtual {p1}, Lcom/android/server/wm/TaskFragment;->getBounds()Landroid/graphics/Rect;
/* .line 298 */
/* .local v0, "tf1Rect":Landroid/graphics/Rect; */
(( com.android.server.wm.TaskFragment ) p2 ).getBounds ( ); // invoke-virtual {p2}, Lcom/android/server/wm/TaskFragment;->getBounds()Landroid/graphics/Rect;
/* .line 299 */
/* .local v1, "tf2Rect":Landroid/graphics/Rect; */
/* iget v2, v0, Landroid/graphics/Rect;->right:I */
/* iget v3, v1, Landroid/graphics/Rect;->left:I */
/* if-ge v2, v3, :cond_0 */
/* .line 300 */
/* iget v2, v0, Landroid/graphics/Rect;->right:I */
/* iget v3, p3, Landroid/graphics/Rect;->top:I */
/* iget v4, v1, Landroid/graphics/Rect;->left:I */
/* iget v5, p3, Landroid/graphics/Rect;->bottom:I */
(( android.graphics.Rect ) p4 ).set ( v2, v3, v4, v5 ); // invoke-virtual {p4, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V
/* .line 302 */
} // :cond_0
/* iget v2, v1, Landroid/graphics/Rect;->right:I */
/* iget v3, p3, Landroid/graphics/Rect;->top:I */
/* iget v4, v0, Landroid/graphics/Rect;->left:I */
/* iget v5, p3, Landroid/graphics/Rect;->bottom:I */
(( android.graphics.Rect ) p4 ).set ( v2, v3, v4, v5 ); // invoke-virtual {p4, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V
/* .line 305 */
} // .end local v0 # "tf1Rect":Landroid/graphics/Rect;
} // .end local v1 # "tf2Rect":Landroid/graphics/Rect;
} // :cond_1
} // :goto_0
return;
} // .end method
private Boolean getWallpaperBitmap ( ) {
/* .locals 2 */
/* .line 213 */
v0 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 214 */
/* .line 216 */
} // :cond_0
v0 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v0 );
v0 = this.mWmService;
(( com.android.server.wm.WindowManagerService ) v0 ).getBlurWallpaperBmp ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getBlurWallpaperBmp()Landroid/graphics/Bitmap;
/* .line 217 */
/* .local v0, "backgroundBitmap":Landroid/graphics/Bitmap; */
/* if-nez v0, :cond_1 */
/* .line 218 */
/* .line 220 */
} // :cond_1
this.mBackgroundBitmap = v0;
/* .line 221 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
/* # virtual methods */
public Boolean applyLetterboxSurfaceChanges ( android.view.SurfaceControl$Transaction p0, android.graphics.Rect p1, android.graphics.Rect p2, java.lang.String p3 ) {
/* .locals 1 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "mSurfaceFrameRelative" # Landroid/graphics/Rect; */
/* .param p3, "mLayoutFrameRelative" # Landroid/graphics/Rect; */
/* .param p4, "mType" # Ljava/lang/String; */
/* .line 386 */
v0 = (( android.graphics.Rect ) p2 ).equals ( p3 ); // invoke-virtual {p2, p3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 387 */
final String v0 = "bitmapBackground"; // const-string v0, "bitmapBackground"
v0 = (( java.lang.String ) v0 ).equals ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 388 */
v0 = (( android.graphics.Rect ) p2 ).isEmpty ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v0, :cond_0 */
/* .line 389 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getWallpaperBitmap()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mBackgroundBitmap;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 390 */
v0 = /* invoke-direct {p0, p2, p3}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->applyLetterboxSurfaceChangesForActivityEmbedding(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 391 */
int v0 = 1; // const/4 v0, 0x1
/* .line 393 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public android.view.SurfaceControl createFlipSmallSurface ( android.view.SurfaceControl$Transaction p0, java.util.function.Supplier p1 ) {
/* .locals 2 */
/* .param p1, "sct" # Landroid/view/SurfaceControl$Transaction; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/view/SurfaceControl$Transaction;", */
/* "Ljava/util/function/Supplier<", */
/* "Landroid/view/SurfaceControl$Builder;", */
/* ">;)", */
/* "Landroid/view/SurfaceControl;" */
/* } */
} // .end annotation
/* .line 339 */
/* .local p2, "supplier":Ljava/util/function/Supplier;, "Ljava/util/function/Supplier<Landroid/view/SurfaceControl$Builder;>;" */
/* check-cast v0, Landroid/view/SurfaceControl$Builder; */
/* .line 340 */
final String v1 = "Letterbox - NBC"; // const-string v1, "Letterbox - NBC"
(( android.view.SurfaceControl$Builder ) v0 ).setName ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 341 */
int v1 = 4; // const/4 v1, 0x4
(( android.view.SurfaceControl$Builder ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setFlags(I)Landroid/view/SurfaceControl$Builder;
/* .line 342 */
(( android.view.SurfaceControl$Builder ) v0 ).setColorLayer ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->setColorLayer()Landroid/view/SurfaceControl$Builder;
/* .line 343 */
final String v1 = "LetterboxSurface.createSurface"; // const-string v1, "LetterboxSurface.createSurface"
(( android.view.SurfaceControl$Builder ) v0 ).setCallsite ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 344 */
(( android.view.SurfaceControl$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
/* .line 345 */
/* .local v0, "surfaceControl":Landroid/view/SurfaceControl; */
v1 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$mupdateColor ( v1,v0,p1 );
/* .line 346 */
(( android.view.SurfaceControl$Transaction ) p1 ).show ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 347 */
} // .end method
public android.view.SurfaceControl createSurface ( android.view.SurfaceControl$Transaction p0, java.lang.String p1, java.util.function.Supplier p2 ) {
/* .locals 6 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "mType" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/view/SurfaceControl$Transaction;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/function/Supplier<", */
/* "Landroid/view/SurfaceControl$Builder;", */
/* ">;)", */
/* "Landroid/view/SurfaceControl;" */
/* } */
} // .end annotation
/* .line 352 */
/* .local p3, "mSurfaceControlFactory":Ljava/util/function/Supplier;, "Ljava/util/function/Supplier<Landroid/view/SurfaceControl$Builder;>;" */
/* new-instance v0, Landroid/graphics/Rect; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V */
/* .line 353 */
/* .local v0, "rect":Landroid/graphics/Rect; */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getBitmapSurfaceParams(Landroid/graphics/Rect;)V */
/* .line 354 */
v2 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
v3 = (( android.graphics.Rect ) v0 ).height ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
v2 = java.lang.Math .max ( v2,v3 );
/* .line 355 */
/* .local v2, "longSide":I */
v3 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v3 );
v3 = (( com.android.server.wm.ActivityRecord ) v3 ).isEmbedded ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z
/* if-nez v3, :cond_1 */
v3 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v3 );
v3 = (( com.android.server.wm.ActivityRecord ) v3 ).isLetterboxedForFixedOrientationAndAspectRatio ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->isLetterboxedForFixedOrientationAndAspectRatio()Z
if ( v3 != null) { // if-eqz v3, :cond_0
} // :cond_0
/* new-instance v3, Landroid/graphics/Rect; */
/* invoke-direct {v3, v1, v1, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V */
} // :cond_1
} // :goto_0
/* move-object v3, v0 */
} // :goto_1
/* move-object v1, v3 */
/* .line 356 */
/* .local v1, "bitmapRect":Landroid/graphics/Rect; */
/* check-cast v3, Landroid/view/SurfaceControl$Builder; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Letterbox - "; // const-string v5, "Letterbox - "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " - "; // const-string v5, " - "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 357 */
(( android.view.SurfaceControl$Builder ) v3 ).setName ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 358 */
int v4 = 4; // const/4 v4, 0x4
(( android.view.SurfaceControl$Builder ) v3 ).setFlags ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setFlags(I)Landroid/view/SurfaceControl$Builder;
/* .line 360 */
v4 = (( android.graphics.Rect ) v1 ).width ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->width()I
v5 = (( android.graphics.Rect ) v1 ).height ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->height()I
(( android.view.SurfaceControl$Builder ) v3 ).setBufferSize ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/view/SurfaceControl$Builder;->setBufferSize(II)Landroid/view/SurfaceControl$Builder;
/* .line 361 */
final String v4 = "LetterboxSurface.createSurface"; // const-string v4, "LetterboxSurface.createSurface"
(( android.view.SurfaceControl$Builder ) v3 ).setCallsite ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 362 */
(( android.view.SurfaceControl$Builder ) v3 ).build ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
/* .line 363 */
/* .local v3, "mSurface":Landroid/view/SurfaceControl; */
v4 = /* invoke-direct {p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getWallpaperBitmap()Z */
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = this.mBackgroundBitmap;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 364 */
/* new-instance v4, Landroid/view/Surface; */
/* invoke-direct {v4, v3}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceControl;)V */
this.mBackgroundSurface = v4;
/* .line 365 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
v5 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " create "; // const-string v5, " create "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "bitmapBackground"; // const-string v5, "bitmapBackground"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "LetterboxImpl"; // const-string v5, "LetterboxImpl"
android.util.Slog .d ( v5,v4 );
/* .line 366 */
v4 = this.mBackgroundBitmap;
/* invoke-direct {p0, v4, v1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->drawBackgroundBitmapLocked(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V */
/* .line 367 */
v4 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v4 );
v4 = (( com.android.server.wm.ActivityRecord ) v4 ).isEmbedded ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z
/* if-nez v4, :cond_2 */
v4 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v4 );
v4 = (( com.android.server.wm.ActivityRecord ) v4 ).isLetterboxedForFixedOrientationAndAspectRatio ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->isLetterboxedForFixedOrientationAndAspectRatio()Z
/* if-nez v4, :cond_2 */
/* .line 368 */
(( android.view.SurfaceControl$Transaction ) p1 ).show ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 371 */
} // :cond_2
} // .end method
public Boolean needRemoveMiuiLetterbox ( ) {
/* .locals 1 */
/* .line 422 */
v0 = this.mBackgroundSurface;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void redrawBitmapSurface ( ) {
/* .locals 5 */
/* .line 405 */
/* new-instance v0, Landroid/graphics/Rect; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V */
/* .line 406 */
/* .local v0, "rect":Landroid/graphics/Rect; */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getBitmapSurfaceParams(Landroid/graphics/Rect;)V */
/* .line 407 */
v2 = this.mBackgroundSurface;
/* if-nez v2, :cond_1 */
/* .line 408 */
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) p0 ).getSurface ( ); // invoke-virtual {p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getSurface()Landroid/view/SurfaceControl;
/* if-nez v2, :cond_0 */
/* .line 409 */
return;
/* .line 411 */
} // :cond_0
/* new-instance v2, Landroid/view/Surface; */
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) p0 ).getSurface ( ); // invoke-virtual {p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getSurface()Landroid/view/SurfaceControl;
/* invoke-direct {v2, v3}, Landroid/view/Surface;-><init>(Landroid/view/SurfaceControl;)V */
this.mBackgroundSurface = v2;
/* .line 413 */
} // :cond_1
v2 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
v3 = (( android.graphics.Rect ) v0 ).height ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
v2 = java.lang.Math .max ( v2,v3 );
/* .line 414 */
/* .local v2, "longSide":I */
v3 = this.mBackgroundBitmap;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 415 */
v3 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v3 );
v3 = (( com.android.server.wm.ActivityRecord ) v3 ).isEmbedded ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z
/* if-nez v3, :cond_3 */
v3 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v3 );
v3 = (( com.android.server.wm.ActivityRecord ) v3 ).isLetterboxedForFixedOrientationAndAspectRatio ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->isLetterboxedForFixedOrientationAndAspectRatio()Z
if ( v3 != null) { // if-eqz v3, :cond_2
} // :cond_2
/* new-instance v3, Landroid/graphics/Rect; */
/* invoke-direct {v3, v1, v1, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V */
} // :cond_3
} // :goto_0
/* move-object v3, v0 */
} // :goto_1
/* move-object v1, v3 */
/* .line 416 */
/* .local v1, "bitmapRect":Landroid/graphics/Rect; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " redraw "; // const-string v4, " redraw "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "bitmapBackground"; // const-string v4, "bitmapBackground"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "LetterboxImpl"; // const-string v4, "LetterboxImpl"
android.util.Slog .d ( v4,v3 );
/* .line 417 */
v3 = this.mBackgroundBitmap;
/* invoke-direct {p0, v3, v1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->drawBackgroundBitmapLocked(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V */
/* .line 419 */
} // .end local v1 # "bitmapRect":Landroid/graphics/Rect;
} // :cond_4
return;
} // .end method
public void remove ( ) {
/* .locals 2 */
/* .line 375 */
/* invoke-super {p0}, Lcom/android/server/wm/Letterbox$LetterboxSurface;->remove()V */
/* .line 376 */
v0 = this.mBackgroundSurface;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 377 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "remove letterbox surface bitmapBackground for "; // const-string v1, "remove letterbox surface bitmapBackground for "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
com.android.server.wm.LetterboxImpl .-$$Nest$fgetmActivityRecord ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "LetterboxImpl"; // const-string v1, "LetterboxImpl"
android.util.Slog .d ( v1,v0 );
/* .line 378 */
v0 = this.mBackgroundSurface;
(( android.view.Surface ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/view/Surface;->release()V
/* .line 379 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->mShowed:Z */
/* .line 380 */
int v0 = 0; // const/4 v0, 0x0
this.mBackgroundSurface = v0;
/* .line 382 */
} // :cond_0
return;
} // .end method
