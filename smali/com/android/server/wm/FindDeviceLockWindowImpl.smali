.class public Lcom/android/server/wm/FindDeviceLockWindowImpl;
.super Ljava/lang/Object;
.source "FindDeviceLockWindowImpl.java"

# interfaces
.implements Lcom/android/server/wm/FindDeviceLockWindowStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/FindDeviceLockWindowImpl$LockDeviceWindowPolicy;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FindDeviceLockWindowImpl"

.field private static sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

.field private static sTmpLockWindow:Lcom/android/server/wm/WindowState;


# direct methods
.method static bridge synthetic -$$Nest$sfgetsTmpFirstAppWindow()Lcom/android/server/wm/WindowState;
    .locals 1

    sget-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetsTmpLockWindow()Lcom/android/server/wm/WindowState;
    .locals 1

    sget-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpLockWindow:Lcom/android/server/wm/WindowState;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfputsTmpFirstAppWindow(Lcom/android/server/wm/WindowState;)V
    .locals 0

    sput-object p0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputsTmpLockWindow(Lcom/android/server/wm/WindowState;)V
    .locals 0

    sput-object p0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpLockWindow:Lcom/android/server/wm/WindowState;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isObscuringFullScreen(Lcom/android/server/wm/WindowState;Landroid/view/WindowManager$LayoutParams;)Z
    .locals 3
    .param p0, "win"    # Lcom/android/server/wm/WindowState;
    .param p1, "params"    # Landroid/view/WindowManager$LayoutParams;

    .line 110
    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 111
    :cond_0
    if-nez p1, :cond_1

    return v0

    .line 113
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isObscuringDisplay()Z

    move-result v1

    if-nez v1, :cond_2

    return v0

    .line 114
    :cond_2
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    if-nez v1, :cond_3

    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    if-nez v1, :cond_3

    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    if-ne v1, v2, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0
.end method


# virtual methods
.method public updateLockDeviceWindowLocked(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/DisplayContent;)V
    .locals 7
    .param p1, "wms"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "dc"    # Lcom/android/server/wm/DisplayContent;

    .line 18
    if-eqz p1, :cond_6

    if-nez p2, :cond_0

    goto/16 :goto_2

    .line 21
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lcom/android/server/wm/FindDeviceLockWindowImpl$1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/FindDeviceLockWindowImpl$1;-><init>(Lcom/android/server/wm/FindDeviceLockWindowImpl;)V

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Lcom/android/server/wm/DisplayContent;->forAllWindows(Lcom/android/internal/util/ToBooleanFunction;Z)Z

    .line 46
    sget-object v1, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpLockWindow:Lcom/android/server/wm/WindowState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 103
    sput-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpLockWindow:Lcom/android/server/wm/WindowState;

    .line 104
    sput-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    .line 47
    return-void

    .line 50
    :cond_1
    const/4 v1, 0x0

    .line 51
    .local v1, "hideLockWindow":Z
    :try_start_1
    sget-object v3, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    if-eqz v3, :cond_3

    .line 52
    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v3, v3, 0x1000

    if-eqz v3, :cond_3

    .line 54
    sget-object v3, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->isDrawn()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 55
    sget-object v3, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    iget-object v4, v3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    invoke-static {v3, v4}, Lcom/android/server/wm/FindDeviceLockWindowImpl;->isObscuringFullScreen(Lcom/android/server/wm/WindowState;Landroid/view/WindowManager$LayoutParams;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 56
    const/4 v1, 0x1

    goto :goto_0

    .line 57
    :cond_2
    sget-object v3, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->inFreeformWindowingMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 58
    sget-object v3, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    .line 59
    .local v3, "task":Lcom/android/server/wm/Task;
    if-eqz v3, :cond_3

    .line 60
    iget-object v4, v3, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    iget v5, v3, Lcom/android/server/wm/Task;->mTaskId:I

    .line 61
    invoke-interface {v4, v5}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v4

    .line 62
    .local v4, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    if-eqz v4, :cond_3

    .line 63
    iget-object v5, v3, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    iget v6, v3, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-interface {v5, v6}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->fullscreenFreeformTask(I)V

    .line 85
    .end local v3    # "task":Lcom/android/server/wm/Task;
    .end local v4    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    :cond_3
    :goto_0
    const/4 v3, 0x0

    if-eqz v1, :cond_4

    .line 86
    sget-object v4, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpLockWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v4, v3, v3}, Lcom/android/server/wm/WindowState;->hide(ZZ)Z

    move-result v3

    .local v3, "change":Z
    goto :goto_1

    .line 88
    .end local v3    # "change":Z
    :cond_4
    sget-object v4, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpLockWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v4, v3, v3}, Lcom/android/server/wm/WindowState;->show(ZZ)Z

    move-result v3

    .line 91
    .restart local v3    # "change":Z
    :goto_1
    if-eqz v3, :cond_5

    .line 92
    iput-boolean v2, p1, Lcom/android/server/wm/WindowManagerService;->mFocusMayChange:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    .end local v1    # "hideLockWindow":Z
    .end local v3    # "change":Z
    :cond_5
    sput-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpLockWindow:Lcom/android/server/wm/WindowState;

    .line 104
    sput-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    .line 105
    nop

    .line 106
    return-void

    .line 103
    :catchall_0
    move-exception v1

    sput-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpLockWindow:Lcom/android/server/wm/WindowState;

    .line 104
    sput-object v0, Lcom/android/server/wm/FindDeviceLockWindowImpl;->sTmpFirstAppWindow:Lcom/android/server/wm/WindowState;

    .line 105
    throw v1

    .line 18
    :cond_6
    :goto_2
    return-void
.end method
