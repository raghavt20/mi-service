.class public Lcom/android/server/wm/MirrorActiveUidsImpl;
.super Lcom/android/server/wm/MirrorActiveUidsStub;
.source "MirrorActiveUidsImpl.java"


# instance fields
.field private final mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 10
    invoke-direct {p0}, Lcom/android/server/wm/MirrorActiveUidsStub;-><init>()V

    .line 12
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mNumAppVisibleWindowForUserMap:["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 38
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    .line 39
    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 38
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 37
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 41
    .end local v0    # "i":I
    :cond_0
    const-string v0, "]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public hasVisibleWindowForUser(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 16
    iget-object v0, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onVisibleWindowForUserChanged(IZ)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "visible"    # Z

    .line 21
    iget-object v0, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    .line 22
    .local v0, "index":I
    const/4 v1, 0x1

    if-ltz v0, :cond_2

    .line 23
    iget-object v2, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    :goto_0
    add-int/2addr v2, v1

    .line 24
    .local v2, "num":I
    if-lez v2, :cond_1

    .line 25
    iget-object v1, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseIntArray;->setValueAt(II)V

    goto :goto_1

    .line 27
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->removeAt(I)V

    goto :goto_1

    .line 29
    .end local v2    # "num":I
    :cond_2
    if-eqz p2, :cond_3

    .line 30
    iget-object v2, p0, Lcom/android/server/wm/MirrorActiveUidsImpl;->mNumAppVisibleWindowForUserMap:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseIntArray;->append(II)V

    goto :goto_2

    .line 29
    :cond_3
    :goto_1
    nop

    .line 32
    :goto_2
    return-void
.end method
