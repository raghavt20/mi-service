class com.android.server.wm.AppResolutionTuner$AppRTConfig {
	 /* .source "AppResolutionTuner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AppResolutionTuner; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "AppRTConfig" */
} // .end annotation
/* # instance fields */
private java.util.List activityWhitelist;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List filteredWindows;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Float scale;
private java.lang.String scalingFlow;
final com.android.server.wm.AppResolutionTuner this$0; //synthetic
/* # direct methods */
static java.util.List -$$Nest$mgetFilteredWindows ( com.android.server.wm.AppResolutionTuner$AppRTConfig p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->getFilteredWindows()Ljava/util/List; */
} // .end method
static Float -$$Nest$mgetScale ( com.android.server.wm.AppResolutionTuner$AppRTConfig p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->getScale()F */
} // .end method
static java.lang.String -$$Nest$mgetScalingFlow ( com.android.server.wm.AppResolutionTuner$AppRTConfig p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->getScalingFlow()Ljava/lang/String; */
} // .end method
static Boolean -$$Nest$misActivityWhitelist ( com.android.server.wm.AppResolutionTuner$AppRTConfig p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->isActivityWhitelist(Ljava/lang/String;)Z */
} // .end method
public com.android.server.wm.AppResolutionTuner$AppRTConfig ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/wm/AppResolutionTuner; */
/* .param p2, "scale" # F */
/* .param p4, "scalingFlow" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 160 */
/* .local p3, "filteredWindows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p5, "activityWhitelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 155 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* iput v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scale:F */
/* .line 156 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.filteredWindows = v0;
/* .line 157 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.activityWhitelist = v0;
/* .line 158 */
/* const-string/jumbo v0, "wms" */
this.scalingFlow = v0;
/* .line 161 */
/* iput p2, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scale:F */
/* .line 162 */
this.filteredWindows = p3;
/* .line 163 */
this.scalingFlow = p4;
/* .line 164 */
this.activityWhitelist = p5;
/* .line 165 */
return;
} // .end method
private java.util.List getFilteredWindows ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 172 */
v0 = this.filteredWindows;
} // .end method
private Float getScale ( ) {
/* .locals 1 */
/* .line 168 */
/* iget v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scale:F */
} // .end method
private java.lang.String getScalingFlow ( ) {
/* .locals 1 */
/* .line 185 */
v0 = this.scalingFlow;
} // .end method
private Boolean isActivityWhitelist ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "target" # Ljava/lang/String; */
/* .line 176 */
v0 = this.activityWhitelist;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 177 */
/* .local v1, "s":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "target: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " list: "; // const-string v3, " list: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " isEmpty: "; // const-string v3, " isEmpty: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "APP_RT"; // const-string v3, "APP_RT"
android.util.Slog .e ( v3,v2 );
/* .line 178 */
v2 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v2, :cond_0 */
v2 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 179 */
int v0 = 1; // const/4 v0, 0x1
/* .line 180 */
} // .end local v1 # "s":Ljava/lang/String;
} // :cond_0
/* .line 181 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 190 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppRTConfig{scale="; // const-string v1, "AppRTConfig{scale="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scale:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", filteredWindows="; // const-string v1, ", filteredWindows="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.filteredWindows;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", activityWhitelist="; // const-string v1, ", activityWhitelist="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.activityWhitelist;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", scalingFlow=\'"; // const-string v1, ", scalingFlow=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.scalingFlow;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
