.class Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;
.super Ljava/lang/Object;
.source "MiuiRefreshRatePolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiRefreshRatePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageRefreshRate"
.end annotation


# instance fields
.field private final mPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/wm/MiuiRefreshRatePolicy;


# direct methods
.method static bridge synthetic -$$Nest$fgetmPackages(Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;->mPackages:Ljava/util/HashMap;

    return-object p0
.end method

.method constructor <init>(Lcom/android/server/wm/MiuiRefreshRatePolicy;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiRefreshRatePolicy;

    .line 25
    iput-object p1, p0, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;->this$0:Lcom/android/server/wm/MiuiRefreshRatePolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;->mPackages:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;F)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "preferredMaxRefreshRate"    # F

    .line 29
    iget-object v0, p0, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;->mPackages:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method

.method public get(Ljava/lang/String;)Ljava/lang/Float;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .line 33
    iget-object v0, p0, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    return-object v0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .line 37
    iget-object v0, p0, Lcom/android/server/wm/MiuiRefreshRatePolicy$PackageRefreshRate;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method
