.class public Lcom/android/server/wm/OneTrackVulkanHelper;
.super Ljava/lang/Object;
.source "OneTrackVulkanHelper.java"


# static fields
.field public static APP_CRASH_INFO:Ljava/lang/String; = null

.field private static final APP_ID:Ljava/lang/String; = "31000401696"

.field private static final EVENT_NAME:Ljava/lang/String; = "app_use_vulkan"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final ONETRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final ONETRACK_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "android"

.field private static final TAG:Ljava/lang/String;

.field static mContext:Landroid/content/Context;

.field private static sInstance:Lcom/android/server/wm/OneTrackVulkanHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    const-class v0, Lcom/android/server/wm/OneTrackVulkanHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/OneTrackVulkanHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/android/server/wm/OneTrackVulkanHelper;
    .locals 3

    const-class v0, Lcom/android/server/wm/OneTrackVulkanHelper;

    monitor-enter v0

    .line 29
    :try_start_0
    sget-object v1, Lcom/android/server/wm/OneTrackVulkanHelper;->sInstance:Lcom/android/server/wm/OneTrackVulkanHelper;

    if-nez v1, :cond_0

    .line 30
    new-instance v1, Lcom/android/server/wm/OneTrackVulkanHelper;

    invoke-direct {v1}, Lcom/android/server/wm/OneTrackVulkanHelper;-><init>()V

    sput-object v1, Lcom/android/server/wm/OneTrackVulkanHelper;->sInstance:Lcom/android/server/wm/OneTrackVulkanHelper;

    .line 32
    :cond_0
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;

    move-result-object v1

    sput-object v1, Lcom/android/server/wm/OneTrackVulkanHelper;->mContext:Landroid/content/Context;

    .line 33
    if-nez v1, :cond_1

    .line 34
    sget-object v1, Lcom/android/server/wm/OneTrackVulkanHelper;->TAG:Ljava/lang/String;

    const-string v2, "init OneTrackVulkanHelper mContext = null"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :cond_1
    sget-object v1, Lcom/android/server/wm/OneTrackVulkanHelper;->sInstance:Lcom/android/server/wm/OneTrackVulkanHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 28
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public getCrashInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "trace"    # Ljava/lang/String;

    .line 40
    sput-object p1, Lcom/android/server/wm/OneTrackVulkanHelper;->APP_CRASH_INFO:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public reportOneTrack(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 45
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 46
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const-string v1, "APP_ID"

    const-string v2, "31000401696"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const-string v1, "PACKAGE"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    const-string v1, "EVENT_NAME"

    const-string v2, "app_use_vulkan"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 52
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 53
    const-string/jumbo v2, "vulkan_package_name"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    const-string/jumbo v2, "vulkan_crash_info"

    sget-object v3, Lcom/android/server/wm/OneTrackVulkanHelper;->APP_CRASH_INFO:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_0

    .line 57
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 60
    :cond_0
    sget-object v2, Lcom/android/server/wm/OneTrackVulkanHelper;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 61
    sget-object v2, Lcom/android/server/wm/OneTrackVulkanHelper;->TAG:Ljava/lang/String;

    const-string v3, "reportOneTrack"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "params":Landroid/os/Bundle;
    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/android/server/wm/OneTrackVulkanHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Upload use VulkanApp exception! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 65
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
