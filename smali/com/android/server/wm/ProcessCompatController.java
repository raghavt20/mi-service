public class com.android.server.wm.ProcessCompatController implements com.android.server.wm.ProcessCompatControllerStub {
	 /* .source "ProcessCompatController.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.wm.ActivityTaskManagerService mAtm;
	 private android.graphics.Rect mBounds;
	 private Float mCurrentScale;
	 private Float mFixedAspectRatio;
	 private Float mGlobalScale;
	 private com.android.server.wm.WindowProcessController mProcess;
	 private Integer mState;
	 /* # direct methods */
	 public com.android.server.wm.ProcessCompatController ( ) {
		 /* .locals 3 */
		 /* .line 24 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 28 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I */
		 /* .line 31 */
		 /* new-instance v1, Landroid/graphics/Rect; */
		 int v2 = 1; // const/4 v2, 0x1
		 /* invoke-direct {v1, v0, v0, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V */
		 this.mBounds = v1;
		 /* .line 52 */
		 /* const/high16 v0, -0x40800000 # -1.0f */
		 /* iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F */
		 /* .line 53 */
		 /* const/high16 v0, 0x3f800000 # 1.0f */
		 /* iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F */
		 return;
	 } // .end method
	 private void updateState ( ) {
		 /* .locals 4 */
		 /* .line 118 */
		 /* iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I */
		 /* .line 119 */
		 /* .local v0, "newState":I */
		 v1 = 		 (( com.android.server.wm.ProcessCompatController ) p0 ).canUseFixedAspectRatio ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->canUseFixedAspectRatio()Z
		 final String v2 = "ProcessCompat"; // const-string v2, "ProcessCompat"
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 120 */
			 /* or-int/lit8 v0, v0, 0x8 */
			 /* .line 121 */
			 /* new-instance v1, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v3 = "Set "; // const-string v3, "Set "
			 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v3 = this.mProcess;
			 v3 = this.mName;
			 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 final String v3 = " fixed-aspect-ratio "; // const-string v3, " fixed-aspect-ratio "
			 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* iget v3, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F */
			 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .i ( v2,v1 );
			 /* .line 123 */
		 } // :cond_0
		 /* and-int/lit8 v0, v0, -0x9 */
		 /* .line 125 */
	 } // :goto_0
	 /* iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I */
	 /* if-eq v1, v0, :cond_1 */
	 /* .line 126 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "Update "; // const-string v3, "Update "
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v3 = this.mProcess;
	 v3 = this.mName;
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v3 = " comapt state "; // const-string v3, " comapt state "
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v3, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I */
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v3 = "->"; // const-string v3, "->"
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v1 );
	 /* .line 127 */
	 /* iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I */
	 /* .line 128 */
	 (( com.android.server.wm.ProcessCompatController ) p0 ).sendCompatState ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->sendCompatState()V
	 /* .line 130 */
} // :cond_1
return;
} // .end method
/* # virtual methods */
public Boolean canUseFixedAspectRatio ( ) {
/* .locals 3 */
/* .line 133 */
/* iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F */
int v1 = 0; // const/4 v1, 0x0
/* cmpg-float v0, v0, v1 */
/* if-lez v0, :cond_3 */
/* .line 134 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
/* if-nez v0, :cond_0 */
v0 = this.mProcess;
/* iget v0, v0, Lcom/android/server/wm/WindowProcessController;->mUid:I */
/* const/16 v1, 0x2710 */
/* if-lt v0, v1, :cond_3 */
} // :cond_0
v0 = this.mAtm;
v0 = this.mWindowManager;
v0 = this.mPolicy;
v0 = /* .line 136 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 137 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_3
	 /* .line 138 */
} // :cond_1
v0 = (( com.android.server.wm.ProcessCompatController ) p0 ).shouldNotUseFixedAspectRatio ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->shouldNotUseFixedAspectRatio()Z
/* if-nez v0, :cond_3 */
/* .line 139 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 140 */
	 com.android.server.wm.BoundsCompatUtils .getInstance ( );
	 v1 = this.mAtm;
	 v2 = this.mProcess;
	 v2 = this.mInfo;
	 v2 = this.packageName;
	 v0 = 	 (( com.android.server.wm.BoundsCompatUtils ) v0 ).getFlipCompatModeByApp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I
	 /* if-nez v0, :cond_2 */
	 /* .line 144 */
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
/* .line 142 */
} // :cond_3
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 106 */
/* iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F */
/* const/high16 v1, -0x40800000 # -1.0f */
/* cmpl-float v0, v0, v1 */
/* if-nez v0, :cond_0 */
/* .line 107 */
return;
/* .line 109 */
} // :cond_0
/* monitor-enter p0 */
/* .line 110 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "MiuiSizeCompat Scale:"; // const-string v1, "MiuiSizeCompat Scale:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 111 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mGlobalScale="; // const-string v1, " mGlobalScale="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = " mCurrentScale="; // const-string v1, " mCurrentScale="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 113 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 114 */
/* monitor-exit p0 */
/* .line 115 */
return;
/* .line 114 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public android.content.res.Configuration getCompatConfiguration ( android.content.res.Configuration p0 ) {
/* .locals 5 */
/* .param p1, "config" # Landroid/content/res/Configuration; */
/* .line 56 */
v0 = (( com.android.server.wm.ProcessCompatController ) p0 ).isFixedAspectRatioEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->isFixedAspectRatioEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 57 */
/* iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F */
/* const/high16 v1, -0x40800000 # -1.0f */
/* cmpl-float v0, v0, v1 */
/* if-nez v0, :cond_0 */
/* .line 58 */
com.android.server.wm.BoundsCompatUtils .getInstance ( );
v2 = this.mAtm;
v3 = this.mProcess;
v3 = this.mInfo;
v3 = this.packageName;
/* .line 59 */
v0 = (( com.android.server.wm.BoundsCompatUtils ) v0 ).getFlipCompatModeByApp ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I
/* .line 60 */
/* .local v0, "mode":I */
com.android.server.wm.BoundsCompatUtils .getInstance ( );
v3 = this.mProcess;
v3 = this.mName;
v4 = this.windowConfiguration;
/* .line 61 */
(( android.app.WindowConfiguration ) v4 ).getBounds ( ); // invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* .line 60 */
v2 = (( com.android.server.wm.BoundsCompatUtils ) v2 ).getGlobalScaleByName ( v3, v0, v4 ); // invoke-virtual {v2, v3, v0, v4}, Lcom/android/server/wm/BoundsCompatUtils;->getGlobalScaleByName(Ljava/lang/String;ILandroid/graphics/Rect;)F
/* iput v2, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F */
/* .line 64 */
} // .end local v0 # "mode":I
} // :cond_0
/* iget v0, p1, Landroid/content/res/Configuration;->orientation:I */
int v2 = 2; // const/4 v2, 0x2
/* if-eq v0, v2, :cond_1 */
/* .line 65 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
/* iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F */
/* cmpl-float v1, v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 67 */
/* iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F */
/* .line 69 */
} // :cond_2
/* const/high16 v0, 0x3f800000 # 1.0f */
/* iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F */
/* .line 71 */
} // :goto_0
com.android.server.wm.BoundsCompatUtils .getInstance ( );
/* iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F */
v2 = this.mAtm;
v2 = this.mWindowManager;
/* .line 72 */
(( com.android.server.wm.WindowManagerService ) v2 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
/* iget v3, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F */
/* .line 71 */
(( com.android.server.wm.BoundsCompatUtils ) v0 ).getCompatConfiguration ( p1, v1, v2, v3 ); // invoke-virtual {v0, p1, v1, v2, v3}, Lcom/android/server/wm/BoundsCompatUtils;->getCompatConfiguration(Landroid/content/res/Configuration;FLcom/android/server/wm/DisplayContent;F)Landroid/content/res/Configuration;
/* .line 74 */
} // :cond_3
} // .end method
public void initBoundsCompatController ( com.android.server.wm.ActivityTaskManagerService p0, com.android.server.wm.WindowProcessController p1 ) {
/* .locals 3 */
/* .param p1, "atm" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "process" # Lcom/android/server/wm/WindowProcessController; */
/* .line 35 */
this.mAtm = p1;
/* .line 36 */
this.mProcess = p2;
/* .line 37 */
v0 = this.mInfo;
v0 = this.packageName;
/* .line 38 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 39 */
com.android.server.wm.BoundsCompatUtils .getInstance ( );
v2 = this.mAtm;
v1 = (( com.android.server.wm.BoundsCompatUtils ) v1 ).getFlipCompatModeByApp ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I
/* if-nez v1, :cond_0 */
/* .line 41 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F */
/* .line 43 */
} // :cond_0
/* const v1, 0x3fdc3e06 */
/* iput v1, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F */
/* .line 47 */
} // :cond_1
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v1 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v1 ).getAspectRatio ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getAspectRatio(Ljava/lang/String;)F
/* iput v1, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F */
/* .line 49 */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/wm/ProcessCompatController;->updateState()V */
/* .line 50 */
return;
} // .end method
public Boolean isFixedAspectRatioEnabled ( ) {
/* .locals 2 */
/* .line 79 */
android.app.servertransaction.BoundsCompatStub .get ( );
/* iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I */
v0 = (( android.app.servertransaction.BoundsCompatStub ) v0 ).isFixedAspectRatioModeEnabled ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/servertransaction/BoundsCompatStub;->isFixedAspectRatioModeEnabled(I)Z
} // .end method
public void onSetThread ( ) {
/* .locals 1 */
/* .line 100 */
/* iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 101 */
(( com.android.server.wm.ProcessCompatController ) p0 ).sendCompatState ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->sendCompatState()V
/* .line 103 */
} // :cond_0
return;
} // .end method
public void sendCompatState ( ) {
/* .locals 4 */
/* .line 83 */
v0 = this.mProcess;
(( com.android.server.wm.WindowProcessController ) v0 ).getThread ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
/* if-nez v0, :cond_0 */
/* .line 84 */
return;
/* .line 86 */
} // :cond_0
v0 = this.mProcess;
(( com.android.server.wm.WindowProcessController ) v0 ).getThread ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
int v1 = 0; // const/4 v1, 0x0
android.app.servertransaction.ClientTransaction .obtain ( v0,v1 );
/* .line 87 */
/* .local v0, "transaction":Landroid/app/servertransaction/ClientTransaction; */
/* iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I */
v2 = this.mBounds;
/* iget v3, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F */
android.app.servertransaction.BoundsCompatInfoChangeItem .obtain ( v1,v2,v3 );
(( android.app.servertransaction.ClientTransaction ) v0 ).addCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 89 */
try { // :try_start_0
v1 = this.mAtm;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getLifecycleManager ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;
(( com.android.server.wm.ClientLifecycleManager ) v1 ).scheduleTransaction ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 92 */
/* .line 90 */
/* :catch_0 */
/* move-exception v1 */
/* .line 93 */
} // :goto_0
return;
} // .end method
public Boolean shouldNotUseFixedAspectRatio ( ) {
/* .locals 4 */
/* .line 148 */
v0 = this.mProcess;
(( com.android.server.wm.WindowProcessController ) v0 ).getActivities ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getActivities()Ljava/util/List;
/* .line 149 */
v1 = /* .local v0, "mActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;" */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 150 */
int v1 = 0; // const/4 v1, 0x0
/* .line 153 */
v1 = } // :cond_0
/* add-int/lit8 v1, v1, -0x1 */
/* .line 154 */
/* .local v1, "lastIndex":I */
/* check-cast v2, Lcom/android/server/wm/ActivityRecord; */
/* .line 155 */
/* .local v2, "topRecord":Lcom/android/server/wm/ActivityRecord; */
v3 = (( com.android.server.wm.ActivityRecord ) v2 ).inFreeformWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z
} // .end method
public void updateConfiguration ( android.content.res.Configuration p0 ) {
/* .locals 0 */
/* .param p1, "config" # Landroid/content/res/Configuration; */
/* .line 96 */
/* invoke-direct {p0}, Lcom/android/server/wm/ProcessCompatController;->updateState()V */
/* .line 97 */
return;
} // .end method
