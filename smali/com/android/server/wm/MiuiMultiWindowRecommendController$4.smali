.class Lcom/android/server/wm/MiuiMultiWindowRecommendController$4;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    .line 148
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$4;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 152
    const-string v0, "MiuiMultiWindowRecommendController"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$4;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 153
    const-string v1, "dismissSplitScreenRecommendViewRunnable "

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$4;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->startMultiWindowRecommendAnimation(Landroid/view/View;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :cond_0
    goto :goto_0

    .line 156
    :catch_0
    move-exception v1

    .line 157
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, " dismissSplitScreenRecommendViewRunnable error: "

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 158
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 160
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
