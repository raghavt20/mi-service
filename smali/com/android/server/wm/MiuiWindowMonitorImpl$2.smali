.class Lcom/android/server/wm/MiuiWindowMonitorImpl$2;
.super Ljava/lang/Object;
.source "MiuiWindowMonitorImpl.java"

# interfaces
.implements Lcom/android/server/am/MiuiWarnings$WarningCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiWindowMonitorImpl;->showKillAppDialog(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

.field final synthetic val$appLabel:Ljava/lang/String;

.field final synthetic val$persistent:Z

.field final synthetic val$pid:I

.field final synthetic val$pkgName:Ljava/lang/String;

.field final synthetic val$processWindows:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiWindowMonitorImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 243
    iput-object p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    iput-object p2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$processWindows:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    iput p3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I

    iput-object p4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pkgName:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$appLabel:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$persistent:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallback(Z)V
    .locals 5
    .param p1, "positive"    # Z

    .line 246
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    monitor-enter v0

    .line 247
    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 248
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$processWindows:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V

    goto :goto_0

    .line 250
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$processWindows:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V

    .line 253
    :goto_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I

    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$processWindows:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    invoke-static {v2, v3, v4, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->-$$Nest$mreportIfNeeded(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V

    .line 255
    if-eqz p1, :cond_1

    .line 256
    const-string v2, "MiuiWindowMonitorImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "kill to avoid window leak, pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", package="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", label="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$appLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", persistent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$persistent:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I

    invoke-static {v2, v3}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->-$$Nest$mkillApp(Lcom/android/server/wm/MiuiWindowMonitorImpl;I)V

    goto :goto_1

    .line 260
    :cond_1
    const-string v2, "MiuiWindowMonitorImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "application create too many windows, pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pid:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", package="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$pkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", label="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$appLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", persistent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$persistent:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$processWindows:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V

    .line 264
    :goto_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;->val$processWindows:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V

    .line 265
    monitor-exit v0

    .line 266
    return-void

    .line 265
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
