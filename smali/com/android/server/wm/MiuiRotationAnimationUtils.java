public class com.android.server.wm.MiuiRotationAnimationUtils {
	 /* .source "MiuiRotationAnimationUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseSineInOutInterpolator;, */
	 /* Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseQuartOutInterpolator;, */
	 /* Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseOutInterpolater;, */
	 /* Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseInInterpolater; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer DEVICE_HIGH_END;
private static final Integer DEVICE_LOW_END;
private static final Integer DEVICE_MIDDLE_END;
private static final Integer HIGH_ROTATION_ANIMATION_DURATION;
private static final Integer LOW_ROTATION_ANIMATION_DURATION;
private static final Integer MIDDLE_ROTATION_ANIMATION_DURATION;
private static final java.lang.String TAG;
/* # direct methods */
public com.android.server.wm.MiuiRotationAnimationUtils ( ) {
	 /* .locals 0 */
	 /* .line 7 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
static Integer getRotationDuration ( ) {
	 /* .locals 3 */
	 /* .line 18 */
	 /* sget-boolean v0, Lmiui/os/Build;->IS_MIUI_LITE_VERSION:Z */
	 /* const/16 v1, 0x12c */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 19 */
		 /* .line 21 */
	 } // :cond_0
	 v0 = 	 miui.os.Build .getDeviceLevelForAnimation ( );
	 /* .line 22 */
	 /* .local v0, "devicelevel":I */
	 /* const/16 v2, 0x1f4 */
	 /* packed-switch v0, :pswitch_data_0 */
	 /* .line 30 */
	 /* .line 28 */
	 /* :pswitch_0 */
	 /* .line 26 */
	 /* :pswitch_1 */
	 /* .line 24 */
	 /* :pswitch_2 */
	 /* :pswitch_data_0 */
	 /* .packed-switch 0x1 */
	 /* :pswitch_2 */
	 /* :pswitch_1 */
	 /* :pswitch_0 */
} // .end packed-switch
} // .end method
