.class Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;
.super Ljava/lang/Object;
.source "OneTrackRotationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/OneTrackRotationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AppUsageMessageObj"
.end annotation


# instance fields
.field public obj:Ljava/lang/Object;

.field public time:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J

    .line 581
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .line 583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 584
    iput-object p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->obj:Ljava/lang/Object;

    .line 585
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J

    .line 586
    return-void
.end method
