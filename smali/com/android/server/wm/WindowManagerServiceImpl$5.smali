.class Lcom/android/server/wm/WindowManagerServiceImpl$5;
.super Ljava/lang/Object;
.source "WindowManagerServiceImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/WindowManagerServiceImpl;->updateContrastAlpha(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

.field final synthetic val$darkmode:Z


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerServiceImpl;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/WindowManagerServiceImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1315
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iput-boolean p2, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->val$darkmode:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1318
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "contrast_alpha"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    .line 1320
    .local v0, "alpha":F
    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 1321
    const/4 v0, 0x0

    .line 1323
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->-$$Nest$misDarkModeContrastEnable(Lcom/android/server/wm/WindowManagerServiceImpl;)Z

    move-result v1

    .line 1324
    .local v1, "isContrastEnabled":Z
    const-string v2, "WindowManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateContrastOverlay, darkmode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->val$darkmode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isContrastEnabled: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " alpha: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    monitor-enter v2

    .line 1327
    :try_start_0
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1329
    :try_start_1
    iget-boolean v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->val$darkmode:Z

    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    .line 1330
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiContrastOverlay:Lcom/android/server/wm/MiuiContrastOverlayStub;

    if-nez v3, :cond_1

    .line 1331
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    invoke-static {}, Lcom/android/server/wm/MiuiContrastOverlayStub;->getInstance()Lcom/android/server/wm/MiuiContrastOverlayStub;

    move-result-object v4

    iput-object v4, v3, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiContrastOverlay:Lcom/android/server/wm/MiuiContrastOverlayStub;

    .line 1332
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v3

    .line 1333
    .local v3, "displayContent":Lcom/android/server/wm/DisplayContent;
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v4, v4, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiContrastOverlay:Lcom/android/server/wm/MiuiContrastOverlayStub;

    iget-object v5, v3, Lcom/android/server/wm/DisplayContent;->mRealDisplayMetrics:Landroid/util/DisplayMetrics;

    iget-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v6, v6, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-interface {v4, v3, v5, v6}, Lcom/android/server/wm/MiuiContrastOverlayStub;->init(Lcom/android/server/wm/DisplayContent;Landroid/util/DisplayMetrics;Landroid/content/Context;)V

    .line 1335
    .end local v3    # "displayContent":Lcom/android/server/wm/DisplayContent;
    :cond_1
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiContrastOverlay:Lcom/android/server/wm/MiuiContrastOverlayStub;

    invoke-interface {v3, v0}, Lcom/android/server/wm/MiuiContrastOverlayStub;->showContrastOverlay(F)V

    goto :goto_0

    .line 1337
    :cond_2
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiContrastOverlay:Lcom/android/server/wm/MiuiContrastOverlayStub;

    if-eqz v3, :cond_3

    .line 1338
    const-string v3, "WindowManagerService"

    const-string v4, " hideContrastOverlay "

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiContrastOverlay:Lcom/android/server/wm/MiuiContrastOverlayStub;

    invoke-interface {v3}, Lcom/android/server/wm/MiuiContrastOverlayStub;->hideContrastOverlay()V

    .line 1341
    :cond_3
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    const/4 v4, 0x0

    iput-object v4, v3, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiContrastOverlay:Lcom/android/server/wm/MiuiContrastOverlayStub;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1344
    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    const-string v4, "MiuiContrastOverlay"

    invoke-virtual {v3, v4}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 1345
    nop

    .line 1346
    monitor-exit v2

    .line 1347
    return-void

    .line 1344
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v4, v4, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    const-string v5, "MiuiContrastOverlay"

    invoke-virtual {v4, v5}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 1345
    nop

    .end local v0    # "alpha":F
    .end local v1    # "isContrastEnabled":Z
    .end local p0    # "this":Lcom/android/server/wm/WindowManagerServiceImpl$5;
    throw v3

    .line 1346
    .restart local v0    # "alpha":F
    .restart local v1    # "isContrastEnabled":Z
    .restart local p0    # "this":Lcom/android/server/wm/WindowManagerServiceImpl$5;
    :catchall_1
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3
.end method
