.class public Lcom/android/server/wm/GetDragEventProxy;
.super Ljava/lang/Object;
.source "GetDragEventProxy.java"


# static fields
.field public static DRAG_FLAGS_URI_ACCESS:Lcom/xiaomi/reflect/RefInt;
    .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments;
    .end annotation
.end field

.field public static DRAG_FLAGS_URI_PERMISSIONS:Lcom/xiaomi/reflect/RefInt;
    .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments;
    .end annotation
.end field

.field public static mData:Lcom/xiaomi/reflect/RefObject;
    .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefObject<",
            "Landroid/content/ClipData;",
            ">;"
        }
    .end annotation
.end field

.field public static mFlags:Lcom/xiaomi/reflect/RefInt;
    .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments;
    .end annotation
.end field

.field public static mService:Lcom/xiaomi/reflect/RefObject;
    .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefObject<",
            "Lcom/android/server/wm/WindowManagerService;",
            ">;"
        }
    .end annotation
.end field

.field public static mSourceUserId:Lcom/xiaomi/reflect/RefInt;
    .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments;
    .end annotation
.end field

.field public static mUid:Lcom/xiaomi/reflect/RefInt;
    .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments;
    .end annotation
.end field

.field public static obtainDragEvent:Lcom/xiaomi/reflect/RefMethod;
    .annotation runtime Lcom/xiaomi/reflect/annotation/MethodQualifiedArguments;
        classNames = {
            "int",
            "float",
            "float",
            "android.content.ClipData",
            "boolean",
            "com.android.internal.view.IDragAndDropPermissions"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefMethod<",
            "Landroid/view/DragEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 18
    const-class v0, Lcom/android/server/wm/GetDragEventProxy;

    const-string v1, "com.android.server.wm.DragState"

    invoke-static {v0, v1}, Lcom/xiaomi/reflect/RefClass;->attach(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Class;

    .line 19
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
