public class com.android.server.wm.VelocityMonitor {
	 /* .source "VelocityMonitor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long MAX_DELTA;
private static final Integer MAX_RECORD_COUNT;
private static final Long MIN_DELTA;
private static final Long TIME_THRESHOLD;
private static final Integer UNIT_SECOND;
/* # instance fields */
private java.util.LinkedList mHistory;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/wm/VelocityMonitor$MoveRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private mVelocity;
/* # direct methods */
public com.android.server.wm.VelocityMonitor ( ) {
/* .locals 1 */
/* .line 8 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 21 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mHistory = v0;
return;
} // .end method
private void addAndUpdate ( com.android.server.wm.VelocityMonitor$MoveRecord p0 ) {
/* .locals 2 */
/* .param p1, "record" # Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .line 53 */
v0 = this.mHistory;
(( java.util.LinkedList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 54 */
v0 = this.mHistory;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* const/16 v1, 0xa */
/* if-le v0, v1, :cond_0 */
/* .line 55 */
v0 = this.mHistory;
int v1 = 0; // const/4 v1, 0x0
(( java.util.LinkedList ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;
/* .line 57 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/wm/VelocityMonitor;->updateVelocity()V */
/* .line 58 */
return;
} // .end method
private Float calVelocity ( Integer p0, com.android.server.wm.VelocityMonitor$MoveRecord p1, com.android.server.wm.VelocityMonitor$MoveRecord p2 ) {
/* .locals 25 */
/* .param p1, "idx" # I */
/* .param p2, "lastRecord" # Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .param p3, "lastRecord1" # Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .line 99 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p2 */
/* move-object/from16 v9, p3 */
v0 = this.values;
/* aget-wide v10, v0, p1 */
/* .line 100 */
/* .local v10, "lastValue":D */
/* iget-wide v12, v8, Lcom/android/server/wm/VelocityMonitor$MoveRecord;->timeStamp:J */
/* .line 101 */
/* .local v12, "lastTime":J */
v0 = this.values;
/* aget-wide v14, v0, p1 */
/* .line 102 */
/* .local v14, "lastValue1":D */
/* iget-wide v5, v9, Lcom/android/server/wm/VelocityMonitor$MoveRecord;->timeStamp:J */
/* .line 103 */
/* .local v5, "lastTime1":J */
/* sub-long v16, v12, v5 */
/* move-object/from16 v0, p0 */
/* move-wide v1, v10 */
/* move-wide v3, v14 */
/* move-wide/from16 v18, v5 */
} // .end local v5 # "lastTime1":J
/* .local v18, "lastTime1":J */
/* move-wide/from16 v5, v16 */
v0 = /* invoke-direct/range {v0 ..v6}, Lcom/android/server/wm/VelocityMonitor;->getVelocity(DDJ)F */
/* float-to-double v5, v0 */
/* .line 104 */
/* .local v5, "v1":D */
/* const v16, 0x7f7fffff # Float.MAX_VALUE */
/* .line 105 */
/* .local v16, "velocity":F */
/* const-wide/16 v0, 0x0 */
/* .line 106 */
/* .local v0, "deltaT":J */
int v2 = 0; // const/4 v2, 0x0
/* .line 107 */
/* .local v2, "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
v3 = this.mHistory;
v3 = (( java.util.LinkedList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/LinkedList;->size()I
/* add-int/lit8 v3, v3, -0x2 */
/* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_3 */
/* .line 108 */
v4 = this.mHistory;
(( java.util.LinkedList ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .line 109 */
} // .end local v2 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
/* .local v4, "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* move-wide/from16 v20, v0 */
} // .end local v0 # "deltaT":J
/* .local v20, "deltaT":J */
/* iget-wide v0, v4, Lcom/android/server/wm/VelocityMonitor$MoveRecord;->timeStamp:J */
/* sub-long v20, v12, v0 */
/* .line 110 */
/* const-wide/16 v0, 0x1e */
/* cmp-long v0, v20, v0 */
/* if-lez v0, :cond_2 */
/* const-wide/16 v0, 0x64 */
/* cmp-long v0, v20, v0 */
/* if-gez v0, :cond_2 */
/* .line 111 */
v0 = this.values;
/* aget-wide v22, v0, p1 */
/* move-object/from16 v0, p0 */
/* move-wide v1, v10 */
/* move/from16 v17, v3 */
/* move-object/from16 v24, v4 */
} // .end local v3 # "i":I
} // .end local v4 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
/* .local v17, "i":I */
/* .local v24, "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* move-wide/from16 v3, v22 */
/* move-wide v7, v5 */
} // .end local v5 # "v1":D
/* .local v7, "v1":D */
/* move-wide/from16 v5, v20 */
v0 = /* invoke-direct/range {v0 ..v6}, Lcom/android/server/wm/VelocityMonitor;->getVelocity(DDJ)F */
/* .line 112 */
/* .local v0, "v2":F */
/* float-to-double v1, v0 */
/* mul-double v5, v7, v1 */
/* const-wide/16 v1, 0x0 */
/* cmpl-double v1, v5, v1 */
/* if-lez v1, :cond_1 */
/* .line 113 */
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v1, v0, v1 */
/* if-lez v1, :cond_0 */
/* float-to-double v1, v0 */
java.lang.Math .max ( v7,v8,v1,v2 );
/* move-result-wide v1 */
} // :cond_0
/* float-to-double v1, v0 */
java.lang.Math .min ( v7,v8,v1,v2 );
/* move-result-wide v1 */
} // :goto_1
/* double-to-float v1, v1 */
/* move/from16 v16, v1 */
/* move-object/from16 v5, v24 */
} // .end local v16 # "velocity":F
/* .local v1, "velocity":F */
/* .line 115 */
} // .end local v1 # "velocity":F
/* .restart local v16 # "velocity":F */
} // :cond_1
/* move/from16 v16, v0 */
/* .line 117 */
/* move-object/from16 v5, v24 */
/* .line 110 */
} // .end local v0 # "v2":F
} // .end local v7 # "v1":D
} // .end local v17 # "i":I
} // .end local v24 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
/* .restart local v3 # "i":I */
/* .restart local v4 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .restart local v5 # "v1":D */
} // :cond_2
/* move/from16 v17, v3 */
/* move-object/from16 v24, v4 */
/* move-wide v7, v5 */
/* .line 107 */
} // .end local v3 # "i":I
} // .end local v4 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
} // .end local v5 # "v1":D
/* .restart local v7 # "v1":D */
/* .restart local v17 # "i":I */
/* .restart local v24 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* add-int/lit8 v3, v17, -0x1 */
/* move-wide v5, v7 */
/* move-wide/from16 v0, v20 */
/* move-object/from16 v2, v24 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p2 */
} // .end local v17 # "i":I
/* .restart local v3 # "i":I */
} // .end local v7 # "v1":D
} // .end local v20 # "deltaT":J
} // .end local v24 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
/* .local v0, "deltaT":J */
/* .restart local v2 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .restart local v5 # "v1":D */
} // :cond_3
/* move-wide/from16 v20, v0 */
/* move/from16 v17, v3 */
/* move-wide v7, v5 */
} // .end local v0 # "deltaT":J
} // .end local v3 # "i":I
} // .end local v5 # "v1":D
/* .restart local v7 # "v1":D */
/* .restart local v17 # "i":I */
/* .restart local v20 # "deltaT":J */
/* move-object v5, v2 */
/* .line 120 */
} // .end local v2 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
} // .end local v17 # "i":I
/* .local v5, "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
} // :goto_2
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* cmpl-float v0, v16, v0 */
/* if-nez v0, :cond_4 */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 121 */
v0 = this.values;
/* aget-wide v3, v0, p1 */
/* move-object/from16 v0, p0 */
/* move-wide v1, v10 */
/* move-object/from16 v24, v5 */
} // .end local v5 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
/* .restart local v24 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* move-wide/from16 v5, v20 */
v16 = /* invoke-direct/range {v0 ..v6}, Lcom/android/server/wm/VelocityMonitor;->getVelocity(DDJ)F */
/* .line 120 */
} // .end local v24 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
/* .restart local v5 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
} // :cond_4
/* move-object/from16 v24, v5 */
/* .line 123 */
} // .end local v5 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
/* .restart local v24 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
} // :goto_3
} // .end method
private void clearVelocity ( ) {
/* .locals 2 */
/* .line 76 */
v0 = this.mVelocity;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 77 */
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 79 */
} // :cond_0
return;
} // .end method
private com.android.server.wm.VelocityMonitor$MoveRecord getMoveRecord ( ) {
/* .locals 3 */
/* .line 47 */
/* new-instance v0, Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/android/server/wm/VelocityMonitor$MoveRecord;-><init>(Lcom/android/server/wm/VelocityMonitor$MoveRecord-IA;)V */
/* .line 48 */
/* .local v0, "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/server/wm/VelocityMonitor$MoveRecord;->timeStamp:J */
/* .line 49 */
} // .end method
private Float getVelocity ( Double p0, Double p1, Long p2 ) {
/* .locals 4 */
/* .param p1, "value1" # D */
/* .param p3, "value2" # D */
/* .param p5, "deltaT" # J */
/* .line 127 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p5, v0 */
/* if-nez v0, :cond_0 */
/* const-wide/16 v0, 0x0 */
/* .line 128 */
} // :cond_0
/* sub-double v0, p1, p3 */
/* long-to-float v2, p5 */
/* const/high16 v3, 0x447a0000 # 1000.0f */
/* div-float/2addr v2, v3 */
/* float-to-double v2, v2 */
/* div-double/2addr v0, v2 */
} // :goto_0
/* double-to-float v0, v0 */
/* .line 127 */
} // .end method
private void updateVelocity ( ) {
/* .locals 6 */
/* .line 82 */
v0 = this.mHistory;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* .line 83 */
/* .local v0, "size":I */
int v1 = 2; // const/4 v1, 0x2
/* if-lt v0, v1, :cond_3 */
/* .line 84 */
v1 = this.mHistory;
(( java.util.LinkedList ) v1 ).getLast ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .line 85 */
/* .local v1, "lastRecord":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
v2 = this.mHistory;
/* add-int/lit8 v3, v0, -0x2 */
(( java.util.LinkedList ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .line 87 */
/* .local v2, "lastRecord1":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
v3 = this.mVelocity;
if ( v3 != null) { // if-eqz v3, :cond_0
/* array-length v3, v3 */
v4 = this.values;
/* array-length v4, v4 */
/* if-ge v3, v4, :cond_1 */
/* .line 88 */
} // :cond_0
v3 = this.values;
/* array-length v3, v3 */
/* new-array v3, v3, [F */
this.mVelocity = v3;
/* .line 90 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = this.values;
/* array-length v4, v4 */
/* if-ge v3, v4, :cond_2 */
/* .line 91 */
v4 = this.mVelocity;
v5 = /* invoke-direct {p0, v3, v1, v2}, Lcom/android/server/wm/VelocityMonitor;->calVelocity(ILcom/android/server/wm/VelocityMonitor$MoveRecord;Lcom/android/server/wm/VelocityMonitor$MoveRecord;)F */
/* aput v5, v4, v3 */
/* .line 90 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 93 */
} // .end local v1 # "lastRecord":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
} // .end local v2 # "lastRecord1":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
} // .end local v3 # "i":I
} // :cond_2
/* .line 94 */
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/wm/VelocityMonitor;->clearVelocity()V */
/* .line 96 */
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void clear ( ) {
/* .locals 1 */
/* .line 71 */
v0 = this.mHistory;
(( java.util.LinkedList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
/* .line 72 */
/* invoke-direct {p0}, Lcom/android/server/wm/VelocityMonitor;->clearVelocity()V */
/* .line 73 */
return;
} // .end method
public Float getVelocity ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "idx" # I */
/* .line 61 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 62 */
/* .local v0, "now":J */
v2 = this.mHistory;
v2 = (( java.util.LinkedList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->size()I
int v3 = 0; // const/4 v3, 0x0
/* if-lez v2, :cond_0 */
v2 = this.mHistory;
(( java.util.LinkedList ) v2 ).getLast ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* iget-wide v4, v2, Lcom/android/server/wm/VelocityMonitor$MoveRecord;->timeStamp:J */
/* sub-long v4, v0, v4 */
java.lang.Math .abs ( v4,v5 );
/* move-result-wide v4 */
/* const-wide/16 v6, 0xc8 */
/* cmp-long v2, v4, v6 */
/* if-lez v2, :cond_0 */
/* .line 63 */
/* .line 64 */
} // :cond_0
v2 = this.mVelocity;
if ( v2 != null) { // if-eqz v2, :cond_1
/* array-length v4, v2 */
/* if-le v4, p1, :cond_1 */
/* .line 65 */
/* aget v2, v2, p1 */
/* .line 67 */
} // :cond_1
} // .end method
public void update ( Double...p0 ) {
/* .locals 1 */
/* .param p1, "value" # [D */
/* .line 38 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* array-length v0, p1 */
/* if-nez v0, :cond_0 */
/* .line 41 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/wm/VelocityMonitor;->getMoveRecord()Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .line 42 */
/* .local v0, "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
this.values = p1;
/* .line 43 */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/VelocityMonitor;->addAndUpdate(Lcom/android/server/wm/VelocityMonitor$MoveRecord;)V */
/* .line 44 */
return;
/* .line 39 */
} // .end local v0 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
} // :cond_1
} // :goto_0
return;
} // .end method
public void update ( Float...p0 ) {
/* .locals 5 */
/* .param p1, "value" # [F */
/* .line 26 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* array-length v0, p1 */
/* if-nez v0, :cond_0 */
/* .line 29 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/wm/VelocityMonitor;->getMoveRecord()Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* .line 30 */
/* .local v0, "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord; */
/* array-length v1, p1 */
/* new-array v1, v1, [D */
this.values = v1;
/* .line 31 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p1 */
/* if-ge v1, v2, :cond_1 */
/* .line 32 */
v2 = this.values;
/* aget v3, p1, v1 */
/* float-to-double v3, v3 */
/* aput-wide v3, v2, v1 */
/* .line 31 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 34 */
} // .end local v1 # "i":I
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/android/server/wm/VelocityMonitor;->addAndUpdate(Lcom/android/server/wm/VelocityMonitor$MoveRecord;)V */
/* .line 35 */
return;
/* .line 27 */
} // .end local v0 # "record":Lcom/android/server/wm/VelocityMonitor$MoveRecord;
} // :cond_2
} // :goto_1
return;
} // .end method
