.class public Lcom/android/server/wm/FgActivityChangedInfo;
.super Ljava/lang/Object;
.source "FgActivityChangedInfo.java"


# instance fields
.field final multiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

.field final pid:I

.field final record:Lcom/android/server/wm/ActivityRecord;

.field final state:Lcom/android/server/wm/ActivityRecord$State;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;ILandroid/content/pm/ApplicationInfo;)V
    .locals 0
    .param p1, "_record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "state"    # Lcom/android/server/wm/ActivityRecord$State;
    .param p3, "pid"    # I
    .param p4, "_info"    # Landroid/content/pm/ApplicationInfo;

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/android/server/wm/FgActivityChangedInfo;->record:Lcom/android/server/wm/ActivityRecord;

    .line 15
    iput-object p2, p0, Lcom/android/server/wm/FgActivityChangedInfo;->state:Lcom/android/server/wm/ActivityRecord$State;

    .line 16
    iput p3, p0, Lcom/android/server/wm/FgActivityChangedInfo;->pid:I

    .line 17
    iput-object p4, p0, Lcom/android/server/wm/FgActivityChangedInfo;->multiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

    .line 18
    return-void
.end method


# virtual methods
.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/android/server/wm/FgActivityChangedInfo;->record:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    return-object v0
.end method
