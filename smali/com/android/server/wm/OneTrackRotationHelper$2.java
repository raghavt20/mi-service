class com.android.server.wm.OneTrackRotationHelper$2 extends android.content.BroadcastReceiver {
	 /* .source "OneTrackRotationHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/OneTrackRotationHelper;->initAllListeners()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.OneTrackRotationHelper this$0; //synthetic
/* # direct methods */
 com.android.server.wm.OneTrackRotationHelper$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/OneTrackRotationHelper; */
/* .line 292 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 295 */
v0 = this.this$0;
v0 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetmIsInit ( v0 );
/* if-nez v0, :cond_0 */
/* .line 296 */
return;
/* .line 298 */
} // :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v2 = 3; // const/4 v2, 0x3
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v1 = "android.intent.action.REBOOT"; // const-string v1, "android.intent.action.REBOOT"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v0, v2 */
/* :sswitch_1 */
final String v1 = "android.intent.action.ACTION_SHUTDOWN"; // const-string v1, "android.intent.action.ACTION_SHUTDOWN"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_2 */
final String v1 = "android.intent.action.USER_PRESENT"; // const-string v1, "android.intent.action.USER_PRESENT"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 int v0 = 1; // const/4 v0, 0x1
	 /* :sswitch_3 */
	 final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
	 int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 307 */
/* :pswitch_0 */
v0 = this.this$0;
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
android.os.Message .obtain ( v0,v1 );
/* .line 308 */
/* .local v0, "message2":Landroid/os/Message; */
/* new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
/* invoke-direct {v1, p2}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>(Ljava/lang/Object;)V */
this.obj = v1;
/* .line 309 */
v1 = this.this$0;
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessageAtFrontOfQueue ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z
/* .line 301 */
} // .end local v0 # "message2":Landroid/os/Message;
/* :pswitch_1 */
v0 = this.this$0;
v0 = this.mHandler;
android.os.Message .obtain ( v0,v2 );
/* .line 302 */
/* .local v0, "message1":Landroid/os/Message; */
/* new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
/* invoke-direct {v1, p2}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>(Ljava/lang/Object;)V */
this.obj = v1;
/* .line 303 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 304 */
/* nop */
/* .line 312 */
} // .end local v0 # "message1":Landroid/os/Message;
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7ed8ea7f -> :sswitch_3 */
/* 0x311a1d6c -> :sswitch_2 */
/* 0x741706da -> :sswitch_1 */
/* 0x79950caa -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
