class com.android.server.wm.MiuiFreeformTrackManager$8 implements java.lang.Runnable {
	 /* .source "MiuiFreeformTrackManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiFreeformTrackManager;->trackSmallWindowResizeEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiFreeformTrackManager this$0; //synthetic
final java.lang.String val$applicationName; //synthetic
final java.lang.String val$packageName; //synthetic
final java.lang.String val$smallWindowFromRatio; //synthetic
final java.lang.String val$smallWindowToRatio; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiFreeformTrackManager$8 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiFreeformTrackManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 381 */
this.this$0 = p1;
this.val$smallWindowFromRatio = p2;
this.val$smallWindowToRatio = p3;
this.val$packageName = p4;
this.val$applicationName = p5;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 383 */
v0 = this.this$0;
v0 = this.mFreeFormTrackLock;
/* monitor-enter v0 */
/* .line 385 */
try { // :try_start_0
v1 = this.this$0;
com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmITrackBinder ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 386 */
	 /* new-instance v1, Lorg/json/JSONObject; */
	 /* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
	 /* .line 387 */
	 /* .local v1, "jsonData":Lorg/json/JSONObject; */
	 /* const-string/jumbo v2, "tip" */
	 final String v3 = "621.1.0.1.14012"; // const-string v3, "621.1.0.1.14012"
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 388 */
	 v2 = this.this$0;
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$mputCommomParam ( v2,v1 );
	 /* .line 389 */
	 final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
	 final String v3 = "resize"; // const-string v3, "resize"
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 390 */
	 /* const-string/jumbo v2, "small_window_from_ratio" */
	 v3 = this.val$smallWindowFromRatio;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 391 */
	 /* const-string/jumbo v2, "small_window_to_ratio" */
	 v3 = this.val$smallWindowToRatio;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 392 */
	 final String v2 = "app_package_name"; // const-string v2, "app_package_name"
	 v3 = this.val$packageName;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 393 */
	 final String v2 = "app_display_name"; // const-string v2, "app_display_name"
	 v3 = this.val$applicationName;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 394 */
	 v2 = this.this$0;
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmITrackBinder ( v2 );
	 final String v3 = "31000000538"; // const-string v3, "31000000538"
	 final String v4 = "android"; // const-string v4, "android"
	 /* .line 395 */
	 (( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
	 /* .line 394 */
	 int v6 = 0; // const/4 v6, 0x0
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 399 */
} // .end local v1 # "jsonData":Lorg/json/JSONObject;
} // :cond_0
/* .line 400 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 397 */
/* :catch_0 */
/* move-exception v1 */
/* .line 398 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 400 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit v0 */
/* .line 401 */
return;
/* .line 400 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
