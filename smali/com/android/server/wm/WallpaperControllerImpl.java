public class com.android.server.wm.WallpaperControllerImpl implements com.android.server.wm.WallpaperControllerStub {
	 /* .source "WallpaperControllerImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final java.lang.String KEYGUARD_DESCRIPTOR;
	 private static final java.lang.String SCROLL_DESKTOP_WALLPAPER;
	 private static final java.lang.String TAG;
	 private static final Float WALLPAPER_OFFSET_CENTER;
	 private static final Float WALLPAPER_OFFSET_DEFAULT;
	 /* # direct methods */
	 public com.android.server.wm.WallpaperControllerImpl ( ) {
		 /* .locals 0 */
		 /* .line 29 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void findWallpapers ( com.android.server.wm.WindowState p0, com.android.server.wm.WallpaperController$FindWallpaperTargetResult p1 ) {
		 /* .locals 3 */
		 /* .param p1, "w" # Lcom/android/server/wm/WindowState; */
		 /* .param p2, "findResult" # Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult; */
		 /* .line 59 */
		 v0 = this.mAttrs;
		 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
		 /* const/16 v1, 0x7dd */
		 /* if-ne v0, v1, :cond_3 */
		 /* .line 60 */
		 v0 = this.mToken;
		 (( com.android.server.wm.WindowToken ) v0 ).asWallpaperToken ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowToken;->asWallpaperToken()Lcom/android/server/wm/WallpaperWindowToken;
		 /* .line 61 */
		 /* .local v0, "token":Lcom/android/server/wm/WallpaperWindowToken; */
		 v1 = 		 (( com.android.server.wm.WallpaperWindowToken ) v0 ).canShowWhenLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WallpaperWindowToken;->canShowWhenLocked()Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 v1 = 			 (( com.android.server.wm.WallpaperController$FindWallpaperTargetResult ) p2 ).hasTopShowWhenLockedWallpaper ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->hasTopShowWhenLockedWallpaper()Z
			 /* if-nez v1, :cond_0 */
			 /* .line 62 */
			 (( com.android.server.wm.WallpaperController$FindWallpaperTargetResult ) p2 ).setTopShowWhenLockedWallpaper ( p1 ); // invoke-virtual {p2, p1}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->setTopShowWhenLockedWallpaper(Lcom/android/server/wm/WindowState;)V
			 /* .line 63 */
		 } // :cond_0
		 v1 = 		 (( com.android.server.wm.WallpaperWindowToken ) v0 ).canShowWhenLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WallpaperWindowToken;->canShowWhenLocked()Z
		 /* if-nez v1, :cond_3 */
		 /* .line 65 */
		 try { // :try_start_0
			 final String v1 = "miui.systemui.keyguard.Wallpaper"; // const-string v1, "miui.systemui.keyguard.Wallpaper"
			 v2 = this.mToken;
			 v2 = this.token;
			 v1 = 			 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v1 != null) { // if-eqz v1, :cond_1
				 /* .line 66 */
				 (( com.android.server.wm.WallpaperController$FindWallpaperTargetResult ) p2 ).setTopShowWhenLockedWallpaper ( p1 ); // invoke-virtual {p2, p1}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->setTopShowWhenLockedWallpaper(Lcom/android/server/wm/WindowState;)V
				 /* .line 67 */
			 } // :cond_1
			 v1 = 			 (( com.android.server.wm.WallpaperController$FindWallpaperTargetResult ) p2 ).hasTopHideWhenLockedWallpaper ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->hasTopHideWhenLockedWallpaper()Z
			 /* if-nez v1, :cond_2 */
			 /* .line 68 */
			 (( com.android.server.wm.WallpaperController$FindWallpaperTargetResult ) p2 ).setTopHideWhenLockedWallpaper ( p1 ); // invoke-virtual {p2, p1}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->setTopHideWhenLockedWallpaper(Lcom/android/server/wm/WindowState;)V
			 /* :try_end_0 */
			 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 72 */
		 } // :cond_2
	 } // :goto_0
	 /* .line 70 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 71 */
	 /* .local v1, "e":Landroid/os/RemoteException; */
	 (( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
	 /* .line 75 */
} // .end local v0 # "token":Lcom/android/server/wm/WallpaperWindowToken;
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_3
} // :goto_1
return;
} // .end method
public Float getLastWallpaperX ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 39 */
/* const/high16 v0, -0x40800000 # -1.0f */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 40 */
/* sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 42 */
/* nop */
/* .line 41 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "pref_key_wallpaper_screen_scrolled_span"; // const-string v2, "pref_key_wallpaper_screen_scrolled_span"
int v3 = -1; // const/4 v3, -0x1
v1 = android.provider.Settings$Secure .getInt ( v1,v2,v3 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* .line 43 */
/* .line 44 */
} // :cond_0
/* const/high16 v0, 0x3f000000 # 0.5f */
} // :goto_0
/* nop */
/* .line 48 */
/* .local v0, "wallpaperX":F */
/* .line 51 */
} // .end local v0 # "wallpaperX":F
} // :cond_1
final String v1 = "WallpaperControllerImpl"; // const-string v1, "WallpaperControllerImpl"
final String v2 = "getLastWallpaperX: fail, context null"; // const-string v2, "getLastWallpaperX: fail, context null"
android.util.Slog .e ( v1,v2 );
/* .line 53 */
} // :cond_2
} // .end method
public void showWallpaperIfNeeded ( android.window.TransitionInfo p0, com.android.server.wm.DisplayContent p1, android.view.SurfaceControl$Transaction p2 ) {
/* .locals 10 */
/* .param p1, "info" # Landroid/window/TransitionInfo; */
/* .param p2, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p3, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .line 79 */
v0 = (( android.window.TransitionInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Landroid/window/TransitionInfo;->getType()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_a */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_FOLD_DEVICE:Z */
/* if-nez v0, :cond_a */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_2 */
/* .line 82 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 83 */
/* .local v0, "hasHomeOpen":Z */
int v2 = 0; // const/4 v2, 0x0
/* .line 84 */
/* .local v2, "hasAppClose":Z */
int v3 = 0; // const/4 v3, 0x0
/* .line 85 */
/* .local v3, "hasWallpaperOpen":Z */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( android.window.TransitionInfo ) p1 ).getChanges ( ); // invoke-virtual {p1}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;
int v6 = 1; // const/4 v6, 0x1
/* if-ge v4, v5, :cond_7 */
/* .line 86 */
(( android.window.TransitionInfo ) p1 ).getChanges ( ); // invoke-virtual {p1}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;
/* check-cast v5, Landroid/window/TransitionInfo$Change; */
/* .line 87 */
/* .local v5, "change":Landroid/window/TransitionInfo$Change; */
v7 = (( android.window.TransitionInfo$Change ) v5 ).getMode ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I
/* if-eq v7, v1, :cond_1 */
v7 = (( android.window.TransitionInfo$Change ) v5 ).getMode ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I
int v8 = 4; // const/4 v8, 0x4
/* if-ne v7, v8, :cond_2 */
/* .line 88 */
} // :cond_1
v7 = android.window.TransitionInfo .isIndependent ( v5,p1 );
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 89 */
(( android.window.TransitionInfo$Change ) v5 ).getTaskInfo ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 90 */
(( android.window.TransitionInfo$Change ) v5 ).getTaskInfo ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;
v7 = (( android.app.ActivityManager$RunningTaskInfo ) v7 ).getActivityType ( ); // invoke-virtual {v7}, Landroid/app/ActivityManager$RunningTaskInfo;->getActivityType()I
/* if-ne v7, v6, :cond_2 */
/* .line 91 */
int v2 = 1; // const/4 v2, 0x1
/* .line 93 */
} // :cond_2
v7 = (( android.window.TransitionInfo$Change ) v5 ).getMode ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I
int v8 = 3; // const/4 v8, 0x3
/* if-eq v7, v6, :cond_3 */
v7 = (( android.window.TransitionInfo$Change ) v5 ).getMode ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I
/* if-ne v7, v8, :cond_4 */
/* .line 94 */
} // :cond_3
v7 = android.window.TransitionInfo .isIndependent ( v5,p1 );
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 95 */
(( android.window.TransitionInfo$Change ) v5 ).getTaskInfo ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 96 */
(( android.window.TransitionInfo$Change ) v5 ).getTaskInfo ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;
v7 = (( android.app.ActivityManager$RunningTaskInfo ) v7 ).getActivityType ( ); // invoke-virtual {v7}, Landroid/app/ActivityManager$RunningTaskInfo;->getActivityType()I
/* if-ne v7, v1, :cond_4 */
/* .line 97 */
int v0 = 1; // const/4 v0, 0x1
/* .line 100 */
} // :cond_4
v7 = (( android.window.TransitionInfo$Change ) v5 ).getMode ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I
/* if-eq v7, v6, :cond_5 */
v6 = (( android.window.TransitionInfo$Change ) v5 ).getMode ( ); // invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I
/* if-ne v6, v8, :cond_6 */
/* .line 101 */
} // :cond_5
v6 = (( android.window.TransitionInfo$Change ) v5 ).hasFlags ( v1 ); // invoke-virtual {v5, v1}, Landroid/window/TransitionInfo$Change;->hasFlags(I)Z
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 102 */
int v3 = 1; // const/4 v3, 0x1
/* .line 85 */
} // .end local v5 # "change":Landroid/window/TransitionInfo$Change;
} // :cond_6
/* add-int/lit8 v4, v4, 0x1 */
/* .line 105 */
} // .end local v4 # "i":I
} // :cond_7
/* if-nez v2, :cond_9 */
if ( v0 != null) { // if-eqz v0, :cond_9
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 106 */
v1 = this.mWallpaperController;
(( com.android.server.wm.WallpaperController ) v1 ).getAllTopWallpapers ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WallpaperController;->getAllTopWallpapers()Ljava/util/List;
/* .line 107 */
/* .local v1, "wallpapers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/WindowState;>;" */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 108 */
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_9
/* check-cast v5, Lcom/android/server/wm/WindowState; */
/* .line 109 */
/* .local v5, "wallpaper":Lcom/android/server/wm/WindowState; */
v7 = this.mWinAnimator;
/* .line 110 */
/* .local v7, "wsa":Lcom/android/server/wm/WindowStateAnimator; */
if ( v7 != null) { // if-eqz v7, :cond_8
/* iget v8, v7, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I */
/* if-ne v8, v6, :cond_8 */
/* iget v8, v7, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F */
/* const/high16 v9, 0x3f800000 # 1.0f */
/* cmpl-float v8, v8, v9 */
/* if-nez v8, :cond_8 */
/* .line 111 */
v8 = this.mSurfaceController;
(( com.android.server.wm.WindowSurfaceController ) v8 ).showRobustly ( p3 ); // invoke-virtual {v8, p3}, Lcom/android/server/wm/WindowSurfaceController;->showRobustly(Landroid/view/SurfaceControl$Transaction;)V
/* .line 113 */
} // .end local v5 # "wallpaper":Lcom/android/server/wm/WindowState;
} // .end local v7 # "wsa":Lcom/android/server/wm/WindowStateAnimator;
} // :cond_8
/* .line 116 */
} // .end local v1 # "wallpapers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/WindowState;>;"
} // :cond_9
return;
/* .line 80 */
} // .end local v0 # "hasHomeOpen":Z
} // .end local v2 # "hasAppClose":Z
} // .end local v3 # "hasWallpaperOpen":Z
} // :cond_a
} // :goto_2
return;
} // .end method
