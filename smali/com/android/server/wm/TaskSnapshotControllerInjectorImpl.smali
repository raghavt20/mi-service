.class Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;
.super Lcom/android/server/wm/TaskSnapshotControllerInjectorStub;
.source "TaskSnapshotControllerInjectorImpl.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TaskSnapshot_CInjector"


# direct methods
.method public static synthetic $r8$lambda$QavdEXcV9xH1ZFJZ3ISxlrC3VSY(Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;Lcom/android/server/wm/ActivityRecord;ZLcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Z)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->lambda$doSnapshotQS$0(Lcom/android/server/wm/ActivityRecord;ZLcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Z)V

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/android/server/wm/TaskSnapshotControllerInjectorStub;-><init>()V

    return-void
.end method

.method private doSnapshotQSInternal(Lcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Lcom/android/server/wm/ActivityRecord;ZZ)V
    .locals 7
    .param p1, "controller"    # Lcom/android/server/wm/TaskSnapshotController;
    .param p2, "persister"    # Lcom/android/server/wm/TaskSnapshotPersister;
    .param p3, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "visible"    # Z
    .param p5, "forceUpdate"    # Z

    .line 78
    invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 79
    .local v0, "task":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->isVisible()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-nez p5, :cond_1

    .line 80
    return-void

    .line 82
    :cond_1
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->isActivityTypeHome()Z

    move-result v1

    const-string v2, "TaskSnapshot_CInjector"

    if-eqz v1, :cond_2

    .line 83
    const-string v1, "doSnapshotQSInternal()...return as isActivityTypeHome!"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    return-void

    .line 88
    :cond_2
    :try_start_0
    iget-object v1, v0, Lcom/android/server/wm/Task;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    iget-object v1, v1, Lcom/android/server/wm/DisplayContent;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->getWindowInfo()Landroid/view/WindowInfo;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowInfo;->type:I

    .line 89
    .local v1, "windowType":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_3

    .line 90
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doSnapshotQSInternal()... return as window type is incorrect, type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    return-void

    .line 94
    :cond_3
    invoke-static {}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorStub;->get()Lcom/android/server/wm/TaskSnapshotPersisterInjectorStub;

    move-result-object v4

    new-instance v5, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$$ExternalSyntheticLambda0;

    invoke-direct {v5}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {v4, v5, p3}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorStub;->couldPersist(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 95
    const-string v3, "couldPersist()... false."

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    return-void

    .line 99
    :cond_4
    invoke-virtual {p1, v0}, Lcom/android/server/wm/TaskSnapshotController;->getSnapshotMode(Lcom/android/server/wm/WindowContainer;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 106
    goto :goto_0

    .line 101
    :pswitch_0
    invoke-virtual {p1, v0, v3}, Lcom/android/server/wm/TaskSnapshotController;->snapshot(Lcom/android/server/wm/WindowContainer;Z)Landroid/window/TaskSnapshot;

    move-result-object v3

    .line 102
    .local v3, "snapshot":Landroid/window/TaskSnapshot;
    goto :goto_1

    .line 106
    .end local v3    # "snapshot":Landroid/window/TaskSnapshot;
    :goto_0
    const-string v3, "doSnapshotQSInternal()...snapshot mode NOT ok!"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const/4 v3, 0x0

    .line 111
    .restart local v3    # "snapshot":Landroid/window/TaskSnapshot;
    :goto_1
    if-nez v3, :cond_6

    if-eqz p5, :cond_6

    .line 112
    sget-object v4, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_STARTING_WINDOW:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v4}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 113
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "snapshot is null and use snapshot if needed. current state "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_5
    sget-object v4, Lcom/android/server/wm/ActivityRecord$State;->RESUMED:Lcom/android/server/wm/ActivityRecord$State;

    invoke-virtual {p3, v4}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 116
    iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I

    iget v5, v0, Lcom/android/server/wm/Task;->mUserId:I

    const/4 v6, 0x0

    invoke-virtual {p1, v4, v5, v6, v6}, Lcom/android/server/wm/TaskSnapshotController;->getSnapshot(IIZZ)Landroid/window/TaskSnapshot;

    move-result-object v4

    move-object v3, v4

    .line 119
    :cond_6
    if-eqz v3, :cond_7

    .line 120
    sget-object v4, Landroid/app/TaskSnapshotHelperImpl;->QUICK_START_NAME_WITH_ACTIVITY_LIST:Ljava/util/List;

    iget-object v5, p3, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 121
    iget-object v4, p3, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/window/TaskSnapshot;->setClassNameQS(Ljava/lang/String;)V

    .line 125
    :cond_7
    invoke-direct {p0, v3, v0, p2}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->saveSnapshot(Landroid/window/TaskSnapshot;Lcom/android/server/wm/Task;Lcom/android/server/wm/TaskSnapshotPersister;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    .end local v1    # "windowType":I
    .end local v3    # "snapshot":Landroid/window/TaskSnapshot;
    goto :goto_2

    .line 126
    :catch_0
    move-exception v1

    .line 127
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private synthetic lambda$doSnapshotQS$0(Lcom/android/server/wm/ActivityRecord;ZLcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Z)V
    .locals 7
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "forceUpdate"    # Z
    .param p3, "controller"    # Lcom/android/server/wm/TaskSnapshotController;
    .param p4, "persister"    # Lcom/android/server/wm/TaskSnapshotPersister;
    .param p5, "visible"    # Z

    .line 54
    const-string v0, "TaskSnapshot_CInjector"

    :try_start_0
    sget-object v1, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_STARTING_WINDOW:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v1}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doSnapshotQS()...later! state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->toState(Lcom/android/server/wm/ActivityRecord$State;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;

    move-result-object v1

    sget-object v2, Lcom/android/server/wm/ActivityRecord$State;->RESUMED:Lcom/android/server/wm/ActivityRecord$State;

    if-ne v1, v2, :cond_1

    .line 59
    invoke-virtual {p0, p1}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->canTakeSnapshot(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60
    invoke-virtual {p0, p1}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->isTop(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    if-nez p2, :cond_2

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doSnapshotQS()... launchedBy="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPackage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    return-void

    .line 66
    :cond_2
    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move v5, p5

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->doSnapshotQSInternal(Lcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Lcom/android/server/wm/ActivityRecord;ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    goto :goto_0

    .line 67
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "exception:"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 70
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private saveSnapshot(Landroid/window/TaskSnapshot;Lcom/android/server/wm/Task;Lcom/android/server/wm/TaskSnapshotPersister;)V
    .locals 4
    .param p1, "snapshot"    # Landroid/window/TaskSnapshot;
    .param p2, "task"    # Lcom/android/server/wm/Task;
    .param p3, "persister"    # Lcom/android/server/wm/TaskSnapshotPersister;

    .line 131
    const-string v0, "TaskSnapshot_CInjector"

    if-eqz p1, :cond_2

    .line 132
    invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getSnapshot()Landroid/graphics/GraphicBuffer;

    move-result-object v1

    .line 133
    .local v1, "buffer":Landroid/graphics/GraphicBuffer;
    invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->getWidth()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->getHeight()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 138
    :cond_0
    iget v0, p2, Lcom/android/server/wm/Task;->mTaskId:I

    iget v2, p2, Lcom/android/server/wm/Task;->mUserId:I

    invoke-virtual {p3, v0, v2, p1}, Lcom/android/server/wm/TaskSnapshotPersister;->persistSnapshotQS(IILandroid/window/TaskSnapshot;)V

    goto :goto_1

    .line 134
    :cond_1
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->destroy()V

    .line 135
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid task snapshot dimensions "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 136
    invoke-virtual {v1}, Landroid/graphics/GraphicBuffer;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 135
    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    .end local v1    # "buffer":Landroid/graphics/GraphicBuffer;
    :goto_1
    goto :goto_2

    .line 141
    :cond_2
    const-string v1, "doSnapshotQSInternal()...snapshot is null!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :goto_2
    return-void
.end method

.method public static toState(Lcom/android/server/wm/ActivityRecord$State;)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # Lcom/android/server/wm/ActivityRecord$State;

    .line 160
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->DESTROYED:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_0

    .line 161
    const-string v0, "DESTROYED"

    return-object v0

    .line 163
    :cond_0
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->DESTROYING:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_1

    .line 164
    const-string v0, "DESTROYING"

    return-object v0

    .line 166
    :cond_1
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->FINISHING:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_2

    .line 167
    const-string v0, "FINISHING"

    return-object v0

    .line 169
    :cond_2
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->INITIALIZING:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_3

    .line 170
    const-string v0, "INITIALIZING"

    return-object v0

    .line 172
    :cond_3
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->PAUSED:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_4

    .line 173
    const-string v0, "PAUSED"

    return-object v0

    .line 175
    :cond_4
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->PAUSING:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_5

    .line 176
    const-string v0, "PAUSING"

    return-object v0

    .line 178
    :cond_5
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->RESTARTING_PROCESS:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_6

    .line 179
    const-string v0, "RESTARTING_PROCESS"

    return-object v0

    .line 181
    :cond_6
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->RESUMED:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_7

    .line 182
    const-string v0, "RESUMED"

    return-object v0

    .line 184
    :cond_7
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->STARTED:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_8

    .line 185
    const-string v0, "STARTED"

    return-object v0

    .line 187
    :cond_8
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->STOPPED:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_9

    .line 188
    const-string v0, "STOPPED"

    return-object v0

    .line 190
    :cond_9
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->STOPPING:Lcom/android/server/wm/ActivityRecord$State;

    if-ne p0, v0, :cond_a

    .line 191
    const-string v0, "STOPPING"

    return-object v0

    .line 193
    :cond_a
    const-string v0, "Unknown"

    return-object v0
.end method


# virtual methods
.method public adaptLetterboxInsets(Lcom/android/server/wm/ActivityRecord;Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "activity"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "letterboxInsets"    # Landroid/graphics/Rect;

    .line 203
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 204
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 205
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->adaptLetterboxInsets(Lcom/android/server/wm/ActivityRecord;Landroid/graphics/Rect;)V

    .line 208
    :cond_0
    return-void
.end method

.method public canTakeSnapshot(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 197
    sget-object v0, Landroid/app/TaskSnapshotHelperImpl;->QUICK_START_HOME_PACKAGE_NAME_LIST:Ljava/util/List;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPackage:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/app/TaskSnapshotHelperImpl;->QUICK_START_SPECIAL_PACKAGE_NAME_LIST:Ljava/util/List;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 198
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 197
    :goto_1
    return v0
.end method

.method public doSnapshotQS(Lcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Landroid/os/Handler;Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 7
    .param p1, "controller"    # Lcom/android/server/wm/TaskSnapshotController;
    .param p2, "persister"    # Lcom/android/server/wm/TaskSnapshotPersister;
    .param p3, "handler"    # Landroid/os/Handler;
    .param p4, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p5, "visible"    # Z

    .line 32
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->doSnapshotQS(Lcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Landroid/os/Handler;Lcom/android/server/wm/ActivityRecord;ZZ)V

    .line 33
    return-void
.end method

.method public doSnapshotQS(Lcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Landroid/os/Handler;Lcom/android/server/wm/ActivityRecord;ZZ)V
    .locals 9
    .param p1, "controller"    # Lcom/android/server/wm/TaskSnapshotController;
    .param p2, "persister"    # Lcom/android/server/wm/TaskSnapshotPersister;
    .param p3, "handler"    # Landroid/os/Handler;
    .param p4, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p5, "visible"    # Z
    .param p6, "forceUpdate"    # Z

    .line 41
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    if-nez p4, :cond_0

    goto :goto_0

    .line 45
    :cond_0
    sget-object v0, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_STARTING_WINDOW:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v0}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doSnapshotQS()...ago! activity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p4, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 47
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 48
    invoke-virtual {p4}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;->toState(Lcom/android/server/wm/ActivityRecord$State;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastVisibleTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p4, Lcom/android/server/wm/ActivityRecord;->lastVisibleTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 46
    const-string v1, "TaskSnapshot_CInjector"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :cond_1
    new-instance v0, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$$ExternalSyntheticLambda1;

    move-object v2, v0

    move-object v3, p0

    move-object v4, p4

    move v5, p6

    move-object v6, p1

    move-object v7, p2

    move v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl;Lcom/android/server/wm/ActivityRecord;ZLcom/android/server/wm/TaskSnapshotController;Lcom/android/server/wm/TaskSnapshotPersister;Z)V

    .line 70
    invoke-static {}, Landroid/app/TaskSnapshotHelperStub;->get()Landroid/app/TaskSnapshotHelperStub;

    move-result-object v1

    iget-object v2, p4, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroid/app/TaskSnapshotHelperStub;->delayTime(Ljava/lang/String;)J

    move-result-wide v1

    .line 52
    invoke-virtual {p3, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 71
    return-void

    .line 42
    :cond_2
    :goto_0
    return-void
.end method

.method public isTop(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 5
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 146
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 147
    .local v0, "task":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_1

    .line 148
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "current":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    .line 151
    .local v2, "top":Ljava/lang/String;
    sget-object v3, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_STARTING_WINDOW:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v3}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isTop()...current="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", topActivity="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "TaskSnapshot_CInjector"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    return v3

    .line 156
    .end local v1    # "current":Ljava/lang/String;
    .end local v2    # "top":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    return v1
.end method
