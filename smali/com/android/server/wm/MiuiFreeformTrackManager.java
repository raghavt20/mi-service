public class com.android.server.wm.MiuiFreeformTrackManager {
	 /* .source "MiuiFreeformTrackManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiFreeformTrackManager$CommonTrackConstants;, */
	 /* Lcom/android/server/wm/MiuiFreeformTrackManager$MultiWindowRecommendTrackConstants;, */
	 /* Lcom/android/server/wm/MiuiFreeformTrackManager$MultiFreeformTrackConstants;, */
	 /* Lcom/android/server/wm/MiuiFreeformTrackManager$SmallWindowTrackConstants;, */
	 /* Lcom/android/server/wm/MiuiFreeformTrackManager$MiniWindowTrackConstants; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID;
private static final Integer FLAG_NON_ANONYMOUS;
private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
private static final java.lang.String SERVER_CLASS_NAME;
private static final java.lang.String SERVER_PKG_NAME;
private static final java.lang.String TAG;
/* # instance fields */
private java.lang.Runnable mBindOneTrackService;
private android.content.Context mContext;
public final java.lang.Object mFreeFormTrackLock;
private android.os.Handler mHandler;
private com.miui.analytics.ITrackBinder mITrackBinder;
private com.android.server.wm.MiuiFreeFormGestureController mMiuiFreeFormGestureController;
private android.content.ServiceConnection mServiceConnection;
/* # direct methods */
static java.lang.Runnable -$$Nest$fgetmBindOneTrackService ( com.android.server.wm.MiuiFreeformTrackManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBindOneTrackService;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.wm.MiuiFreeformTrackManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.wm.MiuiFreeformTrackManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static com.miui.analytics.ITrackBinder -$$Nest$fgetmITrackBinder ( com.android.server.wm.MiuiFreeformTrackManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mITrackBinder;
} // .end method
static void -$$Nest$fputmITrackBinder ( com.android.server.wm.MiuiFreeformTrackManager p0, com.miui.analytics.ITrackBinder p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mITrackBinder = p1;
	 return;
} // .end method
static void -$$Nest$mputCommomParam ( com.android.server.wm.MiuiFreeformTrackManager p0, org.json.JSONObject p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->putCommomParam(Lorg/json/JSONObject;)V */
	 return;
} // .end method
 com.android.server.wm.MiuiFreeformTrackManager ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "controller" # Lcom/android/server/wm/MiuiFreeFormGestureController; */
	 /* .line 175 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 40 */
	 /* new-instance v0, Ljava/lang/Object; */
	 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
	 this.mFreeFormTrackLock = v0;
	 /* .line 224 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiFreeformTrackManager$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeformTrackManager$2;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;)V */
	 this.mBindOneTrackService = v0;
	 /* .line 176 */
	 this.mMiuiFreeFormGestureController = p2;
	 /* .line 177 */
	 this.mContext = p1;
	 /* .line 178 */
	 /* new-instance v0, Landroid/os/Handler; */
	 com.android.server.MiuiBgThread .get ( );
	 (( com.android.server.MiuiBgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 179 */
	 (( com.android.server.wm.MiuiFreeformTrackManager ) p0 ).bindOneTrackService ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformTrackManager;->bindOneTrackService()V
	 /* .line 180 */
	 return;
} // .end method
private static java.lang.String getRegion ( ) {
	 /* .locals 10 */
	 /* .line 758 */
	 final String v0 = ""; // const-string v0, ""
	 try { // :try_start_0
		 final String v1 = "ro.miui.region"; // const-string v1, "ro.miui.region"
		 android.os.SystemProperties .get ( v1,v0 );
		 /* .line 759 */
		 /* .local v1, "region":Ljava/lang/String; */
		 v2 = 		 android.text.TextUtils .isEmpty ( v1 );
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 760 */
			 final String v2 = "ro.product.locale.region"; // const-string v2, "ro.product.locale.region"
			 android.os.SystemProperties .get ( v2,v0 );
			 /* move-object v1, v2 */
			 /* .line 762 */
		 } // :cond_0
		 v2 = 		 android.text.TextUtils .isEmpty ( v1 );
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* .line 764 */
			 final String v2 = "android.os.LocaleList"; // const-string v2, "android.os.LocaleList"
			 /* .line 765 */
			 java.lang.Class .forName ( v2 );
			 final String v3 = "getDefault"; // const-string v3, "getDefault"
			 int v4 = 0; // const/4 v4, 0x0
			 /* new-array v5, v4, [Ljava/lang/Class; */
			 (( java.lang.Class ) v2 ).getMethod ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
			 /* new-array v3, v4, [Ljava/lang/Object; */
			 int v5 = 0; // const/4 v5, 0x0
			 (( java.lang.reflect.Method ) v2 ).invoke ( v5, v3 ); // invoke-virtual {v2, v5, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* .line 766 */
			 /* .local v2, "localeList":Ljava/lang/Object; */
			 (( java.lang.Object ) v2 ).getClass ( ); // invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
			 /* const-string/jumbo v5, "size" */
			 /* new-array v6, v4, [Ljava/lang/Class; */
			 (( java.lang.Class ) v3 ).getMethod ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
			 /* new-array v5, v4, [Ljava/lang/Object; */
			 (( java.lang.reflect.Method ) v3 ).invoke ( v2, v5 ); // invoke-virtual {v3, v2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* .line 767 */
			 /* .local v3, "size":Ljava/lang/Object; */
			 /* instance-of v5, v3, Ljava/lang/Integer; */
			 if ( v5 != null) { // if-eqz v5, :cond_1
				 /* move-object v5, v3 */
				 /* check-cast v5, Ljava/lang/Integer; */
				 v5 = 				 (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
				 /* if-lez v5, :cond_1 */
				 /* .line 768 */
				 /* nop */
				 /* .line 769 */
				 (( java.lang.Object ) v2 ).getClass ( ); // invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
				 final String v6 = "get"; // const-string v6, "get"
				 int v7 = 1; // const/4 v7, 0x1
				 /* new-array v8, v7, [Ljava/lang/Class; */
				 v9 = java.lang.Integer.TYPE;
				 /* aput-object v9, v8, v4 */
				 (( java.lang.Class ) v5 ).getMethod ( v6, v8 ); // invoke-virtual {v5, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
				 /* new-array v6, v7, [Ljava/lang/Object; */
				 java.lang.Integer .valueOf ( v4 );
				 /* aput-object v7, v6, v4 */
				 (( java.lang.reflect.Method ) v5 ).invoke ( v2, v6 ); // invoke-virtual {v5, v2, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
				 /* .line 770 */
				 /* .local v5, "locale":Ljava/lang/Object; */
				 (( java.lang.Object ) v5 ).getClass ( ); // invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
				 final String v7 = "getCountry"; // const-string v7, "getCountry"
				 /* new-array v8, v4, [Ljava/lang/Class; */
				 (( java.lang.Class ) v6 ).getMethod ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
				 /* new-array v4, v4, [Ljava/lang/Object; */
				 (( java.lang.reflect.Method ) v6 ).invoke ( v5, v4 ); // invoke-virtual {v6, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
				 /* .line 771 */
				 /* .local v4, "country":Ljava/lang/Object; */
				 /* instance-of v6, v4, Ljava/lang/String; */
				 if ( v6 != null) { // if-eqz v6, :cond_1
					 /* .line 772 */
					 /* move-object v6, v4 */
					 /* check-cast v6, Ljava/lang/String; */
					 /* move-object v1, v6 */
					 /* .line 776 */
				 } // .end local v2 # "localeList":Ljava/lang/Object;
			 } // .end local v3 # "size":Ljava/lang/Object;
		 } // .end local v4 # "country":Ljava/lang/Object;
	 } // .end local v5 # "locale":Ljava/lang/Object;
} // :cond_1
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_2
	 /* .line 777 */
	 java.util.Locale .getDefault ( );
	 (( java.util.Locale ) v2 ).getCountry ( ); // invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;
	 /* move-object v1, v2 */
	 /* .line 780 */
} // :cond_2
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_3 */
/* .line 781 */
(( java.lang.String ) v1 ).trim ( ); // invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 785 */
} // .end local v1 # "region":Ljava/lang/String;
} // :cond_3
/* .line 783 */
/* :catch_0 */
/* move-exception v1 */
/* .line 784 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "MiuiFreeformOneTrackManager"; // const-string v2, "MiuiFreeformOneTrackManager"
final String v3 = "getRegion Exception: "; // const-string v3, "getRegion Exception: "
android.util.Log .e ( v2,v3,v1 );
/* .line 786 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public static Boolean isCanOneTrack ( ) {
/* .locals 2 */
/* .line 753 */
final String v0 = "CN"; // const-string v0, "CN"
com.android.server.wm.MiuiFreeformTrackManager .getRegion ( );
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
private void putCommomParam ( org.json.JSONObject p0 ) {
/* .locals 2 */
/* .param p1, "jsonData" # Lorg/json/JSONObject; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
/* .line 738 */
v0 = this.mContext;
v0 = android.util.MiuiMultiWindowUtils .isPadScreen ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 739 */
final String v0 = "pad"; // const-string v0, "pad"
/* .line 740 */
} // :cond_0
/* const-string/jumbo v0, "\u624b\u673a" */
/* .line 738 */
} // :goto_0
final String v1 = "model_type"; // const-string v1, "model_type"
(( org.json.JSONObject ) p1 ).put ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 741 */
v0 = this.mMiuiFreeFormGestureController;
/* iget-boolean v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsPortrait:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 742 */
/* const-string/jumbo v0, "\u7ad6\u5c4f" */
/* .line 743 */
} // :cond_1
/* const-string/jumbo v0, "\u6a2a\u5c4f" */
/* .line 741 */
} // :goto_1
final String v1 = "screen_orientation"; // const-string v1, "screen_orientation"
(( org.json.JSONObject ) p1 ).put ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 744 */
v0 = this.mContext;
v0 = android.util.MiuiMultiWindowUtils .isFoldInnerScreen ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 745 */
/* const-string/jumbo v0, "\u5185\u5c4f" */
/* .line 746 */
} // :cond_2
v0 = this.mContext;
v0 = android.util.MiuiMultiWindowUtils .isFoldOuterScreen ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 747 */
/* const-string/jumbo v0, "\u5916\u5c4f" */
/* .line 748 */
} // :cond_3
final String v0 = "nothing"; // const-string v0, "nothing"
/* .line 744 */
} // :goto_2
/* const-string/jumbo v1, "screen_type" */
(( org.json.JSONObject ) p1 ).put ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 749 */
final String v0 = "data_version"; // const-string v0, "data_version"
final String v1 = "22053100"; // const-string v1, "22053100"
(( org.json.JSONObject ) p1 ).put ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 750 */
return;
} // .end method
/* # virtual methods */
public void bindOneTrackService ( ) {
/* .locals 4 */
/* .line 186 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
/* .line 187 */
final String v0 = "MiuiFreeformOneTrackManager"; // const-string v0, "MiuiFreeformOneTrackManager"
final String v1 = "Context == null"; // const-string v1, "Context == null"
android.util.Slog .d ( v0,v1 );
/* .line 188 */
return;
/* .line 190 */
} // :cond_0
v0 = this.mFreeFormTrackLock;
/* monitor-enter v0 */
/* .line 191 */
try { // :try_start_0
v1 = this.mITrackBinder;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 192 */
final String v1 = "MiuiFreeformOneTrackManager"; // const-string v1, "MiuiFreeformOneTrackManager"
final String v2 = "aready bound"; // const-string v2, "aready bound"
android.util.Slog .d ( v1,v2 );
/* .line 193 */
/* monitor-exit v0 */
return;
/* .line 195 */
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 197 */
try { // :try_start_1
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 198 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
final String v2 = "com.miui.analytics.onetrack.TrackService"; // const-string v2, "com.miui.analytics.onetrack.TrackService"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 199 */
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeformTrackManager$1;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;)V */
this.mServiceConnection = v1;
/* .line 216 */
final String v1 = "MiuiFreeformOneTrackManager"; // const-string v1, "MiuiFreeformOneTrackManager"
final String v2 = "Start Bind OneTrack Service"; // const-string v2, "Start Bind OneTrack Service"
android.util.Slog .d ( v1,v2 );
/* .line 217 */
v1 = this.mContext;
v2 = this.mServiceConnection;
int v3 = 1; // const/4 v3, 0x1
(( android.content.Context ) v1 ).bindService ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 221 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 218 */
/* :catch_0 */
/* move-exception v0 */
/* .line 219 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 220 */
final String v1 = "MiuiFreeformOneTrackManager"; // const-string v1, "MiuiFreeformOneTrackManager"
final String v2 = "Bind OneTrack Service Exception"; // const-string v2, "Bind OneTrack Service Exception"
android.util.Slog .e ( v1,v2 );
/* .line 222 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 195 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public void closeOneTrackService ( android.content.Context p0, android.content.ServiceConnection p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "serviceConnection" # Landroid/content/ServiceConnection; */
/* .line 231 */
/* if-nez p1, :cond_0 */
/* .line 232 */
final String v0 = "MiuiFreeformOneTrackManager"; // const-string v0, "MiuiFreeformOneTrackManager"
/* const-string/jumbo v1, "unBindOneTrackService: mContext == null " */
android.util.Slog .d ( v0,v1 );
/* .line 233 */
return;
/* .line 235 */
} // :cond_0
v0 = this.mFreeFormTrackLock;
/* monitor-enter v0 */
/* .line 236 */
try { // :try_start_0
v1 = this.mITrackBinder;
if ( v1 != null) { // if-eqz v1, :cond_1
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 237 */
(( android.content.Context ) p1 ).unbindService ( p2 ); // invoke-virtual {p1, p2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 238 */
final String v1 = "MiuiFreeformOneTrackManager"; // const-string v1, "MiuiFreeformOneTrackManager"
/* const-string/jumbo v2, "unBindOneTrackService success" */
android.util.Slog .d ( v1,v2 );
/* .line 240 */
} // :cond_1
final String v1 = "MiuiFreeformOneTrackManager"; // const-string v1, "MiuiFreeformOneTrackManager"
/* const-string/jumbo v2, "unBindOneTrackService failed: mServiceConnection == null" */
android.util.Slog .d ( v1,v2 );
/* .line 242 */
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
this.mITrackBinder = v1;
/* .line 243 */
/* monitor-exit v0 */
/* .line 244 */
return;
/* .line 243 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void trackClickSmallWindowEvent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .line 431 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$10; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$10;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 451 */
return;
} // .end method
public void trackFreeFormRecommendClickEvent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .line 710 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$21; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$21;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 735 */
return;
} // .end method
public void trackFreeFormRecommendExposeEvent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .line 654 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$19; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$19;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 679 */
return;
} // .end method
public void trackMiniWindowEnterWayEvent ( java.lang.String p0, android.graphics.Point p1, java.lang.String p2, java.lang.String p3, Integer p4 ) {
/* .locals 9 */
/* .param p1, "enterWay" # Ljava/lang/String; */
/* .param p2, "leftTopPosition" # Landroid/graphics/Point; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "applicationName" # Ljava/lang/String; */
/* .param p5, "freeformAppCount" # I */
/* .line 248 */
v0 = this.mHandler;
/* new-instance v8, Lcom/android/server/wm/MiuiFreeformTrackManager$3; */
/* move-object v1, v8 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* move v7, p5 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/wm/MiuiFreeformTrackManager$3;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;I)V */
(( android.os.Handler ) v0 ).post ( v8 ); // invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 272 */
return;
} // .end method
public void trackMiniWindowMoveEvent ( android.graphics.Point p0, android.graphics.Point p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 8 */
/* .param p1, "startPosition" # Landroid/graphics/Point; */
/* .param p2, "endPosition" # Landroid/graphics/Point; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "applicationName" # Ljava/lang/String; */
/* .line 276 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$4; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$4;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Landroid/graphics/Point;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 300 */
return;
} // .end method
public void trackMiniWindowPinedEvent ( java.lang.String p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .param p3, "posX" # I */
/* .param p4, "posY" # I */
/* .line 481 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$12; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move v5, p3 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$12;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;II)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 503 */
return;
} // .end method
public void trackMiniWindowPinedMoveEvent ( java.lang.String p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .param p3, "posX" # I */
/* .param p4, "posY" # I */
/* .line 531 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$14; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move v5, p3 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$14;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;II)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 553 */
return;
} // .end method
public void trackMiniWindowPinedQuitEvent ( java.lang.String p0, java.lang.String p1, Float p2 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .param p3, "pinedDuration" # F */
/* .line 580 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$16; */
/* invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeformTrackManager$16;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;F)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 601 */
return;
} // .end method
public void trackMiniWindowQuitEvent ( java.lang.String p0, android.graphics.Point p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 8 */
/* .param p1, "quitWay" # Ljava/lang/String; */
/* .param p2, "leftTopPosition" # Landroid/graphics/Point; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "applicationName" # Ljava/lang/String; */
/* .line 304 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$5; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$5;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Landroid/graphics/Point;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 327 */
return;
} // .end method
public void trackMultiFreeformEnterEvent ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "nums" # I */
/* .line 604 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$17; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeformTrackManager$17;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 623 */
return;
} // .end method
public void trackSmallWindowEnterWayEvent ( java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, Integer p4 ) {
/* .locals 9 */
/* .param p1, "enterWay" # Ljava/lang/String; */
/* .param p2, "smallWindowRatio" # Ljava/lang/String; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "applicationName" # Ljava/lang/String; */
/* .param p5, "freeformAppCount" # I */
/* .line 331 */
v0 = this.mHandler;
/* new-instance v8, Lcom/android/server/wm/MiuiFreeformTrackManager$6; */
/* move-object v1, v8 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* move v7, p5 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/wm/MiuiFreeformTrackManager$6;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V */
(( android.os.Handler ) v0 ).post ( v8 ); // invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 354 */
return;
} // .end method
public void trackSmallWindowMoveEvent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .line 357 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$7; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$7;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 377 */
return;
} // .end method
public void trackSmallWindowPinedEvent ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 9 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .param p3, "pinedWay" # Ljava/lang/String; */
/* .param p4, "posX" # I */
/* .param p5, "posY" # I */
/* .line 455 */
v0 = this.mHandler;
/* new-instance v8, Lcom/android/server/wm/MiuiFreeformTrackManager$11; */
/* move-object v1, v8 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move v6, p4 */
/* move v7, p5 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/wm/MiuiFreeformTrackManager$11;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V */
(( android.os.Handler ) v0 ).post ( v8 ); // invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 478 */
return;
} // .end method
public void trackSmallWindowPinedMoveEvent ( java.lang.String p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .param p3, "posX" # I */
/* .param p4, "posY" # I */
/* .line 506 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$13; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move v5, p3 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$13;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;II)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 528 */
return;
} // .end method
public void trackSmallWindowPinedQuitEvent ( java.lang.String p0, java.lang.String p1, Float p2 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .param p3, "pinedDuration" # F */
/* .line 556 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$15; */
/* invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeformTrackManager$15;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;F)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 577 */
return;
} // .end method
public void trackSmallWindowQuitEvent ( java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, Long p4 ) {
/* .locals 8 */
/* .param p1, "quitWay" # Ljava/lang/String; */
/* .param p2, "smallWindowRatio" # Ljava/lang/String; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "applicationName" # Ljava/lang/String; */
/* .param p5, "useDuration" # J */
/* .line 407 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$9; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$9;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 428 */
return;
} // .end method
public void trackSmallWindowResizeEvent ( java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, Long p4 ) {
/* .locals 8 */
/* .param p1, "smallWindowFromRatio" # Ljava/lang/String; */
/* .param p2, "smallWindowToRatio" # Ljava/lang/String; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "applicationName" # Ljava/lang/String; */
/* .param p5, "useDuration" # J */
/* .line 381 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/wm/MiuiFreeformTrackManager$8; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiFreeformTrackManager$8;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 403 */
return;
} // .end method
public void trackSplitScreenRecommendClickEvent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .line 682 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$20; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$20;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 707 */
return;
} // .end method
public void trackSplitScreenRecommendExposeEvent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "applicationName" # Ljava/lang/String; */
/* .line 626 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeformTrackManager$18; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/MiuiFreeformTrackManager$18;-><init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 651 */
return;
} // .end method
