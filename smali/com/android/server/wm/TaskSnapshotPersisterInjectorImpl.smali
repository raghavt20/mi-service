.class Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;
.super Lcom/android/server/wm/TaskSnapshotPersisterInjectorStub;
.source "TaskSnapshotPersisterInjectorImpl.java"


# static fields
.field public static final BITMAP_EXTENSION:Ljava/lang/String; = ".jpg"

.field public static final LOW_RES_FILE_POSTFIX:Ljava/lang/String; = "_reduced"

.field public static final PROTO_EXTENSION:Ljava/lang/String; = ".proto"

.field public static final SNAPSHOTS_DIRNAME_QS:Ljava/lang/String; = "qs_snapshots"

.field private static final TAG:Ljava/lang/String; = "TaskSnapshot_PInjector"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorStub;-><init>()V

    return-void
.end method


# virtual methods
.method public checkName(Landroid/window/TaskSnapshot;)Ljava/lang/String;
    .locals 3
    .param p1, "snapshot"    # Landroid/window/TaskSnapshot;

    .line 184
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getTopActivityComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 189
    :cond_0
    invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getTopActivityComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, "name":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Landroid/app/TaskSnapshotHelperImpl;->QUICK_START_NAME_WITH_ACTIVITY_LIST:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 191
    invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getClassNameQS()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getClassNameQS()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 194
    :cond_1
    return-object v0

    .line 185
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    :goto_0
    const-string v0, ""

    return-object v0
.end method

.method public closeQuitely(Ljava/io/Closeable;)V
    .locals 3
    .param p1, "stream"    # Ljava/io/Closeable;

    .line 199
    if-eqz p1, :cond_0

    .line 200
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closeQuitely()...close file failed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TaskSnapshot_PInjector"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 204
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    nop

    .line 205
    :goto_1
    return-void
.end method

.method public couldPersist(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p1, "resolver"    # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;
    .param p2, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 38
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->couldPersist(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;Lcom/android/server/wm/ActivityRecord;Z)Z

    move-result v0

    return v0
.end method

.method public couldPersist(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;Lcom/android/server/wm/ActivityRecord;Z)Z
    .locals 18
    .param p1, "resolver"    # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;
    .param p2, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "forceUpdate"    # Z

    .line 52
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    const/4 v5, 0x0

    if-eqz v3, :cond_7

    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    goto/16 :goto_5

    .line 56
    :cond_0
    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v12

    .line 57
    .local v12, "pkg":Ljava/lang/String;
    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mWmService:Lcom/android/server/wm/WindowManagerService;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lcom/android/server/wm/ActivityRecord;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    move-object v13, v0

    .line 59
    .local v13, "context":Landroid/content/Context;
    invoke-static {}, Landroid/app/TaskSnapshotHelperStub;->get()Landroid/app/TaskSnapshotHelperStub;

    move-result-object v0

    iget-object v6, v3, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-interface {v0, v13, v12, v6}, Landroid/app/TaskSnapshotHelperStub;->ensureEnable(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 60
    return v5

    .line 63
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v14

    .line 64
    .local v14, "task":Lcom/android/server/wm/Task;
    iget v0, v14, Lcom/android/server/wm/Task;->mUserId:I

    invoke-virtual {v1, v2, v0, v12, v5}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getBitmapFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    .line 65
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    const/4 v15, 0x1

    const-string v7, "TaskSnapshot_PInjector"

    if-nez v6, :cond_3

    .line 66
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "couldPersist()...return true (shot not exist)! file="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    return v15

    .line 70
    :cond_3
    iget v6, v14, Lcom/android/server/wm/Task;->mUserId:I

    iget-object v8, v3, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v2, v6, v8}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getProtoFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 71
    .end local v0    # "file":Ljava/io/File;
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "couldPersist()...return true (proto not exist)! file="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    return v15

    .line 75
    :cond_4
    const/4 v8, 0x0

    .line 77
    .local v8, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/FileReader;

    invoke-direct {v9, v6}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-object v8, v0

    .line 78
    :try_start_1
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "val":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 80
    .local v10, "beforeTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-object v5, v8

    .end local v8    # "br":Ljava/io/BufferedReader;
    .local v5, "br":Ljava/io/BufferedReader;
    move-wide/from16 v8, v16

    .line 81
    .local v8, "current":J
    :try_start_2
    invoke-static {}, Landroid/app/TaskSnapshotHelperStub;->get()Landroid/app/TaskSnapshotHelperStub;

    move-result-object v16
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v17, v6

    .end local v6    # "file":Ljava/io/File;
    .local v17, "file":Ljava/io/File;
    move-object/from16 v6, v16

    move-object v15, v7

    move-object v7, v12

    :try_start_3
    invoke-interface/range {v6 .. v11}, Landroid/app/TaskSnapshotHelperStub;->checkExpired(Ljava/lang/String;JJ)Z

    move-result v6
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v6, :cond_6

    if-eqz v4, :cond_5

    goto :goto_1

    .line 88
    .end local v0    # "val":Ljava/lang/String;
    .end local v8    # "current":J
    .end local v10    # "beforeTime":J
    :cond_5
    invoke-virtual {v1, v5}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V

    .line 89
    move-object v8, v5

    goto/16 :goto_3

    .line 82
    .restart local v0    # "val":Ljava/lang/String;
    .restart local v8    # "current":J
    .restart local v10    # "beforeTime":J
    :cond_6
    :goto_1
    :try_start_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "couldPersist()...return true (expired) forceUpdate = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v15, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 83
    nop

    .line 88
    invoke-virtual {v1, v5}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V

    .line 83
    const/4 v6, 0x1

    return v6

    .line 88
    .end local v0    # "val":Ljava/lang/String;
    .end local v8    # "current":J
    .end local v10    # "beforeTime":J
    :catchall_0
    move-exception v0

    move-object v8, v5

    goto :goto_4

    .line 85
    :catch_0
    move-exception v0

    move-object v8, v5

    goto :goto_2

    .line 88
    .end local v17    # "file":Ljava/io/File;
    .restart local v6    # "file":Ljava/io/File;
    :catchall_1
    move-exception v0

    move-object/from16 v17, v6

    move-object v8, v5

    .end local v6    # "file":Ljava/io/File;
    .restart local v17    # "file":Ljava/io/File;
    goto :goto_4

    .line 85
    .end local v17    # "file":Ljava/io/File;
    .restart local v6    # "file":Ljava/io/File;
    :catch_1
    move-exception v0

    move-object/from16 v17, v6

    move-object v15, v7

    move-object v8, v5

    .end local v6    # "file":Ljava/io/File;
    .restart local v17    # "file":Ljava/io/File;
    goto :goto_2

    .line 88
    .end local v5    # "br":Ljava/io/BufferedReader;
    .end local v17    # "file":Ljava/io/File;
    .restart local v6    # "file":Ljava/io/File;
    .local v8, "br":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v0

    move-object/from16 v17, v6

    move-object v5, v8

    .end local v6    # "file":Ljava/io/File;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "br":Ljava/io/BufferedReader;
    .restart local v17    # "file":Ljava/io/File;
    goto :goto_4

    .line 85
    .end local v5    # "br":Ljava/io/BufferedReader;
    .end local v17    # "file":Ljava/io/File;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_2
    move-exception v0

    move-object/from16 v17, v6

    move-object v15, v7

    move-object v5, v8

    .end local v6    # "file":Ljava/io/File;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "br":Ljava/io/BufferedReader;
    .restart local v17    # "file":Ljava/io/File;
    goto :goto_2

    .line 88
    .end local v5    # "br":Ljava/io/BufferedReader;
    .end local v17    # "file":Ljava/io/File;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catchall_3
    move-exception v0

    move-object/from16 v17, v6

    .end local v6    # "file":Ljava/io/File;
    .restart local v17    # "file":Ljava/io/File;
    goto :goto_4

    .line 85
    .end local v17    # "file":Ljava/io/File;
    .restart local v6    # "file":Ljava/io/File;
    :catch_3
    move-exception v0

    move-object/from16 v17, v6

    move-object v15, v7

    .line 86
    .end local v6    # "file":Ljava/io/File;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v17    # "file":Ljava/io/File;
    :goto_2
    :try_start_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "couldPersist()...exception:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v15, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 88
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v1, v8}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V

    .line 89
    nop

    .line 90
    :goto_3
    const/4 v5, 0x0

    return v5

    .line 88
    :catchall_4
    move-exception v0

    :goto_4
    invoke-virtual {v1, v8}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V

    .line 89
    throw v0

    .line 53
    .end local v8    # "br":Ljava/io/BufferedReader;
    .end local v12    # "pkg":Ljava/lang/String;
    .end local v13    # "context":Landroid/content/Context;
    .end local v14    # "task":Lcom/android/server/wm/Task;
    .end local v17    # "file":Ljava/io/File;
    :cond_7
    :goto_5
    const/4 v5, 0x0

    return v5
.end method

.method public createDirectory(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;I)Z
    .locals 3
    .param p1, "resolver"    # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;
    .param p2, "userId"    # I

    .line 126
    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getDirectoryQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;I)Ljava/io/File;

    move-result-object v0

    .line 127
    .local v0, "dirQS":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    const-string v1, "TaskSnapshot_PInjector"

    const-string v2, "Fail to create dir!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/4 v1, 0x0

    return v1

    .line 131
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public getBitmapFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;Z)Ljava/io/File;
    .locals 4
    .param p1, "resolver"    # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;
    .param p2, "userId"    # I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "lowRes"    # Z

    .line 154
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getDirectoryQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;I)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 155
    if-eqz p4, :cond_0

    const-string v3, "_reduced"

    goto :goto_0

    :cond_0
    const-string v3, ""

    :goto_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 154
    return-object v0
.end method

.method public getBitmapFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;Landroid/window/TaskSnapshot;IZ)Ljava/io/File;
    .locals 1
    .param p1, "resolver"    # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;
    .param p2, "snapshot"    # Landroid/window/TaskSnapshot;
    .param p3, "userId"    # I
    .param p4, "lowRes"    # Z

    .line 147
    invoke-virtual {p0, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->checkName(Landroid/window/TaskSnapshot;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p3, v0, p4}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getBitmapFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getDirectoryQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;I)Ljava/io/File;
    .locals 3
    .param p1, "resolver"    # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;
    .param p2, "userId"    # I

    .line 174
    new-instance v0, Ljava/io/File;

    invoke-interface {p1, p2}, Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;->getSystemDirectoryForUser(I)Ljava/io/File;

    move-result-object v1

    const-string v2, "qs_snapshots"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public getProtoFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;)Ljava/io/File;
    .locals 4
    .param p1, "resolver"    # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;
    .param p2, "userId"    # I
    .param p3, "name"    # Ljava/lang/String;

    .line 168
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getDirectoryQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;I)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".proto"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public writeProto(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;Landroid/window/TaskSnapshot;I)Z
    .locals 5
    .param p1, "resolver"    # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;
    .param p2, "snapshot"    # Landroid/window/TaskSnapshot;
    .param p3, "userId"    # I

    .line 104
    const/4 v0, 0x0

    .line 106
    .local v0, "bw":Ljava/io/BufferedWriter;
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->checkName(Landroid/window/TaskSnapshot;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, p3, v1}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getProtoFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 107
    .local v1, "file":Ljava/io/File;
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    invoke-direct {v3, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object v0, v2

    .line 108
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    .end local v1    # "file":Ljava/io/File;
    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V

    .line 113
    goto :goto_1

    .line 112
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 109
    :catch_0
    move-exception v1

    .line 110
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "TaskSnapshot_PInjector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to open for proto write. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    nop

    .end local v1    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 114
    :goto_1
    const/4 v1, 0x1

    return v1

    .line 112
    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V

    .line 113
    throw v1
.end method
