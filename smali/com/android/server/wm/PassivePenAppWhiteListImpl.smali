.class public Lcom/android/server/wm/PassivePenAppWhiteListImpl;
.super Ljava/lang/Object;
.source "PassivePenAppWhiteListImpl.java"


# static fields
.field private static final NOT_WHITE_LIST_TOP_APP:I = 0x0

.field private static final PASSIVE_PEN_APP_WHILTE_LIST:Ljava/lang/String; = "whiteList"

.field private static final PASSIVE_PEN_MODE_ENABLED:Ljava/lang/Boolean;

.field private static final PASSIVE_PEN_MODULE_NAME:Ljava/lang/String; = "passivepen"

.field public static final TAG:Ljava/lang/String; = "PassivePenAppWhiteListImpl"

.field private static final URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

.field private static final WHITE_LIST_TOP_APP:I = 0x1

.field private static mPassivePenImpl:Lcom/android/server/wm/PassivePenAppWhiteListImpl;


# instance fields
.field private codeWhiteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultWhiteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private lastWhiteListTopApp:I

.field private mAppObserver:Lmiui/process/IForegroundInfoListener$Stub;

.field public mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

.field public mContext:Landroid/content/Context;

.field mKeyguardStateReceiver:Landroid/content/BroadcastReceiver;

.field protected splitMode:Z

.field private whiteListTopApp:I


# direct methods
.method static bridge synthetic -$$Nest$fgetwhiteListTopApp(Lcom/android/server/wm/PassivePenAppWhiteListImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I

    return p0
.end method

.method static bridge synthetic -$$Nest$mreadLocalCloudControlData(Lcom/android/server/wm/PassivePenAppWhiteListImpl;Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->readLocalCloudControlData(Landroid/content/ContentResolver;Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 46
    nop

    .line 47
    const-string v0, "ro.miui.passive_pen.enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 46
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->PASSIVE_PEN_MODE_ENABLED:Ljava/lang/Boolean;

    .line 48
    nop

    .line 49
    const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    .line 48
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->defaultWhiteList:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->codeWhiteList:Ljava/util/ArrayList;

    .line 140
    new-instance v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl$1;-><init>(Lcom/android/server/wm/PassivePenAppWhiteListImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mKeyguardStateReceiver:Landroid/content/BroadcastReceiver;

    .line 165
    new-instance v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl$2;-><init>(Lcom/android/server/wm/PassivePenAppWhiteListImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mAppObserver:Lmiui/process/IForegroundInfoListener$Stub;

    .line 57
    return-void
.end method

.method public static getInstance()Lcom/android/server/wm/PassivePenAppWhiteListImpl;
    .locals 1

    .line 51
    sget-object v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mPassivePenImpl:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    invoke-direct {v0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;-><init>()V

    sput-object v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mPassivePenImpl:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    .line 54
    :cond_0
    sget-object v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mPassivePenImpl:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    return-object v0
.end method

.method private getPassivePenAppWhiteListFromXml()V
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->defaultWhiteList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1103004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 83
    return-void
.end method

.method private readLocalCloudControlData(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 6
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "moduleName"    # Ljava/lang/String;

    .line 189
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "PassivePenAppWhiteListImpl"

    if-eqz v0, :cond_0

    .line 190
    const-string v0, "moduleName can not be null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    return-void

    .line 194
    :cond_0
    :try_start_0
    const-string/jumbo v0, "whiteList"

    const/4 v2, 0x0

    invoke-static {p1, p2, v0, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "data":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 196
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 197
    .local v2, "apps":Lorg/json/JSONArray;
    iget-object v3, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->codeWhiteList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 198
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 199
    iget-object v4, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->codeWhiteList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 201
    .end local v3    # "i":I
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud data for listpassivepen "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->codeWhiteList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    .end local v0    # "data":Ljava/lang/String;
    .end local v2    # "apps":Lorg/json/JSONArray;
    :cond_2
    goto :goto_1

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Exception when readLocalCloudControlData  :"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 206
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private registerCloudControlObserver(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 3
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "moduleName"    # Ljava/lang/String;

    .line 214
    sget-object v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    new-instance v1, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;-><init>(Lcom/android/server/wm/PassivePenAppWhiteListImpl;Landroid/os/Handler;Landroid/content/ContentResolver;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 222
    return-void
.end method

.method private registerKeyguardReceiver()V
    .locals 3

    .line 73
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 74
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 75
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 76
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 77
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mKeyguardStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 79
    return-void
.end method


# virtual methods
.method blackAppAndWhiteAppChanged(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 124
    iget v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->lastWhiteListTopApp:I

    iget v1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I

    if-ne v0, v1, :cond_0

    .line 125
    const/4 v0, 0x0

    return v0

    .line 127
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method dispatchTopAppWhiteState(I)Z
    .locals 5
    .param p1, "value"    # I

    .line 98
    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v1

    .line 99
    .local v1, "touchFeature":Lmiui/util/ITouchFeature;
    if-eqz v1, :cond_0

    .line 100
    const-string v2, "PassivePenAppWhiteListImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " dispatchTopAppWhiteState value = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2, p1}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 105
    .end local v1    # "touchFeature":Lmiui/util/ITouchFeature;
    :cond_0
    goto :goto_0

    .line 103
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 106
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method dispatchTopAppWhiteState(III)Z
    .locals 2
    .param p1, "touchId"    # I
    .param p2, "mode"    # I
    .param p3, "value"    # I

    .line 87
    :try_start_0
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v0

    .line 88
    .local v0, "touchFeature":Lmiui/util/ITouchFeature;
    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {v0, p1, p2, p3}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 93
    .end local v0    # "touchFeature":Lmiui/util/ITouchFeature;
    :cond_0
    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 94
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method init(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/content/Context;)V
    .locals 1
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 59
    sget-object v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->PASSIVE_PEN_MODE_ENABLED:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iput-object p1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 61
    iput-object p2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mContext:Landroid/content/Context;

    .line 63
    :cond_0
    return-void
.end method

.method onSplitScreenExit()V
    .locals 2

    .line 133
    sget-object v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->PASSIVE_PEN_MODE_ENABLED:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    iget v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 135
    invoke-virtual {p0, v1}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->dispatchTopAppWhiteState(I)Z

    .line 137
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->splitMode:Z

    .line 139
    :cond_1
    return-void
.end method

.method onSystemReady()V
    .locals 2

    .line 65
    sget-object v0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->PASSIVE_PEN_MODE_ENABLED:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mAppObserver:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 67
    invoke-direct {p0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->getPassivePenAppWhiteListFromXml()V

    .line 68
    invoke-direct {p0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->registerKeyguardReceiver()V

    .line 69
    iget-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "passivepen"

    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->registerCloudControlObserver(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 71
    :cond_0
    return-void
.end method

.method protected whiteListApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .line 109
    iget v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I

    iput v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->lastWhiteListTopApp:I

    .line 110
    iget-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->defaultWhiteList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->codeWhiteList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 114
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I

    .line 115
    return v0

    .line 111
    :cond_1
    :goto_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListTopApp:I

    .line 112
    return v0
.end method
