.class public Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;
.super Ljava/lang/Object;
.source "OneTrackDragDropHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mClipData:Landroid/content/ClipData;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mDragWindow:Lcom/android/server/wm/WindowState;

.field private final mDropWindow:Lcom/android/server/wm/WindowState;

.field private final mResult:Z


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;ZLandroid/content/ClipData;Landroid/content/ContentResolver;)V
    .locals 0
    .param p1, "dragWindow"    # Lcom/android/server/wm/WindowState;
    .param p2, "dropWindow"    # Lcom/android/server/wm/WindowState;
    .param p3, "result"    # Z
    .param p4, "clipData"    # Landroid/content/ClipData;
    .param p5, "contentResolver"    # Landroid/content/ContentResolver;

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p1, p0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mDragWindow:Lcom/android/server/wm/WindowState;

    .line 128
    iput-object p2, p0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mDropWindow:Lcom/android/server/wm/WindowState;

    .line 129
    iput-boolean p3, p0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mResult:Z

    .line 130
    iput-object p4, p0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mClipData:Landroid/content/ClipData;

    .line 131
    iput-object p5, p0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mContentResolver:Landroid/content/ContentResolver;

    .line 132
    return-void
.end method


# virtual methods
.method public build()Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;
    .locals 16

    .line 135
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mDragWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mDragWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "dragPackage":Ljava/lang/String;
    :goto_0
    iget-object v3, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mDropWindow:Lcom/android/server/wm/WindowState;

    if-nez v3, :cond_1

    :goto_1
    goto :goto_2

    :cond_1
    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    iget-object v2, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mDropWindow:Lcom/android/server/wm/WindowState;

    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "dropPackage":Ljava/lang/String;
    :goto_2
    iget-boolean v3, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mResult:Z

    if-eqz v3, :cond_3

    const-string/jumbo v3, "\u6210\u529f"

    goto :goto_3

    :cond_3
    const-string/jumbo v3, "\u5931\u8d25"

    .line 140
    .local v3, "result":Ljava/lang/String;
    :goto_3
    const/4 v4, 0x0

    .local v4, "hasText":Z
    const/4 v5, 0x0

    .local v5, "hasImage":Z
    const/4 v6, 0x0

    .line 141
    .local v6, "hasFile":Z
    const/4 v7, 0x0

    .local v7, "i":I
    iget-object v8, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mClipData:Landroid/content/ClipData;

    if-nez v8, :cond_4

    const/4 v8, 0x0

    goto :goto_4

    :cond_4
    invoke-virtual {v8}, Landroid/content/ClipData;->getItemCount()I

    move-result v8

    .local v8, "count":I
    :goto_4
    const-string v10, "image/*"

    if-ge v7, v8, :cond_c

    .line 142
    if-eqz v4, :cond_5

    if-eqz v5, :cond_5

    if-eqz v6, :cond_5

    .line 143
    goto :goto_7

    .line 145
    :cond_5
    iget-object v11, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mClipData:Landroid/content/ClipData;

    invoke-virtual {v11, v7}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v11

    .line 146
    .local v11, "item":Landroid/content/ClipData$Item;
    invoke-virtual {v11}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v12

    if-nez v12, :cond_6

    invoke-virtual {v11}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_7

    .line 147
    :cond_6
    const/4 v4, 0x1

    .line 149
    :cond_7
    invoke-virtual {v11}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v12

    .line 150
    .local v12, "uri":Landroid/net/Uri;
    if-eqz v12, :cond_b

    .line 151
    const/4 v13, 0x0

    .line 152
    .local v13, "mimeType":Ljava/lang/String;
    const-string v14, "content"

    invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 153
    iget-object v14, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v14, v12}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v13

    goto :goto_5

    .line 154
    :cond_8
    const-string v14, "file"

    invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 155
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v14

    new-instance v15, Ljava/io/File;

    .line 157
    invoke-virtual {v12}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v15, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    invoke-static {v15}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    .line 157
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 156
    invoke-static {v9}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 155
    invoke-virtual {v14, v9}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 159
    :cond_9
    :goto_5
    if-eqz v13, :cond_b

    .line 160
    invoke-static {v13, v10}, Landroid/content/ClipDescription;->compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 161
    const/4 v5, 0x1

    goto :goto_6

    .line 163
    :cond_a
    const/4 v6, 0x1

    .line 141
    .end local v11    # "item":Landroid/content/ClipData$Item;
    .end local v12    # "uri":Landroid/net/Uri;
    .end local v13    # "mimeType":Ljava/lang/String;
    :cond_b
    :goto_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 168
    .end local v7    # "i":I
    .end local v8    # "count":I
    :cond_c
    :goto_7
    const/4 v7, 0x0

    .restart local v7    # "i":I
    iget-object v8, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mClipData:Landroid/content/ClipData;

    if-nez v8, :cond_d

    const/4 v9, 0x0

    goto :goto_8

    :cond_d
    invoke-virtual {v8}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ClipDescription;->getMimeTypeCount()I

    move-result v9

    :goto_8
    move v8, v9

    .restart local v8    # "count":I
    :goto_9
    if-ge v7, v8, :cond_11

    .line 169
    if-eqz v4, :cond_e

    if-eqz v5, :cond_e

    .line 170
    goto :goto_b

    .line 172
    :cond_e
    iget-object v9, v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->mClipData:Landroid/content/ClipData;

    invoke-virtual {v9}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v9

    .line 173
    .local v9, "mimeType":Ljava/lang/String;
    const-string/jumbo v11, "text/*"

    invoke-static {v9, v11}, Landroid/content/ClipDescription;->compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 174
    const/4 v4, 0x1

    goto :goto_a

    .line 175
    :cond_f
    invoke-static {v9, v10}, Landroid/content/ClipDescription;->compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 176
    const/4 v5, 0x1

    .line 168
    .end local v9    # "mimeType":Ljava/lang/String;
    :cond_10
    :goto_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    .line 179
    .end local v7    # "i":I
    .end local v8    # "count":I
    :cond_11
    :goto_b
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 180
    .local v7, "contentStyles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v4, :cond_12

    const-string/jumbo v8, "\u6587\u5b57"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    :cond_12
    if-eqz v5, :cond_13

    const-string/jumbo v8, "\u56fe\u7247"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    :cond_13
    if-eqz v6, :cond_14

    const-string/jumbo v8, "\u6587\u4ef6"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_14
    const-string v8, ","

    invoke-static {v8, v7}, Ljava/lang/String;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    .line 185
    .local v8, "contentStyle":Ljava/lang/String;
    new-instance v9, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;

    invoke-direct {v9, v1, v2, v3, v8}, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v9
.end method
