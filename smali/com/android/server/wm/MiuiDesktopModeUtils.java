public class com.android.server.wm.MiuiDesktopModeUtils {
	 /* .source "MiuiDesktopModeUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean IS_SUPPORTED;
private static final java.lang.String TAG;
private static Boolean mDesktopOn;
/* # direct methods */
static Boolean -$$Nest$sfgetmDesktopOn ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z */
} // .end method
static void -$$Nest$sfputmDesktopOn ( Boolean p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.android.server.wm.MiuiDesktopModeUtils.mDesktopOn = (p0!= 0);
	 return;
} // .end method
static com.android.server.wm.MiuiDesktopModeUtils ( ) {
	 /* .locals 2 */
	 /* .line 34 */
	 final String v0 = "ro.config.miui_desktop_mode_enabled"; // const-string v0, "ro.config.miui_desktop_mode_enabled"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.wm.MiuiDesktopModeUtils.IS_SUPPORTED = (v0!= 0);
	 /* .line 37 */
	 com.android.server.wm.MiuiDesktopModeUtils.mDesktopOn = (v1!= 0);
	 return;
} // .end method
public com.android.server.wm.MiuiDesktopModeUtils ( ) {
	 /* .locals 0 */
	 /* .line 30 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
static void initDesktop ( android.content.Context p0 ) {
	 /* .locals 1 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 40 */
	 v0 = 	 com.android.server.wm.MiuiDesktopModeUtils .isEnabled ( );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 41 */
		 v0 = 		 com.android.server.wm.MiuiDesktopModeUtils .isActive ( p0 );
		 com.android.server.wm.MiuiDesktopModeUtils.mDesktopOn = (v0!= 0);
		 /* .line 42 */
		 /* new-instance v0, Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver; */
		 /* invoke-direct {v0}, Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver;-><init>()V */
		 /* .line 43 */
		 /* .local v0, "mSettingsObserver":Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver; */
		 (( com.android.server.wm.MiuiDesktopModeUtils$SettingsObserver ) v0 ).observe ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver;->observe(Landroid/content/Context;)V
		 /* .line 45 */
	 } // .end local v0 # "mSettingsObserver":Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver;
} // :cond_0
return;
} // .end method
public static Boolean isActive ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 51 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->IS_SUPPORTED:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 52 */
com.android.server.wm.MiuiDesktopModeUtils.mDesktopOn = (v1!= 0);
/* .line 53 */
/* .line 57 */
} // :cond_0
try { // :try_start_0
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "miui_dkt_mode"; // const-string v2, "miui_dkt_mode"
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3 );
/* .line 59 */
/* .local v0, "result":I */
if ( v0 != null) { // if-eqz v0, :cond_1
	 int v2 = 1; // const/4 v2, 0x1
} // :cond_1
/* move v2, v1 */
} // :goto_0
com.android.server.wm.MiuiDesktopModeUtils.mDesktopOn = (v2!= 0);
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 60 */
/* .line 61 */
} // .end local v0 # "result":I
/* :catch_0 */
/* move-exception v0 */
/* .line 62 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to read MIUI_DESKTOP_MODE settings "; // const-string v3, "Failed to read MIUI_DESKTOP_MODE settings "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiDesktopModeUtils"; // const-string v3, "MiuiDesktopModeUtils"
android.util.Slog .d ( v3,v2 );
/* .line 63 */
com.android.server.wm.MiuiDesktopModeUtils.mDesktopOn = (v1!= 0);
/* .line 64 */
} // .end method
public static Boolean isDesktopActive ( ) {
/* .locals 1 */
/* .line 68 */
v0 = com.android.server.wm.MiuiDesktopModeUtils .isEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean isEnabled ( ) {
/* .locals 1 */
/* .line 47 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->IS_SUPPORTED:Z */
} // .end method
