.class Lcom/android/server/wm/MiuiFreeformTrackManager$16;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiFreeformTrackManager;->trackMiniWindowPinedQuitEvent(Ljava/lang/String;Ljava/lang/String;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

.field final synthetic val$applicationName:Ljava/lang/String;

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$pinedDuration:F


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;F)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiFreeformTrackManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 580
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iput-object p2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->val$applicationName:Ljava/lang/String;

    iput p4, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->val$pinedDuration:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 582
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mFreeFormTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 584
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 585
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 586
    .local v1, "jsonData":Lorg/json/JSONObject;
    const-string/jumbo v2, "tip"

    const-string v3, "621.2.1.1.21752"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 587
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v2, v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$mputCommomParam(Lcom/android/server/wm/MiuiFreeformTrackManager;Lorg/json/JSONObject;)V

    .line 588
    const-string v2, "EVENT_NAME"

    const-string v3, "quit_hidden_window"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 589
    const-string v2, "app_package_name"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->val$packageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 590
    const-string v2, "app_display_name"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->val$applicationName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 591
    const-string/jumbo v2, "use_duration"

    iget v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->val$pinedDuration:F

    float-to-double v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 592
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$16;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v2}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v2

    const-string v3, "31000000538"

    const-string v4, "android"

    .line 593
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    .line 592
    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 597
    .end local v1    # "jsonData":Lorg/json/JSONObject;
    :cond_0
    goto :goto_0

    .line 598
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 595
    :catch_0
    move-exception v1

    .line 596
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 598
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v0

    .line 599
    return-void

    .line 598
    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
