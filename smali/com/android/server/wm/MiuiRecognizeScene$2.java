class com.android.server.wm.MiuiRecognizeScene$2 implements android.content.ServiceConnection {
	 /* .source "MiuiRecognizeScene.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiRecognizeScene; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiRecognizeScene this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiRecognizeScene$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiRecognizeScene; */
/* .line 104 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "arg0" # Landroid/content/ComponentName; */
/* .param p2, "arg1" # Landroid/os/IBinder; */
/* .line 116 */
v0 = this.this$0;
com.xiaomi.joyose.smartop.gamebooster.scenerecognize.IPhashSceneRecognizeInterface$Stub .asInterface ( p2 );
com.android.server.wm.MiuiRecognizeScene .-$$Nest$fputmRecognizeService ( v0,v1 );
/* .line 117 */
v0 = this.this$0;
com.android.server.wm.MiuiRecognizeScene .-$$Nest$fgetmBgHandler ( v0 );
v1 = this.this$0;
com.android.server.wm.MiuiRecognizeScene .-$$Nest$fgetmBindServiceRunnable ( v1 );
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 120 */
try { // :try_start_0
	 v0 = this.this$0;
	 com.android.server.wm.MiuiRecognizeScene .-$$Nest$fgetmRecognizeService ( v0 );
	 v1 = this.this$0;
	 com.android.server.wm.MiuiRecognizeScene .-$$Nest$fgetmDeathHandler ( v1 );
	 int v2 = 0; // const/4 v2, 0x0
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 123 */
	 /* .line 121 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 122 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 (( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
	 /* .line 124 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "arg0" # Landroid/content/ComponentName; */
/* .line 108 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.wm.MiuiRecognizeScene .-$$Nest$fputmRecognizeService ( v0,v1 );
/* .line 109 */
v0 = this.this$0;
com.android.server.wm.MiuiRecognizeScene .-$$Nest$fgetmContext ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 110 */
v0 = this.this$0;
com.android.server.wm.MiuiRecognizeScene .-$$Nest$fgetmContext ( v0 );
v1 = this.this$0;
com.android.server.wm.MiuiRecognizeScene .-$$Nest$fgetmPerformanceConnection ( v1 );
(( android.content.Context ) v0 ).unbindService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 112 */
} // :cond_0
return;
} // .end method
