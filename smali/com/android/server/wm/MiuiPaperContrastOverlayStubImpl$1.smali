.class Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;
.super Landroid/database/ContentObserver;
.source "MiuiPaperContrastOverlayStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->registerObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 96
    iput-object p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 99
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->-$$Nest$mgetSecurityCenterStatus(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Z

    move-result v0

    .line 100
    .local v0, "textureEyecareEnable":Z
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->-$$Nest$fgetmTextureEyeCareLevelUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->-$$Nest$fgetmPaperModeTypeUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->-$$Nest$fgetmPaperModeEnableUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;

    move-result-object v1

    .line 101
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->-$$Nest$fgetmSecurityModeVtbUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->-$$Nest$fgetmSecurityModeGbUri(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;)Landroid/net/Uri;

    move-result-object v1

    .line 102
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 109
    :cond_0
    return-void

    .line 103
    :cond_1
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerObserver uri:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", enable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPaperContrastOverlayStubImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->-$$Nest$fputmFoldDeviceReady(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Z)V

    .line 106
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->updateTextureEyeCareLevel(Z)V

    .line 107
    return-void
.end method
