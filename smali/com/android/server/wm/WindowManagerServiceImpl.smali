.class public Lcom/android/server/wm/WindowManagerServiceImpl;
.super Ljava/lang/Object;
.source "WindowManagerServiceImpl.java"

# interfaces
.implements Lcom/android/server/wm/WindowManagerServiceStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/WindowManagerServiceImpl$WindowOpChangedListener;,
        Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;
    }
.end annotation


# static fields
.field private static final BACKGROUND_BLUR_STATUS_DEFAULT:Z

.field private static final BACKGROUND_BLUR_SUPPORTED:Z

.field private static final BUILD_DENSITY_PROP:Ljava/lang/String; = "ro.sf.lcd_density"

.field private static final CARWITH_PACKAGE_NAME:Ljava/lang/String; = "com.miui.carlink"

.field private static final CARWITH_TRUSTED_DAILY_SIG:Ljava/lang/String; = "c8a2e9bccf597c2fb6dc66bee293fc13f2fc47ec77bc6b2b0d52c11f51192ab8"

.field private static final CARWITH_TRUSTED_NON_DAILY_SIG:Ljava/lang/String; = "c9009d01ebf9f5d0302bc71b2fe9aa9a47a432bba17308a3111b75d7b2149025"

.field private static final CIRCULATE:Ljava/lang/String; = "home_worldCirculate_and_smallWindow_crop"

.field private static final CIRCULATEACTIVITY:Ljava/lang/String; = "com.miui.circulate.world.AppCirculateActivity"

.field private static final CLIENT_DIMMER_NAME:Ljava/lang/String; = "ClientDimmerForAppPair"

.field private static CUR_DEVICE:Ljava/lang/String; = null

.field public static FIND_DEVICE:Ljava/lang/String; = null

.field private static FORCE_ORI_DEVICES_LIST:[Ljava/lang/String; = null

.field private static FORCE_ORI_LIST:[Ljava/lang/String; = null

.field public static GOOGLE:Ljava/lang/String; = null

.field public static GOOGLE_FLOATING:Ljava/lang/String; = null

.field private static final INVALID_APPEARANCE:I = -0x1

.field private static final LAUNCHER:Ljava/lang/String; = "com.miui.home/.launcher.Launcher"

.field private static final MIUI_RESOLUTION:Ljava/lang/String; = "persist.sys.miui_resolution"

.field public static MM:Ljava/lang/String; = null

.field public static MM_FLOATING:Ljava/lang/String; = null

.field public static final MSG_SERVICE_INIT_AFTER_BOOT:I = 0x6b

.field public static final MSG_UPDATE_BLUR_WALLPAPER:I = 0x6a

.field public static QQ:Ljava/lang/String; = null

.field public static QQ_FLOATING:Ljava/lang/String; = null

.field public static final SCREEN_SHARE_PROTECTION_OPEN:I = 0x1

.field public static final SCREEN_SHARE_PROTECTION_WITH_BLUR:I = 0x1

.field public static SECURITY:Ljava/lang/String; = null

.field public static SECURITY_FLOATING:Ljava/lang/String; = null

.field private static final SHA_256:Ljava/lang/String; = "SHA-256"

.field private static final SUPPORT_UNION_POWER_CORE:Z

.field private static final SUPPPORT_CLOUD_DIM:Z

.field private static final TAG:Ljava/lang/String; = "WindowManagerService"

.field private static final VIRTUAL_CAMERA_BOUNDARY:I = 0x64

.field private static mFloatWindowMirrorWhiteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mProjectionBlackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field HAS_SCREEN_SHOT:Z

.field IS_CTS_MODE:Z

.field isFpClientOn:Z

.field private leftAppearance:I

.field private leftStatusBounds:Landroid/graphics/Rect;

.field mAppOps:Landroid/app/AppOpsManager;

.field mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

.field private final mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

.field private mBlackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBlurWallpaperBmp:Landroid/graphics/Bitmap;

.field private mBootCompletedReceiver:Landroid/content/BroadcastReceiver;

.field private mCameraManager:Landroid/hardware/camera2/CameraManager;

.field mContext:Landroid/content/Context;

.field private final mContrastAlphaUri:Landroid/net/Uri;

.field private final mDarkModeContrastEnable:Landroid/net/Uri;

.field private final mDarkModeEnable:Landroid/net/Uri;

.field private mDimNeedAssistMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mDimUserTimeoutMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mDisplayContent:Lcom/android/server/wm/DisplayContent;

.field mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

.field private final mFlipListenerLock:Ljava/lang/Object;

.field private mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

.field private mIsResolutionChanged:Z

.field private mLastIsFullScreen:Ljava/lang/Boolean;

.field public mLastWindowHasSecure:Z

.field mMiuiContrastOverlay:Lcom/android/server/wm/MiuiContrastOverlayStub;

.field private mMiuiDisplayDensity:I

.field private mMiuiDisplayHeight:I

.field private mMiuiDisplayWidth:I

.field private mMiuiPaperContrastOverlay:Landroid/view/SurfaceControl;

.field private mMiuiSecurityImeWhiteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mNavigationBarColorListenerMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Landroid/os/IBinder;",
            "Landroid/view/IFlipWindowStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mNavigationColor:I

.field private mOpeningCameraID:Ljava/util/Set;

.field private mPendingSwitchResolution:Z

.field private mPhysicalDensity:I

.field private mPhysicalHeight:I

.field private mPhysicalWidth:I

.field private mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

.field private mRunningRecentsAnimation:Z

.field final mSecureChangeListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/IWindowSecureChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mSecurityInternal:Lmiui/security/SecurityManagerInternal;

.field private mSmartPowerServiceInternal:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

.field private mSplitMode:Z

.field private mSupportActiveModeSwitch:Z

.field private mSupportSwitchResolutionFeature:Z

.field private mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mTransitionReadyList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mUiModeAnimFinishedCallback:Landroid/view/IWindowAnimationFinishedCallback;

.field private mWallpaperCallback:Landroid/app/IWallpaperManagerCallback;

.field mWmService:Lcom/android/server/wm/WindowManagerService;

.field private miuiHoverModeInternal:Lcom/android/server/wm/MiuiHoverModeInternal;

.field private rightAppearance:I

.field private rightStatusBounds:Landroid/graphics/Rect;


# direct methods
.method public static synthetic $r8$lambda$1lnNTwiTZ_nxbKMU1QsrrULku74(Lcom/android/server/wm/WindowManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$updateScreenShareProjectFlag$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$3ghNrU5cJMB_bzbhqECkNZvRDXc(Lcom/android/server/wm/WindowManagerServiceImpl;Lcom/android/server/wm/Task;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$notifySystemBrightnessChange$3(Lcom/android/server/wm/Task;)V

    return-void
.end method

.method public static synthetic $r8$lambda$GFgBx0SKCCw_mYD0oRn4aIBbQlM(Lcom/android/server/wm/WindowManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$notifySystemBrightnessChange$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$S5prd5M3_QmsIlsmsVTm6fHeB2U(Lcom/android/server/wm/WindowManagerServiceImpl;Lcom/android/server/wm/WindowState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$updateScreenShareProjectFlag$1(Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method public static synthetic $r8$lambda$uIIcu0OABV4fP1JVJfM-GkB_XLA(Lcom/android/server/wm/WindowManagerServiceImpl;Lcom/android/server/wm/WindowState;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$notAllowCaptureDisplay$5(Lcom/android/server/wm/WindowState;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$zt8KgAfS7hjr7NVYTjRMsKehgEc(Lcom/android/server/wm/WindowManagerServiceImpl;ZLcom/android/server/wm/WindowState;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$setRoundedCornerOverlaysCanScreenShot$0(ZLcom/android/server/wm/WindowState;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmOpeningCameraID(Lcom/android/server/wm/WindowManagerServiceImpl;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mOpeningCameraID:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misDarkModeContrastEnable(Lcom/android/server/wm/WindowManagerServiceImpl;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isDarkModeContrastEnable()Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mFloatWindowMirrorWhiteList:Ljava/util/ArrayList;

    .line 167
    const-string v1, "com.alibaba.android.rimet"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mFloatWindowMirrorWhiteList:Ljava/util/ArrayList;

    const-string v1, "com.tencent.mm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    const-string v0, "com.tencent.mm/com.tencent.mm.plugin.voip.ui.VideoActivity"

    const-string v2, "com.tencent.mm/com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"

    filled-new-array {v0, v2}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->FORCE_ORI_LIST:[Ljava/lang/String;

    .line 175
    const-string v0, "chiron"

    const-string v2, "polaris"

    const-string v3, "lithium"

    filled-new-array {v3, v0, v2}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->FORCE_ORI_DEVICES_LIST:[Ljava/lang/String;

    .line 176
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->CUR_DEVICE:Ljava/lang/String;

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    .line 179
    sput-object v1, Lcom/android/server/wm/WindowManagerServiceImpl;->MM:Ljava/lang/String;

    .line 180
    const-string v0, "com.tencent.mobileqq"

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->QQ:Ljava/lang/String;

    .line 181
    const-string v0, "com.google.android.dialer"

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->GOOGLE:Ljava/lang/String;

    .line 182
    const-string v0, "com.miui.securitycenter"

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SECURITY:Ljava/lang/String;

    .line 183
    const-string v0, "com.tencent.mm/.FloatingWindow"

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->MM_FLOATING:Ljava/lang/String;

    .line 184
    const-string v0, "com.tencent.mobileqq/.FloatingWindow"

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->QQ_FLOATING:Ljava/lang/String;

    .line 185
    const-string v0, "com.google.android.dialer/.FloatingWindow"

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->GOOGLE_FLOATING:Ljava/lang/String;

    .line 186
    const-string v0, "com.miui.securitycenter/.FloatingWindow"

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SECURITY_FLOATING:Ljava/lang/String;

    .line 187
    const-string v0, "com.xiaomi.finddevice"

    sput-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->FIND_DEVICE:Ljava/lang/String;

    .line 190
    const-string/jumbo v0, "support_cloud_dim"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SUPPPORT_CLOUD_DIM:Z

    .line 193
    const-string v0, "persist.sys.unionpower.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SUPPORT_UNION_POWER_CORE:Z

    .line 282
    const-string v0, "persist.sys.background_blur_supported"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->BACKGROUND_BLUR_SUPPORTED:Z

    .line 284
    const-string v0, "persist.sys.background_blur_status_default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->BACKGROUND_BLUR_STATUS_DEFAULT:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimUserTimeoutMap:Ljava/util/HashMap;

    .line 189
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimNeedAssistMap:Ljava/util/HashMap;

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    .line 207
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    .line 237
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->IS_CTS_MODE:Z

    .line 238
    iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->HAS_SCREEN_SHOT:Z

    .line 239
    iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSplitMode:Z

    .line 241
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mOpeningCameraID:Ljava/util/Set;

    .line 245
    nop

    .line 246
    const-string/jumbo v2, "ui_night_mode"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDarkModeEnable:Landroid/net/Uri;

    .line 249
    nop

    .line 250
    const-string v2, "dark_mode_contrast_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDarkModeContrastEnable:Landroid/net/Uri;

    .line 251
    nop

    .line 252
    const-string v2, "contrast_alpha"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContrastAlphaUri:Landroid/net/Uri;

    .line 279
    iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mLastWindowHasSecure:Z

    .line 299
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftStatusBounds:Landroid/graphics/Rect;

    .line 300
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightStatusBounds:Landroid/graphics/Rect;

    .line 304
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I

    .line 305
    iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I

    .line 309
    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiPaperContrastOverlay:Landroid/view/SurfaceControl;

    .line 312
    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mLastIsFullScreen:Ljava/lang/Boolean;

    .line 314
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    .line 324
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mFlipListenerLock:Ljava/lang/Object;

    .line 327
    new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$1;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiSecurityImeWhiteList:Ljava/util/ArrayList;

    .line 334
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTransitionReadyList:Ljava/util/HashSet;

    .line 484
    new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$4;

    invoke-direct {v0, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$4;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    .line 1241
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecureChangeListeners:Ljava/util/ArrayList;

    return-void
.end method

.method private addShowOnFindDeviceKeyguardAttrsIfNecessary(Landroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V
    .locals 3
    .param p1, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 693
    const-string v0, "com.google.android.dialer"

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 695
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.xiaomi.system.devicelock.locked"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_1

    .line 696
    return-void

    .line 698
    :cond_1
    const/4 v0, -0x1

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 699
    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 700
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    .line 701
    return-void
.end method

.method private adjustFindDeviceAttrs(ILandroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p3, "packageName"    # Ljava/lang/String;

    .line 673
    invoke-direct {p0, p2, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->addShowOnFindDeviceKeyguardAttrsIfNecessary(Landroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V

    .line 675
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->removeFindDeviceKeyguardFlagsIfNecessary(ILandroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V

    .line 676
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->killFreeformForFindDeviceKeyguardIfNecessary(ILandroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V

    .line 677
    return-void
.end method

.method private adjustGoogleAssistantAttrs(Landroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V
    .locals 2
    .param p1, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 562
    const-string v0, "com.google.android.googlequicksearchbox"

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 563
    return-void

    .line 565
    :cond_0
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    .line 566
    return-void
.end method

.method private calcDensityAndUpdateForceDensityIfNeed(I)I
    .locals 5
    .param p1, "desireWidth"    # I

    .line 822
    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getForcedDensity()I

    move-result v0

    .line 823
    .local v0, "density":I
    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isDensityForced()Z

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    if-nez v1, :cond_0

    .line 824
    iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalDensity:I

    mul-int/2addr v1, p1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 825
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-boolean v1, v1, Lcom/android/server/wm/WindowManagerService;->mSystemBooted:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I

    if-eqz v1, :cond_1

    .line 826
    mul-int v3, p1, v0

    int-to-float v3, v3

    mul-float/2addr v3, v2

    int-to-float v1, v1

    div-float/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 827
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    iput v0, v1, Lcom/android/server/wm/DisplayContent;->mBaseDisplayDensity:I

    .line 828
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 830
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v3

    .line 828
    const-string v4, "display_density_forced"

    invoke-static {v1, v4, v2, v3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 832
    :cond_1
    :goto_0
    return v0
.end method

.method private checkDDICSupportAndInitPhysicalSize()V
    .locals 7

    .line 909
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManagerInternal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    move-result-object v0

    .line 910
    .local v0, "defaultDisplayInfo":Landroid/view/DisplayInfo;
    iget-object v2, v0, Landroid/view/DisplayInfo;->supportedModes:[Landroid/view/Display$Mode;

    .line 911
    .local v2, "modes":[Landroid/view/Display$Mode;
    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 912
    .local v4, "mode":Landroid/view/Display$Mode;
    iget v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalHeight()I

    move-result v5

    iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I

    if-eq v5, v6, :cond_0

    .line 913
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportActiveModeSwitch:Z

    .line 915
    :cond_0
    invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalHeight()I

    move-result v5

    iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I

    if-le v5, v6, :cond_1

    .line 916
    invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalWidth()I

    move-result v5

    iput v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I

    .line 917
    invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalHeight()I

    move-result v5

    iput v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I

    .line 911
    .end local v4    # "mode":Landroid/view/Display$Mode;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 920
    :cond_2
    const-string v1, "ro.sf.lcd_density"

    const/16 v3, 0x230

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalDensity:I

    .line 921
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init resolution mSupportActiveModeSwitch:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportActiveModeSwitch:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mPhysicalWidth:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mPhysicalHeight:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mPhysicalDensity:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalDensity:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "WindowManagerService"

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    return-void
.end method

.method private checkIfUnusualWindowEvent(ILjava/lang/String;II)V
    .locals 9
    .param p1, "uid"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .line 1768
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/pm/IPackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1771
    .local v0, "packageName":Ljava/lang/String;
    nop

    .line 1772
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1773
    const-string v1, "com.miui.home"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1774
    const-string v1, "com.android.systemui"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1775
    const-string v1, "android"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2

    .line 1778
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v1, :cond_1

    .line 1779
    const-class v1, Lmiui/security/SecurityManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/security/SecurityManagerInternal;

    iput-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    .line 1781
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-eqz v1, :cond_5

    .line 1782
    const/16 v2, 0x25

    invoke-virtual {v1, v2}, Lmiui/security/SecurityManagerInternal;->getThreshold(I)J

    move-result-wide v7

    .line 1783
    .local v7, "threshold":J
    int-to-long v1, p3

    cmp-long v1, v1, v7

    if-lez v1, :cond_4

    int-to-long v1, p4

    cmp-long v1, v1, v7

    if-gtz v1, :cond_2

    goto :goto_0

    .line 1786
    :cond_2
    iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I

    if-lez v1, :cond_5

    iget v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I

    if-lez v2, :cond_5

    int-to-long v3, p3

    int-to-long v5, v1

    add-long/2addr v5, v7

    cmp-long v1, v3, v5

    if-gez v1, :cond_3

    int-to-long v3, p4

    int-to-long v1, v2

    add-long/2addr v1, v7

    cmp-long v1, v3, v1

    if-ltz v1, :cond_5

    .line 1788
    :cond_3
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    const/16 v2, 0x26

    const-wide/16 v4, 0x1

    move-object v3, v0

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    goto :goto_1

    .line 1784
    :cond_4
    :goto_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    const/16 v2, 0x25

    const-wide/16 v4, 0x1

    move-object v3, v0

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    .line 1792
    .end local v7    # "threshold":J
    :cond_5
    :goto_1
    return-void

    .line 1776
    :cond_6
    :goto_2
    return-void

    .line 1769
    .end local v0    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1770
    .local v0, "e":Ljava/lang/Exception;
    return-void
.end method

.method private createAppearance(Landroid/graphics/Rect;)I
    .locals 9
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .line 1689
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1690
    .local v0, "bitmapBounds":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1691
    .local v1, "intersectBounds":Landroid/graphics/Rect;
    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1693
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    iget v4, v1, Landroid/graphics/Rect;->left:I

    iget v5, v1, Landroid/graphics/Rect;->top:I

    .line 1694
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v7

    .line 1693
    invoke-static {v2, v4, v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1695
    .local v2, "intersectBitmap":Landroid/graphics/Bitmap;
    invoke-direct {p0, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->getAverageColorBitmap(Landroid/graphics/Bitmap;)I

    move-result v4

    .line 1696
    .local v4, "avgColor":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1697
    invoke-static {v4}, Lcom/android/internal/graphics/ColorUtils;->calculateLuminance(I)D

    move-result-wide v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    cmpl-double v5, v5, v7

    if-ltz v5, :cond_0

    const/16 v3, 0x8

    :cond_0
    return v3

    .line 1698
    .end local v2    # "intersectBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "avgColor":I
    :catch_0
    move-exception v2

    .line 1699
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "create appearance fail "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " e = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "WindowManagerService"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1702
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v2, -0x1

    return v2
.end method

.method private enableFullScreen(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 5
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 2064
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    .line 2065
    .local v0, "manager":Landroid/content/pm/IPackageManager;
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->getFlipCompatModeByActivity(Landroid/content/pm/IPackageManager;Landroid/content/ComponentName;)I

    move-result v1

    .line 2066
    .local v1, "activityMode":I
    const/4 v2, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v1, v2, :cond_1

    .line 2067
    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->getFlipCompatModeByApp(Landroid/content/pm/IPackageManager;Ljava/lang/String;)I

    move-result v2

    .line 2068
    .local v2, "appMode":I
    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move v3, v4

    :goto_0
    return v3

    .line 2070
    .end local v2    # "appMode":I
    :cond_1
    if-nez v1, :cond_2

    goto :goto_1

    :cond_2
    move v3, v4

    :goto_1
    return v3
.end method

.method private enableWmsDebugConfig(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "config"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .line 1162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enableWMSDebugConfig, config=:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enable=:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WindowManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1163
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string v0, "DEBUG_UNKNOWN_APP_VISIBILITY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x18

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "DEBUG_INPUT_METHOD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto/16 :goto_1

    :sswitch_2
    const-string v0, "DEBUG_POWER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x13

    goto/16 :goto_1

    :sswitch_3
    const-string v0, "DEBUG_INPUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto/16 :goto_1

    :sswitch_4
    const-string v0, "DEBUG_LAYOUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto/16 :goto_1

    :sswitch_5
    const-string v0, "DEBUG_LAYERS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto/16 :goto_1

    :sswitch_6
    const-string v0, "DEBUG_WINDOW_CROP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x17

    goto/16 :goto_1

    :sswitch_7
    const-string v0, "SHOW_STACK_CRAWLS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16

    goto/16 :goto_1

    :sswitch_8
    const-string v0, "DEBUG_STARTING_WINDOW_VERBOSE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto/16 :goto_1

    :sswitch_9
    const-string v0, "DEBUG_LAYOUT_REPEATS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_1

    :sswitch_a
    const-string v0, "DEBUG_TASK_MOVEMENT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_1

    :sswitch_b
    const-string v0, "DEBUG_CONFIGURATION"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto/16 :goto_1

    :sswitch_c
    const-string v0, "DEBUG_ROOT_TASK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_1

    :sswitch_d
    const-string v0, "SHOW_VERBOSE_TRANSACTIONS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_1

    :sswitch_e
    const-string v0, "DEBUG_DISPLAY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_1

    :sswitch_f
    const-string v0, "DEBUG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto/16 :goto_1

    :sswitch_10
    const-string v0, "DEBUG_SCREENSHOT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    goto :goto_1

    :sswitch_11
    const-string v0, "DEBUG_TASK_POSITIONING"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    goto :goto_1

    :sswitch_12
    const-string v0, "DEBUG_WALLPAPER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_13
    const-string v0, "DEBUG_DRAG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    goto :goto_1

    :sswitch_14
    const-string v0, "DEBUG_ANIM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_15
    const-string v0, "DEBUG_WINDOW_TRACE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe

    goto :goto_1

    :sswitch_16
    const-string v0, "SHOW_LIGHT_TRANSACTIONS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    goto :goto_1

    :sswitch_17
    const-string v0, "DEBUG_WALLPAPER_LIGHT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    goto :goto_1

    :sswitch_18
    const-string v0, "DEBUG_VISIBILITY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 1188
    :pswitch_0
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_UNKNOWN_APP_VISIBILITY:Z

    goto :goto_2

    .line 1187
    :pswitch_1
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_WINDOW_CROP:Z

    goto :goto_2

    .line 1186
    :pswitch_2
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->SHOW_STACK_CRAWLS:Z

    goto :goto_2

    .line 1185
    :pswitch_3
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->SHOW_LIGHT_TRANSACTIONS:Z

    goto :goto_2

    .line 1184
    :pswitch_4
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->SHOW_VERBOSE_TRANSACTIONS:Z

    goto :goto_2

    .line 1183
    :pswitch_5
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_POWER:Z

    goto :goto_2

    .line 1182
    :pswitch_6
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_DISPLAY:Z

    goto :goto_2

    .line 1181
    :pswitch_7
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_ROOT_TASK:Z

    goto :goto_2

    .line 1180
    :pswitch_8
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_TASK_POSITIONING:Z

    goto :goto_2

    .line 1179
    :pswitch_9
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_TASK_MOVEMENT:Z

    goto :goto_2

    .line 1178
    :pswitch_a
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_WINDOW_TRACE:Z

    goto :goto_2

    .line 1177
    :pswitch_b
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_LAYOUT_REPEATS:Z

    goto :goto_2

    .line 1176
    :pswitch_c
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_SCREENSHOT:Z

    goto :goto_2

    .line 1175
    :pswitch_d
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_DRAG:Z

    goto :goto_2

    .line 1174
    :pswitch_e
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_WALLPAPER_LIGHT:Z

    goto :goto_2

    .line 1173
    :pswitch_f
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_WALLPAPER:Z

    goto :goto_2

    .line 1172
    :pswitch_10
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_STARTING_WINDOW_VERBOSE:Z

    goto :goto_2

    .line 1171
    :pswitch_11
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_CONFIGURATION:Z

    goto :goto_2

    .line 1170
    :pswitch_12
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_VISIBILITY:Z

    goto :goto_2

    .line 1169
    :pswitch_13
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_INPUT_METHOD:Z

    goto :goto_2

    .line 1168
    :pswitch_14
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_INPUT:Z

    goto :goto_2

    .line 1167
    :pswitch_15
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_LAYERS:Z

    goto :goto_2

    .line 1166
    :pswitch_16
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_LAYOUT:Z

    goto :goto_2

    .line 1165
    :pswitch_17
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG_ANIM:Z

    goto :goto_2

    .line 1164
    :pswitch_18
    sput-boolean p2, Lcom/android/server/wm/WindowManagerDebugConfig;->DEBUG:Z

    .line 1192
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7caac762 -> :sswitch_18
        -0x644285b3 -> :sswitch_17
        -0x5ff91180 -> :sswitch_16
        -0x541dd15e -> :sswitch_15
        -0x4f0b4363 -> :sswitch_14
        -0x4f09d840 -> :sswitch_13
        -0x44802d2a -> :sswitch_12
        -0x27886d35 -> :sswitch_11
        -0x933a50e -> :sswitch_10
        0x3de9e33 -> :sswitch_f
        0x27030696 -> :sswitch_e
        0x27931d14 -> :sswitch_d
        0x31af5fb6 -> :sswitch_c
        0x37e7a7ca -> :sswitch_b
        0x4796751d -> :sswitch_a
        0x4bab218f -> :sswitch_9
        0x4c2f9be6 -> :sswitch_8
        0x4d5f5ce5 -> :sswitch_7
        0x58185973 -> :sswitch_6
        0x58cd58ce -> :sswitch_5
        0x58cd7eb6 -> :sswitch_4
        0x6e13ae9e -> :sswitch_3
        0x6e76dfd9 -> :sswitch_2
        0x78cf05a2 -> :sswitch_1
        0x7d7e2051 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getAverageColorBitmap(Landroid/graphics/Bitmap;)I
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .line 1741
    const/4 v0, 0x0

    .line 1742
    .local v0, "redColors":I
    const/4 v1, 0x0

    .line 1743
    .local v1, "greenColors":I
    const/4 v2, 0x0

    .line 1744
    .local v2, "blueColors":I
    const/4 v3, 0x0

    .line 1745
    .local v3, "alphaColor":I
    const/4 v4, 0x0

    .line 1747
    .local v4, "pixelCount":I
    const/4 v5, 0x0

    .local v5, "y":I
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 1748
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-ge v6, v7, :cond_0

    .line 1749
    invoke-virtual {p1, v6, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v7

    .line 1750
    .local v7, "c":I
    add-int/lit8 v4, v4, 0x1

    .line 1751
    invoke-static {v7}, Landroid/graphics/Color;->red(I)I

    move-result v8

    add-int/2addr v0, v8

    .line 1752
    invoke-static {v7}, Landroid/graphics/Color;->green(I)I

    move-result v8

    add-int/2addr v1, v8

    .line 1753
    invoke-static {v7}, Landroid/graphics/Color;->blue(I)I

    move-result v8

    add-int/2addr v2, v8

    .line 1754
    invoke-static {v7}, Landroid/graphics/Color;->alpha(I)I

    move-result v8

    add-int/2addr v3, v8

    .line 1748
    .end local v7    # "c":I
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1747
    .end local v6    # "x":I
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1757
    .end local v5    # "y":I
    :cond_1
    div-int v5, v0, v4

    .line 1758
    .local v5, "red":I
    div-int v6, v1, v4

    .line 1759
    .local v6, "green":I
    div-int v7, v2, v4

    .line 1760
    .local v7, "blue":I
    div-int v8, v3, v4

    .line 1761
    .local v8, "alpha":I
    invoke-static {v8, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v9

    return v9
.end method

.method private getDefaultDisplayContent()Lcom/android/server/wm/DisplayContent;
    .locals 2

    .line 1013
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    if-nez v0, :cond_0

    .line 1014
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 1016
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    return-object v0
.end method

.method private getFlipCompatModeByActivity(Landroid/content/pm/IPackageManager;Landroid/content/ComponentName;)I
    .locals 4
    .param p1, "manager"    # Landroid/content/pm/IPackageManager;
    .param p2, "componentName"    # Landroid/content/ComponentName;

    .line 2086
    const/4 v0, 0x0

    .line 2088
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    nop

    .line 2089
    :try_start_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 2088
    const-wide/16 v2, 0x80

    invoke-interface {p1, p2, v2, v3, v1}, Landroid/content/pm/IPackageManager;->getActivityInfo(Landroid/content/ComponentName;JI)Landroid/content/pm/ActivityInfo;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 2092
    goto :goto_0

    .line 2090
    :catch_0
    move-exception v1

    .line 2091
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "WindowManagerService"

    const-string v3, "getActivityInfo fail"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2093
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-direct {p0, v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getFullScreenValue(Landroid/content/pm/PackageItemInfo;)I

    move-result v1

    return v1
.end method

.method private getFlipCompatModeByApp(Landroid/content/pm/IPackageManager;Ljava/lang/String;)I
    .locals 4
    .param p1, "manager"    # Landroid/content/pm/IPackageManager;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 2075
    const/4 v0, 0x0

    .line 2077
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    nop

    .line 2078
    :try_start_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 2077
    const-wide/16 v2, 0x80

    invoke-interface {p1, p2, v2, v3, v1}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 2081
    goto :goto_0

    .line 2079
    :catch_0
    move-exception v1

    .line 2080
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "WindowManagerService"

    const-string v3, "getApplicationInfo fail"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2082
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-direct {p0, v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getFullScreenValue(Landroid/content/pm/PackageItemInfo;)I

    move-result v1

    return v1
.end method

.method private getFullScreenValue(Landroid/content/pm/PackageItemInfo;)I
    .locals 2
    .param p1, "info"    # Landroid/content/pm/PackageItemInfo;

    .line 2097
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    const-string v1, "miui.supportFlipFullScreen"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2099
    iget-object v0, p1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 2101
    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public static getInstance()Lcom/android/server/wm/WindowManagerServiceImpl;
    .locals 1

    .line 352
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerServiceImpl;

    return-object v0
.end method

.method public static getLastFrame(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 1069
    const-string v0, "Splash Screen com.android.incallui"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1070
    const-string v0, "com.android.incallui/com.android.incallui.InCallActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1071
    const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.AVActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1072
    const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.AVLoadingDialogActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1073
    const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1074
    const-string v0, "Splash Screen com.tencent.mm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1075
    const-string v0, "com.tencent.mm/com.tencent.mm.plugin.voip.ui.VideoActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1076
    const-string v0, "com.google.android.dialer/com.android.incallui.InCallActivity"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1077
    const-string v0, "com.whatsapp/com.whatsapp.voipcalling.VoipActivityV2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1080
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 1078
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public static getProjectionBlackList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1033
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1034
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "StatusBar"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1035
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "Splash Screen com.android.incallui"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1036
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.android.incallui/com.android.incallui.InCallActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1037
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "FloatAssistantView"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1038
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "MiuiFreeformBorderView"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1039
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "SnapshotStartingWindow for"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1040
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "ScreenshotThumbnail"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1041
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.milink.ui.activity.ScreeningConsoleWindow"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1042
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "FloatNotificationPanel"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1043
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.tencent.mobileqq/com.tencent.av.ui.AVActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1044
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.tencent.mobileqq/com.tencent.av.ui.AVLoadingDialogActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1046
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.tencent.mobileqq/.FloatingWindow"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1047
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "Splash Screen com.tencent.mm"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1048
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.tencent.mm/com.tencent.mm.plugin.voip.ui.VideoActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.tencent.mm/.FloatingWindow"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1050
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.whatsapp/com.whatsapp.voipcalling.VoipActivityV2"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1051
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.google.android.dialer/com.android.incallui.InCallActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1052
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.google.android.dialer/.FloatingWindow"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1053
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.miui.yellowpage/com.miui.yellowpage.activity.MarkNumberActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1054
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.miui.securitycenter/.FloatingWindow"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1055
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.milink.service.ui.PrivateWindow"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1056
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "com.milink.ui.activity.NFCLoadingActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1057
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "Freeform-OverLayView"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1058
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "Freeform-HotSpotView"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1059
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    const-string v1, "Freeform-TipView"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1061
    :cond_0
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    return-object v0
.end method

.method private isDarkModeContrastEnable()Z
    .locals 3

    .line 1352
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v0, v0, 0x30

    const/16 v1, 0x20

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 1354
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dark_mode_contrast_enable"

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    nop

    :goto_0
    move v0, v2

    .line 1356
    .local v0, "isContrastEnable":Z
    return v0
.end method

.method private isDensityForced()Z
    .locals 1

    .line 836
    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getForcedDensity()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isFindDeviceFlagUsePermitted(ILjava/lang/String;)Z
    .locals 6
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 721
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 723
    :cond_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    .line 724
    .local v0, "pm":Landroid/content/pm/IPackageManager;
    if-nez v0, :cond_1

    return v1

    .line 727
    :cond_1
    :try_start_0
    const-string v2, "android"

    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    invoke-interface {v0, v2, p2, v3}, Landroid/content/pm/IPackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x1

    if-nez v2, :cond_2

    .line 728
    return v3

    .line 731
    :cond_2
    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    const-wide/16 v4, 0x0

    invoke-interface {v0, p2, v4, v5, v2}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 732
    .local v2, "ai":Landroid/content/pm/ApplicationInfo;
    if-eqz v2, :cond_3

    iget v4, v2, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/2addr v4, v3

    if-eqz v4, :cond_3

    return v3

    .line 736
    .end local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    :cond_3
    goto :goto_0

    .line 734
    :catch_0
    move-exception v2

    .line 737
    :goto_0
    return v1
.end method

.method private isSupportSwitchResoluton()Z
    .locals 4

    .line 899
    const/4 v0, 0x0

    .line 900
    .local v0, "supportSwitchResolution":Z
    const-string/jumbo v1, "screen_resolution_supported"

    invoke-static {v1}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    .line 901
    .local v1, "mScreenResolutionsSupported":[I
    if-eqz v1, :cond_0

    array-length v2, v1

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 902
    const/4 v0, 0x1

    .line 904
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSupportSwitchResoluton:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "WindowManagerService"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    return v0
.end method

.method private killFreeformForFindDeviceKeyguardIfNecessary(ILandroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p3, "packageName"    # Ljava/lang/String;

    .line 682
    if-eqz p3, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    .line 685
    :cond_0
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->FIND_DEVICE:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->isFindDeviceFlagUsePermitted(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_1

    .line 687
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    invoke-interface {v0}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->freeformKillAll()V

    .line 689
    :cond_1
    return-void

    .line 683
    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic lambda$bindFloatwindowAndTargetTask$6(Landroid/view/WindowManager$LayoutParams;Ljava/util/ArrayList;Lcom/android/server/wm/Task;)V
    .locals 2
    .param p0, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p1, "targetTaskList"    # Ljava/util/ArrayList;
    .param p2, "task"    # Lcom/android/server/wm/Task;

    .line 2141
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2142
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2143
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2145
    :cond_0
    return-void
.end method

.method private synthetic lambda$notAllowCaptureDisplay$5(Lcom/android/server/wm/WindowState;)Z
    .locals 3
    .param p1, "w"    # Lcom/android/server/wm/WindowState;

    .line 1811
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->isVisible()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowStateAnimator;->hasSurface()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->isOnScreen()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1814
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_2

    .line 1815
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    .line 1816
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v2

    iget v2, v2, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-interface {v0, v2}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->isHideStackFromFullScreen(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1817
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " Secure window Can\'t CaptureDisplay   return ! "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WindowManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1818
    const/4 v0, 0x1

    return v0

    .line 1820
    :cond_2
    return v1

    .line 1812
    :cond_3
    :goto_0
    return v1
.end method

.method private synthetic lambda$notifySystemBrightnessChange$3(Lcom/android/server/wm/Task;)V
    .locals 1
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 1541
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopVisibleAppMainWindow()Lcom/android/server/wm/WindowState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->notifySystemBrightnessChange(Lcom/android/server/wm/WindowState;)V

    .line 1542
    return-void
.end method

.method private synthetic lambda$notifySystemBrightnessChange$4()V
    .locals 3

    .line 1535
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v0

    .line 1537
    .local v0, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 1538
    :try_start_0
    invoke-virtual {v0}, Lcom/android/server/wm/TaskDisplayArea;->getVisibleTasks()Ljava/util/ArrayList;

    move-result-object v2

    .line 1539
    .local v2, "visibleTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;"
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1540
    new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V

    .line 1543
    return-void

    .line 1539
    .end local v2    # "visibleTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private synthetic lambda$setRoundedCornerOverlaysCanScreenShot$0(ZLcom/android/server/wm/WindowState;)V
    .locals 1
    .param p1, "canScreenshot"    # Z
    .param p2, "w"    # Lcom/android/server/wm/WindowState;

    .line 860
    iget-object v0, p2, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    iget-boolean v0, v0, Lcom/android/server/wm/WindowToken;->mRoundedCornerOverlay:Z

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowStateAnimator;->hasSurface()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 863
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getClientViewRootSurface()Landroid/view/SurfaceControl;

    move-result-object v0

    .line 864
    .local v0, "clientSurfaceControl":Landroid/view/SurfaceControl;
    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->setSurfaceCanScreenShot(Landroid/view/SurfaceControl;Z)V

    .line 865
    return-void

    .line 861
    .end local v0    # "clientSurfaceControl":Landroid/view/SurfaceControl;
    :cond_1
    :goto_0
    return-void
.end method

.method private synthetic lambda$updateScreenShareProjectFlag$1(Lcom/android/server/wm/WindowState;)V
    .locals 2
    .param p1, "w"    # Lcom/android/server/wm/WindowState;

    .line 1390
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    if-eqz v0, :cond_0

    .line 1391
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget-object v0, v0, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    iget-object v0, v0, Lcom/android/server/wm/WindowSurfaceController;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 1392
    .local v0, "surfaceControl":Landroid/view/SurfaceControl;
    if-eqz v0, :cond_0

    .line 1393
    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    invoke-virtual {p0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->getScreenShareProjectAndPrivateCastFlag(Lcom/android/server/wm/WindowStateAnimator;)I

    move-result v1

    .line 1394
    .local v1, "flags":I
    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl;->setScreenProjection(I)V

    .line 1398
    .end local v0    # "surfaceControl":Landroid/view/SurfaceControl;
    .end local v1    # "flags":I
    :cond_0
    return-void
.end method

.method private synthetic lambda$updateScreenShareProjectFlag$2()V
    .locals 4

    .line 1387
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 1389
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    new-instance v2, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/RootWindowContainer;->forAllWindows(Ljava/util/function/Consumer;Z)V

    .line 1400
    monitor-exit v0

    .line 1401
    return-void

    .line 1400
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private notifySystemBrightnessChange(Lcom/android/server/wm/WindowState;)V
    .locals 3
    .param p1, "w"    # Lcom/android/server/wm/WindowState;

    .line 1550
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    .line 1551
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v1

    iget v1, v1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1554
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v1

    iget v1, v1, Lcom/android/server/wm/Task;->mTaskId:I

    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1557
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPowerManagerInternal:Landroid/os/PowerManagerInternal;

    .line 1558
    const/high16 v1, 0x7fc00000    # Float.NaN

    invoke-virtual {v0, v1}, Landroid/os/PowerManagerInternal;->setScreenBrightnessOverrideFromWindowManager(F)V

    .line 1559
    return-void

    .line 1552
    :cond_1
    :goto_0
    return-void
.end method

.method private registerBootCompletedReceiver()V
    .locals 3

    .line 357
    new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$2;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWallpaperCallback:Landroid/app/IWallpaperManagerCallback;

    .line 371
    new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$3;

    invoke-direct {v0, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$3;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBootCompletedReceiver:Landroid/content/BroadcastReceiver;

    .line 378
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 379
    .local v0, "bootCompletedFilter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBootCompletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 380
    return-void
.end method

.method private removeFindDeviceKeyguardFlagsIfNecessary(ILandroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p3, "packageName"    # Ljava/lang/String;

    .line 705
    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v0, v0, 0x800

    if-nez v0, :cond_0

    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v0, v0, 0x1000

    if-nez v0, :cond_0

    .line 707
    return-void

    .line 710
    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->isFindDeviceFlagUsePermitted(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_1

    .line 712
    const-string v0, "com.xiaomi.finddevice"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 713
    :cond_1
    return-void

    .line 716
    :cond_2
    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    .line 717
    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    .line 718
    return-void
.end method

.method public static setAlertWindowTitle(Landroid/view/WindowManager$LayoutParams;)V
    .locals 2
    .param p0, "attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 1084
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    invoke-static {v0}, Landroid/view/WindowManager$LayoutParams;->isSystemAlertWindowType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1085
    return-void

    .line 1088
    :cond_0
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->QQ:Ljava/lang/String;

    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1089
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->QQ_FLOATING:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 1091
    :cond_1
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->MM:Ljava/lang/String;

    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1092
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->MM_FLOATING:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 1094
    :cond_2
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->GOOGLE:Ljava/lang/String;

    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1095
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->GOOGLE_FLOATING:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 1097
    :cond_3
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SECURITY:Ljava/lang/String;

    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1098
    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SECURITY_FLOATING:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 1100
    :cond_4
    return-void
.end method

.method private setDefaultBlurConfigIfNeeded(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 1836
    sget-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->BACKGROUND_BLUR_SUPPORTED:Z

    if-eqz v0, :cond_0

    .line 1837
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "background_blur_enable"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_0

    sget-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->BACKGROUND_BLUR_STATUS_DEFAULT:Z

    if-eqz v0, :cond_0

    .line 1840
    const-string v0, "WindowManagerService"

    const-string/jumbo v2, "set default background blur config to true"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1841
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1843
    :cond_0
    return-void
.end method

.method public static setProjectionBlackList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1065
    .local p0, "blacklist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sput-object p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mProjectionBlackList:Ljava/util/List;

    .line 1066
    return-void
.end method

.method private setRoundedCornerOverlaysCanScreenShot(Z)V
    .locals 3
    .param p1, "canScreenshot"    # Z

    .line 859
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;Z)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/DisplayContent;->forAllWindows(Ljava/util/function/Consumer;Z)V

    .line 866
    return-void
.end method

.method private setSurfaceCanScreenShot(Landroid/view/SurfaceControl;Z)V
    .locals 1
    .param p1, "clientSurfaceControl"    # Landroid/view/SurfaceControl;
    .param p2, "canScreenshot"    # Z

    .line 869
    if-nez p1, :cond_0

    .line 870
    return-void

    .line 872
    :cond_0
    xor-int/lit8 v0, p2, 0x1

    invoke-virtual {p1, v0}, Landroid/view/SurfaceControl;->setSkipScreenshot(Z)V

    .line 873
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->requestTraversal()V

    .line 874
    return-void
.end method

.method private switchResolutionInternal(II)V
    .locals 6
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 789
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 790
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z

    if-eqz v1, :cond_0

    monitor-exit v0

    return-void

    .line 791
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->calcDensityAndUpdateForceDensityIfNeed(I)I

    move-result v1

    .line 792
    .local v1, "density":I
    const-string v2, "WindowManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start switching resolution, width:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",height:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",density:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    iput p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I

    .line 795
    iput p2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I

    .line 796
    iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayDensity:I

    .line 797
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z

    .line 798
    invoke-direct {p0, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->setRoundedCornerOverlaysCanScreenShot(Z)V

    .line 799
    const-string v2, "persist.sys.miui_resolution"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayDensity:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mH:Lcom/android/server/wm/ActivityTaskManagerService$H;

    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mAmInternal:Landroid/app/ActivityManagerInternal;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda7;

    invoke-direct {v4, v3}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda7;-><init>(Landroid/app/ActivityManagerInternal;)V

    invoke-virtual {v2, v4}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 804
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSmartPowerServiceInternal:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v2, p1, p2, v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onDisplaySwitchResolutionLocked(III)V

    .line 808
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 809
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v2, v3}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 813
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-direct {p0, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->updateScreenResolutionLocked(Lcom/android/server/wm/DisplayContent;)V

    .line 814
    iget-boolean v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportActiveModeSwitch:Z

    if-eqz v2, :cond_2

    .line 815
    invoke-static {}, Lcom/android/server/display/mode/DisplayModeDirectorStub;->getInstance()Lcom/android/server/display/mode/DisplayModeDirectorStub;

    move-result-object v2

    iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I

    iget v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I

    invoke-interface {v2, v3, v4, v5}, Lcom/android/server/display/mode/DisplayModeDirectorStub;->updateDisplaySize(III)V

    .line 818
    .end local v1    # "density":I
    :cond_2
    monitor-exit v0

    .line 819
    return-void

    .line 818
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private topRunningActivityChange(Lcom/android/server/wm/WindowState;)Z
    .locals 5
    .param p1, "win"    # Lcom/android/server/wm/WindowState;

    .line 1288
    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getDefaultDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 1289
    .local v0, "topRunningActivity":Lcom/android/server/wm/ActivityRecord;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1290
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I

    move-result v2

    .line 1291
    .local v2, "windowingMode":I
    iget-object v3, v0, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v3

    .line 1292
    .local v3, "flattenToString":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isKeyGuardShowing()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v4, 0x5

    if-eq v2, v4, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method private updateAppearance()V
    .locals 8

    .line 1706
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 1708
    .local v0, "displayContent":Lcom/android/server/wm/DisplayContent;
    if-eqz v0, :cond_6

    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    goto/16 :goto_0

    .line 1712
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/DisplayPolicy;->getStatusBar()Lcom/android/server/wm/WindowState;

    move-result-object v2

    .line 1714
    .local v2, "statusBar":Lcom/android/server/wm/WindowState;
    if-nez v2, :cond_1

    .line 1715
    return-void

    .line 1718
    :cond_1
    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getFrame()Landroid/graphics/Rect;

    move-result-object v3

    .line 1719
    .local v3, "statusBarFrame":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 1720
    .local v4, "width":I
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 1721
    .local v5, "height":I
    iget-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1722
    new-instance v6, Landroid/graphics/Rect;

    div-int/lit8 v7, v4, 0x4

    invoke-direct {v6, v1, v1, v7, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftStatusBounds:Landroid/graphics/Rect;

    .line 1724
    :cond_2
    iget-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1725
    new-instance v6, Landroid/graphics/Rect;

    mul-int/lit8 v7, v4, 0x3

    div-int/lit8 v7, v7, 0x4

    invoke-direct {v6, v7, v1, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightStatusBounds:Landroid/graphics/Rect;

    .line 1728
    :cond_3
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1729
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftStatusBounds:Landroid/graphics/Rect;

    invoke-direct {p0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->createAppearance(Landroid/graphics/Rect;)I

    move-result v1

    iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I

    .line 1731
    :cond_4
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1732
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightStatusBounds:Landroid/graphics/Rect;

    invoke-direct {p0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->createAppearance(Landroid/graphics/Rect;)I

    move-result v1

    iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I

    .line 1734
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateAppearance left"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " right"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " appearance = "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ", "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v6, "WindowManagerService"

    invoke-static {v6, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1738
    return-void

    .line 1709
    .end local v2    # "statusBar":Lcom/android/server/wm/WindowState;
    .end local v3    # "statusBarFrame":Landroid/graphics/Rect;
    .end local v4    # "width":I
    .end local v5    # "height":I
    :cond_6
    :goto_0
    return-void
.end method

.method private updateScreenResolutionLocked(Lcom/android/server/wm/DisplayContent;)V
    .locals 9
    .param p1, "dc"    # Lcom/android/server/wm/DisplayContent;

    .line 840
    iget v0, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayWidth:I

    iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_1

    iget v0, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayHeight:I

    iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v3

    :goto_1
    iput-boolean v0, p1, Lcom/android/server/wm/DisplayContent;->mIsSizeForced:Z

    .line 842
    iget v0, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayDensity:I

    iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayDensity:I

    if-eq v0, v1, :cond_2

    move v2, v3

    :cond_2
    iput-boolean v2, p1, Lcom/android/server/wm/DisplayContent;->mIsDensityForced:Z

    .line 843
    iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I

    iget v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I

    iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayDensity:I

    iget v7, p1, Lcom/android/server/wm/DisplayContent;->mBaseDisplayPhysicalXDpi:F

    iget v8, p1, Lcom/android/server/wm/DisplayContent;->mBaseDisplayPhysicalYDpi:F

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/wm/DisplayContent;->updateBaseDisplayMetrics(IIIFF)V

    .line 845
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->reconfigureDisplayLocked()V

    .line 846
    return-void
.end method


# virtual methods
.method public addMiuiPaperContrastOverlay(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/view/SurfaceControl;",
            ">;)V"
        }
    .end annotation

    .line 1915
    .local p1, "excludeLayersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/SurfaceControl;>;"
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 1916
    invoke-static {}, Lcom/android/server/wm/MiuiPaperContrastOverlayStub;->get()Lcom/android/server/wm/MiuiPaperContrastOverlayStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStub;->getMiuiPaperSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v0

    .line 1917
    .local v0, "miuiPaperContrastOverlay":Landroid/view/SurfaceControl;
    if-eqz v0, :cond_0

    .line 1918
    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiPaperContrastOverlay:Landroid/view/SurfaceControl;

    .line 1919
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1922
    .end local v0    # "miuiPaperContrastOverlay":Landroid/view/SurfaceControl;
    :cond_0
    return-void
.end method

.method public addSecureChangedListener(Landroid/app/IWindowSecureChangeListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/app/IWindowSecureChangeListener;

    .line 1243
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 1244
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecureChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1245
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecureChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1247
    :cond_0
    monitor-exit v0

    .line 1248
    return-void

    .line 1247
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public adjustWindowParams(Landroid/view/WindowManager$LayoutParams;Ljava/lang/String;I)V
    .locals 10
    .param p1, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uid"    # I

    .line 518
    if-nez p1, :cond_0

    return-void

    .line 520
    :cond_0
    const/16 v0, 0x3e8

    if-eq p3, v0, :cond_5

    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-nez v0, :cond_1

    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    if-eqz v0, :cond_5

    .line 523
    :cond_1
    const/4 v0, 0x0

    .line 525
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 527
    invoke-static {p3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    .line 526
    const/4 v3, 0x0

    invoke-virtual {v1, p2, v3, v2}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 530
    goto :goto_0

    .line 528
    :catch_0
    move-exception v1

    .line 529
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 531
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    if-nez v0, :cond_2

    move v1, p3

    goto :goto_1

    :cond_2
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 532
    .local v1, "appUid":I
    :goto_1
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mAppOps:Landroid/app/AppOpsManager;

    const/16 v3, 0x2724

    const/4 v6, 0x0

    const-string v7, "WindowManagerServiceImpl#adjustWindowParams"

    move v4, v1

    move-object v5, p2

    invoke-virtual/range {v2 .. v7}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 534
    .local v2, "mode":I
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v3, :cond_3

    .line 535
    const-class v3, Lmiui/security/SecurityManagerInternal;

    invoke-static {v3}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/security/SecurityManagerInternal;

    iput-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    .line 537
    :cond_3
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-eqz v4, :cond_4

    .line 538
    const/16 v5, 0x24

    const-wide/16 v7, 0x1

    .line 539
    invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 538
    move-object v6, p2

    invoke-virtual/range {v4 .. v9}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    .line 542
    :cond_4
    if-eqz v2, :cond_5

    .line 543
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v4, -0x80001

    and-int/2addr v3, v4

    iput v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 544
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v4, -0x400001

    and-int/2addr v3, v4

    iput v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 545
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MIUILOG- Show when locked PermissionDenied pkg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " uid : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "WindowManagerService"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "appUid":I
    .end local v2    # "mode":I
    :cond_5
    invoke-direct {p0, p3, p1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl;->adjustFindDeviceAttrs(ILandroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V

    .line 552
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl;->adjustGoogleAssistantAttrs(Landroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V

    .line 556
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_6

    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v1, 0x63

    if-gt v0, v1, :cond_6

    .line 557
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 559
    :cond_6
    return-void
.end method

.method public bindFloatwindowAndTargetTask(Landroid/view/WindowManager$LayoutParams;Lcom/android/server/wm/WindowToken;Lcom/android/server/wm/RootWindowContainer;)V
    .locals 6
    .param p1, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p2, "token"    # Lcom/android/server/wm/WindowToken;
    .param p3, "root"    # Lcom/android/server/wm/RootWindowContainer;

    .line 2131
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v1, 0x7f6

    if-ne v0, v1, :cond_3

    .line 2132
    invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "home_worldCirculate_and_smallWindow_crop"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mFloatWindowMirrorWhiteList:Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    .line 2133
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2134
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 2135
    .local v0, "topFocusedRootTask":Lcom/android/server/wm/Task;
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getTopResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 2136
    .local v1, "topActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.miui.home/.launcher.Launcher"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2137
    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.miui.circulate.world.AppCirculateActivity"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2138
    :cond_0
    invoke-virtual {p3}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    .line 2139
    .local v2, "defaultDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2140
    .local v3, "targetTaskList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;"
    new-instance v4, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda4;

    invoke-direct {v4, p1, v3}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda4;-><init>(Landroid/view/WindowManager$LayoutParams;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v4}, Lcom/android/server/wm/TaskDisplayArea;->forAllRootTasks(Ljava/util/function/Consumer;)V

    .line 2146
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 2147
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/Task;

    iput-object p2, v5, Lcom/android/server/wm/Task;->floatWindow:Lcom/android/server/wm/WindowToken;

    .line 2146
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2149
    .end local v2    # "defaultDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    .end local v3    # "targetTaskList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;"
    .end local v4    # "i":I
    :cond_1
    goto :goto_1

    .line 2150
    :cond_2
    iput-object p2, v0, Lcom/android/server/wm/Task;->floatWindow:Lcom/android/server/wm/WindowToken;

    .line 2153
    .end local v0    # "topFocusedRootTask":Lcom/android/server/wm/Task;
    .end local v1    # "topActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_3
    :goto_1
    return-void
.end method

.method public calcHoverAnimatingColor([F)V
    .locals 1
    .param p1, "startColor"    # [F

    .line 1864
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->miuiHoverModeInternal:Lcom/android/server/wm/MiuiHoverModeInternal;

    if-eqz v0, :cond_0

    .line 1865
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->calcHoverAnimatingColor([F)V

    .line 1867
    :cond_0
    return-void
.end method

.method public checkFlipFullScreenChange(Lcom/android/server/wm/ActivityRecord;)V
    .locals 9
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 2036
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->enableFullScreen(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    .line 2037
    .local v0, "activityEnableFullScreen":Z
    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 2038
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getWindowConfiguration()Landroid/app/WindowConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-gtz v2, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 2039
    .local v2, "nowIsFullScreen":Z
    :goto_0
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mLastIsFullScreen:Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-ne v3, v2, :cond_1

    .line 2040
    const-string v1, "WindowManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkFlipFullScreenChange is same, nowIsFullScreen = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2041
    return-void

    .line 2043
    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mLastIsFullScreen:Ljava/lang/Boolean;

    .line 2044
    const-string v3, "WindowManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "before notify flip small screen change, is full = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2045
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mFlipListenerLock:Ljava/lang/Object;

    monitor-enter v3

    .line 2046
    :try_start_0
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    sub-int/2addr v4, v1

    .local v4, "i":I
    :goto_1
    if-ltz v4, :cond_3

    .line 2047
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    invoke-virtual {v1, v4}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/IFlipWindowStateListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2049
    .local v1, "listener":Landroid/view/IFlipWindowStateListener;
    :try_start_1
    invoke-interface {v1, v2}, Landroid/view/IFlipWindowStateListener;->onIsFullScreenChange(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2058
    goto :goto_2

    .line 2050
    :catch_0
    move-exception v5

    .line 2051
    .local v5, "e":Landroid/os/RemoteException;
    :try_start_2
    instance-of v6, v5, Landroid/os/DeadObjectException;

    if-eqz v6, :cond_2

    .line 2052
    const-string v6, "WindowManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onNavigationColorChange binder died, remove it, key = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    .line 2053
    invoke-virtual {v8, v4}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2052
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2054
    iget-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    invoke-virtual {v6, v4}, Landroid/util/ArrayMap;->removeAt(I)Ljava/lang/Object;

    goto :goto_2

    .line 2056
    :cond_2
    const-string v6, "WindowManagerService"

    const-string v7, "onNavigationColorChange fail"

    invoke-static {v6, v7, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2046
    .end local v1    # "listener":Landroid/view/IFlipWindowStateListener;
    .end local v5    # "e":Landroid/os/RemoteException;
    :goto_2
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 2060
    .end local v4    # "i":I
    :cond_3
    monitor-exit v3

    .line 2061
    return-void

    .line 2060
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public clearForcedDisplaySize(Lcom/android/server/wm/DisplayContent;)V
    .locals 3
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 988
    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getUserSetResolution()[I

    move-result-object v0

    .line 989
    .local v0, "screenResolution":[I
    if-eqz v0, :cond_0

    .line 990
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 991
    .local v1, "width":I
    const/4 v2, 0x1

    aget v2, v0, v2

    .line 992
    .local v2, "height":I
    invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/DisplayContent;->setForcedSize(II)V

    .line 993
    .end local v1    # "width":I
    .end local v2    # "height":I
    goto :goto_0

    .line 994
    :cond_0
    iget v1, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayWidth:I

    iget v2, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayHeight:I

    invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/DisplayContent;->setForcedSize(II)V

    .line 997
    :goto_0
    return-void
.end method

.method public clearOverrideBrightnessRecord(I)V
    .locals 3
    .param p1, "taskId"    # I

    .line 1594
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    .line 1595
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 1596
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTaskRemoved: mTaskIdScreenBrightnessOverrides : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " taskId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WindowManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V

    .line 1600
    :cond_0
    return-void
.end method

.method public doesAddCarWithNavigationBarRequireToken(Ljava/lang/String;)Z
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1954
    const-string v0, "WindowManagerService"

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "com.miui.carlink"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1955
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/16 v3, 0x40

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v2, v2, v1

    .line 1956
    .local v2, "releaseSig":Landroid/content/pm/Signature;
    invoke-virtual {v2}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1958
    .local v3, "sigBytes":[B
    const/4 v4, 0x0

    .line 1960
    .local v4, "digest":Ljava/security/MessageDigest;
    :try_start_1
    const-string v5, "SHA-256"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v5

    move-object v4, v5

    .line 1961
    invoke-virtual {v4, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v5

    .line 1962
    .local v5, "hash":[B
    new-instance v6, Ljava/lang/StringBuilder;

    array-length v7, v5

    mul-int/lit8 v7, v7, 0x2

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1963
    .local v6, "hexString":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v8, v5

    const/4 v9, 0x1

    if-ge v7, v8, :cond_1

    .line 1964
    aget-byte v8, v5, v7

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    .line 1965
    .local v8, "hex":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-ne v10, v9, :cond_0

    .line 1966
    const/16 v9, 0x30

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1968
    :cond_0
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1963
    nop

    .end local v8    # "hex":Ljava/lang/String;
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1971
    .end local v7    # "i":I
    :cond_1
    const-string v7, "c8a2e9bccf597c2fb6dc66bee293fc13f2fc47ec77bc6b2b0d52c11f51192ab8"

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    const-string v7, "c9009d01ebf9f5d0302bc71b2fe9aa9a47a432bba17308a3111b75d7b2149025"

    .line 1972
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_2

    goto :goto_1

    .line 1979
    .end local v5    # "hash":[B
    .end local v6    # "hexString":Ljava/lang/StringBuilder;
    :cond_2
    goto :goto_2

    .line 1973
    .restart local v5    # "hash":[B
    .restart local v6    # "hexString":Ljava/lang/StringBuilder;
    :cond_3
    :goto_1
    return v9

    .line 1977
    .end local v5    # "hash":[B
    .end local v6    # "hexString":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v5

    .line 1978
    .local v5, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "packageName"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "NoSuchAlgorithmException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1981
    .end local v2    # "releaseSig":Landroid/content/pm/Signature;
    .end local v3    # "sigBytes":[B
    .end local v4    # "digest":Ljava/security/MessageDigest;
    .end local v5    # "e":Ljava/security/NoSuchAlgorithmException;
    :goto_2
    nop

    .line 1987
    goto :goto_3

    .line 1982
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "packageName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not carlink"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1983
    return v1

    .line 1985
    :catch_1
    move-exception v2

    .line 1986
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NameNotFoundException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1988
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_3
    return v1
.end method

.method public enableWmsDebugConfig(Lcom/android/server/wm/WindowManagerShellCommand;Ljava/io/PrintWriter;)I
    .locals 4
    .param p1, "shellcmd"    # Lcom/android/server/wm/WindowManagerShellCommand;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 1148
    invoke-virtual {p1}, Lcom/android/server/wm/WindowManagerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v0

    .line 1149
    .local v0, "cmd":Ljava/lang/String;
    const-string v1, "enable-text"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "disable-text"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1150
    const-string v1, "Error: wrong args , must be enable-text or disable-text"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1151
    const/4 v1, -0x1

    return v1

    .line 1153
    :cond_0
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1155
    .local v1, "enable":Z
    :goto_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowManagerShellCommand;->getNextArg()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .local v3, "config":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 1156
    invoke-direct {p0, v3, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->enableWmsDebugConfig(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1158
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method public executeShellCommand(Ljava/io/PrintWriter;[Ljava/lang/String;ILjava/lang/String;)Z
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I
    .param p4, "opt"    # Ljava/lang/String;

    .line 435
    array-length v0, p2

    const/4 v1, 0x0

    if-ge p3, v0, :cond_0

    .line 436
    array-length v0, p2

    sub-int/2addr v0, p3

    new-array v0, v0, [Ljava/lang/String;

    .local v0, "str":[Ljava/lang/String;
    goto :goto_0

    .line 438
    .end local v0    # "str":[Ljava/lang/String;
    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    .line 440
    .restart local v0    # "str":[Ljava/lang/String;
    :goto_0
    array-length v2, p2

    sub-int/2addr v2, p3

    invoke-static {p2, p3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 441
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WindowManagerServiceImpl:executeShellCommand opti = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " opt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 443
    :try_start_0
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v2

    invoke-virtual {v2, p4, v0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_1

    .line 444
    const/4 v1, 0x1

    return v1

    .line 448
    :cond_1
    goto :goto_1

    .line 446
    :catch_0
    move-exception v2

    .line 447
    .local v2, "exception":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 449
    .end local v2    # "exception":Ljava/lang/Exception;
    :goto_1
    return v1
.end method

.method public finishSwitchResolution()V
    .locals 4

    .line 850
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 851
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->setRoundedCornerOverlaysCanScreenShot(Z)V

    .line 852
    iget-boolean v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z

    if-eqz v2, :cond_0

    .line 853
    const-string v2, "WindowManagerService"

    const-string v3, "finished switching resolution"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z

    .line 856
    :cond_0
    monitor-exit v0

    .line 857
    return-void

    .line 856
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getBlurWallpaperAppearance(Landroid/graphics/Rect;Landroid/graphics/Rect;[I)V
    .locals 6
    .param p1, "leftBounds"    # Landroid/graphics/Rect;
    .param p2, "rightBounds"    # Landroid/graphics/Rect;
    .param p3, "outAppearance"    # [I

    .line 1657
    if-eqz p3, :cond_5

    array-length v0, p3

    const/4 v1, 0x2

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    if-nez p2, :cond_0

    goto/16 :goto_2

    .line 1662
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    const-string v1, "appearance = "

    const-string v2, "WindowManagerService"

    const/4 v3, -0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_1

    .line 1663
    aput v3, p3, v4

    goto :goto_0

    .line 1665
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I

    if-eq v0, v3, :cond_2

    .line 1666
    aput v0, p3, v4

    goto :goto_0

    .line 1668
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->createAppearance(Landroid/graphics/Rect;)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I

    aput v0, p3, v4

    .line 1669
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1670
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getBlurWallpaperAppearance left bounds = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget v4, p3, v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1674
    :goto_0
    invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    const/4 v4, 0x1

    if-eqz v0, :cond_3

    .line 1675
    aput v3, p3, v4

    goto :goto_1

    .line 1677
    :cond_3
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I

    if-eq v0, v3, :cond_4

    .line 1678
    aput v0, p3, v4

    goto :goto_1

    .line 1680
    :cond_4
    invoke-direct {p0, p2}, Lcom/android/server/wm/WindowManagerServiceImpl;->createAppearance(Landroid/graphics/Rect;)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I

    aput v0, p3, v4

    .line 1681
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightStatusBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1682
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBlurWallpaperAppearance right bounds = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget v1, p3, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1686
    :goto_1
    return-void

    .line 1660
    :cond_5
    :goto_2
    return-void
.end method

.method public getBlurWallpaperBmp()Landroid/graphics/Bitmap;
    .locals 3

    .line 389
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    const/16 v2, 0x6a

    invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(I)V

    .line 391
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowManagerService$H;->sendEmptyMessage(I)Z

    .line 392
    const-string v0, "WindowManagerService"

    const-string v2, "mBlurWallpaperBmp is null, wait to update"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    return-object v1

    .line 396
    :cond_0
    if-eqz v0, :cond_1

    .line 397
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 398
    :cond_1
    nop

    .line 396
    :goto_0
    return-object v1
.end method

.method public getCompatScale(Ljava/lang/String;I)F
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 1644
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mCompatModePackages:Lcom/android/server/wm/CompatModePackages;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/CompatModePackages;->getCompatScale(Ljava/lang/String;I)F

    move-result v0

    return v0
.end method

.method public getDefaultDensity()I
    .locals 4

    .line 950
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 951
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportSwitchResolutionFeature:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I

    iget v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I

    mul-int v3, v1, v2

    if-eqz v3, :cond_1

    .line 952
    iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalDensity:I

    mul-int/2addr v0, v2

    int-to-float v0, v0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0

    .line 954
    :cond_1
    iget v0, v0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayDensity:I

    return v0
.end method

.method public getForcedDensity()I
    .locals 3

    .line 877
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 878
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v1

    .line 877
    const-string v2, "display_density_forced"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 879
    .local v0, "densityString":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 880
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    .line 882
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public getNavigationBarColor()I
    .locals 1

    .line 2107
    iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationColor:I

    return v0
.end method

.method public getResolutionSwitchShotBuffer(ZLandroid/graphics/Rect;)Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;
    .locals 5
    .param p1, "isDisplayRotation"    # Z
    .param p2, "cropBounds"    # Landroid/graphics/Rect;

    .line 970
    const/4 v0, 0x0

    .line 971
    .local v0, "screenshotBuffer":Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;
    iget-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z

    if-eqz v1, :cond_0

    .line 972
    invoke-static {}, Lcom/android/server/display/DisplayControl;->getPhysicalDisplayIds()[J

    move-result-object v1

    .line 973
    .local v1, "physicalDisplayIds":[J
    const/4 v2, 0x0

    aget-wide v2, v1, v2

    invoke-static {v2, v3}, Lcom/android/server/display/DisplayControl;->getPhysicalDisplayToken(J)Landroid/os/IBinder;

    move-result-object v2

    .line 974
    .local v2, "displayToken":Landroid/os/IBinder;
    new-instance v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;

    invoke-direct {v3, v2}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;-><init>(Landroid/os/IBinder;)V

    .line 976
    invoke-virtual {v3, p2}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setSourceCrop(Landroid/graphics/Rect;)Landroid/window/ScreenCapture$CaptureArgs$Builder;

    move-result-object v3

    check-cast v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;

    .line 977
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setCaptureSecureLayers(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;

    move-result-object v3

    check-cast v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;

    .line 978
    invoke-virtual {v3, v4}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setAllowProtected(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;

    move-result-object v3

    check-cast v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;

    .line 979
    invoke-virtual {v3, p1}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setHintForSeamlessTransition(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;

    move-result-object v3

    check-cast v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;

    .line 980
    invoke-virtual {v3}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->build()Landroid/window/ScreenCapture$DisplayCaptureArgs;

    move-result-object v3

    .line 981
    .local v3, "displayCaptureArgs":Landroid/window/ScreenCapture$DisplayCaptureArgs;
    invoke-static {v3}, Landroid/window/ScreenCapture;->captureDisplay(Landroid/window/ScreenCapture$DisplayCaptureArgs;)Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;

    move-result-object v0

    .line 983
    .end local v1    # "physicalDisplayIds":[J
    .end local v2    # "displayToken":Landroid/os/IBinder;
    .end local v3    # "displayCaptureArgs":Landroid/window/ScreenCapture$DisplayCaptureArgs;
    :cond_0
    return-object v0
.end method

.method public getScreenShareProjectAndPrivateCastFlag(Lcom/android/server/wm/WindowStateAnimator;)I
    .locals 12
    .param p1, "winAnimator"    # Lcom/android/server/wm/WindowStateAnimator;

    .line 1410
    const/4 v0, 0x0

    .line 1411
    .local v0, "applyFlag":I
    invoke-static {}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getInstance()Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;

    move-result-object v1

    .line 1412
    .local v1, "imp":Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "screen_share_protection_on"

    const/4 v4, 0x0

    const/4 v5, -0x2

    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    .line 1415
    .local v2, "isScreenShareProtection":Z
    :goto_0
    iget-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 1416
    invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getMiuiInScreeningSettingsKey()Ljava/lang/String;

    move-result-object v7

    .line 1415
    invoke-static {v6, v7, v4, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    .line 1417
    .local v6, "screenProjectionOnOrOff":I
    iget-object v7, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 1418
    invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getMiuiPrivacyOnSettingsKey()Ljava/lang/String;

    move-result-object v8

    .line 1417
    invoke-static {v7, v8, v4, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v4

    .line 1420
    .local v4, "screenProjectionPrivacy":I
    if-eqz p1, :cond_8

    iget-object v5, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    if-eqz v5, :cond_8

    iget-object v5, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    invoke-virtual {v5}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    if-eqz v5, :cond_8

    iget-object v5, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    .line 1421
    invoke-virtual {v5}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 1422
    iget-object v5, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    invoke-virtual {v5}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1423
    .local v5, "name":Ljava/lang/String;
    if-lez v6, :cond_3

    if-lez v4, :cond_3

    .line 1424
    invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getProjectionBlackList()Ljava/util/ArrayList;

    move-result-object v7

    .line 1425
    .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v7, :cond_3

    .line 1426
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1427
    .local v9, "blackTitle":Ljava/lang/String;
    invoke-virtual {v5, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1428
    invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getExtraScreenProjectFlag()I

    move-result v10

    or-int/2addr v0, v10

    goto :goto_2

    .line 1429
    :cond_1
    iget-object v10, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget-boolean v10, v10, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    if-eqz v10, :cond_2

    iget-object v10, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    .line 1430
    invoke-virtual {v10}, Lcom/android/server/wm/WindowState;->getName()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_2

    iget-object v10, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    .line 1431
    invoke-virtual {v10}, Lcom/android/server/wm/WindowState;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "PopupWindow"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget v10, v10, Lcom/android/server/wm/WindowState;->mViewVisibility:I

    if-nez v10, :cond_2

    .line 1433
    invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getExtraScreenProjectFlag()I

    move-result v10

    or-int/2addr v0, v10

    .line 1435
    .end local v9    # "blackTitle":Ljava/lang/String;
    :cond_2
    :goto_2
    goto :goto_1

    .line 1440
    .end local v7    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    const/4 v7, 0x0

    .line 1441
    .local v7, "mode":I
    const/4 v8, 0x0

    .line 1442
    .local v8, "packageName":Ljava/lang/String;
    iget-object v9, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget-object v9, v9, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v9, :cond_4

    iget-object v9, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget-object v9, v9, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v9, v9, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    if-eqz v9, :cond_4

    .line 1443
    iget-object v9, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget-object v9, v9, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v9, v9, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 1444
    .local v9, "uid":I
    iget-object v10, p1, Lcom/android/server/wm/WindowStateAnimator;->mWin:Lcom/android/server/wm/WindowState;

    iget-object v10, v10, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v10, v10, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v8, v10, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 1446
    iget-object v10, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mAppOps:Landroid/app/AppOpsManager;

    const/16 v11, 0x2739

    invoke-virtual {v10, v11, v9, v8}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v7

    .line 1450
    .end local v9    # "uid":I
    :cond_4
    if-eqz v2, :cond_8

    .line 1451
    if-ne v7, v3, :cond_5

    if-eqz v8, :cond_5

    .line 1452
    or-int/lit8 v0, v0, 0x1

    .line 1454
    :cond_5
    invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getScreenShareProjectBlackList()Ljava/util/ArrayList;

    move-result-object v3

    .line 1455
    .local v3, "screenShareProjectBlacklist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v3, :cond_6

    return v0

    .line 1456
    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1457
    .local v10, "shareName":Ljava/lang/String;
    invoke-virtual {v5, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1458
    or-int/lit8 v0, v0, 0x2

    .line 1460
    .end local v10    # "shareName":Ljava/lang/String;
    :cond_7
    goto :goto_3

    .line 1465
    .end local v3    # "screenShareProjectBlacklist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "name":Ljava/lang/String;
    .end local v7    # "mode":I
    .end local v8    # "packageName":Ljava/lang/String;
    :cond_8
    return v0
.end method

.method public getTransitionReadyState()Z
    .locals 1

    .line 1947
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTransitionReadyList:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getUserActivityTime(Ljava/lang/String;)I
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .line 1206
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimUserTimeoutMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getUserSetResolution()[I
    .locals 5

    .line 929
    const-string v0, ","

    const/4 v1, 0x3

    new-array v1, v1, [I

    .line 930
    .local v1, "screenSizeInfo":[I
    const-string v2, "persist.sys.miui_resolution"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 931
    .local v2, "miuiResolution":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 934
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v4

    .line 936
    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v4

    .line 938
    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    aput v0, v1, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 942
    goto :goto_0

    .line 939
    :catch_0
    move-exception v0

    .line 940
    .local v0, "e":Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    .line 941
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getResolutionFromProperty exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "WindowManagerService"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 943
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :goto_0
    return-object v1

    .line 945
    :cond_0
    return-object v3
.end method

.method public init(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V
    .locals 4
    .param p1, "wms"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 455
    iput-object p2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 456
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    .line 457
    iget-object v0, p1, Lcom/android/server/wm/WindowManagerService;->mAppOps:Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mAppOps:Landroid/app/AppOpsManager;

    .line 458
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 459
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    .line 461
    const-class v0, Landroid/hardware/display/DisplayManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    .line 462
    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isSupportSwitchResoluton()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportSwitchResolutionFeature:Z

    .line 463
    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->checkDDICSupportAndInitPhysicalSize()V

    .line 467
    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->registerBootCompletedReceiver()V

    .line 469
    new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$WindowOpChangedListener;

    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-direct {v0, v1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl$WindowOpChangedListener;-><init>(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V

    .line 471
    .local v0, "listener":Landroid/app/AppOpsManager$OnOpChangedListener;
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mAppOps:Landroid/app/AppOpsManager;

    const/16 v2, 0x2739

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/AppOpsManager;->startWatchingMode(ILjava/lang/String;Landroid/app/AppOpsManager$OnOpChangedListener;)V

    .line 473
    invoke-static {}, Lcom/miui/whetstone/PowerKeeperPolicy;->getInstance()Lcom/miui/whetstone/PowerKeeperPolicy;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    .line 474
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 475
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 474
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlackList:Ljava/util/List;

    .line 480
    const-class v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSmartPowerServiceInternal:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 482
    return-void
.end method

.method public initAfterBoot()V
    .locals 1

    .line 383
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 384
    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->updateBlurWallpaperBmp()V

    .line 386
    :cond_0
    return-void
.end method

.method public initializeMiuiResolutionLocked()V
    .locals 4

    .line 887
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportSwitchResolutionFeature:Z

    if-nez v0, :cond_0

    .line 888
    return-void

    .line 890
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 891
    invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getUserSetResolution()[I

    move-result-object v0

    .line 892
    .local v0, "screenResolution":[I
    if-eqz v0, :cond_1

    aget v2, v0, v1

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I

    .line 893
    .local v2, "screenWidth":I
    :goto_0
    if-eqz v0, :cond_2

    const/4 v3, 0x1

    aget v3, v0, v3

    goto :goto_1

    :cond_2
    iget v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I

    .line 894
    .local v3, "screenHeight":I
    :goto_1
    invoke-direct {p0, v2, v3}, Lcom/android/server/wm/WindowManagerServiceImpl;->switchResolutionInternal(II)V

    .line 895
    iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z

    .line 896
    return-void
.end method

.method public isAbortTransitionInScreenOff(ILcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/Transition;)Z
    .locals 7
    .param p1, "type"    # I
    .param p2, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p3, "transition"    # Lcom/android/server/wm/Transition;

    .line 2186
    const/4 v0, 0x4

    const/4 v1, 0x0

    if-eq p1, v0, :cond_0

    .line 2187
    return v1

    .line 2190
    :cond_0
    const/4 v0, 0x0

    .line 2191
    .local v0, "isVoiceassist":Z
    if-eqz p3, :cond_2

    .line 2192
    iget-object v2, p3, Lcom/android/server/wm/Transition;->mParticipants:Landroid/util/ArraySet;

    .line 2193
    .local v2, "participants":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowContainer;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/util/ArraySet;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 2194
    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/wm/WindowContainer;

    .line 2195
    .local v4, "wc":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v5

    iget-object v5, v5, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 2196
    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v5

    iget-object v5, v5, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    const-string v6, "com.miui.voiceassist.floatactivity"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2197
    const/4 v0, 0x1

    .line 2198
    goto :goto_1

    .line 2193
    .end local v4    # "wc":Lcom/android/server/wm/WindowContainer;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2203
    .end local v2    # "participants":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowContainer;>;"
    .end local v3    # "i":I
    :cond_2
    :goto_1
    if-eqz p2, :cond_3

    iget-object v2, p2, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    if-eqz v2, :cond_3

    .line 2204
    iget-object v2, p2, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v2

    .line 2205
    .local v2, "dc":Lcom/android/server/wm/DisplayContent;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2206
    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v3

    iget v3, v3, Landroid/view/DisplayInfo;->state:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    if-eqz v0, :cond_3

    .line 2208
    invoke-virtual {p3}, Lcom/android/server/wm/Transition;->abort()V

    .line 2209
    return v4

    .line 2212
    .end local v2    # "dc":Lcom/android/server/wm/DisplayContent;
    :cond_3
    return v1
.end method

.method public isAdjustScreenOff(Ljava/lang/CharSequence;)Z
    .locals 3
    .param p1, "tag"    # Ljava/lang/CharSequence;

    .line 1210
    sget-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SUPPPORT_CLOUD_DIM:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1211
    return v1

    .line 1213
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1214
    .local v0, "targetWindowName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimUserTimeoutMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimUserTimeoutMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1215
    const/4 v1, 0x1

    return v1

    .line 1217
    :cond_1
    return v1
.end method

.method public isAssistResson(Ljava/lang/CharSequence;)Z
    .locals 2
    .param p1, "tag"    # Ljava/lang/CharSequence;

    .line 1221
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1222
    .local v0, "targetWindowName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimNeedAssistMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimNeedAssistMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimNeedAssistMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1223
    const/4 v1, 0x1

    return v1

    .line 1225
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isCameraOpen()Z
    .locals 1

    .line 1229
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mOpeningCameraID:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCtsModeEnabled()Z
    .locals 1

    .line 1121
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->IS_CTS_MODE:Z

    return v0
.end method

.method public isKeyGuardShowing()Z
    .locals 2

    .line 1524
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mKeyguardController:Lcom/android/server/wm/KeyguardController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/KeyguardController;->isKeyguardShowing(I)Z

    move-result v0

    return v0
.end method

.method public isNotifyTouchEnable()Z
    .locals 1

    .line 1511
    sget-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SUPPORT_UNION_POWER_CORE:Z

    return v0
.end method

.method public isPendingSwitchResolution()Z
    .locals 1

    .line 959
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z

    return v0
.end method

.method public isSplitMode()Z
    .locals 1

    .line 1139
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSplitMode:Z

    return v0
.end method

.method public isSupportSetActiveModeSwitchResolution()Z
    .locals 1

    .line 964
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportActiveModeSwitch:Z

    return v0
.end method

.method public isTopLayoutFullScreen()Z
    .locals 2

    .line 1025
    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getDefaultDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1026
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;

    move-result-object v0

    .line 1027
    .local v0, "displayPolicy":Lcom/android/server/wm/DisplayPolicy;
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayPolicy;->isTopLayoutFullscreen()Z

    move-result v1

    return v1

    .line 1029
    .end local v0    # "displayPolicy":Lcom/android/server/wm/DisplayPolicy;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public linkWallpaperWindowTokenDeathMonitor(Landroid/os/IBinder;I)V
    .locals 2
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "displayId"    # I

    .line 1234
    :try_start_0
    new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;Landroid/os/IBinder;I)V

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1237
    goto :goto_0

    .line 1235
    :catch_0
    move-exception v0

    .line 1238
    :goto_0
    return-void
.end method

.method public mayChangeToMiuiSecurityInputMethod()Z
    .locals 8

    .line 1877
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 1878
    .local v0, "defaultDisplay":Lcom/android/server/wm/DisplayContent;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1879
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getImeInputTarget()Lcom/android/server/wm/InputTarget;

    move-result-object v2

    .line 1880
    .local v2, "imeInputTarget":Lcom/android/server/wm/InputTarget;
    if-nez v2, :cond_1

    return v1

    .line 1881
    :cond_1
    invoke-interface {v2}, Lcom/android/server/wm/InputTarget;->getWindowState()Lcom/android/server/wm/WindowState;

    move-result-object v3

    .line 1882
    .local v3, "windowState":Lcom/android/server/wm/WindowState;
    if-nez v3, :cond_2

    return v1

    .line 1883
    :cond_2
    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    .line 1884
    .local v4, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    if-eqz v4, :cond_4

    .line 1885
    iget-object v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiSecurityImeWhiteList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1886
    .local v6, "windowName":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1887
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "windowName: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " activityRecord: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "WindowManagerService"

    invoke-static {v5, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1888
    const/4 v1, 0x1

    return v1

    .line 1890
    .end local v6    # "windowName":Ljava/lang/String;
    :cond_3
    goto :goto_0

    .line 1892
    :cond_4
    return v1
.end method

.method public mayChangeToMiuiSecurityInputMethod(Landroid/os/IBinder;)Z
    .locals 7
    .param p1, "token"    # Landroid/os/IBinder;

    .line 1896
    const-class v0, Lcom/android/server/wm/WindowManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerInternal;

    .line 1897
    .local v0, "service":Lcom/android/server/wm/WindowManagerInternal;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1899
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v2

    .line 1900
    :try_start_0
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v3, v3, Lcom/android/server/wm/WindowManagerService;->mWindowMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/WindowState;

    .line 1901
    .local v3, "windowState":Lcom/android/server/wm/WindowState;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1902
    if-nez v3, :cond_1

    return v1

    .line 1903
    :cond_1
    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 1904
    .local v2, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    if-eqz v2, :cond_3

    .line 1905
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiSecurityImeWhiteList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1906
    .local v5, "windowName":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1907
    const/4 v1, 0x1

    return v1

    .line 1909
    .end local v5    # "windowName":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 1911
    :cond_3
    return v1

    .line 1901
    .end local v2    # "activityRecord":Lcom/android/server/wm/ActivityRecord;
    .end local v3    # "windowState":Lcom/android/server/wm/WindowState;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public notAllowCaptureDisplay(Lcom/android/server/wm/RootWindowContainer;I)Z
    .locals 4
    .param p1, "windowState"    # Lcom/android/server/wm/RootWindowContainer;
    .param p2, "displayId"    # I

    .line 1797
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    const/4 v2, 0x0

    if-eq v0, v1, :cond_2

    .line 1798
    invoke-static {}, Lcom/android/server/wm/ActivityRecordStub;->get()Lcom/android/server/wm/ActivityRecordStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub;->isCompatibilityMode()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1802
    :cond_0
    invoke-virtual {p1, p2}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 1804
    .local v0, "displayContent":Lcom/android/server/wm/DisplayContent;
    if-nez v0, :cond_1

    .line 1805
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Screenshot on invalid display "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1806
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1805
    const-string v3, "WindowManagerService"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1807
    return v2

    .line 1810
    :cond_1
    new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/DisplayContent;->forAllWindows(Lcom/android/internal/util/ToBooleanFunction;Z)Z

    move-result v1

    return v1

    .line 1799
    .end local v0    # "displayContent":Lcom/android/server/wm/DisplayContent;
    :cond_2
    :goto_0
    return v2
.end method

.method public notifyFloatWindowScene(Ljava/lang/String;IZ)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "windowType"    # I
    .param p3, "status"    # Z

    .line 1520
    return-void
.end method

.method public notifySystemBrightnessChange()V
    .locals 2

    .line 1534
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 1544
    return-void
.end method

.method public notifyTaskFocusChange(Lcom/android/server/wm/ActivityRecord;)V
    .locals 4
    .param p1, "touchedActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 1482
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-nez v0, :cond_0

    goto :goto_0

    .line 1485
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 1486
    .local v0, "componentName":Landroid/content/ComponentName;
    if-eqz v0, :cond_1

    .line 1487
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyTaskFocusChange(Ljava/lang/String;Ljava/lang/String;)V

    .line 1489
    :cond_1
    return-void

    .line 1483
    .end local v0    # "componentName":Landroid/content/ComponentName;
    :cond_2
    :goto_0
    return-void
.end method

.method public notifyTouchFromNative(Z)V
    .locals 0
    .param p1, "isTouched"    # Z

    .line 1473
    return-void
.end method

.method public onAnimationFinished()V
    .locals 3

    .line 1364
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mUiModeAnimFinishedCallback:Landroid/view/IWindowAnimationFinishedCallback;

    if-eqz v0, :cond_0

    .line 1365
    invoke-interface {v0}, Landroid/view/IWindowAnimationFinishedCallback;->onWindowAnimFinished()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1369
    :cond_0
    goto :goto_0

    .line 1367
    :catch_0
    move-exception v0

    .line 1368
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call mUiModeAnimFinishedCallback.onWindowAnimFinished error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WindowManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1370
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public onFinishTransition()V
    .locals 1

    .line 1852
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->miuiHoverModeInternal:Lcom/android/server/wm/MiuiHoverModeInternal;

    if-eqz v0, :cond_0

    .line 1853
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->onFinishTransition()V

    .line 1855
    :cond_0
    return-void
.end method

.method public onHoverModeRecentAnimStart()V
    .locals 1

    .line 1846
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->miuiHoverModeInternal:Lcom/android/server/wm/MiuiHoverModeInternal;

    if-eqz v0, :cond_0

    .line 1847
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->onHoverModeRecentAnimStart()V

    .line 1849
    :cond_0
    return-void
.end method

.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 1496
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    if-eqz v0, :cond_3

    if-nez p1, :cond_0

    goto :goto_1

    .line 1499
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 1500
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    invoke-virtual {v0, v1}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyTouchStatus(Z)V

    goto :goto_0

    .line 1501
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 1502
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyTouchStatus(Z)V

    .line 1504
    :cond_2
    :goto_0
    return-void

    .line 1497
    :cond_3
    :goto_1
    return-void
.end method

.method public onScreenRotationAnimationEnd()V
    .locals 1

    .line 1870
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->miuiHoverModeInternal:Lcom/android/server/wm/MiuiHoverModeInternal;

    if-eqz v0, :cond_0

    .line 1871
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->onScreenRotationAnimationEnd()V

    .line 1873
    :cond_0
    return-void
.end method

.method public onSecureChangedListener(Lcom/android/server/wm/WindowState;Z)V
    .locals 6
    .param p1, "win"    # Lcom/android/server/wm/WindowState;
    .param p2, "ignoreActivityState"    # Z

    .line 1259
    if-eqz p1, :cond_7

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    if-eqz v0, :cond_7

    .line 1260
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x2000

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 1261
    .local v0, "hasSecure":Z
    :goto_0
    iget-object v3, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    sget-object v4, Lcom/android/server/wm/ActivityRecord$State;->RESUMED:Lcom/android/server/wm/ActivityRecord$State;

    .line 1262
    invoke-virtual {v3, v4}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    sget-object v4, Lcom/android/server/wm/ActivityRecord$State;->STARTED:Lcom/android/server/wm/ActivityRecord$State;

    invoke-virtual {v3, v4}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 1263
    .local v1, "isResumeOrStart":Z
    :cond_2
    :goto_1
    iget-boolean v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mLastWindowHasSecure:Z

    if-eq v0, v2, :cond_7

    if-nez p2, :cond_3

    if-eqz v1, :cond_7

    .line 1265
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->topRunningActivityChange(Lcom/android/server/wm/WindowState;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1266
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v2

    const/16 v3, 0x7e1

    if-eq v2, v3, :cond_4

    .line 1267
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v2

    const/16 v3, 0x7f8

    if-ne v2, v3, :cond_7

    .line 1268
    :cond_4
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_7

    .line 1269
    iput-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mLastWindowHasSecure:Z

    .line 1270
    const-string v2, "WindowManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSecureChanged, current win = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", hasSecure = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v2

    .line 1272
    :try_start_0
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecureChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1273
    monitor-exit v2

    return-void

    .line 1275
    :cond_5
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecureChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/IWindowSecureChangeListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1277
    .local v4, "listener":Landroid/app/IWindowSecureChangeListener;
    :try_start_1
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Landroid/app/IWindowSecureChangeListener;->onSecureChangeCallback(Ljava/lang/String;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1280
    goto :goto_3

    .line 1278
    :catch_0
    move-exception v5

    .line 1279
    .local v5, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1281
    .end local v4    # "listener":Landroid/app/IWindowSecureChangeListener;
    .end local v5    # "e":Landroid/os/RemoteException;
    :goto_3
    goto :goto_2

    .line 1282
    :cond_6
    monitor-exit v2

    goto :goto_4

    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 1285
    .end local v0    # "hasSecure":Z
    .end local v1    # "isResumeOrStart":Z
    :cond_7
    :goto_4
    return-void
.end method

.method public onSettingsObserverChange(ZLandroid/net/Uri;)Z
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 1304
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDarkModeEnable:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDarkModeContrastEnable:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContrastAlphaUri:Landroid/net/Uri;

    .line 1305
    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1311
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 1306
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateContrast : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WindowManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1309
    const/4 v0, 0x1

    return v0
.end method

.method public onStopFreezingDisplayLocked()V
    .locals 1

    .line 1858
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->miuiHoverModeInternal:Lcom/android/server/wm/MiuiHoverModeInternal;

    if-eqz v0, :cond_0

    .line 1859
    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->onStopFreezingDisplayLocked()V

    .line 1861
    :cond_0
    return-void
.end method

.method public onSystemReady()V
    .locals 2

    .line 1826
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    iput-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->miuiHoverModeInternal:Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 1827
    if-eqz v0, :cond_0

    .line 1828
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiHoverModeInternal;->onSystemReady(Lcom/android/server/wm/WindowManagerService;)V

    .line 1830
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->setDefaultBlurConfigIfNeeded(Landroid/content/Context;)V

    .line 1831
    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 1
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I

    .line 742
    const/16 v0, 0xff

    if-ne p1, v0, :cond_0

    .line 743
    const-string v0, "android.view.IWindowManager"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 744
    invoke-virtual {p0, p2, p3, p4}, Lcom/android/server/wm/WindowManagerServiceImpl;->switchResolution(Landroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 745
    :cond_0
    nop

    .line 748
    const/4 v0, 0x0

    return v0
.end method

.method public onWindowRequestSize(IIILjava/lang/CharSequence;)V
    .locals 2
    .param p1, "ownerUid"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "tag"    # Ljava/lang/CharSequence;

    .line 1649
    invoke-static {p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_0

    if-lez p2, :cond_0

    if-lez p3, :cond_0

    .line 1651
    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->checkIfUnusualWindowEvent(ILjava/lang/String;II)V

    .line 1653
    :cond_0
    return-void
.end method

.method public positionContrastSurface(II)V
    .locals 0
    .param p1, "defaultDw"    # I
    .param p2, "defaultDh"    # I

    .line 1378
    return-void
.end method

.method public registerFlipWindowStateListener(Landroid/view/IFlipWindowStateListener;)V
    .locals 4
    .param p1, "listener"    # Landroid/view/IFlipWindowStateListener;

    .line 2112
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mFlipListenerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2113
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    invoke-interface {p1}, Landroid/view/IFlipWindowStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2114
    const-string v1, "WindowManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerNavigationBarColorListener binder = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Landroid/view/IFlipWindowStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    .line 2115
    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2114
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2116
    monitor-exit v0

    .line 2117
    return-void

    .line 2116
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerSettingsObserver(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService$SettingsObserver;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "settingsObserver"    # Lcom/android/server/wm/WindowManagerService$SettingsObserver;

    .line 1298
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1299
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDarkModeEnable:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1300
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDarkModeContrastEnable:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1301
    return-void
.end method

.method public registerUiModeAnimFinishedCallback(Landroid/view/IWindowAnimationFinishedCallback;)V
    .locals 0
    .param p1, "callback"    # Landroid/view/IWindowAnimationFinishedCallback;

    .line 1359
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mUiModeAnimFinishedCallback:Landroid/view/IWindowAnimationFinishedCallback;

    .line 1360
    return-void
.end method

.method public removeFlipWindowStateListener(Landroid/view/IFlipWindowStateListener;)V
    .locals 4
    .param p1, "listener"    # Landroid/view/IFlipWindowStateListener;

    .line 2121
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mFlipListenerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2122
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    invoke-interface {p1}, Landroid/view/IFlipWindowStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123
    const-string v1, "WindowManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeNavigationBarColorListener binder = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Landroid/view/IFlipWindowStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    .line 2124
    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2123
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2125
    monitor-exit v0

    .line 2126
    return-void

    .line 2125
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeMiuiPaperContrastOverlay([Landroid/view/SurfaceControl;)V
    .locals 3
    .param p1, "excludeLayers"    # [Landroid/view/SurfaceControl;

    .line 1925
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiPaperContrastOverlay:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    .line 1926
    return-void

    .line 1928
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 1929
    aget-object v1, p1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiPaperContrastOverlay:Landroid/view/SurfaceControl;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1930
    const/4 v1, 0x0

    aput-object v1, p1, v0

    .line 1931
    iput-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiPaperContrastOverlay:Landroid/view/SurfaceControl;

    .line 1928
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1934
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method public removeSecureChangedListener(Landroid/app/IWindowSecureChangeListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/app/IWindowSecureChangeListener;

    .line 1251
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 1252
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecureChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1253
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSecureChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1255
    :cond_0
    monitor-exit v0

    .line 1256
    return-void

    .line 1255
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public saveNavigationBarColor(I)V
    .locals 7
    .param p1, "color"    # I

    .line 2012
    iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationColor:I

    if-eq p1, v0, :cond_2

    .line 2013
    iput p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationColor:I

    .line 2014
    const-string v0, "WindowManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "before notify flip navigation bar color change, color = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2015
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 2016
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 2017
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/IFlipWindowStateListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2019
    .local v2, "listener":Landroid/view/IFlipWindowStateListener;
    :try_start_1
    invoke-interface {v2, p1}, Landroid/view/IFlipWindowStateListener;->onNavigationColorChange(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2028
    goto :goto_1

    .line 2020
    :catch_0
    move-exception v3

    .line 2021
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_2
    instance-of v4, v3, Landroid/os/DeadObjectException;

    if-eqz v4, :cond_0

    .line 2022
    iget-object v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    invoke-virtual {v4, v1}, Landroid/util/ArrayMap;->removeAt(I)Ljava/lang/Object;

    .line 2023
    const-string v4, "WindowManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onNavigationColorChange binder died, remove it, key = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationBarColorListenerMap:Landroid/util/ArrayMap;

    .line 2024
    invoke-virtual {v6, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2023
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2026
    :cond_0
    const-string v4, "WindowManagerService"

    const-string v5, "onNavigationColorChange fail"

    invoke-static {v4, v5, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2016
    .end local v2    # "listener":Landroid/view/IFlipWindowStateListener;
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2030
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 2032
    :cond_2
    :goto_2
    return-void
.end method

.method public setCloudDimControllerList(Ljava/lang/String;)V
    .locals 7
    .param p1, "DimCloudConfig"    # Ljava/lang/String;

    .line 1195
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1196
    .local v0, "DimConfigArray":[Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimUserTimeoutMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 1197
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimNeedAssistMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 1198
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 1199
    aget-object v2, v0, v1

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1200
    .local v2, "DimConfigTotal":[Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimUserTimeoutMap:Ljava/util/HashMap;

    const/4 v4, 0x0

    aget-object v5, v2, v4

    const/4 v6, 0x1

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1201
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mDimNeedAssistMap:Ljava/util/HashMap;

    aget-object v4, v2, v4

    const/4 v5, 0x2

    aget-object v5, v2, v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198
    .end local v2    # "DimConfigTotal":[Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1203
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method public setRunningRecentsAnimation(Z)V
    .locals 0
    .param p1, "running"    # Z

    .line 1632
    iput-boolean p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mRunningRecentsAnimation:Z

    .line 1633
    return-void
.end method

.method public setSplittable(Z)V
    .locals 0
    .param p1, "splittable"    # Z

    .line 1143
    iput-boolean p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSplitMode:Z

    .line 1144
    return-void
.end method

.method public setTransitionReadyState(IZ)V
    .locals 2
    .param p1, "syncId"    # I
    .param p2, "ready"    # Z

    .line 1937
    if-eqz p2, :cond_0

    .line 1938
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTransitionReadyList:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1939
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTransitionReadyList:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1942
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTransitionReadyList:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1944
    :cond_1
    :goto_0
    return-void
.end method

.method public shouldApplyOverrideBrightness(Lcom/android/server/wm/WindowState;)Z
    .locals 5
    .param p1, "w"    # Lcom/android/server/wm/WindowState;

    .line 1569
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 1570
    .local v0, "task":Lcom/android/server/wm/Task;
    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 1571
    return v1

    .line 1573
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    iget v3, v0, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v2

    .line 1574
    .local v2, "index":I
    if-gez v2, :cond_1

    .line 1575
    return v1

    .line 1577
    :cond_1
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v4, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlackList:Ljava/util/List;

    .line 1578
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1579
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "shouldApplyOverrideBrightness: mTaskIdScreenBrightnessOverrides="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " taskId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "WindowManagerService"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1581
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mTaskIdScreenBrightnessOverrides:Landroid/util/SparseArray;

    iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->delete(I)V

    .line 1582
    return v1

    .line 1584
    :cond_2
    const/4 v1, 0x0

    return v1
.end method

.method public shouldApplyOverrideBrightnessForPip(Lcom/android/server/wm/WindowState;)Z
    .locals 4
    .param p1, "w"    # Lcom/android/server/wm/WindowState;

    .line 1993
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    const/4 v1, 0x0

    if-ltz v0, :cond_4

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 1994
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isVisibleRequested()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->inTransition()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1998
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-boolean v0, v0, Lcom/android/server/wm/ActivityRecord;->mWaitForEnteringPinnedMode:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->inPinnedWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1999
    :cond_1
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mTransitionController:Lcom/android/server/wm/TransitionController;

    invoke-virtual {v0}, Lcom/android/server/wm/TransitionController;->getCollectingTransition()Lcom/android/server/wm/Transition;

    move-result-object v0

    .line 2000
    .local v0, "transition":Lcom/android/server/wm/Transition;
    if-eqz v0, :cond_3

    iget v2, v0, Lcom/android/server/wm/Transition;->mType:I

    const/16 v3, 0xa

    if-eq v2, v3, :cond_2

    iget v2, v0, Lcom/android/server/wm/Transition;->mType:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_3

    .line 2002
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Override brightness when "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is entering PIP."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WindowManagerService"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2004
    const/4 v1, 0x1

    return v1

    .line 2007
    .end local v0    # "transition":Lcom/android/server/wm/Transition;
    :cond_3
    return v1

    .line 1995
    :cond_4
    :goto_0
    return v1
.end method

.method public showPinnedTaskIfNeeded(Lcom/android/server/wm/Task;)V
    .locals 2
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 1636
    iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mRunningRecentsAnimation:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isRootTask()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1637
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getPendingTransaction()Landroid/view/SurfaceControl$Transaction;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/wm/Task;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 1638
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "show pinned task surface: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WindowManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1640
    :cond_0
    return-void
.end method

.method switchResolution(Landroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "data"    # Landroid/os/Parcel;
    .param p2, "reply"    # Landroid/os/Parcel;
    .param p3, "flags"    # I

    .line 767
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_1

    .line 770
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 771
    .local v0, "displayId":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 772
    .local v1, "width":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 774
    .local v2, "height":I
    if-nez v0, :cond_0

    .line 778
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3

    .line 780
    .local v3, "ident":J
    :try_start_0
    invoke-direct {p0, v1, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->switchResolutionInternal(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 782
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 783
    nop

    .line 784
    invoke-virtual {p2}, Landroid/os/Parcel;->writeNoException()V

    .line 785
    const/4 v5, 0x1

    return v5

    .line 782
    :catchall_0
    move-exception v5

    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 783
    throw v5

    .line 775
    .end local v3    # "ident":J
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Can only set the default display"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 768
    .end local v0    # "displayId":I
    .end local v1    # "width":I
    .end local v2    # "height":I
    :cond_1
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Only system uid can switch resolution"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public trackScreenData(ILjava/lang/String;)V
    .locals 1
    .param p1, "wakefulness"    # I
    .param p2, "details"    # Ljava/lang/String;

    .line 2180
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->getInstance()Lcom/android/server/wm/OneTrackRotationHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->trackScreenData(ILjava/lang/String;)V

    .line 2181
    return-void
.end method

.method public updateBlurWallpaperBmp()V
    .locals 6

    .line 402
    const-class v0, Lcom/android/server/wallpaper/WallpaperManagerInternal;

    .line 403
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wallpaper/WallpaperManagerInternal;

    .line 404
    .local v0, "wpMgr":Lcom/android/server/wallpaper/WallpaperManagerInternal;
    const-string v1, "WindowManagerService"

    if-nez v0, :cond_0

    .line 405
    const-string v2, "WallpaperManagerInternal is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    return-void

    .line 408
    :cond_0
    const/4 v2, 0x0

    .line 410
    .local v2, "fd":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    iget-object v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWallpaperCallback:Landroid/app/IWallpaperManagerCallback;

    invoke-virtual {v0, v3}, Lcom/android/server/wallpaper/WallpaperManagerInternal;->getBlurWallpaper(Landroid/app/IWallpaperManagerCallback;)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    move-object v2, v3

    .line 412
    if-eqz v2, :cond_2

    .line 413
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 414
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    .line 416
    invoke-static {}, Lcom/android/server/wm/BoundsCompatControllerStub;->newInstance()Lcom/android/server/wm/BoundsCompatControllerStub;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/wm/BoundsCompatControllerStub;->isDebugEnable()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 417
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "decodeFileDescriptor, mBlurWallpaperBmp = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mBlurWallpaperBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    :cond_1
    invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->updateAppearance()V

    .line 423
    .end local v3    # "options":Landroid/graphics/BitmapFactory$Options;
    goto :goto_0

    .line 424
    :cond_2
    const-string v3, "getWallpaper, fd is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 429
    :goto_0
    goto :goto_1

    :catchall_0
    move-exception v1

    goto :goto_2

    .line 426
    :catch_0
    move-exception v3

    .line 427
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v4, "getWallpaper wrong"

    invoke-static {v1, v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 429
    nop

    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-static {v2}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 430
    nop

    .line 431
    return-void

    .line 429
    :goto_2
    invoke-static {v2}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 430
    throw v1
.end method

.method public updateContrastAlpha(Z)V
    .locals 2
    .param p1, "darkmode"    # Z

    .line 1315
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$5;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl$5;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;Z)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 1349
    return-void
.end method

.method public updateScreenShareProjectFlag()V
    .locals 2

    .line 1384
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mH:Lcom/android/server/wm/ActivityTaskManagerService$H;

    new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 1403
    return-void
.end method

.method public updateSurfaceParentIfNeed(Lcom/android/server/wm/WindowState;)V
    .locals 7
    .param p1, "ws"    # Lcom/android/server/wm/WindowState;

    .line 1604
    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    if-eqz v0, :cond_5

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 1606
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 1607
    .local v0, "topRootTask1":Lcom/android/server/wm/Task;
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v1

    .line 1608
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;

    move-result-object v1

    .line 1610
    .local v1, "topRootTask2":Lcom/android/server/wm/Task;
    const/4 v2, 0x0

    .line 1612
    .local v2, "pairRootTask":Lcom/android/server/wm/Task;
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1613
    move-object v2, v0

    goto :goto_0

    .line 1614
    :cond_1
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1615
    move-object v2, v1

    .line 1618
    :cond_2
    :goto_0
    if-nez v2, :cond_3

    return-void

    .line 1620
    :cond_3
    const-string v3, "ClientDimmerForAppPair"

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 1621
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1622
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 1623
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1624
    new-instance v3, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v3}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    .line 1625
    .local v3, "t":Landroid/view/SurfaceControl$Transaction;
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v4

    iget-object v5, v2, Lcom/android/server/wm/Task;->mSurfaceControl:Landroid/view/SurfaceControl;

    const v6, 0x7fffffff

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/SurfaceControl$Transaction;->setRelativeLayer(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    .line 1627
    invoke-virtual {v3}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 1629
    .end local v3    # "t":Landroid/view/SurfaceControl$Transaction;
    :cond_4
    return-void

    .line 1604
    .end local v0    # "topRootTask1":Lcom/android/server/wm/Task;
    .end local v1    # "topRootTask2":Lcom/android/server/wm/Task;
    .end local v2    # "pairRootTask":Lcom/android/server/wm/Task;
    :cond_5
    :goto_1
    return-void
.end method
