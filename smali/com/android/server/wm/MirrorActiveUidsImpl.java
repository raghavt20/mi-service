public class com.android.server.wm.MirrorActiveUidsImpl extends com.android.server.wm.MirrorActiveUidsStub {
	 /* .source "MirrorActiveUidsImpl.java" */
	 /* # instance fields */
	 private final android.util.SparseIntArray mNumAppVisibleWindowForUserMap;
	 /* # direct methods */
	 public com.android.server.wm.MirrorActiveUidsImpl ( ) {
		 /* .locals 1 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/MirrorActiveUidsStub;-><init>()V */
		 /* .line 12 */
		 /* new-instance v0, Landroid/util/SparseIntArray; */
		 /* invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V */
		 this.mNumAppVisibleWindowForUserMap = v0;
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
		 /* .locals 3 */
		 /* .param p1, "pw" # Ljava/io/PrintWriter; */
		 /* .param p2, "prefix" # Ljava/lang/String; */
		 /* .line 36 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v1 = "mNumAppVisibleWindowForUserMap:["; // const-string v1, "mNumAppVisibleWindowForUserMap:["
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
		 /* .line 37 */
		 v0 = this.mNumAppVisibleWindowForUserMap;
		 v0 = 		 (( android.util.SparseIntArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I
		 /* add-int/lit8 v0, v0, -0x1 */
		 /* .local v0, "i":I */
	 } // :goto_0
	 /* if-ltz v0, :cond_0 */
	 /* .line 38 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = " "; // const-string v2, " "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.mNumAppVisibleWindowForUserMap;
	 v2 = 	 (( android.util.SparseIntArray ) v2 ).keyAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->keyAt(I)I
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v2 = ":"; // const-string v2, ":"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.mNumAppVisibleWindowForUserMap;
	 /* .line 39 */
	 v2 = 	 (( android.util.SparseIntArray ) v2 ).valueAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->valueAt(I)I
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 38 */
	 (( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 37 */
	 /* add-int/lit8 v0, v0, -0x1 */
	 /* .line 41 */
} // .end local v0 # "i":I
} // :cond_0
final String v0 = "]"; // const-string v0, "]"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 42 */
return;
} // .end method
public Boolean hasVisibleWindowForUser ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 16 */
v0 = this.mNumAppVisibleWindowForUserMap;
v0 = (( android.util.SparseIntArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void onVisibleWindowForUserChanged ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "visible" # Z */
/* .line 21 */
v0 = this.mNumAppVisibleWindowForUserMap;
v0 = (( android.util.SparseIntArray ) v0 ).indexOfKey ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I
/* .line 22 */
/* .local v0, "index":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ltz v0, :cond_2 */
/* .line 23 */
v2 = this.mNumAppVisibleWindowForUserMap;
v2 = (( android.util.SparseIntArray ) v2 ).valueAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->valueAt(I)I
if ( p2 != null) { // if-eqz p2, :cond_0
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_0
/* add-int/2addr v2, v1 */
/* .line 24 */
/* .local v2, "num":I */
/* if-lez v2, :cond_1 */
/* .line 25 */
v1 = this.mNumAppVisibleWindowForUserMap;
(( android.util.SparseIntArray ) v1 ).setValueAt ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/util/SparseIntArray;->setValueAt(II)V
/* .line 27 */
} // :cond_1
v1 = this.mNumAppVisibleWindowForUserMap;
(( android.util.SparseIntArray ) v1 ).removeAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->removeAt(I)V
/* .line 29 */
} // .end local v2 # "num":I
} // :cond_2
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 30 */
v2 = this.mNumAppVisibleWindowForUserMap;
(( android.util.SparseIntArray ) v2 ).append ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Landroid/util/SparseIntArray;->append(II)V
/* .line 29 */
} // :cond_3
} // :goto_1
/* nop */
/* .line 32 */
} // :goto_2
return;
} // .end method
