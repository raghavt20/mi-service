public class com.android.server.wm.DisplayContentStubImpl implements com.android.server.wm.DisplayContentStub {
	 /* .source "DisplayContentStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;, */
	 /* Lcom/android/server/wm/DisplayContentStubImpl$MutableDisplayContentImpl; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DESCRIPTOR;
private static final java.util.ArrayList DISPLAY_DISABLE_SYSTEM_ANIMATION;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final Integer FOCUSED_WINDOW_RETRY_TIME;
private static final Integer FPS_COMMON;
private static final Integer NO_FOCUS_WINDOW_TIMEOUT;
private static final java.lang.String PROVISION_CONGRATULATION_ACTIVITY;
private static final java.lang.String PROVISION_DEFAULT_ACTIVITY;
private static final Integer SCREEN_DPI_MODE;
private static final java.util.ArrayList WHITE_LIST_ALLOW_FIXED_ROTATION_FOR_NOT_OCCLUDES_PARENT;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static Integer sCurrentRefreshRate;
public static Integer sLastUserRefreshRate;
/* # instance fields */
private final java.lang.String KEY_BLUR_ENABLE;
private final java.lang.String TAG;
private final Boolean blurEnabled;
private Integer mBlur;
private android.content.Context mContext;
private com.android.server.wm.DisplayContentStubImpl$UpdateFocusRunnable mFocusRunnable;
private java.util.HashSet mHiddenWindowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Lcom/android/server/wm/WindowState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mIsCameraRotating;
private com.android.server.PowerConsumptionServiceInternal mPowerConsumptionServiceInternal;
private Boolean mSkipUpdateSurfaceRotation;
private Boolean mSkipVisualDisplayTransition;
private Boolean mStatusBarVisible;
private Long mTimestamp;
/* # direct methods */
public static Integer $r8$lambda$2tFncwLoQkNxfq66P9eO3es2Sms ( com.android.server.wm.WindowToken p0 ) { //synthethic
/* .locals 0 */
p0 = (( com.android.server.wm.WindowToken ) p0 ).getWindowLayerFromType ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowToken;->getWindowLayerFromType()I
} // .end method
public static void $r8$lambda$VG-blJgiOwYfKhnQUYIgLfpxRUg ( com.android.server.wm.DisplayContentStubImpl p0, com.android.server.wm.WindowState p1, Long p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/DisplayContentStubImpl;->lambda$focusChangedLw$0(Lcom/android/server/wm/WindowState;J)V */
return;
} // .end method
static java.util.ArrayList -$$Nest$sfgetDISPLAY_DISABLE_SYSTEM_ANIMATION ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.DisplayContentStubImpl.DISPLAY_DISABLE_SYSTEM_ANIMATION;
} // .end method
static java.util.ArrayList -$$Nest$sfgetWHITE_LIST_ALLOW_FIXED_ROTATION_FOR_NOT_OCCLUDES_PARENT ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.DisplayContentStubImpl.WHITE_LIST_ALLOW_FIXED_ROTATION_FOR_NOT_OCCLUDES_PARENT;
} // .end method
static com.android.server.wm.DisplayContentStubImpl ( ) {
/* .locals 3 */
/* .line 94 */
int v0 = -1; // const/4 v0, -0x1
/* .line 95 */
/* .line 115 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 118 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 122 */
final String v2 = "com.miui.mediaviewer/com.miui.video.gallery.galleryvideo.FrameLocalPlayActivity"; // const-string v2, "com.miui.mediaviewer/com.miui.video.gallery.galleryvideo.FrameLocalPlayActivity"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 124 */
final String v2 = "com.miui.gallery/.activity.InternalPhotoPageActivity"; // const-string v2, "com.miui.gallery/.activity.InternalPhotoPageActivity"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 126 */
final String v0 = "com.miui.carlink"; // const-string v0, "com.miui.carlink"
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 127 */
final String v0 = "com.xiaomi.ucar.minimap"; // const-string v0, "com.xiaomi.ucar.minimap"
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 128 */
return;
} // .end method
public com.android.server.wm.DisplayContentStubImpl ( ) {
/* .locals 2 */
/* .line 92 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 93 */
final String v0 = "DisplayContentStubImpl"; // const-string v0, "DisplayContentStubImpl"
this.TAG = v0;
/* .line 99 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mStatusBarVisible:Z */
/* .line 101 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipVisualDisplayTransition:Z */
/* .line 102 */
final String v1 = "persist.sys.background_blur_status_default"; // const-string v1, "persist.sys.background_blur_status_default"
v1 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* iput-boolean v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->blurEnabled:Z */
/* .line 104 */
final String v1 = "background_blur_enable"; // const-string v1, "background_blur_enable"
this.KEY_BLUR_ENABLE = v1;
/* .line 107 */
int v1 = 0; // const/4 v1, 0x0
this.mFocusRunnable = v1;
/* .line 108 */
/* iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipUpdateSurfaceRotation:Z */
/* .line 109 */
/* iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mIsCameraRotating:Z */
/* .line 110 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mHiddenWindowList = v0;
/* .line 190 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J */
return;
} // .end method
private void computeBlurConfiguration ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .line 216 */
/* if-nez p1, :cond_0 */
return;
/* .line 217 */
} // :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "background_blur_enable"; // const-string v1, "background_blur_enable"
int v2 = -1; // const/4 v2, -0x1
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* .line 218 */
/* .local v0, "blur":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 219 */
/* iput v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I */
/* .line 220 */
} // :cond_1
/* if-nez v0, :cond_2 */
/* .line 221 */
int v1 = 2; // const/4 v1, 0x2
/* iput v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I */
/* .line 222 */
} // :cond_2
/* iget-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->blurEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 223 */
/* iput v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I */
/* .line 225 */
} // :cond_3
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I */
/* .line 227 */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "computeBlurConfiguration blurEnabled= "; // const-string v2, "computeBlurConfiguration blurEnabled= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->blurEnabled:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " blur= "; // const-string v2, " blur= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mBlur= "; // const-string v2, " mBlur= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "DisplayContentStubImpl"; // const-string v2, "DisplayContentStubImpl"
android.util.Slog .d ( v2,v1 );
/* .line 228 */
return;
} // .end method
private static Integer getCurrentRefreshRate ( ) {
/* .locals 4 */
/* .line 284 */
int v0 = -1; // const/4 v0, -0x1
/* .line 285 */
/* .local v0, "refreshRate":I */
/* nop */
/* .line 286 */
final String v1 = "persist.vendor.dfps.level"; // const-string v1, "persist.vendor.dfps.level"
/* const/16 v2, 0x3c */
v1 = android.os.SystemProperties .getInt ( v1,v2 );
/* .line 287 */
/* .local v1, "fps":I */
/* nop */
/* .line 288 */
final String v2 = "persist.vendor.power.dfps.level"; // const-string v2, "persist.vendor.power.dfps.level"
int v3 = 0; // const/4 v3, 0x0
v2 = android.os.SystemProperties .getInt ( v2,v3 );
/* .line 289 */
/* .local v2, "powerFps":I */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 290 */
/* move v0, v2 */
/* .line 292 */
} // :cond_0
/* move v0, v1 */
/* .line 294 */
} // :goto_0
} // .end method
static Integer getFullScreenIndex ( com.android.server.wm.Task p0, com.android.server.wm.WindowList p1, Integer p2 ) {
/* .locals 5 */
/* .param p0, "stack" # Lcom/android/server/wm/Task; */
/* .param p2, "targetPosition" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/wm/Task;", */
/* "Lcom/android/server/wm/WindowList<", */
/* "Lcom/android/server/wm/Task;", */
/* ">;I)I" */
/* } */
} // .end annotation
/* .line 237 */
/* .local p1, "children":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<Lcom/android/server/wm/Task;>;" */
v0 = (( com.android.server.wm.Task ) p0 ).getWindowingMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 238 */
(( com.android.server.wm.WindowList ) p1 ).iterator ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/wm/Task; */
/* .line 239 */
/* .local v2, "tStack":Lcom/android/server/wm/Task; */
v3 = (( com.android.server.wm.Task ) v2 ).getWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v4 = 5; // const/4 v4, 0x5
/* if-ne v3, v4, :cond_0 */
/* .line 240 */
v3 = (( com.android.server.wm.WindowList ) p1 ).indexOf ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/wm/WindowList;->indexOf(Ljava/lang/Object;)I
/* if-lez v3, :cond_0 */
/* .line 241 */
v0 = (( com.android.server.wm.WindowList ) p1 ).indexOf ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/wm/WindowList;->indexOf(Ljava/lang/Object;)I
/* add-int/lit8 p2, v0, -0x1 */
/* .line 242 */
/* .line 244 */
} // .end local v2 # "tStack":Lcom/android/server/wm/Task;
} // :cond_0
/* .line 246 */
} // :cond_1
} // :goto_1
} // .end method
static Integer getFullScreenIndex ( Boolean p0, com.android.server.wm.Task p1, com.android.server.wm.WindowList p2, Integer p3, Boolean p4 ) {
/* .locals 4 */
/* .param p0, "toTop" # Z */
/* .param p1, "stack" # Lcom/android/server/wm/Task; */
/* .param p3, "targetPosition" # I */
/* .param p4, "adding" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z", */
/* "Lcom/android/server/wm/Task;", */
/* "Lcom/android/server/wm/WindowList<", */
/* "Lcom/android/server/wm/Task;", */
/* ">;IZ)I" */
/* } */
} // .end annotation
/* .line 251 */
/* .local p2, "children":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<Lcom/android/server/wm/Task;>;" */
if ( p0 != null) { // if-eqz p0, :cond_3
v0 = (( com.android.server.wm.Task ) p1 ).getWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_3 */
/* .line 252 */
(( com.android.server.wm.WindowList ) p2 ).iterator ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Lcom/android/server/wm/Task; */
/* .line 253 */
/* .local v1, "tStack":Lcom/android/server/wm/Task; */
v2 = (( com.android.server.wm.Task ) v1 ).getWindowingMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v3 = 5; // const/4 v3, 0x5
/* if-ne v2, v3, :cond_2 */
/* .line 254 */
v0 = (( com.android.server.wm.WindowList ) p2 ).indexOf ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/wm/WindowList;->indexOf(Ljava/lang/Object;)I
/* .line 255 */
/* .local v0, "topChildPosition":I */
if ( p4 != null) { // if-eqz p4, :cond_0
/* move v2, v0 */
} // :cond_0
/* if-lez v0, :cond_1 */
/* add-int/lit8 v2, v0, -0x1 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :goto_1
/* move p3, v2 */
/* .line 256 */
/* .line 258 */
} // .end local v0 # "topChildPosition":I
} // .end local v1 # "tStack":Lcom/android/server/wm/Task;
} // :cond_2
/* .line 260 */
} // :cond_3
} // :goto_2
} // .end method
private android.graphics.Rect getInputMethodWindowVisibleRegion ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 9 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 335 */
(( com.android.server.wm.DisplayContent ) p1 ).getInsetsStateController ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getInsetsStateController()Lcom/android/server/wm/InsetsStateController;
(( com.android.server.wm.InsetsStateController ) v0 ).getRawInsetsState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/InsetsStateController;->getRawInsetsState()Landroid/view/InsetsState;
/* .line 336 */
/* .local v0, "state":Landroid/view/InsetsState; */
(( android.view.InsetsState ) v0 ).peekSource ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/InsetsState;->peekSource(I)Landroid/view/InsetsSource;
/* .line 337 */
/* .local v1, "imeSource":Landroid/view/InsetsSource; */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
v3 = (( android.view.InsetsSource ) v1 ).isVisible ( ); // invoke-virtual {v1}, Landroid/view/InsetsSource;->isVisible()Z
/* if-nez v3, :cond_0 */
/* .line 340 */
} // :cond_0
(( android.view.InsetsSource ) v1 ).getVisibleFrame ( ); // invoke-virtual {v1}, Landroid/view/InsetsSource;->getVisibleFrame()Landroid/graphics/Rect;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 341 */
(( android.view.InsetsSource ) v1 ).getVisibleFrame ( ); // invoke-virtual {v1}, Landroid/view/InsetsSource;->getVisibleFrame()Landroid/graphics/Rect;
} // :cond_1
(( android.view.InsetsSource ) v1 ).getFrame ( ); // invoke-virtual {v1}, Landroid/view/InsetsSource;->getFrame()Landroid/graphics/Rect;
/* .line 342 */
/* .local v3, "imeFrame":Landroid/graphics/Rect; */
} // :goto_0
final String v4 = "mTmpRect"; // const-string v4, "mTmpRect"
/* const-class v5, Landroid/graphics/Rect; */
miui.util.ReflectionUtils .tryGetObjectField ( p1,v4,v5 );
(( miui.util.ObjectReference ) v4 ).get ( ); // invoke-virtual {v4}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
/* check-cast v4, Landroid/graphics/Rect; */
/* .line 343 */
/* .local v4, "dockFrame":Landroid/graphics/Rect; */
(( android.view.InsetsState ) v0 ).getDisplayFrame ( ); // invoke-virtual {v0}, Landroid/view/InsetsState;->getDisplayFrame()Landroid/graphics/Rect;
(( android.graphics.Rect ) v4 ).set ( v5 ); // invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 344 */
v5 = android.view.WindowInsets$Type .systemBars ( );
v6 = android.view.WindowInsets$Type .displayCutout ( );
/* or-int/2addr v5, v6 */
(( android.view.InsetsState ) v0 ).calculateInsets ( v4, v5, v2 ); // invoke-virtual {v0, v4, v5, v2}, Landroid/view/InsetsState;->calculateInsets(Landroid/graphics/Rect;IZ)Landroid/graphics/Insets;
(( android.graphics.Rect ) v4 ).inset ( v2 ); // invoke-virtual {v4, v2}, Landroid/graphics/Rect;->inset(Landroid/graphics/Insets;)V
/* .line 346 */
/* new-instance v2, Landroid/graphics/Rect; */
/* iget v5, v4, Landroid/graphics/Rect;->left:I */
/* iget v6, v3, Landroid/graphics/Rect;->top:I */
/* iget v7, v4, Landroid/graphics/Rect;->right:I */
/* iget v8, v4, Landroid/graphics/Rect;->bottom:I */
/* invoke-direct {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V */
/* .line 338 */
} // .end local v3 # "imeFrame":Landroid/graphics/Rect;
} // .end local v4 # "dockFrame":Landroid/graphics/Rect;
} // :cond_2
} // :goto_1
/* new-instance v3, Landroid/graphics/Rect; */
/* invoke-direct {v3, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V */
} // .end method
private Integer getSmallScreenLayout ( android.content.res.Configuration p0, com.android.server.wm.DisplayPolicy p1, Float p2, Boolean p3, Integer p4, Integer p5 ) {
/* .locals 11 */
/* .param p1, "outConfig" # Landroid/content/res/Configuration; */
/* .param p2, "mDisplayPolicy" # Lcom/android/server/wm/DisplayPolicy; */
/* .param p3, "density" # F */
/* .param p4, "rotated" # Z */
/* .param p5, "dw" # I */
/* .param p6, "dh" # I */
/* .line 569 */
if ( p4 != null) { // if-eqz p4, :cond_0
/* .line 570 */
/* move/from16 v0, p6 */
/* .line 571 */
/* .local v0, "unrotDw":I */
/* move/from16 v1, p5 */
/* .local v1, "unrotDh":I */
/* .line 573 */
} // .end local v0 # "unrotDw":I
} // .end local v1 # "unrotDh":I
} // :cond_0
/* move/from16 v0, p5 */
/* .line 574 */
/* .restart local v0 # "unrotDw":I */
/* move/from16 v1, p6 */
/* .line 576 */
/* .restart local v1 # "unrotDh":I */
} // :goto_0
/* move-object v9, p1 */
/* iget v2, v9, Landroid/content/res/Configuration;->screenLayout:I */
v10 = android.content.res.Configuration .resetScreenLayout ( v2 );
/* .line 577 */
/* .local v10, "sl":I */
int v5 = 0; // const/4 v5, 0x0
/* move-object v2, p0 */
/* move-object v3, p2 */
/* move v4, v10 */
/* move v6, p3 */
/* move v7, v0 */
/* move v8, v1 */
v10 = /* invoke-direct/range {v2 ..v8}, Lcom/android/server/wm/DisplayContentStubImpl;->reduceConfigLayout(Lcom/android/server/wm/DisplayPolicy;IIFII)I */
/* .line 578 */
int v5 = 1; // const/4 v5, 0x1
/* move v4, v10 */
/* move v7, v1 */
/* move v8, v0 */
v10 = /* invoke-direct/range {v2 ..v8}, Lcom/android/server/wm/DisplayContentStubImpl;->reduceConfigLayout(Lcom/android/server/wm/DisplayPolicy;IIFII)I */
/* .line 579 */
int v5 = 2; // const/4 v5, 0x2
/* move v4, v10 */
/* move v7, v0 */
/* move v8, v1 */
v10 = /* invoke-direct/range {v2 ..v8}, Lcom/android/server/wm/DisplayContentStubImpl;->reduceConfigLayout(Lcom/android/server/wm/DisplayPolicy;IIFII)I */
/* .line 580 */
int v5 = 3; // const/4 v5, 0x3
/* move v4, v10 */
/* move v7, v1 */
/* move v8, v0 */
v2 = /* invoke-direct/range {v2 ..v8}, Lcom/android/server/wm/DisplayContentStubImpl;->reduceConfigLayout(Lcom/android/server/wm/DisplayPolicy;IIFII)I */
/* .line 581 */
} // .end local v10 # "sl":I
/* .local v2, "sl":I */
} // .end method
private void lambda$focusChangedLw$0 ( com.android.server.wm.WindowState p0, Long p1 ) { //synthethic
/* .locals 0 */
/* .param p1, "focus" # Lcom/android/server/wm/WindowState; */
/* .param p2, "durationMillis" # J */
/* .line 379 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/DisplayContentStubImpl;->trackNoFocusWindowTimeout(Lcom/android/server/wm/WindowState;J)V */
return;
} // .end method
private Integer reduceConfigLayout ( com.android.server.wm.DisplayPolicy p0, Integer p1, Integer p2, Float p3, Integer p4, Integer p5 ) {
/* .locals 6 */
/* .param p1, "mDisplayPolicy" # Lcom/android/server/wm/DisplayPolicy; */
/* .param p2, "curLayout" # I */
/* .param p3, "rotation" # I */
/* .param p4, "density" # F */
/* .param p5, "dw" # I */
/* .param p6, "dh" # I */
/* .line 585 */
/* nop */
/* .line 586 */
(( com.android.server.wm.DisplayPolicy ) p1 ).getDecorInsetsInfo ( p3, p5, p6 ); // invoke-virtual {p1, p3, p5, p6}, Lcom/android/server/wm/DisplayPolicy;->getDecorInsetsInfo(III)Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info;
v0 = this.mNonDecorFrame;
/* .line 587 */
/* .local v0, "nonDecorSize":Landroid/graphics/Rect; */
v1 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
/* .line 588 */
/* .local v1, "w":I */
v2 = (( android.graphics.Rect ) v0 ).height ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
/* .line 591 */
/* .local v2, "h":I */
/* move v3, v1 */
/* .line 592 */
/* .local v3, "longSize":I */
/* move v4, v2 */
/* .line 593 */
/* .local v4, "shortSize":I */
/* if-ge v3, v4, :cond_0 */
/* .line 594 */
/* move v5, v3 */
/* .line 595 */
/* .local v5, "tmp":I */
/* move v3, v4 */
/* .line 596 */
/* move v4, v5 */
/* .line 598 */
} // .end local v5 # "tmp":I
} // :cond_0
/* int-to-float v5, v3 */
/* div-float/2addr v5, p4 */
/* float-to-int v3, v5 */
/* .line 599 */
/* int-to-float v5, v4 */
/* div-float/2addr v5, p4 */
/* float-to-int v4, v5 */
/* .line 600 */
v5 = android.content.res.Configuration .reduceScreenLayout ( p2,v3,v4 );
} // .end method
private static void setCurrentRefreshRate ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "fps" # I */
/* .line 298 */
/* .line 299 */
miui.hardware.display.DisplayFeatureManager .getInstance ( );
/* const/16 v1, 0x18 */
(( miui.hardware.display.DisplayFeatureManager ) v0 ).setScreenEffect ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V
/* .line 300 */
return;
} // .end method
private void trackNoFocusWindowTimeout ( com.android.server.wm.WindowState p0, Long p1 ) {
/* .locals 6 */
/* .param p1, "focus" # Lcom/android/server/wm/WindowState; */
/* .param p2, "durationMillis" # J */
/* .line 386 */
try { // :try_start_0
(( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* .line 387 */
/* .local v0, "packageName":Ljava/lang/String; */
(( com.android.server.wm.WindowState ) p1 ).getWindowTag ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;
/* .line 388 */
/* .local v1, "window":Ljava/lang/String; */
v2 = java.util.Objects .equals ( v1,v0 );
/* if-nez v2, :cond_0 */
v2 = (( java.lang.String ) v1 ).startsWith ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 389 */
v2 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* add-int/lit8 v2, v2, 0x1 */
(( java.lang.String ) v1 ).substring ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* move-object v1, v2 */
/* .line 392 */
} // :cond_0
/* new-instance v2, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* invoke-direct {v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V */
/* .line 393 */
/* .local v2, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* const/16 v3, 0x1b2 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v2 ).setType ( v3 ); // invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 394 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v2 ).setPackageName ( v0 ); // invoke-virtual {v2, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V
/* .line 395 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v2 ).setTimeStamp ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V
/* .line 396 */
final String v3 = "no focus window timeout"; // const-string v3, "no focus window timeout"
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v2 ).setSummary ( v3 ); // invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V
/* .line 397 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "window=" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " duration="; // const-string v4, " duration="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2, p3 ); // invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = "ms"; // const-string v4, "ms"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v2 ).setDetails ( v3 ); // invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 398 */
/* long-to-int v3, p2 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v2 ).setPid ( v3 ); // invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V
/* .line 399 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v3 ).reportGeneralException ( v2 ); // invoke-virtual {v3, v2}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z
/* .line 401 */
/* new-instance v3, Lcom/miui/misight/MiEvent; */
/* const v4, 0x35b4375d */
/* invoke-direct {v3, v4}, Lcom/miui/misight/MiEvent;-><init>(I)V */
/* .line 402 */
/* .local v3, "miEvent":Lcom/miui/misight/MiEvent; */
final String v4 = "PackageName"; // const-string v4, "PackageName"
(( com.miui.misight.MiEvent ) v3 ).addStr ( v4, v0 ); // invoke-virtual {v3, v4, v0}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 403 */
final String v4 = "WindowName"; // const-string v4, "WindowName"
(( com.miui.misight.MiEvent ) v3 ).addStr ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 404 */
final String v4 = "Duration"; // const-string v4, "Duration"
/* long-to-int v5, p2 */
(( com.miui.misight.MiEvent ) v3 ).addInt ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/miui/misight/MiEvent;->addInt(Ljava/lang/String;I)Lcom/miui/misight/MiEvent;
/* .line 405 */
com.miui.misight.MiSight .sendEvent ( v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 408 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v1 # "window":Ljava/lang/String;
} // .end local v2 # "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
} // .end local v3 # "miEvent":Lcom/miui/misight/MiEvent;
/* .line 406 */
/* :catch_0 */
/* move-exception v0 */
/* .line 409 */
} // :goto_0
return;
} // .end method
static void updateRefreshRateIfNeed ( Boolean p0 ) {
/* .locals 3 */
/* .param p0, "isInMultiWindow" # Z */
/* .line 264 */
v0 = com.android.server.wm.DisplayContentStubImpl .getCurrentRefreshRate ( );
/* .line 266 */
/* .local v0, "currentFps":I */
/* if-eq v1, v0, :cond_0 */
/* .line 267 */
/* .line 270 */
} // :cond_0
/* const/16 v1, 0x3c */
if ( p0 != null) { // if-eqz p0, :cond_1
/* .line 271 */
/* if-le v2, v1, :cond_2 */
/* .line 272 */
/* .line 273 */
com.android.server.wm.DisplayContentStubImpl .setCurrentRefreshRate ( v1 );
/* .line 276 */
} // :cond_1
/* if-lt v2, v1, :cond_2 */
/* .line 277 */
com.android.server.wm.DisplayContentStubImpl .setCurrentRefreshRate ( v2 );
/* .line 278 */
int v1 = -1; // const/4 v1, -0x1
/* .line 281 */
} // :cond_2
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean attachToDisplayCompatMode ( com.android.server.wm.WindowState p0 ) {
/* .locals 3 */
/* .param p1, "mImeLayeringTarget" # Lcom/android/server/wm/WindowState; */
/* .line 421 */
int v0 = 0; // const/4 v0, 0x0
/* .line 422 */
/* .local v0, "result":Z */
if ( p1 != null) { // if-eqz p1, :cond_0
v1 = this.mActivityRecord;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 423 */
v1 = this.mActivityRecord;
(( com.android.server.wm.ActivityRecord ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* .line 424 */
/* .local v1, "task":Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget-boolean v2, v1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 425 */
int v0 = 1; // const/4 v0, 0x1
/* .line 428 */
} // .end local v1 # "task":Lcom/android/server/wm/Task;
} // :cond_0
} // .end method
public void checkWindowHiddenWhenFixedRotation ( com.android.server.wm.WindowManagerService p0, com.android.server.wm.DisplayContent p1, com.android.server.wm.WindowState p2 ) {
/* .locals 5 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p3, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 671 */
v0 = (( com.android.server.wm.DisplayContent ) p2 ).hasTopFixedRotationLaunchingApp ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->hasTopFixedRotationLaunchingApp()Z
/* if-nez v0, :cond_0 */
return;
/* .line 672 */
} // :cond_0
(( com.android.server.wm.DisplayContent ) p2 ).getLastOrientationSource ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getLastOrientationSource()Lcom/android/server/wm/WindowContainer;
/* .line 674 */
/* .local v0, "orientationSource":Lcom/android/server/wm/WindowContainer; */
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.wm.WindowContainer ) v0 ).asActivityRecord ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 675 */
/* .local v1, "r":Lcom/android/server/wm/ActivityRecord; */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 676 */
v2 = (( com.android.server.wm.ActivityRecord ) v1 ).isVisibleRequested ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->isVisibleRequested()Z
/* if-nez v2, :cond_2 */
(( com.android.server.wm.DisplayContent ) p2 ).topRunningActivity ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->topRunningActivity()Lcom/android/server/wm/ActivityRecord;
} // :cond_2
/* move-object v2, v1 */
/* .line 677 */
/* .local v2, "topCandidate":Lcom/android/server/wm/ActivityRecord; */
} // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_3
v3 = (( com.android.server.wm.ActivityRecord ) v2 ).hasFixedRotationTransform ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->hasFixedRotationTransform()Z
if ( v3 != null) { // if-eqz v3, :cond_3
if ( p3 != null) { // if-eqz p3, :cond_3
v3 = this.mAttrs;
if ( v3 != null) { // if-eqz v3, :cond_3
v3 = this.mAttrs;
/* iget v3, v3, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* const/high16 v4, 0x20000000 */
/* and-int/2addr v3, v4 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 680 */
(( com.android.server.wm.DisplayContent ) p2 ).getPendingTransaction ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getPendingTransaction()Landroid/view/SurfaceControl$Transaction;
v4 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v3 ).hide ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 681 */
v3 = this.mHiddenWindowList;
(( java.util.HashSet ) v3 ).add ( p3 ); // invoke-virtual {v3, p3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 684 */
} // .end local v2 # "topCandidate":Lcom/android/server/wm/ActivityRecord;
} // :cond_3
return;
} // .end method
public Integer compare ( com.android.server.wm.WindowToken p0, com.android.server.wm.WindowToken p1 ) {
/* .locals 2 */
/* .param p1, "token1" # Lcom/android/server/wm/WindowToken; */
/* .param p2, "token2" # Lcom/android/server/wm/WindowToken; */
/* .line 307 */
try { // :try_start_0
/* iget v0, p1, Lcom/android/server/wm/WindowToken;->windowType:I */
/* iget v1, p2, Lcom/android/server/wm/WindowToken;->windowType:I */
/* if-ne v0, v1, :cond_1 */
/* iget v0, p1, Lcom/android/server/wm/WindowToken;->windowType:I */
/* const/16 v1, 0x7dd */
/* if-ne v0, v1, :cond_1 */
/* .line 309 */
v0 = this.token;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
final String v1 = "miui.systemui.keyguard.Wallpaper"; // const-string v1, "miui.systemui.keyguard.Wallpaper"
if ( v0 != null) { // if-eqz v0, :cond_0
try { // :try_start_1
v0 = this.token;
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 310 */
int v0 = 1; // const/4 v0, 0x1
/* .line 311 */
} // :cond_0
v0 = this.token;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.token;
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 312 */
int v0 = -1; // const/4 v0, -0x1
/* .line 316 */
} // :cond_1
/* .line 314 */
/* :catch_0 */
/* move-exception v0 */
/* .line 315 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 317 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
/* new-instance v0, Lcom/android/server/wm/DisplayContentStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v0}, Lcom/android/server/wm/DisplayContentStubImpl$$ExternalSyntheticLambda1;-><init>()V */
v0 = java.util.Comparator .comparingInt ( v0 );
} // .end method
public void displayReady ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .line 198 */
this.mContext = p1;
/* .line 199 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/DisplayContentStubImpl;->computeBlurConfiguration(Landroid/content/Context;)V */
/* .line 200 */
return;
} // .end method
public void finishLayoutLw ( com.android.server.policy.WindowManagerPolicy p0, com.android.server.wm.DisplayContent p1, com.android.server.wm.DisplayFrames p2 ) {
/* .locals 4 */
/* .param p1, "policy" # Lcom/android/server/policy/WindowManagerPolicy; */
/* .param p2, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p3, "displayFrames" # Lcom/android/server/wm/DisplayFrames; */
/* .line 322 */
/* instance-of v0, p1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 323 */
/* invoke-direct {p0, p2}, Lcom/android/server/wm/DisplayContentStubImpl;->getInputMethodWindowVisibleRegion(Lcom/android/server/wm/DisplayContent;)Landroid/graphics/Rect; */
/* .line 324 */
/* .local v0, "inputMethodWindowRegion":Landroid/graphics/Rect; */
/* move-object v1, p1 */
/* check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* iget v2, p2, Lcom/android/server/wm/DisplayContent;->mDisplayId:I */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v1 ).finishLayoutLw ( p3, v0, v2 ); // invoke-virtual {v1, p3, v0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->finishLayoutLw(Lcom/android/server/wm/DisplayFrames;Landroid/graphics/Rect;I)V
/* .line 325 */
(( com.android.server.wm.DisplayContent ) p2 ).getDisplayPolicy ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;
(( com.android.server.wm.DisplayPolicy ) v1 ).getStatusBar ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayPolicy;->getStatusBar()Lcom/android/server/wm/WindowState;
/* .line 326 */
/* .local v1, "statusBar":Lcom/android/server/wm/WindowState; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mStatusBarVisible:Z */
v3 = (( com.android.server.wm.WindowState ) v1 ).isVisible ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isVisible()Z
/* if-eq v2, v3, :cond_0 */
/* .line 327 */
v2 = (( com.android.server.wm.WindowState ) v1 ).isVisible ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isVisible()Z
/* iput-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mStatusBarVisible:Z */
/* .line 328 */
com.miui.server.input.AutoDisableScreenButtonsManager .onStatusBarVisibilityChangeStatic ( v2 );
/* .line 329 */
/* iget-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mStatusBarVisible:Z */
com.miui.server.input.knock.MiuiKnockGestureService .finishPostLayoutPolicyLw ( v2 );
/* .line 332 */
} // .end local v0 # "inputMethodWindowRegion":Landroid/graphics/Rect;
} // .end local v1 # "statusBar":Lcom/android/server/wm/WindowState;
} // :cond_0
return;
} // .end method
public void focusChangedLw ( Integer p0, com.android.server.wm.WindowState p1 ) {
/* .locals 6 */
/* .param p1, "displayId" # I */
/* .param p2, "focus" # Lcom/android/server/wm/WindowState; */
/* .line 366 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 367 */
v0 = (( com.android.server.wm.WindowState ) p2 ).getPid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getPid()I
java.lang.Integer .valueOf ( v0 );
v1 = (( com.android.server.wm.WindowState ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getUid()I
java.lang.Integer .valueOf ( v1 );
/* .line 368 */
java.lang.Integer .valueOf ( p1 );
(( com.android.server.wm.WindowState ) p2 ).getOwningPackage ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* filled-new-array {v0, v1, v2, v3}, [Ljava/lang/Object; */
/* .line 367 */
final String v1 = "notifyFocusWindowChanged"; // const-string v1, "notifyFocusWindowChanged"
com.android.server.camera.CameraOpt .callMethod ( v1,v0 );
/* .line 371 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
return;
/* .line 372 */
} // :cond_1
/* if-nez p2, :cond_2 */
/* .line 373 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J */
/* .line 374 */
} // :cond_2
/* iget-wide v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 375 */
/* iget-wide v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J */
/* .line 376 */
/* .local v0, "timestamp":J */
/* iput-wide v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J */
/* .line 377 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* sub-long/2addr v2, v0 */
/* .line 378 */
/* .local v2, "durationMillis":J */
/* const-wide/16 v4, 0x1388 */
/* cmp-long v4, v2, v4 */
/* if-lez v4, :cond_3 */
/* .line 379 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v5, Lcom/android/server/wm/DisplayContentStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v5, p0, p2, v2, v3}, Lcom/android/server/wm/DisplayContentStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/DisplayContentStubImpl;Lcom/android/server/wm/WindowState;J)V */
(( android.os.Handler ) v4 ).post ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 382 */
} // .end local v0 # "timestamp":J
} // .end local v2 # "durationMillis":J
} // :cond_3
} // :goto_0
return;
} // .end method
public Boolean forceEnableSeamless ( com.android.server.wm.WindowState p0 ) {
/* .locals 2 */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 492 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.mAttrs;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAttrs;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* const/high16 v1, 0x40000000 # 2.0f */
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 494 */
int v0 = 1; // const/4 v0, 0x1
/* .line 496 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getSmallScreenLayout ( android.content.res.Configuration p0, com.android.server.wm.DisplayPolicy p1, Float p2, Integer p3, Integer p4 ) {
/* .locals 8 */
/* .param p1, "outConfig" # Landroid/content/res/Configuration; */
/* .param p2, "mDisplayPolicy" # Lcom/android/server/wm/DisplayPolicy; */
/* .param p3, "density" # F */
/* .param p4, "dw" # I */
/* .param p5, "dh" # I */
/* .line 561 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = com.android.server.wm.ActivityRecordStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 562 */
/* if-gt p4, p5, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* move v5, v0 */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move v4, p3 */
/* move v6, p4 */
/* move v7, p5 */
v0 = /* invoke-direct/range {v1 ..v7}, Lcom/android/server/wm/DisplayContentStubImpl;->getSmallScreenLayout(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayPolicy;FZII)I */
/* .line 564 */
} // :cond_1
/* iget v0, p1, Landroid/content/res/Configuration;->screenLayout:I */
} // .end method
public void initOneTrackRotationHelper ( ) {
/* .locals 1 */
/* .line 412 */
com.android.server.wm.OneTrackRotationHelper .getInstance ( );
(( com.android.server.wm.OneTrackRotationHelper ) v0 ).init ( ); // invoke-virtual {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->init()V
/* .line 413 */
return;
} // .end method
public Boolean isCameraRotating ( ) {
/* .locals 1 */
/* .line 624 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_FOLD:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_TABLET:Z */
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 625 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mIsCameraRotating:Z */
} // .end method
public Boolean isSubDisplay ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .line 359 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isSubDisplayOff ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 2 */
/* .param p1, "display" # Lcom/android/server/wm/DisplayContent; */
/* .line 351 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 353 */
v0 = (( com.android.server.wm.DisplayContent ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 354 */
(( com.android.server.wm.DisplayContent ) p1 ).getDisplay ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;
v0 = (( android.view.Display ) v0 ).getState ( ); // invoke-virtual {v0}, Landroid/view/Display;->getState()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 351 */
} // :goto_0
} // .end method
public Boolean isSupportedImeSnapShot ( ) {
/* .locals 1 */
/* .line 443 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_IME_SNAPSHOT:Z */
} // .end method
public Boolean needEnsureVisible ( com.android.server.wm.DisplayContent p0, com.android.server.wm.Task p1 ) {
/* .locals 3 */
/* .param p1, "dc" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .line 432 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_3
v1 = this.affinity;
/* if-nez v1, :cond_0 */
/* .line 435 */
} // :cond_0
v1 = (( com.android.server.wm.DisplayContentStubImpl ) p0 ).isSubDisplayOff ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/DisplayContentStubImpl;->isSubDisplayOff(Lcom/android/server/wm/DisplayContent;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.affinity;
final String v2 = "com.xiaomi.misubscreenui"; // const-string v2, "com.xiaomi.misubscreenui"
v1 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v1, :cond_1 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :cond_2
} // :goto_0
/* .line 433 */
} // :cond_3
} // :goto_1
} // .end method
public Boolean needSkipUpdateSurfaceRotation ( ) {
/* .locals 1 */
/* .line 613 */
/* iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipUpdateSurfaceRotation:Z */
} // .end method
public Boolean needSkipVisualDisplayTransition ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .line 475 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipVisualDisplayTransition:Z */
/* .line 476 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void noteFoucsChangeForPowerConsumptionIfNeeded ( com.android.server.wm.WindowState p0 ) {
/* .locals 5 */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 453 */
v0 = this.mAttrs;
/* .line 454 */
/* .local v0, "mAttrs":Landroid/view/WindowManager$LayoutParams; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 455 */
(( android.view.WindowManager$LayoutParams ) v0 ).getTitle ( ); // invoke-virtual {v0}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
/* .line 456 */
/* .local v1, "tag":Ljava/lang/CharSequence; */
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mMiuiWindowStateEx;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mPowerConsumptionServiceInternal;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 459 */
v4 = this.mMiuiWindowStateEx;
(( com.android.server.PowerConsumptionServiceInternal ) v2 ).noteFoucsChangeForPowerConsumption ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/PowerConsumptionServiceInternal;->noteFoucsChangeForPowerConsumption(Ljava/lang/String;Lcom/android/server/wm/IMiuiWindowStateEx;)V
/* .line 462 */
} // .end local v1 # "tag":Ljava/lang/CharSequence;
} // :cond_0
return;
} // .end method
public void onDisplayOverrideConfigUpdate ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 1 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 466 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 467 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 468 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 469 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onDisplayOverrideConfigUpdate ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->onDisplayOverrideConfigUpdate(Lcom/android/server/wm/DisplayContent;)V
/* .line 471 */
} // :cond_0
return;
} // .end method
public void onFocusedWindowChanged ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 3 */
/* .param p1, "display" # Lcom/android/server/wm/DisplayContent; */
/* .line 501 */
/* iget-boolean v0, p1, Lcom/android/server/wm/DisplayContent;->isDefaultDisplay:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v0 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v0 ).isControllerAMonkey ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isControllerAMonkey()Z
/* if-nez v0, :cond_0 */
/* .line 504 */
} // :cond_0
v0 = this.mWmService;
/* .line 505 */
/* .local v0, "wmService":Lcom/android/server/wm/WindowManagerService; */
v1 = this.mFocusRunnable;
/* if-nez v1, :cond_1 */
/* .line 506 */
/* new-instance v1, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable; */
/* invoke-direct {v1, v0}, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;-><init>(Lcom/android/server/wm/WindowManagerService;)V */
this.mFocusRunnable = v1;
/* .line 508 */
} // :cond_1
v1 = this.mFocusRunnable;
v2 = this.mCurrentFocus;
/* if-nez v2, :cond_2 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
com.android.server.wm.DisplayContentStubImpl$UpdateFocusRunnable .-$$Nest$mpost ( v1,v2 );
/* .line 509 */
return;
/* .line 502 */
} // .end local v0 # "wmService":Lcom/android/server/wm/WindowManagerService;
} // :cond_3
} // :goto_1
return;
} // .end method
public Boolean onSettingsObserverChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 208 */
if ( p2 != null) { // if-eqz p2, :cond_0
final String v0 = "background_blur_enable"; // const-string v0, "background_blur_enable"
android.provider.Settings$Secure .getUriFor ( v0 );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 209 */
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/wm/DisplayContentStubImpl;->computeBlurConfiguration(Landroid/content/Context;)V */
/* .line 210 */
int v0 = 1; // const/4 v0, 0x1
/* .line 212 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void onSystemReady ( ) {
/* .locals 1 */
/* .line 194 */
/* const-class v0, Lcom/android/server/PowerConsumptionServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/PowerConsumptionServiceInternal; */
this.mPowerConsumptionServiceInternal = v0;
/* .line 195 */
return;
} // .end method
public void registerSettingsObserver ( android.content.Context p0, com.android.server.wm.WindowManagerService$SettingsObserver p1 ) {
/* .locals 4 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "settingsObserver" # Lcom/android/server/wm/WindowManagerService$SettingsObserver; */
/* .line 203 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 204 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "background_blur_enable"; // const-string v1, "background_blur_enable"
android.provider.Settings$Secure .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p2, v3 ); // invoke-virtual {v0, v1, v2, p2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 205 */
return;
} // .end method
public void reportDisplayRotationChanged ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .param p2, "rotation" # I */
/* .line 416 */
com.android.server.wm.OneTrackRotationHelper .getInstance ( );
(( com.android.server.wm.OneTrackRotationHelper ) v0 ).reportRotationChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->reportRotationChanged(II)V
/* .line 417 */
return;
} // .end method
public void restoreHiddenWindow ( com.android.server.wm.DisplayContent p0, android.view.SurfaceControl$Transaction p1 ) {
/* .locals 3 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "transaction" # Landroid/view/SurfaceControl$Transaction; */
/* .line 687 */
v0 = this.mHiddenWindowList;
v0 = (( java.util.HashSet ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 688 */
} // :cond_0
v0 = this.mHiddenWindowList;
(( java.util.HashSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/wm/WindowState; */
/* .line 689 */
/* .local v1, "w":Lcom/android/server/wm/WindowState; */
v2 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) p2 ).show ( v2 ); // invoke-virtual {p2, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 690 */
} // .end local v1 # "w":Lcom/android/server/wm/WindowState;
/* .line 691 */
} // :cond_1
v0 = this.mHiddenWindowList;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 692 */
return;
} // .end method
public void setBlur ( android.content.res.Configuration p0, android.content.res.Configuration p1 ) {
/* .locals 1 */
/* .param p1, "current" # Landroid/content/res/Configuration; */
/* .param p2, "tmp" # Landroid/content/res/Configuration; */
/* .line 231 */
/* iget v0, p1, Landroid/content/res/Configuration;->blur:I */
/* if-nez v0, :cond_0 */
/* .line 232 */
/* iget v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I */
/* iput v0, p2, Landroid/content/res/Configuration;->blur:I */
/* .line 234 */
} // :cond_0
return;
} // .end method
public void setCameraRotationState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isCameraRotating" # Z */
/* .line 617 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_FOLD:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_TABLET:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 618 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mIsCameraRotating:Z */
/* if-eq v0, p1, :cond_1 */
/* .line 619 */
/* iput-boolean p1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mIsCameraRotating:Z */
/* .line 621 */
} // :cond_1
return;
} // .end method
public void setFixedRotationState ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 2 */
/* .param p1, "prevActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "nowActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 604 */
if ( p1 != null) { // if-eqz p1, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
v0 = this.packageName;
v1 = this.packageName;
/* .line 605 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 606 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipUpdateSurfaceRotation:Z */
/* .line 607 */
return;
/* .line 609 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipUpdateSurfaceRotation:Z */
/* .line 610 */
return;
} // .end method
public Boolean shouldInterceptRelaunch ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "vendorId" # I */
/* .param p2, "productId" # I */
/* .line 439 */
v0 = com.android.server.input.padkeyboard.MiuiPadKeyboardManager .isXiaomiKeyboard ( p1,p2 );
} // .end method
public Boolean skipChangeTransition ( Integer p0, android.window.TransitionRequestInfo$DisplayChange p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "changes" # I */
/* .param p2, "displayChange" # Landroid/window/TransitionRequestInfo$DisplayChange; */
/* .param p3, "resumeActivity" # Ljava/lang/String; */
/* .line 514 */
/* if-nez p2, :cond_1 */
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 515 */
final String v0 = "com.android.provision/.activities.DefaultActivity"; // const-string v0, "com.android.provision/.activities.DefaultActivity"
v0 = (( java.lang.String ) p3 ).contains ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_0 */
/* .line 516 */
final String v0 = "com.android.provision/.activities.CongratulationActivity"; // const-string v0, "com.android.provision/.activities.CongratulationActivity"
v0 = (( java.lang.String ) p3 ).contains ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 514 */
} // :goto_0
} // .end method
public Boolean skipImeWindowsForMiui ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 2 */
/* .param p1, "display" # Lcom/android/server/wm/DisplayContent; */
/* .line 447 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).isVerticalSplit ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isVerticalSplit()Z
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 448 */
} // :cond_0
v0 = this.mAtmService;
v0 = (( com.android.server.wm.ActivityTaskManagerService ) v0 ).isInSplitScreenWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z
/* xor-int/2addr v0, v1 */
} // .end method
public void updateCurrentDisplayRotation ( com.android.server.wm.DisplayContent p0, Boolean p1, Boolean p2 ) {
/* .locals 7 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "alwaysSendConfiguration" # Z */
/* .param p3, "forceRelayout" # Z */
/* .line 630 */
/* const-string/jumbo v0, "updateRotation" */
/* const-wide/16 v1, 0x20 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 632 */
try { // :try_start_0
v0 = this.mWmService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 633 */
int v3 = 0; // const/4 v3, 0x0
/* .line 635 */
/* .local v3, "layoutNeeded":Z */
try { // :try_start_1
v4 = (( com.android.server.wm.DisplayContent ) p1 ).updateRotationUnchecked ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->updateRotationUnchecked()Z
/* .line 637 */
/* .local v4, "rotationChanged":Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 638 */
v5 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v5 ).getTaskChangeNotificationController ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getTaskChangeNotificationController()Lcom/android/server/wm/TaskChangeNotificationController;
/* iget v6, p1, Lcom/android/server/wm/DisplayContent;->mDisplayId:I */
/* .line 639 */
(( com.android.server.wm.TaskChangeNotificationController ) v5 ).notifyOnActivityRotation ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/TaskChangeNotificationController;->notifyOnActivityRotation(I)V
/* .line 642 */
} // :cond_0
if ( v4 != null) { // if-eqz v4, :cond_2
v5 = this.mRemoteDisplayChangeController;
/* .line 643 */
v5 = (( com.android.server.wm.RemoteDisplayChangeController ) v5 ).isWaitingForRemoteDisplayChange ( ); // invoke-virtual {v5}, Lcom/android/server/wm/RemoteDisplayChangeController;->isWaitingForRemoteDisplayChange()Z
/* if-nez v5, :cond_1 */
v5 = this.mTransitionController;
/* .line 644 */
v5 = (( com.android.server.wm.TransitionController ) v5 ).isCollecting ( ); // invoke-virtual {v5}, Lcom/android/server/wm/TransitionController;->isCollecting()Z
if ( v5 != null) { // if-eqz v5, :cond_2
} // :cond_1
int v5 = 1; // const/4 v5, 0x1
} // :cond_2
int v5 = 0; // const/4 v5, 0x0
/* .line 646 */
/* .local v5, "pendingRemoteDisplayChange":Z */
} // :goto_0
/* if-nez v5, :cond_5 */
/* .line 648 */
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 649 */
(( com.android.server.wm.DisplayContent ) p1 ).setLayoutNeeded ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->setLayoutNeeded()V
/* .line 650 */
int v3 = 1; // const/4 v3, 0x1
/* .line 652 */
} // :cond_3
/* if-nez v4, :cond_4 */
if ( p2 != null) { // if-eqz p2, :cond_5
/* .line 653 */
} // :cond_4
(( com.android.server.wm.DisplayContent ) p1 ).sendNewConfiguration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->sendNewConfiguration()V
/* .line 657 */
} // :cond_5
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 658 */
/* const-string/jumbo v6, "updateRotation: performSurfacePlacement" */
android.os.Trace .traceBegin ( v1,v2,v6 );
/* .line 660 */
v6 = this.mWmService;
v6 = this.mWindowPlacerLocked;
(( com.android.server.wm.WindowSurfacePlacer ) v6 ).performSurfacePlacement ( ); // invoke-virtual {v6}, Lcom/android/server/wm/WindowSurfacePlacer;->performSurfacePlacement()V
/* .line 661 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 663 */
} // .end local v3 # "layoutNeeded":Z
} // .end local v4 # "rotationChanged":Z
} // .end local v5 # "pendingRemoteDisplayChange":Z
} // :cond_6
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 665 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 666 */
/* nop */
/* .line 667 */
return;
/* .line 663 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/wm/DisplayContentStubImpl;
} // .end local p1 # "displayContent":Lcom/android/server/wm/DisplayContent;
} // .end local p2 # "alwaysSendConfiguration":Z
} // .end local p3 # "forceRelayout":Z
try { // :try_start_3
/* throw v3 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 665 */
/* .restart local p0 # "this":Lcom/android/server/wm/DisplayContentStubImpl; */
/* .restart local p1 # "displayContent":Lcom/android/server/wm/DisplayContent; */
/* .restart local p2 # "alwaysSendConfiguration":Z */
/* .restart local p3 # "forceRelayout":Z */
/* :catchall_1 */
/* move-exception v0 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 666 */
/* throw v0 */
} // .end method
public void updateSkipVisualDisplayTransition ( Integer p0, java.lang.String p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .param p2, "tokenTag" # Ljava/lang/String; */
/* .param p3, "shouldSkip" # Z */
/* .line 480 */
if ( p1 != null) { // if-eqz p1, :cond_1
final String v0 = "Display-off"; // const-string v0, "Display-off"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 481 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 482 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipVisualDisplayTransition:Z */
/* .line 484 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipVisualDisplayTransition:Z */
/* .line 487 */
} // :cond_1
} // :goto_0
return;
} // .end method
