.class public Lcom/android/server/wm/RecommendDataEntry;
.super Ljava/lang/Object;
.source "RecommendDataEntry.java"


# static fields
.field private static final FREEFORM_TASKID:Ljava/lang/String; = "freeFormTaskId"

.field private static final MIUI_RECOMMEND_CONTENT:Ljava/lang/String; = "miuiRecommendContent"

.field private static final RECOMMEND_SCENE:Ljava/lang/String; = "recommendScene"

.field public static final RECOMMEND_SCENE_TYPE_GAME_UPDATING:I = 0x2

.field public static final RECOMMEND_SCENE_TYPE_SPLIT_SCREEN:I = 0x3

.field public static final RECOMMEND_SCENE_TYPE_TAXI_WAITING:I = 0x1

.field private static final RECOMMEND_TRANSACTION_TYPE:Ljava/lang/String; = "recommendTransactionType"

.field public static final RECOMMEND_TYPE_FREE_FORM:I = 0x1

.field public static final RECOMMEND_TYPE_FREE_FORM_REMOVE:I = 0x3

.field public static final RECOMMEND_TYPE_SPLIT_SCREEN:I = 0x2

.field private static final SENDER_PACKAGENAME:Ljava/lang/String; = "senderPackageName"

.field private static final TAG:Ljava/lang/String; = "RecommendDataEntry"


# instance fields
.field private freeformPackageName:Ljava/lang/String;

.field private freeformTaskId:I

.field private freeformUserId:I

.field private inFreeFormRecommend:Z

.field private inSplitScreenRecommend:Z

.field private primaryPackageName:Ljava/lang/String;

.field private primaryTaskId:I

.field private primaryUserId:I

.field private recommendSceneType:I

.field private recommendTransactionType:I

.field private secondaryPackageName:Ljava/lang/String;

.field private secondaryTaskId:I

.field private secondaryUserId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method


# virtual methods
.method public getFreeFormRecommendState()Z
    .locals 1

    .line 123
    iget-boolean v0, p0, Lcom/android/server/wm/RecommendDataEntry;->inFreeFormRecommend:Z

    return v0
.end method

.method public getFreeFormTaskId()I
    .locals 1

    .line 71
    iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformTaskId:I

    return v0
.end method

.method public getFreeformPackageName()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getFreeformUserId()I
    .locals 1

    .line 135
    iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformUserId:I

    return v0
.end method

.method public getPrimaryPackageName()Ljava/lang/String;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPrimaryTaskId()I
    .locals 1

    .line 79
    iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryTaskId:I

    return v0
.end method

.method public getPrimaryUserId()I
    .locals 1

    .line 143
    iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryUserId:I

    return v0
.end method

.method public getRecommendSceneType()I
    .locals 1

    .line 59
    iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->recommendSceneType:I

    return v0
.end method

.method public getSecondaryPackageName()Ljava/lang/String;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondaryTaskId()I
    .locals 1

    .line 87
    iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryTaskId:I

    return v0
.end method

.method public getSecondaryUserId()I
    .locals 1

    .line 151
    iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryUserId:I

    return v0
.end method

.method public getSplitScreenRecommendState()Z
    .locals 1

    .line 115
    iget-boolean v0, p0, Lcom/android/server/wm/RecommendDataEntry;->inSplitScreenRecommend:Z

    return v0
.end method

.method public getTransactionType()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->recommendTransactionType:I

    return v0
.end method

.method public setFreeFormRecommendState(Z)V
    .locals 0
    .param p1, "inFreeFormRecommend"    # Z

    .line 127
    iput-boolean p1, p0, Lcom/android/server/wm/RecommendDataEntry;->inFreeFormRecommend:Z

    .line 128
    return-void
.end method

.method public setFreeFormTaskId(I)V
    .locals 0
    .param p1, "freeformTaskId"    # I

    .line 67
    iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformTaskId:I

    .line 68
    return-void
.end method

.method public setFreeformPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .line 95
    iput-object p1, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformPackageName:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setFreeformUserId(I)V
    .locals 0
    .param p1, "userId"    # I

    .line 131
    iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformUserId:I

    .line 132
    return-void
.end method

.method public setPrimaryPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .line 103
    iput-object p1, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryPackageName:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setPrimaryTaskId(I)V
    .locals 0
    .param p1, "primaryTaskId"    # I

    .line 75
    iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryTaskId:I

    .line 76
    return-void
.end method

.method public setPrimaryUserId(I)V
    .locals 0
    .param p1, "userId"    # I

    .line 139
    iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryUserId:I

    .line 140
    return-void
.end method

.method public setRecommendSceneType(I)V
    .locals 0
    .param p1, "recommendSceneType"    # I

    .line 63
    iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->recommendSceneType:I

    .line 64
    return-void
.end method

.method public setSecondaryPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .line 111
    iput-object p1, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryPackageName:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setSecondaryTaskId(I)V
    .locals 0
    .param p1, "secondaryTaskId"    # I

    .line 83
    iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryTaskId:I

    .line 84
    return-void
.end method

.method public setSecondaryUserId(I)V
    .locals 0
    .param p1, "userId"    # I

    .line 147
    iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryUserId:I

    .line 148
    return-void
.end method

.method public setSplitScreenRecommendState(Z)V
    .locals 0
    .param p1, "inSplitScreenRecommend"    # Z

    .line 119
    iput-boolean p1, p0, Lcom/android/server/wm/RecommendDataEntry;->inSplitScreenRecommend:Z

    .line 120
    return-void
.end method

.method public setTransactionType(I)V
    .locals 0
    .param p1, "transactionType"    # I

    .line 55
    iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->recommendTransactionType:I

    .line 56
    return-void
.end method
