public abstract class com.android.server.wm.MiuiSizeCompatInternal {
	 /* .source "MiuiSizeCompatInternal.java" */
	 /* # direct methods */
	 public com.android.server.wm.MiuiSizeCompatInternal ( ) {
		 /* .locals 0 */
		 /* .line 11 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract Boolean executeShellCommand ( java.lang.String p0, java.lang.String[] p1, java.io.PrintWriter p2 ) {
	 } // .end method
	 public abstract Integer getAspectGravityByPackage ( java.lang.String p0 ) {
	 } // .end method
	 public abstract Float getAspectRatioByPackage ( java.lang.String p0 ) {
	 } // .end method
	 public abstract java.util.Map getMiuiGameSizeCompatEnabledApps ( ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "()", */
		 /* "Ljava/util/Map<", */
		 /* "Ljava/lang/String;", */
		 /* "Landroid/sizecompat/AspectRatioInfo;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
} // .end method
public abstract java.util.Map getMiuiSizeCompatEnabledApps ( ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Landroid/sizecompat/AspectRatioInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end method
public abstract Integer getScaleModeByPackage ( java.lang.String p0 ) {
} // .end method
public abstract Boolean inMiuiGameSizeCompat ( java.lang.String p0 ) {
} // .end method
public abstract Boolean isAppSizeCompatRestarting ( java.lang.String p0 ) {
} // .end method
public abstract void onSystemReady ( com.android.server.wm.ActivityTaskManagerService p0 ) {
} // .end method
public abstract void showWarningNotification ( com.android.server.wm.ActivityRecord p0 ) {
} // .end method
