public class com.android.server.wm.SplitScreenRecommendPredictHelper {
	 /* .source "SplitScreenRecommendPredictHelper.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 com.android.server.wm.MiuiMultiWindowRecommendHelper mMiuiMultiWindowRecommendHelper;
	 /* # direct methods */
	 public com.android.server.wm.SplitScreenRecommendPredictHelper ( ) {
		 /* .locals 0 */
		 /* .param p1, "miuiMultiWindowRecommendHelper" # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 17 */
		 this.mMiuiMultiWindowRecommendHelper = p1;
		 /* .line 18 */
		 return;
	 } // .end method
	 private java.util.List getDeduplicatedTaskList ( java.util.List p0 ) {
		 /* .locals 5 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/List<", */
		 /* "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;", */
		 /* ">;)", */
		 /* "Ljava/util/List<", */
		 /* "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 84 */
	 /* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;" */
	 /* new-instance v0, Ljava/util/ArrayList; */
	 /* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 85 */
	 /* .local v0, "arrayList":Ljava/util/ArrayList; */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
	 /* check-cast v2, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
	 /* .line 86 */
	 /* .local v2, "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
	 v3 = 	 (( java.util.ArrayList ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
	 /* if-nez v3, :cond_0 */
	 v3 = 	 (( com.android.server.wm.SplitScreenRecommendTaskInfo ) v2 ).getTaskId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
	 /* .line 87 */
	 v4 = 	 (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
	 /* add-int/lit8 v4, v4, -0x1 */
	 (( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
	 /* check-cast v4, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
	 v4 = 	 (( com.android.server.wm.SplitScreenRecommendTaskInfo ) v4 ).getTaskId ( ); // invoke-virtual {v4}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
	 /* if-eq v3, v4, :cond_1 */
	 /* .line 88 */
} // :cond_0
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 90 */
} // .end local v2 # "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
} // :cond_1
/* .line 91 */
} // :cond_2
} // .end method
/* # virtual methods */
public java.util.List getFrequentSwitchedTask ( java.util.List p0 ) {
/* .locals 11 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 21 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;" */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendPredictHelper;->getDeduplicatedTaskList(Ljava/util/List;)Ljava/util/List; */
/* .line 22 */
/* .local v0, "deduplicatedTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 23 */
/* .local v1, "candidateTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 24 */
v3 = /* .local v2, "candidateTaskIndexList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
int v4 = 0; // const/4 v4, 0x0
int v5 = 4; // const/4 v5, 0x4
/* if-ge v3, v5, :cond_0 */
/* .line 25 */
final String v3 = "SplitScreenRecommendPredictHelper"; // const-string v3, "SplitScreenRecommendPredictHelper"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "appSwitchList size is "; // const-string v6, "appSwitchList size is "
v6 = (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " less than 4"; // const-string v6, " less than 4"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v5 );
/* .line 26 */
/* .line 29 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v6 = } // :goto_0
/* if-ge v3, v6, :cond_2 */
/* .line 30 */
/* check-cast v6, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 31 */
/* .local v6, "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* new-instance v8, Lcom/android/server/wm/SplitScreenRecommendPredictHelper$1; */
/* invoke-direct {v8, p0, v6}, Lcom/android/server/wm/SplitScreenRecommendPredictHelper$1;-><init>(Lcom/android/server/wm/SplitScreenRecommendPredictHelper;Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V */
/* .line 36 */
/* move-result-wide v7 */
/* const-wide/16 v9, 0x2 */
/* cmp-long v7, v7, v9 */
/* if-ltz v7, :cond_1 */
/* .line 37 */
/* .line 38 */
java.lang.Integer .valueOf ( v3 );
/* .line 29 */
} // .end local v6 # "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 42 */
} // .end local v3 # "i":I
v3 = } // :cond_2
/* if-eq v3, v5, :cond_3 */
/* .line 43 */
final String v3 = "SplitScreenRecommendPredictHelper"; // const-string v3, "SplitScreenRecommendPredictHelper"
final String v5 = "not find frequent switched task"; // const-string v5, "not find frequent switched task"
android.util.Slog .d ( v3,v5 );
/* goto/16 :goto_4 */
/* .line 45 */
} // :cond_3
int v3 = 0; // const/4 v3, 0x0
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
int v6 = 1; // const/4 v6, 0x1
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* sub-int/2addr v7, v6 */
/* if-ne v5, v7, :cond_a */
/* .line 46 */
int v5 = 2; // const/4 v5, 0x2
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
int v8 = 3; // const/4 v8, 0x3
/* check-cast v8, Ljava/lang/Integer; */
v8 = (( java.lang.Integer ) v8 ).intValue ( ); // invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I
/* sub-int/2addr v8, v6 */
/* if-ne v7, v8, :cond_a */
/* .line 47 */
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* check-cast v8, Ljava/lang/Integer; */
v8 = (( java.lang.Integer ) v8 ).intValue ( ); // invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I
/* sub-int/2addr v8, v6 */
/* if-ne v7, v8, :cond_a */
/* .line 48 */
/* check-cast v7, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v7 ).getTask ( ); // invoke-virtual {v7}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;
/* .line 49 */
/* .local v7, "task1":Lcom/android/server/wm/Task; */
/* check-cast v8, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v8 ).getTask ( ); // invoke-virtual {v8}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;
/* .line 50 */
/* .local v8, "task2":Lcom/android/server/wm/Task; */
v9 = this.mMiuiMultiWindowRecommendHelper;
v9 = this.mFreeFormManagerService;
v9 = this.mActivityTaskManagerService;
(( com.android.server.wm.ActivityTaskManagerService ) v9 ).getGlobalLock ( ); // invoke-virtual {v9}, Lcom/android/server/wm/ActivityTaskManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;
/* monitor-enter v9 */
/* .line 51 */
try { // :try_start_0
v10 = (( com.android.server.wm.Task ) v7 ).supportsSplitScreenWindowingMode ( ); // invoke-virtual {v7}, Lcom/android/server/wm/Task;->supportsSplitScreenWindowingMode()Z
if ( v10 != null) { // if-eqz v10, :cond_9
/* .line 52 */
v10 = (( com.android.server.wm.Task ) v8 ).supportsSplitScreenWindowingMode ( ); // invoke-virtual {v8}, Lcom/android/server/wm/Task;->supportsSplitScreenWindowingMode()Z
/* if-nez v10, :cond_4 */
/* goto/16 :goto_3 */
/* .line 56 */
} // :cond_4
/* monitor-exit v9 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 57 */
v9 = (( com.android.server.wm.Task ) v7 ).getWindowingMode ( ); // invoke-virtual {v7}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-ne v9, v6, :cond_8 */
/* .line 58 */
v9 = (( com.android.server.wm.Task ) v8 ).getWindowingMode ( ); // invoke-virtual {v8}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-eq v9, v6, :cond_5 */
/* .line 62 */
} // :cond_5
v9 = com.android.server.wm.RecommendUtils .isInSplitScreenWindowingMode ( v7 );
/* if-nez v9, :cond_7 */
/* .line 63 */
v9 = com.android.server.wm.RecommendUtils .isInSplitScreenWindowingMode ( v8 );
if ( v9 != null) { // if-eqz v9, :cond_6
/* .line 68 */
} // :cond_6
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 70 */
/* .local v4, "frequentSwitchedTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;" */
/* check-cast v5, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 71 */
/* check-cast v5, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 72 */
final String v5 = "SplitScreenRecommendPredictHelper"; // const-string v5, "SplitScreenRecommendPredictHelper"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "find frequent switched task, task 1: "; // const-string v10, "find frequent switched task, task 1: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v3, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v3 ).getPkgName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;
(( java.lang.StringBuilder ) v9 ).append ( v3 ); // invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " task 2: "; // const-string v9, " task 2: "
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 73 */
/* check-cast v6, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v6 ).getPkgName ( ); // invoke-virtual {v6}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 72 */
android.util.Slog .d ( v5,v3 );
/* .line 74 */
/* .line 64 */
} // .end local v4 # "frequentSwitchedTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
} // :cond_7
} // :goto_1
final String v3 = "SplitScreenRecommendPredictHelper"; // const-string v3, "SplitScreenRecommendPredictHelper"
final String v5 = " task isInSplitScreenWindowingMode"; // const-string v5, " task isInSplitScreenWindowingMode"
android.util.Slog .d ( v3,v5 );
/* .line 65 */
/* .line 59 */
} // :cond_8
} // :goto_2
final String v3 = "SplitScreenRecommendPredictHelper"; // const-string v3, "SplitScreenRecommendPredictHelper"
final String v5 = " task windowingMode is not fullscreen"; // const-string v5, " task windowingMode is not fullscreen"
android.util.Slog .d ( v3,v5 );
/* .line 60 */
/* .line 53 */
} // :cond_9
} // :goto_3
try { // :try_start_1
final String v3 = "SplitScreenRecommendPredictHelper"; // const-string v3, "SplitScreenRecommendPredictHelper"
final String v5 = " not supportsSplitScreenWindowingMode"; // const-string v5, " not supportsSplitScreenWindowingMode"
android.util.Slog .d ( v3,v5 );
/* .line 54 */
/* monitor-exit v9 */
/* .line 56 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v9 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
/* .line 76 */
} // .end local v7 # "task1":Lcom/android/server/wm/Task;
} // .end local v8 # "task2":Lcom/android/server/wm/Task;
} // :cond_a
final String v3 = "SplitScreenRecommendPredictHelper"; // const-string v3, "SplitScreenRecommendPredictHelper"
final String v5 = "not find frequent switched task, index is not contiguous"; // const-string v5, "not find frequent switched task, index is not contiguous"
android.util.Slog .d ( v3,v5 );
/* .line 79 */
} // :goto_4
} // .end method
