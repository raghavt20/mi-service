.class public Lcom/android/server/wm/MiuiDesktopModeUtils;
.super Ljava/lang/Object;
.source "MiuiDesktopModeUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver;
    }
.end annotation


# static fields
.field private static final IS_SUPPORTED:Z

.field private static final TAG:Ljava/lang/String; = "MiuiDesktopModeUtils"

.field private static mDesktopOn:Z


# direct methods
.method static bridge synthetic -$$Nest$sfgetmDesktopOn()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfputmDesktopOn(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 34
    const-string v0, "ro.config.miui_desktop_mode_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->IS_SUPPORTED:Z

    .line 37
    sput-boolean v1, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static initDesktop(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 40
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-static {p0}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z

    .line 42
    new-instance v0, Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver;-><init>()V

    .line 43
    .local v0, "mSettingsObserver":Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver;
    invoke-virtual {v0, p0}, Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver;->observe(Landroid/content/Context;)V

    .line 45
    .end local v0    # "mSettingsObserver":Lcom/android/server/wm/MiuiDesktopModeUtils$SettingsObserver;
    :cond_0
    return-void
.end method

.method public static isActive(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 51
    sget-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->IS_SUPPORTED:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 52
    sput-boolean v1, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z

    .line 53
    return v1

    .line 57
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "miui_dkt_mode"

    const/4 v3, -0x2

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 59
    .local v0, "result":I
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    sput-boolean v2, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    return v2

    .line 61
    .end local v0    # "result":I
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to read MIUI_DESKTOP_MODE settings "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiDesktopModeUtils"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    sput-boolean v1, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z

    .line 64
    return v1
.end method

.method public static isDesktopActive()Z
    .locals 1

    .line 68
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->mDesktopOn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static isEnabled()Z
    .locals 1

    .line 47
    sget-boolean v0, Lcom/android/server/wm/MiuiDesktopModeUtils;->IS_SUPPORTED:Z

    return v0
.end method
