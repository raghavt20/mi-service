.class public Lcom/android/server/wm/AppResurrectionServiceImpl;
.super Lcom/android/server/wm/AppResurrectionServiceStub;
.source "AppResurrectionServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.wm.AppResurrectionServiceStub$$"
.end annotation


# static fields
.field private static final ATTR_REBORN_CHILDREN_SIZE:Ljava/lang/String; = "reborn_children_size"

.field private static final CLOUD_KEY_NAME:Ljava/lang/String; = "KeyAppResurrection"

.field private static final CLOUD_MODULE_NAME:Ljava/lang/String; = "ModuleAppResurrection"

.field public static final TAG:Ljava/lang/String; = "AppResurrectionServiceImpl"

.field private static final TAG_REBORN_ACTIVITY:Ljava/lang/String; = "reborn_activity"

.field private static final sDefaultAppResurrectionEnableActivityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sDefaultAppResurrectionEnablePKGList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sDefaultPkg2MaxChildCountJObject:Lorg/json/JSONObject;

.field private static final sDefaultRebootResurrectionEnableActivityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sDefaultRebootResurrectionEnablePkgList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sDefaultResurrectionEnableReasonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private DEBUG:Z

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCPULevel:Ljava/lang/String;

.field private mCloudAppResurrectionActivityJArray:Lorg/json/JSONArray;

.field private mCloudAppResurrectionEnable:Z

.field private mCloudAppResurrectionInactiveDurationHour:I

.field private mCloudAppResurrectionLaunchTimeThresholdMillis:J

.field private mCloudAppResurrectionMaxChildCount:I

.field private mCloudAppResurrectionName:Ljava/lang/String;

.field private mCloudAppResurrectionPKGJArray:Lorg/json/JSONArray;

.field private mCloudAppResurrectionVersion:J

.field private mCloudManager:Lcom/android/server/wm/AppResurrectionCloudManager;

.field private mCloudPkg2MaxChildCountJObject:Lorg/json/JSONObject;

.field private mCloudRebootResurrectionActivityJArray:Lorg/json/JSONArray;

.field private mCloudRebootResurrectionEnable:Z

.field private mCloudRebootResurrectionPKGJArray:Lorg/json/JSONArray;

.field private mContext:Landroid/content/Context;

.field private mIsDeviceSupport:Z

.field private mIsLoadDone:Z

.field private mIsTestMode:Z

.field private mLocalSpFile:Ljava/io/File;

.field private final mLoggerHandler:Landroid/os/Handler;

.field private final mMIUIBgHandler:Landroid/os/Handler;

.field private mPkg2KillReasonMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPkg2StartThemeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mRebornPkg:Ljava/lang/String;

.field private mResolver:Landroid/content/ContentResolver;

.field private mTrackManager:Lcom/android/server/wm/AppResurrectionTrackManager;


# direct methods
.method public static synthetic $r8$lambda$EW8OAHUU9KE_drSaYZPEZqsi8D4(Lcom/android/server/wm/AppResurrectionServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$init$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$HiShqve5mvNS1bL4jHQiI48pWtk(Lcom/android/server/wm/AppResurrectionServiceImpl;Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$isChildrenActivityFromSamePkg$7(Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$S_5WDZhsUHesxGVTyqpzrScQ-Ho(Lcom/android/server/wm/AppResurrectionServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$init$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$UgwBb6vyJZCFQgUjn9t4Fk6VWwA(Lcom/android/server/wm/AppResurrectionServiceImpl;Landroid/content/ComponentName;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$notifyWindowsDrawn$3(Landroid/content/ComponentName;JLjava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$asL8P4pw8XJDSm4G0yJfUyYUCuM(Lcom/android/server/wm/AppResurrectionServiceImpl;Landroid/content/ComponentName;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$notifyWindowsDrawn$4(Landroid/content/ComponentName;JLjava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$bBI9Pmj3b6gXZZkk-EoZ5HguWV8(Lcom/android/server/wm/AppResurrectionServiceImpl;Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$notifyWindowsDrawn$6(Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$qBpG3NxpPuEk0f4xy5ujJ-ezYBA(Lcom/android/server/wm/AppResurrectionServiceImpl;Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$notifyWindowsDrawn$5(Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$sBgvzErTrkVFc9eLjrRb93MMUPE(Lcom/android/server/wm/AppResurrectionServiceImpl;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$updateStartingWindowResolvedTheme$2(Ljava/lang/String;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmIsLoadDone(Lcom/android/server/wm/AppResurrectionServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsLoadDone:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMIUIBgHandler(Lcom/android/server/wm/AppResurrectionServiceImpl;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mMIUIBgHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsLoadDone(Lcom/android/server/wm/AppResurrectionServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsLoadDone:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetCloudData(Lcom/android/server/wm/AppResurrectionServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->getCloudData()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mloadDataFromSP(Lcom/android/server/wm/AppResurrectionServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->loadDataFromSP()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 8

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultAppResurrectionEnablePKGList:Ljava/util/ArrayList;

    .line 62
    const-string v1, "com.youku.phone"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    const-string v2, "com.qiyi.video"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    const-string v2, "com.hunantv.imgo.activity"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    const-string/jumbo v3, "tv.danmaku.bili"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    const-string v4, "com.tencent.qqlive"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    const-string v5, "com.qiyi.video.lite"

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultAppResurrectionEnableActivityList:Ljava/util/ArrayList;

    .line 73
    const-string v6, "com.tencent.qqlive/.ona.activity.VideoDetailActivity"

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultPkg2MaxChildCountJObject:Lorg/json/JSONObject;

    .line 79
    const/4 v7, 0x2

    :try_start_0
    invoke-virtual {v0, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    goto :goto_0

    .line 80
    :catch_0
    move-exception v0

    .line 85
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultResurrectionEnableReasonList:Ljava/util/ArrayList;

    .line 87
    const-string v7, "AutoIdleKill"

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    const-string v7, "AutoPowerKill"

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultRebootResurrectionEnablePkgList:Ljava/util/ArrayList;

    .line 93
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultRebootResurrectionEnableActivityList:Ljava/util/ArrayList;

    .line 102
    const-string/jumbo v1, "tv.danmaku.bili/.ui.video.VideoDetailsActivity"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    const-string/jumbo v1, "tv.danmaku.bili/com.bilibili.ship.theseus.all.UnitedBizDetailsActivity"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    const-string/jumbo v1, "tv.danmaku.bili/com.bilibili.ship.theseus.detail.UnitedBizDetailsActivity"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    const-string v1, "com.hunantv.imgo.activity/com.mgtv.ui.player.VodPlayerPageActivity"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    const-string v1, "com.hunantv.imgo.activity/com.mgtv.ui.videoplay.MGVideoPlayActivity"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v1, "com.youku.phone/com.youku.ui.activity.DetailActivity"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    const-string v1, "com.qiyi.video.lite/.videoplayer.activity.PlayerV2Activity"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .line 47
    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceStub;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    .line 52
    invoke-static {}, Lcom/android/server/FgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mLoggerHandler:Landroid/os/Handler;

    .line 53
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mMIUIBgHandler:Landroid/os/Handler;

    .line 54
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudManager:Lcom/android/server/wm/AppResurrectionCloudManager;

    .line 55
    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mTrackManager:Lcom/android/server/wm/AppResurrectionTrackManager;

    .line 57
    const-string v2, ""

    iput-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    .line 117
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    const-string/jumbo v5, "system"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v4, "app_resurrection_pkg2theme.xml"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mLocalSpFile:Ljava/io/File;

    .line 119
    iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    .line 129
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    .line 130
    iput-boolean v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z

    .line 131
    const-string v2, "local"

    iput-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionName:Ljava/lang/String;

    .line 132
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionVersion:J

    .line 133
    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionPKGJArray:Lorg/json/JSONArray;

    .line 134
    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionActivityJArray:Lorg/json/JSONArray;

    .line 135
    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionPKGJArray:Lorg/json/JSONArray;

    .line 136
    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionActivityJArray:Lorg/json/JSONArray;

    .line 137
    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudPkg2MaxChildCountJObject:Lorg/json/JSONObject;

    .line 139
    const/4 v1, 0x3

    iput v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I

    .line 140
    const/16 v1, 0x24

    iput v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionInactiveDurationHour:I

    .line 142
    const-wide/16 v1, 0xdac

    iput-wide v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J

    .line 143
    const-string v1, "Unknown"

    iput-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCPULevel:Ljava/lang/String;

    .line 146
    iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsLoadDone:Z

    .line 150
    iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsTestMode:Z

    return-void
.end method

.method private checkCPULevel(JJJJ)V
    .locals 4
    .param p1, "high"    # J
    .param p3, "middle"    # J
    .param p5, "low"    # J
    .param p7, "unknown"    # J

    .line 787
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const/4 v1, 0x1

    const-string v2, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeviceLevel.getDeviceLevel(DeviceLevel.DEV_STANDARD_VER, DeviceLevel.CPU): "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v3, Lmiui/util/DeviceLevel;->CPU:I

    invoke-static {v1, v3}, Lmiui/util/DeviceLevel;->getDeviceLevel(II)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeviceLevel.getDeviceLevel(DeviceLevel.DEV_STANDARD_VER, DeviceLevel.GPU): "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v3, Lmiui/util/DeviceLevel;->GPU:I

    invoke-static {v1, v3}, Lmiui/util/DeviceLevel;->getDeviceLevel(II)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeviceLevel.getDeviceLevel(DeviceLevel.DEV_STANDARD_VER, DeviceLevel.RAM): "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v3, Lmiui/util/DeviceLevel;->RAM:I

    invoke-static {v1, v3}, Lmiui/util/DeviceLevel;->getDeviceLevel(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    :cond_2
    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isCPUHighLevelDevice()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 792
    const-string v0, "High"

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCPULevel:Ljava/lang/String;

    .line 793
    iput-wide p1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J

    .line 794
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_6

    const-string v0, "checkCPULevel ret high"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 795
    :cond_3
    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isCPUMiddleLevelDevice()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 796
    const-string v0, "Middle"

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCPULevel:Ljava/lang/String;

    .line 797
    iput-wide p3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J

    .line 798
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_6

    const-string v0, "checkCPULevel ret middle"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 799
    :cond_4
    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isCPULowLevelDevice()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 800
    const-string v0, "Low"

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCPULevel:Ljava/lang/String;

    .line 801
    iput-wide p5, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J

    .line 802
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_6

    const-string v0, "checkCPULevel ret low"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 803
    :cond_5
    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isCPUUnknownLevelDevice()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 804
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCPULevel:Ljava/lang/String;

    .line 805
    iput-wide p7, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J

    .line 806
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_6

    const-string v0, "checkCPULevel ret unknown"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    :cond_6
    :goto_0
    return-void
.end method

.method private checkLaunchTimeOverThreshold(Ljava/lang/String;J)V
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "windowsDrawnDelay"    # J

    .line 582
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v1, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkLaunchTimeOverThreshold  enter delay:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", pkg: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    :cond_0
    iget-wide v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J

    cmp-long v0, p2, v2

    if-lez v0, :cond_2

    .line 584
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionPKGJArray:Lorg/json/JSONArray;

    const-string v2, "checkLaunchTimeOverThreshold = "

    if-nez v0, :cond_1

    .line 585
    sget-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultAppResurrectionEnablePKGList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 586
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", D: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 590
    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 591
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionPKGJArray:Lorg/json/JSONArray;

    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->removeKeyFromJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z

    .line 592
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", C: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_2
    :goto_0
    return-void
.end method

.method private getCloudData()V
    .locals 19

    .line 734
    move-object/from16 v9, p0

    iget-object v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_7

    .line 735
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "KeyAppResurrection"

    const/4 v2, 0x0

    const/4 v10, 0x0

    invoke-static {v0, v1, v2, v2, v10}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v11

    .line 736
    .local v11, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-nez v11, :cond_0

    .line 737
    return-void

    .line 740
    :cond_0
    invoke-virtual {v11}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v12

    .line 741
    .local v12, "cloudDataJson":Lorg/json/JSONObject;
    const-string v0, "AppResurrectionServiceImpl"

    if-eqz v12, :cond_6

    .line 742
    const-string v1, "getCloudData cloudDataJson !=null"

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    iget-boolean v1, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCloudData cloudDataJson:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    :cond_1
    const-string v0, "app_resur_enable"

    invoke-virtual {v12, v0, v10}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    .line 746
    const-string/jumbo v0, "version"

    const-wide/16 v1, 0x0

    invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionVersion:J

    .line 747
    const-string v0, "name"

    const-string v1, "local"

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionName:Ljava/lang/String;

    .line 748
    const-string v0, "app_resur_max_child_count"

    const/4 v1, 0x3

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I

    .line 749
    const-string v0, "app_resur_max_child_map"

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudPkg2MaxChildCountJObject:Lorg/json/JSONObject;

    .line 750
    const-string v0, "app_resur_inactive_hour"

    const/16 v1, 0x24

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionInactiveDurationHour:I

    .line 752
    const-string v0, "app_resur_high_cpu_launch_time_threshold"

    const-wide/16 v1, 0xbb8

    invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v13

    .line 753
    .local v13, "highCPULaunchTimeThreshold":J
    const-string v0, "app_resur_middle_cpu_launch_time_threshold"

    const-wide/16 v1, 0xdac

    invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v15

    .line 754
    .local v15, "middleCPULaunchTimeThreshold":J
    const-string v0, "app_resur_low_cpu_launch_time_threshold"

    invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v17

    .line 755
    .local v17, "lowCPULaunchTimeThreshold":J
    move-wide/from16 v7, v17

    .line 756
    .local v7, "unknwonCPULaunchTimeThreshold":J
    move-object/from16 v0, p0

    move-wide v1, v13

    move-wide v3, v15

    move-wide/from16 v5, v17

    invoke-direct/range {v0 .. v8}, Lcom/android/server/wm/AppResurrectionServiceImpl;->checkCPULevel(JJJJ)V

    .line 758
    const-string v0, "app_resur_pkg"

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 759
    .local v0, "appResurJArray":Lorg/json/JSONArray;
    if-eqz v0, :cond_2

    .line 760
    iput-object v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionPKGJArray:Lorg/json/JSONArray;

    .line 763
    :cond_2
    const-string v1, "app_resur_activity"

    invoke-virtual {v12, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 764
    .local v1, "appResurActivityJArray":Lorg/json/JSONArray;
    if-eqz v1, :cond_3

    .line 765
    iput-object v1, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionActivityJArray:Lorg/json/JSONArray;

    .line 768
    :cond_3
    const-string v2, "reboot_resur_pkg"

    invoke-virtual {v12, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 769
    .local v2, "appRebootResurJArray":Lorg/json/JSONArray;
    if-eqz v2, :cond_4

    .line 770
    iput-object v2, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionPKGJArray:Lorg/json/JSONArray;

    .line 773
    :cond_4
    const-string v3, "reboot_resur_activity"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 774
    .local v3, "appRebootResurActivityJArray":Lorg/json/JSONArray;
    if-eqz v3, :cond_5

    .line 775
    iput-object v3, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionActivityJArray:Lorg/json/JSONArray;

    .line 779
    :cond_5
    const-string v4, "reboot_resur_enable"

    invoke-virtual {v12, v4, v10}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z

    .line 780
    .end local v0    # "appResurJArray":Lorg/json/JSONArray;
    .end local v1    # "appResurActivityJArray":Lorg/json/JSONArray;
    .end local v2    # "appRebootResurJArray":Lorg/json/JSONArray;
    .end local v3    # "appRebootResurActivityJArray":Lorg/json/JSONArray;
    .end local v7    # "unknwonCPULaunchTimeThreshold":J
    .end local v13    # "highCPULaunchTimeThreshold":J
    .end local v15    # "middleCPULaunchTimeThreshold":J
    .end local v17    # "lowCPULaunchTimeThreshold":J
    goto :goto_0

    .line 781
    :cond_6
    const-string v1, "getCloudData cloudDataJson == null"

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    .end local v11    # "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v12    # "cloudDataJson":Lorg/json/JSONObject;
    :cond_7
    :goto_0
    return-void
.end method

.method private isCPUHighLevelDevice()Z
    .locals 3

    .line 811
    sget v0, Lmiui/util/DeviceLevel;->CPU:I

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lmiui/util/DeviceLevel;->getDeviceLevel(II)I

    move-result v0

    sget v2, Lmiui/util/DeviceLevel;->HIGH:I

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isCPULowLevelDevice()Z
    .locals 3

    .line 819
    sget v0, Lmiui/util/DeviceLevel;->CPU:I

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lmiui/util/DeviceLevel;->getDeviceLevel(II)I

    move-result v0

    sget v2, Lmiui/util/DeviceLevel;->LOW:I

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isCPUMiddleLevelDevice()Z
    .locals 3

    .line 815
    sget v0, Lmiui/util/DeviceLevel;->CPU:I

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lmiui/util/DeviceLevel;->getDeviceLevel(II)I

    move-result v0

    sget v2, Lmiui/util/DeviceLevel;->MIDDLE:I

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isCPUUnknownLevelDevice()Z
    .locals 3

    .line 823
    sget v0, Lmiui/util/DeviceLevel;->CPU:I

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lmiui/util/DeviceLevel;->getDeviceLevel(II)I

    move-result v0

    sget v2, Lmiui/util/DeviceLevel;->UNKNOWN:I

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isChildrenActivityFromSamePkg(Lcom/android/server/wm/Task;)Z
    .locals 5
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 715
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 716
    .local v0, "rootActivityPKG":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v2, "AppResurrectionServiceImpl"

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isChildrenActivitySameFromPkg ,rootActivityPKG:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", task.getRootActivity():"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    :cond_0
    new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 724
    .local v1, "notSamePKG":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_2

    .line 725
    iget-boolean v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isChildrenActivitySameFromPkg notSamePKGAR="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ret false"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    :cond_1
    const/4 v2, 0x0

    return v2

    .line 728
    :cond_2
    iget-boolean v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v3, :cond_3

    const-string v3, "isChildrenActivitySameFromPkg notSamePKGAR == null, ret true"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    :cond_3
    const/4 v2, 0x1

    return v2
.end method

.method private isDeviceSupport()Z
    .locals 2

    .line 710
    const-string v0, "persist.sys.app_resurrection.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 711
    .local v0, "isDeviceSupport":Z
    return v0
.end method

.method private isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z
    .locals 4
    .param p1, "jArray"    # Lorg/json/JSONArray;
    .param p2, "key"    # Ljava/lang/String;

    .line 673
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 674
    return v1

    .line 677
    :cond_0
    if-eqz p1, :cond_3

    .line 678
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 679
    .local v0, "len":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 680
    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 681
    const/4 v1, 0x1

    return v1

    .line 679
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 684
    .end local v2    # "i":I
    :cond_2
    return v1

    .line 686
    .end local v0    # "len":I
    :cond_3
    return v1
.end method

.method private isPKGInAppResurrectionActivityList(Ljava/lang/String;)Z
    .locals 6
    .param p1, "PKG"    # Ljava/lang/String;

    .line 837
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionActivityJArray:Lorg/json/JSONArray;

    const-string v1, "/"

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 838
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 839
    .local v0, "len":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_1

    .line 840
    iget-object v4, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionActivityJArray:Lorg/json/JSONArray;

    const-string v5, ""

    invoke-virtual {v4, v3, v5}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 841
    .local v4, "activityStr":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 842
    return v2

    .line 839
    .end local v4    # "activityStr":Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 845
    .end local v0    # "len":I
    .end local v3    # "i":I
    :cond_1
    goto :goto_2

    .line 846
    :cond_2
    sget-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultAppResurrectionEnableActivityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 847
    .local v3, "activityStr":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 848
    return v2

    .line 850
    .end local v3    # "activityStr":Ljava/lang/String;
    :cond_3
    goto :goto_1

    .line 852
    :cond_4
    :goto_2
    const/4 v0, 0x0

    return v0
.end method

.method private synthetic lambda$init$0()V
    .locals 9

    .line 195
    const-wide/16 v1, 0xbb8

    const-wide/16 v3, 0xdac

    const-wide/16 v5, 0xdac

    const-wide/16 v7, 0xdac

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/android/server/wm/AppResurrectionServiceImpl;->checkCPULevel(JJJJ)V

    return-void
.end method

.method private synthetic lambda$init$1()V
    .locals 0

    .line 198
    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->getCloudData()V

    return-void
.end method

.method private synthetic lambda$isChildrenActivityFromSamePkg$7(Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 3
    .param p1, "rootActivityPKG"    # Ljava/lang/String;
    .param p2, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 719
    iget-object v0, p2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 720
    .local v0, "ret":Z
    iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isChildrenActivitySameFromPkg getActivity, r.packageName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", rootActivityPKG="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ret="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AppResurrectionServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :cond_0
    return v0
.end method

.method private synthetic lambda$notifyWindowsDrawn$3(Landroid/content/ComponentName;JLjava/lang/String;)V
    .locals 1
    .param p1, "curComponent"    # Landroid/content/ComponentName;
    .param p2, "windowsDrawnDelay"    # J
    .param p4, "fKillReason"    # Ljava/lang/String;

    .line 568
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->logAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$notifyWindowsDrawn$4(Landroid/content/ComponentName;JLjava/lang/String;)V
    .locals 1
    .param p1, "curComponent"    # Landroid/content/ComponentName;
    .param p2, "windowsDrawnDelay"    # J
    .param p4, "fKillReason"    # Ljava/lang/String;

    .line 569
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->trackAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$notifyWindowsDrawn$5(Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V
    .locals 1
    .param p1, "curTransitionActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "windowsDrawnDelay"    # J
    .param p4, "fKillReason"    # Ljava/lang/String;

    .line 571
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->logAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$notifyWindowsDrawn$6(Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V
    .locals 1
    .param p1, "curTransitionActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "windowsDrawnDelay"    # J
    .param p4, "fKillReason"    # Ljava/lang/String;

    .line 572
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->trackAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$updateStartingWindowResolvedTheme$2(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "resolvedTheme"    # I

    .line 499
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->saveOneDataToSP(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method private loadDataFromSP()V
    .locals 9

    .line 896
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v1, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_0

    const-string v0, "loadDataFromSP start"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 899
    return-void

    .line 901
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mLocalSpFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 902
    .local v0, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 903
    .local v2, "keyValueMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 904
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 905
    .local v5, "pkg":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 906
    .local v6, "theme":Ljava/lang/Integer;
    iget-object v7, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    if-eqz v7, :cond_3

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 907
    iget-boolean v7, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v7, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "loadDataFromSP pkg="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", theme="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    :cond_2
    iget-object v7, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    invoke-virtual {v7, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 910
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v5    # "pkg":Ljava/lang/String;
    .end local v6    # "theme":Ljava/lang/Integer;
    :cond_3
    goto :goto_0

    .line 913
    .end local v0    # "sp":Landroid/content/SharedPreferences;
    .end local v2    # "keyValueMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_4
    goto :goto_1

    .line 911
    :catch_0
    move-exception v0

    .line 912
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadDataFromSP Exception e:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_5

    const-string v0, "loadDataFromSP done"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    :cond_5
    return-void
.end method

.method private logAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V
    .locals 2
    .param p1, "shortComponentName"    # Ljava/lang/String;
    .param p2, "delay"    # J
    .param p4, "killReason"    # Ljava/lang/String;

    .line 827
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "logAppResurrectionDisplayed, delay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", comp = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", killReason= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppResurrectionServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0xfb928

    invoke-static {v1, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 829
    return-void
.end method

.method private printList(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 660
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, ""

    .line 661
    .local v0, "ret":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 662
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 663
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 664
    .local v3, "pkg":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 666
    .end local v3    # "pkg":Ljava/lang/String;
    goto :goto_0

    .line 667
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 669
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    return-object v0
.end method

.method private removeKeyFromJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z
    .locals 4
    .param p1, "jArray"    # Lorg/json/JSONArray;
    .param p2, "key"    # Ljava/lang/String;

    .line 691
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 692
    return v1

    .line 695
    :cond_0
    if-eqz p1, :cond_3

    .line 696
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 697
    .local v0, "len":I
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 698
    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 699
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->remove(I)Ljava/lang/Object;

    .line 700
    const/4 v1, 0x1

    return v1

    .line 697
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 703
    .end local v2    # "index":I
    :cond_2
    return v1

    .line 705
    .end local v0    # "len":I
    :cond_3
    return v1
.end method

.method private saveAllDataToSP()V
    .locals 7

    .line 874
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v1, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_0

    const-string v0, "saveAllDataToSP"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 877
    return-void

    .line 879
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mLocalSpFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 880
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    if-eqz v2, :cond_3

    .line 881
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 882
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 883
    .local v4, "key":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 884
    .local v5, "value":Ljava/lang/Integer;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 885
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 887
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/Integer;
    :cond_2
    goto :goto_0

    .line 889
    :cond_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 892
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_1

    .line 890
    :catch_0
    move-exception v0

    .line 891
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveAllDataToSP Exception e:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private saveOneDataToSP(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "theme"    # Ljava/lang/Integer;

    .line 856
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v1, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveOneDataToSP, pkg="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", theme = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 858
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 859
    return-void

    .line 861
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mLocalSpFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 862
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 863
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 865
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 868
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_0

    .line 866
    :catch_0
    move-exception v0

    .line 867
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveAllDataToSP Exception e:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_3

    const-string v0, "saveOneDataToSP done"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    :cond_3
    return-void
.end method

.method private trackAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V
    .locals 2
    .param p1, "shortComponentName"    # Ljava/lang/String;
    .param p2, "delay"    # J
    .param p4, "killReason"    # Ljava/lang/String;

    .line 832
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "trackAppResurrectionDisplayed, delay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", comp = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", killReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppResurrectionServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mTrackManager:Lcom/android/server/wm/AppResurrectionTrackManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionTrackManager;->sendTrack(Ljava/lang/String;JLjava/lang/String;)V

    .line 834
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 615
    if-nez p1, :cond_0

    .line 616
    return-void

    .line 618
    :cond_0
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurDevice:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 619
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 620
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurVer:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionVersion:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 621
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurEnable:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 622
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurCPU:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCPULevel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 623
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurLaunchThres:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 624
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurMaxChildCount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 625
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurInactiveDuraH:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionInactiveDurationHour:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudPkg2MaxChildCountJObject:Lorg/json/JSONObject;

    if-eqz v0, :cond_1

    .line 629
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurPkg2MaxChildC:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudPkg2MaxChildCountJObject:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 630
    :cond_1
    sget-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultPkg2MaxChildCountJObject:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 631
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AppResurPkg2MaxChildD:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 634
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionPKGJArray:Lorg/json/JSONArray;

    if-eqz v0, :cond_3

    .line 635
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurPkgCArray:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionPKGJArray:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 637
    :cond_3
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurPkgDArray:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultAppResurrectionEnablePKGList:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->printList(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 640
    :goto_1
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionActivityJArray:Lorg/json/JSONArray;

    if-eqz v0, :cond_4

    .line 641
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurActivityCArray:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionActivityJArray:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 643
    :cond_4
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppResurActivityDArray:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultAppResurrectionEnableActivityList:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->printList(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 646
    :goto_2
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReResurEnable:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 647
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionPKGJArray:Lorg/json/JSONArray;

    if-eqz v0, :cond_5

    .line 648
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReResurPkgCArray:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionPKGJArray:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 650
    :cond_5
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReResurPkgDArray:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultRebootResurrectionEnablePkgList:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->printList(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 652
    :goto_3
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionActivityJArray:Lorg/json/JSONArray;

    if-eqz v0, :cond_6

    .line 653
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReResurActivityCArray:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionActivityJArray:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_4

    .line 655
    :cond_6
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReResurActivityDArray:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultRebootResurrectionEnableActivityList:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->printList(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 657
    :goto_4
    return-void
.end method

.method public getActivityIndex(Ljava/lang/String;)I
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .line 941
    invoke-virtual {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isActivityTag(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 942
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, "reborn_activity"

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    .local v0, "stringTokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 944
    .local v1, "index":I
    return v1

    .line 946
    .end local v0    # "stringTokenizer":Ljava/util/StringTokenizer;
    .end local v1    # "index":I
    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public getAllowChildCount(Ljava/lang/String;)I
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;

    .line 206
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudPkg2MaxChildCountJObject:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudPkg2MaxChildCountJObject:Lorg/json/JSONObject;

    iget v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    return v0

    .line 210
    :cond_0
    sget-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultPkg2MaxChildCountJObject:Lorg/json/JSONObject;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 211
    iget v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    return v0

    .line 213
    :cond_1
    iget v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I

    return v0
.end method

.method public getSplashScreenTheme(Ljava/lang/String;IZZZZ)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "resolvedTheme"    # I
    .param p3, "newTask"    # Z
    .param p4, "taskSwitch"    # Z
    .param p5, "processRunning"    # Z
    .param p6, "startActivity"    # Z

    .line 424
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 436
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v1, ", resolvedTheme:"

    const-string v2, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_1

    .line 437
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSplashScreenResolvedTheme, pkg:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", newTask:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", taskSwitch:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ",processRunning:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", startActivity:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :cond_1
    if-eqz p3, :cond_4

    if-eqz p4, :cond_4

    if-nez p5, :cond_4

    if-eqz p6, :cond_4

    .line 442
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    if-eqz v0, :cond_3

    .line 443
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_2

    .line 444
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 446
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    :cond_3
    return p2

    .line 449
    :cond_4
    if-nez p3, :cond_7

    if-eqz p4, :cond_7

    if-nez p5, :cond_7

    .line 451
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    if-eqz v0, :cond_6

    .line 452
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 453
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 454
    .local v0, "retResolvedTheme":I
    iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSplashScreenResolvedTheme reborn, get store "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", retResolvedTheme:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    :cond_5
    iput-object p1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    .line 457
    return v0

    .line 460
    .end local v0    # "retResolvedTheme":I
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSplashScreenResolvedTheme, no get "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    return p2

    .line 463
    :cond_7
    if-nez p3, :cond_9

    if-nez p4, :cond_9

    if-eqz p5, :cond_9

    if-nez p6, :cond_9

    .line 465
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_8

    const-string v0, "getSplashScreenResolvedTheme, back to main, resolvedTheme = 0"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 468
    :cond_9
    return p2

    .line 425
    :cond_a
    :goto_0
    return p2
.end method

.method public getStartingWindowResolvedTheme(Ljava/lang/String;ZZZZ)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "newTask"    # Z
    .param p3, "taskSwitch"    # Z
    .param p4, "processRunning"    # Z
    .param p5, "startActivity"    # Z

    .line 512
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 515
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v2, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStartingWindowResolvedTheme, start newTask="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", taskSwitch="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", processRunning="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", startActivity="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", mRebornPkg="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    :cond_1
    if-nez p2, :cond_3

    if-eqz p3, :cond_3

    if-nez p4, :cond_3

    .line 528
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    if-eqz v0, :cond_4

    .line 529
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 530
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 531
    .local v0, "retResolvedTheme":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStartingWindowResolvedTheme, get stored "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", retResolvedTheme:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    iput-object p1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    .line 534
    return v0

    .line 536
    .end local v0    # "retResolvedTheme":I
    :cond_2
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStartingWindowResolvedTheme, no get "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", ret 0"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 538
    :cond_3
    if-nez p2, :cond_4

    if-nez p3, :cond_4

    if-eqz p4, :cond_4

    if-nez p5, :cond_4

    .line 540
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_4

    const-string v0, "getStartingWindowResolvedTheme, back to main"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    :cond_4
    :goto_0
    return v1

    .line 513
    :cond_5
    :goto_1
    return v1
.end method

.method public init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 154
    iput-object p1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mContext:Landroid/content/Context;

    .line 155
    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isDeviceSupport()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    .line 156
    if-nez v0, :cond_0

    .line 157
    return-void

    .line 159
    :cond_0
    const-string v0, "persist.sys.test_app_resurrection.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsTestMode:Z

    .line 160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    .line 161
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2KillReasonMap:Ljava/util/HashMap;

    .line 162
    new-instance v0, Lcom/android/server/wm/AppResurrectionCloudManager;

    invoke-direct {v0, p1}, Lcom/android/server/wm/AppResurrectionCloudManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudManager:Lcom/android/server/wm/AppResurrectionCloudManager;

    .line 163
    new-instance v0, Lcom/android/server/wm/AppResurrectionTrackManager;

    invoke-direct {v0, p1}, Lcom/android/server/wm/AppResurrectionTrackManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mTrackManager:Lcom/android/server/wm/AppResurrectionTrackManager;

    .line 164
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mResolver:Landroid/content/ContentResolver;

    .line 165
    nop

    .line 166
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/AppResurrectionServiceImpl$1;

    iget-object v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mMIUIBgHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/wm/AppResurrectionServiceImpl$1;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Landroid/os/Handler;)V

    .line 165
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 176
    new-instance v0, Lcom/android/server/wm/AppResurrectionServiceImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/AppResurrectionServiceImpl$2;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 190
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 191
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 192
    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 195
    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mMIUIBgHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 198
    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mMIUIBgHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 199
    return-void
.end method

.method public isActivityTag(Ljava/lang/String;)Z
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .line 934
    const-string v0, "reborn_activity"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 935
    const/4 v0, 0x1

    return v0

    .line 936
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isRebootResurrectionActivity(Ljava/lang/String;)Z
    .locals 3
    .param p1, "activityName"    # Ljava/lang/String;

    .line 971
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsTestMode:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 972
    return v1

    .line 974
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z

    if-nez v0, :cond_1

    goto :goto_0

    .line 977
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionActivityJArray:Lorg/json/JSONArray;

    if-nez v0, :cond_2

    .line 978
    sget-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultRebootResurrectionEnableActivityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 979
    return v1

    .line 982
    :cond_2
    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 983
    return v1

    .line 987
    :cond_3
    return v2

    .line 975
    :cond_4
    :goto_0
    return v2
.end method

.method public isRebootResurrectionEnable(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .line 951
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsTestMode:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 952
    return v1

    .line 954
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z

    if-nez v0, :cond_1

    goto :goto_0

    .line 957
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionPKGJArray:Lorg/json/JSONArray;

    if-nez v0, :cond_2

    .line 958
    sget-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultRebootResurrectionEnablePkgList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 959
    return v1

    .line 962
    :cond_2
    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 963
    return v1

    .line 966
    :cond_3
    return v2

    .line 955
    :cond_4
    :goto_0
    return v2
.end method

.method public isRestoreTask(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 608
    nop

    .line 609
    const/4 v0, 0x0

    return v0
.end method

.method public isResurrectionEnable(Ljava/lang/String;)Z
    .locals 5
    .param p1, "pkg"    # Ljava/lang/String;

    .line 218
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v1, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isResurrectionEnable, start "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 220
    return v2

    .line 223
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    if-nez v0, :cond_2

    goto :goto_0

    .line 226
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionPKGJArray:Lorg/json/JSONArray;

    const/4 v3, 0x1

    const-string v4, "isResurrectionEnable, End "

    if-nez v0, :cond_4

    .line 227
    sget-object v0, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultAppResurrectionEnablePKGList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 228
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " in default, ret true"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_3
    return v3

    .line 232
    :cond_4
    invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 233
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " in cloud, ret true"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_5
    return v3

    .line 237
    :cond_6
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ret false"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :cond_7
    return v2

    .line 224
    :cond_8
    :goto_0
    return v2
.end method

.method public isResurrectionEnable(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 12
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;)Z"
        }
    .end annotation

    .line 243
    .local p3, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v0, 0x0

    .line 244
    .local v0, "ret":Z
    iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    if-eqz v1, :cond_16

    iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    if-nez v1, :cond_0

    goto/16 :goto_5

    .line 249
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isResurrectionEnable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 250
    return v0

    .line 253
    :cond_1
    const-string v1, ", reason:"

    const/4 v2, 0x0

    const-string v3, "AppResurrectionServiceImpl"

    if-eqz p3, :cond_14

    .line 254
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    .line 255
    .local v4, "tasksize":I
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_13

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 256
    .local v6, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    if-eqz v6, :cond_12

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    if-eqz v7, :cond_12

    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v7, :cond_12

    .line 257
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 258
    iget-boolean v5, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isResE, start "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", numAct:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", taskInfo="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isPKGInAppResurrectionActivityList(Ljava/lang/String;)Z

    move-result v5

    const-string v7, ", "

    const-string v8, "isResE, "

    if-eqz v5, :cond_9

    .line 262
    iget-object v5, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 263
    .local v5, "topActivityComp":Landroid/content/ComponentName;
    if-eqz v5, :cond_8

    .line 264
    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v9

    .line 265
    .local v9, "topActivityStr":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 266
    iget-boolean v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v10, :cond_4

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", topActivityStr:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_4
    iget-object v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionActivityJArray:Lorg/json/JSONArray;

    if-eqz v10, :cond_6

    .line 269
    invoke-direct {p0, v10, v9}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 270
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " not in Clist, ret f, setRemove t"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    return v2

    .line 274
    :cond_5
    iget-boolean v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v10, :cond_8

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", topActivity in Clist, go on"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 277
    :cond_6
    sget-object v10, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultAppResurrectionEnableActivityList:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 278
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " not in Dlist, ret f, setRemove t"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    return v2

    .line 282
    :cond_7
    iget-boolean v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v10, :cond_8

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", topActivity in DList, go on"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    .end local v5    # "topActivityComp":Landroid/content/ComponentName;
    .end local v9    # "topActivityStr":Ljava/lang/String;
    :cond_8
    :goto_1
    goto :goto_2

    .line 289
    :cond_9
    iget-boolean v5, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v5, :cond_a

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", not in activity list, go on"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :cond_a
    :goto_2
    invoke-virtual {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->getAllowChildCount(Ljava/lang/String;)I

    move-result v5

    .line 293
    .local v5, "allowChildCount":I
    iget v9, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I

    const/4 v10, 0x2

    if-ge v9, v10, :cond_b

    .line 294
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " too less, ret f, setRemove t"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    return v2

    .line 298
    :cond_b
    iget v9, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I

    if-le v9, v5, :cond_c

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " over allow "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", ret f, setRemove t"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    return v2

    .line 303
    :cond_c
    iget-object v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 304
    .local v7, "basePkg":Ljava/lang/String;
    iget-object v9, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    .line 305
    .local v9, "topPkg":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_d

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_d

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d

    .line 306
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " not "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " ret f, setRemove t"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    return v2

    .line 310
    :cond_d
    sget-object v2, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultResurrectionEnableReasonList:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 311
    iget-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2KillReasonMap:Ljava/util/HashMap;

    if-eqz v2, :cond_f

    .line 312
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    const/16 v8, 0xc8

    if-le v2, v8, :cond_e

    .line 313
    iget-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2KillReasonMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 315
    :cond_e
    iget-object v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2KillReasonMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    :cond_f
    const/4 v0, 0x1

    goto :goto_3

    .line 319
    :cond_10
    const/4 v0, 0x0

    .line 321
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isResE, final "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ret:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " setRemove:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    xor-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    return v0

    .line 324
    .end local v5    # "allowChildCount":I
    .end local v7    # "basePkg":Ljava/lang/String;
    .end local v9    # "topPkg":Ljava/lang/String;
    :cond_11
    iget-boolean v7, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v7, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isResE, start not equal, pkg: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", taskInfo.baseActivity.getPackageName():"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", continue"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 328
    :cond_12
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isResE error, pkg:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    .end local v6    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    goto/16 :goto_0

    .line 331
    .end local v4    # "tasksize":I
    :cond_13
    goto :goto_4

    .line 332
    :cond_14
    iget-boolean v4, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v4, :cond_15

    const-string v4, "isResE, tasks == null"

    invoke-static {v3, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_15
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isResE, End "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", ret: false, setRemove: ture"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return v2

    .line 245
    :cond_16
    :goto_5
    return v0
.end method

.method public isSaveTask(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 600
    nop

    .line 601
    const/4 v0, 0x0

    return v0
.end method

.method public keepTaskInRecent(Lcom/android/server/wm/Task;)Z
    .locals 12
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 342
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_11

    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    if-eqz v0, :cond_11

    if-nez p1, :cond_0

    goto/16 :goto_0

    .line 346
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    const-string v2, "AppResurrectionServiceImpl"

    if-nez v0, :cond_2

    .line 347
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keepTaskInRecent ret false, task.getRootActivity() == null task:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    :cond_1
    return v1

    .line 350
    :cond_2
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 352
    .local v0, "rootActivityPKG":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isResurrectionEnable(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 353
    iget-boolean v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keepTaskInRecent ret false, !isResurrectionEnable rootActivityPKG:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    :cond_3
    return v1

    .line 358
    :cond_4
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v3

    const/4 v4, 0x2

    const-string v5, ", "

    if-ge v3, v4, :cond_5

    .line 359
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keepTaskInRecent ret false, task.getChildCount() <2:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    return v1

    .line 363
    :cond_5
    iget-boolean v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v3, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keepTaskInRecent go on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :cond_6
    sget-object v3, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget v4, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionInactiveDurationHour:I

    int-to-long v6, v4

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    .line 366
    .local v3, "hoursInMs":J
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getInactiveDuration()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v6

    .line 369
    .local v6, "inactiveHours":J
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getInactiveDuration()J

    move-result-wide v8

    cmp-long v8, v8, v3

    if-lez v8, :cond_7

    .line 370
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "keepTaskInRecent ret false, task.getInactiveDuration > hours :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getInactiveDuration()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", 36H:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", inactiveHours="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    return v1

    .line 375
    :cond_7
    invoke-virtual {p0, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->getAllowChildCount(Ljava/lang/String;)I

    move-result v8

    .line 376
    .local v8, "allowChildCount":I
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v9

    if-le v9, v8, :cond_8

    .line 377
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "keepTaskInRecent ret false, task.getChildCount():"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " > allow:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    return v1

    .line 382
    :cond_8
    invoke-direct {p0, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isPKGInAppResurrectionActivityList(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 383
    iget-boolean v5, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v5, :cond_9

    const-string v5, "keepTaskInRecent pkg In ActivityList"

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_9
    const-string v5, ""

    .line 385
    .local v5, "topMostActivity":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopMostActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v9

    .line 386
    .local v9, "arTopMost":Lcom/android/server/wm/ActivityRecord;
    if-eqz v9, :cond_b

    .line 387
    iget-boolean v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v10, :cond_a

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "keepTaskInRecent arTopMost="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v9, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_a
    iget-object v5, v9, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    .line 392
    :cond_b
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 393
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "keepTaskInRecent topMostActivity is empty, ret false, "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    return v1

    .line 396
    :cond_c
    iget-object v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionActivityJArray:Lorg/json/JSONArray;

    if-eqz v10, :cond_d

    .line 397
    invoke-direct {p0, v10, v5}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_e

    .line 398
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "keepTaskInRecent topMostActivity is not in Clist, ret false, "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    return v1

    .line 402
    :cond_d
    sget-object v10, Lcom/android/server/wm/AppResurrectionServiceImpl;->sDefaultAppResurrectionEnableActivityList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    .line 403
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "keepTaskInRecent topMostActivity is not in Dlist, ret false, "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    return v1

    .line 411
    .end local v5    # "topMostActivity":Ljava/lang/String;
    .end local v9    # "arTopMost":Lcom/android/server/wm/ActivityRecord;
    :cond_e
    invoke-direct {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isChildrenActivityFromSamePkg(Lcom/android/server/wm/Task;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 412
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "keepTaskInRecent !isChildrenActivityFromSamePkg, ret false, "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    return v1

    .line 415
    :cond_f
    iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v1, :cond_10

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "keepTaskInRecent task:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_10
    const/4 v1, 0x1

    return v1

    .line 343
    .end local v0    # "rootActivityPKG":Ljava/lang/String;
    .end local v3    # "hoursInMs":J
    .end local v6    # "inactiveHours":J
    .end local v8    # "allowChildCount":I
    :cond_11
    :goto_0
    return v1
.end method

.method public notifyWindowsDrawn(JLcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/content/ComponentName;)V
    .locals 17
    .param p1, "windowsDrawnDelay"    # J
    .param p3, "lastLaunchedActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "curTransitionActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p5, "curComponent"    # Landroid/content/ComponentName;

    .line 547
    move-object/from16 v12, p0

    move-wide/from16 v13, p1

    move-object/from16 v15, p4

    iget-boolean v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    if-eqz v0, :cond_7

    iget-boolean v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 551
    :cond_0
    if-nez v15, :cond_1

    .line 552
    return-void

    .line 555
    :cond_1
    iget-boolean v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v1, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyWindowsDrawn, windowsDrawnDelay = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", lastLaunchedActivity="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v11, p3

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", curTransitionActivity="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mRebornPkg="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", curComponent:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p5 .. p5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move-object/from16 v11, p3

    .line 556
    :goto_0
    iget-boolean v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyWindowsDrawn, caller = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x23

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    :cond_3
    const-string v0, "AppResurrection_notifyWindowsDrawn"

    const-wide/16 v9, 0x8

    invoke-static {v9, v10, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 558
    iget-object v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    iget-object v1, v15, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 559
    const-string v0, "Unknown"

    .line 560
    .local v0, "killReason":Ljava/lang/String;
    iget-object v1, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2KillReasonMap:Ljava/util/HashMap;

    if-eqz v1, :cond_4

    .line 561
    iget-object v2, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 562
    iget-object v1, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2KillReasonMap:Ljava/util/HashMap;

    iget-object v2, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object/from16 v16, v0

    goto :goto_1

    .line 566
    :cond_4
    move-object/from16 v16, v0

    .end local v0    # "killReason":Ljava/lang/String;
    .local v16, "killReason":Ljava/lang/String;
    :goto_1
    move-object/from16 v5, v16

    .line 567
    .local v5, "fKillReason":Ljava/lang/String;
    if-eqz p5, :cond_5

    .line 568
    iget-object v6, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mLoggerHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda3;

    move-object v0, v7

    move-object/from16 v1, p0

    move-object/from16 v2, p5

    move-wide/from16 v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Landroid/content/ComponentName;JLjava/lang/String;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 569
    iget-object v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mMIUIBgHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda4;

    move-object v6, v1

    move-object/from16 v7, p0

    move-object/from16 v8, p5

    move-wide v2, v9

    move-wide/from16 v9, p1

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Landroid/content/ComponentName;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 571
    :cond_5
    move-wide v2, v9

    iget-object v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mLoggerHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda5;

    move-object v6, v1

    move-object/from16 v7, p0

    move-object/from16 v8, p4

    move-wide/from16 v9, p1

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 572
    iget-object v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mMIUIBgHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda6;

    move-object v6, v1

    invoke-direct/range {v6 .. v11}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 575
    :goto_2
    iget-object v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    invoke-direct {v12, v0, v13, v14}, Lcom/android/server/wm/AppResurrectionServiceImpl;->checkLaunchTimeOverThreshold(Ljava/lang/String;J)V

    .line 576
    const-string v0, ""

    iput-object v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mRebornPkg:Ljava/lang/String;

    goto :goto_3

    .line 558
    .end local v5    # "fKillReason":Ljava/lang/String;
    .end local v16    # "killReason":Ljava/lang/String;
    :cond_6
    move-wide v2, v9

    .line 578
    :goto_3
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 579
    return-void

    .line 548
    :cond_7
    :goto_4
    return-void
.end method

.method public saveActivityToXml(Lcom/android/server/wm/ActivityRecord;Lcom/android/modules/utils/TypedXmlSerializer;I)V
    .locals 3
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "out"    # Lcom/android/modules/utils/TypedXmlSerializer;
    .param p3, "index"    # I

    .line 919
    const-string v0, "reborn_activity"

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isPersistable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 922
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p2, v2, v1}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 923
    invoke-virtual {p1, p2}, Lcom/android/server/wm/ActivityRecord;->saveToXml(Lcom/android/modules/utils/TypedXmlSerializer;)V

    .line 924
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v2, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 928
    goto :goto_0

    .line 925
    :catch_0
    move-exception v0

    .line 926
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 927
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "savetoxml failed at pkg:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " activity:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TaskPersister"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 930
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method public updateStartingWindowResolvedTheme(Ljava/lang/String;IZZZZ)Z
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "resolvedTheme"    # I
    .param p3, "newTask"    # Z
    .param p4, "taskSwitch"    # Z
    .param p5, "processRunning"    # Z
    .param p6, "startActivity"    # Z

    .line 473
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 476
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    const-string v2, ", startActivity:"

    const-string v3, ",processRunning:"

    const-string v4, ", taskSwitch:"

    const-string v5, ", newTask:"

    const-string v6, ", resolvedTheme:"

    const-string v7, "AppResurrectionServiceImpl"

    if-eqz v0, :cond_1

    .line 477
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "updateSplashScreenResolvedTheme, start pkg:"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    :cond_1
    if-eqz p3, :cond_4

    if-eqz p4, :cond_4

    if-nez p5, :cond_4

    if-eqz p6, :cond_4

    .line 492
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    if-eqz v0, :cond_4

    .line 493
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_2

    .line 494
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 496
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mPkg2StartThemeMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mMIUIBgHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 500
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 501
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateSplashScreenResolvedTheme, final pkg:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_3
    const/4 v0, 0x1

    return v0

    .line 507
    :cond_4
    return v1

    .line 474
    :cond_5
    :goto_0
    return v1
.end method
