.class public Lcom/android/server/wm/MiuiFreeFormActivityStack;
.super Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
.source "MiuiFreeFormActivityStack.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field isNormalFreeForm:Z

.field mCornerPosition:I

.field private final mEnterMiniFreeformFromRect:Landroid/graphics/Rect;

.field private mEnterMiniFreeformReason:Ljava/lang/String;

.field mFreeFormLaunchFromTaskId:I

.field mFreeFormScale:F

.field private mHadHideStackFromFullScreen:Z

.field mHasHadStackAdded:Z

.field mIsForegroundPin:Z

.field mIsFrontFreeFormStackInfo:Z

.field mIsLandcapeFreeform:Z

.field mMiuiFreeFromWindowMode:I

.field private mNeedAnimation:Z

.field mPinFloatingWindowPos:Landroid/graphics/Rect;

.field mShouldDelayDispatchFreeFormStackModeChanged:Z

.field mTask:Lcom/android/server/wm/Task;

.field pinActiveTime:J

.field timestamp:J


# direct methods
.method public constructor <init>(Lcom/android/server/wm/Task;)V
    .locals 1
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 147
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;-><init>(Lcom/android/server/wm/Task;I)V

    .line 148
    return-void
.end method

.method public constructor <init>(Lcom/android/server/wm/Task;I)V
    .locals 9
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "miuiFreeFromWindowMode"    # I

    .line 150
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStackStub;-><init>()V

    .line 31
    const-string v0, "MiuiFreeFormActivityStack"

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHasHadStackAdded:Z

    .line 34
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z

    .line 41
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z

    .line 42
    iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z

    .line 43
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mPinFloatingWindowPos:Landroid/graphics/Rect;

    .line 47
    const/4 v2, -0x1

    iput v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mCornerPosition:I

    .line 48
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mEnterMiniFreeformFromRect:Landroid/graphics/Rect;

    .line 50
    iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    .line 52
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z

    .line 53
    iput v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormLaunchFromTaskId:I

    .line 151
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 152
    iget-object v1, p1, Lcom/android/server/wm/Task;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->isNormalFreeForm(Lcom/android/server/wm/Task;Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    .line 155
    invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity(Z)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 156
    .local v1, "top":Lcom/android/server/wm/ActivityRecord;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    iget-object v3, p1, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 160
    .local v3, "rInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v1, :cond_0

    .line 161
    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getOverrideOrientation()I

    move-result v4

    invoke-static {v4}, Landroid/util/MiuiMultiWindowUtils;->isOrientationLandscape(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    goto :goto_0

    .line 162
    :cond_0
    if-eqz v3, :cond_1

    .line 163
    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v4, v4, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    invoke-static {v4}, Landroid/util/MiuiMultiWindowUtils;->isOrientationLandscape(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    .line 165
    :cond_1
    :goto_0
    iget-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getForceLandscapeApplication()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    or-int/2addr v4, v5

    iput-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    .line 167
    invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V

    .line 168
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v4, v4, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 169
    iget-object v4, p1, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    iget-boolean v6, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    iget-object v7, p1, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v7, v7, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 170
    invoke-static {v7}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v7

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v8

    .line 169
    invoke-static {v4, v5, v6, v7, v8}, Landroid/util/MiuiMultiWindowUtils;->getOriFreeformScale(Landroid/content/Context;ZZZLjava/lang/String;)F

    move-result v4

    iput v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    goto :goto_1

    .line 172
    :cond_2
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v4, v4, Lcom/android/server/wm/Task;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v4, v5}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F

    move-result v4

    iput v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    .line 174
    :goto_1
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z

    .line 175
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z

    .line 210
    return-void
.end method


# virtual methods
.method public getApplicationName()Ljava/lang/String;
    .locals 4

    .line 243
    const/4 v0, 0x0

    .line 244
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 246
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v2, v2, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    move-object v0, v2

    .line 247
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .line 250
    goto :goto_0

    .line 248
    :catch_0
    move-exception v2

    .line 249
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    .line 251
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    const-string v2, ""

    .line 252
    .local v2, "applicationName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 253
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v2, v3

    check-cast v2, Ljava/lang/String;

    .line 255
    :cond_0
    return-object v2
.end method

.method public getEnterMiniFreeformReason()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mEnterMiniFreeformReason:Ljava/lang/String;

    return-object v0
.end method

.method public getEnterMiniFreeformRect()Landroid/graphics/Rect;
    .locals 2

    .line 68
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mEnterMiniFreeformFromRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getFreeFormLaunchFromTaskId()I
    .locals 1

    .line 129
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormLaunchFromTaskId:I

    return v0
.end method

.method public getFreeFormScale()F
    .locals 1

    .line 121
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    return v0
.end method

.method public getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    .locals 5

    .line 304
    new-instance v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    invoke-direct {v0}, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;-><init>()V

    .line 305
    .local v0, "freeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->stackId:I

    .line 306
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->bounds:Landroid/graphics/Rect;

    .line 307
    iget v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I

    .line 308
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    .line 309
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v1

    iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->displayId:I

    .line 310
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getUid()I

    move-result v1

    iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->userId:I

    .line 311
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iput-object v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->configuration:Landroid/content/res/Configuration;

    .line 312
    iget v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 313
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->smallWindowBounds:Landroid/graphics/Rect;

    goto :goto_0

    .line 314
    :cond_0
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 315
    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->bounds:Landroid/graphics/Rect;

    invoke-direct {v1, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 316
    .local v1, "smallWindowBounds":Landroid/graphics/Rect;
    iget v2, v1, Landroid/graphics/Rect;->left:I

    .line 317
    .local v2, "left":I
    iget v3, v1, Landroid/graphics/Rect;->top:I

    .line 318
    .local v3, "top":I
    iget v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    invoke-virtual {v1, v4}, Landroid/graphics/Rect;->scale(F)V

    .line 319
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 320
    iput-object v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->smallWindowBounds:Landroid/graphics/Rect;

    .line 322
    .end local v1    # "smallWindowBounds":Landroid/graphics/Rect;
    .end local v2    # "left":I
    .end local v3    # "top":I
    :cond_1
    :goto_0
    iget v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->freeFormScale:F

    .line 323
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v1

    iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->inPinMode:Z

    .line 324
    iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z

    iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->isForegroundPin:Z

    .line 325
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mPinFloatingWindowPos:Landroid/graphics/Rect;

    iput-object v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->pinFloatingWindowPos:Landroid/graphics/Rect;

    .line 326
    iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->isNormalFreeForm:Z

    .line 327
    iget v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mCornerPosition:I

    iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->cornerPosition:I

    .line 328
    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mEnterMiniFreeformFromRect:Landroid/graphics/Rect;

    invoke-direct {v1, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->enterMiniFreeformFromRect:Landroid/graphics/Rect;

    .line 329
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mEnterMiniFreeformReason:Ljava/lang/String;

    iput-object v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->enterMiniFreeformReason:Ljava/lang/String;

    .line 330
    iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->isLandcapeFreeform:Z

    .line 331
    iget-wide v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->timestamp:J

    iput-wide v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->timestamp:J

    .line 332
    iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z

    iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->hadHideStackFormFullScreen:Z

    .line 333
    iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z

    iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->needAnimation:Z

    .line 334
    return-object v0
.end method

.method public getMiuiFreeFromWindowMode()I
    .locals 1

    .line 109
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    return v0
.end method

.method public getStackPackageName()Ljava/lang/String;
    .locals 4

    .line 222
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 223
    return-object v1

    .line 225
    :cond_0
    iget-object v0, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    .line 226
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->behindAppLockPkg:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->behindAppLockPkg:Ljava/lang/String;

    .line 227
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->behindAppLockPkg:Ljava/lang/String;

    return-object v0

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    if-eqz v0, :cond_2

    .line 231
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 233
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 236
    :cond_3
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 237
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    return-object v0

    .line 239
    :cond_4
    return-object v1
.end method

.method public getUid()I
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v0, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    .line 214
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v0, v0, Lcom/android/server/wm/Task;->originatingUid:I

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v0, v0, Lcom/android/server/wm/Task;->originatingUid:I

    return v0

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v0, v0, Lcom/android/server/wm/Task;->mUserId:I

    return v0
.end method

.method inMiniPinMode()Z
    .locals 2

    .line 93
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method inNormalPinMode()Z
    .locals 2

    .line 89
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method inPinMode()Z
    .locals 2

    .line 85
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method isFrontFreeFormStackInfo()Z
    .locals 1

    .line 350
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z

    return v0
.end method

.method isHideStackFromFullScreen()Z
    .locals 1

    .line 338
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z

    return v0
.end method

.method public isInFreeFormMode()Z
    .locals 1

    .line 117
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInMiniFreeFormMode()Z
    .locals 2

    .line 113
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isLandcapeFreeform()Z
    .locals 1

    .line 101
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    return v0
.end method

.method public isNeedAnimation()Z
    .locals 1

    .line 60
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z

    return v0
.end method

.method public isNormalFreeForm()Z
    .locals 1

    .line 140
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    return v0
.end method

.method onMiuiFreeFormStasckAdded()V
    .locals 0

    .line 57
    return-void
.end method

.method public setCornerPosition(I)V
    .locals 0
    .param p1, "cornerPosition"    # I

    .line 97
    iput p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mCornerPosition:I

    .line 98
    return-void
.end method

.method public setEnterMiniFreeformReason(Ljava/lang/String;)V
    .locals 0
    .param p1, "enterMiniFreeformReason"    # Ljava/lang/String;

    .line 76
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mEnterMiniFreeformReason:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setEnterMiniFreeformRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "enterMiniFreeformFromRect"    # Landroid/graphics/Rect;

    .line 80
    if-nez p1, :cond_0

    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mEnterMiniFreeformFromRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 82
    return-void
.end method

.method public setFreeformScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .line 125
    iput p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    .line 126
    return-void
.end method

.method setFreeformTimestamp(J)V
    .locals 0
    .param p1, "lastTimestamp"    # J

    .line 143
    iput-wide p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->timestamp:J

    .line 144
    return-void
.end method

.method setHideStackFromFullScreen(Z)V
    .locals 0
    .param p1, "hidden"    # Z

    .line 342
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z

    .line 343
    return-void
.end method

.method setIsFrontFreeFormStackInfo(Z)V
    .locals 0
    .param p1, "isFrontFreeFormStackInfo"    # Z

    .line 346
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z

    .line 347
    return-void
.end method

.method public setNeedAnimation(Z)V
    .locals 0
    .param p1, "needAnimation"    # Z

    .line 64
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z

    .line 65
    return-void
.end method

.method public setStackFreeFormMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .line 105
    iput p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    .line 106
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 261
    .local v0, "info":Ljava/lang/StringBuilder;
    const-string v1, "MiuiFreeFormActivityStack{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mStackID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mPackageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mFreeFormScale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mMiuiFreeFromWindowMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mConfiguration="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mNeedAnimation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mIsLandcapeFreeform="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mPinFloatingWindowPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mPinFloatingWindowPos:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " isNormalFreeForm="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHadHideStackFromFullScreen= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mIsFrontFreeFormStackInfo= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " timestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->timestamp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v1, :cond_0

    .line 296
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mTask.inFreeformWindowingMode()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mTask.originatingUid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v2, v2, Lcom/android/server/wm/Task;->originatingUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    :cond_0
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 300
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
