public class com.android.server.wm.AsyncRotationControllerImpl implements com.android.server.wm.AsyncRotationControllerStub {
	 /* .source "AsyncRotationControllerImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final java.util.HashSet mHiddenWindowList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashSet<", */
	 /* "Lcom/android/server/wm/WindowState;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.lang.Runnable mTimeoutRunnable;
/* # direct methods */
public static void $r8$lambda$3HL6_5LEBxjD_a_x7lqumJ6h4sg ( com.android.server.wm.AsyncRotationControllerImpl p0, com.android.server.wm.WindowManagerService p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/AsyncRotationControllerImpl;->lambda$scheduleRestoreHiddenWindowTimeout$0(Lcom/android/server/wm/WindowManagerService;)V */
return;
} // .end method
public com.android.server.wm.AsyncRotationControllerImpl ( ) {
/* .locals 1 */
/* .line 15 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 17 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mHiddenWindowList = v0;
return;
} // .end method
private Boolean isValid ( com.android.server.wm.WindowState p0 ) {
/* .locals 1 */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 99 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 v0 = this.mToken;
	 v0 = this.mSurfaceControl;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 v0 = this.mToken;
		 v0 = this.mSurfaceControl;
		 v0 = 		 (( android.view.SurfaceControl ) v0 ).isValid ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 100 */
			 int v0 = 1; // const/4 v0, 0x1
			 /* .line 102 */
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 private void lambda$scheduleRestoreHiddenWindowTimeout$0 ( com.android.server.wm.WindowManagerService p0 ) { //synthethic
		 /* .locals 6 */
		 /* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
		 /* .line 78 */
		 v0 = this.mGlobalLock;
		 /* monitor-enter v0 */
		 /* .line 79 */
		 try { // :try_start_0
			 (( com.android.server.wm.WindowManagerService ) p1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
			 v1 = 			 (( com.android.server.wm.DisplayContent ) v1 ).hasTopFixedRotationLaunchingApp ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->hasTopFixedRotationLaunchingApp()Z
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* monitor-exit v0 */
				 return;
				 /* .line 80 */
			 } // :cond_0
			 final String v1 = "AsyncRotationControllerImpl"; // const-string v1, "AsyncRotationControllerImpl"
			 final String v2 = "Restore hidden window timeout"; // const-string v2, "Restore hidden window timeout"
			 android.util.Slog .i ( v1,v2 );
			 /* .line 81 */
			 v1 = this.mHiddenWindowList;
			 (( java.util.HashSet ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
		 v2 = 		 } // :goto_0
		 if ( v2 != null) { // if-eqz v2, :cond_2
			 /* check-cast v2, Lcom/android/server/wm/WindowState; */
			 /* .line 82 */
			 /* .local v2, "w":Lcom/android/server/wm/WindowState; */
			 v3 = 			 /* invoke-direct {p0, v2}, Lcom/android/server/wm/AsyncRotationControllerImpl;->isValid(Lcom/android/server/wm/WindowState;)Z */
			 if ( v3 != null) { // if-eqz v3, :cond_1
				 /* .line 83 */
				 v3 = this.mToken;
				 (( com.android.server.wm.WindowToken ) v3 ).getPendingTransaction ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowToken;->getPendingTransaction()Landroid/view/SurfaceControl$Transaction;
				 v4 = this.mToken;
				 v4 = this.mSurfaceControl;
				 /* const/high16 v5, 0x3f800000 # 1.0f */
				 (( android.view.SurfaceControl$Transaction ) v3 ).setAlpha ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
				 /* .line 85 */
			 } // .end local v2 # "w":Lcom/android/server/wm/WindowState;
		 } // :cond_1
		 /* .line 86 */
	 } // :cond_2
	 v1 = this.mHiddenWindowList;
	 (( java.util.HashSet ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->clear()V
	 /* .line 87 */
	 /* monitor-exit v0 */
	 /* .line 88 */
	 return;
	 /* .line 87 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
/* # virtual methods */
public void clearHiddenWindow ( com.android.server.wm.WindowManagerService p0 ) {
	 /* .locals 3 */
	 /* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
	 /* .line 34 */
	 v0 = this.mHiddenWindowList;
	 v0 = 	 (( java.util.HashSet ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 return;
		 /* .line 35 */
	 } // :cond_0
	 v0 = this.mGlobalLock;
	 /* monitor-enter v0 */
	 /* .line 36 */
	 try { // :try_start_0
		 final String v1 = "AsyncRotationControllerImpl"; // const-string v1, "AsyncRotationControllerImpl"
		 final String v2 = "Clear HiddenWindowList"; // const-string v2, "Clear HiddenWindowList"
		 android.util.Slog .i ( v1,v2 );
		 /* .line 37 */
		 v1 = this.mHiddenWindowList;
		 (( java.util.HashSet ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->clear()V
		 /* .line 38 */
		 /* monitor-exit v0 */
		 /* .line 39 */
		 return;
		 /* .line 38 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
	 } // .end method
	 public Boolean excludeFromFadeRotationAnimation ( com.android.server.wm.WindowState p0, Boolean p1 ) {
		 /* .locals 3 */
		 /* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
		 /* .param p2, "isFade" # Z */
		 /* .line 21 */
		 int v0 = 0; // const/4 v0, 0x0
		 if ( p1 != null) { // if-eqz p1, :cond_4
			 v1 = this.mAttrs;
			 /* if-nez v1, :cond_0 */
			 /* .line 22 */
		 } // :cond_0
		 if ( p2 != null) { // if-eqz p2, :cond_3
			 /* .line 23 */
			 v1 = 			 (( com.android.server.wm.WindowState ) p1 ).getWindowType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I
			 /* const/16 v2, 0x7e1 */
			 /* if-ne v1, v2, :cond_1 */
			 /* .line 24 */
			 (( com.android.server.wm.WindowState ) p1 ).getWindowTag ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;
			 final String v2 = "control_center"; // const-string v2, "control_center"
			 v1 = 			 (( java.lang.Object ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
			 /* if-nez v1, :cond_2 */
		 } // :cond_1
		 v1 = this.mAttrs;
		 /* iget v1, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
		 /* const/high16 v2, 0x10000000 */
		 /* and-int/2addr v1, v2 */
		 if ( v1 != null) { // if-eqz v1, :cond_3
			 /* .line 27 */
		 } // :cond_2
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 30 */
	 } // :cond_3
	 /* .line 21 */
} // :cond_4
} // :goto_0
} // .end method
public void removeHiddenWindowTimeoutCallbacks ( com.android.server.wm.WindowManagerService p0 ) {
/* .locals 2 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .line 95 */
v0 = this.mH;
v1 = this.mTimeoutRunnable;
(( com.android.server.wm.WindowManagerService$H ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 96 */
return;
} // .end method
public void scheduleRestoreHiddenWindowTimeout ( com.android.server.wm.WindowManagerService p0 ) {
/* .locals 4 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .line 75 */
v0 = this.mHiddenWindowList;
v0 = (( java.util.HashSet ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 76 */
} // :cond_0
v0 = this.mTimeoutRunnable;
/* if-nez v0, :cond_1 */
/* .line 77 */
/* new-instance v0, Lcom/android/server/wm/AsyncRotationControllerImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0, p1}, Lcom/android/server/wm/AsyncRotationControllerImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/AsyncRotationControllerImpl;Lcom/android/server/wm/WindowManagerService;)V */
this.mTimeoutRunnable = v0;
/* .line 90 */
} // :cond_1
v0 = this.mH;
v1 = this.mTimeoutRunnable;
/* const-wide/16 v2, 0x7d0 */
(( com.android.server.wm.WindowManagerService$H ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/WindowManagerService$H;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 92 */
return;
} // .end method
public void updateRoundedCornerAlpha ( com.android.server.wm.WindowManagerService p0 ) {
/* .locals 5 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .line 42 */
v0 = this.mHiddenWindowList;
v0 = (( java.util.HashSet ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 43 */
} // :cond_0
v0 = this.mHiddenWindowList;
(( java.util.HashSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lcom/android/server/wm/WindowState; */
/* .line 44 */
/* .local v1, "w":Lcom/android/server/wm/WindowState; */
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/AsyncRotationControllerImpl;->isValid(Lcom/android/server/wm/WindowState;)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 45 */
v2 = this.mToken;
(( com.android.server.wm.WindowToken ) v2 ).getPendingTransaction ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowToken;->getPendingTransaction()Landroid/view/SurfaceControl$Transaction;
v3 = this.mToken;
v3 = this.mSurfaceControl;
int v4 = 0; // const/4 v4, 0x0
(( android.view.SurfaceControl$Transaction ) v2 ).setAlpha ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 47 */
} // .end local v1 # "w":Lcom/android/server/wm/WindowState;
} // :cond_1
/* .line 48 */
} // :cond_2
v0 = this.mH;
v1 = this.mTimeoutRunnable;
(( com.android.server.wm.WindowManagerService$H ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 49 */
(( com.android.server.wm.AsyncRotationControllerImpl ) p0 ).scheduleRestoreHiddenWindowTimeout ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/AsyncRotationControllerImpl;->scheduleRestoreHiddenWindowTimeout(Lcom/android/server/wm/WindowManagerService;)V
/* .line 50 */
return;
} // .end method
public void updateRoundedCornerAlpha ( com.android.server.wm.WindowManagerService p0, com.android.server.wm.WindowState p1 ) {
/* .locals 4 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "w" # Lcom/android/server/wm/WindowState; */
/* .line 53 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 54 */
try { // :try_start_0
v1 = this.mToken;
/* iget-boolean v1, v1, Lcom/android/server/wm/WindowToken;->mRoundedCornerOverlay:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = /* invoke-direct {p0, p2}, Lcom/android/server/wm/AsyncRotationControllerImpl;->isValid(Lcom/android/server/wm/WindowState;)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 55 */
v1 = this.mTransactionFactory;
/* check-cast v1, Landroid/view/SurfaceControl$Transaction; */
v2 = this.mToken;
v2 = this.mSurfaceControl;
/* .line 56 */
int v3 = 0; // const/4 v3, 0x0
(( android.view.SurfaceControl$Transaction ) v1 ).setAlpha ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 57 */
(( android.view.SurfaceControl$Transaction ) v1 ).apply ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 58 */
v1 = this.mHiddenWindowList;
(( java.util.HashSet ) v1 ).add ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 60 */
} // :cond_0
/* monitor-exit v0 */
/* .line 61 */
return;
/* .line 60 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateRoundedCornerAlpha ( com.android.server.wm.WindowManagerService p0, com.android.server.wm.WindowState p1, Float p2, android.view.SurfaceControl$Transaction p3 ) {
/* .locals 3 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "w" # Lcom/android/server/wm/WindowState; */
/* .param p3, "alpha" # F */
/* .param p4, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .line 65 */
v0 = this.mHiddenWindowList;
v0 = (( java.util.HashSet ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z
/* if-nez v0, :cond_2 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* cmpl-float v0, p3, v0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 66 */
} // :cond_0
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 67 */
try { // :try_start_0
(( com.android.server.wm.WindowManagerService ) p1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
v1 = (( com.android.server.wm.DisplayContent ) v1 ).hasTopFixedRotationLaunchingApp ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->hasTopFixedRotationLaunchingApp()Z
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.mToken;
/* iget-boolean v1, v1, Lcom/android/server/wm/WindowToken;->mRoundedCornerOverlay:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 68 */
	 v1 = 	 /* invoke-direct {p0, p2}, Lcom/android/server/wm/AsyncRotationControllerImpl;->isValid(Lcom/android/server/wm/WindowState;)Z */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 69 */
		 v1 = this.mToken;
		 v1 = this.mSurfaceControl;
		 int v2 = 0; // const/4 v2, 0x0
		 (( android.view.SurfaceControl$Transaction ) p4 ).setAlpha ( v1, v2 ); // invoke-virtual {p4, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
		 /* .line 71 */
	 } // :cond_1
	 /* monitor-exit v0 */
	 /* .line 72 */
	 return;
	 /* .line 71 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 65 */
} // :cond_2
} // :goto_0
return;
} // .end method
