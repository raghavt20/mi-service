class com.android.server.wm.MiuiSizeCompatJob$H extends android.os.Handler {
	 /* .source "MiuiSizeCompatJob.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiSizeCompatJob; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # static fields */
private static final Integer MSG_BIND_ONE_TRACK;
private static final Integer MSG_TRACK_EVENT;
/* # instance fields */
final com.android.server.wm.MiuiSizeCompatJob this$0; //synthetic
/* # direct methods */
public com.android.server.wm.MiuiSizeCompatJob$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 77 */
this.this$0 = p1;
/* .line 78 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 79 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 83 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "MiuiSizeCompatJob"; // const-string v1, "MiuiSizeCompatJob"
/* packed-switch v0, :pswitch_data_0 */
/* .line 95 */
final String v0 = "Error message what!"; // const-string v0, "Error message what!"
android.util.Slog .e ( v1,v0 );
/* .line 92 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.wm.MiuiSizeCompatJob .-$$Nest$mbindOneTrackService ( v0 );
/* .line 93 */
/* .line 85 */
/* :pswitch_1 */
v0 = this.obj;
/* instance-of v0, v0, Landroid/app/job/JobParameters; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 86 */
	 v0 = this.this$0;
	 v1 = this.obj;
	 /* check-cast v1, Landroid/app/job/JobParameters; */
	 com.android.server.wm.MiuiSizeCompatJob .-$$Nest$mtrackEvent ( v0,v1 );
	 /* .line 88 */
} // :cond_0
final String v0 = "Error obj ,not job params"; // const-string v0, "Error obj ,not job params"
android.util.Log .e ( v1,v0 );
/* .line 90 */
/* nop */
/* .line 98 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
