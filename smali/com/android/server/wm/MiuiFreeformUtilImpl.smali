.class public Lcom/android/server/wm/MiuiFreeformUtilImpl;
.super Ljava/lang/Object;
.source "MiuiFreeformUtilImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiFreeformUtilStub;


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiFreeformUtilImpl"

.field public static final UNKNOWN:Ljava/lang/String; = "unknown1"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$isSupportFreeFormInDesktopMode$0(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p0, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 638
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->isFullScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    const/4 v0, 0x1

    return v0

    .line 641
    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public adjuestFrameForChild(Lcom/android/server/wm/WindowState;)V
    .locals 0
    .param p1, "win"    # Lcom/android/server/wm/WindowState;

    .line 440
    invoke-static {p1}, Lcom/android/server/wm/WindowStateStubImpl;->adjuestFrameForChild(Lcom/android/server/wm/WindowState;)V

    .line 441
    return-void
.end method

.method public adjuestFreeFormTouchRegion(Lcom/android/server/wm/WindowState;Landroid/graphics/Region;)V
    .locals 0
    .param p1, "win"    # Lcom/android/server/wm/WindowState;
    .param p2, "outRegion"    # Landroid/graphics/Region;

    .line 444
    invoke-static {p1, p2}, Lcom/android/server/wm/WindowStateStubImpl;->adjuestFreeFormTouchRegion(Lcom/android/server/wm/WindowState;Landroid/graphics/Region;)V

    .line 445
    return-void
.end method

.method public adjuestScaleAndFrame(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/Task;)V
    .locals 0
    .param p1, "win"    # Lcom/android/server/wm/WindowState;
    .param p2, "task"    # Lcom/android/server/wm/Task;

    .line 436
    invoke-static {p1, p2}, Lcom/android/server/wm/WindowStateStubImpl;->adjuestScaleAndFrame(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/Task;)V

    .line 437
    return-void
.end method

.method public adjustTopActivityIfNeed(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;
    .locals 3
    .param p1, "oriTop"    # Lcom/android/server/wm/ActivityRecord;

    .line 584
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 585
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->isAlwaysOnTop()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/TaskDisplayArea;->topRunningActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 588
    .local v0, "topAr":Lcom/android/server/wm/ActivityRecord;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " adjustTopActivityIfNeed topAr: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFreeformUtilImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    return-object v0

    .line 591
    .end local v0    # "topAr":Lcom/android/server/wm/ActivityRecord;
    :cond_0
    return-object p1
.end method

.method public checkFreeformSupport(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/app/ActivityOptions;)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "options"    # Landroid/app/ActivityOptions;

    .line 391
    invoke-static {p1, p2}, Lcom/android/server/wm/ActivityStarterInjector;->checkFreeformSupport(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/app/ActivityOptions;)V

    .line 392
    return-void
.end method

.method public getAbnormalFreeformBlackList(Z)Ljava/util/List;
    .locals 1
    .param p1, "fromSystem"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 79
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->getAbnormalFreeformBlackList(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAbnormalFreeformWhiteList(Z)Ljava/util/List;
    .locals 1
    .param p1, "fromSystem"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 87
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->getAbnormalFreeformWhiteList(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAdditionalFreeformAspectRatio1Apps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 350
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getAdditionalFreeformAspectRatio1Apps()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAdditionalFreeformAspectRatio2Apps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 358
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getAdditionalFreeformAspectRatio2Apps()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationLockActivityList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 226
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getApplicationLockActivityList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAudioForegroundPinAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 270
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getAudioForegroundPinAppList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCanStartActivityFromFullscreenToFreefromList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 58
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getCanStartActivityFromFullscreenToFreefromList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCvwUnsupportedFreeformWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 211
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getCvwUnsupportedFreeformWhiteList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDesktopFreeformWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 361
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getDesktopFreeformWhiteList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDesktopModeLaunchFreeformIgnoreTranslucentAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 675
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getDesktopModeLaunchFreeformIgnoreTranslucentAppList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDesktopModeLaunchFullscreenAppList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 655
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v0

    .line 656
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    move-object v1, v0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v1
.end method

.method public getDesktopModeLaunchFullscreenNotHideOtherAppList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 663
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getDesktopModeLaunchFullscreenNotHideOtherAppList()Ljava/util/List;

    move-result-object v0

    .line 664
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    move-object v1, v0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v1
.end method

.method public getEnableAbnormalFreeFormDebug(Z)I
    .locals 1
    .param p1, "fromSystem"    # Z

    .line 334
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->getEnableAbnormalFreeFormDebug(Z)I

    move-result v0

    return v0
.end method

.method public getFixedRotationAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 294
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFixedRotationAppList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getForceLandscapeApplication()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 242
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getForceLandscapeApplication()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getForegroundPinAppBlackList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 286
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getForegroundPinAppBlackList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getForegroundPinAppWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 278
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getForegroundPinAppWhiteList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeFormAccessibleArea(Landroid/content/Context;Lcom/android/server/wm/DisplayContent;)Landroid/graphics/Rect;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 547
    if-nez p2, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    return-object v0

    .line 548
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getInsetsStateController()Lcom/android/server/wm/InsetsStateController;

    move-result-object v0

    .line 549
    .local v0, "insetsStateController":Lcom/android/server/wm/InsetsStateController;
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getRotation()I

    move-result v2

    .line 550
    invoke-static {v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->getStatusBarHeight(Lcom/android/server/wm/InsetsStateController;)I

    move-result v3

    .line 551
    invoke-static {v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->getNavBarHeight(Lcom/android/server/wm/InsetsStateController;)I

    move-result v4

    iget-object v1, p2, Lcom/android/server/wm/DisplayContent;->mDisplayFrames:Lcom/android/server/wm/DisplayFrames;

    .line 552
    invoke-static {v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->getDisplayCutoutHeight(Lcom/android/server/wm/DisplayFrames;)I

    move-result v5

    .line 553
    invoke-static {p1}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v6

    .line 549
    move-object v1, p1

    invoke-static/range {v1 .. v6}, Landroid/util/MiuiMultiWindowUtils;->getFreeFormAccessibleArea(Landroid/content/Context;IIIIZ)Landroid/graphics/Rect;

    move-result-object v1

    return-object v1
.end method

.method public getFreeformBlackList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 66
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformBlackList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformBlackList(Landroid/content/Context;Ljava/util/HashMap;Z)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "isNeedUpdateList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList<",
            "Ljava/lang/Long;",
            ">;>;Z)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 71
    .local p2, "allTimestamps":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/LinkedList<Ljava/lang/Long;>;>;"
    invoke-static {p1, p2, p3}, Landroid/util/MiuiMultiWindowUtils;->getFreeformBlackList(Landroid/content/Context;Ljava/util/HashMap;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformCaptionInsetsHeightToZeroList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 234
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformCaptionInsetsHeightToZeroList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformDisableOverlayList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 159
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformDisableOverlayList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformIgnoreRequestOrientationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 167
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformIgnoreRequestOrientationList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformNeedRelunchList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 175
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformNeedRelunchList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformRecommendMapApplicationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 103
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformRecommendMapApplicationList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformRecommendWaitingApplicationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 111
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformRecommendWaitingApplicationList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformRect(Landroid/content/Context;ZZZZLandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "needDisplayContentRotation"    # Z
    .param p3, "isVertical"    # Z
    .param p4, "isMiniFreeformMode"    # Z
    .param p5, "isFreeformLandscape"    # Z
    .param p6, "outBounds"    # Landroid/graphics/Rect;

    .line 378
    invoke-static/range {p1 .. p6}, Landroid/util/MiuiMultiWindowUtils;->getFreeformRect(Landroid/content/Context;ZZZZLandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformResizeableWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 207
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformResizeableWhiteList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFreeformRoundCorner()F
    .locals 1

    .line 432
    sget v0, Landroid/util/MiuiMultiWindowUtils;->FREEFORM_ROUND_CORNER:F

    return v0
.end method

.method public getFreeformScale(Landroid/app/ActivityOptions;)F
    .locals 4
    .param p1, "options"    # Landroid/app/ActivityOptions;

    .line 557
    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getActivityOptionsInjector"

    invoke-static {p1, v2, v1}, Landroid/util/MiuiMultiWindowUtils;->isMethodExist(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 559
    .local v1, "method":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 561
    :try_start_0
    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "getFreeformScale"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 563
    :catch_0
    move-exception v0

    .line 564
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 567
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public getFreeformVideoWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 95
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getFreeformVideoWhiteList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getHideSelfIfNewFreeformTaskWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 191
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getHideSelfIfNewFreeformTaskWhiteList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getHotSpaceBottomOffsetPad()F
    .locals 1

    .line 518
    sget v0, Landroid/util/MiuiMultiWindowUtils;->HOT_SPACE_BOTTOM_OFFSITE_PAD:I

    int-to-float v0, v0

    return v0
.end method

.method public getHotSpaceOffset()F
    .locals 1

    .line 514
    sget v0, Landroid/util/MiuiMultiWindowUtils;->HOT_SPACE_OFFSITE:I

    int-to-float v0, v0

    return v0
.end method

.method public getHotSpaceResizeOffsetPad()I
    .locals 1

    .line 534
    const/16 v0, 0x21

    return v0
.end method

.method public getHotSpaceTopCaptionUpwardsOffset()I
    .locals 1

    .line 538
    const/16 v0, 0x16

    return v0
.end method

.method public getLaunchInTaskList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 326
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getLaunchInTaskList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLockModeActivityList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 151
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getAboutLockModeActivityList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMiniFreeformPaddingStroke()F
    .locals 1

    .line 522
    const/high16 v0, 0x41a00000    # 20.0f

    return v0
.end method

.method public getMiuiMultiWindowUtilsScale()F
    .locals 1

    .line 416
    sget v0, Landroid/util/MiuiMultiWindowUtils;->sScale:F

    return v0
.end method

.method public getRotationFromDisplayApp()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 302
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getRotationFromDisplayApp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSensorDisableList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 342
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getSensorDisableList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSensorDisableWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 318
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getSensorDisableWhiteList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getShadowRadius()F
    .locals 1

    .line 473
    const/high16 v0, 0x43c80000    # 400.0f

    return v0
.end method

.method public getShowHiddenTaskIfFinishedWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 199
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getShowHiddenTaskIfFinishedWhiteList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getStartFromFreeformBlackList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 183
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getStartFromFreeformBlackList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSupportCvwLevelFullList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 119
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getSupportCvwLevelFullList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSupportCvwLevelHorizontalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 135
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getSupportCvwLevelHorizontalList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSupportCvwLevelVerticalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 127
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getSupportCvwLevelVerticalList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTopDecorCaptionViewHeight()I
    .locals 1

    .line 428
    sget v0, Landroid/util/MiuiMultiWindowUtils;->TOP_DECOR_CAPTIONVIEW_HEIGHT:I

    return v0
.end method

.method public getTopGameList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 250
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getTopGameList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTopVideoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 258
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getTopVideoList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUnsupportCvwLevelFullList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 143
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getUnsupportCvwLevelFullList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUseDefaultCameraPipelineApp()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 310
    invoke-static {}, Landroid/util/MiuiMultiWindowAdapter;->getUseDefaultCameraPipelineApp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public handleFreeformModeRequst(Landroid/os/IBinder;ILandroid/content/Context;)I
    .locals 1
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "cmd"    # I
    .param p3, "mContext"    # Landroid/content/Context;

    .line 407
    invoke-static {p1, p2, p3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->handleFreeformModeRequst(Landroid/os/IBinder;ILandroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public ignoreClearTaskFlagInFreeForm(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 3
    .param p1, "reusedActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "startActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 489
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_1

    .line 493
    :cond_0
    const-string v1, "com.tencent.mobileqq/com.tencent.open.agent.AgentActivity"

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    .line 494
    const-string v2, "com.tencent.mobileqq/.activity.LoginActivity"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    nop

    .line 493
    :goto_0
    return v0

    .line 491
    :cond_2
    :goto_1
    return v0
.end method

.method public inFreeformWhiteList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 219
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->inFreeformWhiteList(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public initDesktop(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 618
    invoke-static {p1}, Lcom/android/server/wm/MiuiDesktopModeUtils;->initDesktop(Landroid/content/Context;)V

    .line 619
    return-void
.end method

.method public isAppLockActivity(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z
    .locals 1
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "cls"    # Landroid/content/ComponentName;

    .line 762
    invoke-static {p1, p2}, Landroid/util/MiuiMultiWindowAdapter;->isAppLockActivity(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method

.method public isAppSetAutoUiInDesktopMode(Lcom/android/server/wm/ActivityRecord;ILandroid/content/Context;)Z
    .locals 6
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "windowingMode"    # I
    .param p3, "context"    # Landroid/content/Context;

    .line 724
    const-string v0, "android.window.PROPERTY_AUTOUI_ALLOW_SYSTEM_OVERRIDE"

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    if-nez p2, :cond_5

    if-nez p1, :cond_0

    goto/16 :goto_5

    .line 727
    :cond_0
    const/4 v1, -0x1

    .line 728
    .local v1, "autouiAllowApplication":I
    const/4 v3, -0x1

    .line 730
    .local v3, "autouiAllowComponent":I
    :try_start_0
    invoke-virtual {p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 731
    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getProperty(Ljava/lang/String;Ljava/lang/String;)Landroid/content/pm/PackageManager$Property;

    move-result-object v4

    .line 732
    .local v4, "propertyAutouiApplication":Landroid/content/pm/PackageManager$Property;
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$Property;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 733
    const/4 v1, -0x1

    goto :goto_0

    .line 735
    :cond_1
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z

    move-result v5
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v5

    .line 739
    .end local v4    # "propertyAutouiApplication":Landroid/content/pm/PackageManager$Property;
    :goto_0
    goto :goto_1

    .line 737
    :catch_0
    move-exception v4

    .line 741
    :goto_1
    :try_start_1
    invoke-virtual {p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 742
    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getProperty(Ljava/lang/String;Landroid/content/ComponentName;)Landroid/content/pm/PackageManager$Property;

    move-result-object v0

    .line 743
    .local v0, "propertyAutouiComponent":Landroid/content/pm/PackageManager$Property;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 744
    const/4 v3, -0x1

    goto :goto_2

    .line 746
    :cond_2
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z

    move-result v4
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move v3, v4

    .line 750
    .end local v0    # "propertyAutouiComponent":Landroid/content/pm/PackageManager$Property;
    :goto_2
    goto :goto_3

    .line 748
    :catch_1
    move-exception v0

    .line 751
    :goto_3
    const/4 v0, 0x1

    if-eq v3, v0, :cond_4

    if-ne v1, v0, :cond_3

    if-eqz v3, :cond_3

    goto :goto_4

    .line 758
    :cond_3
    return v2

    .line 753
    :cond_4
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "autouiAllow:: r.packageName="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", r.mActivityComponent ="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", autouiAllowApplication="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", autouiAllowComponent="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "MiuiFreeformUtilImpl"

    invoke-static {v4, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 756
    return v0

    .line 725
    .end local v1    # "autouiAllowApplication":I
    .end local v3    # "autouiAllowComponent":I
    :cond_5
    :goto_5
    return v2
.end method

.method public isDeskTopModeActive()Z
    .locals 1

    .line 621
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v0

    return v0
.end method

.method public isDisableSplashScreenInFreeFormTaskSwitch(Ljava/lang/String;)Z
    .locals 1
    .param p1, "componentName"    # Ljava/lang/String;

    .line 485
    const-string v0, "com.tencent.qqlive/com.tencent.tauth.AuthActivity"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEnableAbnormalFreeform(Ljava/lang/String;Landroid/content/Context;Ljava/util/List;Ljava/util/List;I)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p5, "enableAbnormalFreeform"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)Z"
        }
    .end annotation

    .line 395
    .local p3, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1, p2, p3, p4, p5}, Landroid/util/MiuiMultiWindowUtils;->isEnableAbnormalFreeform(Ljava/lang/String;Landroid/content/Context;Ljava/util/List;Ljava/util/List;I)Z

    move-result v0

    return v0
.end method

.method public isEnalbleAbnormalFreeform(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 576
    invoke-virtual {p0, p2, p1}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isLandscapeGameApp(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isPadScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 578
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getAbnormalFreeformBlackList(Z)Ljava/util/List;

    move-result-object v4

    .line 579
    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getAbnormalFreeformWhiteList(Z)Ljava/util/List;

    move-result-object v5

    .line 580
    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getEnableAbnormalFreeFormDebug(Z)I

    move-result v6

    .line 577
    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isEnableAbnormalFreeform(Ljava/lang/String;Landroid/content/Context;Ljava/util/List;Ljava/util/List;I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 580
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    nop

    .line 576
    :goto_1
    return v0
.end method

.method public isForceResizeable()Z
    .locals 1

    .line 403
    invoke-static {}, Landroid/util/MiuiMultiWindowUtils;->isForceResizeable()Z

    move-result v0

    return v0
.end method

.method public isFullScreenStrategyNeededInDesktopMode(Lcom/android/server/wm/ActivityRecord;I)Z
    .locals 3
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "windowingMode"    # I

    .line 682
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    if-nez p2, :cond_3

    if-nez p1, :cond_0

    goto :goto_1

    .line 685
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v0

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 686
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v0

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 690
    :cond_1
    return v1

    .line 687
    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " isFullScreenStrategyNeededInDesktopMode, launch fullscreen, r.shortComponentName= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreeformUtilImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    const/4 v0, 0x1

    return v0

    .line 683
    :cond_3
    :goto_1
    return v1
.end method

.method public isFullScreenStrategyNeededInDesktopMode(Ljava/lang/String;)Z
    .locals 2
    .param p1, "shortComponentName"    # Ljava/lang/String;

    .line 697
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 698
    return v1

    .line 700
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 701
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " isFullScreenStrategyNeededInDesktopMode, launch fullscreen, shortComponentName= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFreeformUtilImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    const/4 v0, 0x1

    return v0

    .line 704
    :cond_1
    return v1
.end method

.method public isLandscapeGameApp(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .line 399
    invoke-static {p1, p2}, Landroid/util/MiuiMultiWindowUtils;->isLandscapeGameApp(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public isLaunchNotesInKeyGuard(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .line 708
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    if-nez p1, :cond_0

    goto :goto_2

    .line 711
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 712
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    const-string v2, "scene"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const-string v2, ""

    .line 713
    .local v2, "scene":Ljava/lang/String;
    :goto_0
    const-string v3, "keyguard"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 714
    const-string v3, "off_screen"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 717
    :cond_2
    return v1

    .line 715
    :cond_3
    :goto_1
    const/4 v1, 0x1

    return v1

    .line 709
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "scene":Ljava/lang/String;
    :cond_4
    :goto_2
    return v1
.end method

.method public isMIUIProduct()Z
    .locals 1

    .line 609
    sget-boolean v0, Landroid/os/Build;->IS_MIUI:Z

    return v0
.end method

.method public isMiuiBuild()Z
    .locals 2

    .line 459
    nop

    .line 460
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 459
    xor-int/lit8 v0, v0, 0x1

    const-string v1, "persist.sys.miui_optimization"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isMiuiCvwFeatureEnable()Z
    .locals 1

    .line 469
    const/4 v0, 0x0

    return v0
.end method

.method public isOrientationLandscape(I)Z
    .locals 1
    .param p1, "orientation"    # I

    .line 424
    invoke-static {p1}, Landroid/util/MiuiMultiWindowUtils;->isOrientationLandscape(I)Z

    move-result v0

    return v0
.end method

.method public isPadScreen(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 506
    invoke-static {p1}, Landroid/util/MiuiMultiWindowUtils;->isPadScreen(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public isSkipUpdateFreeFormShadow(Lcom/android/server/wm/Task;)Z
    .locals 4
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 498
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 499
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 501
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    .line 500
    const-string v3, "com.eg.android.AlipayGphone/com.alipay.mobile.quinox.SchemeLauncherActivity"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    nop

    .line 502
    .local v0, "result":Z
    :goto_0
    return v0
.end method

.method public isSupportFreeFormInDesktopMode(Lcom/android/server/wm/Task;)Z
    .locals 5
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 624
    const/4 v0, 0x0

    if-eqz p1, :cond_7

    .line 625
    iget-object v1, p1, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 626
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 627
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 628
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "unknown"

    :goto_0
    nop

    .line 629
    .local v1, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 630
    .local v2, "r":Lcom/android/server/wm/ActivityRecord;
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    if-eqz v2, :cond_3

    .line 631
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    .line 634
    :cond_3
    if-nez v2, :cond_4

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;

    move-result-object v3

    iget-object v4, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 635
    return v0

    .line 637
    :cond_4
    new-instance v0, Lcom/android/server/wm/MiuiFreeformUtilImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiFreeformUtilImpl$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 643
    .local v0, "fullScreenActivity":Lcom/android/server/wm/ActivityRecord;
    const/4 v3, 0x1

    if-eqz v0, :cond_5

    .line 644
    return v3

    .line 646
    :cond_5
    iget-boolean v4, p1, Lcom/android/server/wm/Task;->mLaunchFullScreenInDesktopMode:Z

    xor-int/2addr v3, v4

    return v3

    .line 632
    .end local v0    # "fullScreenActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_6
    :goto_1
    return v0

    .line 649
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v2    # "r":Lcom/android/server/wm/ActivityRecord;
    :cond_7
    return v0
.end method

.method public isSupportFreeFormMultiTask(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 615
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->isSupportFreeFormMultiTask(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isUseFreeFormAnimation(I)Z
    .locals 1
    .param p1, "transit"    # I

    .line 464
    invoke-static {p1}, Lcom/android/server/wm/AppTransitionInjector;->isUseFreeFormAnimation(I)Z

    move-result v0

    return v0
.end method

.method public loadFreeFormAnimation(Lcom/android/server/wm/WindowManagerService;IZLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)Landroid/view/animation/Animation;
    .locals 1
    .param p1, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "transit"    # I
    .param p3, "enter"    # Z
    .param p4, "frame"    # Landroid/graphics/Rect;
    .param p5, "container"    # Lcom/android/server/wm/WindowContainer;

    .line 412
    invoke-static {p1, p2, p3, p4, p5}, Lcom/android/server/wm/AppTransitionInjector;->loadFreeFormAnimation(Lcom/android/server/wm/WindowManagerService;IZLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public modifyLaunchActivityOptionIfNeed(Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/RootWindowContainer;Ljava/lang/String;Landroid/app/ActivityOptions;Lcom/android/server/wm/WindowProcessController;Landroid/content/Intent;ILandroid/content/pm/ActivityInfo;Lcom/android/server/wm/ActivityRecord;)Landroid/app/ActivityOptions;
    .locals 1
    .param p1, "service"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "root"    # Lcom/android/server/wm/RootWindowContainer;
    .param p3, "callingPackgae"    # Ljava/lang/String;
    .param p4, "options"    # Landroid/app/ActivityOptions;
    .param p5, "callerApp"    # Lcom/android/server/wm/WindowProcessController;
    .param p6, "intent"    # Landroid/content/Intent;
    .param p7, "userId"    # I
    .param p8, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p9, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 386
    invoke-static/range {p1 .. p9}, Lcom/android/server/wm/ActivityStarterInjector;->modifyLaunchActivityOptionIfNeed(Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/RootWindowContainer;Ljava/lang/String;Landroid/app/ActivityOptions;Lcom/android/server/wm/WindowProcessController;Landroid/content/Intent;ILandroid/content/pm/ActivityInfo;Lcom/android/server/wm/ActivityRecord;)Landroid/app/ActivityOptions;

    move-result-object v0

    return-object v0
.end method

.method public multiFreeFormSupported(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 510
    invoke-static {p1}, Landroid/util/MiuiMultiWindowUtils;->multiFreeFormSupported(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public needRelunchFreeform(Ljava/lang/String;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "tempConfig"    # Landroid/content/res/Configuration;
    .param p3, "fullConfig"    # Landroid/content/res/Configuration;

    .line 526
    invoke-static {p1, p2, p3}, Landroid/util/MiuiMultiWindowAdapter;->needRelunchFreeform(Ljava/lang/String;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Z

    move-result v0

    return v0
.end method

.method public notNeedRelunchFreeform(Ljava/lang/String;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "tempConfig"    # Landroid/content/res/Configuration;
    .param p3, "fullConfig"    # Landroid/content/res/Configuration;

    .line 373
    invoke-static {p1, p2, p3}, Landroid/util/MiuiMultiWindowAdapter;->notNeedRelunchFreeform(Ljava/lang/String;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Z

    move-result v0

    return v0
.end method

.method public onForegroundWindowChanged(Lcom/android/server/wm/WindowProcessController;Landroid/content/pm/ActivityInfo;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;)V
    .locals 0
    .param p1, "app"    # Lcom/android/server/wm/WindowProcessController;
    .param p2, "info"    # Landroid/content/pm/ActivityInfo;
    .param p3, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "state"    # Lcom/android/server/wm/ActivityRecord$State;

    .line 449
    invoke-static {p1, p2, p3, p4}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundWindowChanged(Lcom/android/server/wm/WindowProcessController;Landroid/content/pm/ActivityInfo;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;)V

    .line 450
    return-void
.end method

.method public reviewFreeFormBounds(Landroid/graphics/Rect;Landroid/graphics/Rect;FLandroid/graphics/Rect;)F
    .locals 1
    .param p1, "currentBounds"    # Landroid/graphics/Rect;
    .param p2, "newBounds"    # Landroid/graphics/Rect;
    .param p3, "currentScale"    # F
    .param p4, "accessibleArea"    # Landroid/graphics/Rect;

    .line 543
    invoke-static {p1, p2, p3, p4}, Landroid/util/MiuiMultiWindowUtils;->reviewFreeFormBounds(Landroid/graphics/Rect;Landroid/graphics/Rect;FLandroid/graphics/Rect;)F

    move-result v0

    return v0
.end method

.method public setAbnormalFreeformBlackList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 75
    .local p1, "abnormalFreeformBlackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setAbnormalFreeformBlackList(Ljava/util/List;)V

    .line 76
    return-void
.end method

.method public setAbnormalFreeformWhiteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 83
    .local p1, "abnormalFreeformWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setAbnormalFreeformWhiteList(Ljava/util/List;)V

    .line 84
    return-void
.end method

.method public setAdditionalFreeformAspectRatio1Apps(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 346
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setAdditionalFreeformAspectRatio1Apps(Ljava/util/List;)V

    .line 347
    return-void
.end method

.method public setAdditionalFreeformAspectRatio2Apps(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 354
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setAdditionalFreeformAspectRatio2Apps(Ljava/util/List;)V

    .line 355
    return-void
.end method

.method public setApplicationLockActivityList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 222
    .local p1, "applicationLockActivityList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setApplicationLockActivityList(Ljava/util/List;)V

    .line 223
    return-void
.end method

.method public setAudioForegroundPinAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 266
    .local p1, "audioForegroundPinAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setAudioForegroundPinAppList(Ljava/util/List;)V

    .line 267
    return-void
.end method

.method public setCanStartActivityFromFullscreenToFreefromList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 54
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setCanStartActivityFromFullscreenToFreefromList(Ljava/util/List;)V

    .line 55
    return-void
.end method

.method public setCvwUnsupportedFreeformWhiteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 215
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setCvwUnsupportedFreeformWhiteList(Ljava/util/List;)V

    .line 216
    return-void
.end method

.method public setDesktopFreeformWhiteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 365
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setDesktopFreeformWhiteList(Ljava/util/List;)V

    .line 366
    return-void
.end method

.method public setDesktopModeLaunchFreeformIgnoreTranslucentAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 671
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setDesktopModeLaunchFreeformIgnoreTranslucentAppList(Ljava/util/List;)V

    .line 672
    return-void
.end method

.method public setDesktopModeLaunchFullscreenAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 659
    .local p1, "needRelunchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setDesktopModeLaunchFullscreenAppList(Ljava/util/List;)V

    .line 660
    return-void
.end method

.method public setDesktopModeLaunchFullscreenNotHideOtherAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 668
    .local p1, "needRelunchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setDesktopModeLaunchFullscreenNotHideOtherAppList(Ljava/util/List;)V

    .line 669
    return-void
.end method

.method public setEnableAbnormalFreeFormDebug(I)V
    .locals 0
    .param p1, "enableAbnormalFreeFormDebug"    # I

    .line 330
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setEnableAbnormalFreeFormDebug(I)V

    .line 331
    return-void
.end method

.method public setEnableForegroundPin(Z)V
    .locals 0
    .param p1, "enableForegroundPin"    # Z

    .line 262
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setEnableForegroundPin(Z)V

    .line 263
    return-void
.end method

.method public setFixedRotationAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 290
    .local p1, "fixedRotationAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFixedRotationAppList(Ljava/util/List;)V

    .line 291
    return-void
.end method

.method public setForceLandscapeApplication(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 238
    .local p1, "forceLandscapeApplication":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setForceLandscapeApplication(Ljava/util/List;)V

    .line 239
    return-void
.end method

.method public setForegroundPinAppBlackList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 282
    .local p1, "foregroundPinAppBlackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setForegroundPinAppBlackList(Ljava/util/List;)V

    .line 283
    return-void
.end method

.method public setForegroundPinAppWhiteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 274
    .local p1, "foregroundPinAppWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setForegroundPinAppWhiteList(Ljava/util/List;)V

    .line 275
    return-void
.end method

.method public setFreeformBlackList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 62
    .local p1, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFreeformBlackList(Ljava/util/List;)V

    .line 63
    return-void
.end method

.method public setFreeformCaptionInsetsHeightToZeroList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 230
    .local p1, "freeformCaptionInsetsHeightToZeroList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFreeformCaptionInsetsHeightToZeroList(Ljava/util/List;)V

    .line 231
    return-void
.end method

.method public setFreeformDisableOverlayList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 155
    .local p1, "disableOverlayList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFreeformDisableOverlayList(Ljava/util/List;)V

    .line 156
    return-void
.end method

.method public setFreeformIgnoreRequestOrientationList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 163
    .local p1, "ignoreRequestOrientationList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFreeformIgnoreRequestOrientationList(Ljava/util/List;)V

    .line 164
    return-void
.end method

.method public setFreeformNeedRelunchList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 171
    .local p1, "needRelunchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFreeformNeedRelunchList(Ljava/util/List;)V

    .line 172
    return-void
.end method

.method public setFreeformRecommendMapApplicationList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 99
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFreeformRecommendMapApplicationList(Ljava/util/List;)V

    .line 100
    return-void
.end method

.method public setFreeformRecommendWaitingApplicationList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 107
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFreeformRecommendWaitingApplicationList(Ljava/util/List;)V

    .line 108
    return-void
.end method

.method public setFreeformResizeableWhiteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 203
    .local p1, "resizeableWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFreeformResizeableWhiteList(Ljava/util/List;)V

    .line 204
    return-void
.end method

.method public setFreeformScale(FLandroid/app/ActivityOptions;)Z
    .locals 7
    .param p1, "scale"    # F
    .param p2, "options"    # Landroid/app/ActivityOptions;

    .line 595
    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getActivityOptionsInjector"

    invoke-static {p2, v2, v1}, Landroid/util/MiuiMultiWindowUtils;->isMethodExist(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 597
    .local v1, "method":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_1

    .line 599
    :try_start_0
    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {v1, p2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const-string/jumbo v3, "setFreeformScale"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    .line 600
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v0

    .line 599
    invoke-static {v2, v3, v5}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    move v0, v4

    :cond_0
    return v0

    .line 601
    :catch_0
    move-exception v2

    .line 602
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 605
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    return v0
.end method

.method public setFreeformVideoWhiteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 91
    .local p1, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setFreeformVideoWhiteList(Ljava/util/List;)V

    .line 92
    return-void
.end method

.method public setHideSelfIfNewFreeformTaskWhiteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 187
    .local p1, "hideSelfIfNewFreeformTaskWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setHideSelfIfNewFreeformTaskWhiteList(Ljava/util/List;)V

    .line 188
    return-void
.end method

.method public setLaunchInTaskList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 322
    .local p1, "launchInTaskList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setLaunchInTaskList(Ljava/util/List;)V

    .line 323
    return-void
.end method

.method public setLockModeActivityList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 147
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setAboutLockModeActivityList(Ljava/util/List;)V

    .line 148
    return-void
.end method

.method public setMiuiMultiWindowUtilsScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .line 420
    sput p1, Landroid/util/MiuiMultiWindowUtils;->sScale:F

    .line 421
    return-void
.end method

.method public setRotationFromDisplayApp(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 298
    .local p1, "rotationFromDisplayApp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setRotationFromDisplayApp(Ljava/util/List;)V

    .line 299
    return-void
.end method

.method public setSafeActivityOptions(Lcom/android/server/wm/Task;)Lcom/android/server/wm/SafeActivityOptions;
    .locals 6
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 766
    if-eqz p1, :cond_5

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 769
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isSupportFreeFormInDesktopMode(Lcom/android/server/wm/Task;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 770
    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object v0

    .line 771
    .local v0, "options":Landroid/app/ActivityOptions;
    invoke-virtual {v0, v1}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V

    .line 772
    new-instance v1, Lcom/android/server/wm/SafeActivityOptions;

    invoke-direct {v1, v0}, Lcom/android/server/wm/SafeActivityOptions;-><init>(Landroid/app/ActivityOptions;)V

    return-object v1

    .line 774
    .end local v0    # "options":Landroid/app/ActivityOptions;
    :cond_1
    iget-object v0, p1, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/server/wm/Task;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 775
    :cond_2
    iget-object v0, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 776
    :cond_3
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 777
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "unknown"

    :goto_0
    nop

    .line 778
    .local v0, "packageName":Ljava/lang/String;
    new-instance v2, Lcom/android/server/wm/SafeActivityOptions;

    iget-object v3, p1, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 779
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z

    move-result v4

    .line 778
    const/4 v5, 0x0

    invoke-static {v3, v0, v1, v5, v4}, Landroid/util/MiuiMultiWindowUtils;->getActivityOptions(Landroid/content/Context;Ljava/lang/String;ZZZ)Landroid/app/ActivityOptions;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/android/server/wm/SafeActivityOptions;-><init>(Landroid/app/ActivityOptions;)V

    return-object v2

    .line 767
    .end local v0    # "packageName":Ljava/lang/String;
    :cond_5
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public setSensorDisableList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 338
    .local p1, "sensorDisableList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setSensorDisableList(Ljava/util/List;)V

    .line 339
    return-void
.end method

.method public setSensorDisableWhiteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 314
    .local p1, "sensorDisableWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setSensorDisableWhiteList(Ljava/util/List;)V

    .line 315
    return-void
.end method

.method public setShowHiddenTaskIfFinishedWhiteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 195
    .local p1, "showHiddenTaskIfFinishedWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setShowHiddenTaskIfFinishedWhiteList(Ljava/util/List;)V

    .line 196
    return-void
.end method

.method public setStartFromFreeformBlackList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 179
    .local p1, "startFromFreeformBlackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setStartFromFreeformBlackList(Ljava/util/List;)V

    .line 180
    return-void
.end method

.method public setSupportCvwLevelFullList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 115
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setSupportCvwLevelFullList(Ljava/util/List;)V

    .line 116
    return-void
.end method

.method public setSupportCvwLevelHorizontalList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 131
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setSupportCvwLevelHorizontalList(Ljava/util/List;)V

    .line 132
    return-void
.end method

.method public setSupportCvwLevelVerticalList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 123
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setSupportCvwLevelVerticalList(Ljava/util/List;)V

    .line 124
    return-void
.end method

.method public setTopGameList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 246
    .local p1, "topGameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setTopGameList(Ljava/util/List;)V

    .line 247
    return-void
.end method

.method public setTopVideoList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 254
    .local p1, "topVideoList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setTopVideoList(Ljava/util/List;)V

    .line 255
    return-void
.end method

.method public setUnsupportCvwLevelFullList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 139
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setUnsupportCvwLevelFullList(Ljava/util/List;)V

    .line 140
    return-void
.end method

.method public setUseDefaultCameraPipelineApp(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 306
    .local p1, "useDefaultCameraPipelineApp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->setUseDefaultCameraPipelineApp(Ljava/util/List;)V

    .line 307
    return-void
.end method

.method public shouldSkipRelaunchForDkt(Ljava/lang/String;)Z
    .locals 1
    .param p1, "activityName"    # Ljava/lang/String;

    .line 530
    invoke-static {p1}, Landroid/util/MiuiMultiWindowAdapter;->shouldSkipRelaunchForDkt(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public supportsFreeform()Z
    .locals 1

    .line 368
    invoke-static {}, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->supportsFreeform()Z

    move-result v0

    return v0
.end method

.method public updateApplicationConfiguration(Lcom/android/server/wm/ActivityTaskSupervisor;Landroid/content/res/Configuration;Ljava/lang/String;)V
    .locals 0
    .param p1, "stackSupervisor"    # Lcom/android/server/wm/ActivityTaskSupervisor;
    .param p2, "globalConfiguration"    # Landroid/content/res/Configuration;
    .param p3, "packageName"    # Ljava/lang/String;

    .line 49
    invoke-static {p1, p2, p3}, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->updateApplicationConfiguration(Lcom/android/server/wm/ActivityTaskSupervisor;Landroid/content/res/Configuration;Ljava/lang/String;)V

    .line 51
    return-void
.end method
