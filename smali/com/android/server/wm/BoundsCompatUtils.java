public class com.android.server.wm.BoundsCompatUtils {
	 /* .source "BoundsCompatUtils.java" */
	 /* # static fields */
	 private static final java.lang.Object M_LOCK;
	 private static volatile com.android.server.wm.BoundsCompatUtils sSingleInstance;
	 /* # direct methods */
	 static com.android.server.wm.BoundsCompatUtils ( ) {
		 /* .locals 1 */
		 /* .line 28 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 29 */
		 /* new-instance v0, Ljava/lang/Object; */
		 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public com.android.server.wm.BoundsCompatUtils ( ) {
		 /* .locals 0 */
		 /* .line 27 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Integer getCompatGravity ( android.app.WindowConfiguration p0, Integer p1 ) {
		 /* .locals 2 */
		 /* .param p0, "windowConfiguration" # Landroid/app/WindowConfiguration; */
		 /* .param p1, "mGravity" # I */
		 /* .line 139 */
		 v0 = 		 android.util.MiuiAppSizeCompatModeStub .get ( );
		 if ( v0 != null) { // if-eqz v0, :cond_3
			 /* .line 140 */
			 v0 = 			 (( android.app.WindowConfiguration ) p0 ).getDisplayRotation ( ); // invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I
			 /* if-nez v0, :cond_0 */
			 /* .line 141 */
			 int p1 = 5; // const/4 p1, 0x5
			 /* .line 142 */
		 } // :cond_0
		 v0 = 		 (( android.app.WindowConfiguration ) p0 ).getDisplayRotation ( ); // invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I
		 int v1 = 2; // const/4 v1, 0x2
		 /* if-ne v0, v1, :cond_1 */
		 /* .line 143 */
		 int p1 = 3; // const/4 p1, 0x3
		 /* .line 144 */
	 } // :cond_1
	 v0 = 	 (( android.app.WindowConfiguration ) p0 ).getDisplayRotation ( ); // invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-ne v0, v1, :cond_2 */
	 /* .line 145 */
	 /* const/16 p1, 0x30 */
	 /* .line 146 */
} // :cond_2
v0 = (( android.app.WindowConfiguration ) p0 ).getDisplayRotation ( ); // invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I
/* if-ne v0, v1, :cond_3 */
/* .line 147 */
/* const/16 p1, 0x50 */
/* .line 150 */
} // :cond_3
} // :goto_0
} // .end method
public static com.android.server.wm.BoundsCompatUtils getInstance ( ) {
/* .locals 2 */
/* .line 32 */
v0 = com.android.server.wm.BoundsCompatUtils.sSingleInstance;
/* if-nez v0, :cond_1 */
/* .line 33 */
v0 = com.android.server.wm.BoundsCompatUtils.M_LOCK;
/* monitor-enter v0 */
/* .line 34 */
try { // :try_start_0
v1 = com.android.server.wm.BoundsCompatUtils.sSingleInstance;
/* if-nez v1, :cond_0 */
/* .line 35 */
/* new-instance v1, Lcom/android/server/wm/BoundsCompatUtils; */
/* invoke-direct {v1}, Lcom/android/server/wm/BoundsCompatUtils;-><init>()V */
/* .line 37 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 39 */
} // :cond_1
} // :goto_0
v0 = com.android.server.wm.BoundsCompatUtils.sSingleInstance;
} // .end method
/* # virtual methods */
public void adaptCompatBounds ( android.content.res.Configuration p0, com.android.server.wm.DisplayContent p1 ) {
/* .locals 10 */
/* .param p1, "config" # Landroid/content/res/Configuration; */
/* .param p2, "dc" # Lcom/android/server/wm/DisplayContent; */
/* .line 82 */
v0 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v0 ).getBounds ( ); // invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* .line 83 */
/* .local v0, "bounds":Landroid/graphics/Rect; */
v1 = (( android.graphics.Rect ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v1, :cond_6 */
/* iget v1, p1, Landroid/content/res/Configuration;->densityDpi:I */
/* if-nez v1, :cond_0 */
/* goto/16 :goto_5 */
/* .line 87 */
} // :cond_0
/* iget v1, p1, Landroid/content/res/Configuration;->densityDpi:I */
/* int-to-float v1, v1 */
/* const/high16 v2, 0x43200000 # 160.0f */
/* div-float/2addr v1, v2 */
/* .line 88 */
/* .local v1, "density":F */
v2 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
/* .line 89 */
/* .local v2, "width":I */
v3 = (( android.graphics.Rect ) v0 ).height ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
/* .line 91 */
/* .local v3, "height":I */
/* int-to-float v4, v2 */
/* div-float/2addr v4, v1 */
/* float-to-double v4, v4 */
/* const-wide/high16 v6, 0x3fe0000000000000L # 0.5 */
/* add-double/2addr v4, v6 */
/* double-to-int v4, v4 */
/* iput v4, p1, Landroid/content/res/Configuration;->compatScreenWidthDp:I */
/* iput v4, p1, Landroid/content/res/Configuration;->screenWidthDp:I */
/* .line 93 */
int v4 = 1; // const/4 v4, 0x1
/* if-gt v2, v3, :cond_1 */
/* .line 94 */
/* move v5, v4 */
} // :cond_1
int v5 = 2; // const/4 v5, 0x2
} // :goto_0
/* iput v5, p1, Landroid/content/res/Configuration;->orientation:I */
/* .line 95 */
v5 = this.windowConfiguration;
int v8 = 0; // const/4 v8, 0x0
/* if-gt v2, v3, :cond_2 */
/* .line 96 */
/* move v9, v8 */
} // :cond_2
v9 = this.windowConfiguration;
v9 = (( android.app.WindowConfiguration ) v9 ).getRotation ( ); // invoke-virtual {v9}, Landroid/app/WindowConfiguration;->getRotation()I
/* .line 95 */
} // :goto_1
(( android.app.WindowConfiguration ) v5 ).setRotation ( v9 ); // invoke-virtual {v5, v9}, Landroid/app/WindowConfiguration;->setRotation(I)V
/* .line 99 */
/* iget v5, p1, Landroid/content/res/Configuration;->orientation:I */
/* if-ne v5, v4, :cond_3 */
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 101 */
(( com.android.server.wm.DisplayContent ) p2 ).getDisplayPolicy ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;
(( com.android.server.wm.DisplayPolicy ) v4 ).getDecorInsetsInfo ( v8, v2, v3 ); // invoke-virtual {v4, v8, v2, v3}, Lcom/android/server/wm/DisplayPolicy;->getDecorInsetsInfo(III)Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info;
/* .line 103 */
/* .local v4, "info":Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info; */
v5 = this.mConfigFrame;
v5 = (( android.graphics.Rect ) v5 ).height ( ); // invoke-virtual {v5}, Landroid/graphics/Rect;->height()I
/* int-to-float v5, v5 */
/* div-float/2addr v5, v1 */
/* float-to-double v8, v5 */
/* add-double/2addr v8, v6 */
/* double-to-int v5, v8 */
/* iput v5, p1, Landroid/content/res/Configuration;->screenHeightDp:I */
/* .line 104 */
} // .end local v4 # "info":Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info;
/* .line 105 */
} // :cond_3
/* int-to-float v4, v3 */
/* div-float/2addr v4, v1 */
/* float-to-int v4, v4 */
/* iput v4, p1, Landroid/content/res/Configuration;->compatScreenHeightDp:I */
/* iput v4, p1, Landroid/content/res/Configuration;->screenHeightDp:I */
/* .line 108 */
} // :goto_2
/* if-gt v2, v3, :cond_4 */
/* iget v4, p1, Landroid/content/res/Configuration;->screenWidthDp:I */
} // :cond_4
/* iget v4, p1, Landroid/content/res/Configuration;->screenHeightDp:I */
/* .line 109 */
/* .local v4, "shortSizeDp":I */
} // :goto_3
/* if-lt v2, v3, :cond_5 */
/* iget v5, p1, Landroid/content/res/Configuration;->screenWidthDp:I */
} // :cond_5
/* iget v5, p1, Landroid/content/res/Configuration;->screenHeightDp:I */
/* .line 111 */
/* .local v5, "longSizeDp":I */
} // :goto_4
/* iput v4, p1, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I */
/* iput v4, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I */
/* .line 112 */
/* iget v6, p1, Landroid/content/res/Configuration;->screenLayout:I */
v6 = android.content.res.Configuration .resetScreenLayout ( v6 );
/* .line 113 */
/* .local v6, "sl":I */
v7 = android.content.res.Configuration .reduceScreenLayout ( v6,v5,v4 );
/* iput v7, p1, Landroid/content/res/Configuration;->screenLayout:I */
/* .line 114 */
return;
/* .line 84 */
} // .end local v1 # "density":F
} // .end local v2 # "width":I
} // .end local v3 # "height":I
} // .end local v4 # "shortSizeDp":I
} // .end local v5 # "longSizeDp":I
} // .end local v6 # "sl":I
} // :cond_6
} // :goto_5
return;
} // .end method
public android.content.res.Configuration getCompatConfiguration ( android.content.res.Configuration p0, Float p1, com.android.server.wm.DisplayContent p2, Float p3 ) {
/* .locals 10 */
/* .param p1, "globalConfig" # Landroid/content/res/Configuration; */
/* .param p2, "aspectRatio" # F */
/* .param p3, "dp" # Lcom/android/server/wm/DisplayContent; */
/* .param p4, "scale" # F */
/* .line 52 */
/* new-instance v0, Landroid/content/res/Configuration; */
/* invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V */
/* .line 53 */
/* .local v0, "compatConfig":Landroid/content/res/Configuration; */
int v1 = 0; // const/4 v1, 0x0
/* cmpg-float v1, p2, v1 */
/* if-gez v1, :cond_0 */
/* .line 54 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "aspectRatio ="; // const-string v2, "aspectRatio ="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ",skip"; // const-string v2, ",skip"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BoundsCompatUtils"; // const-string v2, "BoundsCompatUtils"
android.util.Slog .w ( v2,v1 );
/* .line 55 */
/* .line 58 */
} // :cond_0
v1 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v1 ).getBounds ( ); // invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* .line 59 */
/* .local v1, "globalBounds":Landroid/graphics/Rect; */
v2 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v2 ).getAppBounds ( ); // invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getAppBounds()Landroid/graphics/Rect;
/* .line 61 */
/* .local v2, "globalAppBounds":Landroid/graphics/Rect; */
/* new-instance v5, Landroid/graphics/Point; */
v3 = (( android.graphics.Rect ) v1 ).width ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->width()I
v4 = (( android.graphics.Rect ) v1 ).height ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->height()I
/* invoke-direct {v5, v3, v4}, Landroid/graphics/Point;-><init>(II)V */
/* .line 63 */
/* .local v5, "displaySize":Landroid/graphics/Point; */
android.app.servertransaction.BoundsCompat .getInstance ( );
v4 = this.windowConfiguration;
/* .line 64 */
/* const/16 v6, 0x11 */
v6 = com.android.server.wm.BoundsCompatUtils .getCompatGravity ( v4,v6 );
/* iget v7, p1, Landroid/content/res/Configuration;->orientation:I */
int v8 = 0; // const/4 v8, 0x0
/* .line 63 */
/* move v4, p2 */
/* move v9, p4 */
/* invoke-virtual/range {v3 ..v9}, Landroid/app/servertransaction/BoundsCompat;->computeCompatBounds(FLandroid/graphics/Point;IIIF)Landroid/graphics/Rect; */
/* .line 67 */
/* .local v3, "compatBounds":Landroid/graphics/Rect; */
v4 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v4 ).setBounds ( v3 ); // invoke-virtual {v4, v3}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V
/* .line 68 */
v4 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v4 ).setAppBounds ( v3 ); // invoke-virtual {v4, v3}, Landroid/app/WindowConfiguration;->setAppBounds(Landroid/graphics/Rect;)V
/* .line 69 */
v4 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v4 ).setMaxBounds ( v3 ); // invoke-virtual {v4, v3}, Landroid/app/WindowConfiguration;->setMaxBounds(Landroid/graphics/Rect;)V
/* .line 70 */
(( com.android.server.wm.BoundsCompatUtils ) p0 ).adaptCompatBounds ( v0, p3 ); // invoke-virtual {p0, v0, p3}, Lcom/android/server/wm/BoundsCompatUtils;->adaptCompatBounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayContent;)V
/* .line 72 */
} // .end method
public Integer getFlipCompatModeByActivity ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 7 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 172 */
int v0 = 0; // const/4 v0, 0x0
/* .line 173 */
/* .local v0, "activityInfo":Landroid/content/pm/ActivityInfo; */
final String v1 = "miui.supportFlipFullScreen"; // const-string v1, "miui.supportFlipFullScreen"
/* .line 175 */
/* .local v1, "metaDataKey":Ljava/lang/String; */
try { // :try_start_0
v2 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;
v3 = this.mActivityComponent;
/* .line 176 */
v4 = android.os.UserHandle .myUserId ( );
/* const-wide/16 v5, 0x80 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v2 */
/* .line 179 */
/* nop */
/* .line 181 */
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = this.metaData;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.metaData;
/* .line 182 */
v2 = (( android.os.Bundle ) v2 ).containsKey ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 183 */
v2 = this.metaData;
v2 = (( android.os.Bundle ) v2 ).getInt ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 185 */
} // :cond_0
int v2 = -1; // const/4 v2, -0x1
/* .line 177 */
/* :catch_0 */
/* move-exception v2 */
/* .line 178 */
/* .local v2, "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/RuntimeException; */
/* invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
/* throw v3 */
} // .end method
public Integer getFlipCompatModeByApp ( com.android.server.wm.ActivityTaskManagerService p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 154 */
int v0 = 0; // const/4 v0, 0x0
/* .line 155 */
/* .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
final String v1 = "miui.supportFlipFullScreen"; // const-string v1, "miui.supportFlipFullScreen"
/* .line 157 */
/* .local v1, "metaDataKey":Ljava/lang/String; */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;
/* .line 158 */
v3 = android.os.UserHandle .myUserId ( );
/* const-wide/16 v4, 0x80 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v2 */
/* .line 161 */
/* nop */
/* .line 163 */
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = this.metaData;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.metaData;
/* .line 164 */
v2 = (( android.os.Bundle ) v2 ).containsKey ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 165 */
v2 = this.metaData;
v2 = (( android.os.Bundle ) v2 ).getInt ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 167 */
} // :cond_0
int v2 = -1; // const/4 v2, -0x1
/* .line 159 */
/* :catch_0 */
/* move-exception v2 */
/* .line 160 */
/* .local v2, "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/RuntimeException; */
/* invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
/* throw v3 */
} // .end method
public Float getGlobalScaleByName ( java.lang.String p0, Integer p1, android.graphics.Rect p2 ) {
/* .locals 6 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "flipCompatMode" # I */
/* .param p3, "bounds" # Landroid/graphics/Rect; */
/* .line 117 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* .line 118 */
/* .local v0, "scale":F */
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
/* .line 119 */
v1 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v1 ).getScaleMode ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getScaleMode(Ljava/lang/String;)I
/* .line 120 */
/* .local v1, "scaleMode":I */
int v2 = 3; // const/4 v2, 0x3
/* if-eq v1, v2, :cond_3 */
v2 = (( android.graphics.Rect ) p3 ).isEmpty ( ); // invoke-virtual {p3}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v2, :cond_3 */
/* .line 121 */
v2 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 122 */
int v2 = 1; // const/4 v2, 0x1
/* if-ne p2, v2, :cond_0 */
/* .line 123 */
/* .line 124 */
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
/* if-eq p2, v2, :cond_1 */
int v2 = -1; // const/4 v2, -0x1
/* if-ne p2, v2, :cond_3 */
/* .line 126 */
} // :cond_1
/* .line 129 */
} // :cond_2
v2 = (( android.graphics.Rect ) p3 ).width ( ); // invoke-virtual {p3}, Landroid/graphics/Rect;->width()I
v3 = (( android.graphics.Rect ) p3 ).height ( ); // invoke-virtual {p3}, Landroid/graphics/Rect;->height()I
v2 = java.lang.Math .max ( v2,v3 );
/* .line 130 */
/* .local v2, "longSide":I */
v3 = (( android.graphics.Rect ) p3 ).width ( ); // invoke-virtual {p3}, Landroid/graphics/Rect;->width()I
v4 = (( android.graphics.Rect ) p3 ).height ( ); // invoke-virtual {p3}, Landroid/graphics/Rect;->height()I
v3 = java.lang.Math .min ( v3,v4 );
/* .line 131 */
/* .local v3, "shortSide":I */
/* int-to-float v4, v3 */
/* const/high16 v5, 0x3f800000 # 1.0f */
/* mul-float/2addr v4, v5 */
/* int-to-float v5, v2 */
/* div-float v0, v4, v5 */
/* .line 135 */
} // .end local v2 # "longSide":I
} // .end local v3 # "shortSide":I
} // :cond_3
} // :goto_0
} // .end method
