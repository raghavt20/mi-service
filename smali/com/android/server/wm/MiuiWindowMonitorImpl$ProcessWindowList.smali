.class Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
.super Ljava/lang/Object;
.source "MiuiWindowMonitorImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiWindowMonitorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProcessWindowList"
.end annotation


# static fields
.field private static final KEY_COUNT:Ljava/lang/String; = "count"

.field private static final KEY_CURRENT_COUNT:Ljava/lang/String; = "curCount"

.field private static final KEY_KILLED:Ljava/lang/String; = "killed"

.field private static final KEY_LEAK_TYPE:Ljava/lang/String; = "leakType"

.field private static final KEY_MAXCOUNT:Ljava/lang/String; = "maxCount"

.field private static final KEY_PACKAGE:Ljava/lang/String; = "package"

.field private static final KEY_PID:Ljava/lang/String; = "pid"

.field private static final KEY_RESOURCE_TYPE:Ljava/lang/String; = "resourceType"

.field private static final KEY_TYPE:Ljava/lang/String; = "type"

.field private static final KEY_TYPES:Ljava/lang/String; = "types"

.field private static final KEY_WARNED:Ljava/lang/String; = "warned"

.field private static final KEY_WINDOWS:Ljava/lang/String; = "windows"

.field private static final LEAK_TYPES:[Ljava/lang/String;

.field private static final RESOURCE_TYPE:Ljava/lang/String; = "window"


# instance fields
.field private mCurCount:I

.field private mLastReportCount:I

.field private mLastWarnCount:I

.field private mLeakType:I

.field private mMaxCount:I

.field private mPackageName:Ljava/lang/String;

.field private mPid:I

.field private mWarned:Z

.field private mWillKill:Z

.field private mWindowsByType:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 721
    const-string v0, "application"

    const-string/jumbo v1, "system"

    const-string v2, "none"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->LEAK_TYPES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 723
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    .line 743
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLeakType:I

    .line 746
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWarned:Z

    .line 749
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWillKill:Z

    .line 752
    iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I

    .line 753
    iput-object p2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPackageName:Ljava/lang/String;

    .line 754
    return-void
.end method


# virtual methods
.method getCurrentCount()I
    .locals 1

    .line 844
    iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I

    return v0
.end method

.method getLastReportCount()I
    .locals 1

    .line 856
    iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLastReportCount:I

    return v0
.end method

.method getLastWarnCount()I
    .locals 1

    .line 864
    iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLastWarnCount:I

    return v0
.end method

.method getLeakType()I
    .locals 1

    .line 876
    iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLeakType:I

    return v0
.end method

.method getMaxCount()I
    .locals 1

    .line 840
    iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I

    return v0
.end method

.method getPackageName()Ljava/lang/String;
    .locals 1

    .line 852
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method getPid()I
    .locals 1

    .line 848
    iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I

    return v0
.end method

.method getTopTypeWindows()Ljava/lang/String;
    .locals 10

    .line 896
    const/4 v0, 0x0

    .line 897
    .local v0, "top":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    const/4 v1, -0x1

    .line 898
    .local v1, "topCount":I
    const/4 v2, -0x1

    .line 900
    .local v2, "topType":I
    const/4 v3, 0x0

    .local v3, "i":I
    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    .local v4, "size":I
    :goto_0
    if-ge v3, v4, :cond_4

    .line 901
    iget-object v5, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    .line 902
    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    .line 903
    .local v5, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v6

    if-gtz v6, :cond_0

    .line 904
    goto :goto_2

    .line 906
    :cond_0
    const/4 v6, 0x0

    .line 907
    .local v6, "typeCount":I
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    .line 908
    .local v8, "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v9

    if-lez v9, :cond_1

    .line 909
    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v9

    add-int/2addr v6, v9

    .line 911
    .end local v8    # "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    :cond_1
    goto :goto_1

    .line 912
    :cond_2
    if-le v6, v1, :cond_3

    .line 913
    move v1, v6

    .line 914
    move-object v0, v5

    .line 915
    iget-object v7, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    invoke-virtual {v7, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 900
    .end local v5    # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    .end local v6    # "typeCount":I
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 919
    .end local v3    # "i":I
    .end local v4    # "size":I
    :cond_4
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v3

    if-gtz v3, :cond_5

    goto :goto_4

    .line 923
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 924
    .local v3, "result":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 925
    .local v5, "name":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 926
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "|"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 928
    .end local v5    # "name":Ljava/lang/String;
    :cond_6
    goto :goto_3

    .line 929
    :cond_7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 920
    .end local v3    # "result":Ljava/lang/StringBuilder;
    :cond_8
    :goto_4
    const-string v3, "no window"

    return-object v3
.end method

.method getWindows()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray<",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;",
            ">;>;>;"
        }
    .end annotation

    .line 892
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    return-object v0
.end method

.method put(Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;)V
    .locals 4
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "info"    # Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;

    .line 933
    iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I

    iget v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPid:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPackageName:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPackageName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 940
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    iget v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I

    .line 941
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 942
    .local v0, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    if-nez v0, :cond_1

    .line 943
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move-object v0, v1

    .line 944
    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    iget v2, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 946
    :cond_1
    iget-object v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mWindowName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 947
    .local v1, "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    if-nez v1, :cond_2

    .line 948
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    move-object v1, v2

    .line 949
    iget-object v2, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mWindowName:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 951
    :cond_2
    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 952
    iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I

    .line 955
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v2

    .line 956
    .local v2, "count":I
    iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I

    if-le v2, v3, :cond_3

    .line 957
    iput v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I

    .line 959
    :cond_3
    return-void

    .line 934
    .end local v0    # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    .end local v1    # "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    .end local v2    # "count":I
    :cond_4
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pid not match, mPid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", info.pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPackage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", info.package="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", info.window="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mWindowName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", info.type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiWindowMonitorImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    return-void
.end method

.method remove(ILjava/lang/String;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "token"    # Landroid/os/IBinder;

    .line 962
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 963
    .local v0, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    if-nez v0, :cond_0

    .line 964
    return-void

    .line 966
    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 967
    .local v1, "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    if-nez v1, :cond_1

    .line 968
    return-void

    .line 970
    :cond_1
    invoke-virtual {v1, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 971
    iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I

    .line 972
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 973
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 975
    :cond_2
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 976
    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 978
    :cond_3
    return-void
.end method

.method setLastReportCount(I)V
    .locals 0
    .param p1, "lastReportCount"    # I

    .line 860
    iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLastReportCount:I

    .line 861
    return-void
.end method

.method setLastWarnCount(I)V
    .locals 0
    .param p1, "lastWarnCount"    # I

    .line 868
    iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLastWarnCount:I

    .line 869
    return-void
.end method

.method setLeakType(I)V
    .locals 0
    .param p1, "leakType"    # I

    .line 872
    iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLeakType:I

    .line 873
    return-void
.end method

.method setWarned(Z)V
    .locals 0
    .param p1, "warned"    # Z

    .line 880
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWarned:Z

    .line 881
    return-void
.end method

.method setWillKill(Z)V
    .locals 0
    .param p1, "willKill"    # Z

    .line 888
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWillKill:Z

    .line 889
    return-void
.end method

.method toJson()Lorg/json/JSONObject;
    .locals 15

    .line 788
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 789
    .local v0, "jobj":Lorg/json/JSONObject;
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 791
    .local v1, "jarray":Lorg/json/JSONArray;
    :try_start_0
    const-string v2, "resourceType"

    const-string/jumbo v3, "window"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 792
    const-string v2, "pid"

    iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 793
    const-string v2, "package"

    iget-object v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 794
    const-string v2, "maxCount"

    iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 795
    const-string v2, "leakType"

    sget-object v3, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->LEAK_TYPES:[Ljava/lang/String;

    iget v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLeakType:I

    aget-object v3, v3, v4

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 796
    const-string/jumbo v2, "warned"

    iget-boolean v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWarned:Z

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 797
    const-string v2, "killed"

    iget-boolean v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWillKill:Z

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 799
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    .local v3, "size":I
    :goto_0
    if-ge v2, v3, :cond_4

    .line 800
    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    .line 801
    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    .line 802
    .local v4, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    iget-object v5, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 803
    .local v5, "windowType":I
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 804
    const/4 v6, 0x0

    .line 805
    .local v6, "typeCount":I
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 806
    .local v7, "typeObj":Lorg/json/JSONObject;
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 808
    .local v8, "nameObj":Lorg/json/JSONObject;
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 809
    .local v10, "windows":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 810
    .local v11, "name":Ljava/lang/String;
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/HashMap;

    .line 811
    .local v12, "value":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    const/4 v13, 0x0

    .line 812
    .local v13, "nameCount":I
    if-eqz v12, :cond_0

    .line 813
    invoke-virtual {v12}, Ljava/util/HashMap;->size()I

    move-result v14

    move v13, v14

    .line 815
    :cond_0
    if-lez v13, :cond_1

    .line 816
    add-int/2addr v6, v13

    .line 817
    invoke-virtual {v8, v11, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 819
    .end local v10    # "windows":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    .end local v11    # "name":Ljava/lang/String;
    .end local v12    # "value":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    .end local v13    # "nameCount":I
    :cond_1
    goto :goto_1

    .line 820
    :cond_2
    if-lez v6, :cond_3

    .line 821
    const-string/jumbo v9, "type"

    invoke-virtual {v7, v9, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 822
    const-string v9, "count"

    invoke-virtual {v7, v9, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 823
    const-string/jumbo v9, "windows"

    invoke-virtual {v7, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 824
    invoke-virtual {v1, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 799
    .end local v4    # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    .end local v5    # "windowType":I
    .end local v6    # "typeCount":I
    .end local v7    # "typeObj":Lorg/json/JSONObject;
    .end local v8    # "nameObj":Lorg/json/JSONObject;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 828
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_4
    const-string v2, "curCount"

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 829
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 830
    const-string/jumbo v2, "types"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 835
    :cond_5
    nop

    .line 836
    return-object v0

    .line 832
    :catch_0
    move-exception v2

    .line 833
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 834
    const/4 v3, 0x0

    return-object v3
.end method

.method public toString()Ljava/lang/String;
    .locals 14

    .line 758
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pid="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 759
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "package="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 760
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    .local v4, "size":I
    :goto_0
    if-ge v1, v4, :cond_3

    .line 761
    iget-object v5, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    .line 762
    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    .line 763
    .local v5, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    iget-object v6, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWindowsByType:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 764
    .local v6, "windowType":I
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 765
    const-string v7, "    Type:"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",count="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 766
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 768
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 769
    .local v9, "windows":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 770
    .local v10, "name":Ljava/lang/String;
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashMap;

    .line 771
    .local v11, "value":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    const/4 v12, 0x0

    .line 772
    .local v12, "nameCount":I
    if-eqz v11, :cond_0

    .line 773
    invoke-virtual {v11}, Ljava/util/HashMap;->size()I

    move-result v12

    .line 775
    :cond_0
    if-lez v12, :cond_1

    .line 776
    const-string v13, "      Windows:"

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 777
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 779
    .end local v9    # "windows":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    .end local v10    # "name":Ljava/lang/String;
    .end local v11    # "value":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
    .end local v12    # "nameCount":I
    :cond_1
    goto :goto_1

    .line 760
    .end local v5    # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
    .end local v6    # "windowType":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 782
    .end local v1    # "i":I
    .end local v4    # "size":I
    :cond_3
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "currentCount="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 783
    const-string v2, "maxCount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 784
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method warned()Z
    .locals 1

    .line 884
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWarned:Z

    return v0
.end method
