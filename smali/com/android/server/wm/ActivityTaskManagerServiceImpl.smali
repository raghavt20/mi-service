.class public Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
.super Lcom/android/server/wm/ActivityTaskManagerServiceStub;
.source "ActivityTaskManagerServiceImpl.java"


# static fields
.field private static final BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final CONTINUITY_NOTIFICATION_ID:I = 0x2706

.field private static final EXPAND_DOCK:Ljava/lang/String; = "expand_dock"

.field private static final EXPAND_OTHER:Ljava/lang/String; = "expand_other"

.field private static final FAST_OUT_SLOW_IN_REVERSE:Landroid/view/animation/Interpolator;

.field public static final FLIP_HOME_CLASS:Ljava/lang/String; = "com.miui.fliphome.FlipLauncher"

.field public static final FLIP_HOME_PACKAGE:Ljava/lang/String; = "com.miui.fliphome"

.field public static final FLIP_HOME_SHORT_COMPONENT:Ljava/lang/String; = "com.miui.fliphome/.FlipLauncher"

.field private static final GESTURE_LEFT_TO_RIGHT:Ljava/lang/String; = "gesture_left_to_right"

.field private static final GESTURE_RIGHT_TO_LEFT:Ljava/lang/String; = "gesture_right_to_left"

.field public static final KEY_MULTI_FINGER_SLIDE:Ljava/lang/String; = "multi_finger_slide"

.field private static final MANAGED_PROFILE_NOT_SNAPSHOT_ACTIVITY:Ljava/lang/String; = "com.android.cts.verifier/.managedprovisioning.RecentsRedactionActivity"

.field private static final MIUI_THEMEMANAGER_PKG:Ljava/lang/String; = "com.android.thememanager"

.field private static final PACKAGE_FORE_BUFFER_SIZE:I

.field private static final PACKAGE_NAME_CAMERA:Ljava/lang/String; = "com.android.camera"

.field private static final PRESS_META_KEY_AND_W:Ljava/lang/String; = "press_meta_key_and_w"

.field private static final SNAP_TO_LEFT:Ljava/lang/String; = "snap_to_left"

.field private static final SNAP_TO_RIGHT:Ljava/lang/String; = "snap_to_right"

.field private static final SPLIT_SCREEN_BLACK_LIST:Ljava/lang/String; = "miui_resize_black_list"

.field private static final SPLIT_SCREEN_BLACK_LIST_FOR_FOLD:Ljava/lang/String; = "miui_resize_black_list_for_fold"

.field private static final SPLIT_SCREEN_BLACK_LIST_FOR_PAD:Ljava/lang/String; = "miui_resize_black_list_for_pad"

.field private static final SPLIT_SCREEN_MODULE_NAME:Ljava/lang/String; = "split_screen_applist"

.field private static final SPLIT_SCREEN_MULTI_TASK_WHITE_LIST:Ljava/lang/String; = "miui_multi_task_white_list"

.field private static final SUBSCREEN_MAIN_ACTIVITY:Ljava/lang/String; = "com.xiaomi.misubscreenui.SubScreenMainActivity"

.field private static final SUBSCREEN_PKG:Ljava/lang/String; = "com.xiaomi.misubscreenui"

.field private static final SUPPORT_MULTIPLE_TASK:Z = true

.field private static final TAG:Ljava/lang/String; = "ATMSImpl"

.field private static final THREE_GESTURE_DOCK_TASK:Ljava/lang/String; = "three_gesture_dock_task"

.field private static final UPDATE_SPLIT_SNAP_TARGET:Ljava/lang/String; = "update_split_snap_target"

.field private static final URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

.field private static final WIDE_SCREEN_SIZE:I = 0x258

.field private static lastForegroundPkg:Ljava/lang/String;

.field private static lastMultiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

.field private static final mIgnoreUriCheckPkg:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mLastMainDisplayTopTaskId:I

.field private static final sCachedForegroundPackageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/mqsas/sdk/event/PackageForegroundEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static sSystemBootCompleted:Z


# instance fields
.field private final SUPPORT_DECLINE_BACKGROUND_COLOR:Z

.field private isHasActivityControl:Z

.field private mActivityControlUid:I

.field private mAdvanceTaskIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAppCompatTask:Lcom/android/server/wm/AppCompatTask;

.field private mAppContinuityIsUnfocused:Z

.field public mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

.field public mContext:Landroid/content/Context;

.field private mDeclineColorActivity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFoldablePackagePolicy:Lcom/android/server/wm/FoldablePackagePolicy;

.field public mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

.field private volatile mInAnimationOut:Z

.field private mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

.field private mMultipleTaskWhiteList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mPackageConfigurationController:Lcom/android/server/wm/PackageConfigurationController;

.field mPackageHoldOn:Ljava/lang/String;

.field private mPackageManager:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

.field public mPackageSettingsManager:Lcom/android/server/wm/PackageSettingsManager;

.field mProcessManagerIn:Lcom/miui/server/process/ProcessManagerInternal;

.field private mResizeBlackList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRestartingTaskId:I

.field private mScreenshotLayer:Landroid/view/SurfaceControl;

.field private mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

.field private mTargePackageName:Ljava/lang/String;

.field private mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

.field private mTransitionSyncIdList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$IQuC6QqI36bWb7diJfr_ZQodPv4(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lambda$onSystemReady$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$JveBgNg0OlAxn4POHQ0NP48_yZg(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lambda$showScreenShotForSplitTask$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$QLnmHxw2DGVfzrO7m_eXC20MLc0(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/view/SurfaceControl$Transaction;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lambda$animationOut$1(Landroid/view/SurfaceControl$Transaction;Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method public static synthetic $r8$lambda$b6s6wuAF5h0xR_w0u9uVXDg8mx8(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->animationOut()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmScreenshotLayer(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)Landroid/view/SurfaceControl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mScreenshotLayer:Landroid/view/SurfaceControl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmInAnimationOut(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mInAnimationOut:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScreenshotLayer(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/view/SurfaceControl;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mScreenshotLayer:Landroid/view/SurfaceControl;

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartSubScreenUi(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->startSubScreenUi(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 169
    nop

    .line 170
    const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    .line 196
    new-instance v0, Landroid/view/animation/PathInterpolator;

    const v1, 0x3f19999a    # 0.6f

    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0x3f4ccccd    # 0.8f

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4, v1, v2}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    sput-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->FAST_OUT_SLOW_IN_REVERSE:Landroid/view/animation/Interpolator;

    .line 208
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO:Landroid/util/ArraySet;

    .line 358
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mIgnoreUriCheckPkg:Ljava/util/HashSet;

    .line 362
    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 363
    const-string v1, "com.miui.mishare.connectivity"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 364
    const-string v1, "com.miui.securitycore"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 373
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->sCachedForegroundPackageList:Ljava/util/List;

    .line 377
    nop

    .line 378
    const-string/jumbo v0, "sys.proc.fore_pkg_buffer"

    const/16 v1, 0xf

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->PACKAGE_FORE_BUFFER_SIZE:I

    .line 417
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lastForegroundPkg:Ljava/lang/String;

    .line 418
    sput-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lastMultiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 120
    invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;-><init>()V

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAppContinuityIsUnfocused:Z

    .line 175
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mResizeBlackList:Ljava/util/HashSet;

    .line 191
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMultipleTaskWhiteList:Ljava/util/HashSet;

    .line 198
    nop

    .line 199
    const-string/jumbo v1, "support_decline_background_color"

    invoke-static {v1, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->SUPPORT_DECLINE_BACKGROUND_COLOR:Z

    .line 200
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mDeclineColorActivity:Ljava/util/List;

    .line 202
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mTransitionSyncIdList:Ljava/util/HashSet;

    .line 204
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAdvanceTaskIds:Ljava/util/List;

    .line 1680
    iput-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isHasActivityControl:Z

    .line 1681
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mActivityControlUid:I

    return-void
.end method

.method private animationOut()V
    .locals 4

    .line 1426
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mInAnimationOut:Z

    if-eqz v0, :cond_0

    return-void

    .line 1427
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1428
    .local v0, "anim":Landroid/animation/ValueAnimator;
    new-instance v1, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v1}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    .line 1429
    .local v1, "t":Landroid/view/SurfaceControl$Transaction;
    new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/view/SurfaceControl$Transaction;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1436
    new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;

    invoke-direct {v2, p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/view/SurfaceControl$Transaction;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1462
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1463
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 1464
    sget-object v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->FAST_OUT_SLOW_IN_REVERSE:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1465
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1466
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mInAnimationOut:Z

    .line 1467
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private canEnterPipOnTaskSwitch(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/Task;Z)Z
    .locals 3
    .param p1, "top"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "task"    # Lcom/android/server/wm/Task;
    .param p3, "userLeaving"    # Z

    .line 1843
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 1846
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 1847
    .local v1, "rootTask":Lcom/android/server/wm/Task;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->isActivityTypeAssistant()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 1844
    .end local v1    # "rootTask":Lcom/android/server/wm/Task;
    :cond_2
    :goto_0
    return v0
.end method

.method private exitSplitScreen(Z)V
    .locals 3
    .param p1, "isExpandDock"    # Z

    .line 1223
    if-eqz p1, :cond_0

    const-string v0, "expand_dock"

    goto :goto_0

    :cond_0
    const-string v0, "expand_other"

    .line 1224
    .local v0, "str":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getUiContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "press_meta_key_and_w"

    invoke-static {v1, v2, v0}, Landroid/provider/MiuiSettings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1226
    return-void
.end method

.method private exitSplitScreenIfNeed(Lcom/android/server/wm/WindowContainer;)Z
    .locals 5
    .param p1, "wc"    # Lcom/android/server/wm/WindowContainer;

    .line 1214
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isVerticalSplit()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1215
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 1216
    .local v0, "rootTask":Lcom/android/server/wm/Task;
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getTopLeafTask()Lcom/android/server/wm/Task;

    move-result-object v2

    .line 1217
    .local v2, "topLeafTask":Lcom/android/server/wm/Task;
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    const/4 v4, 0x1

    if-nez v3, :cond_1

    move v1, v4

    .line 1218
    .local v1, "isExpandDock":Z
    :cond_1
    invoke-direct {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->exitSplitScreen(Z)V

    .line 1219
    return v4

    .line 1214
    .end local v0    # "rootTask":Lcom/android/server/wm/Task;
    .end local v1    # "isExpandDock":Z
    .end local v2    # "topLeafTask":Lcom/android/server/wm/Task;
    :cond_2
    :goto_0
    return v1
.end method

.method public static getChildComponentNames(Landroid/app/ActivityTaskManager$RootTaskInfo;)[Landroid/content/ComponentName;
    .locals 5
    .param p0, "rootTaskInfo"    # Landroid/app/ActivityTaskManager$RootTaskInfo;

    .line 1285
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/content/ComponentName;

    .line 1286
    .local v0, "componentNames":[Landroid/content/ComponentName;
    if-eqz p0, :cond_0

    .line 1287
    iget-object v1, p0, Landroid/app/ActivityTaskManager$RootTaskInfo;->childTaskNames:[Ljava/lang/String;

    .line 1288
    .local v1, "childTaskNames":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1289
    array-length v2, v1

    .line 1290
    .local v2, "childTaskCount":I
    new-array v0, v2, [Landroid/content/ComponentName;

    .line 1291
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_0

    .line 1292
    aget-object v4, v1, v3

    invoke-static {v4}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    aput-object v4, v0, v3

    .line 1291
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1296
    .end local v1    # "childTaskNames":[Ljava/lang/String;
    .end local v2    # "childTaskCount":I
    .end local v3    # "i":I
    :cond_0
    return-object v0
.end method

.method public static getChildTaskUserIds(Landroid/app/ActivityTaskManager$RootTaskInfo;)[I
    .locals 4
    .param p0, "rootTaskInfo"    # Landroid/app/ActivityTaskManager$RootTaskInfo;

    .line 1299
    const/4 v0, 0x0

    new-array v1, v0, [I

    .line 1300
    .local v1, "childTaskUserIds":[I
    if-eqz p0, :cond_0

    .line 1301
    iget-object v2, p0, Landroid/app/ActivityTaskManager$RootTaskInfo;->childTaskUserIds:[I

    .line 1302
    .local v2, "childUserIds":[I
    if-eqz v2, :cond_0

    .line 1303
    array-length v3, v2

    .line 1304
    .local v3, "childTaskCount":I
    new-array v1, v3, [I

    .line 1305
    invoke-static {v2, v0, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1308
    .end local v2    # "childUserIds":[I
    .end local v3    # "childTaskCount":I
    :cond_0
    return-object v1
.end method

.method public static getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
    .locals 1

    .line 212
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    return-object v0
.end method

.method private getSplitScreenBlackListFromXml()V
    .locals 3

    .line 813
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    const-string v1, "com.android.settings"

    if-eqz v0, :cond_0

    .line 814
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mResizeBlackList:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 815
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mResizeBlackList:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 817
    :cond_0
    const-string v0, "persist.sys.muiltdisplay_type"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 818
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mResizeBlackList:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 819
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mResizeBlackList:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 822
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mResizeBlackList:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 828
    :goto_0
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_2

    .line 829
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMultipleTaskWhiteList:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300a0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 832
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMultipleTaskWhiteList:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1103009f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 836
    :goto_1
    return-void
.end method

.method public static handleFreeformModeRequst(Landroid/os/IBinder;ILandroid/content/Context;)I
    .locals 7
    .param p0, "token"    # Landroid/os/IBinder;
    .param p1, "cmd"    # I
    .param p2, "mContext"    # Landroid/content/Context;

    .line 697
    const/4 v0, -0x1

    .line 698
    .local v0, "result":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 700
    .local v1, "ident":J
    :try_start_0
    invoke-static {p0}, Lcom/android/server/wm/ActivityRecord;->forTokenLocked(Landroid/os/IBinder;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 701
    .local v3, "r":Lcom/android/server/wm/ActivityRecord;
    const-string v4, "gamebox_stick"

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_2

    .line 717
    :pswitch_0
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 720
    .local v4, "component":Ljava/lang/String;
    if-nez v3, :cond_1

    :cond_0
    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x1

    :goto_0
    move v0, v5

    .line 721
    goto :goto_2

    .line 714
    .end local v4    # "component":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, ""

    invoke-static {v5, v4, v6}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 715
    goto :goto_2

    .line 706
    :pswitch_2
    nop

    .line 707
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 711
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v6

    .line 706
    invoke-static {v5, v4, v6}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 712
    goto :goto_2

    .line 703
    :pswitch_3
    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I

    move-result v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    move v0, v5

    .line 704
    nop

    .line 725
    :goto_2
    nop

    .line 727
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 725
    return v0

    .line 727
    .end local v3    # "r":Lcom/android/server/wm/ActivityRecord;
    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 728
    throw v3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static varargs invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "values"    # [Ljava/lang/Object;

    .line 1775
    const/4 v0, 0x0

    .line 1777
    .local v0, "method":Ljava/lang/reflect/Method;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 1778
    .local v2, "clazz":Ljava/lang/Class;
    const/4 v3, 0x1

    if-nez p2, :cond_0

    .line 1779
    invoke-virtual {v2, p1, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    move-object v0, v4

    .line 1780
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1781
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1

    .line 1783
    :cond_0
    array-length v4, p2

    new-array v4, v4, [Ljava/lang/Class;

    .line 1784
    .local v4, "argsClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v6, p2

    if-ge v5, v6, :cond_4

    .line 1785
    aget-object v6, p2, v5

    instance-of v6, v6, Ljava/lang/Integer;

    if-eqz v6, :cond_1

    .line 1786
    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    goto :goto_1

    .line 1787
    :cond_1
    aget-object v6, p2, v5

    instance-of v6, v6, Ljava/lang/Boolean;

    if-eqz v6, :cond_2

    .line 1788
    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    goto :goto_1

    .line 1789
    :cond_2
    aget-object v6, p2, v5

    instance-of v6, v6, Ljava/lang/Float;

    if-eqz v6, :cond_3

    .line 1790
    sget-object v6, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    goto :goto_1

    .line 1792
    :cond_3
    aget-object v6, p2, v5

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1784
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1795
    .end local v5    # "i":I
    :cond_4
    invoke-virtual {v2, p1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    move-object v0, v5

    .line 1796
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1797
    invoke-virtual {v0, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1798
    .end local v2    # "clazz":Ljava/lang/Class;
    .end local v4    # "argsClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :catch_0
    move-exception v2

    .line 1799
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDeclaredMethod:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ATMSImpl"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1801
    .end local v2    # "e":Ljava/lang/Exception;
    return-object v1
.end method

.method private isCTS()Z
    .locals 2

    .line 961
    nop

    .line 962
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 961
    xor-int/lit8 v0, v0, 0x1

    const-string v1, "persist.sys.miui_optimization"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private isSubScreenFeatureOn(Landroid/content/Context;I)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userId"    # I

    .line 966
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 967
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "subscreen_switch"

    invoke-static {v0, v2, v1, p2}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    nop

    .line 966
    :goto_0
    return v1
.end method

.method private static isSystemBootCompleted()Z
    .locals 2

    .line 569
    sget-boolean v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->sSystemBootCompleted:Z

    if-nez v0, :cond_0

    .line 570
    const-string/jumbo v0, "sys.boot_completed"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->sSystemBootCompleted:Z

    .line 572
    :cond_0
    sget-boolean v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->sSystemBootCompleted:Z

    return v0
.end method

.method private synthetic lambda$animationOut$1(Landroid/view/SurfaceControl$Transaction;Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "animation"    # Landroid/animation/ValueAnimator;

    .line 1430
    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1431
    .local v0, "alpha":F
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mScreenshotLayer:Landroid/view/SurfaceControl;

    if-eqz v1, :cond_0

    .line 1432
    invoke-virtual {p1, v1, v0}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 1434
    :cond_0
    return-void
.end method

.method static synthetic lambda$getTopTaskVisibleActivities$3(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p0, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 1719
    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->RESUMED:Lcom/android/server/wm/ActivityRecord$State;

    invoke-virtual {p0, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$getTopTaskVisibleActivities$4(Lcom/android/server/wm/ActivityRecord;)Landroid/content/Intent;
    .locals 2
    .param p0, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 1721
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1722
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1723
    return-object v0
.end method

.method private synthetic lambda$onSystemReady$0()V
    .locals 0

    .line 269
    invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->registerObserver()V

    return-void
.end method

.method private synthetic lambda$showScreenShotForSplitTask$2()V
    .locals 0

    .line 1501
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->removeSplitTaskShotIfNeed()Z

    return-void
.end method

.method public static onForegroundWindowChanged(Lcom/android/server/wm/WindowProcessController;Landroid/content/pm/ActivityInfo;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;)V
    .locals 4
    .param p0, "app"    # Lcom/android/server/wm/WindowProcessController;
    .param p1, "info"    # Landroid/content/pm/ActivityInfo;
    .param p2, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "state"    # Lcom/android/server/wm/ActivityRecord$State;

    .line 529
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 530
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    new-instance v1, Lcom/android/server/wm/FgWindowChangedInfo;

    iget-object v2, p1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 531
    invoke-virtual {p0}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v3

    invoke-direct {v1, p2, v2, v3}, Lcom/android/server/wm/FgWindowChangedInfo;-><init>(Lcom/android/server/wm/ActivityRecord;Landroid/content/pm/ApplicationInfo;I)V

    .line 530
    invoke-virtual {v0, v1}, Lcom/miui/server/process/ProcessManagerInternal;->notifyForegroundWindowChanged(Lcom/android/server/wm/FgWindowChangedInfo;)V

    .line 533
    :cond_0
    return-void
.end method

.method private registerObserver()V
    .locals 5

    .line 840
    invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getSplitScreenBlackListFromXml()V

    .line 842
    new-instance v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$4;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$4;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/os/Handler;)V

    .line 848
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 852
    invoke-virtual {v0, v4}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 853
    return-void
.end method

.method private reportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)V
    .locals 11
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 404
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mProcessManagerIn:Lcom/miui/server/process/ProcessManagerInternal;

    .line 405
    invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getMultiWindowForegroundAppInfoLocked()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 406
    .local v0, "multiWindowAppInfo":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;

    move-result-object v7

    .line 407
    .local v7, "state":Lcom/android/server/wm/ActivityRecord$State;
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v8

    .line 409
    .local v8, "pid":I
    invoke-static {}, Lcom/android/server/MiuiFgThread;->getHandler()Landroid/os/Handler;

    move-result-object v9

    new-instance v10, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;

    move-object v1, v10

    move-object v2, p0

    move-object v3, p1

    move-object v4, v7

    move v5, v8

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;ILandroid/content/pm/ApplicationInfo;)V

    invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 415
    return-void
.end method

.method private static reportPackageForeground(Lcom/android/server/wm/ActivityRecord;ILjava/lang/String;)V
    .locals 4
    .param p0, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p1, "pid"    # I
    .param p2, "lastPkgName"    # Ljava/lang/String;

    .line 536
    new-instance v0, Lmiui/mqsas/sdk/event/PackageForegroundEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;-><init>()V

    .line 537
    .local v0, "event":Lmiui/mqsas/sdk/event/PackageForegroundEvent;
    iget-object v1, p0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setPackageName(Ljava/lang/String;)V

    .line 538
    iget-object v1, p0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setComponentName(Ljava/lang/String;)V

    .line 539
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setIdentity(I)V

    .line 540
    invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setPid(I)V

    .line 541
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setForegroundTime(J)V

    .line 542
    iget-object v1, p0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v1}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->getIsColdStart()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setColdStart(Z)V

    .line 543
    invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setLastPackageName(Ljava/lang/String;)V

    .line 544
    sget-object v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->sCachedForegroundPackageList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    sget v3, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->PACKAGE_FORE_BUFFER_SIZE:I

    if-lt v2, v3, :cond_0

    .line 547
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isSystemBootCompleted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 548
    const-string v2, "ATMSImpl"

    const-string v3, "Begin to report package foreground events..."

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 550
    .local v2, "events":Ljava/util/List;, "Ljava/util/List<Lmiui/mqsas/sdk/event/PackageForegroundEvent;>;"
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 551
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 553
    invoke-static {v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->reportPackageForegroundEvents(Ljava/util/List;)V

    .line 555
    .end local v2    # "events":Ljava/util/List;, "Ljava/util/List<Lmiui/mqsas/sdk/event/PackageForegroundEvent;>;"
    :cond_0
    return-void
.end method

.method private static reportPackageForegroundEvents(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiui/mqsas/sdk/event/PackageForegroundEvent;",
            ">;)V"
        }
    .end annotation

    .line 558
    .local p0, "events":Ljava/util/List;, "Ljava/util/List<Lmiui/mqsas/sdk/event/PackageForegroundEvent;>;"
    new-instance v0, Landroid/content/pm/ParceledListSlice;

    invoke-direct {v0, p0}, Landroid/content/pm/ParceledListSlice;-><init>(Ljava/util/List;)V

    .line 560
    .local v0, "reportEvents":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Lmiui/mqsas/sdk/event/PackageForegroundEvent;>;"
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$3;

    invoke-direct {v2, v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$3;-><init>(Landroid/content/pm/ParceledListSlice;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 566
    return-void
.end method

.method private shouldNotStartSubscreen()Z
    .locals 7

    .line 972
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 973
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 974
    .local v0, "display":Lcom/android/server/wm/DisplayContent;
    const/4 v1, 0x0

    .line 975
    .local v1, "topRunningActivity":Lcom/android/server/wm/ActivityRecord;
    const/4 v2, 0x0

    .line 976
    .local v2, "switchUser":Z
    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_1

    .line 977
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 978
    if-eqz v1, :cond_0

    iget v5, v1, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    iget-object v6, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v6}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I

    move-result v6

    if-eq v5, v6, :cond_0

    move v5, v3

    goto :goto_0

    :cond_0
    move v5, v4

    :goto_0
    move v2, v5

    .line 979
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "shouldNotStartSubscreen topRunningActivity="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", switchUser="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ATMSImpl"

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 981
    :cond_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->isSleeping()Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getPid()I

    move-result v5

    if-lez v5, :cond_2

    if-nez v2, :cond_2

    goto :goto_1

    :cond_2
    move v3, v4

    :cond_3
    :goto_1
    return v3
.end method

.method private skipReportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 4
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 452
    invoke-static {}, Lcom/android/server/wm/PreloadStateManagerStub;->get()Lcom/android/server/wm/PreloadStateManagerStub;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/server/wm/PreloadStateManagerStub;->isPreloadDisplayId(I)Z

    move-result v0

    const/4 v1, 0x1

    const-string v2, "ATMSImpl"

    if-eqz v0, :cond_0

    .line 453
    const-string v0, "do not report preloadApp event"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    return v1

    .line 456
    :cond_0
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_1

    .line 457
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayId()I

    move-result v0

    const/4 v3, 0x2

    if-ne v3, v0, :cond_1

    .line 458
    const-string v0, "do not report subscreen event"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    return v1

    .line 461
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private startSoSc(Lcom/android/server/wm/Task;I)V
    .locals 2
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "position"    # I

    .line 1405
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->showScreenShotForSplitTask()V

    .line 1406
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->setHasBeenVisible(Z)V

    .line 1407
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->sendTaskAppeared()V

    .line 1408
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskOrganizerController:Lcom/android/server/wm/TaskOrganizerController;

    invoke-virtual {v0}, Lcom/android/server/wm/TaskOrganizerController;->dispatchPendingEvents()V

    .line 1409
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v0

    iget v1, p1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v0, v1, p2}, Lcom/android/server/wm/MiuiSoScManagerStub;->startTaskInSoSc(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1412
    goto :goto_0

    .line 1410
    :catch_0
    move-exception v0

    .line 1411
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1413
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private startSubScreenUi(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V
    .locals 9
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "reason"    # Ljava/lang/String;

    .line 931
    const-string/jumbo v0, "starSubScreenActivity: "

    :try_start_0
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    .line 932
    .local v1, "userId":I
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I

    move-result v2

    if-ne v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 933
    invoke-direct {p0, v2, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isSubScreenFeatureOn(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 934
    invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->shouldNotStartSubscreen()Z

    move-result v2

    if-eqz v2, :cond_0

    goto/16 :goto_0

    .line 937
    :cond_0
    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object v2

    .line 938
    .local v2, "options":Landroid/app/ActivityOptions;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V

    .line 939
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;

    .line 940
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 941
    .local v3, "intent":Landroid/content/Intent;
    new-instance v4, Landroid/content/ComponentName;

    const-string v5, "com.xiaomi.misubscreenui"

    const-string v6, "com.xiaomi.misubscreenui.SubScreenMainActivity"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    .local v4, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 943
    const/high16 v5, 0x10000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 944
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v5

    const-wide/16 v6, 0x400

    invoke-interface {v5, v4, v6, v7, v1}, Landroid/content/pm/IPackageManager;->getActivityInfo(Landroid/content/ComponentName;JI)Landroid/content/pm/ActivityInfo;

    move-result-object v5

    .line 947
    .local v5, "aInfo":Landroid/content/pm/ActivityInfo;
    const-string v6, "ATMSImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for user "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    iget-object v6, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v6}, Lcom/android/server/wm/ActivityTaskManagerService;->getActivityStartController()Lcom/android/server/wm/ActivityStartController;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 949
    invoke-virtual {v6, v3, v0}, Lcom/android/server/wm/ActivityStartController;->obtainStarter(Landroid/content/Intent;Ljava/lang/String;)Lcom/android/server/wm/ActivityStarter;

    move-result-object v0

    .line 950
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/android/server/wm/ActivityStarter;->setCallingUid(I)Lcom/android/server/wm/ActivityStarter;

    move-result-object v0

    .line 951
    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityStarter;->setUserId(I)Lcom/android/server/wm/ActivityStarter;

    move-result-object v0

    .line 952
    invoke-virtual {v0, v5}, Lcom/android/server/wm/ActivityStarter;->setActivityInfo(Landroid/content/pm/ActivityInfo;)Lcom/android/server/wm/ActivityStarter;

    move-result-object v0

    .line 953
    invoke-virtual {v2}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/android/server/wm/ActivityStarter;->setActivityOptions(Landroid/os/Bundle;)Lcom/android/server/wm/ActivityStarter;

    move-result-object v0

    .line 954
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityStarter;->execute()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 957
    nop

    .end local v1    # "userId":I
    .end local v2    # "options":Landroid/app/ActivityOptions;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "componentName":Landroid/content/ComponentName;
    .end local v5    # "aInfo":Landroid/content/pm/ActivityInfo;
    goto :goto_1

    .line 935
    .restart local v1    # "userId":I
    :cond_1
    :goto_0
    return-void

    .line 955
    .end local v1    # "userId":I
    :catch_0
    move-exception v0

    .line 956
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 958
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private updateMultiTaskWhiteList(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 796
    nop

    .line 797
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "split_screen_applist"

    const-string v2, "miui_multi_task_white_list"

    .line 796
    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 799
    .local v0, "data":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 800
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 801
    .local v1, "apps":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 802
    iget-object v3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMultipleTaskWhiteList:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 801
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 804
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 808
    .end local v0    # "data":Ljava/lang/String;
    .end local v1    # "apps":Lorg/json/JSONArray;
    :cond_1
    goto :goto_1

    .line 806
    :catch_0
    move-exception v0

    .line 807
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ATMSImpl"

    const-string v2, "Get multi_task whitelist from xml: "

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 809
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method


# virtual methods
.method public addFlipActivityFullScreen(Ljava/lang/String;)V
    .locals 2
    .param p1, "setFlipActivityFullScreen"    # Ljava/lang/String;

    .line 1885
    sget-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1886
    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 1888
    :cond_0
    return-void
.end method

.method public addTransitionSyncId(I)V
    .locals 2
    .param p1, "syncId"    # I

    .line 1747
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mTransitionSyncIdList:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1748
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;

    .line 1123
    if-eqz p2, :cond_1

    array-length v0, p2

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    goto :goto_0

    .line 1126
    :cond_0
    aget-object v0, p2, v1

    .line 1127
    .local v0, "extCmd":Ljava/lang/String;
    const-string v1, ""

    .line 1128
    .local v1, "prefix":Ljava/lang/String;
    const-string v2, "packages"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1129
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageSettingsManager:Lcom/android/server/wm/PackageSettingsManager;

    invoke-virtual {v2, p1, v1}, Lcom/android/server/wm/PackageSettingsManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    goto :goto_1

    .line 1124
    .end local v0    # "extCmd":Ljava/lang/String;
    .end local v1    # "prefix":Ljava/lang/String;
    :cond_1
    :goto_0
    const-string v0, "dump nothing for ext!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1132
    :cond_2
    :goto_1
    return-void
.end method

.method executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
    .locals 2
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 1089
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/MiuiSizeCompatInternal;->executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1090
    return v1

    .line 1094
    :cond_0
    invoke-static {}, Landroid/appcompat/ApplicationCompatUtilsStub;->get()Landroid/appcompat/ApplicationCompatUtilsStub;

    move-result-object v0

    invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isContinuityEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1095
    invoke-static {}, Lcom/android/server/wm/AppContinuityRouterStub;->get()Lcom/android/server/wm/AppContinuityRouterStub;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/AppContinuityRouterStub;->executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1096
    return v1

    .line 1099
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageConfigurationController:Lcom/android/server/wm/PackageConfigurationController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/PackageConfigurationController;->executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    return v0
.end method

.method public forceLaunchNewTaskForMultipleTask(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;ILandroid/app/ActivityOptions;)Z
    .locals 4
    .param p1, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "intentActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "launchFlag"    # I
    .param p4, "options"    # Landroid/app/ActivityOptions;

    .line 878
    const/4 v0, 0x0

    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p4, :cond_2

    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 879
    invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->inMultipleTaskWhiteList(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 883
    :cond_0
    invoke-virtual {p4}, Landroid/app/ActivityOptions;->getLaunchRootTask()Landroid/window/WindowContainerToken;

    move-result-object v1

    if-eqz v1, :cond_1

    const/high16 v1, 0x8000000

    and-int/2addr v1, p3

    if-eqz v1, :cond_1

    .line 885
    invoke-virtual {p4}, Landroid/app/ActivityOptions;->getLaunchRootTask()Landroid/window/WindowContainerToken;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/wm/Task;->fromWindowContainerToken(Landroid/window/WindowContainerToken;)Lcom/android/server/wm/Task;

    move-result-object v1

    .line 886
    .local v1, "launchRootTask":Lcom/android/server/wm/Task;
    if-eqz v1, :cond_1

    iget-boolean v2, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v2, :cond_1

    .line 887
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 888
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Use new task for split, intentActivity: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ATMSImpl"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    const/4 v0, 0x1

    return v0

    .line 893
    .end local v1    # "launchRootTask":Lcom/android/server/wm/Task;
    :cond_1
    return v0

    .line 880
    :cond_2
    :goto_0
    return v0
.end method

.method public freeFormAndFullScreenToggleByKeyCombination(Z)Z
    .locals 2
    .param p1, "isStartFreeForm"    # Z

    .line 1728
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "freeFormAndFullScreenToggleByKeyCombination "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ATMSImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1729
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 1730
    .local v0, "miuiFreeFormManagerService":Lcom/android/server/wm/MiuiFreeFormManagerService;
    iget-object v1, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->freeFormAndFullScreenToggleByKeyCombination(Z)V

    .line 1731
    const/4 v1, 0x1

    return v1
.end method

.method public getActivityControllerUid()I
    .locals 1

    .line 1690
    iget v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mActivityControlUid:I

    return v0
.end method

.method public getAllFlipActivityFullScreen()Landroid/util/ArraySet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1897
    sget-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO:Landroid/util/ArraySet;

    return-object v0
.end method

.method public getAspectGravity(Ljava/lang/String;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 591
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

    if-eqz v0, :cond_0

    .line 592
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->getAspectGravityByPackage(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 594
    :cond_0
    const/16 v0, 0x11

    return v0
.end method

.method public getAspectRatio(Ljava/lang/String;)F
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 579
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

    if-nez v0, :cond_0

    .line 580
    const/high16 v0, -0x40800000    # -1.0f

    return v0

    .line 581
    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->getAspectRatioByPackage(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getContinuityList(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "policyName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 283
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mFoldablePackagePolicy:Lcom/android/server/wm/FoldablePackagePolicy;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/FoldablePackagePolicy;->getContinuityList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getContinuityVersion()J
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mFoldablePackagePolicy:Lcom/android/server/wm/FoldablePackagePolicy;

    invoke-virtual {v0}, Lcom/android/server/wm/FoldablePackagePolicy;->getContinuityVersion()J

    move-result-wide v0

    return-wide v0
.end method

.method getGlobalConfigurationForMiui(Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/WindowProcessController;)Landroid/content/res/Configuration;
    .locals 2
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "app"    # Lcom/android/server/wm/WindowProcessController;

    .line 1003
    sget-boolean v0, Lmiui/os/Build;->IS_MIUI:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 1004
    const-string v1, "com.android.thememanager"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1005
    invoke-virtual {p2}, Lcom/android/server/wm/WindowProcessController;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 1006
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getGlobalConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0

    .line 1008
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLastResumedActivityInfo()Landroid/content/pm/ActivityInfo;
    .locals 6

    .line 1012
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    .line 1013
    .local v0, "uid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 1014
    .local v1, "pid":I
    const/16 v2, 0x3e8

    const/4 v3, 0x0

    if-eq v0, v2, :cond_0

    const-string v2, "android.permission.REAL_GET_TASKS"

    .line 1015
    invoke-static {v2, v1, v0}, Lcom/android/server/wm/ActivityTaskManagerService;->checkPermission(Ljava/lang/String;II)I

    move-result v2

    if-eqz v2, :cond_0

    .line 1017
    const-string v2, "ATMSImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "permission denied for, callingPid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , callingUid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", requires: android.Manifest.permission.REAL_GET_TASKS"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1019
    return-object v3

    .line 1021
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v2

    .line 1022
    :try_start_0
    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mLastResumedActivity:Lcom/android/server/wm/ActivityRecord;

    .line 1023
    .local v4, "activity":Lcom/android/server/wm/ActivityRecord;
    if-nez v4, :cond_1

    goto :goto_0

    :cond_1
    iget-object v3, v4, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    :goto_0
    monitor-exit v2

    return-object v3

    .line 1024
    .end local v4    # "activity":Lcom/android/server/wm/ActivityRecord;
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "metaDataKey"    # Ljava/lang/String;

    .line 1055
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageManager:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    const-wide/16 v3, 0x80

    invoke-virtual {v1, p1, v3, v4, v2}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 1056
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_0

    .line 1057
    return v0

    .line 1059
    :cond_0
    iget-object v2, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 1060
    .local v2, "metaData":Landroid/os/Bundle;
    if-eqz v2, :cond_1

    invoke-virtual {v2, p2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1061
    invoke-virtual {v2, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 1065
    .end local v1    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "metaData":Landroid/os/Bundle;
    :cond_1
    goto :goto_0

    .line 1063
    :catch_0
    move-exception v1

    .line 1064
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1067
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method getMetaDataFloat(Ljava/lang/String;Ljava/lang/String;)F
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "metaDataKey"    # Ljava/lang/String;

    .line 1072
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageManager:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    const-wide/16 v3, 0x80

    invoke-virtual {v1, p1, v3, v4, v2}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 1073
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_0

    .line 1074
    return v0

    .line 1076
    :cond_0
    iget-object v2, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 1077
    .local v2, "metaData":Landroid/os/Bundle;
    if-eqz v2, :cond_1

    invoke-virtual {v2, p2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1078
    invoke-virtual {v2, p2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 1082
    .end local v1    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "metaData":Landroid/os/Bundle;
    :cond_1
    goto :goto_0

    .line 1080
    :catch_0
    move-exception v1

    .line 1081
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1083
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method public getMiuiActivityController()Lcom/android/server/wm/MiuiActivityController;
    .locals 1

    .line 217
    sget-object v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->INSTANCE:Lcom/android/server/wm/MiuiActivityControllerImpl;

    return-object v0
.end method

.method public getMiuiSizeCompatIn()Lcom/android/server/wm/MiuiSizeCompatInternal;
    .locals 1

    .line 279
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

    return-object v0
.end method

.method public getOrientation(Lcom/android/server/wm/Task;)I
    .locals 5
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 1530
    const/4 v0, -0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1531
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v2, 0x258

    if-ge v1, v2, :cond_0

    goto :goto_0

    .line 1553
    :cond_0
    return v0

    .line 1535
    :cond_1
    :goto_0
    if-eqz p1, :cond_4

    .line 1536
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 1537
    .local v1, "rootTask":Lcom/android/server/wm/Task;
    iget-boolean v2, v1, Lcom/android/server/wm/Task;->mSoScRoot:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiSoScManagerStub;->isInSoScSingleMode(Lcom/android/server/wm/WindowContainer;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/android/server/wm/Task;->mTransitionController:Lcom/android/server/wm/TransitionController;

    .line 1538
    invoke-virtual {v2}, Lcom/android/server/wm/TransitionController;->isCollecting()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1539
    :cond_2
    return v3

    .line 1542
    :cond_3
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mTransitionSyncIdList:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1543
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getRotation()I

    move-result v2

    if-nez v2, :cond_4

    iget-boolean v2, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v2, :cond_4

    .line 1544
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->hasChild()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1545
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I

    move-result v2

    const/4 v4, 0x6

    if-ne v2, v4, :cond_4

    .line 1546
    return v3

    .line 1550
    .end local v1    # "rootTask":Lcom/android/server/wm/Task;
    :cond_4
    return v0
.end method

.method public getPackageHoldOn()Ljava/lang/String;
    .locals 1

    .line 610
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageHoldOn:Ljava/lang/String;

    return-object v0
.end method

.method public getPolicy(Ljava/lang/String;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 600
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageSettingsManager:Lcom/android/server/wm/PackageSettingsManager;

    iget-object v0, v0, Lcom/android/server/wm/PackageSettingsManager;->mDisplayCompatPackages:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->getPolicy(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getScaleMode(Ljava/lang/String;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 585
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

    if-nez v0, :cond_0

    .line 586
    const/4 v0, 0x0

    return v0

    .line 587
    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->getScaleModeByPackage(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getSplitTaskInfo(Landroid/os/IBinder;)Landroid/app/ActivityManager$RunningTaskInfo;
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;

    .line 1104
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 1105
    :try_start_0
    invoke-static {p1}, Lcom/android/server/wm/ActivityRecord;->forTokenLocked(Landroid/os/IBinder;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 1106
    .local v1, "record":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1107
    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/Task;->mTaskStub:Lcom/android/server/wm/TaskStub$MutableTaskStub;

    invoke-interface {v2}, Lcom/android/server/wm/TaskStub$MutableTaskStub;->isSplitMode()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1108
    :cond_0
    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v2

    monitor-exit v0

    return-object v2

    .line 1110
    .end local v1    # "record":Lcom/android/server/wm/ActivityRecord;
    :cond_1
    monitor-exit v0

    .line 1111
    const/4 v0, 0x0

    return-object v0

    .line 1110
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getTopTaskVisibleActivities(Lcom/android/server/wm/Task;)Ljava/util/List;
    .locals 3
    .param p1, "mainStack"    # Lcom/android/server/wm/Task;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/wm/Task;",
            ")",
            "Ljava/util/List<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .line 1710
    if-nez p1, :cond_0

    .line 1711
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 1713
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->topRunningActivitiesLocked()Ljava/util/List;

    move-result-object v0

    .line 1714
    .local v0, "activityRecordList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    if-nez v0, :cond_1

    .line 1715
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    return-object v1

    .line 1718
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda3;

    invoke-direct {v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda3;-><init>()V

    .line 1719
    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda4;

    invoke-direct {v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda4;-><init>()V

    .line 1720
    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v1

    .line 1724
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1718
    return-object v1
.end method

.method public handleQSOnConfigureChanged(II)V
    .locals 1
    .param p1, "userId"    # I
    .param p2, "change"    # I

    .line 1352
    invoke-static {}, Landroid/app/TaskSnapshotHelperStub;->get()Landroid/app/TaskSnapshotHelperStub;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/app/TaskSnapshotHelperStub;->destroyQS(I)V

    .line 1353
    return-void
.end method

.method hasMetaData(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "metaDataKey"    # Ljava/lang/String;

    .line 1038
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageManager:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    const-wide/16 v3, 0x80

    invoke-virtual {v1, p1, v3, v4, v2}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 1039
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_0

    .line 1040
    return v0

    .line 1042
    :cond_0
    iget-object v2, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 1043
    .local v2, "metaData":Landroid/os/Bundle;
    if-eqz v2, :cond_2

    .line 1044
    invoke-virtual {v2, p2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 1048
    .end local v1    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "metaData":Landroid/os/Bundle;
    :cond_2
    goto :goto_0

    .line 1046
    :catch_0
    move-exception v1

    .line 1047
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1050
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method public hideLockedProfile(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 3
    .param p1, "mLastResumedActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 1766
    const/4 v0, 0x1

    if-nez p1, :cond_0

    .line 1767
    return v0

    .line 1769
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.cts.verifier/.managedprovisioning.RecentsRedactionActivity"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public hookGetCallingPkg(Landroid/os/IBinder;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "originCallingPkg"    # Ljava/lang/String;

    .line 315
    const/4 v0, 0x0

    .line 316
    .local v0, "hostApp":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 317
    :try_start_0
    invoke-static {p1}, Lcom/android/server/wm/ActivityRecord;->isInRootTaskLocked(Landroid/os/IBinder;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 318
    .local v2, "r":Lcom/android/server/wm/ActivityRecord;
    if-eqz v2, :cond_0

    .line 319
    iget-object v3, v2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    move-object v0, v3

    .line 321
    .end local v2    # "r":Lcom/android/server/wm/ActivityRecord;
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    invoke-static {v0, p2}, Lcom/miui/hybrid/hook/HookClient;->hookGetCallingPkg(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 321
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public hookStartActivity(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callingPackage"    # Ljava/lang/String;

    .line 308
    invoke-static {p1, p2}, Lcom/miui/hybrid/hook/HookClient;->redirectStartActivity(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 309
    .local v0, "redirectedIntent":Landroid/content/Intent;
    const-string v1, "notify"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    return-object v0
.end method

.method ignoreSpecifiedSource(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 368
    sget-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mIgnoreUriCheckPkg:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public inMiuiGameSizeCompat(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 291
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->inMiuiGameSizeCompat(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public inMultipleTaskWhiteList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 868
    const-string v0, "android"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 869
    const/4 v0, 0x1

    return v0

    .line 873
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMultipleTaskWhiteList:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public inResizeBlackList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 857
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mResizeBlackList:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method init(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/content/Context;)V
    .locals 2
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 222
    iput-object p1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 223
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    .line 224
    iput-object p2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 234
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationImpl;->getInstance()Lcom/android/server/wm/MiuiOrientationImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->init(Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Lcom/android/server/wm/ActivityTaskManagerService;)V

    .line 236
    invoke-static {}, Lcom/android/server/wm/ActivityStarterImpl;->getInstance()Lcom/android/server/wm/ActivityStarterImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/ActivityStarterImpl;->init(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/content/Context;)V

    .line 238
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 239
    invoke-static {}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->getInstance()Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->init(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/content/Context;)V

    .line 240
    return-void
.end method

.method public isAppSizeCompatRestarting(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1357
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->isAppSizeCompatRestarting(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCallerRecents(Lcom/android/server/wm/ActivityTaskManagerService;ILandroid/content/ComponentName;)Z
    .locals 1
    .param p1, "service"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "uid"    # I
    .param p3, "componentName"    # Landroid/content/ComponentName;

    .line 1947
    invoke-virtual {p1, p2}, Lcom/android/server/wm/ActivityTaskManagerService;->isCallerRecents(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1, p3}, Lcom/android/server/wm/ActivityTaskManagerService;->isFlipComponent(Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isCameraForeground()Z
    .locals 2

    .line 1321
    sget-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lastForegroundPkg:Ljava/lang/String;

    const-string v1, "com.android.camera"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isControllerAMonkey()Z
    .locals 1

    .line 1805
    invoke-static {}, Lmiui/os/Build;->isDebuggable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mController:Landroid/app/IActivityController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mControllerIsAMonkey:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isFixedAspectRatioPackage(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 1115
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1116
    return v1

    .line 1117
    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->getAspectRatioByPackage(Ljava/lang/String;)F

    move-result v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public isFreeFormExit(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 1230
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 1232
    .local v0, "miuiFreeFormManagerService":Lcom/android/server/wm/MiuiFreeFormManagerService;
    nop

    .line 1233
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(Ljava/lang/String;I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1234
    .local v1, "miuiFreeFormActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 1237
    if-eqz v1, :cond_0

    .line 1235
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1236
    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isHideStackFromFullScreen()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v2, v2, Lcom/android/server/wm/Task;->mTaskId:I

    .line 1237
    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move v3, v4

    goto :goto_0

    .line 1238
    :cond_1
    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    move v3, v4

    .line 1234
    :goto_0
    return v3
.end method

.method isGetTasksOpAllowed(Ljava/lang/String;II)Z
    .locals 5
    .param p1, "caller"    # Ljava/lang/String;
    .param p2, "pid"    # I
    .param p3, "uid"    # I

    .line 327
    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 328
    return v1

    .line 330
    :cond_0
    const-string v0, "getRunningAppProcesses"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 331
    return v1

    .line 334
    :cond_1
    const/4 v0, 0x0

    .line 335
    .local v0, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v2

    .line 336
    :try_start_0
    iget-object v3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mProcessMap:Lcom/android/server/wm/WindowProcessControllerMap;

    invoke-virtual {v3, p2}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v3

    .line 337
    .local v3, "wpc":Lcom/android/server/wm/WindowProcessController;
    if-eqz v3, :cond_2

    iget-object v4, v3, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v4, :cond_2

    .line 338
    iget-object v4, v3, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object v0, v4

    .line 340
    .end local v3    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    if-nez v0, :cond_3

    .line 342
    return v1

    .line 344
    :cond_3
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getAppOpsManager()Landroid/app/AppOpsManager;

    move-result-object v2

    .line 345
    .local v2, "opsManager":Landroid/app/AppOpsManager;
    const/16 v3, 0x2723

    invoke-virtual {v2, v3, p3, v0}, Landroid/app/AppOpsManager;->checkOp(IILjava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    const/4 v1, 0x1

    :cond_4
    return v1

    .line 340
    .end local v2    # "opsManager":Landroid/app/AppOpsManager;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
    .locals 8
    .param p1, "wc"    # Lcom/android/server/wm/WindowContainer;

    .line 1647
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1648
    :cond_0
    const/4 v1, 0x0

    .line 1649
    .local v1, "task":Lcom/android/server/wm/Task;
    instance-of v2, p1, Lcom/android/server/wm/Task;

    if-eqz v2, :cond_1

    .line 1650
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v1

    goto :goto_0

    .line 1651
    :cond_1
    instance-of v2, p1, Lcom/android/server/wm/ActivityRecord;

    if-eqz v2, :cond_2

    .line 1652
    move-object v2, p1

    check-cast v2, Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v1

    goto :goto_0

    .line 1653
    :cond_2
    instance-of v2, p1, Lcom/android/server/wm/WindowState;

    if-eqz v2, :cond_3

    .line 1654
    move-object v2, p1

    check-cast v2, Lcom/android/server/wm/WindowState;

    invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 1656
    :cond_3
    :goto_0
    const/4 v2, 0x1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->skipTaskForMultiWindow(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1657
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->getTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/android/server/wm/TaskDisplayArea;->getNextFocusableRootTask(Lcom/android/server/wm/Task;Z)Lcom/android/server/wm/Task;

    move-result-object v1

    .line 1659
    :cond_4
    if-nez v1, :cond_5

    return v0

    .line 1660
    :cond_5
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->isRootTask()Z

    move-result v3

    .line 1661
    .local v3, "isRoot":Z
    const/4 v4, 0x6

    if-eqz v3, :cond_7

    .line 1662
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;

    move-result-object v5

    .line 1663
    .local v5, "container":Lcom/android/server/wm/WindowContainer;
    iget-boolean v6, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v6, :cond_6

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_6

    .line 1664
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v6

    if-ne v6, v2, :cond_6

    if-eqz v5, :cond_6

    .line 1666
    invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 1667
    invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I

    move-result v6

    if-ne v6, v4, :cond_6

    .line 1668
    invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->hasChild()Z

    move-result v4

    if-eqz v4, :cond_6

    move v0, v2

    goto :goto_1

    :cond_6
    nop

    .line 1663
    :goto_1
    return v0

    .line 1670
    .end local v5    # "container":Lcom/android/server/wm/WindowContainer;
    :cond_7
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v5

    .line 1671
    .local v5, "rootTask":Lcom/android/server/wm/Task;
    if-nez v5, :cond_8

    return v0

    .line 1672
    :cond_8
    invoke-virtual {v5}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    .line 1673
    .local v6, "container":Lcom/android/server/wm/WindowContainer;
    iget-boolean v7, v5, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v7, :cond_9

    invoke-virtual {v5}, Lcom/android/server/wm/Task;->hasChild()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1674
    invoke-virtual {v5}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v7

    if-ne v7, v2, :cond_9

    if-eqz v6, :cond_9

    .line 1675
    invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 1676
    invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I

    move-result v7

    if-ne v7, v4, :cond_9

    move v0, v2

    goto :goto_2

    :cond_9
    nop

    .line 1673
    :goto_2
    return v0
.end method

.method public isSplitExist(Ljava/lang/String;I)Z
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 1243
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1244
    const/4 v0, 0x0

    return v0

    .line 1246
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->retrieveFirstSplitWindowRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;

    move-result-object v0

    .line 1247
    .local v0, "rootTaskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo;
    const/4 v1, 0x0

    .line 1248
    .local v1, "isInSplitWindow":Z
    const-string v2, "ATMSImpl"

    if-eqz v0, :cond_2

    .line 1249
    invoke-static {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getChildComponentNames(Landroid/app/ActivityTaskManager$RootTaskInfo;)[Landroid/content/ComponentName;

    move-result-object v3

    .line 1250
    .local v3, "componentNames":[Landroid/content/ComponentName;
    invoke-static {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getChildTaskUserIds(Landroid/app/ActivityTaskManager$RootTaskInfo;)[I

    move-result-object v4

    .line 1251
    .local v4, "childTaskUserIds":[I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isInSplitWindow: childTaskNames = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " childTaskUserIds ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1252
    invoke-static {v4}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1251
    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v6, v4

    if-ge v5, v6, :cond_2

    array-length v6, v3

    if-ge v5, v6, :cond_2

    .line 1254
    aget-object v6, v3, v5

    if-eqz v6, :cond_1

    aget-object v6, v3, v5

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    aget v6, v4, v5

    if-ne v6, p2, :cond_1

    .line 1256
    const/4 v1, 0x1

    .line 1257
    goto :goto_1

    .line 1253
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1261
    .end local v3    # "componentNames":[Landroid/content/ComponentName;
    .end local v4    # "childTaskUserIds":[I
    .end local v5    # "i":I
    :cond_2
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isInSplitWindow: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    return v1
.end method

.method public isVerticalSplit()Z
    .locals 2

    .line 1561
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/TaskDisplayArea;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_0

    .line 1563
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceImpl;->getInstance()Lcom/android/server/wm/WindowManagerServiceImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isCtsModeEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1561
    :goto_0
    return v0
.end method

.method public loadFlipComponent()Landroid/content/ComponentName;
    .locals 1

    .line 1940
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1941
    const/4 v0, 0x0

    return-object v0

    .line 1943
    :cond_0
    const-string v0, "com.miui.fliphome/.FlipLauncher"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public notifyActivityResumed(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/WindowManagerService;)V
    .locals 11
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "wms"    # Lcom/android/server/wm/WindowManagerService;

    .line 1326
    const-string v0, "ATMSImpl"

    if-nez p1, :cond_1

    .line 1327
    sget-object v1, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_STARTING_WINDOW:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v1}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1328
    const-string v1, "NotifyActivityResumed failed, activity = null"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1330
    :cond_0
    return-void

    .line 1332
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1333
    sget-object v1, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_STARTING_WINDOW:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v1}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1334
    const-string v1, "In MultiWindowMode, wont do snapshot, return !...."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    :cond_2
    return-void

    .line 1338
    :cond_3
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v3, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v4

    .line 1339
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I

    move-result v5

    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget v7, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromUid:I

    iget v8, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I

    iget-object v9, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPackage:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    .line 1340
    invoke-interface {v1}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->getIsColdStart()Z

    move-result v10

    .line 1338
    invoke-interface/range {v2 .. v10}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onActivityReusmeUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V

    .line 1343
    invoke-static {}, Lcom/android/server/wm/TaskSnapshotControllerInjectorStub;->get()Lcom/android/server/wm/TaskSnapshotControllerInjectorStub;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/wm/TaskSnapshotControllerInjectorStub;->canTakeSnapshot(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1344
    iget-object v0, p2, Lcom/android/server/wm/WindowManagerService;->mTaskSnapshotController:Lcom/android/server/wm/TaskSnapshotController;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/TaskSnapshotController;->notifyAppResumed(Lcom/android/server/wm/ActivityRecord;Z)V

    goto :goto_0

    .line 1345
    :cond_4
    sget-object v1, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_STARTING_WINDOW:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v1}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1346
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No snapshot since not start by launcher, activity="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 1347
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1346
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1349
    :cond_5
    :goto_0
    return-void
.end method

.method public onConfigurationChanged(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 352
    return-void
.end method

.method onForegroundActivityChanged(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;ILandroid/content/pm/ApplicationInfo;)V
    .locals 5
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "state"    # Lcom/android/server/wm/ActivityRecord$State;
    .param p3, "pid"    # I
    .param p4, "multiWindowAppInfo"    # Landroid/content/pm/ApplicationInfo;

    .line 466
    if-eqz p1, :cond_4

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 471
    :cond_0
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->getInstance()Lcom/android/server/wm/OneTrackRotationHelper;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/OneTrackRotationHelper;->reportPackageForeground(Ljava/lang/String;)V

    .line 473
    const/4 v0, 0x0

    .line 474
    .local v0, "skipReportForeground":Z
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 475
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isTopRunningActivity()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 476
    const-string v2, "ATMSImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Don\'t report foreground because "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not top running."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    const/4 v0, 0x1

    .line 479
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    sget-object v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lastForegroundPkg:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lastMultiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

    if-eq v1, p4, :cond_3

    :cond_2
    if-nez v0, :cond_3

    .line 485
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mProcessManagerIn:Lcom/miui/server/process/ProcessManagerInternal;

    new-instance v2, Lcom/android/server/wm/FgActivityChangedInfo;

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/android/server/wm/FgActivityChangedInfo;-><init>(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;ILandroid/content/pm/ApplicationInfo;)V

    invoke-virtual {v1, v2}, Lcom/miui/server/process/ProcessManagerInternal;->notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V

    .line 489
    sget-object v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lastForegroundPkg:Ljava/lang/String;

    invoke-static {p1, p3, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->reportPackageForeground(Lcom/android/server/wm/ActivityRecord;ILjava/lang/String;)V

    .line 491
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    sput-object v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lastForegroundPkg:Ljava/lang/String;

    .line 492
    sput-object p4, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lastMultiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

    .line 496
    :cond_3
    invoke-static {}, Lcom/android/server/wm/MiuiMiPerfStub;->getInstance()Lcom/android/server/wm/MiuiMiPerfStub;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/android/server/wm/MiuiMiPerfStub;->onAfterActivityResumed(Lcom/android/server/wm/ActivityRecord;)V

    .line 499
    invoke-static {}, Lcom/android/server/appcacheopt/AppCacheOptimizerStub;->getInstance()Lcom/android/server/appcacheopt/AppCacheOptimizerStub;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/android/server/appcacheopt/AppCacheOptimizerStub;->onForegroundActivityChanged(Ljava/lang/String;)V

    .line 505
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mProcessManagerIn:Lcom/miui/server/process/ProcessManagerInternal;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Lcom/miui/server/process/ProcessManagerInternal;->notifyActivityChanged(Landroid/content/ComponentName;)V

    .line 506
    return-void

    .line 479
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 467
    .end local v0    # "skipReportForeground":Z
    :cond_4
    :goto_0
    const-string v0, "ATMSImpl"

    const-string v1, "next or next process is null, skip report!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    return-void
.end method

.method public onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V
    .locals 14
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 422
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 425
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 426
    const-string v0, "ATMSImpl"

    const-string v1, "do not report freeform event"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    return-void

    .line 429
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 430
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v4

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I

    move-result v5

    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget v7, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromUid:I

    iget v8, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I

    iget-object v9, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPackage:Ljava/lang/String;

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    .line 432
    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->getIsColdStart()Z

    move-result v10

    .line 433
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    iget v11, v0, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    iget v12, v0, Lcom/android/server/wm/Task;->mCallingUid:I

    .line 434
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    iget-object v13, v0, Lcom/android/server/wm/Task;->mCallingPackage:Ljava/lang/String;

    .line 429
    invoke-interface/range {v2 .. v13}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onForegroundActivityChangedLocked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;ZIILjava/lang/String;)V

    .line 435
    invoke-static {}, Lcom/android/server/PinnerServiceStub;->get()Lcom/android/server/PinnerServiceStub;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/android/server/PinnerServiceStub;->onActivityChanged(Ljava/lang/String;)V

    .line 437
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->skipReportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 439
    :cond_2
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 440
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 441
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z

    move-result v3

    .line 440
    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyResumeTopActivity(ILjava/lang/String;Z)V

    .line 443
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->reportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)V

    .line 445
    invoke-static {}, Landroid/appcompat/ApplicationCompatUtilsStub;->get()Landroid/appcompat/ApplicationCompatUtilsStub;

    move-result-object v0

    invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isContinuityEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 446
    invoke-static {}, Lcom/android/server/wm/AppContinuityRouterStub;->get()Lcom/android/server/wm/AppContinuityRouterStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/AppContinuityRouterStub;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V

    .line 449
    :cond_4
    return-void

    .line 423
    :cond_5
    :goto_0
    return-void
.end method

.method public onFreeFormToFullScreen(Lcom/android/server/wm/ActivityRecord;)V
    .locals 4
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 381
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-nez v0, :cond_0

    goto :goto_0

    .line 385
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;

    move-result-object v0

    .line 386
    .local v0, "state":Lcom/android/server/wm/ActivityRecord$State;
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v1

    .line 387
    .local v1, "pid":I
    invoke-static {}, Lcom/android/server/MiuiFgThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$1;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$1;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;I)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 393
    return-void

    .line 382
    .end local v0    # "state":Lcom/android/server/wm/ActivityRecord$State;
    .end local v1    # "pid":I
    :cond_1
    :goto_0
    return-void
.end method

.method public onFreeFormToFullScreen(Lcom/android/server/wm/Task;)V
    .locals 2
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 637
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onFreeFormToFullScreen task: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ATMSImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 639
    .local v0, "rootTask":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_0

    .line 640
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onFreeFormToFullScreen(Lcom/android/server/wm/ActivityRecord;)V

    .line 642
    :cond_0
    return-void
.end method

.method public onLastResumedActivityChange(Lcom/android/server/wm/ActivityRecord;)V
    .locals 2
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 1757
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/wm/WindowManagerServiceStub;->checkFlipFullScreenChange(Lcom/android/server/wm/ActivityRecord;)V

    .line 1758
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isInitialColor()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1759
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    .line 1760
    invoke-interface {v1}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->getNavigationBarColor()I

    move-result v1

    .line 1759
    invoke-interface {v0, v1}, Lcom/android/server/wm/WindowManagerServiceStub;->saveNavigationBarColor(I)V

    .line 1762
    :cond_0
    return-void
.end method

.method public onMetaKeyCombination()V
    .locals 6

    .line 1184
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    if-nez v0, :cond_0

    return-void

    .line 1185
    :cond_0
    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 1186
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 1187
    .local v1, "topFocusedStack":Lcom/android/server/wm/Task;
    if-nez v1, :cond_1

    .line 1188
    const-string v2, "ATMSImpl"

    const-string v3, "no stack focued, do noting."

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1189
    monitor-exit v0

    return-void

    .line 1192
    :cond_1
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v2

    .line 1193
    .local v2, "windowingMode":I
    sparse-switch v2, :sswitch_data_0

    .line 1208
    monitor-exit v0

    goto :goto_1

    .line 1204
    :sswitch_0
    iget-object v3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    check-cast v3, Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 1205
    .local v3, "miuiFreeFormManagerService":Lcom/android/server/wm/MiuiFreeFormManagerService;
    iget-object v4, v3, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    invoke-virtual {v4, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->exitFreeFormByKeyCombination(Lcom/android/server/wm/Task;)V

    .line 1206
    goto :goto_0

    .line 1195
    .end local v3    # "miuiFreeFormManagerService":Lcom/android/server/wm/MiuiFreeFormManagerService;
    :sswitch_1
    invoke-direct {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->exitSplitScreenIfNeed(Lcom/android/server/wm/WindowContainer;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 1197
    :cond_2
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v3

    const-string v4, "onMetaKeyCombination"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 1198
    .local v3, "result":Ljava/lang/Object;
    if-eqz v3, :cond_3

    move-object v4, v3

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_0

    .line 1199
    :cond_3
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1200
    invoke-virtual {v1, v1}, Lcom/android/server/wm/Task;->moveTaskToBack(Lcom/android/server/wm/Task;)Z

    .line 1210
    .end local v1    # "topFocusedStack":Lcom/android/server/wm/Task;
    .end local v2    # "windowingMode":I
    .end local v3    # "result":Ljava/lang/Object;
    :cond_4
    :goto_0
    monitor-exit v0

    .line 1211
    return-void

    .line 1208
    .restart local v1    # "topFocusedStack":Lcom/android/server/wm/Task;
    .restart local v2    # "windowingMode":I
    :goto_1
    return-void

    .line 1210
    .end local v1    # "topFocusedStack":Lcom/android/server/wm/Task;
    .end local v2    # "windowingMode":I
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method onSystemReady()V
    .locals 2

    .line 245
    const-class v0, Lcom/android/server/wm/MiuiSizeCompatInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiSizeCompatInternal;

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mMiuiSizeCompatIn:Lcom/android/server/wm/MiuiSizeCompatInternal;

    .line 246
    if-eqz v0, :cond_0

    .line 247
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->onSystemReady(Lcom/android/server/wm/ActivityTaskManagerService;)V

    .line 252
    :cond_0
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationImpl;->getInstance()Lcom/android/server/wm/MiuiOrientationImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getPackageManager()Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    .line 253
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationImpl;->getInstance()Lcom/android/server/wm/MiuiOrientationImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->onSystemReady()V

    .line 256
    new-instance v0, Lcom/android/server/wm/PackageSettingsManager;

    invoke-direct {v0, p0}, Lcom/android/server/wm/PackageSettingsManager;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageSettingsManager:Lcom/android/server/wm/PackageSettingsManager;

    .line 257
    new-instance v0, Lcom/android/server/wm/PackageConfigurationController;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-direct {v0, v1}, Lcom/android/server/wm/PackageConfigurationController;-><init>(Lcom/android/server/wm/ActivityTaskManagerService;)V

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageConfigurationController:Lcom/android/server/wm/PackageConfigurationController;

    .line 258
    new-instance v0, Lcom/android/server/wm/FoldablePackagePolicy;

    invoke-direct {v0, p0}, Lcom/android/server/wm/FoldablePackagePolicy;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mFoldablePackagePolicy:Lcom/android/server/wm/FoldablePackagePolicy;

    .line 259
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageConfigurationController:Lcom/android/server/wm/PackageConfigurationController;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/PackageConfigurationController;->registerPolicy(Lcom/android/server/wm/PolicyImpl;)V

    .line 260
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageConfigurationController:Lcom/android/server/wm/PackageConfigurationController;

    invoke-virtual {v0}, Lcom/android/server/wm/PackageConfigurationController;->startThread()V

    .line 261
    const-string v0, "package"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageManager:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    .line 267
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mProcessManagerIn:Lcom/miui/server/process/ProcessManagerInternal;

    .line 268
    invoke-static {}, Lcom/android/server/wm/ActivityStarterImpl;->getInstance()Lcom/android/server/wm/ActivityStarterImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityStarterImpl;->onSystemReady()V

    .line 269
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mH:Lcom/android/server/wm/ActivityTaskManagerService$H;

    new-instance v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 274
    invoke-static {}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->getInstance()Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->onSystemReady()V

    .line 275
    return-void
.end method

.method registerSubScreenSwitchObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 985
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 987
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 988
    const-string/jumbo v1, "subscreen_switch"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$6;

    .line 989
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$6;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/os/Handler;)V

    .line 987
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 996
    return-void

    .line 986
    :cond_1
    :goto_0
    return-void
.end method

.method public removeFlipActivityFullScreen(Ljava/lang/String;)V
    .locals 2
    .param p1, "removeFlipActivityFullScreen"    # Ljava/lang/String;

    .line 1891
    sget-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1892
    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    .line 1894
    :cond_0
    return-void
.end method

.method public removeSplitTaskShotIfNeed()Z
    .locals 2

    .line 1418
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mScreenshotLayer:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mInAnimationOut:Z

    if-nez v0, :cond_0

    .line 1419
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    new-instance v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->post(Ljava/lang/Runnable;)Z

    .line 1420
    const/4 v0, 0x1

    return v0

    .line 1422
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public removeTransitionSyncId(I)V
    .locals 2
    .param p1, "syncId"    # I

    .line 1751
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mTransitionSyncIdList:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1752
    return-void
.end method

.method public restartSubScreenUiIfNeeded(ILjava/lang/String;)V
    .locals 4
    .param p1, "userId"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 901
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    const-string v1, "com.xiaomi.misubscreenui"

    .line 902
    const-wide/16 v2, 0x400

    invoke-interface {v0, v1, v2, v3, p1}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 903
    .local v0, "aInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v0, :cond_0

    .line 904
    const-string v1, "ATMSImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "aInfo is null when start subscreenui for user "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    return-void

    .line 907
    :cond_0
    invoke-virtual {p0, v0, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 910
    .end local v0    # "aInfo":Landroid/content/pm/ApplicationInfo;
    goto :goto_0

    .line 908
    :catch_0
    move-exception v0

    .line 909
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 911
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public restartSubScreenUiIfNeeded(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V
    .locals 4
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "reason"    # Ljava/lang/String;

    .line 914
    if-eqz p1, :cond_1

    const-string v0, "com.xiaomi.misubscreenui"

    iget-object v1, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 915
    invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isCTS()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    if-nez v0, :cond_0

    goto :goto_0

    .line 917
    :cond_0
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 927
    return-void

    .line 916
    :cond_1
    :goto_0
    return-void
.end method

.method public retrieveFirstSplitWindowRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;
    .locals 7

    .line 1273
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getAllRootTaskInfos()Ljava/util/List;

    move-result-object v0

    .line 1274
    .local v0, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityTaskManager$RootTaskInfo;

    .line 1275
    .local v2, "taskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo;
    invoke-virtual {v2}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v3

    .line 1276
    .local v3, "windowMode":I
    iget-boolean v4, v2, Landroid/app/ActivityTaskManager$RootTaskInfo;->visible:Z

    .line 1277
    .local v4, "visible":Z
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "retrieveFirstSplitWindowRootTaskInfo: windowMode = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " visible = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ATMSImpl"

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1278
    if-eqz v4, :cond_0

    const/4 v5, 0x1

    if-ne v3, v5, :cond_0

    .line 1279
    return-object v2

    .line 1281
    .end local v2    # "taskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v3    # "windowMode":I
    .end local v4    # "visible":Z
    :cond_0
    goto :goto_0

    .line 1282
    :cond_1
    const/4 v1, 0x0

    return-object v1
.end method

.method public setActivityController(Landroid/app/IActivityController;)V
    .locals 2
    .param p1, "control"    # Landroid/app/IActivityController;

    .line 1683
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setActivityController callingUid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " control="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ATMSImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1684
    if-eqz p1, :cond_1

    .line 1685
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mActivityControlUid:I

    goto :goto_1

    .line 1687
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mActivityControlUid:I

    .line 1688
    :goto_1
    return-void
.end method

.method public setPackageHoldOn(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)V
    .locals 7
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 615
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 616
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getAllRootTaskInfos()Ljava/util/List;

    move-result-object v0

    .line 617
    .local v0, "stackList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityTaskManager$RootTaskInfo;

    .line 618
    .local v2, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    iget-object v3, v2, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v3, :cond_0

    iget-object v3, v2, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 619
    iget-object v3, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    iget v4, v2, Landroid/app/ActivityTaskManager$RootTaskInfo;->taskId:I

    invoke-virtual {v3, v4}, Lcom/android/server/wm/RootWindowContainer;->getRootTask(I)Lcom/android/server/wm/Task;

    move-result-object v3

    .line 620
    .local v3, "stack":Lcom/android/server/wm/Task;
    if-eqz v3, :cond_0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 621
    iput-object p2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageHoldOn:Ljava/lang/String;

    .line 622
    const-class v1, Lcom/android/server/wm/WindowManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/WindowManagerInternal;

    .line 623
    .local v1, "wm":Lcom/android/server/wm/WindowManagerInternal;
    invoke-virtual {v3, v4, v5}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-virtual {v1, v4, v5}, Lcom/android/server/wm/WindowManagerInternal;->setHoldOn(Landroid/os/IBinder;Z)V

    .line 624
    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    const-string v5, "power"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    .line 625
    .local v4, "pm":Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 626
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Going to sleep and hold on, package name in hold on: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageHoldOn:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ATMSImpl"

    invoke-static {v6, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    goto :goto_1

    .line 630
    .end local v1    # "wm":Lcom/android/server/wm/WindowManagerInternal;
    .end local v2    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v3    # "stack":Lcom/android/server/wm/Task;
    .end local v4    # "pm":Landroid/os/PowerManager;
    :cond_0
    goto :goto_0

    .line 631
    .end local v0    # "stackList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
    :cond_1
    :goto_1
    goto :goto_2

    .line 632
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mPackageHoldOn:Ljava/lang/String;

    .line 634
    :goto_2
    return-void
.end method

.method public setPauseAdvancedForTask([IZ)V
    .locals 10
    .param p1, "taskIds"    # [I
    .param p2, "userLeaving"    # Z

    .line 1812
    array-length v0, p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_6

    aget v3, p1, v2

    .line 1813
    .local v3, "taskId":I
    const/4 v4, -0x1

    if-le v3, v4, :cond_5

    .line 1814
    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v4, v3}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(I)Lcom/android/server/wm/Task;

    move-result-object v4

    .line 1815
    .local v4, "task":Lcom/android/server/wm/Task;
    if-nez v4, :cond_0

    .line 1816
    goto/16 :goto_2

    .line 1818
    :cond_0
    invoke-virtual {v4}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v5

    .line 1819
    .local v5, "topNonFinishingActivity":Lcom/android/server/wm/ActivityRecord;
    if-nez v5, :cond_1

    .line 1820
    goto/16 :goto_2

    .line 1822
    :cond_1
    invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord;->getTaskFragment()Lcom/android/server/wm/TaskFragment;

    move-result-object v6

    .line 1823
    .local v6, "taskFragment":Lcom/android/server/wm/TaskFragment;
    const-string v7, "ATMSImpl"

    if-eqz v6, :cond_4

    .line 1824
    iget-object v8, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAdvanceTaskIds:Ljava/util/List;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1825
    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/android/server/wm/TaskFragment;->mPauseAdvance:Z

    .line 1826
    iget-object v9, v6, Lcom/android/server/wm/TaskFragment;->mTransitionController:Lcom/android/server/wm/TransitionController;

    invoke-virtual {v9, v4}, Lcom/android/server/wm/TransitionController;->isTransientHide(Lcom/android/server/wm/Task;)Z

    move-result v9

    if-eqz v9, :cond_2

    if-eqz p2, :cond_2

    move v9, v8

    goto :goto_1

    :cond_2
    move v9, v1

    :goto_1
    move p2, v9

    .line 1828
    invoke-direct {p0, v5, v4, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->canEnterPipOnTaskSwitch(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/Task;Z)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1829
    iput-boolean v8, v5, Lcom/android/server/wm/ActivityRecord;->supportsEnterPipOnTaskSwitch:Z

    .line 1831
    :cond_3
    const/4 v8, 0x0

    const-string/jumbo v9, "setPauseAdvanced"

    invoke-virtual {v6, p2, v1, v8, v9}, Lcom/android/server/wm/TaskFragment;->startPausing(ZZLcom/android/server/wm/ActivityRecord;Ljava/lang/String;)Z

    .line 1833
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "setPauseAdvanced: userLeaving= "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",Task="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",resume="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v5, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1836
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "setPauseAdvance: Task "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " not found."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1812
    .end local v3    # "taskId":I
    .end local v4    # "task":Lcom/android/server/wm/Task;
    .end local v5    # "topNonFinishingActivity":Lcom/android/server/wm/ActivityRecord;
    .end local v6    # "taskFragment":Lcom/android/server/wm/TaskFragment;
    :cond_5
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1840
    :cond_6
    return-void
.end method

.method public setSplitScreenDirection(I)Z
    .locals 5
    .param p1, "direction"    # I

    .line 1142
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSoScManagerStub;->setSplitScreenDirection(I)Z

    move-result v0

    return v0

    .line 1146
    :cond_0
    const-string v0, ""

    .line 1148
    .local v0, "str":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 1149
    const-string/jumbo v0, "snap_to_left"

    .line 1152
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 1153
    const-string/jumbo v0, "snap_to_right"

    .line 1155
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    return v3

    .line 1157
    :cond_3
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    if-nez v2, :cond_4

    return v3

    .line 1159
    :cond_4
    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v2

    .line 1160
    .local v2, "topFocusedStack":Lcom/android/server/wm/Task;
    if-nez v2, :cond_5

    .line 1161
    const-string v1, "ATMSImpl"

    const-string v4, "no stack focued, do noting."

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1162
    return v3

    .line 1165
    :cond_5
    invoke-virtual {p0, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1166
    iget-object v3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerService;->getUiContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "update_split_snap_target"

    invoke-static {v3, v4, v0}, Landroid/provider/MiuiSettings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 1169
    :cond_6
    const/4 v3, 0x1

    .line 1171
    .local v3, "isLTR":Z
    if-nez p1, :cond_7

    .line 1172
    const/4 v3, 0x0

    .line 1175
    :cond_7
    if-ne p1, v1, :cond_8

    .line 1176
    const/4 v3, 0x1

    .line 1178
    :cond_8
    invoke-virtual {p0, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->toggleSplitByGesture(Z)Z

    .line 1180
    .end local v3    # "isLTR":Z
    :goto_0
    return v1
.end method

.method public shouldExcludeTaskFromRecents(Lcom/android/server/wm/Task;)Z
    .locals 4
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 1028
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1029
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-boolean v1, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-nez v1, :cond_0

    goto :goto_0

    .line 1031
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 1032
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "com.xiaomi.misubscreenui"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v3

    if-ne v2, v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 1030
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_2
    :goto_0
    return v0
.end method

.method public shouldNotApplyAspectRatio(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 4
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 1901
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 1902
    return v0

    .line 1905
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 1906
    invoke-virtual {v1}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[{}]"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1905
    invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->shouldNotApplyAspectRatio(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 1907
    invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->shouldNotApplyAspectRatio(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 1905
    :cond_2
    return v0
.end method

.method public shouldNotApplyAspectRatio(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 1911
    if-nez p1, :cond_0

    .line 1912
    const/4 v0, 0x0

    return v0

    .line 1914
    :cond_0
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1915
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " shouldNotApplyAspectRatio = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO:Landroid/util/ArraySet;

    .line 1916
    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1915
    const-string v1, "ATMSImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1918
    :cond_1
    sget-object v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public showScreenShotForSplitTask()V
    .locals 11

    .line 1470
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mScreenshotLayer:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_0

    return-void

    .line 1472
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 1473
    .local v0, "dc":Lcom/android/server/wm/DisplayContent;
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v1

    .line 1474
    .local v1, "info":Landroid/view/DisplayInfo;
    new-instance v2, Landroid/graphics/Rect;

    iget v3, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    iget v4, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    const/4 v5, 0x0

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1476
    .local v2, "bounds":Landroid/graphics/Rect;
    new-instance v3, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;

    .line 1477
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;-><init>(Landroid/view/SurfaceControl;)V

    .line 1478
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;->setCaptureSecureLayers(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;

    move-result-object v3

    check-cast v3, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;

    .line 1479
    invoke-virtual {v3, v4}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;->setAllowProtected(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;

    move-result-object v3

    check-cast v3, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;

    .line 1480
    invoke-virtual {v3, v2}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;->setSourceCrop(Landroid/graphics/Rect;)Landroid/window/ScreenCapture$CaptureArgs$Builder;

    move-result-object v3

    check-cast v3, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;

    .line 1481
    invoke-virtual {v3}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;->build()Landroid/window/ScreenCapture$LayerCaptureArgs;

    move-result-object v3

    .line 1482
    .local v3, "displayCaptureArgs":Landroid/window/ScreenCapture$LayerCaptureArgs;
    nop

    .line 1483
    invoke-static {v3}, Landroid/window/ScreenCapture;->captureLayers(Landroid/window/ScreenCapture$LayerCaptureArgs;)Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;

    move-result-object v6

    .line 1484
    .local v6, "screenshotHardwareBuffer":Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;
    if-nez v6, :cond_1

    return-void

    .line 1485
    :cond_1
    nop

    .line 1486
    invoke-virtual {v6}, Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;->getHardwareBuffer()Landroid/hardware/HardwareBuffer;

    move-result-object v7

    .line 1485
    invoke-static {v7}, Landroid/graphics/GraphicBuffer;->createFromHardwareBuffer(Landroid/hardware/HardwareBuffer;)Landroid/graphics/GraphicBuffer;

    move-result-object v7

    .line 1488
    .local v7, "buffer":Landroid/graphics/GraphicBuffer;
    new-instance v8, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v8}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    .line 1489
    .local v8, "t":Landroid/view/SurfaceControl$Transaction;
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;

    move-result-object v9

    .line 1490
    const-string v10, "SplitTaskScreenShot"

    invoke-virtual {v9, v10}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v9

    .line 1491
    invoke-virtual {v9, v4}, Landroid/view/SurfaceControl$Builder;->setOpaque(Z)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 1492
    invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setSecure(Z)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 1493
    invoke-virtual {v4, v10}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 1494
    invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 1495
    invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mScreenshotLayer:Landroid/view/SurfaceControl;

    .line 1496
    const v5, 0x1e8480

    invoke-virtual {v8, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    .line 1497
    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mScreenshotLayer:Landroid/view/SurfaceControl;

    invoke-virtual {v8, v4, v7}, Landroid/view/SurfaceControl$Transaction;->setBuffer(Landroid/view/SurfaceControl;Landroid/graphics/GraphicBuffer;)Landroid/view/SurfaceControl$Transaction;

    .line 1498
    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mScreenshotLayer:Landroid/view/SurfaceControl;

    invoke-virtual {v6}, Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;->getColorSpace()Landroid/graphics/ColorSpace;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setColorSpace(Landroid/view/SurfaceControl;Landroid/graphics/ColorSpace;)Landroid/view/SurfaceControl$Transaction;

    .line 1499
    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mScreenshotLayer:Landroid/view/SurfaceControl;

    invoke-virtual {v8, v4}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 1500
    invoke-virtual {v8}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 1501
    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    new-instance v5, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda2;

    invoke-direct {v5, p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V

    const-wide/16 v9, 0x3e8

    invoke-virtual {v4, v5, v9, v10}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1502
    const-string v4, "ATMSImpl"

    const-string/jumbo v5, "show split task screen shot layer."

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1503
    return-void
.end method

.method public skipTaskForMultiWindow(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1735
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1736
    return v1

    .line 1739
    :cond_0
    const-string v0, "com.android.quicksearchbox"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1740
    const/4 v0, 0x1

    return v0

    .line 1742
    :cond_1
    return v1
.end method

.method public splitTaskIfNeed(Landroid/app/ActivityOptions;Lcom/android/server/wm/Task;)V
    .locals 9
    .param p1, "options"    # Landroid/app/ActivityOptions;
    .param p2, "startedActivityRootTask"    # Lcom/android/server/wm/Task;

    .line 1362
    if-eqz p2, :cond_8

    if-eqz p1, :cond_8

    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 1366
    :cond_0
    const-string v0, "getEnterAppPair"

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {p1, v0, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1367
    .local v0, "enterApppair":Ljava/lang/Object;
    const-string v2, "getAppPairPrimary"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, v2, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1368
    .local v1, "isPrimary":Ljava/lang/Object;
    if-eqz v0, :cond_7

    move-object v2, v0

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1369
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    .line 1370
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 1371
    .local v2, "task1":Lcom/android/server/wm/Task;
    move-object v4, p2

    .line 1372
    .local v4, "task2":Lcom/android/server/wm/Task;
    const-string v5, "The top app or drag\'s app is not supports multi window."

    const-string v6, "ATMSImpl"

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->supportsMultiWindow()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v4}, Lcom/android/server/wm/Task;->supportsMultiWindow()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1373
    :cond_1
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 1374
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->skipTaskForMultiWindow(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1380
    :cond_2
    if-ne v2, v4, :cond_3

    .line 1381
    const-string v3, "The task has been front."

    invoke-static {v6, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1382
    return-void

    .line 1385
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "pair t1:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " t2:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " isPrimary:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1386
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScSupported()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1387
    move-object v5, v1

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    xor-int/2addr v3, v5

    invoke-direct {p0, p2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->startSoSc(Lcom/android/server/wm/Task;I)V

    .line 1388
    return-void

    .line 1390
    :cond_4
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1391
    invoke-static {v6, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1392
    return-void

    .line 1395
    :cond_5
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->showScreenShotForSplitTask()V

    .line 1396
    invoke-virtual {v4, v3}, Lcom/android/server/wm/Task;->setHasBeenVisible(Z)V

    .line 1397
    invoke-virtual {v4}, Lcom/android/server/wm/Task;->sendTaskAppeared()V

    .line 1398
    iget-object v3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskOrganizerController:Lcom/android/server/wm/TaskOrganizerController;

    invoke-virtual {v3}, Lcom/android/server/wm/TaskOrganizerController;->dispatchPendingEvents()V

    .line 1399
    iget-object v3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget v5, v4, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v6, v1

    check-cast v6, Ljava/lang/Boolean;

    filled-new-array {v5, v6}, [Ljava/lang/Object;

    move-result-object v5

    const-string v6, "enterSplitScreen"

    invoke-static {v3, v6, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1376
    :cond_6
    invoke-static {v6, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1377
    return-void

    .line 1401
    .end local v2    # "task1":Lcom/android/server/wm/Task;
    .end local v4    # "task2":Lcom/android/server/wm/Task;
    :cond_7
    :goto_0
    return-void

    .line 1363
    .end local v0    # "enterApppair":Ljava/lang/Object;
    .end local v1    # "isPrimary":Ljava/lang/Object;
    :cond_8
    :goto_1
    return-void
.end method

.method public toggleSplitByGesture(Z)Z
    .locals 6
    .param p1, "isLTR"    # Z

    .line 1567
    const/4 v0, 0x0

    .line 1569
    .local v0, "triggered":Z
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScSupported()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 1570
    invoke-static {}, Lcom/android/server/wm/MiuiSplitInputMethodStub;->getInstance()Lcom/android/server/wm/MiuiSplitInputMethodStub;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/android/server/wm/MiuiSplitInputMethodStub;->updateImeVisiblityIfNeed(Z)V

    .line 1571
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiSoScManagerStub;->toggleSplitByGesture(Z)Z

    move-result v1

    return v1

    .line 1574
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isVerticalSplit()Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    .line 1576
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    if-nez v1, :cond_2

    return v0

    .line 1578
    :cond_2
    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    const-string v3, "ATMSImpl"

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 1579
    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getUiContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1578
    const-string v4, "multi_finger_slide"

    invoke-static {v1, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_4

    .line 1580
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    .line 1581
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayPolicy;->getStatusBar()Lcom/android/server/wm/WindowState;

    move-result-object v1

    .line 1582
    .local v1, "statusBar":Lcom/android/server/wm/WindowState;
    const/4 v4, 0x0

    .line 1583
    .local v4, "ignoreMultiFingerFlide":Z
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScSupported()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1584
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSingleModeActivated()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1585
    const/4 v4, 0x1

    .line 1586
    const-string v5, "Ignore MULTI_FINGER_SLIDE."

    invoke-static {v3, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1588
    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isVisible()Z

    move-result v5

    if-nez v5, :cond_4

    if-nez v4, :cond_4

    .line 1589
    const-string v2, "Ignore because MULTI_FINGER_SLIDE is setted and StatusBar is hidden."

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1590
    return v0

    .line 1594
    .end local v1    # "statusBar":Lcom/android/server/wm/WindowState;
    .end local v4    # "ignoreMultiFingerFlide":Z
    :cond_4
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v1}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardLocked()Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 1595
    invoke-interface {v1, v2}, Lcom/android/server/policy/WindowManagerPolicy;->okToAnimate(Z)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_1

    .line 1600
    :cond_5
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getLockTaskModeState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 1601
    return v0

    .line 1603
    :cond_6
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v1

    .line 1604
    invoke-virtual {v1}, Lcom/android/server/wm/TaskDisplayArea;->getFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 1605
    .local v1, "focusedRootTask":Lcom/android/server/wm/Task;
    if-nez v1, :cond_7

    .line 1606
    const-string v2, "Ignore because get none focused task."

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1607
    return v0

    .line 1609
    :cond_7
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->supportsMultiWindow()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1610
    const-string v2, "Ignore because the task not support SplitScreen mode."

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1611
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getTaskChangeNotificationController()Lcom/android/server/wm/TaskChangeNotificationController;

    move-result-object v2

    .line 1612
    invoke-virtual {v2}, Lcom/android/server/wm/TaskChangeNotificationController;->notifyActivityDismissingDockedRootTask()V

    .line 1613
    return v0

    .line 1615
    :cond_8
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1616
    const-string v2, "Ignore because the task at FreeForm mode."

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1617
    return v0

    .line 1623
    :cond_9
    invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1624
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->exitSplitScreen(Z)V

    goto :goto_0

    .line 1628
    :cond_a
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 1629
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1628
    const-string/jumbo v3, "three_gesture_dock_task"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Landroid/provider/MiuiSettings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1630
    .local v2, "threeGestureDockTask":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 1631
    iget-object v5, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v3, v4}, Landroid/provider/MiuiSettings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1634
    :cond_b
    if-eqz p1, :cond_c

    .line 1635
    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "gesture_left_to_right"

    invoke-static {v4, v3, v5}, Landroid/provider/MiuiSettings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 1638
    :cond_c
    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "gesture_right_to_left"

    invoke-static {v4, v3, v5}, Landroid/provider/MiuiSettings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1643
    .end local v2    # "threeGestureDockTask":Ljava/lang/String;
    :goto_0
    return v0

    .line 1596
    .end local v1    # "focusedRootTask":Lcom/android/server/wm/Task;
    :cond_d
    :goto_1
    const-string v1, "Ignore because keyguard is showing or turning screen off."

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1597
    return v0
.end method

.method public unSetPauseAdvancedInner(Z)V
    .locals 8
    .param p1, "resume"    # Z

    .line 1851
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAdvanceTaskIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1852
    return-void

    .line 1854
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAdvanceTaskIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1855
    .local v1, "taskId":I
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(I)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 1856
    .local v2, "task":Lcom/android/server/wm/Task;
    if-nez v2, :cond_1

    .line 1857
    goto :goto_0

    .line 1859
    :cond_1
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 1860
    .local v3, "topNonFinishingActivity":Lcom/android/server/wm/ActivityRecord;
    if-nez v3, :cond_2

    .line 1861
    goto :goto_0

    .line 1863
    :cond_2
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTaskFragment()Lcom/android/server/wm/TaskFragment;

    move-result-object v4

    .line 1864
    .local v4, "taskFragment":Lcom/android/server/wm/TaskFragment;
    const-string v5, "ATMSImpl"

    if-eqz v4, :cond_5

    .line 1865
    iget-object v6, v4, Lcom/android/server/wm/TaskFragment;->mEnterPipDuringPauseAdvance:Ljava/lang/Runnable;

    if-eqz v6, :cond_3

    .line 1866
    iget-object v6, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v6, v6, Lcom/android/server/wm/ActivityTaskManagerService;->mH:Lcom/android/server/wm/ActivityTaskManagerService$H;

    iget-object v7, v4, Lcom/android/server/wm/TaskFragment;->mEnterPipDuringPauseAdvance:Ljava/lang/Runnable;

    invoke-virtual {v6, v7}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 1868
    :cond_3
    const/4 v6, 0x0

    iput-boolean v6, v4, Lcom/android/server/wm/TaskFragment;->mPauseAdvance:Z

    .line 1869
    const/4 v6, 0x0

    iput-object v6, v4, Lcom/android/server/wm/TaskFragment;->mEnterPipDuringPauseAdvance:Ljava/lang/Runnable;

    .line 1870
    if-eqz p1, :cond_4

    .line 1871
    invoke-virtual {v2, v6, v6}, Lcom/android/server/wm/Task;->resumeTopActivityUncheckedLocked(Lcom/android/server/wm/ActivityRecord;Landroid/app/ActivityOptions;)Z

    .line 1873
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "unSetPauseAdvance: Task="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",resume="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1875
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "unsetPauseAdvance: Task "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " not found."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1877
    .end local v1    # "taskId":I
    .end local v2    # "task":Lcom/android/server/wm/Task;
    .end local v3    # "topNonFinishingActivity":Lcom/android/server/wm/ActivityRecord;
    .end local v4    # "taskFragment":Lcom/android/server/wm/TaskFragment;
    :goto_1
    goto/16 :goto_0

    .line 1878
    :cond_6
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAdvanceTaskIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1882
    return-void
.end method

.method public updateHomeIntent(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1922
    invoke-static {}, Landroid/appcompat/ApplicationCompatUtilsStub;->get()Landroid/appcompat/ApplicationCompatUtilsStub;

    move-result-object v0

    invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isDialogContinuityEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1924
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_provisioned"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 1926
    return-object p1

    .line 1929
    :cond_0
    invoke-static {}, Lcom/android/server/wm/ApplicationCompatRouterStub;->get()Lcom/android/server/wm/ApplicationCompatRouterStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ApplicationCompatRouterStub;->getConfigDisplayID()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1931
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.miui.fliphome"

    const-string v2, "com.miui.fliphome.FlipLauncher"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1934
    :cond_1
    return-object p1

    .line 1936
    :cond_2
    return-object p1
.end method

.method public updateResizeBlackList(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 756
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->updateMultiTaskWhiteList(Landroid/content/Context;)V

    .line 762
    :try_start_0
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    const-string/jumbo v2, "split_screen_applist"

    if-eqz v0, :cond_0

    .line 763
    nop

    .line 764
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "miui_resize_black_list_for_pad"

    .line 763
    invoke-static {v0, v2, v3, v1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .local v0, "data":Ljava/lang/String;
    goto :goto_0

    .line 766
    .end local v0    # "data":Ljava/lang/String;
    :cond_0
    const-string v0, "persist.sys.muiltdisplay_type"

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 767
    nop

    .line 768
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "miui_resize_black_list_for_fold"

    .line 767
    invoke-static {v0, v2, v3, v1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "data":Ljava/lang/String;
    goto :goto_0

    .line 771
    .end local v0    # "data":Ljava/lang/String;
    :cond_1
    nop

    .line 772
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "miui_resize_black_list"

    .line 771
    invoke-static {v0, v2, v3, v1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 775
    .restart local v0    # "data":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 776
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 777
    .local v1, "apps":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 778
    iget-object v3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mResizeBlackList:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 777
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 780
    .end local v2    # "i":I
    :cond_2
    return-void

    .line 784
    .end local v0    # "data":Ljava/lang/String;
    .end local v1    # "apps":Lorg/json/JSONArray;
    :cond_3
    goto :goto_2

    .line 782
    :catch_0
    move-exception v0

    .line 783
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ATMSImpl"

    const-string v2, "Get splitscreen blacklist from xml: "

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 785
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method public updateTopActivity(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 396
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-nez v0, :cond_0

    goto :goto_0

    .line 399
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->skipReportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 400
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->reportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)V

    .line 401
    return-void

    .line 397
    :cond_2
    :goto_0
    return-void
.end method

.method updateTopResumedActivity(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)V
    .locals 4
    .param p1, "prev"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "top"    # Lcom/android/server/wm/ActivityRecord;

    .line 509
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_3

    .line 512
    :cond_0
    iget-object v0, p2, Lcom/android/server/wm/ActivityRecord;->mTransitionController:Lcom/android/server/wm/TransitionController;

    .line 513
    .local v0, "tc":Lcom/android/server/wm/TransitionController;
    if-eqz v0, :cond_4

    invoke-virtual {v0, p1}, Lcom/android/server/wm/TransitionController;->isTransientLaunch(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/TransitionController;->isTransientHide(Lcom/android/server/wm/Task;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz p1, :cond_4

    .line 514
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isActivityTypeHomeOrRecents()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 515
    invoke-virtual {v0}, Lcom/android/server/wm/TransitionController;->getCollectingTransitionType()I

    move-result v1

    const v2, 0x7fffff9a

    if-ne v1, v2, :cond_2

    .line 516
    invoke-virtual {v0, p2}, Lcom/android/server/wm/TransitionController;->inCollectingTransition(Lcom/android/server/wm/WindowContainer;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/android/server/wm/TransitionController;->getCollectingTransition()Lcom/android/server/wm/Transition;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/wm/Transition;->mParticipants:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 517
    .local v1, "isTopEnterMini":Z
    :goto_0
    if-eqz v1, :cond_3

    .line 518
    const-string v2, "ATMSImpl"

    const-string/jumbo v3, "updateTopResumedActivity do not report freeform event"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    return-void

    .line 521
    :cond_3
    invoke-virtual {p0, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V

    .end local v1    # "isTopEnterMini":Z
    goto :goto_1

    .line 522
    :cond_4
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z

    move-result v1

    if-nez v1, :cond_5

    .line 523
    invoke-virtual {p0, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->updateTopActivity(Lcom/android/server/wm/ActivityRecord;)V

    goto :goto_2

    .line 522
    :cond_5
    :goto_1
    nop

    .line 525
    :goto_2
    return-void

    .line 510
    .end local v0    # "tc":Lcom/android/server/wm/TransitionController;
    :cond_6
    :goto_3
    return-void
.end method
