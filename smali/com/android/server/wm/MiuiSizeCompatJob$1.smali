.class Lcom/android/server/wm/MiuiSizeCompatJob$1;
.super Ljava/lang/Object;
.source "MiuiSizeCompatJob.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiSizeCompatJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatJob;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiSizeCompatJob;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiSizeCompatJob;

    .line 101
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 104
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmTrackLock(Lcom/android/server/wm/MiuiSizeCompatJob;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 105
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {p2}, Lcom/miui/analytics/ITrackBinder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fputmITrackBinder(Lcom/android/server/wm/MiuiSizeCompatJob;Lcom/miui/analytics/ITrackBinder;)V

    .line 106
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiSizeCompatJob;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 108
    :try_start_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiSizeCompatJob;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    invoke-interface {v1}, Lcom/miui/analytics/ITrackBinder;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatJob$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v2}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmDeathRecipient(Lcom/android/server/wm/MiuiSizeCompatJob;)Landroid/os/IBinder$DeathRecipient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    goto :goto_0

    .line 109
    :catch_0
    move-exception v1

    .line 110
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 113
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 114
    const-string v0, "MiuiSizeCompatJob"

    const-string v1, "Bind OneTrack service success."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    return-void

    .line 113
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 119
    const-string v0, "MiuiSizeCompatJob"

    const-string v1, "OneTrack service disconnected."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatJob$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fgetmTrackLock(Lcom/android/server/wm/MiuiSizeCompatJob;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatJob$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatJob;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiSizeCompatJob;->-$$Nest$fputmITrackBinder(Lcom/android/server/wm/MiuiSizeCompatJob;Lcom/miui/analytics/ITrackBinder;)V

    .line 122
    monitor-exit v0

    .line 123
    return-void

    .line 122
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
