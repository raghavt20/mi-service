class com.android.server.wm.FoldablePackagePolicy extends com.android.server.wm.PolicyImpl {
	 /* .source "FoldablePackagePolicy.java" */
	 /* # static fields */
	 private static final java.lang.String ACTIVITY_NAME;
	 private static final java.lang.String ACTIVITY_NAME_PRINT_WRITER;
	 private static final java.lang.String APP_CONTINUITY_BLACK_LIST_KEY;
	 private static final java.lang.String APP_CONTINUITY_ID_KEY;
	 private static final java.lang.String APP_CONTINUITY_WHILTE_LIST_KEY;
	 private static final java.lang.String APP_INTERCEPT_ALLOWLIST_KEY;
	 private static final java.lang.String APP_INTERCEPT_BLACKLIST_KEY;
	 private static final java.lang.String APP_INTERCEPT_COMPONENT_ALLOWLIST_KEY;
	 private static final java.lang.String APP_INTERCEPT_COMPONENT_BLACKLIST_KEY;
	 private static final java.lang.String APP_RELAUNCH_BLACKLIST_KEY;
	 private static final java.lang.String APP_RESTART_BLACKLIST_KEY;
	 private static final java.lang.String CALLBACK_NAME_DISPLAY_COMPAT;
	 private static final java.lang.String CALLBACK_NAME_FULLSCREEN;
	 private static final java.lang.String CALLBACK_NAME_FULLSCREEN_COMPONENT;
	 private static final java.lang.String COMMAND_OPTION_ALLOW_LIST;
	 private static final java.lang.String COMMAND_OPTION_BLOCK_LIST;
	 private static final java.lang.String COMMAND_OPTION_INTERCEPT_COMPONENT_LIST;
	 private static final java.lang.String COMMAND_OPTION_INTERCEPT_LIST;
	 private static final java.lang.String COMMAND_OPTION_RELAUNCH_LIST;
	 private static final java.lang.String COMMAND_OPTION_RESTART_LIST;
	 private static final Integer DEFAULT_CONTINUITY_VERSION;
	 private static final java.lang.String FORCE_RESIZABLE_ACTIVITIES;
	 private static final java.lang.String FULLSCREEN_DEMO_MODE;
	 public static final java.lang.String MIUI_SMART_ROTATION_ACTIVITY_KEY;
	 public static final java.lang.String MIUI_SMART_ROTATION_FULLSCREEN_KEY;
	 private static final java.lang.String PACKAGE_NAME;
	 private static final java.lang.String PACKAGE_NAME_PRINT_WRITER;
	 private static final java.lang.String POLICY_NAME;
	 public static final java.lang.String POLICY_VALUE_ALLOW_LIST;
	 public static final java.lang.String POLICY_VALUE_BLOCK_LIST;
	 public static final java.lang.String POLICY_VALUE_INTERCEPT_ALLOW_LIST;
	 public static final java.lang.String POLICY_VALUE_INTERCEPT_COMPONENT_ALLOW_LIST;
	 public static final java.lang.String POLICY_VALUE_INTERCEPT_COMPONENT_LIST;
	 public static final java.lang.String POLICY_VALUE_INTERCEPT_LIST;
	 public static final java.lang.String POLICY_VALUE_RELAUNCH_LIST;
	 public static final java.lang.String POLICY_VALUE_RESTART_LIST;
	 private static final java.lang.String PULL_APP_FLAG_COMMAND;
	 private static final java.lang.String PULL_CLOUD_DATA_MODULE_COMMAND;
	 private static final java.lang.String SET_FIXED_ASPECT_RATIO;
	 private static final java.lang.String SET_FIXED_ASPECT_RATIO_COMMAND;
	 private static final java.lang.String SET_FORCE_DISPLAY_COMPAT_COMMAND;
	 private static final java.lang.String SET_FULLSCREEN_COMPONENT_COMMAND;
	 private static final java.lang.String SET_FULLSCREEN_LANDSCAPE_COMMAND;
	 private static final java.lang.String SET_FULLSCREEN_LANDSCAPE_COMMAND_OLD;
	 private static final java.lang.String SET_FULLSCREEN_USER_COMMAND;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final com.android.server.wm.ActivityTaskManagerServiceImpl mAtmServiceImpl;
	 private final java.lang.String mCompatModeModuleName;
	 private final android.content.Context mContext;
	 private final java.lang.String mContinuityModuleName;
	 private Long mContinuityVersion;
	 private Boolean mDevelopmentForceResizable;
	 private final java.util.List mDisplayCompatAllowCloudAppList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final java.util.List mDisplayCompatBlockCloudAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mDisplayInterceptCloudAppAllowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mDisplayInterceptCloudAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mDisplayInterceptComponentCloudAppAllowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mDisplayInterceptComponentCloudAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mDisplayRelaunchCloudAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mDisplayRestartCloudAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mFullScreenActivityCloudList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mFullScreenPackageCloudAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.res.Resources mResources;
private final java.lang.String mSmartRotationNModuleName;
/* # direct methods */
 com.android.server.wm.FoldablePackagePolicy ( ) {
/* .locals 2 */
/* .param p1, "atmServiceImpl" # Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
/* .line 125 */
v0 = this.mPackageConfigurationController;
final String v1 = "FoldablePackagePolicy"; // const-string v1, "FoldablePackagePolicy"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/wm/PolicyImpl;-><init>(Lcom/android/server/wm/PackageConfigurationController;Ljava/lang/String;)V */
/* .line 78 */
/* const-wide/32 v0, 0x124f26 */
/* iput-wide v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityVersion:J */
/* .line 127 */
this.mAtmServiceImpl = p1;
/* .line 128 */
v0 = this.mContext;
this.mContext = v0;
/* .line 129 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
this.mResources = v0;
/* .line 133 */
/* const v1, 0x110f00b6 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
this.mContinuityModuleName = v1;
/* .line 134 */
/* const v1, 0x110f0359 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
this.mCompatModeModuleName = v1;
/* .line 135 */
/* const v1, 0x110f036d */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
this.mSmartRotationNModuleName = v0;
/* .line 137 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 138 */
v0 = this.mPackageSettingsManager;
v0 = this.mDisplayCompatPackages;
v0 = this.mCallback;
final String v1 = "displayCompat"; // const-string v1, "displayCompat"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).registerCallback ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/FoldablePackagePolicy;->registerCallback(Ljava/lang/String;Ljava/util/function/Consumer;)V
/* .line 145 */
v0 = this.mFullScreenPackageManager;
v0 = this.mFullScreenPackagelistCallback;
final String v1 = "fullscreenpackage"; // const-string v1, "fullscreenpackage"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).registerCallback ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/FoldablePackagePolicy;->registerCallback(Ljava/lang/String;Ljava/util/function/Consumer;)V
/* .line 146 */
v0 = this.mFullScreenPackageManager;
v0 = this.mFullScreenComponentCallback;
final String v1 = "fullscreencomponent"; // const-string v1, "fullscreencomponent"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).registerCallback ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/FoldablePackagePolicy;->registerCallback(Ljava/lang/String;Ljava/util/function/Consumer;)V
/* .line 151 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDisplayCompatAllowCloudAppList = v0;
/* .line 152 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDisplayCompatBlockCloudAppList = v0;
/* .line 153 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDisplayRestartCloudAppList = v0;
/* .line 154 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDisplayInterceptCloudAppList = v0;
/* .line 155 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDisplayInterceptComponentCloudAppList = v0;
/* .line 156 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDisplayInterceptCloudAppAllowList = v0;
/* .line 157 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDisplayInterceptComponentCloudAppAllowList = v0;
/* .line 158 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDisplayRelaunchCloudAppList = v0;
/* .line 161 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mFullScreenPackageCloudAppList = v0;
/* .line 162 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mFullScreenActivityCloudList = v0;
/* .line 164 */
return;
} // .end method
private void executeShellCommandLockedForSmartRotation ( java.lang.String p0, java.lang.String[] p1, java.io.PrintWriter p2 ) {
/* .locals 11 */
/* .param p1, "command" # Ljava/lang/String; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 265 */
v0 = com.android.server.wm.MiuiOrientationStub .get ( );
/* if-nez v0, :cond_0 */
/* .line 266 */
final String v0 = " miui smart rotation is not enabled."; // const-string v0, " miui smart rotation is not enabled."
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 267 */
return;
/* .line 269 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
/* .line 270 */
/* .local v0, "str":Ljava/lang/String; */
/* array-length v1, p2 */
final String v2 = "demoMode"; // const-string v2, "demoMode"
int v3 = 2; // const/4 v3, 0x2
final String v4 = "-setFullscreenUser"; // const-string v4, "-setFullscreenUser"
final String v5 = "-setFullscreenActivity"; // const-string v5, "-setFullscreenActivity"
int v6 = 1; // const/4 v6, 0x1
final String v7 = "-setFullscreenPackage"; // const-string v7, "-setFullscreenPackage"
/* if-eq v1, v6, :cond_1 */
/* array-length v1, p2 */
/* if-ne v1, v3, :cond_2 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* aget-object v8, p2, v1 */
/* if-nez v8, :cond_3 */
/* .line 271 */
} // :cond_2
final String v1 = "miui smart rotation(package)"; // const-string v1, "miui smart rotation(package)"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printOptionsRequires ( p3, v1 ); // invoke-virtual {p0, p3, v1}, Lcom/android/server/wm/FoldablePackagePolicy;->printOptionsRequires(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 272 */
final String v1 = "policy(optional)"; // const-string v1, "policy(optional)"
/* filled-new-array {v2, v1}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v7, v2 ); // invoke-virtual {p0, p3, v7, v2}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 273 */
final String v2 = "packageName"; // const-string v2, "packageName"
/* filled-new-array {v2, v1}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v7, v3 ); // invoke-virtual {p0, p3, v7, v3}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 274 */
final String v3 = "--remove"; // const-string v3, "--remove"
/* filled-new-array {v2, v3}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v7, v6 ); // invoke-virtual {p0, p3, v7, v6}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 275 */
final String v6 = "packageName:packageName:..."; // const-string v6, "packageName:packageName:..."
/* filled-new-array {v6, v1}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v7, v8 ); // invoke-virtual {p0, p3, v7, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 276 */
final String v7 = " or "; // const-string v7, " or "
(( java.io.PrintWriter ) p3 ).println ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 277 */
final String v8 = "miui smart rotation(activity)"; // const-string v8, "miui smart rotation(activity)"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printOptionsRequires ( p3, v8 ); // invoke-virtual {p0, p3, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->printOptionsRequires(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 278 */
final String v8 = "activity"; // const-string v8, "activity"
final String v9 = "policy(forced)"; // const-string v9, "policy(forced)"
/* filled-new-array {v8, v9}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v5, v10 ); // invoke-virtual {p0, p3, v5, v10}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 279 */
/* filled-new-array {v8, v3}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v5, v8 ); // invoke-virtual {p0, p3, v5, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 280 */
final String v8 = "activity:activity:..."; // const-string v8, "activity:activity:..."
/* filled-new-array {v8, v9}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v5, v8 ); // invoke-virtual {p0, p3, v5, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 281 */
(( java.io.PrintWriter ) p3 ).println ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 282 */
final String v5 = "miui smart rotation(user)"; // const-string v5, "miui smart rotation(user)"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printOptionsRequires ( p3, v5 ); // invoke-virtual {p0, p3, v5}, Lcom/android/server/wm/FoldablePackagePolicy;->printOptionsRequires(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 283 */
/* filled-new-array {v2, v1}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v4, v5 ); // invoke-virtual {p0, p3, v4, v5}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 284 */
/* filled-new-array {v2, v3}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v4, v2 ); // invoke-virtual {p0, p3, v4, v2}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 285 */
/* filled-new-array {v6, v1}, [Ljava/lang/String; */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).printCommandHelp ( p3, v4, v1 ); // invoke-virtual {p0, p3, v4, v1}, Lcom/android/server/wm/FoldablePackagePolicy;->printCommandHelp(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/String;)V
/* .line 286 */
/* const-string/jumbo v1, "supported policy options is nr:r:ri:t:w:b:nra:nrs" */
(( java.io.PrintWriter ) p3 ).println ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 287 */
final String v1 = "please refer https://xiaomi.f.mioffice.cn/docs/dock4JoODrG0prOdqdZ14fWZ17c for more information"; // const-string v1, "please refer https://xiaomi.f.mioffice.cn/docs/dock4JoODrG0prOdqdZ14fWZ17c for more information"
(( java.io.PrintWriter ) p3 ).println ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* goto/16 :goto_2 */
/* .line 289 */
} // :cond_3
/* array-length v8, p2 */
/* if-ne v8, v3, :cond_4 */
/* aget-object v8, p2, v6 */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 290 */
/* aget-object v0, p2, v6 */
/* .line 292 */
} // :cond_4
/* aget-object v8, p2, v1 */
v2 = (( java.lang.String ) v2 ).equals ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 298 */
com.android.server.wm.MiuiOrientationImpl .getInstance ( );
(( com.android.server.wm.MiuiOrientationImpl ) v1 ).gotoDemoDebugMode ( p3, v0 ); // invoke-virtual {v1, p3, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->gotoDemoDebugMode(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 301 */
} // :cond_5
v2 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_6
/* :sswitch_0 */
v2 = (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
int v3 = 3; // const/4 v3, 0x3
/* :sswitch_1 */
v2 = (( java.lang.String ) p1 ).equals ( v5 ); // invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* :sswitch_2 */
v2 = (( java.lang.String ) p1 ).equals ( v7 ); // invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* move v3, v1 */
/* :sswitch_3 */
final String v2 = "-setFullscreenlandscape"; // const-string v2, "-setFullscreenlandscape"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* move v3, v6 */
} // :goto_0
int v3 = -1; // const/4 v3, -0x1
} // :goto_1
final String v2 = ":"; // const-string v2, ":"
/* packed-switch v3, :pswitch_data_0 */
/* .line 314 */
/* :pswitch_0 */
com.android.server.wm.MiuiOrientationImpl .getInstance ( );
/* aget-object v1, p2, v1 */
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
(( com.android.server.wm.MiuiOrientationImpl ) v3 ).setUserSettings ( p3, v1, v0 ); // invoke-virtual {v3, p3, v1, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->setUserSettings(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;)V
/* .line 316 */
/* .line 307 */
/* :pswitch_1 */
/* aget-object v1, p2, v1 */
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
final String v2 = "fullscreencomponent"; // const-string v2, "fullscreencomponent"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).executeDebugModeLocked ( p3, v1, v0, v2 ); // invoke-virtual {p0, p3, v1, v0, v2}, Lcom/android/server/wm/FoldablePackagePolicy;->executeDebugModeLocked(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
/* .line 308 */
/* .line 304 */
/* :pswitch_2 */
/* aget-object v1, p2, v1 */
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
final String v2 = "fullscreenpackage"; // const-string v2, "fullscreenpackage"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).executeDebugModeLocked ( p3, v1, v0, v2 ); // invoke-virtual {p0, p3, v1, v0, v2}, Lcom/android/server/wm/FoldablePackagePolicy;->executeDebugModeLocked(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
/* .line 305 */
/* nop */
/* .line 323 */
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x543cf9f5 -> :sswitch_3 */
/* -0xa988c8a -> :sswitch_2 */
/* 0x281199df -> :sswitch_1 */
/* 0x3b007a9b -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
Boolean executeShellCommandLocked ( java.lang.String p0, java.lang.String[] p1, java.io.PrintWriter p2 ) {
/* .locals 7 */
/* .param p1, "command" # Ljava/lang/String; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 227 */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v0 = "-pullCloudDataModule"; // const-string v0, "-pullCloudDataModule"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* :sswitch_1 */
final String v0 = "-setFullscreenUser"; // const-string v0, "-setFullscreenUser"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* :sswitch_2 */
final String v0 = "-setFullscreenActivity"; // const-string v0, "-setFullscreenActivity"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_3 */
final String v0 = "-setFullscreenPackage"; // const-string v0, "-setFullscreenPackage"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
/* :sswitch_4 */
final String v0 = "-pullAppFlag"; // const-string v0, "-pullAppFlag"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 5; // const/4 v0, 0x5
/* :sswitch_5 */
final String v0 = "-setFullscreenlandscape"; // const-string v0, "-setFullscreenlandscape"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 260 */
/* .line 241 */
/* :pswitch_0 */
v0 = android.appcompat.ApplicationCompatUtilsStub.MIUI_SUPPORT_APP_CONTINUITY;
/* aget-object v3, p2, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v3 = " = "; // const-string v3, " = "
final String v4 = " not added!"; // const-string v4, " not added!"
/* if-nez v0, :cond_1 */
/* aget-object v0, p2, v2 */
/* .line 242 */
final String v5 = "android.supports_size_changes"; // const-string v5, "android.supports_size_changes"
v0 = (( java.lang.String ) v5 ).equals ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 243 */
} // :cond_1
v0 = this.mAtmServiceImpl;
/* aget-object v5, p2, v1 */
/* aget-object v6, p2, v2 */
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).hasMetaData ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->hasMetaData(Ljava/lang/String;Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 244 */
v0 = this.mAtmServiceImpl;
/* aget-object v5, p2, v1 */
/* aget-object v6, p2, v2 */
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).getMetaDataBoolean ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 245 */
/* .local v0, "continuityFlag":Z */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* aget-object v6, p2, v2 */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v5 ); // invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 246 */
} // .end local v0 # "continuityFlag":Z
/* .line 247 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* aget-object v5, p2, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 251 */
} // :cond_3
} // :goto_2
final String v0 = "android.max_aspect"; // const-string v0, "android.max_aspect"
/* aget-object v5, p2, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 252 */
v0 = this.mAtmServiceImpl;
/* aget-object v5, p2, v1 */
/* aget-object v6, p2, v2 */
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).hasMetaData ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->hasMetaData(Ljava/lang/String;Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 253 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* aget-object v4, p2, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAtmServiceImpl;
/* aget-object v1, p2, v1 */
/* aget-object v4, p2, v2 */
v1 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v3 ).getMetaDataFloat ( v1, v4 ); // invoke-virtual {v3, v1, v4}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataFloat(Ljava/lang/String;Ljava/lang/String;)F
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 255 */
} // :cond_4
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* aget-object v1, p2, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 258 */
} // :cond_5
} // :goto_3
/* .line 236 */
/* :pswitch_1 */
v0 = this.mContext;
/* aget-object v1, p2, v1 */
/* aget-object v3, p2, v2 */
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v0, v1, v3 ); // invoke-virtual {p0, v0, v1, v3}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 237 */
/* .line 232 */
/* :pswitch_2 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/FoldablePackagePolicy;->executeShellCommandLockedForSmartRotation(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)V */
/* .line 233 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x543cf9f5 -> :sswitch_5 */
/* -0x4869b725 -> :sswitch_4 */
/* -0xa988c8a -> :sswitch_3 */
/* 0x281199df -> :sswitch_2 */
/* 0x3b007a9b -> :sswitch_1 */
/* 0x58969c79 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public java.util.List getContinuityList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "policyName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 488 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 489 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 492 */
} // :cond_0
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v0 = "app_intercept_component_allowlist"; // const-string v0, "app_intercept_component_allowlist"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 7; // const/4 v0, 0x7
/* :sswitch_1 */
final String v0 = "app_continuity_blacklist"; // const-string v0, "app_continuity_blacklist"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_2 */
final String v0 = "app_intercept_blacklist"; // const-string v0, "app_intercept_blacklist"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 3; // const/4 v0, 0x3
/* :sswitch_3 */
final String v0 = "app_restart_blacklist"; // const-string v0, "app_restart_blacklist"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_4 */
final String v0 = "app_relaunch_blacklist"; // const-string v0, "app_relaunch_blacklist"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 5; // const/4 v0, 0x5
/* :sswitch_5 */
final String v0 = "app_intercept_allowlist"; // const-string v0, "app_intercept_allowlist"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 6; // const/4 v0, 0x6
/* :sswitch_6 */
final String v0 = "app_continuity_whitelist"; // const-string v0, "app_continuity_whitelist"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_7 */
final String v0 = "app_intercept_component_blacklist"; // const-string v0, "app_intercept_component_blacklist"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 4; // const/4 v0, 0x4
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 510 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 508 */
/* :pswitch_0 */
v0 = this.mDisplayInterceptComponentCloudAppAllowList;
/* .line 506 */
/* :pswitch_1 */
v0 = this.mDisplayInterceptCloudAppAllowList;
/* .line 504 */
/* :pswitch_2 */
v0 = this.mDisplayRelaunchCloudAppList;
/* .line 502 */
/* :pswitch_3 */
v0 = this.mDisplayInterceptComponentCloudAppList;
/* .line 500 */
/* :pswitch_4 */
v0 = this.mDisplayInterceptCloudAppList;
/* .line 498 */
/* :pswitch_5 */
v0 = this.mDisplayRestartCloudAppList;
/* .line 496 */
/* :pswitch_6 */
v0 = this.mDisplayCompatBlockCloudAppList;
/* .line 494 */
/* :pswitch_7 */
v0 = this.mDisplayCompatAllowCloudAppList;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7487e440 -> :sswitch_7 */
/* -0x71d7e56a -> :sswitch_6 */
/* -0x2aa53af4 -> :sswitch_5 */
/* -0x25be055e -> :sswitch_4 */
/* -0x1b36c7b1 -> :sswitch_3 */
/* 0xe9766c2 -> :sswitch_2 */
/* 0x4030036c -> :sswitch_1 */
/* 0x523b7a0a -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
Long getContinuityVersion ( ) {
/* .locals 2 */
/* .line 515 */
/* iget-wide v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityVersion:J */
/* return-wide v0 */
} // .end method
java.util.List getListFromCloud ( android.content.Context p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "moduleName" # Ljava/lang/String; */
/* .param p3, "key" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 168 */
final String v0 = "FoldablePackagePolicy"; // const-string v0, "FoldablePackagePolicy"
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 169 */
/* .local v1, "arrayList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = android.text.TextUtils .isEmpty ( p2 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 170 */
/* .line 173 */
} // :cond_0
/* nop */
/* .line 174 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 173 */
int v3 = 0; // const/4 v3, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v2,p2,p3,v3 );
/* .line 175 */
/* .local v2, "data":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getListFromCloud: data: "; // const-string v4, "getListFromCloud: data: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " moduleName="; // const-string v4, " moduleName="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " key="; // const-string v4, " key="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 177 */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 178 */
/* .line 180 */
} // :cond_1
/* new-instance v3, Lorg/json/JSONArray; */
/* invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 181 */
/* .local v3, "apps":Lorg/json/JSONArray; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
/* if-ge v4, v5, :cond_2 */
/* .line 182 */
(( org.json.JSONArray ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 181 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 184 */
} // .end local v4 # "i":I
} // :cond_2
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getListFromCloud: mCloudList: "; // const-string v5, "getListFromCloud: mCloudList: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 187 */
/* nop */
} // .end local v2 # "data":Ljava/lang/String;
} // .end local v3 # "apps":Lorg/json/JSONArray;
/* .line 185 */
/* :catch_0 */
/* move-exception v2 */
/* .line 186 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v3 = "exception when getListFromCloud: "; // const-string v3, "exception when getListFromCloud: "
android.util.Slog .e ( v0,v3,v2 );
/* .line 188 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_1
} // .end method
Integer getLocalVersion ( ) {
/* .locals 2 */
/* .line 327 */
v0 = this.mResources;
/* const v1, 0x110b0032 */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
} // .end method
java.util.Map getMapFromCloud ( android.content.Context p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "key" # Ljava/lang/String; */
/* .param p3, "jsonObjectName" # Ljava/lang/String; */
/* .param p4, "jsonArrayName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 196 */
final String v0 = "FoldablePackagePolicy"; // const-string v0, "FoldablePackagePolicy"
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 198 */
/* .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;" */
/* nop */
/* .line 199 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 198 */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v2,p2,p2,v3,v4 );
/* .line 200 */
/* .local v2, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getMayFromCloud: data: "; // const-string v4, "getMayFromCloud: data: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 201 */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 202 */
/* .local v3, "activityList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez v2, :cond_0 */
/* .line 203 */
/* .line 205 */
} // :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v2 ).json ( ); // invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v4 ).getJSONArray ( p2 ); // invoke-virtual {v4, p2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 206 */
/* .local v4, "apps":Lorg/json/JSONArray; */
/* if-nez v4, :cond_1 */
/* .line 207 */
/* .line 209 */
} // :cond_1
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
v6 = (( org.json.JSONArray ) v4 ).length ( ); // invoke-virtual {v4}, Lorg/json/JSONArray;->length()I
/* if-ge v5, v6, :cond_3 */
/* .line 210 */
/* .line 211 */
(( org.json.JSONArray ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 212 */
/* .local v6, "jsonObj":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v6 ).getJSONArray ( p4 ); // invoke-virtual {v6, p4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 213 */
/* .local v7, "array":Lorg/json/JSONArray; */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "j":I */
} // :goto_1
v9 = (( org.json.JSONArray ) v7 ).length ( ); // invoke-virtual {v7}, Lorg/json/JSONArray;->length()I
/* if-ge v8, v9, :cond_2 */
/* .line 214 */
(( org.json.JSONArray ) v7 ).getString ( v8 ); // invoke-virtual {v7, v8}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 213 */
/* add-int/lit8 v8, v8, 0x1 */
/* .line 216 */
} // .end local v8 # "j":I
} // :cond_2
(( org.json.JSONObject ) v6 ).getString ( p3 ); // invoke-virtual {v6, p3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 209 */
/* nop */
} // .end local v6 # "jsonObj":Lorg/json/JSONObject;
} // .end local v7 # "array":Lorg/json/JSONArray;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 218 */
} // .end local v5 # "i":I
} // :cond_3
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "getMayFromCloud: mCloudList: "; // const-string v6, "getMayFromCloud: mCloudList: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v5 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 221 */
/* nop */
} // .end local v2 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v3 # "activityList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v4 # "apps":Lorg/json/JSONArray;
/* .line 219 */
/* :catch_0 */
/* move-exception v2 */
/* .line 220 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v3 = "exception when getMayFromCloud: "; // const-string v3, "exception when getMayFromCloud: "
android.util.Slog .e ( v0,v3,v2 );
/* .line 222 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_2
} // .end method
void getPolicyDataMapFromCloud ( java.lang.String p0, java.util.concurrent.ConcurrentHashMap p1 ) {
/* .locals 10 */
/* .param p1, "configurationName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 367 */
/* .local p2, "outPolicyDataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;" */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v1 = 2; // const/4 v1, 0x2
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v0 = "displayCompat"; // const-string v0, "displayCompat"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v3 */
/* :sswitch_1 */
final String v0 = "fullscreencomponent"; // const-string v0, "fullscreencomponent"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
/* :sswitch_2 */
final String v0 = "fullscreenpackage"; // const-string v0, "fullscreenpackage"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
final String v4 = ""; // const-string v4, ""
final String v5 = ","; // const-string v5, ","
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_f */
/* .line 463 */
/* :pswitch_0 */
v0 = this.mFullScreenActivityCloudList;
v6 = this.mContext;
v7 = this.mSmartRotationNModuleName;
/* .line 464 */
final String v8 = "fullscreen_activity_list"; // const-string v8, "fullscreen_activity_list"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v6, v7, v8 ); // invoke-virtual {p0, v6, v7, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 463 */
/* .line 465 */
v0 = this.mFullScreenActivityCloudList;
v0 = if ( v0 != null) { // if-eqz v0, :cond_d
/* if-nez v0, :cond_d */
/* .line 466 */
(( java.util.concurrent.ConcurrentHashMap ) p2 ).clear ( ); // invoke-virtual {p2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 467 */
v0 = this.mFullScreenActivityCloudList;
/* new-array v6, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 468 */
/* .local v0, "packages":[Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "item":I */
} // :goto_2
/* array-length v7, v0 */
/* if-ge v6, v7, :cond_2 */
/* .line 469 */
/* aget-object v7, v0, v6 */
(( java.lang.String ) v7 ).split ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 470 */
/* .local v7, "split":[Ljava/lang/String; */
/* array-length v8, v7 */
/* if-ne v8, v1, :cond_1 */
/* aget-object v8, v7, v2 */
} // :cond_1
/* move-object v8, v4 */
/* .line 471 */
/* .local v8, "str":Ljava/lang/String; */
} // :goto_3
/* aget-object v9, v7, v3 */
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v9, v8 ); // invoke-virtual {p2, v9, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 468 */
} // .end local v7 # "split":[Ljava/lang/String;
} // .end local v8 # "str":Ljava/lang/String;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 473 */
} // .end local v0 # "packages":[Ljava/lang/String;
} // .end local v6 # "item":I
} // :cond_2
/* goto/16 :goto_f */
/* .line 450 */
/* :pswitch_1 */
v0 = this.mFullScreenPackageCloudAppList;
v6 = this.mContext;
v7 = this.mSmartRotationNModuleName;
/* .line 451 */
final String v8 = "fullscreen_landscape_list"; // const-string v8, "fullscreen_landscape_list"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v6, v7, v8 ); // invoke-virtual {p0, v6, v7, v8}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 450 */
/* .line 452 */
v0 = v0 = this.mFullScreenPackageCloudAppList;
/* if-nez v0, :cond_d */
/* .line 453 */
(( java.util.concurrent.ConcurrentHashMap ) p2 ).clear ( ); // invoke-virtual {p2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 454 */
v0 = this.mFullScreenPackageCloudAppList;
/* new-array v6, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 455 */
/* .restart local v0 # "packages":[Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .restart local v6 # "item":I */
} // :goto_4
/* array-length v7, v0 */
/* if-ge v6, v7, :cond_4 */
/* .line 456 */
/* aget-object v7, v0, v6 */
(( java.lang.String ) v7 ).split ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 457 */
/* .restart local v7 # "split":[Ljava/lang/String; */
/* array-length v8, v7 */
/* if-ne v8, v1, :cond_3 */
/* aget-object v8, v7, v2 */
} // :cond_3
/* move-object v8, v4 */
/* .line 458 */
/* .restart local v8 # "str":Ljava/lang/String; */
} // :goto_5
/* aget-object v9, v7, v3 */
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v9, v8 ); // invoke-virtual {p2, v9, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 455 */
} // .end local v7 # "split":[Ljava/lang/String;
} // .end local v8 # "str":Ljava/lang/String;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 460 */
} // .end local v0 # "packages":[Ljava/lang/String;
} // .end local v6 # "item":I
} // :cond_4
/* goto/16 :goto_f */
/* .line 371 */
/* :pswitch_2 */
try { // :try_start_0
v0 = this.mContext;
/* .line 372 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mContinuityModuleName;
final String v2 = "id"; // const-string v2, "id"
/* .line 371 */
v0 = android.provider.MiuiSettings$SettingsCloudData .getCloudDataInt ( v0,v1,v2,v3 );
/* int-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/wm/FoldablePackagePolicy;->mContinuityVersion:J */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 375 */
/* .line 373 */
/* :catch_0 */
/* move-exception v0 */
/* .line 374 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "FoldablePackagePolicy"; // const-string v1, "FoldablePackagePolicy"
final String v2 = "Something is wrong."; // const-string v2, "Something is wrong."
android.util.Slog .w ( v1,v2,v0 );
/* .line 377 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_6
v0 = this.mDisplayCompatAllowCloudAppList;
v1 = this.mContext;
v2 = this.mContinuityModuleName;
/* .line 378 */
final String v4 = "app_continuity_whitelist"; // const-string v4, "app_continuity_whitelist"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v1, v2, v4 ); // invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 377 */
/* .line 379 */
v0 = v0 = this.mDisplayCompatAllowCloudAppList;
/* if-nez v0, :cond_5 */
/* .line 380 */
v0 = this.mDisplayCompatAllowCloudAppList;
/* new-array v1, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 381 */
/* .local v0, "allowPackages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "item":I */
} // :goto_7
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_5 */
/* .line 382 */
/* aget-object v2, v0, v1 */
/* const-string/jumbo v4, "w" */
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v2, v4 ); // invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 381 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 386 */
} // .end local v0 # "allowPackages":[Ljava/lang/String;
} // .end local v1 # "item":I
} // :cond_5
v0 = this.mDisplayCompatBlockCloudAppList;
v1 = this.mContext;
v2 = this.mContinuityModuleName;
/* .line 387 */
final String v4 = "app_continuity_blacklist"; // const-string v4, "app_continuity_blacklist"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v1, v2, v4 ); // invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 386 */
/* .line 388 */
v0 = v0 = this.mDisplayCompatBlockCloudAppList;
/* if-nez v0, :cond_6 */
/* .line 389 */
v0 = this.mDisplayCompatBlockCloudAppList;
/* new-array v1, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 390 */
/* .local v0, "blockPackages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "item":I */
} // :goto_8
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_6 */
/* .line 391 */
/* aget-object v2, v0, v1 */
final String v4 = "b"; // const-string v4, "b"
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v2, v4 ); // invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 390 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 395 */
} // .end local v0 # "blockPackages":[Ljava/lang/String;
} // .end local v1 # "item":I
} // :cond_6
v0 = this.mDisplayRestartCloudAppList;
v1 = this.mContext;
v2 = this.mContinuityModuleName;
/* .line 396 */
final String v4 = "app_restart_blacklist"; // const-string v4, "app_restart_blacklist"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v1, v2, v4 ); // invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 395 */
/* .line 397 */
v0 = v0 = this.mDisplayRestartCloudAppList;
/* if-nez v0, :cond_7 */
/* .line 398 */
v0 = this.mDisplayRestartCloudAppList;
/* new-array v1, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 399 */
/* .restart local v0 # "blockPackages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "item":I */
} // :goto_9
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_7 */
/* .line 400 */
/* aget-object v2, v0, v1 */
final String v4 = "r"; // const-string v4, "r"
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v2, v4 ); // invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 399 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 404 */
} // .end local v0 # "blockPackages":[Ljava/lang/String;
} // .end local v1 # "item":I
} // :cond_7
v0 = this.mDisplayInterceptCloudAppList;
v1 = this.mContext;
v2 = this.mContinuityModuleName;
/* .line 405 */
final String v4 = "app_intercept_blacklist"; // const-string v4, "app_intercept_blacklist"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v1, v2, v4 ); // invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 404 */
/* .line 406 */
v0 = v0 = this.mDisplayInterceptCloudAppList;
/* if-nez v0, :cond_8 */
/* .line 407 */
v0 = this.mDisplayInterceptCloudAppList;
/* new-array v1, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 408 */
/* .restart local v0 # "blockPackages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "item":I */
} // :goto_a
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_8 */
/* .line 409 */
/* aget-object v2, v0, v1 */
final String v4 = "i"; // const-string v4, "i"
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v2, v4 ); // invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 408 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 413 */
} // .end local v0 # "blockPackages":[Ljava/lang/String;
} // .end local v1 # "item":I
} // :cond_8
v0 = this.mDisplayInterceptComponentCloudAppList;
v1 = this.mContext;
v2 = this.mContinuityModuleName;
/* .line 414 */
final String v4 = "app_intercept_component_blacklist"; // const-string v4, "app_intercept_component_blacklist"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v1, v2, v4 ); // invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 413 */
/* .line 415 */
v0 = v0 = this.mDisplayInterceptComponentCloudAppList;
/* if-nez v0, :cond_9 */
/* .line 416 */
v0 = this.mDisplayInterceptComponentCloudAppList;
/* new-array v1, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 417 */
/* .restart local v0 # "blockPackages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "item":I */
} // :goto_b
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_9 */
/* .line 418 */
/* aget-object v2, v0, v1 */
final String v4 = "ic"; // const-string v4, "ic"
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v2, v4 ); // invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 417 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 422 */
} // .end local v0 # "blockPackages":[Ljava/lang/String;
} // .end local v1 # "item":I
} // :cond_9
v0 = this.mDisplayInterceptCloudAppAllowList;
v1 = this.mContext;
v2 = this.mContinuityModuleName;
/* .line 423 */
final String v4 = "app_intercept_allowlist"; // const-string v4, "app_intercept_allowlist"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v1, v2, v4 ); // invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 422 */
/* .line 424 */
v0 = v0 = this.mDisplayInterceptCloudAppAllowList;
/* if-nez v0, :cond_a */
/* .line 425 */
v0 = this.mDisplayInterceptCloudAppAllowList;
/* new-array v1, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 426 */
/* .restart local v0 # "blockPackages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "item":I */
} // :goto_c
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_a */
/* .line 427 */
/* aget-object v2, v0, v1 */
final String v4 = "ia"; // const-string v4, "ia"
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v2, v4 ); // invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 426 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 431 */
} // .end local v0 # "blockPackages":[Ljava/lang/String;
} // .end local v1 # "item":I
} // :cond_a
v0 = this.mDisplayInterceptComponentCloudAppAllowList;
v1 = this.mContext;
v2 = this.mContinuityModuleName;
/* .line 432 */
final String v4 = "app_intercept_component_allowlist"; // const-string v4, "app_intercept_component_allowlist"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v1, v2, v4 ); // invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 431 */
/* .line 433 */
v0 = v0 = this.mDisplayInterceptComponentCloudAppAllowList;
/* if-nez v0, :cond_b */
/* .line 434 */
v0 = this.mDisplayInterceptComponentCloudAppAllowList;
/* new-array v1, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 435 */
/* .restart local v0 # "blockPackages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "item":I */
} // :goto_d
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_b */
/* .line 436 */
/* aget-object v2, v0, v1 */
final String v4 = "ica"; // const-string v4, "ica"
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v2, v4 ); // invoke-virtual {p2, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 435 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 440 */
} // .end local v0 # "blockPackages":[Ljava/lang/String;
} // .end local v1 # "item":I
} // :cond_b
v0 = this.mDisplayRelaunchCloudAppList;
v1 = this.mContext;
v2 = this.mContinuityModuleName;
/* .line 441 */
final String v4 = "app_relaunch_blacklist"; // const-string v4, "app_relaunch_blacklist"
(( com.android.server.wm.FoldablePackagePolicy ) p0 ).getListFromCloud ( v1, v2, v4 ); // invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/wm/FoldablePackagePolicy;->getListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
/* .line 440 */
/* .line 442 */
v0 = v0 = this.mDisplayRelaunchCloudAppList;
/* if-nez v0, :cond_d */
/* .line 443 */
v0 = this.mDisplayRelaunchCloudAppList;
/* new-array v1, v3, [Ljava/lang/String; */
/* check-cast v0, [Ljava/lang/String; */
/* .line 444 */
/* .restart local v0 # "blockPackages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "item":I */
} // :goto_e
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_c */
/* .line 445 */
/* aget-object v2, v0, v1 */
final String v3 = "c"; // const-string v3, "c"
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v2, v3 ); // invoke-virtual {p2, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 444 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 447 */
} // .end local v0 # "blockPackages":[Ljava/lang/String;
} // .end local v1 # "item":I
} // :cond_c
/* nop */
/* .line 479 */
} // :cond_d
} // :goto_f
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2c81202b -> :sswitch_2 */
/* 0x51dfa3a2 -> :sswitch_1 */
/* 0x78901ee4 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
void getPolicyDataMapFromLocal ( java.lang.String p0, java.util.concurrent.ConcurrentHashMap p1 ) {
/* .locals 10 */
/* .param p1, "configurationName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 332 */
/* .local p2, "outPolicyDataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;" */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v1 = 0; // const/4 v1, 0x0
int v2 = 2; // const/4 v2, 0x2
int v3 = 1; // const/4 v3, 0x1
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v0 = "displayCompat"; // const-string v0, "displayCompat"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
/* :sswitch_1 */
final String v0 = "fullscreencomponent"; // const-string v0, "fullscreencomponent"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
/* :sswitch_2 */
final String v0 = "fullscreenpackage"; // const-string v0, "fullscreenpackage"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v3 */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
final String v4 = ""; // const-string v4, ""
final String v5 = ","; // const-string v5, ","
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_8 */
/* .line 352 */
/* :pswitch_0 */
v0 = this.mResources;
/* const v6, 0x1103007d */
(( android.content.res.Resources ) v0 ).getStringArray ( v6 ); // invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 353 */
/* .local v0, "packages":[Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_2
/* array-length v7, v0 */
/* if-ge v6, v7, :cond_2 */
/* .line 354 */
/* aget-object v7, v0, v6 */
(( java.lang.String ) v7 ).split ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 355 */
/* .local v7, "split":[Ljava/lang/String; */
/* array-length v8, v7 */
/* if-ne v8, v2, :cond_1 */
/* aget-object v8, v7, v3 */
} // :cond_1
/* move-object v8, v4 */
/* .line 356 */
/* .local v8, "str":Ljava/lang/String; */
} // :goto_3
/* aget-object v9, v7, v1 */
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v9, v8 ); // invoke-virtual {p2, v9, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 353 */
} // .end local v7 # "split":[Ljava/lang/String;
} // .end local v8 # "str":Ljava/lang/String;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 358 */
} // .end local v6 # "i":I
} // :cond_2
/* .line 344 */
} // .end local v0 # "packages":[Ljava/lang/String;
/* :pswitch_1 */
v0 = this.mResources;
/* const v6, 0x1103007e */
(( android.content.res.Resources ) v0 ).getStringArray ( v6 ); // invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 345 */
/* .local v0, "landscapePackages":[Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .restart local v6 # "i":I */
} // :goto_4
/* array-length v7, v0 */
/* if-ge v6, v7, :cond_4 */
/* .line 346 */
/* aget-object v7, v0, v6 */
(( java.lang.String ) v7 ).split ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 347 */
/* .restart local v7 # "split":[Ljava/lang/String; */
/* array-length v8, v7 */
/* if-ne v8, v2, :cond_3 */
/* aget-object v8, v7, v3 */
} // :cond_3
/* move-object v8, v4 */
/* .line 348 */
/* .restart local v8 # "str":Ljava/lang/String; */
} // :goto_5
/* aget-object v9, v7, v1 */
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v9, v8 ); // invoke-virtual {p2, v9, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 345 */
} // .end local v7 # "split":[Ljava/lang/String;
} // .end local v8 # "str":Ljava/lang/String;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 350 */
} // .end local v6 # "i":I
} // :cond_4
/* .line 334 */
} // .end local v0 # "landscapePackages":[Ljava/lang/String;
/* :pswitch_2 */
v0 = this.mResources;
/* const v1, 0x11030069 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 335 */
/* .local v0, "allowPackages":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "item":I */
} // :goto_6
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_5 */
/* .line 336 */
/* aget-object v2, v0, v1 */
/* const-string/jumbo v3, "w" */
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v2, v3 ); // invoke-virtual {p2, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 335 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 338 */
} // .end local v1 # "item":I
} // :cond_5
v1 = this.mResources;
/* const v2, 0x1103006a */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 339 */
/* .local v1, "blockPackages":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "item":I */
} // :goto_7
/* array-length v3, v1 */
/* if-ge v2, v3, :cond_6 */
/* .line 340 */
/* aget-object v3, v1, v2 */
final String v4 = "b"; // const-string v4, "b"
(( java.util.concurrent.ConcurrentHashMap ) p2 ).put ( v3, v4 ); // invoke-virtual {p2, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 339 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 342 */
} // .end local v2 # "item":I
} // :cond_6
/* nop */
/* .line 363 */
} // .end local v0 # "allowPackages":[Ljava/lang/String;
} // .end local v1 # "blockPackages":[Ljava/lang/String;
} // :goto_8
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2c81202b -> :sswitch_2 */
/* 0x51dfa3a2 -> :sswitch_1 */
/* 0x78901ee4 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
Boolean isDisabledConfiguration ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "configurationName" # Ljava/lang/String; */
/* .line 483 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "force_resizable_activities"; // const-string v1, "force_resizable_activities"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 484 */
final String v0 = "displayCompat"; // const-string v0, "displayCompat"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* nop */
/* .line 483 */
} // :goto_0
} // .end method
void printCommandHelp ( java.io.PrintWriter p0, java.lang.String p1, java.lang.String[] p2 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "cmd" # Ljava/lang/String; */
/* .param p3, "options" # [Ljava/lang/String; */
/* .line 523 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 524 */
/* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "item":I */
} // :goto_0
/* array-length v2, p3 */
/* if-ge v1, v2, :cond_0 */
/* .line 525 */
/* aget-object v2, p3, v1 */
/* .line 526 */
/* .local v2, "option":Ljava/lang/String; */
final String v3 = " ["; // const-string v3, " ["
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 527 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 528 */
final String v3 = "]"; // const-string v3, "]"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 524 */
} // .end local v2 # "option":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 530 */
} // .end local v1 # "item":I
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 531 */
return;
} // .end method
void printOptionsRequires ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "cmd" # Ljava/lang/String; */
/* .line 519 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " options requires:"; // const-string v1, " options requires:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 520 */
return;
} // .end method
