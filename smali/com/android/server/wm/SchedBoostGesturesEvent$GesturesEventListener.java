public abstract class com.android.server.wm.SchedBoostGesturesEvent$GesturesEventListener {
	 /* .source "SchedBoostGesturesEvent.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/SchedBoostGesturesEvent; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "GesturesEventListener" */
} // .end annotation
/* # virtual methods */
public abstract void onDown ( ) {
} // .end method
public abstract void onFling ( Float p0, Float p1, Integer p2 ) {
} // .end method
public abstract void onMove ( ) {
} // .end method
public abstract void onScroll ( Boolean p0 ) {
} // .end method
