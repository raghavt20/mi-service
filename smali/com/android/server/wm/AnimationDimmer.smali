.class Lcom/android/server/wm/AnimationDimmer;
.super Landroid/view/animation/Animation;
.source "AnimationDimmer.java"


# instance fields
.field alpha:F

.field isVisible:Z

.field private mClipRect:Landroid/graphics/Rect;

.field mCornerRadius:F

.field mDimLayer:Landroid/view/SurfaceControl;

.field private mFromAlpha:F

.field private mHost:Lcom/android/server/wm/WindowContainer;

.field private mToAlpha:F


# direct methods
.method public constructor <init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V
    .locals 0
    .param p1, "fromAlpha"    # F
    .param p2, "toAlpha"    # F
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "host"    # Lcom/android/server/wm/WindowContainer;

    .line 30
    invoke-direct {p0, p1, p2, p4}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLcom/android/server/wm/WindowContainer;)V

    .line 31
    iput-object p3, p0, Lcom/android/server/wm/AnimationDimmer;->mClipRect:Landroid/graphics/Rect;

    .line 32
    return-void
.end method

.method public constructor <init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;F)V
    .locals 0
    .param p1, "fromAlpha"    # F
    .param p2, "toAlpha"    # F
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "host"    # Lcom/android/server/wm/WindowContainer;
    .param p5, "cornerRadius"    # F

    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V

    .line 26
    iput p5, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F

    .line 27
    return-void
.end method

.method public constructor <init>(FFLcom/android/server/wm/WindowContainer;)V
    .locals 1
    .param p1, "fromAlpha"    # F
    .param p2, "toAlpha"    # F
    .param p3, "host"    # Lcom/android/server/wm/WindowContainer;

    .line 34
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 11
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/android/server/wm/AnimationDimmer;->alpha:F

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F

    .line 35
    iput p1, p0, Lcom/android/server/wm/AnimationDimmer;->mFromAlpha:F

    .line 36
    iput p2, p0, Lcom/android/server/wm/AnimationDimmer;->mToAlpha:F

    .line 37
    iput-object p3, p0, Lcom/android/server/wm/AnimationDimmer;->mHost:Lcom/android/server/wm/WindowContainer;

    .line 38
    return-void
.end method

.method private dim(Landroid/view/SurfaceControl$Transaction;Lcom/android/server/wm/WindowContainer;IF)V
    .locals 4
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "container"    # Lcom/android/server/wm/WindowContainer;
    .param p3, "relativeLayer"    # I
    .param p4, "alpha"    # F

    .line 53
    monitor-enter p0

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/android/server/wm/AnimationDimmer;->makeDimLayer()Landroid/view/SurfaceControl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    if-nez v0, :cond_1

    .line 58
    monitor-exit p0

    return-void

    .line 61
    :cond_1
    if-eqz p2, :cond_2

    .line 65
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v1

    invoke-virtual {p1, v0, v1, p3}, Landroid/view/SurfaceControl$Transaction;->setRelativeLayer(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    goto :goto_0

    .line 67
    :cond_2
    const v1, 0x7fffffff

    invoke-virtual {p1, v0, v1}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    .line 69
    :goto_0
    iget v0, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    .line 70
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    iget v1, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F

    invoke-virtual {p1, v0, v1}, Landroid/view/SurfaceControl$Transaction;->setCornerRadius(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 73
    :cond_3
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v0, p4}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 74
    iput p4, p0, Lcom/android/server/wm/AnimationDimmer;->alpha:F

    .line 75
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 76
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/AnimationDimmer;->isVisible:Z

    .line 78
    return-void

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private makeDimLayer()Landroid/view/SurfaceControl;
    .locals 3

    .line 41
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mHost:Lcom/android/server/wm/WindowContainer;

    instance-of v1, v0, Lcom/android/server/wm/ActivityRecord;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    instance-of v1, v0, Lcom/android/server/wm/Task;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    return-object v2

    .line 42
    :cond_1
    :goto_0
    invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowContainer;->makeChildSurface(Lcom/android/server/wm/WindowContainer;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AnimationDimmer;->mHost:Lcom/android/server/wm/WindowContainer;

    .line 43
    invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setParent(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->setColorLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Transition Dim Layer for - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/AnimationDimmer;->mHost:Lcom/android/server/wm/WindowContainer;

    .line 45
    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v0

    .line 42
    return-object v0
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 2
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .line 116
    iget v0, p0, Lcom/android/server/wm/AnimationDimmer;->mFromAlpha:F

    .line 117
    .local v0, "tmpAlpha":F
    iget v1, p0, Lcom/android/server/wm/AnimationDimmer;->mToAlpha:F

    sub-float/2addr v1, v0

    mul-float/2addr v1, p1

    add-float/2addr v1, v0

    iput v1, p0, Lcom/android/server/wm/AnimationDimmer;->alpha:F

    .line 118
    return-void
.end method

.method dimAbove(Landroid/view/SurfaceControl$Transaction;Lcom/android/server/wm/WindowContainer;)V
    .locals 2
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "container"    # Lcom/android/server/wm/WindowContainer;

    .line 95
    const/4 v0, 0x1

    iget v1, p0, Lcom/android/server/wm/AnimationDimmer;->mFromAlpha:F

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/server/wm/AnimationDimmer;->dim(Landroid/view/SurfaceControl$Transaction;Lcom/android/server/wm/WindowContainer;IF)V

    .line 96
    return-void
.end method

.method dimBelow(Landroid/view/SurfaceControl$Transaction;Lcom/android/server/wm/WindowContainer;)V
    .locals 2
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "container"    # Lcom/android/server/wm/WindowContainer;

    .line 125
    const/4 v0, -0x1

    iget v1, p0, Lcom/android/server/wm/AnimationDimmer;->mFromAlpha:F

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/server/wm/AnimationDimmer;->dim(Landroid/view/SurfaceControl$Transaction;Lcom/android/server/wm/WindowContainer;IF)V

    .line 126
    return-void
.end method

.method public getClipRect()Landroid/graphics/Rect;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mClipRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getDimmer()Landroid/view/SurfaceControl;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    return-object v0
.end method

.method setAlpha(Landroid/view/SurfaceControl$Transaction;F)V
    .locals 1
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "alpha"    # F

    .line 99
    iget-boolean v0, p0, Lcom/android/server/wm/AnimationDimmer;->isVisible:Z

    if-nez v0, :cond_0

    .line 100
    return-void

    .line 102
    :cond_0
    monitor-enter p0

    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v0, p2}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 107
    monitor-exit p0

    .line 108
    return-void

    .line 104
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method stepTransitionDim(Landroid/view/SurfaceControl$Transaction;)V
    .locals 1
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;

    .line 121
    iget v0, p0, Lcom/android/server/wm/AnimationDimmer;->alpha:F

    invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/AnimationDimmer;->setAlpha(Landroid/view/SurfaceControl$Transaction;F)V

    .line 122
    return-void
.end method

.method stopDim(Landroid/view/SurfaceControl$Transaction;)V
    .locals 2
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;

    .line 81
    monitor-enter p0

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 84
    iget-object v0, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 85
    iput-object v1, p0, Lcom/android/server/wm/AnimationDimmer;->mDimLayer:Landroid/view/SurfaceControl;

    .line 87
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/AnimationDimmer;->isVisible:Z

    .line 89
    iput-object v1, p0, Lcom/android/server/wm/AnimationDimmer;->mClipRect:Landroid/graphics/Rect;

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/AnimationDimmer;->mCornerRadius:F

    .line 91
    return-void

    .line 87
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
