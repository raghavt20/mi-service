.class final Lcom/android/server/wm/MiuiActivityControllerImpl$H;
.super Landroid/os/Handler;
.source "MiuiActivityControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiActivityControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "H"
.end annotation


# static fields
.field static final ACTIVITY_DESTROYED:I = 0x5

.field static final ACTIVITY_IDLE:I = 0x1

.field static final ACTIVITY_PAUSED:I = 0x3

.field static final ACTIVITY_RESUMED:I = 0x2

.field static final ACTIVITY_STOPPED:I = 0x4


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiActivityControllerImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MiuiActivityControllerImpl;Landroid/os/Looper;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiActivityControllerImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 47
    iput-object p1, p0, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->this$0:Lcom/android/server/wm/MiuiActivityControllerImpl;

    .line 48
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 49
    return-void
.end method

.method private canDispatchNow(Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;)Z
    .locals 12
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 113
    const/4 v0, 0x0

    const-string v1, "MiuiLog-ActivityObserver:"

    if-eqz p1, :cond_9

    if-eqz p2, :cond_9

    .line 114
    const-string v2, "packages"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 115
    .local v2, "packages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "activities"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 116
    .local v3, "activities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ComponentName;>;"
    const/4 v4, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    move v5, v4

    goto :goto_0

    :cond_0
    move v5, v0

    .line 117
    .local v5, "needFilterPackage":Z
    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    move v6, v4

    goto :goto_1

    :cond_1
    move v6, v0

    .line 118
    .local v6, "needFilterActivity":Z
    :goto_1
    if-nez v5, :cond_2

    if-nez v6, :cond_2

    .line 119
    return v4

    .line 121
    :cond_2
    const-string v7, " is not matched"

    if-eqz v5, :cond_4

    .line 122
    iget-object v8, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 123
    return v4

    .line 125
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "The package "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Lcom/android/server/wm/MiuiActivityControllerImpl;->logMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 128
    :cond_4
    const-string v8, "Don\'t need to check package"

    invoke-static {v1, v8}, Lcom/android/server/wm/MiuiActivityControllerImpl;->logMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :goto_2
    if-eqz v6, :cond_8

    .line 131
    iget-object v8, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 132
    .local v8, "realActivity":Landroid/content/ComponentName;
    if-eqz v8, :cond_7

    .line 133
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/ComponentName;

    .line 134
    .local v10, "activity":Landroid/content/ComponentName;
    invoke-virtual {v8, v10}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 135
    return v4

    .line 137
    .end local v10    # "activity":Landroid/content/ComponentName;
    :cond_5
    goto :goto_3

    .line 138
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "The activity "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/android/server/wm/MiuiActivityControllerImpl;->logMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 141
    :cond_7
    const-string v4, "The realActivity is null"

    invoke-static {v1, v4}, Lcom/android/server/wm/MiuiActivityControllerImpl;->logMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .end local v8    # "realActivity":Landroid/content/ComponentName;
    :goto_4
    goto :goto_5

    .line 144
    :cond_8
    const-string v4, "Don\'t need to check activity"

    invoke-static {v1, v4}, Lcom/android/server/wm/MiuiActivityControllerImpl;->logMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :goto_5
    return v0

    .line 150
    .end local v2    # "packages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "activities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ComponentName;>;"
    .end local v5    # "needFilterPackage":Z
    .end local v6    # "needFilterActivity":Z
    :cond_9
    const-string v2, "Record or intent is null"

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiActivityControllerImpl;->logMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    return v0
.end method

.method private dispatchEvent(ILandroid/app/IMiuiActivityObserver;Lcom/android/server/wm/ActivityRecord;)V
    .locals 4
    .param p1, "event"    # I
    .param p2, "observer"    # Landroid/app/IMiuiActivityObserver;
    .param p3, "record"    # Lcom/android/server/wm/ActivityRecord;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 156
    iget-object v0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->this$0:Lcom/android/server/wm/MiuiActivityControllerImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiActivityControllerImpl;->-$$Nest$fgetmSendIntent(Lcom/android/server/wm/MiuiActivityControllerImpl;)Landroid/content/Intent;

    move-result-object v0

    .line 157
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p3, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 158
    iget-object v1, p0, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->this$0:Lcom/android/server/wm/MiuiActivityControllerImpl;

    invoke-static {v1, v0, p3}, Lcom/android/server/wm/MiuiActivityControllerImpl;->-$$Nest$mhandleSpecialExtras(Lcom/android/server/wm/MiuiActivityControllerImpl;Landroid/content/Intent;Lcom/android/server/wm/ActivityRecord;)V

    .line 159
    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 185
    :pswitch_0
    invoke-interface {p2, v0}, Landroid/app/IMiuiActivityObserver;->activityDestroyed(Landroid/content/Intent;)V

    .line 186
    goto :goto_1

    .line 182
    :pswitch_1
    invoke-interface {p2, v0}, Landroid/app/IMiuiActivityObserver;->activityStopped(Landroid/content/Intent;)V

    .line 183
    goto :goto_1

    .line 179
    :pswitch_2
    invoke-interface {p2, v0}, Landroid/app/IMiuiActivityObserver;->activityPaused(Landroid/content/Intent;)V

    .line 180
    goto :goto_1

    .line 164
    :pswitch_3
    const-string v1, "appBounds"

    invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getAppBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 165
    invoke-static {}, Lcom/android/server/wm/MiuiActivityControllerImpl;->-$$Nest$sfgetMIUI_ACTIVITY_EMBEDDING_ENABLED()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    const/4 v1, 0x0

    .line 167
    .local v1, "isEmbedded":Z
    iget-object v2, p3, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v2

    .line 168
    :try_start_0
    invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z

    move-result v3

    move v1, v3

    .line 169
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    if-eqz v1, :cond_0

    .line 171
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v2

    iget-object v3, p3, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isEmbeddingEnabledForPackage(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    const-string/jumbo v2, "windowMode"

    const/16 v3, 0xd

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 169
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 175
    .end local v1    # "isEmbedded":Z
    :cond_0
    :goto_0
    const-string v1, "app_dc_displayid"

    iget-object v2, p3, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 176
    invoke-interface {p2, v0}, Landroid/app/IMiuiActivityObserver;->activityResumed(Landroid/content/Intent;)V

    .line 177
    goto :goto_1

    .line 161
    :pswitch_4
    invoke-interface {p2, v0}, Landroid/app/IMiuiActivityObserver;->activityIdle(Landroid/content/Intent;)V

    .line 162
    nop

    .line 190
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized handleActivityStateChanged(ILcom/android/server/wm/ActivityRecord;)V
    .locals 6
    .param p1, "what"    # I
    .param p2, "record"    # Lcom/android/server/wm/ActivityRecord;

    monitor-enter p0

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->this$0:Lcom/android/server/wm/MiuiActivityControllerImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiActivityControllerImpl;->-$$Nest$fgetmActivityObservers(Lcom/android/server/wm/MiuiActivityControllerImpl;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 88
    .local v0, "i":I
    :goto_0
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "i":I
    .local v1, "i":I
    if-lez v0, :cond_3

    .line 89
    iget-object v0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->this$0:Lcom/android/server/wm/MiuiActivityControllerImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiActivityControllerImpl;->-$$Nest$fgetmActivityObservers(Lcom/android/server/wm/MiuiActivityControllerImpl;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/app/IMiuiActivityObserver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    .local v0, "observer":Landroid/app/IMiuiActivityObserver;
    if-nez v0, :cond_0

    .line 91
    goto :goto_3

    .line 94
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->this$0:Lcom/android/server/wm/MiuiActivityControllerImpl;

    invoke-static {v2}, Lcom/android/server/wm/MiuiActivityControllerImpl;->-$$Nest$fgetmActivityObservers(Lcom/android/server/wm/MiuiActivityControllerImpl;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v2

    .line 95
    .local v2, "cookie":Ljava/lang/Object;
    instance-of v3, v2, Landroid/content/Intent;

    if-eqz v3, :cond_2

    .line 96
    move-object v3, v2

    check-cast v3, Landroid/content/Intent;

    invoke-direct {p0, p2, v3}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->canDispatchNow(Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 97
    invoke-direct {p0, p1, v0, p2}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->dispatchEvent(ILandroid/app/IMiuiActivityObserver;Lcom/android/server/wm/ActivityRecord;)V

    goto :goto_1

    .line 99
    .end local p0    # "this":Lcom/android/server/wm/MiuiActivityControllerImpl$H;
    :cond_1
    const-string v3, "MiuiLog-ActivityObserver:"

    const-string v4, " No need to dispatch the event, ignore it!"

    invoke-static {v3, v4}, Lcom/android/server/wm/MiuiActivityControllerImpl;->logMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 103
    :cond_2
    invoke-direct {p0, p1, v0, p2}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->dispatchEvent(ILandroid/app/IMiuiActivityObserver;Lcom/android/server/wm/ActivityRecord;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    .end local v2    # "cookie":Ljava/lang/Object;
    :goto_1
    goto :goto_2

    .line 105
    :catch_0
    move-exception v2

    .line 106
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "MiuiActivityController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MiuiLog-ActivityObserver: There was something wrong : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    .end local v0    # "observer":Landroid/app/IMiuiActivityObserver;
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    nop

    .line 88
    :goto_3
    move v0, v1

    goto :goto_0

    .line 109
    :cond_3
    iget-object v0, p0, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->this$0:Lcom/android/server/wm/MiuiActivityControllerImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiActivityControllerImpl;->-$$Nest$fgetmActivityObservers(Lcom/android/server/wm/MiuiActivityControllerImpl;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 110
    monitor-exit p0

    return-void

    .line 86
    .end local v1    # "i":I
    .end local p1    # "what":I
    .end local p2    # "record":Lcom/android/server/wm/ActivityRecord;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method codeToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "code"    # I

    .line 52
    invoke-static {}, Lcom/android/server/wm/MiuiActivityControllerImpl;->-$$Nest$sfgetDEBUG_MESSAGES()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 58
    :pswitch_0
    const-string v0, "ACTIVITY_DESTROYED"

    return-object v0

    .line 57
    :pswitch_1
    const-string v0, "ACTIVITY_STOPPED"

    return-object v0

    .line 56
    :pswitch_2
    const-string v0, "ACTIVITY_PAUSED"

    return-object v0

    .line 55
    :pswitch_3
    const-string v0, "ACTIVITY_RESUMED"

    return-object v0

    .line 54
    :pswitch_4
    const-string v0, "ACTIVITY_IDLE"

    return-object v0

    .line 61
    :cond_0
    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 66
    iget v0, p1, Landroid/os/Message;->what:I

    .line 67
    .local v0, "what":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 73
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v1, v1, Lcom/android/server/wm/ActivityRecord;

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/wm/ActivityRecord;

    .line 75
    .local v1, "record":Lcom/android/server/wm/ActivityRecord;
    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->handleActivityStateChanged(ILcom/android/server/wm/ActivityRecord;)V

    .line 76
    .end local v1    # "record":Lcom/android/server/wm/ActivityRecord;
    nop

    .line 84
    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
