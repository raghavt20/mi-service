.class Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;
.super Landroid/os/Handler;
.source "MiuiMirrorDragDropController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiMirrorDragDropController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DragHandler"
.end annotation


# instance fields
.field private final mService:Lcom/android/server/wm/WindowManagerService;

.field final synthetic this$0:Lcom/android/server/wm/MiuiMirrorDragDropController;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMirrorDragDropController;Lcom/android/server/wm/WindowManagerService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p3, "looper"    # Landroid/os/Looper;

    .line 242
    iput-object p1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;->this$0:Lcom/android/server/wm/MiuiMirrorDragDropController;

    .line 243
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 244
    iput-object p2, p0, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 245
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 249
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 253
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;->this$0:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-static {v1}, Lcom/android/server/wm/MiuiMirrorDragDropController;->-$$Nest$fgetmDragState(Lcom/android/server/wm/MiuiMirrorDragDropController;)Lcom/android/server/wm/MiuiMirrorDragState;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;->this$0:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-static {v1}, Lcom/android/server/wm/MiuiMirrorDragDropController;->-$$Nest$fgetmDragState(Lcom/android/server/wm/MiuiMirrorDragDropController;)Lcom/android/server/wm/MiuiMirrorDragState;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z

    .line 255
    iget-object v1, p0, Lcom/android/server/wm/MiuiMirrorDragDropController$DragHandler;->this$0:Lcom/android/server/wm/MiuiMirrorDragDropController;

    invoke-static {v1}, Lcom/android/server/wm/MiuiMirrorDragDropController;->-$$Nest$fgetmDragState(Lcom/android/server/wm/MiuiMirrorDragDropController;)Lcom/android/server/wm/MiuiMirrorDragState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V

    .line 257
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 259
    :cond_1
    :goto_0
    return-void
.end method
