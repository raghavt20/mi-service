public class com.android.server.wm.MiuiMultiWindowRecommendHelper {
	 /* .source "MiuiMultiWindowRecommendHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String MULTI_WINDOW_RECOMMEND_SWITCH;
public static final Integer RECENT_APP_LIST_SIZE;
private static final Integer RECOMMEND_SWITCH_ENABLE_STATE;
private static final Integer RECOMMEND_SWITCH_STATELESS;
private static final java.lang.String TAG;
/* # instance fields */
private volatile Long lastSplitScreenRecommendTime;
private miui.process.IForegroundInfoListener$Stub listener;
private android.content.Context mContext;
private Boolean mFirstWindowHasDraw;
com.android.server.wm.MiuiFreeFormManagerService mFreeFormManagerService;
private com.android.server.wm.RecommendDataEntry mFreeFormRecommendDataEntry;
private android.content.res.Configuration mLastConfiguration;
private Boolean mLastIsWideScreen;
final java.lang.Object mLock;
private Long mMaxTimeFrame;
com.android.server.wm.MiuiMultiWindowRecommendController mMiuiMultiWindowRecommendController;
private android.database.ContentObserver mMuiltiWindowRecommendObserver;
private com.android.server.wm.MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver mMultiWindowRecommendReceiver;
private volatile Boolean mMultiWindowRecommendSwitchEnabled;
private java.util.List mRecentAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final java.lang.Object mRecentAppListLock;
private Integer mRecentAppListMaxSize;
private com.android.server.wm.RecommendDataEntry mSpiltScreenRecommendDataEntry;
private com.android.server.wm.SplitScreenRecommendPredictHelper mSplitScreenRecommendPredictHelper;
private final android.app.TaskStackListener mTaskStackListener;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static com.android.server.wm.RecommendDataEntry -$$Nest$fgetmFreeFormRecommendDataEntry ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFreeFormRecommendDataEntry;
} // .end method
static android.content.res.Configuration -$$Nest$fgetmLastConfiguration ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLastConfiguration;
} // .end method
static Boolean -$$Nest$fgetmLastIsWideScreen ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLastIsWideScreen:Z */
} // .end method
static Boolean -$$Nest$fgetmMultiWindowRecommendSwitchEnabled ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z */
} // .end method
static com.android.server.wm.RecommendDataEntry -$$Nest$fgetmSpiltScreenRecommendDataEntry ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSpiltScreenRecommendDataEntry;
} // .end method
static void -$$Nest$fputmLastIsWideScreen ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLastIsWideScreen:Z */
return;
} // .end method
static void -$$Nest$fputmMultiWindowRecommendSwitchEnabled ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z */
return;
} // .end method
static void -$$Nest$mFreeFormRecommendIfNeeded ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0, java.lang.String p1, Integer p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->FreeFormRecommendIfNeeded(Ljava/lang/String;II)V */
return;
} // .end method
static Boolean -$$Nest$mcheckPreConditionsForSplitScreen ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0, com.android.server.wm.Task p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->checkPreConditionsForSplitScreen(Lcom/android/server/wm/Task;)Z */
} // .end method
static Boolean -$$Nest$minFreeFormRecommendState ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->inFreeFormRecommendState()Z */
} // .end method
static Boolean -$$Nest$minSplitScreenRecommendState ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->inSplitScreenRecommendState()Z */
} // .end method
static Boolean -$$Nest$misDeviceSupportFreeFormRecommend ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->isDeviceSupportFreeFormRecommend()Z */
} // .end method
static Boolean -$$Nest$misDeviceSupportSplitScreenRecommend ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->isDeviceSupportSplitScreenRecommend()Z */
} // .end method
static void -$$Nest$mpredictSplitScreen ( com.android.server.wm.MiuiMultiWindowRecommendHelper p0, com.android.server.wm.SplitScreenRecommendTaskInfo p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->predictSplitScreen(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V */
return;
} // .end method
 com.android.server.wm.MiuiMultiWindowRecommendHelper ( ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "service" # Lcom/android/server/wm/MiuiFreeFormManagerService; */
/* .line 76 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 53 */
/* new-instance v0, Ljava/util/ArrayList; */
int v1 = 4; // const/4 v1, 0x4
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mRecentAppList = v0;
/* .line 54 */
/* const-wide/32 v2, 0x1d4c0 */
/* iput-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMaxTimeFrame:J */
/* .line 55 */
/* const-wide/16 v2, 0x0 */
/* iput-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->lastSplitScreenRecommendTime:J */
/* .line 56 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z */
/* .line 58 */
/* iput v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppListMaxSize:I */
/* .line 63 */
/* new-instance v1, Lcom/android/server/wm/RecommendDataEntry; */
/* invoke-direct {v1}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V */
this.mSpiltScreenRecommendDataEntry = v1;
/* .line 64 */
/* new-instance v1, Lcom/android/server/wm/RecommendDataEntry; */
/* invoke-direct {v1}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V */
this.mFreeFormRecommendDataEntry = v1;
/* .line 68 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z */
/* .line 69 */
/* new-instance v1, Landroid/content/res/Configuration; */
/* invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V */
this.mLastConfiguration = v1;
/* .line 73 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.mLock = v1;
/* .line 74 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.mRecentAppListLock = v1;
/* .line 92 */
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V */
this.listener = v1;
/* .line 116 */
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V */
this.mTaskStackListener = v1;
/* .line 151 */
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$3; */
/* new-instance v2, Landroid/os/Handler; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V */
/* invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$3;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Landroid/os/Handler;)V */
this.mMuiltiWindowRecommendObserver = v1;
/* .line 77 */
this.mContext = p1;
/* .line 78 */
this.mFreeFormManagerService = p2;
/* .line 79 */
/* new-instance v1, Lcom/android/server/wm/SplitScreenRecommendPredictHelper; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/SplitScreenRecommendPredictHelper;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V */
this.mSplitScreenRecommendPredictHelper = v1;
/* .line 80 */
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V */
this.mMultiWindowRecommendReceiver = v1;
/* .line 81 */
v2 = this.mContext;
v4 = this.mFilter;
v5 = this.mFreeFormManagerService;
v5 = this.mHandler;
(( android.content.Context ) v2 ).registerReceiver ( v1, v4, v3, v5 ); // invoke-virtual {v2, v1, v4, v3, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 83 */
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController; */
v2 = this.mContext;
v3 = this.mFreeFormManagerService;
v3 = this.mActivityTaskManagerService;
v3 = this.mWindowManager;
/* invoke-direct {v1, v2, v3, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V */
this.mMiuiMultiWindowRecommendController = v1;
/* .line 85 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "MiuiMultiWindowRecommendSwitch"; // const-string v2, "MiuiMultiWindowRecommendSwitch"
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mMuiltiWindowRecommendObserver;
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v0, v3, v4 ); // invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 87 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->initMultiWindowRecommendSwitchState()V */
/* .line 88 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->registerForegroundInfoListener()V */
/* .line 89 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->registerTaskStackListener()V */
/* .line 90 */
return;
} // .end method
private void FreeFormRecommendIfNeeded ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "senderPackageName" # Ljava/lang/String; */
/* .param p2, "recommendTransactionType" # I */
/* .param p3, "recommendScene" # I */
/* .line 230 */
/* if-nez p1, :cond_0 */
/* .line 231 */
return;
/* .line 234 */
} // :cond_0
int v0 = 3; // const/4 v0, 0x3
int v1 = 1; // const/4 v1, 0x1
/* if-eq p2, v1, :cond_1 */
/* if-eq p2, v0, :cond_1 */
/* .line 236 */
return;
/* .line 239 */
} // :cond_1
/* if-eq p3, v1, :cond_2 */
int v2 = 2; // const/4 v2, 0x2
/* if-eq p3, v2, :cond_2 */
/* .line 241 */
return;
/* .line 244 */
} // :cond_2
v2 = v2 = android.util.MiuiMultiWindowAdapter.LIST_ABOUT_FREEFORM_RECOMMEND_WAITING_APPLICATION;
final String v3 = "MiuiMultiWindowRecommendHelper"; // const-string v3, "MiuiMultiWindowRecommendHelper"
/* if-nez v2, :cond_3 */
/* .line 245 */
final String v0 = "FreeFormRecommendIfNeeded senderPackageName is not in LIST_ABOUT_FREEFORM_RECOMMEND_WAITING_APPLICATION "; // const-string v0, "FreeFormRecommendIfNeeded senderPackageName is not in LIST_ABOUT_FREEFORM_RECOMMEND_WAITING_APPLICATION "
android.util.Slog .d ( v3,v0 );
/* .line 247 */
return;
/* .line 250 */
} // :cond_3
v2 = this.mFreeFormManagerService;
v2 = this.mActivityTaskManagerService;
v2 = (( com.android.server.wm.ActivityTaskManagerService ) v2 ).isInSplitScreenWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 251 */
final String v0 = "FreeFormRecommendIfNeeded isInSplitScreenWindowingMode "; // const-string v0, "FreeFormRecommendIfNeeded isInSplitScreenWindowingMode "
android.util.Slog .d ( v3,v0 );
/* .line 252 */
return;
/* .line 255 */
} // :cond_4
v2 = this.mFreeFormManagerService;
v2 = this.mActivityTaskManagerService;
v2 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v2 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 256 */
(( com.android.server.wm.TaskDisplayArea ) v2 ).getTopRootTaskInWindowingMode ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;
/* .line 257 */
/* .local v1, "currentFullRootTask":Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_9
(( com.android.server.wm.Task ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_5 */
/* .line 262 */
} // :cond_5
/* if-ne p2, v0, :cond_6 */
/* .line 263 */
v0 = this.mMiuiMultiWindowRecommendController;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).removeFreeFormRecommendView ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V
/* .line 264 */
return;
/* .line 267 */
} // :cond_6
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->checkPreConditionsForFreeForm()Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 268 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->inSplitScreenRecommendState()Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 269 */
v0 = this.mMiuiMultiWindowRecommendController;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).removeSplitScreenRecommendView ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V
/* .line 271 */
} // :cond_7
/* nop */
/* .line 272 */
v6 = (( com.android.server.wm.Task ) v1 ).getRootTaskId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* iget v7, v1, Lcom/android/server/wm/Task;->mUserId:I */
/* .line 271 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move v4, p2 */
/* move v5, p3 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildFreeFormRecommendDataEntry(Ljava/lang/String;IIII)Lcom/android/server/wm/RecommendDataEntry; */
this.mFreeFormRecommendDataEntry = v0;
/* .line 273 */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildAndAddFreeFormRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V */
/* .line 275 */
} // :cond_8
return;
/* .line 258 */
} // :cond_9
} // :goto_0
final String v0 = "FreeFormRecommendIfNeeded senderPackageName not equals currentFullRootTask.getPackageName() "; // const-string v0, "FreeFormRecommendIfNeeded senderPackageName not equals currentFullRootTask.getPackageName() "
android.util.Slog .d ( v3,v0 );
/* .line 259 */
return;
} // .end method
private void addNewTaskToRecentAppList ( com.android.server.wm.SplitScreenRecommendTaskInfo p0 ) {
/* .locals 6 */
/* .param p1, "splitScreenRecommendTaskInfo" # Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 434 */
v0 = v0 = this.mRecentAppList;
int v1 = 1; // const/4 v1, 0x1
final String v2 = " addNewTaskToRecentAppList task id = "; // const-string v2, " addNewTaskToRecentAppList task id = "
final String v3 = "MiuiMultiWindowRecommendHelper"; // const-string v3, "MiuiMultiWindowRecommendHelper"
/* if-le v0, v1, :cond_2 */
/* .line 435 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = v1 = this.mRecentAppList;
/* if-ge v0, v1, :cond_1 */
/* .line 436 */
v1 = this.mRecentAppList;
/* check-cast v1, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 437 */
/* .local v1, "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) p1 ).getTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;
/* if-ne v4, v5, :cond_0 */
/* .line 438 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.wm.SplitScreenRecommendTaskInfo ) p1 ).getTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 439 */
v2 = this.mRecentAppList;
/* .line 440 */
return;
/* .line 435 */
} // .end local v1 # "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 443 */
} // .end local v0 # "i":I
} // :cond_1
v0 = this.mRecentAppList;
int v1 = 0; // const/4 v1, 0x0
/* .line 445 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( com.android.server.wm.SplitScreenRecommendTaskInfo ) p1 ).getTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v0 );
/* .line 446 */
v0 = this.mRecentAppList;
/* .line 447 */
return;
} // .end method
private void buildAndAddFreeFormRecommendView ( com.android.server.wm.RecommendDataEntry p0 ) {
/* .locals 1 */
/* .param p1, "recommendDataEntry" # Lcom/android/server/wm/RecommendDataEntry; */
/* .line 356 */
v0 = this.mMiuiMultiWindowRecommendController;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).removeFreeFormRecommendView ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V
/* .line 357 */
v0 = this.mMiuiMultiWindowRecommendController;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).addFreeFormRecommendView ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->addFreeFormRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V
/* .line 358 */
return;
} // .end method
private void buildAndAddSpiltScreenRecommendView ( com.android.server.wm.RecommendDataEntry p0 ) {
/* .locals 2 */
/* .param p1, "recommendDataEntry" # Lcom/android/server/wm/RecommendDataEntry; */
/* .line 349 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) p0 ).setLastSplitScreenRecommendTime ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->setLastSplitScreenRecommendTime(J)V
/* .line 350 */
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) p0 ).clearRecentAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V
/* .line 351 */
v0 = this.mMiuiMultiWindowRecommendController;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).removeSplitScreenRecommendView ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V
/* .line 352 */
v0 = this.mMiuiMultiWindowRecommendController;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).addSplitScreenRecommendView ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->addSplitScreenRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V
/* .line 353 */
return;
} // .end method
private void buildAndAddSpiltScreenRecommendViewIfNeeded ( com.android.server.wm.RecommendDataEntry p0 ) {
/* .locals 9 */
/* .param p1, "recommendDataEntry" # Lcom/android/server/wm/RecommendDataEntry; */
/* .line 329 */
/* const-wide/16 v0, 0x3e8 */
/* .line 330 */
/* .local v0, "timeoutMs":J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* add-long/2addr v2, v0 */
/* .line 331 */
/* .local v2, "timeoutAtTimeMs":J */
v4 = this.mLock;
/* monitor-enter v4 */
/* .line 332 */
int v5 = 0; // const/4 v5, 0x0
try { // :try_start_0
/* iput-boolean v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 334 */
} // :goto_0
try { // :try_start_1
/* iget-boolean v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 335 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* sub-long v5, v2, v5 */
/* .line 336 */
/* .local v5, "waitMillis":J */
/* const-wide/16 v7, 0x0 */
/* cmp-long v7, v5, v7 */
/* if-gtz v7, :cond_0 */
/* .line 337 */
/* .line 339 */
} // :cond_0
v7 = this.mLock;
(( java.lang.Object ) v7 ).wait ( v5, v6 ); // invoke-virtual {v7, v5, v6}, Ljava/lang/Object;->wait(J)V
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 340 */
} // .end local v5 # "waitMillis":J
/* .line 343 */
} // :cond_1
} // :goto_1
/* .line 341 */
/* :catch_0 */
/* move-exception v5 */
/* .line 342 */
/* .local v5, "e":Ljava/lang/InterruptedException; */
try { // :try_start_2
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v6 ).interrupt ( ); // invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V
/* .line 344 */
} // .end local v5 # "e":Ljava/lang/InterruptedException;
} // :goto_2
/* monitor-exit v4 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 345 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildAndAddSpiltScreenRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V */
/* .line 346 */
return;
/* .line 344 */
/* :catchall_0 */
/* move-exception v5 */
try { // :try_start_3
/* monitor-exit v4 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v5 */
} // .end method
private com.android.server.wm.RecommendDataEntry buildFreeFormRecommendDataEntry ( java.lang.String p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 1 */
/* .param p1, "senderPackageName" # Ljava/lang/String; */
/* .param p2, "recommendTransactionType" # I */
/* .param p3, "recommendScene" # I */
/* .param p4, "taskId" # I */
/* .param p5, "userId" # I */
/* .line 319 */
/* new-instance v0, Lcom/android/server/wm/RecommendDataEntry; */
/* invoke-direct {v0}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V */
/* .line 320 */
/* .local v0, "dataEntry":Lcom/android/server/wm/RecommendDataEntry; */
(( com.android.server.wm.RecommendDataEntry ) v0 ).setTransactionType ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/RecommendDataEntry;->setTransactionType(I)V
/* .line 321 */
(( com.android.server.wm.RecommendDataEntry ) v0 ).setRecommendSceneType ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/wm/RecommendDataEntry;->setRecommendSceneType(I)V
/* .line 322 */
(( com.android.server.wm.RecommendDataEntry ) v0 ).setFreeformPackageName ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/RecommendDataEntry;->setFreeformPackageName(Ljava/lang/String;)V
/* .line 323 */
(( com.android.server.wm.RecommendDataEntry ) v0 ).setFreeFormTaskId ( p4 ); // invoke-virtual {v0, p4}, Lcom/android/server/wm/RecommendDataEntry;->setFreeFormTaskId(I)V
/* .line 324 */
(( com.android.server.wm.RecommendDataEntry ) v0 ).setFreeformUserId ( p5 ); // invoke-virtual {v0, p5}, Lcom/android/server/wm/RecommendDataEntry;->setFreeformUserId(I)V
/* .line 325 */
} // .end method
private com.android.server.wm.RecommendDataEntry buildSpiltScreenRecommendDataEntry ( java.util.List p0, com.android.server.wm.SplitScreenRecommendTaskInfo p1 ) {
/* .locals 5 */
/* .param p2, "splitScreenRecommendTaskInfo" # Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;", */
/* ">;", */
/* "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;", */
/* ")", */
/* "Lcom/android/server/wm/RecommendDataEntry;" */
/* } */
} // .end annotation
/* .line 299 */
/* .local p1, "frequentSwitchedTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;" */
/* new-instance v0, Lcom/android/server/wm/RecommendDataEntry; */
/* invoke-direct {v0}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V */
/* .line 300 */
/* .local v0, "dataEntry":Lcom/android/server/wm/RecommendDataEntry; */
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.wm.RecommendDataEntry ) v0 ).setTransactionType ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/RecommendDataEntry;->setTransactionType(I)V
/* .line 301 */
int v1 = 3; // const/4 v1, 0x3
(( com.android.server.wm.RecommendDataEntry ) v0 ).setRecommendSceneType ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/RecommendDataEntry;->setRecommendSceneType(I)V
/* .line 302 */
v1 = (( com.android.server.wm.SplitScreenRecommendTaskInfo ) p2 ).getTaskId ( ); // invoke-virtual {p2}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
/* .line 303 */
/* .local v1, "currentTaskId":I */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 304 */
/* .local v3, "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
v4 = (( com.android.server.wm.SplitScreenRecommendTaskInfo ) v3 ).getTaskId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
/* if-ne v4, v1, :cond_0 */
/* .line 305 */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v3 ).getPkgName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;
(( com.android.server.wm.RecommendDataEntry ) v0 ).setPrimaryPackageName ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setPrimaryPackageName(Ljava/lang/String;)V
/* .line 306 */
v4 = (( com.android.server.wm.SplitScreenRecommendTaskInfo ) v3 ).getTaskId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
(( com.android.server.wm.RecommendDataEntry ) v0 ).setPrimaryTaskId ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setPrimaryTaskId(I)V
/* .line 307 */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;
/* iget v4, v4, Lcom/android/server/wm/Task;->mUserId:I */
(( com.android.server.wm.RecommendDataEntry ) v0 ).setPrimaryUserId ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setPrimaryUserId(I)V
/* .line 309 */
} // :cond_0
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v3 ).getPkgName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;
(( com.android.server.wm.RecommendDataEntry ) v0 ).setSecondaryPackageName ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setSecondaryPackageName(Ljava/lang/String;)V
/* .line 310 */
v4 = (( com.android.server.wm.SplitScreenRecommendTaskInfo ) v3 ).getTaskId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
(( com.android.server.wm.RecommendDataEntry ) v0 ).setSecondaryTaskId ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setSecondaryTaskId(I)V
/* .line 311 */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;
/* iget v4, v4, Lcom/android/server/wm/Task;->mUserId:I */
(( com.android.server.wm.RecommendDataEntry ) v0 ).setSecondaryUserId ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setSecondaryUserId(I)V
/* .line 313 */
} // .end local v3 # "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
} // :goto_1
/* .line 314 */
} // :cond_1
} // .end method
private Boolean checkPreConditionsForFreeForm ( ) {
/* .locals 2 */
/* .line 388 */
v0 = (( com.android.server.wm.MiuiMultiWindowRecommendHelper ) p0 ).hasNotification ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->hasNotification()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 389 */
final String v0 = "MiuiMultiWindowRecommendHelper"; // const-string v0, "MiuiMultiWindowRecommendHelper"
final String v1 = "checkPreConditionsForFreeForm hasNotification"; // const-string v1, "checkPreConditionsForFreeForm hasNotification"
android.util.Slog .d ( v0,v1 );
/* .line 390 */
int v0 = 0; // const/4 v0, 0x0
/* .line 392 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean checkPreConditionsForSplitScreen ( com.android.server.wm.Task p0 ) {
/* .locals 4 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 366 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 367 */
/* .line 369 */
} // :cond_0
v1 = (( com.android.server.wm.Task ) p1 ).isActivityTypeHomeOrRecents ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 370 */
/* .line 372 */
} // :cond_1
v1 = (( com.android.server.wm.Task ) p1 ).getWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I
final String v2 = "MiuiMultiWindowRecommendHelper"; // const-string v2, "MiuiMultiWindowRecommendHelper"
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v3, :cond_4 */
/* .line 373 */
v1 = com.android.server.wm.RecommendUtils .isInSplitScreenWindowingMode ( p1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 379 */
} // :cond_2
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->inSplitScreenRecommendState()Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 380 */
final String v1 = " InSplitScreenRecommendState "; // const-string v1, " InSplitScreenRecommendState "
android.util.Slog .d ( v2,v1 );
/* .line 381 */
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) p0 ).clearRecentAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V
/* .line 382 */
/* .line 384 */
} // :cond_3
/* .line 374 */
} // :cond_4
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " task window mode is "; // const-string v3, " task window mode is "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( com.android.server.wm.Task ) p1 ).getWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " isInSplitScreenWindowingMode= "; // const-string v3, " isInSplitScreenWindowingMode= "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 375 */
v3 = com.android.server.wm.RecommendUtils .isInSplitScreenWindowingMode ( p1 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 374 */
android.util.Slog .d ( v2,v1 );
/* .line 376 */
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) p0 ).clearRecentAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V
/* .line 377 */
} // .end method
private Boolean inFreeFormRecommendState ( ) {
/* .locals 1 */
/* .line 403 */
v0 = this.mMiuiMultiWindowRecommendController;
v0 = (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).inFreeFormRecommendState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->inFreeFormRecommendState()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 404 */
int v0 = 1; // const/4 v0, 0x1
/* .line 406 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean inSplitScreenRecommendState ( ) {
/* .locals 1 */
/* .line 396 */
v0 = this.mMiuiMultiWindowRecommendController;
v0 = (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).inSplitScreenRecommendState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->inSplitScreenRecommendState()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 397 */
int v0 = 1; // const/4 v0, 0x1
/* .line 399 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void initMultiWindowRecommendSwitchState ( ) {
/* .locals 6 */
/* .line 166 */
final String v0 = "MiuiMultiWindowRecommendSwitch"; // const-string v0, "MiuiMultiWindowRecommendSwitch"
try { // :try_start_0
v1 = com.android.server.wm.RecommendUtils .isSupportMiuiMultiWindowRecommend ( );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 167 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 168 */
/* .local v1, "contentResolver":Landroid/content/ContentResolver; */
int v2 = -2; // const/4 v2, -0x2
int v3 = -1; // const/4 v3, -0x1
v4 = android.provider.Settings$System .getIntForUser ( v1,v0,v3,v2 );
int v5 = 1; // const/4 v5, 0x1
/* if-ne v4, v3, :cond_0 */
/* .line 171 */
/* iput-boolean v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z */
/* .line 173 */
} // :cond_0
v0 = android.provider.Settings$System .getIntForUser ( v1,v0,v3,v2 );
/* if-ne v0, v5, :cond_1 */
} // :cond_1
int v5 = 0; // const/4 v5, 0x0
} // :goto_0
/* iput-boolean v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z */
/* .line 178 */
} // .end local v1 # "contentResolver":Landroid/content/ContentResolver;
} // :cond_2
} // :goto_1
final String v0 = "MiuiMultiWindowRecommendHelper"; // const-string v0, "MiuiMultiWindowRecommendHelper"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " initMultiWindowRecommendSwitchState mMultiWindowRecommendSwitchEnabled= "; // const-string v2, " initMultiWindowRecommendSwitchState mMultiWindowRecommendSwitchEnabled= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 182 */
/* .line 180 */
/* :catch_0 */
/* move-exception v0 */
/* .line 181 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 183 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
private Boolean isDeviceSupportFreeFormRecommend ( ) {
/* .locals 2 */
/* .line 220 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = com.android.server.wm.RecommendUtils .isSupportMiuiMultiWindowRecommend ( );
/* if-nez v0, :cond_0 */
/* .line 223 */
} // :cond_0
v0 = android.util.MiuiMultiWindowUtils .supportFreeform ( );
/* if-nez v0, :cond_1 */
/* .line 224 */
/* .line 226 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 221 */
} // :cond_2
} // :goto_0
} // .end method
private Boolean isDeviceSupportSplitScreenRecommend ( ) {
/* .locals 2 */
/* .line 139 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = com.android.server.wm.RecommendUtils .isSupportMiuiMultiWindowRecommend ( );
/* if-nez v0, :cond_0 */
/* .line 142 */
} // :cond_0
v0 = android.util.MiuiMultiWindowUtils .isSupportSplitScreenFeature ( );
/* if-nez v0, :cond_1 */
/* .line 143 */
/* .line 145 */
} // :cond_1
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v0, :cond_3 */
v0 = this.mContext;
v0 = android.util.MiuiMultiWindowUtils .isFoldInnerScreen ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 148 */
} // :cond_2
/* .line 146 */
} // :cond_3
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 140 */
} // :cond_4
} // :goto_1
} // .end method
static java.lang.String lambda$printRecentAppInfo$0 ( com.android.server.wm.SplitScreenRecommendTaskInfo p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "taskInfo" # Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 483 */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) p0 ).getPkgName ( ); // invoke-virtual {p0}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;
} // .end method
private void predictSplitScreen ( com.android.server.wm.SplitScreenRecommendTaskInfo p0 ) {
/* .locals 4 */
/* .param p1, "splitScreenRecommendTaskInfo" # Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 278 */
final String v0 = "MiuiMultiWindowRecommendHelper"; // const-string v0, "MiuiMultiWindowRecommendHelper"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "current foreground task: taskId is"; // const-string v2, "current foreground task: taskId is"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.android.server.wm.SplitScreenRecommendTaskInfo ) p1 ).getTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " package name: "; // const-string v2, " package name: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 279 */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) p1 ).getPkgName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 278 */
android.util.Slog .d ( v0,v1 );
/* .line 281 */
v0 = this.mRecentAppListLock;
/* monitor-enter v0 */
/* .line 282 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->updateAppDataList(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V */
/* .line 283 */
v1 = this.mSplitScreenRecommendPredictHelper;
v2 = this.mRecentAppList;
(( com.android.server.wm.SplitScreenRecommendPredictHelper ) v1 ).getFrequentSwitchedTask ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/SplitScreenRecommendPredictHelper;->getFrequentSwitchedTask(Ljava/util/List;)Ljava/util/List;
/* .line 284 */
/* .local v1, "frequentSwitchedTask":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;" */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 285 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 286 */
v0 = (( com.android.server.wm.MiuiMultiWindowRecommendHelper ) p0 ).hasNotification ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->hasNotification()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 287 */
final String v0 = "MiuiMultiWindowRecommendHelper"; // const-string v0, "MiuiMultiWindowRecommendHelper"
final String v2 = "predictSplitScreen hasNotification "; // const-string v2, "predictSplitScreen hasNotification "
android.util.Slog .d ( v0,v2 );
/* .line 288 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) p0 ).setLastSplitScreenRecommendTime ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->setLastSplitScreenRecommendTime(J)V
/* .line 289 */
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) p0 ).clearRecentAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V
/* .line 291 */
} // :cond_0
/* invoke-direct {p0, v1, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildSpiltScreenRecommendDataEntry(Ljava/util/List;Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)Lcom/android/server/wm/RecommendDataEntry; */
this.mSpiltScreenRecommendDataEntry = v0;
/* .line 292 */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildAndAddSpiltScreenRecommendViewIfNeeded(Lcom/android/server/wm/RecommendDataEntry;)V */
/* .line 295 */
} // :cond_1
} // :goto_0
return;
/* .line 284 */
} // .end local v1 # "frequentSwitchedTask":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void printRecentAppInfo ( ) {
/* .locals 3 */
/* .line 483 */
v0 = this.mRecentAppList;
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$$ExternalSyntheticLambda0;-><init>()V */
/* .line 484 */
final String v1 = ","; // const-string v1, ","
java.util.stream.Collectors .joining ( v1 );
/* check-cast v0, Ljava/lang/String; */
/* .line 485 */
/* .local v0, "recentAppInfo":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "printRecentAppInfo: "; // const-string v2, "printRecentAppInfo: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " mRecentAppList size= "; // const-string v2, " mRecentAppList size= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = v2 = this.mRecentAppList;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiMultiWindowRecommendHelper"; // const-string v2, "MiuiMultiWindowRecommendHelper"
android.util.Slog .d ( v2,v1 );
/* .line 486 */
return;
} // .end method
private void registerForegroundInfoListener ( ) {
/* .locals 1 */
/* .line 499 */
v0 = this.listener;
miui.process.ProcessManager .registerForegroundInfoListener ( v0 );
/* .line 500 */
return;
} // .end method
private void registerTaskStackListener ( ) {
/* .locals 2 */
/* .line 507 */
v0 = this.mFreeFormManagerService;
v0 = this.mActivityTaskManagerService;
v1 = this.mTaskStackListener;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).registerTaskStackListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V
/* .line 508 */
return;
} // .end method
private void removeExcessTasks ( ) {
/* .locals 4 */
/* .line 469 */
v0 = v0 = this.mRecentAppList;
/* iget v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppListMaxSize:I */
/* sub-int/2addr v0, v1 */
/* .line 470 */
/* .local v0, "size":I */
/* if-lez v0, :cond_0 */
/* .line 471 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "excess task size is "; // const-string v2, "excess task size is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiMultiWindowRecommendHelper"; // const-string v2, "MiuiMultiWindowRecommendHelper"
android.util.Slog .d ( v2,v1 );
/* .line 472 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* .line 473 */
v2 = this.mRecentAppList;
int v3 = 0; // const/4 v3, 0x0
/* .line 472 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 476 */
} // .end local v1 # "i":I
} // :cond_0
return;
} // .end method
private void removeTimeOutTasks ( ) {
/* .locals 9 */
/* .line 451 */
v0 = v0 = this.mRecentAppList;
/* if-nez v0, :cond_0 */
/* .line 452 */
v1 = v0 = this.mRecentAppList;
/* add-int/lit8 v1, v1, -0x1 */
/* check-cast v0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v0 ).getSwitchTime ( ); // invoke-virtual {v0}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getSwitchTime()J
/* move-result-wide v0 */
/* .line 453 */
/* .local v0, "switchTime":J */
v2 = this.mRecentAppList;
/* new-instance v3, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$4; */
/* invoke-direct {v3, p0, v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$4;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;J)V */
/* .line 458 */
/* move-result-wide v2 */
/* .line 459 */
/* .local v2, "count":J */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* int-to-long v5, v4 */
/* cmp-long v5, v5, v2 */
/* if-gez v5, :cond_0 */
/* .line 460 */
v5 = this.mRecentAppList;
int v6 = 0; // const/4 v6, 0x0
/* check-cast v5, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 461 */
/* .local v5, "remove":Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "remove task exceed max time limit, taskId is "; // const-string v7, "remove task exceed max time limit, taskId is "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = (( com.android.server.wm.SplitScreenRecommendTaskInfo ) v5 ).getTaskId ( ); // invoke-virtual {v5}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " switchTime= "; // const-string v7, " switchTime= "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 462 */
(( com.android.server.wm.SplitScreenRecommendTaskInfo ) v5 ).getSwitchTime ( ); // invoke-virtual {v5}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getSwitchTime()J
/* move-result-wide v7 */
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 461 */
final String v7 = "MiuiMultiWindowRecommendHelper"; // const-string v7, "MiuiMultiWindowRecommendHelper"
android.util.Slog .d ( v7,v6 );
/* .line 459 */
} // .end local v5 # "remove":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 465 */
} // .end local v0 # "switchTime":J
} // .end local v2 # "count":J
} // .end local v4 # "i":I
} // :cond_0
return;
} // .end method
private void unregisterForegroundInfoListener ( ) {
/* .locals 1 */
/* .line 503 */
v0 = this.listener;
miui.process.ProcessManager .unregisterForegroundInfoListener ( v0 );
/* .line 504 */
return;
} // .end method
private void unregisterTaskStackListener ( ) {
/* .locals 2 */
/* .line 511 */
v0 = this.mFreeFormManagerService;
v0 = this.mActivityTaskManagerService;
v1 = this.mTaskStackListener;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).unregisterTaskStackListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService;->unregisterTaskStackListener(Landroid/app/ITaskStackListener;)V
/* .line 512 */
return;
} // .end method
private void updateAppDataList ( com.android.server.wm.SplitScreenRecommendTaskInfo p0 ) {
/* .locals 0 */
/* .param p1, "splitScreenRecommendTaskInfo" # Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
/* .line 426 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->addNewTaskToRecentAppList(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V */
/* .line 427 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->removeTimeOutTasks()V */
/* .line 428 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->removeExcessTasks()V */
/* .line 429 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->printRecentAppInfo()V */
/* .line 430 */
return;
} // .end method
/* # virtual methods */
void clearRecentAppList ( ) {
/* .locals 3 */
/* .line 489 */
v0 = this.mRecentAppListLock;
/* monitor-enter v0 */
/* .line 490 */
try { // :try_start_0
v1 = v1 = this.mRecentAppList;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 491 */
/* monitor-exit v0 */
return;
/* .line 493 */
} // :cond_0
v1 = this.mRecentAppList;
/* .line 494 */
final String v1 = "MiuiMultiWindowRecommendHelper"; // const-string v1, "MiuiMultiWindowRecommendHelper"
final String v2 = "clear recent app list"; // const-string v2, "clear recent app list"
android.util.Slog .d ( v1,v2 );
/* .line 495 */
/* monitor-exit v0 */
/* .line 496 */
return;
/* .line 495 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void displayConfigurationChange ( com.android.server.wm.DisplayContent p0, android.content.res.Configuration p1 ) {
/* .locals 2 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "configuration" # Landroid/content/res/Configuration; */
/* .line 577 */
v0 = this.mFreeFormManagerService;
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayContent;)V */
(( android.os.Handler ) v0 ).postAtFrontOfQueue ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z
/* .line 599 */
return;
} // .end method
public com.android.server.wm.Task getFocusedTask ( ) {
/* .locals 3 */
/* .line 515 */
int v0 = 0; // const/4 v0, 0x0
/* .line 516 */
/* .local v0, "focusedTask":Lcom/android/server/wm/Task; */
v1 = this.mFreeFormManagerService;
v1 = this.mActivityTaskManagerService;
v1 = this.mWindowManager;
(( com.android.server.wm.WindowManagerService ) v1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
/* .line 517 */
/* .local v1, "displayContent":Lcom/android/server/wm/DisplayContent; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.mFocusedApp;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 518 */
v2 = this.mFocusedApp;
(( com.android.server.wm.ActivityRecord ) v2 ).getTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* .line 520 */
} // :cond_0
} // .end method
public Long getMaxTimeFrame ( ) {
/* .locals 2 */
/* .line 479 */
/* iget-wide v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMaxTimeFrame:J */
/* return-wide v0 */
} // .end method
public Boolean hasNotification ( ) {
/* .locals 3 */
/* .line 524 */
v0 = this.mFreeFormManagerService;
v0 = this.mActivityTaskManagerService;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getGlobalLock ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;
/* monitor-enter v0 */
/* .line 525 */
try { // :try_start_0
v1 = this.mFreeFormManagerService;
v1 = this.mActivityTaskManagerService;
v1 = this.mRootWindowContainer;
/* .line 526 */
(( com.android.server.wm.RootWindowContainer ) v1 ).getDefaultDisplay ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v1 ).getDisplayPolicy ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;
(( com.android.server.wm.DisplayPolicy ) v1 ).getNotificationShade ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayPolicy;->getNotificationShade()Lcom/android/server/wm/WindowState;
/* .line 527 */
/* .local v1, "notificationShade":Lcom/android/server/wm/WindowState; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = (( com.android.server.wm.WindowState ) v1 ).isVisible ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isVisible()Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 528 */
} // .end local v1 # "notificationShade":Lcom/android/server/wm/WindowState;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Boolean isSplitScreenRecommendValid ( com.android.server.wm.RecommendDataEntry p0 ) {
/* .locals 6 */
/* .param p1, "recommendDataEntry" # Lcom/android/server/wm/RecommendDataEntry; */
/* .line 410 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 411 */
/* .line 413 */
} // :cond_0
v1 = this.mFreeFormManagerService;
v1 = this.mActivityTaskManagerService;
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 414 */
try { // :try_start_0
v2 = this.mFreeFormManagerService;
v2 = this.mActivityTaskManagerService;
v2 = this.mRootWindowContainer;
v3 = (( com.android.server.wm.RecommendDataEntry ) p1 ).getPrimaryTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryTaskId()I
(( com.android.server.wm.RootWindowContainer ) v2 ).getRootTask ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/RootWindowContainer;->getRootTask(I)Lcom/android/server/wm/Task;
/* .line 415 */
/* .local v2, "primaryTask":Lcom/android/server/wm/Task; */
v3 = this.mFreeFormManagerService;
v3 = this.mActivityTaskManagerService;
v3 = this.mRootWindowContainer;
v4 = (( com.android.server.wm.RecommendDataEntry ) p1 ).getSecondaryTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryTaskId()I
(( com.android.server.wm.RootWindowContainer ) v3 ).getRootTask ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/RootWindowContainer;->getRootTask(I)Lcom/android/server/wm/Task;
/* .line 416 */
/* .local v3, "secondaryTask":Lcom/android/server/wm/Task; */
if ( v2 != null) { // if-eqz v2, :cond_1
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 417 */
/* monitor-exit v1 */
int v0 = 1; // const/4 v0, 0x1
/* .line 419 */
} // :cond_1
final String v4 = "MiuiMultiWindowRecommendHelper"; // const-string v4, "MiuiMultiWindowRecommendHelper"
final String v5 = " SplitScreenRecommend invalid "; // const-string v5, " SplitScreenRecommend invalid "
android.util.Slog .d ( v4,v5 );
/* .line 420 */
/* monitor-exit v1 */
/* .line 422 */
} // .end local v2 # "primaryTask":Lcom/android/server/wm/Task;
} // .end local v3 # "secondaryTask":Lcom/android/server/wm/Task;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void onFirstWindowDrawn ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 5 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 532 */
if ( p1 != null) { // if-eqz p1, :cond_2
(( com.android.server.wm.ActivityRecord ) p1 ).getTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* if-nez v0, :cond_0 */
/* .line 535 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) p1 ).getTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
v0 = (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* .line 536 */
/* .local v0, "taskId":I */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 537 */
try { // :try_start_0
v2 = this.mSpiltScreenRecommendDataEntry;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 538 */
v2 = (( com.android.server.wm.RecommendDataEntry ) v2 ).getPrimaryTaskId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryTaskId()I
/* if-ne v2, v0, :cond_1 */
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z */
/* if-nez v2, :cond_1 */
/* .line 540 */
final String v2 = "MiuiMultiWindowRecommendHelper"; // const-string v2, "MiuiMultiWindowRecommendHelper"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onFirstWindowDrawn: taskId = "; // const-string v4, "onFirstWindowDrawn: taskId = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " activityRecord: "; // const-string v4, " activityRecord: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 541 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z */
/* .line 542 */
v2 = this.mLock;
(( java.lang.Object ) v2 ).notifyAll ( ); // invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V
/* .line 544 */
} // :cond_1
/* monitor-exit v1 */
/* .line 545 */
return;
/* .line 544 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 533 */
} // .end local v0 # "taskId":I
} // :cond_2
} // :goto_0
return;
} // .end method
void setLastSplitScreenRecommendTime ( Long p0 ) {
/* .locals 2 */
/* .param p1, "recommendTime" # J */
/* .line 361 */
/* iput-wide p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->lastSplitScreenRecommendTime:J */
/* .line 362 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " setLastSplitScreenRecommendTime= "; // const-string v1, " setLastSplitScreenRecommendTime= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMultiWindowRecommendHelper"; // const-string v1, "MiuiMultiWindowRecommendHelper"
android.util.Slog .d ( v1,v0 );
/* .line 363 */
return;
} // .end method
public Integer startSmallFreeformFromNotification ( ) {
/* .locals 8 */
/* .line 549 */
v0 = this.mContext;
v0 = com.android.server.wm.RecommendUtils .isKeyguardLocked ( v0 );
int v1 = 2; // const/4 v1, 0x2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 550 */
/* .line 552 */
} // :cond_0
v0 = com.android.server.wm.RecommendUtils .isSupportMiuiMultiWindowRecommend ( );
/* if-nez v0, :cond_1 */
/* .line 553 */
/* .line 555 */
} // :cond_1
v0 = this.mFreeFormManagerService;
v0 = this.mActivityTaskManagerService;
v0 = this.mRootWindowContainer;
/* .line 556 */
(( com.android.server.wm.RootWindowContainer ) v0 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.wm.TaskDisplayArea ) v0 ).getTopRootTaskInWindowingMode ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;
/* .line 557 */
/* .local v0, "currentFullTask":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_6
v3 = this.realActivity;
/* if-nez v3, :cond_2 */
/* .line 560 */
} // :cond_2
v3 = com.android.server.wm.RecommendUtils .isInSplitScreenWindowingMode ( v0 );
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 561 */
/* .line 563 */
} // :cond_3
v3 = this.mFreeFormManagerService;
(( com.android.server.wm.MiuiFreeFormManagerService ) v3 ).getStackPackageName ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getStackPackageName(Lcom/android/server/wm/Task;)Ljava/lang/String;
/* .line 564 */
/* .local v3, "packageName":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "startSmallFreeformFromNotification: currentFullTask packageName= " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiMultiWindowRecommendHelper"; // const-string v5, "MiuiMultiWindowRecommendHelper"
android.util.Slog .d ( v5,v4 );
/* .line 565 */
v4 = v4 = android.util.MiuiMultiWindowAdapter.LIST_ABOUT_FREEFORM_RECOMMEND_MAP_APPLICATION;
/* if-nez v4, :cond_5 */
v4 = android.util.MiuiMultiWindowAdapter.LIST_ABOUT_FREEFORM_RECOMMEND_MAP_APPLICATION;
v5 = this.realActivity;
/* .line 566 */
v4 = (( android.content.ComponentName ) v5 ).flattenToShortString ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 573 */
} // :cond_4
/* .line 567 */
} // :cond_5
} // :goto_0
v4 = this.mFreeFormManagerService;
v4 = this.mActivityTaskManagerService;
/* .line 569 */
v5 = (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
java.lang.Integer .valueOf ( v5 );
java.lang.Integer .valueOf ( v1 );
/* new-instance v6, Landroid/graphics/Rect; */
/* invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V */
final String v7 = "enterSmallFreeFormByNotificationRecommend"; // const-string v7, "enterSmallFreeFormByNotificationRecommend"
/* filled-new-array {v5, v1, v7, v6}, [Ljava/lang/Object; */
/* .line 567 */
final String v5 = "launchMiniFreeFormWindowVersion2"; // const-string v5, "launchMiniFreeFormWindowVersion2"
android.util.MiuiMultiWindowUtils .invoke ( v4,v5,v1 );
/* .line 571 */
/* .line 558 */
} // .end local v3 # "packageName":Ljava/lang/String;
} // :cond_6
} // :goto_1
} // .end method
