.class public Lcom/android/server/wm/MiuiScreenProjectionServiceExStubImpl;
.super Ljava/lang/Object;
.source "MiuiScreenProjectionServiceExStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/IMiuiScreenProjectionServiceExStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCastActivityResumed(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 16
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getMiuiActivityController()Lcom/android/server/wm/MiuiActivityController;

    move-result-object v0

    .line 17
    invoke-interface {v0, p1}, Lcom/android/server/wm/MiuiActivityController;->activityResumed(Lcom/android/server/wm/ActivityRecord;)V

    .line 18
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V

    .line 19
    return-void
.end method

.method public setAlertWindowTitle(Landroid/view/WindowManager$LayoutParams;)V
    .locals 0
    .param p1, "attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 22
    invoke-static {p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->setAlertWindowTitle(Landroid/view/WindowManager$LayoutParams;)V

    .line 23
    return-void
.end method
