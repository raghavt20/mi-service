.class Lcom/android/server/wm/OneTrackDragDropHelper$1;
.super Landroid/os/Handler;
.source "OneTrackDragDropHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/OneTrackDragDropHelper;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/OneTrackDragDropHelper;


# direct methods
.method constructor <init>(Lcom/android/server/wm/OneTrackDragDropHelper;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/OneTrackDragDropHelper;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 50
    iput-object p1, p0, Lcom/android/server/wm/OneTrackDragDropHelper$1;->this$0:Lcom/android/server/wm/OneTrackDragDropHelper;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 53
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 55
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;

    invoke-virtual {v0}, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;->build()Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;

    move-result-object v0

    .line 56
    .local v0, "dragDropData":Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;
    iget-object v1, p0, Lcom/android/server/wm/OneTrackDragDropHelper$1;->this$0:Lcom/android/server/wm/OneTrackDragDropHelper;

    invoke-static {v1, v0}, Lcom/android/server/wm/OneTrackDragDropHelper;->-$$Nest$mreportOneTrack(Lcom/android/server/wm/OneTrackDragDropHelper;Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;)V

    .line 57
    nop

    .line 61
    .end local v0    # "dragDropData":Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
