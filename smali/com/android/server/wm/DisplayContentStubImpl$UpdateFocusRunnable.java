class com.android.server.wm.DisplayContentStubImpl$UpdateFocusRunnable implements java.lang.Runnable {
	 /* .source "DisplayContentStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/DisplayContentStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "UpdateFocusRunnable" */
} // .end annotation
/* # instance fields */
private Boolean mPosted;
private com.android.server.wm.WindowManagerService mWmService;
/* # direct methods */
static void -$$Nest$mpost ( com.android.server.wm.DisplayContentStubImpl$UpdateFocusRunnable p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->post(Z)V */
return;
} // .end method
 com.android.server.wm.DisplayContentStubImpl$UpdateFocusRunnable ( ) {
/* .locals 0 */
/* .param p1, "wmService" # Lcom/android/server/wm/WindowManagerService; */
/* .line 523 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 524 */
this.mWmService = p1;
/* .line 525 */
return;
} // .end method
private void post ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "post" # Z */
/* .line 548 */
/* iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mPosted:Z */
/* if-ne p1, v0, :cond_0 */
/* .line 549 */
return;
/* .line 551 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 552 */
v0 = this.mWmService;
v0 = this.mH;
/* const-wide/16 v1, 0xbb8 */
(( com.android.server.wm.WindowManagerService$H ) v0 ).postDelayed ( p0, v1, v2 ); // invoke-virtual {v0, p0, v1, v2}, Lcom/android/server/wm/WindowManagerService$H;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 554 */
} // :cond_1
v0 = this.mWmService;
v0 = this.mH;
(( com.android.server.wm.WindowManagerService$H ) v0 ).removeCallbacks ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/wm/WindowManagerService$H;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 556 */
} // :goto_0
/* iput-boolean p1, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mPosted:Z */
/* .line 557 */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 529 */
v0 = this.mWmService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 532 */
try { // :try_start_0
final String v1 = "WindowManager"; // const-string v1, "WindowManager"
final String v2 = "retry update focused window after 3000ms"; // const-string v2, "retry update focused window after 3000ms"
android.util.Slog .w ( v1,v2 );
/* .line 538 */
v1 = this.mWmService;
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.wm.WindowManagerService ) v1 ).updateFocusedWindowLocked ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/android/server/wm/WindowManagerService;->updateFocusedWindowLocked(IZ)Z
/* .line 543 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 544 */
/* iput-boolean v3, p0, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->mPosted:Z */
/* .line 545 */
return;
/* .line 543 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
