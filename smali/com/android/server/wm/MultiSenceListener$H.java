class com.android.server.wm.MultiSenceListener$H extends android.os.Handler {
	 /* .source "MultiSenceListener.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MultiSenceListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MultiSenceListener this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MultiSenceListener$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 137 */
this.this$0 = p1;
/* .line 138 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 139 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 143 */
v0 = this.obj;
/* check-cast v0, Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 144 */
/* .local v0, "taskInfo":Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* iget v1, p1, Landroid/os/Message;->what:I */
/* const-wide/16 v2, 0x20 */
/* packed-switch v1, :pswitch_data_0 */
/* .line 153 */
/* :pswitch_0 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "EVENT_MULTI_TASK_ACTION_END: "; // const-string v4, "EVENT_MULTI_TASK_ACTION_END: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) v0 ).getType ( ); // invoke-virtual {v0}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I
miui.smartpower.MultiTaskActionManager .actionTypeToString ( v4 );
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 154 */
/* .local v1, "actionEndDetail":Ljava/lang/String; */
android.os.Trace .traceBegin ( v2,v3,v1 );
/* .line 155 */
v4 = this.this$0;
com.android.server.wm.MultiSenceListener .-$$Nest$mLOG_IF_DEBUG ( v4,v1 );
/* .line 156 */
v4 = this.this$0;
int v5 = 0; // const/4 v5, 0x0
com.android.server.wm.MultiSenceListener .-$$Nest$mupdateDynamicSenceInfo ( v4,v0,v5 );
/* .line 157 */
v4 = this.this$0;
(( com.android.server.wm.MultiSenceListener ) v4 ).updateScreenStatus ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MultiSenceListener;->updateScreenStatus()V
/* .line 158 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 159 */
/* .line 146 */
} // .end local v1 # "actionEndDetail":Ljava/lang/String;
/* :pswitch_1 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "EVENT_MULTI_TASK_ACTION_START: "; // const-string v4, "EVENT_MULTI_TASK_ACTION_START: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) v0 ).getType ( ); // invoke-virtual {v0}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I
miui.smartpower.MultiTaskActionManager .actionTypeToString ( v4 );
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 147 */
/* .local v1, "actionDetail":Ljava/lang/String; */
android.os.Trace .traceBegin ( v2,v3,v1 );
/* .line 148 */
v4 = this.this$0;
com.android.server.wm.MultiSenceListener .-$$Nest$mLOG_IF_DEBUG ( v4,v1 );
/* .line 149 */
v4 = this.this$0;
int v5 = 1; // const/4 v5, 0x1
com.android.server.wm.MultiSenceListener .-$$Nest$mupdateDynamicSenceInfo ( v4,v0,v5 );
/* .line 150 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 151 */
/* nop */
/* .line 163 */
} // .end local v1 # "actionDetail":Ljava/lang/String;
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
