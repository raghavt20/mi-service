class com.android.server.wm.ActivityTaskManagerServiceImpl$2 implements java.lang.Runnable {
	 /* .source "ActivityTaskManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->reportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.ActivityTaskManagerServiceImpl this$0; //synthetic
final android.content.pm.ApplicationInfo val$multiWindowAppInfo; //synthetic
final Integer val$pid; //synthetic
final com.android.server.wm.ActivityRecord val$r; //synthetic
final com.android.server.wm.ActivityRecord$State val$state; //synthetic
/* # direct methods */
 com.android.server.wm.ActivityTaskManagerServiceImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 409 */
this.this$0 = p1;
this.val$r = p2;
this.val$state = p3;
/* iput p4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$pid:I */
this.val$multiWindowAppInfo = p5;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 412 */
v0 = this.this$0;
v1 = this.val$r;
v2 = this.val$state;
/* iget v3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$pid:I */
v4 = this.val$multiWindowAppInfo;
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).onForegroundActivityChanged ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChanged(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;ILandroid/content/pm/ApplicationInfo;)V
/* .line 413 */
return;
} // .end method
