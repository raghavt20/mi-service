public class com.android.server.wm.WindowManagerServiceImpl implements com.android.server.wm.WindowManagerServiceStub {
	 /* .source "WindowManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/WindowManagerServiceImpl$WindowOpChangedListener;, */
	 /* Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean BACKGROUND_BLUR_STATUS_DEFAULT;
private static final Boolean BACKGROUND_BLUR_SUPPORTED;
private static final java.lang.String BUILD_DENSITY_PROP;
private static final java.lang.String CARWITH_PACKAGE_NAME;
private static final java.lang.String CARWITH_TRUSTED_DAILY_SIG;
private static final java.lang.String CARWITH_TRUSTED_NON_DAILY_SIG;
private static final java.lang.String CIRCULATE;
private static final java.lang.String CIRCULATEACTIVITY;
private static final java.lang.String CLIENT_DIMMER_NAME;
private static java.lang.String CUR_DEVICE;
public static java.lang.String FIND_DEVICE;
private static java.lang.String FORCE_ORI_DEVICES_LIST;
private static java.lang.String FORCE_ORI_LIST;
public static java.lang.String GOOGLE;
public static java.lang.String GOOGLE_FLOATING;
private static final Integer INVALID_APPEARANCE;
private static final java.lang.String LAUNCHER;
private static final java.lang.String MIUI_RESOLUTION;
public static java.lang.String MM;
public static java.lang.String MM_FLOATING;
public static final Integer MSG_SERVICE_INIT_AFTER_BOOT;
public static final Integer MSG_UPDATE_BLUR_WALLPAPER;
public static java.lang.String QQ;
public static java.lang.String QQ_FLOATING;
public static final Integer SCREEN_SHARE_PROTECTION_OPEN;
public static final Integer SCREEN_SHARE_PROTECTION_WITH_BLUR;
public static java.lang.String SECURITY;
public static java.lang.String SECURITY_FLOATING;
private static final java.lang.String SHA_256;
private static final Boolean SUPPORT_UNION_POWER_CORE;
private static final Boolean SUPPPORT_CLOUD_DIM;
private static final java.lang.String TAG;
private static final Integer VIRTUAL_CAMERA_BOUNDARY;
private static java.util.ArrayList mFloatWindowMirrorWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mProjectionBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
Boolean HAS_SCREEN_SHOT;
Boolean IS_CTS_MODE;
Boolean isFpClientOn;
private Integer leftAppearance;
private android.graphics.Rect leftStatusBounds;
android.app.AppOpsManager mAppOps;
com.android.server.wm.ActivityTaskManagerService mAtmService;
private final android.hardware.camera2.CameraManager$AvailabilityCallback mAvailabilityCallback;
private java.util.List mBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.graphics.Bitmap mBlurWallpaperBmp;
private android.content.BroadcastReceiver mBootCompletedReceiver;
private android.hardware.camera2.CameraManager mCameraManager;
android.content.Context mContext;
private final android.net.Uri mContrastAlphaUri;
private final android.net.Uri mDarkModeContrastEnable;
private final android.net.Uri mDarkModeEnable;
private java.util.HashMap mDimNeedAssistMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mDimUserTimeoutMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
com.android.server.wm.DisplayContent mDisplayContent;
android.hardware.display.DisplayManagerInternal mDisplayManagerInternal;
private final java.lang.Object mFlipListenerLock;
private com.android.server.wm.WindowManagerGlobalLock mGlobalLock;
private Boolean mIsResolutionChanged;
private java.lang.Boolean mLastIsFullScreen;
public Boolean mLastWindowHasSecure;
com.android.server.wm.MiuiContrastOverlayStub mMiuiContrastOverlay;
private Integer mMiuiDisplayDensity;
private Integer mMiuiDisplayHeight;
private Integer mMiuiDisplayWidth;
private android.view.SurfaceControl mMiuiPaperContrastOverlay;
private java.util.ArrayList mMiuiSecurityImeWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArrayMap mNavigationBarColorListenerMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Landroid/os/IBinder;", */
/* "Landroid/view/IFlipWindowStateListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mNavigationColor;
private java.util.Set mOpeningCameraID;
private Boolean mPendingSwitchResolution;
private Integer mPhysicalDensity;
private Integer mPhysicalHeight;
private Integer mPhysicalWidth;
private com.miui.whetstone.PowerKeeperPolicy mPowerKeeperPolicy;
private Boolean mRunningRecentsAnimation;
final java.util.ArrayList mSecureChangeListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Landroid/app/IWindowSecureChangeListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private miui.security.SecurityManagerInternal mSecurityInternal;
private com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerServiceInternal;
private Boolean mSplitMode;
private Boolean mSupportActiveModeSwitch;
private Boolean mSupportSwitchResolutionFeature;
private android.util.SparseArray mTaskIdScreenBrightnessOverrides;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashSet mTransitionReadyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
android.view.IWindowAnimationFinishedCallback mUiModeAnimFinishedCallback;
private android.app.IWallpaperManagerCallback mWallpaperCallback;
com.android.server.wm.WindowManagerService mWmService;
private com.android.server.wm.MiuiHoverModeInternal miuiHoverModeInternal;
private Integer rightAppearance;
private android.graphics.Rect rightStatusBounds;
/* # direct methods */
public static void $r8$lambda$1lnNTwiTZ_nxbKMU1QsrrULku74 ( com.android.server.wm.WindowManagerServiceImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$updateScreenShareProjectFlag$2()V */
return;
} // .end method
public static void $r8$lambda$3ghNrU5cJMB_bzbhqECkNZvRDXc ( com.android.server.wm.WindowManagerServiceImpl p0, com.android.server.wm.Task p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$notifySystemBrightnessChange$3(Lcom/android/server/wm/Task;)V */
return;
} // .end method
public static void $r8$lambda$GFgBx0SKCCw_mYD0oRn4aIBbQlM ( com.android.server.wm.WindowManagerServiceImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$notifySystemBrightnessChange$4()V */
return;
} // .end method
public static void $r8$lambda$S5prd5M3_QmsIlsmsVTm6fHeB2U ( com.android.server.wm.WindowManagerServiceImpl p0, com.android.server.wm.WindowState p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$updateScreenShareProjectFlag$1(Lcom/android/server/wm/WindowState;)V */
return;
} // .end method
public static Boolean $r8$lambda$uIIcu0OABV4fP1JVJfM-GkB_XLA ( com.android.server.wm.WindowManagerServiceImpl p0, com.android.server.wm.WindowState p1 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$notAllowCaptureDisplay$5(Lcom/android/server/wm/WindowState;)Z */
} // .end method
public static void $r8$lambda$zt8KgAfS7hjr7NVYTjRMsKehgEc ( com.android.server.wm.WindowManagerServiceImpl p0, Boolean p1, com.android.server.wm.WindowState p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl;->lambda$setRoundedCornerOverlaysCanScreenShot$0(ZLcom/android/server/wm/WindowState;)V */
return;
} // .end method
static java.util.Set -$$Nest$fgetmOpeningCameraID ( com.android.server.wm.WindowManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mOpeningCameraID;
} // .end method
static Boolean -$$Nest$misDarkModeContrastEnable ( com.android.server.wm.WindowManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isDarkModeContrastEnable()Z */
} // .end method
static com.android.server.wm.WindowManagerServiceImpl ( ) {
/* .locals 4 */
/* .line 165 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 167 */
final String v1 = "com.alibaba.android.rimet"; // const-string v1, "com.alibaba.android.rimet"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 168 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mFloatWindowMirrorWhiteList;
final String v1 = "com.tencent.mm"; // const-string v1, "com.tencent.mm"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 173 */
final String v0 = "com.tencent.mm/com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v0, "com.tencent.mm/com.tencent.mm.plugin.voip.ui.VideoActivity"
final String v2 = "com.tencent.mm/com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"; // const-string v2, "com.tencent.mm/com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"
/* filled-new-array {v0, v2}, [Ljava/lang/String; */
/* .line 175 */
final String v0 = "chiron"; // const-string v0, "chiron"
final String v2 = "polaris"; // const-string v2, "polaris"
final String v3 = "lithium"; // const-string v3, "lithium"
/* filled-new-array {v3, v0, v2}, [Ljava/lang/String; */
/* .line 176 */
v0 = android.os.Build.DEVICE;
/* .line 178 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 179 */
/* .line 180 */
final String v0 = "com.tencent.mobileqq"; // const-string v0, "com.tencent.mobileqq"
/* .line 181 */
final String v0 = "com.google.android.dialer"; // const-string v0, "com.google.android.dialer"
/* .line 182 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
/* .line 183 */
final String v0 = "com.tencent.mm/.FloatingWindow"; // const-string v0, "com.tencent.mm/.FloatingWindow"
/* .line 184 */
final String v0 = "com.tencent.mobileqq/.FloatingWindow"; // const-string v0, "com.tencent.mobileqq/.FloatingWindow"
/* .line 185 */
final String v0 = "com.google.android.dialer/.FloatingWindow"; // const-string v0, "com.google.android.dialer/.FloatingWindow"
/* .line 186 */
final String v0 = "com.miui.securitycenter/.FloatingWindow"; // const-string v0, "com.miui.securitycenter/.FloatingWindow"
/* .line 187 */
final String v0 = "com.xiaomi.finddevice"; // const-string v0, "com.xiaomi.finddevice"
/* .line 190 */
/* const-string/jumbo v0, "support_cloud_dim" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.wm.WindowManagerServiceImpl.SUPPPORT_CLOUD_DIM = (v0!= 0);
/* .line 193 */
final String v0 = "persist.sys.unionpower.enable"; // const-string v0, "persist.sys.unionpower.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.WindowManagerServiceImpl.SUPPORT_UNION_POWER_CORE = (v0!= 0);
/* .line 282 */
final String v0 = "persist.sys.background_blur_supported"; // const-string v0, "persist.sys.background_blur_supported"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.WindowManagerServiceImpl.BACKGROUND_BLUR_SUPPORTED = (v0!= 0);
/* .line 284 */
final String v0 = "persist.sys.background_blur_status_default"; // const-string v0, "persist.sys.background_blur_status_default"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.WindowManagerServiceImpl.BACKGROUND_BLUR_STATUS_DEFAULT = (v0!= 0);
return;
} // .end method
public com.android.server.wm.WindowManagerServiceImpl ( ) {
/* .locals 3 */
/* .line 158 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 188 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mDimUserTimeoutMap = v0;
/* .line 189 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mDimNeedAssistMap = v0;
/* .line 192 */
int v0 = 0; // const/4 v0, 0x0
this.mPowerKeeperPolicy = v0;
/* .line 207 */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
this.mTaskIdScreenBrightnessOverrides = v1;
/* .line 237 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->IS_CTS_MODE:Z */
/* .line 238 */
/* iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->HAS_SCREEN_SHOT:Z */
/* .line 239 */
/* iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSplitMode:Z */
/* .line 241 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mOpeningCameraID = v2;
/* .line 245 */
/* nop */
/* .line 246 */
/* const-string/jumbo v2, "ui_night_mode" */
android.provider.Settings$Secure .getUriFor ( v2 );
this.mDarkModeEnable = v2;
/* .line 249 */
/* nop */
/* .line 250 */
final String v2 = "dark_mode_contrast_enable"; // const-string v2, "dark_mode_contrast_enable"
android.provider.Settings$System .getUriFor ( v2 );
this.mDarkModeContrastEnable = v2;
/* .line 251 */
/* nop */
/* .line 252 */
final String v2 = "contrast_alpha"; // const-string v2, "contrast_alpha"
android.provider.Settings$System .getUriFor ( v2 );
this.mContrastAlphaUri = v2;
/* .line 279 */
/* iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mLastWindowHasSecure:Z */
/* .line 299 */
/* new-instance v1, Landroid/graphics/Rect; */
/* invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V */
this.leftStatusBounds = v1;
/* .line 300 */
/* new-instance v1, Landroid/graphics/Rect; */
/* invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V */
this.rightStatusBounds = v1;
/* .line 304 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I */
/* .line 305 */
/* iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I */
/* .line 309 */
this.mMiuiPaperContrastOverlay = v0;
/* .line 312 */
this.mLastIsFullScreen = v0;
/* .line 314 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mNavigationBarColorListenerMap = v0;
/* .line 324 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mFlipListenerLock = v0;
/* .line 327 */
/* new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$1;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V */
this.mMiuiSecurityImeWhiteList = v0;
/* .line 334 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mTransitionReadyList = v0;
/* .line 484 */
/* new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$4; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$4;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V */
this.mAvailabilityCallback = v0;
/* .line 1241 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mSecureChangeListeners = v0;
return;
} // .end method
private void addShowOnFindDeviceKeyguardAttrsIfNecessary ( android.view.WindowManager$LayoutParams p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 693 */
final String v0 = "com.google.android.dialer"; // const-string v0, "com.google.android.dialer"
v0 = android.text.TextUtils .equals ( v0,p2 );
/* if-nez v0, :cond_0 */
return;
/* .line 695 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "com.xiaomi.system.devicelock.locked"; // const-string v1, "com.xiaomi.system.devicelock.locked"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* if-nez v0, :cond_1 */
/* .line 696 */
return;
/* .line 698 */
} // :cond_1
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p1, Landroid/view/WindowManager$LayoutParams;->format:I */
/* .line 699 */
/* iput v2, p1, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 700 */
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* or-int/lit16 v0, v0, 0x1000 */
/* iput v0, p1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* .line 701 */
return;
} // .end method
private void adjustFindDeviceAttrs ( Integer p0, android.view.WindowManager$LayoutParams p1, java.lang.String p2 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 673 */
/* invoke-direct {p0, p2, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->addShowOnFindDeviceKeyguardAttrsIfNecessary(Landroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V */
/* .line 675 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->removeFindDeviceKeyguardFlagsIfNecessary(ILandroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V */
/* .line 676 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->killFreeformForFindDeviceKeyguardIfNecessary(ILandroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V */
/* .line 677 */
return;
} // .end method
private void adjustGoogleAssistantAttrs ( android.view.WindowManager$LayoutParams p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 562 */
final String v0 = "com.google.android.googlequicksearchbox"; // const-string v0, "com.google.android.googlequicksearchbox"
v0 = android.text.TextUtils .equals ( v0,p2 );
/* if-nez v0, :cond_0 */
/* .line 563 */
return;
/* .line 565 */
} // :cond_0
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* const v1, 0x8000 */
/* or-int/2addr v0, v1 */
/* iput v0, p1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* .line 566 */
return;
} // .end method
private Integer calcDensityAndUpdateForceDensityIfNeed ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "desireWidth" # I */
/* .line 822 */
v0 = (( com.android.server.wm.WindowManagerServiceImpl ) p0 ).getForcedDensity ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getForcedDensity()I
/* .line 823 */
/* .local v0, "density":I */
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isDensityForced()Z */
/* const/high16 v2, 0x3f800000 # 1.0f */
/* if-nez v1, :cond_0 */
/* .line 824 */
/* iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalDensity:I */
/* mul-int/2addr v1, p1 */
/* int-to-float v1, v1 */
/* mul-float/2addr v1, v2 */
/* iget v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I */
/* int-to-float v2, v2 */
/* div-float/2addr v1, v2 */
v0 = java.lang.Math .round ( v1 );
/* .line 825 */
} // :cond_0
v1 = this.mWmService;
/* iget-boolean v1, v1, Lcom/android/server/wm/WindowManagerService;->mSystemBooted:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 826 */
/* mul-int v3, p1, v0 */
/* int-to-float v3, v3 */
/* mul-float/2addr v3, v2 */
/* int-to-float v1, v1 */
/* div-float/2addr v3, v1 */
v0 = java.lang.Math .round ( v3 );
/* .line 827 */
v1 = this.mDisplayContent;
/* iput v0, v1, Lcom/android/server/wm/DisplayContent;->mBaseDisplayDensity:I */
/* .line 828 */
v1 = this.mWmService;
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ""; // const-string v3, ""
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 830 */
v3 = android.os.UserHandle .getCallingUserId ( );
/* .line 828 */
final String v4 = "display_density_forced"; // const-string v4, "display_density_forced"
android.provider.Settings$Secure .putStringForUser ( v1,v4,v2,v3 );
/* .line 832 */
} // :cond_1
} // :goto_0
} // .end method
private void checkDDICSupportAndInitPhysicalSize ( ) {
/* .locals 7 */
/* .line 909 */
v0 = this.mDisplayManagerInternal;
int v1 = 0; // const/4 v1, 0x0
(( android.hardware.display.DisplayManagerInternal ) v0 ).getDisplayInfo ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManagerInternal;->getDisplayInfo(I)Landroid/view/DisplayInfo;
/* .line 910 */
/* .local v0, "defaultDisplayInfo":Landroid/view/DisplayInfo; */
v2 = this.supportedModes;
/* .line 911 */
/* .local v2, "modes":[Landroid/view/Display$Mode; */
/* array-length v3, v2 */
} // :goto_0
/* if-ge v1, v3, :cond_2 */
/* aget-object v4, v2, v1 */
/* .line 912 */
/* .local v4, "mode":Landroid/view/Display$Mode; */
/* iget v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I */
if ( v5 != null) { // if-eqz v5, :cond_0
v5 = (( android.view.Display$Mode ) v4 ).getPhysicalHeight ( ); // invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalHeight()I
/* iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I */
/* if-eq v5, v6, :cond_0 */
/* .line 913 */
int v5 = 1; // const/4 v5, 0x1
/* iput-boolean v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportActiveModeSwitch:Z */
/* .line 915 */
} // :cond_0
v5 = (( android.view.Display$Mode ) v4 ).getPhysicalHeight ( ); // invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalHeight()I
/* iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I */
/* if-le v5, v6, :cond_1 */
/* .line 916 */
v5 = (( android.view.Display$Mode ) v4 ).getPhysicalWidth ( ); // invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalWidth()I
/* iput v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I */
/* .line 917 */
v5 = (( android.view.Display$Mode ) v4 ).getPhysicalHeight ( ); // invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalHeight()I
/* iput v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I */
/* .line 911 */
} // .end local v4 # "mode":Landroid/view/Display$Mode;
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 920 */
} // :cond_2
final String v1 = "ro.sf.lcd_density"; // const-string v1, "ro.sf.lcd_density"
/* const/16 v3, 0x230 */
v1 = android.os.SystemProperties .getInt ( v1,v3 );
/* iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalDensity:I */
/* .line 921 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "init resolution mSupportActiveModeSwitch:"; // const-string v3, "init resolution mSupportActiveModeSwitch:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportActiveModeSwitch:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " mPhysicalWidth:"; // const-string v3, " mPhysicalWidth:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " mPhysicalHeight:"; // const-string v3, " mPhysicalHeight:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " mPhysicalDensity:"; // const-string v3, " mPhysicalDensity:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalDensity:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "WindowManagerService"; // const-string v3, "WindowManagerService"
android.util.Slog .i ( v3,v1 );
/* .line 925 */
return;
} // .end method
private void checkIfUnusualWindowEvent ( Integer p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "tag" # Ljava/lang/String; */
/* .param p3, "width" # I */
/* .param p4, "height" # I */
/* .line 1768 */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1771 */
/* .local v0, "packageName":Ljava/lang/String; */
/* nop */
/* .line 1772 */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_6 */
/* .line 1773 */
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_6 */
/* .line 1774 */
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_6 */
/* .line 1775 */
final String v1 = "android"; // const-string v1, "android"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1778 */
} // :cond_0
v1 = this.mSecurityInternal;
/* if-nez v1, :cond_1 */
/* .line 1779 */
/* const-class v1, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lmiui/security/SecurityManagerInternal; */
this.mSecurityInternal = v1;
/* .line 1781 */
} // :cond_1
v1 = this.mSecurityInternal;
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1782 */
/* const/16 v2, 0x25 */
(( miui.security.SecurityManagerInternal ) v1 ).getThreshold ( v2 ); // invoke-virtual {v1, v2}, Lmiui/security/SecurityManagerInternal;->getThreshold(I)J
/* move-result-wide v7 */
/* .line 1783 */
/* .local v7, "threshold":J */
/* int-to-long v1, p3 */
/* cmp-long v1, v1, v7 */
/* if-lez v1, :cond_4 */
/* int-to-long v1, p4 */
/* cmp-long v1, v1, v7 */
/* if-gtz v1, :cond_2 */
/* .line 1786 */
} // :cond_2
/* iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I */
/* if-lez v1, :cond_5 */
/* iget v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I */
/* if-lez v2, :cond_5 */
/* int-to-long v3, p3 */
/* int-to-long v5, v1 */
/* add-long/2addr v5, v7 */
/* cmp-long v1, v3, v5 */
/* if-gez v1, :cond_3 */
/* int-to-long v3, p4 */
/* int-to-long v1, v2 */
/* add-long/2addr v1, v7 */
/* cmp-long v1, v3, v1 */
/* if-ltz v1, :cond_5 */
/* .line 1788 */
} // :cond_3
v1 = this.mSecurityInternal;
/* const/16 v2, 0x26 */
/* const-wide/16 v4, 0x1 */
/* move-object v3, v0 */
/* move-object v6, p2 */
/* invoke-virtual/range {v1 ..v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 1784 */
} // :cond_4
} // :goto_0
v1 = this.mSecurityInternal;
/* const/16 v2, 0x25 */
/* const-wide/16 v4, 0x1 */
/* move-object v3, v0 */
/* move-object v6, p2 */
/* invoke-virtual/range {v1 ..v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 1792 */
} // .end local v7 # "threshold":J
} // :cond_5
} // :goto_1
return;
/* .line 1776 */
} // :cond_6
} // :goto_2
return;
/* .line 1769 */
} // .end local v0 # "packageName":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 1770 */
/* .local v0, "e":Ljava/lang/Exception; */
return;
} // .end method
private Integer createAppearance ( android.graphics.Rect p0 ) {
/* .locals 9 */
/* .param p1, "bounds" # Landroid/graphics/Rect; */
/* .line 1689 */
/* new-instance v0, Landroid/graphics/Rect; */
v1 = this.mBlurWallpaperBmp;
v1 = (( android.graphics.Bitmap ) v1 ).getWidth ( ); // invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I
v2 = this.mBlurWallpaperBmp;
v2 = (( android.graphics.Bitmap ) v2 ).getHeight ( ); // invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V */
/* .line 1690 */
/* .local v0, "bitmapBounds":Landroid/graphics/Rect; */
/* new-instance v1, Landroid/graphics/Rect; */
/* invoke-direct {v1, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V */
/* .line 1691 */
/* .local v1, "intersectBounds":Landroid/graphics/Rect; */
v2 = (( android.graphics.Rect ) v1 ).intersect ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = (( android.graphics.Rect ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v2, :cond_1 */
/* .line 1693 */
try { // :try_start_0
v2 = this.mBlurWallpaperBmp;
/* iget v4, v1, Landroid/graphics/Rect;->left:I */
/* iget v5, v1, Landroid/graphics/Rect;->top:I */
/* .line 1694 */
v6 = (( android.graphics.Rect ) v1 ).width ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->width()I
v7 = (( android.graphics.Rect ) v1 ).height ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->height()I
/* .line 1693 */
android.graphics.Bitmap .createBitmap ( v2,v4,v5,v6,v7 );
/* .line 1695 */
/* .local v2, "intersectBitmap":Landroid/graphics/Bitmap; */
v4 = /* invoke-direct {p0, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->getAverageColorBitmap(Landroid/graphics/Bitmap;)I */
/* .line 1696 */
/* .local v4, "avgColor":I */
(( android.graphics.Bitmap ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
/* .line 1697 */
com.android.internal.graphics.ColorUtils .calculateLuminance ( v4 );
/* move-result-wide v5 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* const-wide/high16 v7, 0x3fe0000000000000L # 0.5 */
/* cmpl-double v5, v5, v7 */
/* if-ltz v5, :cond_0 */
/* const/16 v3, 0x8 */
} // :cond_0
/* .line 1698 */
} // .end local v2 # "intersectBitmap":Landroid/graphics/Bitmap;
} // .end local v4 # "avgColor":I
/* :catch_0 */
/* move-exception v2 */
/* .line 1699 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "create appearance fail "; // const-string v4, "create appearance fail "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " e = "; // const-string v4, " e = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "WindowManagerService"; // const-string v4, "WindowManagerService"
android.util.Slog .e ( v4,v3 );
/* .line 1702 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_1
int v2 = -1; // const/4 v2, -0x1
} // .end method
private Boolean enableFullScreen ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 5 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 2064 */
v0 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;
/* .line 2065 */
/* .local v0, "manager":Landroid/content/pm/IPackageManager; */
v1 = this.mActivityComponent;
v1 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->getFlipCompatModeByActivity(Landroid/content/pm/IPackageManager;Landroid/content/ComponentName;)I */
/* .line 2066 */
/* .local v1, "activityMode":I */
int v2 = -1; // const/4 v2, -0x1
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* if-ne v1, v2, :cond_1 */
/* .line 2067 */
v2 = this.packageName;
v2 = /* invoke-direct {p0, v0, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->getFlipCompatModeByApp(Landroid/content/pm/IPackageManager;Ljava/lang/String;)I */
/* .line 2068 */
/* .local v2, "appMode":I */
/* if-nez v2, :cond_0 */
} // :cond_0
/* move v3, v4 */
} // :goto_0
/* .line 2070 */
} // .end local v2 # "appMode":I
} // :cond_1
/* if-nez v1, :cond_2 */
} // :cond_2
/* move v3, v4 */
} // :goto_1
} // .end method
private void enableWmsDebugConfig ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "config" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .line 1162 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enableWMSDebugConfig, config=:"; // const-string v1, "enableWMSDebugConfig, config=:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", enable=:"; // const-string v1, ", enable=:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "WindowManagerService"; // const-string v1, "WindowManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 1163 */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* goto/16 :goto_0 */
/* :sswitch_0 */
final String v0 = "DEBUG_UNKNOWN_APP_VISIBILITY"; // const-string v0, "DEBUG_UNKNOWN_APP_VISIBILITY"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x18 */
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v0 = "DEBUG_INPUT_METHOD"; // const-string v0, "DEBUG_INPUT_METHOD"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 5; // const/4 v0, 0x5
/* goto/16 :goto_1 */
/* :sswitch_2 */
final String v0 = "DEBUG_POWER"; // const-string v0, "DEBUG_POWER"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x13 */
/* goto/16 :goto_1 */
/* :sswitch_3 */
final String v0 = "DEBUG_INPUT"; // const-string v0, "DEBUG_INPUT"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* goto/16 :goto_1 */
/* :sswitch_4 */
final String v0 = "DEBUG_LAYOUT"; // const-string v0, "DEBUG_LAYOUT"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* goto/16 :goto_1 */
/* :sswitch_5 */
final String v0 = "DEBUG_LAYERS"; // const-string v0, "DEBUG_LAYERS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* goto/16 :goto_1 */
/* :sswitch_6 */
final String v0 = "DEBUG_WINDOW_CROP"; // const-string v0, "DEBUG_WINDOW_CROP"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x17 */
/* goto/16 :goto_1 */
/* :sswitch_7 */
final String v0 = "SHOW_STACK_CRAWLS"; // const-string v0, "SHOW_STACK_CRAWLS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x16 */
/* goto/16 :goto_1 */
/* :sswitch_8 */
final String v0 = "DEBUG_STARTING_WINDOW_VERBOSE"; // const-string v0, "DEBUG_STARTING_WINDOW_VERBOSE"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x8 */
/* goto/16 :goto_1 */
/* :sswitch_9 */
final String v0 = "DEBUG_LAYOUT_REPEATS"; // const-string v0, "DEBUG_LAYOUT_REPEATS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xd */
/* goto/16 :goto_1 */
/* :sswitch_a */
final String v0 = "DEBUG_TASK_MOVEMENT"; // const-string v0, "DEBUG_TASK_MOVEMENT"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xf */
/* goto/16 :goto_1 */
/* :sswitch_b */
final String v0 = "DEBUG_CONFIGURATION"; // const-string v0, "DEBUG_CONFIGURATION"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 7; // const/4 v0, 0x7
/* goto/16 :goto_1 */
/* :sswitch_c */
final String v0 = "DEBUG_ROOT_TASK"; // const-string v0, "DEBUG_ROOT_TASK"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x11 */
/* goto/16 :goto_1 */
/* :sswitch_d */
final String v0 = "SHOW_VERBOSE_TRANSACTIONS"; // const-string v0, "SHOW_VERBOSE_TRANSACTIONS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x14 */
/* goto/16 :goto_1 */
/* :sswitch_e */
final String v0 = "DEBUG_DISPLAY"; // const-string v0, "DEBUG_DISPLAY"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x12 */
/* goto/16 :goto_1 */
/* :sswitch_f */
final String v0 = "DEBUG"; // const-string v0, "DEBUG"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* goto/16 :goto_1 */
/* :sswitch_10 */
final String v0 = "DEBUG_SCREENSHOT"; // const-string v0, "DEBUG_SCREENSHOT"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xc */
/* :sswitch_11 */
final String v0 = "DEBUG_TASK_POSITIONING"; // const-string v0, "DEBUG_TASK_POSITIONING"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x10 */
/* :sswitch_12 */
final String v0 = "DEBUG_WALLPAPER"; // const-string v0, "DEBUG_WALLPAPER"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x9 */
/* :sswitch_13 */
final String v0 = "DEBUG_DRAG"; // const-string v0, "DEBUG_DRAG"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xb */
/* :sswitch_14 */
final String v0 = "DEBUG_ANIM"; // const-string v0, "DEBUG_ANIM"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_15 */
final String v0 = "DEBUG_WINDOW_TRACE"; // const-string v0, "DEBUG_WINDOW_TRACE"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xe */
/* :sswitch_16 */
final String v0 = "SHOW_LIGHT_TRANSACTIONS"; // const-string v0, "SHOW_LIGHT_TRANSACTIONS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x15 */
/* :sswitch_17 */
final String v0 = "DEBUG_WALLPAPER_LIGHT"; // const-string v0, "DEBUG_WALLPAPER_LIGHT"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xa */
/* :sswitch_18 */
final String v0 = "DEBUG_VISIBILITY"; // const-string v0, "DEBUG_VISIBILITY"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 6; // const/4 v0, 0x6
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 1188 */
/* :pswitch_0 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_UNKNOWN_APP_VISIBILITY = (p2!= 0);
/* .line 1187 */
/* :pswitch_1 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_WINDOW_CROP = (p2!= 0);
/* .line 1186 */
/* :pswitch_2 */
com.android.server.wm.WindowManagerDebugConfig.SHOW_STACK_CRAWLS = (p2!= 0);
/* .line 1185 */
/* :pswitch_3 */
com.android.server.wm.WindowManagerDebugConfig.SHOW_LIGHT_TRANSACTIONS = (p2!= 0);
/* .line 1184 */
/* :pswitch_4 */
com.android.server.wm.WindowManagerDebugConfig.SHOW_VERBOSE_TRANSACTIONS = (p2!= 0);
/* .line 1183 */
/* :pswitch_5 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_POWER = (p2!= 0);
/* .line 1182 */
/* :pswitch_6 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_DISPLAY = (p2!= 0);
/* .line 1181 */
/* :pswitch_7 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_ROOT_TASK = (p2!= 0);
/* .line 1180 */
/* :pswitch_8 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_TASK_POSITIONING = (p2!= 0);
/* .line 1179 */
/* :pswitch_9 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_TASK_MOVEMENT = (p2!= 0);
/* .line 1178 */
/* :pswitch_a */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_WINDOW_TRACE = (p2!= 0);
/* .line 1177 */
/* :pswitch_b */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_LAYOUT_REPEATS = (p2!= 0);
/* .line 1176 */
/* :pswitch_c */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_SCREENSHOT = (p2!= 0);
/* .line 1175 */
/* :pswitch_d */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_DRAG = (p2!= 0);
/* .line 1174 */
/* :pswitch_e */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_WALLPAPER_LIGHT = (p2!= 0);
/* .line 1173 */
/* :pswitch_f */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_WALLPAPER = (p2!= 0);
/* .line 1172 */
/* :pswitch_10 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_STARTING_WINDOW_VERBOSE = (p2!= 0);
/* .line 1171 */
/* :pswitch_11 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_CONFIGURATION = (p2!= 0);
/* .line 1170 */
/* :pswitch_12 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_VISIBILITY = (p2!= 0);
/* .line 1169 */
/* :pswitch_13 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_INPUT_METHOD = (p2!= 0);
/* .line 1168 */
/* :pswitch_14 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_INPUT = (p2!= 0);
/* .line 1167 */
/* :pswitch_15 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_LAYERS = (p2!= 0);
/* .line 1166 */
/* :pswitch_16 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_LAYOUT = (p2!= 0);
/* .line 1165 */
/* :pswitch_17 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG_ANIM = (p2!= 0);
/* .line 1164 */
/* :pswitch_18 */
com.android.server.wm.WindowManagerDebugConfig.DEBUG = (p2!= 0);
/* .line 1192 */
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7caac762 -> :sswitch_18 */
/* -0x644285b3 -> :sswitch_17 */
/* -0x5ff91180 -> :sswitch_16 */
/* -0x541dd15e -> :sswitch_15 */
/* -0x4f0b4363 -> :sswitch_14 */
/* -0x4f09d840 -> :sswitch_13 */
/* -0x44802d2a -> :sswitch_12 */
/* -0x27886d35 -> :sswitch_11 */
/* -0x933a50e -> :sswitch_10 */
/* 0x3de9e33 -> :sswitch_f */
/* 0x27030696 -> :sswitch_e */
/* 0x27931d14 -> :sswitch_d */
/* 0x31af5fb6 -> :sswitch_c */
/* 0x37e7a7ca -> :sswitch_b */
/* 0x4796751d -> :sswitch_a */
/* 0x4bab218f -> :sswitch_9 */
/* 0x4c2f9be6 -> :sswitch_8 */
/* 0x4d5f5ce5 -> :sswitch_7 */
/* 0x58185973 -> :sswitch_6 */
/* 0x58cd58ce -> :sswitch_5 */
/* 0x58cd7eb6 -> :sswitch_4 */
/* 0x6e13ae9e -> :sswitch_3 */
/* 0x6e76dfd9 -> :sswitch_2 */
/* 0x78cf05a2 -> :sswitch_1 */
/* 0x7d7e2051 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Integer getAverageColorBitmap ( android.graphics.Bitmap p0 ) {
/* .locals 10 */
/* .param p1, "bitmap" # Landroid/graphics/Bitmap; */
/* .line 1741 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1742 */
/* .local v0, "redColors":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 1743 */
/* .local v1, "greenColors":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 1744 */
/* .local v2, "blueColors":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 1745 */
/* .local v3, "alphaColor":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 1747 */
/* .local v4, "pixelCount":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "y":I */
} // :goto_0
v6 = (( android.graphics.Bitmap ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
/* if-ge v5, v6, :cond_1 */
/* .line 1748 */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "x":I */
} // :goto_1
v7 = (( android.graphics.Bitmap ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I
/* if-ge v6, v7, :cond_0 */
/* .line 1749 */
v7 = (( android.graphics.Bitmap ) p1 ).getPixel ( v6, v5 ); // invoke-virtual {p1, v6, v5}, Landroid/graphics/Bitmap;->getPixel(II)I
/* .line 1750 */
/* .local v7, "c":I */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1751 */
v8 = android.graphics.Color .red ( v7 );
/* add-int/2addr v0, v8 */
/* .line 1752 */
v8 = android.graphics.Color .green ( v7 );
/* add-int/2addr v1, v8 */
/* .line 1753 */
v8 = android.graphics.Color .blue ( v7 );
/* add-int/2addr v2, v8 */
/* .line 1754 */
v8 = android.graphics.Color .alpha ( v7 );
/* add-int/2addr v3, v8 */
/* .line 1748 */
} // .end local v7 # "c":I
/* add-int/lit8 v6, v6, 0x1 */
/* .line 1747 */
} // .end local v6 # "x":I
} // :cond_0
/* add-int/lit8 v5, v5, 0x1 */
/* .line 1757 */
} // .end local v5 # "y":I
} // :cond_1
/* div-int v5, v0, v4 */
/* .line 1758 */
/* .local v5, "red":I */
/* div-int v6, v1, v4 */
/* .line 1759 */
/* .local v6, "green":I */
/* div-int v7, v2, v4 */
/* .line 1760 */
/* .local v7, "blue":I */
/* div-int v8, v3, v4 */
/* .line 1761 */
/* .local v8, "alpha":I */
v9 = android.graphics.Color .argb ( v8,v5,v6,v7 );
} // .end method
private com.android.server.wm.DisplayContent getDefaultDisplayContent ( ) {
/* .locals 2 */
/* .line 1013 */
v0 = this.mDisplayContent;
/* if-nez v0, :cond_0 */
/* .line 1014 */
v0 = this.mWmService;
v0 = this.mRoot;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).getDisplayContent ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
this.mDisplayContent = v0;
/* .line 1016 */
} // :cond_0
v0 = this.mDisplayContent;
} // .end method
private Integer getFlipCompatModeByActivity ( android.content.pm.IPackageManager p0, android.content.ComponentName p1 ) {
/* .locals 4 */
/* .param p1, "manager" # Landroid/content/pm/IPackageManager; */
/* .param p2, "componentName" # Landroid/content/ComponentName; */
/* .line 2086 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2088 */
/* .local v0, "activityInfo":Landroid/content/pm/ActivityInfo; */
/* nop */
/* .line 2089 */
try { // :try_start_0
v1 = android.os.UserHandle .myUserId ( );
/* .line 2088 */
/* const-wide/16 v2, 0x80 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 2092 */
/* .line 2090 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2091 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
final String v3 = "getActivityInfo fail"; // const-string v3, "getActivityInfo fail"
android.util.Slog .e ( v2,v3,v1 );
/* .line 2093 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getFullScreenValue(Landroid/content/pm/PackageItemInfo;)I */
} // .end method
private Integer getFlipCompatModeByApp ( android.content.pm.IPackageManager p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "manager" # Landroid/content/pm/IPackageManager; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 2075 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2077 */
/* .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
/* nop */
/* .line 2078 */
try { // :try_start_0
v1 = android.os.UserHandle .myUserId ( );
/* .line 2077 */
/* const-wide/16 v2, 0x80 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 2081 */
/* .line 2079 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2080 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
final String v3 = "getApplicationInfo fail"; // const-string v3, "getApplicationInfo fail"
android.util.Slog .e ( v2,v3,v1 );
/* .line 2082 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getFullScreenValue(Landroid/content/pm/PackageItemInfo;)I */
} // .end method
private Integer getFullScreenValue ( android.content.pm.PackageItemInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Landroid/content/pm/PackageItemInfo; */
/* .line 2097 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.metaData;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.metaData;
final String v1 = "miui.supportFlipFullScreen"; // const-string v1, "miui.supportFlipFullScreen"
v0 = (( android.os.Bundle ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2099 */
v0 = this.metaData;
v0 = (( android.os.Bundle ) v0 ).getInt ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 2101 */
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
} // .end method
public static com.android.server.wm.WindowManagerServiceImpl getInstance ( ) {
/* .locals 1 */
/* .line 352 */
com.android.server.wm.WindowManagerServiceStub .get ( );
/* check-cast v0, Lcom/android/server/wm/WindowManagerServiceImpl; */
} // .end method
public static Boolean getLastFrame ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "name" # Ljava/lang/String; */
/* .line 1069 */
final String v0 = "Splash Screen com.android.incallui"; // const-string v0, "Splash Screen com.android.incallui"
v0 = (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 1070 */
final String v0 = "com.android.incallui/com.android.incallui.InCallActivity"; // const-string v0, "com.android.incallui/com.android.incallui.InCallActivity"
v0 = (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 1071 */
final String v0 = "com.tencent.mobileqq/com.tencent.av.ui.AVActivity"; // const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.AVActivity"
v0 = (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 1072 */
final String v0 = "com.tencent.mobileqq/com.tencent.av.ui.AVLoadingDialogActivity"; // const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.AVLoadingDialogActivity"
v0 = (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 1073 */
final String v0 = "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"; // const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"
v0 = (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 1074 */
final String v0 = "Splash Screen com.tencent.mm"; // const-string v0, "Splash Screen com.tencent.mm"
v0 = (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 1075 */
final String v0 = "com.tencent.mm/com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v0, "com.tencent.mm/com.tencent.mm.plugin.voip.ui.VideoActivity"
v0 = (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 1076 */
final String v0 = "com.google.android.dialer/com.android.incallui.InCallActivity"; // const-string v0, "com.google.android.dialer/com.android.incallui.InCallActivity"
v0 = (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 1077 */
final String v0 = "com.whatsapp/com.whatsapp.voipcalling.VoipActivityV2"; // const-string v0, "com.whatsapp/com.whatsapp.voipcalling.VoipActivityV2"
v0 = (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1080 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1078 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public static java.util.List getProjectionBlackList ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1033 */
v0 = v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
/* if-nez v0, :cond_0 */
/* .line 1034 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "StatusBar"; // const-string v1, "StatusBar"
/* .line 1035 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "Splash Screen com.android.incallui"; // const-string v1, "Splash Screen com.android.incallui"
/* .line 1036 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.android.incallui/com.android.incallui.InCallActivity"; // const-string v1, "com.android.incallui/com.android.incallui.InCallActivity"
/* .line 1037 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "FloatAssistantView"; // const-string v1, "FloatAssistantView"
/* .line 1038 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "MiuiFreeformBorderView"; // const-string v1, "MiuiFreeformBorderView"
/* .line 1039 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "SnapshotStartingWindow for"; // const-string v1, "SnapshotStartingWindow for"
/* .line 1040 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "ScreenshotThumbnail"; // const-string v1, "ScreenshotThumbnail"
/* .line 1041 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.milink.ui.activity.ScreeningConsoleWindow"; // const-string v1, "com.milink.ui.activity.ScreeningConsoleWindow"
/* .line 1042 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "FloatNotificationPanel"; // const-string v1, "FloatNotificationPanel"
/* .line 1043 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.tencent.mobileqq/com.tencent.av.ui.AVActivity"; // const-string v1, "com.tencent.mobileqq/com.tencent.av.ui.AVActivity"
/* .line 1044 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.tencent.mobileqq/com.tencent.av.ui.AVLoadingDialogActivity"; // const-string v1, "com.tencent.mobileqq/com.tencent.av.ui.AVLoadingDialogActivity"
/* .line 1045 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"; // const-string v1, "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"
/* .line 1046 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.tencent.mobileqq/.FloatingWindow"; // const-string v1, "com.tencent.mobileqq/.FloatingWindow"
/* .line 1047 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "Splash Screen com.tencent.mm"; // const-string v1, "Splash Screen com.tencent.mm"
/* .line 1048 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.tencent.mm/com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v1, "com.tencent.mm/com.tencent.mm.plugin.voip.ui.VideoActivity"
/* .line 1049 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.tencent.mm/.FloatingWindow"; // const-string v1, "com.tencent.mm/.FloatingWindow"
/* .line 1050 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.whatsapp/com.whatsapp.voipcalling.VoipActivityV2"; // const-string v1, "com.whatsapp/com.whatsapp.voipcalling.VoipActivityV2"
/* .line 1051 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.google.android.dialer/com.android.incallui.InCallActivity"; // const-string v1, "com.google.android.dialer/com.android.incallui.InCallActivity"
/* .line 1052 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.google.android.dialer/.FloatingWindow"; // const-string v1, "com.google.android.dialer/.FloatingWindow"
/* .line 1053 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.miui.yellowpage/com.miui.yellowpage.activity.MarkNumberActivity"; // const-string v1, "com.miui.yellowpage/com.miui.yellowpage.activity.MarkNumberActivity"
/* .line 1054 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.miui.securitycenter/.FloatingWindow"; // const-string v1, "com.miui.securitycenter/.FloatingWindow"
/* .line 1055 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.milink.service.ui.PrivateWindow"; // const-string v1, "com.milink.service.ui.PrivateWindow"
/* .line 1056 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "com.milink.ui.activity.NFCLoadingActivity"; // const-string v1, "com.milink.ui.activity.NFCLoadingActivity"
/* .line 1057 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "Freeform-OverLayView"; // const-string v1, "Freeform-OverLayView"
/* .line 1058 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "Freeform-HotSpotView"; // const-string v1, "Freeform-HotSpotView"
/* .line 1059 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
final String v1 = "Freeform-TipView"; // const-string v1, "Freeform-TipView"
/* .line 1061 */
} // :cond_0
v0 = com.android.server.wm.WindowManagerServiceImpl.mProjectionBlackList;
} // .end method
private Boolean isDarkModeContrastEnable ( ) {
/* .locals 3 */
/* .line 1352 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* iget v0, v0, Landroid/content/res/Configuration;->uiMode:I */
/* and-int/lit8 v0, v0, 0x30 */
/* const/16 v1, 0x20 */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_0 */
v0 = this.mContext;
/* .line 1354 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_contrast_enable"; // const-string v1, "dark_mode_contrast_enable"
v0 = android.provider.MiuiSettings$System .getBoolean ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* nop */
} // :goto_0
/* move v0, v2 */
/* .line 1356 */
/* .local v0, "isContrastEnable":Z */
} // .end method
private Boolean isDensityForced ( ) {
/* .locals 1 */
/* .line 836 */
v0 = (( com.android.server.wm.WindowManagerServiceImpl ) p0 ).getForcedDensity ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getForcedDensity()I
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isFindDeviceFlagUsePermitted ( Integer p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 721 */
v0 = android.text.TextUtils .isEmpty ( p2 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 723 */
} // :cond_0
android.app.AppGlobals .getPackageManager ( );
/* .line 724 */
/* .local v0, "pm":Landroid/content/pm/IPackageManager; */
/* if-nez v0, :cond_1 */
/* .line 727 */
} // :cond_1
try { // :try_start_0
final String v2 = "android"; // const-string v2, "android"
v2 = v3 = android.os.UserHandle .getUserId ( p1 );
int v3 = 1; // const/4 v3, 0x1
/* if-nez v2, :cond_2 */
/* .line 728 */
/* .line 731 */
} // :cond_2
v2 = android.os.UserHandle .getUserId ( p1 );
/* const-wide/16 v4, 0x0 */
/* .line 732 */
/* .local v2, "ai":Landroid/content/pm/ApplicationInfo; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* iget v4, v2, Landroid/content/pm/ApplicationInfo;->flags:I */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* and-int/2addr v4, v3 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 736 */
} // .end local v2 # "ai":Landroid/content/pm/ApplicationInfo;
} // :cond_3
/* .line 734 */
/* :catch_0 */
/* move-exception v2 */
/* .line 737 */
} // :goto_0
} // .end method
private Boolean isSupportSwitchResoluton ( ) {
/* .locals 4 */
/* .line 899 */
int v0 = 0; // const/4 v0, 0x0
/* .line 900 */
/* .local v0, "supportSwitchResolution":Z */
/* const-string/jumbo v1, "screen_resolution_supported" */
miui.util.FeatureParser .getIntArray ( v1 );
/* .line 901 */
/* .local v1, "mScreenResolutionsSupported":[I */
if ( v1 != null) { // if-eqz v1, :cond_0
/* array-length v2, v1 */
int v3 = 1; // const/4 v3, 0x1
/* if-le v2, v3, :cond_0 */
/* .line 902 */
int v0 = 1; // const/4 v0, 0x1
/* .line 904 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isSupportSwitchResoluton:"; // const-string v3, "isSupportSwitchResoluton:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "WindowManagerService"; // const-string v3, "WindowManagerService"
android.util.Slog .i ( v3,v2 );
/* .line 905 */
} // .end method
private void killFreeformForFindDeviceKeyguardIfNecessary ( Integer p0, android.view.WindowManager$LayoutParams p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 682 */
if ( p3 != null) { // if-eqz p3, :cond_2
/* if-nez p2, :cond_0 */
/* .line 685 */
} // :cond_0
v0 = com.android.server.wm.WindowManagerServiceImpl.FIND_DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->isFindDeviceFlagUsePermitted(ILjava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* and-int/lit16 v0, v0, 0x800 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 687 */
v0 = this.mWmService;
v0 = this.mAtmService;
v0 = this.mMiuiFreeFormManagerService;
/* .line 689 */
} // :cond_1
return;
/* .line 683 */
} // :cond_2
} // :goto_0
return;
} // .end method
static void lambda$bindFloatwindowAndTargetTask$6 ( android.view.WindowManager$LayoutParams p0, java.util.ArrayList p1, com.android.server.wm.Task p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p1, "targetTaskList" # Ljava/util/ArrayList; */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .line 2141 */
(( com.android.server.wm.Task ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;
/* .line 2142 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = this.packageName;
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2143 */
(( java.util.ArrayList ) p1 ).add ( p2 ); // invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2145 */
} // :cond_0
return;
} // .end method
private Boolean lambda$notAllowCaptureDisplay$5 ( com.android.server.wm.WindowState p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 1811 */
v0 = (( com.android.server.wm.WindowState ) p1 ).isVisible ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->isVisible()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mWinAnimator;
v0 = (( com.android.server.wm.WindowStateAnimator ) v0 ).hasSurface ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowStateAnimator;->hasSurface()Z
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = (( com.android.server.wm.WindowState ) p1 ).isOnScreen ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->isOnScreen()Z
/* if-nez v0, :cond_0 */
/* .line 1814 */
} // :cond_0
v0 = this.mAttrs;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* and-int/lit16 v0, v0, 0x2000 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1815 */
(( com.android.server.wm.WindowState ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getRootTask()Lcom/android/server/wm/Task;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mWmService;
v0 = this.mAtmService;
v0 = this.mMiuiFreeFormManagerService;
/* .line 1816 */
(( com.android.server.wm.WindowState ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getRootTask()Lcom/android/server/wm/Task;
v0 = /* iget v2, v2, Lcom/android/server/wm/Task;->mTaskId:I */
/* if-nez v0, :cond_2 */
/* .line 1817 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " Secure window Can\'t CaptureDisplay return ! "; // const-string v1, " Secure window Can\'t CaptureDisplay return ! "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "WindowManagerService"; // const-string v1, "WindowManagerService"
android.util.Slog .e ( v1,v0 );
/* .line 1818 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1820 */
} // :cond_2
/* .line 1812 */
} // :cond_3
} // :goto_0
} // .end method
private void lambda$notifySystemBrightnessChange$3 ( com.android.server.wm.Task p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 1541 */
(( com.android.server.wm.Task ) p1 ).getTopVisibleAppMainWindow ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopVisibleAppMainWindow()Lcom/android/server/wm/WindowState;
/* invoke-direct {p0, v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->notifySystemBrightnessChange(Lcom/android/server/wm/WindowState;)V */
/* .line 1542 */
return;
} // .end method
private void lambda$notifySystemBrightnessChange$4 ( ) { //synthethic
/* .locals 3 */
/* .line 1535 */
v0 = this.mWmService;
v0 = this.mRoot;
(( com.android.server.wm.RootWindowContainer ) v0 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 1537 */
/* .local v0, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea; */
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 1538 */
try { // :try_start_0
(( com.android.server.wm.TaskDisplayArea ) v0 ).getVisibleTasks ( ); // invoke-virtual {v0}, Lcom/android/server/wm/TaskDisplayArea;->getVisibleTasks()Ljava/util/ArrayList;
/* .line 1539 */
/* .local v2, "visibleTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;" */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1540 */
/* new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V */
(( java.util.ArrayList ) v2 ).forEach ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V
/* .line 1543 */
return;
/* .line 1539 */
} // .end local v2 # "visibleTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;"
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
private void lambda$setRoundedCornerOverlaysCanScreenShot$0 ( Boolean p0, com.android.server.wm.WindowState p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "canScreenshot" # Z */
/* .param p2, "w" # Lcom/android/server/wm/WindowState; */
/* .line 860 */
v0 = this.mToken;
/* iget-boolean v0, v0, Lcom/android/server/wm/WindowToken;->mRoundedCornerOverlay:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.wm.WindowState ) p2 ).isVisible ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->isVisible()Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mWinAnimator;
v0 = (( com.android.server.wm.WindowStateAnimator ) v0 ).hasSurface ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowStateAnimator;->hasSurface()Z
/* if-nez v0, :cond_0 */
/* .line 863 */
} // :cond_0
(( com.android.server.wm.WindowState ) p2 ).getClientViewRootSurface ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getClientViewRootSurface()Landroid/view/SurfaceControl;
/* .line 864 */
/* .local v0, "clientSurfaceControl":Landroid/view/SurfaceControl; */
/* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->setSurfaceCanScreenShot(Landroid/view/SurfaceControl;Z)V */
/* .line 865 */
return;
/* .line 861 */
} // .end local v0 # "clientSurfaceControl":Landroid/view/SurfaceControl;
} // :cond_1
} // :goto_0
return;
} // .end method
private void lambda$updateScreenShareProjectFlag$1 ( com.android.server.wm.WindowState p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 1390 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.mWinAnimator;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mWinAnimator;
v0 = this.mSurfaceController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1391 */
v0 = this.mWinAnimator;
v0 = this.mSurfaceController;
v0 = this.mSurfaceControl;
/* .line 1392 */
/* .local v0, "surfaceControl":Landroid/view/SurfaceControl; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1393 */
v1 = this.mWinAnimator;
v1 = (( com.android.server.wm.WindowManagerServiceImpl ) p0 ).getScreenShareProjectAndPrivateCastFlag ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->getScreenShareProjectAndPrivateCastFlag(Lcom/android/server/wm/WindowStateAnimator;)I
/* .line 1394 */
/* .local v1, "flags":I */
(( android.view.SurfaceControl ) v0 ).setScreenProjection ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl;->setScreenProjection(I)V
/* .line 1398 */
} // .end local v0 # "surfaceControl":Landroid/view/SurfaceControl;
} // .end local v1 # "flags":I
} // :cond_0
return;
} // .end method
private void lambda$updateScreenShareProjectFlag$2 ( ) { //synthethic
/* .locals 4 */
/* .line 1387 */
v0 = this.mWmService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 1389 */
try { // :try_start_0
v1 = this.mWmService;
v1 = this.mRoot;
/* new-instance v2, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v2, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.wm.RootWindowContainer ) v1 ).forAllWindows ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/RootWindowContainer;->forAllWindows(Ljava/util/function/Consumer;Z)V
/* .line 1400 */
/* monitor-exit v0 */
/* .line 1401 */
return;
/* .line 1400 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void notifySystemBrightnessChange ( com.android.server.wm.WindowState p0 ) {
/* .locals 3 */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 1550 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.mAttrs;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F */
/* const/high16 v1, -0x40800000 # -1.0f */
/* cmpl-float v0, v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mTaskIdScreenBrightnessOverrides;
/* .line 1551 */
(( com.android.server.wm.WindowState ) p1 ).getTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
/* iget v1, v1, Lcom/android/server/wm/Task;->mTaskId:I */
v0 = (( android.util.SparseArray ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->contains(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1554 */
} // :cond_0
v0 = this.mTaskIdScreenBrightnessOverrides;
(( com.android.server.wm.WindowState ) p1 ).getTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
/* iget v1, v1, Lcom/android/server/wm/Task;->mTaskId:I */
v2 = this.mAttrs;
/* iget v2, v2, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F */
java.lang.Float .valueOf ( v2 );
(( android.util.SparseArray ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1557 */
v0 = this.mWmService;
v0 = this.mPowerManagerInternal;
/* .line 1558 */
/* const/high16 v1, 0x7fc00000 # Float.NaN */
(( android.os.PowerManagerInternal ) v0 ).setScreenBrightnessOverrideFromWindowManager ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/PowerManagerInternal;->setScreenBrightnessOverrideFromWindowManager(F)V
/* .line 1559 */
return;
/* .line 1552 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void registerBootCompletedReceiver ( ) {
/* .locals 3 */
/* .line 357 */
/* new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$2;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V */
this.mWallpaperCallback = v0;
/* .line 371 */
/* new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$3;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V */
this.mBootCompletedReceiver = v0;
/* .line 378 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
/* invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 379 */
/* .local v0, "bootCompletedFilter":Landroid/content/IntentFilter; */
v1 = this.mContext;
v2 = this.mBootCompletedReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 380 */
return;
} // .end method
private void removeFindDeviceKeyguardFlagsIfNecessary ( Integer p0, android.view.WindowManager$LayoutParams p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 705 */
/* iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* and-int/lit16 v0, v0, 0x800 */
/* if-nez v0, :cond_0 */
/* iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* and-int/lit16 v0, v0, 0x1000 */
/* if-nez v0, :cond_0 */
/* .line 707 */
return;
/* .line 710 */
} // :cond_0
v0 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->isFindDeviceFlagUsePermitted(ILjava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* and-int/lit16 v0, v0, 0x800 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 712 */
final String v0 = "com.xiaomi.finddevice"; // const-string v0, "com.xiaomi.finddevice"
v0 = (( java.lang.String ) v0 ).equals ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 713 */
} // :cond_1
return;
/* .line 716 */
} // :cond_2
/* iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* and-int/lit16 v0, v0, -0x801 */
/* iput v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* .line 717 */
/* iget v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* and-int/lit16 v0, v0, -0x1001 */
/* iput v0, p2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* .line 718 */
return;
} // .end method
public static void setAlertWindowTitle ( android.view.WindowManager$LayoutParams p0 ) {
/* .locals 2 */
/* .param p0, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 1084 */
/* iget v0, p0, Landroid/view/WindowManager$LayoutParams;->type:I */
v0 = android.view.WindowManager$LayoutParams .isSystemAlertWindowType ( v0 );
/* if-nez v0, :cond_0 */
/* .line 1085 */
return;
/* .line 1088 */
} // :cond_0
v0 = com.android.server.wm.WindowManagerServiceImpl.QQ;
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1089 */
v0 = com.android.server.wm.WindowManagerServiceImpl.QQ_FLOATING;
(( android.view.WindowManager$LayoutParams ) p0 ).setTitle ( v0 ); // invoke-virtual {p0, v0}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 1091 */
} // :cond_1
v0 = com.android.server.wm.WindowManagerServiceImpl.MM;
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1092 */
v0 = com.android.server.wm.WindowManagerServiceImpl.MM_FLOATING;
(( android.view.WindowManager$LayoutParams ) p0 ).setTitle ( v0 ); // invoke-virtual {p0, v0}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 1094 */
} // :cond_2
v0 = com.android.server.wm.WindowManagerServiceImpl.GOOGLE;
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1095 */
v0 = com.android.server.wm.WindowManagerServiceImpl.GOOGLE_FLOATING;
(( android.view.WindowManager$LayoutParams ) p0 ).setTitle ( v0 ); // invoke-virtual {p0, v0}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 1097 */
} // :cond_3
v0 = com.android.server.wm.WindowManagerServiceImpl.SECURITY;
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1098 */
v0 = com.android.server.wm.WindowManagerServiceImpl.SECURITY_FLOATING;
(( android.view.WindowManager$LayoutParams ) p0 ).setTitle ( v0 ); // invoke-virtual {p0, v0}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 1100 */
} // :cond_4
return;
} // .end method
private void setDefaultBlurConfigIfNeeded ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 1836 */
/* sget-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->BACKGROUND_BLUR_SUPPORTED:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1837 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "background_blur_enable"; // const-string v1, "background_blur_enable"
int v2 = -1; // const/4 v2, -0x1
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* if-ne v0, v2, :cond_0 */
/* sget-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->BACKGROUND_BLUR_STATUS_DEFAULT:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1840 */
final String v0 = "WindowManagerService"; // const-string v0, "WindowManagerService"
/* const-string/jumbo v2, "set default background blur config to true" */
android.util.Slog .d ( v0,v2 );
/* .line 1841 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = 1; // const/4 v2, 0x1
android.provider.Settings$Secure .putInt ( v0,v1,v2 );
/* .line 1843 */
} // :cond_0
return;
} // .end method
public static void setProjectionBlackList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1065 */
/* .local p0, "blacklist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .line 1066 */
return;
} // .end method
private void setRoundedCornerOverlaysCanScreenShot ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "canScreenshot" # Z */
/* .line 859 */
v0 = this.mDisplayContent;
/* new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;Z)V */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.DisplayContent ) v0 ).forAllWindows ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/DisplayContent;->forAllWindows(Ljava/util/function/Consumer;Z)V
/* .line 866 */
return;
} // .end method
private void setSurfaceCanScreenShot ( android.view.SurfaceControl p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "clientSurfaceControl" # Landroid/view/SurfaceControl; */
/* .param p2, "canScreenshot" # Z */
/* .line 869 */
/* if-nez p1, :cond_0 */
/* .line 870 */
return;
/* .line 872 */
} // :cond_0
/* xor-int/lit8 v0, p2, 0x1 */
(( android.view.SurfaceControl ) p1 ).setSkipScreenshot ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/SurfaceControl;->setSkipScreenshot(Z)V
/* .line 873 */
v0 = this.mWmService;
(( com.android.server.wm.WindowManagerService ) v0 ).requestTraversal ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->requestTraversal()V
/* .line 874 */
return;
} // .end method
private void switchResolutionInternal ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "width" # I */
/* .param p2, "height" # I */
/* .line 789 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 790 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* monitor-exit v0 */
return;
/* .line 791 */
} // :cond_0
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->calcDensityAndUpdateForceDensityIfNeed(I)I */
/* .line 792 */
/* .local v1, "density":I */
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "start switching resolution, width:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ",height:"; // const-string v4, ",height:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ",density:"; // const-string v4, ",density:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 794 */
/* iput p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I */
/* .line 795 */
/* iput p2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I */
/* .line 796 */
/* iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayDensity:I */
/* .line 797 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z */
/* .line 798 */
/* invoke-direct {p0, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->setRoundedCornerOverlaysCanScreenShot(Z)V */
/* .line 799 */
final String v2 = "persist.sys.miui_resolution"; // const-string v2, "persist.sys.miui_resolution"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ","; // const-string v4, ","
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ","; // const-string v4, ","
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayDensity:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.os.SystemProperties .set ( v2,v3 );
/* .line 801 */
v2 = this.mWmService;
v2 = this.mAtmService;
v2 = this.mH;
v3 = this.mWmService;
v3 = this.mAmInternal;
java.util.Objects .requireNonNull ( v3 );
/* new-instance v4, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda7; */
/* invoke-direct {v4, v3}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda7;-><init>(Landroid/app/ActivityManagerInternal;)V */
(( com.android.server.wm.ActivityTaskManagerService$H ) v2 ).post ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 804 */
v2 = this.mSmartPowerServiceInternal;
/* .line 808 */
v2 = this.mDisplayContent;
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_1 */
/* .line 809 */
v2 = this.mWmService;
v2 = this.mRoot;
(( com.android.server.wm.RootWindowContainer ) v2 ).getDisplayContent ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
this.mDisplayContent = v2;
/* .line 813 */
} // :cond_1
v2 = this.mDisplayContent;
/* invoke-direct {p0, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->updateScreenResolutionLocked(Lcom/android/server/wm/DisplayContent;)V */
/* .line 814 */
/* iget-boolean v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportActiveModeSwitch:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 815 */
com.android.server.display.mode.DisplayModeDirectorStub .getInstance ( );
/* iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I */
/* iget v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I */
/* .line 818 */
} // .end local v1 # "density":I
} // :cond_2
/* monitor-exit v0 */
/* .line 819 */
return;
/* .line 818 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean topRunningActivityChange ( com.android.server.wm.WindowState p0 ) {
/* .locals 5 */
/* .param p1, "win" # Lcom/android/server/wm/WindowState; */
/* .line 1288 */
/* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getDefaultDisplayContent()Lcom/android/server/wm/DisplayContent; */
(( com.android.server.wm.DisplayContent ) v0 ).topRunningActivity ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 1289 */
/* .local v0, "topRunningActivity":Lcom/android/server/wm/ActivityRecord; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1290 */
} // :cond_0
v2 = (( com.android.server.wm.ActivityRecord ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I
/* .line 1291 */
/* .local v2, "windowingMode":I */
v3 = this.mActivityComponent;
(( android.content.ComponentName ) v3 ).flattenToString ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
/* .line 1292 */
/* .local v3, "flattenToString":Ljava/lang/String; */
(( com.android.server.wm.WindowState ) p1 ).getWindowTag ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;
v4 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_1 */
v4 = (( com.android.server.wm.WindowManagerServiceImpl ) p0 ).isKeyGuardShowing ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isKeyGuardShowing()Z
if ( v4 != null) { // if-eqz v4, :cond_2
} // :cond_1
int v4 = 5; // const/4 v4, 0x5
/* if-eq v2, v4, :cond_2 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
} // .end method
private void updateAppearance ( ) {
/* .locals 8 */
/* .line 1706 */
v0 = this.mWmService;
v0 = this.mRoot;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).getDisplayContent ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
/* .line 1708 */
/* .local v0, "displayContent":Lcom/android/server/wm/DisplayContent; */
if ( v0 != null) { // if-eqz v0, :cond_6
v2 = this.mBlurWallpaperBmp;
/* if-nez v2, :cond_0 */
/* goto/16 :goto_0 */
/* .line 1712 */
} // :cond_0
(( com.android.server.wm.DisplayContent ) v0 ).getDisplayPolicy ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;
(( com.android.server.wm.DisplayPolicy ) v2 ).getStatusBar ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayPolicy;->getStatusBar()Lcom/android/server/wm/WindowState;
/* .line 1714 */
/* .local v2, "statusBar":Lcom/android/server/wm/WindowState; */
/* if-nez v2, :cond_1 */
/* .line 1715 */
return;
/* .line 1718 */
} // :cond_1
(( com.android.server.wm.WindowState ) v2 ).getFrame ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getFrame()Landroid/graphics/Rect;
/* .line 1719 */
/* .local v3, "statusBarFrame":Landroid/graphics/Rect; */
v4 = this.mBlurWallpaperBmp;
v4 = (( android.graphics.Bitmap ) v4 ).getWidth ( ); // invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I
/* .line 1720 */
/* .local v4, "width":I */
v5 = (( android.graphics.Rect ) v3 ).height ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->height()I
v6 = this.mBlurWallpaperBmp;
v6 = (( android.graphics.Bitmap ) v6 ).getHeight ( ); // invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I
v5 = java.lang.Math .min ( v5,v6 );
/* .line 1721 */
/* .local v5, "height":I */
v6 = this.leftStatusBounds;
v6 = (( android.graphics.Rect ) v6 ).isEmpty ( ); // invoke-virtual {v6}, Landroid/graphics/Rect;->isEmpty()Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 1722 */
/* new-instance v6, Landroid/graphics/Rect; */
/* div-int/lit8 v7, v4, 0x4 */
/* invoke-direct {v6, v1, v1, v7, v5}, Landroid/graphics/Rect;-><init>(IIII)V */
this.leftStatusBounds = v6;
/* .line 1724 */
} // :cond_2
v6 = this.rightStatusBounds;
v6 = (( android.graphics.Rect ) v6 ).isEmpty ( ); // invoke-virtual {v6}, Landroid/graphics/Rect;->isEmpty()Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 1725 */
/* new-instance v6, Landroid/graphics/Rect; */
/* mul-int/lit8 v7, v4, 0x3 */
/* div-int/lit8 v7, v7, 0x4 */
/* invoke-direct {v6, v7, v1, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V */
this.rightStatusBounds = v6;
/* .line 1728 */
} // :cond_3
v1 = this.leftStatusBounds;
v1 = (( android.graphics.Rect ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v1, :cond_4 */
/* .line 1729 */
v1 = this.leftStatusBounds;
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->createAppearance(Landroid/graphics/Rect;)I */
/* iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I */
/* .line 1731 */
} // :cond_4
v1 = this.rightStatusBounds;
v1 = (( android.graphics.Rect ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v1, :cond_5 */
/* .line 1732 */
v1 = this.rightStatusBounds;
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->createAppearance(Landroid/graphics/Rect;)I */
/* iput v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I */
/* .line 1734 */
} // :cond_5
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "updateAppearance left" */
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.leftStatusBounds;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " right"; // const-string v6, " right"
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.rightStatusBounds;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " appearance = "; // const-string v6, " appearance = "
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I */
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", "; // const-string v6, ", "
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I */
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "WindowManagerService"; // const-string v6, "WindowManagerService"
android.util.Slog .d ( v6,v1 );
/* .line 1738 */
return;
/* .line 1709 */
} // .end local v2 # "statusBar":Lcom/android/server/wm/WindowState;
} // .end local v3 # "statusBarFrame":Landroid/graphics/Rect;
} // .end local v4 # "width":I
} // .end local v5 # "height":I
} // :cond_6
} // :goto_0
return;
} // .end method
private void updateScreenResolutionLocked ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 9 */
/* .param p1, "dc" # Lcom/android/server/wm/DisplayContent; */
/* .line 840 */
/* iget v0, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayWidth:I */
/* iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v1, :cond_1 */
/* iget v0, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayHeight:I */
/* iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I */
/* if-eq v0, v1, :cond_0 */
} // :cond_0
/* move v0, v2 */
} // :cond_1
} // :goto_0
/* move v0, v3 */
} // :goto_1
/* iput-boolean v0, p1, Lcom/android/server/wm/DisplayContent;->mIsSizeForced:Z */
/* .line 842 */
/* iget v0, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayDensity:I */
/* iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayDensity:I */
/* if-eq v0, v1, :cond_2 */
/* move v2, v3 */
} // :cond_2
/* iput-boolean v2, p1, Lcom/android/server/wm/DisplayContent;->mIsDensityForced:Z */
/* .line 843 */
/* iget v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I */
/* iget v5, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayHeight:I */
/* iget v6, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayDensity:I */
/* iget v7, p1, Lcom/android/server/wm/DisplayContent;->mBaseDisplayPhysicalXDpi:F */
/* iget v8, p1, Lcom/android/server/wm/DisplayContent;->mBaseDisplayPhysicalYDpi:F */
/* move-object v3, p1 */
/* invoke-virtual/range {v3 ..v8}, Lcom/android/server/wm/DisplayContent;->updateBaseDisplayMetrics(IIIFF)V */
/* .line 845 */
(( com.android.server.wm.DisplayContent ) p1 ).reconfigureDisplayLocked ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->reconfigureDisplayLocked()V
/* .line 846 */
return;
} // .end method
/* # virtual methods */
public void addMiuiPaperContrastOverlay ( java.util.ArrayList p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/view/SurfaceControl;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1915 */
/* .local p1, "excludeLayersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/SurfaceControl;>;" */
v0 = android.os.Binder .getCallingUid ( );
/* const/16 v1, 0x3e8 */
/* if-eq v0, v1, :cond_0 */
/* .line 1916 */
com.android.server.wm.MiuiPaperContrastOverlayStub .get ( );
(( com.android.server.wm.MiuiPaperContrastOverlayStub ) v0 ).getMiuiPaperSurfaceControl ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStub;->getMiuiPaperSurfaceControl()Landroid/view/SurfaceControl;
/* .line 1917 */
/* .local v0, "miuiPaperContrastOverlay":Landroid/view/SurfaceControl; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1918 */
this.mMiuiPaperContrastOverlay = v0;
/* .line 1919 */
(( java.util.ArrayList ) p1 ).add ( v0 ); // invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1922 */
} // .end local v0 # "miuiPaperContrastOverlay":Landroid/view/SurfaceControl;
} // :cond_0
return;
} // .end method
public void addSecureChangedListener ( android.app.IWindowSecureChangeListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Landroid/app/IWindowSecureChangeListener; */
/* .line 1243 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 1244 */
if ( p1 != null) { // if-eqz p1, :cond_0
try { // :try_start_0
v1 = this.mSecureChangeListeners;
v1 = (( java.util.ArrayList ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 1245 */
v1 = this.mSecureChangeListeners;
(( java.util.ArrayList ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1247 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1248 */
return;
/* .line 1247 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void adjustWindowParams ( android.view.WindowManager$LayoutParams p0, java.lang.String p1, Integer p2 ) {
/* .locals 10 */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .line 518 */
/* if-nez p1, :cond_0 */
return;
/* .line 520 */
} // :cond_0
/* const/16 v0, 0x3e8 */
/* if-eq p3, v0, :cond_5 */
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* const/high16 v1, 0x80000 */
/* and-int/2addr v0, v1 */
/* if-nez v0, :cond_1 */
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* const/high16 v1, 0x400000 */
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 523 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 525 */
/* .local v0, "appInfo":Landroid/content/pm/ApplicationInfo; */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 527 */
v2 = android.os.UserHandle .getUserId ( p3 );
/* .line 526 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.pm.PackageManager ) v1 ).getApplicationInfoAsUser ( p2, v3, v2 ); // invoke-virtual {v1, p2, v3, v2}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 530 */
/* .line 528 */
/* :catch_0 */
/* move-exception v1 */
/* .line 529 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
(( android.content.pm.PackageManager$NameNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 531 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
/* if-nez v0, :cond_2 */
/* move v1, p3 */
} // :cond_2
/* iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 532 */
/* .local v1, "appUid":I */
} // :goto_1
v2 = this.mAppOps;
/* const/16 v3, 0x2724 */
int v6 = 0; // const/4 v6, 0x0
final String v7 = "WindowManagerServiceImpl#adjustWindowParams"; // const-string v7, "WindowManagerServiceImpl#adjustWindowParams"
/* move v4, v1 */
/* move-object v5, p2 */
v2 = /* invoke-virtual/range {v2 ..v7}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
/* .line 534 */
/* .local v2, "mode":I */
v3 = this.mSecurityInternal;
/* if-nez v3, :cond_3 */
/* .line 535 */
/* const-class v3, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v3 );
/* check-cast v3, Lmiui/security/SecurityManagerInternal; */
this.mSecurityInternal = v3;
/* .line 537 */
} // :cond_3
v4 = this.mSecurityInternal;
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 538 */
/* const/16 v5, 0x24 */
/* const-wide/16 v7, 0x1 */
/* .line 539 */
(( android.view.WindowManager$LayoutParams ) p1 ).getTitle ( ); // invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
java.lang.String .valueOf ( v3 );
/* .line 538 */
/* move-object v6, p2 */
/* invoke-virtual/range {v4 ..v9}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 542 */
} // :cond_4
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 543 */
/* iget v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* const v4, -0x80001 */
/* and-int/2addr v3, v4 */
/* iput v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 544 */
/* iget v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* const v4, -0x400001 */
/* and-int/2addr v3, v4 */
/* iput v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 545 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "MIUILOG- Show when locked PermissionDenied pkg : "; // const-string v4, "MIUILOG- Show when locked PermissionDenied pkg : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " uid : "; // const-string v4, " uid : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "WindowManagerService"; // const-string v4, "WindowManagerService"
android.util.Slog .i ( v4,v3 );
/* .line 549 */
} // .end local v0 # "appInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v1 # "appUid":I
} // .end local v2 # "mode":I
} // :cond_5
/* invoke-direct {p0, p3, p1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl;->adjustFindDeviceAttrs(ILandroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V */
/* .line 552 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl;->adjustGoogleAssistantAttrs(Landroid/view/WindowManager$LayoutParams;Ljava/lang/String;)V */
/* .line 556 */
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* and-int/lit16 v0, v0, 0x1000 */
if ( v0 != null) { // if-eqz v0, :cond_6
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I */
/* const/16 v1, 0x63 */
/* if-gt v0, v1, :cond_6 */
/* .line 557 */
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* and-int/lit16 v0, v0, -0x1001 */
/* iput v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 559 */
} // :cond_6
return;
} // .end method
public void bindFloatwindowAndTargetTask ( android.view.WindowManager$LayoutParams p0, com.android.server.wm.WindowToken p1, com.android.server.wm.RootWindowContainer p2 ) {
/* .locals 6 */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p2, "token" # Lcom/android/server/wm/WindowToken; */
/* .param p3, "root" # Lcom/android/server/wm/RootWindowContainer; */
/* .line 2131 */
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I */
/* const/16 v1, 0x7f6 */
/* if-ne v0, v1, :cond_3 */
/* .line 2132 */
(( android.view.WindowManager$LayoutParams ) p1 ).getTitle ( ); // invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
final String v1 = "home_worldCirculate_and_smallWindow_crop"; // const-string v1, "home_worldCirculate_and_smallWindow_crop"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
v0 = com.android.server.wm.WindowManagerServiceImpl.mFloatWindowMirrorWhiteList;
v1 = this.packageName;
/* .line 2133 */
v0 = (( java.util.ArrayList ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 2134 */
v0 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 2135 */
/* .local v0, "topFocusedRootTask":Lcom/android/server/wm/Task; */
(( com.android.server.wm.Task ) v0 ).getTopResumedActivity ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getTopResumedActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 2136 */
/* .local v1, "topActivity":Lcom/android/server/wm/ActivityRecord; */
if ( v1 != null) { // if-eqz v1, :cond_2
(( com.android.server.wm.ActivityRecord ) v1 ).getName ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getName()Ljava/lang/String;
final String v3 = "com.miui.home/.launcher.Launcher"; // const-string v3, "com.miui.home/.launcher.Launcher"
v2 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_0 */
/* .line 2137 */
(( com.android.server.wm.ActivityRecord ) v1 ).getName ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getName()Ljava/lang/String;
final String v3 = "com.miui.circulate.world.AppCirculateActivity"; // const-string v3, "com.miui.circulate.world.AppCirculateActivity"
v2 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 2138 */
} // :cond_0
(( com.android.server.wm.RootWindowContainer ) p3 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {p3}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 2139 */
/* .local v2, "defaultDisplayArea":Lcom/android/server/wm/TaskDisplayArea; */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 2140 */
/* .local v3, "targetTaskList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;" */
/* new-instance v4, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda4; */
/* invoke-direct {v4, p1, v3}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda4;-><init>(Landroid/view/WindowManager$LayoutParams;Ljava/util/ArrayList;)V */
(( com.android.server.wm.TaskDisplayArea ) v2 ).forAllRootTasks ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/wm/TaskDisplayArea;->forAllRootTasks(Ljava/util/function/Consumer;)V
/* .line 2146 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-ge v4, v5, :cond_1 */
/* .line 2147 */
(( java.util.ArrayList ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/wm/Task; */
this.floatWindow = p2;
/* .line 2146 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 2149 */
} // .end local v2 # "defaultDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
} // .end local v3 # "targetTaskList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;"
} // .end local v4 # "i":I
} // :cond_1
/* .line 2150 */
} // :cond_2
this.floatWindow = p2;
/* .line 2153 */
} // .end local v0 # "topFocusedRootTask":Lcom/android/server/wm/Task;
} // .end local v1 # "topActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_3
} // :goto_1
return;
} // .end method
public void calcHoverAnimatingColor ( Float[] p0 ) {
/* .locals 1 */
/* .param p1, "startColor" # [F */
/* .line 1864 */
v0 = this.miuiHoverModeInternal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1865 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).calcHoverAnimatingColor ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->calcHoverAnimatingColor([F)V
/* .line 1867 */
} // :cond_0
return;
} // .end method
public void checkFlipFullScreenChange ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 9 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 2036 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->enableFullScreen(Lcom/android/server/wm/ActivityRecord;)Z */
/* .line 2037 */
/* .local v0, "activityEnableFullScreen":Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2038 */
(( com.android.server.wm.ActivityRecord ) p1 ).getWindowConfiguration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getWindowConfiguration()Landroid/app/WindowConfiguration;
(( android.app.WindowConfiguration ) v2 ).getBounds ( ); // invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* iget v2, v2, Landroid/graphics/Rect;->left:I */
/* if-gtz v2, :cond_0 */
/* move v2, v1 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 2039 */
/* .local v2, "nowIsFullScreen":Z */
} // :goto_0
v3 = this.mLastIsFullScreen;
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = (( java.lang.Boolean ) v3 ).booleanValue ( ); // invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
/* if-ne v3, v2, :cond_1 */
/* .line 2040 */
final String v1 = "WindowManagerService"; // const-string v1, "WindowManagerService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "checkFlipFullScreenChange is same, nowIsFullScreen = "; // const-string v4, "checkFlipFullScreenChange is same, nowIsFullScreen = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 2041 */
return;
/* .line 2043 */
} // :cond_1
java.lang.Boolean .valueOf ( v2 );
this.mLastIsFullScreen = v3;
/* .line 2044 */
final String v3 = "WindowManagerService"; // const-string v3, "WindowManagerService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "before notify flip small screen change, is full = "; // const-string v5, "before notify flip small screen change, is full = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 2045 */
v3 = this.mFlipListenerLock;
/* monitor-enter v3 */
/* .line 2046 */
try { // :try_start_0
v4 = this.mNavigationBarColorListenerMap;
v4 = (( android.util.ArrayMap ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I
/* sub-int/2addr v4, v1 */
/* .local v4, "i":I */
} // :goto_1
/* if-ltz v4, :cond_3 */
/* .line 2047 */
v1 = this.mNavigationBarColorListenerMap;
(( android.util.ArrayMap ) v1 ).valueAt ( v4 ); // invoke-virtual {v1, v4}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v1, Landroid/view/IFlipWindowStateListener; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2049 */
/* .local v1, "listener":Landroid/view/IFlipWindowStateListener; */
try { // :try_start_1
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 2058 */
/* .line 2050 */
/* :catch_0 */
/* move-exception v5 */
/* .line 2051 */
/* .local v5, "e":Landroid/os/RemoteException; */
try { // :try_start_2
/* instance-of v6, v5, Landroid/os/DeadObjectException; */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 2052 */
final String v6 = "WindowManagerService"; // const-string v6, "WindowManagerService"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "onNavigationColorChange binder died, remove it, key = "; // const-string v8, "onNavigationColorChange binder died, remove it, key = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mNavigationBarColorListenerMap;
/* .line 2053 */
(( android.util.ArrayMap ) v8 ).keyAt ( v4 ); // invoke-virtual {v8, v4}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2052 */
android.util.Slog .d ( v6,v7 );
/* .line 2054 */
v6 = this.mNavigationBarColorListenerMap;
(( android.util.ArrayMap ) v6 ).removeAt ( v4 ); // invoke-virtual {v6, v4}, Landroid/util/ArrayMap;->removeAt(I)Ljava/lang/Object;
/* .line 2056 */
} // :cond_2
final String v6 = "WindowManagerService"; // const-string v6, "WindowManagerService"
final String v7 = "onNavigationColorChange fail"; // const-string v7, "onNavigationColorChange fail"
android.util.Slog .d ( v6,v7,v5 );
/* .line 2046 */
} // .end local v1 # "listener":Landroid/view/IFlipWindowStateListener;
} // .end local v5 # "e":Landroid/os/RemoteException;
} // :goto_2
/* add-int/lit8 v4, v4, -0x1 */
/* .line 2060 */
} // .end local v4 # "i":I
} // :cond_3
/* monitor-exit v3 */
/* .line 2061 */
return;
/* .line 2060 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v3 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public void clearForcedDisplaySize ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 3 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 988 */
(( com.android.server.wm.WindowManagerServiceImpl ) p0 ).getUserSetResolution ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getUserSetResolution()[I
/* .line 989 */
/* .local v0, "screenResolution":[I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 990 */
int v1 = 0; // const/4 v1, 0x0
/* aget v1, v0, v1 */
/* .line 991 */
/* .local v1, "width":I */
int v2 = 1; // const/4 v2, 0x1
/* aget v2, v0, v2 */
/* .line 992 */
/* .local v2, "height":I */
(( com.android.server.wm.DisplayContent ) p1 ).setForcedSize ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/DisplayContent;->setForcedSize(II)V
/* .line 993 */
} // .end local v1 # "width":I
} // .end local v2 # "height":I
/* .line 994 */
} // :cond_0
/* iget v1, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayWidth:I */
/* iget v2, p1, Lcom/android/server/wm/DisplayContent;->mInitialDisplayHeight:I */
(( com.android.server.wm.DisplayContent ) p1 ).setForcedSize ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/DisplayContent;->setForcedSize(II)V
/* .line 997 */
} // :goto_0
return;
} // .end method
public void clearOverrideBrightnessRecord ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .line 1594 */
v0 = this.mTaskIdScreenBrightnessOverrides;
v0 = (( android.util.SparseArray ) v0 ).indexOfKey ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I
/* .line 1595 */
/* .local v0, "index":I */
/* if-ltz v0, :cond_0 */
/* .line 1596 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onTaskRemoved: mTaskIdScreenBrightnessOverrides : "; // const-string v2, "onTaskRemoved: mTaskIdScreenBrightnessOverrides : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTaskIdScreenBrightnessOverrides;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " taskId : "; // const-string v2, " taskId : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
android.util.Slog .i ( v2,v1 );
/* .line 1598 */
v1 = this.mTaskIdScreenBrightnessOverrides;
(( android.util.SparseArray ) v1 ).delete ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V
/* .line 1600 */
} // :cond_0
return;
} // .end method
public Boolean doesAddCarWithNavigationBarRequireToken ( java.lang.String p0 ) {
/* .locals 11 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1954 */
final String v0 = "WindowManagerService"; // const-string v0, "WindowManagerService"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
final String v2 = "com.miui.carlink"; // const-string v2, "com.miui.carlink"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1955 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* const/16 v3, 0x40 */
(( android.content.pm.PackageManager ) v2 ).getPackageInfo ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
v2 = this.signatures;
/* aget-object v2, v2, v1 */
/* .line 1956 */
/* .local v2, "releaseSig":Landroid/content/pm/Signature; */
(( android.content.pm.Signature ) v2 ).toByteArray ( ); // invoke-virtual {v2}, Landroid/content/pm/Signature;->toByteArray()[B
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 1958 */
/* .local v3, "sigBytes":[B */
int v4 = 0; // const/4 v4, 0x0
/* .line 1960 */
/* .local v4, "digest":Ljava/security/MessageDigest; */
try { // :try_start_1
final String v5 = "SHA-256"; // const-string v5, "SHA-256"
java.security.MessageDigest .getInstance ( v5 );
/* move-object v4, v5 */
/* .line 1961 */
(( java.security.MessageDigest ) v4 ).digest ( v3 ); // invoke-virtual {v4, v3}, Ljava/security/MessageDigest;->digest([B)[B
/* .line 1962 */
/* .local v5, "hash":[B */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* array-length v7, v5 */
/* mul-int/lit8 v7, v7, 0x2 */
/* invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 1963 */
/* .local v6, "hexString":Ljava/lang/StringBuilder; */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_0
/* array-length v8, v5 */
int v9 = 1; // const/4 v9, 0x1
/* if-ge v7, v8, :cond_1 */
/* .line 1964 */
/* aget-byte v8, v5, v7 */
/* and-int/lit16 v8, v8, 0xff */
java.lang.Integer .toHexString ( v8 );
/* .line 1965 */
/* .local v8, "hex":Ljava/lang/String; */
v10 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
/* if-ne v10, v9, :cond_0 */
/* .line 1966 */
/* const/16 v9, 0x30 */
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 1968 */
} // :cond_0
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1963 */
/* nop */
} // .end local v8 # "hex":Ljava/lang/String;
/* add-int/lit8 v7, v7, 0x1 */
/* .line 1971 */
} // .end local v7 # "i":I
} // :cond_1
final String v7 = "c8a2e9bccf597c2fb6dc66bee293fc13f2fc47ec77bc6b2b0d52c11f51192ab8"; // const-string v7, "c8a2e9bccf597c2fb6dc66bee293fc13f2fc47ec77bc6b2b0d52c11f51192ab8"
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v7 = (( java.lang.String ) v7 ).equals ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v7, :cond_3 */
final String v7 = "c9009d01ebf9f5d0302bc71b2fe9aa9a47a432bba17308a3111b75d7b2149025"; // const-string v7, "c9009d01ebf9f5d0302bc71b2fe9aa9a47a432bba17308a3111b75d7b2149025"
/* .line 1972 */
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v0 = (( java.lang.String ) v7 ).equals ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1 */
/* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 ..:try_end_1} :catch_1 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1979 */
} // .end local v5 # "hash":[B
} // .end local v6 # "hexString":Ljava/lang/StringBuilder;
} // :cond_2
/* .line 1973 */
/* .restart local v5 # "hash":[B */
/* .restart local v6 # "hexString":Ljava/lang/StringBuilder; */
} // :cond_3
} // :goto_1
/* .line 1977 */
} // .end local v5 # "hash":[B
} // .end local v6 # "hexString":Ljava/lang/StringBuilder;
/* :catch_0 */
/* move-exception v5 */
/* .line 1978 */
/* .local v5, "e":Ljava/security/NoSuchAlgorithmException; */
try { // :try_start_2
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "packageName"; // const-string v7, "packageName"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = "NoSuchAlgorithmException: "; // const-string v7, "NoSuchAlgorithmException: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v6 );
/* .line 1981 */
} // .end local v2 # "releaseSig":Landroid/content/pm/Signature;
} // .end local v3 # "sigBytes":[B
} // .end local v4 # "digest":Ljava/security/MessageDigest;
} // .end local v5 # "e":Ljava/security/NoSuchAlgorithmException;
} // :goto_2
/* nop */
/* .line 1987 */
/* .line 1982 */
} // :cond_4
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "packageName:"; // const-string v3, "packageName:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " is not carlink"; // const-string v3, " is not carlink"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* :try_end_2 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 1983 */
/* .line 1985 */
/* :catch_1 */
/* move-exception v2 */
/* .line 1986 */
/* .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "NameNotFoundException"; // const-string v4, "NameNotFoundException"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 1988 */
} // .end local v2 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_3
} // .end method
public Integer enableWmsDebugConfig ( com.android.server.wm.WindowManagerShellCommand p0, java.io.PrintWriter p1 ) {
/* .locals 4 */
/* .param p1, "shellcmd" # Lcom/android/server/wm/WindowManagerShellCommand; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 1148 */
(( com.android.server.wm.WindowManagerShellCommand ) p1 ).getNextArgRequired ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowManagerShellCommand;->getNextArgRequired()Ljava/lang/String;
/* .line 1149 */
/* .local v0, "cmd":Ljava/lang/String; */
final String v1 = "enable-text"; // const-string v1, "enable-text"
v2 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
final String v2 = "disable-text"; // const-string v2, "disable-text"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 1150 */
final String v1 = "Error: wrong args , must be enable-text or disable-text"; // const-string v1, "Error: wrong args , must be enable-text or disable-text"
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1151 */
int v1 = -1; // const/4 v1, -0x1
/* .line 1153 */
} // :cond_0
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 1155 */
/* .local v1, "enable":Z */
} // :goto_0
(( com.android.server.wm.WindowManagerShellCommand ) p1 ).getNextArg ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowManagerShellCommand;->getNextArg()Ljava/lang/String;
/* move-object v3, v2 */
/* .local v3, "config":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1156 */
/* invoke-direct {p0, v3, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->enableWmsDebugConfig(Ljava/lang/String;Z)V */
/* .line 1158 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // .end method
public Boolean executeShellCommand ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2, java.lang.String p3 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .param p4, "opt" # Ljava/lang/String; */
/* .line 435 */
/* array-length v0, p2 */
int v1 = 0; // const/4 v1, 0x0
/* if-ge p3, v0, :cond_0 */
/* .line 436 */
/* array-length v0, p2 */
/* sub-int/2addr v0, p3 */
/* new-array v0, v0, [Ljava/lang/String; */
/* .local v0, "str":[Ljava/lang/String; */
/* .line 438 */
} // .end local v0 # "str":[Ljava/lang/String;
} // :cond_0
/* new-array v0, v1, [Ljava/lang/String; */
/* .line 440 */
/* .restart local v0 # "str":[Ljava/lang/String; */
} // :goto_0
/* array-length v2, p2 */
/* sub-int/2addr v2, p3 */
java.lang.System .arraycopy ( p2,p3,v0,v1,v2 );
/* .line 441 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "WindowManagerServiceImpl:executeShellCommand opti = "; // const-string v3, "WindowManagerServiceImpl:executeShellCommand opti = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " opt = "; // const-string v3, " opt = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 443 */
try { // :try_start_0
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v2 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v2 ).executeShellCommand ( p4, v0, p1 ); // invoke-virtual {v2, p4, v0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 444 */
int v1 = 1; // const/4 v1, 0x1
/* .line 448 */
} // :cond_1
/* .line 446 */
/* :catch_0 */
/* move-exception v2 */
/* .line 447 */
/* .local v2, "exception":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 449 */
} // .end local v2 # "exception":Ljava/lang/Exception;
} // :goto_1
} // .end method
public void finishSwitchResolution ( ) {
/* .locals 4 */
/* .line 850 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 851 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* invoke-direct {p0, v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->setRoundedCornerOverlaysCanScreenShot(Z)V */
/* .line 852 */
/* iget-boolean v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 853 */
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
final String v3 = "finished switching resolution"; // const-string v3, "finished switching resolution"
android.util.Slog .i ( v2,v3 );
/* .line 854 */
/* iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z */
/* .line 856 */
} // :cond_0
/* monitor-exit v0 */
/* .line 857 */
return;
/* .line 856 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void getBlurWallpaperAppearance ( android.graphics.Rect p0, android.graphics.Rect p1, Integer[] p2 ) {
/* .locals 6 */
/* .param p1, "leftBounds" # Landroid/graphics/Rect; */
/* .param p2, "rightBounds" # Landroid/graphics/Rect; */
/* .param p3, "outAppearance" # [I */
/* .line 1657 */
if ( p3 != null) { // if-eqz p3, :cond_5
/* array-length v0, p3 */
int v1 = 2; // const/4 v1, 0x2
/* if-lt v0, v1, :cond_5 */
v0 = this.mBlurWallpaperBmp;
if ( v0 != null) { // if-eqz v0, :cond_5
if ( p1 != null) { // if-eqz p1, :cond_5
/* if-nez p2, :cond_0 */
/* goto/16 :goto_2 */
/* .line 1662 */
} // :cond_0
v0 = (( android.graphics.Rect ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z
final String v1 = "appearance = "; // const-string v1, "appearance = "
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
int v3 = -1; // const/4 v3, -0x1
int v4 = 0; // const/4 v4, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1663 */
/* aput v3, p3, v4 */
/* .line 1665 */
} // :cond_1
v0 = this.leftStatusBounds;
v0 = (( android.graphics.Rect ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I */
/* if-eq v0, v3, :cond_2 */
/* .line 1666 */
/* aput v0, p3, v4 */
/* .line 1668 */
} // :cond_2
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->createAppearance(Landroid/graphics/Rect;)I */
/* iput v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->leftAppearance:I */
/* aput v0, p3, v4 */
/* .line 1669 */
v0 = this.leftStatusBounds;
(( android.graphics.Rect ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 1670 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getBlurWallpaperAppearance left bounds = "; // const-string v5, "getBlurWallpaperAppearance left bounds = "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v4, p3, v4 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 1674 */
} // :goto_0
v0 = (( android.graphics.Rect ) p2 ).isEmpty ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z
int v4 = 1; // const/4 v4, 0x1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1675 */
/* aput v3, p3, v4 */
/* .line 1677 */
} // :cond_3
v0 = this.rightStatusBounds;
v0 = (( android.graphics.Rect ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I */
/* if-eq v0, v3, :cond_4 */
/* .line 1678 */
/* aput v0, p3, v4 */
/* .line 1680 */
} // :cond_4
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/wm/WindowManagerServiceImpl;->createAppearance(Landroid/graphics/Rect;)I */
/* iput v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->rightAppearance:I */
/* aput v0, p3, v4 */
/* .line 1681 */
v0 = this.rightStatusBounds;
(( android.graphics.Rect ) v0 ).set ( p2 ); // invoke-virtual {v0, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 1682 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getBlurWallpaperAppearance right bounds = "; // const-string v3, "getBlurWallpaperAppearance right bounds = "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v1, p3, v4 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 1686 */
} // :goto_1
return;
/* .line 1660 */
} // :cond_5
} // :goto_2
return;
} // .end method
public android.graphics.Bitmap getBlurWallpaperBmp ( ) {
/* .locals 3 */
/* .line 389 */
v0 = this.mBlurWallpaperBmp;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 390 */
v0 = this.mWmService;
v0 = this.mH;
/* const/16 v2, 0x6a */
(( com.android.server.wm.WindowManagerService$H ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(I)V
/* .line 391 */
v0 = this.mWmService;
v0 = this.mH;
(( com.android.server.wm.WindowManagerService$H ) v0 ).sendEmptyMessage ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowManagerService$H;->sendEmptyMessage(I)Z
/* .line 392 */
final String v0 = "WindowManagerService"; // const-string v0, "WindowManagerService"
final String v2 = "mBlurWallpaperBmp is null, wait to update"; // const-string v2, "mBlurWallpaperBmp is null, wait to update"
android.util.Slog .d ( v0,v2 );
/* .line 393 */
/* .line 396 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 397 */
v1 = android.graphics.Bitmap$Config.ARGB_8888;
int v2 = 1; // const/4 v2, 0x1
(( android.graphics.Bitmap ) v0 ).copy ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
/* .line 398 */
} // :cond_1
/* nop */
/* .line 396 */
} // :goto_0
} // .end method
public Float getCompatScale ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 1644 */
v0 = this.mWmService;
v0 = this.mAtmService;
v0 = this.mCompatModePackages;
v0 = (( com.android.server.wm.CompatModePackages ) v0 ).getCompatScale ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/CompatModePackages;->getCompatScale(Ljava/lang/String;I)F
} // .end method
public Integer getDefaultDensity ( ) {
/* .locals 4 */
/* .line 950 */
v0 = this.mDisplayContent;
/* if-nez v0, :cond_0 */
int v0 = -1; // const/4 v0, -0x1
/* .line 951 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportSwitchResolutionFeature:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I */
/* iget v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mMiuiDisplayWidth:I */
/* mul-int v3, v1, v2 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 952 */
/* iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalDensity:I */
/* mul-int/2addr v0, v2 */
/* int-to-float v0, v0 */
/* const/high16 v2, 0x3f800000 # 1.0f */
/* mul-float/2addr v0, v2 */
/* int-to-float v1, v1 */
/* div-float/2addr v0, v1 */
v0 = java.lang.Math .round ( v0 );
/* .line 954 */
} // :cond_1
/* iget v0, v0, Lcom/android/server/wm/DisplayContent;->mInitialDisplayDensity:I */
} // .end method
public Integer getForcedDensity ( ) {
/* .locals 3 */
/* .line 877 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 878 */
v1 = android.os.UserHandle .getCallingUserId ( );
/* .line 877 */
final String v2 = "display_density_forced"; // const-string v2, "display_density_forced"
android.provider.Settings$Secure .getStringForUser ( v0,v2,v1 );
/* .line 879 */
/* .local v0, "densityString":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 880 */
java.lang.Integer .valueOf ( v0 );
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 882 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Integer getNavigationBarColor ( ) {
/* .locals 1 */
/* .line 2107 */
/* iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationColor:I */
} // .end method
public android.window.ScreenCapture$ScreenshotHardwareBuffer getResolutionSwitchShotBuffer ( Boolean p0, android.graphics.Rect p1 ) {
/* .locals 5 */
/* .param p1, "isDisplayRotation" # Z */
/* .param p2, "cropBounds" # Landroid/graphics/Rect; */
/* .line 970 */
int v0 = 0; // const/4 v0, 0x0
/* .line 971 */
/* .local v0, "screenshotBuffer":Landroid/window/ScreenCapture$ScreenshotHardwareBuffer; */
/* iget-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 972 */
com.android.server.display.DisplayControl .getPhysicalDisplayIds ( );
/* .line 973 */
/* .local v1, "physicalDisplayIds":[J */
int v2 = 0; // const/4 v2, 0x0
/* aget-wide v2, v1, v2 */
com.android.server.display.DisplayControl .getPhysicalDisplayToken ( v2,v3 );
/* .line 974 */
/* .local v2, "displayToken":Landroid/os/IBinder; */
/* new-instance v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder; */
/* invoke-direct {v3, v2}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;-><init>(Landroid/os/IBinder;)V */
/* .line 976 */
(( android.window.ScreenCapture$DisplayCaptureArgs$Builder ) v3 ).setSourceCrop ( p2 ); // invoke-virtual {v3, p2}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setSourceCrop(Landroid/graphics/Rect;)Landroid/window/ScreenCapture$CaptureArgs$Builder;
/* check-cast v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder; */
/* .line 977 */
int v4 = 1; // const/4 v4, 0x1
(( android.window.ScreenCapture$DisplayCaptureArgs$Builder ) v3 ).setCaptureSecureLayers ( v4 ); // invoke-virtual {v3, v4}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setCaptureSecureLayers(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;
/* check-cast v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder; */
/* .line 978 */
(( android.window.ScreenCapture$DisplayCaptureArgs$Builder ) v3 ).setAllowProtected ( v4 ); // invoke-virtual {v3, v4}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setAllowProtected(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;
/* check-cast v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder; */
/* .line 979 */
(( android.window.ScreenCapture$DisplayCaptureArgs$Builder ) v3 ).setHintForSeamlessTransition ( p1 ); // invoke-virtual {v3, p1}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->setHintForSeamlessTransition(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;
/* check-cast v3, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder; */
/* .line 980 */
(( android.window.ScreenCapture$DisplayCaptureArgs$Builder ) v3 ).build ( ); // invoke-virtual {v3}, Landroid/window/ScreenCapture$DisplayCaptureArgs$Builder;->build()Landroid/window/ScreenCapture$DisplayCaptureArgs;
/* .line 981 */
/* .local v3, "displayCaptureArgs":Landroid/window/ScreenCapture$DisplayCaptureArgs; */
android.window.ScreenCapture .captureDisplay ( v3 );
/* .line 983 */
} // .end local v1 # "physicalDisplayIds":[J
} // .end local v2 # "displayToken":Landroid/os/IBinder;
} // .end local v3 # "displayCaptureArgs":Landroid/window/ScreenCapture$DisplayCaptureArgs;
} // :cond_0
} // .end method
public Integer getScreenShareProjectAndPrivateCastFlag ( com.android.server.wm.WindowStateAnimator p0 ) {
/* .locals 12 */
/* .param p1, "winAnimator" # Lcom/android/server/wm/WindowStateAnimator; */
/* .line 1410 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1411 */
/* .local v0, "applyFlag":I */
com.xiaomi.screenprojection.IMiuiScreenProjectionStub .getInstance ( );
/* .line 1412 */
/* .local v1, "imp":Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub; */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v3, "screen_share_protection_on" */
int v4 = 0; // const/4 v4, 0x0
int v5 = -2; // const/4 v5, -0x2
v2 = android.provider.Settings$Secure .getIntForUser ( v2,v3,v4,v5 );
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_0 */
/* move v2, v3 */
} // :cond_0
/* move v2, v4 */
/* .line 1415 */
/* .local v2, "isScreenShareProtection":Z */
} // :goto_0
v6 = this.mContext;
(( android.content.Context ) v6 ).getContentResolver ( ); // invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1416 */
(( com.xiaomi.screenprojection.IMiuiScreenProjectionStub ) v1 ).getMiuiInScreeningSettingsKey ( ); // invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getMiuiInScreeningSettingsKey()Ljava/lang/String;
/* .line 1415 */
v6 = android.provider.Settings$Secure .getIntForUser ( v6,v7,v4,v5 );
/* .line 1417 */
/* .local v6, "screenProjectionOnOrOff":I */
v7 = this.mContext;
(( android.content.Context ) v7 ).getContentResolver ( ); // invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1418 */
(( com.xiaomi.screenprojection.IMiuiScreenProjectionStub ) v1 ).getMiuiPrivacyOnSettingsKey ( ); // invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getMiuiPrivacyOnSettingsKey()Ljava/lang/String;
/* .line 1417 */
v4 = android.provider.Settings$Secure .getIntForUser ( v7,v8,v4,v5 );
/* .line 1420 */
/* .local v4, "screenProjectionPrivacy":I */
if ( p1 != null) { // if-eqz p1, :cond_8
v5 = this.mWin;
if ( v5 != null) { // if-eqz v5, :cond_8
v5 = this.mWin;
(( com.android.server.wm.WindowState ) v5 ).getAttrs ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
if ( v5 != null) { // if-eqz v5, :cond_8
v5 = this.mWin;
/* .line 1421 */
(( com.android.server.wm.WindowState ) v5 ).getAttrs ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
(( android.view.WindowManager$LayoutParams ) v5 ).getTitle ( ); // invoke-virtual {v5}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 1422 */
v5 = this.mWin;
(( com.android.server.wm.WindowState ) v5 ).getAttrs ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;
(( android.view.WindowManager$LayoutParams ) v5 ).getTitle ( ); // invoke-virtual {v5}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
/* .line 1423 */
/* .local v5, "name":Ljava/lang/String; */
/* if-lez v6, :cond_3 */
/* if-lez v4, :cond_3 */
/* .line 1424 */
(( com.xiaomi.screenprojection.IMiuiScreenProjectionStub ) v1 ).getProjectionBlackList ( ); // invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getProjectionBlackList()Ljava/util/ArrayList;
/* .line 1425 */
/* .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 1426 */
(( java.util.ArrayList ) v7 ).iterator ( ); // invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v9 = } // :goto_1
if ( v9 != null) { // if-eqz v9, :cond_3
/* check-cast v9, Ljava/lang/String; */
/* .line 1427 */
/* .local v9, "blackTitle":Ljava/lang/String; */
v10 = (( java.lang.String ) v5 ).contains ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v10 != null) { // if-eqz v10, :cond_1
/* .line 1428 */
v10 = (( com.xiaomi.screenprojection.IMiuiScreenProjectionStub ) v1 ).getExtraScreenProjectFlag ( ); // invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getExtraScreenProjectFlag()I
/* or-int/2addr v0, v10 */
/* .line 1429 */
} // :cond_1
v10 = this.mWin;
/* iget-boolean v10, v10, Lcom/android/server/wm/WindowState;->mIsImWindow:Z */
if ( v10 != null) { // if-eqz v10, :cond_2
v10 = this.mWin;
/* .line 1430 */
(( com.android.server.wm.WindowState ) v10 ).getName ( ); // invoke-virtual {v10}, Lcom/android/server/wm/WindowState;->getName()Ljava/lang/String;
if ( v10 != null) { // if-eqz v10, :cond_2
v10 = this.mWin;
/* .line 1431 */
(( com.android.server.wm.WindowState ) v10 ).getName ( ); // invoke-virtual {v10}, Lcom/android/server/wm/WindowState;->getName()Ljava/lang/String;
final String v11 = "PopupWindow"; // const-string v11, "PopupWindow"
v10 = (( java.lang.String ) v10 ).contains ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v10 != null) { // if-eqz v10, :cond_2
v10 = this.mWin;
/* iget v10, v10, Lcom/android/server/wm/WindowState;->mViewVisibility:I */
/* if-nez v10, :cond_2 */
/* .line 1433 */
v10 = (( com.xiaomi.screenprojection.IMiuiScreenProjectionStub ) v1 ).getExtraScreenProjectFlag ( ); // invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getExtraScreenProjectFlag()I
/* or-int/2addr v0, v10 */
/* .line 1435 */
} // .end local v9 # "blackTitle":Ljava/lang/String;
} // :cond_2
} // :goto_2
/* .line 1440 */
} // .end local v7 # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :cond_3
int v7 = 0; // const/4 v7, 0x0
/* .line 1441 */
/* .local v7, "mode":I */
int v8 = 0; // const/4 v8, 0x0
/* .line 1442 */
/* .local v8, "packageName":Ljava/lang/String; */
v9 = this.mWin;
v9 = this.mActivityRecord;
if ( v9 != null) { // if-eqz v9, :cond_4
v9 = this.mWin;
v9 = this.mActivityRecord;
v9 = this.info;
if ( v9 != null) { // if-eqz v9, :cond_4
/* .line 1443 */
v9 = this.mWin;
v9 = this.mActivityRecord;
v9 = this.info;
v9 = this.applicationInfo;
/* iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 1444 */
/* .local v9, "uid":I */
v10 = this.mWin;
v10 = this.mActivityRecord;
v10 = this.info;
v10 = this.applicationInfo;
v8 = this.packageName;
/* .line 1446 */
v10 = this.mAppOps;
/* const/16 v11, 0x2739 */
v7 = (( android.app.AppOpsManager ) v10 ).checkOpNoThrow ( v11, v9, v8 ); // invoke-virtual {v10, v11, v9, v8}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I
/* .line 1450 */
} // .end local v9 # "uid":I
} // :cond_4
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 1451 */
/* if-ne v7, v3, :cond_5 */
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 1452 */
/* or-int/lit8 v0, v0, 0x1 */
/* .line 1454 */
} // :cond_5
(( com.xiaomi.screenprojection.IMiuiScreenProjectionStub ) v1 ).getScreenShareProjectBlackList ( ); // invoke-virtual {v1}, Lcom/xiaomi/screenprojection/IMiuiScreenProjectionStub;->getScreenShareProjectBlackList()Ljava/util/ArrayList;
/* .line 1455 */
/* .local v3, "screenShareProjectBlacklist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* if-nez v3, :cond_6 */
/* .line 1456 */
} // :cond_6
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v10 = } // :goto_3
if ( v10 != null) { // if-eqz v10, :cond_8
/* check-cast v10, Ljava/lang/String; */
/* .line 1457 */
/* .local v10, "shareName":Ljava/lang/String; */
v11 = (( java.lang.String ) v5 ).contains ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v11 != null) { // if-eqz v11, :cond_7
/* .line 1458 */
/* or-int/lit8 v0, v0, 0x2 */
/* .line 1460 */
} // .end local v10 # "shareName":Ljava/lang/String;
} // :cond_7
/* .line 1465 */
} // .end local v3 # "screenShareProjectBlacklist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v5 # "name":Ljava/lang/String;
} // .end local v7 # "mode":I
} // .end local v8 # "packageName":Ljava/lang/String;
} // :cond_8
} // .end method
public Boolean getTransitionReadyState ( ) {
/* .locals 1 */
/* .line 1947 */
v0 = this.mTransitionReadyList;
v0 = (( java.util.HashSet ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
public Integer getUserActivityTime ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 1206 */
v0 = this.mDimUserTimeoutMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // .end method
public getUserSetResolution ( ) {
/* .locals 5 */
/* .line 929 */
final String v0 = ","; // const-string v0, ","
int v1 = 3; // const/4 v1, 0x3
/* new-array v1, v1, [I */
/* .line 930 */
/* .local v1, "screenSizeInfo":[I */
final String v2 = "persist.sys.miui_resolution"; // const-string v2, "persist.sys.miui_resolution"
int v3 = 0; // const/4 v3, 0x0
android.os.SystemProperties .get ( v2,v3 );
/* .line 931 */
/* .local v2, "miuiResolution":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v4, :cond_0 */
/* .line 934 */
try { // :try_start_0
(( java.lang.String ) v2 ).split ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v4 = 0; // const/4 v4, 0x0
/* aget-object v3, v3, v4 */
v3 = java.lang.Integer .parseInt ( v3 );
/* aput v3, v1, v4 */
/* .line 936 */
(( java.lang.String ) v2 ).split ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v4 = 1; // const/4 v4, 0x1
/* aget-object v3, v3, v4 */
v3 = java.lang.Integer .parseInt ( v3 );
/* aput v3, v1, v4 */
/* .line 938 */
(( java.lang.String ) v2 ).split ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v3 = 2; // const/4 v3, 0x2
/* aget-object v0, v0, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* aput v0, v1, v3 */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 942 */
/* .line 939 */
/* :catch_0 */
/* move-exception v0 */
/* .line 940 */
/* .local v0, "e":Ljava/lang/NumberFormatException; */
int v1 = 0; // const/4 v1, 0x0
/* .line 941 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getResolutionFromProperty exception:"; // const-string v4, "getResolutionFromProperty exception:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.NumberFormatException ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "WindowManagerService"; // const-string v4, "WindowManagerService"
android.util.Slog .e ( v4,v3 );
/* .line 943 */
} // .end local v0 # "e":Ljava/lang/NumberFormatException;
} // :goto_0
/* .line 945 */
} // :cond_0
} // .end method
public void init ( com.android.server.wm.WindowManagerService p0, android.content.Context p1 ) {
/* .locals 4 */
/* .param p1, "wms" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 455 */
this.mContext = p2;
/* .line 456 */
this.mWmService = p1;
/* .line 457 */
v0 = this.mAppOps;
this.mAppOps = v0;
/* .line 458 */
v0 = this.mWmService;
v0 = this.mAtmService;
this.mAtmService = v0;
/* .line 459 */
v0 = this.mWmService;
v0 = this.mGlobalLock;
this.mGlobalLock = v0;
/* .line 461 */
/* const-class v0, Landroid/hardware/display/DisplayManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/hardware/display/DisplayManagerInternal; */
this.mDisplayManagerInternal = v0;
/* .line 462 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isSupportSwitchResoluton()Z */
/* iput-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportSwitchResolutionFeature:Z */
/* .line 463 */
/* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->checkDDICSupportAndInitPhysicalSize()V */
/* .line 467 */
/* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->registerBootCompletedReceiver()V */
/* .line 469 */
/* new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$WindowOpChangedListener; */
v1 = this.mWmService;
/* invoke-direct {v0, v1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl$WindowOpChangedListener;-><init>(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V */
/* .line 471 */
/* .local v0, "listener":Landroid/app/AppOpsManager$OnOpChangedListener; */
v1 = this.mAppOps;
/* const/16 v2, 0x2739 */
int v3 = 0; // const/4 v3, 0x0
(( android.app.AppOpsManager ) v1 ).startWatchingMode ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/app/AppOpsManager;->startWatchingMode(ILjava/lang/String;Landroid/app/AppOpsManager$OnOpChangedListener;)V
/* .line 473 */
com.miui.whetstone.PowerKeeperPolicy .getInstance ( );
this.mPowerKeeperPolicy = v1;
/* .line 474 */
v1 = this.mContext;
/* .line 475 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300a9 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 474 */
java.util.Arrays .asList ( v1 );
this.mBlackList = v1;
/* .line 480 */
/* const-class v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerServiceInternal = v1;
/* .line 482 */
return;
} // .end method
public void initAfterBoot ( ) {
/* .locals 1 */
/* .line 383 */
v0 = this.mBlurWallpaperBmp;
/* if-nez v0, :cond_0 */
/* .line 384 */
(( com.android.server.wm.WindowManagerServiceImpl ) p0 ).updateBlurWallpaperBmp ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->updateBlurWallpaperBmp()V
/* .line 386 */
} // :cond_0
return;
} // .end method
public void initializeMiuiResolutionLocked ( ) {
/* .locals 4 */
/* .line 887 */
/* iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportSwitchResolutionFeature:Z */
/* if-nez v0, :cond_0 */
/* .line 888 */
return;
/* .line 890 */
} // :cond_0
v0 = this.mWmService;
v0 = this.mRoot;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).getDisplayContent ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
this.mDisplayContent = v0;
/* .line 891 */
(( com.android.server.wm.WindowManagerServiceImpl ) p0 ).getUserSetResolution ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getUserSetResolution()[I
/* .line 892 */
/* .local v0, "screenResolution":[I */
if ( v0 != null) { // if-eqz v0, :cond_1
/* aget v2, v0, v1 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalWidth:I */
/* .line 893 */
/* .local v2, "screenWidth":I */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
int v3 = 1; // const/4 v3, 0x1
/* aget v3, v0, v3 */
} // :cond_2
/* iget v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPhysicalHeight:I */
/* .line 894 */
/* .local v3, "screenHeight":I */
} // :goto_1
/* invoke-direct {p0, v2, v3}, Lcom/android/server/wm/WindowManagerServiceImpl;->switchResolutionInternal(II)V */
/* .line 895 */
/* iput-boolean v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z */
/* .line 896 */
return;
} // .end method
public Boolean isAbortTransitionInScreenOff ( Integer p0, com.android.server.wm.ActivityTaskManagerService p1, com.android.server.wm.Transition p2 ) {
/* .locals 7 */
/* .param p1, "type" # I */
/* .param p2, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p3, "transition" # Lcom/android/server/wm/Transition; */
/* .line 2186 */
int v0 = 4; // const/4 v0, 0x4
int v1 = 0; // const/4 v1, 0x0
/* if-eq p1, v0, :cond_0 */
/* .line 2187 */
/* .line 2190 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 2191 */
/* .local v0, "isVoiceassist":Z */
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 2192 */
v2 = this.mParticipants;
/* .line 2193 */
/* .local v2, "participants":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowContainer;>;" */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( android.util.ArraySet ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->size()I
/* if-ge v3, v4, :cond_2 */
/* .line 2194 */
(( android.util.ArraySet ) v2 ).valueAt ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/wm/WindowContainer; */
/* .line 2195 */
/* .local v4, "wc":Lcom/android/server/wm/WindowContainer; */
(( com.android.server.wm.WindowContainer ) v4 ).asTask ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
if ( v5 != null) { // if-eqz v5, :cond_1
(( com.android.server.wm.WindowContainer ) v4 ).asTask ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
v5 = this.affinity;
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 2196 */
(( com.android.server.wm.WindowContainer ) v4 ).asTask ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
v5 = this.affinity;
final String v6 = "com.miui.voiceassist.floatactivity"; // const-string v6, "com.miui.voiceassist.floatactivity"
v5 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 2197 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2198 */
/* .line 2193 */
} // .end local v4 # "wc":Lcom/android/server/wm/WindowContainer;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 2203 */
} // .end local v2 # "participants":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowContainer;>;"
} // .end local v3 # "i":I
} // :cond_2
} // :goto_1
if ( p2 != null) { // if-eqz p2, :cond_3
v2 = this.mRootWindowContainer;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 2204 */
v2 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v2 ).getDisplayContent ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
/* .line 2205 */
/* .local v2, "dc":Lcom/android/server/wm/DisplayContent; */
if ( v2 != null) { // if-eqz v2, :cond_3
(( com.android.server.wm.DisplayContent ) v2 ).getDisplayInfo ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 2206 */
(( com.android.server.wm.DisplayContent ) v2 ).getDisplayInfo ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;
/* iget v3, v3, Landroid/view/DisplayInfo;->state:I */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_3 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 2208 */
(( com.android.server.wm.Transition ) p3 ).abort ( ); // invoke-virtual {p3}, Lcom/android/server/wm/Transition;->abort()V
/* .line 2209 */
/* .line 2212 */
} // .end local v2 # "dc":Lcom/android/server/wm/DisplayContent;
} // :cond_3
} // .end method
public Boolean isAdjustScreenOff ( java.lang.CharSequence p0 ) {
/* .locals 3 */
/* .param p1, "tag" # Ljava/lang/CharSequence; */
/* .line 1210 */
/* sget-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SUPPPORT_CLOUD_DIM:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1211 */
/* .line 1213 */
} // :cond_0
/* .line 1214 */
/* .local v0, "targetWindowName":Ljava/lang/String; */
v2 = this.mDimUserTimeoutMap;
v2 = (( java.util.HashMap ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z
/* if-nez v2, :cond_1 */
v2 = this.mDimUserTimeoutMap;
v2 = (( java.util.HashMap ) v2 ).containsKey ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1215 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1217 */
} // :cond_1
} // .end method
public Boolean isAssistResson ( java.lang.CharSequence p0 ) {
/* .locals 2 */
/* .param p1, "tag" # Ljava/lang/CharSequence; */
/* .line 1221 */
/* .line 1222 */
/* .local v0, "targetWindowName":Ljava/lang/String; */
v1 = this.mDimNeedAssistMap;
v1 = (( java.util.HashMap ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z
/* if-nez v1, :cond_0 */
v1 = this.mDimNeedAssistMap;
v1 = (( java.util.HashMap ) v1 ).containsKey ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mDimNeedAssistMap;
(( java.util.HashMap ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1223 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1225 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isCameraOpen ( ) {
/* .locals 1 */
/* .line 1229 */
v0 = v0 = this.mOpeningCameraID;
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isCtsModeEnabled ( ) {
/* .locals 1 */
/* .line 1121 */
/* iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->IS_CTS_MODE:Z */
} // .end method
public Boolean isKeyGuardShowing ( ) {
/* .locals 2 */
/* .line 1524 */
v0 = this.mAtmService;
v0 = this.mKeyguardController;
int v1 = 0; // const/4 v1, 0x0
v0 = (( com.android.server.wm.KeyguardController ) v0 ).isKeyguardShowing ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/KeyguardController;->isKeyguardShowing(I)Z
} // .end method
public Boolean isNotifyTouchEnable ( ) {
/* .locals 1 */
/* .line 1511 */
/* sget-boolean v0, Lcom/android/server/wm/WindowManagerServiceImpl;->SUPPORT_UNION_POWER_CORE:Z */
} // .end method
public Boolean isPendingSwitchResolution ( ) {
/* .locals 1 */
/* .line 959 */
/* iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mPendingSwitchResolution:Z */
} // .end method
public Boolean isSplitMode ( ) {
/* .locals 1 */
/* .line 1139 */
/* iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSplitMode:Z */
} // .end method
public Boolean isSupportSetActiveModeSwitchResolution ( ) {
/* .locals 1 */
/* .line 964 */
/* iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSupportActiveModeSwitch:Z */
} // .end method
public Boolean isTopLayoutFullScreen ( ) {
/* .locals 2 */
/* .line 1025 */
/* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->getDefaultDisplayContent()Lcom/android/server/wm/DisplayContent; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1026 */
v0 = this.mDisplayContent;
(( com.android.server.wm.DisplayContent ) v0 ).getDisplayPolicy ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;
/* .line 1027 */
/* .local v0, "displayPolicy":Lcom/android/server/wm/DisplayPolicy; */
v1 = (( com.android.server.wm.DisplayPolicy ) v0 ).isTopLayoutFullscreen ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayPolicy;->isTopLayoutFullscreen()Z
/* .line 1029 */
} // .end local v0 # "displayPolicy":Lcom/android/server/wm/DisplayPolicy;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void linkWallpaperWindowTokenDeathMonitor ( android.os.IBinder p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "binder" # Landroid/os/IBinder; */
/* .param p2, "displayId" # I */
/* .line 1234 */
try { // :try_start_0
/* new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;Landroid/os/IBinder;I)V */
int v1 = 0; // const/4 v1, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1237 */
/* .line 1235 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1238 */
} // :goto_0
return;
} // .end method
public Boolean mayChangeToMiuiSecurityInputMethod ( ) {
/* .locals 8 */
/* .line 1877 */
v0 = this.mWmService;
v0 = this.mRoot;
(( com.android.server.wm.RootWindowContainer ) v0 ).getDefaultDisplay ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;
/* .line 1878 */
/* .local v0, "defaultDisplay":Lcom/android/server/wm/DisplayContent; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1879 */
} // :cond_0
(( com.android.server.wm.DisplayContent ) v0 ).getImeInputTarget ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getImeInputTarget()Lcom/android/server/wm/InputTarget;
/* .line 1880 */
/* .local v2, "imeInputTarget":Lcom/android/server/wm/InputTarget; */
/* if-nez v2, :cond_1 */
/* .line 1881 */
} // :cond_1
/* .line 1882 */
/* .local v3, "windowState":Lcom/android/server/wm/WindowState; */
/* if-nez v3, :cond_2 */
/* .line 1883 */
} // :cond_2
(( com.android.server.wm.WindowState ) v3 ).getActivityRecord ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;
/* .line 1884 */
/* .local v4, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 1885 */
v5 = this.mMiuiSecurityImeWhiteList;
(( java.util.ArrayList ) v5 ).iterator ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_4
/* check-cast v6, Ljava/lang/String; */
/* .line 1886 */
/* .local v6, "windowName":Ljava/lang/String; */
(( com.android.server.wm.ActivityRecord ) v4 ).toString ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;
v7 = (( java.lang.String ) v7 ).contains ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 1887 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "windowName: " */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " activityRecord: "; // const-string v5, " activityRecord: "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "WindowManagerService"; // const-string v5, "WindowManagerService"
android.util.Slog .i ( v5,v1 );
/* .line 1888 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1890 */
} // .end local v6 # "windowName":Ljava/lang/String;
} // :cond_3
/* .line 1892 */
} // :cond_4
} // .end method
public Boolean mayChangeToMiuiSecurityInputMethod ( android.os.IBinder p0 ) {
/* .locals 7 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 1896 */
/* const-class v0, Lcom/android/server/wm/WindowManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerInternal; */
/* .line 1897 */
/* .local v0, "service":Lcom/android/server/wm/WindowManagerInternal; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1899 */
} // :cond_0
v2 = this.mWmService;
v2 = this.mGlobalLock;
/* monitor-enter v2 */
/* .line 1900 */
try { // :try_start_0
v3 = this.mWmService;
v3 = this.mWindowMap;
(( java.util.HashMap ) v3 ).get ( p1 ); // invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/WindowState; */
/* .line 1901 */
/* .local v3, "windowState":Lcom/android/server/wm/WindowState; */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1902 */
/* if-nez v3, :cond_1 */
/* .line 1903 */
} // :cond_1
(( com.android.server.wm.WindowState ) v3 ).getActivityRecord ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;
/* .line 1904 */
/* .local v2, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1905 */
v4 = this.mMiuiSecurityImeWhiteList;
(( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_3
/* check-cast v5, Ljava/lang/String; */
/* .line 1906 */
/* .local v5, "windowName":Ljava/lang/String; */
(( com.android.server.wm.ActivityRecord ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;
v6 = (( java.lang.String ) v6 ).contains ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 1907 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1909 */
} // .end local v5 # "windowName":Ljava/lang/String;
} // :cond_2
/* .line 1911 */
} // :cond_3
/* .line 1901 */
} // .end local v2 # "activityRecord":Lcom/android/server/wm/ActivityRecord;
} // .end local v3 # "windowState":Lcom/android/server/wm/WindowState;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean notAllowCaptureDisplay ( com.android.server.wm.RootWindowContainer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "windowState" # Lcom/android/server/wm/RootWindowContainer; */
/* .param p2, "displayId" # I */
/* .line 1797 */
v0 = android.os.Binder .getCallingUid ( );
/* const/16 v1, 0x3e8 */
int v2 = 0; // const/4 v2, 0x0
/* if-eq v0, v1, :cond_2 */
/* .line 1798 */
v0 = com.android.server.wm.ActivityRecordStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1802 */
} // :cond_0
(( com.android.server.wm.RootWindowContainer ) p1 ).getDisplayContent ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
/* .line 1804 */
/* .local v0, "displayContent":Lcom/android/server/wm/DisplayContent; */
/* if-nez v0, :cond_1 */
/* .line 1805 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " Screenshot on invalid display "; // const-string v3, " Screenshot on invalid display "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1806 */
v3 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1805 */
final String v3 = "WindowManagerService"; // const-string v3, "WindowManagerService"
android.util.Slog .e ( v3,v1 );
/* .line 1807 */
/* .line 1810 */
} // :cond_1
/* new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda6; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V */
v1 = (( com.android.server.wm.DisplayContent ) v0 ).forAllWindows ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/DisplayContent;->forAllWindows(Lcom/android/internal/util/ToBooleanFunction;Z)Z
/* .line 1799 */
} // .end local v0 # "displayContent":Lcom/android/server/wm/DisplayContent;
} // :cond_2
} // :goto_0
} // .end method
public void notifyFloatWindowScene ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "windowType" # I */
/* .param p3, "status" # Z */
/* .line 1520 */
return;
} // .end method
public void notifySystemBrightnessChange ( ) {
/* .locals 2 */
/* .line 1534 */
v0 = this.mWmService;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V */
(( com.android.server.wm.WindowManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 1544 */
return;
} // .end method
public void notifyTaskFocusChange ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 4 */
/* .param p1, "touchedActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1482 */
v0 = this.mPowerKeeperPolicy;
if ( v0 != null) { // if-eqz v0, :cond_2
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = this.intent;
/* if-nez v0, :cond_0 */
/* .line 1485 */
} // :cond_0
v0 = this.intent;
(( android.content.Intent ) v0 ).getComponent ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 1486 */
/* .local v0, "componentName":Landroid/content/ComponentName; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1487 */
v1 = this.mPowerKeeperPolicy;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( android.content.ComponentName ) v0 ).getClassName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
(( com.miui.whetstone.PowerKeeperPolicy ) v1 ).notifyTaskFocusChange ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyTaskFocusChange(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1489 */
} // :cond_1
return;
/* .line 1483 */
} // .end local v0 # "componentName":Landroid/content/ComponentName;
} // :cond_2
} // :goto_0
return;
} // .end method
public void notifyTouchFromNative ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isTouched" # Z */
/* .line 1473 */
return;
} // .end method
public void onAnimationFinished ( ) {
/* .locals 3 */
/* .line 1364 */
try { // :try_start_0
v0 = this.mUiModeAnimFinishedCallback;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1365 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1369 */
} // :cond_0
/* .line 1367 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1368 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Call mUiModeAnimFinishedCallback.onWindowAnimFinished error "; // const-string v2, "Call mUiModeAnimFinishedCallback.onWindowAnimFinished error "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
android.util.Slog .e ( v2,v1 );
/* .line 1370 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void onFinishTransition ( ) {
/* .locals 1 */
/* .line 1852 */
v0 = this.miuiHoverModeInternal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1853 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onFinishTransition ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->onFinishTransition()V
/* .line 1855 */
} // :cond_0
return;
} // .end method
public void onHoverModeRecentAnimStart ( ) {
/* .locals 1 */
/* .line 1846 */
v0 = this.miuiHoverModeInternal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1847 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onHoverModeRecentAnimStart ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->onHoverModeRecentAnimStart()V
/* .line 1849 */
} // :cond_0
return;
} // .end method
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 2 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 1496 */
v0 = this.mPowerKeeperPolicy;
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-nez p1, :cond_0 */
/* .line 1499 */
} // :cond_0
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
/* .line 1500 */
v0 = this.mPowerKeeperPolicy;
(( com.miui.whetstone.PowerKeeperPolicy ) v0 ).notifyTouchStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyTouchStatus(Z)V
/* .line 1501 */
} // :cond_1
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
/* if-ne v0, v1, :cond_2 */
/* .line 1502 */
v0 = this.mPowerKeeperPolicy;
int v1 = 0; // const/4 v1, 0x0
(( com.miui.whetstone.PowerKeeperPolicy ) v0 ).notifyTouchStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyTouchStatus(Z)V
/* .line 1504 */
} // :cond_2
} // :goto_0
return;
/* .line 1497 */
} // :cond_3
} // :goto_1
return;
} // .end method
public void onScreenRotationAnimationEnd ( ) {
/* .locals 1 */
/* .line 1870 */
v0 = this.miuiHoverModeInternal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1871 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onScreenRotationAnimationEnd ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->onScreenRotationAnimationEnd()V
/* .line 1873 */
} // :cond_0
return;
} // .end method
public void onSecureChangedListener ( com.android.server.wm.WindowState p0, Boolean p1 ) {
/* .locals 6 */
/* .param p1, "win" # Lcom/android/server/wm/WindowState; */
/* .param p2, "ignoreActivityState" # Z */
/* .line 1259 */
if ( p1 != null) { // if-eqz p1, :cond_7
v0 = this.mAttrs;
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 1260 */
v0 = this.mAttrs;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* and-int/lit16 v0, v0, 0x2000 */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
/* .line 1261 */
/* .local v0, "hasSecure":Z */
} // :goto_0
v3 = this.mActivityRecord;
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = this.mActivityRecord;
v4 = com.android.server.wm.ActivityRecord$State.RESUMED;
/* .line 1262 */
v3 = (( com.android.server.wm.ActivityRecord ) v3 ).isState ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z
/* if-nez v3, :cond_2 */
v3 = this.mActivityRecord;
v4 = com.android.server.wm.ActivityRecord$State.STARTED;
v3 = (( com.android.server.wm.ActivityRecord ) v3 ).isState ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_1
/* move v1, v2 */
/* .line 1263 */
/* .local v1, "isResumeOrStart":Z */
} // :cond_2
} // :goto_1
/* iget-boolean v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mLastWindowHasSecure:Z */
/* if-eq v0, v2, :cond_7 */
/* if-nez p2, :cond_3 */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1265 */
} // :cond_3
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl;->topRunningActivityChange(Lcom/android/server/wm/WindowState;)Z */
/* if-nez v2, :cond_4 */
/* .line 1266 */
v2 = (( com.android.server.wm.WindowState ) p1 ).getWindowType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* const/16 v3, 0x7e1 */
/* if-eq v2, v3, :cond_4 */
/* .line 1267 */
v2 = (( com.android.server.wm.WindowState ) p1 ).getWindowType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* const/16 v3, 0x7f8 */
/* if-ne v2, v3, :cond_7 */
/* .line 1268 */
} // :cond_4
v2 = (( com.android.server.wm.WindowState ) p1 ).getWindowType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I
int v3 = 3; // const/4 v3, 0x3
/* if-eq v2, v3, :cond_7 */
/* .line 1269 */
/* iput-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mLastWindowHasSecure:Z */
/* .line 1270 */
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onSecureChanged, current win = "; // const-string v4, "onSecureChanged, current win = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = ", hasSecure = "; // const-string v4, ", hasSecure = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1271 */
v2 = this.mGlobalLock;
/* monitor-enter v2 */
/* .line 1272 */
try { // :try_start_0
v3 = this.mSecureChangeListeners;
v3 = (( java.util.ArrayList ) v3 ).isEmpty ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 1273 */
/* monitor-exit v2 */
return;
/* .line 1275 */
} // :cond_5
v3 = this.mSecureChangeListeners;
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_6
/* check-cast v4, Landroid/app/IWindowSecureChangeListener; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1277 */
/* .local v4, "listener":Landroid/app/IWindowSecureChangeListener; */
try { // :try_start_1
(( com.android.server.wm.WindowState ) p1 ).getWindowTag ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1280 */
/* .line 1278 */
/* :catch_0 */
/* move-exception v5 */
/* .line 1279 */
/* .local v5, "e":Landroid/os/RemoteException; */
try { // :try_start_2
(( android.os.RemoteException ) v5 ).printStackTrace ( ); // invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 1281 */
} // .end local v4 # "listener":Landroid/app/IWindowSecureChangeListener;
} // .end local v5 # "e":Landroid/os/RemoteException;
} // :goto_3
/* .line 1282 */
} // :cond_6
/* monitor-exit v2 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v3 */
/* .line 1285 */
} // .end local v0 # "hasSecure":Z
} // .end local v1 # "isResumeOrStart":Z
} // :cond_7
} // :goto_4
return;
} // .end method
public Boolean onSettingsObserverChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 1304 */
v0 = this.mDarkModeEnable;
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = this.mDarkModeContrastEnable;
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = this.mContrastAlphaUri;
/* .line 1305 */
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1311 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1306 */
} // :cond_1
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateContrast : " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "WindowManagerService"; // const-string v1, "WindowManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 1309 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void onStopFreezingDisplayLocked ( ) {
/* .locals 1 */
/* .line 1858 */
v0 = this.miuiHoverModeInternal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1859 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onStopFreezingDisplayLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->onStopFreezingDisplayLocked()V
/* .line 1861 */
} // :cond_0
return;
} // .end method
public void onSystemReady ( ) {
/* .locals 2 */
/* .line 1826 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
this.miuiHoverModeInternal = v0;
/* .line 1827 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1828 */
v1 = this.mWmService;
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onSystemReady ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiHoverModeInternal;->onSystemReady(Lcom/android/server/wm/WindowManagerService;)V
/* .line 1830 */
} // :cond_0
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->setDefaultBlurConfigIfNeeded(Landroid/content/Context;)V */
/* .line 1831 */
return;
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .line 742 */
/* const/16 v0, 0xff */
/* if-ne p1, v0, :cond_0 */
/* .line 743 */
final String v0 = "android.view.IWindowManager"; // const-string v0, "android.view.IWindowManager"
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 744 */
v0 = (( com.android.server.wm.WindowManagerServiceImpl ) p0 ).switchResolution ( p2, p3, p4 ); // invoke-virtual {p0, p2, p3, p4}, Lcom/android/server/wm/WindowManagerServiceImpl;->switchResolution(Landroid/os/Parcel;Landroid/os/Parcel;I)Z
/* .line 745 */
} // :cond_0
/* nop */
/* .line 748 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void onWindowRequestSize ( Integer p0, Integer p1, Integer p2, java.lang.CharSequence p3 ) {
/* .locals 2 */
/* .param p1, "ownerUid" # I */
/* .param p2, "width" # I */
/* .param p3, "height" # I */
/* .param p4, "tag" # Ljava/lang/CharSequence; */
/* .line 1649 */
v0 = android.os.UserHandle .getAppId ( p1 );
/* const/16 v1, 0x2710 */
/* if-lt v0, v1, :cond_0 */
/* if-lez p2, :cond_0 */
/* if-lez p3, :cond_0 */
/* .line 1651 */
java.lang.String .valueOf ( p4 );
/* invoke-direct {p0, p1, v0, p2, p3}, Lcom/android/server/wm/WindowManagerServiceImpl;->checkIfUnusualWindowEvent(ILjava/lang/String;II)V */
/* .line 1653 */
} // :cond_0
return;
} // .end method
public void positionContrastSurface ( Integer p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "defaultDw" # I */
/* .param p2, "defaultDh" # I */
/* .line 1378 */
return;
} // .end method
public void registerFlipWindowStateListener ( android.view.IFlipWindowStateListener p0 ) {
/* .locals 4 */
/* .param p1, "listener" # Landroid/view/IFlipWindowStateListener; */
/* .line 2112 */
v0 = this.mFlipListenerLock;
/* monitor-enter v0 */
/* .line 2113 */
try { // :try_start_0
v1 = this.mNavigationBarColorListenerMap;
(( android.util.ArrayMap ) v1 ).put ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2114 */
final String v1 = "WindowManagerService"; // const-string v1, "WindowManagerService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "registerNavigationBarColorListener binder = "; // const-string v3, "registerNavigationBarColorListener binder = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ", size = "; // const-string v3, ", size = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mNavigationBarColorListenerMap;
/* .line 2115 */
v3 = (( android.util.ArrayMap ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2114 */
android.util.Slog .i ( v1,v2 );
/* .line 2116 */
/* monitor-exit v0 */
/* .line 2117 */
return;
/* .line 2116 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerSettingsObserver ( android.content.Context p0, com.android.server.wm.WindowManagerService$SettingsObserver p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "settingsObserver" # Lcom/android/server/wm/WindowManagerService$SettingsObserver; */
/* .line 1298 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1299 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
v1 = this.mDarkModeEnable;
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p2, v3 ); // invoke-virtual {v0, v1, v2, p2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1300 */
v1 = this.mDarkModeContrastEnable;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p2, v3 ); // invoke-virtual {v0, v1, v2, p2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1301 */
return;
} // .end method
public void registerUiModeAnimFinishedCallback ( android.view.IWindowAnimationFinishedCallback p0 ) {
/* .locals 0 */
/* .param p1, "callback" # Landroid/view/IWindowAnimationFinishedCallback; */
/* .line 1359 */
this.mUiModeAnimFinishedCallback = p1;
/* .line 1360 */
return;
} // .end method
public void removeFlipWindowStateListener ( android.view.IFlipWindowStateListener p0 ) {
/* .locals 4 */
/* .param p1, "listener" # Landroid/view/IFlipWindowStateListener; */
/* .line 2121 */
v0 = this.mFlipListenerLock;
/* monitor-enter v0 */
/* .line 2122 */
try { // :try_start_0
v1 = this.mNavigationBarColorListenerMap;
(( android.util.ArrayMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2123 */
final String v1 = "WindowManagerService"; // const-string v1, "WindowManagerService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "removeNavigationBarColorListener binder = "; // const-string v3, "removeNavigationBarColorListener binder = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ", size = "; // const-string v3, ", size = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mNavigationBarColorListenerMap;
/* .line 2124 */
v3 = (( android.util.ArrayMap ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2123 */
android.util.Slog .i ( v1,v2 );
/* .line 2125 */
/* monitor-exit v0 */
/* .line 2126 */
return;
/* .line 2125 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void removeMiuiPaperContrastOverlay ( android.view.SurfaceControl[] p0 ) {
/* .locals 3 */
/* .param p1, "excludeLayers" # [Landroid/view/SurfaceControl; */
/* .line 1925 */
v0 = this.mMiuiPaperContrastOverlay;
/* if-nez v0, :cond_0 */
/* .line 1926 */
return;
/* .line 1928 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* array-length v1, p1 */
/* if-ge v0, v1, :cond_2 */
/* .line 1929 */
/* aget-object v1, p1, v0 */
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.mMiuiPaperContrastOverlay;
/* aget-object v2, p1, v0 */
v1 = (( java.lang.Object ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1930 */
int v1 = 0; // const/4 v1, 0x0
/* aput-object v1, p1, v0 */
/* .line 1931 */
this.mMiuiPaperContrastOverlay = v1;
/* .line 1928 */
} // :cond_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1934 */
} // .end local v0 # "i":I
} // :cond_2
return;
} // .end method
public void removeSecureChangedListener ( android.app.IWindowSecureChangeListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Landroid/app/IWindowSecureChangeListener; */
/* .line 1251 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 1252 */
if ( p1 != null) { // if-eqz p1, :cond_0
try { // :try_start_0
v1 = this.mSecureChangeListeners;
v1 = (( java.util.ArrayList ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1253 */
v1 = this.mSecureChangeListeners;
(( java.util.ArrayList ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1255 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1256 */
return;
/* .line 1255 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void saveNavigationBarColor ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "color" # I */
/* .line 2012 */
/* iget v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationColor:I */
/* if-eq p1, v0, :cond_2 */
/* .line 2013 */
/* iput p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mNavigationColor:I */
/* .line 2014 */
final String v0 = "WindowManagerService"; // const-string v0, "WindowManagerService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "before notify flip navigation bar color change, color = "; // const-string v2, "before notify flip navigation bar color change, color = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 2015 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 2016 */
try { // :try_start_0
v1 = this.mNavigationBarColorListenerMap;
v1 = (( android.util.ArrayMap ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
/* add-int/lit8 v1, v1, -0x1 */
/* .local v1, "i":I */
} // :goto_0
/* if-ltz v1, :cond_1 */
/* .line 2017 */
v2 = this.mNavigationBarColorListenerMap;
(( android.util.ArrayMap ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Landroid/view/IFlipWindowStateListener; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2019 */
/* .local v2, "listener":Landroid/view/IFlipWindowStateListener; */
try { // :try_start_1
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 2028 */
/* .line 2020 */
/* :catch_0 */
/* move-exception v3 */
/* .line 2021 */
/* .local v3, "e":Landroid/os/RemoteException; */
try { // :try_start_2
/* instance-of v4, v3, Landroid/os/DeadObjectException; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 2022 */
v4 = this.mNavigationBarColorListenerMap;
(( android.util.ArrayMap ) v4 ).removeAt ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/ArrayMap;->removeAt(I)Ljava/lang/Object;
/* .line 2023 */
final String v4 = "WindowManagerService"; // const-string v4, "WindowManagerService"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "onNavigationColorChange binder died, remove it, key = "; // const-string v6, "onNavigationColorChange binder died, remove it, key = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mNavigationBarColorListenerMap;
/* .line 2024 */
(( android.util.ArrayMap ) v6 ).keyAt ( v1 ); // invoke-virtual {v6, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2023 */
android.util.Slog .d ( v4,v5 );
/* .line 2026 */
} // :cond_0
final String v4 = "WindowManagerService"; // const-string v4, "WindowManagerService"
final String v5 = "onNavigationColorChange fail"; // const-string v5, "onNavigationColorChange fail"
android.util.Slog .d ( v4,v5,v3 );
/* .line 2016 */
} // .end local v2 # "listener":Landroid/view/IFlipWindowStateListener;
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v1, v1, -0x1 */
/* .line 2030 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 2032 */
} // :cond_2
} // :goto_2
return;
} // .end method
public void setCloudDimControllerList ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "DimCloudConfig" # Ljava/lang/String; */
/* .line 1195 */
final String v0 = ";"; // const-string v0, ";"
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1196 */
/* .local v0, "DimConfigArray":[Ljava/lang/String; */
v1 = this.mDimUserTimeoutMap;
(( java.util.HashMap ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->clear()V
/* .line 1197 */
v1 = this.mDimNeedAssistMap;
(( java.util.HashMap ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->clear()V
/* .line 1198 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_0 */
/* .line 1199 */
/* aget-object v2, v0, v1 */
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) v2 ).split ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1200 */
/* .local v2, "DimConfigTotal":[Ljava/lang/String; */
v3 = this.mDimUserTimeoutMap;
int v4 = 0; // const/4 v4, 0x0
/* aget-object v5, v2, v4 */
int v6 = 1; // const/4 v6, 0x1
/* aget-object v6, v2, v6 */
v6 = java.lang.Integer .parseInt ( v6 );
java.lang.Integer .valueOf ( v6 );
(( java.util.HashMap ) v3 ).put ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1201 */
v3 = this.mDimNeedAssistMap;
/* aget-object v4, v2, v4 */
int v5 = 2; // const/4 v5, 0x2
/* aget-object v5, v2, v5 */
v5 = java.lang.Boolean .parseBoolean ( v5 );
java.lang.Boolean .valueOf ( v5 );
(( java.util.HashMap ) v3 ).put ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1198 */
} // .end local v2 # "DimConfigTotal":[Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1203 */
} // .end local v1 # "i":I
} // :cond_0
return;
} // .end method
public void setRunningRecentsAnimation ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "running" # Z */
/* .line 1632 */
/* iput-boolean p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mRunningRecentsAnimation:Z */
/* .line 1633 */
return;
} // .end method
public void setSplittable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "splittable" # Z */
/* .line 1143 */
/* iput-boolean p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mSplitMode:Z */
/* .line 1144 */
return;
} // .end method
public void setTransitionReadyState ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "syncId" # I */
/* .param p2, "ready" # Z */
/* .line 1937 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1938 */
v0 = this.mTransitionReadyList;
java.lang.Integer .valueOf ( p1 );
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 1939 */
v0 = this.mTransitionReadyList;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1942 */
} // :cond_0
v0 = this.mTransitionReadyList;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashSet ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 1944 */
} // :cond_1
} // :goto_0
return;
} // .end method
public Boolean shouldApplyOverrideBrightness ( com.android.server.wm.WindowState p0 ) {
/* .locals 5 */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 1569 */
(( com.android.server.wm.WindowState ) p1 ).getTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
/* .line 1570 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 1571 */
/* .line 1573 */
} // :cond_0
v2 = this.mTaskIdScreenBrightnessOverrides;
/* iget v3, v0, Lcom/android/server/wm/Task;->mTaskId:I */
v2 = (( android.util.SparseArray ) v2 ).indexOfKey ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/SparseArray;->indexOfKey(I)I
/* .line 1574 */
/* .local v2, "index":I */
/* if-gez v2, :cond_1 */
/* .line 1575 */
/* .line 1577 */
} // :cond_1
v3 = this.mTaskIdScreenBrightnessOverrides;
/* iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I */
(( android.util.SparseArray ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
v4 = this.mAttrs;
/* iget v4, v4, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F */
/* cmpl-float v3, v3, v4 */
if ( v3 != null) { // if-eqz v3, :cond_2
v3 = this.mBlackList;
/* .line 1578 */
v3 = (( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* if-nez v3, :cond_2 */
/* .line 1579 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "shouldApplyOverrideBrightness: mTaskIdScreenBrightnessOverrides=" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mTaskIdScreenBrightnessOverrides;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " taskId : "; // const-string v4, " taskId : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "WindowManagerService"; // const-string v4, "WindowManagerService"
android.util.Slog .i ( v4,v3 );
/* .line 1581 */
v3 = this.mTaskIdScreenBrightnessOverrides;
/* iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I */
(( android.util.SparseArray ) v3 ).delete ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/SparseArray;->delete(I)V
/* .line 1582 */
/* .line 1584 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean shouldApplyOverrideBrightnessForPip ( com.android.server.wm.WindowState p0 ) {
/* .locals 4 */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 1993 */
v0 = this.mAttrs;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F */
int v1 = 0; // const/4 v1, 0x0
/* cmpg-float v0, v0, v1 */
int v1 = 0; // const/4 v1, 0x0
/* if-ltz v0, :cond_4 */
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = this.mActivityRecord;
/* .line 1994 */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).isVisibleRequested ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isVisibleRequested()Z
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = (( com.android.server.wm.WindowState ) p1 ).inTransition ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->inTransition()Z
/* if-nez v0, :cond_0 */
/* .line 1998 */
} // :cond_0
v0 = this.mActivityRecord;
/* iget-boolean v0, v0, Lcom/android/server/wm/ActivityRecord;->mWaitForEnteringPinnedMode:Z */
/* if-nez v0, :cond_1 */
v0 = (( com.android.server.wm.WindowState ) p1 ).inPinnedWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->inPinnedWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1999 */
} // :cond_1
v0 = this.mTransitionController;
(( com.android.server.wm.TransitionController ) v0 ).getCollectingTransition ( ); // invoke-virtual {v0}, Lcom/android/server/wm/TransitionController;->getCollectingTransition()Lcom/android/server/wm/Transition;
/* .line 2000 */
/* .local v0, "transition":Lcom/android/server/wm/Transition; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v2, v0, Lcom/android/server/wm/Transition;->mType:I */
/* const/16 v3, 0xa */
/* if-eq v2, v3, :cond_2 */
/* iget v2, v0, Lcom/android/server/wm/Transition;->mType:I */
int v3 = 6; // const/4 v3, 0x6
/* if-ne v2, v3, :cond_3 */
/* .line 2002 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Override brightness when "; // const-string v2, "Override brightness when "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mActivityRecord;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " is entering PIP."; // const-string v2, " is entering PIP."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
android.util.Slog .i ( v2,v1 );
/* .line 2004 */
int v1 = 1; // const/4 v1, 0x1
/* .line 2007 */
} // .end local v0 # "transition":Lcom/android/server/wm/Transition;
} // :cond_3
/* .line 1995 */
} // :cond_4
} // :goto_0
} // .end method
public void showPinnedTaskIfNeeded ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 1636 */
/* iget-boolean v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl;->mRunningRecentsAnimation:Z */
/* if-nez v0, :cond_0 */
v0 = (( com.android.server.wm.Task ) p1 ).isRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isRootTask()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.wm.Task ) p1 ).isVisible ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isVisible()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1637 */
(( com.android.server.wm.Task ) p1 ).getPendingTransaction ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getPendingTransaction()Landroid/view/SurfaceControl$Transaction;
v1 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v0 ).show ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 1638 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "show pinned task surface: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "WindowManagerService"; // const-string v1, "WindowManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 1640 */
} // :cond_0
return;
} // .end method
Boolean switchResolution ( android.os.Parcel p0, android.os.Parcel p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .param p2, "reply" # Landroid/os/Parcel; */
/* .param p3, "flags" # I */
/* .line 767 */
v0 = android.os.Binder .getCallingUid ( );
v0 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v1, 0x3e8 */
/* if-ne v0, v1, :cond_1 */
/* .line 770 */
v0 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* .line 771 */
/* .local v0, "displayId":I */
v1 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* .line 772 */
/* .local v1, "width":I */
v2 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* .line 774 */
/* .local v2, "height":I */
/* if-nez v0, :cond_0 */
/* .line 778 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v3 */
/* .line 780 */
/* .local v3, "ident":J */
try { // :try_start_0
/* invoke-direct {p0, v1, v2}, Lcom/android/server/wm/WindowManagerServiceImpl;->switchResolutionInternal(II)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 782 */
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 783 */
/* nop */
/* .line 784 */
(( android.os.Parcel ) p2 ).writeNoException ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->writeNoException()V
/* .line 785 */
int v5 = 1; // const/4 v5, 0x1
/* .line 782 */
/* :catchall_0 */
/* move-exception v5 */
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 783 */
/* throw v5 */
/* .line 775 */
} // .end local v3 # "ident":J
} // :cond_0
/* new-instance v3, Ljava/lang/IllegalArgumentException; */
final String v4 = "Can only set the default display"; // const-string v4, "Can only set the default display"
/* invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 768 */
} // .end local v0 # "displayId":I
} // .end local v1 # "width":I
} // .end local v2 # "height":I
} // :cond_1
/* new-instance v0, Ljava/lang/SecurityException; */
final String v1 = "Only system uid can switch resolution"; // const-string v1, "Only system uid can switch resolution"
/* invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void trackScreenData ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "wakefulness" # I */
/* .param p2, "details" # Ljava/lang/String; */
/* .line 2180 */
com.android.server.wm.OneTrackRotationHelper .getInstance ( );
(( com.android.server.wm.OneTrackRotationHelper ) v0 ).trackScreenData ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->trackScreenData(ILjava/lang/String;)V
/* .line 2181 */
return;
} // .end method
public void updateBlurWallpaperBmp ( ) {
/* .locals 6 */
/* .line 402 */
/* const-class v0, Lcom/android/server/wallpaper/WallpaperManagerInternal; */
/* .line 403 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wallpaper/WallpaperManagerInternal; */
/* .line 404 */
/* .local v0, "wpMgr":Lcom/android/server/wallpaper/WallpaperManagerInternal; */
final String v1 = "WindowManagerService"; // const-string v1, "WindowManagerService"
/* if-nez v0, :cond_0 */
/* .line 405 */
final String v2 = "WallpaperManagerInternal is null"; // const-string v2, "WallpaperManagerInternal is null"
android.util.Slog .w ( v1,v2 );
/* .line 406 */
return;
/* .line 408 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 410 */
/* .local v2, "fd":Landroid/os/ParcelFileDescriptor; */
try { // :try_start_0
v3 = this.mWallpaperCallback;
(( com.android.server.wallpaper.WallpaperManagerInternal ) v0 ).getBlurWallpaper ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/wallpaper/WallpaperManagerInternal;->getBlurWallpaper(Landroid/app/IWallpaperManagerCallback;)Landroid/os/ParcelFileDescriptor;
/* move-object v2, v3 */
/* .line 412 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 413 */
/* new-instance v3, Landroid/graphics/BitmapFactory$Options; */
/* invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V */
/* .line 414 */
/* .local v3, "options":Landroid/graphics/BitmapFactory$Options; */
(( android.os.ParcelFileDescriptor ) v2 ).getFileDescriptor ( ); // invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
int v5 = 0; // const/4 v5, 0x0
android.graphics.BitmapFactory .decodeFileDescriptor ( v4,v5,v3 );
this.mBlurWallpaperBmp = v4;
/* .line 416 */
com.android.server.wm.BoundsCompatControllerStub .newInstance ( );
v4 = (( com.android.server.wm.BoundsCompatControllerStub ) v4 ).isDebugEnable ( ); // invoke-virtual {v4}, Lcom/android/server/wm/BoundsCompatControllerStub;->isDebugEnable()Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 417 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "decodeFileDescriptor, mBlurWallpaperBmp = "; // const-string v5, "decodeFileDescriptor, mBlurWallpaperBmp = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mBlurWallpaperBmp;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v4 );
/* .line 421 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/wm/WindowManagerServiceImpl;->updateAppearance()V */
/* .line 423 */
} // .end local v3 # "options":Landroid/graphics/BitmapFactory$Options;
/* .line 424 */
} // :cond_2
final String v3 = "getWallpaper, fd is null"; // const-string v3, "getWallpaper, fd is null"
android.util.Slog .w ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 429 */
} // :goto_0
/* :catchall_0 */
/* move-exception v1 */
/* .line 426 */
/* :catch_0 */
/* move-exception v3 */
/* .line 427 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v4 = "getWallpaper wrong"; // const-string v4, "getWallpaper wrong"
android.util.Slog .w ( v1,v4,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 429 */
/* nop */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_1
miui.io.IOUtils .closeQuietly ( v2 );
/* .line 430 */
/* nop */
/* .line 431 */
return;
/* .line 429 */
} // :goto_2
miui.io.IOUtils .closeQuietly ( v2 );
/* .line 430 */
/* throw v1 */
} // .end method
public void updateContrastAlpha ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "darkmode" # Z */
/* .line 1315 */
v0 = this.mWmService;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$5; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/WindowManagerServiceImpl$5;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;Z)V */
(( com.android.server.wm.WindowManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 1349 */
return;
} // .end method
public void updateScreenShareProjectFlag ( ) {
/* .locals 2 */
/* .line 1384 */
v0 = this.mAtmService;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/WindowManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V */
(( com.android.server.wm.ActivityTaskManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 1403 */
return;
} // .end method
public void updateSurfaceParentIfNeed ( com.android.server.wm.WindowState p0 ) {
/* .locals 7 */
/* .param p1, "ws" # Lcom/android/server/wm/WindowState; */
/* .line 1604 */
if ( p1 != null) { // if-eqz p1, :cond_5
v0 = this.mWmService;
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = this.mAtmService;
/* if-nez v0, :cond_0 */
/* goto/16 :goto_1 */
/* .line 1606 */
} // :cond_0
v0 = this.mWmService;
v0 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 1607 */
/* .local v0, "topRootTask1":Lcom/android/server/wm/Task; */
v1 = this.mWmService;
v1 = this.mRoot;
(( com.android.server.wm.RootWindowContainer ) v1 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 1608 */
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.wm.TaskDisplayArea ) v1 ).getTopRootTaskInWindowingMode ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;
/* .line 1610 */
/* .local v1, "topRootTask2":Lcom/android/server/wm/Task; */
int v2 = 0; // const/4 v2, 0x0
/* .line 1612 */
/* .local v2, "pairRootTask":Lcom/android/server/wm/Task; */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
v3 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v3 ).isInSystemSplitScreen ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1613 */
/* move-object v2, v0 */
/* .line 1614 */
} // :cond_1
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
v3 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v3 ).isInSystemSplitScreen ( v1 ); // invoke-virtual {v3, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1615 */
/* move-object v2, v1 */
/* .line 1618 */
} // :cond_2
} // :goto_0
/* if-nez v2, :cond_3 */
return;
/* .line 1620 */
} // :cond_3
final String v3 = "ClientDimmerForAppPair"; // const-string v3, "ClientDimmerForAppPair"
(( com.android.server.wm.WindowState ) p1 ).getWindowTag ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
(( com.android.server.wm.WindowState ) p1 ).getSurfaceControl ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getSurfaceControl()Landroid/view/SurfaceControl;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1621 */
(( com.android.server.wm.WindowState ) p1 ).getSurfaceControl ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getSurfaceControl()Landroid/view/SurfaceControl;
v3 = (( android.view.SurfaceControl ) v3 ).isValid ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControl;->isValid()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1622 */
(( com.android.server.wm.Task ) v2 ).getSurfaceControl ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1623 */
(( com.android.server.wm.Task ) v2 ).getSurfaceControl ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;
v3 = (( android.view.SurfaceControl ) v3 ).isValid ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControl;->isValid()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1624 */
/* new-instance v3, Landroid/view/SurfaceControl$Transaction; */
/* invoke-direct {v3}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
/* .line 1625 */
/* .local v3, "t":Landroid/view/SurfaceControl$Transaction; */
(( com.android.server.wm.WindowState ) p1 ).getSurfaceControl ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getSurfaceControl()Landroid/view/SurfaceControl;
v5 = this.mSurfaceControl;
/* const v6, 0x7fffffff */
(( android.view.SurfaceControl$Transaction ) v3 ).setRelativeLayer ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/view/SurfaceControl$Transaction;->setRelativeLayer(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
/* .line 1627 */
(( android.view.SurfaceControl$Transaction ) v3 ).apply ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 1629 */
} // .end local v3 # "t":Landroid/view/SurfaceControl$Transaction;
} // :cond_4
return;
/* .line 1604 */
} // .end local v0 # "topRootTask1":Lcom/android/server/wm/Task;
} // .end local v1 # "topRootTask2":Lcom/android/server/wm/Task;
} // .end local v2 # "pairRootTask":Lcom/android/server/wm/Task;
} // :cond_5
} // :goto_1
return;
} // .end method
