.class Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;
.super Ljava/lang/Object;
.source "AppTransitionInjector.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/AppTransitionInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActivityTranstionInterpolator"
.end annotation


# static fields
.field private static c:F

.field private static c1:F

.field private static c2:F

.field private static initial:F

.field private static k:F

.field private static m:F

.field private static mDamping:F

.field private static mResponse:F

.field private static r:F

.field private static w:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1269
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->mDamping:F

    .line 1270
    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->mResponse:F

    .line 1271
    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->initial:F

    .line 1272
    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->m:F

    .line 1273
    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->k:F

    .line 1274
    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->c:F

    .line 1275
    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->w:F

    .line 1276
    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->r:F

    .line 1277
    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->c1:F

    .line 1278
    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->c2:F

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 7
    .param p1, "response"    # F
    .param p2, "damping"    # F

    .line 1279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280
    sput p2, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->mDamping:F

    .line 1281
    sput p1, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->mResponse:F

    .line 1282
    const/high16 v0, -0x40800000    # -1.0f

    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->initial:F

    .line 1283
    const-wide v0, 0x401921fb54442d18L    # 6.283185307179586

    float-to-double v2, p1

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    sget v2, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->m:F

    float-to-double v3, v2

    mul-double/2addr v0, v3

    double-to-float v0, v0

    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->k:F

    .line 1284
    sget v1, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->mDamping:F

    float-to-double v3, v1

    const-wide v5, 0x402921fb54442d18L    # 12.566370614359172

    mul-double/2addr v3, v5

    float-to-double v5, v2

    mul-double/2addr v3, v5

    sget v1, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->mResponse:F

    float-to-double v5, v1

    div-double/2addr v3, v5

    double-to-float v1, v3

    sput v1, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->c:F

    .line 1285
    const/high16 v3, 0x40800000    # 4.0f

    mul-float/2addr v2, v3

    mul-float/2addr v2, v0

    mul-float/2addr v1, v1

    sub-float/2addr v2, v1

    float-to-double v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sget v1, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->m:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float v3, v1, v2

    div-float/2addr v0, v3

    sput v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->w:F

    .line 1286
    sget v3, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->c:F

    div-float/2addr v3, v2

    mul-float/2addr v3, v1

    neg-float v1, v3

    sput v1, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->r:F

    .line 1287
    sget v2, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->initial:F

    sput v2, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->c1:F

    .line 1288
    const/4 v3, 0x0

    mul-float/2addr v1, v2

    sub-float/2addr v3, v1

    div-float/2addr v3, v0

    sput v3, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->c2:F

    .line 1289
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 8
    .param p1, "input"    # F

    .line 1293
    sget v0, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->r:F

    mul-float/2addr v0, p1

    float-to-double v0, v0

    const-wide v2, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    sget v2, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->c1:F

    float-to-double v2, v2

    sget v4, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->w:F

    mul-float/2addr v4, p1

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    sget v4, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->c2:F

    float-to-double v4, v4

    sget v6, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;->w:F

    mul-float/2addr v6, p1

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method
