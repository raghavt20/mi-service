public class com.android.server.wm.ForegroundInfoManager {
	 /* .source "ForegroundInfoManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final android.os.RemoteCallbackList mActivityChangeListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lmiui/process/IActivityChangeListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mActivityLock;
private miui.process.ForegroundInfo mForegroundInfo;
private final android.os.RemoteCallbackList mForegroundInfoListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lmiui/process/IForegroundInfoListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mForegroundLock;
private miui.process.ForegroundInfo mForegroundWindowInfo;
private final android.os.RemoteCallbackList mForegroundWindowListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lmiui/process/IForegroundWindowListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.ComponentName mLastActivityComponent;
private com.android.server.am.ProcessManagerService mProcessManagerService;
/* # direct methods */
public com.android.server.wm.ForegroundInfoManager ( ) {
/* .locals 1 */
/* .param p1, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .line 75 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 32 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mForegroundInfoListeners = v0;
/* .line 34 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mActivityChangeListeners = v0;
/* .line 36 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mForegroundWindowListeners = v0;
/* .line 41 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mForegroundLock = v0;
/* .line 42 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mActivityLock = v0;
/* .line 76 */
this.mProcessManagerService = p1;
/* .line 77 */
/* new-instance v0, Lmiui/process/ForegroundInfo; */
/* invoke-direct {v0}, Lmiui/process/ForegroundInfo;-><init>()V */
this.mForegroundInfo = v0;
/* .line 78 */
/* new-instance v0, Lmiui/process/ForegroundInfo; */
/* invoke-direct {v0}, Lmiui/process/ForegroundInfo;-><init>()V */
this.mForegroundWindowInfo = v0;
/* .line 79 */
return;
} // .end method
private Boolean isMultiWindowChanged ( android.content.pm.ApplicationInfo p0 ) {
/* .locals 3 */
/* .param p1, "multiWindowAppInfo" # Landroid/content/pm/ApplicationInfo; */
/* .line 82 */
v0 = this.mForegroundInfo;
v0 = this.mMultiWindowForegroundPackageName;
/* .line 83 */
/* .local v0, "lastPkg":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* if-nez p1, :cond_1 */
/* .line 84 */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* .line 86 */
} // :cond_1
v2 = this.packageName;
v2 = android.text.TextUtils .equals ( v2,v0 );
/* xor-int/2addr v1, v2 */
} // .end method
private void notifyForegroundInfoLocked ( ) {
/* .locals 3 */
/* .line 183 */
v0 = this.mForegroundInfoListeners;
v0 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_0 */
/* .line 185 */
try { // :try_start_0
v1 = this.mForegroundInfoListeners;
(( android.os.RemoteCallbackList ) v1 ).getBroadcastItem ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v1, Lmiui/process/IForegroundInfoListener; */
v2 = this.mForegroundInfo;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 188 */
/* .line 186 */
/* :catch_0 */
/* move-exception v1 */
/* .line 187 */
/* .local v1, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 183 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v0, v0, -0x1 */
/* .line 190 */
} // .end local v0 # "i":I
} // :cond_0
v0 = this.mForegroundInfoListeners;
(( android.os.RemoteCallbackList ) v0 ).finishBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 191 */
return;
} // .end method
private void notifyForegroundWindowLocked ( ) {
/* .locals 3 */
/* .line 194 */
v0 = this.mForegroundWindowListeners;
v0 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_0 */
/* .line 196 */
try { // :try_start_0
v1 = this.mForegroundWindowListeners;
(( android.os.RemoteCallbackList ) v1 ).getBroadcastItem ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v1, Lmiui/process/IForegroundWindowListener; */
v2 = this.mForegroundWindowInfo;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 199 */
/* .line 197 */
/* :catch_0 */
/* move-exception v1 */
/* .line 198 */
/* .local v1, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 194 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v0, v0, -0x1 */
/* .line 201 */
} // .end local v0 # "i":I
} // :cond_0
v0 = this.mForegroundWindowListeners;
(( android.os.RemoteCallbackList ) v0 ).finishBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 202 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 320 */
final String v0 = "ForegroundInfo Listener:"; // const-string v0, "ForegroundInfo Listener:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 321 */
v0 = this.mForegroundLock;
/* monitor-enter v0 */
/* .line 322 */
try { // :try_start_0
v1 = this.mForegroundInfoListeners;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* add-int/lit8 v1, v1, -0x1 */
/* .local v1, "i":I */
} // :goto_0
/* if-ltz v1, :cond_0 */
/* .line 323 */
final String v2 = " #"; // const-string v2, " #"
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 324 */
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V
/* .line 325 */
final String v2 = ": "; // const-string v2, ": "
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 326 */
v2 = this.mForegroundInfoListeners;
(( android.os.RemoteCallbackList ) v2 ).getBroadcastItem ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v2, Lmiui/process/IForegroundInfoListener; */
(( java.lang.Object ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 322 */
/* add-int/lit8 v1, v1, -0x1 */
/* .line 328 */
} // .end local v1 # "i":I
} // :cond_0
v1 = this.mForegroundInfoListeners;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 329 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 330 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mForegroundInfo="; // const-string v1, "mForegroundInfo="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mForegroundInfo;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 332 */
final String v0 = "ActivityChange Listener:"; // const-string v0, "ActivityChange Listener:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 333 */
v1 = this.mActivityLock;
/* monitor-enter v1 */
/* .line 334 */
try { // :try_start_1
v0 = this.mActivityChangeListeners;
v0 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_1
/* if-ltz v0, :cond_1 */
/* .line 335 */
final String v2 = " #"; // const-string v2, " #"
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 336 */
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V
/* .line 337 */
final String v2 = ": "; // const-string v2, ": "
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 338 */
v2 = this.mActivityChangeListeners;
(( android.os.RemoteCallbackList ) v2 ).getBroadcastCookie ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 334 */
/* add-int/lit8 v0, v0, -0x1 */
/* .line 340 */
} // .end local v0 # "i":I
} // :cond_1
v0 = this.mActivityChangeListeners;
(( android.os.RemoteCallbackList ) v0 ).finishBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 341 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 342 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mLastActivityComponent="; // const-string v1, "mLastActivityComponent="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mLastActivityComponent;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 343 */
return;
/* .line 341 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 329 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v1 */
} // .end method
public miui.process.ForegroundInfo getForegroundInfo ( ) {
/* .locals 3 */
/* .line 205 */
v0 = this.mForegroundLock;
/* monitor-enter v0 */
/* .line 206 */
try { // :try_start_0
/* new-instance v1, Lmiui/process/ForegroundInfo; */
v2 = this.mForegroundInfo;
/* invoke-direct {v1, v2}, Lmiui/process/ForegroundInfo;-><init>(Lmiui/process/ForegroundInfo;)V */
/* monitor-exit v0 */
/* .line 207 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isForegroundApp ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 215 */
int v0 = -1; // const/4 v0, -0x1
/* if-eq p2, v0, :cond_1 */
v0 = this.mForegroundInfo;
/* iget v0, v0, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
/* if-ne v0, p2, :cond_0 */
/* .line 219 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 217 */
} // :cond_1
} // :goto_0
/* if-nez p1, :cond_2 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
v0 = this.mForegroundInfo;
v0 = this.mForegroundPackageName;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // :goto_1
} // .end method
public void notifyActivitiesChangedLocked ( android.content.ComponentName p0 ) {
/* .locals 3 */
/* .param p1, "curComponent" # Landroid/content/ComponentName; */
/* .line 286 */
v0 = this.mActivityChangeListeners;
v0 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_0 */
/* .line 288 */
try { // :try_start_0
v1 = this.mActivityChangeListeners;
(( android.os.RemoteCallbackList ) v1 ).getBroadcastItem ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v1, Lmiui/process/IActivityChangeListener; */
/* .line 289 */
/* .local v1, "listener":Lmiui/process/IActivityChangeListener; */
v2 = this.mActivityChangeListeners;
(( android.os.RemoteCallbackList ) v2 ).getBroadcastCookie ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;
/* .line 290 */
/* .local v2, "cookie":Ljava/lang/Object; */
(( com.android.server.wm.ForegroundInfoManager ) p0 ).notifyActivityChangedIfNeededLocked ( v1, v2, p1 ); // invoke-virtual {p0, v1, v2, p1}, Lcom/android/server/wm/ForegroundInfoManager;->notifyActivityChangedIfNeededLocked(Lmiui/process/IActivityChangeListener;Ljava/lang/Object;Landroid/content/ComponentName;)V
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 293 */
} // .end local v1 # "listener":Lmiui/process/IActivityChangeListener;
} // .end local v2 # "cookie":Ljava/lang/Object;
/* .line 291 */
/* :catch_0 */
/* move-exception v1 */
/* .line 292 */
/* .local v1, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 286 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v0, v0, -0x1 */
/* .line 295 */
} // .end local v0 # "i":I
} // :cond_0
v0 = this.mActivityChangeListeners;
(( android.os.RemoteCallbackList ) v0 ).finishBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 296 */
return;
} // .end method
public void notifyActivityChanged ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "curActivityComponent" # Landroid/content/ComponentName; */
/* .line 274 */
v0 = this.mActivityLock;
/* monitor-enter v0 */
/* .line 275 */
if ( p1 != null) { // if-eqz p1, :cond_1
try { // :try_start_0
v1 = this.mLastActivityComponent;
/* .line 276 */
v1 = (( android.content.ComponentName ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 280 */
} // :cond_0
(( com.android.server.wm.ForegroundInfoManager ) p0 ).notifyActivitiesChangedLocked ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->notifyActivitiesChangedLocked(Landroid/content/ComponentName;)V
/* .line 281 */
this.mLastActivityComponent = p1;
/* .line 282 */
/* monitor-exit v0 */
/* .line 283 */
return;
/* .line 277 */
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
return;
/* .line 282 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void notifyActivityChangedIfNeededLocked ( miui.process.IActivityChangeListener p0, java.lang.Object p1, android.content.ComponentName p2 ) {
/* .locals 5 */
/* .param p1, "listener" # Lmiui/process/IActivityChangeListener; */
/* .param p2, "cookie" # Ljava/lang/Object; */
/* .param p3, "curComponent" # Landroid/content/ComponentName; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 300 */
if ( p2 != null) { // if-eqz p2, :cond_5
/* instance-of v0, p2, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo; */
/* if-nez v0, :cond_0 */
/* .line 304 */
} // :cond_0
/* move-object v0, p2 */
/* check-cast v0, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo; */
/* .line 305 */
/* .local v0, "info":Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo; */
v1 = this.mLastActivityComponent;
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 306 */
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
} // :cond_1
/* move-object v1, v2 */
/* .line 307 */
/* .local v1, "lastPackage":Ljava/lang/String; */
} // :goto_0
v3 = this.mLastActivityComponent;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 308 */
(( android.content.ComponentName ) v3 ).getClassName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
} // :cond_2
/* nop */
/* .line 310 */
/* .local v2, "lastActivity":Ljava/lang/String; */
} // :goto_1
v3 = this.targetPackages;
if ( v3 != null) { // if-eqz v3, :cond_4
v3 = this.targetActivities;
if ( v3 != null) { // if-eqz v3, :cond_4
v3 = this.targetPackages;
/* .line 311 */
v3 = (( android.content.ComponentName ) p3 ).getPackageName ( ); // invoke-virtual {p3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* if-nez v3, :cond_3 */
v3 = this.targetPackages;
v3 = /* .line 312 */
/* if-nez v3, :cond_3 */
v3 = this.targetActivities;
/* .line 313 */
v3 = (( android.content.ComponentName ) p3 ).getClassName ( ); // invoke-virtual {p3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* if-nez v3, :cond_3 */
v3 = this.targetActivities;
v3 = /* .line 314 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 315 */
} // :cond_3
v3 = this.mLastActivityComponent;
/* .line 317 */
} // :cond_4
return;
/* .line 301 */
} // .end local v0 # "info":Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;
} // .end local v1 # "lastPackage":Ljava/lang/String;
} // .end local v2 # "lastActivity":Ljava/lang/String;
} // :cond_5
} // :goto_2
return;
} // .end method
public void notifyForegroundInfoChanged ( com.android.server.wm.FgActivityChangedInfo p0 ) {
/* .locals 10 */
/* .param p1, "changedInfo" # Lcom/android/server/wm/FgActivityChangedInfo; */
/* .line 91 */
v0 = this.record;
/* .line 92 */
/* .local v0, "foregroundRecord":Lcom/android/server/wm/ActivityRecord; */
v1 = this.state;
/* .line 93 */
/* .local v1, "state":Lcom/android/server/wm/ActivityRecord$State; */
/* iget v2, p1, Lcom/android/server/wm/FgActivityChangedInfo;->pid:I */
/* .line 94 */
/* .local v2, "pid":I */
v3 = this.multiWindowAppInfo;
/* .line 96 */
/* .local v3, "multiWindowAppInfo":Landroid/content/pm/ApplicationInfo; */
v4 = this.mForegroundLock;
/* monitor-enter v4 */
/* .line 97 */
try { // :try_start_0
v5 = this.info;
v5 = this.applicationInfo;
/* .line 98 */
/* .local v5, "foregroundAppInfo":Landroid/content/pm/ApplicationInfo; */
if ( v5 != null) { // if-eqz v5, :cond_4
v6 = this.mForegroundInfo;
v6 = this.mForegroundPackageName;
v7 = this.packageName;
/* .line 99 */
v6 = android.text.TextUtils .equals ( v6,v7 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 100 */
v6 = /* invoke-direct {p0, v3}, Lcom/android/server/wm/ForegroundInfoManager;->isMultiWindowChanged(Landroid/content/pm/ApplicationInfo;)Z */
/* if-nez v6, :cond_0 */
v6 = this.mForegroundInfo;
/* iget v6, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
/* iget v7, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* if-ne v6, v7, :cond_0 */
/* goto/16 :goto_2 */
/* .line 106 */
} // :cond_0
v6 = this.mForegroundInfo;
(( miui.process.ForegroundInfo ) v6 ).resetFlags ( ); // invoke-virtual {v6}, Lmiui/process/ForegroundInfo;->resetFlags()V
/* .line 107 */
v6 = v6 = this.mActivityRecordStub;
if ( v6 != null) { // if-eqz v6, :cond_1
v6 = com.android.server.wm.ActivityRecord$State.INITIALIZING;
/* if-ne v1, v6, :cond_1 */
/* .line 108 */
v6 = this.mForegroundInfo;
int v7 = 1; // const/4 v7, 0x1
(( miui.process.ForegroundInfo ) v6 ).addFlags ( v7 ); // invoke-virtual {v6, v7}, Lmiui/process/ForegroundInfo;->addFlags(I)V
/* .line 111 */
} // :cond_1
v6 = this.mForegroundInfo;
v7 = this.mForegroundPackageName;
this.mLastForegroundPackageName = v7;
/* .line 112 */
v6 = this.mForegroundInfo;
/* iget v7, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
/* .line 113 */
v6 = this.mForegroundInfo;
/* iget v7, v6, Lmiui/process/ForegroundInfo;->mForegroundPid:I */
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundPid:I */
/* .line 115 */
v6 = this.mForegroundInfo;
v7 = this.packageName;
this.mForegroundPackageName = v7;
/* .line 116 */
v6 = this.mForegroundInfo;
/* iget v7, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
/* .line 117 */
v6 = this.mForegroundInfo;
/* iput v2, v6, Lmiui/process/ForegroundInfo;->mForegroundPid:I */
/* .line 119 */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 120 */
v6 = this.mForegroundInfo;
v7 = this.packageName;
this.mMultiWindowForegroundPackageName = v7;
/* .line 121 */
v6 = this.mForegroundInfo;
/* iget v7, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I */
/* .line 123 */
} // :cond_2
v6 = this.mForegroundInfo;
int v7 = 0; // const/4 v7, 0x0
this.mMultiWindowForegroundPackageName = v7;
/* .line 124 */
v6 = this.mForegroundInfo;
int v7 = -1; // const/4 v7, -0x1
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I */
/* .line 127 */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/wm/ForegroundInfoManager;->notifyForegroundInfoLocked()V */
/* .line 128 */
v6 = this.mProcessManagerService;
v7 = this.mForegroundInfo;
v7 = this.mForegroundPackageName;
v8 = this.mActivityComponent;
/* .line 130 */
v9 = this.resultTo;
if ( v9 != null) { // if-eqz v9, :cond_3
v9 = this.resultTo;
v9 = this.processName;
} // :cond_3
final String v9 = ""; // const-string v9, ""
/* .line 128 */
} // :goto_1
(( com.android.server.am.ProcessManagerService ) v6 ).foregroundInfoChanged ( v7, v8, v9 ); // invoke-virtual {v6, v7, v8, v9}, Lcom/android/server/am/ProcessManagerService;->foregroundInfoChanged(Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)V
/* .line 131 */
com.android.server.wm.ActivityStarterStub .get ( );
v7 = this.mForegroundInfo;
v7 = this.mForegroundPackageName;
v8 = this.mForegroundInfo;
/* iget v8, v8, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
(( com.android.server.wm.ActivityStarterStub ) v6 ).updateLastStartActivityUid ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lcom/android/server/wm/ActivityStarterStub;->updateLastStartActivityUid(Ljava/lang/String;I)V
/* .line 136 */
com.android.server.am.MemoryStandardProcessControlStub .getInstance ( );
v7 = this.mForegroundInfo;
/* iget v7, v7, Lmiui/process/ForegroundInfo;->mLastForegroundPid:I */
v8 = this.mForegroundInfo;
v8 = this.mLastForegroundPackageName;
/* .line 139 */
com.android.server.am.MemoryStandardProcessControlStub .getInstance ( );
v7 = this.packageName;
/* .line 141 */
} // .end local v5 # "foregroundAppInfo":Landroid/content/pm/ApplicationInfo;
/* monitor-exit v4 */
/* .line 142 */
return;
/* .line 102 */
/* .restart local v5 # "foregroundAppInfo":Landroid/content/pm/ApplicationInfo; */
} // :cond_4
} // :goto_2
final String v6 = "ProcessManager"; // const-string v6, "ProcessManager"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "skip notify foregroundAppInfo:" */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v6,v7 );
/* .line 103 */
/* monitor-exit v4 */
return;
/* .line 141 */
} // .end local v5 # "foregroundAppInfo":Landroid/content/pm/ApplicationInfo;
/* :catchall_0 */
/* move-exception v5 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v5 */
} // .end method
public void notifyForegroundWindowChanged ( com.android.server.wm.FgWindowChangedInfo p0 ) {
/* .locals 9 */
/* .param p1, "changedInfo" # Lcom/android/server/wm/FgWindowChangedInfo; */
/* .line 145 */
v0 = this.record;
/* .line 146 */
/* .local v0, "foregroundRecord":Lcom/android/server/wm/ActivityRecord; */
(( com.android.server.wm.ActivityRecord ) v0 ).getState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;
/* .line 147 */
/* .local v1, "state":Lcom/android/server/wm/ActivityRecord$State; */
/* iget v2, p1, Lcom/android/server/wm/FgWindowChangedInfo;->pid:I */
/* .line 148 */
/* .local v2, "pid":I */
v3 = this.multiWindowAppInfo;
/* .line 150 */
/* .local v3, "multiWindowAppInfo":Landroid/content/pm/ApplicationInfo; */
v4 = this.mForegroundLock;
/* monitor-enter v4 */
/* .line 151 */
try { // :try_start_0
v5 = this.info;
v5 = this.applicationInfo;
/* .line 152 */
/* .local v5, "foregroundAppInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v5, :cond_0 */
/* .line 153 */
final String v6 = "ProcessManager"; // const-string v6, "ProcessManager"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "skip notify foregroundAppInfo:" */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v6,v7 );
/* .line 154 */
/* monitor-exit v4 */
return;
/* .line 157 */
} // :cond_0
v6 = this.mForegroundWindowInfo;
(( miui.process.ForegroundInfo ) v6 ).resetFlags ( ); // invoke-virtual {v6}, Lmiui/process/ForegroundInfo;->resetFlags()V
/* .line 158 */
v6 = v6 = this.mActivityRecordStub;
if ( v6 != null) { // if-eqz v6, :cond_1
v6 = com.android.server.wm.ActivityRecord$State.INITIALIZING;
/* if-ne v1, v6, :cond_1 */
/* .line 159 */
v6 = this.mForegroundWindowInfo;
int v7 = 1; // const/4 v7, 0x1
(( miui.process.ForegroundInfo ) v6 ).addFlags ( v7 ); // invoke-virtual {v6, v7}, Lmiui/process/ForegroundInfo;->addFlags(I)V
/* .line 162 */
} // :cond_1
v6 = this.mForegroundWindowInfo;
v7 = this.mForegroundPackageName;
this.mLastForegroundPackageName = v7;
/* .line 163 */
v6 = this.mForegroundWindowInfo;
/* iget v7, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I */
/* .line 164 */
v6 = this.mForegroundWindowInfo;
/* iget v7, v6, Lmiui/process/ForegroundInfo;->mForegroundPid:I */
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundPid:I */
/* .line 166 */
v6 = this.mForegroundWindowInfo;
v7 = this.packageName;
this.mForegroundPackageName = v7;
/* .line 167 */
v6 = this.mForegroundWindowInfo;
/* iget v7, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
/* .line 168 */
v6 = this.mForegroundWindowInfo;
/* iput v2, v6, Lmiui/process/ForegroundInfo;->mForegroundPid:I */
/* .line 170 */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 171 */
v6 = this.mForegroundWindowInfo;
v7 = this.packageName;
this.mMultiWindowForegroundPackageName = v7;
/* .line 172 */
v6 = this.mForegroundWindowInfo;
/* iget v7, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I */
/* .line 174 */
} // :cond_2
v6 = this.mForegroundWindowInfo;
int v7 = 0; // const/4 v7, 0x0
this.mMultiWindowForegroundPackageName = v7;
/* .line 175 */
v6 = this.mForegroundWindowInfo;
int v7 = -1; // const/4 v7, -0x1
/* iput v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I */
/* .line 178 */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/wm/ForegroundInfoManager;->notifyForegroundWindowLocked()V */
/* .line 179 */
} // .end local v5 # "foregroundAppInfo":Landroid/content/pm/ApplicationInfo;
/* monitor-exit v4 */
/* .line 180 */
return;
/* .line 179 */
/* :catchall_0 */
/* move-exception v5 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v5 */
} // .end method
public void registerActivityChangeListener ( java.util.List p0, java.util.List p1, miui.process.IActivityChangeListener p2 ) {
/* .locals 3 */
/* .param p3, "listener" # Lmiui/process/IActivityChangeListener; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Lmiui/process/IActivityChangeListener;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 256 */
/* .local p1, "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p2, "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 257 */
v0 = this.mActivityLock;
/* monitor-enter v0 */
/* .line 258 */
try { // :try_start_0
/* new-instance v1, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo; */
v2 = android.os.Binder .getCallingPid ( );
/* invoke-direct {v1, p0, v2, p1, p2}, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;-><init>(Lcom/android/server/wm/ForegroundInfoManager;ILjava/util/List;Ljava/util/List;)V */
/* .line 260 */
/* .local v1, "info":Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo; */
v2 = this.mActivityChangeListeners;
(( android.os.RemoteCallbackList ) v2 ).register ( p3, v1 ); // invoke-virtual {v2, p3, v1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z
/* .line 261 */
/* nop */
} // .end local v1 # "info":Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 263 */
} // :cond_0
} // :goto_0
return;
} // .end method
public void registerForegroundInfoListener ( miui.process.IForegroundInfoListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/process/IForegroundInfoListener; */
/* .line 223 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 224 */
v0 = this.mForegroundLock;
/* monitor-enter v0 */
/* .line 225 */
try { // :try_start_0
v1 = this.mForegroundInfoListeners;
(( android.os.RemoteCallbackList ) v1 ).register ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 226 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 228 */
} // :cond_0
} // :goto_0
return;
} // .end method
public void registerForegroundWindowListener ( miui.process.IForegroundWindowListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/process/IForegroundWindowListener; */
/* .line 239 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 240 */
v0 = this.mForegroundLock;
/* monitor-enter v0 */
/* .line 241 */
try { // :try_start_0
v1 = this.mForegroundWindowListeners;
(( android.os.RemoteCallbackList ) v1 ).register ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 242 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 244 */
} // :cond_0
} // :goto_0
return;
} // .end method
public void unregisterActivityChangeListener ( miui.process.IActivityChangeListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/process/IActivityChangeListener; */
/* .line 266 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 267 */
v0 = this.mActivityLock;
/* monitor-enter v0 */
/* .line 268 */
try { // :try_start_0
v1 = this.mActivityChangeListeners;
(( android.os.RemoteCallbackList ) v1 ).unregister ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 269 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 271 */
} // :cond_0
} // :goto_0
return;
} // .end method
public void unregisterForegroundInfoListener ( miui.process.IForegroundInfoListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/process/IForegroundInfoListener; */
/* .line 231 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 232 */
v0 = this.mForegroundLock;
/* monitor-enter v0 */
/* .line 233 */
try { // :try_start_0
v1 = this.mForegroundInfoListeners;
(( android.os.RemoteCallbackList ) v1 ).unregister ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 234 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 236 */
} // :cond_0
} // :goto_0
return;
} // .end method
public void unregisterForegroundWindowListener ( miui.process.IForegroundWindowListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/process/IForegroundWindowListener; */
/* .line 247 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 248 */
v0 = this.mForegroundLock;
/* monitor-enter v0 */
/* .line 249 */
try { // :try_start_0
v1 = this.mForegroundWindowListeners;
(( android.os.RemoteCallbackList ) v1 ).unregister ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 250 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 252 */
} // :cond_0
} // :goto_0
return;
} // .end method
