class com.android.server.wm.DisplayRotationStubImpl$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "DisplayRotationStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/DisplayRotationStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.wm.DisplayRotationStubImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.DisplayRotationStubImpl$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 432 */
this.this$0 = p1;
/* .line 433 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 434 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 437 */
v0 = this.this$0;
com.android.server.wm.DisplayRotationStubImpl .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 438 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "accelerometer_rotation_outer"; // const-string v1, "accelerometer_rotation_outer"
android.provider.Settings$System .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 441 */
/* const-string/jumbo v1, "user_rotation_outer" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 444 */
final String v1 = "accelerometer_rotation_inner"; // const-string v1, "accelerometer_rotation_inner"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 447 */
/* const-string/jumbo v1, "user_rotation_inner" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 451 */
v1 = this.this$0;
(( com.android.server.wm.DisplayRotationStubImpl ) v1 ).updateRotationMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayRotationStubImpl;->updateRotationMode()V
/* .line 452 */
return;
} // .end method
public void onChange ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .line 456 */
v0 = this.this$0;
(( com.android.server.wm.DisplayRotationStubImpl ) v0 ).updateRotationMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayRotationStubImpl;->updateRotationMode()V
/* .line 457 */
return;
} // .end method
