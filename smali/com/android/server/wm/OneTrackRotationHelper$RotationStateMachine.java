class com.android.server.wm.OneTrackRotationHelper$RotationStateMachine {
	 /* .source "OneTrackRotationHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/OneTrackRotationHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "RotationStateMachine" */
} // .end annotation
/* # static fields */
private static final Integer DATA_CACHED_THRESHOLD;
private static final Integer DATA_CACHED_THRESHOLD_DEBUG;
private static Integer mCacheThreshold;
/* # instance fields */
private final java.util.HashMap appUsageDataCache;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "[[I>;" */
/* } */
} // .end annotation
} // .end field
private Long dateOfToday;
private java.util.HashMap filpUsageDate;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean initialized;
private Boolean isNextSending;
private Boolean isShutDown;
private java.lang.String mCurPackageName;
private mDirection;
private Integer mDisplayRotation;
private Long mEndTime;
private Boolean mFolded;
private Integer mLaunchCount;
private Boolean mScreenState;
private Long mStartTime;
private final com.android.server.wm.OneTrackRotationHelper oneTrackRotationHelper;
/* # direct methods */
static java.util.HashMap -$$Nest$fgetfilpUsageDate ( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.filpUsageDate;
} // .end method
static Integer -$$Nest$fgetmDisplayRotation ( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I */
} // .end method
static Boolean -$$Nest$fgetmFolded ( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
} // .end method
static com.android.server.wm.OneTrackRotationHelper -$$Nest$fgetoneTrackRotationHelper ( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.oneTrackRotationHelper;
} // .end method
static void -$$Nest$monTodayIsOver ( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onTodayIsOver()V */
return;
} // .end method
static void -$$Nest$msetToday ( com.android.server.wm.OneTrackRotationHelper$RotationStateMachine p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setToday(J)V */
return;
} // .end method
static com.android.server.wm.OneTrackRotationHelper$RotationStateMachine ( ) {
/* .locals 1 */
/* .line 602 */
v0 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xf */
} // :cond_0
/* const/16 v0, 0x1e */
} // :goto_0
return;
} // .end method
public com.android.server.wm.OneTrackRotationHelper$RotationStateMachine ( ) {
/* .locals 3 */
/* .param p1, "helper" # Lcom/android/server/wm/OneTrackRotationHelper; */
/* .line 616 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 592 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
/* .line 593 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
/* .line 595 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I */
/* .line 596 */
/* const-wide/16 v1, -0x1 */
/* iput-wide v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mStartTime:J */
/* .line 597 */
/* iput-wide v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J */
/* .line 604 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
this.appUsageDataCache = v1;
/* .line 606 */
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->initialized:Z */
/* .line 611 */
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isShutDown:Z */
/* .line 614 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.filpUsageDate = v0;
/* .line 617 */
this.oneTrackRotationHelper = p1;
/* .line 618 */
return;
} // .end method
private Long collectData ( ) {
/* .locals 8 */
/* .line 1010 */
v0 = this.mCurPackageName;
v0 = android.text.TextUtils .isEmpty ( v0 );
final String v1 = "OneTrackRotationHelper"; // const-string v1, "OneTrackRotationHelper"
/* const-wide/16 v2, 0x0 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1011 */
v0 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1012 */
final String v0 = "collectData empty package"; // const-string v0, "collectData empty package"
android.util.Slog .i ( v1,v0 );
/* .line 1014 */
} // :cond_0
/* return-wide v2 */
/* .line 1017 */
} // :cond_1
/* iget-wide v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J */
/* iget-wide v6, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mStartTime:J */
/* sub-long/2addr v4, v6 */
/* .line 1018 */
/* .local v4, "duration":J */
/* const-wide/16 v6, 0x3e8 */
/* cmp-long v0, v4, v6 */
/* if-gez v0, :cond_3 */
/* .line 1019 */
v0 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1020 */
final String v0 = "collectData drop this data"; // const-string v0, "collectData drop this data"
android.util.Slog .i ( v1,v0 );
/* .line 1022 */
} // :cond_2
/* return-wide v2 */
/* .line 1025 */
} // :cond_3
/* div-long v0, v4, v6 */
/* return-wide v0 */
} // .end method
private void onTodayIsOver ( ) {
/* .locals 3 */
/* .line 856 */
v0 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 857 */
final String v0 = "OneTrackRotationHelper"; // const-string v0, "OneTrackRotationHelper"
final String v1 = "onTodayIsOver"; // const-string v1, "onTodayIsOver"
android.util.Slog .i ( v0,v1 );
/* .line 860 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z */
/* .line 861 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 862 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 864 */
/* .local v0, "now":J */
/* iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J */
/* .line 865 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 866 */
/* const-string/jumbo v2, "today-is-over-sending" */
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V */
/* .line 867 */
} // .end local v0 # "now":J
/* .line 868 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 870 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z */
/* .line 871 */
v0 = this.oneTrackRotationHelper;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$mprepareFinalSendingInToday ( v0 );
/* .line 872 */
return;
} // .end method
private java.util.ArrayList prepareAllData ( ) {
/* .locals 15 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1034 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1035 */
/* .local v0, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v1 = this.appUsageDataCache;
(( java.util.HashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_9
/* check-cast v2, Ljava/lang/String; */
/* .line 1036 */
/* .local v2, "packageName":Ljava/lang/String; */
v3 = this.appUsageDataCache;
(( java.util.HashMap ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, [[I */
/* .line 1037 */
/* .local v3, "detail":[[I */
/* if-nez v3, :cond_0 */
/* .line 1038 */
/* .line 1042 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
try { // :try_start_0
/* aget-object v5, v3, v4 */
/* aget v5, v5, v4 */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* const-string/jumbo v6, "screen_type" */
/* const-string/jumbo v7, "start_number" */
final String v8 = "landscape"; // const-string v8, "landscape"
final String v9 = "portrait"; // const-string v9, "portrait"
final String v10 = "package_name"; // const-string v10, "package_name"
int v11 = 2; // const/4 v11, 0x2
int v12 = 1; // const/4 v12, 0x1
/* if-nez v5, :cond_1 */
try { // :try_start_1
/* aget-object v5, v3, v4 */
/* aget v5, v5, v12 */
/* if-nez v5, :cond_1 */
/* aget-object v5, v3, v4 */
/* aget v5, v5, v11 */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 1043 */
} // :cond_1
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V */
/* .line 1044 */
/* .local v5, "jsonData":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v5 ).put ( v10, v2 ); // invoke-virtual {v5, v10, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 1045 */
/* aget-object v13, v3, v4 */
/* aget v13, v13, v4 */
(( org.json.JSONObject ) v5 ).put ( v9, v13 ); // invoke-virtual {v5, v9, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1046 */
/* aget-object v13, v3, v4 */
/* aget v13, v13, v12 */
(( org.json.JSONObject ) v5 ).put ( v8, v13 ); // invoke-virtual {v5, v8, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1047 */
/* aget-object v13, v3, v4 */
/* aget v13, v13, v11 */
(( org.json.JSONObject ) v5 ).put ( v7, v13 ); // invoke-virtual {v5, v7, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1048 */
v13 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FOLD ( );
/* if-nez v13, :cond_2 */
v13 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FLIP ( );
if ( v13 != null) { // if-eqz v13, :cond_3
/* .line 1049 */
} // :cond_2
/* const-string/jumbo v13, "\u5185\u5c4f" */
(( org.json.JSONObject ) v5 ).put ( v6, v13 ); // invoke-virtual {v5, v6, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 1051 */
} // :cond_3
(( org.json.JSONObject ) v5 ).toString ( ); // invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.util.ArrayList ) v0 ).add ( v13 ); // invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1054 */
} // .end local v5 # "jsonData":Lorg/json/JSONObject;
} // :cond_4
v5 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FOLD ( );
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* const-string/jumbo v13, "\u5916\u5c4f" */
if ( v5 != null) { // if-eqz v5, :cond_6
try { // :try_start_2
/* aget-object v5, v3, v12 */
/* aget v5, v5, v4 */
/* if-nez v5, :cond_5 */
/* aget-object v5, v3, v12 */
/* aget v5, v5, v12 */
/* if-nez v5, :cond_5 */
/* aget-object v5, v3, v12 */
/* aget v5, v5, v11 */
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 1055 */
} // :cond_5
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V */
/* .line 1056 */
/* .restart local v5 # "jsonData":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v5 ).put ( v10, v2 ); // invoke-virtual {v5, v10, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 1057 */
/* aget-object v14, v3, v12 */
/* aget v14, v14, v4 */
(( org.json.JSONObject ) v5 ).put ( v9, v14 ); // invoke-virtual {v5, v9, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1058 */
/* aget-object v14, v3, v12 */
/* aget v14, v14, v12 */
(( org.json.JSONObject ) v5 ).put ( v8, v14 ); // invoke-virtual {v5, v8, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1059 */
/* aget-object v14, v3, v12 */
/* aget v14, v14, v11 */
(( org.json.JSONObject ) v5 ).put ( v7, v14 ); // invoke-virtual {v5, v7, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1060 */
(( org.json.JSONObject ) v5 ).put ( v6, v13 ); // invoke-virtual {v5, v6, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 1061 */
(( org.json.JSONObject ) v5 ).toString ( ); // invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.util.ArrayList ) v0 ).add ( v14 ); // invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1064 */
} // .end local v5 # "jsonData":Lorg/json/JSONObject;
} // :cond_6
v5 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FLIP ( );
if ( v5 != null) { // if-eqz v5, :cond_8
/* aget-object v5, v3, v12 */
/* aget v5, v5, v4 */
/* if-nez v5, :cond_7 */
/* aget-object v5, v3, v12 */
/* aget v5, v5, v12 */
/* if-nez v5, :cond_7 */
/* aget-object v5, v3, v12 */
/* aget v5, v5, v11 */
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 1065 */
} // :cond_7
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V */
/* .line 1066 */
/* .restart local v5 # "jsonData":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v5 ).put ( v10, v2 ); // invoke-virtual {v5, v10, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 1067 */
/* aget-object v10, v3, v12 */
/* aget v4, v10, v4 */
(( org.json.JSONObject ) v5 ).put ( v9, v4 ); // invoke-virtual {v5, v9, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1068 */
/* aget-object v4, v3, v12 */
/* aget v4, v4, v12 */
(( org.json.JSONObject ) v5 ).put ( v8, v4 ); // invoke-virtual {v5, v8, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1069 */
/* aget-object v4, v3, v12 */
/* aget v4, v4, v11 */
(( org.json.JSONObject ) v5 ).put ( v7, v4 ); // invoke-virtual {v5, v7, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1070 */
(( org.json.JSONObject ) v5 ).put ( v6, v13 ); // invoke-virtual {v5, v6, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 1071 */
final String v4 = "screen_direction_0"; // const-string v4, "screen_direction_0"
/* aget-object v6, v3, v12 */
int v7 = 3; // const/4 v7, 0x3
/* aget v6, v6, v7 */
(( org.json.JSONObject ) v5 ).put ( v4, v6 ); // invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1072 */
final String v4 = "screen_direction_90"; // const-string v4, "screen_direction_90"
/* aget-object v6, v3, v12 */
int v7 = 4; // const/4 v7, 0x4
/* aget v6, v6, v7 */
(( org.json.JSONObject ) v5 ).put ( v4, v6 ); // invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1073 */
final String v4 = "screen_direction_180"; // const-string v4, "screen_direction_180"
/* aget-object v6, v3, v12 */
int v7 = 5; // const/4 v7, 0x5
/* aget v6, v6, v7 */
(( org.json.JSONObject ) v5 ).put ( v4, v6 ); // invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1074 */
final String v4 = "screen_direction_270"; // const-string v4, "screen_direction_270"
/* aget-object v6, v3, v12 */
int v7 = 6; // const/4 v7, 0x6
/* aget v6, v6, v7 */
(( org.json.JSONObject ) v5 ).put ( v4, v6 ); // invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1075 */
(( org.json.JSONObject ) v5 ).toString ( ); // invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_2 */
/* .catch Lorg/json/JSONException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1079 */
} // .end local v5 # "jsonData":Lorg/json/JSONObject;
} // :cond_8
/* .line 1077 */
/* :catch_0 */
/* move-exception v4 */
/* .line 1078 */
/* .local v4, "e":Lorg/json/JSONException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "prepareAllData e = "; // const-string v6, "prepareAllData e = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "OneTrackRotationHelper"; // const-string v6, "OneTrackRotationHelper"
android.util.Slog .e ( v6,v5 );
/* .line 1080 */
} // .end local v2 # "packageName":Ljava/lang/String;
} // .end local v3 # "detail":[[I
} // .end local v4 # "e":Lorg/json/JSONException;
} // :goto_1
/* goto/16 :goto_0 */
/* .line 1081 */
} // :cond_9
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-nez v1, :cond_a */
/* .line 1082 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1084 */
} // :cond_a
} // .end method
private void reportAppUsageData ( ) {
/* .locals 15 */
/* .line 923 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
final String v1 = "OneTrackRotationHelper"; // const-string v1, "OneTrackRotationHelper"
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 924 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->collectData()J */
/* move-result-wide v2 */
/* .line 925 */
/* .local v2, "duration":J */
v0 = this.mCurPackageName;
/* .line 926 */
/* .local v0, "packageName":Ljava/lang/String; */
v4 = this.appUsageDataCache;
(( java.util.HashMap ) v4 ).get ( v0 ); // invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, [[I */
/* .line 927 */
/* .local v4, "appData":[[I */
int v5 = 3; // const/4 v5, 0x3
int v6 = 2; // const/4 v6, 0x2
/* if-nez v4, :cond_1 */
/* .line 928 */
v7 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FLIP ( );
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 933 */
int v7 = 7; // const/4 v7, 0x7
/* filled-new-array {v6, v7}, [I */
v8 = java.lang.Integer.TYPE;
java.lang.reflect.Array .newInstance ( v8,v7 );
/* move-object v4, v7 */
/* check-cast v4, [[I */
/* .line 939 */
} // :cond_0
/* filled-new-array {v6, v5}, [I */
v8 = java.lang.Integer.TYPE;
java.lang.reflect.Array .newInstance ( v8,v7 );
/* move-object v4, v7 */
/* check-cast v4, [[I */
/* .line 941 */
} // :goto_0
v7 = this.appUsageDataCache;
(( java.util.HashMap ) v7 ).put ( v0, v4 ); // invoke-virtual {v7, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 944 */
} // :cond_1
/* iget-boolean v7, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
/* .line 947 */
/* .local v7, "foldIndex":I */
/* iget v8, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I */
int v9 = 1; // const/4 v9, 0x1
int v10 = 0; // const/4 v10, 0x0
/* packed-switch v8, :pswitch_data_0 */
/* .line 954 */
/* :pswitch_0 */
/* aget-object v8, v4, v7 */
/* aget v11, v8, v9 */
/* int-to-long v11, v11 */
/* add-long/2addr v11, v2 */
/* long-to-int v11, v11 */
/* aput v11, v8, v9 */
/* .line 950 */
/* :pswitch_1 */
/* aget-object v8, v4, v7 */
/* aget v11, v8, v10 */
/* int-to-long v11, v11 */
/* add-long/2addr v11, v2 */
/* long-to-int v11, v11 */
/* aput v11, v8, v10 */
/* .line 951 */
/* nop */
/* .line 960 */
} // :goto_1
v8 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FLIP ( );
if ( v8 != null) { // if-eqz v8, :cond_2
/* iget-boolean v8, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 961 */
/* aget-object v8, v4, v7 */
/* aget v11, v8, v5 */
v12 = this.mDirection;
/* aget v13, v12, v10 */
/* add-int/2addr v11, v13 */
/* aput v11, v8, v5 */
/* .line 962 */
/* aget-object v8, v4, v7 */
int v11 = 4; // const/4 v11, 0x4
/* aget v13, v8, v11 */
/* aget v14, v12, v9 */
/* add-int/2addr v13, v14 */
/* aput v13, v8, v11 */
/* .line 963 */
/* aget-object v8, v4, v7 */
int v11 = 5; // const/4 v11, 0x5
/* aget v13, v8, v11 */
/* aget v14, v12, v6 */
/* add-int/2addr v13, v14 */
/* aput v13, v8, v11 */
/* .line 964 */
/* aget-object v8, v4, v7 */
int v11 = 6; // const/4 v11, 0x6
/* aget v13, v8, v11 */
/* aget v12, v12, v5 */
/* add-int/2addr v13, v12 */
/* aput v13, v8, v11 */
/* .line 968 */
} // :cond_2
/* aget-object v8, v4, v7 */
/* aget v11, v8, v6 */
/* iget v12, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I */
/* add-int/2addr v11, v12 */
/* aput v11, v8, v6 */
/* .line 970 */
v8 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 971 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "generate new data = packageName = "; // const-string v11, "generate new data = packageName = "
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " rotation = "; // const-string v11, " rotation = "
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v11, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I */
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = " duration = "; // const-string v11, " duration = "
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v2, v3 ); // invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v11 = " "; // const-string v11, " "
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 974 */
v11 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FLIP ( );
if ( v11 != null) { // if-eqz v11, :cond_3
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "direction[0][90][180][270] = ["; // const-string v12, "direction[0][90][180][270] = ["
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v12 = this.mDirection;
/* aget v10, v12, v10 */
(( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = "]["; // const-string v11, "]["
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v12 = this.mDirection;
/* aget v9, v12, v9 */
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = this.mDirection;
/* aget v6, v10, v6 */
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mDirection;
/* aget v5, v9, v5 */
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = "]"; // const-string v6, "]"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :cond_3
final String v5 = ""; // const-string v5, ""
} // :goto_2
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " cache size = "; // const-string v6, " cache size = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.appUsageDataCache;
/* .line 975 */
v6 = (( java.util.HashMap ) v6 ).size ( ); // invoke-virtual {v6}, Ljava/util/HashMap;->size()I
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 971 */
android.util.Slog .i ( v1,v5 );
/* .line 979 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v2 # "duration":J
} // .end local v4 # "appData":[[I
} // .end local v7 # "foldIndex":I
} // :cond_4
v0 = this.appUsageDataCache;
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* .line 980 */
/* .local v0, "cacheSize":I */
/* iget-boolean v2, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isShutDown:Z */
/* if-nez v2, :cond_5 */
/* if-ge v0, v2, :cond_5 */
/* iget-boolean v2, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 981 */
} // :cond_5
v2 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 982 */
final String v2 = "reportMergedAppUsageData sendAll"; // const-string v2, "reportMergedAppUsageData sendAll"
android.util.Slog .i ( v1,v2 );
/* .line 985 */
} // :cond_6
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->sendAllAppUsageData()V */
/* .line 986 */
v1 = this.oneTrackRotationHelper;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$mprepareNextSending ( v1 );
/* .line 988 */
} // :cond_7
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void reportRotationChange ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "rotation" # I */
/* .line 991 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 992 */
int v0 = 1; // const/4 v0, 0x1
/* packed-switch p1, :pswitch_data_0 */
/* .line 1003 */
/* :pswitch_0 */
v1 = this.mDirection;
int v2 = 3; // const/4 v2, 0x3
/* aget v3, v1, v2 */
/* add-int/2addr v3, v0 */
/* aput v3, v1, v2 */
/* .line 1000 */
/* :pswitch_1 */
v1 = this.mDirection;
int v2 = 2; // const/4 v2, 0x2
/* aget v3, v1, v2 */
/* add-int/2addr v3, v0 */
/* aput v3, v1, v2 */
/* .line 1001 */
/* .line 997 */
/* :pswitch_2 */
v1 = this.mDirection;
/* aget v2, v1, v0 */
/* add-int/2addr v2, v0 */
/* aput v2, v1, v0 */
/* .line 998 */
/* .line 994 */
/* :pswitch_3 */
v1 = this.mDirection;
int v2 = 0; // const/4 v2, 0x0
/* aget v3, v1, v2 */
/* add-int/2addr v3, v0 */
/* aput v3, v1, v2 */
/* .line 995 */
/* nop */
/* .line 1007 */
} // :cond_0
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void resetDirectionCount ( ) {
/* .locals 2 */
/* .line 895 */
v0 = this.mDirection;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 896 */
return;
} // .end method
private void resetLaunchCount ( ) {
/* .locals 1 */
/* .line 891 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I */
/* .line 892 */
return;
} // .end method
private void resetStartTime ( Long p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "time" # J */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 875 */
/* iput-wide p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mStartTime:J */
/* .line 876 */
v0 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 878 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss" */
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 879 */
/* .local v0, "simpleDateFormat":Ljava/text/SimpleDateFormat; */
/* new-instance v1, Ljava/util/Date; */
/* invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v0 ).format ( v1 ); // invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 881 */
/* .local v1, "sTime":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "start recording new data pkgname=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCurPackageName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " rotation = "; // const-string v3, " rotation = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " folded = "; // const-string v3, " folded = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " startTime = "; // const-string v3, " startTime = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " reason = "; // const-string v3, " reason = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "OneTrackRotationHelper"; // const-string v3, "OneTrackRotationHelper"
android.util.Slog .i ( v3,v2 );
/* .line 888 */
} // .end local v0 # "simpleDateFormat":Ljava/text/SimpleDateFormat;
} // .end local v1 # "sTime":Ljava/lang/String;
} // :cond_0
return;
} // .end method
private void sendAllAppUsageData ( ) {
/* .locals 6 */
/* .line 1091 */
v0 = this.appUsageDataCache;
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* if-nez v0, :cond_0 */
/* .line 1092 */
return;
/* .line 1096 */
} // :cond_0
/* new-instance v0, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v1, "yyyyMMdd" */
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 1097 */
/* .local v0, "simpleDateFormat":Ljava/text/SimpleDateFormat; */
/* new-instance v1, Ljava/util/Date; */
/* iget-wide v2, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->dateOfToday:J */
/* invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v0 ).format ( v1 ); // invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 1099 */
/* .local v1, "dateTime":Ljava/lang/String; */
v2 = java.lang.Integer .parseInt ( v1 );
/* .line 1101 */
/* .local v2, "intDateTime":I */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->prepareAllData()Ljava/util/ArrayList; */
/* .line 1103 */
/* .local v3, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v4 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1104 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "sendAllAppUsageData data = " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "OneTrackRotationHelper"; // const-string v5, "OneTrackRotationHelper"
android.util.Slog .v ( v5,v4 );
/* .line 1107 */
} // :cond_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1108 */
v4 = this.oneTrackRotationHelper;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$mreportOneTrack ( v4,v3,v2 );
/* .line 1111 */
} // :cond_2
v4 = this.appUsageDataCache;
(( java.util.HashMap ) v4 ).clear ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->clear()V
/* .line 1112 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetLaunchCount()V */
/* .line 1113 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetDirectionCount()V */
/* .line 1114 */
return;
} // .end method
private void setEndTime ( Long p0 ) {
/* .locals 0 */
/* .param p1, "time" # J */
/* .line 899 */
/* iput-wide p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J */
/* .line 900 */
return;
} // .end method
private void setToday ( Long p0 ) {
/* .locals 0 */
/* .param p1, "time" # J */
/* .line 919 */
/* iput-wide p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->dateOfToday:J */
/* .line 920 */
return;
} // .end method
private void updateFolded ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "folded" # Z */
/* .line 915 */
/* iput-boolean p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
/* .line 916 */
return;
} // .end method
private void updateNewPackage ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 903 */
this.mCurPackageName = p1;
/* .line 904 */
return;
} // .end method
private void updateRotation ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "rotation" # I */
/* .line 911 */
/* iput p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I */
/* .line 912 */
return;
} // .end method
private void updateScreenState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "state" # Z */
/* .line 907 */
/* iput-boolean p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
/* .line 908 */
return;
} // .end method
/* # virtual methods */
public void init ( java.lang.String p0, Boolean p1, Boolean p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "screenState" # Z */
/* .param p3, "folded" # Z */
/* .param p4, "rotation" # I */
/* .line 621 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->initialized:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 622 */
return;
/* .line 625 */
} // :cond_0
this.mCurPackageName = p1;
/* .line 626 */
/* iput-boolean p2, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
/* .line 627 */
/* iput-boolean p3, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
/* .line 628 */
/* iput p4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I */
/* .line 629 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mStartTime:J */
/* .line 630 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I */
/* .line 631 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v0, v0, [I */
this.mDirection = v0;
/* .line 632 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->initialized:Z */
/* .line 634 */
v1 = this.filpUsageDate;
final String v2 = "screen_direction_up"; // const-string v2, "screen_direction_up"
int v3 = 0; // const/4 v3, 0x0
java.lang.Float .valueOf ( v3 );
(( java.util.HashMap ) v1 ).put ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 635 */
v1 = this.filpUsageDate;
final String v2 = "screen_direction_left"; // const-string v2, "screen_direction_left"
java.lang.Float .valueOf ( v3 );
(( java.util.HashMap ) v1 ).put ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 636 */
v1 = this.filpUsageDate;
final String v2 = "screen_direction_down"; // const-string v2, "screen_direction_down"
java.lang.Float .valueOf ( v3 );
(( java.util.HashMap ) v1 ).put ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 637 */
v1 = this.filpUsageDate;
final String v2 = "screen_direction_right"; // const-string v2, "screen_direction_right"
java.lang.Float .valueOf ( v3 );
(( java.util.HashMap ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 639 */
final String v1 = "debug.onetrack.log"; // const-string v1, "debug.onetrack.log"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
final String v2 = "android"; // const-string v2, "android"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v2 = "OneTrackRotationHelper"; // const-string v2, "OneTrackRotationHelper"
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 640 */
final String v1 = "init DEBUG = true"; // const-string v1, "init DEBUG = true"
android.util.Slog .i ( v2,v1 );
/* .line 641 */
com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfputDEBUG ( v0 );
/* .line 642 */
/* const-wide/32 v0, 0x1d4c0 */
com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfputmReportInterval ( v0,v1 );
/* .line 643 */
/* const/16 v0, 0xf */
/* .line 646 */
} // :cond_1
v0 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 647 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "init RotationStateMachine package = "; // const-string v1, "init RotationStateMachine package = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCurPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " screenState = "; // const-string v1, " screenState = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " rotation = "; // const-string v1, " rotation = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " launchCount = "; // const-string v1, " launchCount = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " direction = "; // const-string v1, " direction = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDirection;
/* .line 652 */
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " folded = "; // const-string v1, " folded = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 647 */
android.util.Slog .i ( v2,v0 );
/* .line 656 */
} // :cond_2
v0 = this.oneTrackRotationHelper;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$mprepareNextSending ( v0 );
/* .line 657 */
v0 = this.oneTrackRotationHelper;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$mprepareFinalSendingInToday ( v0 );
/* .line 658 */
return;
} // .end method
public void onDeviceFoldChanged ( android.os.Message p0 ) {
/* .locals 7 */
/* .param p1, "foldMessage" # Landroid/os/Message; */
/* .line 789 */
v0 = this.obj;
/* check-cast v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
/* .line 790 */
/* .local v0, "aumObj":Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 791 */
/* .local v1, "folded":Z */
/* iget-wide v2, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J */
/* .line 792 */
/* .local v2, "newStartTime":J */
v4 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 793 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "onDeviceFoldChanged "; // const-string v5, "onDeviceFoldChanged "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "OneTrackRotationHelper"; // const-string v5, "OneTrackRotationHelper"
android.util.Slog .i ( v5,v4 );
/* .line 796 */
} // :cond_0
/* iget-boolean v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
/* if-eq v4, v1, :cond_3 */
/* .line 797 */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setEndTime(J)V */
/* .line 798 */
/* iget-boolean v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 799 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 801 */
} // :cond_1
v4 = this.oneTrackRotationHelper;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$mreportOneTrackForScreenData ( v4 );
/* .line 802 */
/* invoke-direct {p0, v1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateFolded(Z)V */
/* .line 803 */
v4 = this.oneTrackRotationHelper;
v4 = this.mPm;
v4 = (( android.os.PowerManager ) v4 ).isScreenOn ( ); // invoke-virtual {v4}, Landroid/os/PowerManager;->isScreenOn()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 804 */
v4 = this.oneTrackRotationHelper;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fputcurrentTimeMillis ( v4,v5,v6 );
/* .line 806 */
} // :cond_2
final String v4 = "fold-changed"; // const-string v4, "fold-changed"
/* invoke-direct {p0, v2, v3, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V */
/* .line 808 */
} // :cond_3
return;
} // .end method
public void onForegroundAppChanged ( android.os.Message p0 ) {
/* .locals 8 */
/* .param p1, "foregroundMessage" # Landroid/os/Message; */
/* .line 666 */
v0 = this.obj;
/* check-cast v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
/* .line 667 */
/* .local v0, "aumObj":Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
/* .line 669 */
/* .local v1, "newPackageName":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 670 */
return;
/* .line 673 */
} // :cond_0
v2 = this.mCurPackageName;
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_3 */
/* .line 674 */
v2 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 675 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "onForegroundAppChanged pkgname="; // const-string v3, "onForegroundAppChanged pkgname="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "OneTrackRotationHelper"; // const-string v3, "OneTrackRotationHelper"
android.util.Slog .i ( v3,v2 );
/* .line 677 */
} // :cond_1
/* iget-wide v2, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J */
/* .line 678 */
/* .local v2, "newStartTime":J */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setEndTime(J)V */
/* .line 679 */
/* iget v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I */
int v5 = 1; // const/4 v5, 0x1
/* add-int/2addr v4, v5 */
/* iput v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I */
/* .line 682 */
v4 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FLIP ( );
if ( v4 != null) { // if-eqz v4, :cond_2
/* iget-boolean v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 683 */
/* iget v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I */
/* packed-switch v4, :pswitch_data_0 */
/* .line 694 */
/* :pswitch_0 */
v4 = this.mDirection;
int v6 = 3; // const/4 v6, 0x3
/* aget v7, v4, v6 */
/* add-int/2addr v7, v5 */
/* aput v7, v4, v6 */
/* .line 691 */
/* :pswitch_1 */
v4 = this.mDirection;
int v6 = 2; // const/4 v6, 0x2
/* aget v7, v4, v6 */
/* add-int/2addr v7, v5 */
/* aput v7, v4, v6 */
/* .line 692 */
/* .line 688 */
/* :pswitch_2 */
v4 = this.mDirection;
/* aget v6, v4, v5 */
/* add-int/2addr v6, v5 */
/* aput v6, v4, v5 */
/* .line 689 */
/* .line 685 */
/* :pswitch_3 */
v4 = this.mDirection;
int v6 = 0; // const/4 v6, 0x0
/* aget v7, v4, v6 */
/* add-int/2addr v7, v5 */
/* aput v7, v4, v6 */
/* .line 686 */
/* nop */
/* .line 699 */
} // :cond_2
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 700 */
/* invoke-direct {p0, v1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateNewPackage(Ljava/lang/String;)V */
/* .line 701 */
final String v4 = "new-package"; // const-string v4, "new-package"
/* invoke-direct {p0, v2, v3, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V */
/* .line 702 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetLaunchCount()V */
/* .line 703 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetDirectionCount()V */
/* .line 705 */
} // .end local v2 # "newStartTime":J
} // :cond_3
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onNextSending ( ) {
/* .locals 3 */
/* .line 815 */
v0 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 816 */
final String v0 = "OneTrackRotationHelper"; // const-string v0, "OneTrackRotationHelper"
final String v1 = "onNextSending"; // const-string v1, "onNextSending"
android.util.Slog .i ( v0,v1 );
/* .line 819 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z */
/* .line 820 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 821 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 823 */
/* .local v0, "now":J */
/* iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J */
/* .line 824 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 825 */
final String v2 = "next-sending"; // const-string v2, "next-sending"
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V */
/* .line 826 */
} // .end local v0 # "now":J
/* .line 827 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 829 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z */
/* .line 830 */
return;
} // .end method
public void onRotationChanged ( android.os.Message p0 ) {
/* .locals 9 */
/* .param p1, "rotationMessage" # Landroid/os/Message; */
/* .line 754 */
v0 = this.obj;
/* check-cast v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
/* .line 755 */
/* .local v0, "aumObj":Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* .line 756 */
/* .local v1, "displayId":I */
/* iget v2, p1, Landroid/os/Message;->arg2:I */
/* .line 757 */
/* .local v2, "rotation":I */
/* iget-wide v3, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J */
/* .line 759 */
/* .local v3, "newStartTime":J */
v5 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 760 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "onRotationChanged "; // const-string v6, "onRotationChanged "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "OneTrackRotationHelper"; // const-string v6, "OneTrackRotationHelper"
android.util.Slog .i ( v6,v5 );
/* .line 763 */
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 764 */
return;
/* .line 767 */
} // :cond_1
/* iget v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I */
/* if-eq v2, v5, :cond_6 */
/* .line 769 */
/* invoke-direct {p0, v3, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setEndTime(J)V */
/* .line 770 */
/* iget-boolean v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 771 */
/* invoke-direct {p0, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportRotationChange(I)V */
/* .line 772 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 774 */
} // :cond_2
v5 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FLIP ( );
if ( v5 != null) { // if-eqz v5, :cond_4
/* iget-boolean v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 775 */
v5 = this.oneTrackRotationHelper;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetcurrentTimeMillis ( v5 );
/* move-result-wide v5 */
/* const-wide/16 v7, 0x0 */
/* cmp-long v5, v5, v7 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 776 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
v7 = this.oneTrackRotationHelper;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fgetcurrentTimeMillis ( v7 );
/* move-result-wide v7 */
/* sub-long/2addr v5, v7 */
/* long-to-float v5, v5 */
/* const/high16 v6, 0x447a0000 # 1000.0f */
/* div-float/2addr v5, v6 */
} // :cond_3
int v5 = 0; // const/4 v5, 0x0
/* .line 777 */
/* .local v5, "duration":F */
} // :goto_0
v6 = this.oneTrackRotationHelper;
com.android.server.wm.OneTrackRotationHelper .-$$Nest$mrecordTimeForFlip ( v6,v5 );
/* .line 779 */
} // .end local v5 # "duration":F
} // :cond_4
/* invoke-direct {p0, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateRotation(I)V */
/* .line 780 */
v5 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetIS_FLIP ( );
if ( v5 != null) { // if-eqz v5, :cond_5
/* iget-boolean v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 781 */
v5 = this.oneTrackRotationHelper;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fputcurrentTimeMillis ( v5,v6,v7 );
/* .line 783 */
} // :cond_5
final String v5 = "rotation-changed"; // const-string v5, "rotation-changed"
/* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V */
/* .line 784 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetDirectionCount()V */
/* .line 786 */
} // :cond_6
return;
} // .end method
public void onScreenStateChanged ( android.os.Message p0 ) {
/* .locals 8 */
/* .param p1, "screenStateMessage" # Landroid/os/Message; */
/* .line 715 */
v0 = this.obj;
/* check-cast v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
/* .line 716 */
/* .local v0, "aumObj":Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj; */
v1 = this.obj;
/* check-cast v1, Landroid/content/Intent; */
/* .line 717 */
/* .local v1, "intent":Landroid/content/Intent; */
/* if-nez v1, :cond_0 */
/* .line 718 */
return;
/* .line 721 */
} // :cond_0
v2 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 722 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "onScreenStateChanged "; // const-string v3, "onScreenStateChanged "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) v1 ).getAction ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "OneTrackRotationHelper"; // const-string v3, "OneTrackRotationHelper"
android.util.Slog .i ( v3,v2 );
/* .line 725 */
} // :cond_1
(( android.content.Intent ) v1 ).getAction ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 726 */
/* .local v2, "action":Ljava/lang/String; */
/* iget-wide v3, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J */
/* .line 727 */
/* .local v3, "newStartTime":J */
final String v5 = "android.intent.action.USER_PRESENT"; // const-string v5, "android.intent.action.USER_PRESENT"
v5 = (( java.lang.String ) v5 ).equals ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 728 */
/* iget-boolean v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
/* if-nez v5, :cond_2 */
/* .line 729 */
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {p0, v5}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateScreenState(Z)V */
/* .line 730 */
final String v5 = "screen-on"; // const-string v5, "screen-on"
/* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V */
/* .line 732 */
} // :cond_2
v5 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetmIsScreenOnNotification ( );
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 733 */
v5 = this.oneTrackRotationHelper;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
com.android.server.wm.OneTrackRotationHelper .-$$Nest$fputcurrentTimeMillis ( v5,v6,v7 );
/* .line 734 */
int v5 = 0; // const/4 v5, 0x0
com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfputmIsScreenOnNotification ( v5 );
/* .line 736 */
} // :cond_3
final String v5 = "android.intent.action.SCREEN_OFF"; // const-string v5, "android.intent.action.SCREEN_OFF"
v5 = (( java.lang.String ) v5 ).equals ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 737 */
/* invoke-direct {p0, v3, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setEndTime(J)V */
/* .line 738 */
v5 = this.oneTrackRotationHelper;
v5 = (( com.android.server.wm.OneTrackRotationHelper ) v5 ).isScreenRealUnlocked ( ); // invoke-virtual {v5}, Lcom/android/server/wm/OneTrackRotationHelper;->isScreenRealUnlocked()Z
/* .line 739 */
/* .local v5, "realScreenState":Z */
/* if-nez v5, :cond_4 */
/* iget-boolean v6, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 740 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 742 */
} // :cond_4
/* invoke-direct {p0, v5}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateScreenState(Z)V */
/* .line 743 */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 744 */
final String v6 = "real-screen-on"; // const-string v6, "real-screen-on"
/* invoke-direct {p0, v3, v4, v6}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V */
/* .line 747 */
} // .end local v5 # "realScreenState":Z
} // :cond_5
} // :goto_0
return;
} // .end method
public void onShutDown ( ) {
/* .locals 3 */
/* .line 836 */
v0 = com.android.server.wm.OneTrackRotationHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 837 */
final String v0 = "OneTrackRotationHelper"; // const-string v0, "OneTrackRotationHelper"
final String v1 = "onShutDown"; // const-string v1, "onShutDown"
android.util.Slog .i ( v0,v1 );
/* .line 840 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isShutDown:Z */
/* .line 841 */
/* iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 842 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 844 */
/* .local v0, "now":J */
/* iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J */
/* .line 845 */
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 846 */
/* const-string/jumbo v2, "shutdown" */
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V */
/* .line 847 */
} // .end local v0 # "now":J
/* .line 848 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V */
/* .line 850 */
} // :goto_0
return;
} // .end method
