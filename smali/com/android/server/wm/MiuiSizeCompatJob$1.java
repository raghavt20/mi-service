class com.android.server.wm.MiuiSizeCompatJob$1 implements android.content.ServiceConnection {
	 /* .source "MiuiSizeCompatJob.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiSizeCompatJob; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiSizeCompatJob this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiSizeCompatJob$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiSizeCompatJob; */
/* .line 101 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 4 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 104 */
v0 = this.this$0;
com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmTrackLock ( v0 );
/* monitor-enter v0 */
/* .line 105 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.miui.analytics.ITrackBinder$Stub .asInterface ( p2 );
	 com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fputmITrackBinder ( v1,v2 );
	 /* .line 106 */
	 v1 = this.this$0;
	 com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmITrackBinder ( v1 );
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 108 */
		 try { // :try_start_1
			 v1 = this.this$0;
			 com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmITrackBinder ( v1 );
			 v2 = this.this$0;
			 com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmDeathRecipient ( v2 );
			 int v3 = 0; // const/4 v3, 0x0
			 /* :try_end_1 */
			 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 111 */
			 /* .line 109 */
			 /* :catch_0 */
			 /* move-exception v1 */
			 /* .line 110 */
			 /* .local v1, "e":Landroid/os/RemoteException; */
			 try { // :try_start_2
				 (( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
				 /* .line 113 */
			 } // .end local v1 # "e":Landroid/os/RemoteException;
		 } // :cond_0
	 } // :goto_0
	 /* monitor-exit v0 */
	 /* :try_end_2 */
	 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
	 /* .line 114 */
	 final String v0 = "MiuiSizeCompatJob"; // const-string v0, "MiuiSizeCompatJob"
	 final String v1 = "Bind OneTrack service success."; // const-string v1, "Bind OneTrack service success."
	 android.util.Log .d ( v0,v1 );
	 /* .line 115 */
	 return;
	 /* .line 113 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 try { // :try_start_3
		 /* monitor-exit v0 */
		 /* :try_end_3 */
		 /* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
		 /* throw v1 */
	 } // .end method
	 public void onServiceDisconnected ( android.content.ComponentName p0 ) {
		 /* .locals 3 */
		 /* .param p1, "name" # Landroid/content/ComponentName; */
		 /* .line 119 */
		 final String v0 = "MiuiSizeCompatJob"; // const-string v0, "MiuiSizeCompatJob"
		 final String v1 = "OneTrack service disconnected."; // const-string v1, "OneTrack service disconnected."
		 android.util.Log .d ( v0,v1 );
		 /* .line 120 */
		 v0 = this.this$0;
		 com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmTrackLock ( v0 );
		 /* monitor-enter v0 */
		 /* .line 121 */
		 try { // :try_start_0
			 v1 = this.this$0;
			 int v2 = 0; // const/4 v2, 0x0
			 com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fputmITrackBinder ( v1,v2 );
			 /* .line 122 */
			 /* monitor-exit v0 */
			 /* .line 123 */
			 return;
			 /* .line 122 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 /* monitor-exit v0 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* throw v1 */
		 } // .end method
