class com.android.server.wm.WindowManagerServiceImpl$5 implements java.lang.Runnable {
	 /* .source "WindowManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/WindowManagerServiceImpl;->updateContrastAlpha(Z)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.WindowManagerServiceImpl this$0; //synthetic
final Boolean val$darkmode; //synthetic
/* # direct methods */
 com.android.server.wm.WindowManagerServiceImpl$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/WindowManagerServiceImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 1315 */
this.this$0 = p1;
/* iput-boolean p2, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->val$darkmode:Z */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 1318 */
v0 = this.this$0;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "contrast_alpha"; // const-string v1, "contrast_alpha"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getFloat ( v0,v1,v2 );
/* .line 1320 */
/* .local v0, "alpha":F */
/* const/high16 v1, 0x3f000000 # 0.5f */
/* cmpl-float v1, v0, v1 */
/* if-ltz v1, :cond_0 */
/* .line 1321 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1323 */
} // :cond_0
v1 = this.this$0;
v1 = com.android.server.wm.WindowManagerServiceImpl .-$$Nest$misDarkModeContrastEnable ( v1 );
/* .line 1324 */
/* .local v1, "isContrastEnabled":Z */
final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updateContrastOverlay, darkmode: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->val$darkmode:Z */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " isContrastEnabled: "; // const-string v4, " isContrastEnabled: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " alpha: "; // const-string v4, " alpha: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 1326 */
v2 = this.this$0;
v2 = this.mWmService;
v2 = this.mWindowMap;
/* monitor-enter v2 */
/* .line 1327 */
try { // :try_start_0
v3 = this.this$0;
v3 = this.mWmService;
(( com.android.server.wm.WindowManagerService ) v3 ).openSurfaceTransaction ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1329 */
try { // :try_start_1
/* iget-boolean v3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$5;->val$darkmode:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 1330 */
		 v3 = this.this$0;
		 v3 = this.mMiuiContrastOverlay;
		 /* if-nez v3, :cond_1 */
		 /* .line 1331 */
		 v3 = this.this$0;
		 com.android.server.wm.MiuiContrastOverlayStub .getInstance ( );
		 this.mMiuiContrastOverlay = v4;
		 /* .line 1332 */
		 v3 = this.this$0;
		 v3 = this.mWmService;
		 (( com.android.server.wm.WindowManagerService ) v3 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
		 /* .line 1333 */
		 /* .local v3, "displayContent":Lcom/android/server/wm/DisplayContent; */
		 v4 = this.this$0;
		 v4 = this.mMiuiContrastOverlay;
		 v5 = this.mRealDisplayMetrics;
		 v6 = this.this$0;
		 v6 = this.mContext;
		 /* .line 1335 */
	 } // .end local v3 # "displayContent":Lcom/android/server/wm/DisplayContent;
} // :cond_1
v3 = this.this$0;
v3 = this.mMiuiContrastOverlay;
/* .line 1337 */
} // :cond_2
v3 = this.this$0;
v3 = this.mMiuiContrastOverlay;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1338 */
final String v3 = "WindowManagerService"; // const-string v3, "WindowManagerService"
final String v4 = " hideContrastOverlay "; // const-string v4, " hideContrastOverlay "
android.util.Slog .i ( v3,v4 );
/* .line 1339 */
v3 = this.this$0;
v3 = this.mMiuiContrastOverlay;
/* .line 1341 */
} // :cond_3
v3 = this.this$0;
int v4 = 0; // const/4 v4, 0x0
this.mMiuiContrastOverlay = v4;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1344 */
} // :goto_0
try { // :try_start_2
v3 = this.this$0;
v3 = this.mWmService;
final String v4 = "MiuiContrastOverlay"; // const-string v4, "MiuiContrastOverlay"
(( com.android.server.wm.WindowManagerService ) v3 ).closeSurfaceTransaction ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* .line 1345 */
/* nop */
/* .line 1346 */
/* monitor-exit v2 */
/* .line 1347 */
return;
/* .line 1344 */
/* :catchall_0 */
/* move-exception v3 */
v4 = this.this$0;
v4 = this.mWmService;
final String v5 = "MiuiContrastOverlay"; // const-string v5, "MiuiContrastOverlay"
(( com.android.server.wm.WindowManagerService ) v4 ).closeSurfaceTransaction ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* .line 1345 */
/* nop */
} // .end local v0 # "alpha":F
} // .end local v1 # "isContrastEnabled":Z
} // .end local p0 # "this":Lcom/android/server/wm/WindowManagerServiceImpl$5;
/* throw v3 */
/* .line 1346 */
/* .restart local v0 # "alpha":F */
/* .restart local v1 # "isContrastEnabled":Z */
/* .restart local p0 # "this":Lcom/android/server/wm/WindowManagerServiceImpl$5; */
/* :catchall_1 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v3 */
} // .end method
