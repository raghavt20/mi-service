public class com.android.server.wm.LetterboxImpl extends com.android.server.wm.LetterboxStub {
	 /* .source "LetterboxImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BITMAP_SURFACE_NAME;
private static final java.lang.String FLIP_SMALL_SURFACE_NAME;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.wm.ActivityRecord mActivityRecord;
protected com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx mBackgroundSurfaceEx;
private Boolean mIsDarkMode;
private com.android.server.wm.Letterbox mLetterbox;
private Boolean mNeedRedraw;
/* # direct methods */
static com.android.server.wm.ActivityRecord -$$Nest$fgetmActivityRecord ( com.android.server.wm.LetterboxImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mActivityRecord;
} // .end method
static com.android.server.wm.Letterbox -$$Nest$fgetmLetterbox ( com.android.server.wm.LetterboxImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mLetterbox;
} // .end method
static void -$$Nest$mupdateColor ( com.android.server.wm.LetterboxImpl p0, android.view.SurfaceControl p1, android.view.SurfaceControl$Transaction p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/LetterboxImpl;->updateColor(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;)V */
	 return;
} // .end method
public com.android.server.wm.LetterboxImpl ( ) {
	 /* .locals 1 */
	 /* .line 32 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/LetterboxStub;-><init>()V */
	 /* .line 43 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z */
	 return;
} // .end method
private void updateColor ( android.view.SurfaceControl p0, android.view.SurfaceControl$Transaction p1 ) {
	 /* .locals 4 */
	 /* .param p1, "sc" # Landroid/view/SurfaceControl; */
	 /* .param p2, "sct" # Landroid/view/SurfaceControl$Transaction; */
	 /* .line 428 */
	 v0 = this.mActivityRecord;
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-nez v0, :cond_0 */
	 /* .line 429 */
	 android.graphics.Color .valueOf ( v1 );
	 /* .local v0, "color":Landroid/graphics/Color; */
	 /* .line 431 */
} // .end local v0 # "color":Landroid/graphics/Color;
} // :cond_0
v0 = v0 = this.mActivityRecordStub;
android.graphics.Color .valueOf ( v0 );
/* .line 433 */
/* .restart local v0 # "color":Landroid/graphics/Color; */
} // :goto_0
int v2 = 3; // const/4 v2, 0x3
/* new-array v2, v2, [F */
v3 = (( android.graphics.Color ) v0 ).red ( ); // invoke-virtual {v0}, Landroid/graphics/Color;->red()F
/* aput v3, v2, v1 */
int v1 = 1; // const/4 v1, 0x1
v3 = (( android.graphics.Color ) v0 ).green ( ); // invoke-virtual {v0}, Landroid/graphics/Color;->green()F
/* aput v3, v2, v1 */
int v1 = 2; // const/4 v1, 0x2
v3 = (( android.graphics.Color ) v0 ).blue ( ); // invoke-virtual {v0}, Landroid/graphics/Color;->blue()F
/* aput v3, v2, v1 */
(( android.view.SurfaceControl$Transaction ) p2 ).setColor ( p1, v2 ); // invoke-virtual {p2, p1, v2}, Landroid/view/SurfaceControl$Transaction;->setColor(Landroid/view/SurfaceControl;[F)Landroid/view/SurfaceControl$Transaction;
/* .line 434 */
return;
} // .end method
/* # virtual methods */
public Boolean applyLetterboxSurfaceChanges ( android.view.SurfaceControl$Transaction p0, android.graphics.Rect p1, android.graphics.Rect p2, java.lang.String p3 ) {
/* .locals 1 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "mSurfaceFrameRelative" # Landroid/graphics/Rect; */
/* .param p3, "mLayoutFrameRelative" # Landroid/graphics/Rect; */
/* .param p4, "mType" # Ljava/lang/String; */
/* .line 133 */
v0 = this.mBackgroundSurfaceEx;
v0 = (( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).applyLetterboxSurfaceChanges ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->applyLetterboxSurfaceChanges(Landroid/view/SurfaceControl$Transaction;Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/String;)Z
} // .end method
public void applySurfaceChanges ( android.view.SurfaceControl$Transaction p0, com.android.server.wm.Letterbox$LetterboxSurface[] p1, com.android.server.wm.Letterbox$LetterboxSurface p2 ) {
/* .locals 3 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "mSurfaces" # [Lcom/android/server/wm/Letterbox$LetterboxSurface; */
/* .param p3, "mBehind" # Lcom/android/server/wm/Letterbox$LetterboxSurface; */
/* .line 99 */
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).applySurfaceChanges ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->applySurfaceChanges(Landroid/view/SurfaceControl$Transaction;)V
/* .line 101 */
/* array-length v0, p2 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* aget-object v2, p2, v1 */
/* .line 102 */
/* .local v2, "surface":Lcom/android/server/wm/Letterbox$LetterboxSurface; */
(( com.android.server.wm.Letterbox$LetterboxSurface ) v2 ).remove ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Letterbox$LetterboxSurface;->remove()V
/* .line 101 */
} // .end local v2 # "surface":Lcom/android/server/wm/Letterbox$LetterboxSurface;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 105 */
} // :cond_0
(( com.android.server.wm.Letterbox$LetterboxSurface ) p3 ).remove ( ); // invoke-virtual {p3}, Lcom/android/server/wm/Letterbox$LetterboxSurface;->remove()V
/* .line 106 */
return;
} // .end method
public Boolean attachInput ( com.android.server.wm.WindowState p0 ) {
/* .locals 1 */
/* .param p1, "win" # Lcom/android/server/wm/WindowState; */
/* .line 57 */
v0 = this.mActivityRecord;
this.mActivityRecord = v0;
/* .line 58 */
v0 = (( com.android.server.wm.LetterboxImpl ) p0 ).useMiuiBackgroundWindowSurface ( ); // invoke-virtual {p0}, Lcom/android/server/wm/LetterboxImpl;->useMiuiBackgroundWindowSurface()Z
/* if-nez v0, :cond_0 */
/* .line 59 */
int v0 = 0; // const/4 v0, 0x0
/* .line 61 */
} // :cond_0
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).attachInput ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->attachInput(Lcom/android/server/wm/WindowState;)V
/* .line 62 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public android.view.SurfaceControl createLetterboxSurfaceSurface ( android.view.SurfaceControl$Transaction p0, java.lang.String p1, java.util.function.Supplier p2 ) {
/* .locals 1 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "mType" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/view/SurfaceControl$Transaction;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/function/Supplier<", */
/* "Landroid/view/SurfaceControl$Builder;", */
/* ">;)", */
/* "Landroid/view/SurfaceControl;" */
/* } */
} // .end annotation
/* .line 117 */
/* .local p3, "mSurfaceControlFactory":Ljava/util/function/Supplier;, "Ljava/util/function/Supplier<Landroid/view/SurfaceControl$Builder;>;" */
v0 = (( com.android.server.wm.LetterboxImpl ) p0 ).isFlipSmallSurface ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/LetterboxImpl;->isFlipSmallSurface(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 118 */
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).createFlipSmallSurface ( p1, p3 ); // invoke-virtual {v0, p1, p3}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->createFlipSmallSurface(Landroid/view/SurfaceControl$Transaction;Ljava/util/function/Supplier;)Landroid/view/SurfaceControl;
/* .line 120 */
} // :cond_0
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).createSurface ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->createSurface(Landroid/view/SurfaceControl$Transaction;Ljava/lang/String;Ljava/util/function/Supplier;)Landroid/view/SurfaceControl;
} // .end method
public Boolean darkModeChanged ( ) {
/* .locals 5 */
/* .line 80 */
v0 = this.mActivityRecord;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).isEmbedded ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 81 */
v0 = this.mActivityRecord;
v0 = this.mAtmService;
v0 = this.mContext;
/* .line 82 */
/* .local v0, "context":Landroid/content/Context; */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v2 ).getConfiguration ( ); // invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* iget v2, v2, Landroid/content/res/Configuration;->uiMode:I */
/* and-int/lit8 v2, v2, 0x30 */
/* const/16 v3, 0x20 */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v2, v3, :cond_0 */
/* move v2, v4 */
} // :cond_0
/* move v2, v1 */
/* .line 84 */
/* .local v2, "isDarkMode":Z */
} // :goto_0
/* iget-boolean v3, p0, Lcom/android/server/wm/LetterboxImpl;->mIsDarkMode:Z */
/* if-eq v3, v2, :cond_1 */
/* .line 85 */
/* iput-boolean v2, p0, Lcom/android/server/wm/LetterboxImpl;->mIsDarkMode:Z */
/* .line 86 */
/* iput-boolean v4, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z */
/* .line 87 */
/* .line 90 */
} // .end local v0 # "context":Landroid/content/Context;
} // .end local v2 # "isDarkMode":Z
} // :cond_1
} // .end method
public Integer getNavigationBarColor ( ) {
/* .locals 1 */
/* .line 198 */
v0 = this.mActivityRecord;
/* if-nez v0, :cond_0 */
/* .line 199 */
int v0 = 0; // const/4 v0, 0x0
/* .line 200 */
} // :cond_0
v0 = v0 = this.mActivityRecordStub;
} // .end method
public Boolean isBitmapSurface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "mType" # Ljava/lang/String; */
/* .line 124 */
final String v0 = "bitmapBackground"; // const-string v0, "bitmapBackground"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isFlipSmallSurface ( ) {
/* .locals 1 */
/* .line 438 */
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).getType ( ); // invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getType()Ljava/lang/String;
v0 = (( com.android.server.wm.LetterboxImpl ) p0 ).isFlipSmallSurface ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/LetterboxImpl;->isFlipSmallSurface(Ljava/lang/String;)Z
} // .end method
public Boolean isFlipSmallSurface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "type" # Ljava/lang/String; */
/* .line 128 */
final String v0 = "flipSmallBackground"; // const-string v0, "flipSmallBackground"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public void layout ( android.graphics.Rect p0, android.graphics.Rect p1, android.graphics.Point p2 ) {
/* .locals 6 */
/* .param p1, "outer" # Landroid/graphics/Rect; */
/* .param p2, "inner" # Landroid/graphics/Rect; */
/* .param p3, "surfaceOrigin" # Landroid/graphics/Point; */
/* .line 66 */
v0 = this.mBackgroundSurfaceEx;
/* iget v1, p1, Landroid/graphics/Rect;->left:I */
/* iget v2, p1, Landroid/graphics/Rect;->top:I */
/* iget v3, p1, Landroid/graphics/Rect;->right:I */
/* iget v4, p1, Landroid/graphics/Rect;->bottom:I */
/* move-object v5, p3 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->layout(IIIILandroid/graphics/Point;)V */
/* .line 67 */
return;
} // .end method
public Boolean navigationBarColorChanged ( ) {
/* .locals 1 */
/* .line 443 */
v0 = this.mActivityRecord;
v0 = v0 = this.mActivityRecordStub;
} // .end method
public Boolean needRemoveMiuiLetterbox ( ) {
/* .locals 4 */
/* .line 187 */
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mBackgroundSurfaceEx;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 189 */
v0 = (( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).needRemoveMiuiLetterbox ( ); // invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->needRemoveMiuiLetterbox()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mActivityRecord;
/* .line 190 */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).isClientVisible ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isClientVisible()Z
/* if-nez v0, :cond_0 */
v0 = this.mActivityRecord;
v1 = com.android.server.wm.ActivityRecord$State.STOPPED;
v2 = com.android.server.wm.ActivityRecord$State.DESTROYING;
v3 = com.android.server.wm.ActivityRecord$State.DESTROYED;
/* .line 191 */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).isState ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;Lcom/android/server/wm/ActivityRecord$State;Lcom/android/server/wm/ActivityRecord$State;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 192 */
int v0 = 1; // const/4 v0, 0x1
/* .line 194 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean needShowLetterbox ( ) {
/* .locals 3 */
/* .line 173 */
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 174 */
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
v1 = this.mActivityRecord;
v0 = int v2 = 1; // const/4 v2, 0x1
/* if-nez v0, :cond_0 */
/* .line 175 */
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
v1 = this.mActivityRecord;
v0 = v1 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mActivityRecord;
/* .line 176 */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).isActivityEmbedded ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/ActivityRecord;->isActivityEmbedded(Z)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 177 */
} // :cond_0
v0 = this.mActivityRecord;
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).isVisible ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isVisible()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 178 */
/* .line 180 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 183 */
} // :cond_2
v0 = /* invoke-super {p0}, Lcom/android/server/wm/LetterboxStub;->needShowLetterbox()Z */
} // .end method
public Boolean needsApplySurfaceChanges ( ) {
/* .locals 1 */
/* .line 94 */
v0 = this.mBackgroundSurfaceEx;
v0 = (( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).needsApplySurfaceChanges ( ); // invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->needsApplySurfaceChanges()Z
} // .end method
public void onMovedToDisplay ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .line 109 */
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).getInputInterceptor ( ); // invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getInputInterceptor()Lcom/android/server/wm/Letterbox$InputInterceptor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 110 */
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).getInputInterceptor ( ); // invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getInputInterceptor()Lcom/android/server/wm/Letterbox$InputInterceptor;
v0 = this.mWindowHandle;
/* iput p1, v0, Landroid/view/InputWindowHandle;->displayId:I */
/* .line 112 */
} // :cond_0
return;
} // .end method
public void redraw ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "forceRedraw" # Z */
/* .line 138 */
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 139 */
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
v1 = this.mActivityRecord;
v0 = int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 141 */
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).redrawBitmapSurface ( ); // invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->redrawBitmapSurface()V
/* .line 142 */
return;
/* .line 145 */
} // :cond_0
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 146 */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).isEmbedded ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z */
/* if-nez v0, :cond_1 */
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 147 */
} // :cond_1
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).redrawBitmapSurface ( ); // invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->redrawBitmapSurface()V
/* .line 148 */
/* iget-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 149 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z */
/* .line 151 */
} // :cond_2
return;
/* .line 154 */
} // :cond_3
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 155 */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).inMiuiSizeCompatMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z
if ( v0 != null) { // if-eqz v0, :cond_4
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 157 */
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).redrawBitmapSurface ( ); // invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->redrawBitmapSurface()V
/* .line 160 */
} // :cond_4
v0 = (( com.android.server.wm.LetterboxImpl ) p0 ).navigationBarColorChanged ( ); // invoke-virtual {p0}, Lcom/android/server/wm/LetterboxImpl;->navigationBarColorChanged()Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 161 */
try { // :try_start_0
/* new-instance v0, Landroid/view/SurfaceControl$Transaction; */
/* invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 162 */
/* .local v0, "sct":Landroid/view/SurfaceControl$Transaction; */
try { // :try_start_1
	 v1 = this.mBackgroundSurfaceEx;
	 (( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v1 ).getSurface ( ); // invoke-virtual {v1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getSurface()Landroid/view/SurfaceControl;
	 /* invoke-direct {p0, v1, v0}, Lcom/android/server/wm/LetterboxImpl;->updateColor(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;)V */
	 /* .line 163 */
	 (( android.view.SurfaceControl$Transaction ) v0 ).apply ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 164 */
	 try { // :try_start_2
		 (( android.view.SurfaceControl$Transaction ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->close()V
		 /* :try_end_2 */
		 /* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
		 /* .line 165 */
	 } // .end local v0 # "sct":Landroid/view/SurfaceControl$Transaction;
	 /* .line 161 */
	 /* .restart local v0 # "sct":Landroid/view/SurfaceControl$Transaction; */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 try { // :try_start_3
		 (( android.view.SurfaceControl$Transaction ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->close()V
		 /* :try_end_3 */
		 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
		 /* :catchall_1 */
		 /* move-exception v2 */
		 try { // :try_start_4
			 (( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
		 } // .end local p0 # "this":Lcom/android/server/wm/LetterboxImpl;
	 } // .end local p1 # "forceRedraw":Z
} // :goto_0
/* throw v1 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 164 */
} // .end local v0 # "sct":Landroid/view/SurfaceControl$Transaction;
/* .restart local p0 # "this":Lcom/android/server/wm/LetterboxImpl; */
/* .restart local p1 # "forceRedraw":Z */
/* :catch_0 */
/* move-exception v0 */
/* .line 166 */
} // :goto_1
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 167 */
v0 = this.mActivityRecordStub;
/* .line 170 */
} // :cond_5
return;
} // .end method
public void remove ( ) {
/* .locals 1 */
/* .line 70 */
v0 = this.mBackgroundSurfaceEx;
(( com.android.server.wm.LetterboxImpl$LetterboxSurfaceEx ) v0 ).remove ( ); // invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->remove()V
/* .line 71 */
return;
} // .end method
public void setLetterbox ( com.android.server.wm.Letterbox p0 ) {
/* .locals 3 */
/* .param p1, "letterbox" # Lcom/android/server/wm/Letterbox; */
/* .line 46 */
this.mLetterbox = p1;
/* .line 47 */
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 48 */
android.hardware.devicestate.DeviceStateManagerGlobal .getInstance ( );
v0 = (( android.hardware.devicestate.DeviceStateManagerGlobal ) v0 ).getCurrentState ( ); // invoke-virtual {v0}, Landroid/hardware/devicestate/DeviceStateManagerGlobal;->getCurrentState()I
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 49 */
/* .local v0, "isFlipSmallScreen":Z */
} // :goto_0
/* new-instance v1, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx; */
/* .line 50 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 51 */
final String v2 = "flipSmallBackground"; // const-string v2, "flipSmallBackground"
/* .line 52 */
} // :cond_1
final String v2 = "bitmapBackground"; // const-string v2, "bitmapBackground"
} // :goto_1
/* invoke-direct {v1, p0, v2, p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;-><init>(Lcom/android/server/wm/LetterboxImpl;Ljava/lang/String;Lcom/android/server/wm/LetterboxImpl;)V */
this.mBackgroundSurfaceEx = v1;
/* .line 54 */
return;
} // .end method
public Boolean useMiuiBackgroundWindowSurface ( ) {
/* .locals 2 */
/* .line 74 */
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 75 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mActivityRecord;
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).inMiuiSizeCompatMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z
/* if-nez v0, :cond_1 */
/* .line 76 */
} // :cond_0
com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
v0 = v1 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 74 */
} // :goto_0
} // .end method
