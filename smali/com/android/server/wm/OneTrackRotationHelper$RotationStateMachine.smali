.class Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;
.super Ljava/lang/Object;
.source "OneTrackRotationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/OneTrackRotationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RotationStateMachine"
.end annotation


# static fields
.field private static final DATA_CACHED_THRESHOLD:I = 0x1e

.field private static final DATA_CACHED_THRESHOLD_DEBUG:I = 0xf

.field private static mCacheThreshold:I


# instance fields
.field private final appUsageDataCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[[I>;"
        }
    .end annotation
.end field

.field private dateOfToday:J

.field private filpUsageDate:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private initialized:Z

.field private isNextSending:Z

.field private isShutDown:Z

.field private mCurPackageName:Ljava/lang/String;

.field private mDirection:[I

.field private mDisplayRotation:I

.field private mEndTime:J

.field private mFolded:Z

.field private mLaunchCount:I

.field private mScreenState:Z

.field private mStartTime:J

.field private final oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;


# direct methods
.method static bridge synthetic -$$Nest$fgetfilpUsageDate(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->filpUsageDate:Ljava/util/HashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayRotation(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)I
    .locals 0

    iget p0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmFolded(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetoneTrackRotationHelper(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Lcom/android/server/wm/OneTrackRotationHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$monTodayIsOver(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onTodayIsOver()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetToday(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setToday(J)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 602
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    goto :goto_0

    :cond_0
    const/16 v0, 0x1e

    :goto_0
    sput v0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCacheThreshold:I

    return-void
.end method

.method public constructor <init>(Lcom/android/server/wm/OneTrackRotationHelper;)V
    .locals 3
    .param p1, "helper"    # Lcom/android/server/wm/OneTrackRotationHelper;

    .line 616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 592
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    .line 593
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    .line 595
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I

    .line 596
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mStartTime:J

    .line 597
    iput-wide v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J

    .line 604
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->appUsageDataCache:Ljava/util/HashMap;

    .line 606
    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->initialized:Z

    .line 611
    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isShutDown:Z

    .line 614
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->filpUsageDate:Ljava/util/HashMap;

    .line 617
    iput-object p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    .line 618
    return-void
.end method

.method private collectData()J
    .locals 8

    .line 1010
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCurPackageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "OneTrackRotationHelper"

    const-wide/16 v2, 0x0

    if-eqz v0, :cond_1

    .line 1011
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1012
    const-string v0, "collectData empty package"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    :cond_0
    return-wide v2

    .line 1017
    :cond_1
    iget-wide v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J

    iget-wide v6, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mStartTime:J

    sub-long/2addr v4, v6

    .line 1018
    .local v4, "duration":J
    const-wide/16 v6, 0x3e8

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    .line 1019
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1020
    const-string v0, "collectData drop this data"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    :cond_2
    return-wide v2

    .line 1025
    :cond_3
    div-long v0, v4, v6

    return-wide v0
.end method

.method private onTodayIsOver()V
    .locals 3

    .line 856
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 857
    const-string v0, "OneTrackRotationHelper"

    const-string v1, "onTodayIsOver"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z

    .line 861
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    if-eqz v0, :cond_1

    .line 862
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 864
    .local v0, "now":J
    iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J

    .line 865
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 866
    const-string/jumbo v2, "today-is-over-sending"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V

    .line 867
    .end local v0    # "now":J
    goto :goto_0

    .line 868
    :cond_1
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 870
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z

    .line 871
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$mprepareFinalSendingInToday(Lcom/android/server/wm/OneTrackRotationHelper;)V

    .line 872
    return-void
.end method

.method private prepareAllData()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1034
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1035
    .local v0, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->appUsageDataCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1036
    .local v2, "packageName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->appUsageDataCache:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[I

    .line 1037
    .local v3, "detail":[[I
    if-nez v3, :cond_0

    .line 1038
    goto :goto_0

    .line 1042
    :cond_0
    const/4 v4, 0x0

    :try_start_0
    aget-object v5, v3, v4

    aget v5, v5, v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string/jumbo v6, "screen_type"

    const-string/jumbo v7, "start_number"

    const-string v8, "landscape"

    const-string v9, "portrait"

    const-string v10, "package_name"

    const/4 v11, 0x2

    const/4 v12, 0x1

    if-nez v5, :cond_1

    :try_start_1
    aget-object v5, v3, v4

    aget v5, v5, v12

    if-nez v5, :cond_1

    aget-object v5, v3, v4

    aget v5, v5, v11

    if-eqz v5, :cond_4

    .line 1043
    :cond_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 1044
    .local v5, "jsonData":Lorg/json/JSONObject;
    invoke-virtual {v5, v10, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1045
    aget-object v13, v3, v4

    aget v13, v13, v4

    invoke-virtual {v5, v9, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1046
    aget-object v13, v3, v4

    aget v13, v13, v12

    invoke-virtual {v5, v8, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1047
    aget-object v13, v3, v4

    aget v13, v13, v11

    invoke-virtual {v5, v7, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1048
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FOLD()Z

    move-result v13

    if-nez v13, :cond_2

    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FLIP()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1049
    :cond_2
    const-string/jumbo v13, "\u5185\u5c4f"

    invoke-virtual {v5, v6, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1051
    :cond_3
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1054
    .end local v5    # "jsonData":Lorg/json/JSONObject;
    :cond_4
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FOLD()Z

    move-result v5
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    const-string/jumbo v13, "\u5916\u5c4f"

    if-eqz v5, :cond_6

    :try_start_2
    aget-object v5, v3, v12

    aget v5, v5, v4

    if-nez v5, :cond_5

    aget-object v5, v3, v12

    aget v5, v5, v12

    if-nez v5, :cond_5

    aget-object v5, v3, v12

    aget v5, v5, v11

    if-eqz v5, :cond_6

    .line 1055
    :cond_5
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 1056
    .restart local v5    # "jsonData":Lorg/json/JSONObject;
    invoke-virtual {v5, v10, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1057
    aget-object v14, v3, v12

    aget v14, v14, v4

    invoke-virtual {v5, v9, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1058
    aget-object v14, v3, v12

    aget v14, v14, v12

    invoke-virtual {v5, v8, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1059
    aget-object v14, v3, v12

    aget v14, v14, v11

    invoke-virtual {v5, v7, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1060
    invoke-virtual {v5, v6, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1061
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1064
    .end local v5    # "jsonData":Lorg/json/JSONObject;
    :cond_6
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FLIP()Z

    move-result v5

    if-eqz v5, :cond_8

    aget-object v5, v3, v12

    aget v5, v5, v4

    if-nez v5, :cond_7

    aget-object v5, v3, v12

    aget v5, v5, v12

    if-nez v5, :cond_7

    aget-object v5, v3, v12

    aget v5, v5, v11

    if-eqz v5, :cond_8

    .line 1065
    :cond_7
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 1066
    .restart local v5    # "jsonData":Lorg/json/JSONObject;
    invoke-virtual {v5, v10, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1067
    aget-object v10, v3, v12

    aget v4, v10, v4

    invoke-virtual {v5, v9, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1068
    aget-object v4, v3, v12

    aget v4, v4, v12

    invoke-virtual {v5, v8, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1069
    aget-object v4, v3, v12

    aget v4, v4, v11

    invoke-virtual {v5, v7, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1070
    invoke-virtual {v5, v6, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1071
    const-string v4, "screen_direction_0"

    aget-object v6, v3, v12

    const/4 v7, 0x3

    aget v6, v6, v7

    invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1072
    const-string v4, "screen_direction_90"

    aget-object v6, v3, v12

    const/4 v7, 0x4

    aget v6, v6, v7

    invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1073
    const-string v4, "screen_direction_180"

    aget-object v6, v3, v12

    const/4 v7, 0x5

    aget v6, v6, v7

    invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1074
    const-string v4, "screen_direction_270"

    aget-object v6, v3, v12

    const/4 v7, 0x6

    aget v6, v6, v7

    invoke-virtual {v5, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1075
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1079
    .end local v5    # "jsonData":Lorg/json/JSONObject;
    :cond_8
    goto :goto_1

    .line 1077
    :catch_0
    move-exception v4

    .line 1078
    .local v4, "e":Lorg/json/JSONException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "prepareAllData e = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "OneTrackRotationHelper"

    invoke-static {v6, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1080
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "detail":[[I
    .end local v4    # "e":Lorg/json/JSONException;
    :goto_1
    goto/16 :goto_0

    .line 1081
    :cond_9
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_a

    .line 1082
    const/4 v1, 0x0

    return-object v1

    .line 1084
    :cond_a
    return-object v0
.end method

.method private reportAppUsageData()V
    .locals 15

    .line 923
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    const-string v1, "OneTrackRotationHelper"

    if-eqz v0, :cond_4

    .line 924
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->collectData()J

    move-result-wide v2

    .line 925
    .local v2, "duration":J
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCurPackageName:Ljava/lang/String;

    .line 926
    .local v0, "packageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->appUsageDataCache:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[I

    .line 927
    .local v4, "appData":[[I
    const/4 v5, 0x3

    const/4 v6, 0x2

    if-nez v4, :cond_1

    .line 928
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FLIP()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 933
    const/4 v7, 0x7

    filled-new-array {v6, v7}, [I

    move-result-object v7

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v8, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v7

    move-object v4, v7

    check-cast v4, [[I

    goto :goto_0

    .line 939
    :cond_0
    filled-new-array {v6, v5}, [I

    move-result-object v7

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v8, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v7

    move-object v4, v7

    check-cast v4, [[I

    .line 941
    :goto_0
    iget-object v7, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->appUsageDataCache:Ljava/util/HashMap;

    invoke-virtual {v7, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 944
    :cond_1
    iget-boolean v7, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    .line 947
    .local v7, "foldIndex":I
    iget v8, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I

    const/4 v9, 0x1

    const/4 v10, 0x0

    packed-switch v8, :pswitch_data_0

    goto :goto_1

    .line 954
    :pswitch_0
    aget-object v8, v4, v7

    aget v11, v8, v9

    int-to-long v11, v11

    add-long/2addr v11, v2

    long-to-int v11, v11

    aput v11, v8, v9

    goto :goto_1

    .line 950
    :pswitch_1
    aget-object v8, v4, v7

    aget v11, v8, v10

    int-to-long v11, v11

    add-long/2addr v11, v2

    long-to-int v11, v11

    aput v11, v8, v10

    .line 951
    nop

    .line 960
    :goto_1
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FLIP()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-boolean v8, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    if-eqz v8, :cond_2

    .line 961
    aget-object v8, v4, v7

    aget v11, v8, v5

    iget-object v12, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    aget v13, v12, v10

    add-int/2addr v11, v13

    aput v11, v8, v5

    .line 962
    aget-object v8, v4, v7

    const/4 v11, 0x4

    aget v13, v8, v11

    aget v14, v12, v9

    add-int/2addr v13, v14

    aput v13, v8, v11

    .line 963
    aget-object v8, v4, v7

    const/4 v11, 0x5

    aget v13, v8, v11

    aget v14, v12, v6

    add-int/2addr v13, v14

    aput v13, v8, v11

    .line 964
    aget-object v8, v4, v7

    const/4 v11, 0x6

    aget v13, v8, v11

    aget v12, v12, v5

    add-int/2addr v13, v12

    aput v13, v8, v11

    .line 968
    :cond_2
    aget-object v8, v4, v7

    aget v11, v8, v6

    iget v12, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I

    add-int/2addr v11, v12

    aput v11, v8, v6

    .line 970
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 971
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "generate new data =  packageName = "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, " rotation = "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v11, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, " duration = "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, " "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 974
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FLIP()Z

    move-result v11

    if-eqz v11, :cond_3

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "direction[0][90][180][270] = ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    aget v10, v12, v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v12, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    aget v9, v12, v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    aget v6, v10, v6

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v9, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    aget v5, v9, v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_3
    const-string v5, ""

    :goto_2
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " cache size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->appUsageDataCache:Ljava/util/HashMap;

    .line 975
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 971
    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 979
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v2    # "duration":J
    .end local v4    # "appData":[[I
    .end local v7    # "foldIndex":I
    :cond_4
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->appUsageDataCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    .line 980
    .local v0, "cacheSize":I
    iget-boolean v2, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isShutDown:Z

    if-nez v2, :cond_5

    sget v2, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCacheThreshold:I

    if-ge v0, v2, :cond_5

    iget-boolean v2, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z

    if-eqz v2, :cond_7

    .line 981
    :cond_5
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 982
    const-string v2, "reportMergedAppUsageData sendAll"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 985
    :cond_6
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->sendAllAppUsageData()V

    .line 986
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v1}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$mprepareNextSending(Lcom/android/server/wm/OneTrackRotationHelper;)V

    .line 988
    :cond_7
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private reportRotationChange(I)V
    .locals 4
    .param p1, "rotation"    # I

    .line 991
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    if-eqz v0, :cond_0

    .line 992
    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1003
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    const/4 v2, 0x3

    aget v3, v1, v2

    add-int/2addr v3, v0

    aput v3, v1, v2

    goto :goto_0

    .line 1000
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    const/4 v2, 0x2

    aget v3, v1, v2

    add-int/2addr v3, v0

    aput v3, v1, v2

    .line 1001
    goto :goto_0

    .line 997
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    aget v2, v1, v0

    add-int/2addr v2, v0

    aput v2, v1, v0

    .line 998
    goto :goto_0

    .line 994
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    const/4 v2, 0x0

    aget v3, v1, v2

    add-int/2addr v3, v0

    aput v3, v1, v2

    .line 995
    nop

    .line 1007
    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private resetDirectionCount()V
    .locals 2

    .line 895
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 896
    return-void
.end method

.method private resetLaunchCount()V
    .locals 1

    .line 891
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I

    .line 892
    return-void
.end method

.method private resetStartTime(JLjava/lang/String;)V
    .locals 4
    .param p1, "time"    # J
    .param p3, "reason"    # Ljava/lang/String;

    .line 875
    iput-wide p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mStartTime:J

    .line 876
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 878
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 879
    .local v0, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 881
    .local v1, "sTime":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start recording new data  pkgname="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCurPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rotation = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " folded = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " startTime = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reason = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OneTrackRotationHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    .end local v0    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    .end local v1    # "sTime":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private sendAllAppUsageData()V
    .locals 6

    .line 1091
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->appUsageDataCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1092
    return-void

    .line 1096
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyyMMdd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1097
    .local v0, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    iget-wide v2, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->dateOfToday:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1099
    .local v1, "dateTime":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1101
    .local v2, "intDateTime":I
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->prepareAllData()Ljava/util/ArrayList;

    move-result-object v3

    .line 1103
    .local v3, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1104
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "sendAllAppUsageData data = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "OneTrackRotationHelper"

    invoke-static {v5, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    :cond_1
    if-eqz v3, :cond_2

    .line 1108
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v4, v3, v2}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$mreportOneTrack(Lcom/android/server/wm/OneTrackRotationHelper;Ljava/util/ArrayList;I)V

    .line 1111
    :cond_2
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->appUsageDataCache:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 1112
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetLaunchCount()V

    .line 1113
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetDirectionCount()V

    .line 1114
    return-void
.end method

.method private setEndTime(J)V
    .locals 0
    .param p1, "time"    # J

    .line 899
    iput-wide p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J

    .line 900
    return-void
.end method

.method private setToday(J)V
    .locals 0
    .param p1, "time"    # J

    .line 919
    iput-wide p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->dateOfToday:J

    .line 920
    return-void
.end method

.method private updateFolded(Z)V
    .locals 0
    .param p1, "folded"    # Z

    .line 915
    iput-boolean p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    .line 916
    return-void
.end method

.method private updateNewPackage(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .line 903
    iput-object p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCurPackageName:Ljava/lang/String;

    .line 904
    return-void
.end method

.method private updateRotation(I)V
    .locals 0
    .param p1, "rotation"    # I

    .line 911
    iput p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I

    .line 912
    return-void
.end method

.method private updateScreenState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .line 907
    iput-boolean p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    .line 908
    return-void
.end method


# virtual methods
.method public init(Ljava/lang/String;ZZI)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "screenState"    # Z
    .param p3, "folded"    # Z
    .param p4, "rotation"    # I

    .line 621
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->initialized:Z

    if-eqz v0, :cond_0

    .line 622
    return-void

    .line 625
    :cond_0
    iput-object p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCurPackageName:Ljava/lang/String;

    .line 626
    iput-boolean p2, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    .line 627
    iput-boolean p3, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    .line 628
    iput p4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I

    .line 629
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mStartTime:J

    .line 630
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I

    .line 631
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    .line 632
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->initialized:Z

    .line 634
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->filpUsageDate:Ljava/util/HashMap;

    const-string v2, "screen_direction_up"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 635
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->filpUsageDate:Ljava/util/HashMap;

    const-string v2, "screen_direction_left"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->filpUsageDate:Ljava/util/HashMap;

    const-string v2, "screen_direction_down"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 637
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->filpUsageDate:Ljava/util/HashMap;

    const-string v2, "screen_direction_right"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 639
    const-string v1, "debug.onetrack.log"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "android"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "OneTrackRotationHelper"

    if-eqz v1, :cond_1

    .line 640
    const-string v1, "init DEBUG = true"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfputDEBUG(Z)V

    .line 642
    const-wide/32 v0, 0x1d4c0

    invoke-static {v0, v1}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfputmReportInterval(J)V

    .line 643
    const/16 v0, 0xf

    sput v0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCacheThreshold:I

    .line 646
    :cond_1
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 647
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init RotationStateMachine  package = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCurPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " screenState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rotation = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " launchCount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " direction = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    .line 652
    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " folded = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 647
    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$mprepareNextSending(Lcom/android/server/wm/OneTrackRotationHelper;)V

    .line 657
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$mprepareFinalSendingInToday(Lcom/android/server/wm/OneTrackRotationHelper;)V

    .line 658
    return-void
.end method

.method public onDeviceFoldChanged(Landroid/os/Message;)V
    .locals 7
    .param p1, "foldMessage"    # Landroid/os/Message;

    .line 789
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;

    .line 790
    .local v0, "aumObj":Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;
    iget-object v1, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 791
    .local v1, "folded":Z
    iget-wide v2, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J

    .line 792
    .local v2, "newStartTime":J
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 793
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onDeviceFoldChanged "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "OneTrackRotationHelper"

    invoke-static {v5, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    :cond_0
    iget-boolean v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    if-eq v4, v1, :cond_3

    .line 797
    invoke-direct {p0, v2, v3}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setEndTime(J)V

    .line 798
    iget-boolean v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    if-eqz v4, :cond_1

    .line 799
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 801
    :cond_1
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v4}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$mreportOneTrackForScreenData(Lcom/android/server/wm/OneTrackRotationHelper;)V

    .line 802
    invoke-direct {p0, v1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateFolded(Z)V

    .line 803
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    iget-object v4, v4, Lcom/android/server/wm/OneTrackRotationHelper;->mPm:Landroid/os/PowerManager;

    invoke-virtual {v4}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 804
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fputcurrentTimeMillis(Lcom/android/server/wm/OneTrackRotationHelper;J)V

    .line 806
    :cond_2
    const-string v4, "fold-changed"

    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V

    .line 808
    :cond_3
    return-void
.end method

.method public onForegroundAppChanged(Landroid/os/Message;)V
    .locals 8
    .param p1, "foregroundMessage"    # Landroid/os/Message;

    .line 666
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;

    .line 667
    .local v0, "aumObj":Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;
    iget-object v1, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 669
    .local v1, "newPackageName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 670
    return-void

    .line 673
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mCurPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 674
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 675
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onForegroundAppChanged  pkgname="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OneTrackRotationHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    :cond_1
    iget-wide v2, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J

    .line 678
    .local v2, "newStartTime":J
    invoke-direct {p0, v2, v3}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setEndTime(J)V

    .line 679
    iget v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I

    const/4 v5, 0x1

    add-int/2addr v4, v5

    iput v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mLaunchCount:I

    .line 682
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FLIP()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    if-eqz v4, :cond_2

    .line 683
    iget v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 694
    :pswitch_0
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    const/4 v6, 0x3

    aget v7, v4, v6

    add-int/2addr v7, v5

    aput v7, v4, v6

    goto :goto_0

    .line 691
    :pswitch_1
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    const/4 v6, 0x2

    aget v7, v4, v6

    add-int/2addr v7, v5

    aput v7, v4, v6

    .line 692
    goto :goto_0

    .line 688
    :pswitch_2
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    aget v6, v4, v5

    add-int/2addr v6, v5

    aput v6, v4, v5

    .line 689
    goto :goto_0

    .line 685
    :pswitch_3
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDirection:[I

    const/4 v6, 0x0

    aget v7, v4, v6

    add-int/2addr v7, v5

    aput v7, v4, v6

    .line 686
    nop

    .line 699
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 700
    invoke-direct {p0, v1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateNewPackage(Ljava/lang/String;)V

    .line 701
    const-string v4, "new-package"

    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V

    .line 702
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetLaunchCount()V

    .line 703
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetDirectionCount()V

    .line 705
    .end local v2    # "newStartTime":J
    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onNextSending()V
    .locals 3

    .line 815
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816
    const-string v0, "OneTrackRotationHelper"

    const-string v1, "onNextSending"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z

    .line 820
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    if-eqz v0, :cond_1

    .line 821
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 823
    .local v0, "now":J
    iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J

    .line 824
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 825
    const-string v2, "next-sending"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V

    .line 826
    .end local v0    # "now":J
    goto :goto_0

    .line 827
    :cond_1
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 829
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isNextSending:Z

    .line 830
    return-void
.end method

.method public onRotationChanged(Landroid/os/Message;)V
    .locals 9
    .param p1, "rotationMessage"    # Landroid/os/Message;

    .line 754
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;

    .line 755
    .local v0, "aumObj":Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 756
    .local v1, "displayId":I
    iget v2, p1, Landroid/os/Message;->arg2:I

    .line 757
    .local v2, "rotation":I
    iget-wide v3, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J

    .line 759
    .local v3, "newStartTime":J
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 760
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onRotationChanged "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "OneTrackRotationHelper"

    invoke-static {v6, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    :cond_0
    if-eqz v1, :cond_1

    .line 764
    return-void

    .line 767
    :cond_1
    iget v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mDisplayRotation:I

    if-eq v2, v5, :cond_6

    .line 769
    invoke-direct {p0, v3, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setEndTime(J)V

    .line 770
    iget-boolean v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    if-eqz v5, :cond_2

    .line 771
    invoke-direct {p0, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportRotationChange(I)V

    .line 772
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 774
    :cond_2
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FLIP()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-boolean v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    if-eqz v5, :cond_4

    .line 775
    iget-object v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v5}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetcurrentTimeMillis(Lcom/android/server/wm/OneTrackRotationHelper;)J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_3

    .line 776
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-object v7, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v7}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetcurrentTimeMillis(Lcom/android/server/wm/OneTrackRotationHelper;)J

    move-result-wide v7

    sub-long/2addr v5, v7

    long-to-float v5, v5

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v5, v6

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    .line 777
    .local v5, "duration":F
    :goto_0
    iget-object v6, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v6, v5}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$mrecordTimeForFlip(Lcom/android/server/wm/OneTrackRotationHelper;F)V

    .line 779
    .end local v5    # "duration":F
    :cond_4
    invoke-direct {p0, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateRotation(I)V

    .line 780
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetIS_FLIP()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-boolean v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mFolded:Z

    if-eqz v5, :cond_5

    .line 781
    iget-object v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fputcurrentTimeMillis(Lcom/android/server/wm/OneTrackRotationHelper;J)V

    .line 783
    :cond_5
    const-string v5, "rotation-changed"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V

    .line 784
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetDirectionCount()V

    .line 786
    :cond_6
    return-void
.end method

.method public onScreenStateChanged(Landroid/os/Message;)V
    .locals 8
    .param p1, "screenStateMessage"    # Landroid/os/Message;

    .line 715
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;

    .line 716
    .local v0, "aumObj":Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;
    iget-object v1, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    .line 717
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_0

    .line 718
    return-void

    .line 721
    :cond_0
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 722
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onScreenStateChanged "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OneTrackRotationHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 726
    .local v2, "action":Ljava/lang/String;
    iget-wide v3, v0, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;->time:J

    .line 727
    .local v3, "newStartTime":J
    const-string v5, "android.intent.action.USER_PRESENT"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 728
    iget-boolean v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    if-nez v5, :cond_2

    .line 729
    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateScreenState(Z)V

    .line 730
    const-string v5, "screen-on"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V

    .line 732
    :cond_2
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetmIsScreenOnNotification()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 733
    iget-object v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fputcurrentTimeMillis(Lcom/android/server/wm/OneTrackRotationHelper;J)V

    .line 734
    const/4 v5, 0x0

    invoke-static {v5}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfputmIsScreenOnNotification(Z)V

    goto :goto_0

    .line 736
    :cond_3
    const-string v5, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 737
    invoke-direct {p0, v3, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->setEndTime(J)V

    .line 738
    iget-object v5, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->oneTrackRotationHelper:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-virtual {v5}, Lcom/android/server/wm/OneTrackRotationHelper;->isScreenRealUnlocked()Z

    move-result v5

    .line 739
    .local v5, "realScreenState":Z
    if-nez v5, :cond_4

    iget-boolean v6, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    if-eqz v6, :cond_4

    .line 740
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 742
    :cond_4
    invoke-direct {p0, v5}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->updateScreenState(Z)V

    .line 743
    if-eqz v5, :cond_5

    .line 744
    const-string v6, "real-screen-on"

    invoke-direct {p0, v3, v4, v6}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V

    .line 747
    .end local v5    # "realScreenState":Z
    :cond_5
    :goto_0
    return-void
.end method

.method public onShutDown()V
    .locals 3

    .line 836
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 837
    const-string v0, "OneTrackRotationHelper"

    const-string v1, "onShutDown"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->isShutDown:Z

    .line 841
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mScreenState:Z

    if-eqz v0, :cond_1

    .line 842
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 844
    .local v0, "now":J
    iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->mEndTime:J

    .line 845
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 846
    const-string/jumbo v2, "shutdown"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->resetStartTime(JLjava/lang/String;)V

    .line 847
    .end local v0    # "now":J
    goto :goto_0

    .line 848
    :cond_1
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->reportAppUsageData()V

    .line 850
    :goto_0
    return-void
.end method
