class com.android.server.wm.MiuiSizeCompatService$2 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "MiuiSizeCompatService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiSizeCompatService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiSizeCompatService this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiSizeCompatService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiSizeCompatService; */
/* .line 517 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 4 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 520 */
v0 = this.this$0;
com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmCurFullAct ( v0 );
final String v1 = "exit foregroud"; // const-string v1, "exit foregroud"
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.this$0;
com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmCurFullAct ( v0 );
v0 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 v0 = 	 com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmNotificationShowing ( v0 );
	 /* if-nez v0, :cond_1 */
	 /* .line 521 */
} // :cond_0
v0 = this.this$0;
(( com.android.server.wm.MiuiSizeCompatService ) v0 ).cancelSizeCompatNotification ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->cancelSizeCompatNotification(Ljava/lang/String;)V
/* .line 523 */
} // :cond_1
if ( p1 != null) { // if-eqz p1, :cond_3
v0 = this.this$0;
com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmCurFullAct ( v0 );
v0 = this.packageName;
v2 = this.mForegroundPackageName;
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
/* .line 524 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 525 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Last is "; // const-string v2, "Last is "
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.this$0;
	 com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmCurFullAct ( v2 );
	 v2 = this.packageName;
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v2 = " and current is "; // const-string v2, " and current is "
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.mForegroundPackageName;
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "MiuiSizeCompatService"; // const-string v2, "MiuiSizeCompatService"
	 android.util.Slog .d ( v2,v0 );
	 /* .line 527 */
} // :cond_2
v0 = this.this$0;
com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmAtms ( v0 );
v0 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v0 ).getTopResumedActivity ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getTopResumedActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 528 */
/* .local v0, "top":Lcom/android/server/wm/ActivityRecord; */
if ( v0 != null) { // if-eqz v0, :cond_3
	 v2 = this.this$0;
	 com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetPERMISSION_ACTIVITY ( v2 );
	 v3 = this.shortComponentName;
	 v2 = 	 (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v2, :cond_3 */
	 /* .line 529 */
	 v2 = this.this$0;
	 (( com.android.server.wm.MiuiSizeCompatService ) v2 ).cancelSizeCompatNotification ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->cancelSizeCompatNotification(Ljava/lang/String;)V
	 /* .line 532 */
} // .end local v0 # "top":Lcom/android/server/wm/ActivityRecord;
} // :cond_3
return;
} // .end method
