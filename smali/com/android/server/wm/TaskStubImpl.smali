.class public Lcom/android/server/wm/TaskStubImpl;
.super Ljava/lang/Object;
.source "TaskStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/TaskStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TaskStubImpl"

.field private static final mNeedFinishAct:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final mVtCameraNeedFinishAct:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isHierarchyBottom:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/TaskStubImpl;->mNeedFinishAct:Ljava/util/List;

    .line 43
    const-string v1, "com.miui.securitycenter/com.miui.permcenter.permissions.SystemAppPermissionDialogActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    const-string v1, "com.xiaomi.account/.ui.SystemAccountAuthDialogActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/TaskStubImpl;->mVtCameraNeedFinishAct:Ljava/util/List;

    .line 50
    const-string v1, "com.milink.service/com.xiaomi.vtcamera.activities.CameraServerActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/TaskStubImpl;->isHierarchyBottom:Z

    return-void
.end method

.method static synthetic lambda$clearSizeCompatInSplitScreen$1(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p0, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 275
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->hasSizeCompatBounds()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->getCompatDisplayInsets()Lcom/android/server/wm/ActivityRecord$CompatDisplayInsets;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 276
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->clearSizeCompatMode()V

    .line 278
    :cond_1
    return-void
.end method

.method static synthetic lambda$isSplitScreenModeDismissed$0(Lcom/android/server/wm/Task;Lcom/android/server/wm/Task;)Z
    .locals 1
    .param p0, "root"    # Lcom/android/server/wm/Task;
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 103
    if-eq p1, p0, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->hasChild()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public addInvisiblePipTaskToTransition(ZLcom/android/server/wm/WindowContainer;I)Z
    .locals 2
    .param p1, "inVisibleState"    # Z
    .param p2, "wc"    # Lcom/android/server/wm/WindowContainer;
    .param p3, "windowingMode"    # I

    .line 387
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/android/server/wm/WindowContainer;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/android/server/wm/WindowContainer;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->isSleeping()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 391
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->isOrganized()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 392
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I

    move-result v0

    if-eq p3, v0, :cond_1

    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->inPinnedWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Add invisible pip task to transition, taskId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 394
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v1

    iget v1, v1, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 393
    const-string v1, "TaskStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    const/4 v0, 0x1

    return v0

    .line 398
    :cond_1
    return v1

    .line 388
    :cond_2
    :goto_0
    return v1
.end method

.method public checkFreeFormActivityRecordIfNeeded(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/wm/Transition$ChangeInfo;",
            ">;)V"
        }
    .end annotation

    .line 404
    .local p1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Transition$ChangeInfo;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 405
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/Transition$ChangeInfo;

    .line 406
    .local v1, "targetChange":Lcom/android/server/wm/Transition$ChangeInfo;
    iget-object v2, v1, Lcom/android/server/wm/Transition$ChangeInfo;->mContainer:Lcom/android/server/wm/WindowContainer;

    .line 407
    .local v2, "target":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->inFreeformWindowingMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 408
    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/TaskStubImpl;->shouldSkipFreeFormActivityRecord(Ljava/util/ArrayList;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 409
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 410
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " skipFreeFormActivityRecordIfNeeded: target= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "TaskStubImpl"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    .end local v1    # "targetChange":Lcom/android/server/wm/Transition$ChangeInfo;
    .end local v2    # "target":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 413
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public clearRootProcess(Lcom/android/server/wm/WindowProcessController;Lcom/android/server/wm/Task;)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/wm/WindowProcessController;
    .param p2, "task"    # Lcom/android/server/wm/Task;

    .line 227
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/android/server/wm/WindowProcessController;->isRemoved()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getNonFinishingActivityCount()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 230
    :cond_0
    iget-object v0, p2, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    const-string v1, "com.xiaomi.vipaccount"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 231
    :cond_1
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 232
    .local v0, "top":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eq v1, p1, :cond_2

    sget-object v1, Lcom/android/server/wm/TaskStubImpl;->mNeedFinishAct:Ljava/util/List;

    iget-object v2, v0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 233
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only left "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in task "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and finish it."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TaskStubImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    const-string v1, "clearNonAppAct"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/ActivityRecord;->finishIfPossible(Ljava/lang/String;Z)I

    .line 236
    :cond_2
    return-void

    .line 228
    .end local v0    # "top":Lcom/android/server/wm/ActivityRecord;
    :cond_3
    :goto_0
    return-void
.end method

.method public clearSizeCompatInSplitScreen(IILcom/android/server/wm/WindowContainer;)V
    .locals 3
    .param p1, "prevWinMode"    # I
    .param p2, "newWinMode"    # I
    .param p3, "wc"    # Lcom/android/server/wm/WindowContainer;

    .line 268
    sget-boolean v0, Landroid/os/Build;->IS_MIUI:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_1

    .line 269
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 270
    .local v0, "task":Lcom/android/server/wm/Task;
    :goto_0
    const/4 v1, 0x6

    if-ne p2, v1, :cond_1

    if-eqz v0, :cond_1

    .line 271
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 272
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 273
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 274
    new-instance v1, Lcom/android/server/wm/TaskStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v1}, Lcom/android/server/wm/TaskStubImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/Task;->forAllActivities(Ljava/util/function/Consumer;)V

    .line 281
    .end local v0    # "task":Lcom/android/server/wm/Task;
    :cond_1
    return-void
.end method

.method public clearVirtualCamera(Lcom/android/server/wm/WindowProcessController;Lcom/android/server/wm/Task;)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/wm/WindowProcessController;
    .param p2, "task"    # Lcom/android/server/wm/Task;

    .line 239
    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_0

    .line 241
    :cond_0
    iget-object v0, p2, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    const-string v1, "com.xiaomi.vtcamera"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 242
    :cond_1
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 243
    .local v0, "top":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eq v1, p1, :cond_2

    sget-object v1, Lcom/android/server/wm/TaskStubImpl;->mVtCameraNeedFinishAct:Ljava/util/List;

    iget-object v2, v0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 244
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only left "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in task "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and finish it."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TaskStubImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    const-string v1, "clearVtCameraAct"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/ActivityRecord;->finishIfPossible(Ljava/lang/String;Z)I

    .line 247
    :cond_2
    return-void

    .line 239
    .end local v0    # "top":Lcom/android/server/wm/ActivityRecord;
    :cond_3
    :goto_0
    return-void
.end method

.method public disallowEnterPip(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "toFrontActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 377
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.tencent.mm/.plugin.base.stub.WXEntryActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Skip pip enter request for transient activity : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TaskStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const/4 v0, 0x1

    return v0

    .line 382
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getActivities(Ljava/util/function/Predicate;ZLcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/WindowList;)Ljava/util/List;
    .locals 5
    .param p2, "traverseTopToBottom"    # Z
    .param p3, "boundary"    # Lcom/android/server/wm/ActivityRecord;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/function/Predicate<",
            "Lcom/android/server/wm/ActivityRecord;",
            ">;Z",
            "Lcom/android/server/wm/ActivityRecord;",
            "Lcom/android/server/wm/WindowList<",
            "+",
            "Lcom/android/server/wm/WindowContainer;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/server/wm/ActivityRecord;",
            ">;"
        }
    .end annotation

    .line 181
    .local p1, "callback":Ljava/util/function/Predicate;, "Ljava/util/function/Predicate<Lcom/android/server/wm/ActivityRecord;>;"
    .local p4, "children":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<+Lcom/android/server/wm/WindowContainer;>;"
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 182
    .local v0, "activityRecordList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    if-eqz p2, :cond_3

    .line 183
    invoke-virtual {p4}, Lcom/android/server/wm/WindowList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 184
    invoke-virtual {p4, v1}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/WindowContainer;

    .line 186
    .local v2, "wc":Lcom/android/server/wm/WindowContainer;
    if-ne v2, p3, :cond_0

    .line 187
    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    goto :goto_1

    .line 190
    :cond_0
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/wm/WindowContainer;->getActivity(Ljava/util/function/Predicate;ZLcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 191
    .local v3, "r":Lcom/android/server/wm/ActivityRecord;
    if-eqz v3, :cond_1

    .line 192
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    .end local v2    # "wc":Lcom/android/server/wm/WindowContainer;
    .end local v3    # "r":Lcom/android/server/wm/ActivityRecord;
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .end local v1    # "i":I
    :cond_2
    :goto_1
    goto :goto_3

    .line 196
    :cond_3
    invoke-virtual {p4}, Lcom/android/server/wm/WindowList;->size()I

    move-result v1

    .line 197
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v1, :cond_6

    .line 198
    invoke-virtual {p4, v2}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/WindowContainer;

    .line 200
    .local v3, "wc":Lcom/android/server/wm/WindowContainer;
    if-ne v3, p3, :cond_4

    .line 201
    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    goto :goto_3

    .line 204
    :cond_4
    invoke-virtual {v3, p1, p2, p3}, Lcom/android/server/wm/WindowContainer;->getActivity(Ljava/util/function/Predicate;ZLcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    .line 205
    .local v4, "r":Lcom/android/server/wm/ActivityRecord;
    if-eqz v4, :cond_5

    .line 206
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    .end local v3    # "wc":Lcom/android/server/wm/WindowContainer;
    .end local v4    # "r":Lcom/android/server/wm/ActivityRecord;
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 210
    .end local v1    # "count":I
    .end local v2    # "i":I
    :cond_6
    :goto_3
    return-object v0
.end method

.method public getLayoutInDisplayCutoutMode(Landroid/view/WindowManager$LayoutParams;)I
    .locals 1
    .param p1, "attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 215
    if-nez p1, :cond_0

    .line 216
    const/4 v0, 0x0

    return v0

    .line 222
    :cond_0
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    return v0
.end method

.method public inSplitScreenWindowingMode(Lcom/android/server/wm/Task;)Z
    .locals 6
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 251
    iget-object v0, p1, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 252
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isRootTask()Z

    move-result v1

    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_1

    .line 253
    iget-boolean v1, p1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 254
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->hasChild()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 255
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I

    move-result v1

    if-ne v1, v2, :cond_0

    move v3, v4

    goto :goto_0

    :cond_0
    nop

    :goto_0
    monitor-exit v0

    .line 253
    return v3

    .line 257
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 258
    .local v1, "rootTask":Lcom/android/server/wm/Task;
    if-eqz v1, :cond_2

    iget-boolean v5, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->hasChild()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 259
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v5

    if-ne v5, v4, :cond_2

    .line 260
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 261
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I

    move-result v5

    if-ne v5, v2, :cond_2

    move v3, v4

    goto :goto_1

    :cond_2
    nop

    :goto_1
    monitor-exit v0

    .line 258
    return v3

    .line 263
    .end local v1    # "rootTask":Lcom/android/server/wm/Task;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isHierarchyBottom()Z
    .locals 1

    .line 137
    iget-boolean v0, p0, Lcom/android/server/wm/TaskStubImpl;->isHierarchyBottom:Z

    return v0
.end method

.method public isSplitScreenModeDismissed(Lcom/android/server/wm/Task;)Z
    .locals 1
    .param p1, "root"    # Lcom/android/server/wm/Task;

    .line 103
    if-eqz p1, :cond_0

    new-instance v0, Lcom/android/server/wm/TaskStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0, p1}, Lcom/android/server/wm/TaskStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/Task;)V

    invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->getTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public notifyActivityPipModeChangedForHoverMode(ZLcom/android/server/wm/ActivityRecord;)V
    .locals 2
    .param p1, "inPip"    # Z
    .param p2, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 294
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 295
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 296
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 297
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onActivityPipModeChangedForHoverMode(ZLcom/android/server/wm/ActivityRecord;)V

    .line 299
    :cond_0
    return-void
.end method

.method public notifyFreeformModeFocus(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .line 170
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyFreeformModeFocus(Ljava/lang/String;I)V

    .line 171
    return-void
.end method

.method public notifyMovetoFront(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "inFreeformSmallWinMode"    # Z

    .line 166
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyMovetoFront(IZ)V

    .line 167
    return-void
.end method

.method public notifyMultitaskLaunch(ILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 174
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyMultitaskLaunch(ILjava/lang/String;)V

    .line 175
    return-void
.end method

.method public onHoverModeTaskParentChanged(Lcom/android/server/wm/Task;Lcom/android/server/wm/WindowContainer;)V
    .locals 2
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "newParent"    # Lcom/android/server/wm/WindowContainer;

    .line 302
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 303
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 304
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 305
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onHoverModeTaskParentChanged(Lcom/android/server/wm/Task;Lcom/android/server/wm/WindowContainer;)V

    .line 307
    :cond_0
    return-void
.end method

.method public onHoverModeTaskPrepareSurfaces(Lcom/android/server/wm/Task;)V
    .locals 2
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 286
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 287
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 288
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->onHoverModeTaskPrepareSurfaces(Lcom/android/server/wm/Task;)V

    .line 291
    :cond_0
    return-void
.end method

.method public onSplitScreenParentChanged(Lcom/android/server/wm/WindowContainer;Lcom/android/server/wm/WindowContainer;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/wm/WindowContainer<",
            "*>;",
            "Lcom/android/server/wm/WindowContainer<",
            "*>;)V"
        }
    .end annotation

    .line 108
    .local p1, "oldParent":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    .local p2, "newParent":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 109
    .local v0, "start":J
    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 110
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->inSplitScreenWindowingMode()Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v3

    goto :goto_0

    :cond_0
    move v4, v2

    .line 111
    .local v4, "oldInSpliScreenMode":Z
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 112
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I

    move-result v5

    if-ne v5, v3, :cond_1

    move v2, v3

    goto :goto_1

    :cond_1
    nop

    .line 113
    .local v2, "newInFullScreenMode":Z
    :goto_1
    if-eqz v4, :cond_5

    if-eqz v2, :cond_5

    .line 114
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/android/server/wm/TaskStubImpl;->isSplitScreenModeDismissed(Lcom/android/server/wm/Task;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 115
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;

    move-result-object v3

    .line 116
    .local v3, "task":Lcom/android/server/wm/Task;
    if-nez v3, :cond_2

    .line 117
    return-void

    .line 119
    :cond_2
    invoke-virtual {v3}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v5

    .line 120
    .local v5, "topActivity":Lcom/android/server/wm/ActivityRecord;
    if-nez v5, :cond_3

    .line 121
    return-void

    .line 123
    :cond_3
    invoke-static {}, Landroid/appcompat/ApplicationCompatUtilsStub;->get()Landroid/appcompat/ApplicationCompatUtilsStub;

    move-result-object v6

    invoke-virtual {v6}, Landroid/appcompat/ApplicationCompatUtilsStub;->isContinuityEnabled()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 124
    invoke-static {}, Lcom/android/server/wm/AppContinuityRouterStub;->get()Lcom/android/server/wm/AppContinuityRouterStub;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/android/server/wm/AppContinuityRouterStub;->onSplitToFullScreenChanged(Lcom/android/server/wm/ActivityRecord;)V

    .line 126
    :cond_4
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V

    .line 128
    invoke-static {}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->getInstance()Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->onSplitScreenExit()V

    .line 130
    .end local v3    # "task":Lcom/android/server/wm/Task;
    .end local v5    # "topActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v0

    .line 131
    .local v5, "took":J
    const-wide/16 v7, 0x32

    cmp-long v3, v5, v7

    if-lez v3, :cond_6

    .line 132
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onSplitScreenParentChanged took "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "ms"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "TaskStubImpl"

    invoke-static {v7, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_6
    return-void
.end method

.method public onTaskConfigurationChanged(II)V
    .locals 2
    .param p1, "prevWindowingMode"    # I
    .param p2, "overrideWindowingMode"    # I

    .line 310
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 311
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 312
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onTaskConfigurationChanged(II)V

    .line 315
    :cond_0
    return-void
.end method

.method public setHierarchy(Z)V
    .locals 0
    .param p1, "bottom"    # Z

    .line 141
    iput-boolean p1, p0, Lcom/android/server/wm/TaskStubImpl;->isHierarchyBottom:Z

    .line 142
    return-void
.end method

.method public shouldSkipFreeFormActivityRecord(Ljava/util/ArrayList;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 7
    .param p2, "freeformActivityRecord"    # Lcom/android/server/wm/ActivityRecord;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/wm/Transition$ChangeInfo;",
            ">;",
            "Lcom/android/server/wm/ActivityRecord;",
            ")Z"
        }
    .end annotation

    .line 417
    .local p1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Transition$ChangeInfo;>;"
    const/4 v0, 0x0

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 420
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_3

    .line 421
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/Transition$ChangeInfo;

    .line 422
    .local v3, "targetChange":Lcom/android/server/wm/Transition$ChangeInfo;
    iget-object v4, v3, Lcom/android/server/wm/Transition$ChangeInfo;->mContainer:Lcom/android/server/wm/WindowContainer;

    .line 423
    .local v4, "target":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    invoke-virtual {v4, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asWallpaperToken()Lcom/android/server/wm/WallpaperWindowToken;

    move-result-object v5

    if-nez v5, :cond_2

    .line 424
    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 425
    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v5

    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 426
    goto :goto_1

    .line 428
    :cond_1
    return v2

    .line 420
    .end local v3    # "targetChange":Lcom/android/server/wm/Transition$ChangeInfo;
    .end local v4    # "target":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 430
    .end local v1    # "i":I
    :cond_3
    return v0

    .line 418
    :cond_4
    :goto_2
    return v0
.end method

.method public skipPinnedTaskIfNeeded(Lcom/android/server/wm/WindowContainer;Lcom/android/server/wm/Transition$ChangeInfo;Landroid/util/ArraySet;)Z
    .locals 5
    .param p1, "container"    # Lcom/android/server/wm/WindowContainer;
    .param p2, "changeInfo"    # Lcom/android/server/wm/Transition$ChangeInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/wm/WindowContainer;",
            "Lcom/android/server/wm/Transition$ChangeInfo;",
            "Landroid/util/ArraySet<",
            "Lcom/android/server/wm/WindowContainer;",
            ">;)Z"
        }
    .end annotation

    .line 353
    .local p3, "participants":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowContainer;>;"
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->inPinnedWindowingMode()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->isVisibleRequested()Z

    move-result v0

    if-nez v0, :cond_7

    iget-boolean v0, p2, Lcom/android/server/wm/Transition$ChangeInfo;->mVisible:Z

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/android/server/wm/WindowContainer;->mTransitionController:Lcom/android/server/wm/TransitionController;

    .line 355
    invoke-virtual {v0}, Lcom/android/server/wm/TransitionController;->getCollectingTransitionType()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    goto :goto_3

    .line 359
    :cond_0
    const/4 v0, 0x0

    .line 360
    .local v0, "activitySwitchInFreeform":Z
    invoke-virtual {p3}, Landroid/util/ArraySet;->size()I

    move-result v3

    sub-int/2addr v3, v2

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_5

    .line 361
    invoke-virtual {p3, v3}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/WindowContainer;

    .line 362
    .local v2, "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_1

    .line 363
    :cond_1
    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->inPinnedWindowingMode()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->isVisibleRequested()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_1

    .line 364
    :cond_2
    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->inFreeformWindowingMode()Z

    move-result v4

    if-nez v4, :cond_3

    goto :goto_2

    .line 367
    :cond_3
    const/4 v0, 0x1

    .line 360
    .end local v2    # "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    :goto_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 365
    .restart local v2    # "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    :cond_4
    :goto_2
    return v1

    .line 370
    .end local v2    # "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    .end local v3    # "i":I
    :cond_5
    if-eqz v0, :cond_6

    .line 371
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "skip pip task "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for activity switch in freeform task"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TaskStubImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_6
    return v0

    .line 356
    .end local v0    # "activitySwitchInFreeform":Z
    :cond_7
    :goto_3
    return v1
.end method

.method public skipSplitScreenTaskIfNeeded(Lcom/android/server/wm/WindowContainer;Landroid/util/ArraySet;)Z
    .locals 8
    .param p1, "container"    # Lcom/android/server/wm/WindowContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/wm/WindowContainer;",
            "Landroid/util/ArraySet<",
            "Lcom/android/server/wm/WindowContainer;",
            ">;)Z"
        }
    .end annotation

    .line 320
    .local p2, "participants":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowContainer;>;"
    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->inSplitScreenWindowingMode()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_5

    .line 322
    :cond_0
    const/4 v0, 0x0

    .line 323
    .local v0, "taskChildInParticipants":Z
    iget-object v2, p1, Lcom/android/server/wm/WindowContainer;->mChildren:Lcom/android/server/wm/WindowList;

    invoke-virtual {v2}, Lcom/android/server/wm/WindowList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_2

    .line 324
    invoke-virtual {p1, v2}, Lcom/android/server/wm/WindowContainer;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v3

    .line 325
    .local v3, "child":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {p2, v3}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 326
    const/4 v0, 0x1

    .line 327
    goto :goto_1

    .line 323
    .end local v3    # "child":Lcom/android/server/wm/WindowContainer;
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 330
    .end local v2    # "i":I
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    return v1

    .line 332
    :cond_3
    const/4 v2, 0x0

    .line 333
    .local v2, "otherTaskChildInParticipants":Z
    invoke-virtual {p2}, Landroid/util/ArraySet;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .local v3, "i":I
    :goto_2
    if-ltz v3, :cond_7

    .line 334
    invoke-virtual {p2, v3}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/wm/WindowContainer;

    .line 335
    .local v4, "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    goto :goto_4

    .line 336
    :cond_4
    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->inSplitScreenWindowingMode()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 337
    iget-object v5, v4, Lcom/android/server/wm/WindowContainer;->mChildren:Lcom/android/server/wm/WindowList;

    invoke-virtual {v5}, Lcom/android/server/wm/WindowList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .local v5, "j":I
    :goto_3
    if-ltz v5, :cond_6

    .line 338
    iget-object v6, v4, Lcom/android/server/wm/WindowContainer;->mChildren:Lcom/android/server/wm/WindowList;

    invoke-virtual {v6, v5}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wm/WindowContainer;

    .line 339
    .local v6, "child":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {p2, v6}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 340
    const/4 v2, 0x1

    .line 341
    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    return v1

    .line 337
    .end local v6    # "child":Lcom/android/server/wm/WindowContainer;
    :cond_5
    add-int/lit8 v5, v5, -0x1

    goto :goto_3

    .line 333
    .end local v4    # "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
    .end local v5    # "j":I
    :cond_6
    :goto_4
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 346
    .end local v3    # "i":I
    :cond_7
    return v2

    .line 320
    .end local v0    # "taskChildInParticipants":Z
    .end local v2    # "otherTaskChildInParticipants":Z
    :cond_8
    :goto_5
    return v1
.end method

.method public updateForegroundActivityInAppPair(Lcom/android/server/wm/Task;Z)V
    .locals 4
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "isInAppPair"    # Z

    .line 145
    if-nez p2, :cond_3

    .line 146
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v0

    .line 147
    .local v0, "displayArea":Lcom/android/server/wm/TaskDisplayArea;
    if-nez v0, :cond_0

    return-void

    .line 148
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;

    move-result-object v1

    .line 149
    .local v1, "topStack":Lcom/android/server/wm/Task;
    if-nez v1, :cond_1

    return-void

    .line 150
    :cond_1
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 151
    .local v2, "topActivity":Lcom/android/server/wm/ActivityRecord;
    if-nez v2, :cond_2

    return-void

    .line 152
    :cond_2
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v3

    .line 153
    invoke-virtual {v3, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V

    .line 154
    .end local v0    # "displayArea":Lcom/android/server/wm/TaskDisplayArea;
    .end local v1    # "topStack":Lcom/android/server/wm/Task;
    .end local v2    # "topActivity":Lcom/android/server/wm/ActivityRecord;
    goto :goto_0

    .line 155
    :cond_3
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 156
    .local v0, "topActivity":Lcom/android/server/wm/ActivityRecord;
    if-nez v0, :cond_4

    .line 157
    return-void

    .line 159
    :cond_4
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v1

    .line 160
    invoke-virtual {v1, v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V

    .line 162
    .end local v0    # "topActivity":Lcom/android/server/wm/ActivityRecord;
    :goto_0
    return-void
.end method
