public class com.android.server.wm.WindowProcessUtils {
	 /* .source "WindowProcessUtils.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static com.android.server.wm.ActivityTaskManagerService sAtmsInstance;
	 /* # direct methods */
	 public static Boolean $r8$lambda$9MUgBEw-OrJMC63pLXH4-UeMT5A ( com.android.server.wm.Task p0 ) { //synthethic
		 /* .locals 0 */
		 p0 = 		 com.android.server.wm.WindowProcessUtils .isMultiWindowStackLocked ( p0 );
	 } // .end method
	 static com.android.server.wm.WindowProcessUtils ( ) {
		 /* .locals 1 */
		 /* .line 31 */
		 /* const-class v0, Lcom/android/server/wm/WindowProcessUtils; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 /* .line 33 */
		 int v0 = 0; // const/4 v0, 0x0
		 return;
	 } // .end method
	 public com.android.server.wm.WindowProcessUtils ( ) {
		 /* .locals 0 */
		 /* .line 30 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static java.lang.String getActivityComponentName ( com.android.server.wm.ActivityRecord p0 ) {
		 /* .locals 1 */
		 /* .param p0, "activity" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 450 */
		 v0 = this.shortComponentName;
	 } // .end method
	 static synchronized com.android.server.wm.ActivityTaskManagerService getActivityTaskManagerService ( ) {
		 /* .locals 2 */
		 /* const-class v0, Lcom/android/server/wm/WindowProcessUtils; */
		 /* monitor-enter v0 */
		 /* .line 36 */
		 try { // :try_start_0
			 v1 = com.android.server.wm.WindowProcessUtils.sAtmsInstance;
			 /* if-nez v1, :cond_0 */
			 /* .line 37 */
			 final String v1 = "activity_task"; // const-string v1, "activity_task"
			 android.os.ServiceManager .getService ( v1 );
			 /* check-cast v1, Lcom/android/server/wm/ActivityTaskManagerService; */
			 /* .line 40 */
		 } // :cond_0
		 v1 = com.android.server.wm.WindowProcessUtils.sAtmsInstance;
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* monitor-exit v0 */
		 /* .line 35 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* throw v1 */
	 } // .end method
	 public static java.util.List getAllTaskIdList ( com.android.server.wm.ActivityTaskManagerService p0 ) {
		 /* .locals 6 */
		 /* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Lcom/android/server/wm/ActivityTaskManagerService;", */
		 /* ")", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/Integer;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 113 */
	 v0 = this.mGlobalLock;
	 /* monitor-enter v0 */
	 /* .line 114 */
	 try { // :try_start_0
		 (( com.android.server.wm.ActivityTaskManagerService ) p0 ).getRecentTasks ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;
		 (( com.android.server.wm.RecentTasks ) v1 ).getRawTasks ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;
		 /* .line 115 */
		 /* .local v1, "allRecentTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;" */
		 /* new-instance v2, Ljava/util/ArrayList; */
		 /* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
		 /* .line 116 */
		 /* .local v2, "allTaskIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
		 (( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
	 v4 = 	 } // :goto_0
	 if ( v4 != null) { // if-eqz v4, :cond_0
		 /* check-cast v4, Lcom/android/server/wm/Task; */
		 /* .line 117 */
		 /* .local v4, "task":Lcom/android/server/wm/Task; */
		 /* iget v5, v4, Lcom/android/server/wm/Task;->mTaskId:I */
		 java.lang.Integer .valueOf ( v5 );
		 (( java.util.ArrayList ) v2 ).add ( v5 ); // invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
		 /* .line 118 */
		 /* nop */
	 } // .end local v4 # "task":Lcom/android/server/wm/Task;
	 /* .line 119 */
} // :cond_0
/* monitor-exit v0 */
/* .line 120 */
} // .end local v1 # "allRecentTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;"
} // .end local v2 # "allTaskIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static miui.security.CallerInfo getCallerInfo ( com.android.server.wm.ActivityTaskManagerService p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p1, "callingPid" # I */
/* .param p2, "callingUid" # I */
/* .line 491 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 492 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p0 ).getProcessController ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(II)Lcom/android/server/wm/WindowProcessController;
/* .line 493 */
/* .local v1, "wpc":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.mInfo;
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 494 */
	 /* new-instance v2, Lmiui/security/CallerInfo; */
	 /* invoke-direct {v2}, Lmiui/security/CallerInfo;-><init>()V */
	 /* .line 495 */
	 /* .local v2, "info":Lmiui/security/CallerInfo; */
	 v3 = this.mInfo;
	 v3 = this.packageName;
	 this.callerPkg = v3;
	 /* .line 496 */
	 v3 = this.mInfo;
	 /* iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
	 /* iput v3, v2, Lmiui/security/CallerInfo;->callerUid:I */
	 /* .line 497 */
	 v3 = 	 (( com.android.server.wm.WindowProcessController ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
	 /* iput v3, v2, Lmiui/security/CallerInfo;->callerPid:I */
	 /* .line 498 */
	 v3 = this.mName;
	 this.callerProcessName = v3;
	 /* .line 499 */
	 /* monitor-exit v0 */
	 /* .line 501 */
} // .end local v2 # "info":Lmiui/security/CallerInfo;
} // :cond_0
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 502 */
} // .end local v1 # "wpc":Lcom/android/server/wm/WindowProcessController;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static miui.security.CallerInfo getCallerInfo ( com.android.server.wm.ActivityTaskManagerService p0, android.app.IApplicationThread p1 ) {
/* .locals 4 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p1, "caller" # Landroid/app/IApplicationThread; */
/* .line 476 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 477 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p0 ).getProcessController ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Landroid/app/IApplicationThread;)Lcom/android/server/wm/WindowProcessController;
/* .line 478 */
/* .local v1, "wpc":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.mInfo;
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 479 */
	 /* new-instance v2, Lmiui/security/CallerInfo; */
	 /* invoke-direct {v2}, Lmiui/security/CallerInfo;-><init>()V */
	 /* .line 480 */
	 /* .local v2, "info":Lmiui/security/CallerInfo; */
	 v3 = this.mInfo;
	 v3 = this.packageName;
	 this.callerPkg = v3;
	 /* .line 481 */
	 v3 = this.mInfo;
	 /* iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
	 /* iput v3, v2, Lmiui/security/CallerInfo;->callerUid:I */
	 /* .line 482 */
	 v3 = 	 (( com.android.server.wm.WindowProcessController ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
	 /* iput v3, v2, Lmiui/security/CallerInfo;->callerPid:I */
	 /* .line 483 */
	 v3 = this.mName;
	 this.callerProcessName = v3;
	 /* .line 484 */
	 /* monitor-exit v0 */
	 /* .line 486 */
} // .end local v2 # "info":Lmiui/security/CallerInfo;
} // :cond_0
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 487 */
} // .end local v1 # "wpc":Lcom/android/server/wm/WindowProcessController;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static java.lang.String getCallerPackageName ( com.android.server.wm.ActivityTaskManagerService p0, android.app.IApplicationThread p1 ) {
/* .locals 3 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p1, "caller" # Landroid/app/IApplicationThread; */
/* .line 454 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 455 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p0 ).getProcessController ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Landroid/app/IApplicationThread;)Lcom/android/server/wm/WindowProcessController;
/* .line 456 */
/* .local v1, "wpc":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = this.mInfo;
/* if-nez v2, :cond_0 */
/* .line 459 */
} // :cond_0
v2 = this.mInfo;
v2 = this.packageName;
/* monitor-exit v0 */
/* .line 457 */
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 461 */
} // .end local v1 # "wpc":Lcom/android/server/wm/WindowProcessController;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static Integer getCallerUid ( com.android.server.wm.ActivityTaskManagerService p0, android.app.IApplicationThread p1 ) {
/* .locals 3 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p1, "caller" # Landroid/app/IApplicationThread; */
/* .line 465 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 466 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p0 ).getProcessController ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Landroid/app/IApplicationThread;)Lcom/android/server/wm/WindowProcessController;
/* .line 467 */
/* .local v1, "wpc":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = this.mInfo;
/* if-nez v2, :cond_0 */
/* .line 470 */
} // :cond_0
v2 = this.mInfo;
/* iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I */
/* monitor-exit v0 */
/* .line 468 */
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
int v0 = -1; // const/4 v0, -0x1
/* .line 472 */
} // .end local v1 # "wpc":Lcom/android/server/wm/WindowProcessController;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static android.content.pm.ApplicationInfo getMultiWindowForegroundAppInfoLocked ( com.android.server.wm.ActivityTaskManagerService p0 ) {
/* .locals 5 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 371 */
int v0 = 0; // const/4 v0, 0x0
/* .line 372 */
/* .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 373 */
try { // :try_start_0
v2 = this.mTaskSupervisor;
v2 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v2 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 374 */
/* .local v2, "multiWindowStack":Lcom/android/server/wm/Task; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 375 */
(( com.android.server.wm.Task ) v2 ).topRunningActivityLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 376 */
/* .local v3, "multiWindowActivity":Lcom/android/server/wm/ActivityRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 377 */
v4 = this.info;
v4 = this.applicationInfo;
/* move-object v0, v4 */
/* .line 380 */
} // .end local v2 # "multiWindowStack":Lcom/android/server/wm/Task;
} // .end local v3 # "multiWindowActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_0
/* monitor-exit v1 */
/* .line 381 */
/* .line 380 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private static com.android.server.wm.Task getMultiWindowStackLocked ( com.android.server.wm.ActivityTaskManagerService p0 ) {
/* .locals 4 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 124 */
v0 = this.mTaskSupervisor;
v0 = this.mRootWindowContainer;
v0 = (( com.android.server.wm.RootWindowContainer ) v0 ).getChildCount ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getChildCount()I
/* .line 125 */
/* .local v0, "numDisplays":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "displayNdx":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 126 */
v2 = this.mTaskSupervisor;
v2 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v2 ).getChildAt ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/RootWindowContainer;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
/* check-cast v2, Lcom/android/server/wm/DisplayContent; */
/* .line 127 */
/* .local v2, "display":Lcom/android/server/wm/DisplayContent; */
/* new-instance v3, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda2; */
/* invoke-direct {v3}, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda2;-><init>()V */
(( com.android.server.wm.DisplayContent ) v2 ).getRootTask ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/DisplayContent;->getRootTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;
/* .line 128 */
/* .local v3, "result":Lcom/android/server/wm/Task; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 129 */
/* .line 125 */
} // .end local v2 # "display":Lcom/android/server/wm/DisplayContent;
} // .end local v3 # "result":Lcom/android/server/wm/Task;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 132 */
} // .end local v1 # "displayNdx":I
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public static java.util.Map getPerceptibleRecentAppList ( com.android.server.wm.ActivityTaskManagerService p0 ) {
/* .locals 7 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/wm/ActivityTaskManagerService;", */
/* ")", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 74 */
int v0 = 0; // const/4 v0, 0x0
/* .line 76 */
/* .local v0, "taskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;" */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* move-object v0, v1 */
/* .line 79 */
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 80 */
try { // :try_start_0
com.android.server.wm.WindowProcessUtils .getMultiWindowStackLocked ( p0 );
/* .line 81 */
/* .local v2, "dockedStack":Lcom/android/server/wm/Task; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 82 */
(( com.android.server.wm.Task ) v2 ).topRunningActivityLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 83 */
/* .local v3, "r":Lcom/android/server/wm/ActivityRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
(( com.android.server.wm.ActivityRecord ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 84 */
(( com.android.server.wm.ActivityRecord ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* iget v4, v4, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v4 );
v5 = this.packageName;
/* .line 89 */
} // .end local v3 # "r":Lcom/android/server/wm/ActivityRecord;
} // :cond_0
(( com.android.server.wm.ActivityTaskManagerService ) p0 ).getRecentTasks ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;
(( com.android.server.wm.RecentTasks ) v3 ).getRawTasks ( ); // invoke-virtual {v3}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :cond_1
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Lcom/android/server/wm/Task; */
/* .line 90 */
/* .local v4, "task":Lcom/android/server/wm/Task; */
if ( v4 != null) { // if-eqz v4, :cond_1
(( com.android.server.wm.Task ) v4 ).getRootTask ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
/* if-nez v5, :cond_2 */
/* .line 91 */
/* .line 94 */
} // :cond_2
com.android.server.wm.WindowProcessUtils .getTaskPackageNameLocked ( v4 );
/* .line 95 */
/* .local v5, "taskPackageName":Ljava/lang/String; */
v6 = com.android.server.wm.WindowProcessUtils .isTaskInMultiWindowStackLocked ( v4 );
/* if-nez v6, :cond_1 */
/* .line 96 */
v6 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v6, :cond_1 */
final String v6 = "com.android.systemui"; // const-string v6, "com.android.systemui"
/* .line 97 */
v6 = android.text.TextUtils .equals ( v5,v6 );
/* if-nez v6, :cond_1 */
/* .line 98 */
v6 = (( com.android.server.wm.Task ) v4 ).inFreeformWindowingMode ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
/* if-nez v6, :cond_1 */
/* .line 99 */
v6 = (( com.android.server.wm.Task ) v4 ).isVisible ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->isVisible()Z
/* if-nez v6, :cond_3 */
/* .line 100 */
/* .line 103 */
} // :cond_3
/* iget v3, v4, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v3 );
/* .line 104 */
/* nop */
/* .line 106 */
} // .end local v2 # "dockedStack":Lcom/android/server/wm/Task;
} // .end local v4 # "task":Lcom/android/server/wm/Task;
} // .end local v5 # "taskPackageName":Ljava/lang/String;
} // :cond_4
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 107 */
v1 = com.android.server.wm.WindowProcessUtils.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getPerceptibleRecentAppList: "; // const-string v3, "getPerceptibleRecentAppList: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 109 */
/* .line 106 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public static java.util.ArrayList getTaskIntentForToken ( android.os.IBinder p0 ) {
/* .locals 6 */
/* .param p0, "token" # Landroid/os/IBinder; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/IBinder;", */
/* ")", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/content/Intent;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 416 */
com.android.server.wm.WindowProcessUtils .getActivityTaskManagerService ( );
/* .line 417 */
/* .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService; */
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 418 */
try { // :try_start_0
com.android.server.wm.ActivityRecord .isInRootTaskLocked ( p0 );
/* .line 419 */
/* .local v2, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 420 */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 421 */
/* .local v3, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
(( com.android.server.wm.ActivityRecord ) v2 ).getTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* new-instance v5, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda1; */
/* invoke-direct {v5, v3}, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda1;-><init>(Ljava/util/ArrayList;)V */
(( com.android.server.wm.Task ) v4 ).forAllActivities ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/Task;->forAllActivities(Ljava/util/function/Consumer;)V
/* .line 424 */
/* monitor-exit v1 */
/* .line 426 */
} // .end local v2 # "activityRecord":Lcom/android/server/wm/ActivityRecord;
} // .end local v3 # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
} // :cond_0
/* monitor-exit v1 */
/* .line 427 */
int v1 = 0; // const/4 v1, 0x0
/* .line 426 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private static java.lang.String getTaskPackageNameLocked ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p0, "task" # Lcom/android/server/wm/Task; */
/* .line 153 */
int v0 = 0; // const/4 v0, 0x0
/* .line 154 */
/* .local v0, "packageName":Ljava/lang/String; */
if ( p0 != null) { // if-eqz p0, :cond_0
(( com.android.server.wm.Task ) p0 ).getBaseIntent ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 155 */
(( com.android.server.wm.Task ) p0 ).getBaseIntent ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 156 */
(( com.android.server.wm.Task ) p0 ).getBaseIntent ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 158 */
} // :cond_0
} // .end method
private static com.android.server.wm.WindowProcessController getTaskRootOrTopAppLocked ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p0, "task" # Lcom/android/server/wm/Task; */
/* .line 170 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 172 */
} // :cond_0
(( com.android.server.wm.Task ) p0 ).getRootActivity ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 173 */
/* .local v1, "record":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v1, :cond_1 */
/* .line 174 */
(( com.android.server.wm.Task ) p0 ).topRunningActivityLocked ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 177 */
} // :cond_1
/* if-nez v1, :cond_2 */
/* .line 178 */
/* .line 180 */
} // :cond_2
v0 = this.app;
} // .end method
public static com.android.server.wm.WindowProcessController getTaskTopApp ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "taskId" # I */
/* .line 162 */
com.android.server.wm.WindowProcessUtils .getActivityTaskManagerService ( );
/* .line 163 */
/* .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService; */
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 164 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getRecentTasks ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;
(( com.android.server.wm.RecentTasks ) v2 ).getTask ( p0 ); // invoke-virtual {v2, p0}, Lcom/android/server/wm/RecentTasks;->getTask(I)Lcom/android/server/wm/Task;
/* .line 165 */
/* .local v2, "task":Lcom/android/server/wm/Task; */
com.android.server.wm.WindowProcessUtils .getTaskTopAppLocked ( v2 );
/* monitor-exit v1 */
/* .line 166 */
} // .end local v2 # "task":Lcom/android/server/wm/Task;
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private static com.android.server.wm.WindowProcessController getTaskTopAppLocked ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p0, "task" # Lcom/android/server/wm/Task; */
/* .line 184 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 185 */
(( com.android.server.wm.Task ) p0 ).topRunningActivityLocked ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 186 */
/* .local v0, "topActivity":Lcom/android/server/wm/ActivityRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 187 */
v1 = this.app;
/* .line 190 */
} // .end local v0 # "topActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static java.lang.String getTaskTopAppProcessNameLocked ( com.android.server.wm.Task p0 ) {
/* .locals 3 */
/* .param p0, "task" # Lcom/android/server/wm/Task; */
/* .line 206 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 207 */
/* .line 209 */
} // :cond_0
(( com.android.server.wm.Task ) p0 ).topRunningActivityLocked ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 210 */
/* .local v1, "topActivity":Lcom/android/server/wm/ActivityRecord; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = this.info;
v2 = this.applicationInfo;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 211 */
v0 = this.processName;
/* .line 213 */
} // :cond_1
} // .end method
private static Integer getTaskTopAppUidLocked ( com.android.server.wm.Task p0 ) {
/* .locals 3 */
/* .param p0, "task" # Lcom/android/server/wm/Task; */
/* .line 194 */
int v0 = -1; // const/4 v0, -0x1
/* .line 195 */
/* .local v0, "uid":I */
/* if-nez p0, :cond_0 */
/* .line 196 */
/* .line 198 */
} // :cond_0
(( com.android.server.wm.Task ) p0 ).topRunningActivityLocked ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 199 */
/* .local v1, "topActivity":Lcom/android/server/wm/ActivityRecord; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = this.info;
v2 = this.applicationInfo;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 200 */
v2 = this.info;
v2 = this.applicationInfo;
/* iget v0, v2, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 202 */
} // :cond_1
} // .end method
public static java.util.HashMap getTopRunningActivityInfo ( ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 431 */
com.android.server.wm.WindowProcessUtils .getActivityTaskManagerService ( );
/* .line 432 */
/* .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService; */
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 433 */
try { // :try_start_0
v2 = this.mTaskSupervisor;
v2 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v2 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 434 */
/* .local v2, "activityStack":Lcom/android/server/wm/Task; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 435 */
(( com.android.server.wm.Task ) v2 ).topRunningActivityLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 436 */
/* .local v3, "r":Lcom/android/server/wm/ActivityRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 437 */
/* new-instance v4, Ljava/util/HashMap; */
/* invoke-direct {v4}, Ljava/util/HashMap;-><init>()V */
/* .line 438 */
/* .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;" */
final String v5 = "packageName"; // const-string v5, "packageName"
v6 = this.packageName;
(( java.util.HashMap ) v4 ).put ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 439 */
/* const-string/jumbo v5, "token" */
v6 = this.token;
(( java.util.HashMap ) v4 ).put ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 440 */
/* const-string/jumbo v5, "userId" */
/* iget v6, v3, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
java.lang.Integer .valueOf ( v6 );
(( java.util.HashMap ) v4 ).put ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 441 */
final String v5 = "intent"; // const-string v5, "intent"
v6 = this.intent;
(( java.util.HashMap ) v4 ).put ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 442 */
/* monitor-exit v1 */
/* .line 445 */
} // .end local v2 # "activityStack":Lcom/android/server/wm/Task;
} // .end local v3 # "r":Lcom/android/server/wm/ActivityRecord;
} // .end local v4 # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
} // :cond_0
/* monitor-exit v1 */
/* .line 446 */
int v1 = 0; // const/4 v1, 0x0
/* .line 445 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public static Integer getTopRunningPidLocked ( ) {
/* .locals 5 */
/* .line 388 */
com.android.server.wm.WindowProcessUtils .getActivityTaskManagerService ( );
/* .line 389 */
/* .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService; */
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 390 */
try { // :try_start_0
v2 = this.mTaskSupervisor;
v2 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v2 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 391 */
/* .local v2, "topStack":Lcom/android/server/wm/Task; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 392 */
(( com.android.server.wm.Task ) v2 ).topRunningActivityLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 393 */
/* .local v3, "record":Lcom/android/server/wm/ActivityRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
v4 = this.app;
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 394 */
v4 = this.app;
v4 = (( com.android.server.wm.WindowProcessController ) v4 ).getPid ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* monitor-exit v1 */
/* .line 397 */
} // .end local v2 # "topStack":Lcom/android/server/wm/Task;
} // .end local v3 # "record":Lcom/android/server/wm/ActivityRecord;
} // :cond_0
/* monitor-exit v1 */
/* .line 398 */
int v1 = 0; // const/4 v1, 0x0
/* .line 397 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public static com.android.server.wm.WindowProcessController getTopRunningProcessController ( ) {
/* .locals 5 */
/* .line 402 */
com.android.server.wm.WindowProcessUtils .getActivityTaskManagerService ( );
/* .line 403 */
/* .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService; */
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 404 */
try { // :try_start_0
v2 = this.mTaskSupervisor;
v2 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v2 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 405 */
/* .local v2, "topStack":Lcom/android/server/wm/Task; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 406 */
(( com.android.server.wm.Task ) v2 ).topRunningActivityLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 407 */
/* .local v3, "record":Lcom/android/server/wm/ActivityRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
v4 = this.app;
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 408 */
v4 = this.app;
/* monitor-exit v1 */
/* .line 411 */
} // .end local v2 # "topStack":Lcom/android/server/wm/Task;
} // .end local v3 # "record":Lcom/android/server/wm/ActivityRecord;
} // :cond_0
/* monitor-exit v1 */
/* .line 412 */
int v1 = 0; // const/4 v1, 0x0
/* .line 411 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private static Boolean isMultiWindowStackLocked ( com.android.server.wm.Task p0 ) {
/* .locals 1 */
/* .param p0, "stack" # Lcom/android/server/wm/Task; */
/* .line 149 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean isPackageRunning ( com.android.server.wm.ActivityTaskManagerService p0, java.lang.String p1, java.lang.String p2, Integer p3 ) {
/* .locals 8 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .line 526 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_5 */
v0 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v0, :cond_5 */
/* if-nez p3, :cond_0 */
/* .line 530 */
} // :cond_0
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 531 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p0 ).getProcessController ( p2, p3 ); // invoke-virtual {p0, p2, p3}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 532 */
/* monitor-exit v0 */
/* .line 535 */
} // :cond_1
v2 = this.mProcessMap;
(( com.android.server.wm.WindowProcessControllerMap ) v2 ).getProcesses ( p3 ); // invoke-virtual {v2, p3}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcesses(I)Landroid/util/ArraySet;
/* .line 536 */
/* .local v2, "uidMap":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;" */
/* if-nez v2, :cond_2 */
/* .line 537 */
/* monitor-exit v0 */
/* .line 539 */
} // :cond_2
(( android.util.ArraySet ) v2 ).iterator ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_4
/* check-cast v5, Lcom/android/server/wm/WindowProcessController; */
/* .line 540 */
/* .local v5, "wpc":Lcom/android/server/wm/WindowProcessController; */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 541 */
(( com.android.server.wm.WindowProcessController ) v5 ).getThread ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 542 */
v6 = (( com.android.server.wm.WindowProcessController ) v5 ).isCrashing ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowProcessController;->isCrashing()Z
/* if-nez v6, :cond_3 */
v6 = (( com.android.server.wm.WindowProcessController ) v5 ).isNotResponding ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowProcessController;->isNotResponding()Z
/* if-nez v6, :cond_3 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ":widgetProvider"; // const-string v7, ":widgetProvider"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v7 = this.mName;
/* .line 545 */
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_3 */
/* .line 546 */
/* monitor-exit v0 */
/* .line 548 */
} // .end local v5 # "wpc":Lcom/android/server/wm/WindowProcessController;
} // :cond_3
/* .line 549 */
} // .end local v2 # "uidMap":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;"
} // :cond_4
/* monitor-exit v0 */
/* .line 550 */
/* .line 549 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 527 */
} // :cond_5
} // :goto_1
} // .end method
public static Boolean isProcessHasActivityInOtherTaskLocked ( com.android.server.wm.WindowProcessController p0, Integer p1 ) {
/* .locals 7 */
/* .param p0, "app" # Lcom/android/server/wm/WindowProcessController; */
/* .param p1, "curTaskId" # I */
/* .line 45 */
com.android.server.wm.WindowProcessUtils .getActivityTaskManagerService ( );
/* .line 46 */
/* .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService; */
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 47 */
try { // :try_start_0
(( com.android.server.wm.WindowProcessController ) p0 ).getActivities ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowProcessController;->getActivities()Ljava/util/List;
/* .line 48 */
/* .local v2, "activities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;" */
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* monitor-exit v1 */
/* .line 49 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
v5 = } // :goto_0
/* if-ge v4, v5, :cond_2 */
/* .line 50 */
/* check-cast v5, Lcom/android/server/wm/ActivityRecord; */
(( com.android.server.wm.ActivityRecord ) v5 ).getTask ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* .line 51 */
/* .local v5, "otherTask":Lcom/android/server/wm/Task; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* iget v6, v5, Lcom/android/server/wm/Task;->mTaskId:I */
/* if-eq p1, v6, :cond_1 */
/* iget-boolean v6, v5, Lcom/android/server/wm/Task;->inRecents:Z */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 53 */
v6 = com.android.server.wm.WindowProcessUtils .isTaskVisibleInRecents ( v5 );
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 54 */
/* monitor-exit v1 */
int v1 = 1; // const/4 v1, 0x1
/* .line 49 */
} // .end local v5 # "otherTask":Lcom/android/server/wm/Task;
} // :cond_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 57 */
} // .end local v2 # "activities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
} // .end local v4 # "i":I
} // :cond_2
/* monitor-exit v1 */
/* .line 58 */
/* .line 57 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public static Boolean isProcessRunning ( com.android.server.wm.ActivityTaskManagerService p0, java.lang.String p1, Integer p2 ) {
/* .locals 3 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 554 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_2 */
/* if-nez p2, :cond_0 */
/* .line 557 */
} // :cond_0
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 558 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p0 ).getProcessController ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;
if ( v2 != null) { // if-eqz v2, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* monitor-exit v0 */
/* .line 559 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 555 */
} // :cond_2
} // :goto_0
} // .end method
public static Boolean isRemoveTaskDisabled ( Integer p0, java.lang.String p1, com.android.server.wm.ActivityTaskManagerService p2 ) {
/* .locals 3 */
/* .param p0, "taskId" # I */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 217 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 218 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p2 ).getRecentTasks ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;
(( com.android.server.wm.RecentTasks ) v1 ).getTask ( p0 ); // invoke-virtual {v1, p0}, Lcom/android/server/wm/RecentTasks;->getTask(I)Lcom/android/server/wm/Task;
/* .line 220 */
/* .local v1, "task":Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 221 */
com.android.server.wm.WindowProcessUtils .getTaskTopAppProcessNameLocked ( v1 );
v2 = android.text.TextUtils .equals ( p1,v2 );
/* monitor-exit v0 */
/* .line 223 */
} // .end local v1 # "task":Lcom/android/server/wm/Task;
} // :cond_0
/* monitor-exit v0 */
/* .line 224 */
int v0 = 0; // const/4 v0, 0x0
/* .line 223 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static Boolean isRunningOnCarDisplay ( com.android.server.wm.ActivityTaskManagerService p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p0, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 506 */
if ( p1 != null) { // if-eqz p1, :cond_1
final String v0 = "com.miui.carlink"; // const-string v0, "com.miui.carlink"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_0 */
final String v0 = "com.baidu.carlife.xiaomi"; // const-string v0, "com.baidu.carlife.xiaomi"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 507 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 509 */
} // :cond_1
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
/* .line 510 */
/* .local v0, "result":Ljava/util/concurrent/atomic/AtomicBoolean; */
v1 = this.mTaskSupervisor;
v1 = this.mRootWindowContainer;
/* new-instance v2, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p1, v0}, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;)V */
(( com.android.server.wm.RootWindowContainer ) v1 ).forAllTasks ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/RootWindowContainer;->forAllTasks(Ljava/util/function/Consumer;)V
/* .line 521 */
v1 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
} // .end method
private static Boolean isTaskInMultiWindowStackLocked ( com.android.server.wm.Task p0 ) {
/* .locals 1 */
/* .param p0, "task" # Lcom/android/server/wm/Task; */
/* .line 136 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 137 */
(( com.android.server.wm.Task ) p0 ).topRunningActivityLocked ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 143 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static Boolean isTaskVisibleInRecents ( com.android.server.wm.Task p0 ) {
/* .locals 4 */
/* .param p0, "task" # Lcom/android/server/wm/Task; */
/* .line 62 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 63 */
/* .line 64 */
} // :cond_0
v1 = this.intent;
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_1 */
/* .line 65 */
/* .line 66 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/android/server/wm/Task;->isAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.intent;
/* .line 67 */
v1 = (( android.content.Intent ) v1 ).getFlags ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I
/* const/high16 v3, 0x800000 */
/* and-int/2addr v1, v3 */
/* if-nez v1, :cond_2 */
/* .line 68 */
/* .line 70 */
} // :cond_2
} // .end method
static void lambda$getTaskIntentForToken$0 ( java.util.ArrayList p0, com.android.server.wm.ActivityRecord p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "arrayList" # Ljava/util/ArrayList; */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 422 */
v0 = this.intent;
(( java.util.ArrayList ) p0 ).add ( v0 ); // invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 423 */
return;
} // .end method
static void lambda$isRunningOnCarDisplay$1 ( java.lang.String p0, java.util.concurrent.atomic.AtomicBoolean p1, com.android.server.wm.Task p2 ) { //synthethic
/* .locals 3 */
/* .param p0, "name" # Ljava/lang/String; */
/* .param p1, "result" # Ljava/util/concurrent/atomic/AtomicBoolean; */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .line 511 */
(( com.android.server.wm.Task ) p2 ).getTopMostActivity ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getTopMostActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 512 */
/* .local v0, "ac":Lcom/android/server/wm/ActivityRecord; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = this.packageName;
v1 = (( java.lang.String ) p0 ).contains ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 513 */
(( com.android.server.wm.Task ) p2 ).getDisplayContent ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v1 ).getDisplayInfo ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;
v1 = this.name;
/* .line 514 */
/* .local v1, "displayName":Ljava/lang/String; */
final String v2 = "com.miui.carlink"; // const-string v2, "com.miui.carlink"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_0 */
final String v2 = "com.miui.car.launcher"; // const-string v2, "com.miui.car.launcher"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_0 */
/* .line 515 */
final String v2 = "com.baidu.carlife.xiaomi"; // const-string v2, "com.baidu.carlife.xiaomi"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 516 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
(( java.util.concurrent.atomic.AtomicBoolean ) p1 ).set ( v2 ); // invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 519 */
} // .end local v1 # "displayName":Ljava/lang/String;
} // :cond_1
return;
} // .end method
public static void removeAllTasks ( com.miui.server.process.ProcessManagerInternal p0, Integer p1, com.android.server.wm.ActivityTaskManagerService p2 ) {
/* .locals 6 */
/* .param p0, "pms" # Lcom/miui/server/process/ProcessManagerInternal; */
/* .param p1, "userId" # I */
/* .param p2, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 228 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 229 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 230 */
/* .local v1, "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;" */
(( com.android.server.wm.ActivityTaskManagerService ) p2 ).getRecentTasks ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;
(( com.android.server.wm.RecentTasks ) v2 ).getRawTasks ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/android/server/wm/Task; */
/* .line 231 */
/* .local v3, "task":Lcom/android/server/wm/Task; */
/* iget v4, v3, Lcom/android/server/wm/Task;->mUserId:I */
/* if-eq v4, p1, :cond_0 */
/* .line 232 */
/* .line 235 */
} // :cond_0
com.android.server.wm.WindowProcessUtils .getTaskRootOrTopAppLocked ( v3 );
/* .line 236 */
/* .local v4, "app":Lcom/android/server/wm/WindowProcessController; */
/* if-nez v4, :cond_1 */
/* .line 237 */
/* .line 238 */
/* .line 241 */
} // :cond_1
v5 = this.mInfo;
v5 = this.packageName;
v5 = (( com.miui.server.process.ProcessManagerInternal ) p0 ).isTrimMemoryEnable ( v5 ); // invoke-virtual {p0, v5}, Lcom/miui/server/process/ProcessManagerInternal;->isTrimMemoryEnable(Ljava/lang/String;)Z
/* if-nez v5, :cond_2 */
/* .line 242 */
/* .line 245 */
} // :cond_2
/* .line 246 */
/* nop */
} // .end local v3 # "task":Lcom/android/server/wm/Task;
} // .end local v4 # "app":Lcom/android/server/wm/WindowProcessController;
/* .line 248 */
} // :cond_3
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Lcom/android/server/wm/Task; */
/* .line 249 */
/* .restart local v3 # "task":Lcom/android/server/wm/Task; */
com.android.server.wm.WindowProcessUtils .removeTaskLocked ( v3,p2 );
/* .line 250 */
} // .end local v3 # "task":Lcom/android/server/wm/Task;
/* .line 251 */
} // .end local v1 # "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
} // :cond_4
/* monitor-exit v0 */
/* .line 252 */
return;
/* .line 251 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static void removeTask ( Integer p0, com.android.server.wm.ActivityTaskManagerService p1 ) {
/* .locals 2 */
/* .param p0, "taskId" # I */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 352 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 353 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p1 ).getRecentTasks ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;
(( com.android.server.wm.RecentTasks ) v1 ).getTask ( p0 ); // invoke-virtual {v1, p0}, Lcom/android/server/wm/RecentTasks;->getTask(I)Lcom/android/server/wm/Task;
/* .line 354 */
/* .local v1, "task":Lcom/android/server/wm/Task; */
com.android.server.wm.WindowProcessUtils .removeTaskLocked ( v1,p1 );
/* .line 355 */
} // .end local v1 # "task":Lcom/android/server/wm/Task;
/* monitor-exit v0 */
/* .line 356 */
return;
/* .line 355 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private static void removeTaskLocked ( com.android.server.wm.Task p0, com.android.server.wm.ActivityTaskManagerService p1 ) {
/* .locals 4 */
/* .param p0, "task" # Lcom/android/server/wm/Task; */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 360 */
/* if-nez p0, :cond_0 */
/* .line 361 */
return;
/* .line 367 */
} // :cond_0
v0 = this.mTaskSupervisor;
int v1 = 1; // const/4 v1, 0x1
final String v2 = "removeTaskLocked"; // const-string v2, "removeTaskLocked"
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.wm.ActivityTaskSupervisor ) v0 ).removeTask ( p0, v3, v1, v2 ); // invoke-virtual {v0, p0, v3, v1, v2}, Lcom/android/server/wm/ActivityTaskSupervisor;->removeTask(Lcom/android/server/wm/Task;ZZLjava/lang/String;)V
/* .line 368 */
return;
} // .end method
public static void removeTasks ( java.util.List p0, java.util.Set p1, com.android.server.am.IProcessPolicy p2, com.android.server.wm.ActivityTaskManagerService p3, java.util.List p4, java.util.List p5 ) {
/* .locals 18 */
/* .param p2, "processPolicy" # Lcom/android/server/am/IProcessPolicy; */
/* .param p3, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Lcom/android/server/am/IProcessPolicy;", */
/* "Lcom/android/server/wm/ActivityTaskManagerService;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 257 */
/* .local p0, "taskIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p1, "whiteTaskSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local p4, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p5, "whiteListTaskId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p2 */
/* move-object/from16 v4, p3 */
/* move-object/from16 v5, p4 */
/* move-object/from16 v6, p5 */
if ( v1 != null) { // if-eqz v1, :cond_12
v0 = /* invoke-interface/range {p0 ..p0}, Ljava/util/List;->isEmpty()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_5 */
/* .line 261 */
} // :cond_0
v7 = this.mGlobalLock;
/* monitor-enter v7 */
/* .line 263 */
try { // :try_start_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 264 */
/* .local v0, "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;" */
int v8 = 2; // const/4 v8, 0x2
/* new-array v9, v8, [Ljava/lang/String; */
final String v10 = "com.miui.home"; // const-string v10, "com.miui.home"
int v11 = 0; // const/4 v11, 0x0
/* aput-object v10, v9, v11 */
final String v10 = "com.mi.android.globallauncher"; // const-string v10, "com.mi.android.globallauncher"
int v12 = 1; // const/4 v12, 0x1
/* aput-object v10, v9, v12 */
/* .line 265 */
/* .local v9, "whiteListPackageNames":[Ljava/lang/String; */
/* invoke-virtual/range {p3 ..p3}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks; */
(( com.android.server.wm.RecentTasks ) v10 ).getRawTasks ( ); // invoke-virtual {v10}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;
(( java.util.ArrayList ) v10 ).iterator ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :cond_1
v13 = } // :goto_0
if ( v13 != null) { // if-eqz v13, :cond_10
/* check-cast v13, Lcom/android/server/wm/Task; */
/* .line 266 */
/* .local v13, "task":Lcom/android/server/wm/Task; */
/* if-nez v13, :cond_2 */
/* .line 267 */
/* .line 269 */
} // :cond_2
com.android.server.wm.WindowProcessUtils .getTaskPackageNameLocked ( v13 );
/* .line 272 */
/* .local v14, "taskPackageName":Ljava/lang/String; */
/* if-nez v6, :cond_3 */
v15 = if ( v5 != null) { // if-eqz v5, :cond_3
if ( v15 != null) { // if-eqz v15, :cond_3
/* .line 273 */
/* .line 276 */
} // :cond_3
v15 = /* iget v15, v13, Lcom/android/server/wm/Task;->mUserId:I */
/* if-nez v15, :cond_f */
if ( v2 != null) { // if-eqz v2, :cond_4
/* iget v15, v13, Lcom/android/server/wm/Task;->mTaskId:I */
/* .line 277 */
v15 = java.lang.Integer .valueOf ( v15 );
/* if-nez v15, :cond_1 */
} // :cond_4
if ( v6 != null) { // if-eqz v6, :cond_5
/* iget v15, v13, Lcom/android/server/wm/Task;->mTaskId:I */
/* .line 278 */
v15 = java.lang.Integer .valueOf ( v15 );
if ( v15 != null) { // if-eqz v15, :cond_5
/* .line 279 */
/* .line 284 */
} // :cond_5
/* iget-boolean v15, v13, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v15 != null) { // if-eqz v15, :cond_8
v15 = (( com.android.server.wm.Task ) v13 ).getChildCount ( ); // invoke-virtual {v13}, Lcom/android/server/wm/Task;->getChildCount()I
/* if-lt v15, v8, :cond_8 */
/* .line 286 */
(( com.android.server.wm.Task ) v13 ).getChildAt ( v11 ); // invoke-virtual {v13, v11}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v15 ).asTask ( ); // invoke-virtual {v15}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
com.android.server.wm.WindowProcessUtils .getTaskPackageNameLocked ( v15 );
/* iget v8, v13, Lcom/android/server/wm/Task;->mUserId:I */
v8 = /* .line 285 */
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 288 */
(( com.android.server.wm.Task ) v13 ).getChildAt ( v12 ); // invoke-virtual {v13, v12}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v8 ).asTask ( ); // invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
com.android.server.wm.WindowProcessUtils .getTaskPackageNameLocked ( v8 );
/* iget v15, v13, Lcom/android/server/wm/Task;->mUserId:I */
v8 = /* .line 287 */
/* if-nez v8, :cond_6 */
} // :cond_6
int v8 = 2; // const/4 v8, 0x2
} // :cond_7
} // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 290 */
(( com.android.server.wm.Task ) v13 ).getChildAt ( v11 ); // invoke-virtual {v13, v11}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v8 ).asTask ( ); // invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
v8 = com.android.server.wm.WindowProcessUtils .getTaskPackageNameLocked ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_8
/* .line 291 */
(( com.android.server.wm.Task ) v13 ).getChildAt ( v12 ); // invoke-virtual {v13, v12}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v8 ).asTask ( ); // invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
v8 = com.android.server.wm.WindowProcessUtils .getTaskPackageNameLocked ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_8
/* .line 292 */
int v8 = 2; // const/4 v8, 0x2
/* goto/16 :goto_0 */
/* .line 297 */
} // :cond_8
com.android.server.wm.MiuiSoScManagerStub .get ( );
v8 = (( com.android.server.wm.MiuiSoScManagerStub ) v8 ).isInSoScSingleMode ( v13 ); // invoke-virtual {v8, v13}, Lcom/android/server/wm/MiuiSoScManagerStub;->isInSoScSingleMode(Lcom/android/server/wm/WindowContainer;)Z
if ( v8 != null) { // if-eqz v8, :cond_9
/* .line 298 */
int v8 = 2; // const/4 v8, 0x2
/* goto/16 :goto_0 */
/* .line 301 */
} // :cond_9
int v8 = 0; // const/4 v8, 0x0
/* .line 302 */
/* .local v8, "flag":Z */
/* array-length v15, v9 */
} // :goto_2
/* if-ge v11, v15, :cond_b */
/* aget-object v16, v9, v11 */
/* move-object/from16 v17, v16 */
/* .line 303 */
/* .local v17, "whiteListPackageName":Ljava/lang/String; */
/* move-object/from16 v12, v17 */
} // .end local v17 # "whiteListPackageName":Ljava/lang/String;
/* .local v12, "whiteListPackageName":Ljava/lang/String; */
v17 = (( java.lang.String ) v12 ).equals ( v14 ); // invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v17 != null) { // if-eqz v17, :cond_a
/* .line 304 */
int v8 = 1; // const/4 v8, 0x1
/* .line 305 */
/* .line 302 */
} // .end local v12 # "whiteListPackageName":Ljava/lang/String;
} // :cond_a
/* add-int/lit8 v11, v11, 0x1 */
int v12 = 1; // const/4 v12, 0x1
/* .line 308 */
} // :cond_b
} // :goto_3
if ( v8 != null) { // if-eqz v8, :cond_c
int v8 = 2; // const/4 v8, 0x2
int v11 = 0; // const/4 v11, 0x0
int v12 = 1; // const/4 v12, 0x1
/* goto/16 :goto_0 */
/* .line 312 */
} // :cond_c
/* iget v11, v13, Lcom/android/server/wm/Task;->mTaskId:I */
v11 = java.lang.Integer .valueOf ( v11 );
if ( v11 != null) { // if-eqz v11, :cond_e
v11 = android.text.TextUtils .isEmpty ( v14 );
if ( v11 != null) { // if-eqz v11, :cond_d
/* iget-boolean v11, v13, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v11 != null) { // if-eqz v11, :cond_e
/* .line 313 */
v11 = (( com.android.server.wm.Task ) v13 ).hasChild ( ); // invoke-virtual {v13}, Lcom/android/server/wm/Task;->hasChild()Z
if ( v11 != null) { // if-eqz v11, :cond_e
/* .line 314 */
} // :cond_d
/* .line 316 */
} // .end local v8 # "flag":Z
} // .end local v13 # "task":Lcom/android/server/wm/Task;
} // .end local v14 # "taskPackageName":Ljava/lang/String;
} // :cond_e
int v8 = 2; // const/4 v8, 0x2
int v11 = 0; // const/4 v11, 0x0
int v12 = 1; // const/4 v12, 0x1
/* goto/16 :goto_0 */
/* .line 276 */
/* .restart local v13 # "task":Lcom/android/server/wm/Task; */
/* .restart local v14 # "taskPackageName":Ljava/lang/String; */
} // :cond_f
int v8 = 2; // const/4 v8, 0x2
int v11 = 0; // const/4 v11, 0x0
int v12 = 1; // const/4 v12, 0x1
/* goto/16 :goto_0 */
/* .line 318 */
} // .end local v13 # "task":Lcom/android/server/wm/Task;
} // .end local v14 # "taskPackageName":Ljava/lang/String;
} // :cond_10
v10 = } // :goto_4
if ( v10 != null) { // if-eqz v10, :cond_11
/* check-cast v10, Lcom/android/server/wm/Task; */
/* .line 319 */
/* .local v10, "task":Lcom/android/server/wm/Task; */
com.android.server.wm.WindowProcessUtils .removeTaskLocked ( v10,v4 );
/* .line 320 */
} // .end local v10 # "task":Lcom/android/server/wm/Task;
/* .line 321 */
} // .end local v0 # "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
} // .end local v9 # "whiteListPackageNames":[Ljava/lang/String;
} // :cond_11
/* monitor-exit v7 */
/* .line 322 */
return;
/* .line 321 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v7 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
/* .line 258 */
} // :cond_12
} // :goto_5
return;
} // .end method
public static void removeTasksInPackages ( java.util.List p0, Integer p1, com.android.server.am.IProcessPolicy p2, com.android.server.wm.ActivityTaskManagerService p3 ) {
/* .locals 6 */
/* .param p1, "userId" # I */
/* .param p2, "processPolicy" # Lcom/android/server/am/IProcessPolicy; */
/* .param p3, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I", */
/* "Lcom/android/server/am/IProcessPolicy;", */
/* "Lcom/android/server/wm/ActivityTaskManagerService;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 326 */
/* .local p0, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = if ( p0 != null) { // if-eqz p0, :cond_5
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 330 */
} // :cond_0
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 332 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 333 */
/* .local v1, "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;" */
(( com.android.server.wm.ActivityTaskManagerService ) p3 ).getRecentTasks ( ); // invoke-virtual {p3}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;
(( com.android.server.wm.RecentTasks ) v2 ).getRawTasks ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/android/server/wm/Task; */
/* .line 334 */
/* .local v3, "task":Lcom/android/server/wm/Task; */
com.android.server.wm.WindowProcessUtils .getTaskPackageNameLocked ( v3 );
/* .line 335 */
v5 = /* .local v4, "taskPackageName":Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 336 */
/* .line 339 */
} // :cond_1
/* iget v5, v3, Lcom/android/server/wm/Task;->mUserId:I */
/* if-ne v5, p1, :cond_2 */
v5 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v5, :cond_2 */
v5 = /* .line 340 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 341 */
/* .line 343 */
} // .end local v3 # "task":Lcom/android/server/wm/Task;
} // .end local v4 # "taskPackageName":Ljava/lang/String;
} // :cond_2
/* .line 345 */
} // :cond_3
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Lcom/android/server/wm/Task; */
/* .line 346 */
/* .restart local v3 # "task":Lcom/android/server/wm/Task; */
com.android.server.wm.WindowProcessUtils .removeTaskLocked ( v3,p3 );
/* .line 347 */
} // .end local v3 # "task":Lcom/android/server/wm/Task;
/* .line 348 */
} // .end local v1 # "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
} // :cond_4
/* monitor-exit v0 */
/* .line 349 */
return;
/* .line 348 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 327 */
} // :cond_5
} // :goto_2
return;
} // .end method
