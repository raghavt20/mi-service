.class Lcom/android/server/wm/TalkbackWatermark$1;
.super Landroid/database/ContentObserver;
.source "TalkbackWatermark.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/TalkbackWatermark;->init(Lcom/android/server/wm/WindowManagerService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/TalkbackWatermark;

.field final synthetic val$talkbackWatermarkEnableUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/android/server/wm/TalkbackWatermark;Landroid/os/Handler;Landroid/net/Uri;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/TalkbackWatermark;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 105
    iput-object p1, p0, Lcom/android/server/wm/TalkbackWatermark$1;->this$0:Lcom/android/server/wm/TalkbackWatermark;

    iput-object p3, p0, Lcom/android/server/wm/TalkbackWatermark$1;->val$talkbackWatermarkEnableUri:Landroid/net/Uri;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 108
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark$1;->val$talkbackWatermarkEnableUri:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/android/server/wm/TalkbackWatermark$1;->this$0:Lcom/android/server/wm/TalkbackWatermark;

    invoke-static {v0}, Lcom/android/server/wm/TalkbackWatermark;->-$$Nest$mupdateWaterMark(Lcom/android/server/wm/TalkbackWatermark;)V

    .line 111
    :cond_0
    return-void
.end method
