public class com.android.server.wm.DragDropControllerImpl implements com.android.server.wm.DragDropControllerStub {
	 /* .source "DragDropControllerImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.wm.DragDropController mDragDropController;
	 /* # direct methods */
	 static com.android.server.wm.DragDropControllerImpl ( ) {
		 /* .locals 1 */
		 /* .line 14 */
		 /* const-class v0, Lcom/android/server/wm/DragDropControllerImpl; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.server.wm.DragDropControllerImpl ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean cancelCurrentDrag ( ) {
		 /* .locals 4 */
		 /* .line 54 */
		 v0 = this.mDragDropController;
		 (( com.android.server.wm.DragDropController ) v0 ).getDragState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DragDropController;->getDragState()Lcom/android/server/wm/DragState;
		 /* .line 55 */
		 /* .local v0, "dragState":Lcom/android/server/wm/DragState; */
		 /* if-nez v0, :cond_0 */
		 /* .line 56 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 59 */
	 } // :cond_0
	 v1 = this.mDragDropController;
	 v2 = this.mToken;
	 int v3 = 1; // const/4 v3, 0x1
	 (( com.android.server.wm.DragDropController ) v1 ).cancelDragAndDrop ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/DragDropController;->cancelDragAndDrop(Landroid/os/IBinder;Z)V
	 /* .line 60 */
} // .end method
public void initDragDropController ( com.android.server.wm.DragDropController p0 ) {
	 /* .locals 0 */
	 /* .param p1, "dragDropController" # Lcom/android/server/wm/DragDropController; */
	 /* .line 18 */
	 this.mDragDropController = p1;
	 /* .line 19 */
	 return;
} // .end method
public void notifyDragFinish ( Boolean p0 ) {
	 /* .locals 3 */
	 /* .param p1, "dragResult" # Z */
	 /* .line 26 */
	 v0 = this.mDragDropController;
	 (( com.android.server.wm.DragDropController ) v0 ).getDragState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DragDropController;->getDragState()Lcom/android/server/wm/DragState;
	 /* .line 27 */
	 /* .local v0, "dragState":Lcom/android/server/wm/DragState; */
	 final String v1 = ""; // const-string v1, ""
	 /* .line 28 */
	 /* .local v1, "packageName":Ljava/lang/String; */
	 v2 = this.mDropWindow;
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 29 */
		 v2 = this.mDropWindow;
		 (( com.android.server.wm.WindowState ) v2 ).getOwningPackage ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
		 /* .line 31 */
	 } // :cond_0
	 com.xiaomi.mirror.service.MirrorServiceInternal .getInstance ( );
	 (( com.xiaomi.mirror.service.MirrorServiceInternal ) v2 ).notifyDragFinish ( v1, p1 ); // invoke-virtual {v2, v1, p1}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->notifyDragFinish(Ljava/lang/String;Z)V
	 /* .line 32 */
	 return;
} // .end method
public void notifyDragStart ( android.content.ClipData p0, Integer p1, Integer p2, Integer p3 ) {
	 /* .locals 1 */
	 /* .param p1, "data" # Landroid/content/ClipData; */
	 /* .param p2, "uid" # I */
	 /* .param p3, "pid" # I */
	 /* .param p4, "flag" # I */
	 /* .line 22 */
	 com.xiaomi.mirror.service.MirrorServiceInternal .getInstance ( );
	 (( com.xiaomi.mirror.service.MirrorServiceInternal ) v0 ).notifyDragStart ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->notifyDragStart(Landroid/content/ClipData;III)V
	 /* .line 23 */
	 return;
} // .end method
public Boolean setDragSurfaceVisible ( Boolean p0 ) {
	 /* .locals 5 */
	 /* .param p1, "visible" # Z */
	 /* .line 35 */
	 v0 = this.mDragDropController;
	 (( com.android.server.wm.DragDropController ) v0 ).getDragState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DragDropController;->getDragState()Lcom/android/server/wm/DragState;
	 /* .line 36 */
	 /* .local v0, "dragState":Lcom/android/server/wm/DragState; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-nez v0, :cond_0 */
	 /* .line 37 */
	 /* .line 39 */
} // :cond_0
try { // :try_start_0
	 /* new-instance v2, Landroid/view/SurfaceControl$Transaction; */
	 /* invoke-direct {v2}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 40 */
	 /* .local v2, "transaction":Landroid/view/SurfaceControl$Transaction; */
	 if ( p1 != null) { // if-eqz p1, :cond_1
		 /* .line 41 */
		 try { // :try_start_1
			 v3 = this.mSurfaceControl;
			 (( android.view.SurfaceControl$Transaction ) v2 ).show ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
			 /* .line 43 */
		 } // :cond_1
		 v3 = this.mSurfaceControl;
		 (( android.view.SurfaceControl$Transaction ) v2 ).hide ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
		 /* .line 45 */
	 } // :goto_0
	 (( android.view.SurfaceControl$Transaction ) v2 ).apply ( ); // invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->apply()V
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 46 */
	 /* nop */
	 /* .line 47 */
	 try { // :try_start_2
		 (( android.view.SurfaceControl$Transaction ) v2 ).close ( ); // invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->close()V
		 /* :try_end_2 */
		 /* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
		 /* .line 46 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* .line 39 */
		 /* :catchall_0 */
		 /* move-exception v3 */
		 try { // :try_start_3
			 (( android.view.SurfaceControl$Transaction ) v2 ).close ( ); // invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->close()V
			 /* :try_end_3 */
			 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
			 /* :catchall_1 */
			 /* move-exception v4 */
			 try { // :try_start_4
				 (( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
			 } // .end local v0 # "dragState":Lcom/android/server/wm/DragState;
		 } // .end local p0 # "this":Lcom/android/server/wm/DragDropControllerImpl;
	 } // .end local p1 # "visible":Z
} // :goto_1
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 47 */
} // .end local v2 # "transaction":Landroid/view/SurfaceControl$Transaction;
/* .restart local v0 # "dragState":Lcom/android/server/wm/DragState; */
/* .restart local p0 # "this":Lcom/android/server/wm/DragDropControllerImpl; */
/* .restart local p1 # "visible":Z */
/* :catch_0 */
/* move-exception v2 */
/* .line 48 */
/* .local v2, "e":Ljava/lang/Exception; */
v3 = com.android.server.wm.DragDropControllerImpl.TAG;
(( java.lang.Exception ) v2 ).getMessage ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 50 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // .end method
