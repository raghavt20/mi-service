.class Lcom/android/server/wm/AppResurrectionServiceImpl$2;
.super Landroid/content/BroadcastReceiver;
.source "AppResurrectionServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/AppResurrectionServiceImpl;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/AppResurrectionServiceImpl;


# direct methods
.method public static synthetic $r8$lambda$lldN_kQjnXncGJXBg1hW26PG8EI(Lcom/android/server/wm/AppResurrectionServiceImpl$2;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl$2;->lambda$onReceive$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/AppResurrectionServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/AppResurrectionServiceImpl;

    .line 176
    iput-object p1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl$2;->this$0:Lcom/android/server/wm/AppResurrectionServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private synthetic lambda$onReceive$0()V
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl$2;->this$0:Lcom/android/server/wm/AppResurrectionServiceImpl;

    invoke-static {v0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->-$$Nest$mloadDataFromSP(Lcom/android/server/wm/AppResurrectionServiceImpl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 179
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl$2;->this$0:Lcom/android/server/wm/AppResurrectionServiceImpl;

    invoke-static {v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->-$$Nest$fgetmIsLoadDone(Lcom/android/server/wm/AppResurrectionServiceImpl;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl$2;->this$0:Lcom/android/server/wm/AppResurrectionServiceImpl;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/wm/AppResurrectionServiceImpl;->-$$Nest$fputmIsLoadDone(Lcom/android/server/wm/AppResurrectionServiceImpl;Z)V

    .line 185
    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl$2;->this$0:Lcom/android/server/wm/AppResurrectionServiceImpl;

    invoke-static {v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->-$$Nest$fgetmMIUIBgHandler(Lcom/android/server/wm/AppResurrectionServiceImpl;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/AppResurrectionServiceImpl$2$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/wm/AppResurrectionServiceImpl$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl$2;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 188
    :cond_0
    return-void
.end method
