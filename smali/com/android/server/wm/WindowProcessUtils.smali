.class public Lcom/android/server/wm/WindowProcessUtils;
.super Ljava/lang/Object;
.source "WindowProcessUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sAtmsInstance:Lcom/android/server/wm/ActivityTaskManagerService;


# direct methods
.method public static synthetic $r8$lambda$9MUgBEw-OrJMC63pLXH4-UeMT5A(Lcom/android/server/wm/Task;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/server/wm/WindowProcessUtils;->isMultiWindowStackLocked(Lcom/android/server/wm/Task;)Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 31
    const-class v0, Lcom/android/server/wm/WindowProcessUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/WindowProcessUtils;->TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/wm/WindowProcessUtils;->sAtmsInstance:Lcom/android/server/wm/ActivityTaskManagerService;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getActivityComponentName(Lcom/android/server/wm/ActivityRecord;)Ljava/lang/String;
    .locals 1
    .param p0, "activity"    # Lcom/android/server/wm/ActivityRecord;

    .line 450
    iget-object v0, p0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    return-object v0
.end method

.method static declared-synchronized getActivityTaskManagerService()Lcom/android/server/wm/ActivityTaskManagerService;
    .locals 2

    const-class v0, Lcom/android/server/wm/WindowProcessUtils;

    monitor-enter v0

    .line 36
    :try_start_0
    sget-object v1, Lcom/android/server/wm/WindowProcessUtils;->sAtmsInstance:Lcom/android/server/wm/ActivityTaskManagerService;

    if-nez v1, :cond_0

    .line 37
    const-string v1, "activity_task"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/ActivityTaskManagerService;

    sput-object v1, Lcom/android/server/wm/WindowProcessUtils;->sAtmsInstance:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 40
    :cond_0
    sget-object v1, Lcom/android/server/wm/WindowProcessUtils;->sAtmsInstance:Lcom/android/server/wm/ActivityTaskManagerService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 35
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getAllTaskIdList(Lcom/android/server/wm/ActivityTaskManagerService;)Ljava/util/List;
    .locals 6
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/wm/ActivityTaskManagerService;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 114
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;

    move-result-object v1

    .line 115
    .local v1, "allRecentTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 116
    .local v2, "allTaskIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/wm/Task;

    .line 117
    .local v4, "task":Lcom/android/server/wm/Task;
    iget v5, v4, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    nop

    .end local v4    # "task":Lcom/android/server/wm/Task;
    goto :goto_0

    .line 119
    :cond_0
    monitor-exit v0

    return-object v2

    .line 120
    .end local v1    # "allRecentTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Task;>;"
    .end local v2    # "allTaskIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getCallerInfo(Lcom/android/server/wm/ActivityTaskManagerService;II)Lmiui/security/CallerInfo;
    .locals 4
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p1, "callingPid"    # I
    .param p2, "callingUid"    # I

    .line 491
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 492
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(II)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 493
    .local v1, "wpc":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_0

    .line 494
    new-instance v2, Lmiui/security/CallerInfo;

    invoke-direct {v2}, Lmiui/security/CallerInfo;-><init>()V

    .line 495
    .local v2, "info":Lmiui/security/CallerInfo;
    iget-object v3, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v3, v2, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    .line 496
    iget-object v3, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v3, v2, Lmiui/security/CallerInfo;->callerUid:I

    .line 497
    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v3

    iput v3, v2, Lmiui/security/CallerInfo;->callerPid:I

    .line 498
    iget-object v3, v1, Lcom/android/server/wm/WindowProcessController;->mName:Ljava/lang/String;

    iput-object v3, v2, Lmiui/security/CallerInfo;->callerProcessName:Ljava/lang/String;

    .line 499
    monitor-exit v0

    return-object v2

    .line 501
    .end local v2    # "info":Lmiui/security/CallerInfo;
    :cond_0
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    .line 502
    .end local v1    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getCallerInfo(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/app/IApplicationThread;)Lmiui/security/CallerInfo;
    .locals 4
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p1, "caller"    # Landroid/app/IApplicationThread;

    .line 476
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 477
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Landroid/app/IApplicationThread;)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 478
    .local v1, "wpc":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_0

    .line 479
    new-instance v2, Lmiui/security/CallerInfo;

    invoke-direct {v2}, Lmiui/security/CallerInfo;-><init>()V

    .line 480
    .local v2, "info":Lmiui/security/CallerInfo;
    iget-object v3, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v3, v2, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    .line 481
    iget-object v3, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v3, v2, Lmiui/security/CallerInfo;->callerUid:I

    .line 482
    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v3

    iput v3, v2, Lmiui/security/CallerInfo;->callerPid:I

    .line 483
    iget-object v3, v1, Lcom/android/server/wm/WindowProcessController;->mName:Ljava/lang/String;

    iput-object v3, v2, Lmiui/security/CallerInfo;->callerProcessName:Ljava/lang/String;

    .line 484
    monitor-exit v0

    return-object v2

    .line 486
    .end local v2    # "info":Lmiui/security/CallerInfo;
    :cond_0
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    .line 487
    .end local v1    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getCallerPackageName(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/app/IApplicationThread;)Ljava/lang/String;
    .locals 3
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p1, "caller"    # Landroid/app/IApplicationThread;

    .line 454
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 455
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Landroid/app/IApplicationThread;)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 456
    .local v1, "wpc":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    if-nez v2, :cond_0

    goto :goto_0

    .line 459
    :cond_0
    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    monitor-exit v0

    return-object v2

    .line 457
    :cond_1
    :goto_0
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    .line 461
    .end local v1    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getCallerUid(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/app/IApplicationThread;)I
    .locals 3
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p1, "caller"    # Landroid/app/IApplicationThread;

    .line 465
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 466
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Landroid/app/IApplicationThread;)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 467
    .local v1, "wpc":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    if-nez v2, :cond_0

    goto :goto_0

    .line 470
    :cond_0
    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    monitor-exit v0

    return v2

    .line 468
    :cond_1
    :goto_0
    monitor-exit v0

    const/4 v0, -0x1

    return v0

    .line 472
    .end local v1    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getMultiWindowForegroundAppInfoLocked(Lcom/android/server/wm/ActivityTaskManagerService;)Landroid/content/pm/ApplicationInfo;
    .locals 5
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 371
    const/4 v0, 0x0

    .line 372
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 373
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v2

    .line 374
    .local v2, "multiWindowStack":Lcom/android/server/wm/Task;
    if-eqz v2, :cond_0

    .line 375
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 376
    .local v3, "multiWindowActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v3, :cond_0

    .line 377
    iget-object v4, v3, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object v0, v4

    .line 380
    .end local v2    # "multiWindowStack":Lcom/android/server/wm/Task;
    .end local v3    # "multiWindowActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_0
    monitor-exit v1

    .line 381
    return-object v0

    .line 380
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static getMultiWindowStackLocked(Lcom/android/server/wm/ActivityTaskManagerService;)Lcom/android/server/wm/Task;
    .locals 4
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 124
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getChildCount()I

    move-result v0

    .line 125
    .local v0, "numDisplays":I
    const/4 v1, 0x0

    .local v1, "displayNdx":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 126
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/RootWindowContainer;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/DisplayContent;

    .line 127
    .local v2, "display":Lcom/android/server/wm/DisplayContent;
    new-instance v3, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda2;

    invoke-direct {v3}, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda2;-><init>()V

    invoke-virtual {v2, v3}, Lcom/android/server/wm/DisplayContent;->getRootTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;

    move-result-object v3

    .line 128
    .local v3, "result":Lcom/android/server/wm/Task;
    if-eqz v3, :cond_0

    .line 129
    return-object v3

    .line 125
    .end local v2    # "display":Lcom/android/server/wm/DisplayContent;
    .end local v3    # "result":Lcom/android/server/wm/Task;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    .end local v1    # "displayNdx":I
    :cond_1
    const/4 v1, 0x0

    return-object v1
.end method

.method public static getPerceptibleRecentAppList(Lcom/android/server/wm/ActivityTaskManagerService;)Ljava/util/Map;
    .locals 7
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/wm/ActivityTaskManagerService;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 74
    const/4 v0, 0x0

    .line 76
    .local v0, "taskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move-object v0, v1

    .line 79
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 80
    :try_start_0
    invoke-static {p0}, Lcom/android/server/wm/WindowProcessUtils;->getMultiWindowStackLocked(Lcom/android/server/wm/ActivityTaskManagerService;)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 81
    .local v2, "dockedStack":Lcom/android/server/wm/Task;
    if-eqz v2, :cond_0

    .line 82
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 83
    .local v3, "r":Lcom/android/server/wm/ActivityRecord;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 84
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v4

    iget v4, v4, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, v3, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    .end local v3    # "r":Lcom/android/server/wm/ActivityRecord;
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/wm/Task;

    .line 90
    .local v4, "task":Lcom/android/server/wm/Task;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v5

    if-nez v5, :cond_2

    .line 91
    goto :goto_0

    .line 94
    :cond_2
    invoke-static {v4}, Lcom/android/server/wm/WindowProcessUtils;->getTaskPackageNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "taskPackageName":Ljava/lang/String;
    invoke-static {v4}, Lcom/android/server/wm/WindowProcessUtils;->isTaskInMultiWindowStackLocked(Lcom/android/server/wm/Task;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 96
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "com.android.systemui"

    .line 97
    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 98
    invoke-virtual {v4}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z

    move-result v6

    if-nez v6, :cond_1

    .line 99
    invoke-virtual {v4}, Lcom/android/server/wm/Task;->isVisible()Z

    move-result v6

    if-nez v6, :cond_3

    .line 100
    goto :goto_0

    .line 103
    :cond_3
    iget v3, v4, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    nop

    .line 106
    .end local v2    # "dockedStack":Lcom/android/server/wm/Task;
    .end local v4    # "task":Lcom/android/server/wm/Task;
    .end local v5    # "taskPackageName":Ljava/lang/String;
    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    sget-object v1, Lcom/android/server/wm/WindowProcessUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPerceptibleRecentAppList: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return-object v0

    .line 106
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public static getTaskIntentForToken(Landroid/os/IBinder;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "token"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")",
            "Ljava/util/ArrayList<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .line 416
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getActivityTaskManagerService()Lcom/android/server/wm/ActivityTaskManagerService;

    move-result-object v0

    .line 417
    .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService;
    iget-object v1, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 418
    :try_start_0
    invoke-static {p0}, Lcom/android/server/wm/ActivityRecord;->isInRootTaskLocked(Landroid/os/IBinder;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 419
    .local v2, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    if-eqz v2, :cond_0

    .line 420
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 421
    .local v3, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v4

    new-instance v5, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda1;

    invoke-direct {v5, v3}, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda1;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {v4, v5}, Lcom/android/server/wm/Task;->forAllActivities(Ljava/util/function/Consumer;)V

    .line 424
    monitor-exit v1

    return-object v3

    .line 426
    .end local v2    # "activityRecord":Lcom/android/server/wm/ActivityRecord;
    .end local v3    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    :cond_0
    monitor-exit v1

    .line 427
    const/4 v1, 0x0

    return-object v1

    .line 426
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static getTaskPackageNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;
    .locals 2
    .param p0, "task"    # Lcom/android/server/wm/Task;

    .line 153
    const/4 v0, 0x0

    .line 154
    .local v0, "packageName":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 158
    :cond_0
    return-object v0
.end method

.method private static getTaskRootOrTopAppLocked(Lcom/android/server/wm/Task;)Lcom/android/server/wm/WindowProcessController;
    .locals 2
    .param p0, "task"    # Lcom/android/server/wm/Task;

    .line 170
    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 172
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 173
    .local v1, "record":Lcom/android/server/wm/ActivityRecord;
    if-nez v1, :cond_1

    .line 174
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 177
    :cond_1
    if-nez v1, :cond_2

    .line 178
    return-object v0

    .line 180
    :cond_2
    iget-object v0, v1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    return-object v0
.end method

.method public static getTaskTopApp(I)Lcom/android/server/wm/WindowProcessController;
    .locals 4
    .param p0, "taskId"    # I

    .line 162
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getActivityTaskManagerService()Lcom/android/server/wm/ActivityTaskManagerService;

    move-result-object v0

    .line 163
    .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService;
    iget-object v1, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 164
    :try_start_0
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/android/server/wm/RecentTasks;->getTask(I)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 165
    .local v2, "task":Lcom/android/server/wm/Task;
    invoke-static {v2}, Lcom/android/server/wm/WindowProcessUtils;->getTaskTopAppLocked(Lcom/android/server/wm/Task;)Lcom/android/server/wm/WindowProcessController;

    move-result-object v3

    monitor-exit v1

    return-object v3

    .line 166
    .end local v2    # "task":Lcom/android/server/wm/Task;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static getTaskTopAppLocked(Lcom/android/server/wm/Task;)Lcom/android/server/wm/WindowProcessController;
    .locals 2
    .param p0, "task"    # Lcom/android/server/wm/Task;

    .line 184
    if-eqz p0, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 186
    .local v0, "topActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_0

    .line 187
    iget-object v1, v0, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    return-object v1

    .line 190
    .end local v0    # "topActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private static getTaskTopAppProcessNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;
    .locals 3
    .param p0, "task"    # Lcom/android/server/wm/Task;

    .line 206
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 207
    return-object v0

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 210
    .local v1, "topActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_1

    .line 211
    iget-object v0, v1, Lcom/android/server/wm/ActivityRecord;->processName:Ljava/lang/String;

    return-object v0

    .line 213
    :cond_1
    return-object v0
.end method

.method private static getTaskTopAppUidLocked(Lcom/android/server/wm/Task;)I
    .locals 3
    .param p0, "task"    # Lcom/android/server/wm/Task;

    .line 194
    const/4 v0, -0x1

    .line 195
    .local v0, "uid":I
    if-nez p0, :cond_0

    .line 196
    return v0

    .line 198
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 199
    .local v1, "topActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_1

    .line 200
    iget-object v2, v1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 202
    :cond_1
    return v0
.end method

.method public static getTopRunningActivityInfo()Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 431
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getActivityTaskManagerService()Lcom/android/server/wm/ActivityTaskManagerService;

    move-result-object v0

    .line 432
    .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService;
    iget-object v1, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 433
    :try_start_0
    iget-object v2, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v2

    .line 434
    .local v2, "activityStack":Lcom/android/server/wm/Task;
    if-eqz v2, :cond_0

    .line 435
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 436
    .local v3, "r":Lcom/android/server/wm/ActivityRecord;
    if-eqz v3, :cond_0

    .line 437
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 438
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v5, "packageName"

    iget-object v6, v3, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    const-string/jumbo v5, "token"

    iget-object v6, v3, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    const-string/jumbo v5, "userId"

    iget v6, v3, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    const-string v5, "intent"

    iget-object v6, v3, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    monitor-exit v1

    return-object v4

    .line 445
    .end local v2    # "activityStack":Lcom/android/server/wm/Task;
    .end local v3    # "r":Lcom/android/server/wm/ActivityRecord;
    .end local v4    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    monitor-exit v1

    .line 446
    const/4 v1, 0x0

    return-object v1

    .line 445
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static getTopRunningPidLocked()I
    .locals 5

    .line 388
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getActivityTaskManagerService()Lcom/android/server/wm/ActivityTaskManagerService;

    move-result-object v0

    .line 389
    .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService;
    iget-object v1, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 390
    :try_start_0
    iget-object v2, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v2

    .line 391
    .local v2, "topStack":Lcom/android/server/wm/Task;
    if-eqz v2, :cond_0

    .line 392
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 393
    .local v3, "record":Lcom/android/server/wm/ActivityRecord;
    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v4, :cond_0

    .line 394
    iget-object v4, v3, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    invoke-virtual {v4}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v4

    monitor-exit v1

    return v4

    .line 397
    .end local v2    # "topStack":Lcom/android/server/wm/Task;
    .end local v3    # "record":Lcom/android/server/wm/ActivityRecord;
    :cond_0
    monitor-exit v1

    .line 398
    const/4 v1, 0x0

    return v1

    .line 397
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static getTopRunningProcessController()Lcom/android/server/wm/WindowProcessController;
    .locals 5

    .line 402
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getActivityTaskManagerService()Lcom/android/server/wm/ActivityTaskManagerService;

    move-result-object v0

    .line 403
    .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService;
    iget-object v1, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 404
    :try_start_0
    iget-object v2, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v2

    .line 405
    .local v2, "topStack":Lcom/android/server/wm/Task;
    if-eqz v2, :cond_0

    .line 406
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    .line 407
    .local v3, "record":Lcom/android/server/wm/ActivityRecord;
    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v4, :cond_0

    .line 408
    iget-object v4, v3, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    monitor-exit v1

    return-object v4

    .line 411
    .end local v2    # "topStack":Lcom/android/server/wm/Task;
    .end local v3    # "record":Lcom/android/server/wm/ActivityRecord;
    :cond_0
    monitor-exit v1

    .line 412
    const/4 v1, 0x0

    return-object v1

    .line 411
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static isMultiWindowStackLocked(Lcom/android/server/wm/Task;)Z
    .locals 1
    .param p0, "stack"    # Lcom/android/server/wm/Task;

    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public static isPackageRunning(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 8
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "uid"    # I

    .line 526
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_5

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    if-nez p3, :cond_0

    goto :goto_1

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 531
    :try_start_0
    invoke-virtual {p0, p2, p3}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    .line 532
    monitor-exit v0

    return v3

    .line 535
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mProcessMap:Lcom/android/server/wm/WindowProcessControllerMap;

    invoke-virtual {v2, p3}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcesses(I)Landroid/util/ArraySet;

    move-result-object v2

    .line 536
    .local v2, "uidMap":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;"
    if-nez v2, :cond_2

    .line 537
    monitor-exit v0

    return v1

    .line 539
    :cond_2
    invoke-virtual {v2}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/WindowProcessController;

    .line 540
    .local v5, "wpc":Lcom/android/server/wm/WindowProcessController;
    if-eqz v5, :cond_3

    .line 541
    invoke-virtual {v5}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 542
    invoke-virtual {v5}, Lcom/android/server/wm/WindowProcessController;->isCrashing()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v5}, Lcom/android/server/wm/WindowProcessController;->isNotResponding()Z

    move-result v6

    if-nez v6, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":widgetProvider"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v5, Lcom/android/server/wm/WindowProcessController;->mName:Ljava/lang/String;

    .line 545
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 546
    monitor-exit v0

    return v3

    .line 548
    .end local v5    # "wpc":Lcom/android/server/wm/WindowProcessController;
    :cond_3
    goto :goto_0

    .line 549
    .end local v2    # "uidMap":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;"
    :cond_4
    monitor-exit v0

    .line 550
    return v1

    .line 549
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 527
    :cond_5
    :goto_1
    return v1
.end method

.method public static isProcessHasActivityInOtherTaskLocked(Lcom/android/server/wm/WindowProcessController;I)Z
    .locals 7
    .param p0, "app"    # Lcom/android/server/wm/WindowProcessController;
    .param p1, "curTaskId"    # I

    .line 45
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getActivityTaskManagerService()Lcom/android/server/wm/ActivityTaskManagerService;

    move-result-object v0

    .line 46
    .local v0, "atms":Lcom/android/server/wm/ActivityTaskManagerService;
    iget-object v1, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 47
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wm/WindowProcessController;->getActivities()Ljava/util/List;

    move-result-object v2

    .line 48
    .local v2, "activities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    const/4 v3, 0x0

    if-nez v2, :cond_0

    monitor-exit v1

    return v3

    .line 49
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 50
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v5

    .line 51
    .local v5, "otherTask":Lcom/android/server/wm/Task;
    if-eqz v5, :cond_1

    iget v6, v5, Lcom/android/server/wm/Task;->mTaskId:I

    if-eq p1, v6, :cond_1

    iget-boolean v6, v5, Lcom/android/server/wm/Task;->inRecents:Z

    if-eqz v6, :cond_1

    .line 53
    invoke-static {v5}, Lcom/android/server/wm/WindowProcessUtils;->isTaskVisibleInRecents(Lcom/android/server/wm/Task;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 54
    monitor-exit v1

    const/4 v1, 0x1

    return v1

    .line 49
    .end local v5    # "otherTask":Lcom/android/server/wm/Task;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 57
    .end local v2    # "activities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    .end local v4    # "i":I
    :cond_2
    monitor-exit v1

    .line 58
    return v3

    .line 57
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static isProcessRunning(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;I)Z
    .locals 3
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 554
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    .line 557
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 558
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    monitor-exit v0

    return v1

    .line 559
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 555
    :cond_2
    :goto_0
    return v1
.end method

.method public static isRemoveTaskDisabled(ILjava/lang/String;Lcom/android/server/wm/ActivityTaskManagerService;)Z
    .locals 3
    .param p0, "taskId"    # I
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 217
    iget-object v0, p2, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 218
    :try_start_0
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/android/server/wm/RecentTasks;->getTask(I)Lcom/android/server/wm/Task;

    move-result-object v1

    .line 220
    .local v1, "task":Lcom/android/server/wm/Task;
    if-eqz v1, :cond_0

    .line 221
    invoke-static {v1}, Lcom/android/server/wm/WindowProcessUtils;->getTaskTopAppProcessNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    monitor-exit v0

    return v2

    .line 223
    .end local v1    # "task":Lcom/android/server/wm/Task;
    :cond_0
    monitor-exit v0

    .line 224
    const/4 v0, 0x0

    return v0

    .line 223
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static isRunningOnCarDisplay(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)Z
    .locals 3
    .param p0, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p1, "name"    # Ljava/lang/String;

    .line 506
    if-eqz p1, :cond_1

    const-string v0, "com.miui.carlink"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.baidu.carlife.xiaomi"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 507
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 509
    :cond_1
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 510
    .local v0, "result":Ljava/util/concurrent/atomic/AtomicBoolean;
    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    new-instance v2, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda0;

    invoke-direct {v2, p1, v0}, Lcom/android/server/wm/WindowProcessUtils$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {v1, v2}, Lcom/android/server/wm/RootWindowContainer;->forAllTasks(Ljava/util/function/Consumer;)V

    .line 521
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    return v1
.end method

.method private static isTaskInMultiWindowStackLocked(Lcom/android/server/wm/Task;)Z
    .locals 1
    .param p0, "task"    # Lcom/android/server/wm/Task;

    .line 136
    if-eqz p0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    .line 143
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private static isTaskVisibleInRecents(Lcom/android/server/wm/Task;)Z
    .locals 4
    .param p0, "task"    # Lcom/android/server/wm/Task;

    .line 62
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 63
    return v0

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    const/4 v2, 0x1

    if-nez v1, :cond_1

    .line 65
    return v2

    .line 66
    :cond_1
    iget-boolean v1, p0, Lcom/android/server/wm/Task;->isAvailable:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 67
    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const/high16 v3, 0x800000

    and-int/2addr v1, v3

    if-nez v1, :cond_2

    .line 68
    return v2

    .line 70
    :cond_2
    return v0
.end method

.method static synthetic lambda$getTaskIntentForToken$0(Ljava/util/ArrayList;Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p0, "arrayList"    # Ljava/util/ArrayList;
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 422
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    return-void
.end method

.method static synthetic lambda$isRunningOnCarDisplay$1(Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/android/server/wm/Task;)V
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "result"    # Ljava/util/concurrent/atomic/AtomicBoolean;
    .param p2, "task"    # Lcom/android/server/wm/Task;

    .line 511
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getTopMostActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 512
    .local v0, "ac":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 513
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    .line 514
    .local v1, "displayName":Ljava/lang/String;
    const-string v2, "com.miui.carlink"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.miui.car.launcher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 515
    const-string v2, "com.baidu.carlife.xiaomi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 516
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 519
    .end local v1    # "displayName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static removeAllTasks(Lcom/miui/server/process/ProcessManagerInternal;ILcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 6
    .param p0, "pms"    # Lcom/miui/server/process/ProcessManagerInternal;
    .param p1, "userId"    # I
    .param p2, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 228
    iget-object v0, p2, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 229
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 230
    .local v1, "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/Task;

    .line 231
    .local v3, "task":Lcom/android/server/wm/Task;
    iget v4, v3, Lcom/android/server/wm/Task;->mUserId:I

    if-eq v4, p1, :cond_0

    .line 232
    goto :goto_0

    .line 235
    :cond_0
    invoke-static {v3}, Lcom/android/server/wm/WindowProcessUtils;->getTaskRootOrTopAppLocked(Lcom/android/server/wm/Task;)Lcom/android/server/wm/WindowProcessController;

    move-result-object v4

    .line 236
    .local v4, "app":Lcom/android/server/wm/WindowProcessController;
    if-nez v4, :cond_1

    .line 237
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    goto :goto_0

    .line 241
    :cond_1
    iget-object v5, v4, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/miui/server/process/ProcessManagerInternal;->isTrimMemoryEnable(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 242
    goto :goto_0

    .line 245
    :cond_2
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    nop

    .end local v3    # "task":Lcom/android/server/wm/Task;
    .end local v4    # "app":Lcom/android/server/wm/WindowProcessController;
    goto :goto_0

    .line 248
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/Task;

    .line 249
    .restart local v3    # "task":Lcom/android/server/wm/Task;
    invoke-static {v3, p2}, Lcom/android/server/wm/WindowProcessUtils;->removeTaskLocked(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityTaskManagerService;)V

    .line 250
    .end local v3    # "task":Lcom/android/server/wm/Task;
    goto :goto_1

    .line 251
    .end local v1    # "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
    :cond_4
    monitor-exit v0

    .line 252
    return-void

    .line 251
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static removeTask(ILcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 2
    .param p0, "taskId"    # I
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 352
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 353
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/android/server/wm/RecentTasks;->getTask(I)Lcom/android/server/wm/Task;

    move-result-object v1

    .line 354
    .local v1, "task":Lcom/android/server/wm/Task;
    invoke-static {v1, p1}, Lcom/android/server/wm/WindowProcessUtils;->removeTaskLocked(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityTaskManagerService;)V

    .line 355
    .end local v1    # "task":Lcom/android/server/wm/Task;
    monitor-exit v0

    .line 356
    return-void

    .line 355
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static removeTaskLocked(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 4
    .param p0, "task"    # Lcom/android/server/wm/Task;
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 360
    if-nez p0, :cond_0

    .line 361
    return-void

    .line 367
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    const/4 v1, 0x1

    const-string v2, "removeTaskLocked"

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v3, v1, v2}, Lcom/android/server/wm/ActivityTaskSupervisor;->removeTask(Lcom/android/server/wm/Task;ZZLjava/lang/String;)V

    .line 368
    return-void
.end method

.method public static removeTasks(Ljava/util/List;Ljava/util/Set;Lcom/android/server/am/IProcessPolicy;Lcom/android/server/wm/ActivityTaskManagerService;Ljava/util/List;Ljava/util/List;)V
    .locals 18
    .param p2, "processPolicy"    # Lcom/android/server/am/IProcessPolicy;
    .param p3, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/android/server/am/IProcessPolicy;",
            "Lcom/android/server/wm/ActivityTaskManagerService;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 257
    .local p0, "taskIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p1, "whiteTaskSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p4, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "whiteListTaskId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    if-eqz v1, :cond_12

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_5

    .line 261
    :cond_0
    iget-object v7, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v7

    .line 263
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 264
    .local v0, "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
    const/4 v8, 0x2

    new-array v9, v8, [Ljava/lang/String;

    const-string v10, "com.miui.home"

    const/4 v11, 0x0

    aput-object v10, v9, v11

    const-string v10, "com.mi.android.globallauncher"

    const/4 v12, 0x1

    aput-object v10, v9, v12

    .line 265
    .local v9, "whiteListPackageNames":[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_10

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/server/wm/Task;

    .line 266
    .local v13, "task":Lcom/android/server/wm/Task;
    if-nez v13, :cond_2

    .line 267
    goto :goto_0

    .line 269
    :cond_2
    invoke-static {v13}, Lcom/android/server/wm/WindowProcessUtils;->getTaskPackageNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v14

    .line 272
    .local v14, "taskPackageName":Ljava/lang/String;
    if-nez v6, :cond_3

    if-eqz v5, :cond_3

    invoke-interface {v5, v14}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 273
    goto :goto_0

    .line 276
    :cond_3
    iget v15, v13, Lcom/android/server/wm/Task;->mUserId:I

    invoke-interface {v3, v14, v15}, Lcom/android/server/am/IProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v15

    if-nez v15, :cond_f

    if-eqz v2, :cond_4

    iget v15, v13, Lcom/android/server/wm/Task;->mTaskId:I

    .line 277
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v2, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    :cond_4
    if-eqz v6, :cond_5

    iget v15, v13, Lcom/android/server/wm/Task;->mTaskId:I

    .line 278
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v6, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 279
    goto :goto_0

    .line 284
    :cond_5
    iget-boolean v15, v13, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v15, :cond_8

    invoke-virtual {v13}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v15

    if-lt v15, v8, :cond_8

    .line 286
    invoke-virtual {v13, v11}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v15

    invoke-static {v15}, Lcom/android/server/wm/WindowProcessUtils;->getTaskPackageNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v15

    iget v8, v13, Lcom/android/server/wm/Task;->mUserId:I

    .line 285
    invoke-interface {v3, v15, v8}, Lcom/android/server/am/IProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 288
    invoke-virtual {v13, v12}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v8

    invoke-static {v8}, Lcom/android/server/wm/WindowProcessUtils;->getTaskPackageNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v8

    iget v15, v13, Lcom/android/server/wm/Task;->mUserId:I

    .line 287
    invoke-interface {v3, v8, v15}, Lcom/android/server/am/IProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v8

    if-nez v8, :cond_6

    goto :goto_1

    :cond_6
    const/4 v8, 0x2

    goto :goto_0

    :cond_7
    :goto_1
    if-eqz v5, :cond_8

    .line 290
    invoke-virtual {v13, v11}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v8

    invoke-static {v8}, Lcom/android/server/wm/WindowProcessUtils;->getTaskPackageNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 291
    invoke-virtual {v13, v12}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v8

    invoke-static {v8}, Lcom/android/server/wm/WindowProcessUtils;->getTaskPackageNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 292
    const/4 v8, 0x2

    goto/16 :goto_0

    .line 297
    :cond_8
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v8

    invoke-virtual {v8, v13}, Lcom/android/server/wm/MiuiSoScManagerStub;->isInSoScSingleMode(Lcom/android/server/wm/WindowContainer;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 298
    const/4 v8, 0x2

    goto/16 :goto_0

    .line 301
    :cond_9
    const/4 v8, 0x0

    .line 302
    .local v8, "flag":Z
    array-length v15, v9

    :goto_2
    if-ge v11, v15, :cond_b

    aget-object v16, v9, v11

    move-object/from16 v17, v16

    .line 303
    .local v17, "whiteListPackageName":Ljava/lang/String;
    move-object/from16 v12, v17

    .end local v17    # "whiteListPackageName":Ljava/lang/String;
    .local v12, "whiteListPackageName":Ljava/lang/String;
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 304
    const/4 v8, 0x1

    .line 305
    goto :goto_3

    .line 302
    .end local v12    # "whiteListPackageName":Ljava/lang/String;
    :cond_a
    add-int/lit8 v11, v11, 0x1

    const/4 v12, 0x1

    goto :goto_2

    .line 308
    :cond_b
    :goto_3
    if-eqz v8, :cond_c

    const/4 v8, 0x2

    const/4 v11, 0x0

    const/4 v12, 0x1

    goto/16 :goto_0

    .line 312
    :cond_c
    iget v11, v13, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v1, v11}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_d

    iget-boolean v11, v13, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v11, :cond_e

    .line 313
    invoke-virtual {v13}, Lcom/android/server/wm/Task;->hasChild()Z

    move-result v11

    if-eqz v11, :cond_e

    .line 314
    :cond_d
    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    .end local v8    # "flag":Z
    .end local v13    # "task":Lcom/android/server/wm/Task;
    .end local v14    # "taskPackageName":Ljava/lang/String;
    :cond_e
    const/4 v8, 0x2

    const/4 v11, 0x0

    const/4 v12, 0x1

    goto/16 :goto_0

    .line 276
    .restart local v13    # "task":Lcom/android/server/wm/Task;
    .restart local v14    # "taskPackageName":Ljava/lang/String;
    :cond_f
    const/4 v8, 0x2

    const/4 v11, 0x0

    const/4 v12, 0x1

    goto/16 :goto_0

    .line 318
    .end local v13    # "task":Lcom/android/server/wm/Task;
    .end local v14    # "taskPackageName":Ljava/lang/String;
    :cond_10
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_11

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/server/wm/Task;

    .line 319
    .local v10, "task":Lcom/android/server/wm/Task;
    invoke-static {v10, v4}, Lcom/android/server/wm/WindowProcessUtils;->removeTaskLocked(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityTaskManagerService;)V

    .line 320
    .end local v10    # "task":Lcom/android/server/wm/Task;
    goto :goto_4

    .line 321
    .end local v0    # "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
    .end local v9    # "whiteListPackageNames":[Ljava/lang/String;
    :cond_11
    monitor-exit v7

    .line 322
    return-void

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 258
    :cond_12
    :goto_5
    return-void
.end method

.method public static removeTasksInPackages(Ljava/util/List;ILcom/android/server/am/IProcessPolicy;Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 6
    .param p1, "userId"    # I
    .param p2, "processPolicy"    # Lcom/android/server/am/IProcessPolicy;
    .param p3, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/android/server/am/IProcessPolicy;",
            "Lcom/android/server/wm/ActivityTaskManagerService;",
            ")V"
        }
    .end annotation

    .line 326
    .local p0, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p0, :cond_5

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 330
    :cond_0
    iget-object v0, p3, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 332
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 333
    .local v1, "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
    invoke-virtual {p3}, Lcom/android/server/wm/ActivityTaskManagerService;->getRecentTasks()Lcom/android/server/wm/RecentTasks;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/RecentTasks;->getRawTasks()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/Task;

    .line 334
    .local v3, "task":Lcom/android/server/wm/Task;
    invoke-static {v3}, Lcom/android/server/wm/WindowProcessUtils;->getTaskPackageNameLocked(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v4

    .line 335
    .local v4, "taskPackageName":Ljava/lang/String;
    invoke-interface {p2, v4, p1}, Lcom/android/server/am/IProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 336
    goto :goto_0

    .line 339
    :cond_1
    iget v5, v3, Lcom/android/server/wm/Task;->mUserId:I

    if-ne v5, p1, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 340
    invoke-interface {p0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 341
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    .end local v3    # "task":Lcom/android/server/wm/Task;
    .end local v4    # "taskPackageName":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 345
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/Task;

    .line 346
    .restart local v3    # "task":Lcom/android/server/wm/Task;
    invoke-static {v3, p3}, Lcom/android/server/wm/WindowProcessUtils;->removeTaskLocked(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityTaskManagerService;)V

    .line 347
    .end local v3    # "task":Lcom/android/server/wm/Task;
    goto :goto_1

    .line 348
    .end local v1    # "removedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;"
    :cond_4
    monitor-exit v0

    .line 349
    return-void

    .line 348
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 327
    :cond_5
    :goto_2
    return-void
.end method
