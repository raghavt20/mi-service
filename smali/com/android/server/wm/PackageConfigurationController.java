public class com.android.server.wm.PackageConfigurationController extends java.lang.Thread {
	 /* .source "PackageConfigurationController.java" */
	 /* # static fields */
	 private static final java.lang.String COMMAND_OPTION_FORCE_UPDATE;
	 private static final java.lang.String COMMAND_OPTION_POLICY_RESET;
	 private static final java.lang.String PACKAGE_CONFIGURATION_COMMAND;
	 private static final java.lang.String PREFIX_ACTION_POLICY_UPDATED;
	 private static final java.lang.String SET_POLICY_DISABLED_COMMAND;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 final com.android.server.wm.ActivityTaskManagerService mAtmService;
	 private final android.content.Context mContext;
	 private final java.util.ArrayList mLogs;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
Boolean mPolicyDisabled;
private final java.util.Map mPolicyImplMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/wm/PolicyImpl;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Set mPolicyRequestQueue;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Set mTmpPolicyRequestQueue;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$ILQ1kQlQ9cml7xWQF4no8p7i5hY ( com.android.server.wm.PackageConfigurationController p0, java.lang.String p1, com.android.server.wm.PolicyImpl p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/PackageConfigurationController;->lambda$initialize$0(Ljava/lang/String;Lcom/android/server/wm/PolicyImpl;)V */
return;
} // .end method
public static void $r8$lambda$dRw9l6JiR3mL2yOX_InG527pou4 ( com.android.server.wm.PackageConfigurationController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/PackageConfigurationController;->lambda$scheduleUpdatePolicyItem$3()V */
return;
} // .end method
 com.android.server.wm.PackageConfigurationController ( ) {
/* .locals 1 */
/* .param p1, "atmService" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 41 */
final String v0 = "PackageConfigurationUpdateThread"; // const-string v0, "PackageConfigurationUpdateThread"
/* invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V */
/* .line 33 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mLogs = v0;
/* .line 34 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mPolicyImplMap = v0;
/* .line 35 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mPolicyRequestQueue = v0;
/* .line 36 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mTmpPolicyRequestQueue = v0;
/* .line 42 */
this.mAtmService = p1;
/* .line 43 */
v0 = this.mContext;
this.mContext = v0;
/* .line 44 */
return;
} // .end method
private void initialize ( ) {
/* .locals 3 */
/* .line 47 */
final String v0 = "PackageConfigurationController"; // const-string v0, "PackageConfigurationController"
final String v1 = "initialize"; // const-string v1, "initialize"
android.util.Slog .d ( v0,v1 );
/* .line 48 */
v0 = this.mPolicyImplMap;
/* new-instance v1, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/PackageConfigurationController;)V */
/* .line 52 */
int v0 = 0; // const/4 v0, 0x0
/* const-wide/32 v1, 0x493e0 */
(( com.android.server.wm.PackageConfigurationController ) p0 ).scheduleUpdatePolicyItem ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/wm/PackageConfigurationController;->scheduleUpdatePolicyItem(Ljava/lang/String;J)V
/* .line 53 */
return;
} // .end method
static void lambda$executeShellCommand$1 ( java.io.PrintWriter p0, java.lang.String p1, com.android.server.wm.PolicyImpl p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "pw" # Ljava/io/PrintWriter; */
/* .param p1, "str" # Ljava/lang/String; */
/* .param p2, "policy" # Lcom/android/server/wm/PolicyImpl; */
/* .line 69 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.wm.PolicyImpl ) p2 ).updatePolicyItem ( v0 ); // invoke-virtual {p2, v0}, Lcom/android/server/wm/PolicyImpl;->updatePolicyItem(Z)V
/* .line 70 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " update forced."; // const-string v1, " update forced."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 71 */
return;
} // .end method
static void lambda$executeShellCommand$2 ( java.lang.String p0, com.android.server.wm.PolicyImpl p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "str" # Ljava/lang/String; */
/* .param p1, "policy" # Lcom/android/server/wm/PolicyImpl; */
/* .line 87 */
(( com.android.server.wm.PolicyImpl ) p1 ).propagateToCallbacks ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V
/* .line 88 */
return;
} // .end method
private void lambda$initialize$0 ( java.lang.String p0, com.android.server.wm.PolicyImpl p1 ) { //synthethic
/* .locals 3 */
/* .param p1, "str" # Ljava/lang/String; */
/* .param p2, "policy" # Lcom/android/server/wm/PolicyImpl; */
/* .line 49 */
(( com.android.server.wm.PolicyImpl ) p2 ).init ( ); // invoke-virtual {p2}, Lcom/android/server/wm/PolicyImpl;->init()V
/* .line 50 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "sec.app.policy.UPDATE." */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-wide/16 v1, 0x0 */
(( com.android.server.wm.PackageConfigurationController ) p0 ).scheduleUpdatePolicyItem ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/wm/PackageConfigurationController;->scheduleUpdatePolicyItem(Ljava/lang/String;J)V
/* .line 51 */
return;
} // .end method
private void lambda$scheduleUpdatePolicyItem$3 ( ) { //synthethic
/* .locals 2 */
/* .line 137 */
/* monitor-enter p0 */
/* .line 139 */
try { // :try_start_0
v0 = this.mPolicyRequestQueue;
v1 = this.mTmpPolicyRequestQueue;
/* .line 140 */
v0 = this.mTmpPolicyRequestQueue;
/* .line 141 */
(( java.lang.Object ) p0 ).notifyAll ( ); // invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 143 */
/* .line 144 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 142 */
/* :catch_0 */
/* move-exception v0 */
/* .line 144 */
} // :goto_0
try { // :try_start_1
/* monitor-exit p0 */
/* .line 145 */
return;
/* .line 144 */
} // :goto_1
/* monitor-exit p0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
} // .end method
/* # virtual methods */
Boolean executeShellCommand ( java.lang.String p0, java.lang.String[] p1, java.io.PrintWriter p2 ) {
/* .locals 6 */
/* .param p1, "command" # Ljava/lang/String; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 56 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 57 */
final String v0 = "PackageConfigurationController"; // const-string v0, "PackageConfigurationController"
final String v2 = "MiuiAppSizeCompatMode not enabled"; // const-string v2, "MiuiAppSizeCompatMode not enabled"
android.util.Slog .d ( v0,v2 );
/* .line 58 */
/* .line 61 */
} // :cond_0
/* monitor-enter p0 */
/* .line 62 */
int v0 = 0; // const/4 v0, 0x0
/* .line 63 */
/* .local v0, "number":I */
try { // :try_start_0
final String v2 = "-packageconfiguration"; // const-string v2, "-packageconfiguration"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 64 */
/* array-length v2, p2 */
/* if-ne v2, v3, :cond_1 */
/* .line 65 */
(( java.io.PrintWriter ) p3 ).println ( ); // invoke-virtual {p3}, Ljava/io/PrintWriter;->println()V
/* .line 66 */
final String v2 = "ForceUpdate"; // const-string v2, "ForceUpdate"
/* aget-object v1, p2, v1 */
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 67 */
final String v1 = "Started the update."; // const-string v1, "Started the update."
(( java.io.PrintWriter ) p3 ).println ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 68 */
v1 = this.mPolicyImplMap;
/* new-instance v2, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, p3}, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda2;-><init>(Ljava/io/PrintWriter;)V */
/* .line 75 */
} // :cond_1
/* monitor-exit p0 */
/* .line 78 */
} // :cond_2
final String v2 = "-setPolicyDisabled"; // const-string v2, "-setPolicyDisabled"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 79 */
/* array-length v2, p2 */
/* if-ne v2, v3, :cond_4 */
/* .line 80 */
/* aget-object v2, p2, v1 */
/* if-nez v2, :cond_3 */
/* .line 81 */
/* monitor-exit p0 */
/* .line 83 */
} // :cond_3
/* aget-object v1, p2, v1 */
v1 = java.lang.Boolean .parseBoolean ( v1 );
/* .line 84 */
/* .local v1, "newPolicyDisabled":Z */
/* iget-boolean v2, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyDisabled:Z */
/* if-eq v2, v1, :cond_4 */
/* .line 85 */
/* iput-boolean v1, p0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyDisabled:Z */
/* .line 86 */
v2 = this.mPolicyImplMap;
/* new-instance v4, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda3; */
/* invoke-direct {v4}, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda3;-><init>()V */
/* .line 92 */
} // .end local v1 # "newPolicyDisabled":Z
} // :cond_4
/* monitor-exit p0 */
/* .line 95 */
} // :cond_5
v2 = this.mPolicyImplMap;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 96 */
/* .local v4, "item":Ljava/lang/Object; */
/* move-object v5, v4 */
/* check-cast v5, Lcom/android/server/wm/PolicyImpl; */
v5 = (( com.android.server.wm.PolicyImpl ) v5 ).executeShellCommandLocked ( p1, p2, p3 ); // invoke-virtual {v5, p1, p2, p3}, Lcom/android/server/wm/PolicyImpl;->executeShellCommandLocked(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 97 */
/* monitor-exit p0 */
/* .line 99 */
} // .end local v4 # "item":Ljava/lang/Object;
} // :cond_6
/* .line 100 */
} // :cond_7
/* monitor-exit p0 */
/* .line 101 */
} // .end local v0 # "number":I
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
void registerPolicy ( com.android.server.wm.PolicyImpl p0 ) {
/* .locals 2 */
/* .param p1, "impl" # Lcom/android/server/wm/PolicyImpl; */
/* .line 105 */
v0 = this.mPolicyImplMap;
(( com.android.server.wm.PolicyImpl ) p1 ).getPolicyName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PolicyImpl;->getPolicyName()Ljava/lang/String;
/* .line 106 */
v0 = this.mTmpPolicyRequestQueue;
(( com.android.server.wm.PolicyImpl ) p1 ).getPolicyName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PolicyImpl;->getPolicyName()Ljava/lang/String;
/* .line 107 */
return;
} // .end method
public void run ( ) {
/* .locals 4 */
/* .line 111 */
/* const/16 v0, 0xa */
android.os.Process .setThreadPriority ( v0 );
/* .line 112 */
/* invoke-direct {p0}, Lcom/android/server/wm/PackageConfigurationController;->initialize()V */
/* .line 113 */
/* monitor-enter p0 */
/* .line 116 */
} // :goto_0
try { // :try_start_0
v0 = this.mPolicyRequestQueue;
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 117 */
/* .local v1, "str":Ljava/lang/String; */
v2 = this.mPolicyImplMap;
/* check-cast v2, Lcom/android/server/wm/PolicyImpl; */
/* .line 118 */
/* .local v2, "policyImpl":Lcom/android/server/wm/PolicyImpl; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 119 */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.wm.PolicyImpl ) v2 ).updatePolicyItem ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/PolicyImpl;->updatePolicyItem(Z)V
/* .line 121 */
} // .end local v1 # "str":Ljava/lang/String;
} // .end local v2 # "policyImpl":Lcom/android/server/wm/PolicyImpl;
} // :cond_0
/* .line 122 */
} // :cond_1
v0 = this.mPolicyRequestQueue;
/* .line 123 */
(( java.lang.Object ) p0 ).wait ( ); // invoke-virtual {p0}, Ljava/lang/Object;->wait()V
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 126 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 124 */
/* :catch_0 */
/* move-exception v0 */
/* .line 125 */
/* .local v0, "e":Ljava/lang/InterruptedException; */
try { // :try_start_1
(( java.lang.InterruptedException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 127 */
} // .end local v0 # "e":Ljava/lang/InterruptedException;
} // :goto_2
} // :goto_3
/* nop */
} // .end local p0 # "this":Lcom/android/server/wm/PackageConfigurationController;
try { // :try_start_2
/* throw v0 */
/* .line 129 */
/* .restart local p0 # "this":Lcom/android/server/wm/PackageConfigurationController; */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v0 */
} // .end method
void scheduleUpdatePolicyItem ( java.lang.String p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "policyRequest" # Ljava/lang/String; */
/* .param p2, "delayMillis" # J */
/* .line 133 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 134 */
v0 = this.mTmpPolicyRequestQueue;
/* .line 136 */
} // :cond_0
v0 = this.mAtmService;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/PackageConfigurationController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/PackageConfigurationController;)V */
(( com.android.server.wm.ActivityTaskManagerService$H ) v0 ).postDelayed ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/wm/ActivityTaskManagerService$H;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 146 */
return;
} // .end method
void startThread ( ) {
/* .locals 1 */
/* .line 149 */
v0 = v0 = this.mPolicyImplMap;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 150 */
return;
/* .line 152 */
} // :cond_0
(( com.android.server.wm.PackageConfigurationController ) p0 ).start ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PackageConfigurationController;->start()V
/* .line 153 */
return;
} // .end method
