.class abstract Lcom/android/server/wm/PolicyImpl;
.super Ljava/lang/Object;
.source "PolicyImpl.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.miui.android.sm.policy"

.field private static final AUTHORITY_URI:Landroid/net/Uri;

.field private static final CALLBACKS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/util/function/Consumer<",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;>;"
        }
    .end annotation
.end field

.field private static final INDEX_CONFIGURATION_NAME:I = 0x3

.field private static final INDEX_CONFIGURATION_VALUE:I = 0x4

.field private static final INDEX_PACKAGE_NAME:I = 0x2

.field private static final POLICY_ITEM:Ljava/lang/String; = "policy_item"

.field private static final POLICY_LIST:Ljava/lang/String; = "policy_list"

.field private static final POLICY_LIST_PROJECTION:[Ljava/lang/String;

.field private static final POLICY_LIST_SELECTION:Ljava/lang/String; = "policyName=?"

.field private static final POLICY_LIST_URI:Landroid/net/Uri;

.field private static final RETRY_NUMBER:I = 0x3

.field private static final RETRY_TIME_OUT:J = 0x927c0L

.field private static final TAG:Ljava/lang/String; = "PolicyImpl"


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private final mController:Lcom/android/server/wm/PackageConfigurationController;

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/wm/PackageConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private mPolicyItem:Lcom/android/server/wm/PolicyItem;

.field private mRetryNumber:I


# direct methods
.method public static synthetic $r8$lambda$MEBaMqmM1drI2U0bvDLMLdratpc(Lcom/android/server/wm/PolicyImpl;Lcom/android/server/wm/PackageConfiguration;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/PolicyImpl;->lambda$init$1(Lcom/android/server/wm/PackageConfiguration;)V

    return-void
.end method

.method public static synthetic $r8$lambda$cGov_C-B0bsh28ndWHu4wDqVMVY(Lcom/android/server/wm/PolicyImpl;Lcom/android/server/wm/PackageConfiguration;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/PolicyImpl;->lambda$updateDataMapFromCloud$0(Lcom/android/server/wm/PackageConfiguration;)V

    return-void
.end method

.method public static synthetic $r8$lambda$hHmS8NowQw5dluNSESrbBxUVa7w(Lcom/android/server/wm/PolicyImpl;Lcom/android/server/wm/PackageConfiguration;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/PolicyImpl;->lambda$propagateToCallbacks$3(Lcom/android/server/wm/PackageConfiguration;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 34
    const-string v0, "com.miui.android.sm.policy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/PolicyImpl;->AUTHORITY_URI:Landroid/net/Uri;

    .line 37
    const-string v1, "policyVersion"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/wm/PolicyImpl;->POLICY_LIST_PROJECTION:[Ljava/lang/String;

    .line 39
    const-string v1, "policy_list"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/PolicyImpl;->POLICY_LIST_URI:Landroid/net/Uri;

    .line 42
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/android/server/wm/PolicyImpl;->CALLBACKS:Ljava/util/Map;

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/PackageConfigurationController;Ljava/lang/String;)V
    .locals 1
    .param p1, "controller"    # Lcom/android/server/wm/PackageConfigurationController;
    .param p2, "policyName"    # Ljava/lang/String;

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/server/wm/PolicyImpl;->mRetryNumber:I

    .line 58
    iput-object p1, p0, Lcom/android/server/wm/PolicyImpl;->mController:Lcom/android/server/wm/PackageConfigurationController;

    .line 59
    iput-object p2, p0, Lcom/android/server/wm/PolicyImpl;->mName:Ljava/lang/String;

    .line 60
    iget-object v0, p1, Lcom/android/server/wm/PackageConfigurationController;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/server/wm/PolicyImpl;->mContext:Landroid/content/Context;

    .line 61
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/PolicyImpl;->mContentResolver:Landroid/content/ContentResolver;

    .line 62
    return-void
.end method

.method private synthetic lambda$init$1(Lcom/android/server/wm/PackageConfiguration;)V
    .locals 2
    .param p1, "pkgConfig"    # Lcom/android/server/wm/PackageConfiguration;

    .line 181
    invoke-virtual {p1}, Lcom/android/server/wm/PackageConfiguration;->getPolicyDataMap()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    .line 182
    .local v0, "policyMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p1, Lcom/android/server/wm/PackageConfiguration;->mName:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/PolicyImpl;->getPolicyDataMapFromLocal(Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)V

    .line 183
    return-void
.end method

.method static synthetic lambda$propagateToCallbacks$2(Ljava/util/List;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 3
    .param p0, "consumerMap"    # Ljava/util/List;
    .param p1, "policyMap"    # Ljava/util/concurrent/ConcurrentHashMap;

    .line 208
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 209
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/function/Consumer;

    .line 210
    .local v1, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1, p1}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    .end local v1    # "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 214
    .end local v0    # "i":I
    :cond_0
    goto :goto_1

    .line 212
    :catch_0
    move-exception v0

    .line 213
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PolicyImpl"

    const-string v2, "propagateToCallbacks"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 215
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private synthetic lambda$propagateToCallbacks$3(Lcom/android/server/wm/PackageConfiguration;)V
    .locals 4
    .param p1, "pkgConfig"    # Lcom/android/server/wm/PackageConfiguration;

    .line 196
    sget-object v0, Lcom/android/server/wm/PolicyImpl;->CALLBACKS:Ljava/util/Map;

    iget-object v1, p1, Lcom/android/server/wm/PackageConfiguration;->mName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 197
    .local v0, "consumerMap":Ljava/util/List;, "Ljava/util/List<Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    if-nez v0, :cond_0

    .line 198
    return-void

    .line 200
    :cond_0
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 201
    .local v1, "policyMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/android/server/wm/PolicyImpl;->mController:Lcom/android/server/wm/PackageConfigurationController;

    iget-boolean v2, v2, Lcom/android/server/wm/PackageConfigurationController;->mPolicyDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p1, Lcom/android/server/wm/PackageConfiguration;->mName:Ljava/lang/String;

    .line 202
    invoke-virtual {p0, v2}, Lcom/android/server/wm/PolicyImpl;->isDisabledConfiguration(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 203
    invoke-virtual {p1}, Lcom/android/server/wm/PackageConfiguration;->getPolicyDataMap()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->putAll(Ljava/util/Map;)V

    .line 206
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/PolicyImpl;->mController:Lcom/android/server/wm/PackageConfigurationController;

    iget-object v2, v2, Lcom/android/server/wm/PackageConfigurationController;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mH:Lcom/android/server/wm/ActivityTaskManagerService$H;

    new-instance v3, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda0;

    invoke-direct {v3, v0, v1}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda0;-><init>(Ljava/util/List;Ljava/util/concurrent/ConcurrentHashMap;)V

    invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 216
    return-void
.end method

.method private synthetic lambda$updateDataMapFromCloud$0(Lcom/android/server/wm/PackageConfiguration;)V
    .locals 2
    .param p1, "pkgConfig"    # Lcom/android/server/wm/PackageConfiguration;

    .line 143
    invoke-virtual {p1}, Lcom/android/server/wm/PackageConfiguration;->getPolicyDataMap()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    .line 144
    .local v0, "policyMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p1, Lcom/android/server/wm/PackageConfiguration;->mName:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/PolicyImpl;->getPolicyDataMapFromCloud(Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)V

    .line 145
    return-void
.end method

.method static synthetic lambda$updatePackageConfigurationsIfNeeded$4(Lcom/android/server/wm/PackageConfiguration;)V
    .locals 0
    .param p0, "pkgConfig"    # Lcom/android/server/wm/PackageConfiguration;

    .line 258
    invoke-virtual {p0}, Lcom/android/server/wm/PackageConfiguration;->updatePrepared()V

    .line 259
    return-void
.end method

.method static synthetic lambda$updatePackageConfigurationsIfNeeded$5(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/server/wm/PackageConfiguration;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "pkgConfig"    # Lcom/android/server/wm/PackageConfiguration;

    .line 266
    iget-object v0, p3, Lcom/android/server/wm/PackageConfiguration;->mName:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {p3, p1, p2}, Lcom/android/server/wm/PackageConfiguration;->updateFromScpm(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_0
    return-void
.end method

.method static synthetic lambda$updatePackageConfigurationsIfNeeded$6(Lcom/android/server/wm/PackageConfiguration;)V
    .locals 0
    .param p0, "pkgConfig"    # Lcom/android/server/wm/PackageConfiguration;

    .line 273
    invoke-virtual {p0}, Lcom/android/server/wm/PackageConfiguration;->updateCompleted()V

    .line 274
    return-void
.end method

.method private registerDataObserver()V
    .locals 4

    .line 120
    invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->updateDataMapFromCloud()V

    .line 121
    iget-object v0, p0, Lcom/android/server/wm/PolicyImpl;->mContentResolver:Landroid/content/ContentResolver;

    .line 122
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/PolicyImpl$1;

    .line 123
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/wm/PolicyImpl$1;-><init>(Lcom/android/server/wm/PolicyImpl;Landroid/os/Handler;)V

    .line 121
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 130
    return-void
.end method


# virtual methods
.method executeDebugModeLocked(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "pkgNames"    # [Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "configurationName"    # Ljava/lang/String;

    .line 69
    sget-object v0, Lcom/android/server/wm/PolicyImpl;->CALLBACKS:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-string v0, "Can not execute, There is no registered callback."

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 71
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/PolicyImpl;->mController:Lcom/android/server/wm/PackageConfigurationController;

    iget-boolean v0, v0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyDisabled:Z

    if-eqz v0, :cond_1

    .line 74
    const-string v0, "Policy is disabled."

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 75
    return-void

    .line 77
    :cond_1
    invoke-virtual {p0, p4}, Lcom/android/server/wm/PolicyImpl;->isDisabledConfiguration(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    const-string v0, "Configuration is disabled."

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 79
    return-void

    .line 81
    :cond_2
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 82
    .local v0, "tmpPolicyDataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v1, "tmpPolicyRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p2

    if-ge v2, v3, :cond_6

    .line 84
    const-string v3, "fullscreenpackage"

    invoke-virtual {v3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 85
    const-string v3, "fullscreencomponent"

    invoke-virtual {v3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    nop

    .line 86
    const-string v3, "--remove"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "0"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    goto :goto_1

    .line 89
    :cond_4
    aget-object v3, p2, v2

    invoke-virtual {v0, v3, p3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 87
    :cond_5
    :goto_1
    aget-object v3, p2, v2

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 93
    .end local v2    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/PackageConfiguration;

    .line 94
    .local v3, "pkgConfig":Lcom/android/server/wm/PackageConfiguration;
    iget-object v4, v3, Lcom/android/server/wm/PackageConfiguration;->mName:Ljava/lang/String;

    invoke-virtual {v4, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 95
    invoke-virtual {v3}, Lcom/android/server/wm/PackageConfiguration;->getPolicyDataMap()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    .line 98
    .local v2, "dataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 99
    .local v5, "name":Ljava/lang/String;
    const-string v6, "-1"

    invoke-virtual {v2, v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    .end local v5    # "name":Ljava/lang/String;
    goto :goto_4

    .line 101
    :cond_7
    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putAll(Ljava/util/Map;)V

    .line 102
    goto :goto_5

    .line 104
    .end local v2    # "dataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "pkgConfig":Lcom/android/server/wm/PackageConfiguration;
    :cond_8
    goto :goto_3

    .line 106
    :cond_9
    :goto_5
    iget-object v2, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    const-string v3, "Modified"

    invoke-virtual {v2, v3}, Lcom/android/server/wm/PolicyItem;->setCurrentVersion(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V

    .line 108
    return-void
.end method

.method executeShellCommandLocked(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
    .locals 1
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method abstract getLocalVersion()I
.end method

.method abstract getPolicyDataMapFromCloud(Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method abstract getPolicyDataMapFromLocal(Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public getPolicyName()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/android/server/wm/PolicyImpl;->mName:Ljava/lang/String;

    return-object v0
.end method

.method getScpmVersionFromQuery()I
    .locals 6

    .line 152
    iget-object v0, p0, Lcom/android/server/wm/PolicyImpl;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/server/wm/PolicyImpl;->POLICY_LIST_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/server/wm/PolicyImpl;->POLICY_LIST_PROJECTION:[Ljava/lang/String;

    const-string v3, "policyName=?"

    iget-object v4, p0, Lcom/android/server/wm/PolicyImpl;->mName:Ljava/lang/String;

    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 153
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 154
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 155
    return v1

    .line 157
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_1

    .line 158
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 159
    return v1

    .line 161
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 162
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 163
    .local v1, "cursorValue":Ljava/lang/String;
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 164
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method init()V
    .locals 5

    .line 169
    sget-object v0, Lcom/android/server/wm/PolicyImpl;->CALLBACKS:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    return-void

    .line 172
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    .line 173
    .local v0, "callbackNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    if-nez v1, :cond_1

    .line 174
    new-instance v1, Lcom/android/server/wm/PolicyItem;

    invoke-direct {v1, v0}, Lcom/android/server/wm/PolicyItem;-><init>(Ljava/util/Set;)V

    iput-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {v1}, Lcom/android/server/wm/PolicyItem;->getPackageConfigurationList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    .line 177
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->getLocalVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/server/wm/PolicyItem;->setLocalVersion(I)V

    .line 178
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {v1}, Lcom/android/server/wm/PolicyItem;->getScpmVersion()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {v2}, Lcom/android/server/wm/PolicyItem;->getLocalVersion()I

    move-result v2

    if-ge v1, v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 179
    .local v1, "version":I
    :goto_0
    if-eqz v1, :cond_3

    .line 180
    iget-object v2, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    new-instance v3, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda5;

    invoke-direct {v3, p0}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/PolicyImpl;)V

    invoke-interface {v2, v3}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 184
    iget-object v2, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {v4}, Lcom/android/server/wm/PolicyItem;->getLocalVersion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(LOCAL)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/wm/PolicyItem;->setCurrentVersion(Ljava/lang/String;)V

    .line 186
    :cond_3
    invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V

    .line 187
    invoke-direct {p0}, Lcom/android/server/wm/PolicyImpl;->registerDataObserver()V

    .line 188
    return-void
.end method

.method isDisabledConfiguration(Ljava/lang/String;)Z
    .locals 1
    .param p1, "configurationName"    # Ljava/lang/String;

    .line 191
    const/4 v0, 0x0

    return v0
.end method

.method propagateToCallbacks()V
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    new-instance v1, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/PolicyImpl;)V

    invoke-interface {v0, v1}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 217
    return-void
.end method

.method registerCallback(Ljava/lang/String;Ljava/util/function/Consumer;)V
    .locals 3
    .param p1, "callbackName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/function/Consumer<",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 220
    .local p2, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    sget-object v0, Lcom/android/server/wm/PolicyImpl;->CALLBACKS:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 221
    .local v1, "callbackList":Ljava/util/List;, "Ljava/util/List<Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    if-nez v1, :cond_0

    .line 222
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 225
    :cond_0
    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    return-void
.end method

.method updateDataMapFromCloud()V
    .locals 4

    .line 133
    sget-object v0, Lcom/android/server/wm/PolicyImpl;->CALLBACKS:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    return-void

    .line 136
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    .line 137
    .local v0, "callbackNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    if-nez v1, :cond_1

    .line 138
    new-instance v1, Lcom/android/server/wm/PolicyItem;

    invoke-direct {v1, v0}, Lcom/android/server/wm/PolicyItem;-><init>(Ljava/util/Set;)V

    iput-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    .line 140
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {v1}, Lcom/android/server/wm/PolicyItem;->getPackageConfigurationList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    .line 141
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->getLocalVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/server/wm/PolicyItem;->setLocalVersion(I)V

    .line 142
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    new-instance v2, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda6;

    invoke-direct {v2, p0}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/PolicyImpl;)V

    invoke-interface {v1, v2}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 146
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {v3}, Lcom/android/server/wm/PolicyItem;->getLocalVersion()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "(CLOUD)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/wm/PolicyItem;->setCurrentVersion(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V

    .line 149
    return-void
.end method

.method updatePackageConfigurationsIfNeeded()Z
    .locals 14

    .line 230
    const/4 v0, 0x0

    .line 231
    .local v0, "updated":Z
    iget-object v1, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {v1}, Lcom/android/server/wm/PolicyItem;->getLocalVersion()I

    move-result v1

    .line 232
    .local v1, "localVersion":I
    iget-object v2, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {v2}, Lcom/android/server/wm/PolicyItem;->getScpmVersion()I

    move-result v2

    .line 233
    .local v2, "scpmVersion":I
    invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->getScpmVersionFromQuery()I

    move-result v3

    .line 234
    .local v3, "scpmVersionFromQuery":I
    if-nez v3, :cond_0

    .line 235
    iget v4, p0, Lcom/android/server/wm/PolicyImpl;->mRetryNumber:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/server/wm/PolicyImpl;->mRetryNumber:I

    .line 236
    if-lez v4, :cond_0

    .line 237
    iget-object v4, p0, Lcom/android/server/wm/PolicyImpl;->mController:Lcom/android/server/wm/PackageConfigurationController;

    iget-object v5, p0, Lcom/android/server/wm/PolicyImpl;->mName:Ljava/lang/String;

    const-wide/32 v6, 0x927c0

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/server/wm/PackageConfigurationController;->scheduleUpdatePolicyItem(Ljava/lang/String;J)V

    .line 238
    return v0

    .line 242
    :cond_0
    if-le v3, v2, :cond_6

    if-le v3, v1, :cond_6

    .line 243
    iget-object v4, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    .line 244
    .local v4, "size":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 245
    .local v5, "selections":Ljava/lang/StringBuilder;
    new-array v12, v4, [Ljava/lang/String;

    .line 246
    .local v12, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v4, :cond_2

    .line 247
    if-lez v6, :cond_1

    .line 248
    const-string v7, " or "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    :cond_1
    const-string v7, "data1=?"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    iget-object v7, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/wm/PackageConfiguration;

    iget-object v7, v7, Lcom/android/server/wm/PackageConfiguration;->mName:Ljava/lang/String;

    aput-object v7, v12, v6

    .line 246
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 253
    .end local v6    # "i":I
    :cond_2
    sget-object v6, Lcom/android/server/wm/PolicyImpl;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v7, "policy_item"

    invoke-static {v6, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/wm/PolicyImpl;->mName:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 254
    .local v13, "policyItemUri":Landroid/net/Uri;
    iget-object v6, p0, Lcom/android/server/wm/PolicyImpl;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v8, 0x0

    .line 255
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    move-object v7, v13

    move-object v10, v12

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 256
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_5

    .line 257
    :try_start_0
    iget-object v7, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    new-instance v8, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda1;

    invoke-direct {v8}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-interface {v7, v8}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 261
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 262
    const/4 v7, 0x2

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 263
    .local v7, "packageName":Ljava/lang/String;
    const/4 v8, 0x3

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 264
    .local v8, "key":Ljava/lang/String;
    const/4 v9, 0x4

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 265
    .local v9, "value":Ljava/lang/String;
    iget-object v10, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    new-instance v11, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda2;

    invoke-direct {v11, v8, v7, v9}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda2;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v10, v11}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 271
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v8    # "key":Ljava/lang/String;
    .end local v9    # "value":Ljava/lang/String;
    goto :goto_1

    .line 272
    :cond_3
    iget-object v7, p0, Lcom/android/server/wm/PolicyImpl;->mList:Ljava/util/List;

    new-instance v8, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda3;

    invoke-direct {v8}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda3;-><init>()V

    invoke-interface {v7, v8}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 276
    iget-object v7, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    invoke-virtual {v7, v3}, Lcom/android/server/wm/PolicyItem;->setScpmVersion(I)V

    .line 277
    iget-object v7, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "(SCPM)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/server/wm/PolicyItem;->setCurrentVersion(Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x1

    .line 279
    invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 254
    :catchall_0
    move-exception v7

    if-eqz v6, :cond_4

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v8

    invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :cond_4
    :goto_2
    throw v7

    .line 281
    :cond_5
    :goto_3
    if-eqz v6, :cond_6

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 283
    .end local v4    # "size":I
    .end local v5    # "selections":Ljava/lang/StringBuilder;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v12    # "selectionArgs":[Ljava/lang/String;
    .end local v13    # "policyItemUri":Landroid/net/Uri;
    :cond_6
    return v0
.end method

.method updatePolicyItem(Z)V
    .locals 2
    .param p1, "forced"    # Z

    .line 287
    sget-object v0, Lcom/android/server/wm/PolicyImpl;->CALLBACKS:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    return-void

    .line 291
    :cond_0
    if-eqz p1, :cond_1

    .line 292
    iget-object v0, p0, Lcom/android/server/wm/PolicyImpl;->mPolicyItem:Lcom/android/server/wm/PolicyItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/PolicyItem;->setScpmVersion(I)V

    .line 295
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->updatePackageConfigurationsIfNeeded()Z

    .line 296
    return-void
.end method
