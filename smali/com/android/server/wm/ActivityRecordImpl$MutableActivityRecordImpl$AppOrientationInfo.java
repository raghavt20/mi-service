class com.android.server.wm.ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo {
	 /* .source "ActivityRecordImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "AppOrientationInfo" */
} // .end annotation
/* # instance fields */
Integer mFlipScreenOrientationOuter;
Integer mScreenOrientationInner;
Integer mScreenOrientationOuter;
Integer mScreenOrientationPad;
/* # direct methods */
private com.android.server.wm.ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo ( ) {
/* .locals 1 */
/* .line 389 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 390 */
int v0 = -2; // const/4 v0, -0x2
/* iput v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationInner:I */
/* .line 391 */
/* iput v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationOuter:I */
/* .line 392 */
/* iput v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mScreenOrientationPad:I */
/* .line 393 */
/* iput v0, p0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;->mFlipScreenOrientationOuter:I */
return;
} // .end method
 com.android.server.wm.ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$AppOrientationInfo;-><init>()V */
return;
} // .end method
