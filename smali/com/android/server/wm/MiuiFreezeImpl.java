public class com.android.server.wm.MiuiFreezeImpl implements com.android.server.wm.MiuiFreezeStub {
	 /* .source "MiuiFreezeImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;, */
	 /* Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;, */
	 /* Lcom/android/server/wm/MiuiFreezeImpl$State; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String DELAY_REMOVE_SPLASH_PROP;
private static final Integer DELAY_REMOVE_SPLASH_TIME;
private static final Integer DIALOG_DELAY_IMAGE_REC_TIME;
private static final java.lang.String DIALOG_DELAY_SHOW_TIME_PROP;
private static final java.lang.String FILE_PATH;
private static final java.lang.String MEMFREEZE_ENABLE_DEBUG;
private static final java.lang.String MEMFREEZE_IMAGAE_UNKNOWN;
private static final java.lang.String PUBG_PACKAGE_NAME;
private static final java.lang.String PUBG_SPLASH_CLASS_NAME;
private static final Integer SPLASH_Z_ORDOR_LEVEL;
private static final java.lang.String TAG;
private static com.android.server.wm.MiuiFreezeImpl sInstance;
/* # instance fields */
private java.util.concurrent.atomic.AtomicBoolean atomicImageRecEnd;
private com.android.server.wm.MiuiFreezeImpl$DialogShowListener dialogShowListener;
private Boolean isFreeForm;
private android.os.Handler mAnimationHandler;
private com.android.server.wm.ActivityTaskManagerService mAtms;
private android.os.Handler mBgHandler;
private Integer mCurPid;
private Integer mCurUid;
private volatile Boolean mDialogShowing;
private android.os.Handler mIoHandler;
private com.android.server.wm.MiuiLoadingDialog mLoadingDialog;
private java.lang.String mPackageName;
private android.content.BroadcastReceiver mReceiver;
private java.lang.Runnable mRecogImageRunnable;
private java.lang.Runnable mRecogTimeoutRunnable;
private java.lang.Runnable mRemoveStartingRunnable;
private com.android.server.wm.MiuiRecognizeScene mScene;
private final com.android.server.wm.MiuiFreezeImpl$ShowDialogRunnable mShowDialogRunnable;
private volatile com.android.server.wm.MiuiFreezeImpl$State mState;
private android.content.Context mUiContext;
private android.os.Handler mUiHandler;
private com.android.server.wm.MiuiFreeFormManagerService mffms;
/* # direct methods */
public static void $r8$lambda$5mQuvweHICGoYvvJqfJLiwl_exs ( com.android.server.wm.MiuiFreezeImpl p0, com.android.server.wm.MiuiFreezeImpl$State p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$writeFile$7(Lcom/android/server/wm/MiuiFreezeImpl$State;)V */
	 return;
} // .end method
public static void $r8$lambda$6S5YNi2her4Axe9EnomkzgvWiz0 ( com.android.server.wm.MiuiFreezeImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$new$2()V */
	 return;
} // .end method
public static void $r8$lambda$AqzeD0GTjQr_4zPP1eUtxx6IEN8 ( com.android.server.wm.MiuiFreezeImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$unRegisterReceiver$6()V */
	 return;
} // .end method
public static void $r8$lambda$b13oYu0d3gv6oskzAWDgipcYBW0 ( com.android.server.wm.MiuiFreezeImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$dismissDialog$4()V */
	 return;
} // .end method
public static void $r8$lambda$e8ohJtqrEFwHXfeuBirRcUVGSI4 ( com.android.server.wm.MiuiFreezeImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$registerReceiver$5()V */
	 return;
} // .end method
public static void $r8$lambda$lFvHWxzUph6kR3mgLj5ulIW_PuQ ( com.android.server.wm.MiuiFreezeImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$new$0()V */
	 return;
} // .end method
public static void $r8$lambda$oSF8cYjK2HfRevYalq1k0aG0I5o ( com.android.server.wm.MiuiFreezeImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$new$1()V */
	 return;
} // .end method
public static void $r8$lambda$rVFNB-XP3L9tBenDMv3Ix7nYqPQ ( com.android.server.wm.MiuiFreezeImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->lambda$init$3()V */
	 return;
} // .end method
static com.android.server.wm.MiuiFreezeImpl$DialogShowListener -$$Nest$fgetdialogShowListener ( com.android.server.wm.MiuiFreezeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.dialogShowListener;
} // .end method
static Integer -$$Nest$fgetmCurUid ( com.android.server.wm.MiuiFreezeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I */
} // .end method
static com.android.server.wm.MiuiLoadingDialog -$$Nest$fgetmLoadingDialog ( com.android.server.wm.MiuiFreezeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mLoadingDialog;
} // .end method
static java.lang.String -$$Nest$fgetmPackageName ( com.android.server.wm.MiuiFreezeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPackageName;
} // .end method
static android.content.BroadcastReceiver -$$Nest$fgetmReceiver ( com.android.server.wm.MiuiFreezeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mReceiver;
} // .end method
static android.content.Context -$$Nest$fgetmUiContext ( com.android.server.wm.MiuiFreezeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mUiContext;
} // .end method
static void -$$Nest$fputmDialogShowing ( com.android.server.wm.MiuiFreezeImpl p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mDialogShowing:Z */
	 return;
} // .end method
static void -$$Nest$fputmLoadingDialog ( com.android.server.wm.MiuiFreezeImpl p0, com.android.server.wm.MiuiLoadingDialog p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mLoadingDialog = p1;
	 return;
} // .end method
static void -$$Nest$mdismissDialog ( com.android.server.wm.MiuiFreezeImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->dismissDialog()V */
	 return;
} // .end method
static com.android.server.wm.MiuiFreezeImpl ( ) {
	 /* .locals 2 */
	 /* .line 68 */
	 final String v0 = "persist.sys.memfreeze.dialog.delay.time"; // const-string v0, "persist.sys.memfreeze.dialog.delay.time"
	 /* const/16 v1, 0x3e8 */
	 v0 = 	 android.os.SystemProperties .getInt ( v0,v1 );
	 /* .line 70 */
	 final String v0 = "persist.miui.freeze.time"; // const-string v0, "persist.miui.freeze.time"
	 /* const/16 v1, 0x1f40 */
	 v0 = 	 android.os.SystemProperties .getInt ( v0,v1 );
	 /* .line 72 */
	 final String v0 = "persist.sys.memfreeze.debug"; // const-string v0, "persist.sys.memfreeze.debug"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.wm.MiuiFreezeImpl.DEBUG = (v0!= 0);
	 /* .line 76 */
	 int v0 = 0; // const/4 v0, 0x0
	 return;
} // .end method
public com.android.server.wm.MiuiFreezeImpl ( ) {
	 /* .locals 2 */
	 /* .line 138 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 74 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mffms = v0;
	 /* .line 86 */
	 this.mAtms = v0;
	 /* .line 94 */
	 /* new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable; */
	 /* invoke-direct {v1, p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable-IA;)V */
	 this.mShowDialogRunnable = v1;
	 /* .line 96 */
	 /* new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener; */
	 /* invoke-direct {v1, p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener-IA;)V */
	 this.dialogShowListener = v1;
	 /* .line 108 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->isFreeForm:Z */
	 /* .line 110 */
	 /* new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean; */
	 /* invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
	 this.atomicImageRecEnd = v1;
	 /* .line 114 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda5; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
	 this.mRemoveStartingRunnable = v0;
	 /* .line 119 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda6; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
	 this.mRecogImageRunnable = v0;
	 /* .line 133 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda7; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
	 this.mRecogTimeoutRunnable = v0;
	 /* .line 139 */
	 final String v0 = "MiuiFreezeImpl"; // const-string v0, "MiuiFreezeImpl"
	 final String v1 = "MiuiFreezeImpl is Initialized!"; // const-string v1, "MiuiFreezeImpl is Initialized!"
	 android.util.Slog .d ( v0,v1 );
	 /* .line 140 */
	 v0 = com.android.server.wm.MiuiFreezeImpl.sInstance;
	 /* if-nez v0, :cond_0 */
	 /* .line 141 */
	 /* .line 143 */
} // :cond_0
return;
} // .end method
private void createFile ( java.io.File p0 ) {
/* .locals 6 */
/* .param p1, "file" # Ljava/io/File; */
/* .line 432 */
final String v0 = "MiuiFreezeImpl"; // const-string v0, "MiuiFreezeImpl"
try { // :try_start_0
	 (( java.io.File ) p1 ).getParentFile ( ); // invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;
	 /* .line 433 */
	 /* .local v1, "parent":Ljava/io/File; */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 v2 = 		 (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
		 /* if-nez v2, :cond_1 */
		 /* .line 434 */
		 final String v2 = "parent not exists"; // const-string v2, "parent not exists"
		 android.util.Log .e ( v0,v2 );
		 /* .line 435 */
		 (( java.io.File ) v1 ).mkdir ( ); // invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
		 /* .line 436 */
		 (( java.io.File ) p1 ).createNewFile ( ); // invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z
		 /* .line 437 */
		 /* const/16 v2, 0x1fd */
		 /* .line 438 */
		 /* .local v2, "perms":I */
		 (( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
		 int v4 = -1; // const/4 v4, -0x1
		 android.os.FileUtils .setPermissions ( v3,v2,v4,v4 );
		 /* .line 439 */
		 (( java.io.File ) p1 ).getAbsolutePath ( ); // invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
		 android.os.FileUtils .setPermissions ( v3,v2,v4,v4 );
		 /* .line 440 */
		 (( java.io.File ) p1 ).getAbsolutePath ( ); // invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
		 android.os.SELinux .fileSelabelLookup ( v3 );
		 /* .line 441 */
		 /* .local v3, "ctx":Ljava/lang/String; */
		 /* if-nez v3, :cond_0 */
		 /* .line 442 */
		 /* new-instance v4, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v5 = "Failed to get SELinux context for "; // const-string v5, "Failed to get SELinux context for "
		 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.io.File ) p1 ).getAbsolutePath ( ); // invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .wtf ( v0,v4 );
		 /* .line 444 */
	 } // :cond_0
	 (( java.io.File ) p1 ).getAbsolutePath ( ); // invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
	 v4 = 	 android.os.SELinux .setFileContext ( v4,v3 );
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 /* .line 445 */
		 final String v4 = "Failed to set SELinux context"; // const-string v4, "Failed to set SELinux context"
		 android.util.Slog .wtf ( v0,v4 );
		 /* :try_end_0 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 450 */
	 } // .end local v1 # "parent":Ljava/io/File;
} // .end local v2 # "perms":I
} // .end local v3 # "ctx":Ljava/lang/String;
} // :cond_1
/* .line 448 */
/* :catch_0 */
/* move-exception v1 */
/* .line 449 */
/* .local v1, "e":Ljava/io/IOException; */
/* const-string/jumbo v2, "writeFile packageName: " */
android.util.Log .e ( v0,v2,v1 );
/* .line 451 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private void dismissDialog ( ) {
/* .locals 2 */
/* .line 335 */
v0 = this.mUiHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 350 */
return;
} // .end method
public static com.android.server.wm.MiuiFreezeImpl getImpl ( ) {
/* .locals 1 */
/* .line 146 */
v0 = com.android.server.wm.MiuiFreezeImpl.sInstance;
} // .end method
private java.lang.String getTopFocusTaskPackageName ( ) {
/* .locals 2 */
/* .line 227 */
v0 = this.mAtms;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 228 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
/* if-nez v0, :cond_0 */
final String v1 = ""; // const-string v1, ""
/* .line 229 */
} // :cond_0
(( com.android.server.wm.Task ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;
} // .end method
private void hideDialogIfNeed ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 320 */
v0 = (( com.android.server.wm.MiuiFreezeImpl ) p0 ).needShowLoading ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->needShowLoading(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 321 */
return;
/* .line 325 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->dismissDialog()V */
/* .line 328 */
v0 = com.android.server.wm.MiuiFreezeImpl$State.DISABLE;
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->writeFile(Lcom/android/server/wm/MiuiFreezeImpl$State;)V */
/* .line 331 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->unRegisterReceiver()V */
/* .line 332 */
return;
} // .end method
private Boolean isPropEnable ( ) {
/* .locals 1 */
/* .line 150 */
v0 = com.android.server.am.MemoryFreezeStub .getInstance ( );
} // .end method
private Boolean isPubgSplash ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 4 */
/* .param p1, "activity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 381 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
v1 = this.mActivityComponent;
/* if-nez v1, :cond_0 */
/* .line 384 */
} // :cond_0
v1 = this.mActivityComponent;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 385 */
/* .local v1, "packageName":Ljava/lang/String; */
v2 = this.mActivityComponent;
(( android.content.ComponentName ) v2 ).getClassName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 386 */
/* .local v2, "className":Ljava/lang/String; */
final String v3 = "com.tencent.tmgp.pubgmhd"; // const-string v3, "com.tencent.tmgp.pubgmhd"
v3 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
final String v3 = "com.epicgames.ue4.SplashActivity"; // const-string v3, "com.epicgames.ue4.SplashActivity"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* .line 382 */
} // .end local v1 # "packageName":Ljava/lang/String;
} // .end local v2 # "className":Ljava/lang/String;
} // :cond_2
} // :goto_0
} // .end method
private void lambda$dismissDialog$4 ( ) { //synthethic
/* .locals 2 */
/* .line 336 */
v0 = this.mLoadingDialog;
/* if-nez v0, :cond_0 */
/* .line 337 */
return;
/* .line 340 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mDialogShowing:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 341 */
v0 = this.mLoadingDialog;
(( com.android.server.wm.MiuiLoadingDialog ) v0 ).dismiss ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiLoadingDialog;->dismiss()V
/* .line 342 */
int v0 = 0; // const/4 v0, 0x0
this.mLoadingDialog = v0;
/* .line 343 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mDialogShowing:Z */
/* .line 344 */
final String v0 = "MiuiFreezeImpl"; // const-string v0, "MiuiFreezeImpl"
/* const-string/jumbo v1, "start dismiss dialog" */
android.util.Log .d ( v0,v1 );
/* .line 347 */
} // :cond_1
v0 = this.dialogShowListener;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.wm.MiuiFreezeImpl$DialogShowListener ) v0 ).setNeedDismissDialog ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->setNeedDismissDialog(Z)V
/* .line 349 */
} // :goto_0
return;
} // .end method
private void lambda$init$3 ( ) { //synthethic
/* .locals 2 */
/* .line 168 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/memgreezer/notify_splash_dismiss_switch"; // const-string v1, "/data/system/memgreezer/notify_splash_dismiss_switch"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->createFile(Ljava/io/File;)V */
return;
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 2 */
/* .line 115 */
final String v0 = "MiuiFreezeImpl"; // const-string v0, "MiuiFreezeImpl"
final String v1 = "mRemoveStartingRunnable timeout"; // const-string v1, "mRemoveStartingRunnable timeout"
android.util.Slog .d ( v0,v1 );
/* .line 116 */
v0 = this.mPackageName;
/* iget v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I */
(( com.android.server.wm.MiuiFreezeImpl ) p0 ).finishAndRemoveSplashScreen ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/MiuiFreezeImpl;->finishAndRemoveSplashScreen(Ljava/lang/String;I)V
/* .line 117 */
return;
} // .end method
private void lambda$new$1 ( ) { //synthethic
/* .locals 4 */
/* .line 120 */
/* const-string/jumbo v0, "start image recognize " */
final String v1 = "MiuiFreezeImpl"; // const-string v1, "MiuiFreezeImpl"
android.util.Slog .d ( v1,v0 );
/* .line 121 */
/* const-string/jumbo v0, "unknown" */
/* .line 124 */
/* .local v0, "result":Ljava/lang/String; */
try { // :try_start_0
v2 = this.mScene;
v3 = this.mPackageName;
(( com.android.server.wm.MiuiRecognizeScene ) v2 ).recognizeScene ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiRecognizeScene;->recognizeScene(Ljava/lang/String;)Ljava/lang/String;
/* move-object v0, v2 */
/* .line 125 */
/* sget-boolean v2, Lcom/android/server/wm/MiuiFreezeImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "image recog result is "; // const-string v3, "image recog result is "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 128 */
} // :cond_0
/* .line 126 */
/* :catch_0 */
/* move-exception v2 */
/* .line 127 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "image recog exception "; // const-string v3, "image recog exception "
android.util.Slog .e ( v1,v3 );
/* .line 130 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->processRecgResult(Ljava/lang/String;)V */
/* .line 131 */
return;
} // .end method
private void lambda$new$2 ( ) { //synthethic
/* .locals 1 */
/* .line 135 */
/* const-string/jumbo v0, "unknown" */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->processRecgResult(Ljava/lang/String;)V */
/* .line 136 */
return;
} // .end method
private void lambda$registerReceiver$5 ( ) { //synthethic
/* .locals 8 */
/* .line 391 */
v0 = this.mReceiver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 392 */
return;
/* .line 394 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/wm/MiuiFreezeImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 395 */
final String v0 = "MiuiFreezeImpl"; // const-string v0, "MiuiFreezeImpl"
/* const-string/jumbo v1, "start register broadcast" */
android.util.Log .d ( v0,v1 );
/* .line 397 */
} // :cond_1
/* new-instance v3, Lcom/android/server/wm/MiuiFreezeImpl$1; */
/* invoke-direct {v3, p0}, Lcom/android/server/wm/MiuiFreezeImpl$1;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
this.mReceiver = v3;
/* .line 406 */
v2 = this.mUiContext;
/* new-instance v4, Landroid/content/IntentFilter; */
final String v0 = "android.intent.action.CLOSE_SYSTEM_DIALOGS"; // const-string v0, "android.intent.action.CLOSE_SYSTEM_DIALOGS"
/* invoke-direct {v4, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
int v5 = 0; // const/4 v5, 0x0
v6 = this.mUiHandler;
int v7 = 2; // const/4 v7, 0x2
/* invoke-virtual/range {v2 ..v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent; */
/* .line 409 */
return;
} // .end method
private void lambda$unRegisterReceiver$6 ( ) { //synthethic
/* .locals 4 */
/* .line 414 */
v0 = this.mReceiver;
/* if-nez v0, :cond_0 */
/* .line 415 */
return;
/* .line 417 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/wm/MiuiFreezeImpl;->DEBUG:Z */
final String v1 = "MiuiFreezeImpl"; // const-string v1, "MiuiFreezeImpl"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 418 */
/* const-string/jumbo v0, "start unRegister broadcast" */
android.util.Log .d ( v1,v0 );
/* .line 421 */
} // :cond_1
try { // :try_start_0
v0 = this.mUiContext;
v2 = this.mReceiver;
(( android.content.Context ) v0 ).unregisterReceiver ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 425 */
/* .line 422 */
/* :catch_0 */
/* move-exception v0 */
/* .line 424 */
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "unregisterReceiver threw exception: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.IllegalArgumentException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 426 */
} // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
this.mReceiver = v0;
/* .line 427 */
return;
} // .end method
private void lambda$writeFile$7 ( com.android.server.wm.MiuiFreezeImpl$State p0 ) { //synthethic
/* .locals 6 */
/* .param p1, "state" # Lcom/android/server/wm/MiuiFreezeImpl$State; */
/* .line 461 */
/* const-string/jumbo v0, "writeFile close: " */
final String v1 = "MiuiFreezeImpl"; // const-string v1, "MiuiFreezeImpl"
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/system/memgreezer/notify_splash_dismiss_switch"; // const-string v3, "/data/system/memgreezer/notify_splash_dismiss_switch"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 462 */
/* .local v2, "file":Ljava/io/File; */
int v3 = 0; // const/4 v3, 0x0
/* .line 464 */
/* .local v3, "fileWriter":Ljava/io/FileWriter; */
try { // :try_start_0
/* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiFreezeImpl;->createFile(Ljava/io/File;)V */
/* .line 465 */
/* new-instance v4, Ljava/io/FileWriter; */
/* invoke-direct {v4, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V */
/* move-object v3, v4 */
/* .line 466 */
v4 = com.android.server.wm.MiuiFreezeImpl$State.ENABLE;
/* if-ne p1, v4, :cond_0 */
final String v4 = "1"; // const-string v4, "1"
} // :cond_0
final String v4 = "0"; // const-string v4, "0"
} // :goto_0
(( java.io.FileWriter ) v3 ).write ( v4 ); // invoke-virtual {v3, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
/* .line 467 */
(( java.io.FileWriter ) v3 ).flush ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->flush()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 472 */
/* nop */
/* .line 473 */
try { // :try_start_1
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 477 */
} // :cond_1
} // :goto_1
/* .line 475 */
/* :catch_0 */
/* move-exception v4 */
/* .line 476 */
/* .local v4, "e":Ljava/io/IOException; */
android.util.Log .e ( v1,v0,v4 );
/* .line 478 */
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 471 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 468 */
/* :catch_1 */
/* move-exception v4 */
/* .line 469 */
/* .restart local v4 # "e":Ljava/io/IOException; */
try { // :try_start_2
/* const-string/jumbo v5, "writeFile packageName: " */
android.util.Log .e ( v1,v5,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 472 */
} // .end local v4 # "e":Ljava/io/IOException;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 473 */
try { // :try_start_3
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 479 */
} // :goto_2
return;
/* .line 472 */
} // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 473 */
try { // :try_start_4
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 475 */
/* :catch_2 */
/* move-exception v5 */
/* .line 476 */
/* .local v5, "e":Ljava/io/IOException; */
android.util.Log .e ( v1,v0,v5 );
/* .line 477 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_4
/* nop */
/* .line 478 */
} // :goto_5
/* throw v4 */
} // .end method
private void postRemoveSplashRunnable ( java.lang.String p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 253 */
v0 = this.mAtms;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 254 */
try { // :try_start_0
v1 = this.mAtms;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getProcessController ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;
/* .line 255 */
/* .local v1, "app":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = (( com.android.server.wm.WindowProcessController ) v1 ).hasThread ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->hasThread()Z
/* if-nez v2, :cond_0 */
/* .line 258 */
} // :cond_0
(( com.android.server.wm.WindowProcessController ) v1 ).getActivities ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getActivities()Ljava/util/List;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/wm/ActivityRecord; */
/* .line 259 */
/* .local v3, "record":Lcom/android/server/wm/ActivityRecord; */
v4 = this.mAnimationHandler;
v5 = this.mRemoveSplashRunnable;
(( android.os.Handler ) v4 ).post ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 260 */
/* nop */
} // .end local v3 # "record":Lcom/android/server/wm/ActivityRecord;
/* .line 261 */
} // .end local v1 # "app":Lcom/android/server/wm/WindowProcessController;
} // :cond_1
/* monitor-exit v0 */
/* .line 262 */
return;
/* .line 256 */
/* .restart local v1 # "app":Lcom/android/server/wm/WindowProcessController; */
} // :cond_2
} // :goto_1
/* monitor-exit v0 */
return;
/* .line 261 */
} // .end local v1 # "app":Lcom/android/server/wm/WindowProcessController;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void processRecgResult ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "result" # Ljava/lang/String; */
/* .line 213 */
v0 = this.atomicImageRecEnd;
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).compareAndSet ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z
/* if-nez v0, :cond_0 */
/* .line 214 */
return;
/* .line 217 */
} // :cond_0
/* const-string/jumbo v0, "unknown" */
v0 = android.text.TextUtils .equals ( p1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 218 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->getTopFocusTaskPackageName()Ljava/lang/String; */
v1 = this.mPackageName;
v0 = android.text.TextUtils .equals ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 219 */
v0 = this.mUiHandler;
v1 = this.mShowDialogRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 220 */
return;
/* .line 223 */
} // :cond_1
v0 = this.mPackageName;
/* iget v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I */
(( com.android.server.wm.MiuiFreezeImpl ) p0 ).finishAndRemoveSplashScreen ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/MiuiFreezeImpl;->finishAndRemoveSplashScreen(Ljava/lang/String;I)V
/* .line 224 */
return;
} // .end method
private void registerReceiver ( ) {
/* .locals 2 */
/* .line 390 */
v0 = this.mUiHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 410 */
return;
} // .end method
private void removeTimeRunnable ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 240 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiRecognizeScene;->REC_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 242 */
v0 = this.mBgHandler;
v1 = this.mRecogImageRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 244 */
v0 = this.mUiHandler;
v1 = this.mRecogTimeoutRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 247 */
} // :cond_0
v0 = this.mUiHandler;
v1 = this.mShowDialogRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 249 */
v0 = this.mAnimationHandler;
v1 = this.mRemoveStartingRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 250 */
return;
} // .end method
private void unRegisterReceiver ( ) {
/* .locals 2 */
/* .line 413 */
v0 = this.mUiHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 428 */
return;
} // .end method
private void writeFile ( com.android.server.wm.MiuiFreezeImpl$State p0 ) {
/* .locals 2 */
/* .param p1, "state" # Lcom/android/server/wm/MiuiFreezeImpl$State; */
/* .line 455 */
v0 = this.mState;
/* if-ne v0, p1, :cond_0 */
/* .line 456 */
return;
/* .line 458 */
} // :cond_0
this.mState = p1;
/* .line 459 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "write file " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mState;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreezeImpl"; // const-string v1, "MiuiFreezeImpl"
android.util.Log .d ( v1,v0 );
/* .line 460 */
v0 = this.mIoHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiFreezeImpl$State;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 480 */
return;
} // .end method
/* # virtual methods */
public void checkFreeForm ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "taskId" # I */
/* .line 233 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->isPropEnable()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 234 */
} // :cond_0
v0 = com.android.server.am.MemoryFreezeStub .getInstance ( );
/* if-nez v0, :cond_1 */
return;
/* .line 235 */
} // :cond_1
v0 = this.mffms;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mFreeFormActivityStacks;
/* if-nez v0, :cond_2 */
/* .line 236 */
} // :cond_2
v0 = this.mffms;
v0 = this.mFreeFormActivityStacks;
java.lang.Integer .valueOf ( p2 );
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->isFreeForm:Z */
/* .line 237 */
return;
/* .line 235 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void finishAndRemoveSplashScreen ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 191 */
v0 = (( com.android.server.wm.MiuiFreezeImpl ) p0 ).needShowLoading ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->needShowLoading(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 192 */
return;
/* .line 194 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I */
/* if-eq v0, p2, :cond_1 */
/* .line 195 */
return;
/* .line 197 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "finishAndRemoveSplashScreen packageName: "; // const-string v1, "finishAndRemoveSplashScreen packageName: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreezeImpl"; // const-string v1, "MiuiFreezeImpl"
android.util.Log .d ( v1,v0 );
/* .line 198 */
final String v0 = "MF#finishAndRemoveSplashScreen"; // const-string v0, "MF#finishAndRemoveSplashScreen"
/* const-wide/16 v1, 0x20 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 201 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreezeImpl;->removeTimeRunnable(Ljava/lang/String;I)V */
/* .line 204 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->hideDialogIfNeed(Ljava/lang/String;)V */
/* .line 207 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreezeImpl;->postRemoveSplashRunnable(Ljava/lang/String;I)V */
/* .line 209 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 210 */
return;
} // .end method
public void init ( com.android.server.wm.ActivityTaskManagerService p0 ) {
/* .locals 2 */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 159 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->isPropEnable()Z */
/* if-nez v0, :cond_0 */
return;
/* .line 160 */
} // :cond_0
this.mAtms = p1;
/* .line 161 */
/* new-instance v0, Landroid/os/Handler; */
com.android.server.AnimationThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mAnimationHandler = v0;
/* .line 162 */
/* new-instance v0, Landroid/os/Handler; */
com.android.server.IoThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mIoHandler = v0;
/* .line 163 */
/* new-instance v0, Landroid/os/Handler; */
com.android.server.UiThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mUiHandler = v0;
/* .line 164 */
/* new-instance v0, Landroid/os/Handler; */
com.android.server.MiuiBgThread .get ( );
(( com.android.server.MiuiBgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mBgHandler = v0;
/* .line 165 */
v0 = this.mUiContext;
this.mUiContext = v0;
/* .line 166 */
/* new-instance v0, Lcom/android/server/wm/MiuiRecognizeScene; */
v1 = this.mUiContext;
/* invoke-direct {v0, v1}, Lcom/android/server/wm/MiuiRecognizeScene;-><init>(Landroid/content/Context;)V */
this.mScene = v0;
/* .line 168 */
v0 = this.mIoHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreezeImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 169 */
v0 = this.mMiuiFreeFormManagerService;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService; */
this.mffms = v0;
/* .line 170 */
return;
} // .end method
public Boolean isSameProcess ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "newPid" # I */
/* .line 353 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurPid:I */
/* if-ne v0, p1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 354 */
/* .local v0, "result":Z */
} // :goto_0
/* if-nez v0, :cond_1 */
/* .line 355 */
final String v1 = "MiuiFreezeImpl"; // const-string v1, "MiuiFreezeImpl"
final String v2 = "isSameProcess process has changed"; // const-string v2, "isSameProcess process has changed"
android.util.Log .i ( v1,v2 );
/* .line 357 */
} // :cond_1
} // .end method
public Boolean needShowLoading ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 179 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->isPropEnable()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl;->isFreeForm:Z */
/* if-nez v0, :cond_0 */
/* .line 180 */
v0 = com.android.server.am.MemoryFreezeStub .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 179 */
} // :goto_0
} // .end method
public void reparentPubgGame ( com.android.server.wm.ActivityRecord p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "from" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "isProcessExists" # Z */
/* .line 369 */
final String v0 = "MiuiFreezeImpl"; // const-string v0, "MiuiFreezeImpl"
if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez p2, :cond_0 */
/* .line 373 */
} // :cond_0
v1 = this.packageName;
v1 = (( com.android.server.wm.MiuiFreezeImpl ) p0 ).needShowLoading ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreezeImpl;->needShowLoading(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->isPubgSplash(Lcom/android/server/wm/ActivityRecord;)Z */
/* if-nez v1, :cond_1 */
/* .line 377 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p1, Lcom/android/server/wm/ActivityRecord;->needSkipAssignLayer:Z */
/* .line 378 */
return;
/* .line 374 */
} // :cond_2
} // :goto_0
final String v1 = "reparentPubgGame not need show or not from pubg splash, return"; // const-string v1, "reparentPubgGame not need show or not from pubg splash, return"
android.util.Log .i ( v0,v1 );
/* .line 375 */
return;
/* .line 370 */
} // :cond_3
} // :goto_1
final String v1 = "reparentPubgGame AR is null or process not exist, return"; // const-string v1, "reparentPubgGame AR is null or process not exist, return"
android.util.Log .i ( v0,v1 );
/* .line 371 */
return;
} // .end method
public void reportAppDied ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 310 */
/* if-nez p1, :cond_0 */
return;
/* .line 311 */
} // :cond_0
v0 = this.processName;
v1 = this.mPackageName;
v0 = android.text.TextUtils .equals ( v0,v1 );
/* if-nez v0, :cond_1 */
return;
/* .line 312 */
} // :cond_1
final String v0 = "MiuiFreezeImpl"; // const-string v0, "MiuiFreezeImpl"
final String v1 = "reportAppDied start hide dialog when process die"; // const-string v1, "reportAppDied start hide dialog when process die"
android.util.Log .i ( v0,v1 );
/* .line 313 */
v0 = this.mPackageName;
/* iget v1, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I */
(( com.android.server.wm.MiuiFreezeImpl ) p0 ).finishAndRemoveSplashScreen ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/MiuiFreezeImpl;->finishAndRemoveSplashScreen(Ljava/lang/String;I)V
/* .line 314 */
return;
} // .end method
public void showDialogIfNeed ( java.lang.String p0, com.android.server.wm.ActivityRecord p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "uid" # I */
/* .line 271 */
v0 = (( com.android.server.wm.MiuiFreezeImpl ) p0 ).needShowLoading ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl;->needShowLoading(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 272 */
return;
/* .line 275 */
} // :cond_0
v0 = com.android.server.wm.MiuiFreezeImpl$State.ENABLE;
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl;->writeFile(Lcom/android/server/wm/MiuiFreezeImpl$State;)V */
/* .line 279 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreezeImpl;->registerReceiver()V */
/* .line 282 */
v0 = this.mAnimationHandler;
v1 = this.mRemoveStartingRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 283 */
v0 = this.mAnimationHandler;
v1 = this.mRemoveStartingRunnable;
/* int-to-long v2, v2 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 286 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiRecognizeScene;->REC_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 287 */
v0 = this.mBgHandler;
v1 = this.mRecogImageRunnable;
/* int-to-long v3, v2 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 289 */
v0 = this.atomicImageRecEnd;
int v1 = 0; // const/4 v1, 0x0
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 290 */
v0 = this.mUiHandler;
v1 = this.mRecogTimeoutRunnable;
/* mul-int/lit8 v2, v2, 0x2 */
/* int-to-long v2, v2 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 293 */
} // :cond_1
v0 = this.mUiHandler;
v1 = this.mShowDialogRunnable;
/* int-to-long v2, v2 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 296 */
} // :goto_0
this.mPackageName = p1;
/* .line 298 */
/* iput p3, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurUid:I */
/* .line 301 */
v0 = this.mAtms;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 302 */
try { // :try_start_0
v1 = this.mAtms;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getProcessController ( p1, p3 ); // invoke-virtual {v1, p1, p3}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;
/* .line 303 */
/* .local v1, "callerApp":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 304 */
v2 = (( com.android.server.wm.WindowProcessController ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* iput v2, p0, Lcom/android/server/wm/MiuiFreezeImpl;->mCurPid:I */
/* .line 306 */
} // .end local v1 # "callerApp":Lcom/android/server/wm/WindowProcessController;
} // :cond_2
/* monitor-exit v0 */
/* .line 307 */
return;
/* .line 306 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
