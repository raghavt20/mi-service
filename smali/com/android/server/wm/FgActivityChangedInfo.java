public class com.android.server.wm.FgActivityChangedInfo {
	 /* .source "FgActivityChangedInfo.java" */
	 /* # instance fields */
	 final android.content.pm.ApplicationInfo multiWindowAppInfo;
	 final Integer pid;
	 final com.android.server.wm.ActivityRecord record;
	 final com.android.server.wm.ActivityRecord$State state;
	 /* # direct methods */
	 public com.android.server.wm.FgActivityChangedInfo ( ) {
		 /* .locals 0 */
		 /* .param p1, "_record" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p2, "state" # Lcom/android/server/wm/ActivityRecord$State; */
		 /* .param p3, "pid" # I */
		 /* .param p4, "_info" # Landroid/content/pm/ApplicationInfo; */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 14 */
		 this.record = p1;
		 /* .line 15 */
		 this.state = p2;
		 /* .line 16 */
		 /* iput p3, p0, Lcom/android/server/wm/FgActivityChangedInfo;->pid:I */
		 /* .line 17 */
		 this.multiWindowAppInfo = p4;
		 /* .line 18 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getPackageName ( ) {
		 /* .locals 1 */
		 /* .line 21 */
		 v0 = this.record;
		 v0 = this.packageName;
	 } // .end method
