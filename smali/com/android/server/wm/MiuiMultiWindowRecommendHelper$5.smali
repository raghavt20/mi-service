.class Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->displayConfigurationChange(Lcom/android/server/wm/DisplayContent;Landroid/content/res/Configuration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

.field final synthetic val$configuration:Landroid/content/res/Configuration;

.field final synthetic val$displayContent:Lcom/android/server/wm/DisplayContent;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayContent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 577
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iput-object p2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->val$configuration:Landroid/content/res/Configuration;

    iput-object p3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->val$displayContent:Lcom/android/server/wm/DisplayContent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "displayConfigurationChange: configuration= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->val$configuration:Landroid/content/res/Configuration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiWindowRecommendHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->val$displayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    return-void

    .line 584
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->val$configuration:Landroid/content/res/Configuration;

    invoke-static {v0}, Landroid/util/MiuiMultiWindowUtils;->isWideScreen(Landroid/content/res/Configuration;)Z

    move-result v0

    .line 585
    .local v0, "isWideScreen":Z
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fgetmLastIsWideScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z

    move-result v2

    if-eq v2, v0, :cond_1

    .line 586
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v2, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fputmLastIsWideScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Z)V

    .line 587
    if-nez v0, :cond_1

    .line 588
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v2, v2, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V

    .line 591
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fgetmLastConfiguration(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->val$configuration:Landroid/content/res/Configuration;

    invoke-virtual {v2, v3}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    move-result v2

    .line 592
    .local v2, "changes":I
    and-int/lit16 v3, v2, 0x80

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    .line 593
    .local v3, "orientationChange":Z
    :goto_0
    if-eqz v3, :cond_3

    .line 594
    const-string v4, "orientationChange"

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v1, v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeRecommendView()V

    .line 597
    :cond_3
    return-void
.end method
