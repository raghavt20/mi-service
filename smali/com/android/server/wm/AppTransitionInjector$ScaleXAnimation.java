class com.android.server.wm.AppTransitionInjector$ScaleXAnimation extends android.view.animation.Animation {
	 /* .source "AppTransitionInjector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AppTransitionInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ScaleXAnimation" */
} // .end annotation
/* # instance fields */
private Float mFromX;
private Float mPivotX;
private Float mToX;
/* # direct methods */
public com.android.server.wm.AppTransitionInjector$ScaleXAnimation ( ) {
/* .locals 1 */
/* .param p1, "fromX" # F */
/* .param p2, "toX" # F */
/* .line 1361 */
/* invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V */
/* .line 1362 */
/* iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mFromX:F */
/* .line 1363 */
/* iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mToX:F */
/* .line 1364 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mPivotX:F */
/* .line 1365 */
return;
} // .end method
public com.android.server.wm.AppTransitionInjector$ScaleXAnimation ( ) {
/* .locals 0 */
/* .param p1, "fromX" # F */
/* .param p2, "toX" # F */
/* .param p3, "pivotX" # F */
/* .line 1367 */
/* invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V */
/* .line 1368 */
/* iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mFromX:F */
/* .line 1369 */
/* iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mToX:F */
/* .line 1370 */
/* iput p3, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mPivotX:F */
/* .line 1371 */
return;
} // .end method
/* # virtual methods */
protected void applyTransformation ( Float p0, android.view.animation.Transformation p1 ) {
/* .locals 6 */
/* .param p1, "interpolatedTime" # F */
/* .param p2, "t" # Landroid/view/animation/Transformation; */
/* .line 1375 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 1376 */
/* .local v0, "sx":F */
v1 = (( com.android.server.wm.AppTransitionInjector$ScaleXAnimation ) p0 ).getScaleFactor ( ); // invoke-virtual {p0}, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->getScaleFactor()F
/* .line 1378 */
/* .local v1, "scale":F */
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mFromX:F */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* cmpl-float v4, v2, v3 */
/* if-nez v4, :cond_0 */
/* iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mToX:F */
/* cmpl-float v4, v4, v3 */
if ( v4 != null) { // if-eqz v4, :cond_1
	 /* .line 1379 */
} // :cond_0
/* iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mToX:F */
/* sub-float/2addr v4, v2 */
/* mul-float/2addr v4, p1 */
/* add-float v0, v2, v4 */
/* .line 1382 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mPivotX:F */
int v4 = 0; // const/4 v4, 0x0
/* cmpl-float v2, v2, v4 */
/* if-nez v2, :cond_2 */
/* .line 1383 */
(( android.view.animation.Transformation ) p2 ).getMatrix ( ); // invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
(( android.graphics.Matrix ) v2 ).setScale ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Landroid/graphics/Matrix;->setScale(FF)V
/* .line 1385 */
} // :cond_2
(( android.view.animation.Transformation ) p2 ).getMatrix ( ); // invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
/* iget v5, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;->mPivotX:F */
/* mul-float/2addr v5, v1 */
(( android.graphics.Matrix ) v2 ).setScale ( v0, v3, v5, v4 ); // invoke-virtual {v2, v0, v3, v5, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V
/* .line 1387 */
} // :goto_0
return;
} // .end method
