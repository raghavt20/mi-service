.class Lcom/android/server/wm/MIUIWatermark$2;
.super Ljava/lang/Object;
.source "MIUIWatermark.java"

# interfaces
.implements Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MIUIWatermark;->init(Lcom/android/server/wm/WindowManagerService;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MIUIWatermark;

.field final synthetic val$msg:Ljava/lang/String;

.field final synthetic val$wms:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MIUIWatermark;Lcom/android/server/wm/WindowManagerService;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MIUIWatermark;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 303
    iput-object p1, p0, Lcom/android/server/wm/MIUIWatermark$2;->this$0:Lcom/android/server/wm/MIUIWatermark;

    iput-object p2, p0, Lcom/android/server/wm/MIUIWatermark$2;->val$wms:Lcom/android/server/wm/WindowManagerService;

    iput-object p3, p0, Lcom/android/server/wm/MIUIWatermark$2;->val$msg:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHideWatermark()V
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark$2;->this$0:Lcom/android/server/wm/MIUIWatermark;

    invoke-static {v0}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$mhideWaterMarker(Lcom/android/server/wm/MIUIWatermark;)V

    .line 318
    return-void
.end method

.method public onShowWatermark()V
    .locals 2

    .line 306
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark$2;->this$0:Lcom/android/server/wm/MIUIWatermark;

    invoke-static {v0}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$fgetmSurfaceControl(Lcom/android/server/wm/MIUIWatermark;)Landroid/view/SurfaceControl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark$2;->this$0:Lcom/android/server/wm/MIUIWatermark;

    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark$2;->val$wms:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$mupdateText(Lcom/android/server/wm/MIUIWatermark;Landroid/content/Context;)V

    .line 308
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark$2;->this$0:Lcom/android/server/wm/MIUIWatermark;

    invoke-static {v0}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$mshowWaterMarker(Lcom/android/server/wm/MIUIWatermark;)V

    .line 309
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark$2;->this$0:Lcom/android/server/wm/MIUIWatermark;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$fputmEnableMIUIWatermark(Lcom/android/server/wm/MIUIWatermark;Z)V

    .line 312
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark$2;->this$0:Lcom/android/server/wm/MIUIWatermark;

    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark$2;->val$msg:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$mcreateSurfaceLocked(Lcom/android/server/wm/MIUIWatermark;Ljava/lang/String;)V

    .line 313
    return-void
.end method
