.class Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;
.super Ljava/lang/Object;
.source "ActivityTaskManagerServiceImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->reportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

.field final synthetic val$multiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

.field final synthetic val$pid:I

.field final synthetic val$r:Lcom/android/server/wm/ActivityRecord;

.field final synthetic val$state:Lcom/android/server/wm/ActivityRecord$State;


# direct methods
.method constructor <init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;ILandroid/content/pm/ApplicationInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 409
    iput-object p1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    iput-object p2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$r:Lcom/android/server/wm/ActivityRecord;

    iput-object p3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$state:Lcom/android/server/wm/ActivityRecord$State;

    iput p4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$pid:I

    iput-object p5, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$multiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 412
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$r:Lcom/android/server/wm/ActivityRecord;

    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$state:Lcom/android/server/wm/ActivityRecord$State;

    iget v3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$pid:I

    iget-object v4, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;->val$multiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChanged(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;ILandroid/content/pm/ApplicationInfo;)V

    .line 413
    return-void
.end method
