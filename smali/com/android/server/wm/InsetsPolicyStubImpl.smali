.class public Lcom/android/server/wm/InsetsPolicyStubImpl;
.super Lcom/android/server/wm/InsetsPolicyStub;
.source "InsetsPolicyStubImpl.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/android/server/wm/InsetsPolicyStub;-><init>()V

    return-void
.end method


# virtual methods
.method public useFocusedWindowForStatusControl(Lcom/android/server/wm/WindowState;Z)Z
    .locals 3
    .param p1, "focusedWin"    # Lcom/android/server/wm/WindowState;
    .param p2, "forceShowsSystemBarsForWindowingMode"    # Z

    .line 13
    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 14
    return v0

    .line 16
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 17
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "control_center"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    nop

    .line 16
    :goto_0
    return v0
.end method
