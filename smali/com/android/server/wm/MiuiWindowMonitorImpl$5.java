class com.android.server.wm.MiuiWindowMonitorImpl$5 implements java.lang.Runnable {
	 /* .source "MiuiWindowMonitorImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiWindowMonitorImpl;->onProcessDiedLocked(ILcom/android/server/am/ProcessRecord;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiWindowMonitorImpl this$0; //synthetic
final com.android.server.wm.MiuiWindowMonitorImpl$ProcessKey val$key; //synthetic
final Integer val$pid; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiWindowMonitorImpl$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiWindowMonitorImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 324 */
this.this$0 = p1;
this.val$key = p2;
/* iput p3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->val$pid:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 327 */
v0 = this.this$0;
/* monitor-enter v0 */
/* .line 328 */
try { // :try_start_0
v1 = this.this$0;
v1 = com.android.server.wm.MiuiWindowMonitorImpl .-$$Nest$misEnabled ( v1 );
/* if-nez v1, :cond_0 */
/* .line 329 */
v1 = this.this$0;
v2 = this.val$key;
com.android.server.wm.MiuiWindowMonitorImpl .-$$Nest$mremoveWindowInfo ( v1,v2 );
/* .line 330 */
/* monitor-exit v0 */
return;
/* .line 332 */
} // :cond_0
v1 = this.this$0;
/* iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->val$pid:I */
com.android.server.wm.MiuiWindowMonitorImpl .-$$Nest$fgetmWindowsByApp ( v1 );
v4 = this.val$key;
(( java.util.HashMap ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
int v4 = 1; // const/4 v4, 0x1
com.android.server.wm.MiuiWindowMonitorImpl .-$$Nest$mreportIfNeeded ( v1,v2,v3,v4 );
/* .line 333 */
/* monitor-exit v0 */
/* .line 334 */
return;
/* .line 333 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
