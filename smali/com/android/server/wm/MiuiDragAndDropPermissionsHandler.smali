.class Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;
.super Lcom/android/server/wm/DragAndDropPermissionsHandler;
.source "MiuiDragAndDropPermissionsHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener;,
        Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener;
    }
.end annotation


# instance fields
.field private mOnPermissionReleaseListener:Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener;

.field private mOnPermissionTakeListener:Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener;

.field private mUris:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerGlobalLock;Landroid/content/ClipData;ILjava/lang/String;III)V
    .locals 1
    .param p1, "lock"    # Lcom/android/server/wm/WindowManagerGlobalLock;
    .param p2, "clipData"    # Landroid/content/ClipData;
    .param p3, "sourceUid"    # I
    .param p4, "targetPackage"    # Ljava/lang/String;
    .param p5, "mode"    # I
    .param p6, "sourceUserId"    # I
    .param p7, "targetUserId"    # I

    .line 18
    invoke-direct/range {p0 .. p7}, Lcom/android/server/wm/DragAndDropPermissionsHandler;-><init>(Lcom/android/server/wm/WindowManagerGlobalLock;Landroid/content/ClipData;ILjava/lang/String;III)V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;->mUris:Ljava/util/List;

    .line 20
    invoke-virtual {p2, v0}, Landroid/content/ClipData;->collectUris(Ljava/util/List;)V

    .line 21
    return-void
.end method


# virtual methods
.method public release()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 33
    invoke-super {p0}, Lcom/android/server/wm/DragAndDropPermissionsHandler;->release()V

    .line 34
    iget-object v0, p0, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;->mOnPermissionReleaseListener:Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener;

    if-eqz v0, :cond_0

    .line 35
    iget-object v1, p0, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;->mUris:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener;->onPermissionRelease(Ljava/util/List;)V

    .line 37
    :cond_0
    return-void
.end method

.method public setOnPermissionReleaseListener(Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener;

    .line 44
    iput-object p1, p0, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;->mOnPermissionReleaseListener:Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener;

    .line 45
    return-void
.end method

.method public setOnPermissionTakeListener(Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener;

    .line 40
    iput-object p1, p0, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;->mOnPermissionTakeListener:Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener;

    .line 41
    return-void
.end method

.method public take(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "activityToken"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 25
    invoke-super {p0, p1}, Lcom/android/server/wm/DragAndDropPermissionsHandler;->take(Landroid/os/IBinder;)V

    .line 26
    iget-object v0, p0, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;->mOnPermissionTakeListener:Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener;

    if-eqz v0, :cond_0

    .line 27
    iget-object v1, p0, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;->mUris:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener;->onPermissionTake(Ljava/util/List;)V

    .line 29
    :cond_0
    return-void
.end method
