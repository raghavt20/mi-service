.class public Lcom/android/server/wm/MiGameTypeIdentificationImpl;
.super Ljava/lang/Object;
.source "MiGameTypeIdentificationImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiGameTypeIdentificationStub;


# static fields
.field private static final JOYOSE_NAME:Ljava/lang/String; = "com.xiaomi.joyose"

.field private static final SERVICE_NAME:Ljava/lang/String; = "xiaomi.joyose"

.field private static final TAG:Ljava/lang/String; = "MiGgameTypeIdentificationImpl"

.field private static mJoyose:Lcom/xiaomi/joyose/IJoyoseInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/wm/MiGameTypeIdentificationImpl;->mJoyose:Lcom/xiaomi/joyose/IJoyoseInterface;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkIfGameBeSupported(Ljava/lang/String;)Z
    .locals 2
    .param p1, "app"    # Ljava/lang/String;

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkIfGameBeSupported app:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiGgameTypeIdentificationImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    const/4 v0, 0x0

    return v0
.end method
