class com.android.server.wm.AccountHelper$2 extends android.app.IMiuiActivityObserver$Stub {
	 /* .source "AccountHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AccountHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.AccountHelper this$0; //synthetic
/* # direct methods */
 com.android.server.wm.AccountHelper$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/AccountHelper; */
/* .line 216 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/IMiuiActivityObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void activityDestroyed ( android.content.Intent p0 ) {
/* .locals 0 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 256 */
return;
} // .end method
public void activityIdle ( android.content.Intent p0 ) {
/* .locals 0 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 219 */
return;
} // .end method
public void activityPaused ( android.content.Intent p0 ) {
/* .locals 0 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 248 */
return;
} // .end method
public void activityResumed ( android.content.Intent p0 ) {
/* .locals 5 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 223 */
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v0 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v0 ).isControllerAMonkey ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isControllerAMonkey()Z
final String v1 = "MiuiPermision"; // const-string v1, "MiuiPermision"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 224 */
v0 = com.android.server.wm.AccountHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 225 */
/* const-string/jumbo v0, "skip account check !" */
android.util.Log .v ( v1,v0 );
/* .line 227 */
} // :cond_0
return;
/* .line 229 */
} // :cond_1
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v0 ).getClassName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 230 */
/* .local v0, "className":Ljava/lang/String; */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 231 */
/* .local v2, "packageName":Ljava/lang/String; */
v3 = com.android.server.wm.AccountHelper .-$$Nest$sfgetDEBUG ( );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 232 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "resume packageName:"; // const-string v4, "resume packageName:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "mListenMode :"; // const-string v4, "mListenMode :"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
v4 = com.android.server.wm.AccountHelper .-$$Nest$fgetmListenMode ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v3 );
/* .line 235 */
} // :cond_2
com.android.server.wm.AccountHelper .-$$Nest$sfgetmContext ( );
android.accounts.AccountManager .get ( v1 );
final String v3 = "com.xiaomi"; // const-string v3, "com.xiaomi"
(( android.accounts.AccountManager ) v1 ).getAccountsByType ( v3 ); // invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;
/* .line 236 */
/* .local v1, "accounts":[Landroid/accounts/Account; */
com.android.server.wm.AccountHelper .-$$Nest$sfgetsAccessPackage ( );
v3 = (( java.util.ArrayList ) v3 ).contains ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v3, :cond_4 */
com.android.server.wm.AccountHelper .-$$Nest$sfgetsAccessActivities ( );
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
v3 = (( java.util.ArrayList ) v3 ).contains ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v3, :cond_4 */
/* .line 237 */
v3 = this.this$0;
v3 = com.android.server.wm.AccountHelper .-$$Nest$fgetmListenMode ( v3 );
/* and-int/lit8 v3, v3, 0x2 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 239 */
com.android.server.wm.AccountHelper .-$$Nest$sfgetmCallBack ( );
/* .line 240 */
} // :cond_3
v3 = this.this$0;
v3 = com.android.server.wm.AccountHelper .-$$Nest$fgetmListenMode ( v3 );
/* and-int/lit8 v3, v3, 0x1 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 241 */
v3 = this.this$0;
com.android.server.wm.AccountHelper .-$$Nest$sfgetmContext ( );
(( com.android.server.wm.AccountHelper ) v3 ).addAccount ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/AccountHelper;->addAccount(Landroid/content/Context;)V
/* .line 244 */
} // :cond_4
} // :goto_0
return;
} // .end method
public void activityStopped ( android.content.Intent p0 ) {
/* .locals 0 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 252 */
return;
} // .end method
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 260 */
} // .end method
