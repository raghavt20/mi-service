class com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager$1 implements java.util.function.BiConsumer {
	 /* .source "PackageSettingsManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;-><init>(Lcom/android/server/wm/PackageSettingsManager;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/function/BiConsumer<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager this$1; //synthetic
/* # direct methods */
 com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager; */
/* .line 167 */
this.this$1 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void accept ( java.lang.Object p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 167 */
/* check-cast p1, Ljava/lang/String; */
/* check-cast p2, Ljava/lang/String; */
(( com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager$1 ) p0 ).accept ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->accept(Ljava/lang/String;Ljava/lang/String;)V
return;
} // .end method
public void accept ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .line 170 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "DisplayCompatPackageManager key = "; // const-string v1, "DisplayCompatPackageManager key = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " value = "; // const-string v1, " value = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BoundsCompat"; // const-string v1, "BoundsCompat"
android.util.Slog .d ( v1,v0 );
/* .line 171 */
/* const-string/jumbo v0, "w" */
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 172 */
v0 = this.this$1;
com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager .-$$Nest$fgetmPackagesMapBySystem ( v0 );
int v1 = 3; // const/4 v1, 0x3
java.lang.Integer .valueOf ( v1 );
/* .line 174 */
} // :cond_0
final String v0 = "b"; // const-string v0, "b"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 175 */
v0 = this.this$1;
com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager .-$$Nest$fgetmPackagesMapBySystem ( v0 );
int v1 = 4; // const/4 v1, 0x4
java.lang.Integer .valueOf ( v1 );
/* .line 177 */
} // :cond_1
final String v0 = "r"; // const-string v0, "r"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 178 */
v0 = this.this$1;
com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager .-$$Nest$fgetmPackagesMapBySystem ( v0 );
int v1 = 7; // const/4 v1, 0x7
java.lang.Integer .valueOf ( v1 );
/* .line 180 */
} // :cond_2
final String v0 = "i"; // const-string v0, "i"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 181 */
v0 = this.this$1;
com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager .-$$Nest$fgetmPackagesMapBySystem ( v0 );
/* const/16 v1, 0x8 */
java.lang.Integer .valueOf ( v1 );
/* .line 183 */
} // :cond_3
final String v0 = "ic"; // const-string v0, "ic"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 184 */
v0 = this.this$1;
com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager .-$$Nest$fgetmPackagesMapBySystem ( v0 );
/* const/16 v1, 0x9 */
java.lang.Integer .valueOf ( v1 );
/* .line 186 */
} // :cond_4
final String v0 = "ia"; // const-string v0, "ia"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 187 */
v0 = this.this$1;
com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager .-$$Nest$fgetmPackagesMapBySystem ( v0 );
/* const/16 v1, 0xa */
java.lang.Integer .valueOf ( v1 );
/* .line 189 */
} // :cond_5
final String v0 = "ica"; // const-string v0, "ica"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 190 */
v0 = this.this$1;
com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager .-$$Nest$fgetmPackagesMapBySystem ( v0 );
/* const/16 v1, 0xb */
java.lang.Integer .valueOf ( v1 );
/* .line 192 */
} // :cond_6
final String v0 = "c"; // const-string v0, "c"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 193 */
v0 = this.this$1;
com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager .-$$Nest$fgetmPackagesMapBySystem ( v0 );
int v1 = 6; // const/4 v1, 0x6
java.lang.Integer .valueOf ( v1 );
/* .line 195 */
} // :cond_7
return;
} // .end method
