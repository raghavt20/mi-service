public class com.android.server.wm.MultiSenceManagerInternalStubImpl implements com.android.server.wm.MultiSenceManagerInternalStub {
	 /* .source "MultiSenceManagerInternalStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.wm.MultiSenceManagerInternalStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_MULTISENCE_FOCUSED_CHANGE;
private static final Boolean DEBUG;
private static final java.lang.String EXTRA_MULTISENCE_FOCUSED_PACKAGE;
private static final java.lang.String TAG;
private static com.miui.server.multisence.MultiSenceManagerInternal mMultiSenceMI;
private static com.android.server.wm.WindowManagerService service;
/* # instance fields */
private Boolean mBootComplete;
private android.content.Context mContext;
private com.android.server.wm.WindowState mCurrentFocusWindow;
public android.os.Handler mHandler;
/* # direct methods */
static Boolean -$$Nest$fgetmBootComplete ( com.android.server.wm.MultiSenceManagerInternalStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mBootComplete:Z */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.wm.MultiSenceManagerInternalStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static void -$$Nest$fputmCurrentFocusWindow ( com.android.server.wm.MultiSenceManagerInternalStubImpl p0, com.android.server.wm.WindowState p1 ) { //bridge//synthethic
/* .locals 0 */
this.mCurrentFocusWindow = p1;
return;
} // .end method
static void -$$Nest$mLOG_IF_DEBUG ( com.android.server.wm.MultiSenceManagerInternalStubImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->LOG_IF_DEBUG(Ljava/lang/String;)V */
return;
} // .end method
static Boolean -$$Nest$misConnectToWMS ( com.android.server.wm.MultiSenceManagerInternalStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->isConnectToWMS()Z */
} // .end method
static Boolean -$$Nest$misWindowPackageChanged ( com.android.server.wm.MultiSenceManagerInternalStubImpl p0, com.android.server.wm.WindowState p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->isWindowPackageChanged(Lcom/android/server/wm/WindowState;)Z */
} // .end method
static com.miui.server.multisence.MultiSenceManagerInternal -$$Nest$sfgetmMultiSenceMI ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.MultiSenceManagerInternalStubImpl.mMultiSenceMI;
} // .end method
static com.android.server.wm.WindowManagerService -$$Nest$sfgetservice ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.MultiSenceManagerInternalStubImpl.service;
} // .end method
static com.android.server.wm.MultiSenceManagerInternalStubImpl ( ) {
/* .locals 2 */
/* .line 40 */
final String v0 = "persist.multisence.debug.on"; // const-string v0, "persist.multisence.debug.on"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.MultiSenceManagerInternalStubImpl.DEBUG = (v0!= 0);
/* .line 46 */
int v0 = 0; // const/4 v0, 0x0
/* .line 47 */
return;
} // .end method
public com.android.server.wm.MultiSenceManagerInternalStubImpl ( ) {
/* .locals 1 */
/* .line 38 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 44 */
int v0 = 0; // const/4 v0, 0x0
this.mCurrentFocusWindow = v0;
/* .line 50 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mBootComplete:Z */
return;
} // .end method
private void LOG_IF_DEBUG ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "log" # Ljava/lang/String; */
/* .line 193 */
/* sget-boolean v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 194 */
	 final String v0 = "MultiSenceManagerInternalStubImpl"; // const-string v0, "MultiSenceManagerInternalStubImpl"
	 android.util.Slog .d ( v0,p1 );
	 /* .line 196 */
} // :cond_0
return;
} // .end method
private Boolean isConnectToWMS ( ) {
/* .locals 2 */
/* .line 153 */
v0 = com.android.server.wm.MultiSenceManagerInternalStubImpl.service;
/* if-nez v0, :cond_0 */
/* .line 154 */
final String v0 = "MultiSenceManagerInternalStubImpl"; // const-string v0, "MultiSenceManagerInternalStubImpl"
final String v1 = "MultiSenceManagerInternalStubImpl does not connect to WMS"; // const-string v1, "MultiSenceManagerInternalStubImpl does not connect to WMS"
android.util.Slog .e ( v0,v1 );
/* .line 155 */
int v0 = 0; // const/4 v0, 0x0
/* .line 157 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean isWindowPackageChanged ( com.android.server.wm.WindowState p0 ) {
/* .locals 3 */
/* .param p1, "newFocus" # Lcom/android/server/wm/WindowState; */
/* .line 165 */
/* if-nez p1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 166 */
} // :cond_0
v0 = this.mCurrentFocusWindow;
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
/* .line 168 */
} // :cond_1
(( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
v2 = this.mCurrentFocusWindow;
(( com.android.server.wm.WindowState ) v2 ).getOwningPackage ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/2addr v0, v1 */
} // .end method
/* # virtual methods */
public void bootComplete ( ) {
/* .locals 1 */
/* .line 54 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;->mBootComplete:Z */
/* .line 55 */
return;
} // .end method
public Boolean checkFWSupport ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 173 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->FW_ENABLE:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_2 */
/* .line 174 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_CONFIG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
/* move v1, v2 */
} // :cond_1
} // :goto_0
final String v0 = "floating window is not supported by multisence config"; // const-string v0, "floating window is not supported by multisence config"
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v1,v0 );
/* .line 175 */
/* .line 177 */
} // :cond_2
if ( p1 != null) { // if-eqz p1, :cond_5
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* if-nez v0, :cond_3 */
/* .line 181 */
} // :cond_3
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
v0 = this.floatingWindowWhiteList;
/* monitor-enter v0 */
/* .line 182 */
try { // :try_start_0
com.miui.server.multisence.MultiSenceConfig .getInstance ( );
v3 = v3 = this.floatingWindowWhiteList;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 183 */
/* sget-boolean v2, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z */
final String v3 = "floating window is supported"; // const-string v3, "floating window is supported"
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v2,v3 );
/* .line 184 */
/* monitor-exit v0 */
/* .line 186 */
} // :cond_4
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 187 */
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->DEBUG_FW:Z */
final String v1 = "floating window is not supported"; // const-string v1, "floating window is not supported"
com.miui.server.multisence.MultiSenceServiceUtils .msLogD ( v0,v1 );
/* .line 188 */
/* .line 186 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 178 */
} // :cond_5
} // :goto_1
} // .end method
public java.util.Map getWindowsNeedToSched ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 161 */
com.android.server.wm.MultiSenceUtils .getInstance ( );
(( com.android.server.wm.MultiSenceUtils ) v0 ).getWindowsNeedToSched ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MultiSenceUtils;->getWindowsNeedToSched()Ljava/util/Map;
} // .end method
public void init ( android.content.Context p0, android.os.Handler p1 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 59 */
this.mContext = p1;
/* .line 60 */
this.mHandler = p2;
/* .line 61 */
return;
} // .end method
public void sendFocusWindowsBroadcast ( com.android.server.wm.WindowState p0 ) {
/* .locals 6 */
/* .param p1, "window" # Lcom/android/server/wm/WindowState; */
/* .line 138 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.xiaomi.multisence.action.FOCUSED_CHANGE"; // const-string v1, "com.xiaomi.multisence.action.FOCUSED_CHANGE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 139 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = ""; // const-string v1, ""
/* .line 140 */
/* .local v1, "focusPackage":Ljava/lang/String; */
(( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* .line 141 */
final String v2 = "com.xiaomi.multisence.extra.FOCUSED_PACKAGE"; // const-string v2, "com.xiaomi.multisence.extra.FOCUSED_PACKAGE"
final String v3 = "MultiSenceManagerInternalStubImpl"; // const-string v3, "MultiSenceManagerInternalStubImpl"
/* if-nez v1, :cond_0 */
/* .line 142 */
final String v4 = "focusPackage\u4e3anull"; // const-string v4, "focusPackage\u4e3anull"
android.util.Slog .i ( v3,v4 );
/* .line 143 */
final String v3 = "NO_PACKAGE"; // const-string v3, "NO_PACKAGE"
(( android.content.Intent ) v0 ).putExtra ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 144 */
v2 = this.mContext;
(( android.content.Context ) v2 ).sendBroadcast ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 146 */
} // :cond_0
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "packageName = "; // const-string v5, "packageName = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 147 */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 148 */
v2 = this.mContext;
(( android.content.Context ) v2 ).sendBroadcast ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 150 */
} // :goto_0
return;
} // .end method
public Boolean systemReady ( ) {
/* .locals 2 */
/* .line 65 */
final String v0 = "miui_multi_sence"; // const-string v0, "miui_multi_sence"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/miui/server/multisence/MultiSenceManagerInternal; */
/* .line 66 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
/* .line 67 */
v1 = com.android.server.wm.MultiSenceManagerInternalStubImpl.mMultiSenceMI;
if ( v1 != null) { // if-eqz v1, :cond_1
/* if-nez v0, :cond_0 */
/* .line 71 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 68 */
} // :cond_1
} // :goto_0
final String v0 = "MultiSenceManagerInternalStubImpl"; // const-string v0, "MultiSenceManagerInternalStubImpl"
final String v1 = "MultiSenceManagerInternalStubImpl does not connect to miui_multi_sence or WMS"; // const-string v1, "MultiSenceManagerInternalStubImpl does not connect to miui_multi_sence or WMS"
android.util.Slog .e ( v0,v1 );
/* .line 69 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void updateScreenStatusWithFoucs ( com.android.server.wm.WindowState p0 ) {
/* .locals 2 */
/* .param p1, "newFocus" # Lcom/android/server/wm/WindowState; */
/* .line 133 */
/* new-instance v0, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus; */
/* invoke-direct {v0, p0, p1}, Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus;-><init>(Lcom/android/server/wm/MultiSenceManagerInternalStubImpl;Lcom/android/server/wm/WindowState;)V */
/* .line 134 */
/* .local v0, "runnable":Lcom/android/server/wm/MultiSenceManagerInternalStubImpl$UpdateScreenStatus; */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).post ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 135 */
return;
} // .end method
