class com.android.server.wm.ScreenRotationAnimationImpl$TmpValues {
	 /* .source "ScreenRotationAnimationImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/ScreenRotationAnimationImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "TmpValues" */
} // .end annotation
/* # instance fields */
final floats;
final android.view.animation.Transformation transformation;
/* # direct methods */
 com.android.server.wm.ScreenRotationAnimationImpl$TmpValues ( ) {
/* .locals 1 */
/* .line 666 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 667 */
/* new-instance v0, Landroid/view/animation/Transformation; */
/* invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V */
this.transformation = v0;
/* .line 668 */
/* const/16 v0, 0x9 */
/* new-array v0, v0, [F */
this.floats = v0;
return;
} // .end method
