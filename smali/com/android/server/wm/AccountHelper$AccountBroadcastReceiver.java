class com.android.server.wm.AccountHelper$AccountBroadcastReceiver extends android.content.BroadcastReceiver {
	 /* .source "AccountHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AccountHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AccountBroadcastReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.wm.AccountHelper this$0; //synthetic
/* # direct methods */
private com.android.server.wm.AccountHelper$AccountBroadcastReceiver ( ) {
/* .locals 0 */
/* .line 113 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.wm.AccountHelper$AccountBroadcastReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/AccountHelper$AccountBroadcastReceiver;-><init>(Lcom/android/server/wm/AccountHelper;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 116 */
/* if-nez p2, :cond_0 */
/* .line 117 */
return;
/* .line 119 */
} // :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 120 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"; // const-string v1, "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"
v1 = android.text.TextUtils .equals ( v0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 121 */
final String v1 = "extra_update_type"; // const-string v1, "extra_update_type"
int v2 = -1; // const/4 v2, -0x1
v1 = (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 122 */
/* .local v1, "type":I */
final String v2 = "extra_account"; // const-string v2, "extra_account"
(( android.content.Intent ) p2 ).getParcelableExtra ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v2, Landroid/accounts/Account; */
/* .line 123 */
/* .local v2, "account":Landroid/accounts/Account; */
final String v3 = "MiuiPermision"; // const-string v3, "MiuiPermision"
if ( v2 != null) { // if-eqz v2, :cond_4
	 v4 = this.type;
	 final String v5 = "com.xiaomi"; // const-string v5, "com.xiaomi"
	 v4 = 	 android.text.TextUtils .equals ( v4,v5 );
	 /* if-nez v4, :cond_1 */
	 /* .line 128 */
} // :cond_1
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
/* .line 129 */
/* .local v4, "appContext":Landroid/content/Context; */
int v5 = 1; // const/4 v5, 0x1
/* if-ne v1, v5, :cond_2 */
/* .line 131 */
v3 = this.this$0;
(( com.android.server.wm.AccountHelper ) v3 ).onXiaomiAccountLogout ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Lcom/android/server/wm/AccountHelper;->onXiaomiAccountLogout(Landroid/content/Context;Landroid/accounts/Account;)V
/* .line 132 */
} // :cond_2
int v5 = 2; // const/4 v5, 0x2
/* if-ne v1, v5, :cond_3 */
/* .line 134 */
v3 = this.this$0;
(( com.android.server.wm.AccountHelper ) v3 ).onXiaomiAccountLogin ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Lcom/android/server/wm/AccountHelper;->onXiaomiAccountLogin(Landroid/content/Context;Landroid/accounts/Account;)V
/* .line 136 */
} // :cond_3
java.lang.Integer .valueOf ( v1 );
/* filled-new-array {v5}, [Ljava/lang/Object; */
final String v6 = "Xiaomi account changed, but unknown type: %s."; // const-string v6, "Xiaomi account changed, but unknown type: %s."
java.lang.String .format ( v6,v5 );
android.util.Log .w ( v3,v5 );
/* .line 124 */
} // .end local v4 # "appContext":Landroid/content/Context;
} // :cond_4
} // :goto_0
final String v4 = "It isn\'t a xiaomi account changed."; // const-string v4, "It isn\'t a xiaomi account changed."
android.util.Log .i ( v3,v4 );
/* .line 125 */
return;
/* .line 139 */
} // .end local v1 # "type":I
} // .end local v2 # "account":Landroid/accounts/Account;
} // :cond_5
} // :goto_1
return;
} // .end method
