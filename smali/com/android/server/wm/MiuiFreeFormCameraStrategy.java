public class com.android.server.wm.MiuiFreeFormCameraStrategy {
	 /* .source "MiuiFreeFormCameraStrategy.java" */
	 /* # static fields */
	 public static final Integer CAMERA_INITIATED;
	 public static final Integer CAMERA_RELEASED;
	 private static final Integer DEFAULT_CAMERA_ORIENTATION;
	 private static final java.lang.String FREEFORM_CAMERA_ORIENTATION_KEY;
	 private static final java.lang.String FREEFORM_CAMERA_PROP_KEY;
	 /* # instance fields */
	 private final java.lang.String TAG;
	 private Integer mCameraStateGlobal;
	 private Integer mCameraStateInFreeForm;
	 private java.lang.String mPackageUseCamera;
	 private com.android.server.wm.MiuiFreeFormManagerService mService;
	 /* # direct methods */
	 public com.android.server.wm.MiuiFreeFormCameraStrategy ( ) {
		 /* .locals 2 */
		 /* .param p1, "service" # Lcom/android/server/wm/MiuiFreeFormManagerService; */
		 /* .line 32 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 18 */
		 final String v0 = "MiuiFreeFormCameraStrategy"; // const-string v0, "MiuiFreeFormCameraStrategy"
		 this.TAG = v0;
		 /* .line 28 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I */
		 /* .line 29 */
		 /* iput v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I */
		 /* .line 30 */
		 final String v1 = ""; // const-string v1, ""
		 this.mPackageUseCamera = v1;
		 /* .line 33 */
		 this.mService = p1;
		 /* .line 34 */
		 (( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setFreeFormCameraProp ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setFreeFormCameraProp(I)V
		 /* .line 35 */
		 int v1 = -1; // const/4 v1, -0x1
		 (( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setCameraOrientation ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V
		 /* .line 36 */
		 /* iput v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I */
		 /* .line 37 */
		 /* iput v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I */
		 /* .line 38 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void onCameraStateChanged ( java.lang.String p0, Integer p1 ) {
		 /* .locals 3 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .param p2, "cameraState" # I */
		 /* .line 41 */
		 /* iput p2, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I */
		 /* .line 42 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* if-ne p2, v0, :cond_0 */
		 /* move-object v1, p1 */
	 } // :cond_0
	 final String v1 = ""; // const-string v1, ""
} // :goto_0
this.mPackageUseCamera = v1;
/* .line 44 */
v1 = this.mService;
v1 = (( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).isInFreeForm ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isInFreeForm(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
	 v1 = android.util.MiuiMultiWindowAdapter.USE_DEFAULT_CAMERA_PIPELINE_APP;
	 v1 = 	 /* .line 45 */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 48 */
	 } // :cond_1
	 /* iget v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I */
	 /* iget v2, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I */
	 /* if-eq v1, v2, :cond_3 */
	 /* .line 49 */
	 (( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setFreeFormCameraProp ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setFreeFormCameraProp(I)V
	 /* .line 50 */
	 /* iget v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I */
	 final String v2 = "MiuiFreeFormCameraStrategy"; // const-string v2, "MiuiFreeFormCameraStrategy"
	 /* if-ne v1, v0, :cond_2 */
	 /* .line 51 */
	 final String v0 = "camera opened!"; // const-string v0, "camera opened!"
	 android.util.Slog .d ( v2,v0 );
	 /* .line 52 */
	 (( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setCameraOrientation ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation()V
	 /* .line 54 */
} // :cond_2
final String v0 = "camera released!"; // const-string v0, "camera released!"
android.util.Slog .d ( v2,v0 );
/* .line 55 */
int v0 = -1; // const/4 v0, -0x1
(( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setCameraOrientation ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V
/* .line 58 */
} // :cond_3
} // :goto_1
return;
/* .line 46 */
} // :cond_4
} // :goto_2
return;
} // .end method
public Boolean openCameraInFreeForm ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 76 */
v0 = this.mService;
v0 = (( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).isInFreeForm ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isInFreeForm(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mPackageUseCamera;
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void rotateCameraIfNeeded ( ) {
/* .locals 2 */
/* .line 61 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateGlobal:I */
/* if-nez v0, :cond_0 */
return;
/* .line 63 */
} // :cond_0
v0 = this.mService;
v1 = this.mPackageUseCamera;
v0 = (( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).isInFreeForm ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isInFreeForm(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = android.util.MiuiMultiWindowAdapter.USE_DEFAULT_CAMERA_PIPELINE_APP;
v1 = this.mPackageUseCamera;
v0 = /* .line 64 */
/* if-nez v0, :cond_1 */
/* .line 65 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I */
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_2 */
/* .line 66 */
(( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setFreeFormCameraProp ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setFreeFormCameraProp(I)V
/* .line 67 */
(( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setCameraOrientation ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation()V
/* .line 70 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setFreeFormCameraProp ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setFreeFormCameraProp(I)V
/* .line 71 */
int v0 = -1; // const/4 v0, -0x1
(( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setCameraOrientation ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V
/* .line 73 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void setCameraOrientation ( ) {
/* .locals 1 */
/* .line 92 */
v0 = this.mService;
v0 = this.mActivityTaskManagerService;
v0 = this.mWindowManager;
/* .line 93 */
(( com.android.server.wm.WindowManagerService ) v0 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
v0 = this.mDisplay;
v0 = (( android.view.Display ) v0 ).getRotation ( ); // invoke-virtual {v0}, Landroid/view/Display;->getRotation()I
/* .line 92 */
(( com.android.server.wm.MiuiFreeFormCameraStrategy ) p0 ).setCameraOrientation ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V
/* .line 94 */
return;
} // .end method
public void setCameraOrientation ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "orientation" # I */
/* .line 80 */
final String v0 = "MiuiFreeFormCameraStrategy"; // const-string v0, "MiuiFreeFormCameraStrategy"
/* iget v1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I */
/* if-nez v1, :cond_0 */
int v1 = -1; // const/4 v1, -0x1
/* if-eq p1, v1, :cond_0 */
/* .line 81 */
return;
/* .line 84 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "persist.vendor.freeform.cam.orientation:"; // const-string v2, "persist.vendor.freeform.cam.orientation:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 85 */
final String v1 = "persist.vendor.freeform.cam.orientation"; // const-string v1, "persist.vendor.freeform.cam.orientation"
final String v2 = "%d"; // const-string v2, "%d"
int v3 = 1; // const/4 v3, 0x1
/* new-array v3, v3, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
int v5 = 0; // const/4 v5, 0x0
/* aput-object v4, v3, v5 */
java.lang.String .format ( v2,v3 );
android.os.SystemProperties .set ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 88 */
/* .line 86 */
/* :catch_0 */
/* move-exception v1 */
/* .line 87 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "set persist.vendor.freeform.cam.orientation error! " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 89 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void setFreeFormCameraProp ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "cameraProp" # I */
/* .line 97 */
final String v0 = "MiuiFreeFormCameraStrategy"; // const-string v0, "MiuiFreeFormCameraStrategy"
/* iput p1, p0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->mCameraStateInFreeForm:I */
/* .line 99 */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "persist.vendor.freeform.3appcam:"; // const-string v2, "persist.vendor.freeform.3appcam:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 100 */
final String v1 = "persist.vendor.freeform.3appcam"; // const-string v1, "persist.vendor.freeform.3appcam"
final String v2 = "%d"; // const-string v2, "%d"
int v3 = 1; // const/4 v3, 0x1
/* new-array v3, v3, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
int v5 = 0; // const/4 v5, 0x0
/* aput-object v4, v3, v5 */
java.lang.String .format ( v2,v3 );
android.os.SystemProperties .set ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 103 */
/* .line 101 */
/* :catch_0 */
/* move-exception v1 */
/* .line 102 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "set persist.vendor.freeform.3appcam error! " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 104 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
