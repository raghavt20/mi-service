.class Lcom/android/server/wm/MiuiRecognizeScene$2;
.super Ljava/lang/Object;
.source "MiuiRecognizeScene.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiRecognizeScene;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiRecognizeScene;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiRecognizeScene;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiRecognizeScene;

    .line 104
    iput-object p1, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arg1"    # Landroid/os/IBinder;

    .line 116
    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    invoke-static {p2}, Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/wm/MiuiRecognizeScene;->-$$Nest$fputmRecognizeService(Lcom/android/server/wm/MiuiRecognizeScene;Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;)V

    .line 117
    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    invoke-static {v0}, Lcom/android/server/wm/MiuiRecognizeScene;->-$$Nest$fgetmBgHandler(Lcom/android/server/wm/MiuiRecognizeScene;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    invoke-static {v1}, Lcom/android/server/wm/MiuiRecognizeScene;->-$$Nest$fgetmBindServiceRunnable(Lcom/android/server/wm/MiuiRecognizeScene;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    invoke-static {v0}, Lcom/android/server/wm/MiuiRecognizeScene;->-$$Nest$fgetmRecognizeService(Lcom/android/server/wm/MiuiRecognizeScene;)Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    invoke-static {v1}, Lcom/android/server/wm/MiuiRecognizeScene;->-$$Nest$fgetmDeathHandler(Lcom/android/server/wm/MiuiRecognizeScene;)Landroid/os/IBinder$DeathRecipient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    goto :goto_0

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 124
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .line 108
    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/wm/MiuiRecognizeScene;->-$$Nest$fputmRecognizeService(Lcom/android/server/wm/MiuiRecognizeScene;Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;)V

    .line 109
    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    invoke-static {v0}, Lcom/android/server/wm/MiuiRecognizeScene;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiRecognizeScene;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    invoke-static {v0}, Lcom/android/server/wm/MiuiRecognizeScene;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiRecognizeScene;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiRecognizeScene$2;->this$0:Lcom/android/server/wm/MiuiRecognizeScene;

    invoke-static {v1}, Lcom/android/server/wm/MiuiRecognizeScene;->-$$Nest$fgetmPerformanceConnection(Lcom/android/server/wm/MiuiRecognizeScene;)Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 112
    :cond_0
    return-void
.end method
