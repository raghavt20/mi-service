.class Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;
.super Ljava/lang/Object;
.source "MiuiFreezeImpl.java"

# interfaces
.implements Lmiuix/appcompat/app/AlertDialog$OnDialogShowAnimListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiFreezeImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DialogShowListener"
.end annotation


# instance fields
.field private isNeedDismissDialog:Z

.field final synthetic this$0:Lcom/android/server/wm/MiuiFreezeImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0

    .line 497
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 499
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->isNeedDismissDialog:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/wm/MiuiFreezeImpl;Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V

    return-void
.end method


# virtual methods
.method public onShowAnimComplete()V
    .locals 2

    .line 507
    const-string v0, "MiuiFreezeImpl"

    const-string v1, "onShowAnimComplete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fputmDialogShowing(Lcom/android/server/wm/MiuiFreezeImpl;Z)V

    .line 510
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->isNeedDismissDialog:Z

    if-eqz v0, :cond_0

    .line 511
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->setNeedDismissDialog(Z)V

    .line 512
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$mdismissDialog(Lcom/android/server/wm/MiuiFreezeImpl;)V

    .line 514
    :cond_0
    return-void
.end method

.method public onShowAnimStart()V
    .locals 0

    .line 518
    return-void
.end method

.method setNeedDismissDialog(Z)V
    .locals 0
    .param p1, "needDismissDialog"    # Z

    .line 502
    iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->isNeedDismissDialog:Z

    .line 503
    return-void
.end method
