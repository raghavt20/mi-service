public class com.android.server.wm.PackageSettingsManager {
	 /* .source "PackageSettingsManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;, */
	 /* Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPolicy;, */
	 /* Lcom/android/server/wm/PackageSettingsManager$TaskStarter;, */
	 /* Lcom/android/server/wm/PackageSettingsManager$RequestPackageSettings; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final com.android.server.wm.ActivityTaskManagerService mAtmService;
private final com.android.server.wm.ActivityTaskManagerServiceImpl mAtmServiceImpl;
public final com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager mDisplayCompatPackages;
private com.android.server.wm.SafeActivityOptions mFullScreenLaunchOption;
Boolean mHasDisplayCutout;
/* # direct methods */
static com.android.server.wm.ActivityTaskManagerService -$$Nest$fgetmAtmService ( com.android.server.wm.PackageSettingsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAtmService;
} // .end method
static com.android.server.wm.ActivityTaskManagerServiceImpl -$$Nest$fgetmAtmServiceImpl ( com.android.server.wm.PackageSettingsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAtmServiceImpl;
} // .end method
static com.android.server.wm.SafeActivityOptions -$$Nest$fgetmFullScreenLaunchOption ( com.android.server.wm.PackageSettingsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mFullScreenLaunchOption;
} // .end method
static void -$$Nest$fputmFullScreenLaunchOption ( com.android.server.wm.PackageSettingsManager p0, com.android.server.wm.SafeActivityOptions p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mFullScreenLaunchOption = p1;
	 return;
} // .end method
 com.android.server.wm.PackageSettingsManager ( ) {
	 /* .locals 1 */
	 /* .param p1, "atmServiceImpl" # Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
	 /* .line 28 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 29 */
	 this.mAtmServiceImpl = p1;
	 /* .line 30 */
	 v0 = this.mAtmService;
	 this.mAtmService = v0;
	 /* .line 31 */
	 /* new-instance v0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;-><init>(Lcom/android/server/wm/PackageSettingsManager;)V */
	 this.mDisplayCompatPackages = v0;
	 /* .line 32 */
	 return;
} // .end method
static java.lang.String displayCompatPolicyToString ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p0, "policy" # I */
	 /* .line 35 */
	 /* if-nez p0, :cond_0 */
	 /* .line 36 */
	 final String v0 = "NONE"; // const-string v0, "NONE"
	 /* .line 39 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, v0, :cond_1 */
/* .line 40 */
final String v0 = "FORCED_RESIZEABLE_BY_USER_SETTING"; // const-string v0, "FORCED_RESIZEABLE_BY_USER_SETTING"
/* .line 43 */
} // :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-ne p0, v0, :cond_2 */
/* .line 44 */
final String v0 = "FORCED_UNRESIZEABLE_BY_USER_SETTING"; // const-string v0, "FORCED_UNRESIZEABLE_BY_USER_SETTING"
/* .line 47 */
} // :cond_2
int v0 = 3; // const/4 v0, 0x3
/* if-ne p0, v0, :cond_3 */
/* .line 48 */
final String v0 = "FORCED_RESIZEABLE_BY_ALLOW_LIST"; // const-string v0, "FORCED_RESIZEABLE_BY_ALLOW_LIST"
/* .line 51 */
} // :cond_3
int v0 = 4; // const/4 v0, 0x4
/* if-ne p0, v0, :cond_4 */
/* .line 52 */
final String v0 = "FORCED_UNRESIZEABLE_BY_BLOCK_LIST"; // const-string v0, "FORCED_UNRESIZEABLE_BY_BLOCK_LIST"
/* .line 55 */
} // :cond_4
int v0 = 7; // const/4 v0, 0x7
/* if-ne p0, v0, :cond_5 */
/* .line 56 */
final String v0 = "CONTINUITY_RESTART_BY_BLOCK_LIST"; // const-string v0, "CONTINUITY_RESTART_BY_BLOCK_LIST"
/* .line 59 */
} // :cond_5
/* const/16 v0, 0x8 */
/* if-ne p0, v0, :cond_6 */
/* .line 60 */
final String v0 = "CONTINUITY_INTERCEPT_BY_BLOCK_LIST"; // const-string v0, "CONTINUITY_INTERCEPT_BY_BLOCK_LIST"
/* .line 63 */
} // :cond_6
/* const/16 v0, 0x9 */
/* if-ne p0, v0, :cond_7 */
/* .line 64 */
final String v0 = "CONTINUITY_INTERCEPT_COMPONENT_BY_BLOCK_LIST"; // const-string v0, "CONTINUITY_INTERCEPT_COMPONENT_BY_BLOCK_LIST"
/* .line 67 */
} // :cond_7
/* const/16 v0, 0xa */
/* if-ne p0, v0, :cond_8 */
/* .line 68 */
final String v0 = "CONTINUITY_INTERCEPT_BY_ALLOW_LIST"; // const-string v0, "CONTINUITY_INTERCEPT_BY_ALLOW_LIST"
/* .line 71 */
} // :cond_8
/* const/16 v0, 0xb */
/* if-ne p0, v0, :cond_9 */
/* .line 72 */
final String v0 = "CONTINUITY_INTERCEPT_COMPONENT_BY_ALLOW_LIST"; // const-string v0, "CONTINUITY_INTERCEPT_COMPONENT_BY_ALLOW_LIST"
/* .line 75 */
} // :cond_9
int v0 = 6; // const/4 v0, 0x6
/* if-ne p0, v0, :cond_a */
/* .line 76 */
final String v0 = "CONTINUITY_RELAUNCH_BY_BLOCK_LIST"; // const-string v0, "CONTINUITY_RELAUNCH_BY_BLOCK_LIST"
/* .line 79 */
} // :cond_a
int v0 = 5; // const/4 v0, 0x5
/* if-ne p0, v0, :cond_b */
/* .line 80 */
final String v0 = "EXCLUDE_BY_META_DATA"; // const-string v0, "EXCLUDE_BY_META_DATA"
/* .line 83 */
} // :cond_b
java.lang.Integer .toString ( p0 );
} // .end method
public static Boolean isForcedResizeableDisplayCompatPolicy ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "policy" # I */
/* .line 87 */
int v0 = 1; // const/4 v0, 0x1
/* .line 88 */
/* .local v0, "isForceResize":Z */
int v1 = 3; // const/4 v1, 0x3
/* if-eq p0, v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
/* if-eq p0, v1, :cond_0 */
int v1 = 5; // const/4 v1, 0x5
/* if-eq p0, v1, :cond_0 */
/* .line 91 */
int v0 = 0; // const/4 v0, 0x0
/* .line 93 */
} // :cond_0
} // .end method
public static Boolean isForcedUnresizeableDisplayCompatPolicy ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "policy" # I */
/* .line 97 */
int v0 = 4; // const/4 v0, 0x4
/* if-eq p0, v0, :cond_1 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p0, v0, :cond_1 */
int v0 = 7; // const/4 v0, 0x7
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0x8 */
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0x9 */
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0xa */
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0xb */
/* if-eq p0, v0, :cond_1 */
int v0 = 6; // const/4 v0, 0x6
/* if-ne p0, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
static Boolean lambda$killAndRestartTask$0 ( Integer p0, java.lang.String p1, com.android.server.wm.Task p2 ) { //synthethic
/* .locals 3 */
/* .param p0, "userId" # I */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "t" # Lcom/android/server/wm/Task; */
/* .line 136 */
/* iget v0, p2, Lcom/android/server/wm/Task;->mUserId:I */
int v1 = 0; // const/4 v1, 0x0
/* if-eq v0, p0, :cond_0 */
/* .line 137 */
/* .line 139 */
} // :cond_0
v0 = this.realActivity;
/* if-nez v0, :cond_1 */
/* .line 140 */
/* .line 142 */
} // :cond_1
v0 = this.realActivity;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 143 */
/* .local v0, "pkgName":Ljava/lang/String; */
v2 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
/* .line 144 */
/* .line 146 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
} // .end method
static void lambda$killAndRestartTask$1 ( com.android.server.wm.PackageSettingsManager$TaskStarter p0 ) { //synthethic
/* .locals 0 */
/* .param p0, "taskStarter" # Lcom/android/server/wm/PackageSettingsManager$TaskStarter; */
/* .line 154 */
com.android.server.wm.PackageSettingsManager$TaskStarter .-$$Nest$mrestartTask ( p0 );
/* .line 155 */
return;
} // .end method
/* # virtual methods */
void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 108 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
/* if-nez v0, :cond_0 */
/* .line 109 */
final String v0 = "MiuiAppSizeCompatMode not enabled"; // const-string v0, "MiuiAppSizeCompatMode not enabled"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 110 */
return;
/* .line 113 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 114 */
/* .local v0, "innerPrefix":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "PACKAGE SETTINGS MANAGER"; // const-string v2, "PACKAGE SETTINGS MANAGER"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 116 */
/* iget-boolean v1, p0, Lcom/android/server/wm/PackageSettingsManager;->mHasDisplayCutout:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 117 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mHasDisplayCutout=true"; // const-string v2, "mHasDisplayCutout=true"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 119 */
} // :cond_1
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 120 */
v1 = this.mDisplayCompatPackages;
com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager .-$$Nest$mdump ( v1,p1,v0 );
/* .line 121 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 122 */
return;
} // .end method
void killAndRestartTask ( Integer p0, java.lang.String p1, android.app.ActivityOptions p2, Integer p3, java.lang.String p4 ) {
/* .locals 9 */
/* .param p1, "restartTaskId" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "activityOptions" # Landroid/app/ActivityOptions; */
/* .param p4, "userId" # I */
/* .param p5, "reason" # Ljava/lang/String; */
/* .line 125 */
v0 = this.mAtmService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 128 */
try { // :try_start_0
com.android.server.wm.WindowManagerService .boostPriorityForLockedSection ( );
/* .line 129 */
int v1 = -1; // const/4 v1, -0x1
int v2 = 1; // const/4 v2, 0x1
/* if-eq p1, v1, :cond_0 */
/* .line 130 */
v1 = this.mAtmService;
v1 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v1 ).anyTaskForId ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;
/* .line 131 */
/* .local v5, "targetTask":Lcom/android/server/wm/Task; */
/* new-instance v1, Lcom/android/server/wm/PackageSettingsManager$TaskStarter; */
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v1 */
/* move-object v4, p0 */
/* move-object v6, p3 */
/* move-object v7, p5 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;-><init>(Lcom/android/server/wm/PackageSettingsManager;Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;Ljava/lang/String;Lcom/android/server/wm/PackageSettingsManager$TaskStarter-IA;)V */
/* .line 132 */
/* .local v1, "taskStarter":Lcom/android/server/wm/PackageSettingsManager$TaskStarter; */
/* move-object v3, v5 */
/* .line 133 */
} // .end local v5 # "targetTask":Lcom/android/server/wm/Task;
/* .local v3, "task":Lcom/android/server/wm/Task; */
/* .line 134 */
} // .end local v1 # "taskStarter":Lcom/android/server/wm/PackageSettingsManager$TaskStarter;
} // .end local v3 # "task":Lcom/android/server/wm/Task;
} // :cond_0
v1 = this.mAtmService;
v1 = this.mRootWindowContainer;
/* .line 135 */
/* .local v1, "rootWindowContainer":Lcom/android/server/wm/RootWindowContainer; */
/* new-instance v3, Lcom/android/server/wm/PackageSettingsManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p4, p2}, Lcom/android/server/wm/PackageSettingsManager$$ExternalSyntheticLambda0;-><init>(ILjava/lang/String;)V */
(( com.android.server.wm.RootWindowContainer ) v1 ).getTask ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/wm/RootWindowContainer;->getTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;
/* .line 148 */
/* .restart local v3 # "task":Lcom/android/server/wm/Task; */
int v4 = 0; // const/4 v4, 0x0
/* move-object v1, v4 */
/* .line 150 */
/* .local v1, "taskStarter":Lcom/android/server/wm/PackageSettingsManager$TaskStarter; */
} // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 151 */
v4 = this.mAtmService;
v4 = this.mTaskSupervisor;
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.wm.ActivityTaskSupervisor ) v4 ).removeTask ( v3, v2, v5, p5 ); // invoke-virtual {v4, v3, v2, v5, p5}, Lcom/android/server/wm/ActivityTaskSupervisor;->removeTask(Lcom/android/server/wm/Task;ZZLjava/lang/String;)V
/* .line 152 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 153 */
v2 = this.mAtmService;
v2 = this.mH;
/* new-instance v4, Lcom/android/server/wm/PackageSettingsManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v4, v1}, Lcom/android/server/wm/PackageSettingsManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/PackageSettingsManager$TaskStarter;)V */
(( com.android.server.wm.ActivityTaskManagerService$H ) v2 ).post ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 158 */
} // :cond_1
com.android.server.wm.WindowManagerService .resetPriorityAfterLockedSection ( );
/* .line 159 */
} // .end local v1 # "taskStarter":Lcom/android/server/wm/PackageSettingsManager$TaskStarter;
} // .end local v3 # "task":Lcom/android/server/wm/Task;
/* monitor-exit v0 */
/* .line 160 */
return;
/* .line 159 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
