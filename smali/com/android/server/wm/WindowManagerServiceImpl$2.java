class com.android.server.wm.WindowManagerServiceImpl$2 extends android.app.IWallpaperManagerCallback$Stub {
	 /* .source "WindowManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/WindowManagerServiceImpl;->registerBootCompletedReceiver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.WindowManagerServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.WindowManagerServiceImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/WindowManagerServiceImpl; */
/* .line 357 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/IWallpaperManagerCallback$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onWallpaperChanged ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 360 */
v0 = this.this$0;
v0 = this.mWmService;
v0 = this.mH;
/* const/16 v1, 0x6a */
(( com.android.server.wm.WindowManagerService$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(I)V
/* .line 361 */
v0 = this.this$0;
v0 = this.mWmService;
v0 = this.mH;
(( com.android.server.wm.WindowManagerService$H ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->sendEmptyMessage(I)Z
/* .line 362 */
return;
} // .end method
public void onWallpaperColorsChanged ( android.app.WallpaperColors p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "colors" # Landroid/app/WallpaperColors; */
/* .param p2, "which" # I */
/* .param p3, "userId" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 366 */
v0 = this.this$0;
v0 = this.mWmService;
v0 = this.mH;
/* const/16 v1, 0x6a */
(( com.android.server.wm.WindowManagerService$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(I)V
/* .line 367 */
v0 = this.this$0;
v0 = this.mWmService;
v0 = this.mH;
(( com.android.server.wm.WindowManagerService$H ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->sendEmptyMessage(I)Z
/* .line 368 */
return;
} // .end method
