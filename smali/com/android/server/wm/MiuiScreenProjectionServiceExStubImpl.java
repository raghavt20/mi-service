public class com.android.server.wm.MiuiScreenProjectionServiceExStubImpl implements com.android.server.wm.IMiuiScreenProjectionServiceExStub {
	 /* .source "MiuiScreenProjectionServiceExStubImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.wm.MiuiScreenProjectionServiceExStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void onCastActivityResumed ( com.android.server.wm.ActivityRecord p0 ) {
		 /* .locals 1 */
		 /* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 16 */
		 com.android.server.wm.ActivityTaskManagerServiceImpl .get ( );
		 (( com.android.server.wm.ActivityTaskManagerServiceStub ) v0 ).getMiuiActivityController ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getMiuiActivityController()Lcom/android/server/wm/MiuiActivityController;
		 /* .line 17 */
		 /* .line 18 */
		 com.android.server.wm.ActivityTaskManagerServiceImpl .get ( );
		 (( com.android.server.wm.ActivityTaskManagerServiceStub ) v0 ).onForegroundActivityChangedLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V
		 /* .line 19 */
		 return;
	 } // .end method
	 public void setAlertWindowTitle ( android.view.WindowManager$LayoutParams p0 ) {
		 /* .locals 0 */
		 /* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
		 /* .line 22 */
		 com.android.server.wm.WindowManagerServiceImpl .setAlertWindowTitle ( p1 );
		 /* .line 23 */
		 return;
	 } // .end method
