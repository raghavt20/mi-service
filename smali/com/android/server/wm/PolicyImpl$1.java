class com.android.server.wm.PolicyImpl$1 extends android.database.ContentObserver {
	 /* .source "PolicyImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/PolicyImpl;->registerDataObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.PolicyImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.PolicyImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/PolicyImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 123 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .line 126 */
final String v0 = "PolicyImpl"; // const-string v0, "PolicyImpl"
final String v1 = "AppContinuity policyDataMapFromCloud onChange--"; // const-string v1, "AppContinuity policyDataMapFromCloud onChange--"
android.util.Slog .w ( v0,v1 );
/* .line 127 */
v0 = this.this$0;
(( com.android.server.wm.PolicyImpl ) v0 ).updateDataMapFromCloud ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PolicyImpl;->updateDataMapFromCloud()V
/* .line 128 */
return;
} // .end method
