public class com.android.server.wm.TaskStubImpl implements com.android.server.wm.TaskStub {
	 /* .source "TaskStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static final java.util.List mNeedFinishAct;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.List mVtCameraNeedFinishAct;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean isHierarchyBottom;
/* # direct methods */
static com.android.server.wm.TaskStubImpl ( ) {
/* .locals 2 */
/* .line 40 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 43 */
final String v1 = "com.miui.securitycenter/com.miui.permcenter.permissions.SystemAppPermissionDialogActivity"; // const-string v1, "com.miui.securitycenter/com.miui.permcenter.permissions.SystemAppPermissionDialogActivity"
/* .line 44 */
final String v1 = "com.xiaomi.account/.ui.SystemAccountAuthDialogActivity"; // const-string v1, "com.xiaomi.account/.ui.SystemAccountAuthDialogActivity"
/* .line 47 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 50 */
final String v1 = "com.milink.service/com.xiaomi.vtcamera.activities.CameraServerActivity"; // const-string v1, "com.milink.service/com.xiaomi.vtcamera.activities.CameraServerActivity"
/* .line 51 */
return;
} // .end method
public com.android.server.wm.TaskStubImpl ( ) {
/* .locals 1 */
/* .line 33 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 37 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/TaskStubImpl;->isHierarchyBottom:Z */
return;
} // .end method
static void lambda$clearSizeCompatInSplitScreen$1 ( com.android.server.wm.ActivityRecord p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 275 */
v0 = (( com.android.server.wm.ActivityRecord ) p0 ).hasSizeCompatBounds ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->hasSizeCompatBounds()Z
/* if-nez v0, :cond_0 */
(( com.android.server.wm.ActivityRecord ) p0 ).getCompatDisplayInsets ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->getCompatDisplayInsets()Lcom/android/server/wm/ActivityRecord$CompatDisplayInsets;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 276 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) p0 ).clearSizeCompatMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->clearSizeCompatMode()V
/* .line 278 */
} // :cond_1
return;
} // .end method
static Boolean lambda$isSplitScreenModeDismissed$0 ( com.android.server.wm.Task p0, com.android.server.wm.Task p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "root" # Lcom/android/server/wm/Task; */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 103 */
/* if-eq p1, p0, :cond_0 */
v0 = (( com.android.server.wm.Task ) p1 ).hasChild ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->hasChild()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
/* # virtual methods */
public Boolean addInvisiblePipTaskToTransition ( Boolean p0, com.android.server.wm.WindowContainer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "inVisibleState" # Z */
/* .param p2, "wc" # Lcom/android/server/wm/WindowContainer; */
/* .param p3, "windowingMode" # I */
/* .line 387 */
(( com.android.server.wm.WindowContainer ) p2 ).asTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mDisplayContent;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mDisplayContent;
v0 = (( com.android.server.wm.DisplayContent ) v0 ).isSleeping ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->isSleeping()Z
/* if-nez v0, :cond_0 */
/* .line 391 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( com.android.server.wm.WindowContainer ) p2 ).isOrganized ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->isOrganized()Z
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 392 */
v0 = (( com.android.server.wm.WindowContainer ) p2 ).getWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
/* if-eq p3, v0, :cond_1 */
v0 = (( com.android.server.wm.WindowContainer ) p2 ).inPinnedWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->inPinnedWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 393 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Add invisible pip task to transition, taskId : "; // const-string v1, "Add invisible pip task to transition, taskId : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 394 */
(( com.android.server.wm.WindowContainer ) p2 ).asTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
/* iget v1, v1, Lcom/android/server/wm/Task;->mTaskId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 393 */
final String v1 = "TaskStubImpl"; // const-string v1, "TaskStubImpl"
android.util.Slog .i ( v1,v0 );
/* .line 395 */
int v0 = 1; // const/4 v0, 0x1
/* .line 398 */
} // :cond_1
/* .line 388 */
} // :cond_2
} // :goto_0
} // .end method
public void checkFreeFormActivityRecordIfNeeded ( java.util.ArrayList p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/wm/Transition$ChangeInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 404 */
/* .local p1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Transition$ChangeInfo;>;" */
v0 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 405 */
(( java.util.ArrayList ) p1 ).get ( v0 ); // invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/wm/Transition$ChangeInfo; */
/* .line 406 */
/* .local v1, "targetChange":Lcom/android/server/wm/Transition$ChangeInfo; */
v2 = this.mContainer;
/* .line 407 */
/* .local v2, "target":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;" */
(( com.android.server.wm.WindowContainer ) v2 ).asActivityRecord ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = (( com.android.server.wm.WindowContainer ) v2 ).inFreeformWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->inFreeformWindowingMode()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 408 */
(( com.android.server.wm.WindowContainer ) v2 ).asActivityRecord ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
v3 = (( com.android.server.wm.TaskStubImpl ) p0 ).shouldSkipFreeFormActivityRecord ( p1, v3 ); // invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/TaskStubImpl;->shouldSkipFreeFormActivityRecord(Ljava/util/ArrayList;Lcom/android/server/wm/ActivityRecord;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 409 */
(( java.util.ArrayList ) p1 ).remove ( v0 ); // invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* .line 410 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " skipFreeFormActivityRecordIfNeeded: target= "; // const-string v4, " skipFreeFormActivityRecordIfNeeded: target= "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "TaskStubImpl"; // const-string v4, "TaskStubImpl"
android.util.Slog .d ( v4,v3 );
/* .line 404 */
} // .end local v1 # "targetChange":Lcom/android/server/wm/Transition$ChangeInfo;
} // .end local v2 # "target":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 413 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
public void clearRootProcess ( com.android.server.wm.WindowProcessController p0, com.android.server.wm.Task p1 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/wm/WindowProcessController; */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .line 227 */
if ( p1 != null) { // if-eqz p1, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_3
v0 = (( com.android.server.wm.WindowProcessController ) p1 ).isRemoved ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowProcessController;->isRemoved()Z
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = (( com.android.server.wm.Task ) p2 ).getNonFinishingActivityCount ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getNonFinishingActivityCount()I
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_0 */
/* .line 230 */
} // :cond_0
v0 = this.affinity;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.affinity;
final String v1 = "com.xiaomi.vipaccount"; // const-string v1, "com.xiaomi.vipaccount"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
return;
/* .line 231 */
} // :cond_1
(( com.android.server.wm.Task ) p2 ).getTopNonFinishingActivity ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 232 */
/* .local v0, "top":Lcom/android/server/wm/ActivityRecord; */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = this.app;
/* if-eq v1, p1, :cond_2 */
v1 = com.android.server.wm.TaskStubImpl.mNeedFinishAct;
v1 = v2 = this.shortComponentName;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 233 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Only left "; // const-string v2, "Only left "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.shortComponentName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " in task "; // const-string v2, " in task "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p2, Lcom/android/server/wm/Task;->mTaskId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " and finish it."; // const-string v2, " and finish it."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "TaskStubImpl"; // const-string v2, "TaskStubImpl"
android.util.Slog .d ( v2,v1 );
/* .line 234 */
final String v1 = "clearNonAppAct"; // const-string v1, "clearNonAppAct"
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.ActivityRecord ) v0 ).finishIfPossible ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/ActivityRecord;->finishIfPossible(Ljava/lang/String;Z)I
/* .line 236 */
} // :cond_2
return;
/* .line 228 */
} // .end local v0 # "top":Lcom/android/server/wm/ActivityRecord;
} // :cond_3
} // :goto_0
return;
} // .end method
public void clearSizeCompatInSplitScreen ( Integer p0, Integer p1, com.android.server.wm.WindowContainer p2 ) {
/* .locals 3 */
/* .param p1, "prevWinMode" # I */
/* .param p2, "newWinMode" # I */
/* .param p3, "wc" # Lcom/android/server/wm/WindowContainer; */
/* .line 268 */
/* sget-boolean v0, Landroid/os/Build;->IS_MIUI:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v0, :cond_1 */
/* .line 269 */
if ( p3 != null) { // if-eqz p3, :cond_0
(( com.android.server.wm.WindowContainer ) p3 ).asTask ( ); // invoke-virtual {p3}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 270 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
} // :goto_0
int v1 = 6; // const/4 v1, 0x6
/* if-ne p2, v1, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 271 */
v2 = (( com.android.server.wm.Task ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-ne v2, v1, :cond_1 */
/* .line 272 */
(( com.android.server.wm.Task ) v0 ).getParent ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getParent()Lcom/android/server/wm/WindowContainer;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 273 */
(( com.android.server.wm.Task ) v0 ).getParent ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getParent()Lcom/android/server/wm/WindowContainer;
v2 = (( com.android.server.wm.WindowContainer ) v2 ).getWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
/* if-ne v2, v1, :cond_1 */
/* .line 274 */
/* new-instance v1, Lcom/android/server/wm/TaskStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v1}, Lcom/android/server/wm/TaskStubImpl$$ExternalSyntheticLambda1;-><init>()V */
(( com.android.server.wm.Task ) v0 ).forAllActivities ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/Task;->forAllActivities(Ljava/util/function/Consumer;)V
/* .line 281 */
} // .end local v0 # "task":Lcom/android/server/wm/Task;
} // :cond_1
return;
} // .end method
public void clearVirtualCamera ( com.android.server.wm.WindowProcessController p0, com.android.server.wm.Task p1 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/wm/WindowProcessController; */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .line 239 */
if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez p2, :cond_0 */
/* .line 241 */
} // :cond_0
v0 = this.affinity;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.affinity;
final String v1 = "com.xiaomi.vtcamera"; // const-string v1, "com.xiaomi.vtcamera"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
return;
/* .line 242 */
} // :cond_1
(( com.android.server.wm.Task ) p2 ).getTopNonFinishingActivity ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 243 */
/* .local v0, "top":Lcom/android/server/wm/ActivityRecord; */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = this.app;
/* if-eq v1, p1, :cond_2 */
v1 = com.android.server.wm.TaskStubImpl.mVtCameraNeedFinishAct;
v1 = v2 = this.shortComponentName;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 244 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Only left "; // const-string v2, "Only left "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.shortComponentName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " in task "; // const-string v2, " in task "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p2, Lcom/android/server/wm/Task;->mTaskId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " and finish it."; // const-string v2, " and finish it."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "TaskStubImpl"; // const-string v2, "TaskStubImpl"
android.util.Slog .d ( v2,v1 );
/* .line 245 */
final String v1 = "clearVtCameraAct"; // const-string v1, "clearVtCameraAct"
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.ActivityRecord ) v0 ).finishIfPossible ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/ActivityRecord;->finishIfPossible(Ljava/lang/String;Z)I
/* .line 247 */
} // :cond_2
return;
/* .line 239 */
} // .end local v0 # "top":Lcom/android/server/wm/ActivityRecord;
} // :cond_3
} // :goto_0
return;
} // .end method
public Boolean disallowEnterPip ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "toFrontActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 377 */
if ( p1 != null) { // if-eqz p1, :cond_0
(( com.android.server.wm.ActivityRecord ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;
final String v1 = "com.tencent.mm/.plugin.base.stub.WXEntryActivity"; // const-string v1, "com.tencent.mm/.plugin.base.stub.WXEntryActivity"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 379 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Skip pip enter request for transient activity : "; // const-string v1, "Skip pip enter request for transient activity : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "TaskStubImpl"; // const-string v1, "TaskStubImpl"
android.util.Slog .i ( v1,v0 );
/* .line 380 */
int v0 = 1; // const/4 v0, 0x1
/* .line 382 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.util.List getActivities ( java.util.function.Predicate p0, Boolean p1, com.android.server.wm.ActivityRecord p2, com.android.server.wm.WindowList p3 ) {
/* .locals 5 */
/* .param p2, "traverseTopToBottom" # Z */
/* .param p3, "boundary" # Lcom/android/server/wm/ActivityRecord; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/function/Predicate<", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* ">;Z", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* "Lcom/android/server/wm/WindowList<", */
/* "+", */
/* "Lcom/android/server/wm/WindowContainer;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 181 */
/* .local p1, "callback":Ljava/util/function/Predicate;, "Ljava/util/function/Predicate<Lcom/android/server/wm/ActivityRecord;>;" */
/* .local p4, "children":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<+Lcom/android/server/wm/WindowContainer;>;" */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
/* .line 182 */
/* .local v0, "activityRecordList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;" */
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 183 */
v1 = (( com.android.server.wm.WindowList ) p4 ).size ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowList;->size()I
/* add-int/lit8 v1, v1, -0x1 */
/* .local v1, "i":I */
} // :goto_0
/* if-ltz v1, :cond_2 */
/* .line 184 */
(( com.android.server.wm.WindowList ) p4 ).get ( v1 ); // invoke-virtual {p4, v1}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/WindowContainer; */
/* .line 186 */
/* .local v2, "wc":Lcom/android/server/wm/WindowContainer; */
/* if-ne v2, p3, :cond_0 */
/* .line 187 */
/* .line 188 */
/* .line 190 */
} // :cond_0
(( com.android.server.wm.WindowContainer ) v2 ).getActivity ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/wm/WindowContainer;->getActivity(Ljava/util/function/Predicate;ZLcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;
/* .line 191 */
/* .local v3, "r":Lcom/android/server/wm/ActivityRecord; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 192 */
/* .line 183 */
} // .end local v2 # "wc":Lcom/android/server/wm/WindowContainer;
} // .end local v3 # "r":Lcom/android/server/wm/ActivityRecord;
} // :cond_1
/* add-int/lit8 v1, v1, -0x1 */
} // .end local v1 # "i":I
} // :cond_2
} // :goto_1
/* .line 196 */
} // :cond_3
v1 = (( com.android.server.wm.WindowList ) p4 ).size ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowList;->size()I
/* .line 197 */
/* .local v1, "count":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_2
/* if-ge v2, v1, :cond_6 */
/* .line 198 */
(( com.android.server.wm.WindowList ) p4 ).get ( v2 ); // invoke-virtual {p4, v2}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/WindowContainer; */
/* .line 200 */
/* .local v3, "wc":Lcom/android/server/wm/WindowContainer; */
/* if-ne v3, p3, :cond_4 */
/* .line 201 */
/* .line 202 */
/* .line 204 */
} // :cond_4
(( com.android.server.wm.WindowContainer ) v3 ).getActivity ( p1, p2, p3 ); // invoke-virtual {v3, p1, p2, p3}, Lcom/android/server/wm/WindowContainer;->getActivity(Ljava/util/function/Predicate;ZLcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;
/* .line 205 */
/* .local v4, "r":Lcom/android/server/wm/ActivityRecord; */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 206 */
/* .line 197 */
} // .end local v3 # "wc":Lcom/android/server/wm/WindowContainer;
} // .end local v4 # "r":Lcom/android/server/wm/ActivityRecord;
} // :cond_5
/* add-int/lit8 v2, v2, 0x1 */
/* .line 210 */
} // .end local v1 # "count":I
} // .end local v2 # "i":I
} // :cond_6
} // :goto_3
} // .end method
public Integer getLayoutInDisplayCutoutMode ( android.view.WindowManager$LayoutParams p0 ) {
/* .locals 1 */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 215 */
/* if-nez p1, :cond_0 */
/* .line 216 */
int v0 = 0; // const/4 v0, 0x0
/* .line 222 */
} // :cond_0
/* iget v0, p1, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
} // .end method
public Boolean inSplitScreenWindowingMode ( com.android.server.wm.Task p0 ) {
/* .locals 6 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 251 */
v0 = this.mAtmService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 252 */
try { // :try_start_0
v1 = (( com.android.server.wm.Task ) p1 ).isRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isRootTask()Z
int v2 = 6; // const/4 v2, 0x6
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 253 */
/* iget-boolean v1, p1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( com.android.server.wm.Task ) p1 ).getWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-ne v1, v4, :cond_0 */
/* .line 254 */
v1 = (( com.android.server.wm.Task ) p1 ).hasChild ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->hasChild()Z
if ( v1 != null) { // if-eqz v1, :cond_0
(( com.android.server.wm.Task ) p1 ).getTopChild ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
if ( v1 != null) { // if-eqz v1, :cond_0
(( com.android.server.wm.Task ) p1 ).getTopChild ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v1 ).asTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 255 */
(( com.android.server.wm.Task ) p1 ).getTopChild ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
v1 = (( com.android.server.wm.WindowContainer ) v1 ).getWindowingMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
/* if-ne v1, v2, :cond_0 */
/* move v3, v4 */
} // :cond_0
/* nop */
} // :goto_0
/* monitor-exit v0 */
/* .line 253 */
/* .line 257 */
} // :cond_1
(( com.android.server.wm.Task ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
/* .line 258 */
/* .local v1, "rootTask":Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* iget-boolean v5, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
v5 = (( com.android.server.wm.Task ) v1 ).hasChild ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->hasChild()Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 259 */
v5 = (( com.android.server.wm.Task ) v1 ).getWindowingMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-ne v5, v4, :cond_2 */
/* .line 260 */
(( com.android.server.wm.Task ) v1 ).getTopChild ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
if ( v5 != null) { // if-eqz v5, :cond_2
(( com.android.server.wm.Task ) v1 ).getTopChild ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v5 ).asTask ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 261 */
(( com.android.server.wm.Task ) v1 ).getTopChild ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
v5 = (( com.android.server.wm.WindowContainer ) v5 ).getWindowingMode ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
/* if-ne v5, v2, :cond_2 */
/* move v3, v4 */
} // :cond_2
/* nop */
} // :goto_1
/* monitor-exit v0 */
/* .line 258 */
/* .line 263 */
} // .end local v1 # "rootTask":Lcom/android/server/wm/Task;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isHierarchyBottom ( ) {
/* .locals 1 */
/* .line 137 */
/* iget-boolean v0, p0, Lcom/android/server/wm/TaskStubImpl;->isHierarchyBottom:Z */
} // .end method
public Boolean isSplitScreenModeDismissed ( com.android.server.wm.Task p0 ) {
/* .locals 1 */
/* .param p1, "root" # Lcom/android/server/wm/Task; */
/* .line 103 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* new-instance v0, Lcom/android/server/wm/TaskStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p1}, Lcom/android/server/wm/TaskStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/Task;)V */
(( com.android.server.wm.Task ) p1 ).getTask ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->getTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void notifyActivityPipModeChangedForHoverMode ( Boolean p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 2 */
/* .param p1, "inPip" # Z */
/* .param p2, "ar" # Lcom/android/server/wm/ActivityRecord; */
/* .line 294 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 295 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 296 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 297 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onActivityPipModeChangedForHoverMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onActivityPipModeChangedForHoverMode(ZLcom/android/server/wm/ActivityRecord;)V
/* .line 299 */
} // :cond_0
return;
} // .end method
public void notifyFreeformModeFocus ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "mode" # I */
/* .line 170 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).notifyFreeformModeFocus ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyFreeformModeFocus(Ljava/lang/String;I)V
/* .line 171 */
return;
} // .end method
public void notifyMovetoFront ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "inFreeformSmallWinMode" # Z */
/* .line 166 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).notifyMovetoFront ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyMovetoFront(IZ)V
/* .line 167 */
return;
} // .end method
public void notifyMultitaskLaunch ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 174 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).notifyMultitaskLaunch ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyMultitaskLaunch(ILjava/lang/String;)V
/* .line 175 */
return;
} // .end method
public void onHoverModeTaskParentChanged ( com.android.server.wm.Task p0, com.android.server.wm.WindowContainer p1 ) {
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "newParent" # Lcom/android/server/wm/WindowContainer; */
/* .line 302 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 303 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 304 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 305 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onHoverModeTaskParentChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onHoverModeTaskParentChanged(Lcom/android/server/wm/Task;Lcom/android/server/wm/WindowContainer;)V
/* .line 307 */
} // :cond_0
return;
} // .end method
public void onHoverModeTaskPrepareSurfaces ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 286 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 287 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 288 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 289 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onHoverModeTaskPrepareSurfaces ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->onHoverModeTaskPrepareSurfaces(Lcom/android/server/wm/Task;)V
/* .line 291 */
} // :cond_0
return;
} // .end method
public void onSplitScreenParentChanged ( com.android.server.wm.WindowContainer p0, com.android.server.wm.WindowContainer p1 ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/wm/WindowContainer<", */
/* "*>;", */
/* "Lcom/android/server/wm/WindowContainer<", */
/* "*>;)V" */
/* } */
} // .end annotation
/* .line 108 */
/* .local p1, "oldParent":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;" */
/* .local p2, "newParent":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;" */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 109 */
/* .local v0, "start":J */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
(( com.android.server.wm.WindowContainer ) p1 ).asTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 110 */
v4 = (( com.android.server.wm.WindowContainer ) p1 ).inSplitScreenWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->inSplitScreenWindowingMode()Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* move v4, v3 */
} // :cond_0
/* move v4, v2 */
/* .line 111 */
/* .local v4, "oldInSpliScreenMode":Z */
} // :goto_0
if ( p2 != null) { // if-eqz p2, :cond_1
(( com.android.server.wm.WindowContainer ) p2 ).asTaskDisplayArea ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 112 */
v5 = (( com.android.server.wm.WindowContainer ) p2 ).getWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
/* if-ne v5, v3, :cond_1 */
/* move v2, v3 */
} // :cond_1
/* nop */
/* .line 113 */
/* .local v2, "newInFullScreenMode":Z */
} // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_5
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 114 */
(( com.android.server.wm.WindowContainer ) p1 ).asTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.Task ) v5 ).getRootTask ( ); // invoke-virtual {v5}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
v5 = (( com.android.server.wm.TaskStubImpl ) p0 ).isSplitScreenModeDismissed ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/wm/TaskStubImpl;->isSplitScreenModeDismissed(Lcom/android/server/wm/Task;)Z
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 115 */
(( com.android.server.wm.WindowContainer ) p2 ).asTaskDisplayArea ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
(( com.android.server.wm.TaskDisplayArea ) v5 ).getTopRootTaskInWindowingMode ( v3 ); // invoke-virtual {v5, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;
/* .line 116 */
/* .local v3, "task":Lcom/android/server/wm/Task; */
/* if-nez v3, :cond_2 */
/* .line 117 */
return;
/* .line 119 */
} // :cond_2
(( com.android.server.wm.Task ) v3 ).topRunningActivityLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 120 */
/* .local v5, "topActivity":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v5, :cond_3 */
/* .line 121 */
return;
/* .line 123 */
} // :cond_3
android.appcompat.ApplicationCompatUtilsStub .get ( );
v6 = (( android.appcompat.ApplicationCompatUtilsStub ) v6 ).isContinuityEnabled ( ); // invoke-virtual {v6}, Landroid/appcompat/ApplicationCompatUtilsStub;->isContinuityEnabled()Z
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 124 */
com.android.server.wm.AppContinuityRouterStub .get ( );
(( com.android.server.wm.AppContinuityRouterStub ) v6 ).onSplitToFullScreenChanged ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/wm/AppContinuityRouterStub;->onSplitToFullScreenChanged(Lcom/android/server/wm/ActivityRecord;)V
/* .line 126 */
} // :cond_4
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v6 ).onForegroundActivityChangedLocked ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V
/* .line 128 */
com.android.server.wm.PassivePenAppWhiteListImpl .getInstance ( );
(( com.android.server.wm.PassivePenAppWhiteListImpl ) v6 ).onSplitScreenExit ( ); // invoke-virtual {v6}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->onSplitScreenExit()V
/* .line 130 */
} // .end local v3 # "task":Lcom/android/server/wm/Task;
} // .end local v5 # "topActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_5
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v5 */
/* sub-long/2addr v5, v0 */
/* .line 131 */
/* .local v5, "took":J */
/* const-wide/16 v7, 0x32 */
/* cmp-long v3, v5, v7 */
/* if-lez v3, :cond_6 */
/* .line 132 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "onSplitScreenParentChanged took "; // const-string v7, "onSplitScreenParentChanged took "
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "ms"; // const-string v7, "ms"
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "TaskStubImpl"; // const-string v7, "TaskStubImpl"
android.util.Slog .d ( v7,v3 );
/* .line 134 */
} // :cond_6
return;
} // .end method
public void onTaskConfigurationChanged ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "prevWindowingMode" # I */
/* .param p2, "overrideWindowingMode" # I */
/* .line 310 */
/* const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 311 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal; */
/* .line 312 */
/* .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.wm.MiuiHoverModeInternal ) v0 ).isDeviceInOpenedOrHalfOpenedState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 313 */
(( com.android.server.wm.MiuiHoverModeInternal ) v0 ).onTaskConfigurationChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onTaskConfigurationChanged(II)V
/* .line 315 */
} // :cond_0
return;
} // .end method
public void setHierarchy ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "bottom" # Z */
/* .line 141 */
/* iput-boolean p1, p0, Lcom/android/server/wm/TaskStubImpl;->isHierarchyBottom:Z */
/* .line 142 */
return;
} // .end method
public Boolean shouldSkipFreeFormActivityRecord ( java.util.ArrayList p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 7 */
/* .param p2, "freeformActivityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/wm/Transition$ChangeInfo;", */
/* ">;", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 417 */
/* .local p1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Transition$ChangeInfo;>;" */
int v0 = 0; // const/4 v0, 0x0
if ( p2 != null) { // if-eqz p2, :cond_4
v1 = (( com.android.server.wm.ActivityRecord ) p2 ).inFreeformWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z
/* if-nez v1, :cond_0 */
/* .line 420 */
} // :cond_0
v1 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
int v2 = 1; // const/4 v2, 0x1
/* sub-int/2addr v1, v2 */
/* .local v1, "i":I */
} // :goto_0
/* if-ltz v1, :cond_3 */
/* .line 421 */
(( java.util.ArrayList ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/Transition$ChangeInfo; */
/* .line 422 */
/* .local v3, "targetChange":Lcom/android/server/wm/Transition$ChangeInfo; */
v4 = this.mContainer;
/* .line 423 */
/* .local v4, "target":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;" */
v5 = (( java.lang.Object ) v4 ).equals ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_2 */
(( com.android.server.wm.WindowContainer ) v4 ).asWallpaperToken ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asWallpaperToken()Lcom/android/server/wm/WallpaperWindowToken;
/* if-nez v5, :cond_2 */
/* .line 424 */
(( com.android.server.wm.WindowContainer ) v4 ).asActivityRecord ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
if ( v5 != null) { // if-eqz v5, :cond_1
(( com.android.server.wm.WindowContainer ) v4 ).getParent ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->getParent()Lcom/android/server/wm/WindowContainer;
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 425 */
(( com.android.server.wm.WindowContainer ) v4 ).getParent ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->getParent()Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.ActivityRecord ) p2 ).getTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
v5 = (( java.lang.Object ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 426 */
/* .line 428 */
} // :cond_1
/* .line 420 */
} // .end local v3 # "targetChange":Lcom/android/server/wm/Transition$ChangeInfo;
} // .end local v4 # "target":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
} // :cond_2
} // :goto_1
/* add-int/lit8 v1, v1, -0x1 */
/* .line 430 */
} // .end local v1 # "i":I
} // :cond_3
/* .line 418 */
} // :cond_4
} // :goto_2
} // .end method
public Boolean skipPinnedTaskIfNeeded ( com.android.server.wm.WindowContainer p0, com.android.server.wm.Transition$ChangeInfo p1, android.util.ArraySet p2 ) {
/* .locals 5 */
/* .param p1, "container" # Lcom/android/server/wm/WindowContainer; */
/* .param p2, "changeInfo" # Lcom/android/server/wm/Transition$ChangeInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/wm/WindowContainer;", */
/* "Lcom/android/server/wm/Transition$ChangeInfo;", */
/* "Landroid/util/ArraySet<", */
/* "Lcom/android/server/wm/WindowContainer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 353 */
/* .local p3, "participants":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowContainer;>;" */
v0 = (( com.android.server.wm.WindowContainer ) p1 ).inPinnedWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->inPinnedWindowingMode()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_7
v0 = (( com.android.server.wm.WindowContainer ) p1 ).isVisibleRequested ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->isVisibleRequested()Z
/* if-nez v0, :cond_7 */
/* iget-boolean v0, p2, Lcom/android/server/wm/Transition$ChangeInfo;->mVisible:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
v0 = this.mTransitionController;
/* .line 355 */
v0 = (( com.android.server.wm.TransitionController ) v0 ).getCollectingTransitionType ( ); // invoke-virtual {v0}, Lcom/android/server/wm/TransitionController;->getCollectingTransitionType()I
int v2 = 1; // const/4 v2, 0x1
/* if-eq v0, v2, :cond_0 */
/* .line 359 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 360 */
/* .local v0, "activitySwitchInFreeform":Z */
v3 = (( android.util.ArraySet ) p3 ).size ( ); // invoke-virtual {p3}, Landroid/util/ArraySet;->size()I
/* sub-int/2addr v3, v2 */
/* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_5 */
/* .line 361 */
(( android.util.ArraySet ) p3 ).valueAt ( v3 ); // invoke-virtual {p3, v3}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/WindowContainer; */
/* .line 362 */
/* .local v2, "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;" */
v4 = (( java.lang.Object ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 363 */
} // :cond_1
v4 = (( com.android.server.wm.WindowContainer ) v2 ).inPinnedWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->inPinnedWindowingMode()Z
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = (( com.android.server.wm.WindowContainer ) v2 ).isVisibleRequested ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->isVisibleRequested()Z
/* if-nez v4, :cond_2 */
/* .line 364 */
} // :cond_2
(( com.android.server.wm.WindowContainer ) v2 ).asActivityRecord ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
if ( v4 != null) { // if-eqz v4, :cond_4
v4 = (( com.android.server.wm.WindowContainer ) v2 ).inFreeformWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->inFreeformWindowingMode()Z
/* if-nez v4, :cond_3 */
/* .line 367 */
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
/* .line 360 */
} // .end local v2 # "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
} // :goto_1
/* add-int/lit8 v3, v3, -0x1 */
/* .line 365 */
/* .restart local v2 # "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;" */
} // :cond_4
} // :goto_2
/* .line 370 */
} // .end local v2 # "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
} // .end local v3 # "i":I
} // :cond_5
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 371 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "skip pip task " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " for activity switch in freeform task"; // const-string v2, " for activity switch in freeform task"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "TaskStubImpl"; // const-string v2, "TaskStubImpl"
android.util.Slog .i ( v2,v1 );
/* .line 373 */
} // :cond_6
/* .line 356 */
} // .end local v0 # "activitySwitchInFreeform":Z
} // :cond_7
} // :goto_3
} // .end method
public Boolean skipSplitScreenTaskIfNeeded ( com.android.server.wm.WindowContainer p0, android.util.ArraySet p1 ) {
/* .locals 8 */
/* .param p1, "container" # Lcom/android/server/wm/WindowContainer; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/wm/WindowContainer;", */
/* "Landroid/util/ArraySet<", */
/* "Lcom/android/server/wm/WindowContainer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 320 */
/* .local p2, "participants":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowContainer;>;" */
(( com.android.server.wm.WindowContainer ) p1 ).asTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_8
v0 = (( com.android.server.wm.WindowContainer ) p1 ).inSplitScreenWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->inSplitScreenWindowingMode()Z
/* if-nez v0, :cond_0 */
/* .line 322 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 323 */
/* .local v0, "taskChildInParticipants":Z */
v2 = this.mChildren;
v2 = (( com.android.server.wm.WindowList ) v2 ).size ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowList;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_2 */
/* .line 324 */
(( com.android.server.wm.WindowContainer ) p1 ).getChildAt ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/wm/WindowContainer;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
/* .line 325 */
/* .local v3, "child":Lcom/android/server/wm/WindowContainer; */
v4 = (( android.util.ArraySet ) p2 ).contains ( v3 ); // invoke-virtual {p2, v3}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 326 */
int v0 = 1; // const/4 v0, 0x1
/* .line 327 */
/* .line 323 */
} // .end local v3 # "child":Lcom/android/server/wm/WindowContainer;
} // :cond_1
/* add-int/lit8 v2, v2, -0x1 */
/* .line 330 */
} // .end local v2 # "i":I
} // :cond_2
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 332 */
} // :cond_3
int v2 = 0; // const/4 v2, 0x0
/* .line 333 */
/* .local v2, "otherTaskChildInParticipants":Z */
v3 = (( android.util.ArraySet ) p2 ).size ( ); // invoke-virtual {p2}, Landroid/util/ArraySet;->size()I
/* add-int/lit8 v3, v3, -0x1 */
/* .local v3, "i":I */
} // :goto_2
/* if-ltz v3, :cond_7 */
/* .line 334 */
(( android.util.ArraySet ) p2 ).valueAt ( v3 ); // invoke-virtual {p2, v3}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/wm/WindowContainer; */
/* .line 335 */
/* .local v4, "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;" */
v5 = (( java.lang.Object ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 336 */
} // :cond_4
(( com.android.server.wm.WindowContainer ) v4 ).asTask ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
if ( v5 != null) { // if-eqz v5, :cond_6
v5 = (( com.android.server.wm.WindowContainer ) v4 ).inSplitScreenWindowingMode ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->inSplitScreenWindowingMode()Z
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 337 */
v5 = this.mChildren;
v5 = (( com.android.server.wm.WindowList ) v5 ).size ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowList;->size()I
/* add-int/lit8 v5, v5, -0x1 */
/* .local v5, "j":I */
} // :goto_3
/* if-ltz v5, :cond_6 */
/* .line 338 */
v6 = this.mChildren;
(( com.android.server.wm.WindowList ) v6 ).get ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wm/WindowContainer; */
/* .line 339 */
/* .local v6, "child":Lcom/android/server/wm/WindowContainer; */
v7 = (( android.util.ArraySet ) p2 ).contains ( v6 ); // invoke-virtual {p2, v6}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 340 */
int v2 = 1; // const/4 v2, 0x1
/* .line 341 */
v7 = (( java.lang.Object ) p1 ).equals ( v6 ); // invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 337 */
} // .end local v6 # "child":Lcom/android/server/wm/WindowContainer;
} // :cond_5
/* add-int/lit8 v5, v5, -0x1 */
/* .line 333 */
} // .end local v4 # "wc":Lcom/android/server/wm/WindowContainer;, "Lcom/android/server/wm/WindowContainer<*>;"
} // .end local v5 # "j":I
} // :cond_6
} // :goto_4
/* add-int/lit8 v3, v3, -0x1 */
/* .line 346 */
} // .end local v3 # "i":I
} // :cond_7
/* .line 320 */
} // .end local v0 # "taskChildInParticipants":Z
} // .end local v2 # "otherTaskChildInParticipants":Z
} // :cond_8
} // :goto_5
} // .end method
public void updateForegroundActivityInAppPair ( com.android.server.wm.Task p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "isInAppPair" # Z */
/* .line 145 */
/* if-nez p2, :cond_3 */
/* .line 146 */
(( com.android.server.wm.Task ) p1 ).getDisplayArea ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 147 */
/* .local v0, "displayArea":Lcom/android/server/wm/TaskDisplayArea; */
/* if-nez v0, :cond_0 */
return;
/* .line 148 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.wm.TaskDisplayArea ) v0 ).getTopRootTaskInWindowingMode ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;
/* .line 149 */
/* .local v1, "topStack":Lcom/android/server/wm/Task; */
/* if-nez v1, :cond_1 */
return;
/* .line 150 */
} // :cond_1
(( com.android.server.wm.Task ) v1 ).topRunningActivityLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 151 */
/* .local v2, "topActivity":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v2, :cond_2 */
return;
/* .line 152 */
} // :cond_2
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
/* .line 153 */
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v3 ).onForegroundActivityChangedLocked ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V
/* .line 154 */
} // .end local v0 # "displayArea":Lcom/android/server/wm/TaskDisplayArea;
} // .end local v1 # "topStack":Lcom/android/server/wm/Task;
} // .end local v2 # "topActivity":Lcom/android/server/wm/ActivityRecord;
/* .line 155 */
} // :cond_3
(( com.android.server.wm.Task ) p1 ).topRunningActivityLocked ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 156 */
/* .local v0, "topActivity":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v0, :cond_4 */
/* .line 157 */
return;
/* .line 159 */
} // :cond_4
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
/* .line 160 */
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v1 ).onForegroundActivityChangedLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V
/* .line 162 */
} // .end local v0 # "topActivity":Lcom/android/server/wm/ActivityRecord;
} // :goto_0
return;
} // .end method
