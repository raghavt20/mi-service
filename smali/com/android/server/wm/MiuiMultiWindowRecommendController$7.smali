.class Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->addFreeFormRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

.field final synthetic val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 242
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iput-object p2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 246
    const-string v0, "MiuiMultiWindowRecommendController"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmFreeFormRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V

    .line 247
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x110c000f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/FreeFormRecommendLayout;

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/FreeFormRecommendLayout;)V

    .line 249
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 250
    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getInsetsStateController()Lcom/android/server/wm/InsetsStateController;

    move-result-object v1

    .line 249
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getStatusBarHeight(Lcom/android/server/wm/InsetsStateController;Z)I

    move-result v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v3, v3, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 251
    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/wm/DisplayContent;->mDisplayFrames:Lcom/android/server/wm/DisplayFrames;

    invoke-static {v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getDisplayCutoutHeight(Lcom/android/server/wm/DisplayFrames;)I

    move-result v3

    .line 249
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 252
    .local v1, "statusBarHeight":I
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/android/server/wm/FreeFormRecommendLayout;->createLayoutParams(I)Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmLayoutParams(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Landroid/view/WindowManager$LayoutParams;)V

    .line 253
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/wm/FreeFormRecommendLayout;->getFreeFormIconContainer()Landroid/widget/RelativeLayout;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmFreeFormRecommendIconContainer(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Landroid/widget/RelativeLayout;)V

    .line 254
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    invoke-virtual {v4}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v5}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 255
    invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformUserId()I

    move-result v6

    .line 254
    invoke-static {v4, v5, v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->loadDrawableByPackageName(Ljava/lang/String;Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/server/wm/FreeFormRecommendLayout;->setFreeFormIcon(Landroid/graphics/drawable/Drawable;)V

    .line 256
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->freeFormClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/android/server/wm/FreeFormRecommendLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    const/4 v3, 0x1

    new-array v4, v3, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v5}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v4}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v4

    invoke-interface {v4}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v5}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v5

    new-array v2, v2, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {v4, v5, v2}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    .line 258
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v4, 0x2

    if-ge v2, v4, :cond_2

    .line 259
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mWindowManager:Landroid/view/WindowManager;

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v5}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmLayoutParams(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 260
    invoke-static {}, Landroid/view/inspector/WindowInspector;->getGlobalWindowViews()Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v5}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 261
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v4, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setFreeFormRecommendState(Z)V

    .line 262
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->startMultiWindowRecommendAnimation(Landroid/view/View;Z)V

    .line 263
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendViewForTimer()V

    .line 264
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v3, v3, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v3, v3, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v3, v3, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v3, v3, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    if-eqz v3, :cond_0

    .line 265
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v3, v3, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v3, v3, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v3, v3, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v3, v3, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 266
    invoke-virtual {v4}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 267
    invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 266
    invoke-virtual {v3, v4, v5}, Lcom/android/server/wm/MiuiFreeformTrackManager;->trackFreeFormRecommendExposeEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_0
    return-void

    .line 258
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 272
    .end local v2    # "i":I
    :cond_2
    const-string v2, " addFreeFormRecommendView twice fail"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    nop

    .end local v1    # "statusBarHeight":I
    goto :goto_1

    .line 273
    :catch_0
    move-exception v1

    .line 274
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendViewForTimer()V

    .line 275
    const-string v2, " addFreeFormRecommendView fail"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 277
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
