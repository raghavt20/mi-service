.class public Lcom/android/server/wm/MiuiDisplayReportImpl;
.super Ljava/lang/Object;
.source "MiuiDisplayReportImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiDisplayReportStub;


# static fields
.field private static final TAG:Ljava/lang/String; = "ReportHelper"

.field private static final WHITE_LIST_FOR_TRACING:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAmInternal:Landroid/app/ActivityManagerInternal;

.field private mShellCmd:Landroid/os/ShellCommand;

.field private mWmInternal:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public static synthetic $r8$lambda$iHxXkgqpVSSYc7hnUtkP-iwSsKY(Lcom/android/server/wm/MiuiDisplayReportImpl;Landroid/view/IWindow;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiDisplayReportImpl;->lambda$dumpLocalWindowAsync$1(Landroid/view/IWindow;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    return-void
.end method

.method public static synthetic $r8$lambda$m7i2XhbOeX96SqfxF5uHHAvjAdk(Lcom/android/server/wm/MiuiDisplayReportImpl;Ljava/lang/String;Ljava/lang/String;[Lcom/android/internal/os/ByteTransferPipe;Lcom/android/server/wm/WindowState;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiDisplayReportImpl;->lambda$dumpViewCaptures$0(Ljava/lang/String;Ljava/lang/String;[Lcom/android/internal/os/ByteTransferPipe;Lcom/android/server/wm/WindowState;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/wm/MiuiDisplayReportImpl;->WHITE_LIST_FOR_TRACING:Landroid/util/ArraySet;

    .line 28
    const-string v1, "com.miui.uireporter"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 29
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private dumpLocalWindowAsync(Landroid/view/IWindow;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .locals 8
    .param p1, "client"    # Landroid/view/IWindow;
    .param p2, "cmd"    # Ljava/lang/String;
    .param p3, "params"    # Ljava/lang/String;
    .param p4, "pfd"    # Landroid/os/ParcelFileDescriptor;

    .line 110
    invoke-static {}, Lcom/android/server/IoThread;->getExecutor()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v7, Lcom/android/server/wm/MiuiDisplayReportImpl$$ExternalSyntheticLambda0;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiDisplayReportImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiDisplayReportImpl;Landroid/view/IWindow;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    invoke-interface {v0, v7}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 121
    return-void
.end method

.method private synthetic lambda$dumpLocalWindowAsync$1(Landroid/view/IWindow;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .locals 2
    .param p1, "client"    # Landroid/view/IWindow;
    .param p2, "cmd"    # Ljava/lang/String;
    .param p3, "params"    # Ljava/lang/String;
    .param p4, "pfd"    # Landroid/os/ParcelFileDescriptor;

    .line 111
    iget-object v0, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mWmInternal:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 113
    :try_start_0
    invoke-interface {p1, p2, p3, p4}, Landroid/view/IWindow;->executeCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    goto :goto_0

    .line 119
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 114
    :catch_0
    move-exception v1

    .line 117
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 119
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v0

    .line 120
    return-void

    .line 119
    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private synthetic lambda$dumpViewCaptures$0(Ljava/lang/String;Ljava/lang/String;[Lcom/android/internal/os/ByteTransferPipe;Lcom/android/server/wm/WindowState;)V
    .locals 4
    .param p1, "windowName"    # Ljava/lang/String;
    .param p2, "viewName"    # Ljava/lang/String;
    .param p3, "pip"    # [Lcom/android/internal/os/ByteTransferPipe;
    .param p4, "w"    # Lcom/android/server/wm/WindowState;

    .line 78
    invoke-virtual {p4}, Lcom/android/server/wm/WindowState;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p4}, Lcom/android/server/wm/WindowState;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    const/4 v0, 0x0

    .line 81
    .local v0, "pipe":Lcom/android/internal/os/ByteTransferPipe;
    :try_start_0
    new-instance v1, Lcom/android/internal/os/ByteTransferPipe;

    invoke-direct {v1}, Lcom/android/internal/os/ByteTransferPipe;-><init>()V

    move-object v0, v1

    .line 82
    invoke-virtual {v0}, Lcom/android/internal/os/ByteTransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 83
    .local v1, "pfd":Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p4}, Lcom/android/server/wm/WindowState;->isClientLocal()Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "CAPTURE"

    if-eqz v2, :cond_0

    .line 84
    :try_start_1
    iget-object v2, p4, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-direct {p0, v2, v3, p2, v1}, Lcom/android/server/wm/MiuiDisplayReportImpl;->dumpLocalWindowAsync(Landroid/view/IWindow;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    .line 86
    :cond_0
    iget-object v2, p4, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v2, v3, p2, v1}, Landroid/view/IWindow;->executeCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    .line 88
    :goto_0
    const/4 v2, 0x0

    aput-object v0, p3, v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 93
    .end local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    goto :goto_1

    .line 89
    :catch_0
    move-exception v1

    .line 90
    .local v1, "e":Ljava/lang/Exception;
    if-eqz v0, :cond_1

    .line 91
    invoke-virtual {v0}, Lcom/android/internal/os/ByteTransferPipe;->kill()V

    .line 95
    .end local v0    # "pipe":Lcom/android/internal/os/ByteTransferPipe;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-void
.end method


# virtual methods
.method public dumpViewCaptures()I
    .locals 8

    .line 59
    iget-object v0, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mWmInternal:Lcom/android/server/wm/WindowManagerService;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mShellCmd:Landroid/os/ShellCommand;

    if-nez v2, :cond_0

    goto :goto_2

    .line 62
    :cond_0
    const-string v2, "android.permission.DUMP"

    const-string v3, "runDumpViewCapture()"

    invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/WindowManagerService;->checkCallingPermission(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 66
    iget-object v0, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mShellCmd:Landroid/os/ShellCommand;

    invoke-virtual {v0}, Landroid/os/ShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "windowName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mShellCmd:Landroid/os/ShellCommand;

    invoke-virtual {v2}, Landroid/os/ShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, "viewName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    .line 72
    :cond_1
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/android/internal/os/ByteTransferPipe;

    .line 74
    .local v3, "pip":[Lcom/android/internal/os/ByteTransferPipe;
    iget-object v4, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mShellCmd:Landroid/os/ShellCommand;

    invoke-virtual {v4}, Landroid/os/ShellCommand;->getRawOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    check-cast v4, Ljava/io/FileOutputStream;

    .line 75
    .local v4, "out":Ljava/io/FileOutputStream;
    iget-object v5, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mWmInternal:Lcom/android/server/wm/WindowManagerService;

    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v5

    .line 77
    :try_start_0
    iget-object v6, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mWmInternal:Lcom/android/server/wm/WindowManagerService;

    iget-object v6, v6, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    new-instance v7, Lcom/android/server/wm/MiuiDisplayReportImpl$$ExternalSyntheticLambda1;

    invoke-direct {v7, p0, v0, v2, v3}, Lcom/android/server/wm/MiuiDisplayReportImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiDisplayReportImpl;Ljava/lang/String;Ljava/lang/String;[Lcom/android/internal/os/ByteTransferPipe;)V

    invoke-virtual {v6, v7, v1}, Lcom/android/server/wm/RootWindowContainer;->forAllWindows(Ljava/util/function/Consumer;Z)V

    .line 96
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :try_start_1
    aget-object v5, v3, v1

    invoke-virtual {v5}, Lcom/android/internal/os/ByteTransferPipe;->get()[B

    move-result-object v5

    .line 100
    .local v5, "data":[B
    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 103
    .end local v5    # "data":[B
    goto :goto_0

    .line 101
    :catch_0
    move-exception v5

    .line 102
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 104
    .end local v5    # "e":Ljava/io/IOException;
    :goto_0
    return v1

    .line 96
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 69
    .end local v3    # "pip":[Lcom/android/internal/os/ByteTransferPipe;
    .end local v4    # "out":Ljava/io/FileOutputStream;
    :cond_2
    :goto_1
    return v1

    .line 64
    .end local v0    # "windowName":Ljava/lang/String;
    .end local v2    # "viewName":Ljava/lang/String;
    :cond_3
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Requires DUMP permission"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_4
    :goto_2
    return v1
.end method

.method public init(Landroid/os/ShellCommand;Lcom/android/server/wm/WindowManagerService;)V
    .locals 0
    .param p1, "shellcmd"    # Landroid/os/ShellCommand;
    .param p2, "wms"    # Lcom/android/server/wm/WindowManagerService;

    .line 37
    iput-object p1, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mShellCmd:Landroid/os/ShellCommand;

    .line 38
    iput-object p2, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mWmInternal:Lcom/android/server/wm/WindowManagerService;

    .line 39
    return-void
.end method

.method public traceEnableForCaller()Z
    .locals 4

    .line 43
    iget-object v0, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mAmInternal:Landroid/app/ActivityManagerInternal;

    if-nez v0, :cond_0

    .line 44
    const-class v0, Landroid/app/ActivityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mAmInternal:Landroid/app/ActivityManagerInternal;

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mAmInternal:Landroid/app/ActivityManagerInternal;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 47
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 48
    .local v0, "callingPid":I
    iget-object v2, p0, Lcom/android/server/wm/MiuiDisplayReportImpl;->mAmInternal:Landroid/app/ActivityManagerInternal;

    invoke-virtual {v2, v0}, Landroid/app/ActivityManagerInternal;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 50
    return v1

    .line 52
    :cond_1
    sget-object v1, Lcom/android/server/wm/MiuiDisplayReportImpl;->WHITE_LIST_FOR_TRACING:Landroid/util/ArraySet;

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v1

    return v1

    .line 54
    .end local v0    # "callingPid":I
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_2
    return v1
.end method
