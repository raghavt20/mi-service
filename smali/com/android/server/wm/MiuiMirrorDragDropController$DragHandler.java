class com.android.server.wm.MiuiMirrorDragDropController$DragHandler extends android.os.Handler {
	 /* .source "MiuiMirrorDragDropController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiMirrorDragDropController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DragHandler" */
} // .end annotation
/* # instance fields */
private final com.android.server.wm.WindowManagerService mService;
final com.android.server.wm.MiuiMirrorDragDropController this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMirrorDragDropController$DragHandler ( ) {
/* .locals 0 */
/* .param p2, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p3, "looper" # Landroid/os/Looper; */
/* .line 242 */
this.this$0 = p1;
/* .line 243 */
/* invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 244 */
this.mService = p2;
/* .line 245 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 249 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* if-nez v0, :cond_1 */
/* .line 251 */
v0 = this.mService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 253 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.android.server.wm.MiuiMirrorDragDropController .-$$Nest$fgetmDragState ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 254 */
		 v1 = this.this$0;
		 com.android.server.wm.MiuiMirrorDragDropController .-$$Nest$fgetmDragState ( v1 );
		 int v2 = 0; // const/4 v2, 0x0
		 /* iput-boolean v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z */
		 /* .line 255 */
		 v1 = this.this$0;
		 com.android.server.wm.MiuiMirrorDragDropController .-$$Nest$fgetmDragState ( v1 );
		 (( com.android.server.wm.MiuiMirrorDragState ) v1 ).closeLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V
		 /* .line 257 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 259 */
} // :cond_1
} // :goto_0
return;
} // .end method
