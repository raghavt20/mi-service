.class Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;
.super Ljava/lang/Object;
.source "ActivityTaskManagerServiceImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

.field final synthetic val$info:Landroid/content/pm/ApplicationInfo;

.field final synthetic val$reason:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 917
    iput-object p1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    iput-object p2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;->val$info:Landroid/content/pm/ApplicationInfo;

    iput-object p3, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;->val$reason:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 921
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;->val$info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;->val$reason:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->-$$Nest$mstartSubScreenUi(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 924
    goto :goto_0

    .line 922
    :catch_0
    move-exception v0

    .line 923
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ATMSImpl"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
