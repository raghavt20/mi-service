class com.android.server.wm.MiuiSizeCompatJob$2 implements android.os.IBinder$DeathRecipient {
	 /* .source "MiuiSizeCompatJob.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiSizeCompatJob; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiSizeCompatJob this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiSizeCompatJob$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiSizeCompatJob; */
/* .line 126 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 5 */
/* .line 130 */
final String v0 = "MiuiSizeCompatJob"; // const-string v0, "MiuiSizeCompatJob"
final String v1 = "DeathRecipient "; // const-string v1, "DeathRecipient "
android.util.Log .d ( v0,v1 );
/* .line 131 */
v0 = this.this$0;
com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmTrackLock ( v0 );
/* monitor-enter v0 */
/* .line 132 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmITrackBinder ( v1 );
	 /* if-nez v1, :cond_0 */
	 /* .line 133 */
	 /* monitor-exit v0 */
	 return;
	 /* .line 135 */
} // :cond_0
v1 = this.this$0;
com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmITrackBinder ( v1 );
v2 = this.this$0;
com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmDeathRecipient ( v2 );
int v3 = 0; // const/4 v3, 0x0
/* .line 136 */
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fputmITrackBinder ( v1,v2 );
/* .line 137 */
v1 = this.this$0;
com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmH ( v1 );
int v2 = 2; // const/4 v2, 0x2
(( android.os.Handler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 138 */
v1 = this.this$0;
com.android.server.wm.MiuiSizeCompatJob .-$$Nest$fgetmH ( v1 );
/* const-wide/16 v3, 0x12c */
(( android.os.Handler ) v1 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 139 */
/* monitor-exit v0 */
/* .line 140 */
return;
/* .line 139 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
