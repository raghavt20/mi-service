.class Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$3;
.super Landroid/database/ContentObserver;
.source "MiuiMultiWindowRecommendHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 151
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$3;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 154
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 155
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$3;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 156
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$3;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    const/4 v2, -0x1

    const/4 v3, -0x2

    const-string v4, "MiuiMultiWindowRecommendSwitch"

    invoke-static {v0, v4, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-static {v1, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fputmMultiWindowRecommendSwitchEnabled(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Z)V

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " MultiWindowRecommendSwitch change,new mMultiWindowRecommendSwitchEnabled= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$3;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->-$$Nest$fgetmMultiWindowRecommendSwitchEnabled(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiMultiWindowRecommendHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    return-void
.end method
