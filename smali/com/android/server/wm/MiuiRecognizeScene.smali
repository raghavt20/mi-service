.class public final Lcom/android/server/wm/MiuiRecognizeScene;
.super Ljava/lang/Object;
.source "MiuiRecognizeScene.java"


# static fields
.field private static final BIND_FAIL_RETRY_TIME:J = 0xea60L

.field private static final DEBUG:Z = false

.field private static final JOYOSE_PACKAGE:Ljava/lang/String; = "com.xiaomi.joyose"

.field private static final JOYOSE_SERVICE_CLASS:Ljava/lang/String; = "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.SceneRecognizeService"

.field private static final RECOG_SCENE_ENABLE:Ljava/lang/String; = "persist.miui.recog.scene.enable"

.field public static final REC_ENABLE:Z

.field private static final TAG:Ljava/lang/String; = "MiuiRecognizeScene"


# instance fields
.field private mBgHandler:Landroid/os/Handler;

.field private mBindServiceRunnable:Ljava/lang/Runnable;

.field private mContext:Landroid/content/Context;

.field private mDeathHandler:Landroid/os/IBinder$DeathRecipient;

.field private final mPerformanceConnection:Landroid/content/ServiceConnection;

.field private mRecognizeService:Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;


# direct methods
.method public static synthetic $r8$lambda$wfWHYVF_3YrS2gyohN4gVuQ6MYc(Lcom/android/server/wm/MiuiRecognizeScene;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiRecognizeScene;->lambda$new$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmBgHandler(Lcom/android/server/wm/MiuiRecognizeScene;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mBgHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBindServiceRunnable(Lcom/android/server/wm/MiuiRecognizeScene;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mBindServiceRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/MiuiRecognizeScene;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeathHandler(Lcom/android/server/wm/MiuiRecognizeScene;)Landroid/os/IBinder$DeathRecipient;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPerformanceConnection(Lcom/android/server/wm/MiuiRecognizeScene;)Landroid/content/ServiceConnection;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mPerformanceConnection:Landroid/content/ServiceConnection;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRecognizeService(Lcom/android/server/wm/MiuiRecognizeScene;)Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mRecognizeService:Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmRecognizeService(Lcom/android/server/wm/MiuiRecognizeScene;Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mRecognizeService:Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;

    return-void
.end method

.method static bridge synthetic -$$Nest$msendRebindServiceMsg(Lcom/android/server/wm/MiuiRecognizeScene;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiRecognizeScene;->sendRebindServiceMsg()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 27
    const-string v0, "persist.miui.recog.scene.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiRecognizeScene;->REC_ENABLE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/android/server/wm/MiuiRecognizeScene$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiRecognizeScene$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiRecognizeScene;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mBindServiceRunnable:Ljava/lang/Runnable;

    .line 56
    new-instance v0, Lcom/android/server/wm/MiuiRecognizeScene$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiRecognizeScene$1;-><init>(Lcom/android/server/wm/MiuiRecognizeScene;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    .line 104
    new-instance v0, Lcom/android/server/wm/MiuiRecognizeScene$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiRecognizeScene$2;-><init>(Lcom/android/server/wm/MiuiRecognizeScene;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mPerformanceConnection:Landroid/content/ServiceConnection;

    .line 44
    iput-object p1, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mContext:Landroid/content/Context;

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mBgHandler:Landroid/os/Handler;

    .line 46
    sget-boolean v1, Lcom/android/server/wm/MiuiRecognizeScene;->REC_ENABLE:Z

    if-eqz v1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mBindServiceRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 49
    :cond_0
    return-void
.end method

.method private bindService()V
    .locals 5

    .line 79
    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mRecognizeService:Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;

    if-eqz v0, :cond_0

    goto :goto_1

    .line 84
    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 85
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.xiaomi.joyose"

    const-string v2, "com.xiaomi.joyose.smartop.gamebooster.scenerecognize.SceneRecognizeService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    iget-object v1, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mPerformanceConnection:Landroid/content/ServiceConnection;

    sget-object v3, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v2, v4, v3}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 89
    invoke-direct {p0}, Lcom/android/server/wm/MiuiRecognizeScene;->sendRebindServiceMsg()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MiuiRecognizeScene"

    const-string v2, "bindService exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 97
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 81
    :cond_2
    :goto_1
    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 2

    .line 52
    const-string v0, "MiuiRecognizeScene"

    const-string/jumbo v1, "start bind service"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-direct {p0}, Lcom/android/server/wm/MiuiRecognizeScene;->bindService()V

    .line 54
    return-void
.end method

.method private sendRebindServiceMsg()V
    .locals 4

    .line 100
    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mBindServiceRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 101
    iget-object v0, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mBindServiceRunnable:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 102
    return-void
.end method


# virtual methods
.method public recognizeScene(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 65
    const-string v0, "null"

    .line 66
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/MiuiRecognizeScene;->mRecognizeService:Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;

    if-nez v1, :cond_0

    .line 68
    return-object v0

    .line 71
    :cond_0
    :try_start_0
    invoke-interface {v1, p1}, Lcom/xiaomi/joyose/smartop/gamebooster/scenerecognize/IPhashSceneRecognizeInterface;->recognizeScene(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 74
    goto :goto_0

    .line 72
    :catch_0
    move-exception v1

    .line 73
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "MiuiRecognizeScene"

    const-string v3, "recognizeScene exception "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 75
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v0
.end method
