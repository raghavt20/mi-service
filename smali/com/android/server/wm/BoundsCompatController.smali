.class public Lcom/android/server/wm/BoundsCompatController;
.super Lcom/android/server/wm/BoundsCompatControllerStub;
.source "BoundsCompatController.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.wm.BoundsCompatControllerStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/BoundsCompatController$AspectRatioPolicy;
    }
.end annotation


# instance fields
.field private mBounds:Landroid/graphics/Rect;

.field private mBoundsCompatEnabled:Z

.field private mCurrentScale:F

.field private mDispatchNeeded:Z

.field private mDispatchedBounds:Landroid/graphics/Rect;

.field private mDispatchedState:I

.field private mFixedAspectRatio:F

.field private mFixedAspectRatioAvailable:Z

.field private mGlobalScale:F

.field private mGravity:I

.field private mOwner:Lcom/android/server/wm/ActivityRecord;

.field private mState:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 47
    invoke-direct {p0}, Lcom/android/server/wm/BoundsCompatControllerStub;-><init>()V

    .line 56
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F

    .line 66
    iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F

    .line 67
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    return-void
.end method

.method private applyPolicyIfNeeded(Landroid/content/pm/ActivityInfo;)V
    .locals 4
    .param p1, "info"    # Landroid/content/pm/ActivityInfo;

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z

    .line 99
    const/high16 v0, -0x40800000    # -1.0f

    .line 100
    .local v0, "fixedAspectRatio":F
    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v1, v1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v1}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 101
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v1

    invoke-interface {v1}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlip()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    :cond_0
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v1

    invoke-interface {v1}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 104
    .local v1, "task":Lcom/android/server/wm/Task;
    if-eqz v1, :cond_4

    iget-object v2, v1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z

    move-result v2

    if-nez v2, :cond_4

    .line 105
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v2

    invoke-interface {v2}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 106
    invoke-direct {p0}, Lcom/android/server/wm/BoundsCompatController;->getActivityFlipCompatMode()I

    move-result v2

    if-nez v2, :cond_2

    .line 107
    const/4 v0, 0x0

    goto :goto_0

    .line 109
    :cond_2
    const v0, 0x3fdc3e06

    goto :goto_0

    .line 112
    :cond_3
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v2

    iget-object v3, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getAspectRatio(Ljava/lang/String;)F

    move-result v0

    .line 114
    :goto_0
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v2

    iget-object v3, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getAspectGravity(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/server/wm/BoundsCompatController;->mGravity:I

    .line 118
    .end local v1    # "task":Lcom/android/server/wm/Task;
    :cond_4
    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F

    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_5

    .line 119
    iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F

    .line 122
    :cond_5
    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_7

    .line 123
    invoke-static {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->hasDefinedAspectRatio(F)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 124
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z

    .line 126
    :cond_6
    return-void

    .line 128
    :cond_7
    return-void
.end method

.method private aspectRatioPolicyToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "policy"    # I

    .line 187
    packed-switch p1, :pswitch_data_0

    .line 193
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 191
    :pswitch_0
    const-string v0, "FullScreen"

    return-object v0

    .line 189
    :pswitch_1
    const-string v0, "Default"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private canUseFixedAspectRatio(Landroid/content/res/Configuration;)Z
    .locals 2
    .param p1, "parentConfiguration"    # Landroid/content/res/Configuration;

    .line 543
    iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z

    if-eqz v0, :cond_3

    .line 544
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlip()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    .line 545
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v0

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_3

    .line 546
    :cond_0
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->shouldNotApplyAspectRatio(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 548
    :cond_1
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 549
    invoke-direct {p0}, Lcom/android/server/wm/BoundsCompatController;->getActivityFlipCompatMode()I

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 553
    :cond_2
    const/4 v0, 0x1

    return v0

    .line 551
    :cond_3
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private getActivityFlipCompatMode()I
    .locals 4

    .line 569
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByActivity(Lcom/android/server/wm/ActivityRecord;)I

    move-result v0

    .line 570
    .local v0, "activityMode":I
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v3, v3, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 571
    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I

    move-result v1

    .line 573
    .local v1, "appMode":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 574
    return v0

    .line 575
    :cond_0
    if-eq v1, v2, :cond_1

    .line 576
    return v1

    .line 578
    :cond_1
    return v2
.end method

.method private getRequestedConfigurationOrientation(Lcom/android/server/wm/ActivityRecord;)I
    .locals 8
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 482
    iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I

    iget v1, p1, Lcom/android/server/wm/ActivityRecord;->mOriginScreenOrientation:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_1

    iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v3

    .line 486
    .local v0, "useOriginRequestOrientation":Z
    :goto_1
    if-eqz v0, :cond_2

    .line 487
    iget v1, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I

    goto :goto_2

    .line 488
    :cond_2
    iget v1, p1, Lcom/android/server/wm/ActivityRecord;->mOriginScreenOrientation:I

    :goto_2
    nop

    .line 490
    .local v1, "requestedOrientation":I
    const/4 v4, 0x5

    if-ne v1, v4, :cond_3

    .line 492
    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    if-eqz v3, :cond_9

    .line 493
    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getNaturalOrientation()I

    move-result v2

    return v2

    .line 495
    :cond_3
    const/16 v4, 0xe

    if-ne v1, v4, :cond_4

    .line 497
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    return v2

    .line 498
    :cond_4
    invoke-static {v1}, Landroid/content/pm/ActivityInfo;->isFixedOrientationLandscape(I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 499
    const/4 v2, 0x2

    return v2

    .line 500
    :cond_5
    invoke-static {v1}, Landroid/content/pm/ActivityInfo;->isFixedOrientationPortrait(I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 501
    return v3

    .line 502
    :cond_6
    const/4 v3, 0x4

    if-ne v1, v3, :cond_9

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    if-eqz v3, :cond_9

    .line 505
    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplayRotation()Lcom/android/server/wm/DisplayRotation;

    move-result-object v3

    .line 507
    .local v3, "displayRotation":Lcom/android/server/wm/DisplayRotation;
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/android/server/wm/DisplayRotation;->getOrientationListener()Lcom/android/server/wm/WindowOrientationListener;

    move-result-object v4

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    .line 509
    .local v4, "listener":Lcom/android/server/wm/WindowOrientationListener;
    :goto_3
    const/4 v5, -0x1

    if-eqz v4, :cond_8

    .line 510
    invoke-virtual {v4}, Lcom/android/server/wm/WindowOrientationListener;->getProposedRotation()I

    move-result v6

    goto :goto_4

    .line 511
    :cond_8
    move v6, v5

    :goto_4
    nop

    .line 513
    .local v6, "sensorRotation":I
    invoke-direct {p0, v6}, Lcom/android/server/wm/BoundsCompatController;->rotationToOrientation(I)I

    move-result v7

    .line 514
    .local v7, "sensorOrientation":I
    if-eq v7, v5, :cond_9

    .line 515
    return v7

    .line 518
    .end local v3    # "displayRotation":Lcom/android/server/wm/DisplayRotation;
    .end local v4    # "listener":Lcom/android/server/wm/WindowOrientationListener;
    .end local v6    # "sensorRotation":I
    .end local v7    # "sensorOrientation":I
    :cond_9
    return v2
.end method

.method static synthetic lambda$clearSizeCompatMode$0(Lcom/android/server/wm/ActivityRecord;ZLcom/android/server/wm/ActivityRecord;)V
    .locals 2
    .param p0, "excluded"    # Lcom/android/server/wm/ActivityRecord;
    .param p1, "isClearConfig"    # Z
    .param p2, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 78
    if-ne p2, p0, :cond_0

    .line 79
    return-void

    .line 81
    :cond_0
    if-nez p1, :cond_1

    .line 82
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->clearCompatDisplayInsets()V

    .line 83
    return-void

    .line 85
    :cond_1
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->clearSizeCompatMode()V

    .line 86
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->attachedToProcess()Z

    move-result v0

    if-nez v0, :cond_2

    .line 87
    return-void

    .line 90
    :cond_2
    :try_start_0
    iget-object v0, p2, Lcom/android/server/wm/ActivityRecord;->mBoundsCompatControllerStub:Lcom/android/server/wm/BoundsCompatControllerStub;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/BoundsCompatControllerStub;->interceptScheduleTransactionIfNeeded(Landroid/app/servertransaction/ClientTransactionItem;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    .line 93
    :goto_0
    return-void
.end method

.method private rotationToOrientation(I)I
    .locals 1
    .param p1, "rotation"    # I

    .line 528
    packed-switch p1, :pswitch_data_0

    .line 538
    const/4 v0, -0x1

    return v0

    .line 535
    :pswitch_0
    const/4 v0, 0x2

    return v0

    .line 531
    :pswitch_1
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private taskContainsActivityRecord(Lcom/android/server/wm/Task;)Z
    .locals 6
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 325
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    if-ge v0, v1, :cond_6

    .line 326
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isLeafTask()Z

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_3

    .line 327
    invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 328
    .local v1, "ar":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 330
    iget-object v4, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    if-ne v1, v4, :cond_0

    .line 331
    return v3

    .line 335
    :cond_0
    invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    .line 336
    .local v4, "currentOrientation":I
    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    .line 338
    .local v5, "bottomOrientation":I
    if-eqz v4, :cond_1

    if-eqz v5, :cond_1

    if-eq v4, v5, :cond_1

    .line 341
    return v3

    .line 344
    :cond_1
    return v2

    .line 346
    .end local v1    # "ar":Lcom/android/server/wm/ActivityRecord;
    .end local v4    # "currentOrientation":I
    .end local v5    # "bottomOrientation":I
    :cond_2
    goto :goto_2

    .line 347
    :cond_3
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v1

    sub-int/2addr v1, v3

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_5

    .line 348
    invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v2

    .line 349
    .local v2, "t":Lcom/android/server/wm/Task;
    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 350
    invoke-direct {p0, v2}, Lcom/android/server/wm/BoundsCompatController;->taskContainsActivityRecord(Lcom/android/server/wm/Task;)Z

    move-result v3

    return v3

    .line 347
    .end local v2    # "t":Lcom/android/server/wm/Task;
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 325
    .end local v1    # "i":I
    :cond_5
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 355
    .end local v0    # "j":I
    :cond_6
    return v2
.end method


# virtual methods
.method adaptCompatConfiguration(Landroid/content/res/Configuration;Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayContent;)Z
    .locals 8
    .param p1, "parentConfiguration"    # Landroid/content/res/Configuration;
    .param p2, "resolvedConfig"    # Landroid/content/res/Configuration;
    .param p3, "dc"    # Lcom/android/server/wm/DisplayContent;

    .line 414
    invoke-direct {p0, p1}, Lcom/android/server/wm/BoundsCompatController;->canUseFixedAspectRatio(Landroid/content/res/Configuration;)Z

    move-result v0

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, -0x40800000    # -1.0f

    if-eqz v0, :cond_8

    .line 415
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->clearSizeCompatModeWithoutConfigChange()V

    .line 417
    iget-object v0, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getAppBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 419
    .local v0, "parentAppBounds":Landroid/graphics/Rect;
    if-eqz p3, :cond_7

    iget-object v4, p3, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    if-nez v4, :cond_0

    goto/16 :goto_1

    .line 424
    :cond_0
    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_1

    iget-object v1, p3, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget-object v1, v1, Landroid/view/DisplayInfo;->uniqueId:Ljava/lang/String;

    iget-object v4, p3, Lcom/android/server/wm/DisplayContent;->mCurrentUniqueDisplayId:Ljava/lang/String;

    .line 425
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 426
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v4, v4, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 428
    invoke-direct {p0}, Lcom/android/server/wm/BoundsCompatController;->getActivityFlipCompatMode()I

    move-result v5

    iget-object v6, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    .line 429
    invoke-virtual {v6}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 427
    invoke-virtual {v1, v4, v5, v6}, Lcom/android/server/wm/BoundsCompatUtils;->getGlobalScaleByName(Ljava/lang/String;ILandroid/graphics/Rect;)F

    move-result v1

    iput v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F

    .line 432
    :cond_1
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-eq v1, v4, :cond_2

    .line 433
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v1

    invoke-interface {v1}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_3

    .line 435
    iput v1, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    goto :goto_0

    .line 437
    :cond_3
    iput v2, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    .line 440
    :goto_0
    iget-object v1, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    const/16 v2, 0x11

    invoke-static {v1, v2}, Lcom/android/server/wm/BoundsCompatUtils;->getCompatGravity(Landroid/app/WindowConfiguration;I)I

    move-result v1

    iput v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGravity:I

    .line 443
    invoke-static {}, Landroid/app/servertransaction/BoundsCompat;->getInstance()Landroid/app/servertransaction/BoundsCompat;

    move-result-object v2

    iget v3, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F

    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    .line 445
    invoke-direct {p0, v1}, Lcom/android/server/wm/BoundsCompatController;->getRequestedConfigurationOrientation(Lcom/android/server/wm/ActivityRecord;)I

    move-result v5

    iget v6, p0, Lcom/android/server/wm/BoundsCompatController;->mGravity:I

    iget v7, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    .line 443
    move-object v4, p1

    invoke-virtual/range {v2 .. v7}, Landroid/app/servertransaction/BoundsCompat;->computeCompatBounds(FLandroid/content/res/Configuration;IIF)Landroid/graphics/Rect;

    move-result-object v1

    .line 447
    .local v1, "compatBounds":Landroid/graphics/Rect;
    iget v2, p2, Landroid/content/res/Configuration;->densityDpi:I

    if-nez v2, :cond_4

    .line 448
    iget v2, p1, Landroid/content/res/Configuration;->densityDpi:I

    iput v2, p2, Landroid/content/res/Configuration;->densityDpi:I

    .line 450
    :cond_4
    iget-object v2, p2, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v2, v1}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V

    .line 451
    iget-object v2, p2, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v2, v1}, Landroid/app/WindowConfiguration;->setAppBounds(Landroid/graphics/Rect;)V

    .line 454
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeImpl;->getInstance()Landroid/util/MiuiAppSizeCompatModeImpl;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/MiuiAppSizeCompatModeImpl;->shouldUseMaxBoundsFullscreen(Ljava/lang/String;)Z

    move-result v2

    .line 456
    .local v2, "shouldUseMaxBoundsFullscreen":Z
    if-nez v2, :cond_5

    .line 457
    iget-object v3, p2, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v3, v1}, Landroid/app/WindowConfiguration;->setMaxBounds(Landroid/graphics/Rect;)V

    .line 460
    :cond_5
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Lcom/android/server/wm/BoundsCompatUtils;->adaptCompatBounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayContent;)V

    .line 461
    iget-boolean v3, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z

    if-eqz v3, :cond_6

    .line 462
    iget v3, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    .line 465
    :cond_6
    const/4 v3, 0x1

    return v3

    .line 420
    .end local v1    # "compatBounds":Landroid/graphics/Rect;
    .end local v2    # "shouldUseMaxBoundsFullscreen":Z
    :cond_7
    :goto_1
    return v1

    .line 469
    .end local v0    # "parentAppBounds":Landroid/graphics/Rect;
    :cond_8
    iput v3, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F

    .line 470
    iput v2, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    .line 472
    return v1
.end method

.method addCallbackIfNeeded(Landroid/app/servertransaction/ClientTransaction;)V
    .locals 3
    .param p1, "transaction"    # Landroid/app/servertransaction/ClientTransaction;

    .line 136
    iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z

    .line 138
    iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I

    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    invoke-static {v0, v1, v2}, Landroid/app/servertransaction/BoundsCompatInfoChangeItem;->obtain(ILandroid/graphics/Rect;F)Landroid/app/servertransaction/BoundsCompatInfoChangeItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 140
    :cond_0
    return-void
.end method

.method adjustWindowParamsIfNeededLocked(Lcom/android/server/wm/WindowState;Landroid/view/WindowManager$LayoutParams;)V
    .locals 2
    .param p1, "win"    # Lcom/android/server/wm/WindowState;
    .param p2, "attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 143
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    if-eqz v0, :cond_0

    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    if-nez v0, :cond_1

    .line 146
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 147
    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 148
    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 150
    :cond_1
    return-void
.end method

.method areBoundsLetterboxed()Z
    .locals 1

    .line 311
    invoke-virtual {p0}, Lcom/android/server/wm/BoundsCompatController;->inMiuiSizeCompatMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->areBoundsLetterboxed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method clearAppBoundsIfNeeded(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "resolvedConfig"    # Landroid/content/res/Configuration;

    .line 153
    iget-object v0, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getAppBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 154
    .local v0, "resolvedAppBounds":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    invoke-static {}, Landroid/app/servertransaction/BoundsCompatStub;->get()Landroid/app/servertransaction/BoundsCompatStub;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    invoke-virtual {v1, v2}, Landroid/app/servertransaction/BoundsCompatStub;->isFixedAspectRatioModeEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 157
    :cond_0
    return-void
.end method

.method public clearSizeCompatMode(Lcom/android/server/wm/Task;ZLcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "isClearConfig"    # Z
    .param p3, "excluded"    # Lcom/android/server/wm/ActivityRecord;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z

    .line 77
    new-instance v0, Lcom/android/server/wm/BoundsCompatController$$ExternalSyntheticLambda0;

    invoke-direct {v0, p3, p2}, Lcom/android/server/wm/BoundsCompatController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityRecord;Z)V

    invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->forAllActivities(Ljava/util/function/Consumer;)V

    .line 95
    return-void
.end method

.method dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MiuiSizeCompat info:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  mState=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mCompatBounds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 164
    iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z

    if-eqz v0, :cond_0

    .line 165
    const-string v0, " mDispatchNeeded=true"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 168
    :cond_0
    iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mBounds:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 169
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mDispatchedState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mDispatchedBounds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 173
    :cond_2
    iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mFixedAspectRatio="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatio:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 177
    :cond_3
    iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    .line 178
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  mGlobalScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mGlobalScale:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mCurrentScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 183
    :cond_4
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 184
    return-void
.end method

.method dumpBounds(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/app/WindowConfiguration;)V
    .locals 8
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "winConfig"    # Landroid/app/WindowConfiguration;

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 199
    const-string v0, " Bounds="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p4}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 201
    .local v0, "bounds":Landroid/graphics/Rect;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ")"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 202
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, "x"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 203
    invoke-virtual {p4}, Landroid/app/WindowConfiguration;->getAppBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 204
    .local v1, "appBounds":Landroid/graphics/Rect;
    if-eqz v1, :cond_0

    .line 205
    const-string v6, " AppBounds="

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 206
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v6, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 207
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 210
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 211
    return-void
.end method

.method getCurrentMiuiSizeCompatScale(F)F
    .locals 1
    .param p1, "currentScale"    # F

    .line 288
    iget v0, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    return v0
.end method

.method public getDispatchedBounds()Landroid/graphics/Rect;
    .locals 1

    .line 392
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getScaledBounds(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 3
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .line 561
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 562
    .local v0, "realFrame":Landroid/graphics/Rect;
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 563
    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->scale(F)V

    .line 564
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 565
    return-object v0
.end method

.method inMiuiSizeCompatMode()Z
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public initBoundsCompatController(Lcom/android/server/wm/ActivityRecord;)V
    .locals 0
    .param p1, "owner"    # Lcom/android/server/wm/ActivityRecord;

    .line 71
    iput-object p1, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    .line 72
    return-void
.end method

.method public interceptScheduleTransactionIfNeeded(Landroid/app/servertransaction/ClientTransactionItem;)Z
    .locals 4
    .param p1, "originalTransactionItem"    # Landroid/app/servertransaction/ClientTransactionItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 215
    iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 216
    iput-boolean v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z

    .line 217
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v1, v1, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-static {v0, v1}, Landroid/app/servertransaction/ClientTransaction;->obtain(Landroid/app/IApplicationThread;Landroid/os/IBinder;)Landroid/app/servertransaction/ClientTransaction;

    move-result-object v0

    .line 218
    .local v0, "transaction":Landroid/app/servertransaction/ClientTransaction;
    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I

    iget-object v2, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/server/wm/BoundsCompatController;->mCurrentScale:F

    invoke-static {v1, v2, v3}, Landroid/app/servertransaction/BoundsCompatInfoChangeItem;->obtain(ILandroid/graphics/Rect;F)Landroid/app/servertransaction/BoundsCompatInfoChangeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 219
    if-eqz p1, :cond_0

    .line 220
    invoke-virtual {v0, p1}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 222
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v1, v1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V

    .line 223
    const/4 v1, 0x1

    return v1

    .line 225
    .end local v0    # "transaction":Landroid/app/servertransaction/ClientTransaction;
    :cond_1
    return v1
.end method

.method isBoundsCompatEnabled()Z
    .locals 1

    .line 229
    iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mBoundsCompatEnabled:Z

    return v0
.end method

.method public isDebugEnable()Z
    .locals 1

    .line 557
    sget-boolean v0, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z

    return v0
.end method

.method isDisplayCompatAvailable(Lcom/android/server/wm/Task;)Z
    .locals 4
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 233
    iget-object v0, p1, Lcom/android/server/wm/Task;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 234
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlip()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    :cond_0
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-nez v0, :cond_7

    .line 236
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isActivityTypeStandard()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 237
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z

    move-result v0

    if-nez v0, :cond_6

    .line 238
    iget v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatPolicy:I

    invoke-static {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isForcedResizeableDisplayCompatPolicy(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239
    return v1

    .line 242
    :cond_2
    iget v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatPolicy:I

    invoke-static {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isForcedUnresizeableDisplayCompatPolicy(I)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    .line 243
    return v2

    .line 246
    :cond_3
    iget v0, p1, Lcom/android/server/wm/Task;->mResizeMode:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_5

    iget v0, p1, Lcom/android/server/wm/Task;->mResizeMode:I

    if-ne v0, v2, :cond_4

    goto :goto_0

    .line 250
    :cond_4
    return v2

    .line 248
    :cond_5
    :goto_0
    return v1

    .line 252
    :cond_6
    return v1

    .line 255
    :cond_7
    iget-boolean v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 256
    return v1

    .line 258
    :cond_8
    iget-boolean v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z

    return v0
.end method

.method public isFixedAspectRatioAvailable()Z
    .locals 1

    .line 263
    iget-boolean v0, p0, Lcom/android/server/wm/BoundsCompatController;->mFixedAspectRatioAvailable:Z

    return v0
.end method

.method public isFixedAspectRatioEnabled()Z
    .locals 2

    .line 268
    invoke-static {}, Landroid/app/servertransaction/BoundsCompat;->getInstance()Landroid/app/servertransaction/BoundsCompat;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    invoke-virtual {v0, v1}, Landroid/app/servertransaction/BoundsCompat;->isFixedAspectRatioModeEnabled(I)Z

    move-result v0

    return v0
.end method

.method public prepareBoundsCompat()V
    .locals 2

    .line 273
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    .line 274
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isActivityTypeStandardOrUndefined()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlip()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    .line 276
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getActivityType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    invoke-direct {p0, v0}, Lcom/android/server/wm/BoundsCompatController;->applyPolicyIfNeeded(Landroid/content/pm/ActivityInfo;)V

    .line 279
    :cond_1
    return-void
.end method

.method resolveBoundsCompat(Landroid/content/res/Configuration;Landroid/content/res/Configuration;Landroid/graphics/Rect;F)V
    .locals 5
    .param p1, "resolvedConfig"    # Landroid/content/res/Configuration;
    .param p2, "parentConfig"    # Landroid/content/res/Configuration;
    .param p3, "sizeCompatBounds"    # Landroid/graphics/Rect;
    .param p4, "sizeCompatScale"    # F

    .line 359
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByActivity(Lcom/android/server/wm/ActivityRecord;)I

    move-result v0

    .line 360
    .local v0, "activityMode":I
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v3, v3, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 361
    invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I

    move-result v1

    .line 362
    .local v1, "appMode":I
    const/4 v2, 0x1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 364
    iput-boolean v2, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z

    .line 366
    :cond_0
    invoke-static {}, Landroid/app/servertransaction/BoundsCompat;->getInstance()Landroid/app/servertransaction/BoundsCompat;

    move-result-object v3

    iget v4, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    invoke-virtual {v3, v4}, Landroid/app/servertransaction/BoundsCompat;->isFixedAspectRatioModeEnabled(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/wm/BoundsCompatController;->mBoundsCompatEnabled:Z

    .line 367
    if-eqz v3, :cond_3

    .line 368
    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mBounds:Landroid/graphics/Rect;

    if-nez v3, :cond_1

    .line 369
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mBounds:Landroid/graphics/Rect;

    .line 371
    :cond_1
    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mBounds:Landroid/graphics/Rect;

    iget-object v4, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 372
    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    if-nez v3, :cond_2

    .line 373
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    .line 375
    :cond_2
    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/server/wm/BoundsCompatController;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 376
    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/server/wm/BoundsCompatController;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 377
    iput-boolean v2, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z

    goto :goto_0

    .line 379
    :cond_3
    invoke-static {}, Landroid/app/servertransaction/BoundsCompat;->getInstance()Landroid/app/servertransaction/BoundsCompat;

    move-result-object v3

    iget v4, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I

    invoke-virtual {v3, v4}, Landroid/app/servertransaction/BoundsCompat;->isFixedAspectRatioModeEnabled(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 380
    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    .line 381
    iget-object v3, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedBounds:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    .line 382
    iput-boolean v2, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z

    .line 385
    :cond_4
    :goto_0
    iget v3, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    iget v4, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I

    if-eq v3, v4, :cond_5

    .line 386
    iput v3, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchedState:I

    .line 387
    iput-boolean v2, p0, Lcom/android/server/wm/BoundsCompatController;->mDispatchNeeded:Z

    .line 389
    :cond_5
    return-void
.end method

.method shouldNotCreateCompatDisplayInsets()Z
    .locals 2

    .line 303
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mForceResizableActivities:Z

    if-nez v0, :cond_0

    .line 304
    invoke-static {}, Landroid/app/servertransaction/BoundsCompatStub;->get()Landroid/app/servertransaction/BoundsCompatStub;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/BoundsCompatController;->mState:I

    invoke-virtual {v0, v1}, Landroid/app/servertransaction/BoundsCompatStub;->isFixedAspectRatioModeEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 306
    .local v0, "inMiuiSizeCompatMode":Z
    :goto_0
    return v0
.end method

.method shouldRelaunchInMiuiSizeCompatMode(III)I
    .locals 1
    .param p1, "changes"    # I
    .param p2, "configChanged"    # I
    .param p3, "lastReportedDisplayId"    # I

    .line 294
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getDisplayId()I

    move-result v0

    if-ne v0, p3, :cond_0

    and-int/lit16 v0, p1, 0x80

    if-eqz v0, :cond_0

    .line 296
    or-int/lit16 p2, p2, 0xd00

    .line 299
    :cond_0
    return p2
.end method

.method shouldShowLetterboxUi()Z
    .locals 2

    .line 315
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/android/server/wm/BoundsCompatController;->mOwner:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 317
    .local v0, "task":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_0

    .line 318
    invoke-direct {p0, v0}, Lcom/android/server/wm/BoundsCompatController;->taskContainsActivityRecord(Lcom/android/server/wm/Task;)Z

    move-result v1

    return v1

    .line 321
    .end local v0    # "task":Lcom/android/server/wm/Task;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public updateDisplayCompatModeAvailableIfNeeded(Lcom/android/server/wm/Task;)V
    .locals 2
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 396
    iget-object v0, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    .line 397
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getPolicy(Ljava/lang/String;)I

    move-result v0

    .line 398
    .local v0, "policy":I
    iget v1, p1, Lcom/android/server/wm/Task;->mDisplayCompatPolicy:I

    if-eq v1, v0, :cond_0

    .line 399
    iput v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatPolicy:I

    .line 403
    .end local v0    # "policy":I
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/wm/BoundsCompatController;->isDisplayCompatAvailable(Lcom/android/server/wm/Task;)Z

    move-result v0

    .line 404
    .local v0, "isAvailable":Z
    iget-boolean v1, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z

    if-eq v1, v0, :cond_1

    .line 405
    iput-boolean v0, p1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z

    .line 406
    if-nez v0, :cond_1

    .line 407
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/android/server/wm/BoundsCompatController;->clearSizeCompatMode(Lcom/android/server/wm/Task;Z)V

    .line 410
    :cond_1
    return-void
.end method
