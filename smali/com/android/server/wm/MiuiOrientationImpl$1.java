class com.android.server.wm.MiuiOrientationImpl$1 implements java.util.function.BiConsumer {
	 /* .source "MiuiOrientationImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiOrientationImpl;->setEmbeddedFullRuleData(Ljava/util/Map;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/function/BiConsumer<", */
/* "Ljava/lang/String;", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;>;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiOrientationImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiOrientationImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiOrientationImpl; */
/* .line 331 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void accept ( java.lang.Object p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 331 */
/* check-cast p1, Ljava/lang/String; */
/* check-cast p2, Landroid/util/Pair; */
(( com.android.server.wm.MiuiOrientationImpl$1 ) p0 ).accept ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/MiuiOrientationImpl$1;->accept(Ljava/lang/String;Landroid/util/Pair;)V
return;
} // .end method
public void accept ( java.lang.String p0, android.util.Pair p1 ) {
/* .locals 25 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 334 */
/* .local p2, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;" */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p2 */
v2 = this.first;
/* check-cast v2, Ljava/lang/String; */
final String v3 = ":"; // const-string v3, ":"
(( java.lang.String ) v2 ).split ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 335 */
/* .local v2, "rules":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 336 */
/* .local v3, "nra":Z */
int v4 = 0; // const/4 v4, 0x0
/* .line 337 */
/* .local v4, "nrs":Z */
int v5 = 1; // const/4 v5, 0x1
/* .line 338 */
/* .local v5, "cr":Z */
int v6 = 1; // const/4 v6, 0x1
/* .line 339 */
/* .local v6, "rcr":Z */
int v7 = 0; // const/4 v7, 0x0
/* .line 340 */
/* .local v7, "nr":Z */
int v8 = 0; // const/4 v8, 0x0
/* .line 341 */
/* .local v8, "r":Z */
int v9 = 0; // const/4 v9, 0x0
/* .line 342 */
/* .local v9, "ri":Z */
int v10 = 0; // const/4 v10, 0x0
/* .line 343 */
/* .local v10, "uc":Z */
/* array-length v11, v2 */
int v13 = 0; // const/4 v13, 0x0
} // :goto_0
/* if-ge v13, v11, :cond_1 */
/* aget-object v14, v2, v13 */
/* .line 344 */
/* .local v14, "str":Ljava/lang/String; */
v15 = (( java.lang.String ) v14 ).hashCode ( ); // invoke-virtual {v14}, Ljava/lang/String;->hashCode()I
/* sparse-switch v15, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v15 = "rcr"; // const-string v15, "rcr"
v15 = (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v15 != null) { // if-eqz v15, :cond_0
int v15 = 3; // const/4 v15, 0x3
/* :sswitch_1 */
final String v15 = "nrs"; // const-string v15, "nrs"
v15 = (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v15 != null) { // if-eqz v15, :cond_0
int v15 = 1; // const/4 v15, 0x1
/* :sswitch_2 */
final String v15 = "nra"; // const-string v15, "nra"
v15 = (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v15 != null) { // if-eqz v15, :cond_0
int v15 = 0; // const/4 v15, 0x0
/* :sswitch_3 */
/* const-string/jumbo v15, "uc" */
v15 = (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v15 != null) { // if-eqz v15, :cond_0
int v15 = 7; // const/4 v15, 0x7
/* :sswitch_4 */
final String v15 = "ri"; // const-string v15, "ri"
v15 = (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v15 != null) { // if-eqz v15, :cond_0
	 int v15 = 6; // const/4 v15, 0x6
	 /* :sswitch_5 */
	 final String v15 = "nr"; // const-string v15, "nr"
	 v15 = 	 (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v15 != null) { // if-eqz v15, :cond_0
		 int v15 = 4; // const/4 v15, 0x4
		 /* :sswitch_6 */
		 final String v15 = "cr"; // const-string v15, "cr"
		 v15 = 		 (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v15 != null) { // if-eqz v15, :cond_0
			 int v15 = 2; // const/4 v15, 0x2
			 /* :sswitch_7 */
			 final String v15 = "r"; // const-string v15, "r"
			 v15 = 			 (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v15 != null) { // if-eqz v15, :cond_0
				 int v15 = 5; // const/4 v15, 0x5
			 } // :goto_1
			 int v15 = -1; // const/4 v15, -0x1
		 } // :goto_2
		 /* packed-switch v15, :pswitch_data_0 */
		 /* .line 370 */
		 /* new-instance v15, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v12 = "Unknown options for setEmbeddedFullRule :"; // const-string v12, "Unknown options for setEmbeddedFullRule :"
		 (( java.lang.StringBuilder ) v15 ).append ( v12 ); // invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v12 ).append ( v14 ); // invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v15 = "MiuiOrientationImpl"; // const-string v15, "MiuiOrientationImpl"
		 android.util.Slog .d ( v15,v12 );
		 /* .line 367 */
		 /* :pswitch_0 */
		 int v10 = 1; // const/4 v10, 0x1
		 /* .line 368 */
		 /* .line 364 */
		 /* :pswitch_1 */
		 int v9 = 1; // const/4 v9, 0x1
		 /* .line 365 */
		 /* .line 361 */
		 /* :pswitch_2 */
		 int v8 = 1; // const/4 v8, 0x1
		 /* .line 362 */
		 /* .line 358 */
		 /* :pswitch_3 */
		 int v7 = 1; // const/4 v7, 0x1
		 /* .line 359 */
		 /* .line 355 */
		 /* :pswitch_4 */
		 int v6 = 0; // const/4 v6, 0x0
		 /* .line 356 */
		 /* .line 352 */
		 /* :pswitch_5 */
		 int v5 = 0; // const/4 v5, 0x0
		 /* .line 353 */
		 /* .line 349 */
		 /* :pswitch_6 */
		 int v4 = 1; // const/4 v4, 0x1
		 /* .line 350 */
		 /* .line 346 */
		 /* :pswitch_7 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* .line 347 */
		 /* nop */
		 /* .line 343 */
	 } // .end local v14 # "str":Ljava/lang/String;
} // :goto_3
/* add-int/lit8 v13, v13, 0x1 */
/* goto/16 :goto_0 */
/* .line 373 */
} // :cond_1
v11 = this.this$0;
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmEmbeddedFullRules ( v11 );
/* new-instance v12, Lcom/android/server/wm/MiuiOrientationImpl$FullRule; */
v14 = this.this$0;
v13 = this.second;
/* check-cast v13, Ljava/lang/Boolean; */
v16 = (( java.lang.Boolean ) v13 ).booleanValue ( ); // invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z
/* move-object v13, v12 */
/* move-object/from16 v15, p1 */
/* move/from16 v17, v3 */
/* move/from16 v18, v4 */
/* move/from16 v19, v5 */
/* move/from16 v20, v6 */
/* move/from16 v21, v7 */
/* move/from16 v22, v8 */
/* move/from16 v23, v9 */
/* move/from16 v24, v10 */
/* invoke-direct/range {v13 ..v24}, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;Ljava/lang/String;ZZZZZZZZZ)V */
/* move-object/from16 v13, p1 */
/* .line 374 */
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x72 -> :sswitch_7 */
/* 0xc6f -> :sswitch_6 */
/* 0xdc4 -> :sswitch_5 */
/* 0xe37 -> :sswitch_4 */
/* 0xe8e -> :sswitch_3 */
/* 0x1ab1d -> :sswitch_2 */
/* 0x1ab2f -> :sswitch_1 */
/* 0x1b861 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
