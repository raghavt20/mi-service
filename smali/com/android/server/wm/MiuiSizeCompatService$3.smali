.class Lcom/android/server/wm/MiuiSizeCompatService$3;
.super Landroid/view/IDisplayFoldListener$Stub;
.source "MiuiSizeCompatService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiSizeCompatService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiSizeCompatService;

    .line 535
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService$3;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-direct {p0}, Landroid/view/IDisplayFoldListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisplayFoldChanged(IZ)V
    .locals 2
    .param p1, "displayId"    # I
    .param p2, "folded"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 539
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$3;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$3;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$3;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmNotificationShowing(Lcom/android/server/wm/MiuiSizeCompatService;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_2

    .line 541
    :cond_0
    sget-boolean v0, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 542
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$3;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ,mNotificationShowing= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$3;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmNotificationShowing(Lcom/android/server/wm/MiuiSizeCompatService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ,folded= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiSizeCompatService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$3;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    const-string v1, "device fold"

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->cancelSizeCompatNotification(Ljava/lang/String;)V

    .line 547
    :cond_2
    return-void
.end method
