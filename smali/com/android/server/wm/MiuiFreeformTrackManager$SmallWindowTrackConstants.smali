.class public final Lcom/android/server/wm/MiuiFreeformTrackManager$SmallWindowTrackConstants;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiFreeformTrackManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SmallWindowTrackConstants"
.end annotation


# static fields
.field public static final CLICK_EVENT_NAME:Ljava/lang/String; = "click"

.field public static final CLICK_TIP:Ljava/lang/String; = "621.1.1.1.14014"

.field public static final ENTER_EVENT_NAME:Ljava/lang/String; = "enter"

.field public static final ENTER_TIP:Ljava/lang/String; = "621.1.0.1.14010"

.field public static final ENTER_WAY_NAME1:Ljava/lang/String; = "\u5168\u5c4f\u5e94\u7528_\u65e0\u6781\u7f29\u653e_\u5de6\u4e0b\u89d2"

.field public static final ENTER_WAY_NAME2:Ljava/lang/String; = "\u5168\u5c4f\u5e94\u7528_\u65e0\u6781\u7f29\u653e_\u53f3\u4e0b\u89d2"

.field public static final ENTER_WAY_NAME3:Ljava/lang/String; = "\u8ff7\u4f60\u7a97"

.field public static final ENTER_WAY_NAME4:Ljava/lang/String; = "\u5176\u4ed6"

.field public static final ENTER_WAY_NAME5:Ljava/lang/String; = "\u62d6\u62fd\u8ff7\u4f60\u7a97\u8fdb\u5165\u5c0f\u7a97"

.field public static final ENTER_WAY_UNPIN:Ljava/lang/String; = "\u8d34\u8fb9\u547c\u51fa"

.field public static final MOVE_EVENT_NAME:Ljava/lang/String; = "move"

.field public static final MOVE_TIP:Ljava/lang/String; = "621.1.0.1.14011"

.field public static final PINED_EVENT_NAME:Ljava/lang/String; = "hide_window"

.field public static final PINED_EXIT_EVENT_NAME:Ljava/lang/String; = "quit_hidden_window"

.field public static final PINED_EXIT_TIP:Ljava/lang/String; = "621.1.2.1.21751"

.field public static final PINED_MOVE_EVENT_NAME:Ljava/lang/String; = "move_hidden_window"

.field public static final PINED_MOVE_TIP:Ljava/lang/String; = "621.1.2.1.21749"

.field public static final PINED_TIP:Ljava/lang/String; = "621.1.2.1.21747"

.field public static final PINED_WAY_ENTER_RECENT_TASK:Ljava/lang/String; = "\u5c0f\u7a97\u8fdb\u5165\u6700\u8fd1\u4efb\u52a1"

.field public static final PINED_WAY_SLIDE:Ljava/lang/String; = "\u62d6\u62fd\u5c0f\u7a97"

.field public static final QUIT_EVENT_NAME:Ljava/lang/String; = "quit"

.field public static final QUIT_TIP:Ljava/lang/String; = "621.1.0.1.14013"

.field public static final QUIT_WAY_NAME1:Ljava/lang/String; = "\u4e0a\u6ed1"

.field public static final QUIT_WAY_NAME2:Ljava/lang/String; = "\u6a2a\u5c4f\u65f6\u5168\u5c4f"

.field public static final QUIT_WAY_NAME3:Ljava/lang/String; = "\u7ad6\u5c4f\u65f6\u5168\u5c4f"

.field public static final QUIT_WAY_NAME4:Ljava/lang/String; = "\u8fdb\u5165\u8ff7\u4f60\u7a97"

.field public static final QUIT_WAY_NAME5:Ljava/lang/String; = "\u62d6\u62fd\u5c0f\u7a97\u81f3\u5168\u5c4f"

.field public static final QUIT_WAY_NAME6:Ljava/lang/String; = "\u5176\u4ed6"

.field public static final QUIT_WAY_PIN:Ljava/lang/String; = "\u8fdb\u5165\u5c0f\u7a97\u8d34\u8fb9"

.field public static final RESIZE_EVENT_NAME:Ljava/lang/String; = "resize"

.field public static final RESIZE_TIP:Ljava/lang/String; = "621.1.0.1.14012"

.field public static final STACK_RATIO1:Ljava/lang/String; = "\u624b\u673a\u6bd4\u4f8b"

.field public static final STACK_RATIO2:Ljava/lang/String; = "\u6e38\u620f/\u89c6\u9891\u6bd4\u4f8b"

.field public static final STACK_RATIO3:Ljava/lang/String; = "4:3"

.field public static final STACK_RATIO4:Ljava/lang/String; = "3:4"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
