.class public Lcom/android/server/wm/SplitScreenRecommendPredictHelper;
.super Ljava/lang/Object;
.source "SplitScreenRecommendPredictHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SplitScreenRecommendPredictHelper"


# instance fields
.field mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V
    .locals 0
    .param p1, "miuiMultiWindowRecommendHelper"    # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/android/server/wm/SplitScreenRecommendPredictHelper;->mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    .line 18
    return-void
.end method

.method private getDeduplicatedTaskList(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;",
            ">;"
        }
    .end annotation

    .line 84
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 85
    .local v0, "arrayList":Ljava/util/ArrayList;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    .line 86
    .local v2, "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v3

    .line 87
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    invoke-virtual {v4}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 88
    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    .end local v2    # "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    :cond_1
    goto :goto_0

    .line 91
    :cond_2
    return-object v0
.end method


# virtual methods
.method public getFrequentSwitchedTask(Ljava/util/List;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;",
            ">;"
        }
    .end annotation

    .line 21
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
    invoke-direct {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendPredictHelper;->getDeduplicatedTaskList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 22
    .local v0, "deduplicatedTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .local v1, "candidateTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .local v2, "candidateTaskIndexList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    if-ge v3, v5, :cond_0

    .line 25
    const-string v3, "SplitScreenRecommendPredictHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "appSwitchList size is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " less than 4"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    return-object v4

    .line 29
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 30
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    .line 31
    .local v6, "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v7

    new-instance v8, Lcom/android/server/wm/SplitScreenRecommendPredictHelper$1;

    invoke-direct {v8, p0, v6}, Lcom/android/server/wm/SplitScreenRecommendPredictHelper$1;-><init>(Lcom/android/server/wm/SplitScreenRecommendPredictHelper;Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V

    invoke-interface {v7, v8}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v7

    .line 36
    invoke-interface {v7}, Ljava/util/stream/Stream;->count()J

    move-result-wide v7

    const-wide/16 v9, 0x2

    cmp-long v7, v7, v9

    if-ltz v7, :cond_1

    .line 37
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    .end local v6    # "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 42
    .end local v3    # "i":I
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-eq v3, v5, :cond_3

    .line 43
    const-string v3, "SplitScreenRecommendPredictHelper"

    const-string v5, "not find frequent switched task"

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 45
    :cond_3
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x1

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    sub-int/2addr v7, v6

    if-ne v5, v7, :cond_a

    .line 46
    const/4 v5, 0x2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x3

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sub-int/2addr v8, v6

    if-ne v7, v8, :cond_a

    .line 47
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sub-int/2addr v8, v6

    if-ne v7, v8, :cond_a

    .line 48
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    invoke-virtual {v7}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;

    move-result-object v7

    .line 49
    .local v7, "task1":Lcom/android/server/wm/Task;
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    invoke-virtual {v8}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;

    move-result-object v8

    .line 50
    .local v8, "task2":Lcom/android/server/wm/Task;
    iget-object v9, p0, Lcom/android/server/wm/SplitScreenRecommendPredictHelper;->mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v9, v9, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v9, v9, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v9}, Lcom/android/server/wm/ActivityTaskManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;

    move-result-object v9

    monitor-enter v9

    .line 51
    :try_start_0
    invoke-virtual {v7}, Lcom/android/server/wm/Task;->supportsSplitScreenWindowingMode()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 52
    invoke-virtual {v8}, Lcom/android/server/wm/Task;->supportsSplitScreenWindowingMode()Z

    move-result v10

    if-nez v10, :cond_4

    goto/16 :goto_3

    .line 56
    :cond_4
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    invoke-virtual {v7}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v9

    if-ne v9, v6, :cond_8

    .line 58
    invoke-virtual {v8}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v9

    if-eq v9, v6, :cond_5

    goto :goto_2

    .line 62
    :cond_5
    invoke-static {v7}, Lcom/android/server/wm/RecommendUtils;->isInSplitScreenWindowingMode(Lcom/android/server/wm/Task;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 63
    invoke-static {v8}, Lcom/android/server/wm/RecommendUtils;->isInSplitScreenWindowingMode(Lcom/android/server/wm/Task;)Z

    move-result v9

    if-eqz v9, :cond_6

    goto :goto_1

    .line 68
    :cond_6
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 70
    .local v4, "frequentSwitchedTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    const-string v5, "SplitScreenRecommendPredictHelper"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "find frequent switched task, task 1: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, " task 2: "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 73
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    invoke-virtual {v6}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 72
    invoke-static {v5, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    return-object v4

    .line 64
    .end local v4    # "frequentSwitchedTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
    :cond_7
    :goto_1
    const-string v3, "SplitScreenRecommendPredictHelper"

    const-string v5, " task isInSplitScreenWindowingMode"

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    return-object v4

    .line 59
    :cond_8
    :goto_2
    const-string v3, "SplitScreenRecommendPredictHelper"

    const-string v5, " task windowingMode is not fullscreen"

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    return-object v4

    .line 53
    :cond_9
    :goto_3
    :try_start_1
    const-string v3, "SplitScreenRecommendPredictHelper"

    const-string v5, " not supportsSplitScreenWindowingMode"

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    monitor-exit v9

    return-object v4

    .line 56
    :catchall_0
    move-exception v3

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 76
    .end local v7    # "task1":Lcom/android/server/wm/Task;
    .end local v8    # "task2":Lcom/android/server/wm/Task;
    :cond_a
    const-string v3, "SplitScreenRecommendPredictHelper"

    const-string v5, "not find frequent switched task, index is not contiguous"

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :goto_4
    return-object v4
.end method
