class com.android.server.wm.MiuiWindowMonitorImpl$WindowInfo {
	 /* .source "MiuiWindowMonitorImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiWindowMonitorImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "WindowInfo" */
} // .end annotation
/* # static fields */
private static final java.lang.String KEY_NAME;
private static final java.lang.String KEY_TYPE;
/* # instance fields */
java.lang.String mPackageName;
Integer mPid;
android.os.IBinder mToken;
Integer mType;
java.lang.String mWindowName;
/* # direct methods */
public com.android.server.wm.MiuiWindowMonitorImpl$WindowInfo ( ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "token" # Landroid/os/IBinder; */
/* .param p4, "windowName" # Ljava/lang/String; */
/* .param p5, "type" # I */
/* .line 634 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 635 */
/* iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPid:I */
/* .line 636 */
this.mPackageName = p2;
/* .line 637 */
this.mToken = p3;
/* .line 638 */
this.mWindowName = p4;
/* .line 639 */
/* iput p5, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I */
/* .line 640 */
return;
} // .end method
/* # virtual methods */
public org.json.JSONObject toJson ( ) {
/* .locals 3 */
/* .line 648 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 650 */
/* .local v0, "jobj":Lorg/json/JSONObject; */
try { // :try_start_0
	 final String v1 = "name"; // const-string v1, "name"
	 v2 = this.mWindowName;
	 (( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* :try_end_0 */
	 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 654 */
	 /* nop */
	 /* .line 655 */
	 /* .line 651 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 652 */
	 /* .local v1, "e":Lorg/json/JSONException; */
	 (( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
	 /* .line 653 */
	 int v2 = 0; // const/4 v2, 0x0
} // .end method
public java.lang.String toString ( ) {
	 /* .locals 2 */
	 /* .line 644 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 v1 = this.mWindowName;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* const-string/jumbo v1, "|" */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
