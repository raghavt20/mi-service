.class public abstract Lcom/android/server/wm/MiuiSizeCompatInternal;
.super Ljava/lang/Object;
.source "MiuiSizeCompatInternal.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
.end method

.method public abstract getAspectGravityByPackage(Ljava/lang/String;)I
.end method

.method public abstract getAspectRatioByPackage(Ljava/lang/String;)F
.end method

.method public abstract getMiuiGameSizeCompatEnabledApps()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMiuiSizeCompatEnabledApps()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getScaleModeByPackage(Ljava/lang/String;)I
.end method

.method public abstract inMiuiGameSizeCompat(Ljava/lang/String;)Z
.end method

.method public abstract isAppSizeCompatRestarting(Ljava/lang/String;)Z
.end method

.method public abstract onSystemReady(Lcom/android/server/wm/ActivityTaskManagerService;)V
.end method

.method public abstract showWarningNotification(Lcom/android/server/wm/ActivityRecord;)V
.end method
