.class public Lcom/android/server/wm/AppRTWmsImpl;
.super Ljava/lang/Object;
.source "AppRTWmsImpl.java"

# interfaces
.implements Lcom/android/server/wm/AppRTWmsStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getTunerList()Lcom/android/server/wm/AppResolutionTuner;
    .locals 1

    .line 72
    invoke-static {}, Lcom/android/server/wm/AppResolutionTuner;->getInstance()Lcom/android/server/wm/AppResolutionTuner;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getTAG()Ljava/lang/String;
    .locals 1

    .line 63
    const-string v0, "APP_RT"

    return-object v0
.end method

.method public isAppResolutionTunerSupport()Z
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/AppResolutionTuner;->isAppRTEnable()Z

    move-result v0

    return v0
.end method

.method public isDebug()Z
    .locals 1

    .line 68
    sget-boolean v0, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z

    return v0
.end method

.method public setAppResolutionTunerSupport(Z)V
    .locals 1
    .param p1, "support"    # Z

    .line 16
    invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/AppResolutionTuner;->setAppRTEnable(Z)V

    .line 17
    return-void
.end method

.method public setWindowScaleByWL(Lcom/android/server/wm/WindowState;Landroid/view/DisplayInfo;Landroid/view/WindowManager$LayoutParams;II)V
    .locals 7
    .param p1, "win"    # Lcom/android/server/wm/WindowState;
    .param p2, "displayInfo"    # Landroid/view/DisplayInfo;
    .param p3, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p4, "requestedWidth"    # I
    .param p5, "requestedHeight"    # I

    .line 27
    iget v0, p2, Landroid/view/DisplayInfo;->logicalWidth:I

    .line 28
    .local v0, "width":I
    iget v1, p2, Landroid/view/DisplayInfo;->logicalHeight:I

    .line 29
    .local v1, "height":I
    const/4 v2, 0x0

    if-eqz p3, :cond_0

    iget-object v3, p3, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v3, v2

    .line 30
    .local v3, "packageName":Ljava/lang/String;
    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 31
    invoke-virtual {p3}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    nop

    .line 32
    .local v2, "windowName":Ljava/lang/String;
    :goto_1
    if-eqz p3, :cond_4

    if-eqz v3, :cond_4

    if-eqz v2, :cond_4

    .line 33
    const-string v4, "FastStarting"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 35
    const-string v4, "Splash Screen"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 37
    const-string v4, "PopupWindow"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    if-ne v1, p5, :cond_2

    if-eq v0, p4, :cond_3

    :cond_2
    iget v4, p3, Landroid/view/WindowManager$LayoutParams;->width:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_4

    iget v4, p3, Landroid/view/WindowManager$LayoutParams;->height:I

    if-ne v4, v5, :cond_4

    iget v4, p3, Landroid/view/WindowManager$LayoutParams;->x:I

    if-nez v4, :cond_4

    iget v4, p3, Landroid/view/WindowManager$LayoutParams;->y:I

    if-nez v4, :cond_4

    .line 42
    :cond_3
    invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Lcom/android/server/wm/AppResolutionTuner;->isScaledByWMS(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 43
    invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/server/wm/AppResolutionTuner;->getScaleValue(Ljava/lang/String;)F

    move-result v4

    .line 45
    .local v4, "scale":F
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v4, v5

    if-eqz v5, :cond_4

    .line 46
    iput v4, p1, Lcom/android/server/wm/WindowState;->mHWScale:F

    .line 47
    const/4 v5, 0x1

    iput-boolean v5, p1, Lcom/android/server/wm/WindowState;->mNeedHWResizer:Z

    .line 48
    sget-boolean v5, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z

    if-eqz v5, :cond_4

    .line 49
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setWindowScaleByWL, scale: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", win: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",attrs: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 50
    invoke-virtual {p3}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 49
    const-string v6, "APP_RT"

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    .end local v4    # "scale":F
    :cond_4
    return-void
.end method

.method public updateResolutionTunerConfig(Ljava/lang/String;)Z
    .locals 1
    .param p1, "config"    # Ljava/lang/String;

    .line 58
    invoke-direct {p0}, Lcom/android/server/wm/AppRTWmsImpl;->getTunerList()Lcom/android/server/wm/AppResolutionTuner;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/AppResolutionTuner;->updateResolutionTunerConfig(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
