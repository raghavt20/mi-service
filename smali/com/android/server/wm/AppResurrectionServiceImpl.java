public class com.android.server.wm.AppResurrectionServiceImpl extends com.android.server.wm.AppResurrectionServiceStub {
	 /* .source "AppResurrectionServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.wm.AppResurrectionServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String ATTR_REBORN_CHILDREN_SIZE;
private static final java.lang.String CLOUD_KEY_NAME;
private static final java.lang.String CLOUD_MODULE_NAME;
public static final java.lang.String TAG;
private static final java.lang.String TAG_REBORN_ACTIVITY;
private static final java.util.ArrayList sDefaultAppResurrectionEnableActivityList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sDefaultAppResurrectionEnablePKGList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final org.json.JSONObject sDefaultPkg2MaxChildCountJObject;
private static final java.util.ArrayList sDefaultRebootResurrectionEnableActivityList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sDefaultRebootResurrectionEnablePkgList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sDefaultResurrectionEnableReasonList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean DEBUG;
private android.content.BroadcastReceiver mBroadcastReceiver;
private java.lang.String mCPULevel;
private org.json.JSONArray mCloudAppResurrectionActivityJArray;
private Boolean mCloudAppResurrectionEnable;
private Integer mCloudAppResurrectionInactiveDurationHour;
private Long mCloudAppResurrectionLaunchTimeThresholdMillis;
private Integer mCloudAppResurrectionMaxChildCount;
private java.lang.String mCloudAppResurrectionName;
private org.json.JSONArray mCloudAppResurrectionPKGJArray;
private Long mCloudAppResurrectionVersion;
private com.android.server.wm.AppResurrectionCloudManager mCloudManager;
private org.json.JSONObject mCloudPkg2MaxChildCountJObject;
private org.json.JSONArray mCloudRebootResurrectionActivityJArray;
private Boolean mCloudRebootResurrectionEnable;
private org.json.JSONArray mCloudRebootResurrectionPKGJArray;
private android.content.Context mContext;
private Boolean mIsDeviceSupport;
private Boolean mIsLoadDone;
private Boolean mIsTestMode;
private java.io.File mLocalSpFile;
private final android.os.Handler mLoggerHandler;
private final android.os.Handler mMIUIBgHandler;
private java.util.HashMap mPkg2KillReasonMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mPkg2StartThemeMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private volatile java.lang.String mRebornPkg;
private android.content.ContentResolver mResolver;
private com.android.server.wm.AppResurrectionTrackManager mTrackManager;
/* # direct methods */
public static void $r8$lambda$EW8OAHUU9KE_drSaYZPEZqsi8D4 ( com.android.server.wm.AppResurrectionServiceImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$init$1()V */
return;
} // .end method
public static Boolean $r8$lambda$HiShqve5mvNS1bL4jHQiI48pWtk ( com.android.server.wm.AppResurrectionServiceImpl p0, java.lang.String p1, com.android.server.wm.ActivityRecord p2 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$isChildrenActivityFromSamePkg$7(Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;)Z */
} // .end method
public static void $r8$lambda$S_5WDZhsUHesxGVTyqpzrScQ-Ho ( com.android.server.wm.AppResurrectionServiceImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$init$0()V */
return;
} // .end method
public static void $r8$lambda$UgwBb6vyJZCFQgUjn9t4Fk6VWwA ( com.android.server.wm.AppResurrectionServiceImpl p0, android.content.ComponentName p1, Long p2, java.lang.String p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$notifyWindowsDrawn$3(Landroid/content/ComponentName;JLjava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$asL8P4pw8XJDSm4G0yJfUyYUCuM ( com.android.server.wm.AppResurrectionServiceImpl p0, android.content.ComponentName p1, Long p2, java.lang.String p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$notifyWindowsDrawn$4(Landroid/content/ComponentName;JLjava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$bBI9Pmj3b6gXZZkk-EoZ5HguWV8 ( com.android.server.wm.AppResurrectionServiceImpl p0, com.android.server.wm.ActivityRecord p1, Long p2, java.lang.String p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$notifyWindowsDrawn$6(Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$qBpG3NxpPuEk0f4xy5ujJ-ezYBA ( com.android.server.wm.AppResurrectionServiceImpl p0, com.android.server.wm.ActivityRecord p1, Long p2, java.lang.String p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$notifyWindowsDrawn$5(Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$sBgvzErTrkVFc9eLjrRb93MMUPE ( com.android.server.wm.AppResurrectionServiceImpl p0, java.lang.String p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/AppResurrectionServiceImpl;->lambda$updateStartingWindowResolvedTheme$2(Ljava/lang/String;I)V */
return;
} // .end method
static Boolean -$$Nest$fgetmIsLoadDone ( com.android.server.wm.AppResurrectionServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsLoadDone:Z */
} // .end method
static android.os.Handler -$$Nest$fgetmMIUIBgHandler ( com.android.server.wm.AppResurrectionServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMIUIBgHandler;
} // .end method
static void -$$Nest$fputmIsLoadDone ( com.android.server.wm.AppResurrectionServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsLoadDone:Z */
return;
} // .end method
static void -$$Nest$mgetCloudData ( com.android.server.wm.AppResurrectionServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->getCloudData()V */
return;
} // .end method
static void -$$Nest$mloadDataFromSP ( com.android.server.wm.AppResurrectionServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->loadDataFromSP()V */
return;
} // .end method
static com.android.server.wm.AppResurrectionServiceImpl ( ) {
/* .locals 8 */
/* .line 60 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 62 */
final String v1 = "com.youku.phone"; // const-string v1, "com.youku.phone"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 63 */
final String v2 = "com.qiyi.video"; // const-string v2, "com.qiyi.video"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 64 */
final String v2 = "com.hunantv.imgo.activity"; // const-string v2, "com.hunantv.imgo.activity"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 65 */
/* const-string/jumbo v3, "tv.danmaku.bili" */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 66 */
final String v4 = "com.tencent.qqlive"; // const-string v4, "com.tencent.qqlive"
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 67 */
final String v5 = "com.qiyi.video.lite"; // const-string v5, "com.qiyi.video.lite"
(( java.util.ArrayList ) v0 ).add ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 71 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 73 */
final String v6 = "com.tencent.qqlive/.ona.activity.VideoDetailActivity"; // const-string v6, "com.tencent.qqlive/.ona.activity.VideoDetailActivity"
(( java.util.ArrayList ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 76 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 79 */
int v7 = 2; // const/4 v7, 0x2
try { // :try_start_0
(( org.json.JSONObject ) v0 ).put ( v4, v7 ); // invoke-virtual {v0, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 82 */
/* .line 80 */
/* :catch_0 */
/* move-exception v0 */
/* .line 85 */
} // :goto_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 87 */
final String v7 = "AutoIdleKill"; // const-string v7, "AutoIdleKill"
(( java.util.ArrayList ) v0 ).add ( v7 ); // invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 88 */
final String v7 = "AutoPowerKill"; // const-string v7, "AutoPowerKill"
(( java.util.ArrayList ) v0 ).add ( v7 ); // invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 91 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 93 */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 94 */
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 95 */
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 96 */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 97 */
(( java.util.ArrayList ) v0 ).add ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 100 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 102 */
/* const-string/jumbo v1, "tv.danmaku.bili/.ui.video.VideoDetailsActivity" */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 103 */
/* const-string/jumbo v1, "tv.danmaku.bili/com.bilibili.ship.theseus.all.UnitedBizDetailsActivity" */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 104 */
/* const-string/jumbo v1, "tv.danmaku.bili/com.bilibili.ship.theseus.detail.UnitedBizDetailsActivity" */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 105 */
final String v1 = "com.hunantv.imgo.activity/com.mgtv.ui.player.VodPlayerPageActivity"; // const-string v1, "com.hunantv.imgo.activity/com.mgtv.ui.player.VodPlayerPageActivity"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 106 */
final String v1 = "com.hunantv.imgo.activity/com.mgtv.ui.videoplay.MGVideoPlayActivity"; // const-string v1, "com.hunantv.imgo.activity/com.mgtv.ui.videoplay.MGVideoPlayActivity"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 107 */
final String v1 = "com.youku.phone/com.youku.ui.activity.DetailActivity"; // const-string v1, "com.youku.phone/com.youku.ui.activity.DetailActivity"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 108 */
(( java.util.ArrayList ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 109 */
final String v1 = "com.qiyi.video.lite/.videoplayer.activity.PlayerV2Activity"; // const-string v1, "com.qiyi.video.lite/.videoplayer.activity.PlayerV2Activity"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 111 */
return;
} // .end method
public com.android.server.wm.AppResurrectionServiceImpl ( ) {
/* .locals 6 */
/* .line 47 */
/* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceStub;-><init>()V */
/* .line 50 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
/* .line 52 */
com.android.server.FgThread .getHandler ( );
this.mLoggerHandler = v1;
/* .line 53 */
com.android.server.MiuiBgThread .getHandler ( );
this.mMIUIBgHandler = v1;
/* .line 54 */
int v1 = 0; // const/4 v1, 0x0
this.mCloudManager = v1;
/* .line 55 */
this.mTrackManager = v1;
/* .line 57 */
final String v2 = ""; // const-string v2, ""
this.mRebornPkg = v2;
/* .line 117 */
/* new-instance v2, Ljava/io/File; */
/* new-instance v3, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v5, "system" */
/* invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
final String v4 = "app_resurrection_pkg2theme.xml"; // const-string v4, "app_resurrection_pkg2theme.xml"
/* invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mLocalSpFile = v2;
/* .line 119 */
/* iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
/* .line 129 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
/* .line 130 */
/* iput-boolean v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z */
/* .line 131 */
final String v2 = "local"; // const-string v2, "local"
this.mCloudAppResurrectionName = v2;
/* .line 132 */
/* const-wide/16 v2, 0x0 */
/* iput-wide v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionVersion:J */
/* .line 133 */
this.mCloudAppResurrectionPKGJArray = v1;
/* .line 134 */
this.mCloudAppResurrectionActivityJArray = v1;
/* .line 135 */
this.mCloudRebootResurrectionPKGJArray = v1;
/* .line 136 */
this.mCloudRebootResurrectionActivityJArray = v1;
/* .line 137 */
this.mCloudPkg2MaxChildCountJObject = v1;
/* .line 139 */
int v1 = 3; // const/4 v1, 0x3
/* iput v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I */
/* .line 140 */
/* const/16 v1, 0x24 */
/* iput v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionInactiveDurationHour:I */
/* .line 142 */
/* const-wide/16 v1, 0xdac */
/* iput-wide v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J */
/* .line 143 */
final String v1 = "Unknown"; // const-string v1, "Unknown"
this.mCPULevel = v1;
/* .line 146 */
/* iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsLoadDone:Z */
/* .line 150 */
/* iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsTestMode:Z */
return;
} // .end method
private void checkCPULevel ( Long p0, Long p1, Long p2, Long p3 ) {
/* .locals 4 */
/* .param p1, "high" # J */
/* .param p3, "middle" # J */
/* .param p5, "low" # J */
/* .param p7, "unknown" # J */
/* .line 787 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
int v1 = 1; // const/4 v1, 0x1
final String v2 = "AppResurrectionServiceImpl"; // const-string v2, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "DeviceLevel.getDeviceLevel(DeviceLevel.DEV_STANDARD_VER, DeviceLevel.CPU): "; // const-string v3, "DeviceLevel.getDeviceLevel(DeviceLevel.DEV_STANDARD_VER, DeviceLevel.CPU): "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = miui.util.DeviceLevel .getDeviceLevel ( v1,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 788 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "DeviceLevel.getDeviceLevel(DeviceLevel.DEV_STANDARD_VER, DeviceLevel.GPU): "; // const-string v3, "DeviceLevel.getDeviceLevel(DeviceLevel.DEV_STANDARD_VER, DeviceLevel.GPU): "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = miui.util.DeviceLevel .getDeviceLevel ( v1,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 789 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "DeviceLevel.getDeviceLevel(DeviceLevel.DEV_STANDARD_VER, DeviceLevel.RAM): "; // const-string v3, "DeviceLevel.getDeviceLevel(DeviceLevel.DEV_STANDARD_VER, DeviceLevel.RAM): "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = miui.util.DeviceLevel .getDeviceLevel ( v1,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 791 */
} // :cond_2
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isCPUHighLevelDevice()Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 792 */
final String v0 = "High"; // const-string v0, "High"
this.mCPULevel = v0;
/* .line 793 */
/* iput-wide p1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J */
/* .line 794 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
final String v0 = "checkCPULevel ret high"; // const-string v0, "checkCPULevel ret high"
android.util.Slog .d ( v2,v0 );
/* .line 795 */
} // :cond_3
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isCPUMiddleLevelDevice()Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 796 */
final String v0 = "Middle"; // const-string v0, "Middle"
this.mCPULevel = v0;
/* .line 797 */
/* iput-wide p3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J */
/* .line 798 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
final String v0 = "checkCPULevel ret middle"; // const-string v0, "checkCPULevel ret middle"
android.util.Slog .d ( v2,v0 );
/* .line 799 */
} // :cond_4
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isCPULowLevelDevice()Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 800 */
final String v0 = "Low"; // const-string v0, "Low"
this.mCPULevel = v0;
/* .line 801 */
/* iput-wide p5, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J */
/* .line 802 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
final String v0 = "checkCPULevel ret low"; // const-string v0, "checkCPULevel ret low"
android.util.Slog .d ( v2,v0 );
/* .line 803 */
} // :cond_5
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isCPUUnknownLevelDevice()Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 804 */
final String v0 = "Unknown"; // const-string v0, "Unknown"
this.mCPULevel = v0;
/* .line 805 */
/* iput-wide p7, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J */
/* .line 806 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
final String v0 = "checkCPULevel ret unknown"; // const-string v0, "checkCPULevel ret unknown"
android.util.Slog .d ( v2,v0 );
/* .line 808 */
} // :cond_6
} // :goto_0
return;
} // .end method
private void checkLaunchTimeOverThreshold ( java.lang.String p0, Long p1 ) {
/* .locals 4 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "windowsDrawnDelay" # J */
/* .line 582 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v1 = "AppResurrectionServiceImpl"; // const-string v1, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "checkLaunchTimeOverThreshold enter delay:"; // const-string v2, "checkLaunchTimeOverThreshold enter delay:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", pkg: "; // const-string v2, ", pkg: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 583 */
} // :cond_0
/* iget-wide v2, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J */
/* cmp-long v0, p2, v2 */
/* if-lez v0, :cond_2 */
/* .line 584 */
v0 = this.mCloudAppResurrectionPKGJArray;
final String v2 = "checkLaunchTimeOverThreshold = "; // const-string v2, "checkLaunchTimeOverThreshold = "
/* if-nez v0, :cond_1 */
/* .line 585 */
v0 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultAppResurrectionEnablePKGList;
v3 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 586 */
(( java.util.ArrayList ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 587 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", D: "; // const-string v2, ", D: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 590 */
} // :cond_1
v0 = /* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 591 */
v0 = this.mCloudAppResurrectionPKGJArray;
/* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->removeKeyFromJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z */
/* .line 592 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", C: "; // const-string v2, ", C: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 596 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void getCloudData ( ) {
/* .locals 19 */
/* .line 734 */
/* move-object/from16 v9, p0 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 735 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "KeyAppResurrection"; // const-string v1, "KeyAppResurrection"
int v2 = 0; // const/4 v2, 0x0
int v10 = 0; // const/4 v10, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v2,v10 );
/* .line 736 */
/* .local v11, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* if-nez v11, :cond_0 */
/* .line 737 */
return;
/* .line 740 */
} // :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v11 ).json ( ); // invoke-virtual {v11}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* .line 741 */
/* .local v12, "cloudDataJson":Lorg/json/JSONObject; */
final String v0 = "AppResurrectionServiceImpl"; // const-string v0, "AppResurrectionServiceImpl"
if ( v12 != null) { // if-eqz v12, :cond_6
/* .line 742 */
final String v1 = "getCloudData cloudDataJson !=null"; // const-string v1, "getCloudData cloudDataJson !=null"
android.util.Slog .v ( v0,v1 );
/* .line 743 */
/* iget-boolean v1, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getCloudData cloudDataJson:"; // const-string v2, "getCloudData cloudDataJson:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v0,v1 );
/* .line 745 */
} // :cond_1
final String v0 = "app_resur_enable"; // const-string v0, "app_resur_enable"
v0 = (( org.json.JSONObject ) v12 ).optBoolean ( v0, v10 ); // invoke-virtual {v12, v0, v10}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
/* .line 746 */
/* const-string/jumbo v0, "version" */
/* const-wide/16 v1, 0x0 */
(( org.json.JSONObject ) v12 ).optLong ( v0, v1, v2 ); // invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J
/* move-result-wide v0 */
/* iput-wide v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionVersion:J */
/* .line 747 */
final String v0 = "name"; // const-string v0, "name"
final String v1 = "local"; // const-string v1, "local"
(( org.json.JSONObject ) v12 ).optString ( v0, v1 ); // invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
this.mCloudAppResurrectionName = v0;
/* .line 748 */
final String v0 = "app_resur_max_child_count"; // const-string v0, "app_resur_max_child_count"
int v1 = 3; // const/4 v1, 0x3
v0 = (( org.json.JSONObject ) v12 ).optInt ( v0, v1 ); // invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I */
/* .line 749 */
final String v0 = "app_resur_max_child_map"; // const-string v0, "app_resur_max_child_map"
(( org.json.JSONObject ) v12 ).optJSONObject ( v0 ); // invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
this.mCloudPkg2MaxChildCountJObject = v0;
/* .line 750 */
final String v0 = "app_resur_inactive_hour"; // const-string v0, "app_resur_inactive_hour"
/* const/16 v1, 0x24 */
v0 = (( org.json.JSONObject ) v12 ).optInt ( v0, v1 ); // invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v0, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionInactiveDurationHour:I */
/* .line 752 */
final String v0 = "app_resur_high_cpu_launch_time_threshold"; // const-string v0, "app_resur_high_cpu_launch_time_threshold"
/* const-wide/16 v1, 0xbb8 */
(( org.json.JSONObject ) v12 ).optLong ( v0, v1, v2 ); // invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J
/* move-result-wide v13 */
/* .line 753 */
/* .local v13, "highCPULaunchTimeThreshold":J */
final String v0 = "app_resur_middle_cpu_launch_time_threshold"; // const-string v0, "app_resur_middle_cpu_launch_time_threshold"
/* const-wide/16 v1, 0xdac */
(( org.json.JSONObject ) v12 ).optLong ( v0, v1, v2 ); // invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J
/* move-result-wide v15 */
/* .line 754 */
/* .local v15, "middleCPULaunchTimeThreshold":J */
final String v0 = "app_resur_low_cpu_launch_time_threshold"; // const-string v0, "app_resur_low_cpu_launch_time_threshold"
(( org.json.JSONObject ) v12 ).optLong ( v0, v1, v2 ); // invoke-virtual {v12, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J
/* move-result-wide v17 */
/* .line 755 */
/* .local v17, "lowCPULaunchTimeThreshold":J */
/* move-wide/from16 v7, v17 */
/* .line 756 */
/* .local v7, "unknwonCPULaunchTimeThreshold":J */
/* move-object/from16 v0, p0 */
/* move-wide v1, v13 */
/* move-wide v3, v15 */
/* move-wide/from16 v5, v17 */
/* invoke-direct/range {v0 ..v8}, Lcom/android/server/wm/AppResurrectionServiceImpl;->checkCPULevel(JJJJ)V */
/* .line 758 */
final String v0 = "app_resur_pkg"; // const-string v0, "app_resur_pkg"
(( org.json.JSONObject ) v12 ).optJSONArray ( v0 ); // invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 759 */
/* .local v0, "appResurJArray":Lorg/json/JSONArray; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 760 */
this.mCloudAppResurrectionPKGJArray = v0;
/* .line 763 */
} // :cond_2
final String v1 = "app_resur_activity"; // const-string v1, "app_resur_activity"
(( org.json.JSONObject ) v12 ).optJSONArray ( v1 ); // invoke-virtual {v12, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 764 */
/* .local v1, "appResurActivityJArray":Lorg/json/JSONArray; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 765 */
this.mCloudAppResurrectionActivityJArray = v1;
/* .line 768 */
} // :cond_3
final String v2 = "reboot_resur_pkg"; // const-string v2, "reboot_resur_pkg"
(( org.json.JSONObject ) v12 ).optJSONArray ( v2 ); // invoke-virtual {v12, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 769 */
/* .local v2, "appRebootResurJArray":Lorg/json/JSONArray; */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 770 */
this.mCloudRebootResurrectionPKGJArray = v2;
/* .line 773 */
} // :cond_4
final String v3 = "reboot_resur_activity"; // const-string v3, "reboot_resur_activity"
(( org.json.JSONObject ) v12 ).optJSONArray ( v3 ); // invoke-virtual {v12, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 774 */
/* .local v3, "appRebootResurActivityJArray":Lorg/json/JSONArray; */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 775 */
this.mCloudRebootResurrectionActivityJArray = v3;
/* .line 779 */
} // :cond_5
final String v4 = "reboot_resur_enable"; // const-string v4, "reboot_resur_enable"
v4 = (( org.json.JSONObject ) v12 ).optBoolean ( v4, v10 ); // invoke-virtual {v12, v4, v10}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v4, v9, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z */
/* .line 780 */
} // .end local v0 # "appResurJArray":Lorg/json/JSONArray;
} // .end local v1 # "appResurActivityJArray":Lorg/json/JSONArray;
} // .end local v2 # "appRebootResurJArray":Lorg/json/JSONArray;
} // .end local v3 # "appRebootResurActivityJArray":Lorg/json/JSONArray;
} // .end local v7 # "unknwonCPULaunchTimeThreshold":J
} // .end local v13 # "highCPULaunchTimeThreshold":J
} // .end local v15 # "middleCPULaunchTimeThreshold":J
} // .end local v17 # "lowCPULaunchTimeThreshold":J
/* .line 781 */
} // :cond_6
final String v1 = "getCloudData cloudDataJson == null"; // const-string v1, "getCloudData cloudDataJson == null"
android.util.Slog .v ( v0,v1 );
/* .line 784 */
} // .end local v11 # "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v12 # "cloudDataJson":Lorg/json/JSONObject;
} // :cond_7
} // :goto_0
return;
} // .end method
private Boolean isCPUHighLevelDevice ( ) {
/* .locals 3 */
/* .line 811 */
int v1 = 1; // const/4 v1, 0x1
v0 = miui.util.DeviceLevel .getDeviceLevel ( v1,v0 );
/* if-ne v0, v2, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private Boolean isCPULowLevelDevice ( ) {
/* .locals 3 */
/* .line 819 */
int v1 = 1; // const/4 v1, 0x1
v0 = miui.util.DeviceLevel .getDeviceLevel ( v1,v0 );
/* if-ne v0, v2, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private Boolean isCPUMiddleLevelDevice ( ) {
/* .locals 3 */
/* .line 815 */
int v1 = 1; // const/4 v1, 0x1
v0 = miui.util.DeviceLevel .getDeviceLevel ( v1,v0 );
/* if-ne v0, v2, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private Boolean isCPUUnknownLevelDevice ( ) {
/* .locals 3 */
/* .line 823 */
int v1 = 1; // const/4 v1, 0x1
v0 = miui.util.DeviceLevel .getDeviceLevel ( v1,v0 );
/* if-ne v0, v2, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private Boolean isChildrenActivityFromSamePkg ( com.android.server.wm.Task p0 ) {
/* .locals 5 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 715 */
(( com.android.server.wm.Task ) p1 ).getRootActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;
v0 = this.packageName;
/* .line 716 */
/* .local v0, "rootActivityPKG":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v2 = "AppResurrectionServiceImpl"; // const-string v2, "AppResurrectionServiceImpl"
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isChildrenActivitySameFromPkg ,rootActivityPKG:"; // const-string v3, "isChildrenActivitySameFromPkg ,rootActivityPKG:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", task.getRootActivity():"; // const-string v3, ", task.getRootActivity():"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.Task ) p1 ).getRootActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 718 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Ljava/lang/String;)V */
(( com.android.server.wm.Task ) p1 ).getActivity ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;
/* .line 724 */
/* .local v1, "notSamePKG":Lcom/android/server/wm/ActivityRecord; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 725 */
/* iget-boolean v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "isChildrenActivitySameFromPkg notSamePKGAR="; // const-string v4, "isChildrenActivitySameFromPkg notSamePKGAR="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = ", ret false"; // const-string v4, ", ret false"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 726 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .line 728 */
} // :cond_2
/* iget-boolean v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_3
final String v3 = "isChildrenActivitySameFromPkg notSamePKGAR == null, ret true"; // const-string v3, "isChildrenActivitySameFromPkg notSamePKGAR == null, ret true"
android.util.Slog .d ( v2,v3 );
/* .line 729 */
} // :cond_3
int v2 = 1; // const/4 v2, 0x1
} // .end method
private Boolean isDeviceSupport ( ) {
/* .locals 2 */
/* .line 710 */
final String v0 = "persist.sys.app_resurrection.enable"; // const-string v0, "persist.sys.app_resurrection.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 711 */
/* .local v0, "isDeviceSupport":Z */
} // .end method
private Boolean isKeyInJArray ( org.json.JSONArray p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "jArray" # Lorg/json/JSONArray; */
/* .param p2, "key" # Ljava/lang/String; */
/* .line 673 */
v0 = android.text.TextUtils .isEmpty ( p2 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 674 */
/* .line 677 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 678 */
v0 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* .line 679 */
/* .local v0, "len":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_2 */
/* .line 680 */
final String v3 = ""; // const-string v3, ""
(( org.json.JSONArray ) p1 ).optString ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;
v3 = (( java.lang.String ) p2 ).equals ( v3 ); // invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 681 */
int v1 = 1; // const/4 v1, 0x1
/* .line 679 */
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 684 */
} // .end local v2 # "i":I
} // :cond_2
/* .line 686 */
} // .end local v0 # "len":I
} // :cond_3
} // .end method
private Boolean isPKGInAppResurrectionActivityList ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "PKG" # Ljava/lang/String; */
/* .line 837 */
v0 = this.mCloudAppResurrectionActivityJArray;
final String v1 = "/"; // const-string v1, "/"
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 838 */
v0 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* .line 839 */
/* .local v0, "len":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v0, :cond_1 */
/* .line 840 */
v4 = this.mCloudAppResurrectionActivityJArray;
final String v5 = ""; // const-string v5, ""
(( org.json.JSONArray ) v4 ).optString ( v3, v5 ); // invoke-virtual {v4, v3, v5}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;
/* .line 841 */
/* .local v4, "activityStr":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v5 = (( java.lang.String ) v4 ).startsWith ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 842 */
/* .line 839 */
} // .end local v4 # "activityStr":Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 845 */
} // .end local v0 # "len":I
} // .end local v3 # "i":I
} // :cond_1
/* .line 846 */
} // :cond_2
v0 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultAppResurrectionEnableActivityList;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Ljava/lang/String; */
/* .line 847 */
/* .local v3, "activityStr":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v4 = (( java.lang.String ) v3 ).startsWith ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 848 */
/* .line 850 */
} // .end local v3 # "activityStr":Ljava/lang/String;
} // :cond_3
/* .line 852 */
} // :cond_4
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void lambda$init$0 ( ) { //synthethic
/* .locals 9 */
/* .line 195 */
/* const-wide/16 v1, 0xbb8 */
/* const-wide/16 v3, 0xdac */
/* const-wide/16 v5, 0xdac */
/* const-wide/16 v7, 0xdac */
/* move-object v0, p0 */
/* invoke-direct/range {v0 ..v8}, Lcom/android/server/wm/AppResurrectionServiceImpl;->checkCPULevel(JJJJ)V */
return;
} // .end method
private void lambda$init$1 ( ) { //synthethic
/* .locals 0 */
/* .line 198 */
/* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->getCloudData()V */
return;
} // .end method
private Boolean lambda$isChildrenActivityFromSamePkg$7 ( java.lang.String p0, com.android.server.wm.ActivityRecord p1 ) { //synthethic
/* .locals 3 */
/* .param p1, "rootActivityPKG" # Ljava/lang/String; */
/* .param p2, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 719 */
v0 = this.packageName;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/lit8 v0, v0, 0x1 */
/* .line 720 */
/* .local v0, "ret":Z */
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isChildrenActivitySameFromPkg getActivity, r.packageName:"; // const-string v2, "isChildrenActivitySameFromPkg getActivity, r.packageName:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", rootActivityPKG="; // const-string v2, ", rootActivityPKG="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", ret="; // const-string v2, ", ret="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AppResurrectionServiceImpl"; // const-string v2, "AppResurrectionServiceImpl"
android.util.Slog .d ( v2,v1 );
/* .line 721 */
} // :cond_0
} // .end method
private void lambda$notifyWindowsDrawn$3 ( android.content.ComponentName p0, Long p1, java.lang.String p2 ) { //synthethic
/* .locals 1 */
/* .param p1, "curComponent" # Landroid/content/ComponentName; */
/* .param p2, "windowsDrawnDelay" # J */
/* .param p4, "fKillReason" # Ljava/lang/String; */
/* .line 568 */
(( android.content.ComponentName ) p1 ).flattenToShortString ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->logAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V */
return;
} // .end method
private void lambda$notifyWindowsDrawn$4 ( android.content.ComponentName p0, Long p1, java.lang.String p2 ) { //synthethic
/* .locals 1 */
/* .param p1, "curComponent" # Landroid/content/ComponentName; */
/* .param p2, "windowsDrawnDelay" # J */
/* .param p4, "fKillReason" # Ljava/lang/String; */
/* .line 569 */
(( android.content.ComponentName ) p1 ).flattenToShortString ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->trackAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V */
return;
} // .end method
private void lambda$notifyWindowsDrawn$5 ( com.android.server.wm.ActivityRecord p0, Long p1, java.lang.String p2 ) { //synthethic
/* .locals 1 */
/* .param p1, "curTransitionActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "windowsDrawnDelay" # J */
/* .param p4, "fKillReason" # Ljava/lang/String; */
/* .line 571 */
v0 = this.packageName;
/* invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->logAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V */
return;
} // .end method
private void lambda$notifyWindowsDrawn$6 ( com.android.server.wm.ActivityRecord p0, Long p1, java.lang.String p2 ) { //synthethic
/* .locals 1 */
/* .param p1, "curTransitionActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "windowsDrawnDelay" # J */
/* .param p4, "fKillReason" # Ljava/lang/String; */
/* .line 572 */
v0 = this.packageName;
/* invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionServiceImpl;->trackAppResurrectionDisplayed(Ljava/lang/String;JLjava/lang/String;)V */
return;
} // .end method
private void lambda$updateStartingWindowResolvedTheme$2 ( java.lang.String p0, Integer p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "resolvedTheme" # I */
/* .line 499 */
java.lang.Integer .valueOf ( p2 );
/* invoke-direct {p0, p1, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->saveOneDataToSP(Ljava/lang/String;Ljava/lang/Integer;)V */
return;
} // .end method
private void loadDataFromSP ( ) {
/* .locals 9 */
/* .line 896 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v1 = "AppResurrectionServiceImpl"; // const-string v1, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "loadDataFromSP start"; // const-string v0, "loadDataFromSP start"
android.util.Slog .d ( v1,v0 );
/* .line 898 */
} // :cond_0
try { // :try_start_0
v0 = this.mContext;
/* if-nez v0, :cond_1 */
/* .line 899 */
return;
/* .line 901 */
} // :cond_1
v2 = this.mLocalSpFile;
int v3 = 0; // const/4 v3, 0x0
(( android.content.Context ) v0 ).getSharedPreferences ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 902 */
/* .local v0, "sp":Landroid/content/SharedPreferences; */
/* .line 903 */
/* .local v2, "keyValueMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 904 */
/* .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* check-cast v5, Ljava/lang/String; */
/* .line 905 */
/* .local v5, "pkg":Ljava/lang/String; */
/* check-cast v6, Ljava/lang/Integer; */
/* .line 906 */
/* .local v6, "theme":Ljava/lang/Integer; */
v7 = this.mPkg2StartThemeMap;
if ( v7 != null) { // if-eqz v7, :cond_3
v7 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v7, :cond_3 */
/* .line 907 */
/* iget-boolean v7, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_2
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "loadDataFromSP pkg="; // const-string v8, "loadDataFromSP pkg="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ", theme="; // const-string v8, ", theme="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v7 );
/* .line 908 */
} // :cond_2
v7 = this.mPkg2StartThemeMap;
(( java.util.HashMap ) v7 ).put ( v5, v6 ); // invoke-virtual {v7, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 910 */
} // .end local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
} // .end local v5 # "pkg":Ljava/lang/String;
} // .end local v6 # "theme":Ljava/lang/Integer;
} // :cond_3
/* .line 913 */
} // .end local v0 # "sp":Landroid/content/SharedPreferences;
} // .end local v2 # "keyValueMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
} // :cond_4
/* .line 911 */
/* :catch_0 */
/* move-exception v0 */
/* .line 912 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "loadDataFromSP Exception e:"; // const-string v3, "loadDataFromSP Exception e:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 914 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
final String v0 = "loadDataFromSP done"; // const-string v0, "loadDataFromSP done"
android.util.Slog .d ( v1,v0 );
/* .line 915 */
} // :cond_5
return;
} // .end method
private void logAppResurrectionDisplayed ( java.lang.String p0, Long p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "shortComponentName" # Ljava/lang/String; */
/* .param p2, "delay" # J */
/* .param p4, "killReason" # Ljava/lang/String; */
/* .line 827 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "logAppResurrectionDisplayed, delay="; // const-string v1, "logAppResurrectionDisplayed, delay="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", comp = "; // const-string v1, ", comp = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", killReason= "; // const-string v1, ", killReason= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AppResurrectionServiceImpl"; // const-string v1, "AppResurrectionServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 828 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "["; // const-string v1, "["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ","; // const-string v1, ","
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v1, 0xfb928 */
android.util.EventLog .writeEvent ( v1,v0 );
/* .line 829 */
return;
} // .end method
private java.lang.String printList ( java.util.ArrayList p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Ljava/lang/String;" */
/* } */
} // .end annotation
/* .line 660 */
/* .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
final String v0 = ""; // const-string v0, ""
/* .line 661 */
/* .local v0, "ret":Ljava/lang/String; */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 662 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 663 */
/* .local v1, "sb":Ljava/lang/StringBuilder; */
(( java.util.ArrayList ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/lang/String; */
/* .line 664 */
/* .local v3, "pkg":Ljava/lang/String; */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 665 */
final String v4 = ", "; // const-string v4, ", "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 666 */
} // .end local v3 # "pkg":Ljava/lang/String;
/* .line 667 */
} // :cond_0
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 669 */
} // .end local v1 # "sb":Ljava/lang/StringBuilder;
} // :cond_1
} // .end method
private Boolean removeKeyFromJArray ( org.json.JSONArray p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "jArray" # Lorg/json/JSONArray; */
/* .param p2, "key" # Ljava/lang/String; */
/* .line 691 */
v0 = android.text.TextUtils .isEmpty ( p2 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 692 */
/* .line 695 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 696 */
v0 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* .line 697 */
/* .local v0, "len":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "index":I */
} // :goto_0
/* if-ge v2, v0, :cond_2 */
/* .line 698 */
final String v3 = ""; // const-string v3, ""
(( org.json.JSONArray ) p1 ).optString ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;
v3 = (( java.lang.String ) p2 ).equals ( v3 ); // invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 699 */
(( org.json.JSONArray ) p1 ).remove ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONArray;->remove(I)Ljava/lang/Object;
/* .line 700 */
int v1 = 1; // const/4 v1, 0x1
/* .line 697 */
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 703 */
} // .end local v2 # "index":I
} // :cond_2
/* .line 705 */
} // .end local v0 # "len":I
} // :cond_3
} // .end method
private void saveAllDataToSP ( ) {
/* .locals 7 */
/* .line 874 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v1 = "AppResurrectionServiceImpl"; // const-string v1, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "saveAllDataToSP"; // const-string v0, "saveAllDataToSP"
android.util.Slog .d ( v1,v0 );
/* .line 876 */
} // :cond_0
try { // :try_start_0
v0 = this.mContext;
/* if-nez v0, :cond_1 */
/* .line 877 */
return;
/* .line 879 */
} // :cond_1
v2 = this.mLocalSpFile;
int v3 = 0; // const/4 v3, 0x0
(( android.content.Context ) v0 ).getSharedPreferences ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 880 */
/* .local v0, "editor":Landroid/content/SharedPreferences$Editor; */
v2 = this.mPkg2StartThemeMap;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 881 */
(( java.util.HashMap ) v2 ).entrySet ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 882 */
/* .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* check-cast v4, Ljava/lang/String; */
/* .line 883 */
/* .local v4, "key":Ljava/lang/String; */
/* check-cast v5, Ljava/lang/Integer; */
/* .line 884 */
/* .local v5, "value":Ljava/lang/Integer; */
v6 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v6, :cond_2 */
/* .line 885 */
v6 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* .line 887 */
} // .end local v3 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
} // .end local v4 # "key":Ljava/lang/String;
} // .end local v5 # "value":Ljava/lang/Integer;
} // :cond_2
/* .line 889 */
} // :cond_3
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 892 */
} // .end local v0 # "editor":Landroid/content/SharedPreferences$Editor;
/* .line 890 */
/* :catch_0 */
/* move-exception v0 */
/* .line 891 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "saveAllDataToSP Exception e:"; // const-string v3, "saveAllDataToSP Exception e:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 893 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void saveOneDataToSP ( java.lang.String p0, java.lang.Integer p1 ) {
/* .locals 4 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "theme" # Ljava/lang/Integer; */
/* .line 856 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v1 = "AppResurrectionServiceImpl"; // const-string v1, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "saveOneDataToSP, pkg="; // const-string v2, "saveOneDataToSP, pkg="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", theme = "; // const-string v2, ", theme = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 858 */
} // :cond_0
try { // :try_start_0
v0 = this.mContext;
/* if-nez v0, :cond_1 */
/* .line 859 */
return;
/* .line 861 */
} // :cond_1
v2 = this.mLocalSpFile;
int v3 = 0; // const/4 v3, 0x0
(( android.content.Context ) v0 ).getSharedPreferences ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 862 */
/* .local v0, "editor":Landroid/content/SharedPreferences$Editor; */
v2 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v2, :cond_2 */
/* .line 863 */
v2 = (( java.lang.Integer ) p2 ).intValue ( ); // invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I
/* .line 865 */
} // :cond_2
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 868 */
} // .end local v0 # "editor":Landroid/content/SharedPreferences$Editor;
/* .line 866 */
/* :catch_0 */
/* move-exception v0 */
/* .line 867 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "saveAllDataToSP Exception e:"; // const-string v3, "saveAllDataToSP Exception e:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 869 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
final String v0 = "saveOneDataToSP done"; // const-string v0, "saveOneDataToSP done"
android.util.Slog .d ( v1,v0 );
/* .line 870 */
} // :cond_3
return;
} // .end method
private void trackAppResurrectionDisplayed ( java.lang.String p0, Long p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "shortComponentName" # Ljava/lang/String; */
/* .param p2, "delay" # J */
/* .param p4, "killReason" # Ljava/lang/String; */
/* .line 832 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "trackAppResurrectionDisplayed, delay=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", comp = "; // const-string v1, ", comp = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", killReason="; // const-string v1, ", killReason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AppResurrectionServiceImpl"; // const-string v1, "AppResurrectionServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 833 */
} // :cond_0
v0 = this.mTrackManager;
(( com.android.server.wm.AppResurrectionTrackManager ) v0 ).sendTrack ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/AppResurrectionTrackManager;->sendTrack(Ljava/lang/String;JLjava/lang/String;)V
/* .line 834 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 615 */
/* if-nez p1, :cond_0 */
/* .line 616 */
return;
/* .line 618 */
} // :cond_0
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurDevice:"; // const-string v1, "AppResurDevice:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 619 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurName:"; // const-string v1, "AppResurName:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCloudAppResurrectionName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 620 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurVer:"; // const-string v1, "AppResurVer:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionVersion:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 621 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurEnable:"; // const-string v1, "AppResurEnable:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 622 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurCPU:"; // const-string v1, "AppResurCPU:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCPULevel;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 623 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurLaunchThres:"; // const-string v1, "AppResurLaunchThres:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionLaunchTimeThresholdMillis:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 624 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurMaxChildCount:"; // const-string v1, "AppResurMaxChildCount:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 625 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurInactiveDuraH:"; // const-string v1, "AppResurInactiveDuraH:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionInactiveDurationHour:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 628 */
v0 = this.mCloudPkg2MaxChildCountJObject;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 629 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurPkg2MaxChildC:"; // const-string v1, "AppResurPkg2MaxChildC:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCloudPkg2MaxChildCountJObject;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 630 */
} // :cond_1
v0 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultPkg2MaxChildCountJObject;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 631 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "AppResurPkg2MaxChildD:"; // const-string v2, "AppResurPkg2MaxChildD:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 634 */
} // :cond_2
} // :goto_0
v0 = this.mCloudAppResurrectionPKGJArray;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 635 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurPkgCArray:"; // const-string v1, "AppResurPkgCArray:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCloudAppResurrectionPKGJArray;
(( org.json.JSONArray ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 637 */
} // :cond_3
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurPkgDArray:"; // const-string v1, "AppResurPkgDArray:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultAppResurrectionEnablePKGList;
/* invoke-direct {p0, v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->printList(Ljava/util/ArrayList;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 640 */
} // :goto_1
v0 = this.mCloudAppResurrectionActivityJArray;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 641 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurActivityCArray:"; // const-string v1, "AppResurActivityCArray:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCloudAppResurrectionActivityJArray;
(( org.json.JSONArray ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 643 */
} // :cond_4
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AppResurActivityDArray:"; // const-string v1, "AppResurActivityDArray:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultAppResurrectionEnableActivityList;
/* invoke-direct {p0, v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->printList(Ljava/util/ArrayList;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 646 */
} // :goto_2
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ReResurEnable:"; // const-string v1, "ReResurEnable:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 647 */
v0 = this.mCloudRebootResurrectionPKGJArray;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 648 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ReResurPkgCArray:"; // const-string v1, "ReResurPkgCArray:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCloudRebootResurrectionPKGJArray;
(( org.json.JSONArray ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 650 */
} // :cond_5
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ReResurPkgDArray:"; // const-string v1, "ReResurPkgDArray:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultRebootResurrectionEnablePkgList;
/* invoke-direct {p0, v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->printList(Ljava/util/ArrayList;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 652 */
} // :goto_3
v0 = this.mCloudRebootResurrectionActivityJArray;
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 653 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ReResurActivityCArray:"; // const-string v1, "ReResurActivityCArray:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCloudRebootResurrectionActivityJArray;
(( org.json.JSONArray ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 655 */
} // :cond_6
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ReResurActivityDArray:"; // const-string v1, "ReResurActivityDArray:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultRebootResurrectionEnableActivityList;
/* invoke-direct {p0, v1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->printList(Ljava/util/ArrayList;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 657 */
} // :goto_4
return;
} // .end method
public Integer getActivityIndex ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "tag" # Ljava/lang/String; */
/* .line 941 */
v0 = (( com.android.server.wm.AppResurrectionServiceImpl ) p0 ).isActivityTag ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isActivityTag(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 942 */
/* new-instance v0, Ljava/util/StringTokenizer; */
final String v1 = "reborn_activity"; // const-string v1, "reborn_activity"
/* invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 943 */
/* .local v0, "stringTokenizer":Ljava/util/StringTokenizer; */
(( java.util.StringTokenizer ) v0 ).nextToken ( ); // invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;
v1 = java.lang.Integer .parseInt ( v1 );
/* .line 944 */
/* .local v1, "index":I */
/* .line 946 */
} // .end local v0 # "stringTokenizer":Ljava/util/StringTokenizer;
} // .end local v1 # "index":I
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
} // .end method
public Integer getAllowChildCount ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 206 */
v0 = this.mCloudPkg2MaxChildCountJObject;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 207 */
v0 = (( org.json.JSONObject ) v0 ).has ( p1 ); // invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 208 */
v0 = this.mCloudPkg2MaxChildCountJObject;
/* iget v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I */
v0 = (( org.json.JSONObject ) v0 ).optInt ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* .line 210 */
} // :cond_0
v0 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultPkg2MaxChildCountJObject;
v1 = (( org.json.JSONObject ) v0 ).has ( p1 ); // invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 211 */
/* iget v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I */
v0 = (( org.json.JSONObject ) v0 ).optInt ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* .line 213 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionMaxChildCount:I */
} // .end method
public Integer getSplashScreenTheme ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3, Boolean p4, Boolean p5 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "resolvedTheme" # I */
/* .param p3, "newTask" # Z */
/* .param p4, "taskSwitch" # Z */
/* .param p5, "processRunning" # Z */
/* .param p6, "startActivity" # Z */
/* .line 424 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_a
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_0 */
/* .line 436 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v1 = ", resolvedTheme:"; // const-string v1, ", resolvedTheme:"
final String v2 = "AppResurrectionServiceImpl"; // const-string v2, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 437 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getSplashScreenResolvedTheme, pkg:"; // const-string v3, "getSplashScreenResolvedTheme, pkg:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", newTask:"; // const-string v3, ", newTask:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", taskSwitch:"; // const-string v3, ", taskSwitch:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ",processRunning:"; // const-string v3, ",processRunning:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", startActivity:"; // const-string v3, ", startActivity:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p6 ); // invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 440 */
} // :cond_1
if ( p3 != null) { // if-eqz p3, :cond_4
if ( p4 != null) { // if-eqz p4, :cond_4
/* if-nez p5, :cond_4 */
if ( p6 != null) { // if-eqz p6, :cond_4
/* .line 442 */
v0 = this.mPkg2StartThemeMap;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 443 */
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* const/16 v1, 0x32 */
/* if-le v0, v1, :cond_2 */
/* .line 444 */
v0 = this.mPkg2StartThemeMap;
(( java.util.HashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
/* .line 446 */
} // :cond_2
v0 = this.mPkg2StartThemeMap;
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 448 */
} // :cond_3
/* .line 449 */
} // :cond_4
/* if-nez p3, :cond_7 */
if ( p4 != null) { // if-eqz p4, :cond_7
/* if-nez p5, :cond_7 */
/* .line 451 */
v0 = this.mPkg2StartThemeMap;
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 452 */
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 453 */
v0 = this.mPkg2StartThemeMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 454 */
/* .local v0, "retResolvedTheme":I */
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getSplashScreenResolvedTheme reborn, get store "; // const-string v3, "getSplashScreenResolvedTheme reborn, get store "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", retResolvedTheme:"; // const-string v3, ", retResolvedTheme:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 456 */
} // :cond_5
this.mRebornPkg = p1;
/* .line 457 */
/* .line 460 */
} // .end local v0 # "retResolvedTheme":I
} // :cond_6
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getSplashScreenResolvedTheme, no get "; // const-string v3, "getSplashScreenResolvedTheme, no get "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 462 */
/* .line 463 */
} // :cond_7
/* if-nez p3, :cond_9 */
/* if-nez p4, :cond_9 */
if ( p5 != null) { // if-eqz p5, :cond_9
/* if-nez p6, :cond_9 */
/* .line 465 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
final String v0 = "getSplashScreenResolvedTheme, back to main, resolvedTheme = 0"; // const-string v0, "getSplashScreenResolvedTheme, back to main, resolvedTheme = 0"
android.util.Slog .d ( v2,v0 );
/* .line 466 */
} // :cond_8
int v0 = 0; // const/4 v0, 0x0
/* .line 468 */
} // :cond_9
/* .line 425 */
} // :cond_a
} // :goto_0
} // .end method
public Integer getStartingWindowResolvedTheme ( java.lang.String p0, Boolean p1, Boolean p2, Boolean p3, Boolean p4 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "newTask" # Z */
/* .param p3, "taskSwitch" # Z */
/* .param p4, "processRunning" # Z */
/* .param p5, "startActivity" # Z */
/* .line 512 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_1 */
/* .line 515 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v2 = "AppResurrectionServiceImpl"; // const-string v2, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getStartingWindowResolvedTheme, start newTask="; // const-string v3, "getStartingWindowResolvedTheme, start newTask="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", taskSwitch="; // const-string v3, ", taskSwitch="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", processRunning="; // const-string v3, ", processRunning="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", startActivity="; // const-string v3, ", startActivity="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", mRebornPkg="; // const-string v3, ", mRebornPkg="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mRebornPkg;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 526 */
} // :cond_1
/* if-nez p2, :cond_3 */
if ( p3 != null) { // if-eqz p3, :cond_3
/* if-nez p4, :cond_3 */
/* .line 528 */
v0 = this.mPkg2StartThemeMap;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 529 */
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 530 */
v0 = this.mPkg2StartThemeMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 531 */
/* .local v0, "retResolvedTheme":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getStartingWindowResolvedTheme, get stored "; // const-string v3, "getStartingWindowResolvedTheme, get stored "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", retResolvedTheme:"; // const-string v3, ", retResolvedTheme:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v1 );
/* .line 533 */
this.mRebornPkg = p1;
/* .line 534 */
/* .line 536 */
} // .end local v0 # "retResolvedTheme":I
} // :cond_2
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getStartingWindowResolvedTheme, no get "; // const-string v3, "getStartingWindowResolvedTheme, no get "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", ret 0"; // const-string v3, ", ret 0"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 538 */
} // :cond_3
/* if-nez p2, :cond_4 */
/* if-nez p3, :cond_4 */
if ( p4 != null) { // if-eqz p4, :cond_4
/* if-nez p5, :cond_4 */
/* .line 540 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
final String v0 = "getStartingWindowResolvedTheme, back to main"; // const-string v0, "getStartingWindowResolvedTheme, back to main"
android.util.Slog .d ( v2,v0 );
/* .line 542 */
} // :cond_4
} // :goto_0
/* .line 513 */
} // :cond_5
} // :goto_1
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 154 */
this.mContext = p1;
/* .line 155 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isDeviceSupport()Z */
/* iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
/* .line 156 */
/* if-nez v0, :cond_0 */
/* .line 157 */
return;
/* .line 159 */
} // :cond_0
final String v0 = "persist.sys.test_app_resurrection.enable"; // const-string v0, "persist.sys.test_app_resurrection.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsTestMode:Z */
/* .line 160 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mPkg2StartThemeMap = v0;
/* .line 161 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mPkg2KillReasonMap = v0;
/* .line 162 */
/* new-instance v0, Lcom/android/server/wm/AppResurrectionCloudManager; */
/* invoke-direct {v0, p1}, Lcom/android/server/wm/AppResurrectionCloudManager;-><init>(Landroid/content/Context;)V */
this.mCloudManager = v0;
/* .line 163 */
/* new-instance v0, Lcom/android/server/wm/AppResurrectionTrackManager; */
/* invoke-direct {v0, p1}, Lcom/android/server/wm/AppResurrectionTrackManager;-><init>(Landroid/content/Context;)V */
this.mTrackManager = v0;
/* .line 164 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mResolver = v0;
/* .line 165 */
/* nop */
/* .line 166 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/wm/AppResurrectionServiceImpl$1; */
v3 = this.mMIUIBgHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/wm/AppResurrectionServiceImpl$1;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Landroid/os/Handler;)V */
/* .line 165 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 176 */
/* new-instance v0, Lcom/android/server/wm/AppResurrectionServiceImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/AppResurrectionServiceImpl$2;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;)V */
this.mBroadcastReceiver = v0;
/* .line 190 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 191 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.USER_PRESENT"; // const-string v1, "android.intent.action.USER_PRESENT"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 192 */
v1 = this.mBroadcastReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 195 */
v1 = this.mMIUIBgHandler;
/* new-instance v2, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 198 */
v1 = this.mMIUIBgHandler;
/* new-instance v2, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, p0}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 199 */
return;
} // .end method
public Boolean isActivityTag ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/String; */
/* .line 934 */
final String v0 = "reborn_activity"; // const-string v0, "reborn_activity"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 935 */
int v0 = 1; // const/4 v0, 0x1
/* .line 936 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isRebootResurrectionActivity ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "activityName" # Ljava/lang/String; */
/* .line 971 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsTestMode:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 972 */
/* .line 974 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z */
/* if-nez v0, :cond_1 */
/* .line 977 */
} // :cond_1
v0 = this.mCloudRebootResurrectionActivityJArray;
/* if-nez v0, :cond_2 */
/* .line 978 */
v0 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultRebootResurrectionEnableActivityList;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 979 */
/* .line 982 */
} // :cond_2
v0 = /* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 983 */
/* .line 987 */
} // :cond_3
/* .line 975 */
} // :cond_4
} // :goto_0
} // .end method
public Boolean isRebootResurrectionEnable ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 951 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsTestMode:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 952 */
/* .line 954 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudRebootResurrectionEnable:Z */
/* if-nez v0, :cond_1 */
/* .line 957 */
} // :cond_1
v0 = this.mCloudRebootResurrectionPKGJArray;
/* if-nez v0, :cond_2 */
/* .line 958 */
v0 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultRebootResurrectionEnablePkgList;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 959 */
/* .line 962 */
} // :cond_2
v0 = /* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 963 */
/* .line 966 */
} // :cond_3
/* .line 955 */
} // :cond_4
} // :goto_0
} // .end method
public Boolean isRestoreTask ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 608 */
/* nop */
/* .line 609 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isResurrectionEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 218 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v1 = "AppResurrectionServiceImpl"; // const-string v1, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isResurrectionEnable, start "; // const-string v2, "isResurrectionEnable, start "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 219 */
} // :cond_0
v0 = android.text.TextUtils .isEmpty ( p1 );
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 220 */
/* .line 223 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
/* if-nez v0, :cond_2 */
/* .line 226 */
} // :cond_2
v0 = this.mCloudAppResurrectionPKGJArray;
int v3 = 1; // const/4 v3, 0x1
final String v4 = "isResurrectionEnable, End "; // const-string v4, "isResurrectionEnable, End "
/* if-nez v0, :cond_4 */
/* .line 227 */
v0 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultAppResurrectionEnablePKGList;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 228 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " in default, ret true"; // const-string v2, " in default, ret true"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 229 */
} // :cond_3
/* .line 232 */
} // :cond_4
v0 = /* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 233 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " in cloud, ret true"; // const-string v2, " in cloud, ret true"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 234 */
} // :cond_5
/* .line 237 */
} // :cond_6
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " ret false"; // const-string v3, " ret false"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 238 */
} // :cond_7
/* .line 224 */
} // :cond_8
} // :goto_0
} // .end method
public Boolean isResurrectionEnable ( java.lang.String p0, java.lang.String p1, java.util.List p2 ) {
/* .locals 12 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Landroid/app/ActivityManager$RunningTaskInfo;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 243 */
/* .local p3, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 244 */
/* .local v0, "ret":Z */
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
if ( v1 != null) { // if-eqz v1, :cond_16
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
/* if-nez v1, :cond_0 */
/* goto/16 :goto_5 */
/* .line 249 */
} // :cond_0
v1 = (( com.android.server.wm.AppResurrectionServiceImpl ) p0 ).isResurrectionEnable ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isResurrectionEnable(Ljava/lang/String;)Z
/* if-nez v1, :cond_1 */
/* .line 250 */
/* .line 253 */
} // :cond_1
final String v1 = ", reason:"; // const-string v1, ", reason:"
int v2 = 0; // const/4 v2, 0x0
final String v3 = "AppResurrectionServiceImpl"; // const-string v3, "AppResurrectionServiceImpl"
if ( p3 != null) { // if-eqz p3, :cond_14
v4 = /* .line 254 */
/* .line 255 */
/* .local v4, "tasksize":I */
} // :cond_2
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_13
/* check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo; */
/* .line 256 */
/* .local v6, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo; */
if ( v6 != null) { // if-eqz v6, :cond_12
v7 = this.baseActivity;
if ( v7 != null) { // if-eqz v7, :cond_12
v7 = this.topActivity;
if ( v7 != null) { // if-eqz v7, :cond_12
/* .line 257 */
v7 = this.baseActivity;
(( android.content.ComponentName ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v7 = (( java.lang.String ) v7 ).equals ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_11
/* .line 258 */
/* iget-boolean v5, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_3
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "isResE, start "; // const-string v7, "isResE, start "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", numAct:"; // const-string v7, ", numAct:"
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", taskInfo="; // const-string v7, ", taskInfo="
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v5 );
/* .line 261 */
} // :cond_3
v5 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isPKGInAppResurrectionActivityList(Ljava/lang/String;)Z */
final String v7 = ", "; // const-string v7, ", "
final String v8 = "isResE, "; // const-string v8, "isResE, "
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 262 */
v5 = this.topActivity;
/* .line 263 */
/* .local v5, "topActivityComp":Landroid/content/ComponentName; */
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 264 */
(( android.content.ComponentName ) v5 ).flattenToShortString ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* .line 265 */
/* .local v9, "topActivityStr":Ljava/lang/String; */
v10 = android.text.TextUtils .isEmpty ( v9 );
/* if-nez v10, :cond_8 */
/* .line 266 */
/* iget-boolean v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v10 != null) { // if-eqz v10, :cond_4
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( p1 ); // invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ", topActivityStr:"; // const-string v11, ", topActivityStr:"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v10 );
/* .line 268 */
} // :cond_4
v10 = this.mCloudAppResurrectionActivityJArray;
if ( v10 != null) { // if-eqz v10, :cond_6
/* .line 269 */
v10 = /* invoke-direct {p0, v10, v9}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z */
/* if-nez v10, :cond_5 */
/* .line 270 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " not in Clist, ret f, setRemove t"; // const-string v7, " not in Clist, ret f, setRemove t"
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v1 );
/* .line 271 */
/* .line 274 */
} // :cond_5
/* iget-boolean v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v10 != null) { // if-eqz v10, :cond_8
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ", topActivity in Clist, go on"; // const-string v11, ", topActivity in Clist, go on"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v10 );
/* .line 277 */
} // :cond_6
v10 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultAppResurrectionEnableActivityList;
v10 = (( java.util.ArrayList ) v10 ).contains ( v9 ); // invoke-virtual {v10, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v10, :cond_7 */
/* .line 278 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " not in Dlist, ret f, setRemove t"; // const-string v7, " not in Dlist, ret f, setRemove t"
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v1 );
/* .line 279 */
/* .line 282 */
} // :cond_7
/* iget-boolean v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v10 != null) { // if-eqz v10, :cond_8
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ", topActivity in DList, go on"; // const-string v11, ", topActivity in DList, go on"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v10 );
/* .line 287 */
} // .end local v5 # "topActivityComp":Landroid/content/ComponentName;
} // .end local v9 # "topActivityStr":Ljava/lang/String;
} // :cond_8
} // :goto_1
/* .line 289 */
} // :cond_9
/* iget-boolean v5, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_a
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ", not in activity list, go on"; // const-string v9, ", not in activity list, go on"
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v5 );
/* .line 292 */
} // :cond_a
} // :goto_2
v5 = (( com.android.server.wm.AppResurrectionServiceImpl ) p0 ).getAllowChildCount ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->getAllowChildCount(Ljava/lang/String;)I
/* .line 293 */
/* .local v5, "allowChildCount":I */
/* iget v9, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I */
int v10 = 2; // const/4 v10, 0x2
/* if-ge v9, v10, :cond_b */
/* .line 294 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I */
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " too less, ret f, setRemove t"; // const-string v7, " too less, ret f, setRemove t"
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v1 );
/* .line 295 */
/* .line 298 */
} // :cond_b
/* iget v9, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I */
/* if-le v9, v5, :cond_c */
/* .line 299 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v6, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I */
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " over allow "; // const-string v7, " over allow "
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", ret f, setRemove t"; // const-string v7, ", ret f, setRemove t"
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v1 );
/* .line 300 */
/* .line 303 */
} // :cond_c
v7 = this.baseActivity;
(( android.content.ComponentName ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 304 */
/* .local v7, "basePkg":Ljava/lang/String; */
v9 = this.topActivity;
(( android.content.ComponentName ) v9 ).getPackageName ( ); // invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 305 */
/* .local v9, "topPkg":Ljava/lang/String; */
v10 = android.text.TextUtils .isEmpty ( v7 );
/* if-nez v10, :cond_d */
v10 = android.text.TextUtils .isEmpty ( v9 );
/* if-nez v10, :cond_d */
v10 = (( java.lang.String ) v7 ).equals ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v10, :cond_d */
/* .line 306 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " not "; // const-string v8, " not "
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " ret f, setRemove t"; // const-string v8, " ret f, setRemove t"
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v1 );
/* .line 307 */
/* .line 310 */
} // :cond_d
v2 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultResurrectionEnableReasonList;
v2 = (( java.util.ArrayList ) v2 ).contains ( p2 ); // invoke-virtual {v2, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_10
/* .line 311 */
v2 = this.mPkg2KillReasonMap;
if ( v2 != null) { // if-eqz v2, :cond_f
/* .line 312 */
v2 = (( java.util.HashMap ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->size()I
/* const/16 v8, 0xc8 */
/* if-le v2, v8, :cond_e */
/* .line 313 */
v2 = this.mPkg2KillReasonMap;
(( java.util.HashMap ) v2 ).clear ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->clear()V
/* .line 315 */
} // :cond_e
v2 = this.mPkg2KillReasonMap;
(( java.util.HashMap ) v2 ).put ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 317 */
} // :cond_f
int v0 = 1; // const/4 v0, 0x1
/* .line 319 */
} // :cond_10
int v0 = 0; // const/4 v0, 0x0
/* .line 321 */
} // :goto_3
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "isResE, final "; // const-string v8, "isResE, final "
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", ret:"; // const-string v2, ", ret:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " setRemove:"; // const-string v2, " setRemove:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* xor-int/lit8 v2, v0, 0x1 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v1 );
/* .line 322 */
/* .line 324 */
} // .end local v5 # "allowChildCount":I
} // .end local v7 # "basePkg":Ljava/lang/String;
} // .end local v9 # "topPkg":Ljava/lang/String;
} // :cond_11
/* iget-boolean v7, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_2
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "isResE, start not equal, pkg: "; // const-string v8, "isResE, start not equal, pkg: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ", taskInfo.baseActivity.getPackageName():"; // const-string v8, ", taskInfo.baseActivity.getPackageName():"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.baseActivity;
(( android.content.ComponentName ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ", continue"; // const-string v8, ", continue"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v7 );
/* goto/16 :goto_0 */
/* .line 328 */
} // :cond_12
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "isResE error, pkg:"; // const-string v8, "isResE error, pkg:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v7 );
/* .line 330 */
} // .end local v6 # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
/* goto/16 :goto_0 */
/* .line 331 */
} // .end local v4 # "tasksize":I
} // :cond_13
/* .line 332 */
} // :cond_14
/* iget-boolean v4, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_15
final String v4 = "isResE, tasks == null"; // const-string v4, "isResE, tasks == null"
android.util.Slog .v ( v3,v4 );
/* .line 334 */
} // :cond_15
} // :goto_4
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "isResE, End "; // const-string v5, "isResE, End "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", ret: false, setRemove: ture"; // const-string v4, ", ret: false, setRemove: ture"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v1 );
/* .line 335 */
/* .line 245 */
} // :cond_16
} // :goto_5
} // .end method
public Boolean isSaveTask ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 600 */
/* nop */
/* .line 601 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean keepTaskInRecent ( com.android.server.wm.Task p0 ) {
/* .locals 12 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 342 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_11
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_11
/* if-nez p1, :cond_0 */
/* goto/16 :goto_0 */
/* .line 346 */
} // :cond_0
(( com.android.server.wm.Task ) p1 ).getRootActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;
final String v2 = "AppResurrectionServiceImpl"; // const-string v2, "AppResurrectionServiceImpl"
/* if-nez v0, :cond_2 */
/* .line 347 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "keepTaskInRecent ret false, task.getRootActivity() == null task:"; // const-string v3, "keepTaskInRecent ret false, task.getRootActivity() == null task:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 348 */
} // :cond_1
/* .line 350 */
} // :cond_2
(( com.android.server.wm.Task ) p1 ).getRootActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;
v0 = this.packageName;
/* .line 352 */
/* .local v0, "rootActivityPKG":Ljava/lang/String; */
v3 = (( com.android.server.wm.AppResurrectionServiceImpl ) p0 ).isResurrectionEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isResurrectionEnable(Ljava/lang/String;)Z
/* if-nez v3, :cond_4 */
/* .line 353 */
/* iget-boolean v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "keepTaskInRecent ret false, !isResurrectionEnable rootActivityPKG:"; // const-string v4, "keepTaskInRecent ret false, !isResurrectionEnable rootActivityPKG:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 354 */
} // :cond_3
/* .line 358 */
} // :cond_4
v3 = (( com.android.server.wm.Task ) p1 ).getChildCount ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I
int v4 = 2; // const/4 v4, 0x2
final String v5 = ", "; // const-string v5, ", "
/* if-ge v3, v4, :cond_5 */
/* .line 359 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "keepTaskInRecent ret false, task.getChildCount() <2:"; // const-string v4, "keepTaskInRecent ret false, task.getChildCount() <2:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.wm.Task ) p1 ).getChildCount ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 360 */
/* .line 363 */
} // :cond_5
/* iget-boolean v3, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_6
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "keepTaskInRecent go on:"; // const-string v4, "keepTaskInRecent go on:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 365 */
} // :cond_6
v3 = java.util.concurrent.TimeUnit.HOURS;
/* iget v4, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionInactiveDurationHour:I */
/* int-to-long v6, v4 */
(( java.util.concurrent.TimeUnit ) v3 ).toMillis ( v6, v7 ); // invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
/* move-result-wide v3 */
/* .line 366 */
/* .local v3, "hoursInMs":J */
v6 = java.util.concurrent.TimeUnit.MILLISECONDS;
(( com.android.server.wm.Task ) p1 ).getInactiveDuration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getInactiveDuration()J
/* move-result-wide v7 */
(( java.util.concurrent.TimeUnit ) v6 ).toHours ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/util/concurrent/TimeUnit;->toHours(J)J
/* move-result-wide v6 */
/* .line 369 */
/* .local v6, "inactiveHours":J */
(( com.android.server.wm.Task ) p1 ).getInactiveDuration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getInactiveDuration()J
/* move-result-wide v8 */
/* cmp-long v8, v8, v3 */
/* if-lez v8, :cond_7 */
/* .line 370 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "keepTaskInRecent ret false, task.getInactiveDuration > hours :"; // const-string v9, "keepTaskInRecent ret false, task.getInactiveDuration > hours :"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.Task ) p1 ).getInactiveDuration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getInactiveDuration()J
/* move-result-wide v9 */
(( java.lang.StringBuilder ) v8 ).append ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = ", 36H:"; // const-string v9, ", 36H:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3, v4 ); // invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = ", inactiveHours="; // const-string v9, ", inactiveHours="
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6, v7 ); // invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* .line 371 */
/* .line 375 */
} // :cond_7
v8 = (( com.android.server.wm.AppResurrectionServiceImpl ) p0 ).getAllowChildCount ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->getAllowChildCount(Ljava/lang/String;)I
/* .line 376 */
/* .local v8, "allowChildCount":I */
v9 = (( com.android.server.wm.Task ) p1 ).getChildCount ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I
/* if-le v9, v8, :cond_8 */
/* .line 377 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "keepTaskInRecent ret false, task.getChildCount():"; // const-string v10, "keepTaskInRecent ret false, task.getChildCount():"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = (( com.android.server.wm.Task ) p1 ).getChildCount ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " > allow:"; // const-string v10, " > allow:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* .line 378 */
/* .line 382 */
} // :cond_8
v5 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isPKGInAppResurrectionActivityList(Ljava/lang/String;)Z */
if ( v5 != null) { // if-eqz v5, :cond_e
/* .line 383 */
/* iget-boolean v5, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_9
final String v5 = "keepTaskInRecent pkg In ActivityList"; // const-string v5, "keepTaskInRecent pkg In ActivityList"
android.util.Slog .d ( v2,v5 );
/* .line 384 */
} // :cond_9
final String v5 = ""; // const-string v5, ""
/* .line 385 */
/* .local v5, "topMostActivity":Ljava/lang/String; */
(( com.android.server.wm.Task ) p1 ).getTopMostActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopMostActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 386 */
/* .local v9, "arTopMost":Lcom/android/server/wm/ActivityRecord; */
if ( v9 != null) { // if-eqz v9, :cond_b
/* .line 387 */
/* iget-boolean v10, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v10 != null) { // if-eqz v10, :cond_a
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "keepTaskInRecent arTopMost="; // const-string v11, "keepTaskInRecent arTopMost="
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v11 = this.shortComponentName;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v10 );
/* .line 388 */
} // :cond_a
v5 = this.shortComponentName;
/* .line 392 */
} // :cond_b
v10 = android.text.TextUtils .isEmpty ( v5 );
if ( v10 != null) { // if-eqz v10, :cond_c
/* .line 393 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "keepTaskInRecent topMostActivity is empty, ret false, "; // const-string v11, "keepTaskInRecent topMostActivity is empty, ret false, "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v10 );
/* .line 394 */
/* .line 396 */
} // :cond_c
v10 = this.mCloudAppResurrectionActivityJArray;
if ( v10 != null) { // if-eqz v10, :cond_d
/* .line 397 */
v10 = /* invoke-direct {p0, v10, v5}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isKeyInJArray(Lorg/json/JSONArray;Ljava/lang/String;)Z */
/* if-nez v10, :cond_e */
/* .line 398 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "keepTaskInRecent topMostActivity is not in Clist, ret false, "; // const-string v11, "keepTaskInRecent topMostActivity is not in Clist, ret false, "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v10 );
/* .line 399 */
/* .line 402 */
} // :cond_d
v10 = com.android.server.wm.AppResurrectionServiceImpl.sDefaultAppResurrectionEnableActivityList;
v10 = (( java.util.ArrayList ) v10 ).contains ( v5 ); // invoke-virtual {v10, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v10, :cond_e */
/* .line 403 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "keepTaskInRecent topMostActivity is not in Dlist, ret false, "; // const-string v11, "keepTaskInRecent topMostActivity is not in Dlist, ret false, "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v10 );
/* .line 404 */
/* .line 411 */
} // .end local v5 # "topMostActivity":Ljava/lang/String;
} // .end local v9 # "arTopMost":Lcom/android/server/wm/ActivityRecord;
} // :cond_e
v5 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/AppResurrectionServiceImpl;->isChildrenActivityFromSamePkg(Lcom/android/server/wm/Task;)Z */
/* if-nez v5, :cond_f */
/* .line 412 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "keepTaskInRecent !isChildrenActivityFromSamePkg, ret false, "; // const-string v9, "keepTaskInRecent !isChildrenActivityFromSamePkg, ret false, "
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* .line 413 */
/* .line 415 */
} // :cond_f
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_10
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "keepTaskInRecent task:"; // const-string v5, "keepTaskInRecent task:"
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v1 );
/* .line 416 */
} // :cond_10
int v1 = 1; // const/4 v1, 0x1
/* .line 343 */
} // .end local v0 # "rootActivityPKG":Ljava/lang/String;
} // .end local v3 # "hoursInMs":J
} // .end local v6 # "inactiveHours":J
} // .end local v8 # "allowChildCount":I
} // :cond_11
} // :goto_0
} // .end method
public void notifyWindowsDrawn ( Long p0, com.android.server.wm.ActivityRecord p1, com.android.server.wm.ActivityRecord p2, android.content.ComponentName p3 ) {
/* .locals 17 */
/* .param p1, "windowsDrawnDelay" # J */
/* .param p3, "lastLaunchedActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "curTransitionActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p5, "curComponent" # Landroid/content/ComponentName; */
/* .line 547 */
/* move-object/from16 v12, p0 */
/* move-wide/from16 v13, p1 */
/* move-object/from16 v15, p4 */
/* iget-boolean v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* iget-boolean v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_4 */
/* .line 551 */
} // :cond_0
/* if-nez v15, :cond_1 */
/* .line 552 */
return;
/* .line 555 */
} // :cond_1
/* iget-boolean v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v1 = "AppResurrectionServiceImpl"; // const-string v1, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyWindowsDrawn, windowsDrawnDelay = "; // const-string v2, "notifyWindowsDrawn, windowsDrawnDelay = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13, v14 ); // invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", lastLaunchedActivity="; // const-string v2, ", lastLaunchedActivity="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v11, p3 */
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ", curTransitionActivity="; // const-string v2, ", curTransitionActivity="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ", mRebornPkg="; // const-string v2, ", mRebornPkg="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mRebornPkg;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", curComponent:"; // const-string v2, ", curComponent:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-virtual/range {p5 ..p5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
} // :cond_2
/* move-object/from16 v11, p3 */
/* .line 556 */
} // :goto_0
/* iget-boolean v0, v12, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyWindowsDrawn, caller = "; // const-string v2, "notifyWindowsDrawn, caller = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x23 */
android.os.Debug .getCallers ( v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 557 */
} // :cond_3
final String v0 = "AppResurrection_notifyWindowsDrawn"; // const-string v0, "AppResurrection_notifyWindowsDrawn"
/* const-wide/16 v9, 0x8 */
android.os.Trace .traceBegin ( v9,v10,v0 );
/* .line 558 */
v0 = this.mRebornPkg;
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 559 */
final String v0 = "Unknown"; // const-string v0, "Unknown"
/* .line 560 */
/* .local v0, "killReason":Ljava/lang/String; */
v1 = this.mPkg2KillReasonMap;
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 561 */
v2 = this.mRebornPkg;
v1 = (( java.util.HashMap ) v1 ).containsKey ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 562 */
v1 = this.mPkg2KillReasonMap;
v2 = this.mRebornPkg;
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object v0, v1 */
/* check-cast v0, Ljava/lang/String; */
/* move-object/from16 v16, v0 */
/* .line 566 */
} // :cond_4
/* move-object/from16 v16, v0 */
} // .end local v0 # "killReason":Ljava/lang/String;
/* .local v16, "killReason":Ljava/lang/String; */
} // :goto_1
/* move-object/from16 v5, v16 */
/* .line 567 */
/* .local v5, "fKillReason":Ljava/lang/String; */
if ( p5 != null) { // if-eqz p5, :cond_5
/* .line 568 */
v6 = this.mLoggerHandler;
/* new-instance v7, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda3; */
/* move-object v0, v7 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p5 */
/* move-wide/from16 v3, p1 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Landroid/content/ComponentName;JLjava/lang/String;)V */
(( android.os.Handler ) v6 ).post ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 569 */
v0 = this.mMIUIBgHandler;
/* new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda4; */
/* move-object v6, v1 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p5 */
/* move-wide v2, v9 */
/* move-wide/from16 v9, p1 */
/* move-object v11, v5 */
/* invoke-direct/range {v6 ..v11}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Landroid/content/ComponentName;JLjava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 571 */
} // :cond_5
/* move-wide v2, v9 */
v0 = this.mLoggerHandler;
/* new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda5; */
/* move-object v6, v1 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p4 */
/* move-wide/from16 v9, p1 */
/* move-object v11, v5 */
/* invoke-direct/range {v6 ..v11}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 572 */
v0 = this.mMIUIBgHandler;
/* new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda6; */
/* move-object v6, v1 */
/* invoke-direct/range {v6 ..v11}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Lcom/android/server/wm/ActivityRecord;JLjava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 575 */
} // :goto_2
v0 = this.mRebornPkg;
/* invoke-direct {v12, v0, v13, v14}, Lcom/android/server/wm/AppResurrectionServiceImpl;->checkLaunchTimeOverThreshold(Ljava/lang/String;J)V */
/* .line 576 */
final String v0 = ""; // const-string v0, ""
this.mRebornPkg = v0;
/* .line 558 */
} // .end local v5 # "fKillReason":Ljava/lang/String;
} // .end local v16 # "killReason":Ljava/lang/String;
} // :cond_6
/* move-wide v2, v9 */
/* .line 578 */
} // :goto_3
android.os.Trace .traceEnd ( v2,v3 );
/* .line 579 */
return;
/* .line 548 */
} // :cond_7
} // :goto_4
return;
} // .end method
public void saveActivityToXml ( com.android.server.wm.ActivityRecord p0, com.android.modules.utils.TypedXmlSerializer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "out" # Lcom/android/modules/utils/TypedXmlSerializer; */
/* .param p3, "index" # I */
/* .line 919 */
final String v0 = "reborn_activity"; // const-string v0, "reborn_activity"
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).isPersistable ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isPersistable()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 922 */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
/* .line 923 */
(( com.android.server.wm.ActivityRecord ) p1 ).saveToXml ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/wm/ActivityRecord;->saveToXml(Lcom/android/modules/utils/TypedXmlSerializer;)V
/* .line 924 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 928 */
/* .line 925 */
/* :catch_0 */
/* move-exception v0 */
/* .line 926 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 927 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "savetoxml failed at pkg:"; // const-string v2, "savetoxml failed at pkg:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " activity:"; // const-string v2, " activity:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.ActivityRecord ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "TaskPersister"; // const-string v2, "TaskPersister"
android.util.Slog .e ( v2,v1 );
/* .line 930 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
public Boolean updateStartingWindowResolvedTheme ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3, Boolean p4, Boolean p5 ) {
/* .locals 9 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "resolvedTheme" # I */
/* .param p3, "newTask" # Z */
/* .param p4, "taskSwitch" # Z */
/* .param p5, "processRunning" # Z */
/* .param p6, "startActivity" # Z */
/* .line 473 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mIsDeviceSupport:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->mCloudAppResurrectionEnable:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_0 */
/* .line 476 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
final String v2 = ", startActivity:"; // const-string v2, ", startActivity:"
final String v3 = ",processRunning:"; // const-string v3, ",processRunning:"
final String v4 = ", taskSwitch:"; // const-string v4, ", taskSwitch:"
final String v5 = ", newTask:"; // const-string v5, ", newTask:"
final String v6 = ", resolvedTheme:"; // const-string v6, ", resolvedTheme:"
final String v7 = "AppResurrectionServiceImpl"; // const-string v7, "AppResurrectionServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 477 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "updateSplashScreenResolvedTheme, start pkg:" */
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p6 ); // invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v0 );
/* .line 490 */
} // :cond_1
if ( p3 != null) { // if-eqz p3, :cond_4
if ( p4 != null) { // if-eqz p4, :cond_4
/* if-nez p5, :cond_4 */
if ( p6 != null) { // if-eqz p6, :cond_4
/* .line 492 */
v0 = this.mPkg2StartThemeMap;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 493 */
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* const/16 v1, 0x32 */
/* if-le v0, v1, :cond_2 */
/* .line 494 */
v0 = this.mPkg2StartThemeMap;
(( java.util.HashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
/* .line 496 */
} // :cond_2
v0 = this.mPkg2StartThemeMap;
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 499 */
v0 = this.mMIUIBgHandler;
/* new-instance v1, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda7; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/AppResurrectionServiceImpl$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/wm/AppResurrectionServiceImpl;Ljava/lang/String;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 500 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionServiceImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 501 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateSplashScreenResolvedTheme, final pkg:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p6 ); // invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v0 );
/* .line 504 */
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
/* .line 507 */
} // :cond_4
/* .line 474 */
} // :cond_5
} // :goto_0
} // .end method
