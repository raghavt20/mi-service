.class Lcom/android/server/wm/MiuiFreezeImpl$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiFreezeImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiFreezeImpl;->registerReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiFreezeImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiFreezeImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiFreezeImpl;

    .line 397
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreezeImpl$1;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 400
    const-string v0, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    const-string v0, "MiuiFreezeImpl"

    const-string v1, "receive broadcast,try remove"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$1;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-static {v0}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fgetmPackageName(Lcom/android/server/wm/MiuiFreezeImpl;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreezeImpl$1;->this$0:Lcom/android/server/wm/MiuiFreezeImpl;

    invoke-static {v2}, Lcom/android/server/wm/MiuiFreezeImpl;->-$$Nest$fgetmCurUid(Lcom/android/server/wm/MiuiFreezeImpl;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/MiuiFreezeImpl;->finishAndRemoveSplashScreen(Ljava/lang/String;I)V

    .line 404
    :cond_0
    return-void
.end method
