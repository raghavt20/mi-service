.class final Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;
.super Ljava/lang/Object;
.source "WindowManagerServiceImpl.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WallpaperDeathMonitor"
.end annotation


# instance fields
.field final mDisplayId:I

.field final mIBinder:Landroid/os/IBinder;

.field final synthetic this$0:Lcom/android/server/wm/WindowManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerServiceImpl;Landroid/os/IBinder;I)V
    .locals 0
    .param p2, "binder"    # Landroid/os/IBinder;
    .param p3, "displayId"    # I

    .line 337
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338
    iput p3, p0, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;->mDisplayId:I

    .line 339
    iput-object p2, p0, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;->mIBinder:Landroid/os/IBinder;

    .line 340
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .line 343
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;->mIBinder:Landroid/os/IBinder;

    iget v2, p0, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;->mDisplayId:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/WindowManagerService;->removeWindowToken(Landroid/os/IBinder;I)V

    .line 344
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl$WallpaperDeathMonitor;->mIBinder:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 345
    return-void
.end method
