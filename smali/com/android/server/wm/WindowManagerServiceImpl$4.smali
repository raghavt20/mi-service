.class Lcom/android/server/wm/WindowManagerServiceImpl$4;
.super Landroid/hardware/camera2/CameraManager$AvailabilityCallback;
.source "WindowManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/WindowManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/WindowManagerServiceImpl;

    .line 484
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$4;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraAvailable(Ljava/lang/String;)V
    .locals 3
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 487
    invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraAvailable(Ljava/lang/String;)V

    .line 488
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 489
    .local v0, "id":I
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 490
    return-void

    .line 492
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$4;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->-$$Nest$fgetmOpeningCameraID(Lcom/android/server/wm/WindowManagerServiceImpl;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 493
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$4;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->-$$Nest$fgetmOpeningCameraID(Lcom/android/server/wm/WindowManagerServiceImpl;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$4;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerServiceImpl;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 499
    :cond_1
    return-void
.end method

.method public onCameraUnavailable(Ljava/lang/String;)V
    .locals 3
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 503
    invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraUnavailable(Ljava/lang/String;)V

    .line 504
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 505
    .local v0, "id":I
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 506
    return-void

    .line 508
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$4;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/wm/WindowManagerServiceImpl;->-$$Nest$fgetmOpeningCameraID(Lcom/android/server/wm/WindowManagerServiceImpl;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 509
    return-void
.end method
