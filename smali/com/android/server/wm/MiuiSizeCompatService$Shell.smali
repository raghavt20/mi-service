.class final Lcom/android/server/wm/MiuiSizeCompatService$Shell;
.super Landroid/os/ShellCommand;
.source "MiuiSizeCompatService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiSizeCompatService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Shell"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatService;


# direct methods
.method private constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;)V
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/MiuiSizeCompatService$Shell-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;)V

    return-void
.end method

.method private listPackages(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 10
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "pkg"    # Ljava/lang/String;

    .line 331
    const-string v0, "  "

    .line 332
    .local v0, "innerPrefix":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "GameAspectRatioPackages"

    const-string v3, "FixedAspectRatioPackages(System)"

    const-string v4, "FixedAspectRatioPackages(UserSetting)"

    const-string v5, "] "

    const-string v6, " "

    const-string v7, "["

    if-eqz v1, :cond_3

    .line 333
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmStaticSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 334
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 335
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmStaticSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 336
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/sizecompat/AspectRatioInfo;

    iget v9, v9, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 337
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/sizecompat/AspectRatioInfo;

    invoke-virtual {v9}, Landroid/sizecompat/AspectRatioInfo;->getScaleMode()I

    move-result v9

    invoke-static {v9}, Landroid/sizecompat/AspectRatioInfo;->parseScaleModeStr(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 336
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 338
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    goto :goto_0

    .line 340
    :cond_0
    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 341
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 342
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 343
    .restart local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/sizecompat/AspectRatioInfo;

    iget v8, v8, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 344
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/sizecompat/AspectRatioInfo;

    invoke-virtual {v8}, Landroid/sizecompat/AspectRatioInfo;->getScaleMode()I

    move-result v8

    invoke-static {v8}, Landroid/sizecompat/AspectRatioInfo;->parseScaleModeStr(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 343
    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 345
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    goto :goto_1

    .line 347
    :cond_1
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 348
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 349
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 350
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 351
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/sizecompat/AspectRatioInfo;

    iget v4, v4, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/sizecompat/AspectRatioInfo;

    iget v4, v4, Landroid/sizecompat/AspectRatioInfo;->mGravity:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 352
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/sizecompat/AspectRatioInfo;

    invoke-virtual {v4}, Landroid/sizecompat/AspectRatioInfo;->getScaleMode()I

    move-result v4

    invoke-static {v4}, Landroid/sizecompat/AspectRatioInfo;->parseScaleModeStr(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 350
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 353
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
    goto :goto_2

    :cond_2
    goto/16 :goto_3

    .line 356
    :cond_3
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmStaticSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmStaticSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 357
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 358
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmStaticSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/sizecompat/AspectRatioInfo;

    iget v3, v3, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmStaticSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v3

    .line 360
    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/sizecompat/AspectRatioInfo;

    invoke-virtual {v3}, Landroid/sizecompat/AspectRatioInfo;->getScaleMode()I

    move-result v3

    .line 359
    invoke-static {v3}, Landroid/sizecompat/AspectRatioInfo;->parseScaleModeStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 364
    :cond_4
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 365
    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 366
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/sizecompat/AspectRatioInfo;

    iget v3, v3, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v3

    .line 368
    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/sizecompat/AspectRatioInfo;

    invoke-virtual {v3}, Landroid/sizecompat/AspectRatioInfo;->getScaleMode()I

    move-result v3

    .line 367
    invoke-static {v3}, Landroid/sizecompat/AspectRatioInfo;->parseScaleModeStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 366
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 371
    :cond_5
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 372
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 373
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v2}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/sizecompat/AspectRatioInfo;

    iget v2, v2, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v2}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v2

    .line 375
    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/sizecompat/AspectRatioInfo;

    invoke-virtual {v2}, Landroid/sizecompat/AspectRatioInfo;->getScaleMode()I

    move-result v2

    .line 374
    invoke-static {v2}, Landroid/sizecompat/AspectRatioInfo;->parseScaleModeStr(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 373
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 378
    :cond_6
    :goto_3
    return-void
.end method

.method private modeFlipActivityFullScreen(Ljava/lang/String;ZLjava/io/PrintWriter;)V
    .locals 5
    .param p1, "activityName"    # Ljava/lang/String;
    .param p2, "isAdd"    # Z
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 401
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getErrPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "Error: need ${packageName} or list"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 403
    return-void

    .line 405
    :cond_0
    const-string v0, "list"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 406
    if-eqz p2, :cond_1

    .line 407
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->addFlipActivityFullScreen(Ljava/lang/String;)V

    goto :goto_0

    .line 409
    :cond_1
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->removeFlipActivityFullScreen(Ljava/lang/String;)V

    .line 412
    :cond_2
    :goto_0
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getAllFlipActivityFullScreen()Landroid/util/ArraySet;

    move-result-object v0

    .line 413
    .local v0, "allFlipActivityFullScreen":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    invoke-virtual {v0}, Landroid/util/ArraySet;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 414
    const-string v1, "FlipActivityFullScreen null"

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 416
    :cond_3
    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 417
    .local v2, "acName":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current FlipActivityFullScreen  : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 418
    .end local v2    # "acName":Ljava/lang/String;
    goto :goto_1

    .line 420
    :cond_4
    :goto_2
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 13
    .param p1, "cmd"    # Ljava/lang/String;

    .line 254
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 255
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string/jumbo v1, "setFlipScale"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getNextArg()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    sput v1, Lcom/android/server/wm/MiuiSizeCompatService;->FLIP_DEFAULT_SCALE:F

    goto/16 :goto_7

    .line 257
    :cond_0
    const-string v1, "addFlipActivityFullScreen"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    .line 258
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getNextArg()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v3, v0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->modeFlipActivityFullScreen(Ljava/lang/String;ZLjava/io/PrintWriter;)V

    goto/16 :goto_7

    .line 259
    :cond_1
    const-string v1, "removeFlipActivityFullScreen"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 260
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getNextArg()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v2, v0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->modeFlipActivityFullScreen(Ljava/lang/String;ZLjava/io/PrintWriter;)V

    goto/16 :goto_7

    .line 261
    :cond_2
    const-string/jumbo v1, "setFixedAspectRatio"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string/jumbo v4, "set "

    const/4 v5, -0x1

    if-eqz v1, :cond_9

    .line 262
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getNextArg()Ljava/lang/String;

    move-result-object v1

    .line 263
    .local v1, "pkgs":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getNextArg()Ljava/lang/String;

    move-result-object v6

    .line 264
    .local v6, "param":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 265
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getErrPrintWriter()Ljava/io/PrintWriter;

    move-result-object v2

    const-string v3, "Error: need ${packageName}"

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 266
    return v5

    .line 269
    :cond_3
    const-string v5, "--remove"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    goto :goto_0

    :cond_4
    move v5, v2

    goto :goto_1

    :cond_5
    :goto_0
    move v5, v3

    .line 272
    .local v5, "remove":Z
    :goto_1
    const-string v7, ":"

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 273
    .local v7, "pkgArray":[Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    array-length v9, v7

    if-ge v8, v9, :cond_8

    .line 274
    array-length v9, v7

    sub-int/2addr v9, v3

    if-ne v8, v9, :cond_6

    move v9, v3

    goto :goto_3

    :cond_6
    move v9, v2

    .line 275
    .local v9, "write":Z
    :goto_3
    if-eqz v5, :cond_7

    .line 276
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "remove "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    aget-object v11, v7, v8

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 277
    iget-object v10, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    aget-object v11, v7, v8

    const/high16 v12, -0x40800000    # -1.0f

    invoke-virtual {v10, v11, v12, v9, v3}, Lcom/android/server/wm/MiuiSizeCompatService;->setMiuiSizeCompatRatio(Ljava/lang/String;FZZ)Z

    goto :goto_4

    .line 279
    :cond_7
    iget-object v10, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-virtual {v10, v6}, Lcom/android/server/wm/MiuiSizeCompatService;->parseRatioFromStr(Ljava/lang/String;)F

    move-result v10

    .line 280
    .local v10, "ratio":F
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v7, v8

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ratio="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 281
    iget-object v11, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    aget-object v12, v7, v8

    invoke-virtual {v11, v12, v10, v9, v3}, Lcom/android/server/wm/MiuiSizeCompatService;->setMiuiSizeCompatRatio(Ljava/lang/String;FZZ)Z

    .line 273
    .end local v9    # "write":Z
    .end local v10    # "ratio":F
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 284
    .end local v1    # "pkgs":Ljava/lang/String;
    .end local v5    # "remove":Z
    .end local v6    # "param":Ljava/lang/String;
    .end local v7    # "pkgArray":[Ljava/lang/String;
    .end local v8    # "i":I
    :cond_8
    goto/16 :goto_7

    :cond_9
    const-string/jumbo v1, "setSizeCompatDebug"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 285
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getNextArg()Ljava/lang/String;

    move-result-object v1

    .line 286
    .local v1, "debug":Ljava/lang/String;
    const-string/jumbo v3, "true"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 287
    const-string v3, "Set size compat debug mode enable."

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_5

    .line 288
    :cond_a
    const-string v3, "false"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 289
    const-string v3, "Set size compat debug mode disable."

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_5

    .line 291
    :cond_b
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->onHelp()V

    .line 293
    .end local v1    # "debug":Ljava/lang/String;
    :goto_5
    goto/16 :goto_7

    :cond_c
    const-string v1, "list"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 294
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getNextArg()Ljava/lang/String;

    move-result-object v1

    .line 295
    .local v1, "pkg":Ljava/lang/String;
    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->listPackages(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 296
    .end local v1    # "pkg":Ljava/lang/String;
    goto/16 :goto_7

    :cond_d
    const-string/jumbo v1, "setScale"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 297
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getNextArg()Ljava/lang/String;

    move-result-object v1

    .line 298
    .restart local v1    # "pkg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getNextArg()Ljava/lang/String;

    move-result-object v6

    .line 299
    .local v6, "scaleModeStr":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_10

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_e

    goto :goto_6

    .line 309
    :cond_e
    iget-object v7, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v7, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$mgetAspectRatioByPackage(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/lang/String;)F

    move-result v7

    const/4 v8, 0x0

    cmpg-float v7, v7, v8

    if-gtz v7, :cond_f

    .line 310
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getErrPrintWriter()Ljava/io/PrintWriter;

    move-result-object v2

    const-string v3, "Error: need setFixedAspectRatio first"

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 311
    return v5

    .line 315
    :cond_f
    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 317
    .local v7, "scaleMode":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " scaleMode: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v7}, Landroid/sizecompat/AspectRatioInfo;->parseScaleModeStr(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 318
    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v4, v1, v7, v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$msetMiuiSizeCompatScaleMode(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/lang/String;IZ)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    nop

    .line 324
    .end local v1    # "pkg":Ljava/lang/String;
    .end local v6    # "scaleModeStr":Ljava/lang/String;
    .end local v7    # "scaleMode":I
    goto :goto_7

    .line 320
    .restart local v1    # "pkg":Ljava/lang/String;
    .restart local v6    # "scaleModeStr":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 321
    .local v2, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getErrPrintWriter()Ljava/io/PrintWriter;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error: bad number "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 322
    return v5

    .line 300
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_10
    :goto_6
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getErrPrintWriter()Ljava/io/PrintWriter;

    move-result-object v2

    const-string v3, "Error: use setScale ${packageName} 0|1|2|3"

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 301
    const-string v2, "      Set package scale mode."

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 302
    const-string v2, "          0:SCALE_MODE_UNDEFINED"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 303
    const-string v2, "          1:SCALE_MODE_LANDSCAPE"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 304
    const-string v2, "          2:SCALE_MODE_PORTRAIT"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 305
    const-string v2, "          3:SCALE_MODE_DISABLE"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 306
    return v5

    .line 325
    .end local v1    # "pkg":Ljava/lang/String;
    .end local v6    # "scaleModeStr":Ljava/lang/String;
    :cond_11
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->handleDefaultCommands(Ljava/lang/String;)I

    .line 327
    :goto_7
    return v2
.end method

.method public onHelp()V
    .locals 2

    .line 382
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatService$Shell;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 383
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "MiuiSizeCompat commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 384
    const-string v1, "  help"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 385
    const-string v1, "      Print this help text."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 386
    const-string v1, "  setFixedAspectRatio ${packageName} [longSize:shortSize]|[--remove]"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 387
    const-string v1, "      Set PACKAGE default ratio."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 388
    const-string v1, "  list [${packageName}]"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 389
    const-string v1, "      List state of PACKAGE, if null, list all."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 390
    const-string v1, "  setSizeCompatDebug true|false"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 391
    const-string v1, "      Enable or disable MiuiSizeCompat debug."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 392
    const-string v1, "  setScale ${packageName} 0|1|2|3"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 393
    const-string v1, "      Set package scale mode."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 394
    const-string v1, "          0: SCALE_MODE_UNDEFINED"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 395
    const-string v1, "          1: SCALE_MODE_LANDSCAPE"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 396
    const-string v1, "          2: SCALE_MODE_PORTRAIT"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 397
    const-string v1, "          3: SCALE_MODE_DISABLE"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 398
    return-void
.end method
