class com.android.server.wm.MiuiSizeCompatService$3 extends android.view.IDisplayFoldListener$Stub {
	 /* .source "MiuiSizeCompatService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiSizeCompatService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiSizeCompatService this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiSizeCompatService$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiSizeCompatService; */
/* .line 535 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/view/IDisplayFoldListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onDisplayFoldChanged ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .param p2, "folded" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 539 */
v0 = this.this$0;
com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmCurFullAct ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.this$0;
com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmCurFullAct ( v0 );
v0 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 v0 = 	 com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmNotificationShowing ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 if ( p2 != null) { // if-eqz p2, :cond_2
			 /* .line 541 */
		 } // :cond_0
		 /* sget-boolean v0, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 542 */
			 /* new-instance v0, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
			 v1 = this.this$0;
			 com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmCurFullAct ( v1 );
			 v1 = this.shortComponentName;
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 final String v1 = " ,mNotificationShowing= "; // const-string v1, " ,mNotificationShowing= "
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v1 = this.this$0;
			 v1 = 			 com.android.server.wm.MiuiSizeCompatService .-$$Nest$fgetmNotificationShowing ( v1 );
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
			 final String v1 = " ,folded= "; // const-string v1, " ,folded= "
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 final String v1 = "MiuiSizeCompatService"; // const-string v1, "MiuiSizeCompatService"
			 android.util.Slog .d ( v1,v0 );
			 /* .line 545 */
		 } // :cond_1
		 v0 = this.this$0;
		 final String v1 = "device fold"; // const-string v1, "device fold"
		 (( com.android.server.wm.MiuiSizeCompatService ) v0 ).cancelSizeCompatNotification ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService;->cancelSizeCompatNotification(Ljava/lang/String;)V
		 /* .line 547 */
	 } // :cond_2
	 return;
} // .end method
