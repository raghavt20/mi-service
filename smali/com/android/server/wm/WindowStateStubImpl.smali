.class public Lcom/android/server/wm/WindowStateStubImpl;
.super Lcom/android/server/wm/WindowStateStub;
.source "WindowStateStubImpl.java"


# static fields
.field private static final DEFAULT_COMPAT_SCALE_VALUE:F = 1.0f

.field private static FLIP_COMPAT_SCALE_SCALE_VALUE:F

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mIsWallpaperOffsetUpdateCompleted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 47
    const-class v0, Lcom/android/server/wm/WindowStateStubImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/WindowStateStubImpl;->TAG:Ljava/lang/String;

    .line 54
    const v0, 0x3f4ccccd    # 0.8f

    sput v0, Lcom/android/server/wm/WindowStateStubImpl;->FLIP_COMPAT_SCALE_SCALE_VALUE:F

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Lcom/android/server/wm/WindowStateStub;-><init>()V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z

    return-void
.end method

.method public static adjuestFrameForChild(Lcom/android/server/wm/WindowState;)V
    .locals 12
    .param p0, "win"    # Lcom/android/server/wm/WindowState;

    .line 98
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->isChildWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    return-void

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 103
    .local v0, "task":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_4

    .line 105
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 106
    .local v1, "rect":Landroid/graphics/Rect;
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v3, v3, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    if-ge v2, v3, :cond_2

    .line 107
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v2, v2, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    .line 108
    .local v2, "relativeLeft":I
    int-to-float v3, v2

    iget v6, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    mul-float/2addr v3, v6

    float-to-int v3, v3

    sub-int v3, v2, v3

    .line 110
    .local v3, "weightOffset":I
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->inFreeformWindowingMode()Z

    move-result v6

    if-eqz v6, :cond_1

    iget v6, p0, Lcom/android/server/wm/WindowState;->mDownscaleScale:F

    cmpl-float v6, v6, v5

    if-lez v6, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;

    move-result-object v6

    .line 112
    .local v6, "windowConfiguration":Landroid/app/WindowConfiguration;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 113
    invoke-virtual {v6}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    .line 115
    .local v7, "bounds":Landroid/graphics/Rect;
    iget v8, v7, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    .line 116
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    iget v10, p0, Lcom/android/server/wm/WindowState;->mRequestedWidth:I

    int-to-float v10, v10

    iget v11, p0, Lcom/android/server/wm/WindowState;->mDownscaleScale:F

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    iget v10, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    mul-float/2addr v9, v10

    div-float/2addr v9, v4

    add-float/2addr v8, v9

    float-to-int v8, v8

    .line 117
    .local v8, "targetLeft":I
    iget-object v9, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v9, v9, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sub-int v3, v9, v8

    .line 121
    .end local v6    # "windowConfiguration":Landroid/app/WindowConfiguration;
    .end local v7    # "bounds":Landroid/graphics/Rect;
    .end local v8    # "targetLeft":I
    :cond_1
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v6, v6, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v7, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v3

    iput v7, v6, Landroid/graphics/Rect;->left:I

    .line 122
    iget-object v6, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v6, v6, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v7, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v7, v3

    iput v7, v6, Landroid/graphics/Rect;->right:I

    .line 125
    .end local v2    # "relativeLeft":I
    .end local v3    # "weightOffset":I
    :cond_2
    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v3, v3, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    if-ge v2, v3, :cond_4

    .line 126
    iget-object v2, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v2, v2, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    .line 127
    .local v2, "relativeTop":I
    int-to-float v3, v2

    iget v6, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    mul-float/2addr v3, v6

    float-to-int v3, v3

    sub-int v3, v2, v3

    .line 129
    .local v3, "heightOffset":I
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->inFreeformWindowingMode()Z

    move-result v6

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/android/server/wm/WindowState;->mDownscaleScale:F

    cmpl-float v5, v6, v5

    if-lez v5, :cond_3

    .line 130
    invoke-virtual {p0}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;

    move-result-object v5

    .line 131
    .local v5, "windowConfiguration":Landroid/app/WindowConfiguration;
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 132
    invoke-virtual {v5}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 134
    .local v6, "bounds":Landroid/graphics/Rect;
    iget v7, v6, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    .line 135
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    iget v9, p0, Lcom/android/server/wm/WindowState;->mRequestedHeight:I

    int-to-float v9, v9

    iget v10, p0, Lcom/android/server/wm/WindowState;->mDownscaleScale:F

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    mul-float/2addr v8, v9

    div-float/2addr v8, v4

    add-float/2addr v7, v8

    float-to-int v4, v7

    .line 136
    .local v4, "targetTop":I
    iget-object v7, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v7, v7, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    sub-int v3, v7, v4

    .line 140
    .end local v4    # "targetTop":I
    .end local v5    # "windowConfiguration":Landroid/app/WindowConfiguration;
    .end local v6    # "bounds":Landroid/graphics/Rect;
    :cond_3
    iget-object v4, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v4, v4, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v3

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 141
    iget-object v4, p0, Lcom/android/server/wm/WindowState;->mWindowFrames:Lcom/android/server/wm/WindowFrames;

    iget-object v4, v4, Lcom/android/server/wm/WindowFrames;->mFrame:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v5, v3

    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    .line 144
    .end local v1    # "rect":Landroid/graphics/Rect;
    .end local v2    # "relativeTop":I
    .end local v3    # "heightOffset":I
    :cond_4
    return-void
.end method

.method public static adjuestFreeFormTouchRegion(Lcom/android/server/wm/WindowState;Landroid/graphics/Region;)V
    .locals 0
    .param p0, "win"    # Lcom/android/server/wm/WindowState;
    .param p1, "outRegion"    # Landroid/graphics/Region;

    .line 147
    return-void
.end method

.method public static adjuestScaleAndFrame(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/Task;)V
    .locals 0
    .param p0, "win"    # Lcom/android/server/wm/WindowState;
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 74
    return-void
.end method

.method private getNavBarHeight(Lcom/android/server/wm/WindowState;)I
    .locals 4
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 206
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getInsetsState()Landroid/view/InsetsState;

    move-result-object v0

    .line 207
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getInsetsState()Landroid/view/InsetsState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/InsetsState;->getDisplayFrame()Landroid/graphics/Rect;

    move-result-object v1

    .line 208
    invoke-static {}, Landroid/view/WindowInsets$Type;->navigationBars()I

    move-result v2

    .line 207
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/InsetsState;->calculateInsets(Landroid/graphics/Rect;IZ)Landroid/graphics/Insets;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Insets;->bottom:I

    .line 206
    return v0
.end method

.method public static getWinStateFromInputWinMap(Lcom/android/server/wm/WindowManagerService;Landroid/os/IBinder;)Lcom/android/server/wm/WindowState;
    .locals 2
    .param p0, "windowManagerService"    # Lcom/android/server/wm/WindowManagerService;
    .param p1, "token"    # Landroid/os/IBinder;

    .line 154
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 155
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/WindowManagerService;->mInputToWindowMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/WindowState;

    monitor-exit v0

    return-object v1

    .line 156
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static getmGravity(Landroid/app/WindowConfiguration;I)I
    .locals 2
    .param p0, "windowconfiguration"    # Landroid/app/WindowConfiguration;
    .param p1, "mGravity"    # I

    .line 368
    invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I

    move-result v0

    if-nez v0, :cond_0

    .line 369
    const/4 p1, 0x5

    goto :goto_0

    .line 370
    :cond_0
    invoke-virtual {p0}, Landroid/app/WindowConfiguration;->getDisplayRotation()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 371
    const/4 p1, 0x3

    .line 373
    :cond_1
    :goto_0
    return p1
.end method

.method private isFixedOrizationScaleEnable(Lcom/android/server/wm/WindowState;)Z
    .locals 1
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 232
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getIgnoreOrientationRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 232
    :goto_0
    return v0
.end method

.method public static isFlipScale(Lcom/android/server/wm/WindowState;)Z
    .locals 2
    .param p0, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 395
    iget v0, p0, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    sget v1, Lcom/android/server/wm/WindowStateStubImpl;->FLIP_COMPAT_SCALE_SCALE_VALUE:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isImageWallpaper(Lcom/android/server/wm/WindowState;)Z
    .locals 2
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 307
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageWallpaper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 308
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isinMiuiSizeCompatModeEnable(Lcom/android/server/wm/WindowState;)Z
    .locals 1
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 238
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 239
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 238
    :goto_0
    return v0
.end method

.method private needNavBarOffset(I)Z
    .locals 2
    .param p1, "gravity"    # I

    .line 253
    and-int/lit8 v0, p1, 0x70

    const/16 v1, 0x50

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private needUpdateWindowEnable(Lcom/android/server/wm/WindowState;)Z
    .locals 3
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 214
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget v0, p1, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    goto :goto_0

    .line 219
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/wm/WindowStateStubImpl;->switchWindowType(Landroid/view/WindowManager$LayoutParams;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 220
    return v1

    .line 223
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->isinMiuiSizeCompatModeEnable(Lcom/android/server/wm/WindowState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 224
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->isFixedOrizationScaleEnable(Lcom/android/server/wm/WindowState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 225
    return v1

    .line 228
    :cond_2
    const/4 v0, 0x1

    return v0

    .line 216
    :cond_3
    :goto_0
    return v1
.end method

.method private needUpdateWindowEnableForFlip(Lcom/android/server/wm/WindowState;)Z
    .locals 3
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 379
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 380
    return v1

    .line 382
    :cond_0
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, v2, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    .line 383
    invoke-virtual {v0, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->shouldNotApplyAspectRatio(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    return v1

    .line 386
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 391
    return v1

    .line 389
    :sswitch_0
    const/4 v0, 0x1

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7d5 -> :sswitch_0
        0x7ef -> :sswitch_0
    .end sparse-switch
.end method

.method private switchWindowType(Landroid/view/WindowManager$LayoutParams;)Z
    .locals 1
    .param p1, "Attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 243
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    sparse-switch v0, :sswitch_data_0

    .line 248
    const/4 v0, 0x0

    return v0

    .line 246
    :sswitch_0
    const/4 v0, 0x1

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3e8 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public adjustFrameForDownscale(Lcom/android/server/wm/WindowState;Landroid/graphics/Rect;Landroid/app/WindowConfiguration;)V
    .locals 6
    .param p1, "ws"    # Lcom/android/server/wm/WindowState;
    .param p2, "frame"    # Landroid/graphics/Rect;
    .param p3, "config"    # Landroid/app/WindowConfiguration;

    .line 258
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->inFreeformWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/server/wm/WindowState;->mDownscaleScale:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 259
    invoke-virtual {p3}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 260
    .local v0, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget v3, p1, Lcom/android/server/wm/WindowState;->mDownscaleScale:F

    div-float v3, v1, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 261
    .local v2, "height":I
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    iget v5, p1, Lcom/android/server/wm/WindowState;->mDownscaleScale:F

    div-float/2addr v1, v5

    mul-float/2addr v4, v1

    add-float/2addr v4, v3

    float-to-int v1, v4

    .line 262
    .local v1, "width":I
    iget v3, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iput v3, p2, Landroid/graphics/Rect;->right:I

    .line 263
    iget v3, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v2

    iput v3, p2, Landroid/graphics/Rect;->bottom:I

    .line 265
    .end local v0    # "bounds":Landroid/graphics/Rect;
    .end local v1    # "width":I
    .end local v2    # "height":I
    :cond_0
    return-void
.end method

.method public adjustInsetsForFlipFolded(Lcom/android/server/wm/WindowState;Landroid/view/InsetsState;)Landroid/view/InsetsState;
    .locals 2
    .param p1, "ws"    # Lcom/android/server/wm/WindowState;
    .param p2, "insets"    # Landroid/view/InsetsState;

    .line 400
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_3

    .line 403
    invoke-static {}, Landroid/view/DisplayCutoutStub;->get()Landroid/view/DisplayCutoutStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/DisplayCutoutStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 404
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 405
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getActivityType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 408
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, v0, Landroid/view/WindowManager$LayoutParams;->accessibilityTitle:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, v0, Landroid/view/WindowManager$LayoutParams;->accessibilityTitle:Ljava/lang/CharSequence;

    .line 409
    const-string v1, "MiuixImmersionDialog"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 410
    :cond_1
    sget-object v0, Landroid/view/DisplayCutout;->NO_CUTOUT:Landroid/view/DisplayCutout;

    invoke-virtual {p2, v0}, Landroid/view/InsetsState;->setDisplayCutout(Landroid/view/DisplayCutout;)V

    .line 412
    :cond_2
    return-object p2

    .line 406
    :cond_3
    :goto_0
    return-object p2
.end method

.method public adjustInsetsForSplit(Lcom/android/server/wm/WindowState;Landroid/view/InsetsState;)Landroid/view/InsetsState;
    .locals 4
    .param p1, "ws"    # Lcom/android/server/wm/WindowState;
    .param p2, "insets"    # Landroid/view/InsetsState;

    .line 166
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->adjustInsetsForEmbedding(Lcom/android/server/wm/WindowState;Landroid/view/InsetsState;)V

    .line 167
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 168
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isVerticalSplit()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 171
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->inSplitScreenWindowingMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-boolean v0, v0, Lcom/android/server/wm/ActivityRecord;->mImeInsetsFrozenUntilStartInput:Z

    if-nez v0, :cond_2

    .line 173
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 174
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;

    move-result-object v0

    .line 175
    .local v0, "imeTarget":Lcom/android/server/wm/InsetsControlTarget;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/android/server/wm/InsetsControlTarget;->getWindow()Lcom/android/server/wm/WindowState;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 176
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v2

    invoke-interface {v0}, Lcom/android/server/wm/InsetsControlTarget;->getWindow()Lcom/android/server/wm/WindowState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    if-eq v2, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    nop

    .line 177
    .local v1, "removeIme":Z
    :goto_0
    if-eqz v1, :cond_2

    sget v2, Landroid/view/InsetsSource;->ID_IME:I

    invoke-virtual {p2, v2}, Landroid/view/InsetsState;->removeSource(I)V

    .line 179
    .end local v0    # "imeTarget":Lcom/android/server/wm/InsetsControlTarget;
    .end local v1    # "removeIme":Z
    :cond_2
    return-object p2

    .line 169
    :cond_3
    :goto_1
    return-object p2
.end method

.method public getMiuiWindowStateEx(Lcom/android/server/wm/WindowManagerService;Ljava/lang/Object;)Lcom/android/server/wm/IMiuiWindowStateEx;
    .locals 1
    .param p1, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "w"    # Ljava/lang/Object;

    .line 161
    new-instance v0, Lcom/android/server/wm/MiuiWindowStateEx;

    invoke-direct {v0, p1, p2}, Lcom/android/server/wm/MiuiWindowStateEx;-><init>(Lcom/android/server/wm/WindowManagerService;Ljava/lang/Object;)V

    return-object v0
.end method

.method public getScaleCurrentDuration(Lcom/android/server/wm/DisplayContent;F)F
    .locals 1
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "windowAnimationScale"    # F

    .line 312
    if-nez p1, :cond_0

    .line 313
    return p2

    .line 315
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mDisplayContentStub:Lcom/android/server/wm/DisplayContentStub$MutableDisplayContentStub;

    invoke-interface {v0}, Lcom/android/server/wm/DisplayContentStub$MutableDisplayContentStub;->isNoAnimation()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, p2

    :goto_0
    return v0
.end method

.method public isMiuiLayoutInCutoutAlways(Landroid/view/WindowManager$LayoutParams;)Z
    .locals 1
    .param p1, "attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public isStatusBarForceBlackWindow(Landroid/view/WindowManager$LayoutParams;)Z
    .locals 2
    .param p1, "attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 68
    const-string v0, "ForceBlack"

    invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.android.systemui"

    iget-object v1, p1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isWallpaperOffsetUpdateCompleted(Lcom/android/server/wm/WindowState;)Z
    .locals 1
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 302
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->isImageWallpaper(Lcom/android/server/wm/WindowState;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 303
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z

    return v0
.end method

.method public notifyNonAppSurfaceVisibilityChanged(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "type"    # I

    .line 64
    const-string v0, "com.miui.home"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public updateClientHoverState(Lcom/android/server/wm/WindowState;ZZ)V
    .locals 1
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;
    .param p2, "hoverMode"    # Z
    .param p3, "supportTouch"    # Z

    .line 271
    :try_start_0
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mClient:Landroid/view/IWindow;

    invoke-interface {v0, p2, p3}, Landroid/view/IWindow;->updateClientHoverState(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 273
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 275
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public updateHoverGuidePanel(Lcom/android/server/wm/WindowState;Z)V
    .locals 2
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;
    .param p2, "add"    # Z

    .line 278
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 279
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 280
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->updateHoverGuidePanel(Lcom/android/server/wm/WindowState;Z)V

    .line 283
    :cond_0
    return-void
.end method

.method public updateSizeCompatModeWindowInsetsPosition(Lcom/android/server/wm/WindowState;Landroid/graphics/Point;Landroid/graphics/Rect;)Z
    .locals 1
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;
    .param p2, "outPos"    # Landroid/graphics/Point;
    .param p3, "surfaceInsets"    # Landroid/graphics/Rect;

    .line 197
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->needUpdateWindowEnable(Lcom/android/server/wm/WindowState;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget v0, p3, Landroid/graphics/Rect;->left:I

    iput v0, p2, Landroid/graphics/Point;->x:I

    .line 199
    iget v0, p3, Landroid/graphics/Rect;->top:I

    iput v0, p2, Landroid/graphics/Point;->y:I

    .line 200
    const/4 v0, 0x1

    return v0

    .line 202
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public updateSizeCompatModeWindowPosition(Landroid/graphics/Point;Lcom/android/server/wm/WindowState;)V
    .locals 3
    .param p1, "mSurfacePosition"    # Landroid/graphics/Point;
    .param p2, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 184
    invoke-direct {p0, p2}, Lcom/android/server/wm/WindowStateStubImpl;->needUpdateWindowEnable(Lcom/android/server/wm/WindowState;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    iget v0, p1, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Point;->x:I

    .line 186
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    invoke-direct {p0, v0}, Lcom/android/server/wm/WindowStateStubImpl;->needNavBarOffset(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget v0, p1, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget v2, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 188
    invoke-direct {p0, p2}, Lcom/android/server/wm/WindowStateStubImpl;->getNavBarHeight(Lcom/android/server/wm/WindowState;)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 190
    :cond_0
    iget v0, p1, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget v2, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Point;->y:I

    .line 193
    :cond_1
    :goto_0
    return-void
.end method

.method public updateSizeCompatModeWindowPositionForFlip(Landroid/graphics/Point;Lcom/android/server/wm/WindowState;)V
    .locals 9
    .param p1, "mSurfacePosition"    # Landroid/graphics/Point;
    .param p2, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 330
    iget-object v0, p2, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    .line 332
    .local v0, "token":Lcom/android/server/wm/WindowToken;
    invoke-direct {p0, p2}, Lcom/android/server/wm/WindowStateStubImpl;->needUpdateWindowEnableForFlip(Lcom/android/server/wm/WindowState;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 334
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v1

    const/16 v2, 0x7d5

    if-eq v1, v2, :cond_0

    .line 335
    sget v1, Lcom/android/server/wm/WindowStateStubImpl;->FLIP_COMPAT_SCALE_SCALE_VALUE:F

    iput v1, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    .line 338
    :cond_0
    iget-object v1, p2, Lcom/android/server/wm/WindowState;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->inTransition()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 339
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 340
    const/4 v1, 0x0

    invoke-virtual {p2, v1, v1}, Lcom/android/server/wm/WindowState;->hide(ZZ)Z

    .line 343
    :cond_1
    invoke-static {}, Landroid/app/servertransaction/BoundsCompat;->getInstance()Landroid/app/servertransaction/BoundsCompat;

    move-result-object v2

    const v3, 0x3fdc3e06

    new-instance v4, Landroid/graphics/Point;

    .line 345
    invoke-virtual {v0}, Lcom/android/server/wm/WindowToken;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/server/wm/WindowToken;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-direct {v4, v1, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 346
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;

    move-result-object v1

    const/16 v5, 0x11

    invoke-static {v1, v5}, Lcom/android/server/wm/WindowStateStubImpl;->getmGravity(Landroid/app/WindowConfiguration;I)I

    move-result v5

    .line 347
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getRotation()I

    move-result v6

    .line 348
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getRotation()I

    move-result v7

    iget v8, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    .line 343
    invoke-virtual/range {v2 .. v8}, Landroid/app/servertransaction/BoundsCompat;->computeCompatBounds(FLandroid/graphics/Point;IIIF)Landroid/graphics/Rect;

    move-result-object v1

    .line 350
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {p2, v1}, Lcom/android/server/wm/WindowState;->setBounds(Landroid/graphics/Rect;)I

    .line 352
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowConfiguration()Landroid/app/WindowConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getDisplayRotation()I

    move-result v2

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    if-ne v2, v3, :cond_2

    .line 353
    iget v2, p1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    add-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, p1, Landroid/graphics/Point;->x:I

    .line 355
    :cond_2
    iget v2, p1, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget v3, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    add-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, p1, Landroid/graphics/Point;->y:I

    .line 356
    .end local v1    # "rect":Landroid/graphics/Rect;
    goto :goto_0

    .line 357
    :cond_3
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v1

    invoke-interface {v1}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlip()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 358
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v1

    invoke-interface {v1}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v1

    if-nez v1, :cond_4

    .line 359
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v1

    const/16 v2, 0x7ef

    if-ne v1, v2, :cond_4

    .line 360
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p2, Lcom/android/server/wm/WindowState;->mGlobalScale:F

    .line 361
    invoke-virtual {v0}, Lcom/android/server/wm/WindowToken;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/android/server/wm/WindowState;->setBounds(Landroid/graphics/Rect;)I

    .line 365
    :cond_4
    :goto_0
    return-void
.end method

.method public updateSyncStateReady(II)Z
    .locals 1
    .param p1, "viewVisibility"    # I
    .param p2, "syncState"    # I

    .line 321
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 322
    return v0

    .line 324
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public updateWallpaperOffsetUpdateState()V
    .locals 1

    .line 288
    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z

    if-eqz v0, :cond_0

    .line 289
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z

    goto :goto_0

    .line 291
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z

    .line 293
    :goto_0
    return-void
.end method

.method public updateWallpaperOffsetUpdateState(Lcom/android/server/wm/WindowState;)V
    .locals 1
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 296
    invoke-direct {p0, p1}, Lcom/android/server/wm/WindowStateStubImpl;->isImageWallpaper(Lcom/android/server/wm/WindowState;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z

    if-nez v0, :cond_0

    .line 297
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/WindowStateStubImpl;->mIsWallpaperOffsetUpdateCompleted:Z

    .line 299
    :cond_0
    return-void
.end method
