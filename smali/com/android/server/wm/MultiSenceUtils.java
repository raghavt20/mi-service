public class com.android.server.wm.MultiSenceUtils {
	 /* .source "MultiSenceUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MultiSenceUtils$MultiSenceUtilsHolder; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String TAG;
private static com.android.server.wm.MiuiFreeFormManagerService mffms;
private static com.android.server.wm.WindowManagerService service;
/* # direct methods */
static com.android.server.wm.MultiSenceUtils ( ) {
	 /* .locals 2 */
	 /* .line 30 */
	 final String v0 = "persist.multisence.debug.on"; // const-string v0, "persist.multisence.debug.on"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.wm.MultiSenceUtils.DEBUG = (v0!= 0);
	 /* .line 32 */
	 com.android.server.wm.MiuiFreeFormManagerServiceStub .getInstance ( );
	 /* check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService; */
	 /* .line 34 */
	 /* const-string/jumbo v0, "window" */
	 android.os.ServiceManager .getService ( v0 );
	 /* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
	 return;
} // .end method
private com.android.server.wm.MultiSenceUtils ( ) {
	 /* .locals 1 */
	 /* .line 36 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 37 */
	 final String v0 = "MultiSenceUtils init"; // const-string v0, "MultiSenceUtils init"
	 /* invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceUtils;->LOG_IF_DEBUG(Ljava/lang/String;)V */
	 /* .line 38 */
	 return;
} // .end method
 com.android.server.wm.MultiSenceUtils ( ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MultiSenceUtils;-><init>()V */
	 return;
} // .end method
private void LOG_IF_DEBUG ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p1, "log" # Ljava/lang/String; */
	 /* .line 192 */
	 /* sget-boolean v0, Lcom/android/server/wm/MultiSenceUtils;->DEBUG:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 193 */
		 final String v0 = "MultiSenceUtils"; // const-string v0, "MultiSenceUtils"
		 android.util.Slog .d ( v0,p1 );
		 /* .line 195 */
	 } // :cond_0
	 return;
} // .end method
private com.miui.server.multisence.SingleWindowInfo$WindowForm getFreeformWindowForm ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
	 /* .locals 1 */
	 /* .param p1, "stack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
	 /* .line 167 */
	 v0 = 	 (( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).inPinMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 168 */
		 v0 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM_PIN;
		 /* .line 171 */
	 } // :cond_0
	 v0 = 	 (( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).isInMiniFreeFormMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 172 */
		 v0 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM_MINI;
		 /* .line 175 */
	 } // :cond_1
	 v0 = 	 (( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).isInFreeFormMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 176 */
		 v0 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM;
		 /* .line 179 */
	 } // :cond_2
	 v0 = com.miui.server.multisence.SingleWindowInfo$WindowForm.MUTIL_FREEDOM;
} // .end method
public static com.android.server.wm.MultiSenceUtils getInstance ( ) {
	 /* .locals 1 */
	 /* .line 45 */
	 v0 = com.android.server.wm.MultiSenceUtils.mffms;
	 /* if-nez v0, :cond_0 */
	 /* .line 46 */
	 com.android.server.wm.MiuiFreeFormManagerServiceStub .getInstance ( );
	 /* check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService; */
	 /* .line 47 */
} // :cond_0
v0 = com.android.server.wm.MultiSenceUtils.service;
/* if-nez v0, :cond_1 */
/* .line 48 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
/* .line 49 */
} // :cond_1
com.android.server.wm.MultiSenceUtils$MultiSenceUtilsHolder .-$$Nest$sfgetINSTANCE ( );
} // .end method
private java.lang.String getWindowMode ( com.android.server.wm.WindowState p0 ) {
/* .locals 2 */
/* .param p1, "newFocus" # Lcom/android/server/wm/WindowState; */
/* .line 183 */
/* if-nez p1, :cond_0 */
/* .line 184 */
/* const-string/jumbo v0, "unknown" */
/* .line 186 */
} // :cond_0
(( com.android.server.wm.WindowState ) p1 ).getConfiguration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;
/* .line 187 */
/* .local v0, "configuration":Landroid/content/res/Configuration; */
v1 = this.windowConfiguration;
/* .line 188 */
v1 = (( android.app.WindowConfiguration ) v1 ).getWindowingMode ( ); // invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getWindowingMode()I
/* .line 187 */
android.app.WindowConfiguration .windowingModeToString ( v1 );
} // .end method
static Boolean lambda$getWindowsNeedToSched$0 ( com.android.server.wm.ActivityRecord p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 69 */
/* iget-boolean v0, p0, Lcom/android/server/wm/ActivityRecord;->finishing:Z */
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
static void lambda$getWindowsNeedToSched$1 ( java.util.ArrayList p0, com.android.server.wm.WindowState p1 ) { //synthethic
/* .locals 6 */
/* .param p0, "windowlist" # Ljava/util/ArrayList; */
/* .param p1, "w" # Lcom/android/server/wm/WindowState; */
/* .line 62 */
(( com.android.server.wm.WindowState ) p1 ).getTaskFragment ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTaskFragment()Lcom/android/server/wm/TaskFragment;
if ( v0 != null) { // if-eqz v0, :cond_4
(( com.android.server.wm.WindowState ) p1 ).getTaskFragment ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getTaskFragment()Lcom/android/server/wm/TaskFragment;
int v1 = 0; // const/4 v1, 0x0
v0 = (( com.android.server.wm.TaskFragment ) v0 ).shouldBeVisible ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/TaskFragment;->shouldBeVisible(Lcom/android/server/wm/ActivityRecord;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 63 */
int v0 = 1; // const/4 v0, 0x1
/* .line 67 */
/* .local v0, "shouldAdd":Z */
(( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
final String v3 = "com.miui.securitycenter"; // const-string v3, "com.miui.securitycenter"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
(( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
final String v4 = "com.lbe.security.miui"; // const-string v4, "com.lbe.security.miui"
v2 = (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 68 */
} // :cond_0
(( com.android.server.wm.WindowState ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getRootTask()Lcom/android/server/wm/Task;
/* .line 69 */
/* .local v2, "rootTask":Lcom/android/server/wm/Task; */
/* if-nez v2, :cond_1 */
/* move-object v4, v1 */
} // :cond_1
/* new-instance v4, Lcom/android/server/wm/MultiSenceUtils$$ExternalSyntheticLambda0; */
/* invoke-direct {v4}, Lcom/android/server/wm/MultiSenceUtils$$ExternalSyntheticLambda0;-><init>()V */
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.wm.Task ) v2 ).getActivity ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Lcom/android/server/wm/Task;->getActivity(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/ActivityRecord;
/* .line 70 */
/* .local v4, "baseActivity":Lcom/android/server/wm/ActivityRecord; */
} // :goto_0
/* if-nez v4, :cond_2 */
} // :cond_2
(( com.android.server.wm.ActivityRecord ) v4 ).getProcessName ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->getProcessName()Ljava/lang/String;
/* .line 71 */
/* .local v1, "baseName":Ljava/lang/String; */
} // :goto_1
v3 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_3 */
/* .line 72 */
int v0 = 0; // const/4 v0, 0x0
/* .line 75 */
} // .end local v1 # "baseName":Ljava/lang/String;
} // .end local v2 # "rootTask":Lcom/android/server/wm/Task;
} // .end local v4 # "baseActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 76 */
(( java.util.ArrayList ) p0 ).add ( p1 ); // invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 79 */
} // .end local v0 # "shouldAdd":Z
} // :cond_4
return;
} // .end method
/* # virtual methods */
public java.util.Map getWindowsNeedToSched ( ) {
/* .locals 20 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/multisence/SingleWindowInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 53 */
/* move-object/from16 v1, p0 */
final String v2 = "MultiSenceUtils"; // const-string v2, "MultiSenceUtils"
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* move-object v3, v0 */
/* .line 54 */
/* .local v3, "sWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v4, v0 */
/* .line 55 */
/* .local v4, "windowlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/WindowState;>;" */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* move-object v5, v0 */
/* .line 56 */
/* .local v5, "mFreeFormLocation":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;>;" */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* move-object v6, v0 */
/* .line 58 */
/* .local v6, "mFloatWindowLocation":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;>;" */
v0 = com.android.server.wm.MultiSenceUtils.service;
int v7 = 0; // const/4 v7, 0x0
/* if-nez v0, :cond_0 */
/* .line 59 */
/* .line 61 */
} // :cond_0
v0 = this.mRoot;
/* new-instance v8, Lcom/android/server/wm/MultiSenceUtils$$ExternalSyntheticLambda1; */
/* invoke-direct {v8, v4}, Lcom/android/server/wm/MultiSenceUtils$$ExternalSyntheticLambda1;-><init>(Ljava/util/ArrayList;)V */
int v9 = 1; // const/4 v9, 0x1
(( com.android.server.wm.RootWindowContainer ) v0 ).forAllWindows ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Lcom/android/server/wm/RootWindowContainer;->forAllWindows(Ljava/util/function/Consumer;Z)V
/* .line 81 */
v0 = com.android.server.wm.MultiSenceUtils.mffms;
/* if-nez v0, :cond_1 */
/* .line 82 */
/* .line 84 */
} // :cond_1
v0 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v0 = } // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_4
/* move-object v8, v0 */
/* check-cast v8, Ljava/lang/Integer; */
/* .line 85 */
/* .local v8, "taskid":Ljava/lang/Integer; */
int v10 = 0; // const/4 v10, 0x0
/* .line 86 */
/* .local v10, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
int v11 = 0; // const/4 v11, 0x0
/* .line 88 */
/* .local v11, "name":Ljava/lang/String; */
try { // :try_start_0
v0 = com.android.server.wm.MultiSenceUtils.mffms;
v0 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v8 ); // invoke-virtual {v0, v8}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* move-object v10, v0 */
/* .line 89 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v10 ).getStackPackageName ( ); // invoke-virtual {v10}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* move-object v11, v0 */
/* .line 93 */
/* nop */
/* .line 95 */
if ( v10 != null) { // if-eqz v10, :cond_3
/* if-nez v11, :cond_2 */
/* .line 96 */
/* .line 99 */
} // :cond_2
int v12 = -1; // const/4 v12, -0x1
/* .line 101 */
/* .local v12, "pid":I */
try { // :try_start_1
v0 = this.mTask;
int v13 = 0; // const/4 v13, 0x0
(( com.android.server.wm.Task ) v0 ).getTopActivity ( v13, v9 ); // invoke-virtual {v0, v13, v9}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
/* .line 102 */
/* .local v0, "r":Lcom/android/server/wm/ActivityRecord; */
v13 = (( com.android.server.wm.ActivityRecord ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getPid()I
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* move v12, v13 */
/* .line 105 */
} // .end local v0 # "r":Lcom/android/server/wm/ActivityRecord;
/* .line 103 */
/* :catch_0 */
/* move-exception v0 */
/* .line 104 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "get pid failed: "; // const-string v14, "get pid failed: "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v13 );
/* .line 107 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
(( com.android.server.wm.MiuiFreeFormActivityStack ) v10 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v10}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
v0 = this.bounds;
/* .line 108 */
/* .local v0, "mBounds":Landroid/graphics/Rect; */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v10 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v10}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* iget v13, v13, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->freeFormScale:F */
/* .line 109 */
/* .local v13, "mFreeformScale":F */
/* iget v14, v0, Landroid/graphics/Rect;->left:I */
/* iget v15, v0, Landroid/graphics/Rect;->right:I */
/* iget v9, v0, Landroid/graphics/Rect;->left:I */
/* sub-int/2addr v15, v9 */
/* int-to-float v9, v15 */
/* mul-float/2addr v9, v13 */
/* float-to-int v9, v9 */
/* add-int/2addr v14, v9 */
/* .line 110 */
/* .local v14, "right":I */
/* iget v9, v0, Landroid/graphics/Rect;->top:I */
/* iget v15, v0, Landroid/graphics/Rect;->bottom:I */
/* move-object/from16 v16, v7 */
/* iget v7, v0, Landroid/graphics/Rect;->top:I */
/* sub-int/2addr v15, v7 */
/* int-to-float v7, v15 */
/* mul-float/2addr v7, v13 */
/* float-to-int v7, v7 */
/* add-int/2addr v9, v7 */
/* .line 111 */
/* .local v9, "bottom":I */
/* new-instance v7, Landroid/graphics/Rect; */
/* iget v15, v0, Landroid/graphics/Rect;->left:I */
/* move-object/from16 v17, v8 */
} // .end local v8 # "taskid":Ljava/lang/Integer;
/* .local v17, "taskid":Ljava/lang/Integer; */
/* iget v8, v0, Landroid/graphics/Rect;->top:I */
/* invoke-direct {v7, v15, v8, v14, v9}, Landroid/graphics/Rect;-><init>(IIII)V */
/* .line 112 */
/* .local v7, "realBounds":Landroid/graphics/Rect; */
/* invoke-direct {v1, v10}, Lcom/android/server/wm/MultiSenceUtils;->getFreeformWindowForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/miui/server/multisence/SingleWindowInfo$WindowForm; */
/* .line 114 */
/* .local v8, "realWindowForm":Lcom/miui/server/multisence/SingleWindowInfo$WindowForm; */
/* new-instance v15, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo; */
/* invoke-direct {v15, v8, v7, v12}, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;-><init>(Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;Landroid/graphics/Rect;I)V */
/* .line 115 */
/* .local v15, "detailInfo":Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo; */
/* .line 117 */
/* move-object/from16 v18, v0 */
} // .end local v0 # "mBounds":Landroid/graphics/Rect;
/* .local v18, "mBounds":Landroid/graphics/Rect; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v19, v7 */
} // .end local v7 # "realBounds":Landroid/graphics/Rect;
/* .local v19, "realBounds":Landroid/graphics/Rect; */
final String v7 = "FreeForm: "; // const-string v7, "FreeForm: "
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", Pid: "; // const-string v7, ", Pid: "
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v0}, Lcom/android/server/wm/MultiSenceUtils;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 120 */
} // .end local v8 # "realWindowForm":Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;
} // .end local v9 # "bottom":I
} // .end local v10 # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v11 # "name":Ljava/lang/String;
} // .end local v12 # "pid":I
} // .end local v13 # "mFreeformScale":F
} // .end local v14 # "right":I
} // .end local v15 # "detailInfo":Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;
} // .end local v17 # "taskid":Ljava/lang/Integer;
} // .end local v18 # "mBounds":Landroid/graphics/Rect;
} // .end local v19 # "realBounds":Landroid/graphics/Rect;
/* move-object/from16 v7, v16 */
int v9 = 1; // const/4 v9, 0x1
/* goto/16 :goto_0 */
/* .line 95 */
/* .local v8, "taskid":Ljava/lang/Integer; */
/* .restart local v10 # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .restart local v11 # "name":Ljava/lang/String; */
} // :cond_3
/* move-object/from16 v16, v7 */
/* move-object/from16 v17, v8 */
} // .end local v8 # "taskid":Ljava/lang/Integer;
/* .restart local v17 # "taskid":Ljava/lang/Integer; */
int v9 = 1; // const/4 v9, 0x1
/* goto/16 :goto_0 */
/* .line 90 */
} // .end local v17 # "taskid":Ljava/lang/Integer;
/* .restart local v8 # "taskid":Ljava/lang/Integer; */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v16, v7 */
/* move-object/from16 v17, v8 */
/* .line 91 */
} // .end local v8 # "taskid":Ljava/lang/Integer;
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v17 # "taskid":Ljava/lang/Integer; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "get freeform app name failed, error: "; // const-string v8, "get freeform app name failed, error: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v7 );
/* .line 92 */
/* move-object/from16 v7, v16 */
int v9 = 1; // const/4 v9, 0x1
/* goto/16 :goto_0 */
/* .line 125 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v10 # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v11 # "name":Ljava/lang/String;
} // .end local v17 # "taskid":Ljava/lang/Integer;
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_2
v2 = (( java.util.ArrayList ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v2, :cond_8 */
/* .line 126 */
(( java.util.ArrayList ) v4 ).get ( v0 ); // invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/WindowState; */
/* .line 127 */
/* .local v2, "windowInfo":Lcom/android/server/wm/WindowState; */
(( com.android.server.wm.WindowState ) v2 ).getOwningPackage ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* .line 129 */
v8 = /* .local v7, "name":Ljava/lang/String; */
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 130 */
/* new-instance v8, Lcom/miui/server/multisence/SingleWindowInfo; */
v9 = com.miui.server.multisence.SingleWindowInfo$AppType.COMMON;
/* invoke-direct {v8, v7, v9}, Lcom/miui/server/multisence/SingleWindowInfo;-><init>(Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$AppType;)V */
/* .line 131 */
final String v9 = "freeform"; // const-string v9, "freeform"
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setWindowingModeString ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setWindowingModeString(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 132 */
int v9 = 1; // const/4 v9, 0x1
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setVisiable ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setVisiable(Z)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 133 */
/* check-cast v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo; */
v9 = this.mWindowForm;
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setgetWindowForm ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setgetWindowForm(Lcom/miui/server/multisence/SingleWindowInfo$WindowForm;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 134 */
/* check-cast v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo; */
/* iget v9, v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo;->mPid:I */
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setPid ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setPid(I)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 135 */
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setLayerOrder ( v0 ); // invoke-virtual {v8, v0}, Lcom/miui/server/multisence/SingleWindowInfo;->setLayerOrder(I)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 136 */
v9 = (( com.android.server.wm.WindowState ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getUid()I
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setUid ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setUid(I)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 137 */
/* check-cast v9, Lcom/miui/server/multisence/SingleWindowInfo$FreeFormInfo; */
v9 = this.mRect;
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setRectValue ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setRectValue(Landroid/graphics/Rect;)Lcom/miui/server/multisence/SingleWindowInfo;
int v9 = 1; // const/4 v9, 0x1
/* .local v8, "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 138 */
} // .end local v8 # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo;
v8 = } // :cond_5
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 139 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "this is floating window: " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v8}, Lcom/android/server/wm/MultiSenceUtils;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 140 */
/* new-instance v8, Lcom/miui/server/multisence/SingleWindowInfo; */
v9 = com.miui.server.multisence.SingleWindowInfo$AppType.COMMON;
/* invoke-direct {v8, v7, v9}, Lcom/miui/server/multisence/SingleWindowInfo;-><init>(Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$AppType;)V */
/* .line 141 */
/* invoke-direct {v1, v2}, Lcom/android/server/wm/MultiSenceUtils;->getWindowMode(Lcom/android/server/wm/WindowState;)Ljava/lang/String; */
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setWindowingModeString ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setWindowingModeString(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 142 */
int v9 = 1; // const/4 v9, 0x1
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setVisiable ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setVisiable(Z)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 143 */
v9 = (( com.android.server.wm.WindowState ) v2 ).getPid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getPid()I
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setPid ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setPid(I)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 144 */
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setLayerOrder ( v0 ); // invoke-virtual {v8, v0}, Lcom/miui/server/multisence/SingleWindowInfo;->setLayerOrder(I)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 145 */
v9 = (( com.android.server.wm.WindowState ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getUid()I
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setUid ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setUid(I)Lcom/miui/server/multisence/SingleWindowInfo;
int v9 = 1; // const/4 v9, 0x1
/* .restart local v8 # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 147 */
} // .end local v8 # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo;
} // :cond_6
/* new-instance v8, Lcom/miui/server/multisence/SingleWindowInfo; */
v9 = com.miui.server.multisence.SingleWindowInfo$AppType.COMMON;
/* invoke-direct {v8, v7, v9}, Lcom/miui/server/multisence/SingleWindowInfo;-><init>(Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo$AppType;)V */
/* .line 148 */
/* invoke-direct {v1, v2}, Lcom/android/server/wm/MultiSenceUtils;->getWindowMode(Lcom/android/server/wm/WindowState;)Ljava/lang/String; */
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setWindowingModeString ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setWindowingModeString(Ljava/lang/String;)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 149 */
int v9 = 1; // const/4 v9, 0x1
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setVisiable ( v9 ); // invoke-virtual {v8, v9}, Lcom/miui/server/multisence/SingleWindowInfo;->setVisiable(Z)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 150 */
v10 = (( com.android.server.wm.WindowState ) v2 ).getPid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getPid()I
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setPid ( v10 ); // invoke-virtual {v8, v10}, Lcom/miui/server/multisence/SingleWindowInfo;->setPid(I)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 151 */
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setLayerOrder ( v0 ); // invoke-virtual {v8, v0}, Lcom/miui/server/multisence/SingleWindowInfo;->setLayerOrder(I)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 152 */
v10 = (( com.android.server.wm.WindowState ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getUid()I
(( com.miui.server.multisence.SingleWindowInfo ) v8 ).setUid ( v10 ); // invoke-virtual {v8, v10}, Lcom/miui/server/multisence/SingleWindowInfo;->setUid(I)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 155 */
/* .restart local v8 # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo; */
v10 = } // :goto_3
if ( v10 != null) { // if-eqz v10, :cond_7
/* .line 156 */
/* check-cast v10, Lcom/miui/server/multisence/SingleWindowInfo; */
/* .line 157 */
/* .local v10, "swi":Lcom/miui/server/multisence/SingleWindowInfo; */
v11 = (( com.miui.server.multisence.SingleWindowInfo ) v10 ).getWindowCount ( ); // invoke-virtual {v10}, Lcom/miui/server/multisence/SingleWindowInfo;->getWindowCount()I
/* .line 158 */
/* .local v11, "windowCount":I */
/* add-int/lit8 v12, v11, 0x1 */
(( com.miui.server.multisence.SingleWindowInfo ) v10 ).setWindowCount ( v12 ); // invoke-virtual {v10, v12}, Lcom/miui/server/multisence/SingleWindowInfo;->setWindowCount(I)Lcom/miui/server/multisence/SingleWindowInfo;
/* .line 159 */
} // .end local v10 # "swi":Lcom/miui/server/multisence/SingleWindowInfo;
} // .end local v11 # "windowCount":I
/* .line 160 */
} // :cond_7
/* .line 125 */
} // .end local v2 # "windowInfo":Lcom/android/server/wm/WindowState;
} // .end local v7 # "name":Ljava/lang/String;
} // .end local v8 # "singleWindow":Lcom/miui/server/multisence/SingleWindowInfo;
} // :goto_4
/* add-int/lit8 v0, v0, 0x1 */
/* goto/16 :goto_2 */
/* .line 163 */
} // .end local v0 # "i":I
} // :cond_8
} // .end method
