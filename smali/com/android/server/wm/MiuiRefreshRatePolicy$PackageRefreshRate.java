class com.android.server.wm.MiuiRefreshRatePolicy$PackageRefreshRate {
	 /* .source "MiuiRefreshRatePolicy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiRefreshRatePolicy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "PackageRefreshRate" */
} // .end annotation
/* # instance fields */
private final java.util.HashMap mPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.wm.MiuiRefreshRatePolicy this$0; //synthetic
/* # direct methods */
static java.util.HashMap -$$Nest$fgetmPackages ( com.android.server.wm.MiuiRefreshRatePolicy$PackageRefreshRate p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackages;
} // .end method
 com.android.server.wm.MiuiRefreshRatePolicy$PackageRefreshRate ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiRefreshRatePolicy; */
/* .line 25 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 26 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mPackages = v0;
return;
} // .end method
/* # virtual methods */
public void add ( java.lang.String p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "s" # Ljava/lang/String; */
/* .param p2, "preferredMaxRefreshRate" # F */
/* .line 29 */
v0 = this.mPackages;
java.lang.Float .valueOf ( p2 );
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 30 */
return;
} // .end method
public java.lang.Float get ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "s" # Ljava/lang/String; */
/* .line 33 */
v0 = this.mPackages;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Float; */
} // .end method
public void remove ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "s" # Ljava/lang/String; */
/* .line 37 */
v0 = this.mPackages;
(( java.util.HashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 38 */
return;
} // .end method
