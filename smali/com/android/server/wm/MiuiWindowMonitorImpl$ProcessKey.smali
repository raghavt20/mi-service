.class Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
.super Ljava/lang/Object;
.source "MiuiWindowMonitorImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiWindowMonitorImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProcessKey"
.end annotation


# instance fields
.field mPackageName:Ljava/lang/String;

.field mPid:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669
    iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPid:I

    .line 670
    iput-object p2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPackageName:Ljava/lang/String;

    .line 671
    return-void
.end method


# virtual methods
.method public equals(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Z
    .locals 4
    .param p1, "that"    # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    .line 674
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 675
    return v0

    .line 677
    :cond_0
    const/4 v1, 0x1

    if-ne p0, p1, :cond_1

    .line 678
    return v1

    .line 680
    :cond_1
    iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPid:I

    iget v3, p1, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPid:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPackageName:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPackageName:Ljava/lang/String;

    .line 681
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 682
    return v1

    .line 684
    :cond_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .line 690
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->equals(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 691
    :catch_0
    move-exception v0

    .line 693
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 698
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
