.class public Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;
.super Ljava/lang/Object;
.source "MiuiFreeFormStackDisplayStrategy.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mDefaultMaxFreeformCount:I

.field private mDefaultMaxGameFreeformCount:I

.field private mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V
    .locals 1
    .param p1, "service"    # Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "MiuiFreeFormStackDisplayStrategy"

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->TAG:Ljava/lang/String;

    .line 28
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxFreeformCount:I

    .line 30
    iput v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxGameFreeformCount:I

    .line 33
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 34
    return-void
.end method

.method private isInEmbeddedWindowingMode(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z
    .locals 4
    .param p1, "stack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 41
    iget-object v0, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v0

    .line 42
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/TaskDisplayArea;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 43
    .local v0, "topActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v2

    iget-object v3, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v3

    if-ne v2, v3, :cond_0

    goto :goto_1

    .line 46
    :cond_0
    nop

    .line 47
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    .line 46
    const-string v3, "isActivityEmbedded"

    invoke-static {v0, v3, v2}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 48
    .local v2, "activityEmbedded":Ljava/lang/Object;
    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    move-object v1, v2

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_0
    return v1

    .line 44
    .end local v2    # "activityEmbedded":Ljava/lang/Object;
    :cond_2
    :goto_1
    return v1
.end method

.method private isSplitScreenMode()Z
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getMaxMiuiFreeFormStackCount(Ljava/lang/String;Lcom/android/server/wm/MiuiFreeFormActivityStack;)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "stack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 144
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const/4 v0, 0x4

    return v0

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/util/MiuiMultiWindowUtils;->multiFreeFormSupported(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 148
    return v1

    .line 150
    :cond_1
    const/4 v0, 0x0

    if-nez p2, :cond_2

    return v0

    .line 151
    :cond_2
    sget v2, Lmiui/os/Build;->TOTAL_RAM:I

    .line 152
    .local v2, "totalMemory":I
    const/4 v3, 0x7

    if-lt v2, v3, :cond_5

    .line 153
    invoke-direct {p0, p2}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->isInEmbeddedWindowingMode(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_3

    .line 154
    return v1

    .line 156
    :cond_3
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->isSplitScreenMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 157
    return v1

    .line 159
    :cond_4
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxFreeformCount:I

    return v0

    .line 160
    :cond_5
    const/4 v3, 0x5

    if-lt v2, v3, :cond_8

    .line 161
    invoke-direct {p0, p2}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->isInEmbeddedWindowingMode(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 162
    return v1

    .line 164
    :cond_6
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->isSplitScreenMode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 165
    return v1

    .line 167
    :cond_7
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxFreeformCount:I

    return v0

    .line 168
    :cond_8
    const/4 v3, 0x3

    if-lt v2, v3, :cond_9

    .line 169
    return v1

    .line 171
    :cond_9
    return v0
.end method

.method public onMiuiFreeFormStasckAdded(Ljava/util/concurrent/ConcurrentHashMap;Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 10
    .param p2, "miuiFreeFormGestureController"    # Lcom/android/server/wm/MiuiFreeFormGestureController;
    .param p3, "addingStack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/wm/MiuiFreeFormActivityStack;",
            ">;",
            "Lcom/android/server/wm/MiuiFreeFormGestureController;",
            "Lcom/android/server/wm/MiuiFreeFormActivityStack;",
            ")V"
        }
    .end annotation

    .line 53
    .local p1, "freeFormActivityStacks":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
    invoke-virtual {p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHasHadStackAdded:Z

    if-eqz v0, :cond_0

    goto/16 :goto_4

    .line 54
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHasHadStackAdded:Z

    .line 55
    invoke-virtual {p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/util/MiuiMultiWindowAdapter;->isInTopGameList(Ljava/lang/String;)Z

    move-result v1

    .line 56
    .local v1, "isAddingTopGame":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .local v2, "log":Ljava/lang/StringBuilder;
    const-string v3, "onMiuiFreeFormStasckAdded  isAddingTopGame= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 59
    const-string v3, " addingStack= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    invoke-virtual {p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiFreeFormStackDisplayStrategy"

    invoke-static {v0, v4, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 62
    if-eqz v1, :cond_4

    .line 63
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v3

    if-nez v3, :cond_3

    .line 64
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 65
    .local v5, "taskId":Ljava/lang/Integer;
    invoke-virtual {p1, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 66
    .local v6, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v8}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v8

    if-eq v7, v8, :cond_1

    invoke-virtual {v6}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/util/MiuiMultiWindowAdapter;->isInTopGameList(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 67
    invoke-virtual {p2, v6}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 68
    const-string v3, "onMiuiFreeFormStasckAdded Max TOP GAME FreeForm Window Num reached!"

    invoke-static {v0, v4, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void

    .line 71
    .end local v5    # "taskId":Ljava/lang/Integer;
    .end local v6    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_1
    goto :goto_0

    :cond_2
    goto :goto_1

    .line 73
    :cond_3
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v3, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getGameFreeFormCount(Lcom/android/server/wm/MiuiFreeFormActivityStack;)I

    move-result v3

    iget v5, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mDefaultMaxGameFreeformCount:I

    if-lt v3, v5, :cond_4

    .line 74
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v3, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getBottomGameFreeFormActivityStack(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v3

    .line 75
    .local v3, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v3, :cond_4

    .line 76
    invoke-virtual {p2, v3}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 77
    const-string v5, "MiuiDesktopModeStatus isActive, onMiuiFreeFormStasckAdded Max TOP GAME FreeForm Window Num reached!"

    invoke-static {v0, v4, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 83
    .end local v3    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_4
    :goto_1
    invoke-virtual {p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->getMaxMiuiFreeFormStackCount(Ljava/lang/String;Lcom/android/server/wm/MiuiFreeFormActivityStack;)I

    move-result v3

    .line 84
    .local v3, "maxStackCount":I
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v5

    .line 85
    .local v5, "size":I
    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 86
    const-string v6, "onMiuiFreeFormStasckAdded size = "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 88
    const-string v6, " getMaxMiuiFreeFormStackCount() = "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 90
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v4, v6}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 91
    if-le v5, v3, :cond_9

    .line 92
    iget-object v6, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v6, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getReplaceFreeForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v6

    .line 93
    .restart local v6    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v6, :cond_9

    .line 94
    iget-object v7, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v7, :cond_5

    iget-object v7, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v7}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v7

    if-eqz v7, :cond_5

    if-eqz p3, :cond_5

    iget-object v7, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v7, :cond_5

    iget-object v7, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v7, v7, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    if-eqz v7, :cond_5

    iget-object v7, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v7, v7, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 96
    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    if-eqz v7, :cond_5

    sget-object v7, Landroid/util/MiuiMultiWindowAdapter;->sNotExitFreeFormWhenAddOtherFreeFormTask:Ljava/util/Map;

    iget-object v8, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 97
    invoke-virtual {v8}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v8

    iget-object v8, v8, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_5

    sget-object v7, Landroid/util/MiuiMultiWindowAdapter;->sNotExitFreeFormWhenAddOtherFreeFormTask:Ljava/util/Map;

    iget-object v8, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 98
    invoke-virtual {v8}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v8

    iget-object v8, v8, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iget-object v8, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v8, v8, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 99
    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 100
    return-void

    .line 102
    :cond_5
    iget-object v7, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v7, :cond_6

    iget-object v7, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v7}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v7

    if-eqz v7, :cond_6

    iget-object v7, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v7}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v7

    iget-object v7, v7, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v7, :cond_6

    iget-object v7, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 103
    invoke-virtual {v7}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v7

    iget-object v7, v7, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    if-eqz v7, :cond_6

    iget-object v7, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v7, :cond_6

    iget-object v7, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v7, v7, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    if-eqz v7, :cond_6

    iget-object v7, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v7, v7, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 104
    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    if-eqz v7, :cond_6

    sget-object v7, Landroid/util/MiuiMultiWindowAdapter;->HIDE_SELF_IF_NEW_FREEFORM_TASK_WHITE_LIST_ACTIVITY:Ljava/util/List;

    iget-object v8, v6, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 106
    invoke-virtual {v8}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v8

    iget-object v8, v8, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    sget-object v7, Landroid/util/MiuiMultiWindowAdapter;->SHOW_HIDDEN_TASK_IF_FINISHED_WHITE_LIST_ACTIVITY:Ljava/util/List;

    iget-object v8, p3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget-object v8, v8, Lcom/android/server/wm/Task;->intent:Landroid/content/Intent;

    .line 108
    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 109
    const-string v7, "onMiuiFreeFormStasckAdded before startExitApplication "

    invoke-static {v0, v4, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 110
    return-void

    .line 112
    :cond_6
    iget-object v7, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v7, v7, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v7, v7, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v7

    const-string v8, "onMiuiFreeFormStasckAdded Max FreeForm Window Num reached! exit mffas\uff1a "

    if-eqz v7, :cond_8

    .line 113
    iget-object v7, p0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v7, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFrontFreeformNum(Lcom/android/server/wm/MiuiFreeFormActivityStack;)I

    move-result v7

    .line 114
    .local v7, "frontSize":I
    if-lt v7, v3, :cond_7

    .line 115
    invoke-virtual {p2, v6}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 116
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v4, v8}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 119
    :cond_7
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onMiuiFreeFormStasckAdded frontSize\uff1a "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", maxStackCount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v4, v8}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 121
    .end local v7    # "frontSize":I
    :goto_2
    goto :goto_3

    .line 122
    :cond_8
    invoke-virtual {p2, v6}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 123
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v4, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->logd(ZLjava/lang/String;Ljava/lang/String;)V

    .line 129
    .end local v6    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_9
    :goto_3
    return-void

    .line 53
    .end local v1    # "isAddingTopGame":Z
    .end local v2    # "log":Ljava/lang/StringBuilder;
    .end local v3    # "maxStackCount":I
    .end local v5    # "size":I
    :cond_a
    :goto_4
    return-void
.end method
