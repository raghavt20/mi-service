.class public Lcom/android/server/wm/FgWindowChangedInfo;
.super Ljava/lang/Object;
.source "FgWindowChangedInfo.java"


# instance fields
.field final multiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

.field final pid:I

.field final record:Lcom/android/server/wm/ActivityRecord;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/ActivityRecord;Landroid/content/pm/ApplicationInfo;I)V
    .locals 0
    .param p1, "_record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "_info"    # Landroid/content/pm/ApplicationInfo;
    .param p3, "_pid"    # I

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/android/server/wm/FgWindowChangedInfo;->record:Lcom/android/server/wm/ActivityRecord;

    .line 13
    iput-object p2, p0, Lcom/android/server/wm/FgWindowChangedInfo;->multiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

    .line 14
    iput p3, p0, Lcom/android/server/wm/FgWindowChangedInfo;->pid:I

    .line 15
    return-void
.end method
