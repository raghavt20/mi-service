.class Lcom/android/server/wm/MiuiPaperContrastOverlay$1;
.super Ljava/lang/Object;
.source "MiuiPaperContrastOverlay.java"

# interfaces
.implements Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiPaperContrastOverlay;-><init>(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;


# direct methods
.method public static synthetic $r8$lambda$qIIL2sVAcckrdwBuMeo8JD6sNB8(Lcom/android/server/wm/MiuiPaperContrastOverlay$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;->lambda$onStateChanged$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/MiuiPaperContrastOverlay;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiPaperContrastOverlay;

    .line 124
    iput-object p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$onStateChanged$0()V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->showPaperModeSurface()V

    .line 140
    return-void
.end method


# virtual methods
.method public onBaseStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .line 133
    return-void
.end method

.method public onStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .line 137
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmNeedShowPaperSurface(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmFoldDeviceReady(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-static {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->-$$Nest$fgetmWms(Lcom/android/server/wm/MiuiPaperContrastOverlay;)Lcom/android/server/wm/WindowManagerService;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    new-instance v1, Lcom/android/server/wm/MiuiPaperContrastOverlay$1$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay$1;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 142
    :cond_0
    return-void
.end method

.method public onSupportedStatesChanged([I)V
    .locals 0
    .param p1, "supportedStates"    # [I

    .line 128
    return-void
.end method
