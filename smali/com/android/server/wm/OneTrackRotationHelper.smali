.class public Lcom/android/server/wm/OneTrackRotationHelper;
.super Ljava/lang/Object;
.source "OneTrackRotationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;,
        Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;
    }
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000000779"

.field private static DEBUG:Z = false

.field private static final DEVICE_TYPE:Ljava/lang/String;

.field private static final ENABLE_TRACK:Z

.field private static final EVENT_NAME:Ljava/lang/String; = "screen_use_duration"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final IS_FLIP:Z

.field private static final IS_FOLD:Z

.field private static final IS_TABLET:Z

.field private static final ONETRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final ONE_TRACE_INTERVAL:J = 0x6ddd00L

.field private static final ONE_TRACE_INTERVAL_DEBUG:J = 0x1d4c0L

.field private static final ON_DEVICE_FOLD_CHANGED:I = 0x7

.field private static final ON_FOREGROUND_WINDOW_CHANGED:I = 0x2

.field private static final ON_NEXT_SENDING_COMING:I = 0x5

.field private static final ON_ROTATION_CHANGED:I = 0x1

.field private static final ON_SCREEN_STATE_CHANGED:I = 0x3

.field private static final ON_SHUT_DOWN:I = 0x4

.field private static final ON_TODAY_IS_OVER:I = 0x6

.field private static final PACKAGE:Ljava/lang/String; = "android"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "android"

.field private static final SCREEN_OFF:I = 0x3

.field private static final SCREEN_ON:I = 0x1

.field private static final SCREEN_ON_NOTIFICATION:Ljava/lang/String; = "com.android.systemui:NOTIFICATION"

.field private static final SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final TAG:Ljava/lang/String; = "OneTrackRotationHelper"

.field private static final TIP:Ljava/lang/String; = "866.2.1.1.28680"

.field private static final TIP_FOR_SCREENDATA:Ljava/lang/String; = "866.2.1.1.32924"

.field private static mIsScreenOnNotification:Z

.field private static mReportInterval:J

.field private static volatile sInstance:Lcom/android/server/wm/OneTrackRotationHelper;


# instance fields
.field private final MIN_TIME:F

.field private currentTimeMillis:J

.field private lastForegroundPkg:Ljava/lang/String;

.field mAm:Landroid/app/ActivityManager;

.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private mIsInit:Z

.field mKm:Landroid/app/KeyguardManager;

.field mPm:Landroid/os/PowerManager;

.field private mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

.field private mScreenStateReceiver:Landroid/content/BroadcastReceiver;

.field mThread:Landroid/os/HandlerThread;

.field mWm:Landroid/view/WindowManager;


# direct methods
.method public static synthetic $r8$lambda$f15T8AsVfpLDSHfz7B6-J2-Lg9M(Lcom/android/server/wm/OneTrackRotationHelper;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->lambda$trackScreenData$0(ILjava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$xga9131XNdXrqjZSygbQkPhkf0A(Lcom/android/server/wm/OneTrackRotationHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->initAllListeners()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetcurrentTimeMillis(Lcom/android/server/wm/OneTrackRotationHelper;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmIsInit(Lcom/android/server/wm/OneTrackRotationHelper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmRotationStateMachine(Lcom/android/server/wm/OneTrackRotationHelper;)Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputcurrentTimeMillis(Lcom/android/server/wm/OneTrackRotationHelper;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J

    return-void
.end method

.method static bridge synthetic -$$Nest$mprepareFinalSendingInToday(Lcom/android/server/wm/OneTrackRotationHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->prepareFinalSendingInToday()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprepareNextSending(Lcom/android/server/wm/OneTrackRotationHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->prepareNextSending()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrecordTimeForFlip(Lcom/android/server/wm/OneTrackRotationHelper;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/OneTrackRotationHelper;->recordTimeForFlip(F)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportOneTrack(Lcom/android/server/wm/OneTrackRotationHelper;Ljava/util/ArrayList;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->reportOneTrack(Ljava/util/ArrayList;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportOneTrackForScreenData(Lcom/android/server/wm/OneTrackRotationHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->reportOneTrackForScreenData()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetIS_FLIP()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetIS_FOLD()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmIsScreenOnNotification()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsScreenOnNotification:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfputDEBUG(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmIsScreenOnNotification(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsScreenOnNotification:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmReportInterval(J)V
    .locals 0

    sput-wide p0, Lcom/android/server/wm/OneTrackRotationHelper;->mReportInterval:J

    return-void
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 51
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z

    .line 92
    sput-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsScreenOnNotification:Z

    .line 96
    const-wide/32 v1, 0x6ddd00

    sput-wide v1, Lcom/android/server/wm/OneTrackRotationHelper;->mReportInterval:J

    .line 98
    const-string v1, "ro.build.characteristics"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tablet"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x1

    if-nez v1, :cond_1

    .line 99
    const-string v1, "ro.config.tablet"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v0

    goto :goto_1

    :cond_1
    :goto_0
    move v1, v3

    :goto_1
    sput-boolean v1, Lcom/android/server/wm/OneTrackRotationHelper;->IS_TABLET:Z

    .line 100
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFoldDevice()Z

    move-result v4

    sput-boolean v4, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z

    .line 101
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v5

    sput-boolean v5, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z

    .line 102
    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    if-eqz v4, :cond_3

    const-string v2, "fold"

    goto :goto_2

    :cond_3
    if-eqz v5, :cond_4

    const-string v2, "flip"

    goto :goto_2

    :cond_4
    const-string v2, "normal"

    :goto_2
    sput-object v2, Lcom/android/server/wm/OneTrackRotationHelper;->DEVICE_TYPE:Ljava/lang/String;

    .line 103
    if-nez v4, :cond_5

    if-nez v1, :cond_5

    if-eqz v5, :cond_6

    :cond_5
    move v0, v3

    :cond_6
    sput-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    .line 113
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->MIN_TIME:F

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onetrack-rotation: enable = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OneTrackRotationHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    return-void
.end method

.method private castToSting(I)Ljava/lang/String;
    .locals 1
    .param p1, "direction"    # I

    .line 552
    packed-switch p1, :pswitch_data_0

    .line 562
    const-string/jumbo v0, "unknown"

    return-object v0

    .line 560
    :pswitch_0
    const-string v0, "right"

    return-object v0

    .line 558
    :pswitch_1
    const-string v0, "down"

    return-object v0

    .line 556
    :pswitch_2
    const-string v0, "left"

    return-object v0

    .line 554
    :pswitch_3
    const-string/jumbo v0, "up"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getDisplayRotation()I
    .locals 1

    .line 193
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    if-nez v0, :cond_0

    .line 194
    const/4 v0, 0x0

    return v0

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mWm:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method public static declared-synchronized getInstance()Lcom/android/server/wm/OneTrackRotationHelper;
    .locals 2

    const-class v0, Lcom/android/server/wm/OneTrackRotationHelper;

    monitor-enter v0

    .line 116
    :try_start_0
    sget-object v1, Lcom/android/server/wm/OneTrackRotationHelper;->sInstance:Lcom/android/server/wm/OneTrackRotationHelper;

    if-nez v1, :cond_0

    .line 117
    new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-direct {v1}, Lcom/android/server/wm/OneTrackRotationHelper;-><init>()V

    sput-object v1, Lcom/android/server/wm/OneTrackRotationHelper;->sInstance:Lcom/android/server/wm/OneTrackRotationHelper;

    .line 119
    :cond_0
    sget-object v1, Lcom/android/server/wm/OneTrackRotationHelper;->sInstance:Lcom/android/server/wm/OneTrackRotationHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 115
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private getKeyguardLocked()Z
    .locals 1

    .line 209
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    if-nez v0, :cond_0

    .line 210
    const/4 v0, 0x0

    return v0

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mKm:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    return v0
.end method

.method private getScreenState()Z
    .locals 1

    .line 201
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    if-nez v0, :cond_0

    .line 202
    const/4 v0, 0x0

    return v0

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mPm:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    return v0
.end method

.method private getTopAppName()Ljava/lang/String;
    .locals 4

    .line 225
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    const-string v1, ""

    if-nez v0, :cond_0

    .line 226
    return-object v1

    .line 230
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mAm:Landroid/app/ActivityManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 231
    .local v0, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_1

    .line 232
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 234
    :cond_1
    return-object v1

    .line 236
    .end local v0    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTopAppName e= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OneTrackRotationHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    return-object v1
.end method

.method private initAllListeners()V
    .locals 6

    .line 275
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    const-string v1, "OneTrackRotationHelper"

    if-nez v0, :cond_0

    .line 276
    const-string v0, "initAllListeners mContext = null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    return-void

    .line 281
    :cond_0
    sget-boolean v2, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z

    if-eqz v2, :cond_3

    .line 282
    :cond_1
    const-class v2, Landroid/hardware/devicestate/DeviceStateManager;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/devicestate/DeviceStateManager;

    .line 283
    .local v0, "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
    if-eqz v0, :cond_2

    .line 284
    new-instance v2, Landroid/os/HandlerExecutor;

    iget-object v3, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, v3}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V

    new-instance v3, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;

    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    new-instance v5, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda2;

    invoke-direct {v5, p0}, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;)V

    invoke-direct {v3, v4, v5}, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;-><init>(Landroid/content/Context;Ljava/util/function/Consumer;)V

    invoke-virtual {v0, v2, v3}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V

    goto :goto_0

    .line 287
    :cond_2
    const-string v2, "deviceStateManager == null"

    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    .end local v0    # "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
    :cond_3
    :goto_0
    new-instance v0, Lcom/android/server/wm/OneTrackRotationHelper$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/OneTrackRotationHelper$2;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;)V

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mScreenStateReceiver:Landroid/content/BroadcastReceiver;

    .line 314
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 315
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 316
    const-string v2, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 317
    const-string v2, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 318
    const-string v2, "android.intent.action.REBOOT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 320
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mScreenStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    nop

    .line 326
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    .line 327
    const-string v2, "OneTrackRotationHelper init successfully"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    return-void

    .line 321
    :catch_0
    move-exception v2

    .line 322
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initAllListeners e = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    return-void
.end method

.method private initOneTrackRotationThread()V
    .locals 2

    .line 243
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "OneTrackRotationThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mThread:Landroid/os/HandlerThread;

    .line 244
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 245
    new-instance v0, Lcom/android/server/wm/OneTrackRotationHelper$1;

    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/OneTrackRotationHelper$1;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    .line 272
    return-void
.end method

.method private initializeData()V
    .locals 8

    .line 162
    const-string v0, ""

    .line 163
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mAm:Landroid/app/ActivityManager;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    .line 165
    :try_start_0
    invoke-virtual {v1, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 166
    .local v1, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 167
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v4

    .line 171
    .end local v1    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_0
    goto :goto_0

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initializeData packageName e= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "OneTrackRotationHelper"

    invoke-static {v5, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    const/4 v1, 0x0

    .line 175
    .local v1, "screenState":Z
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mPm:Landroid/os/PowerManager;

    if-eqz v4, :cond_3

    iget-object v5, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mKm:Landroid/app/KeyguardManager;

    if-eqz v5, :cond_3

    .line 176
    invoke-virtual {v4}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mKm:Landroid/app/KeyguardManager;

    invoke-virtual {v4}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v3

    goto :goto_1

    :cond_2
    move v4, v2

    :goto_1
    move v1, v4

    .line 179
    :cond_3
    const/4 v4, 0x0

    .line 180
    .local v4, "rotation":I
    iget-object v5, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mWm:Landroid/view/WindowManager;

    if-eqz v5, :cond_4

    .line 181
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getRotation()I

    .line 184
    :cond_4
    const/4 v5, 0x0

    .line 185
    .local v5, "folded":Z
    sget-boolean v6, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z

    if-nez v6, :cond_5

    sget-boolean v6, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z

    if-eqz v6, :cond_7

    :cond_5
    iget-object v6, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    if-eqz v6, :cond_7

    .line 186
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "device_posture"

    invoke-static {v6, v7, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v3, :cond_6

    move v2, v3

    :cond_6
    move v5, v2

    .line 189
    :cond_7
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-virtual {v2, v0, v1, v5, v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->init(Ljava/lang/String;ZZI)V

    .line 190
    return-void
.end method

.method private isReportXiaomiServer()Z
    .locals 3

    .line 435
    const-string v0, "ro.miui.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 436
    .local v0, "region":Ljava/lang/String;
    sget-boolean v1, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 437
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "the region is :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OneTrackRotationHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_0
    const-string v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "RU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method private synthetic lambda$trackScreenData$0(ILjava/lang/String;)V
    .locals 0
    .param p1, "wakefulness"    # I
    .param p2, "details"    # Ljava/lang/String;

    .line 447
    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->sendOneTrackForScreenData(ILjava/lang/String;)V

    .line 448
    return-void
.end method

.method private prepareFinalSendingInToday()V
    .locals 10

    .line 374
    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 375
    const-string v0, "OneTrackRotationHelper"

    const-string v1, "prepareFinalSendingInToday"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 381
    .local v0, "now":J
    invoke-static {}, Ljava/time/LocalDate;->now()Ljava/time/LocalDate;

    move-result-object v2

    .line 382
    .local v2, "localDate":Ljava/time/LocalDate;
    const-wide/16 v3, 0x1

    invoke-virtual {v2, v3, v4}, Ljava/time/LocalDate;->plusDays(J)Ljava/time/LocalDate;

    move-result-object v2

    .line 383
    invoke-virtual {v2}, Ljava/time/LocalDate;->getYear()I

    move-result v3

    invoke-virtual {v2}, Ljava/time/LocalDate;->getMonth()Ljava/time/Month;

    move-result-object v4

    invoke-virtual {v2}, Ljava/time/LocalDate;->getDayOfMonth()I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Ljava/time/LocalDateTime;->of(ILjava/time/Month;IIII)Ljava/time/LocalDateTime;

    move-result-object v3

    .line 384
    .local v3, "dateTime":Ljava/time/LocalDateTime;
    invoke-static {}, Ljava/time/ZoneId;->systemDefault()Ljava/time/ZoneId;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/time/LocalDateTime;->atZone(Ljava/time/ZoneId;)Ljava/time/ZonedDateTime;

    move-result-object v4

    invoke-virtual {v4}, Ljava/time/ZonedDateTime;->toInstant()Ljava/time/Instant;

    move-result-object v4

    invoke-virtual {v4}, Ljava/time/Instant;->toEpochMilli()J

    move-result-wide v4

    .line 386
    .local v4, "nextDay":J
    iget-object v6, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v6, v0, v1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$msetToday(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;J)V

    .line 387
    iget-object v6, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x6

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 388
    iget-object v6, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    sub-long v8, v4, v0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 389
    return-void
.end method

.method private prepareNextSending()V
    .locals 4

    .line 362
    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 363
    const-string v0, "OneTrackRotationHelper"

    const-string v1, "prepareNextSending"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 367
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    sget-wide v2, Lcom/android/server/wm/OneTrackRotationHelper;->mReportInterval:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 368
    return-void
.end method

.method private recordTimeForFlip(F)V
    .locals 4
    .param p1, "duration"    # F

    .line 467
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "screen_direction_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetmDisplayRotation(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 468
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetfilpUsageDate(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Ljava/util/HashMap;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 469
    .local v1, "existingDuration":F
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetfilpUsageDate(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Ljava/util/HashMap;

    move-result-object v2

    add-float v3, v1, p1

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    return-void
.end method

.method private reportOneTrack(Ljava/util/ArrayList;I)V
    .locals 18
    .param p2, "reportDate"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 393
    .local p1, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    const-string v4, "OneTrackRotationHelper"

    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/android/server/wm/OneTrackRotationHelper;->isReportXiaomiServer()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    const-string v5, "report_date"

    const-string v6, "model_type"

    const-string v7, "all_app_usage_time"

    const-string v8, "866.2.1.1.28680"

    const-string/jumbo v9, "tip"

    const-string v10, "android"

    const-string v11, "PACKAGE"

    const-string/jumbo v12, "screen_use_duration"

    const-string v13, "EVENT_NAME"

    const-string v14, "31000000779"

    const-string v15, "APP_ID"

    move-object/from16 v16, v4

    const-string v4, "com.miui.analytics"

    const-string v1, "onetrack.action.TRACK_EVENT"

    if-eqz v0, :cond_0

    .line 394
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 395
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    invoke-virtual {v0, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 397
    invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 398
    invoke-virtual {v0, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 399
    invoke-virtual {v0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 400
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 401
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v1, v7, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 402
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 403
    sget-object v4, Lcom/android/server/wm/OneTrackRotationHelper;->DEVICE_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 404
    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 405
    move-object/from16 v4, p0

    :try_start_2
    iget-object v5, v4, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    sget-object v6, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v5, v0, v6}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 406
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "params":Landroid/os/Bundle;
    goto :goto_0

    .line 429
    :catch_0
    move-exception v0

    goto :goto_2

    .line 408
    :cond_0
    move-object v0, v1

    move-object/from16 v1, p0

    :try_start_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 409
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 410
    invoke-virtual {v0, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 411
    invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 412
    invoke-virtual {v0, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 413
    const-string v1, "PROJECT_ID"

    const-string/jumbo v4, "thirdappadaptation"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 414
    const-string v1, "TOPIC"

    const-string/jumbo v4, "topic_ods_pubsub_event_di_31000000779"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 415
    const-string v1, "PRIVATE_KEY_ID"

    const-string v4, "9f3945ec5765512b0ca43029da3f62aa93613c93"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 416
    invoke-virtual {v0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 417
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 418
    .restart local v1    # "params":Landroid/os/Bundle;
    invoke-virtual {v1, v7, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 419
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 420
    sget-object v4, Lcom/android/server/wm/OneTrackRotationHelper;->DEVICE_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 422
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 423
    move-object/from16 v4, p0

    :try_start_4
    iget-object v5, v4, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    sget-object v6, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v5, v0, v6}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 426
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "params":Landroid/os/Bundle;
    :goto_0
    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 427
    const-string v0, "reportOneTrack"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-object/from16 v1, v16

    :try_start_5
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 429
    :catch_1
    move-exception v0

    goto :goto_3

    .line 431
    :cond_1
    :goto_1
    goto :goto_4

    .line 429
    :catch_2
    move-exception v0

    move-object/from16 v4, p0

    :goto_2
    move-object/from16 v1, v16

    goto :goto_3

    :catch_3
    move-exception v0

    move-object/from16 v17, v4

    move-object v4, v1

    move-object/from16 v1, v17

    .line 430
    .local v0, "e":Ljava/lang/Exception;
    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reportOneTrack e = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    return-void
.end method

.method private reportOneTrackForScreenData()V
    .locals 8

    .line 473
    iget-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J

    sub-long/2addr v4, v6

    long-to-float v0, v4

    const/high16 v4, 0x447a0000    # 1000.0f

    div-float/2addr v0, v4

    goto :goto_0

    :cond_0
    move v0, v1

    .line 474
    .local v0, "duration":F
    :goto_0
    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    const v1, 0x3e4ccccd    # 0.2f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    .line 475
    sget-boolean v1, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z

    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetmFolded(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 476
    iget-object v4, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v4}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetoneTrackRotationHelper(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Lcom/android/server/wm/OneTrackRotationHelper;

    move-result-object v4

    invoke-direct {v4, v0}, Lcom/android/server/wm/OneTrackRotationHelper;->recordTimeForFlip(F)V

    .line 478
    :cond_1
    invoke-direct {p0, v0}, Lcom/android/server/wm/OneTrackRotationHelper;->reportOneTrackForScreenData(F)V

    .line 479
    if-eqz v1, :cond_2

    .line 480
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->resetFilpUsageDate()V

    .line 483
    :cond_2
    iput-wide v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J

    .line 484
    return-void
.end method

.method private reportOneTrackForScreenData(F)V
    .locals 20
    .param p1, "duration"    # F

    .line 488
    move-object/from16 v1, p0

    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/android/server/wm/OneTrackRotationHelper;->isReportXiaomiServer()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const-string v2, "_time"

    const-string/jumbo v4, "use_duration"

    const-string/jumbo v5, "\u5916\u5c4f"

    const-string/jumbo v6, "\u5185\u5c4f"

    const-string v7, "model_type"

    const-string v8, "866.2.1.1.32924"

    const-string/jumbo v9, "tip"

    const-string v10, "android"

    const-string v11, "PACKAGE"

    const-string/jumbo v12, "screen_use_duration"

    const-string v13, "EVENT_NAME"

    const-string v14, "31000000779"

    const-string v15, "APP_ID"

    const-string v3, "com.miui.analytics"

    move-object/from16 v16, v4

    const-string v4, "onetrack.action.TRACK_EVENT"

    move-object/from16 v17, v2

    const-string v2, "screen_direction_"

    move-object/from16 v18, v2

    const-string/jumbo v2, "screen_type"

    if-eqz v0, :cond_5

    .line 489
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 490
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 491
    invoke-virtual {v0, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 492
    invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 493
    invoke-virtual {v0, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 494
    invoke-virtual {v0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 495
    sget-object v3, Lcom/android/server/wm/OneTrackRotationHelper;->DEVICE_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    sget-boolean v3, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z

    if-nez v3, :cond_0

    sget-boolean v3, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z

    if-eqz v3, :cond_2

    .line 497
    :cond_0
    iget-object v3, v1, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v3}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetmFolded(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 498
    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 500
    :cond_1
    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 503
    :cond_2
    :goto_0
    sget-boolean v2, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetmFolded(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 504
    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    filled-new-array {v5, v4, v3, v2}, [I

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 505
    .local v2, "directions":[I
    const/4 v3, 0x0

    .line 506
    .end local p1    # "duration":F
    .local v3, "duration":F
    :try_start_2
    array-length v4, v2

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_4

    aget v6, v2, v5

    .line 507
    .local v6, "direction":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v8, v18

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-direct {v1, v6}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 508
    .local v7, "key":Ljava/lang/String;
    iget-object v9, v1, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v9}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetfilpUsageDate(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 509
    .local v9, "time":F
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {v1, v6}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v11, v17

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 510
    .local v10, "timeKey":Ljava/lang/String;
    invoke-virtual {v0, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 511
    add-float/2addr v3, v9

    .line 506
    .end local v6    # "direction":I
    .end local v7    # "key":Ljava/lang/String;
    .end local v9    # "time":F
    .end local v10    # "timeKey":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    move-object/from16 v18, v8

    move-object/from16 v17, v11

    goto :goto_1

    .line 514
    .end local v2    # "directions":[I
    .end local v3    # "duration":F
    .restart local p1    # "duration":F
    :cond_3
    move/from16 v3, p1

    .end local p1    # "duration":F
    .restart local v3    # "duration":F
    :cond_4
    move-object/from16 v2, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 515
    iget-object v2, v1, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v0, v4}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 516
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    goto/16 :goto_4

    .line 517
    .end local v3    # "duration":F
    .restart local p1    # "duration":F
    :cond_5
    move-object/from16 v19, v16

    move-object/from16 v0, v18

    :try_start_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 518
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 519
    invoke-virtual {v0, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 520
    invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 521
    invoke-virtual {v0, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 522
    invoke-virtual {v0, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 523
    sget-object v3, Lcom/android/server/wm/OneTrackRotationHelper;->DEVICE_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 524
    sget-boolean v3, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FOLD:Z

    if-nez v3, :cond_6

    sget-boolean v3, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z

    if-eqz v3, :cond_8

    .line 525
    :cond_6
    iget-object v3, v1, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v3}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetmFolded(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 526
    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 528
    :cond_7
    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 531
    :cond_8
    :goto_2
    sget-boolean v2, Lcom/android/server/wm/OneTrackRotationHelper;->IS_FLIP:Z

    if-eqz v2, :cond_9

    iget-object v2, v1, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetmFolded(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 532
    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    filled-new-array {v5, v4, v3, v2}, [I

    move-result-object v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 533
    .restart local v2    # "directions":[I
    const/4 v3, 0x0

    .line 534
    .end local p1    # "duration":F
    .restart local v3    # "duration":F
    :try_start_4
    array-length v4, v2

    :goto_3
    if-ge v5, v4, :cond_a

    aget v6, v2, v5

    .line 535
    .restart local v6    # "direction":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v8, v18

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-direct {v1, v6}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 536
    .restart local v7    # "key":Ljava/lang/String;
    iget-object v9, v1, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v9}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetfilpUsageDate(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 537
    .restart local v9    # "time":F
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {v1, v6}, Lcom/android/server/wm/OneTrackRotationHelper;->castToSting(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v11, v17

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 538
    .restart local v10    # "timeKey":Ljava/lang/String;
    invoke-virtual {v0, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 539
    add-float/2addr v3, v9

    .line 534
    .end local v6    # "direction":I
    .end local v7    # "key":Ljava/lang/String;
    .end local v9    # "time":F
    .end local v10    # "timeKey":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    move-object/from16 v18, v8

    move-object/from16 v17, v11

    goto :goto_3

    .line 542
    .end local v2    # "directions":[I
    .end local v3    # "duration":F
    .restart local p1    # "duration":F
    :cond_9
    move/from16 v3, p1

    .end local p1    # "duration":F
    .restart local v3    # "duration":F
    :cond_a
    move-object/from16 v2, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 543
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 544
    iget-object v2, v1, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v0, v4}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 548
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_4
    goto :goto_6

    .line 546
    :catch_0
    move-exception v0

    goto :goto_5

    .end local v3    # "duration":F
    .restart local p1    # "duration":F
    :catch_1
    move-exception v0

    move/from16 v3, p1

    .line 547
    .end local p1    # "duration":F
    .local v0, "e":Ljava/lang/Exception;
    .restart local v3    # "duration":F
    :goto_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportOneTrackForScreenData e = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "OneTrackRotationHelper"

    invoke-static {v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_6
    return-void
.end method

.method private resetFilpUsageDate()V
    .locals 4

    .line 567
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetfilpUsageDate(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 568
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-static {v2}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$fgetfilpUsageDate(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)Ljava/util/HashMap;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    .end local v1    # "key":Ljava/lang/String;
    goto :goto_0

    .line 570
    :cond_0
    return-void
.end method

.method private sendOneTrackForScreenData(ILjava/lang/String;)V
    .locals 2
    .param p1, "wakefulness"    # I
    .param p2, "details"    # Ljava/lang/String;

    .line 453
    const/4 v0, 0x1

    if-eqz p2, :cond_0

    const-string v1, "com.android.systemui:NOTIFICATION"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 454
    sput-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsScreenOnNotification:Z

    .line 455
    return-void

    .line 457
    :cond_0
    if-ne p1, v0, :cond_1

    .line 458
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->currentTimeMillis:J

    .line 460
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 461
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsScreenOnNotification:Z

    .line 462
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->reportOneTrackForScreenData()V

    .line 464
    :cond_2
    return-void
.end method


# virtual methods
.method public init()V
    .locals 4

    .line 127
    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z

    if-nez v0, :cond_0

    .line 128
    return-void

    .line 131
    :cond_0
    const-string v0, "onetrack-rotation init"

    const-string v1, "OneTrackRotationHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    .line 134
    if-nez v0, :cond_1

    .line 135
    const-string v0, "init OneTrackRotationHelper mContext = null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    return-void

    .line 139
    :cond_1
    new-instance v0, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    invoke-direct {v0, p0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;)V

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mRotationStateMachine:Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    .line 141
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mAm:Landroid/app/ActivityManager;

    .line 142
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mWm:Landroid/view/WindowManager;

    .line 143
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mPm:Landroid/os/PowerManager;

    .line 144
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mContext:Landroid/content/Context;

    const-string v2, "keyguard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mKm:Landroid/app/KeyguardManager;

    .line 145
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mAm:Landroid/app/ActivityManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mWm:Landroid/view/WindowManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mPm:Landroid/os/PowerManager;

    if-eqz v2, :cond_3

    if-nez v0, :cond_2

    goto :goto_0

    .line 154
    :cond_2
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->initOneTrackRotationThread()V

    .line 156
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->initializeData()V

    .line 158
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 159
    return-void

    .line 146
    :cond_3
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSystemService failed service:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 147
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mAm:Landroid/app/ActivityManager;

    const-string v3, ""

    if-nez v2, :cond_4

    const-string v2, " AM"

    goto :goto_1

    :cond_4
    move-object v2, v3

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 148
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mWm:Landroid/view/WindowManager;

    if-nez v2, :cond_5

    const-string v2, " WM"

    goto :goto_2

    :cond_5
    move-object v2, v3

    :goto_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 149
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mPm:Landroid/os/PowerManager;

    if-nez v2, :cond_6

    const-string v2, " PM"

    goto :goto_3

    :cond_6
    move-object v2, v3

    :goto_3
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 150
    iget-object v2, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mKm:Landroid/app/KeyguardManager;

    if-nez v2, :cond_7

    const-string v3, " KM"

    :cond_7
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    return-void
.end method

.method public isScreenRealUnlocked()Z
    .locals 2

    .line 217
    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 218
    return v1

    .line 221
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->getScreenState()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/wm/OneTrackRotationHelper;->getKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public reportDeviceFolded(Z)V
    .locals 3
    .param p1, "folded"    # Z

    .line 353
    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 357
    .local v0, "message":Landroid/os/Message;
    new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 358
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 359
    return-void

    .line 354
    .end local v0    # "message":Landroid/os/Message;
    :cond_1
    :goto_0
    return-void
.end method

.method public reportPackageForeground(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 342
    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->lastForegroundPkg:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 345
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 346
    .local v0, "message":Landroid/os/Message;
    new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;

    invoke-direct {v1, p1}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 347
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 348
    iput-object p1, p0, Lcom/android/server/wm/OneTrackRotationHelper;->lastForegroundPkg:Ljava/lang/String;

    .line 350
    .end local v0    # "message":Landroid/os/Message;
    :cond_1
    return-void

    .line 343
    :cond_2
    :goto_0
    return-void
.end method

.method public reportRotationChanged(II)V
    .locals 2
    .param p1, "displayId"    # I
    .param p2, "rotation"    # I

    .line 331
    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 335
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 336
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 337
    new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;

    invoke-direct {v1}, Lcom/android/server/wm/OneTrackRotationHelper$AppUsageMessageObj;-><init>()V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 338
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 339
    return-void

    .line 332
    .end local v0    # "message":Landroid/os/Message;
    :cond_1
    :goto_0
    return-void
.end method

.method public trackScreenData(ILjava/lang/String;)V
    .locals 2
    .param p1, "wakefulness"    # I
    .param p2, "details"    # Ljava/lang/String;

    .line 443
    sget-boolean v0, Lcom/android/server/wm/OneTrackRotationHelper;->ENABLE_TRACK:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mIsInit:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/OneTrackRotationHelper;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 449
    return-void

    .line 444
    :cond_1
    :goto_0
    return-void
.end method
