public class com.android.server.wm.PreloadLifecycle {
	 /* .source "PreloadLifecycle.java" */
	 /* # static fields */
	 public static final Integer NO_PRELOAD;
	 public static final Integer ON_FREEZE;
	 public static final Integer ON_STOP;
	 public static final Integer PRELOAD_SOON;
	 public static final Integer PRE_FREEZE;
	 public static final Integer PRE_KILL;
	 public static final Integer PRE_STOP;
	 public static final Integer START_PRELOAD;
	 /* # instance fields */
	 protected Boolean mAlreadyPreloaded;
	 protected miui.process.LifecycleConfig mConfig;
	 private Integer mDisplayId;
	 protected Long mFreezeTimeout;
	 protected Boolean mIgnoreMemory;
	 protected Long mKillTimeout;
	 protected java.lang.String mPackageName;
	 protected Long mStopTimeout;
	 protected Integer mUid;
	 /* # direct methods */
	 public com.android.server.wm.PreloadLifecycle ( ) {
		 /* .locals 2 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .line 31 */
		 int v0 = 0; // const/4 v0, 0x0
		 int v1 = 0; // const/4 v1, 0x0
		 /* invoke-direct {p0, v0, p1, v1}, Lcom/android/server/wm/PreloadLifecycle;-><init>(ZLjava/lang/String;Lmiui/process/LifecycleConfig;)V */
		 /* .line 32 */
		 return;
	 } // .end method
	 public com.android.server.wm.PreloadLifecycle ( ) {
		 /* .locals 0 */
		 /* .param p1, "ignoreMemory" # Z */
		 /* .param p2, "packageName" # Ljava/lang/String; */
		 /* .param p3, "config" # Lmiui/process/LifecycleConfig; */
		 /* .line 35 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 36 */
		 /* iput-boolean p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mIgnoreMemory:Z */
		 /* .line 37 */
		 this.mPackageName = p2;
		 /* .line 38 */
		 this.mConfig = p3;
		 /* .line 39 */
		 (( com.android.server.wm.PreloadLifecycle ) p0 ).initTimeout ( p3 ); // invoke-virtual {p0, p3}, Lcom/android/server/wm/PreloadLifecycle;->initTimeout(Lmiui/process/LifecycleConfig;)V
		 /* .line 40 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean equals ( java.lang.Object p0 ) {
		 /* .locals 4 */
		 /* .param p1, "obj" # Ljava/lang/Object; */
		 /* .line 138 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* if-nez p1, :cond_0 */
		 /* .line 139 */
		 /* .line 142 */
	 } // :cond_0
	 /* move-object v1, p1 */
	 /* check-cast v1, Lcom/android/server/wm/PreloadLifecycle; */
	 /* .line 143 */
	 /* .local v1, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
	 /* iget v2, p0, Lcom/android/server/wm/PreloadLifecycle;->mUid:I */
	 /* iget v3, v1, Lcom/android/server/wm/PreloadLifecycle;->mUid:I */
	 /* if-eq v2, v3, :cond_1 */
	 /* .line 144 */
	 /* .line 147 */
} // :cond_1
v2 = this.mPackageName;
/* if-nez v2, :cond_2 */
/* .line 148 */
/* .line 151 */
} // :cond_2
v3 = this.mPackageName;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_3 */
/* .line 152 */
/* .line 155 */
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
} // .end method
public miui.process.LifecycleConfig getConfig ( ) {
/* .locals 1 */
/* .line 117 */
v0 = this.mConfig;
} // .end method
public Integer getDisplayId ( ) {
/* .locals 1 */
/* .line 129 */
/* iget v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mDisplayId:I */
} // .end method
public Long getFreezeTimeout ( ) {
/* .locals 2 */
/* .line 61 */
/* iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mFreezeTimeout:J */
/* return-wide v0 */
} // .end method
public Long getKillTimeout ( ) {
/* .locals 2 */
/* .line 69 */
/* iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mKillTimeout:J */
/* return-wide v0 */
} // .end method
public java.lang.String getPackageName ( ) {
/* .locals 1 */
/* .line 85 */
v0 = this.mPackageName;
} // .end method
public Long getPreloadNextTimeout ( ) {
/* .locals 4 */
/* .line 49 */
/* iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mFreezeTimeout:J */
/* const-wide/16 v2, 0xbb8 */
java.lang.Math .max ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public getPreloadSchedAffinity ( ) {
/* .locals 1 */
/* .line 93 */
v0 = this.mConfig;
(( miui.process.LifecycleConfig ) v0 ).getSchedAffinity ( ); // invoke-virtual {v0}, Lmiui/process/LifecycleConfig;->getSchedAffinity()[I
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mConfig;
/* .line 94 */
(( miui.process.LifecycleConfig ) v0 ).getSchedAffinity ( ); // invoke-virtual {v0}, Lmiui/process/LifecycleConfig;->getSchedAffinity()[I
/* array-length v0, v0 */
/* if-nez v0, :cond_0 */
/* .line 97 */
} // :cond_0
v0 = this.mConfig;
(( miui.process.LifecycleConfig ) v0 ).getSchedAffinity ( ); // invoke-virtual {v0}, Lmiui/process/LifecycleConfig;->getSchedAffinity()[I
/* .line 95 */
} // :cond_1
} // :goto_0
v0 = com.android.server.am.PreloadAppControllerImpl.sPreloadSchedAffinity;
} // .end method
public Integer getPreloadType ( ) {
/* .locals 1 */
/* .line 125 */
v0 = this.mConfig;
/* if-nez v0, :cond_0 */
int v0 = -1; // const/4 v0, -0x1
} // :cond_0
v0 = (( miui.process.LifecycleConfig ) v0 ).getType ( ); // invoke-virtual {v0}, Lmiui/process/LifecycleConfig;->getType()I
} // :goto_0
} // .end method
public Long getStopTimeout ( ) {
/* .locals 2 */
/* .line 53 */
/* iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J */
/* return-wide v0 */
} // .end method
public Integer getUid ( ) {
/* .locals 1 */
/* .line 77 */
/* iget v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mUid:I */
} // .end method
public void initTimeout ( miui.process.LifecycleConfig p0 ) {
/* .locals 4 */
/* .param p1, "config" # Lmiui/process/LifecycleConfig; */
/* .line 43 */
(( miui.process.LifecycleConfig ) p1 ).getStopTimeout ( ); // invoke-virtual {p1}, Lmiui/process/LifecycleConfig;->getStopTimeout()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J */
/* .line 44 */
(( miui.process.LifecycleConfig ) p1 ).getKillTimeout ( ); // invoke-virtual {p1}, Lmiui/process/LifecycleConfig;->getKillTimeout()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mKillTimeout:J */
/* .line 45 */
/* iget-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J */
/* const-wide/16 v2, 0x1f4 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mFreezeTimeout:J */
/* .line 46 */
return;
} // .end method
public Boolean isAlreadyPreloaded ( ) {
/* .locals 1 */
/* .line 109 */
/* iget-boolean v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mAlreadyPreloaded:Z */
} // .end method
public Boolean isIgnoreMemory ( ) {
/* .locals 1 */
/* .line 101 */
/* iget-boolean v0, p0, Lcom/android/server/wm/PreloadLifecycle;->mIgnoreMemory:Z */
} // .end method
public void setAlreadyPreloaded ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "alreadyPreloaded" # Z */
/* .line 113 */
/* iput-boolean p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mAlreadyPreloaded:Z */
/* .line 114 */
return;
} // .end method
public void setConfig ( miui.process.LifecycleConfig p0 ) {
/* .locals 0 */
/* .param p1, "mConfig" # Lmiui/process/LifecycleConfig; */
/* .line 121 */
this.mConfig = p1;
/* .line 122 */
return;
} // .end method
public void setDisplayId ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 133 */
/* iput p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mDisplayId:I */
/* .line 134 */
return;
} // .end method
public void setFreezeTimeout ( Long p0 ) {
/* .locals 0 */
/* .param p1, "mFreezeTimeout" # J */
/* .line 65 */
/* iput-wide p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mFreezeTimeout:J */
/* .line 66 */
return;
} // .end method
public void setIgnoreMemory ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "ignoreMemory" # Z */
/* .line 105 */
/* iput-boolean p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mIgnoreMemory:Z */
/* .line 106 */
return;
} // .end method
public void setKillTimeout ( Long p0 ) {
/* .locals 0 */
/* .param p1, "mKillTimeout" # J */
/* .line 73 */
/* iput-wide p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mKillTimeout:J */
/* .line 74 */
return;
} // .end method
public void setPackageName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mPackageName" # Ljava/lang/String; */
/* .line 89 */
this.mPackageName = p1;
/* .line 90 */
return;
} // .end method
public void setStopTimeout ( Long p0 ) {
/* .locals 0 */
/* .param p1, "mStopTimeout" # J */
/* .line 57 */
/* iput-wide p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J */
/* .line 58 */
return;
} // .end method
public void setUid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mUid" # I */
/* .line 81 */
/* iput p1, p0, Lcom/android/server/wm/PreloadLifecycle;->mUid:I */
/* .line 82 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 160 */
/* new-instance v0, Ljava/lang/StringBuffer; */
/* invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V */
/* .line 161 */
/* .local v0, "sb":Ljava/lang/StringBuffer; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mPackageName="; // const-string v2, "mPackageName="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mPackageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 162 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = ",mStopTimeout="; // const-string v2, ",mStopTimeout="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/wm/PreloadLifecycle;->mStopTimeout:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 163 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = ",mKillTimeout="; // const-string v2, ",mKillTimeout="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/wm/PreloadLifecycle;->mKillTimeout:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 164 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = ",type="; // const-string v2, ",type="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.android.server.wm.PreloadLifecycle ) p0 ).getPreloadType ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 165 */
(( java.lang.StringBuffer ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
} // .end method
