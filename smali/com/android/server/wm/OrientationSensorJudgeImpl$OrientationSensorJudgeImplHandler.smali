.class final Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler;
.super Landroid/os/Handler;
.source "OrientationSensorJudgeImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/OrientationSensorJudgeImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OrientationSensorJudgeImplHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .line 199
    iput-object p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    .line 200
    const/4 p1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p2, p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 201
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .line 205
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 210
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    invoke-static {v0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$mupdateForegroundApp(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V

    .line 211
    goto :goto_0

    .line 207
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    invoke-static {v0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$mupdateForegroundAppSync(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V

    .line 208
    nop

    .line 215
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
