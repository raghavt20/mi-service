public class com.android.server.wm.MiuiMirrorDragState {
	 /* .source "MiuiMirrorDragState.java" */
	 /* # static fields */
	 private static final Integer DRAG_FLAGS_URI_ACCESS;
	 private static final Integer DRAG_FLAGS_URI_PERMISSIONS;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 java.util.ArrayList mBroadcastDisplayIds;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
Boolean mCrossProfileCopyAllowed;
com.android.server.wm.DisplayContent mCurrentDisplayContent;
Integer mCurrentDisplayId;
Float mCurrentX;
Float mCurrentY;
android.content.ClipData mData;
android.content.ClipDescription mDataDescription;
final com.android.server.wm.MiuiMirrorDragDropController mDragDropController;
Boolean mDragInProgress;
Boolean mDragResult;
Integer mFlags;
private Boolean mIsClosing;
android.os.IBinder mLocalWin;
private com.xiaomi.mirror.service.MirrorServiceInternal mMirrorServiceInternal;
java.util.ArrayList mNotifiedWindows;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/wm/WindowState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
Integer mPid;
final com.android.server.wm.WindowManagerService mService;
Integer mSourceDisplayId;
Integer mSourceUserId;
com.android.server.wm.WindowState mTargetWindow;
android.os.IBinder mToken;
Integer mUid;
/* # direct methods */
public static void $r8$lambda$PizEWj1j54G_mkmKU2MvlZkxsnA ( com.android.server.wm.MiuiMirrorDragState p0, Float p1, Float p2, com.android.server.wm.WindowState p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiMirrorDragState;->lambda$broadcastDragStartedLocked$0(FFLcom/android/server/wm/WindowState;)V */
return;
} // .end method
 com.android.server.wm.MiuiMirrorDragState ( ) {
/* .locals 1 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "controller" # Lcom/android/server/wm/MiuiMirrorDragDropController; */
/* .param p3, "flags" # I */
/* .param p4, "localWin" # Landroid/os/IBinder; */
/* .line 68 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 59 */
com.xiaomi.mirror.service.MirrorServiceInternal .getInstance ( );
this.mMirrorServiceInternal = v0;
/* .line 69 */
this.mService = p1;
/* .line 70 */
this.mDragDropController = p2;
/* .line 71 */
/* iput p3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I */
/* .line 72 */
this.mLocalWin = p4;
/* .line 73 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mNotifiedWindows = v0;
/* .line 74 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mBroadcastDisplayIds = v0;
/* .line 75 */
return;
} // .end method
private void broadcastDragEndedLocked ( ) {
/* .locals 18 */
/* .line 144 */
/* move-object/from16 v1, p0 */
v2 = android.os.Process .myPid ( );
/* .line 146 */
/* .local v2, "myPid":I */
v0 = this.mNotifiedWindows;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v0 = } // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* move-object v4, v0 */
/* check-cast v4, Lcom/android/server/wm/WindowState; */
/* .line 147 */
/* .local v4, "ws":Lcom/android/server/wm/WindowState; */
int v0 = 0; // const/4 v0, 0x0
/* .line 148 */
/* .local v0, "x":F */
int v5 = 0; // const/4 v5, 0x0
/* .line 149 */
/* .local v5, "y":F */
/* iget-boolean v6, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z */
/* if-nez v6, :cond_0 */
v6 = this.mSession;
/* iget v6, v6, Lcom/android/server/wm/Session;->mPid:I */
/* iget v7, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mPid:I */
/* if-ne v6, v7, :cond_0 */
/* .line 151 */
/* iget v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F */
/* .line 152 */
/* iget v5, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F */
/* move/from16 v17, v5 */
/* move v5, v0 */
/* .line 154 */
} // :cond_0
/* move/from16 v17, v5 */
/* move v5, v0 */
} // .end local v0 # "x":F
/* .local v5, "x":F */
/* .local v17, "y":F */
} // :goto_1
int v6 = 4; // const/4 v6, 0x4
int v9 = 0; // const/4 v9, 0x0
int v10 = 0; // const/4 v10, 0x0
int v11 = 0; // const/4 v11, 0x0
int v12 = 0; // const/4 v12, 0x0
int v13 = 0; // const/4 v13, 0x0
int v14 = 0; // const/4 v14, 0x0
int v15 = 0; // const/4 v15, 0x0
/* iget-boolean v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z */
/* move v7, v5 */
/* move/from16 v8, v17 */
/* move/from16 v16, v0 */
/* invoke-static/range {v6 ..v16}, Landroid/view/DragEvent;->obtain(IFFFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Landroid/view/SurfaceControl;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent; */
/* .line 157 */
/* .local v6, "evt":Landroid/view/DragEvent; */
try { // :try_start_0
v0 = this.mClient;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 160 */
/* .line 158 */
/* :catch_0 */
/* move-exception v0 */
/* .line 159 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Unable to drag-end window "; // const-string v8, "Unable to drag-end window "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "MiuiMirrorDragState"; // const-string v8, "MiuiMirrorDragState"
android.util.Slog .w ( v8,v7 );
/* .line 163 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_2
v0 = this.mSession;
/* iget v0, v0, Lcom/android/server/wm/Session;->mPid:I */
/* if-eq v2, v0, :cond_1 */
/* .line 164 */
(( android.view.DragEvent ) v6 ).recycle ( ); // invoke-virtual {v6}, Landroid/view/DragEvent;->recycle()V
/* .line 166 */
} // .end local v4 # "ws":Lcom/android/server/wm/WindowState;
} // .end local v5 # "x":F
} // .end local v6 # "evt":Landroid/view/DragEvent;
} // .end local v17 # "y":F
} // :cond_1
/* .line 167 */
} // :cond_2
v0 = this.mNotifiedWindows;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 168 */
v0 = this.mBroadcastDisplayIds;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 169 */
return;
} // .end method
private Boolean isValidDropTarget ( com.android.server.wm.WindowState p0 ) {
/* .locals 3 */
/* .param p1, "targetWin" # Lcom/android/server/wm/WindowState; */
/* .line 200 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 201 */
/* .line 203 */
} // :cond_0
v1 = (( com.android.server.wm.WindowState ) p1 ).isPotentialDragTarget ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/WindowState;->isPotentialDragTarget(Z)Z
/* if-nez v1, :cond_1 */
/* .line 204 */
/* .line 206 */
} // :cond_1
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMirrorDragState;->targetWindowSupportsGlobalDrag(Lcom/android/server/wm/WindowState;)Z */
/* if-nez v1, :cond_3 */
/* .line 208 */
v1 = this.mLocalWin;
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = this.mClient;
/* if-eq v1, v2, :cond_3 */
/* .line 209 */
} // :cond_2
/* .line 213 */
} // :cond_3
/* iget-boolean v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCrossProfileCopyAllowed:Z */
/* if-nez v1, :cond_4 */
/* iget v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I */
/* .line 214 */
v2 = (( com.android.server.wm.WindowState ) p1 ).getOwningUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningUid()I
v2 = android.os.UserHandle .getUserId ( v2 );
/* if-ne v1, v2, :cond_5 */
} // :cond_4
int v0 = 1; // const/4 v0, 0x1
/* .line 213 */
} // :cond_5
} // .end method
private Boolean isWindowNotified ( com.android.server.wm.WindowState p0 ) {
/* .locals 2 */
/* .param p1, "newWin" # Lcom/android/server/wm/WindowState; */
/* .line 239 */
v0 = this.mNotifiedWindows;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/wm/WindowState; */
/* .line 240 */
/* .local v1, "ws":Lcom/android/server/wm/WindowState; */
/* if-ne v1, p1, :cond_0 */
/* .line 241 */
int v0 = 1; // const/4 v0, 0x1
/* .line 243 */
} // .end local v1 # "ws":Lcom/android/server/wm/WindowState;
} // :cond_0
/* .line 244 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void lambda$broadcastDragStartedLocked$0 ( Float p0, Float p1, com.android.server.wm.WindowState p2 ) { //synthethic
/* .locals 1 */
/* .param p1, "touchX" # F */
/* .param p2, "touchY" # F */
/* .param p3, "w" # Lcom/android/server/wm/WindowState; */
/* .line 138 */
v0 = this.mDataDescription;
/* invoke-direct {p0, p3, p1, p2, v0}, Lcom/android/server/wm/MiuiMirrorDragState;->sendDragStartedLocked(Lcom/android/server/wm/WindowState;FFLandroid/content/ClipDescription;)V */
/* .line 139 */
return;
} // .end method
private static android.view.DragEvent obtainDragEvent ( com.android.server.wm.WindowState p0, Integer p1, Float p2, Float p3, java.lang.Object p4, android.content.ClipDescription p5, android.content.ClipData p6, com.android.internal.view.IDragAndDropPermissions p7, Boolean p8 ) {
/* .locals 16 */
/* .param p0, "win" # Lcom/android/server/wm/WindowState; */
/* .param p1, "action" # I */
/* .param p2, "x" # F */
/* .param p3, "y" # F */
/* .param p4, "localState" # Ljava/lang/Object; */
/* .param p5, "description" # Landroid/content/ClipDescription; */
/* .param p6, "data" # Landroid/content/ClipData; */
/* .param p7, "dragAndDropPermissions" # Lcom/android/internal/view/IDragAndDropPermissions; */
/* .param p8, "result" # Z */
/* .line 371 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p2 */
v13 = (( com.android.server.wm.WindowState ) v0 ).translateToWindowX ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowState;->translateToWindowX(F)F
/* .line 372 */
/* .local v13, "winX":F */
/* move/from16 v14, p3 */
v15 = (( com.android.server.wm.WindowState ) v0 ).translateToWindowY ( v14 ); // invoke-virtual {v0, v14}, Lcom/android/server/wm/WindowState;->translateToWindowY(F)F
/* .line 373 */
/* .local v15, "winY":F */
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v10 = 0; // const/4 v10, 0x0
/* move/from16 v2, p1 */
/* move v3, v13 */
/* move v4, v15 */
/* move-object/from16 v7, p4 */
/* move-object/from16 v8, p5 */
/* move-object/from16 v9, p6 */
/* move-object/from16 v11, p7 */
/* move/from16 v12, p8 */
/* invoke-static/range {v2 ..v12}, Landroid/view/DragEvent;->obtain(IFFFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Landroid/view/SurfaceControl;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent; */
} // .end method
private void sendDragStartedLocked ( com.android.server.wm.WindowState p0, Float p1, Float p2, android.content.ClipDescription p3 ) {
/* .locals 10 */
/* .param p1, "newWin" # Lcom/android/server/wm/WindowState; */
/* .param p2, "touchX" # F */
/* .param p3, "touchY" # F */
/* .param p4, "desc" # Landroid/content/ClipDescription; */
/* .line 181 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMirrorDragState;->isValidDropTarget(Lcom/android/server/wm/WindowState;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 182 */
int v2 = 1; // const/4 v2, 0x1
int v5 = 0; // const/4 v5, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* move-object v1, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move-object v6, p4 */
/* invoke-static/range {v1 ..v9}, Lcom/android/server/wm/MiuiMirrorDragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent; */
/* .line 185 */
/* .local v0, "event":Landroid/view/DragEvent; */
try { // :try_start_0
v1 = this.mClient;
/* .line 187 */
v1 = this.mNotifiedWindows;
(( java.util.ArrayList ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 192 */
v1 = android.os.Process .myPid ( );
v2 = this.mSession;
/* iget v2, v2, Lcom/android/server/wm/Session;->mPid:I */
/* if-eq v1, v2, :cond_1 */
/* .line 193 */
} // :goto_0
(( android.view.DragEvent ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/view/DragEvent;->recycle()V
/* .line 192 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 188 */
/* :catch_0 */
/* move-exception v1 */
/* .line 189 */
/* .local v1, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v2 = "MiuiMirrorDragState"; // const-string v2, "MiuiMirrorDragState"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Unable to drag-start window "; // const-string v4, "Unable to drag-start window "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 192 */
/* nop */
} // .end local v1 # "e":Landroid/os/RemoteException;
v1 = android.os.Process .myPid ( );
v2 = this.mSession;
/* iget v2, v2, Lcom/android/server/wm/Session;->mPid:I */
/* if-eq v1, v2, :cond_1 */
/* .line 193 */
/* .line 192 */
} // :goto_1
v2 = android.os.Process .myPid ( );
v3 = this.mSession;
/* iget v3, v3, Lcom/android/server/wm/Session;->mPid:I */
/* if-eq v2, v3, :cond_0 */
/* .line 193 */
(( android.view.DragEvent ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/view/DragEvent;->recycle()V
/* .line 195 */
} // :cond_0
/* throw v1 */
/* .line 197 */
} // .end local v0 # "event":Landroid/view/DragEvent;
} // :cond_1
} // :goto_2
return;
} // .end method
private Boolean targetWindowSupportsGlobalDrag ( com.android.server.wm.WindowState p0 ) {
/* .locals 2 */
/* .param p1, "targetWin" # Lcom/android/server/wm/WindowState; */
/* .line 220 */
v0 = this.mActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mActivityRecord;
/* iget v0, v0, Lcom/android/server/wm/ActivityRecord;->mTargetSdk:I */
/* const/16 v1, 0x18 */
/* if-lt v0, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
/* # virtual methods */
void broadcastDragStartedLocked ( Integer p0, Float p1, Float p2 ) {
/* .locals 3 */
/* .param p1, "displayId" # I */
/* .param p2, "touchX" # F */
/* .param p3, "touchY" # F */
/* .line 130 */
/* iput p1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayId:I */
/* .line 131 */
v0 = this.mService;
v0 = this.mRoot;
(( com.android.server.wm.RootWindowContainer ) v0 ).getDisplayContent ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
this.mCurrentDisplayContent = v0;
/* .line 132 */
/* iput p2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F */
/* .line 133 */
/* iput p3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F */
/* .line 135 */
v0 = this.mBroadcastDisplayIds;
java.lang.Integer .valueOf ( p1 );
v0 = (( java.util.ArrayList ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 136 */
v0 = this.mBroadcastDisplayIds;
java.lang.Integer .valueOf ( p1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 137 */
v0 = this.mCurrentDisplayContent;
/* new-instance v1, Lcom/android/server/wm/MiuiMirrorDragState$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p2, p3}, Lcom/android/server/wm/MiuiMirrorDragState$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiMirrorDragState;FF)V */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.DisplayContent ) v0 ).forAllWindows ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/DisplayContent;->forAllWindows(Ljava/util/function/Consumer;Z)V
/* .line 141 */
} // :cond_0
return;
} // .end method
void closeLocked ( ) {
/* .locals 1 */
/* .line 86 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.wm.MiuiMirrorDragState ) p0 ).closeLocked ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked(Z)V
/* .line 87 */
return;
} // .end method
void closeLocked ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "fromDelegate" # Z */
/* .line 90 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mIsClosing:Z */
/* .line 92 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 93 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiMirrorDragState;->broadcastDragEndedLocked()V */
/* .line 94 */
/* if-nez p1, :cond_0 */
/* .line 95 */
v0 = this.mMirrorServiceInternal;
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z */
(( com.xiaomi.mirror.service.MirrorServiceInternal ) v0 ).notifyDragResult ( v2 ); // invoke-virtual {v0, v2}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->notifyDragResult(Z)V
/* .line 97 */
} // :cond_0
/* iput-boolean v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z */
/* .line 100 */
} // :cond_1
/* iput v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I */
/* .line 101 */
int v0 = 0; // const/4 v0, 0x0
this.mLocalWin = v0;
/* .line 102 */
this.mToken = v0;
/* .line 103 */
this.mData = v0;
/* .line 104 */
this.mNotifiedWindows = v0;
/* .line 105 */
this.mBroadcastDisplayIds = v0;
/* .line 108 */
v0 = this.mDragDropController;
(( com.android.server.wm.MiuiMirrorDragDropController ) v0 ).onDragStateClosedLocked ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/wm/MiuiMirrorDragDropController;->onDragStateClosedLocked(Lcom/android/server/wm/MiuiMirrorDragState;)V
/* .line 109 */
return;
} // .end method
Boolean isClosing ( ) {
/* .locals 1 */
/* .line 78 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mIsClosing:Z */
} // .end method
Boolean isInProgress ( ) {
/* .locals 1 */
/* .line 363 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z */
} // .end method
void notifyDropLocked ( Integer p0, Float p1, Float p2 ) {
/* .locals 21 */
/* .param p1, "displayId" # I */
/* .param p2, "x" # F */
/* .param p3, "y" # F */
/* .line 296 */
/* move-object/from16 v1, p0 */
/* move/from16 v11, p2 */
/* move/from16 v12, p3 */
/* iget v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayId:I */
/* move/from16 v13, p1 */
/* if-ne v0, v13, :cond_1 */
v0 = this.mCurrentDisplayContent;
/* if-nez v0, :cond_0 */
/* .line 299 */
} // :cond_0
/* iput v11, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F */
/* .line 300 */
/* iput v12, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F */
/* .line 297 */
} // :cond_1
} // :goto_0
/* invoke-virtual/range {p0 ..p3}, Lcom/android/server/wm/MiuiMirrorDragState;->broadcastDragStartedLocked(IFF)V */
/* .line 303 */
} // :goto_1
v0 = this.mCurrentDisplayContent;
(( com.android.server.wm.DisplayContent ) v0 ).getTouchableWinAtPointLocked ( v11, v12 ); // invoke-virtual {v0, v11, v12}, Lcom/android/server/wm/DisplayContent;->getTouchableWinAtPointLocked(FF)Lcom/android/server/wm/WindowState;
/* .line 305 */
/* .local v14, "touchedWin":Lcom/android/server/wm/WindowState; */
v0 = /* invoke-direct {v1, v14}, Lcom/android/server/wm/MiuiMirrorDragState;->isWindowNotified(Lcom/android/server/wm/WindowState;)Z */
int v15 = 0; // const/4 v15, 0x0
/* if-nez v0, :cond_2 */
/* .line 308 */
/* iput-boolean v15, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mDragResult:Z */
/* .line 309 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V */
/* .line 310 */
return;
/* .line 313 */
} // :cond_2
v0 = (( com.android.server.wm.WindowState ) v14 ).getOwningUid ( ); // invoke-virtual {v14}, Lcom/android/server/wm/WindowState;->getOwningUid()I
v10 = android.os.UserHandle .getUserId ( v0 );
/* .line 316 */
/* .local v10, "targetUserId":I */
/* iget v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I */
/* and-int/lit16 v2, v0, 0x100 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* and-int/lit8 v0, v0, 0x3 */
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = this.mData;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 318 */
/* new-instance v0, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler; */
v2 = this.mService;
v3 = this.mGlobalLock;
v4 = this.mData;
/* iget v5, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mUid:I */
/* .line 322 */
(( com.android.server.wm.WindowState ) v14 ).getOwningPackage ( ); // invoke-virtual {v14}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* iget v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I */
/* and-int/lit16 v7, v2, 0xc3 */
/* iget v8, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I */
/* move-object v2, v0 */
/* move v9, v10 */
/* invoke-direct/range {v2 ..v9}, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;-><init>(Lcom/android/server/wm/WindowManagerGlobalLock;Landroid/content/ClipData;ILjava/lang/String;III)V */
/* .line 326 */
/* .local v0, "dragAndDropPermissions":Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler; */
/* iget v2, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mPid:I */
v3 = this.mMirrorServiceInternal;
v3 = (( com.xiaomi.mirror.service.MirrorServiceInternal ) v3 ).getDelegatePid ( ); // invoke-virtual {v3}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->getDelegatePid()I
/* if-ne v2, v3, :cond_3 */
/* .line 327 */
v2 = this.mMirrorServiceInternal;
/* .line 328 */
java.util.Objects .requireNonNull ( v2 );
/* new-instance v3, Lcom/android/server/wm/MiuiMirrorDragState$$ExternalSyntheticLambda1; */
/* invoke-direct {v3, v2}, Lcom/android/server/wm/MiuiMirrorDragState$$ExternalSyntheticLambda1;-><init>(Lcom/xiaomi/mirror/service/MirrorServiceInternal;)V */
/* .line 327 */
(( com.android.server.wm.MiuiDragAndDropPermissionsHandler ) v0 ).setOnPermissionReleaseListener ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;->setOnPermissionReleaseListener(Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener;)V
/* .line 333 */
} // :cond_3
/* move-object/from16 v16, v0 */
/* .line 331 */
} // .end local v0 # "dragAndDropPermissions":Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler;
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
/* move-object/from16 v16, v0 */
/* .line 333 */
/* .local v16, "dragAndDropPermissions":Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler; */
} // :goto_2
/* iget v0, v1, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I */
/* if-eq v0, v10, :cond_5 */
/* .line 334 */
v2 = this.mData;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 335 */
(( android.content.ClipData ) v2 ).fixUris ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/ClipData;->fixUris(I)V
/* .line 338 */
} // :cond_5
v9 = android.os.Process .myPid ( );
/* .line 339 */
/* .local v9, "myPid":I */
v0 = this.mClient;
/* .line 340 */
/* .local v8, "token":Landroid/os/IBinder; */
int v3 = 3; // const/4 v3, 0x3
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
v0 = this.mData;
/* const/16 v17, 0x0 */
/* move-object v2, v14 */
/* move/from16 v4, p2 */
/* move/from16 v5, p3 */
/* move-object/from16 v18, v8 */
} // .end local v8 # "token":Landroid/os/IBinder;
/* .local v18, "token":Landroid/os/IBinder; */
/* move-object v8, v0 */
/* move/from16 v19, v9 */
} // .end local v9 # "myPid":I
/* .local v19, "myPid":I */
/* move-object/from16 v9, v16 */
/* move/from16 v20, v10 */
} // .end local v10 # "targetUserId":I
/* .local v20, "targetUserId":I */
/* move/from16 v10, v17 */
/* invoke-static/range {v2 ..v10}, Lcom/android/server/wm/MiuiMirrorDragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent; */
/* .line 343 */
/* .local v2, "evt":Landroid/view/DragEvent; */
try { // :try_start_0
v0 = this.mClient;
/* .line 346 */
v0 = this.mDragDropController;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move-object/from16 v3, v18 */
} // .end local v18 # "token":Landroid/os/IBinder;
/* .local v3, "token":Landroid/os/IBinder; */
try { // :try_start_1
(( com.android.server.wm.MiuiMirrorDragDropController ) v0 ).sendTimeoutMessage ( v15, v3 ); // invoke-virtual {v0, v15, v3}, Lcom/android/server/wm/MiuiMirrorDragDropController;->sendTimeoutMessage(ILjava/lang/Object;)V
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 351 */
v0 = this.mSession;
/* iget v0, v0, Lcom/android/server/wm/Session;->mPid:I */
/* move/from16 v4, v19 */
} // .end local v19 # "myPid":I
/* .local v4, "myPid":I */
/* if-eq v4, v0, :cond_6 */
/* .line 352 */
} // :goto_3
(( android.view.DragEvent ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/view/DragEvent;->recycle()V
/* .line 351 */
} // .end local v4 # "myPid":I
/* .restart local v19 # "myPid":I */
/* :catchall_0 */
/* move-exception v0 */
/* move/from16 v4, v19 */
} // .end local v19 # "myPid":I
/* .restart local v4 # "myPid":I */
/* .line 347 */
} // .end local v4 # "myPid":I
/* .restart local v19 # "myPid":I */
/* :catch_0 */
/* move-exception v0 */
/* move/from16 v4, v19 */
} // .end local v19 # "myPid":I
/* .restart local v4 # "myPid":I */
/* .line 351 */
} // .end local v3 # "token":Landroid/os/IBinder;
} // .end local v4 # "myPid":I
/* .restart local v18 # "token":Landroid/os/IBinder; */
/* .restart local v19 # "myPid":I */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v3, v18 */
/* move/from16 v4, v19 */
} // .end local v18 # "token":Landroid/os/IBinder;
} // .end local v19 # "myPid":I
/* .restart local v3 # "token":Landroid/os/IBinder; */
/* .restart local v4 # "myPid":I */
/* .line 347 */
} // .end local v3 # "token":Landroid/os/IBinder;
} // .end local v4 # "myPid":I
/* .restart local v18 # "token":Landroid/os/IBinder; */
/* .restart local v19 # "myPid":I */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v3, v18 */
/* move/from16 v4, v19 */
/* .line 348 */
} // .end local v18 # "token":Landroid/os/IBinder;
} // .end local v19 # "myPid":I
/* .local v0, "e":Landroid/os/RemoteException; */
/* .restart local v3 # "token":Landroid/os/IBinder; */
/* .restart local v4 # "myPid":I */
} // :goto_4
try { // :try_start_2
final String v5 = "MiuiMirrorDragState"; // const-string v5, "MiuiMirrorDragState"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "can\'t send drop notification to win "; // const-string v7, "can\'t send drop notification to win "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v6 );
/* .line 349 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/wm/MiuiMirrorDragState;->closeLocked()V */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* .line 351 */
} // .end local v0 # "e":Landroid/os/RemoteException;
v0 = this.mSession;
/* iget v0, v0, Lcom/android/server/wm/Session;->mPid:I */
/* if-eq v4, v0, :cond_6 */
/* .line 352 */
/* .line 355 */
} // :cond_6
} // :goto_5
this.mToken = v3;
/* .line 356 */
return;
/* .line 351 */
/* :catchall_2 */
/* move-exception v0 */
} // :goto_6
v5 = this.mSession;
/* iget v5, v5, Lcom/android/server/wm/Session;->mPid:I */
/* if-eq v4, v5, :cond_7 */
/* .line 352 */
(( android.view.DragEvent ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/view/DragEvent;->recycle()V
/* .line 354 */
} // :cond_7
/* throw v0 */
} // .end method
void notifyMoveLocked ( Integer p0, Float p1, Float p2 ) {
/* .locals 11 */
/* .param p1, "displayId" # I */
/* .param p2, "x" # F */
/* .param p3, "y" # F */
/* .line 248 */
/* iget v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentDisplayId:I */
/* if-ne p1, v0, :cond_1 */
v0 = this.mCurrentDisplayContent;
/* if-nez v0, :cond_0 */
/* .line 251 */
} // :cond_0
/* iput p2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F */
/* .line 252 */
/* iput p3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F */
/* .line 249 */
} // :cond_1
} // :goto_0
(( com.android.server.wm.MiuiMirrorDragState ) p0 ).broadcastDragStartedLocked ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiMirrorDragState;->broadcastDragStartedLocked(IFF)V
/* .line 256 */
} // :goto_1
v0 = this.mCurrentDisplayContent;
/* iget v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F */
/* iget v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F */
/* .line 257 */
(( com.android.server.wm.DisplayContent ) v0 ).getTouchableWinAtPointLocked ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/DisplayContent;->getTouchableWinAtPointLocked(FF)Lcom/android/server/wm/WindowState;
/* .line 258 */
/* .local v0, "touchedWin":Lcom/android/server/wm/WindowState; */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiMirrorDragState;->isWindowNotified(Lcom/android/server/wm/WindowState;)Z */
/* if-nez v1, :cond_2 */
/* .line 261 */
int v0 = 0; // const/4 v0, 0x0
/* .line 265 */
} // :cond_2
try { // :try_start_0
v1 = android.os.Process .myPid ( );
/* move v10, v1 */
/* .line 268 */
/* .local v10, "myPid":I */
v1 = this.mTargetWindow;
/* if-eq v0, v1, :cond_3 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 270 */
int v2 = 6; // const/4 v2, 0x6
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* invoke-static/range {v1 ..v9}, Lcom/android/server/wm/MiuiMirrorDragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent; */
/* .line 272 */
/* .local v1, "evt":Landroid/view/DragEvent; */
v2 = this.mTargetWindow;
v2 = this.mClient;
/* .line 273 */
v2 = this.mTargetWindow;
v2 = this.mSession;
/* iget v2, v2, Lcom/android/server/wm/Session;->mPid:I */
/* if-eq v10, v2, :cond_3 */
/* .line 274 */
(( android.view.DragEvent ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/view/DragEvent;->recycle()V
/* .line 277 */
} // .end local v1 # "evt":Landroid/view/DragEvent;
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 278 */
int v2 = 2; // const/4 v2, 0x2
/* iget v3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F */
/* iget v4, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F */
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* move-object v1, v0 */
/* invoke-static/range {v1 ..v9}, Lcom/android/server/wm/MiuiMirrorDragState;->obtainDragEvent(Lcom/android/server/wm/WindowState;IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Lcom/android/internal/view/IDragAndDropPermissions;Z)Landroid/view/DragEvent; */
/* .line 280 */
/* .restart local v1 # "evt":Landroid/view/DragEvent; */
v2 = this.mClient;
/* .line 281 */
v2 = this.mSession;
/* iget v2, v2, Lcom/android/server/wm/Session;->mPid:I */
/* if-eq v10, v2, :cond_4 */
/* .line 282 */
(( android.view.DragEvent ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/view/DragEvent;->recycle()V
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 287 */
} // .end local v1 # "evt":Landroid/view/DragEvent;
} // .end local v10 # "myPid":I
} // :cond_4
/* .line 285 */
/* :catch_0 */
/* move-exception v1 */
/* .line 286 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "MiuiMirrorDragState"; // const-string v2, "MiuiMirrorDragState"
final String v3 = "can\'t send drag notification to windows"; // const-string v3, "can\'t send drag notification to windows"
android.util.Slog .w ( v2,v3 );
/* .line 288 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_2
this.mTargetWindow = v0;
/* .line 289 */
return;
} // .end method
void prepareDragStartedLocked ( ) {
/* .locals 6 */
/* .line 114 */
v0 = this.mData;
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.content.ClipData ) v0 ).getDescription ( ); // invoke-virtual {v0}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
this.mDataDescription = v0;
/* .line 115 */
v0 = this.mNotifiedWindows;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 116 */
v0 = this.mBroadcastDisplayIds;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 117 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z */
/* .line 119 */
/* iget v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mUid:I */
v1 = android.os.UserHandle .getUserId ( v1 );
/* iput v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I */
/* .line 121 */
/* const-class v1, Lcom/android/server/pm/UserManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/pm/UserManagerInternal; */
/* .line 122 */
/* .local v1, "userManager":Lcom/android/server/pm/UserManagerInternal; */
/* iget v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mSourceUserId:I */
final String v3 = "no_cross_profile_copy_paste"; // const-string v3, "no_cross_profile_copy_paste"
v2 = (( com.android.server.pm.UserManagerInternal ) v1 ).getUserRestriction ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/UserManagerInternal;->getUserRestriction(ILjava/lang/String;)Z
/* xor-int/2addr v0, v2 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCrossProfileCopyAllowed:Z */
/* .line 124 */
v0 = this.mMirrorServiceInternal;
/* iget v2, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mUid:I */
/* iget v3, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mPid:I */
v4 = this.mData;
/* iget v5, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mFlags:I */
(( com.xiaomi.mirror.service.MirrorServiceInternal ) v0 ).notifyDragStart ( v2, v3, v4, v5 ); // invoke-virtual {v0, v2, v3, v4, v5}, Lcom/xiaomi/mirror/service/MirrorServiceInternal;->notifyDragStart(IILandroid/content/ClipData;I)V
/* .line 125 */
return;
} // .end method
void sendDragStartedIfNeededLocked ( com.android.server.wm.WindowState p0 ) {
/* .locals 3 */
/* .param p1, "newWin" # Lcom/android/server/wm/WindowState; */
/* .line 229 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mDragInProgress:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 231 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMirrorDragState;->isWindowNotified(Lcom/android/server/wm/WindowState;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 232 */
return;
/* .line 234 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentX:F */
/* iget v1, p0, Lcom/android/server/wm/MiuiMirrorDragState;->mCurrentY:F */
v2 = this.mDataDescription;
/* invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/wm/MiuiMirrorDragState;->sendDragStartedLocked(Lcom/android/server/wm/WindowState;FFLandroid/content/ClipDescription;)V */
/* .line 236 */
} // :cond_1
return;
} // .end method
