class com.android.server.wm.WindowStateAnimatorImpl implements com.android.server.wm.WindowStateAnimatorStub {
	 /* .source "WindowStateAnimatorImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 com.android.server.wm.WindowStateAnimatorImpl ( ) {
		 /* .locals 0 */
		 /* .line 27 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean isAllowedDisableScreenshot ( com.android.server.wm.WindowState p0 ) {
		 /* .locals 2 */
		 /* .param p1, "w" # Lcom/android/server/wm/WindowState; */
		 /* .line 31 */
		 v0 = this.mAttrs;
		 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
		 /* const/high16 v1, 0x800000 */
		 /* and-int/2addr v0, v1 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public Boolean isNeedPrintLog ( com.android.server.wm.WindowState p0 ) {
	 /* .locals 2 */
	 /* .param p1, "mWin" # Lcom/android/server/wm/WindowState; */
	 /* .line 35 */
	 v0 = this.mAttrs;
	 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
	 /* const/16 v1, 0x7eb */
	 /* if-eq v0, v1, :cond_0 */
	 v0 = this.mAttrs;
	 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
	 /* const/16 v1, 0x7dd */
	 /* if-eq v0, v1, :cond_0 */
	 v0 = this.mAttrs;
	 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
	 /* const/16 v1, 0x7e8 */
	 /* if-eq v0, v1, :cond_0 */
	 v0 = this.mAttrs;
	 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
	 /* const/16 v1, 0x7d0 */
	 /* if-eq v0, v1, :cond_0 */
	 v0 = this.mAttrs;
	 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
	 /* const/16 v1, 0x7e3 */
	 /* if-eq v0, v1, :cond_0 */
	 v0 = this.mAttrs;
	 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
	 int v1 = 3; // const/4 v1, 0x3
	 /* if-eq v0, v1, :cond_0 */
	 v0 = this.mAttrs;
	 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
	 /* const/16 v1, 0x7e4 */
	 /* if-eq v0, v1, :cond_0 */
	 v0 = this.mAttrs;
	 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
	 /* const/16 v1, 0x7d5 */
	 /* if-eq v0, v1, :cond_0 */
	 v0 = this.mAttrs;
	 /* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
	 /* const/16 v1, 0x7d6 */
	 /* if-eq v0, v1, :cond_0 */
	 /* .line 44 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 46 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean needDisplaySyncTransaction ( com.android.server.wm.WindowState p0 ) {
/* .locals 2 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 51 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
	 v1 = this.mAttrs;
	 /* if-nez v1, :cond_0 */
	 /* .line 52 */
} // :cond_0
v1 = this.mAttrs;
/* iget v1, v1, Landroid/view/WindowManager$LayoutParams;->extraFlags:I */
/* and-int/lit16 v1, v1, 0x2000 */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 54 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 56 */
} // :cond_1
/* .line 51 */
} // :cond_2
} // :goto_0
} // .end method
