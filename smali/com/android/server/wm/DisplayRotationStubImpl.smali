.class public Lcom/android/server/wm/DisplayRotationStubImpl;
.super Ljava/lang/Object;
.source "DisplayRotationStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/DisplayRotationStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;
    }
.end annotation


# static fields
.field public static final BOOT_SENSOR_ROTATION_INVALID:I = -0x1

.field private static final TAG:Ljava/lang/String; = "WindowManager"


# instance fields
.field private mBootBlackCoverLayer:Landroid/view/SurfaceControl;

.field private mContext:Landroid/content/Context;

.field private mMiuiSettingsObserver:Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;

.field private mService:Lcom/android/server/wm/WindowManagerService;

.field private mUserRotationInner:I

.field private mUserRotationModeInner:I

.field private mUserRotationModeOuter:I

.field private mUserRotationOuter:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/DisplayRotationStubImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I

    .line 72
    iput v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I

    .line 75
    iput v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I

    return-void
.end method

.method private setUserRotation(II)V
    .locals 5
    .param p1, "userRotationMode"    # I
    .param p2, "userRotation"    # I

    .line 197
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 198
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 200
    .local v1, "res":Landroid/content/ContentResolver;
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    const/4 v2, 0x0

    .line 201
    .local v2, "accelerometerRotation":I
    :cond_0
    const-string v3, "accelerometer_rotation"

    const/4 v4, -0x2

    invoke-static {v1, v3, v2, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 203
    const-string/jumbo v3, "user_rotation"

    invoke-static {v1, v3, p2, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 205
    nop

    .end local v1    # "res":Landroid/content/ContentResolver;
    .end local v2    # "accelerometerRotation":I
    monitor-exit v0

    .line 206
    return-void

    .line 205
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setUserRotationForFold(IIZ)V
    .locals 1
    .param p1, "userRotationMode"    # I
    .param p2, "userRotation"    # I
    .param p3, "isFolded"    # Z

    .line 220
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationStub;->get()Lcom/android/server/wm/MiuiOrientationStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/MiuiOrientationStub;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_FOLD:Z

    if-eqz v0, :cond_1

    .line 221
    if-eqz p3, :cond_0

    .line 222
    iput p1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I

    .line 223
    iput p2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I

    goto :goto_0

    .line 225
    :cond_0
    iput p1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I

    .line 226
    iput p2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I

    .line 229
    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public addBootBlackCoverLayer()V
    .locals 4

    .line 385
    invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->needBootBlackCoverLayer()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->hasBootBlackCoverLayer()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mTransactionFactory:Ljava/util/function/Supplier;

    invoke-interface {v0}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceControl$Transaction;

    .line 387
    .local v0, "t":Landroid/view/SurfaceControl$Transaction;
    iget-object v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    .line 388
    .local v1, "displayContent":Lcom/android/server/wm/DisplayContent;
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;

    move-result-object v2

    .line 389
    const-string v3, "BootBlackCoverLayerSurface"

    invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v2

    .line 390
    invoke-virtual {v2}, Landroid/view/SurfaceControl$Builder;->setColorLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v2

    .line 391
    const-string v3, "BootAnimation"

    invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v2

    .line 392
    invoke-virtual {v2}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mBootBlackCoverLayer:Landroid/view/SurfaceControl;

    .line 393
    const v3, 0x1eab91

    invoke-virtual {v0, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    .line 394
    iget-object v2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mBootBlackCoverLayer:Landroid/view/SurfaceControl;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 395
    iget-object v2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mBootBlackCoverLayer:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 396
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 397
    const-string v2, "DisplayRotationImpl"

    const-string v3, "addBootBlackCoverLayerSurface"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    return-void

    .line 385
    .end local v0    # "t":Landroid/view/SurfaceControl$Transaction;
    .end local v1    # "displayContent":Lcom/android/server/wm/DisplayContent;
    :cond_1
    :goto_0
    return-void
.end method

.method public cancelAppAnimationIfNeeded(Lcom/android/server/wm/DisplayContent;)V
    .locals 5
    .param p1, "mDisplayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 93
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/android/server/wm/DisplayContent;->isAnimating(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 94
    const-string v0, "prepareNormalRA apptransition still animating"

    const-string v1, "WindowManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mAppTransition:Lcom/android/server/wm/AppTransition;

    iget-object v0, v0, Lcom/android/server/wm/AppTransition;->mLastOpeningAnimationTargets:Landroid/util/ArraySet;

    const-string v2, "prepareNormalRA cancelAnimation "

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mAppTransition:Lcom/android/server/wm/AppTransition;

    iget-object v0, v0, Lcom/android/server/wm/AppTransition;->mLastOpeningAnimationTargets:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/WindowContainer;

    .line 98
    .local v3, "animatingContainer":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {v3}, Lcom/android/server/wm/WindowContainer;->cancelAnimation()V

    .line 99
    sget-object v4, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_ORIENTATION:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v4}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 100
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    .end local v3    # "animatingContainer":Lcom/android/server/wm/WindowContainer;
    :cond_0
    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mAppTransition:Lcom/android/server/wm/AppTransition;

    iget-object v0, v0, Lcom/android/server/wm/AppTransition;->mLastClosingAnimationTargets:Landroid/util/ArraySet;

    if-eqz v0, :cond_3

    .line 106
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mAppTransition:Lcom/android/server/wm/AppTransition;

    iget-object v0, v0, Lcom/android/server/wm/AppTransition;->mLastClosingAnimationTargets:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/WindowContainer;

    .line 107
    .restart local v3    # "animatingContainer":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {v3}, Lcom/android/server/wm/WindowContainer;->cancelAnimation()V

    .line 108
    sget-object v4, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_ORIENTATION:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v4}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 109
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    .end local v3    # "animatingContainer":Lcom/android/server/wm/WindowContainer;
    :cond_2
    goto :goto_1

    .line 114
    :cond_3
    return-void
.end method

.method public checkBootBlackCoverLayer()V
    .locals 5

    .line 413
    invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->needBootBlackCoverLayer()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->hasBootBlackCoverLayer()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 415
    .local v0, "displayContent":Lcom/android/server/wm/DisplayContent;
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getRotation()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getWindowConfiguration()Landroid/app/WindowConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getRotation()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 416
    invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->removeBootBlackCoverLayer()V

    .line 417
    return-void

    .line 419
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    const/16 v2, 0x6d

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/android/server/wm/WindowManagerService$H;->sendNewMessageDelayed(ILjava/lang/Object;J)V

    .line 421
    return-void

    .line 413
    .end local v0    # "displayContent":Lcom/android/server/wm/DisplayContent;
    :cond_2
    :goto_0
    return-void
.end method

.method public dumpDoubleSwitch(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  mUserRotationModeOuter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I

    .line 280
    invoke-static {v1}, Lcom/android/server/policy/WindowManagerPolicy;->userRotationModeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 279
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mUserRotationOuter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I

    invoke-static {v1}, Landroid/view/Surface;->rotationToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  mUserRotationModeInner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I

    .line 283
    invoke-static {v1}, Lcom/android/server/policy/WindowManagerPolicy;->userRotationModeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 282
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mUserRotationInner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I

    invoke-static {v1}, Landroid/view/Surface;->rotationToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 285
    return-void
.end method

.method public forceEnableSeamless(Lcom/android/server/wm/WindowState;)Z
    .locals 2
    .param p1, "w"    # Lcom/android/server/wm/WindowState;

    .line 84
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x1

    return v0

    .line 88
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getBootSensorRotation()I
    .locals 2

    .line 361
    const-string v0, "ro.boot.ori"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 362
    .local v0, "bootSensorRotation":I
    packed-switch v0, :pswitch_data_0

    .line 373
    return v1

    .line 370
    :pswitch_0
    const/4 v1, 0x2

    return v1

    .line 368
    :pswitch_1
    const/4 v1, 0x3

    return v1

    .line 366
    :pswitch_2
    const/4 v1, 0x0

    return v1

    .line 364
    :pswitch_3
    return v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getHoverModeRecommendRotation(Lcom/android/server/wm/DisplayContent;)I
    .locals 2
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 127
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 128
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 129
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    .line 130
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->getHoverModeRecommendRotation(Lcom/android/server/wm/DisplayContent;)I

    move-result v1

    return v1

    .line 132
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method public getUserRotationFromSettings(Z)I
    .locals 4
    .param p1, "isFolded"    # Z

    .line 253
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 254
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v1, -0x2

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    .line 255
    const-string/jumbo v3, "user_rotation_outer"

    invoke-static {v0, v3, v2, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    return v1

    .line 258
    :cond_0
    const-string/jumbo v3, "user_rotation_inner"

    invoke-static {v0, v3, v2, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    return v1
.end method

.method public getUserRotationModeFromSettings(Z)I
    .locals 5
    .param p1, "isFolded"    # Z

    .line 237
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 238
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v1, -0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    .line 240
    nop

    .line 239
    const-string v4, "accelerometer_rotation_outer"

    invoke-static {v0, v4, v2, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    goto :goto_0

    .line 242
    :cond_0
    move v2, v3

    .line 239
    :goto_0
    return v2

    .line 246
    :cond_1
    nop

    .line 245
    const-string v4, "accelerometer_rotation_inner"

    invoke-static {v0, v4, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_2

    .line 247
    goto :goto_1

    .line 248
    :cond_2
    move v2, v3

    .line 245
    :goto_1
    return v2
.end method

.method public hasBootBlackCoverLayer()Z
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mBootBlackCoverLayer:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public immediatelyRemoveBootLayer()V
    .locals 3

    .line 424
    invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->needBootBlackCoverLayer()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->hasBootBlackCoverLayer()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 425
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->removeBootBlackCoverLayer()V

    .line 426
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    iget-object v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 427
    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    .line 426
    const/16 v2, 0x6d

    invoke-virtual {v0, v2, v1}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(ILjava/lang/Object;)V

    .line 428
    return-void

    .line 424
    :cond_1
    :goto_0
    return-void
.end method

.method public init(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V
    .locals 2
    .param p1, "wms"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 60
    iput-object p2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mContext:Landroid/content/Context;

    .line 61
    iput-object p1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 62
    new-instance v0, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;

    invoke-static {}, Lcom/android/server/UiThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;-><init>(Lcom/android/server/wm/DisplayRotationStubImpl;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mMiuiSettingsObserver:Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;

    .line 63
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayRotationStubImpl$MiuiSettingsObserver;->observe()V

    .line 64
    return-void
.end method

.method public isProvisioned()Z
    .locals 3

    .line 322
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 323
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "device_provisioned"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public isRotationLocked(Z)Z
    .locals 2
    .param p1, "isFolded"    # Z

    .line 232
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I

    :goto_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public needBootBlackCoverLayer()Z
    .locals 2

    .line 377
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_TABLET:Z

    if-eqz v0, :cond_0

    .line 378
    const-string v0, "ro.boot.ori"

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 377
    :goto_0
    return v0
.end method

.method public removeBootBlackCoverLayer()V
    .locals 3

    .line 401
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mBootBlackCoverLayer:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mTransactionFactory:Ljava/util/function/Supplier;

    invoke-interface {v0}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceControl$Transaction;

    .line 403
    .local v0, "t":Landroid/view/SurfaceControl$Transaction;
    iget-object v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mBootBlackCoverLayer:Landroid/view/SurfaceControl;

    invoke-virtual {v1}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mBootBlackCoverLayer:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 406
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mBootBlackCoverLayer:Landroid/view/SurfaceControl;

    .line 407
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 408
    const-string v1, "DisplayRotationImpl"

    const-string v2, "removeBootBlackCoverLayerSurface"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    .end local v0    # "t":Landroid/view/SurfaceControl$Transaction;
    :cond_1
    return-void
.end method

.method public setUserRotation()V
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    iget v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I

    iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I

    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotation(II)V

    goto :goto_0

    .line 192
    :cond_0
    iget v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I

    iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I

    invoke-direct {p0, v0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotation(II)V

    .line 194
    :goto_0
    return-void
.end method

.method public setUserRotationForFold(IZIZ)V
    .locals 3
    .param p1, "currRotation"    # I
    .param p2, "enable"    # Z
    .param p3, "rotation"    # I
    .param p4, "isFolded"    # Z

    .line 209
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    move v0, p1

    goto :goto_0

    :cond_0
    move v0, p3

    :goto_0
    move p3, v0

    .line 210
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_1

    move v2, v1

    goto :goto_1

    .line 211
    :cond_1
    move v2, v0

    :goto_1
    nop

    .line 213
    .local v2, "userRotationMode":I
    if-ne v2, v1, :cond_2

    goto :goto_2

    :cond_2
    move v0, v1

    .line 214
    .local v0, "accelerometerRotation":I
    :goto_2
    invoke-direct {p0, v2, p3, p4}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotationForFold(IIZ)V

    .line 215
    invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotationLockedForSettings(IIZ)V

    .line 217
    return-void
.end method

.method public setUserRotationLockedForSettings(IIZ)V
    .locals 3
    .param p1, "userRotationMode"    # I
    .param p2, "userRotation"    # I
    .param p3, "isFolded"    # Z

    .line 264
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 265
    .local v0, "res":Landroid/content/ContentResolver;
    const/4 v1, -0x2

    if-eqz p3, :cond_0

    .line 266
    const-string v2, "accelerometer_rotation_outer"

    invoke-static {v0, v2, p1, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 268
    const-string/jumbo v2, "user_rotation_outer"

    invoke-static {v0, v2, p2, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto :goto_0

    .line 271
    :cond_0
    const-string v2, "accelerometer_rotation_inner"

    invoke-static {v0, v2, p1, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 273
    const-string/jumbo v2, "user_rotation_inner"

    invoke-static {v0, v2, p2, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 276
    :goto_0
    return-void
.end method

.method public shouldHoverModeEnableSensor(Lcom/android/server/wm/DisplayContent;)Z
    .locals 2
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 136
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 137
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 138
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->shouldHoverModeEnableSensor(Lcom/android/server/wm/DisplayContent;)Z

    move-result v1

    return v1

    .line 141
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public updateRotation(Lcom/android/server/wm/DisplayContent;I)V
    .locals 1
    .param p1, "dc"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "lastRotation"    # I

    .line 119
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 120
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 121
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->updateLastRotation(Lcom/android/server/wm/DisplayContent;I)V

    .line 124
    :cond_0
    return-void
.end method

.method public updateRotationMode()V
    .locals 9

    .line 148
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationStub;->get()Lcom/android/server/wm/MiuiOrientationStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/MiuiOrientationStub;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_FOLD:Z

    if-eqz v0, :cond_7

    .line 149
    iget-object v0, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 151
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string/jumbo v1, "user_rotation"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 155
    .local v1, "userRotation":I
    nop

    .line 154
    const-string v4, "accelerometer_rotation"

    invoke-static {v0, v4, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    .line 156
    move v3, v2

    goto :goto_0

    .line 157
    :cond_0
    move v3, v4

    :goto_0
    nop

    .line 159
    .local v3, "userRotationMode":I
    const/4 v5, 0x0

    .line 160
    .local v5, "shouldUpdateRotationMode":Z
    invoke-virtual {p0, v4}, Lcom/android/server/wm/DisplayRotationStubImpl;->getUserRotationModeFromSettings(Z)I

    move-result v6

    .line 161
    .local v6, "userRotationModeOuter":I
    invoke-virtual {p0, v4}, Lcom/android/server/wm/DisplayRotationStubImpl;->getUserRotationFromSettings(Z)I

    move-result v4

    .line 162
    .local v4, "userOrientationOuter":I
    iget v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I

    if-ne v7, v6, :cond_1

    iget v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I

    if-eq v7, v4, :cond_3

    .line 164
    :cond_1
    iput v6, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I

    .line 165
    iput v4, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I

    .line 166
    iget-object v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v7, v7, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v7}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v7

    if-eqz v7, :cond_3

    iget v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeOuter:I

    if-ne v7, v3, :cond_2

    iget v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationOuter:I

    if-eq v7, v1, :cond_3

    .line 168
    :cond_2
    const/4 v5, 0x1

    .line 171
    :cond_3
    invoke-virtual {p0, v2}, Lcom/android/server/wm/DisplayRotationStubImpl;->getUserRotationModeFromSettings(Z)I

    move-result v7

    .line 172
    .local v7, "userRotationModeInner":I
    invoke-virtual {p0, v2}, Lcom/android/server/wm/DisplayRotationStubImpl;->getUserRotationFromSettings(Z)I

    move-result v2

    .line 173
    .local v2, "userOrientationInner":I
    iget v8, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I

    if-ne v8, v7, :cond_4

    iget v8, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I

    if-eq v8, v2, :cond_6

    .line 175
    :cond_4
    iput v7, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I

    .line 176
    iput v2, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I

    .line 177
    iget-object v8, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v8, v8, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v8}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v8

    if-nez v8, :cond_6

    iget v8, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationModeInner:I

    if-ne v8, v3, :cond_5

    iget v8, p0, Lcom/android/server/wm/DisplayRotationStubImpl;->mUserRotationInner:I

    if-eq v8, v1, :cond_6

    .line 179
    :cond_5
    const/4 v5, 0x1

    .line 182
    :cond_6
    if-eqz v5, :cond_7

    .line 183
    invoke-virtual {p0}, Lcom/android/server/wm/DisplayRotationStubImpl;->setUserRotation()V

    .line 186
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    .end local v1    # "userRotation":I
    .end local v2    # "userOrientationInner":I
    .end local v3    # "userRotationMode":I
    .end local v4    # "userOrientationOuter":I
    .end local v5    # "shouldUpdateRotationMode":Z
    .end local v6    # "userRotationModeOuter":I
    .end local v7    # "userRotationModeInner":I
    :cond_7
    return-void
.end method

.method public writeRotationChangeToProperties(I)V
    .locals 4
    .param p1, "rotation"    # I

    .line 290
    const/4 v0, -0x1

    .line 291
    .local v0, "currRotation":I
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 302
    :pswitch_0
    const/16 v0, 0x10e

    .line 303
    goto :goto_0

    .line 299
    :pswitch_1
    const/16 v0, 0xb4

    .line 300
    goto :goto_0

    .line 296
    :pswitch_2
    const/16 v0, 0x5a

    .line 297
    goto :goto_0

    .line 293
    :pswitch_3
    const/4 v0, 0x0

    .line 294
    nop

    .line 307
    :goto_0
    if-gez v0, :cond_0

    return-void

    .line 308
    :cond_0
    move v1, v0

    .line 309
    .local v1, "finalRotation":I
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/android/server/wm/DisplayRotationStubImpl$1;

    invoke-direct {v3, p0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl$1;-><init>(Lcom/android/server/wm/DisplayRotationStubImpl;I)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 319
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public writeRotationChangeToPropertiesForDisplay(I)V
    .locals 4
    .param p1, "rotation"    # I

    .line 328
    const/4 v0, -0x1

    .line 329
    .local v0, "currRotation":I
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 340
    :pswitch_0
    const/16 v0, 0x10e

    .line 341
    goto :goto_0

    .line 337
    :pswitch_1
    const/16 v0, 0xb4

    .line 338
    goto :goto_0

    .line 334
    :pswitch_2
    const/16 v0, 0x5a

    .line 335
    goto :goto_0

    .line 331
    :pswitch_3
    const/4 v0, 0x0

    .line 332
    nop

    .line 345
    :goto_0
    if-gez v0, :cond_0

    return-void

    .line 346
    :cond_0
    move v1, v0

    .line 347
    .local v1, "finalRotation":I
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/android/server/wm/DisplayRotationStubImpl$2;

    invoke-direct {v3, p0, v1}, Lcom/android/server/wm/DisplayRotationStubImpl$2;-><init>(Lcom/android/server/wm/DisplayRotationStubImpl;I)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 357
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
