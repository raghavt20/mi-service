class com.android.server.wm.MiuiSizeCompatJob$3 implements java.util.function.BiConsumer {
	 /* .source "MiuiSizeCompatJob.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiSizeCompatJob;->trackEvent(Landroid/app/job/JobParameters;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/function/BiConsumer<", */
/* "Ljava/lang/String;", */
/* "Landroid/sizecompat/AspectRatioInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiSizeCompatJob this$0; //synthetic
final org.json.JSONArray val$array; //synthetic
final java.util.Map val$embeddedApps; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiSizeCompatJob$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiSizeCompatJob; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 205 */
this.this$0 = p1;
this.val$embeddedApps = p2;
this.val$array = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void accept ( java.lang.Object p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 205 */
/* check-cast p1, Ljava/lang/String; */
/* check-cast p2, Landroid/sizecompat/AspectRatioInfo; */
(( com.android.server.wm.MiuiSizeCompatJob$3 ) p0 ).accept ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/MiuiSizeCompatJob$3;->accept(Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;)V
return;
} // .end method
public void accept ( java.lang.String p0, android.sizecompat.AspectRatioInfo p1 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "aspectRatioInfo" # Landroid/sizecompat/AspectRatioInfo; */
/* .line 208 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 210 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "app_package_name"; // const-string v1, "app_package_name"
(( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 211 */
final String v1 = "app_display_name"; // const-string v1, "app_display_name"
v2 = this.mApplicationName;
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 212 */
v1 = this.val$embeddedApps;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
final String v2 = "app_display_style"; // const-string v2, "app_display_style"
if ( v1 != null) { // if-eqz v1, :cond_0
	 v1 = try { // :try_start_1
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* iget v1, p2, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F */
		 int v3 = 0; // const/4 v3, 0x0
		 /* cmpl-float v1, v1, v3 */
		 /* if-nez v1, :cond_0 */
		 /* .line 214 */
		 /* const-string/jumbo v1, "\u5e73\u884c\u7a97\u53e3" */
		 (( org.json.JSONObject ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
		 /* .line 216 */
	 } // :cond_0
	 (( android.sizecompat.AspectRatioInfo ) p2 ).getRatioStr ( ); // invoke-virtual {p2}, Landroid/sizecompat/AspectRatioInfo;->getRatioStr()Ljava/lang/String;
	 (( org.json.JSONObject ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 218 */
} // :goto_0
v1 = this.val$array;
(( org.json.JSONArray ) v1 ).put ( v0 ); // invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 221 */
/* .line 219 */
/* :catch_0 */
/* move-exception v1 */
/* .line 220 */
/* .local v1, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
/* .line 222 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_1
return;
} // .end method
