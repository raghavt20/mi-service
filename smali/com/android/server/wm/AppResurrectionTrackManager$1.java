class com.android.server.wm.AppResurrectionTrackManager$1 implements android.content.ServiceConnection {
	 /* .source "AppResurrectionTrackManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AppResurrectionTrackManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.AppResurrectionTrackManager this$0; //synthetic
/* # direct methods */
 com.android.server.wm.AppResurrectionTrackManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/AppResurrectionTrackManager; */
/* .line 36 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 39 */
v0 = this.this$0;
com.miui.analytics.ITrackBinder$Stub .asInterface ( p2 );
com.android.server.wm.AppResurrectionTrackManager .-$$Nest$fputmITrackBinder ( v0,v1 );
/* .line 40 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onServiceConnected: "; // const-string v1, "onServiceConnected: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
com.android.server.wm.AppResurrectionTrackManager .-$$Nest$fgetmITrackBinder ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AppResurrectionTrackManager"; // const-string v1, "AppResurrectionTrackManager"
android.util.Slog .d ( v1,v0 );
/* .line 41 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 45 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.wm.AppResurrectionTrackManager .-$$Nest$fputmITrackBinder ( v0,v1 );
/* .line 46 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.wm.AppResurrectionTrackManager .-$$Nest$fputmIsBind ( v0,v1 );
/* .line 47 */
final String v0 = "AppResurrectionTrackManager"; // const-string v0, "AppResurrectionTrackManager"
final String v1 = "onServiceDisconnected"; // const-string v1, "onServiceDisconnected"
android.util.Slog .d ( v0,v1 );
/* .line 48 */
return;
} // .end method
