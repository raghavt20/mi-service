public class com.android.server.wm.MiuiRotationAnimationUtils$EaseQuartOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "MiuiRotationAnimationUtils.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiRotationAnimationUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "EaseQuartOutInterpolator" */
} // .end annotation
/* # direct methods */
public com.android.server.wm.MiuiRotationAnimationUtils$EaseQuartOutInterpolator ( ) {
/* .locals 0 */
/* .line 49 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Float getInterpolation ( Float p0 ) {
/* .locals 3 */
/* .param p1, "input" # F */
/* .line 51 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* sub-float v1, p1, v0 */
/* sub-float v2, p1, v0 */
/* mul-float/2addr v1, v2 */
/* sub-float v2, p1, v0 */
/* mul-float/2addr v1, v2 */
/* sub-float v2, p1, v0 */
/* mul-float/2addr v1, v2 */
/* sub-float/2addr v0, v1 */
} // .end method
