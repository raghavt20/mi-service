public class com.android.server.wm.MultiSenceListener implements com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener {
	 /* .source "MultiSenceListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MultiSenceListener$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final Integer EVENT_MULTI_TASK_ACTION_END;
private static final Integer EVENT_MULTI_TASK_ACTION_START;
private static final Integer MSG_DISPATCH_MULTI_TASK;
private static final java.lang.String TAG;
private static com.android.server.wm.WindowManagerService service;
/* # instance fields */
private Boolean isSystemReady;
private final com.android.server.wm.MultiSenceListener$H mHandler;
private com.miui.server.multisence.MultiSenceManagerInternal mMultiSenceMI;
/* # direct methods */
static void -$$Nest$mLOG_IF_DEBUG ( com.android.server.wm.MultiSenceListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V */
	 return;
} // .end method
static void -$$Nest$mupdateDynamicSenceInfo ( com.android.server.wm.MultiSenceListener p0, miui.smartpower.MultiTaskActionManager$ActionInfo p1, Boolean p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MultiSenceListener;->updateDynamicSenceInfo(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;Z)V */
	 return;
} // .end method
static com.android.server.wm.MultiSenceListener ( ) {
	 /* .locals 2 */
	 /* .line 37 */
	 final String v0 = "persist.multisence.debug.on"; // const-string v0, "persist.multisence.debug.on"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.wm.MultiSenceListener.DEBUG = (v0!= 0);
	 return;
} // .end method
public com.android.server.wm.MultiSenceListener ( ) {
	 /* .locals 1 */
	 /* .param p1, "looper" # Landroid/os/Looper; */
	 /* .line 51 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 46 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/wm/MultiSenceListener;->isSystemReady:Z */
	 /* .line 52 */
	 final String v0 = "MultiSenceListener init"; // const-string v0, "MultiSenceListener init"
	 /* invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V */
	 /* .line 54 */
	 /* new-instance v0, Lcom/android/server/wm/MultiSenceListener$H; */
	 /* invoke-direct {v0, p0, p1}, Lcom/android/server/wm/MultiSenceListener$H;-><init>(Lcom/android/server/wm/MultiSenceListener;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 55 */
	 return;
} // .end method
private void LOG_IF_DEBUG ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p1, "log" # Ljava/lang/String; */
	 /* .line 228 */
	 /* sget-boolean v0, Lcom/android/server/wm/MultiSenceListener;->DEBUG:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 229 */
		 final String v0 = "MultiSenceListener"; // const-string v0, "MultiSenceListener"
		 android.util.Slog .d ( v0,p1 );
		 /* .line 231 */
	 } // :cond_0
	 return;
} // .end method
private java.util.Map getWindowsNeedToSched ( ) {
	 /* .locals 1 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Lcom/miui/server/multisence/SingleWindowInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 224 */
com.android.server.wm.MultiSenceUtils .getInstance ( );
(( com.android.server.wm.MultiSenceUtils ) v0 ).getWindowsNeedToSched ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MultiSenceUtils;->getWindowsNeedToSched()Ljava/util/Map;
} // .end method
private Boolean isConnectToWMS ( ) {
/* .locals 2 */
/* .line 216 */
v0 = com.android.server.wm.MultiSenceListener.service;
/* if-nez v0, :cond_0 */
/* .line 217 */
final String v0 = "MultiSenceListener"; // const-string v0, "MultiSenceListener"
final String v1 = "MultiSence Listener does not connect to WMS"; // const-string v1, "MultiSence Listener does not connect to WMS"
android.util.Slog .e ( v0,v1 );
/* .line 218 */
int v0 = 0; // const/4 v0, 0x0
/* .line 220 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void onMultiTaskActionChanged ( Integer p0, miui.smartpower.MultiTaskActionManager$ActionInfo p1 ) {
/* .locals 4 */
/* .param p1, "event" # I */
/* .param p2, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 126 */
v0 = this.mHandler;
(( com.android.server.wm.MultiSenceListener$H ) v0 ).obtainMessage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MultiSenceListener$H;->obtainMessage(I)Landroid/os/Message;
/* .line 127 */
/* .local v0, "message":Landroid/os/Message; */
this.obj = p2;
/* .line 128 */
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_0 */
/* .line 129 */
v1 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) p2 ).getType ( ); // invoke-virtual {p2}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I
/* const/16 v2, 0x1001 */
/* if-ne v1, v2, :cond_0 */
/* .line 130 */
v1 = this.mHandler;
/* const-wide/16 v2, 0x320 */
(( com.android.server.wm.MultiSenceListener$H ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/wm/MultiSenceListener$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 132 */
} // :cond_0
v1 = this.mHandler;
(( com.android.server.wm.MultiSenceListener$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/MultiSenceListener$H;->sendMessage(Landroid/os/Message;)Z
/* .line 134 */
} // :goto_0
return;
} // .end method
private Boolean readyToWork ( ) {
/* .locals 2 */
/* .line 69 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MultiSenceListener;->isSystemReady:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 70 */
/* .line 73 */
} // :cond_0
v0 = this.mMultiSenceMI;
/* if-nez v0, :cond_1 */
/* .line 74 */
final String v0 = "not connect to multi-sence core service"; // const-string v0, "not connect to multi-sence core service"
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 75 */
/* .line 78 */
} // :cond_1
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
/* if-nez v0, :cond_2 */
/* .line 79 */
final String v0 = "func not enable due to Cloud Contrller"; // const-string v0, "func not enable due to Cloud Contrller"
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 80 */
/* .line 83 */
} // :cond_2
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceConfig;->MULTITASK_ANIM_SCHED_ENABLED:Z */
/* if-nez v0, :cond_3 */
/* .line 84 */
final String v0 = "multi-task animation not enable by config"; // const-string v0, "multi-task animation not enable by config"
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 85 */
/* .line 88 */
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void updateDynamicSenceInfo ( miui.smartpower.MultiTaskActionManager$ActionInfo p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "taskInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .param p2, "isStarted" # Z */
/* .line 167 */
final String v0 = "_"; // const-string v0, "_"
/* .line 168 */
/* .local v0, "tids_s":Ljava/lang/String; */
(( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).getDrawnTids ( ); // invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getDrawnTids()[I
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_0 */
/* aget v4, v1, v3 */
/* .line 169 */
/* .local v4, "value":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v4 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "_"; // const-string v6, "_"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 168 */
} // .end local v4 # "value":I
/* add-int/lit8 v3, v3, 0x1 */
/* .line 171 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Task uid: "; // const-string v2, "Task uid: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).getUid ( ); // invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getUid()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", Task tid: "; // const-string v2, ", Task tid: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/android/server/wm/MultiSenceListener;->LOG_IF_DEBUG(Ljava/lang/String;)V */
/* .line 172 */
v1 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getType()I
/* .line 173 */
/* .local v1, "senceId":I */
v2 = (( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).getUid ( ); // invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getUid()I
/* .line 174 */
/* .local v2, "uid":I */
(( miui.smartpower.MultiTaskActionManager$ActionInfo ) p1 ).getDrawnTids ( ); // invoke-virtual {p1}, Lmiui/smartpower/MultiTaskActionManager$ActionInfo;->getDrawnTids()[I
/* .line 175 */
/* .local v3, "tids":[I */
v4 = this.mMultiSenceMI;
/* .line 176 */
return;
} // .end method
/* # virtual methods */
public void onMultiTaskActionEnd ( miui.smartpower.MultiTaskActionManager$ActionInfo p0 ) {
/* .locals 3 */
/* .param p1, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 110 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MultiSenceListener;->readyToWork()Z */
/* if-nez v0, :cond_0 */
/* .line 111 */
return;
/* .line 114 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 115 */
final String v0 = "MultiSenceListener"; // const-string v0, "MultiSenceListener"
final String v1 = "action info must not be null"; // const-string v1, "action info must not be null"
android.util.Slog .w ( v0,v1 );
/* .line 116 */
return;
/* .line 119 */
} // :cond_1
final String v0 = "MultisenceListener.onMultiTaskActionEnd"; // const-string v0, "MultisenceListener.onMultiTaskActionEnd"
/* const-wide/16 v1, 0x20 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 120 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MultiSenceListener;->onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V */
/* .line 121 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 122 */
return;
} // .end method
public void onMultiTaskActionStart ( miui.smartpower.MultiTaskActionManager$ActionInfo p0 ) {
/* .locals 3 */
/* .param p1, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 93 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MultiSenceListener;->readyToWork()Z */
/* if-nez v0, :cond_0 */
/* .line 94 */
return;
/* .line 97 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 98 */
final String v0 = "MultiSenceListener"; // const-string v0, "MultiSenceListener"
final String v1 = "action info must not be null"; // const-string v1, "action info must not be null"
android.util.Slog .w ( v0,v1 );
/* .line 99 */
return;
/* .line 102 */
} // :cond_1
final String v0 = "MultisenceListener.onMultiTaskActionStart"; // const-string v0, "MultisenceListener.onMultiTaskActionStart"
/* const-wide/16 v1, 0x20 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 103 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MultiSenceListener;->onMultiTaskActionChanged(ILmiui/smartpower/MultiTaskActionManager$ActionInfo;)V */
/* .line 104 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 105 */
return;
} // .end method
public Boolean systemReady ( ) {
/* .locals 2 */
/* .line 58 */
final String v0 = "miui_multi_sence"; // const-string v0, "miui_multi_sence"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/miui/server/multisence/MultiSenceManagerInternal; */
this.mMultiSenceMI = v0;
/* .line 59 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
/* .line 60 */
v1 = this.mMultiSenceMI;
if ( v1 != null) { // if-eqz v1, :cond_1
/* if-nez v0, :cond_0 */
/* .line 64 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/MultiSenceListener;->isSystemReady:Z */
/* .line 65 */
/* .line 61 */
} // :cond_1
} // :goto_0
final String v0 = "MultiSenceListener"; // const-string v0, "MultiSenceListener"
final String v1 = "MultiSence Listener does not connect to miui_multi_sence or WMS"; // const-string v1, "MultiSence Listener does not connect to miui_multi_sence or WMS"
android.util.Slog .e ( v0,v1 );
/* .line 62 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void updateScreenStatus ( ) {
/* .locals 9 */
/* .line 186 */
v0 = this.mMultiSenceMI;
if ( v0 != null) { // if-eqz v0, :cond_2
/* sget-boolean v0, Lcom/miui/server/multisence/MultiSenceDynamicController;->IS_CLOUD_ENABLED:Z */
/* if-nez v0, :cond_0 */
/* .line 192 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MultiSenceListener;->isConnectToWMS()Z */
/* .line 194 */
/* .local v0, "connection":Z */
/* if-nez v0, :cond_1 */
return;
/* .line 196 */
} // :cond_1
v1 = com.android.server.wm.MultiSenceListener.service;
(( com.android.server.wm.WindowManagerService ) v1 ).getGlobalLock ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;
/* monitor-enter v1 */
/* .line 197 */
try { // :try_start_0
final String v2 = "getWindowsNeedToSched from multi-stack sence"; // const-string v2, "getWindowsNeedToSched from multi-stack sence"
/* const-wide/16 v3, 0x20 */
android.os.Trace .traceBegin ( v3,v4,v2 );
/* .line 198 */
/* invoke-direct {p0}, Lcom/android/server/wm/MultiSenceListener;->getWindowsNeedToSched()Ljava/util/Map; */
/* .line 199 */
/* .local v2, "sWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;" */
android.os.Trace .traceEnd ( v3,v4 );
/* .line 200 */
v3 = com.android.server.wm.MultiSenceListener.service;
v3 = this.mRoot;
(( com.android.server.wm.RootWindowContainer ) v3 ).getTopFocusedDisplayContent ( ); // invoke-virtual {v3}, Lcom/android/server/wm/RootWindowContainer;->getTopFocusedDisplayContent()Lcom/android/server/wm/DisplayContent;
/* .line 201 */
/* .local v3, "displayContent":Lcom/android/server/wm/DisplayContent; */
v4 = this.mCurrentFocus;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 203 */
/* .local v4, "foucsWindow":Lcom/android/server/wm/WindowState; */
try { // :try_start_1
(( com.android.server.wm.WindowState ) v4 ).getOwningPackage ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
/* check-cast v5, Lcom/miui/server/multisence/SingleWindowInfo; */
int v6 = 1; // const/4 v6, 0x1
(( com.miui.server.multisence.SingleWindowInfo ) v5 ).setFocused ( v6 ); // invoke-virtual {v5, v6}, Lcom/miui/server/multisence/SingleWindowInfo;->setFocused(Z)Lcom/miui/server/multisence/SingleWindowInfo;
/* :try_end_1 */
/* .catch Ljava/lang/NullPointerException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 207 */
/* nop */
/* .line 208 */
} // .end local v3 # "displayContent":Lcom/android/server/wm/DisplayContent;
} // .end local v4 # "foucsWindow":Lcom/android/server/wm/WindowState;
try { // :try_start_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 209 */
v3 = this.mMultiSenceMI;
/* monitor-enter v3 */
/* .line 210 */
try { // :try_start_3
v1 = this.mMultiSenceMI;
final String v4 = "multi-task"; // const-string v4, "multi-task"
/* .line 211 */
v1 = this.mMultiSenceMI;
/* .line 212 */
/* monitor-exit v3 */
/* .line 213 */
return;
/* .line 212 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v3 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v1 */
/* .line 204 */
/* .restart local v3 # "displayContent":Lcom/android/server/wm/DisplayContent; */
/* .restart local v4 # "foucsWindow":Lcom/android/server/wm/WindowState; */
/* :catch_0 */
/* move-exception v5 */
/* .line 205 */
/* .local v5, "npe":Ljava/lang/NullPointerException; */
try { // :try_start_4
final String v6 = "MultiSenceListener"; // const-string v6, "MultiSenceListener"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "setFocus error: " */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v7 );
/* .line 206 */
/* monitor-exit v1 */
return;
/* .line 208 */
} // .end local v2 # "sWindows":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/server/multisence/SingleWindowInfo;>;"
} // .end local v3 # "displayContent":Lcom/android/server/wm/DisplayContent;
} // .end local v4 # "foucsWindow":Lcom/android/server/wm/WindowState;
} // .end local v5 # "npe":Ljava/lang/NullPointerException;
/* :catchall_1 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v2 */
/* .line 187 */
} // .end local v0 # "connection":Z
} // :cond_2
} // :goto_0
final String v0 = "MultiSenceListener"; // const-string v0, "MultiSenceListener"
final String v1 = "return updateScreenStatus"; // const-string v1, "return updateScreenStatus"
android.util.Slog .w ( v0,v1 );
/* .line 188 */
return;
} // .end method
