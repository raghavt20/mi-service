public class com.android.server.wm.RecommendDataEntry {
	 /* .source "RecommendDataEntry.java" */
	 /* # static fields */
	 private static final java.lang.String FREEFORM_TASKID;
	 private static final java.lang.String MIUI_RECOMMEND_CONTENT;
	 private static final java.lang.String RECOMMEND_SCENE;
	 public static final Integer RECOMMEND_SCENE_TYPE_GAME_UPDATING;
	 public static final Integer RECOMMEND_SCENE_TYPE_SPLIT_SCREEN;
	 public static final Integer RECOMMEND_SCENE_TYPE_TAXI_WAITING;
	 private static final java.lang.String RECOMMEND_TRANSACTION_TYPE;
	 public static final Integer RECOMMEND_TYPE_FREE_FORM;
	 public static final Integer RECOMMEND_TYPE_FREE_FORM_REMOVE;
	 public static final Integer RECOMMEND_TYPE_SPLIT_SCREEN;
	 private static final java.lang.String SENDER_PACKAGENAME;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.lang.String freeformPackageName;
	 private Integer freeformTaskId;
	 private Integer freeformUserId;
	 private Boolean inFreeFormRecommend;
	 private Boolean inSplitScreenRecommend;
	 private java.lang.String primaryPackageName;
	 private Integer primaryTaskId;
	 private Integer primaryUserId;
	 private Integer recommendSceneType;
	 private Integer recommendTransactionType;
	 private java.lang.String secondaryPackageName;
	 private Integer secondaryTaskId;
	 private Integer secondaryUserId;
	 /* # direct methods */
	 public com.android.server.wm.RecommendDataEntry ( ) {
		 /* .locals 0 */
		 /* .line 46 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 48 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean getFreeFormRecommendState ( ) {
		 /* .locals 1 */
		 /* .line 123 */
		 /* iget-boolean v0, p0, Lcom/android/server/wm/RecommendDataEntry;->inFreeFormRecommend:Z */
	 } // .end method
	 public Integer getFreeFormTaskId ( ) {
		 /* .locals 1 */
		 /* .line 71 */
		 /* iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformTaskId:I */
	 } // .end method
	 public java.lang.String getFreeformPackageName ( ) {
		 /* .locals 1 */
		 /* .line 91 */
		 v0 = this.freeformPackageName;
	 } // .end method
	 public Integer getFreeformUserId ( ) {
		 /* .locals 1 */
		 /* .line 135 */
		 /* iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformUserId:I */
	 } // .end method
	 public java.lang.String getPrimaryPackageName ( ) {
		 /* .locals 1 */
		 /* .line 99 */
		 v0 = this.primaryPackageName;
	 } // .end method
	 public Integer getPrimaryTaskId ( ) {
		 /* .locals 1 */
		 /* .line 79 */
		 /* iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryTaskId:I */
	 } // .end method
	 public Integer getPrimaryUserId ( ) {
		 /* .locals 1 */
		 /* .line 143 */
		 /* iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryUserId:I */
	 } // .end method
	 public Integer getRecommendSceneType ( ) {
		 /* .locals 1 */
		 /* .line 59 */
		 /* iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->recommendSceneType:I */
	 } // .end method
	 public java.lang.String getSecondaryPackageName ( ) {
		 /* .locals 1 */
		 /* .line 107 */
		 v0 = this.secondaryPackageName;
	 } // .end method
	 public Integer getSecondaryTaskId ( ) {
		 /* .locals 1 */
		 /* .line 87 */
		 /* iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryTaskId:I */
	 } // .end method
	 public Integer getSecondaryUserId ( ) {
		 /* .locals 1 */
		 /* .line 151 */
		 /* iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryUserId:I */
	 } // .end method
	 public Boolean getSplitScreenRecommendState ( ) {
		 /* .locals 1 */
		 /* .line 115 */
		 /* iget-boolean v0, p0, Lcom/android/server/wm/RecommendDataEntry;->inSplitScreenRecommend:Z */
	 } // .end method
	 public Integer getTransactionType ( ) {
		 /* .locals 1 */
		 /* .line 51 */
		 /* iget v0, p0, Lcom/android/server/wm/RecommendDataEntry;->recommendTransactionType:I */
	 } // .end method
	 public void setFreeFormRecommendState ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "inFreeFormRecommend" # Z */
		 /* .line 127 */
		 /* iput-boolean p1, p0, Lcom/android/server/wm/RecommendDataEntry;->inFreeFormRecommend:Z */
		 /* .line 128 */
		 return;
	 } // .end method
	 public void setFreeFormTaskId ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "freeformTaskId" # I */
		 /* .line 67 */
		 /* iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformTaskId:I */
		 /* .line 68 */
		 return;
	 } // .end method
	 public void setFreeformPackageName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .line 95 */
		 this.freeformPackageName = p1;
		 /* .line 96 */
		 return;
	 } // .end method
	 public void setFreeformUserId ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "userId" # I */
		 /* .line 131 */
		 /* iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->freeformUserId:I */
		 /* .line 132 */
		 return;
	 } // .end method
	 public void setPrimaryPackageName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .line 103 */
		 this.primaryPackageName = p1;
		 /* .line 104 */
		 return;
	 } // .end method
	 public void setPrimaryTaskId ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "primaryTaskId" # I */
		 /* .line 75 */
		 /* iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryTaskId:I */
		 /* .line 76 */
		 return;
	 } // .end method
	 public void setPrimaryUserId ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "userId" # I */
		 /* .line 139 */
		 /* iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->primaryUserId:I */
		 /* .line 140 */
		 return;
	 } // .end method
	 public void setRecommendSceneType ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "recommendSceneType" # I */
		 /* .line 63 */
		 /* iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->recommendSceneType:I */
		 /* .line 64 */
		 return;
	 } // .end method
	 public void setSecondaryPackageName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .line 111 */
		 this.secondaryPackageName = p1;
		 /* .line 112 */
		 return;
	 } // .end method
	 public void setSecondaryTaskId ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "secondaryTaskId" # I */
		 /* .line 83 */
		 /* iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryTaskId:I */
		 /* .line 84 */
		 return;
	 } // .end method
	 public void setSecondaryUserId ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "userId" # I */
		 /* .line 147 */
		 /* iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->secondaryUserId:I */
		 /* .line 148 */
		 return;
	 } // .end method
	 public void setSplitScreenRecommendState ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "inSplitScreenRecommend" # Z */
		 /* .line 119 */
		 /* iput-boolean p1, p0, Lcom/android/server/wm/RecommendDataEntry;->inSplitScreenRecommend:Z */
		 /* .line 120 */
		 return;
	 } // .end method
	 public void setTransactionType ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "transactionType" # I */
		 /* .line 55 */
		 /* iput p1, p0, Lcom/android/server/wm/RecommendDataEntry;->recommendTransactionType:I */
		 /* .line 56 */
		 return;
	 } // .end method
