class com.android.server.wm.MiuiPaperContrastOverlay$2 extends android.view.IDisplayWindowListener$Stub {
	 /* .source "MiuiPaperContrastOverlay.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiPaperContrastOverlay; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
private android.graphics.Rect mLastBounds;
private android.graphics.Rect mNewBounds;
private java.lang.Runnable runnable;
final com.android.server.wm.MiuiPaperContrastOverlay this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$Q-GGqp9ypaB816z7ZS9OiANlpjk ( com.android.server.wm.MiuiPaperContrastOverlay$2 p0, android.content.res.Configuration p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiPaperContrastOverlay$2;->lambda$onDisplayConfigurationChanged$0(Landroid/content/res/Configuration;I)V */
return;
} // .end method
 com.android.server.wm.MiuiPaperContrastOverlay$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiPaperContrastOverlay; */
/* .line 210 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/view/IDisplayWindowListener$Stub;-><init>()V */
return;
} // .end method
private void lambda$onDisplayConfigurationChanged$0 ( android.content.res.Configuration p0, Integer p1 ) { //synthethic
/* .locals 5 */
/* .param p1, "newConfig" # Landroid/content/res/Configuration; */
/* .param p2, "displayId" # I */
/* .line 233 */
v0 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$mupdateDisplaySize ( v0 );
/* .line 234 */
v0 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmLastConfiguration ( v0 );
v0 = (( android.content.res.Configuration ) p1 ).diff ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I
/* .line 235 */
/* .local v0, "diff":I */
v1 = this.this$0;
v1 = com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$misSizeChangeHappened ( v1 );
/* .line 237 */
/* .local v1, "isChanged":Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Config size change is "; // const-string v3, "Config size change is "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", from ["; // const-string v3, ", from ["
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLastBounds;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ", "; // const-string v3, ", "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmLastConfiguration ( v4 );
v4 = this.windowConfiguration;
/* .line 238 */
v4 = (( android.app.WindowConfiguration ) v4 ).getRotation ( ); // invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getRotation()I
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "] to ["; // const-string v4, "] to ["
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mNewBounds;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.windowConfiguration;
/* .line 239 */
v4 = (( android.app.WindowConfiguration ) v4 ).getRotation ( ); // invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getRotation()I
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "], "; // const-string v4, "], "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* and-int/lit16 v4, v0, 0x400 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 237 */
final String v3 = "MiuiPaperContrastOverlay"; // const-string v3, "MiuiPaperContrastOverlay"
android.util.Slog .i ( v3,v2 );
/* .line 246 */
/* and-int/lit16 v2, v0, 0x80 */
/* if-nez v2, :cond_0 */
/* if-nez v1, :cond_1 */
} // :cond_0
v2 = this.mLastBounds;
/* .line 250 */
v2 = (( android.graphics.Rect ) v2 ).height ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->height()I
v3 = this.mNewBounds;
v3 = (( android.graphics.Rect ) v3 ).width ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->width()I
/* if-eq v2, v3, :cond_2 */
v2 = this.mLastBounds;
/* .line 251 */
v2 = (( android.graphics.Rect ) v2 ).width ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->width()I
v3 = this.mNewBounds;
v3 = (( android.graphics.Rect ) v3 ).height ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->height()I
/* if-eq v2, v3, :cond_2 */
/* and-int/lit16 v2, v0, 0x400 */
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.mNewBounds;
v3 = this.mLastBounds;
/* .line 253 */
v2 = (( android.graphics.Rect ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
/* .line 255 */
} // :cond_1
v2 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$mdestroySurface ( v2 );
/* .line 256 */
v2 = this.this$0;
v2 = com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmNeedShowPaperSurface ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 257 */
v2 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$mcreateSurface ( v2 );
/* .line 258 */
v2 = this.this$0;
(( com.android.server.wm.MiuiPaperContrastOverlay ) v2 ).showPaperModeSurface ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->showPaperModeSurface()V
/* .line 262 */
} // :cond_2
v2 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmLastConfiguration ( v2 );
(( android.content.res.Configuration ) v2 ).setTo ( p1 ); // invoke-virtual {v2, p1}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V
/* .line 263 */
return;
} // .end method
/* # virtual methods */
public void onDisplayAdded ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 270 */
return;
} // .end method
public void onDisplayConfigurationChanged ( Integer p0, android.content.res.Configuration p1 ) {
/* .locals 6 */
/* .param p1, "displayId" # I */
/* .param p2, "newConfig" # Landroid/content/res/Configuration; */
/* .line 218 */
v0 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmLastConfiguration ( v0 );
v0 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v0 ).getBounds ( ); // invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* .line 219 */
/* .local v0, "lastBounds":Landroid/graphics/Rect; */
v1 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v1 ).getBounds ( ); // invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* .line 221 */
/* .local v1, "newBounds":Landroid/graphics/Rect; */
v2 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmContext ( v2 );
v2 = (( android.content.Context ) v2 ).getDisplayId ( ); // invoke-virtual {v2}, Landroid/content/Context;->getDisplayId()I
/* if-eq v2, p1, :cond_0 */
v2 = this.this$0;
v2 = com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmFoldDeviceReady ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 222 */
v2 = (( android.graphics.Rect ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
	 return;
	 /* .line 224 */
} // :cond_0
v2 = this.this$0;
int v3 = 1; // const/4 v3, 0x1
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fputmFoldDeviceReady ( v2,v3 );
/* .line 225 */
this.mLastBounds = v0;
/* .line 226 */
this.mNewBounds = v1;
/* .line 228 */
v2 = this.runnable;
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 229 */
	 v2 = this.this$0;
	 com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmWms ( v2 );
	 v2 = this.mH;
	 v3 = this.runnable;
	 (( com.android.server.wm.WindowManagerService$H ) v2 ).removeCallbacks ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/WindowManagerService$H;->removeCallbacks(Ljava/lang/Runnable;)V
	 /* .line 232 */
} // :cond_1
/* new-instance v2, Lcom/android/server/wm/MiuiPaperContrastOverlay$2$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0, p2, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlay$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay$2;Landroid/content/res/Configuration;I)V */
this.runnable = v2;
/* .line 264 */
v2 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmWms ( v2 );
v2 = this.mH;
v3 = this.runnable;
/* const-wide/16 v4, 0xc8 */
(( com.android.server.wm.WindowManagerService$H ) v2 ).postDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/wm/WindowManagerService$H;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 265 */
return;
} // .end method
public void onDisplayRemoved ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 273 */
return;
} // .end method
public void onFixedRotationFinished ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 279 */
return;
} // .end method
public void onFixedRotationStarted ( Integer p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .param p2, "newRotation" # I */
/* .line 276 */
return;
} // .end method
public void onKeepClearAreasChanged ( Integer p0, java.util.List p1, java.util.List p2 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/List<", */
/* "Landroid/graphics/Rect;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Landroid/graphics/Rect;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 283 */
/* .local p2, "restricted":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;" */
/* .local p3, "unrestricted":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;" */
return;
} // .end method
