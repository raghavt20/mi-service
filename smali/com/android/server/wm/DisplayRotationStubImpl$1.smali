.class Lcom/android/server/wm/DisplayRotationStubImpl$1;
.super Ljava/lang/Object;
.source "DisplayRotationStubImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/DisplayRotationStubImpl;->writeRotationChangeToProperties(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/DisplayRotationStubImpl;

.field final synthetic val$finalRotation:I


# direct methods
.method constructor <init>(Lcom/android/server/wm/DisplayRotationStubImpl;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/DisplayRotationStubImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 309
    iput-object p1, p0, Lcom/android/server/wm/DisplayRotationStubImpl$1;->this$0:Lcom/android/server/wm/DisplayRotationStubImpl;

    iput p2, p0, Lcom/android/server/wm/DisplayRotationStubImpl$1;->val$finalRotation:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 313
    :try_start_0
    const-string/jumbo v0, "sys.tp.grip_enable"

    iget v1, p0, Lcom/android/server/wm/DisplayRotationStubImpl$1;->val$finalRotation:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    goto :goto_0

    .line 314
    :catch_0
    move-exception v0

    .line 315
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "WindowManager"

    const-string/jumbo v2, "set SystemProperties error."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 317
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
