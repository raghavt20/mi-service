class com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation extends android.view.animation.ScaleAnimation {
	 /* .source "ScreenRotationAnimationImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/ScreenRotationAnimationImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ScreenScale180Animation" */
} // .end annotation
/* # direct methods */
public com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ( ) {
/* .locals 0 */
/* .param p1, "fromX" # F */
/* .param p2, "toX" # F */
/* .param p3, "fromY" # F */
/* .param p4, "toY" # F */
/* .param p5, "pivotXType" # I */
/* .param p6, "pivotXValue" # F */
/* .param p7, "pivotYType" # I */
/* .param p8, "pivotYValue" # F */
/* .line 834 */
/* invoke-direct/range {p0 ..p8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V */
/* .line 835 */
return;
} // .end method
/* # virtual methods */
public Boolean getTransformation ( Long p0, android.view.animation.Transformation p1 ) {
/* .locals 4 */
/* .param p1, "currentTime" # J */
/* .param p3, "outTransformation" # Landroid/view/animation/Transformation; */
/* .line 839 */
(( com.android.server.wm.ScreenRotationAnimationImpl$ScreenScale180Animation ) p0 ).getDuration ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->getDuration()J
/* move-result-wide v0 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 840 */
	 v0 = 	 /* invoke-super {p0, p1, p2, p3}, Landroid/view/animation/ScaleAnimation;->getTransformation(JLandroid/view/animation/Transformation;)Z */
	 /* .line 843 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
