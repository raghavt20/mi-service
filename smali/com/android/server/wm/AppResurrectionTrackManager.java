public class com.android.server.wm.AppResurrectionTrackManager {
	 /* .source "AppResurrectionTrackManager.java" */
	 /* # static fields */
	 private static final java.lang.String APP_ID;
	 private static final Integer FLAG_NON_ANONYMOUS;
	 private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
	 private static final java.lang.String PACKAGE_NAME;
	 private static final java.lang.String SERVICE_NAME;
	 private static final java.lang.String SERVICE_PACKAGE_NAME;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean DEBUG;
	 private final android.content.ServiceConnection mConnection;
	 private final android.content.Context mContext;
	 private com.miui.analytics.ITrackBinder mITrackBinder;
	 private Boolean mIsBind;
	 /* # direct methods */
	 static com.miui.analytics.ITrackBinder -$$Nest$fgetmITrackBinder ( com.android.server.wm.AppResurrectionTrackManager p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mITrackBinder;
	 } // .end method
	 static void -$$Nest$fputmITrackBinder ( com.android.server.wm.AppResurrectionTrackManager p0, com.miui.analytics.ITrackBinder p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mITrackBinder = p1;
		 return;
	 } // .end method
	 static void -$$Nest$fputmIsBind ( com.android.server.wm.AppResurrectionTrackManager p0, Boolean p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iput-boolean p1, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mIsBind:Z */
		 return;
	 } // .end method
	 public com.android.server.wm.AppResurrectionTrackManager ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 51 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 23 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->DEBUG:Z */
		 /* .line 36 */
		 /* new-instance v0, Lcom/android/server/wm/AppResurrectionTrackManager$1; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/wm/AppResurrectionTrackManager$1;-><init>(Lcom/android/server/wm/AppResurrectionTrackManager;)V */
		 this.mConnection = v0;
		 /* .line 52 */
		 this.mContext = p1;
		 /* .line 53 */
		 return;
	 } // .end method
	 private void trackEvent ( org.json.JSONObject p0 ) {
		 /* .locals 6 */
		 /* .param p1, "jsonData" # Lorg/json/JSONObject; */
		 /* .line 99 */
		 v0 = this.mITrackBinder;
		 final String v1 = "AppResurrectionTrackManager"; // const-string v1, "AppResurrectionTrackManager"
		 /* if-nez v0, :cond_0 */
		 /* .line 100 */
		 final String v0 = "TrackService Not Bound,Please Try Again"; // const-string v0, "TrackService Not Bound,Please Try Again"
		 android.util.Slog .d ( v1,v0 );
		 /* .line 101 */
		 (( com.android.server.wm.AppResurrectionTrackManager ) p0 ).bindTrackBinder ( ); // invoke-virtual {p0}, Lcom/android/server/wm/AppResurrectionTrackManager;->bindTrackBinder()V
		 /* .line 102 */
		 return;
		 /* .line 104 */
	 } // :cond_0
	 /* if-nez p1, :cond_1 */
	 /* .line 105 */
	 final String v0 = "JSONObject is null"; // const-string v0, "JSONObject is null"
	 android.util.Slog .w ( v1,v0 );
	 /* .line 106 */
	 return;
	 /* .line 109 */
} // :cond_1
try { // :try_start_0
	 final String v2 = "31000401629"; // const-string v2, "31000401629"
	 final String v3 = "android"; // const-string v3, "android"
	 (( org.json.JSONObject ) p1 ).toString ( ); // invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
	 int v5 = 3; // const/4 v5, 0x3
	 /* .line 110 */
	 /* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->DEBUG:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 111 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "Send to TrackService Success:"; // const-string v2, "Send to TrackService Success:"
		 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( org.json.JSONObject ) p1 ).toString ( ); // invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v1,v0 );
		 /* .line 113 */
	 } // :cond_2
	 final String v0 = "Binder to Success"; // const-string v0, "Binder to Success"
	 android.util.Slog .v ( v1,v0 );
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 117 */
} // :goto_0
/* .line 115 */
/* :catch_0 */
/* move-exception v0 */
/* .line 116 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "trackEvent: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.os.RemoteException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 118 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void bindTrackBinder ( ) {
/* .locals 5 */
/* .line 56 */
v0 = this.mITrackBinder;
/* if-nez v0, :cond_0 */
/* .line 58 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 59 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
final String v2 = "com.miui.analytics.onetrack.TrackService"; // const-string v2, "com.miui.analytics.onetrack.TrackService"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 60 */
v1 = this.mContext;
v2 = this.mConnection;
v3 = android.os.UserHandle.CURRENT;
int v4 = 1; // const/4 v4, 0x1
v1 = (( android.content.Context ) v1 ).bindServiceAsUser ( v0, v2, v4, v3 ); // invoke-virtual {v1, v0, v2, v4, v3}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
/* iput-boolean v1, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mIsBind:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 64 */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 61 */
/* :catch_0 */
/* move-exception v0 */
/* .line 62 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 63 */
final String v1 = "AppResurrectionTrackManager"; // const-string v1, "AppResurrectionTrackManager"
final String v2 = "Bind Service Exception"; // const-string v2, "Bind Service Exception"
android.util.Slog .e ( v1,v2 );
/* .line 66 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
public void sendTrack ( java.lang.String p0, Long p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "launchTime" # J */
/* .param p4, "killReason" # Ljava/lang/String; */
/* .line 81 */
final String v2 = ""; // const-string v2, ""
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-wide v3, p2 */
/* move-object v5, p4 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/wm/AppResurrectionTrackManager;->sendTrack(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V */
/* .line 82 */
return;
} // .end method
public void sendTrack ( java.lang.String p0, java.lang.String p1, Long p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "pkgVersion" # Ljava/lang/String; */
/* .param p3, "launchTime" # J */
/* .param p5, "killReason" # Ljava/lang/String; */
/* .line 85 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 87 */
/* .local v0, "jsonData":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
final String v2 = "event_appres_launch_time"; // const-string v2, "event_appres_launch_time"
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 88 */
final String v1 = "pkg_name"; // const-string v1, "pkg_name"
(( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 89 */
final String v1 = "pkg_version"; // const-string v1, "pkg_version"
(( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 90 */
final String v1 = "kill_reason"; // const-string v1, "kill_reason"
(( org.json.JSONObject ) v0 ).put ( v1, p5 ); // invoke-virtual {v0, v1, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 91 */
final String v1 = "launch_time_num"; // const-string v1, "launch_time_num"
(( org.json.JSONObject ) v0 ).put ( v1, p3, p4 ); // invoke-virtual {v0, v1, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 94 */
/* .line 92 */
/* :catch_0 */
/* move-exception v1 */
/* .line 93 */
/* .local v1, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
/* .line 95 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
/* invoke-direct {p0, v0}, Lcom/android/server/wm/AppResurrectionTrackManager;->trackEvent(Lorg/json/JSONObject;)V */
/* .line 96 */
return;
} // .end method
public void unbindTrackBinder ( ) {
/* .locals 3 */
/* .line 69 */
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mIsBind:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 71 */
try { // :try_start_0
v0 = this.mContext;
v1 = this.mConnection;
(( android.content.Context ) v0 ).unbindService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 75 */
/* .line 72 */
/* :catch_0 */
/* move-exception v0 */
/* .line 73 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 74 */
final String v1 = "AppResurrectionTrackManager"; // const-string v1, "AppResurrectionTrackManager"
final String v2 = "Unbind Service Exception"; // const-string v2, "Unbind Service Exception"
android.util.Slog .e ( v1,v2 );
/* .line 77 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
