abstract class com.android.server.wm.PolicyImpl {
	 /* .source "PolicyImpl.java" */
	 /* # static fields */
	 private static final java.lang.String AUTHORITY;
	 private static final android.net.Uri AUTHORITY_URI;
	 private static final java.util.Map CALLBACKS;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/util/List<", */
	 /* "Ljava/util/function/Consumer<", */
	 /* "Ljava/util/concurrent/ConcurrentHashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;>;>;>;" */
	 /* } */
} // .end annotation
} // .end field
private static final Integer INDEX_CONFIGURATION_NAME;
private static final Integer INDEX_CONFIGURATION_VALUE;
private static final Integer INDEX_PACKAGE_NAME;
private static final java.lang.String POLICY_ITEM;
private static final java.lang.String POLICY_LIST;
private static final java.lang.String POLICY_LIST_PROJECTION;
private static final java.lang.String POLICY_LIST_SELECTION;
private static final android.net.Uri POLICY_LIST_URI;
private static final Integer RETRY_NUMBER;
private static final Long RETRY_TIME_OUT;
private static final java.lang.String TAG;
/* # instance fields */
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private final com.android.server.wm.PackageConfigurationController mController;
private java.util.List mList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/wm/PackageConfiguration;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.String mName;
private com.android.server.wm.PolicyItem mPolicyItem;
private Integer mRetryNumber;
/* # direct methods */
public static void $r8$lambda$MEBaMqmM1drI2U0bvDLMLdratpc ( com.android.server.wm.PolicyImpl p0, com.android.server.wm.PackageConfiguration p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/PolicyImpl;->lambda$init$1(Lcom/android/server/wm/PackageConfiguration;)V */
return;
} // .end method
public static void $r8$lambda$cGov_C-B0bsh28ndWHu4wDqVMVY ( com.android.server.wm.PolicyImpl p0, com.android.server.wm.PackageConfiguration p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/PolicyImpl;->lambda$updateDataMapFromCloud$0(Lcom/android/server/wm/PackageConfiguration;)V */
return;
} // .end method
public static void $r8$lambda$hHmS8NowQw5dluNSESrbBxUVa7w ( com.android.server.wm.PolicyImpl p0, com.android.server.wm.PackageConfiguration p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/PolicyImpl;->lambda$propagateToCallbacks$3(Lcom/android/server/wm/PackageConfiguration;)V */
return;
} // .end method
static com.android.server.wm.PolicyImpl ( ) {
/* .locals 2 */
/* .line 34 */
final String v0 = "com.miui.android.sm.policy"; // const-string v0, "com.miui.android.sm.policy"
android.net.Uri .parse ( v0 );
/* .line 37 */
final String v1 = "policyVersion"; // const-string v1, "policyVersion"
/* filled-new-array {v1}, [Ljava/lang/String; */
/* .line 39 */
final String v1 = "policy_list"; // const-string v1, "policy_list"
android.net.Uri .withAppendedPath ( v0,v1 );
/* .line 42 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
return;
} // .end method
 com.android.server.wm.PolicyImpl ( ) {
/* .locals 1 */
/* .param p1, "controller" # Lcom/android/server/wm/PackageConfigurationController; */
/* .param p2, "policyName" # Ljava/lang/String; */
/* .line 57 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
int v0 = 3; // const/4 v0, 0x3
/* iput v0, p0, Lcom/android/server/wm/PolicyImpl;->mRetryNumber:I */
/* .line 58 */
this.mController = p1;
/* .line 59 */
this.mName = p2;
/* .line 60 */
v0 = this.mAtmService;
v0 = this.mContext;
this.mContext = v0;
/* .line 61 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v0;
/* .line 62 */
return;
} // .end method
private void lambda$init$1 ( com.android.server.wm.PackageConfiguration p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "pkgConfig" # Lcom/android/server/wm/PackageConfiguration; */
/* .line 181 */
(( com.android.server.wm.PackageConfiguration ) p1 ).getPolicyDataMap ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PackageConfiguration;->getPolicyDataMap()Ljava/util/concurrent/ConcurrentHashMap;
/* .line 182 */
/* .local v0, "policyMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;" */
v1 = this.mName;
(( com.android.server.wm.PolicyImpl ) p0 ).getPolicyDataMapFromLocal ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/PolicyImpl;->getPolicyDataMapFromLocal(Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)V
/* .line 183 */
return;
} // .end method
static void lambda$propagateToCallbacks$2 ( java.util.List p0, java.util.concurrent.ConcurrentHashMap p1 ) { //synthethic
/* .locals 3 */
/* .param p0, "consumerMap" # Ljava/util/List; */
/* .param p1, "policyMap" # Ljava/util/concurrent/ConcurrentHashMap; */
/* .line 208 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = try { // :try_start_0
/* if-ge v0, v1, :cond_0 */
/* .line 209 */
/* check-cast v1, Ljava/util/function/Consumer; */
/* .line 210 */
/* .local v1, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;" */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 208 */
} // .end local v1 # "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
/* add-int/lit8 v0, v0, 0x1 */
/* .line 214 */
} // .end local v0 # "i":I
} // :cond_0
/* .line 212 */
/* :catch_0 */
/* move-exception v0 */
/* .line 213 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "PolicyImpl"; // const-string v1, "PolicyImpl"
final String v2 = "propagateToCallbacks"; // const-string v2, "propagateToCallbacks"
android.util.Slog .d ( v1,v2,v0 );
/* .line 215 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void lambda$propagateToCallbacks$3 ( com.android.server.wm.PackageConfiguration p0 ) { //synthethic
/* .locals 4 */
/* .param p1, "pkgConfig" # Lcom/android/server/wm/PackageConfiguration; */
/* .line 196 */
v0 = com.android.server.wm.PolicyImpl.CALLBACKS;
v1 = this.mName;
/* check-cast v0, Ljava/util/List; */
/* .line 197 */
/* .local v0, "consumerMap":Ljava/util/List;, "Ljava/util/List<Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;>;" */
/* if-nez v0, :cond_0 */
/* .line 198 */
return;
/* .line 200 */
} // :cond_0
/* new-instance v1, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
/* .line 201 */
/* .local v1, "policyMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;" */
v2 = this.mController;
/* iget-boolean v2, v2, Lcom/android/server/wm/PackageConfigurationController;->mPolicyDisabled:Z */
/* if-nez v2, :cond_1 */
v2 = this.mName;
/* .line 202 */
v2 = (( com.android.server.wm.PolicyImpl ) p0 ).isDisabledConfiguration ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/PolicyImpl;->isDisabledConfiguration(Ljava/lang/String;)Z
/* if-nez v2, :cond_1 */
/* .line 203 */
(( com.android.server.wm.PackageConfiguration ) p1 ).getPolicyDataMap ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PackageConfiguration;->getPolicyDataMap()Ljava/util/concurrent/ConcurrentHashMap;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).putAll ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->putAll(Ljava/util/Map;)V
/* .line 206 */
} // :cond_1
v2 = this.mController;
v2 = this.mAtmService;
v2 = this.mH;
/* new-instance v3, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, v0, v1}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda0;-><init>(Ljava/util/List;Ljava/util/concurrent/ConcurrentHashMap;)V */
(( com.android.server.wm.ActivityTaskManagerService$H ) v2 ).post ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 216 */
return;
} // .end method
private void lambda$updateDataMapFromCloud$0 ( com.android.server.wm.PackageConfiguration p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "pkgConfig" # Lcom/android/server/wm/PackageConfiguration; */
/* .line 143 */
(( com.android.server.wm.PackageConfiguration ) p1 ).getPolicyDataMap ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PackageConfiguration;->getPolicyDataMap()Ljava/util/concurrent/ConcurrentHashMap;
/* .line 144 */
/* .local v0, "policyMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;" */
v1 = this.mName;
(( com.android.server.wm.PolicyImpl ) p0 ).getPolicyDataMapFromCloud ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/PolicyImpl;->getPolicyDataMapFromCloud(Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)V
/* .line 145 */
return;
} // .end method
static void lambda$updatePackageConfigurationsIfNeeded$4 ( com.android.server.wm.PackageConfiguration p0 ) { //synthethic
/* .locals 0 */
/* .param p0, "pkgConfig" # Lcom/android/server/wm/PackageConfiguration; */
/* .line 258 */
(( com.android.server.wm.PackageConfiguration ) p0 ).updatePrepared ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PackageConfiguration;->updatePrepared()V
/* .line 259 */
return;
} // .end method
static void lambda$updatePackageConfigurationsIfNeeded$5 ( java.lang.String p0, java.lang.String p1, java.lang.String p2, com.android.server.wm.PackageConfiguration p3 ) { //synthethic
/* .locals 1 */
/* .param p0, "key" # Ljava/lang/String; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .param p3, "pkgConfig" # Lcom/android/server/wm/PackageConfiguration; */
/* .line 266 */
v0 = this.mName;
v0 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 267 */
(( com.android.server.wm.PackageConfiguration ) p3 ).updateFromScpm ( p1, p2 ); // invoke-virtual {p3, p1, p2}, Lcom/android/server/wm/PackageConfiguration;->updateFromScpm(Ljava/lang/String;Ljava/lang/String;)V
/* .line 269 */
} // :cond_0
return;
} // .end method
static void lambda$updatePackageConfigurationsIfNeeded$6 ( com.android.server.wm.PackageConfiguration p0 ) { //synthethic
/* .locals 0 */
/* .param p0, "pkgConfig" # Lcom/android/server/wm/PackageConfiguration; */
/* .line 273 */
(( com.android.server.wm.PackageConfiguration ) p0 ).updateCompleted ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PackageConfiguration;->updateCompleted()V
/* .line 274 */
return;
} // .end method
private void registerDataObserver ( ) {
/* .locals 4 */
/* .line 120 */
(( com.android.server.wm.PolicyImpl ) p0 ).updateDataMapFromCloud ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->updateDataMapFromCloud()V
/* .line 121 */
v0 = this.mContentResolver;
/* .line 122 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/wm/PolicyImpl$1; */
/* .line 123 */
com.android.server.MiuiBgThread .getHandler ( );
/* invoke-direct {v2, p0, v3}, Lcom/android/server/wm/PolicyImpl$1;-><init>(Lcom/android/server/wm/PolicyImpl;Landroid/os/Handler;)V */
/* .line 121 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 130 */
return;
} // .end method
/* # virtual methods */
void executeDebugModeLocked ( java.io.PrintWriter p0, java.lang.String[] p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 7 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "pkgNames" # [Ljava/lang/String; */
/* .param p3, "value" # Ljava/lang/String; */
/* .param p4, "configurationName" # Ljava/lang/String; */
/* .line 69 */
v0 = v0 = com.android.server.wm.PolicyImpl.CALLBACKS;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 70 */
final String v0 = "Can not execute, There is no registered callback."; // const-string v0, "Can not execute, There is no registered callback."
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 71 */
return;
/* .line 73 */
} // :cond_0
v0 = this.mController;
/* iget-boolean v0, v0, Lcom/android/server/wm/PackageConfigurationController;->mPolicyDisabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 74 */
final String v0 = "Policy is disabled."; // const-string v0, "Policy is disabled."
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 75 */
return;
/* .line 77 */
} // :cond_1
v0 = (( com.android.server.wm.PolicyImpl ) p0 ).isDisabledConfiguration ( p4 ); // invoke-virtual {p0, p4}, Lcom/android/server/wm/PolicyImpl;->isDisabledConfiguration(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 78 */
final String v0 = "Configuration is disabled."; // const-string v0, "Configuration is disabled."
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 79 */
return;
/* .line 81 */
} // :cond_2
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
/* .line 82 */
/* .local v0, "tmpPolicyDataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 83 */
/* .local v1, "tmpPolicyRemoveList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, p2 */
/* if-ge v2, v3, :cond_6 */
/* .line 84 */
final String v3 = "fullscreenpackage"; // const-string v3, "fullscreenpackage"
v3 = (( java.lang.String ) v3 ).equals ( p4 ); // invoke-virtual {v3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_3 */
/* .line 85 */
final String v3 = "fullscreencomponent"; // const-string v3, "fullscreencomponent"
v3 = (( java.lang.String ) v3 ).equals ( p4 ); // invoke-virtual {v3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
} // :cond_3
/* nop */
/* .line 86 */
final String v3 = "--remove"; // const-string v3, "--remove"
v3 = (( java.lang.String ) p3 ).equals ( v3 ); // invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_5 */
final String v3 = "0"; // const-string v3, "0"
v3 = (( java.lang.String ) p3 ).equals ( v3 ); // invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 89 */
} // :cond_4
/* aget-object v3, p2, v2 */
(( java.util.concurrent.ConcurrentHashMap ) v0 ).put ( v3, p3 ); // invoke-virtual {v0, v3, p3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 87 */
} // :cond_5
} // :goto_1
/* aget-object v3, p2, v2 */
/* .line 83 */
} // :goto_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 93 */
} // .end local v2 # "i":I
} // :cond_6
v2 = this.mList;
v3 = } // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_9
/* check-cast v3, Lcom/android/server/wm/PackageConfiguration; */
/* .line 94 */
/* .local v3, "pkgConfig":Lcom/android/server/wm/PackageConfiguration; */
v4 = this.mName;
v4 = (( java.lang.String ) v4 ).equals ( p4 ); // invoke-virtual {v4, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 95 */
(( com.android.server.wm.PackageConfiguration ) v3 ).getPolicyDataMap ( ); // invoke-virtual {v3}, Lcom/android/server/wm/PackageConfiguration;->getPolicyDataMap()Ljava/util/concurrent/ConcurrentHashMap;
/* .line 98 */
/* .local v2, "dataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;" */
v5 = } // :goto_4
if ( v5 != null) { // if-eqz v5, :cond_7
/* check-cast v5, Ljava/lang/String; */
/* .line 99 */
/* .local v5, "name":Ljava/lang/String; */
final String v6 = "-1"; // const-string v6, "-1"
(( java.util.concurrent.ConcurrentHashMap ) v2 ).put ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 100 */
} // .end local v5 # "name":Ljava/lang/String;
/* .line 101 */
} // :cond_7
(( java.util.concurrent.ConcurrentHashMap ) v2 ).putAll ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putAll(Ljava/util/Map;)V
/* .line 102 */
/* .line 104 */
} // .end local v2 # "dataMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v3 # "pkgConfig":Lcom/android/server/wm/PackageConfiguration;
} // :cond_8
/* .line 106 */
} // :cond_9
} // :goto_5
v2 = this.mPolicyItem;
final String v3 = "Modified"; // const-string v3, "Modified"
(( com.android.server.wm.PolicyItem ) v2 ).setCurrentVersion ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/PolicyItem;->setCurrentVersion(Ljava/lang/String;)V
/* .line 107 */
(( com.android.server.wm.PolicyImpl ) p0 ).propagateToCallbacks ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V
/* .line 108 */
return;
} // .end method
Boolean executeShellCommandLocked ( java.lang.String p0, java.lang.String[] p1, java.io.PrintWriter p2 ) {
/* .locals 1 */
/* .param p1, "command" # Ljava/lang/String; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 111 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
abstract Integer getLocalVersion ( ) {
} // .end method
abstract void getPolicyDataMapFromCloud ( java.lang.String p0, java.util.concurrent.ConcurrentHashMap p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
} // .end method
abstract void getPolicyDataMapFromLocal ( java.lang.String p0, java.util.concurrent.ConcurrentHashMap p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
} // .end method
public java.lang.String getPolicyName ( ) {
/* .locals 1 */
/* .line 65 */
v0 = this.mName;
} // .end method
Integer getScpmVersionFromQuery ( ) {
/* .locals 6 */
/* .line 152 */
v0 = this.mContentResolver;
v1 = com.android.server.wm.PolicyImpl.POLICY_LIST_URI;
v2 = com.android.server.wm.PolicyImpl.POLICY_LIST_PROJECTION;
final String v3 = "policyName=?"; // const-string v3, "policyName=?"
v4 = this.mName;
/* filled-new-array {v4}, [Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .line 153 */
/* invoke-virtual/range {v0 ..v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 154 */
/* .local v0, "cursor":Landroid/database/Cursor; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 155 */
/* .line 157 */
v2 = } // :cond_0
/* if-gtz v2, :cond_1 */
/* .line 158 */
/* .line 159 */
/* .line 161 */
} // :cond_1
/* .line 162 */
/* .line 163 */
/* .local v1, "cursorValue":Ljava/lang/String; */
/* .line 164 */
v2 = java.lang.Integer .parseInt ( v1 );
} // .end method
void init ( ) {
/* .locals 5 */
/* .line 169 */
v1 = v0 = com.android.server.wm.PolicyImpl.CALLBACKS;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 170 */
return;
/* .line 172 */
} // :cond_0
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* move-object v0, v1 */
/* .line 173 */
/* .local v0, "callbackNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
v1 = this.mPolicyItem;
/* if-nez v1, :cond_1 */
/* .line 174 */
/* new-instance v1, Lcom/android/server/wm/PolicyItem; */
/* invoke-direct {v1, v0}, Lcom/android/server/wm/PolicyItem;-><init>(Ljava/util/Set;)V */
this.mPolicyItem = v1;
/* .line 176 */
} // :cond_1
v1 = this.mPolicyItem;
(( com.android.server.wm.PolicyItem ) v1 ).getPackageConfigurationList ( ); // invoke-virtual {v1}, Lcom/android/server/wm/PolicyItem;->getPackageConfigurationList()Ljava/util/List;
this.mList = v1;
/* .line 177 */
v1 = this.mPolicyItem;
v2 = (( com.android.server.wm.PolicyImpl ) p0 ).getLocalVersion ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->getLocalVersion()I
(( com.android.server.wm.PolicyItem ) v1 ).setLocalVersion ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/PolicyItem;->setLocalVersion(I)V
/* .line 178 */
v1 = this.mPolicyItem;
v1 = (( com.android.server.wm.PolicyItem ) v1 ).getScpmVersion ( ); // invoke-virtual {v1}, Lcom/android/server/wm/PolicyItem;->getScpmVersion()I
v2 = this.mPolicyItem;
v2 = (( com.android.server.wm.PolicyItem ) v2 ).getLocalVersion ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PolicyItem;->getLocalVersion()I
/* if-ge v1, v2, :cond_2 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* .line 179 */
/* .local v1, "version":I */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 180 */
v2 = this.mList;
/* new-instance v3, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda5; */
/* invoke-direct {v3, p0}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/PolicyImpl;)V */
/* .line 184 */
v2 = this.mPolicyItem;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.mPolicyItem;
v4 = (( com.android.server.wm.PolicyItem ) v4 ).getLocalVersion ( ); // invoke-virtual {v4}, Lcom/android/server/wm/PolicyItem;->getLocalVersion()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "(LOCAL)"; // const-string v4, "(LOCAL)"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.wm.PolicyItem ) v2 ).setCurrentVersion ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/PolicyItem;->setCurrentVersion(Ljava/lang/String;)V
/* .line 186 */
} // :cond_3
(( com.android.server.wm.PolicyImpl ) p0 ).propagateToCallbacks ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V
/* .line 187 */
/* invoke-direct {p0}, Lcom/android/server/wm/PolicyImpl;->registerDataObserver()V */
/* .line 188 */
return;
} // .end method
Boolean isDisabledConfiguration ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "configurationName" # Ljava/lang/String; */
/* .line 191 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
void propagateToCallbacks ( ) {
/* .locals 2 */
/* .line 195 */
v0 = this.mList;
/* new-instance v1, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/PolicyImpl;)V */
/* .line 217 */
return;
} // .end method
void registerCallback ( java.lang.String p0, java.util.function.Consumer p1 ) {
/* .locals 3 */
/* .param p1, "callbackName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;>;)V" */
/* } */
} // .end annotation
/* .line 220 */
/* .local p2, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;" */
v0 = com.android.server.wm.PolicyImpl.CALLBACKS;
/* check-cast v1, Ljava/util/List; */
/* .line 221 */
/* .local v1, "callbackList":Ljava/util/List;, "Ljava/util/List<Ljava/util/function/Consumer<Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/String;>;>;>;" */
/* if-nez v1, :cond_0 */
/* .line 222 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v1, v2 */
/* .line 225 */
} // :cond_0
/* .line 226 */
/* .line 227 */
return;
} // .end method
void updateDataMapFromCloud ( ) {
/* .locals 4 */
/* .line 133 */
v1 = v0 = com.android.server.wm.PolicyImpl.CALLBACKS;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 134 */
return;
/* .line 136 */
} // :cond_0
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* move-object v0, v1 */
/* .line 137 */
/* .local v0, "callbackNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
v1 = this.mPolicyItem;
/* if-nez v1, :cond_1 */
/* .line 138 */
/* new-instance v1, Lcom/android/server/wm/PolicyItem; */
/* invoke-direct {v1, v0}, Lcom/android/server/wm/PolicyItem;-><init>(Ljava/util/Set;)V */
this.mPolicyItem = v1;
/* .line 140 */
} // :cond_1
v1 = this.mPolicyItem;
(( com.android.server.wm.PolicyItem ) v1 ).getPackageConfigurationList ( ); // invoke-virtual {v1}, Lcom/android/server/wm/PolicyItem;->getPackageConfigurationList()Ljava/util/List;
this.mList = v1;
/* .line 141 */
v1 = this.mPolicyItem;
v2 = (( com.android.server.wm.PolicyImpl ) p0 ).getLocalVersion ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->getLocalVersion()I
(( com.android.server.wm.PolicyItem ) v1 ).setLocalVersion ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/PolicyItem;->setLocalVersion(I)V
/* .line 142 */
v1 = this.mList;
/* new-instance v2, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda6; */
/* invoke-direct {v2, p0}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/PolicyImpl;)V */
/* .line 146 */
v1 = this.mPolicyItem;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.mPolicyItem;
v3 = (( com.android.server.wm.PolicyItem ) v3 ).getLocalVersion ( ); // invoke-virtual {v3}, Lcom/android/server/wm/PolicyItem;->getLocalVersion()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "(CLOUD)"; // const-string v3, "(CLOUD)"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.wm.PolicyItem ) v1 ).setCurrentVersion ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/PolicyItem;->setCurrentVersion(Ljava/lang/String;)V
/* .line 148 */
(( com.android.server.wm.PolicyImpl ) p0 ).propagateToCallbacks ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V
/* .line 149 */
return;
} // .end method
Boolean updatePackageConfigurationsIfNeeded ( ) {
/* .locals 14 */
/* .line 230 */
int v0 = 0; // const/4 v0, 0x0
/* .line 231 */
/* .local v0, "updated":Z */
v1 = this.mPolicyItem;
v1 = (( com.android.server.wm.PolicyItem ) v1 ).getLocalVersion ( ); // invoke-virtual {v1}, Lcom/android/server/wm/PolicyItem;->getLocalVersion()I
/* .line 232 */
/* .local v1, "localVersion":I */
v2 = this.mPolicyItem;
v2 = (( com.android.server.wm.PolicyItem ) v2 ).getScpmVersion ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PolicyItem;->getScpmVersion()I
/* .line 233 */
/* .local v2, "scpmVersion":I */
v3 = (( com.android.server.wm.PolicyImpl ) p0 ).getScpmVersionFromQuery ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->getScpmVersionFromQuery()I
/* .line 234 */
/* .local v3, "scpmVersionFromQuery":I */
/* if-nez v3, :cond_0 */
/* .line 235 */
/* iget v4, p0, Lcom/android/server/wm/PolicyImpl;->mRetryNumber:I */
/* add-int/lit8 v4, v4, -0x1 */
/* iput v4, p0, Lcom/android/server/wm/PolicyImpl;->mRetryNumber:I */
/* .line 236 */
/* if-lez v4, :cond_0 */
/* .line 237 */
v4 = this.mController;
v5 = this.mName;
/* const-wide/32 v6, 0x927c0 */
(( com.android.server.wm.PackageConfigurationController ) v4 ).scheduleUpdatePolicyItem ( v5, v6, v7 ); // invoke-virtual {v4, v5, v6, v7}, Lcom/android/server/wm/PackageConfigurationController;->scheduleUpdatePolicyItem(Ljava/lang/String;J)V
/* .line 238 */
/* .line 242 */
} // :cond_0
/* if-le v3, v2, :cond_6 */
/* if-le v3, v1, :cond_6 */
/* .line 243 */
v4 = v4 = this.mList;
/* .line 244 */
/* .local v4, "size":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 245 */
/* .local v5, "selections":Ljava/lang/StringBuilder; */
/* new-array v12, v4, [Ljava/lang/String; */
/* .line 246 */
/* .local v12, "selectionArgs":[Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
/* if-ge v6, v4, :cond_2 */
/* .line 247 */
/* if-lez v6, :cond_1 */
/* .line 248 */
final String v7 = " or "; // const-string v7, " or "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 250 */
} // :cond_1
final String v7 = "data1=?"; // const-string v7, "data1=?"
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 251 */
v7 = this.mList;
/* check-cast v7, Lcom/android/server/wm/PackageConfiguration; */
v7 = this.mName;
/* aput-object v7, v12, v6 */
/* .line 246 */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 253 */
} // .end local v6 # "i":I
} // :cond_2
v6 = com.android.server.wm.PolicyImpl.AUTHORITY_URI;
final String v7 = "policy_item"; // const-string v7, "policy_item"
android.net.Uri .withAppendedPath ( v6,v7 );
v7 = this.mName;
android.net.Uri .withAppendedPath ( v6,v7 );
/* .line 254 */
/* .local v13, "policyItemUri":Landroid/net/Uri; */
v6 = this.mContentResolver;
int v8 = 0; // const/4 v8, 0x0
/* .line 255 */
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v11 = 0; // const/4 v11, 0x0
/* move-object v7, v13 */
/* move-object v10, v12 */
/* invoke-virtual/range {v6 ..v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 256 */
/* .local v6, "cursor":Landroid/database/Cursor; */
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 257 */
try { // :try_start_0
v7 = this.mList;
/* new-instance v8, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v8}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda1;-><init>()V */
/* .line 261 */
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 262 */
int v7 = 2; // const/4 v7, 0x2
/* .line 263 */
/* .local v7, "packageName":Ljava/lang/String; */
int v8 = 3; // const/4 v8, 0x3
/* .line 264 */
/* .local v8, "key":Ljava/lang/String; */
int v9 = 4; // const/4 v9, 0x4
/* .line 265 */
/* .local v9, "value":Ljava/lang/String; */
v10 = this.mList;
/* new-instance v11, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v11, v8, v7, v9}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda2;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 271 */
} // .end local v7 # "packageName":Ljava/lang/String;
} // .end local v8 # "key":Ljava/lang/String;
} // .end local v9 # "value":Ljava/lang/String;
/* .line 272 */
} // :cond_3
v7 = this.mList;
/* new-instance v8, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v8}, Lcom/android/server/wm/PolicyImpl$$ExternalSyntheticLambda3;-><init>()V */
/* .line 276 */
v7 = this.mPolicyItem;
(( com.android.server.wm.PolicyItem ) v7 ).setScpmVersion ( v3 ); // invoke-virtual {v7, v3}, Lcom/android/server/wm/PolicyItem;->setScpmVersion(I)V
/* .line 277 */
v7 = this.mPolicyItem;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = "(SCPM)"; // const-string v9, "(SCPM)"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.wm.PolicyItem ) v7 ).setCurrentVersion ( v8 ); // invoke-virtual {v7, v8}, Lcom/android/server/wm/PolicyItem;->setCurrentVersion(Ljava/lang/String;)V
/* .line 278 */
int v0 = 1; // const/4 v0, 0x1
/* .line 279 */
(( com.android.server.wm.PolicyImpl ) p0 ).propagateToCallbacks ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->propagateToCallbacks()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 254 */
/* :catchall_0 */
/* move-exception v7 */
if ( v6 != null) { // if-eqz v6, :cond_4
try { // :try_start_1
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* :catchall_1 */
/* move-exception v8 */
(( java.lang.Throwable ) v7 ).addSuppressed ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :cond_4
} // :goto_2
/* throw v7 */
/* .line 281 */
} // :cond_5
} // :goto_3
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 283 */
} // .end local v4 # "size":I
} // .end local v5 # "selections":Ljava/lang/StringBuilder;
} // .end local v6 # "cursor":Landroid/database/Cursor;
} // .end local v12 # "selectionArgs":[Ljava/lang/String;
} // .end local v13 # "policyItemUri":Landroid/net/Uri;
} // :cond_6
} // .end method
void updatePolicyItem ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "forced" # Z */
/* .line 287 */
v0 = v0 = com.android.server.wm.PolicyImpl.CALLBACKS;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 288 */
return;
/* .line 291 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 292 */
v0 = this.mPolicyItem;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.PolicyItem ) v0 ).setScpmVersion ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/PolicyItem;->setScpmVersion(I)V
/* .line 295 */
} // :cond_1
(( com.android.server.wm.PolicyImpl ) p0 ).updatePackageConfigurationsIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/wm/PolicyImpl;->updatePackageConfigurationsIfNeeded()Z
/* .line 296 */
return;
} // .end method
