class com.android.server.wm.MiuiPaperContrastOverlayStubImpl$1 extends android.database.ContentObserver {
	 /* .source "MiuiPaperContrastOverlayStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->registerObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiPaperContrastOverlayStubImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiPaperContrastOverlayStubImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 96 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 99 */
v0 = this.this$0;
v0 = com.android.server.wm.MiuiPaperContrastOverlayStubImpl .-$$Nest$mgetSecurityCenterStatus ( v0 );
/* .line 100 */
/* .local v0, "textureEyecareEnable":Z */
v1 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlayStubImpl .-$$Nest$fgetmTextureEyeCareLevelUri ( v1 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
v1 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlayStubImpl .-$$Nest$fgetmPaperModeTypeUri ( v1 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
v1 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlayStubImpl .-$$Nest$fgetmPaperModeEnableUri ( v1 );
/* .line 101 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
v1 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlayStubImpl .-$$Nest$fgetmSecurityModeVtbUri ( v1 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
v1 = this.this$0;
com.android.server.wm.MiuiPaperContrastOverlayStubImpl .-$$Nest$fgetmSecurityModeGbUri ( v1 );
/* .line 102 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 109 */
} // :cond_0
return;
/* .line 103 */
} // :cond_1
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "registerObserver uri:"; // const-string v2, "registerObserver uri:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ", enable:"; // const-string v2, ", enable:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPaperContrastOverlayStubImpl"; // const-string v2, "MiuiPaperContrastOverlayStubImpl"
android.util.Slog .d ( v2,v1 );
/* .line 105 */
v1 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
com.android.server.wm.MiuiPaperContrastOverlayStubImpl .-$$Nest$fputmFoldDeviceReady ( v1,v2 );
/* .line 106 */
v1 = this.this$0;
(( com.android.server.wm.MiuiPaperContrastOverlayStubImpl ) v1 ).updateTextureEyeCareLevel ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->updateTextureEyeCareLevel(Z)V
/* .line 107 */
return;
} // .end method
