public class com.android.server.wm.FgWindowChangedInfo {
	 /* .source "FgWindowChangedInfo.java" */
	 /* # instance fields */
	 final android.content.pm.ApplicationInfo multiWindowAppInfo;
	 final Integer pid;
	 final com.android.server.wm.ActivityRecord record;
	 /* # direct methods */
	 public com.android.server.wm.FgWindowChangedInfo ( ) {
		 /* .locals 0 */
		 /* .param p1, "_record" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p2, "_info" # Landroid/content/pm/ApplicationInfo; */
		 /* .param p3, "_pid" # I */
		 /* .line 11 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 12 */
		 this.record = p1;
		 /* .line 13 */
		 this.multiWindowAppInfo = p2;
		 /* .line 14 */
		 /* iput p3, p0, Lcom/android/server/wm/FgWindowChangedInfo;->pid:I */
		 /* .line 15 */
		 return;
	 } // .end method
