.class Lcom/android/server/wm/FindDeviceLockWindowImpl$1;
.super Ljava/lang/Object;
.source "FindDeviceLockWindowImpl.java"

# interfaces
.implements Lcom/android/internal/util/ToBooleanFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/FindDeviceLockWindowImpl;->updateLockDeviceWindowLocked(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/DisplayContent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/internal/util/ToBooleanFunction<",
        "Lcom/android/server/wm/WindowState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/FindDeviceLockWindowImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/FindDeviceLockWindowImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/FindDeviceLockWindowImpl;

    .line 21
    iput-object p1, p0, Lcom/android/server/wm/FindDeviceLockWindowImpl$1;->this$0:Lcom/android/server/wm/FindDeviceLockWindowImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/android/server/wm/WindowState;)Z
    .locals 6
    .param p1, "win"    # Lcom/android/server/wm/WindowState;

    .line 24
    const/4 v0, 0x0

    if-eqz p1, :cond_5

    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    if-nez v1, :cond_0

    goto :goto_2

    .line 26
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 27
    .local v1, "type":I
    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    .line 29
    .local v2, "extraFlags":I
    and-int/lit16 v3, v2, 0x800

    if-eqz v3, :cond_1

    .line 30
    invoke-static {p1}, Lcom/android/server/wm/FindDeviceLockWindowImpl;->-$$Nest$sfputsTmpLockWindow(Lcom/android/server/wm/WindowState;)V

    goto :goto_1

    .line 31
    :cond_1
    const/4 v3, 0x1

    if-lt v1, v3, :cond_4

    const/16 v4, 0x7d0

    if-ge v1, v4, :cond_4

    .line 33
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParentWindow()Lcom/android/server/wm/WindowState;

    move-result-object v4

    if-nez v4, :cond_4

    .line 34
    invoke-static {}, Lcom/android/server/wm/FindDeviceLockWindowImpl;->-$$Nest$sfgetsTmpFirstAppWindow()Lcom/android/server/wm/WindowState;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-static {}, Lcom/android/server/wm/FindDeviceLockWindowImpl;->-$$Nest$sfgetsTmpFirstAppWindow()Lcom/android/server/wm/WindowState;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/android/server/wm/FindDeviceLockWindowImpl;->-$$Nest$sfgetsTmpFirstAppWindow()Lcom/android/server/wm/WindowState;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v5, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 36
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0

    .line 38
    :cond_2
    invoke-static {}, Lcom/android/server/wm/FindDeviceLockWindowImpl;->-$$Nest$sfgetsTmpLockWindow()Lcom/android/server/wm/WindowState;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 39
    return v3

    .line 37
    :cond_3
    :goto_0
    invoke-static {p1}, Lcom/android/server/wm/FindDeviceLockWindowImpl;->-$$Nest$sfputsTmpFirstAppWindow(Lcom/android/server/wm/WindowState;)V

    .line 42
    :cond_4
    :goto_1
    return v0

    .line 24
    .end local v1    # "type":I
    .end local v2    # "extraFlags":I
    :cond_5
    :goto_2
    return v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 0

    .line 21
    check-cast p1, Lcom/android/server/wm/WindowState;

    invoke-virtual {p0, p1}, Lcom/android/server/wm/FindDeviceLockWindowImpl$1;->apply(Lcom/android/server/wm/WindowState;)Z

    move-result p1

    return p1
.end method
