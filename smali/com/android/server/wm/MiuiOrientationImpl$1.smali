.class Lcom/android/server/wm/MiuiOrientationImpl$1;
.super Ljava/lang/Object;
.source "MiuiOrientationImpl.java"

# interfaces
.implements Ljava/util/function/BiConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiOrientationImpl;->setEmbeddedFullRuleData(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/function/BiConsumer<",
        "Ljava/lang/String;",
        "Landroid/util/Pair<",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiOrientationImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiOrientationImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiOrientationImpl;

    .line 331
    iput-object p1, p0, Lcom/android/server/wm/MiuiOrientationImpl$1;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 331
    check-cast p1, Ljava/lang/String;

    check-cast p2, Landroid/util/Pair;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/MiuiOrientationImpl$1;->accept(Ljava/lang/String;Landroid/util/Pair;)V

    return-void
.end method

.method public accept(Ljava/lang/String;Landroid/util/Pair;)V
    .locals 25
    .param p1, "pkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 334
    .local p2, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 335
    .local v2, "rules":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 336
    .local v3, "nra":Z
    const/4 v4, 0x0

    .line 337
    .local v4, "nrs":Z
    const/4 v5, 0x1

    .line 338
    .local v5, "cr":Z
    const/4 v6, 0x1

    .line 339
    .local v6, "rcr":Z
    const/4 v7, 0x0

    .line 340
    .local v7, "nr":Z
    const/4 v8, 0x0

    .line 341
    .local v8, "r":Z
    const/4 v9, 0x0

    .line 342
    .local v9, "ri":Z
    const/4 v10, 0x0

    .line 343
    .local v10, "uc":Z
    array-length v11, v2

    const/4 v13, 0x0

    :goto_0
    if-ge v13, v11, :cond_1

    aget-object v14, v2, v13

    .line 344
    .local v14, "str":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/lang/String;->hashCode()I

    move-result v15

    sparse-switch v15, :sswitch_data_0

    :cond_0
    goto :goto_1

    :sswitch_0
    const-string v15, "rcr"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v15, 0x3

    goto :goto_2

    :sswitch_1
    const-string v15, "nrs"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v15, 0x1

    goto :goto_2

    :sswitch_2
    const-string v15, "nra"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v15, 0x0

    goto :goto_2

    :sswitch_3
    const-string/jumbo v15, "uc"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v15, 0x7

    goto :goto_2

    :sswitch_4
    const-string v15, "ri"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v15, 0x6

    goto :goto_2

    :sswitch_5
    const-string v15, "nr"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v15, 0x4

    goto :goto_2

    :sswitch_6
    const-string v15, "cr"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v15, 0x2

    goto :goto_2

    :sswitch_7
    const-string v15, "r"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v15, 0x5

    goto :goto_2

    :goto_1
    const/4 v15, -0x1

    :goto_2
    packed-switch v15, :pswitch_data_0

    .line 370
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unknown options for setEmbeddedFullRule :"

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v15, "MiuiOrientationImpl"

    invoke-static {v15, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 367
    :pswitch_0
    const/4 v10, 0x1

    .line 368
    goto :goto_3

    .line 364
    :pswitch_1
    const/4 v9, 0x1

    .line 365
    goto :goto_3

    .line 361
    :pswitch_2
    const/4 v8, 0x1

    .line 362
    goto :goto_3

    .line 358
    :pswitch_3
    const/4 v7, 0x1

    .line 359
    goto :goto_3

    .line 355
    :pswitch_4
    const/4 v6, 0x0

    .line 356
    goto :goto_3

    .line 352
    :pswitch_5
    const/4 v5, 0x0

    .line 353
    goto :goto_3

    .line 349
    :pswitch_6
    const/4 v4, 0x1

    .line 350
    goto :goto_3

    .line 346
    :pswitch_7
    const/4 v3, 0x1

    .line 347
    nop

    .line 343
    .end local v14    # "str":Ljava/lang/String;
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    .line 373
    :cond_1
    iget-object v11, v0, Lcom/android/server/wm/MiuiOrientationImpl$1;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-static {v11}, Lcom/android/server/wm/MiuiOrientationImpl;->-$$Nest$fgetmEmbeddedFullRules(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;

    move-result-object v11

    new-instance v12, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;

    iget-object v14, v0, Lcom/android/server/wm/MiuiOrientationImpl$1;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    iget-object v13, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v13, Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v16

    move-object v13, v12

    move-object/from16 v15, p1

    move/from16 v17, v3

    move/from16 v18, v4

    move/from16 v19, v5

    move/from16 v20, v6

    move/from16 v21, v7

    move/from16 v22, v8

    move/from16 v23, v9

    move/from16 v24, v10

    invoke-direct/range {v13 .. v24}, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;Ljava/lang/String;ZZZZZZZZZ)V

    move-object/from16 v13, p1

    invoke-interface {v11, v13, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    return-void

    :sswitch_data_0
    .sparse-switch
        0x72 -> :sswitch_7
        0xc6f -> :sswitch_6
        0xdc4 -> :sswitch_5
        0xe37 -> :sswitch_4
        0xe8e -> :sswitch_3
        0x1ab1d -> :sswitch_2
        0x1ab2f -> :sswitch_1
        0x1b861 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
