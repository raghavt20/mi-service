class com.android.server.wm.PreloadStateManagerImpl$GreezeCallback extends miui.greeze.IGreezeCallback$Stub {
	 /* .source "PreloadStateManagerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/PreloadStateManagerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "GreezeCallback" */
} // .end annotation
/* # direct methods */
private com.android.server.wm.PreloadStateManagerImpl$GreezeCallback ( ) {
/* .locals 0 */
/* .line 692 */
/* invoke-direct {p0}, Lmiui/greeze/IGreezeCallback$Stub;-><init>()V */
return;
} // .end method
 com.android.server.wm.PreloadStateManagerImpl$GreezeCallback ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/PreloadStateManagerImpl$GreezeCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void reportBinderState ( Integer p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "tid" # I */
/* .param p4, "binderState" # I */
/* .param p5, "now" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 726 */
final String v0 = " tid="; // const-string v0, " tid="
final String v1 = " pid="; // const-string v1, " pid="
final String v2 = "preloadApp receive binder state: uid="; // const-string v2, "preloadApp receive binder state: uid="
/* packed-switch p4, :pswitch_data_0 */
/* .line 738 */
/* :pswitch_0 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
/* filled-new-array {p2}, [I */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerInternal ) v3 ).thawPids ( v4, v5, v0 ); // invoke-virtual {v3, v4, v5, v0}, Lcom/miui/server/greeze/GreezeManagerInternal;->thawPids([IILjava/lang/String;)Ljava/util/List;
/* .line 742 */
/* .line 730 */
/* :pswitch_1 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
/* filled-new-array {p1}, [I */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerInternal ) v3 ).thawUids ( v4, v5, v0 ); // invoke-virtual {v3, v4, v5, v0}, Lcom/miui/server/greeze/GreezeManagerInternal;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 734 */
/* .line 728 */
/* :pswitch_2 */
/* nop */
/* .line 746 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void reportBinderTrans ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Boolean p5, Long p6, Long p7 ) {
/* .locals 5 */
/* .param p1, "dstUid" # I */
/* .param p2, "dstPid" # I */
/* .param p3, "callerUid" # I */
/* .param p4, "callerPid" # I */
/* .param p5, "callerTid" # I */
/* .param p6, "isOneway" # Z */
/* .param p7, "now" # J */
/* .param p9, "buffer" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 713 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
/* filled-new-array {p1}, [I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "preloadApp receive frozen binder trans: dstUid="; // const-string v4, "preloadApp receive frozen binder trans: dstUid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " dstPid="; // const-string v4, " dstPid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " callerUid="; // const-string v4, " callerUid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " callerPid="; // const-string v4, " callerPid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p4 ); // invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " callerTid="; // const-string v4, " callerTid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p5 ); // invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " oneway="; // const-string v4, " oneway="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p6 ); // invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).thawUids ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerInternal;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 721 */
return;
} // .end method
public void reportNet ( Integer p0, Long p1 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "now" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 706 */
return;
} // .end method
public void reportSignal ( Integer p0, Integer p1, Long p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "now" # J */
/* .line 696 */
com.android.server.wm.PreloadStateManagerImpl .-$$Nest$sfgetsPreloadUidInfos ( );
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 697 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 698 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "preloadApp reportSignal remove uid "; // const-string v1, "preloadApp reportSignal remove uid "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PreloadStateManagerImpl"; // const-string v1, "PreloadStateManagerImpl"
android.util.Slog .w ( v1,v0 );
/* .line 701 */
} // :cond_0
return;
} // .end method
public void serviceReady ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "ready" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 751 */
return;
} // .end method
public void thawedByOther ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "module" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 756 */
return;
} // .end method
