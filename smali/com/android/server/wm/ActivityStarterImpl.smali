.class Lcom/android/server/wm/ActivityStarterImpl;
.super Lcom/android/server/wm/ActivityStarterStub;
.source "ActivityStarterImpl.java"


# static fields
.field private static final APPLICATION_LOCK_NAME:Ljava/lang/String; = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

.field private static final CARLINK_NOT_SUPPORT_IN_MUTIL_DISPLAY_ACTIVITYS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final CARLINK_NOT_SUPPORT_IN_MUTIL_DISPLAY_ACTIVITYS_ACTION:Ljava/lang/String; = "not_support_in_mutil_display_activitys_action"

.field private static final CARLINK_VIRTUAL_DISPLAY_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final EXTRA_ORIGINATING_UID:Ljava/lang/String; = "originating_uid"

.field public static final MIBI_SDK_SIGN_DEDUCT_ACTIVITY:Ljava/lang/String; = "com.mibi.sdk.deduct.ui.SignDeductActivity"

.field public static final PACKAGE_NAME_ALIPAY:Ljava/lang/String; = "com.eg.android.AlipayGphone"

.field private static final SECURITY_CENTER:Ljava/lang/String; = "com.miui.securitycenter"

.field private static final TAG:Ljava/lang/String; = "ActivityStarterImpl"


# instance fields
.field private mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mContext:Landroid/content/Context;

.field private mDefaultHomePkgNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mLastStartActivityUid:I

.field private mPowerConsumptionServiceInternal:Lcom/android/server/PowerConsumptionServiceInternal;

.field private mSecurityHelper:Lmiui/app/ActivitySecurityHelper;

.field private mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

.field private mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

.field private mSplashScreenServiceDelegate:Lcom/miui/server/SplashScreenServiceDelegate;

.field private mSystemReady:Z


# direct methods
.method public static synthetic $r8$lambda$XFwMJl2kKDnqKk0H2XguenoCh8A(Lcom/android/server/wm/ActivityRecord;IZLandroid/content/Intent;Landroid/content/ComponentName;)Z
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/server/wm/ActivityStarterImpl;->matchesPackageName(Lcom/android/server/wm/ActivityRecord;IZLandroid/content/Intent;Landroid/content/ComponentName;)Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 100
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "com.baidu.BaiduMap"

    const-string v2, "com.sinyee.babybus.story"

    const-string v3, "com.autonavi.minimap"

    filled-new-array {v3, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/server/wm/ActivityStarterImpl;->CARLINK_NOT_SUPPORT_IN_MUTIL_DISPLAY_ACTIVITYS:Ljava/util/Set;

    .line 104
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "com.xiaomi.ucar.minimap"

    const-string v2, "com.miui.car.launcher"

    const-string v3, "com.miui.carlink"

    filled-new-array {v3, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/server/wm/ActivityStarterImpl;->CARLINK_VIRTUAL_DISPLAY_SET:Ljava/util/Set;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 89
    invoke-direct {p0}, Lcom/android/server/wm/ActivityStarterStub;-><init>()V

    return-void
.end method

.method private static checkAndNotify(I)V
    .locals 5
    .param p0, "uid"    # I

    .line 150
    :try_start_0
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    .line 151
    .local v0, "gz":Lcom/miui/server/greeze/GreezeManagerInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/miui/server/greeze/GreezeManagerInternal;->isUidFrozen(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    const-string/jumbo v1, "whetstone.activity"

    .line 153
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    .line 152
    invoke-static {v1}, Lcom/miui/whetstone/server/IWhetstoneActivityManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/miui/whetstone/server/IWhetstoneActivityManager;

    move-result-object v1

    .line 154
    .local v1, "ws":Lcom/miui/whetstone/server/IWhetstoneActivityManager;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 155
    .local v2, "b":Landroid/os/Bundle;
    const-string/jumbo v3, "uid"

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 156
    if-eqz v1, :cond_0

    .line 157
    invoke-interface {v1}, Lcom/miui/whetstone/server/IWhetstoneActivityManager;->getPowerKeeperPolicy()Lcom/miui/whetstone/IPowerKeeperPolicy;

    move-result-object v3

    const/16 v4, 0xb

    invoke-interface {v3, v4, v2}, Lcom/miui/whetstone/IPowerKeeperPolicy;->notifyEvent(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v0    # "gz":Lcom/miui/server/greeze/GreezeManagerInternal;
    .end local v1    # "ws":Lcom/miui/whetstone/server/IWhetstoneActivityManager;
    .end local v2    # "b":Landroid/os/Bundle;
    :cond_0
    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkAndNotify error uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActivityStarterImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private checkStorageRestricted(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ILjava/lang/String;Ljava/lang/String;Landroid/os/IBinder;)Landroid/content/Intent;
    .locals 6
    .param p1, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "callingUid"    # I
    .param p4, "callingPackage"    # Ljava/lang/String;
    .param p5, "reason"    # Ljava/lang/String;
    .param p6, "resultTo"    # Landroid/os/IBinder;

    .line 645
    const-string/jumbo v0, "startActivityAsCaller"

    invoke-static {v0, p5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 646
    const-string v0, "As caller, check special action!"

    const-string v1, "ActivityStarterImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    if-eqz p1, :cond_4

    if-eqz p6, :cond_4

    .line 648
    invoke-static {p6}, Lcom/android/server/wm/ActivityRecord;->forTokenLocked(Landroid/os/IBinder;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 649
    .local v0, "record":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_0

    const-string v2, "com.miui.securitycenter"

    iget-object v3, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 650
    .local v2, "fromWakePath":Z
    :goto_0
    if-nez v2, :cond_3

    const-string v3, "android.intent.action.PICK"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 651
    invoke-static {p4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/server/wm/ActivityStarterImpl;->mContext:Landroid/content/Context;

    .line 652
    invoke-static {v3, p4, p3, p2}, Lmiui/app/StorageRestrictedPathManager;->isDenyAccessGallery(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    nop

    .line 654
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.SEND"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 655
    invoke-static {p4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/server/wm/ActivityStarterImpl;->mContext:Landroid/content/Context;

    iget-object v4, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v5, p1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 656
    invoke-static {v3, v4, v5, p2}, Lmiui/app/StorageRestrictedPathManager;->isDenyAccessGallery(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 658
    :cond_2
    const-string/jumbo v3, "startAsCaller to pick pictures, not skip!"

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    .end local v0    # "record":Lcom/android/server/wm/ActivityRecord;
    .end local v2    # "fromWakePath":Z
    goto :goto_1

    .line 660
    .restart local v0    # "record":Lcom/android/server/wm/ActivityRecord;
    .restart local v2    # "fromWakePath":Z
    :cond_3
    return-object p2

    .line 663
    .end local v0    # "record":Lcom/android/server/wm/ActivityRecord;
    .end local v2    # "fromWakePath":Z
    :cond_4
    return-object p2

    .line 666
    :cond_5
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private findActivityInSameApplication(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;ZLcom/android/server/wm/RootWindowContainer;)Lcom/android/server/wm/ActivityRecord;
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "info"    # Landroid/content/pm/ActivityInfo;
    .param p3, "compareIntentFilters"    # Z
    .param p4, "rwc"    # Lcom/android/server/wm/RootWindowContainer;

    .line 233
    iget-object v0, p2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    if-nez p4, :cond_0

    goto :goto_0

    .line 237
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 238
    .local v0, "cls":Landroid/content/ComponentName;
    iget-object v2, p2, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 239
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v4, p2, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 242
    :cond_1
    iget-object v2, p2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v8

    .line 244
    .local v8, "userId":I
    new-instance v2, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda2;

    invoke-direct {v2}, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda2;-><init>()V

    const-class v3, Lcom/android/server/wm/ActivityRecord;

    .line 245
    invoke-static {v3}, Lcom/android/internal/util/function/pooled/PooledLambda;->__(Ljava/lang/Class;)Lcom/android/internal/util/function/pooled/ArgumentPlaceholder;

    move-result-object v3

    .line 246
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 244
    move-object v6, p1

    move-object v7, v0

    invoke-static/range {v2 .. v7}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainPredicate(Lcom/android/internal/util/function/QuintPredicate;Lcom/android/internal/util/function/pooled/ArgumentPlaceholder;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/android/internal/util/function/pooled/PooledPredicate;

    move-result-object v2

    .line 247
    .local v2, "p":Lcom/android/internal/util/function/pooled/PooledPredicate;
    if-nez v2, :cond_2

    .line 248
    return-object v1

    .line 250
    :cond_2
    invoke-virtual {p4, v2}, Lcom/android/server/wm/RootWindowContainer;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 251
    .local v1, "r":Lcom/android/server/wm/ActivityRecord;
    invoke-interface {v2}, Lcom/android/internal/util/function/pooled/PooledPredicate;->recycle()V

    .line 252
    return-object v1

    .line 234
    .end local v0    # "cls":Landroid/content/ComponentName;
    .end local v1    # "r":Lcom/android/server/wm/ActivityRecord;
    .end local v2    # "p":Lcom/android/internal/util/function/pooled/PooledPredicate;
    .end local v8    # "userId":I
    :cond_3
    :goto_0
    return-object v1
.end method

.method private findMapActivityInHistory(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/RootWindowContainer;)Lcom/android/server/wm/ActivityRecord;
    .locals 4
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "rwc"    # Lcom/android/server/wm/RootWindowContainer;

    .line 191
    const/4 v0, 0x0

    .line 192
    .local v0, "result":Lcom/android/server/wm/ActivityRecord;
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/server/wm/ActivityStarterImpl;->CARLINK_NOT_SUPPORT_IN_MUTIL_DISPLAY_ACTIVITYS:Ljava/util/Set;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    .line 194
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 193
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3, p2}, Lcom/android/server/wm/ActivityStarterImpl;->findActivityInSameApplication(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;ZLcom/android/server/wm/RootWindowContainer;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "findMapActivityInHistory result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " r="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  info="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActivityStarterImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    return-object v0
.end method

.method static getInstance()Lcom/android/server/wm/ActivityStarterImpl;
    .locals 1

    .line 138
    invoke-static {}, Lcom/android/server/wm/ActivityStarterStub;->get()Lcom/android/server/wm/ActivityStarterStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityStarterImpl;

    return-object v0
.end method

.method private getRawPackageName(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;)Ljava/lang/String;
    .locals 3
    .param p1, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 589
    iget-object v0, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 590
    .local v0, "rawPackageName":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 591
    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 592
    .local v1, "rawIntent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 593
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 596
    .end local v1    # "rawIntent":Landroid/content/Intent;
    :cond_0
    return-object v0
.end method

.method static synthetic lambda$finishLaunchedFromActivityIfNeeded$1(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p0, "starting"    # Lcom/android/server/wm/ActivityRecord;
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 768
    iget-boolean v0, p1, Lcom/android/server/wm/ActivityRecord;->finishing:Z

    if-nez v0, :cond_0

    .line 769
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I

    move-result v0

    iget v1, p0, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 768
    :goto_0
    return v0
.end method

.method static synthetic lambda$sendBroadCastToUcar$0(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .line 216
    const-string v0, "miui.car.permission.MI_CARLINK_STATUS"

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method private static matchesPackageName(Lcom/android/server/wm/ActivityRecord;IZLandroid/content/Intent;Landroid/content/ComponentName;)Z
    .locals 4
    .param p0, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p1, "userId"    # I
    .param p2, "compareIntentFilters"    # Z
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "cls"    # Landroid/content/ComponentName;

    .line 258
    const/4 v0, 0x0

    if-eqz p0, :cond_5

    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->canBeTopRunning()Z

    move-result v1

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    if-eq v1, p1, :cond_0

    goto :goto_1

    .line 260
    :cond_0
    const/4 v1, 0x1

    if-eqz p2, :cond_1

    .line 261
    iget-object v2, p0, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2, p3}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 262
    return v1

    .line 265
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-eqz v2, :cond_4

    if-nez p4, :cond_2

    goto :goto_0

    .line 270
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "matchesApplication r="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cls="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ActivityStarterImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iget-object v2, p0, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 272
    return v1

    .line 275
    :cond_3
    return v0

    .line 266
    :cond_4
    :goto_0
    return v0

    .line 258
    :cond_5
    :goto_1
    return v0
.end method

.method private recordAppBehavior(ILjava/lang/String;Landroid/content/Intent;)V
    .locals 7
    .param p1, "behavior"    # I
    .param p2, "caller"    # Ljava/lang/String;
    .param p3, "data"    # Landroid/content/Intent;

    .line 868
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v0, :cond_0

    .line 869
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    .line 871
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    if-eqz v1, :cond_1

    .line 872
    const-wide/16 v4, 0x1

    invoke-static {p3}, Lmiui/security/AppBehavior;->parseIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v6

    move v2, p1

    move-object v3, p2

    invoke-virtual/range {v1 .. v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    .line 874
    :cond_1
    return-void
.end method

.method private resolveCheckIntent(Lcom/android/server/wm/ActivityStarter$Request;)Landroid/content/pm/ActivityInfo;
    .locals 11
    .param p1, "request"    # Lcom/android/server/wm/ActivityStarter$Request;

    .line 671
    iget-object v8, p1, Lcom/android/server/wm/ActivityStarter$Request;->intent:Landroid/content/Intent;

    .line 672
    .local v8, "intent":Landroid/content/Intent;
    iget v0, p1, Lcom/android/server/wm/ActivityStarter$Request;->userId:I

    .line 673
    .local v0, "userId":I
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_4

    .line 674
    const/4 v1, 0x0

    .line 675
    .local v1, "transform":Z
    const-string v2, "miui.intent.action.CHECK_ACCESS_CONTROL"

    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 676
    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.app.action.CHECK_ACCESS_CONTROL_PAD"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 677
    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.app.action.CHECK_ALLOW_START_ACTIVITY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 678
    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.app.action.CHECK_ALLOW_START_ACTIVITY_PAD"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 679
    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.miui.gamebooster.action.ACCESS_WINDOWCALLACTIVITY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    .line 680
    invoke-virtual {v2, v8}, Lmiui/security/SecurityManagerInternal;->isBlockActivity(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move v9, v0

    move v10, v1

    goto :goto_1

    .line 681
    :cond_1
    :goto_0
    const/16 v2, 0x3e7

    if-ne v0, v2, :cond_2

    .line 682
    const/4 v0, 0x0

    .line 684
    :cond_2
    const/4 v1, 0x1

    move v9, v0

    move v10, v1

    .line 686
    .end local v0    # "userId":I
    .end local v1    # "transform":Z
    .local v9, "userId":I
    .local v10, "transform":Z
    :goto_1
    if-eqz v10, :cond_3

    .line 687
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/android/server/wm/ActivityStarter$Request;->profilerInfo:Landroid/app/ProfilerInfo;

    iget v6, p1, Lcom/android/server/wm/ActivityStarter$Request;->filterCallingUid:I

    iget v7, p1, Lcom/android/server/wm/ActivityStarter$Request;->callingPid:I

    move-object v1, v8

    move v5, v9

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/wm/ActivityTaskSupervisor;->resolveActivity(Landroid/content/Intent;Ljava/lang/String;ILandroid/app/ProfilerInfo;III)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    iput-object v0, p1, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 692
    .end local v10    # "transform":Z
    :cond_3
    move v0, v9

    .end local v9    # "userId":I
    .restart local v0    # "userId":I
    :cond_4
    iget-object v1, p1, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    return-object v1
.end method

.method private sendBroadCastToUcar(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 4
    .param p1, "srcIntent"    # Landroid/content/Intent;
    .param p2, "context"    # Landroid/content/Context;

    .line 202
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mHandlerThread:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    .line 203
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "carlink-workthread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 204
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 205
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mHandler:Landroid/os/Handler;

    .line 208
    :cond_0
    if-eqz p2, :cond_1

    .line 209
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 210
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "not_support_in_mutil_display_activitys_action"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    const-string v1, "com.miui.carlink"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 213
    .local v1, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "src_intent"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 214
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 215
    iget-object v2, p0, Lcom/android/server/wm/ActivityStarterImpl;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda1;

    invoke-direct {v3, p2, v0}, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda1;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 219
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_1
    return-void
.end method

.method private startActivityByFreeForm(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;)Z
    .locals 3
    .param p1, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 600
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v0, :cond_0

    .line 601
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    iget-object v1, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v2, p1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    invoke-virtual {v0, v1, p2, v2}, Lmiui/security/SecurityManagerInternal;->checkGameBoosterPayPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method activityIdle(Landroid/content/pm/ActivityInfo;)V
    .locals 2
    .param p1, "aInfo"    # Landroid/content/pm/ActivityInfo;

    .line 440
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSystemReady:Z

    if-nez v0, :cond_0

    return-void

    .line 442
    :cond_0
    if-nez p1, :cond_1

    .line 443
    const-string v0, "ActivityStarterImpl"

    const-string v1, "aInfo is null!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    return-void

    .line 446
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSplashScreenServiceDelegate:Lcom/miui/server/SplashScreenServiceDelegate;

    invoke-virtual {v0, p1}, Lcom/miui/server/SplashScreenServiceDelegate;->activityIdle(Landroid/content/pm/ActivityInfo;)V

    .line 447
    return-void
.end method

.method public carlinkJudge(Landroid/content/Context;Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/RootWindowContainer;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetRootTask"    # Lcom/android/server/wm/Task;
    .param p3, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "rwc"    # Lcom/android/server/wm/RootWindowContainer;

    .line 167
    invoke-direct {p0, p3, p4}, Lcom/android/server/wm/ActivityStarterImpl;->findMapActivityInHistory(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/RootWindowContainer;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 169
    .local v0, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 170
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v2

    if-eqz v2, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    .line 175
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/Task;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 176
    .local v2, "preDisplayContent":Lcom/android/server/wm/DisplayContent;
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v3

    .line 178
    .local v3, "targetDisplayid":I
    invoke-virtual {p0, v2}, Lcom/android/server/wm/ActivityStarterImpl;->isCarWithDisplay(Lcom/android/server/wm/DisplayContent;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v3, :cond_1

    .line 179
    const-string v1, "ActivityStarterImpl"

    const-string v4, "activity is already started in carlink display"

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v1, p3, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    .line 181
    .local v1, "srcIntent":Landroid/content/Intent;
    invoke-direct {p0, v1, p1}, Lcom/android/server/wm/ActivityStarterImpl;->sendBroadCastToUcar(Landroid/content/Intent;Landroid/content/Context;)V

    .line 182
    const/4 v4, 0x1

    return v4

    .line 185
    .end local v1    # "srcIntent":Landroid/content/Intent;
    :cond_1
    return v1

    .line 172
    .end local v2    # "preDisplayContent":Lcom/android/server/wm/DisplayContent;
    .end local v3    # "targetDisplayid":I
    :cond_2
    :goto_0
    return v1
.end method

.method public checkDefaultBrowser(Landroid/content/Context;Lcom/android/server/wm/Task;Landroid/content/Intent;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "task"    # Lcom/android/server/wm/Task;
    .param p3, "intent"    # Landroid/content/Intent;

    .line 935
    const/4 v0, 0x0

    if-eqz p1, :cond_5

    if-eqz p2, :cond_5

    if-nez p3, :cond_0

    goto/16 :goto_1

    .line 936
    :cond_0
    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 937
    .local v1, "targetParcelable":Landroid/os/Parcelable;
    instance-of v2, v1, Landroid/content/Intent;

    if-nez v2, :cond_1

    .line 938
    return v0

    .line 940
    :cond_1
    move-object v2, v1

    check-cast v2, Landroid/content/Intent;

    .line 942
    .local v2, "rawIntent":Landroid/content/Intent;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 943
    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-static {v4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 944
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .local v3, "rawPackageName":Ljava/lang/String;
    goto :goto_0

    .line 946
    .end local v3    # "rawPackageName":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 947
    .restart local v3    # "rawPackageName":Ljava/lang/String;
    move-object v2, p3

    .line 949
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getDefaultBrowserPackageNameAsUser(I)Ljava/lang/String;

    move-result-object v4

    .line 950
    .local v4, "defaultBrowserPackageName":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    invoke-interface {v5}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->isInVideoOrGameScene()Z

    move-result v5

    .line 951
    .local v5, "checkVGScene":Z
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    .line 952
    .local v6, "isDefaultBrowser":Z
    if-eqz v6, :cond_4

    .line 953
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 954
    .local v7, "uri":Landroid/net/Uri;
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 955
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 956
    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string v9, "http"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 957
    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string v9, "https"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 958
    :cond_3
    and-int v0, v5, v6

    return v0

    .line 961
    .end local v7    # "uri":Landroid/net/Uri;
    :cond_4
    return v0

    .line 935
    .end local v1    # "targetParcelable":Landroid/os/Parcelable;
    .end local v2    # "rawIntent":Landroid/content/Intent;
    .end local v3    # "rawPackageName":Ljava/lang/String;
    .end local v4    # "defaultBrowserPackageName":Ljava/lang/String;
    .end local v5    # "checkVGScene":Z
    .end local v6    # "isDefaultBrowser":Z
    :cond_5
    :goto_1
    return v0
.end method

.method public checkIntentActivityForAppLock(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 6
    .param p1, "intentActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "startActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 913
    invoke-virtual {p0, p2}, Lcom/android/server/wm/ActivityStarterImpl;->isAppLockActivity(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    .line 915
    if-eqz p1, :cond_7

    const-string v0, "com.miui.securitycenter"

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 917
    const-string v0, "android.intent.extra.shortcut.NAME"

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v3, :cond_0

    .line 918
    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    .line 919
    .local v3, "pkgBehindIntentActivity":Ljava/lang/String;
    :goto_0
    if-eqz p2, :cond_1

    iget-object v4, p2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v4, :cond_1

    .line 920
    iget-object v2, p2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    nop

    :goto_1
    move-object v0, v2

    .line 922
    .local v0, "pkgBehindStartActivity":Ljava/lang/String;
    const-string v2, "originating_uid"

    if-eqz p1, :cond_2

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v4, :cond_2

    .line 923
    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    goto :goto_2

    :cond_2
    move v4, v1

    .line 924
    .local v4, "intentActivityUid":I
    :goto_2
    if-eqz p2, :cond_3

    iget-object v5, p2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v5, :cond_3

    .line 925
    iget-object v5, p2, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v5, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    goto :goto_3

    :cond_3
    move v2, v1

    .line 926
    .local v2, "startActivityUid":I
    :goto_3
    if-eqz v3, :cond_4

    .line 927
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    if-eq v4, v2, :cond_6

    :cond_5
    const/4 v1, 0x1

    .line 926
    :cond_6
    return v1

    .line 931
    .end local v0    # "pkgBehindStartActivity":Ljava/lang/String;
    .end local v2    # "startActivityUid":I
    .end local v3    # "pkgBehindIntentActivity":Ljava/lang/String;
    .end local v4    # "intentActivityUid":I
    :cond_7
    return v1
.end method

.method public checkRunningCompatibility(Landroid/app/IApplicationThread;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ILjava/lang/String;)Z
    .locals 6
    .param p1, "caller"    # Landroid/app/IApplicationThread;
    .param p2, "info"    # Landroid/content/pm/ActivityInfo;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "userId"    # I
    .param p5, "callingPackage"    # Ljava/lang/String;

    .line 144
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceImpl;->getInstance()Lcom/android/server/am/ActivityManagerServiceImpl;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkRunningCompatibility(Landroid/app/IApplicationThread;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method checkStartActivityByFreeForm(Landroid/app/IApplicationThread;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;Ljava/lang/String;ZILjava/lang/String;Lcom/android/server/wm/SafeActivityOptions;)Lcom/android/server/wm/SafeActivityOptions;
    .locals 12
    .param p1, "caller"    # Landroid/app/IApplicationThread;
    .param p2, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "resolvedType"    # Ljava/lang/String;
    .param p5, "ignoreTargetSecurity"    # Z
    .param p6, "callingUid"    # I
    .param p7, "callingPackage"    # Ljava/lang/String;
    .param p8, "bOptions"    # Lcom/android/server/wm/SafeActivityOptions;

    .line 541
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p8

    if-eqz v2, :cond_a

    .line 542
    invoke-direct {p0, p2, p3}, Lcom/android/server/wm/ActivityStarterImpl;->getRawPackageName(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v5

    .line 543
    .local v5, "rawPackageName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 544
    .local v6, "currentFullPackageName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 545
    .local v7, "currentFullActivity":Lcom/android/server/wm/ActivityRecord;
    iget-object v0, v1, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v8, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v8

    .line 546
    :try_start_0
    iget-object v0, v1, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 547
    .local v0, "display":Lcom/android/server/wm/DisplayContent;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 548
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 549
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;

    move-result-object v9

    move-object v6, v9

    .line 550
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;

    move-result-object v9

    move-object v7, v9

    .line 552
    .end local v0    # "display":Lcom/android/server/wm/DisplayContent;
    :cond_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 553
    invoke-direct {p0, p2, p3}, Lcom/android/server/wm/ActivityStarterImpl;->startActivityByFreeForm(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_a

    if-eqz v5, :cond_a

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 554
    if-eqz v7, :cond_1

    iget-object v0, v7, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-eqz v0, :cond_1

    const-string v0, "com.mibi.sdk.deduct.ui.SignDeductActivity"

    iget-object v8, v7, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 555
    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.eg.android.AlipayGphone"

    .line 556
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    return-object v4

    .line 559
    :cond_1
    iget-object v0, v1, Lcom/android/server/wm/ActivityStarterImpl;->mContext:Landroid/content/Context;

    const/4 v8, 0x1

    invoke-static {v0, v5, v8}, Landroid/util/MiuiMultiWindowUtils;->getActivityOptions(Landroid/content/Context;Ljava/lang/String;Z)Landroid/app/ActivityOptions;

    move-result-object v8

    .line 560
    .local v8, "options":Landroid/app/ActivityOptions;
    if-nez v8, :cond_2

    .line 561
    return-object v4

    .line 563
    :cond_2
    iget-object v0, v1, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v5, v9}, Lmiui/security/SecurityManagerInternal;->isForceLaunchNewTask(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v1, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    .line 564
    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Lmiui/security/SecurityManagerInternal;->isApplicationLockActivity(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 565
    :cond_3
    invoke-virtual {v8}, Landroid/app/ActivityOptions;->setForceLaunchNewTask()V

    .line 567
    :cond_4
    if-eqz v7, :cond_7

    .line 568
    const-string v0, "com.miui.securitycore/com.miui.xspace.ui.activity.XSpaceResolveActivity"

    iget-object v9, v7, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 569
    const/4 v9, 0x0

    .line 570
    .local v9, "behindActivity":Lcom/android/server/wm/ActivityRecord;
    iget-object v0, v1, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v10, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v10

    .line 571
    :try_start_1
    iget-object v0, v1, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 572
    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/android/server/wm/DisplayContent;->getActivityBelow(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    move-object v9, v0

    .line 573
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574
    if-eqz v9, :cond_5

    .line 575
    invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/app/ActivityOptions;->setLaunchFromTaskId(I)V

    .line 577
    .end local v9    # "behindActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_5
    goto :goto_0

    .line 573
    .restart local v9    # "behindActivity":Lcom/android/server/wm/ActivityRecord;
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 578
    .end local v9    # "behindActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_6
    invoke-virtual {v7}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/app/ActivityOptions;->setLaunchFromTaskId(I)V

    .line 581
    :cond_7
    :goto_0
    if-eqz v4, :cond_8

    invoke-virtual/range {p8 .. p8}, Lcom/android/server/wm/SafeActivityOptions;->getOriginalOptions()Landroid/app/ActivityOptions;

    move-result-object v0

    invoke-virtual {v4, v0, v8}, Lcom/android/server/wm/SafeActivityOptions;->mergeActivityOptions(Landroid/app/ActivityOptions;Landroid/app/ActivityOptions;)Landroid/app/ActivityOptions;

    move-result-object v0

    goto :goto_1

    :cond_8
    move-object v0, v8

    .line 582
    .end local v8    # "options":Landroid/app/ActivityOptions;
    .local v0, "options":Landroid/app/ActivityOptions;
    :goto_1
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v8

    goto :goto_2

    :cond_9
    const/4 v8, 0x0

    :goto_2
    invoke-static {v8}, Lcom/android/server/wm/SafeActivityOptions;->fromBundle(Landroid/os/Bundle;)Lcom/android/server/wm/SafeActivityOptions;

    move-result-object v8

    return-object v8

    .line 552
    .end local v0    # "options":Landroid/app/ActivityOptions;
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 585
    .end local v5    # "rawPackageName":Ljava/lang/String;
    .end local v6    # "currentFullPackageName":Ljava/lang/String;
    .end local v7    # "currentFullActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_a
    return-object v4
.end method

.method checkStartActivityPermission(Lcom/android/server/wm/ActivityStarter$Request;)Z
    .locals 21
    .param p1, "request"    # Lcom/android/server/wm/ActivityStarter$Request;

    .line 611
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    iget-object v0, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    const/4 v9, 0x1

    if-eqz v0, :cond_5

    .line 612
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceImpl;->getInstance()Lcom/android/server/am/ActivityManagerServiceImpl;

    move-result-object v1

    iget-object v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->caller:Landroid/app/IApplicationThread;

    iget-object v3, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v8, Lcom/android/server/wm/ActivityStarter$Request;->intent:Landroid/content/Intent;

    iget v5, v8, Lcom/android/server/wm/ActivityStarter$Request;->userId:I

    iget-object v6, v8, Lcom/android/server/wm/ActivityStarter$Request;->callingPackage:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkRunningCompatibility(Landroid/app/IApplicationThread;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ILjava/lang/String;)Z

    move-result v0

    const/4 v10, 0x0

    if-nez v0, :cond_0

    .line 615
    return v10

    .line 617
    :cond_0
    iget v0, v8, Lcom/android/server/wm/ActivityStarter$Request;->realCallingUid:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 618
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    goto :goto_0

    :cond_1
    iget v0, v8, Lcom/android/server/wm/ActivityStarter$Request;->realCallingUid:I

    :goto_0
    move v3, v0

    .line 619
    .local v3, "callingUid":I
    iget-object v1, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->intent:Landroid/content/Intent;

    iget-object v4, v8, Lcom/android/server/wm/ActivityStarter$Request;->callingPackage:Ljava/lang/String;

    iget-object v5, v8, Lcom/android/server/wm/ActivityStarter$Request;->reason:Ljava/lang/String;

    iget-object v6, v8, Lcom/android/server/wm/ActivityStarter$Request;->resultTo:Landroid/os/IBinder;

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/server/wm/ActivityStarterImpl;->checkStorageRestricted(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ILjava/lang/String;Ljava/lang/String;Landroid/os/IBinder;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 621
    return v9

    .line 623
    :cond_2
    iget-object v0, v7, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    iget-object v4, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0, v1, v2, v4}, Lcom/android/server/wm/WindowProcessUtils;->isPackageRunning(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    .line 626
    .local v0, "calleeAlreadyStarted":Z
    const/4 v1, 0x0

    .line 627
    .local v1, "bOptions":Landroid/os/Bundle;
    iget-object v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityOptions:Lcom/android/server/wm/SafeActivityOptions;

    if-eqz v2, :cond_3

    iget-object v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityOptions:Lcom/android/server/wm/SafeActivityOptions;

    invoke-virtual {v2}, Lcom/android/server/wm/SafeActivityOptions;->getOriginalOptions()Landroid/app/ActivityOptions;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 628
    iget-object v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityOptions:Lcom/android/server/wm/SafeActivityOptions;

    invoke-virtual {v2}, Lcom/android/server/wm/SafeActivityOptions;->getOriginalOptions()Landroid/app/ActivityOptions;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 630
    :cond_3
    iget-object v11, v7, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityHelper:Lmiui/app/ActivitySecurityHelper;

    iget-object v12, v8, Lcom/android/server/wm/ActivityStarter$Request;->callingPackage:Ljava/lang/String;

    iget-object v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v14, v8, Lcom/android/server/wm/ActivityStarter$Request;->intent:Landroid/content/Intent;

    iget-object v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->resultTo:Landroid/os/IBinder;

    if-eqz v2, :cond_4

    move v15, v9

    goto :goto_1

    :cond_4
    move v15, v10

    :goto_1
    iget v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->requestCode:I

    iget-object v4, v8, Lcom/android/server/wm/ActivityStarter$Request;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 633
    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v18

    .line 630
    move/from16 v16, v2

    move/from16 v17, v0

    move/from16 v19, v3

    move-object/from16 v20, v1

    invoke-virtual/range {v11 .. v20}, Lmiui/app/ActivitySecurityHelper;->getCheckIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;ZIZIILandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    .line 635
    .local v2, "checkIntent":Landroid/content/Intent;
    if-eqz v2, :cond_5

    .line 636
    iput-object v2, v8, Lcom/android/server/wm/ActivityStarter$Request;->intent:Landroid/content/Intent;

    .line 639
    .end local v0    # "calleeAlreadyStarted":Z
    .end local v1    # "bOptions":Landroid/os/Bundle;
    .end local v2    # "checkIntent":Landroid/content/Intent;
    .end local v3    # "callingUid":I
    :cond_5
    invoke-direct/range {p0 .. p1}, Lcom/android/server/wm/ActivityStarterImpl;->resolveCheckIntent(Lcom/android/server/wm/ActivityStarter$Request;)Landroid/content/pm/ActivityInfo;

    .line 640
    return v9
.end method

.method destroyActivity(Landroid/content/pm/ActivityInfo;)V
    .locals 2
    .param p1, "aInfo"    # Landroid/content/pm/ActivityInfo;

    .line 451
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSystemReady:Z

    if-nez v0, :cond_0

    return-void

    .line 453
    :cond_0
    if-nez p1, :cond_1

    .line 454
    const-string v0, "ActivityStarterImpl"

    const-string v1, "aInfo is null!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    return-void

    .line 457
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSplashScreenServiceDelegate:Lcom/miui/server/SplashScreenServiceDelegate;

    invoke-virtual {v0, p1}, Lcom/miui/server/SplashScreenServiceDelegate;->destroyActivity(Landroid/content/pm/ActivityInfo;)V

    .line 458
    return-void
.end method

.method public finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;)Landroid/os/IBinder;
    .locals 0
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "resultCode"    # I
    .param p3, "resultData"    # Landroid/content/Intent;

    .line 420
    return-object p1
.end method

.method public finishLaunchMode(Ljava/lang/String;I)V
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 430
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->finishLaunchMode(Ljava/lang/String;I)V

    .line 431
    return-void
.end method

.method public finishLaunchedFromActivityIfNeeded(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 7
    .param p1, "starting"    # Lcom/android/server/wm/ActivityRecord;

    .line 757
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 758
    return v0

    .line 761
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 764
    .local v1, "rwc":Lcom/android/server/wm/RootWindowContainer;
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isActivityTypeHome()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 765
    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    .local v2, "displayNdx":I
    :goto_0
    if-ltz v2, :cond_2

    .line 766
    invoke-virtual {v1, v2}, Lcom/android/server/wm/RootWindowContainer;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v4

    check-cast v4, Lcom/android/server/wm/DisplayContent;

    .line 767
    .local v4, "display":Lcom/android/server/wm/DisplayContent;
    iget-boolean v5, v4, Lcom/android/server/wm/DisplayContent;->isDefaultDisplay:Z

    if-nez v5, :cond_1

    .line 768
    new-instance v5, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda0;

    invoke-direct {v5, p1}, Lcom/android/server/wm/ActivityStarterImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityRecord;)V

    invoke-virtual {v4, v5}, Lcom/android/server/wm/DisplayContent;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v5

    .line 770
    .local v5, "fromActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v5, :cond_1

    .line 771
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Finish launch from activity "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " starting="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " launchedFromPid="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v6, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " launchedFromPackage="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPackage:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v6, "ActivityStarterImpl"

    invoke-static {v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    const-string v0, "finish-launch-from"

    invoke-virtual {v5, v0, v3}, Lcom/android/server/wm/ActivityRecord;->finishIfPossible(Ljava/lang/String;Z)I

    .line 775
    return v3

    .line 765
    .end local v4    # "display":Lcom/android/server/wm/DisplayContent;
    .end local v5    # "fromActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 781
    .end local v2    # "displayNdx":I
    :cond_2
    return v0
.end method

.method public getRebootReason(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 787
    const-string v0, "android.intent.extra.USER_REQUESTED_SHUTDOWN"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 788
    .local v0, "userRequested":Z
    if-eqz v0, :cond_0

    const-string/jumbo v1, "userrequested"

    goto :goto_0

    .line 789
    :cond_0
    const-string v1, "android.intent.extra.REASON"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    nop

    .line 790
    .local v1, "reason":Ljava/lang/String;
    return-object v1
.end method

.method public getTransitionType(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)I
    .locals 2
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 800
    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 801
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v1}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isSplitMode()Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz p2, :cond_1

    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v1}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isSplitMode()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 806
    :cond_1
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v1}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isKeyguardEditor()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 807
    const v0, 0x7fffffeb

    return v0

    .line 810
    :cond_2
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isForcePortraitActivity(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 811
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isForcePortraitActivity(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 821
    :cond_3
    return v0

    .line 812
    :cond_4
    :goto_0
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    if-eqz v1, :cond_5

    .line 813
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v1, v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->setForcePortraitActivity(Z)V

    .line 815
    :cond_5
    if-eqz p2, :cond_6

    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    if-eqz v1, :cond_6

    .line 816
    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v1, v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->setForcePortraitActivity(Z)V

    .line 818
    :cond_6
    const v0, 0x7ffffff0

    return v0

    .line 802
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v1}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v1

    if-nez v1, :cond_a

    if-eqz p2, :cond_8

    .line 803
    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v1

    if-eqz v1, :cond_8

    goto :goto_3

    .line 804
    :cond_8
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isSecondStateActivity()Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7ffffffe

    goto :goto_2

    :cond_9
    const v0, 0x7ffffffc

    :goto_2
    return v0

    .line 803
    :cond_a
    :goto_3
    return v0
.end method

.method init(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/content/Context;)V
    .locals 1
    .param p1, "service"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 121
    iput-object p2, p0, Lcom/android/server/wm/ActivityStarterImpl;->mContext:Landroid/content/Context;

    .line 122
    iput-object p1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 123
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 124
    return-void
.end method

.method public isAllowedStartActivity(IILjava/lang/String;)Z
    .locals 9
    .param p1, "callingUid"    # I
    .param p2, "callingPid"    # I
    .param p3, "callingPackage"    # Ljava/lang/String;

    .line 280
    invoke-static {p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    const/4 v2, 0x1

    if-lt v0, v1, :cond_7

    .line 281
    invoke-static {p3}, Lcom/android/server/am/PendingIntentRecordImpl;->containsPendingIntent(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I

    if-eq p1, v0, :cond_7

    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 283
    invoke-virtual {v0, p1}, Lcom/android/server/wm/ActivityTaskManagerService;->hasUserVisibleWindow(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getAppOpsManager()Landroid/app/AppOpsManager;

    move-result-object v0

    .line 288
    .local v0, "appops":Landroid/app/AppOpsManager;
    iget-object v1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    .line 289
    .local v1, "display":Lcom/android/server/wm/DisplayContent;
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v3

    .line 290
    .local v3, "stack":Lcom/android/server/wm/Task;
    if-nez v3, :cond_1

    .line 291
    return v2

    .line 293
    :cond_1
    invoke-virtual {v3}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    .line 294
    .local v4, "r":Lcom/android/server/wm/ActivityRecord;
    if-nez v4, :cond_2

    .line 295
    return v2

    .line 298
    :cond_2
    iget-object v5, v4, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne p1, v5, :cond_3

    .line 299
    iput p1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I

    .line 300
    return v2

    .line 302
    :cond_3
    const/16 v5, 0x2725

    invoke-virtual {v0, v5, p1, p3}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_6

    .line 305
    iget-object v5, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mProcessMap:Lcom/android/server/wm/WindowProcessControllerMap;

    invoke-virtual {v5, p1}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcesses(I)Landroid/util/ArraySet;

    move-result-object v5

    .line 306
    .local v5, "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;"
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Landroid/util/ArraySet;->size()I

    move-result v6

    if-eqz v6, :cond_5

    .line 307
    invoke-virtual {v5}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/wm/WindowProcessController;

    .line 308
    .local v7, "app":Lcom/android/server/wm/WindowProcessController;
    if-eqz v7, :cond_4

    iget v8, v7, Lcom/android/server/wm/WindowProcessController;->mUid:I

    if-ne v8, p1, :cond_4

    .line 309
    invoke-virtual {v7}, Lcom/android/server/wm/WindowProcessController;->hasForegroundActivities()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 310
    iput p1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I

    .line 311
    return v2

    .line 313
    .end local v7    # "app":Lcom/android/server/wm/WindowProcessController;
    :cond_4
    goto :goto_0

    .line 315
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MIUILOG- Permission Denied Activity :  pkg : "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " uid : "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " tuid : "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, v4, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, "ActivityStarterImpl"

    invoke-static {v6, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    const/4 v2, 0x0

    return v2

    .line 320
    .end local v5    # "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;"
    :cond_6
    return v2

    .line 284
    .end local v0    # "appops":Landroid/app/AppOpsManager;
    .end local v1    # "display":Lcom/android/server/wm/DisplayContent;
    .end local v3    # "stack":Lcom/android/server/wm/Task;
    .end local v4    # "r":Lcom/android/server/wm/ActivityRecord;
    :cond_7
    :goto_1
    iput p1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I

    .line 285
    return v2
.end method

.method public isAllowedStartActivity(Landroid/content/Intent;IILjava/lang/String;IILandroid/content/pm/ActivityInfo;)Z
    .locals 24
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callingUid"    # I
    .param p3, "callingPid"    # I
    .param p4, "callingPackage"    # Ljava/lang/String;
    .param p5, "realCallingUid"    # I
    .param p6, "realCallingPid"    # I
    .param p7, "aInfo"    # Landroid/content/pm/ActivityInfo;

    .line 326
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p5

    move/from16 v4, p6

    move-object/from16 v5, p7

    iget-object v6, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v6}, Lcom/android/server/wm/ActivityStarterImpl;->checkAndNotify(I)V

    .line 327
    move/from16 v6, p2

    .line 328
    .local v6, "checkingUid":I
    const-string v7, "MIUILOG- Permission Denied Activity : "

    const/16 v8, 0x2710

    const/4 v9, 0x0

    const-string v10, " uid : "

    const-string v11, "ActivityStarterImpl"

    if-eq v2, v3, :cond_1

    invoke-static/range {p5 .. p5}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v12

    if-le v12, v8, :cond_1

    .line 329
    move/from16 v6, p5

    .line 330
    iget-object v12, v0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v12, v4, v3}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(II)Lcom/android/server/wm/WindowProcessController;

    move-result-object v12

    .line 331
    .local v12, "realApp":Lcom/android/server/wm/WindowProcessController;
    if-eqz v12, :cond_0

    .line 332
    iget-object v13, v12, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v13, v13, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move v12, v6

    move-object v6, v13

    move/from16 v13, p3

    .end local p4    # "callingPackage":Ljava/lang/String;
    .local v13, "callingPackage":Ljava/lang/String;
    goto :goto_0

    .line 334
    .end local v13    # "callingPackage":Ljava/lang/String;
    .restart local p4    # "callingPackage":Ljava/lang/String;
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " realPid : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " realUid : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " pid : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v13, p3

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v11, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    return v9

    .line 328
    .end local v12    # "realApp":Lcom/android/server/wm/WindowProcessController;
    :cond_1
    move/from16 v13, p3

    .line 340
    move v12, v6

    move-object/from16 v6, p4

    .end local p4    # "callingPackage":Ljava/lang/String;
    .local v6, "callingPackage":Ljava/lang/String;
    .local v12, "checkingUid":I
    :goto_0
    invoke-static {v12}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v14

    const/16 v20, 0x1

    if-lt v14, v8, :cond_b

    .line 341
    invoke-static {v6}, Lcom/android/server/am/PendingIntentRecordImpl;->containsPendingIntent(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    iget-object v8, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 342
    invoke-static {v8}, Lcom/android/server/am/PendingIntentRecordImpl;->containsPendingIntent(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    iget v8, v0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I

    if-eq v12, v8, :cond_b

    iget-object v8, v0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 344
    invoke-virtual {v8, v12}, Lcom/android/server/wm/ActivityTaskManagerService;->hasUserVisibleWindow(I)Z

    move-result v8

    if-nez v8, :cond_b

    iget-object v8, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 345
    const-string v14, "android.service.dreams.DreamActivity"

    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v8

    if-eqz v8, :cond_2

    goto/16 :goto_2

    .line 350
    :cond_2
    iget-object v8, v0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v8}, Lcom/android/server/wm/ActivityTaskManagerService;->getAppOpsManager()Landroid/app/AppOpsManager;

    move-result-object v8

    .line 351
    .local v8, "appops":Landroid/app/AppOpsManager;
    iget-object v14, v0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v14, v14, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v14}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;

    move-result-object v21

    .line 352
    .local v21, "display":Lcom/android/server/wm/DisplayContent;
    invoke-virtual/range {v21 .. v21}, Lcom/android/server/wm/DisplayContent;->getFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v22

    .line 353
    .local v22, "stack":Lcom/android/server/wm/Task;
    if-nez v22, :cond_3

    .line 354
    return v20

    .line 357
    :cond_3
    iget-object v14, v0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v14, v14, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v14}, Lcom/android/server/wm/WindowManagerService;->isKeyguardLocked()Z

    move-result v14

    const-string v15, " pkg : "

    if-eqz v14, :cond_4

    .line 358
    const/16 v14, 0x24

    invoke-direct {v0, v14, v6, v1}, Lcom/android/server/wm/ActivityStarterImpl;->recordAppBehavior(ILjava/lang/String;Landroid/content/Intent;)V

    .line 359
    const/16 v16, 0x2724

    const/16 v18, 0x0

    const-string v19, "ActivityTaskManagerServiceInjector#isAllowedStartActivity"

    move-object v14, v8

    move-object v9, v15

    move/from16 v15, v16

    move/from16 v16, v12

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v19}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_5

    .line 362
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "MIUILOG- Permission Denied Activity KeyguardLocked: "

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v11, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const/4 v7, 0x0

    return v7

    .line 357
    :cond_4
    move-object v9, v15

    .line 368
    :cond_5
    invoke-virtual/range {v22 .. v22}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v15

    .line 369
    .local v15, "r":Lcom/android/server/wm/ActivityRecord;
    if-nez v15, :cond_6

    .line 370
    return v20

    .line 373
    :cond_6
    iget-object v14, v15, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v14, v14, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v12, v14, :cond_7

    .line 374
    iget-object v7, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v7, v0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I

    .line 375
    return v20

    .line 378
    :cond_7
    const/16 v14, 0x1b

    invoke-direct {v0, v14, v6, v1}, Lcom/android/server/wm/ActivityStarterImpl;->recordAppBehavior(ILjava/lang/String;Landroid/content/Intent;)V

    .line 379
    const/16 v14, 0x2725

    invoke-virtual {v8, v14, v12, v6}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_a

    .line 382
    iget-object v14, v0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v14, v14, Lcom/android/server/wm/ActivityTaskManagerService;->mProcessMap:Lcom/android/server/wm/WindowProcessControllerMap;

    invoke-virtual {v14, v12}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcesses(I)Landroid/util/ArraySet;

    move-result-object v23

    .line 383
    .local v23, "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;"
    if-eqz v23, :cond_9

    invoke-virtual/range {v23 .. v23}, Landroid/util/ArraySet;->size()I

    move-result v14

    if-eqz v14, :cond_9

    .line 384
    invoke-virtual/range {v23 .. v23}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v2, v16

    check-cast v2, Lcom/android/server/wm/WindowProcessController;

    .line 385
    .local v2, "app":Lcom/android/server/wm/WindowProcessController;
    if-eqz v2, :cond_8

    iget v3, v2, Lcom/android/server/wm/WindowProcessController;->mUid:I

    if-ne v3, v12, :cond_8

    .line 386
    invoke-virtual {v2}, Lcom/android/server/wm/WindowProcessController;->hasForegroundActivities()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 387
    iget-object v3, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v3, v0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I

    .line 388
    return v20

    .line 390
    .end local v2    # "app":Lcom/android/server/wm/WindowProcessController;
    :cond_8
    move/from16 v2, p2

    move/from16 v3, p5

    goto :goto_1

    .line 392
    :cond_9
    const/16 v2, 0x2725

    const/16 v18, 0x0

    const-string v19, "ActivityTaskManagerServiceInjector#isAllowedStartActivity"

    move-object v14, v8

    move-object v3, v15

    .end local v15    # "r":Lcom/android/server/wm/ActivityRecord;
    .local v3, "r":Lcom/android/server/wm/ActivityRecord;
    move v15, v2

    move/from16 v16, v12

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v19}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " tuid : "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v7, v3, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    const/4 v2, 0x0

    return v2

    .line 399
    .end local v3    # "r":Lcom/android/server/wm/ActivityRecord;
    .end local v23    # "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/WindowProcessController;>;"
    .restart local v15    # "r":Lcom/android/server/wm/ActivityRecord;
    :cond_a
    return v20

    .line 346
    .end local v8    # "appops":Landroid/app/AppOpsManager;
    .end local v15    # "r":Lcom/android/server/wm/ActivityRecord;
    .end local v21    # "display":Lcom/android/server/wm/DisplayContent;
    .end local v22    # "stack":Lcom/android/server/wm/Task;
    :cond_b
    :goto_2
    iget-object v2, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v2, v0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I

    .line 347
    return v20
.end method

.method public isAppLockActivity(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "activity"    # Lcom/android/server/wm/ActivityRecord;

    .line 907
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 908
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 907
    :goto_0
    return v0
.end method

.method public isCarWithDisplay(Lcom/android/server/wm/DisplayContent;)Z
    .locals 2
    .param p1, "dc"    # Lcom/android/server/wm/DisplayContent;

    .line 222
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "displayName":Ljava/lang/String;
    sget-object v1, Lcom/android/server/wm/ActivityStarterImpl;->CARLINK_VIRTUAL_DISPLAY_SET:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    return v1

    .line 226
    .end local v0    # "displayName":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isCarWithDisplay(Lcom/android/server/wm/RootWindowContainer;Landroid/app/ActivityOptions;)Z
    .locals 3
    .param p1, "rootWindowContainer"    # Lcom/android/server/wm/RootWindowContainer;
    .param p2, "options"    # Landroid/app/ActivityOptions;

    .line 709
    if-nez p1, :cond_0

    .line 710
    const/4 v0, 0x0

    return v0

    .line 712
    :cond_0
    const/4 v0, 0x0

    .line 713
    .local v0, "displayId":I
    if-eqz p2, :cond_1

    .line 714
    invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchDisplayId()I

    move-result v0

    goto :goto_0

    .line 716
    :cond_1
    nop

    .line 717
    invoke-virtual {p1}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 718
    .local v1, "focusedRootTask":Lcom/android/server/wm/Task;
    if-eqz v1, :cond_2

    .line 719
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v0

    .line 722
    .end local v1    # "focusedRootTask":Lcom/android/server/wm/Task;
    :cond_2
    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    .line 723
    .local v1, "displayContent":Lcom/android/server/wm/DisplayContent;
    invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityStarterImpl;->isCarWithDisplay(Lcom/android/server/wm/DisplayContent;)Z

    move-result v2

    return v2
.end method

.method public logStartActivityError(ILandroid/content/Intent;)V
    .locals 3
    .param p1, "err"    # I
    .param p2, "intent"    # Landroid/content/Intent;

    .line 728
    const-string v0, "ActivityStarterImpl"

    packed-switch p1, :pswitch_data_0

    .line 747
    :pswitch_0
    goto :goto_0

    .line 730
    :pswitch_1
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Activity not started, unable to resolve "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 731
    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 730
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    goto :goto_1

    .line 734
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Activity class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 735
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 734
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    goto :goto_1

    .line 739
    :pswitch_3
    const-string v1, "Error: Activity not started, you requested to both forward and receive its result"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    goto :goto_1

    .line 743
    :pswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Activity not started, voice control not allowed for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    goto :goto_1

    .line 750
    :catch_0
    move-exception v0

    goto :goto_2

    .line 747
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Activity not started, unknown error code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 752
    :goto_1
    goto :goto_3

    .line 751
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 753
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    return-void

    :pswitch_data_0
    .packed-switch -0x61
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public moveToFrontForSplitScreen(ZZLcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "differentTopTask"    # Z
    .param p2, "avoidMoveToFront"    # Z
    .param p3, "targetRootTask"    # Lcom/android/server/wm/Task;
    .param p4, "intentActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p5, "sourceRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 832
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    iget-boolean v0, p3, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v0, :cond_0

    .line 834
    invoke-virtual {p3}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->RESUMED:Lcom/android/server/wm/ActivityRecord$State;

    .line 835
    invoke-virtual {p4, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p5, :cond_0

    .line 837
    invoke-virtual {p4, p3}, Lcom/android/server/wm/ActivityRecord;->isDescendantOf(Lcom/android/server/wm/WindowContainer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 838
    invoke-virtual {p4}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 832
    :goto_0
    return v0
.end method

.method onSystemReady()V
    .locals 2

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSystemReady:Z

    .line 128
    new-instance v0, Lcom/miui/server/SplashScreenServiceDelegate;

    iget-object v1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/SplashScreenServiceDelegate;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSplashScreenServiceDelegate:Lcom/miui/server/SplashScreenServiceDelegate;

    .line 129
    new-instance v0, Lmiui/app/ActivitySecurityHelper;

    iget-object v1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiui/app/ActivitySecurityHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityHelper:Lmiui/app/ActivitySecurityHelper;

    .line 130
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    .line 131
    const-class v0, Lcom/android/server/PowerConsumptionServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/PowerConsumptionServiceInternal;

    iput-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mPowerConsumptionServiceInternal:Lcom/android/server/PowerConsumptionServiceInternal;

    .line 132
    return-void
.end method

.method public recordNewIntent(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callingPackage"    # Ljava/lang/String;

    .line 878
    const-string v0, "com.baidu.BaiduMap"

    const-string v1, "com.tencent.map"

    const-string v2, "com.autonavi.minimap"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 883
    .local v0, "mMapList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, ""

    .line 884
    .local v1, "pkg":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 885
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 887
    :cond_0
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 888
    const-string v2, "com.miui.carlink"

    invoke-static {p2, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 891
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    .line 892
    .local v2, "dat":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 893
    return-void

    .line 897
    :cond_2
    :try_start_0
    const-string v3, "UTF-8"

    invoke-static {v2, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    .line 900
    goto :goto_0

    .line 898
    :catch_0
    move-exception v3

    .line 899
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 901
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :goto_0
    const-string v3, "ActivityStarterImpl"

    const-string v4, "Ready To Send BroadCast To carlink"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    new-instance v3, Lcom/android/server/wm/ActivityCarWithStarterImpl;

    invoke-direct {v3}, Lcom/android/server/wm/ActivityCarWithStarterImpl;-><init>()V

    .line 903
    .local v3, "activityCarWithStarter":Lcom/android/server/wm/ActivityCarWithStarterImpl;
    iget-object v4, p0, Lcom/android/server/wm/ActivityStarterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4, p2, v1, v2}, Lcom/android/server/wm/ActivityCarWithStarterImpl;->recordCarWithIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    return-void

    .line 889
    .end local v2    # "dat":Ljava/lang/String;
    .end local v3    # "activityCarWithStarter":Lcom/android/server/wm/ActivityCarWithStarterImpl;
    :cond_3
    :goto_1
    return-void
.end method

.method requestSplashScreen(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Lcom/android/server/wm/SafeActivityOptions;Landroid/app/IApplicationThread;)Landroid/content/Intent;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p3, "options"    # Lcom/android/server/wm/SafeActivityOptions;
    .param p4, "caller"    # Landroid/app/IApplicationThread;

    .line 463
    iget-boolean v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSystemReady:Z

    if-nez v0, :cond_0

    return-object p1

    .line 465
    :cond_0
    if-eqz p1, :cond_5

    if-nez p2, :cond_1

    goto :goto_1

    .line 471
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 472
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1, p4}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Landroid/app/IApplicationThread;)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 473
    .local v1, "callerApp":Lcom/android/server/wm/WindowProcessController;
    if-eqz p3, :cond_2

    .line 474
    iget-object v2, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {p3, p1, p2, v1, v2}, Lcom/android/server/wm/SafeActivityOptions;->getOptions(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Lcom/android/server/wm/WindowProcessController;Lcom/android/server/wm/ActivityTaskSupervisor;)Landroid/app/ActivityOptions;

    move-result-object v2

    goto :goto_0

    .line 475
    :cond_2
    const/4 v2, 0x0

    :goto_0
    move-object v1, v2

    .line 476
    .local v1, "checkedOptions":Landroid/app/ActivityOptions;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478
    if-eqz v1, :cond_4

    .line 479
    invoke-virtual {v1}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v0

    const/4 v2, 0x6

    if-eq v0, v2, :cond_3

    .line 480
    invoke-virtual {v1}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    .line 481
    :cond_3
    const-string v0, "ActivityStarterImpl"

    const-string v2, "The Activity is in freeForm|split windowing mode !"

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    return-object p1

    .line 485
    :cond_4
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSplashScreenServiceDelegate:Lcom/miui/server/SplashScreenServiceDelegate;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/SplashScreenServiceDelegate;->requestSplashScreen(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 476
    .end local v1    # "checkedOptions":Landroid/app/ActivityOptions;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 466
    :cond_5
    :goto_1
    const-string v0, "ActivityStarterImpl"

    const-string v1, "Intent or aInfo is null!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    return-object p1
.end method

.method resolveSplashIntent(Landroid/content/pm/ActivityInfo;Landroid/content/Intent;Landroid/app/ProfilerInfo;III)Landroid/content/pm/ActivityInfo;
    .locals 10
    .param p1, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "profilerInfo"    # Landroid/app/ProfilerInfo;
    .param p4, "userId"    # I
    .param p5, "filterCallingUid"    # I
    .param p6, "callingPid"    # I

    .line 491
    if-nez p2, :cond_0

    .line 492
    return-object p1

    .line 495
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    .line 496
    .local v8, "component":Landroid/content/ComponentName;
    if-nez v8, :cond_1

    .line 497
    return-object p1

    .line 499
    :cond_1
    const-string v0, "com.miui.systemAdSolution"

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 500
    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.miui.systemAdSolution.splashscreen.SplashActivity"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 501
    move-object v9, p0

    iget-object v0, v9, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v1, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/wm/ActivityTaskSupervisor;->resolveActivity(Landroid/content/Intent;Ljava/lang/String;ILandroid/app/ProfilerInfo;III)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    return-object v0

    .line 500
    :cond_2
    move-object v9, p0

    goto :goto_0

    .line 499
    :cond_3
    move-object v9, p0

    .line 504
    :goto_0
    return-object p1
.end method

.method public shouldRebootReasonCheckNull()Z
    .locals 2

    .line 795
    const-string v0, "persist.sys.stability.rebootreason_check"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public skipForSplitScreen(Lcom/android/server/wm/Task;Lcom/android/server/wm/Task;Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;I)Z
    .locals 6
    .param p1, "targetTask"    # Lcom/android/server/wm/Task;
    .param p2, "targetRootTask"    # Lcom/android/server/wm/Task;
    .param p3, "topRootTask"    # Lcom/android/server/wm/Task;
    .param p4, "top"    # Lcom/android/server/wm/ActivityRecord;
    .param p5, "launchFlags"    # I

    .line 843
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    iget-boolean v2, p2, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z

    if-eqz v2, :cond_0

    .line 846
    invoke-virtual {p2}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    if-eq p3, p2, :cond_0

    .line 848
    invoke-virtual {p4, p2}, Lcom/android/server/wm/ActivityRecord;->isDescendantOf(Lcom/android/server/wm/WindowContainer;)Z

    move-result v2

    if-nez v2, :cond_0

    const/high16 v2, 0x8000000

    and-int/2addr v2, p5

    if-eqz v2, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v0

    .line 850
    .local v2, "startForSplit":Z
    :goto_0
    const-string v3, "ActivityStarterImpl"

    if-eqz v2, :cond_1

    .line 851
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip for multiple task, top : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    return v1

    .line 855
    :cond_1
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v4

    iget-object v5, p4, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isEmbeddingEnabledForPackage(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/high16 v4, 0x20000000

    and-int/2addr v4, p5

    if-nez v4, :cond_2

    iget v4, p4, Lcom/android/server/wm/ActivityRecord;->launchMode:I

    if-ne v1, v4, :cond_3

    :cond_2
    move v4, v1

    goto :goto_1

    :cond_3
    move v4, v0

    .line 858
    .local v4, "startForAE":Z
    :goto_1
    if-eqz v4, :cond_4

    .line 859
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "skip for single top activity, top : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    return v1

    .line 863
    :cond_4
    return v0
.end method

.method public startActivityUncheckedBefore(Lcom/android/server/wm/ActivityRecord;)V
    .locals 11
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 697
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mPowerConsumptionServiceInternal:Lcom/android/server/PowerConsumptionServiceInternal;

    if-eqz v0, :cond_0

    .line 698
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/PowerConsumptionServiceInternal;->noteStartActivityForPowerConsumption(Ljava/lang/String;)V

    .line 701
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/ActivityStarterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 702
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v4

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I

    move-result v5

    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget v7, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromUid:I

    iget v8, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I

    iget-object v9, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPackage:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 704
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getProcessName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v10

    invoke-virtual {v0, v1, v10}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(Ljava/lang/String;I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    move v10, v0

    .line 701
    invoke-interface/range {v2 .. v10}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onActivityStartUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V

    .line 705
    return-void
.end method

.method public triggerLaunchMode(Ljava/lang/String;I)V
    .locals 0
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 426
    return-void
.end method

.method public updateLastStartActivityUid(Ljava/lang/String;I)V
    .locals 7
    .param p1, "foregroundPackageName"    # Ljava/lang/String;
    .param p2, "lastUid"    # I

    .line 511
    if-nez p1, :cond_0

    return-void

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mDefaultHomePkgNames:Ljava/util/List;

    if-nez v0, :cond_3

    .line 513
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 514
    .local v0, "homeActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v1

    .line 516
    .local v1, "pm":Landroid/content/pm/IPackageManager;
    :try_start_0
    invoke-interface {v1, v0}, Landroid/content/pm/IPackageManager;->getHomeActivities(Ljava/util/List;)Landroid/content/ComponentName;

    move-result-object v2

    .line 517
    .local v2, "currentDefaultHome":Landroid/content/ComponentName;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 518
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 519
    .local v4, "info":Landroid/content/pm/ResolveInfo;
    iget-object v5, p0, Lcom/android/server/wm/ActivityStarterImpl;->mDefaultHomePkgNames:Ljava/util/List;

    if-nez v5, :cond_1

    .line 520
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/android/server/wm/ActivityStarterImpl;->mDefaultHomePkgNames:Ljava/util/List;

    .line 522
    :cond_1
    iget-object v5, p0, Lcom/android/server/wm/ActivityStarterImpl;->mDefaultHomePkgNames:Ljava/util/List;

    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    nop

    .end local v4    # "info":Landroid/content/pm/ResolveInfo;
    goto :goto_0

    .line 527
    .end local v2    # "currentDefaultHome":Landroid/content/ComponentName;
    :cond_2
    goto :goto_1

    .line 525
    :catch_0
    move-exception v2

    .line 529
    .end local v0    # "homeActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    .end local v1    # "pm":Landroid/content/pm/IPackageManager;
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/server/wm/ActivityStarterImpl;->mDefaultHomePkgNames:Ljava/util/List;

    if-nez v0, :cond_4

    return-void

    .line 530
    :cond_4
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 531
    iput p2, p0, Lcom/android/server/wm/ActivityStarterImpl;->mLastStartActivityUid:I

    .line 533
    :cond_5
    return-void
.end method
