public class com.android.server.wm.MiuiFreeFormActivityStack extends com.android.server.wm.MiuiFreeFormActivityStackStub {
	 /* .source "MiuiFreeFormActivityStack.java" */
	 /* # instance fields */
	 private final java.lang.String TAG;
	 Boolean isNormalFreeForm;
	 Integer mCornerPosition;
	 private final android.graphics.Rect mEnterMiniFreeformFromRect;
	 private java.lang.String mEnterMiniFreeformReason;
	 Integer mFreeFormLaunchFromTaskId;
	 Float mFreeFormScale;
	 private Boolean mHadHideStackFromFullScreen;
	 Boolean mHasHadStackAdded;
	 Boolean mIsForegroundPin;
	 Boolean mIsFrontFreeFormStackInfo;
	 Boolean mIsLandcapeFreeform;
	 Integer mMiuiFreeFromWindowMode;
	 private Boolean mNeedAnimation;
	 android.graphics.Rect mPinFloatingWindowPos;
	 Boolean mShouldDelayDispatchFreeFormStackModeChanged;
	 com.android.server.wm.Task mTask;
	 Long pinActiveTime;
	 Long timestamp;
	 /* # direct methods */
	 public com.android.server.wm.MiuiFreeFormActivityStack ( ) {
		 /* .locals 1 */
		 /* .param p1, "task" # Lcom/android/server/wm/Task; */
		 /* .line 147 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;-><init>(Lcom/android/server/wm/Task;I)V */
		 /* .line 148 */
		 return;
	 } // .end method
	 public com.android.server.wm.MiuiFreeFormActivityStack ( ) {
		 /* .locals 9 */
		 /* .param p1, "task" # Lcom/android/server/wm/Task; */
		 /* .param p2, "miuiFreeFromWindowMode" # I */
		 /* .line 150 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStackStub;-><init>()V */
		 /* .line 31 */
		 final String v0 = "MiuiFreeFormActivityStack"; // const-string v0, "MiuiFreeFormActivityStack"
		 this.TAG = v0;
		 /* .line 33 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHasHadStackAdded:Z */
		 /* .line 34 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z */
		 /* .line 41 */
		 /* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z */
		 /* .line 42 */
		 /* iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z */
		 /* .line 43 */
		 /* new-instance v2, Landroid/graphics/Rect; */
		 /* invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V */
		 this.mPinFloatingWindowPos = v2;
		 /* .line 47 */
		 int v2 = -1; // const/4 v2, -0x1
		 /* iput v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mCornerPosition:I */
		 /* .line 48 */
		 /* new-instance v2, Landroid/graphics/Rect; */
		 /* invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V */
		 this.mEnterMiniFreeformFromRect = v2;
		 /* .line 50 */
		 /* iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
		 /* .line 52 */
		 /* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z */
		 /* .line 53 */
		 /* iput v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormLaunchFromTaskId:I */
		 /* .line 151 */
		 this.mTask = p1;
		 /* .line 152 */
		 v1 = this.mWmService;
		 v1 = this.mAtmService;
		 v1 = this.mMiuiFreeFormManagerService;
		 v1 = 		 (( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).getStackPackageName ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
		 /* iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
		 /* .line 155 */
		 (( com.android.server.wm.Task ) p1 ).getTopNonFinishingActivity ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity(Z)Lcom/android/server/wm/ActivityRecord;
		 /* .line 156 */
		 /* .local v1, "top":Lcom/android/server/wm/ActivityRecord; */
		 /* new-instance v2, Landroid/content/Intent; */
		 final String v3 = "android.intent.action.MAIN"; // const-string v3, "android.intent.action.MAIN"
		 /* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 /* .line 157 */
		 /* .local v2, "intent":Landroid/content/Intent; */
		 final String v3 = "android.intent.category.LAUNCHER"; // const-string v3, "android.intent.category.LAUNCHER"
		 (( android.content.Intent ) v2 ).addCategory ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 158 */
		 (( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).getStackPackageName ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
		 (( android.content.Intent ) v2 ).setPackage ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 159 */
		 v3 = this.mAtmService;
		 v3 = this.mContext;
		 (( android.content.Context ) v3 ).getPackageManager ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
		 (( android.content.pm.PackageManager ) v3 ).resolveActivity ( v2, v0 ); // invoke-virtual {v3, v2, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
		 /* .line 160 */
		 /* .local v3, "rInfo":Landroid/content/pm/ResolveInfo; */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 161 */
			 v4 = 			 (( com.android.server.wm.ActivityRecord ) v1 ).getOverrideOrientation ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getOverrideOrientation()I
			 v4 = 			 android.util.MiuiMultiWindowUtils .isOrientationLandscape ( v4 );
			 /* iput-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
			 /* .line 162 */
		 } // :cond_0
		 if ( v3 != null) { // if-eqz v3, :cond_1
			 /* .line 163 */
			 v4 = this.activityInfo;
			 /* iget v4, v4, Landroid/content/pm/ActivityInfo;->screenOrientation:I */
			 v4 = 			 android.util.MiuiMultiWindowUtils .isOrientationLandscape ( v4 );
			 /* iput-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
			 /* .line 165 */
		 } // :cond_1
	 } // :goto_0
	 /* iget-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
	 android.util.MiuiMultiWindowAdapter .getForceLandscapeApplication ( );
	 v5 = 	 (( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).getStackPackageName ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
	 /* or-int/2addr v4, v5 */
	 /* iput-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
	 /* .line 167 */
	 (( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).setStackFreeFormMode ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V
	 /* .line 168 */
	 v4 = this.mTask;
	 v4 = this.mTaskSupervisor;
	 (( com.android.server.wm.ActivityTaskSupervisor ) v4 ).getLaunchParamsController ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
	 v4 = 	 (( com.android.server.wm.LaunchParamsController ) v4 ).hasFreeformDesktopMemory ( p1 ); // invoke-virtual {v4, p1}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z
	 /* if-nez v4, :cond_2 */
	 /* .line 169 */
	 v4 = this.mAtmService;
	 v4 = this.mContext;
	 /* iget-boolean v5, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
	 /* iget-boolean v6, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
	 v7 = this.mAtmService;
	 v7 = this.mContext;
	 /* .line 170 */
	 v7 = 	 com.android.server.wm.MiuiDesktopModeUtils .isActive ( v7 );
	 (( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).getStackPackageName ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
	 /* .line 169 */
	 v4 = 	 android.util.MiuiMultiWindowUtils .getOriFreeformScale ( v4,v5,v6,v7,v8 );
	 /* iput v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
	 /* .line 172 */
} // :cond_2
v4 = this.mTask;
v4 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v4 ).getLaunchParamsController ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
v5 = this.mTask;
v4 = (( com.android.server.wm.LaunchParamsController ) v4 ).getFreeformLastScale ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F
/* iput v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* .line 174 */
} // :goto_1
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z */
/* .line 175 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z */
/* .line 210 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getApplicationName ( ) {
/* .locals 4 */
/* .line 243 */
int v0 = 0; // const/4 v0, 0x0
/* .line 244 */
/* .local v0, "packageManager":Landroid/content/pm/PackageManager; */
int v1 = 0; // const/4 v1, 0x0
/* .line 246 */
/* .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
try { // :try_start_0
v2 = this.mTask;
v2 = this.mAtmService;
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* move-object v0, v2 */
/* .line 247 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).getStackPackageName ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
int v3 = 0; // const/4 v3, 0x0
(( android.content.pm.PackageManager ) v0 ).getApplicationInfo ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v1, v2 */
/* .line 250 */
/* .line 248 */
/* :catch_0 */
/* move-exception v2 */
/* .line 249 */
/* .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
int v1 = 0; // const/4 v1, 0x0
/* .line 251 */
} // .end local v2 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
final String v2 = ""; // const-string v2, ""
/* .line 252 */
/* .local v2, "applicationName":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 253 */
(( android.content.pm.PackageManager ) v0 ).getApplicationLabel ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
/* move-object v2, v3 */
/* check-cast v2, Ljava/lang/String; */
/* .line 255 */
} // :cond_0
} // .end method
public java.lang.String getEnterMiniFreeformReason ( ) {
/* .locals 1 */
/* .line 72 */
v0 = this.mEnterMiniFreeformReason;
} // .end method
public android.graphics.Rect getEnterMiniFreeformRect ( ) {
/* .locals 2 */
/* .line 68 */
/* new-instance v0, Landroid/graphics/Rect; */
v1 = this.mEnterMiniFreeformFromRect;
/* invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V */
} // .end method
public Integer getFreeFormLaunchFromTaskId ( ) {
/* .locals 1 */
/* .line 129 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormLaunchFromTaskId:I */
} // .end method
public Float getFreeFormScale ( ) {
/* .locals 1 */
/* .line 121 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
} // .end method
public miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo getMiuiFreeFormStackInfo ( ) {
/* .locals 5 */
/* .line 304 */
/* new-instance v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
/* invoke-direct {v0}, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;-><init>()V */
/* .line 305 */
/* .local v0, "freeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
v1 = this.mTask;
v1 = (( com.android.server.wm.Task ) v1 ).getRootTaskId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->stackId:I */
/* .line 306 */
v1 = this.mTask;
(( com.android.server.wm.Task ) v1 ).getBounds ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
this.bounds = v1;
/* .line 307 */
/* iget v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
/* iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I */
/* .line 308 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).getStackPackageName ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
this.packageName = v1;
/* .line 309 */
v1 = this.mTask;
v1 = (( com.android.server.wm.Task ) v1 ).getDisplayId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getDisplayId()I
/* iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->displayId:I */
/* .line 310 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).getUid ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getUid()I
/* iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->userId:I */
/* .line 311 */
v1 = this.mTask;
(( com.android.server.wm.Task ) v1 ).getConfiguration ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getConfiguration()Landroid/content/res/Configuration;
this.configuration = v1;
/* .line 312 */
/* iget v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
int v2 = -1; // const/4 v2, -0x1
/* if-ne v1, v2, :cond_0 */
/* .line 313 */
/* new-instance v1, Landroid/graphics/Rect; */
/* invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V */
this.smallWindowBounds = v1;
/* .line 314 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_1 */
/* .line 315 */
/* new-instance v1, Landroid/graphics/Rect; */
v2 = this.bounds;
/* invoke-direct {v1, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V */
/* .line 316 */
/* .local v1, "smallWindowBounds":Landroid/graphics/Rect; */
/* iget v2, v1, Landroid/graphics/Rect;->left:I */
/* .line 317 */
/* .local v2, "left":I */
/* iget v3, v1, Landroid/graphics/Rect;->top:I */
/* .line 318 */
/* .local v3, "top":I */
/* iget v4, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
(( android.graphics.Rect ) v1 ).scale ( v4 ); // invoke-virtual {v1, v4}, Landroid/graphics/Rect;->scale(F)V
/* .line 319 */
(( android.graphics.Rect ) v1 ).offsetTo ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 320 */
this.smallWindowBounds = v1;
/* .line 322 */
} // .end local v1 # "smallWindowBounds":Landroid/graphics/Rect;
} // .end local v2 # "left":I
} // .end local v3 # "top":I
} // :cond_1
} // :goto_0
/* iget v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->freeFormScale:F */
/* .line 323 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).inPinMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
/* iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->inPinMode:Z */
/* .line 324 */
/* iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z */
/* iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->isForegroundPin:Z */
/* .line 325 */
v1 = this.mPinFloatingWindowPos;
this.pinFloatingWindowPos = v1;
/* .line 326 */
/* iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
/* iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->isNormalFreeForm:Z */
/* .line 327 */
/* iget v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mCornerPosition:I */
/* iput v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->cornerPosition:I */
/* .line 328 */
/* new-instance v1, Landroid/graphics/Rect; */
v2 = this.mEnterMiniFreeformFromRect;
/* invoke-direct {v1, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V */
this.enterMiniFreeformFromRect = v1;
/* .line 329 */
v1 = this.mEnterMiniFreeformReason;
this.enterMiniFreeformReason = v1;
/* .line 330 */
/* iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
/* iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->isLandcapeFreeform:Z */
/* .line 331 */
/* iget-wide v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->timestamp:J */
/* iput-wide v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->timestamp:J */
/* .line 332 */
/* iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z */
/* iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->hadHideStackFormFullScreen:Z */
/* .line 333 */
/* iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z */
/* iput-boolean v1, v0, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->needAnimation:Z */
/* .line 334 */
} // .end method
public Integer getMiuiFreeFromWindowMode ( ) {
/* .locals 1 */
/* .line 109 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
} // .end method
public java.lang.String getStackPackageName ( ) {
/* .locals 4 */
/* .line 222 */
v0 = this.mTask;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 223 */
/* .line 225 */
} // :cond_0
v0 = this.realActivity;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mTask;
v0 = this.realActivity;
/* .line 226 */
(( android.content.ComponentName ) v0 ).flattenToString ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
final String v2 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v2, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
v0 = (( java.lang.String ) v2 ).contains ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mTask;
v0 = this.behindAppLockPkg;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mTask;
v0 = this.behindAppLockPkg;
/* .line 227 */
v0 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-lez v0, :cond_1 */
/* .line 228 */
v0 = this.mTask;
v0 = this.behindAppLockPkg;
/* .line 230 */
} // :cond_1
v0 = this.mTask;
v0 = this.origActivity;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 231 */
v0 = this.mTask;
v0 = this.origActivity;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 233 */
} // :cond_2
v0 = this.mTask;
v0 = this.realActivity;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 234 */
v0 = this.mTask;
v0 = this.realActivity;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 236 */
} // :cond_3
v0 = this.mTask;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.wm.Task ) v0 ).getTopActivity ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 237 */
v0 = this.mTask;
(( com.android.server.wm.Task ) v0 ).getTopActivity ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
v0 = this.packageName;
/* .line 239 */
} // :cond_4
} // .end method
public Integer getUid ( ) {
/* .locals 2 */
/* .line 213 */
v0 = this.mTask;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.realActivity;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mTask;
v0 = this.realActivity;
/* .line 214 */
(( android.content.ComponentName ) v0 ).flattenToString ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
final String v1 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v1, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
v0 = (( java.lang.String ) v1 ).contains ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mTask;
/* iget v0, v0, Lcom/android/server/wm/Task;->originatingUid:I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 216 */
v0 = this.mTask;
/* iget v0, v0, Lcom/android/server/wm/Task;->originatingUid:I */
/* .line 218 */
} // :cond_0
v0 = this.mTask;
/* iget v0, v0, Lcom/android/server/wm/Task;->mUserId:I */
} // .end method
Boolean inMiniPinMode ( ) {
/* .locals 2 */
/* .line 93 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
Boolean inNormalPinMode ( ) {
/* .locals 2 */
/* .line 89 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
Boolean inPinMode ( ) {
/* .locals 2 */
/* .line 85 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_1 */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
Boolean isFrontFreeFormStackInfo ( ) {
/* .locals 1 */
/* .line 350 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z */
} // .end method
Boolean isHideStackFromFullScreen ( ) {
/* .locals 1 */
/* .line 338 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z */
} // .end method
public Boolean isInFreeFormMode ( ) {
/* .locals 1 */
/* .line 117 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isInMiniFreeFormMode ( ) {
/* .locals 2 */
/* .line 113 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public Boolean isLandcapeFreeform ( ) {
/* .locals 1 */
/* .line 101 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
} // .end method
public Boolean isNeedAnimation ( ) {
/* .locals 1 */
/* .line 60 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z */
} // .end method
public Boolean isNormalFreeForm ( ) {
/* .locals 1 */
/* .line 140 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
} // .end method
void onMiuiFreeFormStasckAdded ( ) {
/* .locals 0 */
/* .line 57 */
return;
} // .end method
public void setCornerPosition ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "cornerPosition" # I */
/* .line 97 */
/* iput p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mCornerPosition:I */
/* .line 98 */
return;
} // .end method
public void setEnterMiniFreeformReason ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "enterMiniFreeformReason" # Ljava/lang/String; */
/* .line 76 */
this.mEnterMiniFreeformReason = p1;
/* .line 77 */
return;
} // .end method
public void setEnterMiniFreeformRect ( android.graphics.Rect p0 ) {
/* .locals 1 */
/* .param p1, "enterMiniFreeformFromRect" # Landroid/graphics/Rect; */
/* .line 80 */
/* if-nez p1, :cond_0 */
return;
/* .line 81 */
} // :cond_0
v0 = this.mEnterMiniFreeformFromRect;
(( android.graphics.Rect ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 82 */
return;
} // .end method
public void setFreeformScale ( Float p0 ) {
/* .locals 0 */
/* .param p1, "scale" # F */
/* .line 125 */
/* iput p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* .line 126 */
return;
} // .end method
void setFreeformTimestamp ( Long p0 ) {
/* .locals 0 */
/* .param p1, "lastTimestamp" # J */
/* .line 143 */
/* iput-wide p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->timestamp:J */
/* .line 144 */
return;
} // .end method
void setHideStackFromFullScreen ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "hidden" # Z */
/* .line 342 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z */
/* .line 343 */
return;
} // .end method
void setIsFrontFreeFormStackInfo ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isFrontFreeFormStackInfo" # Z */
/* .line 346 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z */
/* .line 347 */
return;
} // .end method
public void setNeedAnimation ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "needAnimation" # Z */
/* .line 64 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z */
/* .line 65 */
return;
} // .end method
public void setStackFreeFormMode ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mode" # I */
/* .line 105 */
/* iput p1, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
/* .line 106 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 260 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 261 */
/* .local v0, "info":Ljava/lang/StringBuilder; */
final String v1 = "MiuiFreeFormActivityStack{"; // const-string v1, "MiuiFreeFormActivityStack{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 262 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mStackID="; // const-string v2, " mStackID="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTask;
v2 = (( com.android.server.wm.Task ) v2 ).getRootTaskId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 265 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mPackageName="; // const-string v2, " mPackageName="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.MiuiFreeFormActivityStack ) p0 ).getStackPackageName ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 266 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mFreeFormScale="; // const-string v2, " mFreeFormScale="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 269 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mMiuiFreeFromWindowMode="; // const-string v2, " mMiuiFreeFromWindowMode="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 270 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mConfiguration="; // const-string v2, " mConfiguration="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTask;
(( com.android.server.wm.Task ) v2 ).getConfiguration ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getConfiguration()Landroid/content/res/Configuration;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 274 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mNeedAnimation="; // const-string v2, " mNeedAnimation="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mNeedAnimation:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 276 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mIsLandcapeFreeform="; // const-string v2, " mIsLandcapeFreeform="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 289 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mPinFloatingWindowPos="; // const-string v2, " mPinFloatingWindowPos="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mPinFloatingWindowPos;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 291 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " isNormalFreeForm="; // const-string v2, " isNormalFreeForm="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 292 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mHadHideStackFromFullScreen= "; // const-string v2, "mHadHideStackFromFullScreen= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mHadHideStackFromFullScreen:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 293 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mIsFrontFreeFormStackInfo= "; // const-string v2, "mIsFrontFreeFormStackInfo= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 294 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " timestamp="; // const-string v2, " timestamp="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->timestamp:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 295 */
v1 = this.mTask;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 296 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mTask.inFreeformWindowingMode()="; // const-string v2, " mTask.inFreeformWindowingMode()="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTask;
v2 = (( com.android.server.wm.Task ) v2 ).inFreeformWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 297 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mTask.originatingUid="; // const-string v2, " mTask.originatingUid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTask;
/* iget v2, v2, Lcom/android/server/wm/Task;->originatingUid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 299 */
} // :cond_0
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 300 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
