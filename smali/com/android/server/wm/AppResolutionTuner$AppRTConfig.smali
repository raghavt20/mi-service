.class Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;
.super Ljava/lang/Object;
.source "AppResolutionTuner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/AppResolutionTuner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppRTConfig"
.end annotation


# instance fields
.field private activityWhitelist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private filteredWindows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private scale:F

.field private scalingFlow:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/wm/AppResolutionTuner;


# direct methods
.method static bridge synthetic -$$Nest$mgetFilteredWindows(Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;)Ljava/util/List;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->getFilteredWindows()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetScale(Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;)F
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->getScale()F

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetScalingFlow(Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->getScalingFlow()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misActivityWhitelist(Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->isActivityWhitelist(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Lcom/android/server/wm/AppResolutionTuner;FLjava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/wm/AppResolutionTuner;
    .param p2, "scale"    # F
    .param p4, "scalingFlow"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 160
    .local p3, "filteredWindows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "activityWhitelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->this$0:Lcom/android/server/wm/AppResolutionTuner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scale:F

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->filteredWindows:Ljava/util/List;

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->activityWhitelist:Ljava/util/List;

    .line 158
    const-string/jumbo v0, "wms"

    iput-object v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scalingFlow:Ljava/lang/String;

    .line 161
    iput p2, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scale:F

    .line 162
    iput-object p3, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->filteredWindows:Ljava/util/List;

    .line 163
    iput-object p4, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scalingFlow:Ljava/lang/String;

    .line 164
    iput-object p5, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->activityWhitelist:Ljava/util/List;

    .line 165
    return-void
.end method

.method private getFilteredWindows()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->filteredWindows:Ljava/util/List;

    return-object v0
.end method

.method private getScale()F
    .locals 1

    .line 168
    iget v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scale:F

    return v0
.end method

.method private getScalingFlow()Ljava/lang/String;
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scalingFlow:Ljava/lang/String;

    return-object v0
.end method

.method private isActivityWhitelist(Ljava/lang/String;)Z
    .locals 4
    .param p1, "target"    # Ljava/lang/String;

    .line 176
    iget-object v0, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->activityWhitelist:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 177
    .local v1, "s":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "target: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " list: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isEmpty: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "APP_RT"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    const/4 v0, 0x1

    return v0

    .line 180
    .end local v1    # "s":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 181
    :cond_1
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppRTConfig{scale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scale:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", filteredWindows="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->filteredWindows:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", activityWhitelist="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->activityWhitelist:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scalingFlow=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->scalingFlow:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
