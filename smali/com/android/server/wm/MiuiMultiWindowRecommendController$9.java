class com.android.server.wm.MiuiMultiWindowRecommendController$9 extends miuix.animation.listener.TransitionListener {
	 /* .source "MiuiMultiWindowRecommendController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->initFolmeConfig()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendController this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendController$9 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendController; */
/* .line 305 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiuix/animation/listener/TransitionListener;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onBegin ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .line 308 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mShowHideAnimConfig onBegin: toTag= "; // const-string v1, "mShowHideAnimConfig onBegin: toTag= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMultiWindowRecommendController"; // const-string v1, "MiuiMultiWindowRecommendController"
android.util.Slog .d ( v1,v0 );
/* .line 309 */
final String v0 = "freeFormRecommendShow"; // const-string v0, "freeFormRecommendShow"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 310 */
		 v0 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v0 );
		 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).setCornerRadiusAndShadow ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setCornerRadiusAndShadow(Landroid/view/View;)V
		 /* .line 311 */
	 } // :cond_0
	 /* const-string/jumbo v0, "splitScreenRecommendShow" */
	 v0 = 	 (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 v0 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v0 );
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 312 */
			 v0 = this.this$0;
			 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v0 );
			 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).setCornerRadiusAndShadow ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setCornerRadiusAndShadow(Landroid/view/View;)V
			 /* .line 313 */
		 } // :cond_1
		 final String v0 = "freeFormRecommendHide"; // const-string v0, "freeFormRecommendHide"
		 v0 = 		 (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 v0 = this.this$0;
			 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v0 );
			 if ( v0 != null) { // if-eqz v0, :cond_2
				 /* .line 314 */
				 v0 = this.this$0;
				 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v0 );
				 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).resetShadow ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->resetShadow(Landroid/view/View;)V
				 /* .line 315 */
			 } // :cond_2
			 /* const-string/jumbo v0, "splitScreenRecommendHide" */
			 v0 = 			 (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v0 != null) { // if-eqz v0, :cond_3
				 v0 = this.this$0;
				 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v0 );
				 if ( v0 != null) { // if-eqz v0, :cond_3
					 /* .line 316 */
					 v0 = this.this$0;
					 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v0 );
					 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).resetShadow ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->resetShadow(Landroid/view/View;)V
					 /* .line 319 */
				 } // :cond_3
			 } // :goto_0
			 return;
		 } // .end method
		 public void onComplete ( java.lang.Object p0 ) {
			 /* .locals 2 */
			 /* .param p1, "toTag" # Ljava/lang/Object; */
			 /* .line 323 */
			 /* invoke-super {p0, p1}, Lmiuix/animation/listener/TransitionListener;->onComplete(Ljava/lang/Object;)V */
			 /* .line 324 */
			 /* new-instance v0, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v1 = "mShowHideAnimConfig onComplete: toTag= "; // const-string v1, "mShowHideAnimConfig onComplete: toTag= "
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 final String v1 = "MiuiMultiWindowRecommendController"; // const-string v1, "MiuiMultiWindowRecommendController"
			 android.util.Slog .d ( v1,v0 );
			 /* .line 325 */
			 final String v0 = "freeFormRecommendHide"; // const-string v0, "freeFormRecommendHide"
			 v0 = 			 (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 int v1 = 4; // const/4 v1, 0x4
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 v0 = this.this$0;
				 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v0 );
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 /* .line 326 */
					 v0 = this.this$0;
					 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v0 );
					 (( com.android.server.wm.FreeFormRecommendLayout ) v0 ).setVisibility ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/FreeFormRecommendLayout;->setVisibility(I)V
					 /* .line 327 */
					 v0 = this.this$0;
					 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).removeFreeFormRecommendView ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V
					 /* .line 328 */
				 } // :cond_0
				 /* const-string/jumbo v0, "splitScreenRecommendHide" */
				 v0 = 				 (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v0 != null) { // if-eqz v0, :cond_1
					 v0 = this.this$0;
					 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v0 );
					 if ( v0 != null) { // if-eqz v0, :cond_1
						 /* .line 329 */
						 v0 = this.this$0;
						 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v0 );
						 (( com.android.server.wm.SplitScreenRecommendLayout ) v0 ).setVisibility ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->setVisibility(I)V
						 /* .line 330 */
						 v0 = this.this$0;
						 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).removeSplitScreenRecommendView ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V
						 /* .line 332 */
					 } // :cond_1
				 } // :goto_0
				 return;
			 } // .end method
