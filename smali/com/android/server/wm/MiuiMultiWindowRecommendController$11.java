class com.android.server.wm.MiuiMultiWindowRecommendController$11 implements java.lang.Runnable {
	 /* .source "MiuiMultiWindowRecommendController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendController this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendController$11 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendController; */
/* .line 433 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 436 */
v0 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 438 */
	 int v0 = 0; // const/4 v0, 0x0
	 int v1 = 0; // const/4 v1, 0x0
	 try { // :try_start_0
		 v2 = this.this$0;
		 v2 = this.mWindowManager;
		 v3 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmFreeFormRecommendLayout ( v3 );
		 /* .line 439 */
		 v2 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmLayoutParams ( v2,v1 );
		 /* .line 440 */
		 v2 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmFreeFormRecommendLayout ( v2,v1 );
		 /* .line 441 */
		 v2 = this.this$0;
		 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v2 ).setFreeFormRecommendState ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setFreeFormRecommendState(Z)V
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 446 */
		 /* .line 442 */
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 443 */
		 /* .local v2, "e":Ljava/lang/Exception; */
		 v3 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmLayoutParams ( v3,v1 );
		 /* .line 444 */
		 v3 = this.this$0;
		 com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmFreeFormRecommendLayout ( v3,v1 );
		 /* .line 445 */
		 v1 = this.this$0;
		 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v1 ).setFreeFormRecommendState ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setFreeFormRecommendState(Z)V
		 /* .line 448 */
	 } // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
