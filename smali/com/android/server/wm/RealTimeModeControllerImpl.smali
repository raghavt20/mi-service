.class public Lcom/android/server/wm/RealTimeModeControllerImpl;
.super Ljava/lang/Object;
.source "RealTimeModeControllerImpl.java"

# interfaces
.implements Lcom/android/server/wm/RealTimeModeControllerStub;


# static fields
.field public static final DEBUG:Z

.field public static ENABLE_RT_GESTURE:Z = false

.field public static ENABLE_RT_MODE:Z = false

.field public static ENABLE_TEMP_LIMIT_ENABLE:Z = false

.field public static IGNORE_CLOUD_ENABLE:Z = false

.field private static final MSG_REGISTER_CLOUD_OBSERVER:I = 0x1

.field private static final PERF_SHIELDER_GESTURE_KEY:Ljava/lang/String; = "perf_shielder_GESTURE"

.field private static final PERF_SHIELDER_RTMODE_KEY:Ljava/lang/String; = "perf_shielder_RTMODE"

.field private static final PERF_SHIELDER_RTMODE_MOUDLE:Ljava/lang/String; = "perf_rtmode"

.field private static final RT_ENABLE_CLOUD:Ljava/lang/String; = "perf_rt_enable"

.field private static final RT_ENABLE_TEMPLIMIT_CLOUD:Ljava/lang/String; = "rt_enable_templimit"

.field private static final RT_GESTURE_ENABLE_CLOUD:Ljava/lang/String; = "perf_rt_gesture_enable"

.field public static final RT_GESTURE_WHITE_LIST:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final RT_GESTURE_WHITE_LIST_CLOUD:Ljava/lang/String; = "rt_gesture_white_list"

.field public static final RT_PKG_BLACK_LIST:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final RT_PKG_BLACK_LIST_CLOUD:Ljava/lang/String; = "rt_pkg_black_list"

.field public static final RT_PKG_WHITE_LIST:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final RT_PKG_WHITE_LIST_CLOUD:Ljava/lang/String; = "rt_pkg_white_list"

.field public static RT_TEMPLIMIT_BOTTOM:I = 0x0

.field private static final RT_TEMPLIMIT_BOTTOM_CLOUD:Ljava/lang/String; = "rt_templimit_bottom"

.field public static RT_TEMPLIMIT_CEILING:I = 0x0

.field private static final RT_TEMPLIMIT_CEILING_CLOUD:Ljava/lang/String; = "rt_templimit_ceiling"

.field public static final TAG:Ljava/lang/String; = "RTMode"

.field private static mContext:Landroid/content/Context;


# instance fields
.field private isInit:Z

.field private mBoostInternal:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

.field private mCurrentFocus:Lcom/android/server/wm/WindowState;

.field private mH:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private final mLock:Ljava/lang/Object;

.field private mPm:Landroid/content/pm/PackageManager;

.field private mService:Lcom/android/server/wm/WindowManagerService;

.field private final mUidToAppBitType:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$mregisterCloudObserver(Lcom/android/server/wm/RealTimeModeControllerImpl;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/RealTimeModeControllerImpl;->registerCloudObserver(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudControlParas(Lcom/android/server/wm/RealTimeModeControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->updateCloudControlParas()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmContext()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$smupdateGestureCloudControlParas()V
    .locals 0

    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->updateGestureCloudControlParas()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mContext:Landroid/content/Context;

    .line 49
    nop

    .line 50
    const-string v0, "persist.sys.debug_rtmode"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    .line 51
    nop

    .line 52
    const-string v0, "persist.sys.enable_rtmode"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    .line 53
    nop

    .line 54
    const-string v0, "persist.sys.enable_ignorecloud_rtmode"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->IGNORE_CLOUD_ENABLE:Z

    .line 55
    nop

    .line 56
    const-string v0, "persist.sys.enable_sched_gesture"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z

    .line 57
    nop

    .line 58
    const-string v0, "persist.sys.enable_templimit"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_TEMP_LIMIT_ENABLE:Z

    .line 59
    nop

    .line 60
    const-string v0, "persist.sys.rtmode_templimit_bottom"

    const/16 v1, 0x2a

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_BOTTOM:I

    .line 61
    nop

    .line 62
    const-string v0, "persist.sys.rtmode_templimit_ceiling"

    const/16 v1, 0x2d

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_CEILING:I

    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_PKG_WHITE_LIST:Ljava/util/HashSet;

    .line 77
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_PKG_BLACK_LIST:Ljava/util/HashSet;

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_GESTURE_WHITE_LIST:Ljava/util/HashSet;

    .line 90
    const-string v1, "com.miui.home"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 91
    const-string v1, "com.android.systemui"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 92
    const-string v1, "com.mi.android.globallauncher"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 93
    const-string v1, "com.android.provision"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 95
    const-string v1, "com.miui.miwallpaper.snowmountain"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 96
    const-string v1, "com.miui.miwallpaper.earth"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 97
    const-string v1, "com.miui.miwallpaper.geometry"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 98
    const-string v1, "com.miui.miwallpaper.saturn"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 99
    const-string v1, "com.miui.miwallpaper.mars"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 100
    const-string v1, "com.miui.fliphome"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 101
    const-string v1, "com.android.quicksearchbox"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 102
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mPm:Landroid/content/pm/PackageManager;

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mUidToAppBitType:Ljava/util/HashMap;

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mLock:Ljava/lang/Object;

    return-void
.end method

.method private boostTopApp(IJ)V
    .locals 10
    .param p1, "mode"    # I
    .param p2, "durationMs"    # J

    .line 393
    invoke-virtual {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getAppPackageName()Ljava/lang/String;

    move-result-object v6

    .line 394
    .local v6, "focusedPackage":Ljava/lang/String;
    if-nez v6, :cond_0

    .line 395
    const-string v0, "RTMode"

    const-string v1, "Error: package name null"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    return-void

    .line 398
    :cond_0
    invoke-virtual {p0, v6}, Lcom/android/server/wm/RealTimeModeControllerImpl;->couldBoostTopAppProcess(Ljava/lang/String;)Z

    move-result v7

    .line 399
    .local v7, "couldBoost":Z
    if-eqz v7, :cond_1

    .line 400
    invoke-virtual {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getCurrentFocusProcessPid()I

    move-result v8

    .line 401
    .local v8, "pid":I
    invoke-static {v8}, Lmiui/process/ProcessManager;->getRenderThreadTidByPid(I)I

    move-result v9

    .line 402
    .local v9, "renderThreadTid":I
    invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    move-result-object v0

    filled-new-array {v8, v9}, [I

    move-result-object v1

    move-wide v2, p2

    move-object v4, v6

    move v5, p1

    invoke-interface/range {v0 .. v5}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->beginSchedThreads([IJLjava/lang/String;I)V

    .line 404
    .end local v8    # "pid":I
    .end local v9    # "renderThreadTid":I
    goto :goto_0

    .line 406
    :cond_1
    invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    move-result-object v0

    invoke-interface {v0}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->stopCurrentSchedBoost()V

    .line 408
    :goto_0
    return-void
.end method

.method public static dump(Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5
    .param p0, "pw"    # Ljava/io/PrintWriter;
    .param p1, "args"    # [Ljava/lang/String;

    .line 459
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "persist.sys.debug_rtmode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 460
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "persist.sys.enable_rtmode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 461
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "persist.sys.enable_ignorecloud_rtmode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->IGNORE_CLOUD_ENABLE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 462
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "persist.sys.enable_sched_gesture: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "persist.sys.enable_templimit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_TEMP_LIMIT_ENABLE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 464
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rt_templimit_bottom: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_BOTTOM:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 465
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rt_templimit_ceiling: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_CEILING:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 466
    const-string v0, "RT_PKG_WHITE_LIST: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 467
    sget-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_PKG_WHITE_LIST:Ljava/util/HashSet;

    monitor-enter v0

    .line 468
    :try_start_0
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 469
    .local v2, "white":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 470
    .end local v2    # "white":Ljava/lang/String;
    goto :goto_0

    .line 471
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 472
    const-string v0, "RT_PKG_BLACK_LIST: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 473
    sget-object v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_PKG_BLACK_LIST:Ljava/util/HashSet;

    monitor-enter v1

    .line 474
    :try_start_1
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 475
    .local v2, "black":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 476
    .end local v2    # "black":Ljava/lang/String;
    goto :goto_1

    .line 477
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 478
    const-string v0, "RT_GESTURE_WHITE_LIST: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 479
    sget-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_GESTURE_WHITE_LIST:Ljava/util/HashSet;

    monitor-enter v0

    .line 480
    :try_start_2
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 481
    .local v2, "gest_white":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 482
    .end local v2    # "gest_white":Ljava/lang/String;
    goto :goto_2

    .line 483
    :cond_2
    monitor-exit v0

    .line 484
    return-void

    .line 483
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 477
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 471
    :catchall_2
    move-exception v1

    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1
.end method

.method public static get()Lcom/android/server/wm/RealTimeModeControllerImpl;
    .locals 1

    .line 86
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerStub;->get()Lcom/android/server/wm/RealTimeModeControllerStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/RealTimeModeControllerImpl;

    return-object v0
.end method

.method private getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal;
    .locals 1

    .line 432
    iget-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mBoostInternal:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    if-nez v0, :cond_0

    .line 433
    const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mBoostInternal:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mBoostInternal:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    return-object v0
.end method

.method private isCurrentApp32Bit()Z
    .locals 6

    .line 413
    iget-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 414
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 415
    monitor-exit v0

    return v2

    .line 417
    :cond_0
    iget v1, v1, Lcom/android/server/wm/WindowState;->mOwnerUid:I

    .line 418
    .local v1, "currentUid":I
    iget-object v3, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget-object v3, v3, Lcom/android/server/wm/Session;->mPackageName:Ljava/lang/String;

    .line 419
    .local v3, "packageName":Ljava/lang/String;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421
    const/16 v0, 0x2710

    if-ge v1, v0, :cond_1

    .line 422
    return v2

    .line 423
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mUidToAppBitType:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 424
    sget-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, v3}, Landroid/os/MiuiProcess;->is32BitApp(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 425
    .local v0, "is32Bit":Z
    iget-object v2, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mUidToAppBitType:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    .end local v0    # "is32Bit":Z
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mUidToAppBitType:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 419
    .end local v1    # "currentUid":I
    .end local v3    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static isHomeProcess(Lcom/android/server/wm/WindowProcessController;)Z
    .locals 1
    .param p0, "wpc"    # Lcom/android/server/wm/WindowProcessController;

    .line 344
    invoke-virtual {p0}, Lcom/android/server/wm/WindowProcessController;->isHomeProcess()Z

    move-result v0

    return v0
.end method

.method public static isSystemUIProcess(Lcom/android/server/wm/WindowProcessController;)Z
    .locals 2
    .param p0, "wpc"    # Lcom/android/server/wm/WindowProcessController;

    .line 348
    iget-object v0, p0, Lcom/android/server/wm/WindowProcessController;->mName:Ljava/lang/String;

    const-string v1, "com.android.systemui"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private registerCloudObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 147
    new-instance v0, Lcom/android/server/wm/RealTimeModeControllerImpl$2;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/RealTimeModeControllerImpl$2;-><init>(Lcom/android/server/wm/RealTimeModeControllerImpl;Landroid/os/Handler;)V

    .line 158
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 159
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v2

    .line 158
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 160
    return-void
.end method

.method private updateCloudControlParas()V
    .locals 11

    .line 195
    const-string v0, "RTMode"

    sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->IGNORE_CLOUD_ENABLE:Z

    if-nez v1, :cond_7

    .line 196
    sget-object v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->mContext:Landroid/content/Context;

    .line 197
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "perf_shielder_RTMODE"

    const-string v3, ""

    const-string v4, "perf_rtmode"

    invoke-static {v1, v4, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199
    .local v1, "data":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 200
    return-void

    .line 203
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 204
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "perf_rt_enable"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 205
    .local v3, "rtModeEnable":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 206
    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    sput-boolean v4, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    .line 207
    sget-boolean v4, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 208
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RTMode enable cloud control set received : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_1
    const-string v4, "rt_pkg_white_list"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 214
    .local v4, "rtWhiteList":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v6, ","

    if-nez v5, :cond_2

    .line 215
    :try_start_1
    sget-object v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_PKG_WHITE_LIST:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->clear()V

    .line 216
    sget-object v7, Lcom/android/server/wm/RealTimeModeControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 217
    .local v7, "r":Landroid/content/res/Resources;
    nop

    .line 218
    const v8, 0x110300be

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    .line 219
    .local v8, "whiteList":[Ljava/lang/String;
    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 220
    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 221
    sget-boolean v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 222
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RTMode rtWhiteList cloud control set received : "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    .end local v7    # "r":Landroid/content/res/Resources;
    .end local v8    # "whiteList":[Ljava/lang/String;
    :cond_2
    const-string v5, "rt_pkg_black_list"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 228
    .local v5, "rtBlackList":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 229
    sget-object v7, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_PKG_BLACK_LIST:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->clear()V

    .line 230
    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 231
    sget-boolean v6, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v6, :cond_3

    .line 232
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RTMode rtBlackList cloud control set received : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    :cond_3
    const-string v6, "rt_templimit_bottom"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 238
    .local v6, "rtTempLimitBottom":Ljava/lang/String;
    if-eqz v6, :cond_4

    .line 239
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    sput v7, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_BOTTOM:I

    .line 240
    sget-boolean v7, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v7, :cond_4

    .line 241
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "RTMode Temp Limit bottom cloud control set received : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_BOTTOM:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_4
    const-string v7, "rt_templimit_ceiling"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 247
    .local v7, "rtTempLimitCeiling":Ljava/lang/String;
    if-eqz v7, :cond_5

    .line 248
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_CEILING:I

    .line 249
    sget-boolean v8, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v8, :cond_5

    .line 250
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RTMode Temp Limit ceiling cloud control set received : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget v9, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_TEMPLIMIT_CEILING:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_5
    const-string v8, "rt_enable_templimit"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 256
    .local v8, "rtEnableTempLimit":Ljava/lang/String;
    if-eqz v8, :cond_6

    .line 257
    invoke-static {v8}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v9

    sput-boolean v9, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_TEMP_LIMIT_ENABLE:Z

    .line 258
    sget-boolean v9, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v9, :cond_6

    .line 259
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "RTMode Temp Limit enable cloud control set received : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-boolean v10, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_TEMP_LIMIT_ENABLE:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 265
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "rtModeEnable":Ljava/lang/String;
    .end local v4    # "rtWhiteList":Ljava/lang/String;
    .end local v5    # "rtBlackList":Ljava/lang/String;
    .end local v6    # "rtTempLimitBottom":Ljava/lang/String;
    .end local v7    # "rtTempLimitCeiling":Ljava/lang/String;
    .end local v8    # "rtEnableTempLimit":Ljava/lang/String;
    :cond_6
    goto :goto_0

    .line 263
    :catch_0
    move-exception v2

    .line 264
    .local v2, "e":Lorg/json/JSONException;
    const-string/jumbo v3, "updateCloudData error :"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 267
    .end local v1    # "data":Ljava/lang/String;
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_7
    :goto_0
    return-void
.end method

.method private static updateGestureCloudControlParas()V
    .locals 7

    .line 163
    const-string v0, "RTMode"

    sget-boolean v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->IGNORE_CLOUD_ENABLE:Z

    if-nez v1, :cond_3

    .line 164
    sget-object v1, Lcom/android/server/wm/RealTimeModeControllerImpl;->mContext:Landroid/content/Context;

    .line 165
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "perf_shielder_GESTURE"

    const-string v3, ""

    const-string v4, "perf_rtmode"

    invoke-static {v1, v4, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 167
    .local v1, "data":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 168
    return-void

    .line 171
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 172
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "perf_rt_gesture_enable"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 173
    .local v3, "rtGestureEnable":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 174
    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    sput-boolean v4, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z

    .line 175
    sget-boolean v4, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 176
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rtGestureEnable cloud control set received : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :cond_1
    const-string v4, "rt_gesture_white_list"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 182
    .local v4, "rtGestureList":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 183
    sget-object v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_GESTURE_WHITE_LIST:Ljava/util/HashSet;

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 184
    sget-boolean v5, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 185
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rtGestureList cloud control set received : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "rtGestureEnable":Ljava/lang/String;
    .end local v4    # "rtGestureList":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 188
    :catch_0
    move-exception v2

    .line 189
    .local v2, "e":Lorg/json/JSONException;
    const-string/jumbo v3, "updateCloudData error :"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 192
    .end local v1    # "data":Ljava/lang/String;
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public checkCallerPermission(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 352
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z

    const-string v1, "RTMode"

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 360
    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_GESTURE_WHITE_LIST:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 361
    const/4 v0, 0x1

    return v0

    .line 363
    :cond_1
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "pkgName is null or has no permission"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_2
    return v2

    .line 353
    :cond_3
    :goto_0
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_4

    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ENABLE_RT_MODE : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v3, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "ENABLE_RT_GESTURE : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v3, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    :cond_4
    return v2
.end method

.method public checkThreadBoost(I)Z
    .locals 2
    .param p1, "tid"    # I

    .line 439
    iget-boolean v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 440
    return v1

    .line 442
    :cond_0
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    if-nez v0, :cond_1

    goto :goto_0

    .line 445
    :cond_1
    invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->checkThreadBoost(I)Z

    move-result v0

    return v0

    .line 443
    :cond_2
    :goto_0
    return v1
.end method

.method public couldBoostTopAppProcess(Ljava/lang/String;)Z
    .locals 4
    .param p1, "currentPackage"    # Ljava/lang/String;

    .line 270
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    const-string v1, "RTMode"

    const/4 v2, 0x0

    if-nez v0, :cond_1

    .line 271
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ENABLE_RT_MODE : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v3, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_0
    return v2

    .line 274
    :cond_1
    sget-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_PKG_WHITE_LIST:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_PKG_BLACK_LIST:Ljava/util/HashSet;

    .line 275
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 279
    :cond_2
    invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->isCurrentApp32Bit()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 280
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "is a 32bit app, skip boost!"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_3
    return v2

    .line 284
    :cond_4
    const/4 v0, 0x1

    return v0

    .line 276
    :cond_5
    :goto_0
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "is not in whitelist or in blacklist!"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_6
    return v2
.end method

.method public getAppPackageName()Ljava/lang/String;
    .locals 4

    .line 289
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mRoot:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getTopFocusedDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    .line 290
    .local v1, "dc":Lcom/android/server/wm/DisplayContent;
    if-eqz v1, :cond_1

    .line 291
    iget-object v2, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :try_start_1
    iget-object v3, v1, Lcom/android/server/wm/DisplayContent;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    iput-object v3, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    .line 293
    if-eqz v3, :cond_0

    .line 294
    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget-object v3, v3, Lcom/android/server/wm/Session;->mPackageName:Ljava/lang/String;

    monitor-exit v2

    return-object v3

    .line 296
    :cond_0
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/wm/RealTimeModeControllerImpl;
    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 298
    .restart local p0    # "this":Lcom/android/server/wm/RealTimeModeControllerImpl;
    :cond_1
    :goto_0
    return-object v0

    .line 299
    .end local v1    # "dc":Lcom/android/server/wm/DisplayContent;
    :catch_0
    move-exception v1

    .line 300
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "RTMode"

    const-string v3, "failed to getAppPackageName"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 302
    .end local v1    # "e":Ljava/lang/Exception;
    return-object v0
.end method

.method public getCurrentFocusProcessPid()I
    .locals 3

    .line 333
    const/4 v0, -0x1

    .line 334
    .local v0, "pid":I
    iget-object v1, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 335
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    if-nez v2, :cond_0

    .line 336
    monitor-exit v1

    return v0

    .line 338
    :cond_0
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v2, v2, Lcom/android/server/wm/Session;->mPid:I

    move v0, v2

    .line 339
    monitor-exit v1

    .line 340
    return v0

    .line 339
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getWindowProcessController()Lcom/android/server/wm/WindowProcessController;
    .locals 6

    .line 309
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    :try_start_1
    iget-object v2, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    if-nez v2, :cond_0

    .line 311
    monitor-exit v1

    return-object v0

    .line 313
    :cond_0
    iget-object v2, v2, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 314
    .local v2, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    iget-object v3, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    iget-object v3, v3, Lcom/android/server/wm/WindowState;->mSession:Lcom/android/server/wm/Session;

    iget v3, v3, Lcom/android/server/wm/Session;->mPid:I

    .line 315
    .local v3, "pid":I
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    :try_start_2
    iget-object v1, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mHomeProcess:Lcom/android/server/wm/WindowProcessController;

    .line 317
    .local v1, "mHomeProcess":Lcom/android/server/wm/WindowProcessController;
    if-nez v2, :cond_1

    .line 318
    iget-object v4, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mProcessMap:Lcom/android/server/wm/WindowProcessControllerMap;

    invoke-virtual {v4, v3}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v0

    return-object v0

    .line 319
    :cond_1
    if-eqz v1, :cond_2

    iget-object v4, v1, Lcom/android/server/wm/WindowProcessController;->mName:Ljava/lang/String;

    iget-object v5, v2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    if-ne v4, v5, :cond_2

    .line 321
    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v4

    if-eq v4, v3, :cond_2

    .line 322
    iget-object v4, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mProcessMap:Lcom/android/server/wm/WindowProcessControllerMap;

    invoke-virtual {v4, v3}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v0

    return-object v0

    .line 324
    :cond_2
    iget-object v0, v2, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-object v0

    .line 315
    .end local v1    # "mHomeProcess":Lcom/android/server/wm/WindowProcessController;
    .end local v2    # "activityRecord":Lcom/android/server/wm/ActivityRecord;
    .end local v3    # "pid":I
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local p0    # "this":Lcom/android/server/wm/RealTimeModeControllerImpl;
    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 326
    .restart local p0    # "this":Lcom/android/server/wm/RealTimeModeControllerImpl;
    :catch_0
    move-exception v1

    .line 327
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "RTMode"

    const-string v3, "failed to getWindowProcessController"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 329
    .end local v1    # "e":Ljava/lang/Exception;
    return-object v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 105
    iget-boolean v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z

    if-eqz v0, :cond_0

    .line 106
    return-void

    .line 108
    :cond_0
    sput-object p1, Lcom/android/server/wm/RealTimeModeControllerImpl;->mContext:Landroid/content/Context;

    .line 111
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 112
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x110300be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "whiteList":[Ljava/lang/String;
    sget-object v2, Lcom/android/server/wm/RealTimeModeControllerImpl;->RT_PKG_WHITE_LIST:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 115
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "RealTimeModeControllerTh"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 116
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 117
    iget-object v2, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 118
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v2

    .line 117
    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 120
    new-instance v2, Lcom/android/server/wm/RealTimeModeControllerImpl$1;

    iget-object v3, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/wm/RealTimeModeControllerImpl$1;-><init>(Lcom/android/server/wm/RealTimeModeControllerImpl;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mH:Landroid/os/Handler;

    .line 135
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z

    .line 136
    const-string v2, "RTMode"

    const-string v3, "init RealTimeModeController"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    return-void
.end method

.method public onBootPhase()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mH:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 141
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 142
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mH:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 144
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public onDown()V
    .locals 3

    .line 383
    invoke-static {}, Lcom/android/server/am/MiuiProcessStub;->getInstance()Lcom/android/server/am/MiuiProcessStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/am/MiuiProcessStub;->getSchedModeNormal()I

    move-result v0

    .line 384
    invoke-static {}, Lcom/android/server/am/MiuiProcessStub;->getInstance()Lcom/android/server/am/MiuiProcessStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/am/MiuiProcessStub;->getTouchRtSchedDurationMs()J

    move-result-wide v1

    .line 383
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->boostTopApp(IJ)V

    .line 385
    return-void
.end method

.method public onFling(I)V
    .locals 0
    .param p1, "durationMs"    # I

    .line 374
    return-void
.end method

.method public onMove()V
    .locals 3

    .line 388
    invoke-static {}, Lcom/android/server/am/MiuiProcessStub;->getInstance()Lcom/android/server/am/MiuiProcessStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/am/MiuiProcessStub;->getSchedModeNormal()I

    move-result v0

    .line 389
    invoke-static {}, Lcom/android/server/am/MiuiProcessStub;->getInstance()Lcom/android/server/am/MiuiProcessStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/am/MiuiProcessStub;->getTouchRtSchedDurationMs()J

    move-result-wide v1

    .line 388
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->boostTopApp(IJ)V

    .line 390
    return-void
.end method

.method public onScroll(Z)V
    .locals 3
    .param p1, "started"    # Z

    .line 377
    if-nez p1, :cond_0

    return-void

    .line 378
    :cond_0
    invoke-static {}, Lcom/android/server/am/MiuiProcessStub;->getInstance()Lcom/android/server/am/MiuiProcessStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/am/MiuiProcessStub;->getSchedModeNormal()I

    move-result v0

    .line 379
    invoke-static {}, Lcom/android/server/am/MiuiProcessStub;->getInstance()Lcom/android/server/am/MiuiProcessStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/am/MiuiProcessStub;->getScrollRtSchedDurationMs()J

    move-result-wide v1

    .line 378
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->boostTopApp(IJ)V

    .line 380
    return-void
.end method

.method public setThreadSavedPriority([II)V
    .locals 1
    .param p1, "tid"    # [I
    .param p2, "prio"    # I

    .line 449
    iget-boolean v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->isInit:Z

    if-nez v0, :cond_0

    .line 450
    return-void

    .line 452
    :cond_0
    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_GESTURE:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/server/wm/RealTimeModeControllerImpl;->ENABLE_RT_MODE:Z

    if-nez v0, :cond_1

    goto :goto_0

    .line 455
    :cond_1
    invoke-direct {p0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->setThreadSavedPriority([II)V

    .line 456
    return-void

    .line 453
    :cond_2
    :goto_0
    return-void
.end method

.method public setWindowManager(Lcom/android/server/wm/WindowManagerService;)V
    .locals 0
    .param p1, "mService"    # Lcom/android/server/wm/WindowManagerService;

    .line 369
    iput-object p1, p0, Lcom/android/server/wm/RealTimeModeControllerImpl;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 370
    return-void
.end method
