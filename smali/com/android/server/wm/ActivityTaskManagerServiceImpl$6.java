class com.android.server.wm.ActivityTaskManagerServiceImpl$6 extends android.database.ContentObserver {
	 /* .source "ActivityTaskManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->registerSubScreenSwitchObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.ActivityTaskManagerServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.ActivityTaskManagerServiceImpl$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 989 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .param p3, "flags" # I */
/* .line 992 */
/* invoke-super {p0, p1, p2, p3}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;I)V */
/* .line 993 */
v0 = this.this$0;
/* const-string/jumbo v1, "subscreen feature switch on" */
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).restartSubScreenUiIfNeeded ( p3, v1 ); // invoke-virtual {v0, p3, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(ILjava/lang/String;)V
/* .line 994 */
return;
} // .end method
