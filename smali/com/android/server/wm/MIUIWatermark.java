class com.android.server.wm.MIUIWatermark implements com.android.server.wm.MIUIWatermarkStub {
	 /* .source "MIUIWatermark.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final java.lang.String TAG;
	 private static final Integer degree;
	 private static volatile java.lang.String mIMEI;
	 private static Integer mTextSize;
	 private static final Integer textSize;
	 /* # instance fields */
	 private java.lang.String mAccountName;
	 private android.graphics.BLASTBufferQueue mBlastBufferQueue;
	 private com.android.server.policy.MiuiPhoneWindowManager$MIUIWatermarkCallback mCallback;
	 private Boolean mDrawNeeded;
	 private volatile Boolean mEnableMIUIWatermark;
	 private Boolean mImeiThreadisRuning;
	 private Integer mLastDH;
	 private Integer mLastDW;
	 private android.view.Surface mSurface;
	 private android.view.SurfaceControl mSurfaceControl;
	 private final android.graphics.Paint mTextPaint;
	 private android.view.SurfaceControl$Transaction mTransaction;
	 private com.android.server.wm.WindowManagerService mWmService;
	 /* # direct methods */
	 static android.view.SurfaceControl -$$Nest$fgetmSurfaceControl ( com.android.server.wm.MIUIWatermark p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mSurfaceControl;
	 } // .end method
	 static void -$$Nest$fputmEnableMIUIWatermark ( com.android.server.wm.MIUIWatermark p0, Boolean p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iput-boolean p1, p0, Lcom/android/server/wm/MIUIWatermark;->mEnableMIUIWatermark:Z */
		 return;
	 } // .end method
	 static void -$$Nest$fputmImeiThreadisRuning ( com.android.server.wm.MIUIWatermark p0, Boolean p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iput-boolean p1, p0, Lcom/android/server/wm/MIUIWatermark;->mImeiThreadisRuning:Z */
		 return;
	 } // .end method
	 static void -$$Nest$mcreateSurfaceLocked ( com.android.server.wm.MIUIWatermark p0, java.lang.String p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->createSurfaceLocked(Ljava/lang/String;)V */
		 return;
	 } // .end method
	 static void -$$Nest$mhideWaterMarker ( com.android.server.wm.MIUIWatermark p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/MIUIWatermark;->hideWaterMarker()V */
		 return;
	 } // .end method
	 static void -$$Nest$msetImei ( com.android.server.wm.MIUIWatermark p0, java.lang.String p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->setImei(Ljava/lang/String;)V */
		 return;
	 } // .end method
	 static void -$$Nest$mshowWaterMarker ( com.android.server.wm.MIUIWatermark p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/MIUIWatermark;->showWaterMarker()V */
		 return;
	 } // .end method
	 static void -$$Nest$mupdateText ( com.android.server.wm.MIUIWatermark p0, android.content.Context p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->updateText(Landroid/content/Context;)V */
		 return;
	 } // .end method
	 static java.lang.String -$$Nest$smgetImeiID ( android.content.Context p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 com.android.server.wm.MIUIWatermark .getImeiID ( p0 );
	 } // .end method
	 static com.android.server.wm.MIUIWatermark ( ) {
		 /* .locals 1 */
		 /* .line 49 */
		 int v0 = 0; // const/4 v0, 0x0
		 return;
	 } // .end method
	 com.android.server.wm.MIUIWatermark ( ) {
		 /* .locals 2 */
		 /* .line 39 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 45 */
		 /* new-instance v0, Landroid/graphics/Paint; */
		 int v1 = 1; // const/4 v1, 0x1
		 /* invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V */
		 this.mTextPaint = v0;
		 /* .line 56 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDW:I */
		 /* .line 57 */
		 /* iput v0, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDH:I */
		 /* .line 59 */
		 /* iput-boolean v0, p0, Lcom/android/server/wm/MIUIWatermark;->mImeiThreadisRuning:Z */
		 return;
	 } // .end method
	 private void createSurfaceLocked ( java.lang.String p0 ) {
		 /* .locals 4 */
		 /* .param p1, "msg" # Ljava/lang/String; */
		 /* .line 327 */
		 final String v0 = "createWatermarkInTransaction"; // const-string v0, "createWatermarkInTransaction"
		 /* iget-boolean v1, p0, Lcom/android/server/wm/MIUIWatermark;->mEnableMIUIWatermark:Z */
		 /* if-nez v1, :cond_0 */
		 /* .line 328 */
		 return;
		 /* .line 330 */
	 } // :cond_0
	 v1 = this.mSurfaceControl;
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 return;
		 /* .line 332 */
	 } // :cond_1
	 v1 = this.mWmService;
	 v1 = this.mContext;
	 /* const/high16 v2, 0x41500000 # 13.0f */
	 v1 = 	 com.android.server.wm.MIUIWatermark .dp2px ( v1,v2 );
	 /* .line 333 */
	 v1 = this.mWmService;
	 (( com.android.server.wm.WindowManagerService ) v1 ).openSurfaceTransaction ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
	 /* .line 335 */
	 try { // :try_start_0
		 final String v1 = "Watermark"; // const-string v1, "Watermark"
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "create water mark: "; // const-string v3, "create water mark: "
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v1,v2 );
		 /* .line 336 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/MIUIWatermark;->doCreateSurfaceLocked()V */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 338 */
		 v1 = this.mWmService;
		 (( com.android.server.wm.WindowManagerService ) v1 ).closeSurfaceTransaction ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
		 /* .line 339 */
		 /* nop */
		 /* .line 340 */
		 return;
		 /* .line 338 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 v2 = this.mWmService;
		 (( com.android.server.wm.WindowManagerService ) v2 ).closeSurfaceTransaction ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
		 /* .line 339 */
		 /* throw v1 */
	 } // .end method
	 private void doCreateSurfaceLocked ( ) {
		 /* .locals 10 */
		 /* .line 65 */
		 final String v0 = "MIUIWatermarkSurface"; // const-string v0, "MIUIWatermarkSurface"
		 v1 = this.mWmService;
		 v1 = this.mContext;
		 com.android.server.wm.MIUIWatermark .loadAccountId ( v1 );
		 /* .line 66 */
		 /* .local v1, "account":Landroid/accounts/Account; */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 v2 = this.name;
		 } // :cond_0
		 int v2 = 0; // const/4 v2, 0x0
	 } // :goto_0
	 this.mAccountName = v2;
	 /* .line 67 */
	 v2 = this.mWmService;
	 v2 = this.mContext;
	 /* invoke-direct {p0, v2}, Lcom/android/server/wm/MIUIWatermark;->getImei(Landroid/content/Context;)Ljava/lang/String; */
	 /* .line 72 */
	 v2 = this.mTextPaint;
	 /* int-to-float v3, v3 */
	 (( android.graphics.Paint ) v2 ).setTextSize ( v3 ); // invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V
	 /* .line 73 */
	 v2 = this.mTextPaint;
	 v3 = android.graphics.Typeface.SANS_SERIF;
	 int v4 = 0; // const/4 v4, 0x0
	 android.graphics.Typeface .create ( v3,v4 );
	 (( android.graphics.Paint ) v2 ).setTypeface ( v3 ); // invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
	 /* .line 74 */
	 v2 = this.mTextPaint;
	 /* const v3, 0x50ffffff */
	 (( android.graphics.Paint ) v2 ).setColor ( v3 ); // invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V
	 /* .line 75 */
	 v2 = this.mTextPaint;
	 /* const/high16 v3, 0x3f800000 # 1.0f */
	 /* const/high16 v4, 0x50000000 */
	 /* const/high16 v5, 0x40000000 # 2.0f */
	 (( android.graphics.Paint ) v2 ).setShadowLayer ( v3, v5, v5, v4 ); // invoke-virtual {v2, v3, v5, v5, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V
	 /* .line 77 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 81 */
	 /* .local v2, "ctrl":Landroid/view/SurfaceControl; */
	 try { // :try_start_0
		 v3 = this.mWmService;
		 (( com.android.server.wm.WindowManagerService ) v3 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
		 /* .line 82 */
		 /* .local v3, "dc":Lcom/android/server/wm/DisplayContent; */
		 (( com.android.server.wm.DisplayContent ) v3 ).makeOverlay ( ); // invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;
		 /* .line 83 */
		 (( android.view.SurfaceControl$Builder ) v4 ).setName ( v0 ); // invoke-virtual {v4, v0}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
		 /* .line 84 */
		 (( android.view.SurfaceControl$Builder ) v4 ).setBLASTLayer ( ); // invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;
		 /* .line 85 */
		 int v5 = -3; // const/4 v5, -0x3
		 (( android.view.SurfaceControl$Builder ) v4 ).setFormat ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setFormat(I)Landroid/view/SurfaceControl$Builder;
		 /* .line 86 */
		 (( android.view.SurfaceControl$Builder ) v4 ).setCallsite ( v0 ); // invoke-virtual {v4, v0}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
		 /* .line 87 */
		 (( android.view.SurfaceControl$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
		 /* move-object v2, v0 */
		 /* .line 90 */
		 v0 = this.mTransaction;
		 /* const v4, 0xf4240 */
		 (( android.view.SurfaceControl$Transaction ) v0 ).setLayer ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
		 /* .line 91 */
		 v0 = this.mTransaction;
		 int v4 = 0; // const/4 v4, 0x0
		 (( android.view.SurfaceControl$Transaction ) v0 ).setPosition ( v2, v4, v4 ); // invoke-virtual {v0, v2, v4, v4}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;
		 /* .line 92 */
		 v0 = this.mTransaction;
		 (( android.view.SurfaceControl$Transaction ) v0 ).show ( v2 ); // invoke-virtual {v0, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
		 /* .line 93 */
		 v0 = this.mTransaction;
		 v4 = 		 (( com.android.server.wm.DisplayContent ) v3 ).getDisplayId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
		 final String v5 = "MIUIWatermark"; // const-string v5, "MIUIWatermark"
		 com.android.server.wm.InputMonitor .setTrustedOverlayInputInfo ( v2,v0,v4,v5 );
		 /* .line 94 */
		 v0 = this.mTransaction;
		 (( android.view.SurfaceControl$Transaction ) v0 ).apply ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
		 /* .line 95 */
		 this.mSurfaceControl = v2;
		 /* .line 96 */
		 /* new-instance v0, Landroid/graphics/BLASTBufferQueue; */
		 final String v5 = "MIUIWatermarkSurface"; // const-string v5, "MIUIWatermarkSurface"
		 v6 = this.mSurfaceControl;
		 int v7 = 1; // const/4 v7, 0x1
		 int v8 = 1; // const/4 v8, 0x1
		 int v9 = 1; // const/4 v9, 0x1
		 /* move-object v4, v0 */
		 /* invoke-direct/range {v4 ..v9}, Landroid/graphics/BLASTBufferQueue;-><init>(Ljava/lang/String;Landroid/view/SurfaceControl;III)V */
		 this.mBlastBufferQueue = v0;
		 /* .line 98 */
		 (( android.graphics.BLASTBufferQueue ) v0 ).createSurface ( ); // invoke-virtual {v0}, Landroid/graphics/BLASTBufferQueue;->createSurface()Landroid/view/Surface;
		 this.mSurface = v0;
		 /* :try_end_0 */
		 /* .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 101 */
	 } // .end local v3 # "dc":Lcom/android/server/wm/DisplayContent;
	 /* .line 99 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 100 */
	 /* .local v0, "e":Landroid/view/Surface$OutOfResourcesException; */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "createrSurface e"; // const-string v4, "createrSurface e"
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v4 = "Watermark"; // const-string v4, "Watermark"
	 android.util.Log .d ( v4,v3 );
	 /* .line 103 */
} // .end local v0 # "e":Landroid/view/Surface$OutOfResourcesException;
} // :goto_1
return;
} // .end method
private static Integer dp2px ( android.content.Context p0, Float p1 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "dpValue" # F */
/* .line 343 */
(( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* iget v0, v0, Landroid/util/DisplayMetrics;->density:F */
/* .line 344 */
/* .local v0, "scale":F */
/* mul-float v1, p1, v0 */
/* const/high16 v2, 0x3f000000 # 0.5f */
/* add-float/2addr v1, v2 */
/* float-to-int v1, v1 */
} // .end method
private java.lang.String getImei ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 288 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->getImeiInfo(Landroid/content/Context;)V */
/* .line 289 */
v0 = com.android.server.wm.MIUIWatermark.mIMEI;
} // .end method
private static java.lang.String getImeiID ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 272 */
final String v0 = "phone"; // const-string v0, "phone"
(( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/telephony/TelephonyManager; */
/* .line 273 */
/* .local v0, "telephonyMgr":Landroid/telephony/TelephonyManager; */
/* if-nez v0, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
(( android.telephony.TelephonyManager ) v0 ).getImei ( v1 ); // invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getImei(I)Ljava/lang/String;
/* .line 274 */
/* .local v1, "imei":Ljava/lang/String; */
} // :goto_0
/* if-nez v1, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 275 */
(( android.telephony.TelephonyManager ) v0 ).getDeviceId ( ); // invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;
/* .line 277 */
} // :cond_1
v2 = miui.view.MiuiSecurityPermissionHandler .noImei ( );
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 278 */
miui.view.MiuiSecurityPermissionHandler .getWifiMacAddress ( p0 );
/* .line 280 */
} // :cond_2
} // .end method
private void getImeiInfo ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 233 */
com.android.server.wm.MIUIWatermark .getImeiID ( p1 );
/* .line 234 */
/* .local v0, "imei":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 235 */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MIUIWatermark;->setImei(Ljava/lang/String;)V */
/* .line 236 */
return;
/* .line 238 */
} // :cond_0
/* monitor-enter p0 */
/* .line 239 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/wm/MIUIWatermark;->mImeiThreadisRuning:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 240 */
/* monitor-exit p0 */
return;
/* .line 242 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/wm/MIUIWatermark;->mImeiThreadisRuning:Z */
/* .line 243 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 244 */
/* new-instance v1, Lcom/android/server/wm/MIUIWatermark$1; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MIUIWatermark$1;-><init>(Lcom/android/server/wm/MIUIWatermark;Landroid/content/Context;)V */
/* .line 268 */
(( com.android.server.wm.MIUIWatermark$1 ) v1 ).start ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MIUIWatermark$1;->start()V
/* .line 269 */
return;
/* .line 243 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit p0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void hideWaterMarker ( ) {
/* .locals 2 */
/* .line 122 */
v0 = this.mSurfaceControl;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 123 */
	 v1 = this.mTransaction;
	 (( android.view.SurfaceControl$Transaction ) v1 ).hide ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
	 /* .line 125 */
} // :cond_0
return;
} // .end method
private static android.accounts.Account loadAccountId ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 224 */
android.accounts.AccountManager .get ( p0 );
/* .line 225 */
final String v1 = "com.xiaomi"; // const-string v1, "com.xiaomi"
(( android.accounts.AccountManager ) v0 ).getAccountsByType ( v1 ); // invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;
/* .line 226 */
/* .local v0, "accounts":[Landroid/accounts/Account; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* array-length v1, v0 */
	 /* if-lez v1, :cond_0 */
	 /* .line 227 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* aget-object v1, v0, v1 */
	 /* .line 229 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private synchronized void setImei ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "imei" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 284 */
try { // :try_start_0
	 /* .line 285 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 286 */
	 /* monitor-exit p0 */
	 return;
	 /* .line 283 */
} // .end local p0 # "this":Lcom/android/server/wm/MIUIWatermark;
} // .end local p1 # "imei":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private void showWaterMarker ( ) {
/* .locals 2 */
/* .line 116 */
v0 = this.mSurfaceControl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 117 */
v1 = this.mTransaction;
(( android.view.SurfaceControl$Transaction ) v1 ).show ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 119 */
} // :cond_0
return;
} // .end method
private void updateText ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 128 */
com.android.server.wm.MIUIWatermark .loadAccountId ( p1 );
/* .line 129 */
/* .local v0, "account":Landroid/accounts/Account; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 130 */
v1 = this.name;
this.mAccountName = v1;
/* .line 133 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->getImei(Landroid/content/Context;)Ljava/lang/String; */
/* .line 134 */
/* .local v1, "imei":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 135 */
/* .line 138 */
} // :cond_1
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z */
/* .line 139 */
return;
} // .end method
/* # virtual methods */
public void drawIfNeeded ( ) {
/* .locals 24 */
/* .line 143 */
/* move-object/from16 v1, p0 */
final String v2 = "Watermark"; // const-string v2, "Watermark"
v0 = this.mSurfaceControl;
if ( v0 != null) { // if-eqz v0, :cond_b
/* iget-boolean v3, v1, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z */
/* if-nez v3, :cond_0 */
/* goto/16 :goto_6 */
/* .line 145 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* iput-boolean v3, v1, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z */
/* .line 146 */
/* iget v4, v1, Lcom/android/server/wm/MIUIWatermark;->mLastDW:I */
/* .line 147 */
/* .local v4, "dw":I */
/* iget v5, v1, Lcom/android/server/wm/MIUIWatermark;->mLastDH:I */
/* .line 149 */
/* .local v5, "dh":I */
v6 = this.mBlastBufferQueue;
int v7 = 1; // const/4 v7, 0x1
(( android.graphics.BLASTBufferQueue ) v6 ).update ( v0, v4, v5, v7 ); // invoke-virtual {v6, v0, v4, v5, v7}, Landroid/graphics/BLASTBufferQueue;->update(Landroid/view/SurfaceControl;III)V
/* .line 151 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0, v3, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V */
/* move-object v6, v0 */
/* .line 152 */
/* .local v6, "dirty":Landroid/graphics/Rect; */
int v7 = 0; // const/4 v7, 0x0
/* .line 154 */
/* .local v7, "c":Landroid/graphics/Canvas; */
try { // :try_start_0
v0 = this.mSurface;
(( android.view.Surface ) v0 ).lockCanvas ( v6 ); // invoke-virtual {v0, v6}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v7, v0 */
/* .line 157 */
/* .line 155 */
/* :catch_0 */
/* move-exception v0 */
/* .line 156 */
/* .local v0, "e":Ljava/lang/RuntimeException; */
final String v8 = "Failed to lock canvas"; // const-string v8, "Failed to lock canvas"
android.util.Slog .w ( v2,v8,v0 );
/* .line 159 */
} // .end local v0 # "e":Ljava/lang/RuntimeException;
} // :goto_0
/* if-nez v7, :cond_1 */
/* .line 160 */
return;
/* .line 164 */
} // :cond_1
v0 = android.graphics.PorterDuff$Mode.CLEAR;
(( android.graphics.Canvas ) v7 ).drawColor ( v3, v0 ); // invoke-virtual {v7, v3, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V
/* .line 166 */
/* const-wide/high16 v8, 0x403e000000000000L # 30.0 */
java.lang.Math .toRadians ( v8,v9 );
/* move-result-wide v8 */
/* .line 167 */
/* .local v8, "radians":D */
/* const-wide v10, 0x3fe3333333333333L # 0.6 */
/* int-to-double v12, v4 */
/* mul-double/2addr v12, v10 */
/* double-to-int v0, v12 */
/* .line 168 */
/* .local v0, "x":I */
/* const-wide v10, 0x3fa999999999999aL # 0.05 */
/* int-to-double v12, v5 */
/* mul-double/2addr v12, v10 */
/* double-to-int v3, v12 */
/* .line 169 */
/* .local v3, "y":I */
/* if-le v4, v5, :cond_2 */
/* .line 170 */
/* const-wide v10, 0x3fd3333333333333L # 0.3 */
/* int-to-double v12, v4 */
/* mul-double/2addr v12, v10 */
/* double-to-int v0, v12 */
/* .line 172 */
} // :cond_2
/* int-to-double v10, v10 */
java.lang.Math .tan ( v8,v9 );
/* move-result-wide v12 */
/* mul-double/2addr v10, v12 */
/* double-to-int v10, v10 */
/* .line 173 */
/* .local v10, "incDeltx":I */
/* mul-int/lit8 v11, v11, -0x1 */
/* .line 174 */
/* .local v11, "incDelty":I */
/* div-int/lit8 v12, v4, 0x2 */
/* .line 175 */
/* .local v12, "columnDeltX":I */
/* const-wide v13, 0x3fc1cac083126e98L # 0.139 */
/* move-wide v15, v8 */
} // .end local v8 # "radians":D
/* .local v15, "radians":D */
/* int-to-double v8, v5 */
/* mul-double/2addr v8, v13 */
/* double-to-int v8, v8 */
/* .line 176 */
/* .local v8, "columnDeltY":I */
/* const-wide v13, 0x3fceb851eb851eb8L # 0.24 */
/* move/from16 v17, v8 */
} // .end local v8 # "columnDeltY":I
/* .local v17, "columnDeltY":I */
/* int-to-double v8, v5 */
/* mul-double/2addr v8, v13 */
/* double-to-int v8, v8 */
/* .line 177 */
/* .local v8, "deltLine":I */
/* const/16 v9, 0xa */
/* .line 179 */
/* .local v9, "accountNameLength":I */
final String v13 = "cupid"; // const-string v13, "cupid"
v14 = android.os.Build.DEVICE;
v13 = (( java.lang.String ) v13 ).equals ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_3
/* .line 180 */
/* add-int/lit16 v3, v3, 0xb4 */
/* .line 181 */
/* add-int/lit8 v8, v8, -0x32 */
/* .line 185 */
} // :cond_3
/* div-int/lit8 v13, v4, 0x2 */
/* int-to-float v13, v13 */
/* div-int/lit8 v14, v5, 0x2 */
/* int-to-float v14, v14 */
/* move/from16 v18, v0 */
} // .end local v0 # "x":I
/* .local v18, "x":I */
/* const/high16 v0, 0x43a50000 # 330.0f */
(( android.graphics.Canvas ) v7 ).rotate ( v0, v13, v14 ); // invoke-virtual {v7, v0, v13, v14}, Landroid/graphics/Canvas;->rotate(FFF)V
/* .line 188 */
int v0 = 0; // const/4 v0, 0x0
/* move v13, v9 */
/* move v9, v3 */
/* move/from16 v3, v18 */
/* .line 189 */
} // .end local v18 # "x":I
/* .local v0, "i":I */
/* .local v3, "x":I */
/* .local v9, "y":I */
/* .local v13, "accountNameLength":I */
} // :goto_1
int v14 = 4; // const/4 v14, 0x4
/* if-ge v0, v14, :cond_a */
/* .line 191 */
v14 = this.mAccountName;
if ( v14 != null) { // if-eqz v14, :cond_4
/* .line 192 */
v13 = (( java.lang.String ) v14 ).length ( ); // invoke-virtual {v14}, Ljava/lang/String;->length()I
/* .line 193 */
v14 = this.mAccountName;
/* move/from16 v18, v4 */
} // .end local v4 # "dw":I
/* .local v18, "dw":I */
/* int-to-float v4, v3 */
/* move-object/from16 v19, v6 */
} // .end local v6 # "dirty":Landroid/graphics/Rect;
/* .local v19, "dirty":Landroid/graphics/Rect; */
/* int-to-float v6, v9 */
/* move-object/from16 v20, v2 */
v2 = this.mTextPaint;
(( android.graphics.Canvas ) v7 ).drawText ( v14, v4, v6, v2 ); // invoke-virtual {v7, v14, v4, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
/* .line 194 */
v2 = com.android.server.wm.MIUIWatermark.mIMEI;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 195 */
v2 = com.android.server.wm.MIUIWatermark.mIMEI;
v4 = com.android.server.wm.MIUIWatermark.mIMEI;
v4 = (( java.lang.String ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/lang/String;->length()I
/* sub-int/2addr v4, v13 */
/* mul-int/2addr v4, v10 */
/* div-int/lit8 v4, v4, 0x2 */
/* sub-int v4, v3, v4 */
/* int-to-float v4, v4 */
/* sub-int v6, v9, v11 */
/* int-to-float v6, v6 */
v14 = this.mTextPaint;
(( android.graphics.Canvas ) v7 ).drawText ( v2, v4, v6, v14 ); // invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
/* .line 197 */
} // .end local v18 # "dw":I
} // .end local v19 # "dirty":Landroid/graphics/Rect;
/* .restart local v4 # "dw":I */
/* .restart local v6 # "dirty":Landroid/graphics/Rect; */
} // :cond_4
/* move-object/from16 v20, v2 */
/* move/from16 v18, v4 */
/* move-object/from16 v19, v6 */
} // .end local v4 # "dw":I
} // .end local v6 # "dirty":Landroid/graphics/Rect;
/* .restart local v18 # "dw":I */
/* .restart local v19 # "dirty":Landroid/graphics/Rect; */
v2 = com.android.server.wm.MIUIWatermark.mIMEI;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 198 */
v2 = com.android.server.wm.MIUIWatermark.mIMEI;
v4 = com.android.server.wm.MIUIWatermark.mIMEI;
v4 = (( java.lang.String ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/lang/String;->length()I
/* sub-int/2addr v4, v13 */
/* mul-int/2addr v4, v10 */
/* div-int/lit8 v4, v4, 0x2 */
/* sub-int v4, v3, v4 */
/* int-to-float v4, v4 */
/* sub-int v6, v9, v11 */
/* int-to-float v6, v6 */
v14 = this.mTextPaint;
(( android.graphics.Canvas ) v7 ).drawText ( v2, v4, v6, v14 ); // invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
/* .line 202 */
} // :cond_5
} // :goto_2
v2 = this.mAccountName;
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 203 */
/* add-int v4, v3, v12 */
/* int-to-float v4, v4 */
/* add-int v6, v9, v17 */
/* int-to-float v6, v6 */
v14 = this.mTextPaint;
(( android.graphics.Canvas ) v7 ).drawText ( v2, v4, v6, v14 ); // invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
/* .line 204 */
v2 = com.android.server.wm.MIUIWatermark.mIMEI;
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 205 */
v2 = com.android.server.wm.MIUIWatermark.mIMEI;
/* add-int v4, v3, v12 */
v6 = com.android.server.wm.MIUIWatermark.mIMEI;
v6 = (( java.lang.String ) v6 ).length ( ); // invoke-virtual {v6}, Ljava/lang/String;->length()I
/* sub-int/2addr v6, v13 */
/* mul-int/2addr v6, v10 */
/* div-int/lit8 v6, v6, 0x2 */
/* sub-int/2addr v4, v6 */
/* int-to-float v4, v4 */
/* add-int v6, v9, v17 */
/* sub-int/2addr v6, v11 */
/* int-to-float v6, v6 */
v14 = this.mTextPaint;
(( android.graphics.Canvas ) v7 ).drawText ( v2, v4, v6, v14 ); // invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
/* .line 207 */
} // :cond_6
v2 = com.android.server.wm.MIUIWatermark.mIMEI;
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 208 */
v2 = com.android.server.wm.MIUIWatermark.mIMEI;
/* add-int v4, v3, v12 */
v6 = com.android.server.wm.MIUIWatermark.mIMEI;
v6 = (( java.lang.String ) v6 ).length ( ); // invoke-virtual {v6}, Ljava/lang/String;->length()I
/* sub-int/2addr v6, v13 */
/* mul-int/2addr v6, v10 */
/* div-int/lit8 v6, v6, 0x2 */
/* sub-int/2addr v4, v6 */
/* int-to-float v4, v4 */
/* add-int v6, v9, v17 */
/* sub-int/2addr v6, v11 */
/* int-to-float v6, v6 */
v14 = this.mTextPaint;
(( android.graphics.Canvas ) v7 ).drawText ( v2, v4, v6, v14 ); // invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
/* .line 212 */
} // :cond_7
} // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_9
/* if-nez v8, :cond_8 */
/* move v6, v3 */
/* move v14, v10 */
/* move/from16 v21, v11 */
/* move/from16 v22, v12 */
/* move/from16 v23, v13 */
/* .line 216 */
} // :cond_8
/* move v2, v10 */
/* move v4, v11 */
} // .end local v10 # "incDeltx":I
} // .end local v11 # "incDelty":I
/* .local v2, "incDeltx":I */
/* .local v4, "incDelty":I */
/* int-to-double v10, v3 */
/* move v14, v2 */
/* move v6, v3 */
} // .end local v2 # "incDeltx":I
} // .end local v3 # "x":I
/* .local v6, "x":I */
/* .local v14, "incDeltx":I */
/* int-to-double v2, v5 */
/* invoke-static/range {v15 ..v16}, Ljava/lang/Math;->tan(D)D */
/* move-result-wide v21 */
/* mul-double v2, v2, v21 */
/* move/from16 v21, v4 */
} // .end local v4 # "incDelty":I
/* .local v21, "incDelty":I */
/* div-int v4, v5, v8 */
/* move/from16 v22, v12 */
/* move/from16 v23, v13 */
} // .end local v12 # "columnDeltX":I
} // .end local v13 # "accountNameLength":I
/* .local v22, "columnDeltX":I */
/* .local v23, "accountNameLength":I */
/* int-to-double v12, v4 */
/* div-double/2addr v2, v12 */
/* sub-double/2addr v10, v2 */
/* double-to-int v3, v10 */
/* .line 217 */
} // .end local v6 # "x":I
/* .restart local v3 # "x":I */
/* add-int/2addr v9, v8 */
/* .line 218 */
/* add-int/lit8 v0, v0, 0x1 */
/* move v10, v14 */
/* move/from16 v4, v18 */
/* move-object/from16 v6, v19 */
/* move-object/from16 v2, v20 */
/* move/from16 v11, v21 */
/* move/from16 v12, v22 */
/* move/from16 v13, v23 */
/* goto/16 :goto_1 */
/* .line 212 */
} // .end local v14 # "incDeltx":I
} // .end local v21 # "incDelty":I
} // .end local v22 # "columnDeltX":I
} // .end local v23 # "accountNameLength":I
/* .restart local v10 # "incDeltx":I */
/* .restart local v11 # "incDelty":I */
/* .restart local v12 # "columnDeltX":I */
/* .restart local v13 # "accountNameLength":I */
} // :cond_9
/* move v6, v3 */
/* move v14, v10 */
/* move/from16 v21, v11 */
/* move/from16 v22, v12 */
/* move/from16 v23, v13 */
/* .line 213 */
} // .end local v3 # "x":I
} // .end local v10 # "incDeltx":I
} // .end local v11 # "incDelty":I
} // .end local v12 # "columnDeltX":I
} // .end local v13 # "accountNameLength":I
/* .restart local v6 # "x":I */
/* .restart local v14 # "incDeltx":I */
/* .restart local v21 # "incDelty":I */
/* .restart local v22 # "columnDeltX":I */
/* .restart local v23 # "accountNameLength":I */
} // :goto_4
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "dh: "; // const-string v3, "dh: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "deltLine: "; // const-string v3, "deltLine: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object/from16 v3, v20 */
android.util.Log .d ( v3,v2 );
/* .line 214 */
/* move/from16 v13, v23 */
/* .line 189 */
} // .end local v14 # "incDeltx":I
} // .end local v18 # "dw":I
} // .end local v19 # "dirty":Landroid/graphics/Rect;
} // .end local v21 # "incDelty":I
} // .end local v22 # "columnDeltX":I
} // .end local v23 # "accountNameLength":I
/* .restart local v3 # "x":I */
/* .local v4, "dw":I */
/* .local v6, "dirty":Landroid/graphics/Rect; */
/* .restart local v10 # "incDeltx":I */
/* .restart local v11 # "incDelty":I */
/* .restart local v12 # "columnDeltX":I */
/* .restart local v13 # "accountNameLength":I */
} // :cond_a
/* move/from16 v18, v4 */
/* move-object/from16 v19, v6 */
/* move v14, v10 */
/* move/from16 v21, v11 */
/* move/from16 v22, v12 */
/* move v6, v3 */
/* .line 220 */
} // .end local v3 # "x":I
} // .end local v4 # "dw":I
} // .end local v10 # "incDeltx":I
} // .end local v11 # "incDelty":I
} // .end local v12 # "columnDeltX":I
/* .local v6, "x":I */
/* .restart local v14 # "incDeltx":I */
/* .restart local v18 # "dw":I */
/* .restart local v19 # "dirty":Landroid/graphics/Rect; */
/* .restart local v21 # "incDelty":I */
/* .restart local v22 # "columnDeltX":I */
} // :goto_5
v2 = this.mSurface;
(( android.view.Surface ) v2 ).unlockCanvasAndPost ( v7 ); // invoke-virtual {v2, v7}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
/* .line 221 */
return;
/* .line 143 */
} // .end local v0 # "i":I
} // .end local v5 # "dh":I
} // .end local v6 # "x":I
} // .end local v7 # "c":Landroid/graphics/Canvas;
} // .end local v8 # "deltLine":I
} // .end local v9 # "y":I
} // .end local v13 # "accountNameLength":I
} // .end local v14 # "incDeltx":I
} // .end local v15 # "radians":D
} // .end local v17 # "columnDeltY":I
} // .end local v18 # "dw":I
} // .end local v19 # "dirty":Landroid/graphics/Rect;
} // .end local v21 # "incDelty":I
} // .end local v22 # "columnDeltX":I
} // :cond_b
} // :goto_6
return;
} // .end method
public synchronized void init ( com.android.server.wm.WindowManagerService p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "wms" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "msg" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 295 */
try { // :try_start_0
v0 = android.os.Binder .getCallingPid ( );
v1 = android.os.Process .myPid ( );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-eq v0, v1, :cond_0 */
/* .line 296 */
/* monitor-exit p0 */
return;
/* .line 299 */
} // :cond_0
try { // :try_start_1
this.mWmService = p1;
/* .line 300 */
v0 = this.mTransactionFactory;
/* check-cast v0, Landroid/view/SurfaceControl$Transaction; */
this.mTransaction = v0;
/* .line 302 */
v0 = this.mCallback;
/* if-nez v0, :cond_1 */
v0 = this.mPolicy;
/* instance-of v0, v0, Lcom/android/server/policy/MiuiPhoneWindowManager; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 303 */
/* new-instance v0, Lcom/android/server/wm/MIUIWatermark$2; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/android/server/wm/MIUIWatermark$2;-><init>(Lcom/android/server/wm/MIUIWatermark;Lcom/android/server/wm/WindowManagerService;Ljava/lang/String;)V */
this.mCallback = v0;
/* .line 320 */
v0 = this.mPolicy;
/* check-cast v0, Lcom/android/server/policy/MiuiPhoneWindowManager; */
v1 = this.mCallback;
(( com.android.server.policy.MiuiPhoneWindowManager ) v0 ).registerMIUIWatermarkCallback ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->registerMIUIWatermarkCallback(Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;)V
/* .line 322 */
} // .end local p0 # "this":Lcom/android/server/wm/MIUIWatermark;
} // :cond_1
/* invoke-direct {p0, p2}, Lcom/android/server/wm/MIUIWatermark;->createSurfaceLocked(Ljava/lang/String;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 323 */
/* monitor-exit p0 */
return;
/* .line 294 */
} // .end local p1 # "wms":Lcom/android/server/wm/WindowManagerService;
} // .end local p2 # "msg":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void positionSurface ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "dw" # I */
/* .param p2, "dh" # I */
/* .line 107 */
v0 = this.mSurfaceControl;
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v1, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDW:I */
/* if-ne v1, p1, :cond_0 */
/* iget v1, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDH:I */
/* if-eq v1, p2, :cond_1 */
/* .line 108 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDW:I */
/* .line 109 */
/* iput p2, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDH:I */
/* .line 110 */
v1 = this.mTransaction;
(( android.view.SurfaceControl$Transaction ) v1 ).setBufferSize ( v0, p1, p2 ); // invoke-virtual {v1, v0, p1, p2}, Landroid/view/SurfaceControl$Transaction;->setBufferSize(Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;
/* .line 111 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z */
/* .line 113 */
} // :cond_1
return;
} // .end method
