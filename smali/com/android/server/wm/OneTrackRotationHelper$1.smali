.class Lcom/android/server/wm/OneTrackRotationHelper$1;
.super Landroid/os/Handler;
.source "OneTrackRotationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/OneTrackRotationHelper;->initOneTrackRotationThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/OneTrackRotationHelper;


# direct methods
.method constructor <init>(Lcom/android/server/wm/OneTrackRotationHelper;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/OneTrackRotationHelper;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 245
    iput-object p1, p0, Lcom/android/server/wm/OneTrackRotationHelper$1;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .line 248
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 259
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$1;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetmRotationStateMachine(Lcom/android/server/wm/OneTrackRotationHelper;)Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onDeviceFoldChanged(Landroid/os/Message;)V

    .line 260
    goto :goto_0

    .line 268
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$1;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetmRotationStateMachine(Lcom/android/server/wm/OneTrackRotationHelper;)Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->-$$Nest$monTodayIsOver(Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;)V

    goto :goto_0

    .line 265
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$1;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetmRotationStateMachine(Lcom/android/server/wm/OneTrackRotationHelper;)Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onNextSending()V

    .line 266
    goto :goto_0

    .line 262
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$1;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetmRotationStateMachine(Lcom/android/server/wm/OneTrackRotationHelper;)Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onShutDown()V

    .line 263
    goto :goto_0

    .line 256
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$1;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetmRotationStateMachine(Lcom/android/server/wm/OneTrackRotationHelper;)Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onScreenStateChanged(Landroid/os/Message;)V

    .line 257
    goto :goto_0

    .line 250
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$1;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetmRotationStateMachine(Lcom/android/server/wm/OneTrackRotationHelper;)Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onForegroundAppChanged(Landroid/os/Message;)V

    .line 251
    goto :goto_0

    .line 253
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/wm/OneTrackRotationHelper$1;->this$0:Lcom/android/server/wm/OneTrackRotationHelper;

    invoke-static {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->-$$Nest$fgetmRotationStateMachine(Lcom/android/server/wm/OneTrackRotationHelper;)Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/wm/OneTrackRotationHelper$RotationStateMachine;->onRotationChanged(Landroid/os/Message;)V

    .line 254
    nop

    .line 270
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
