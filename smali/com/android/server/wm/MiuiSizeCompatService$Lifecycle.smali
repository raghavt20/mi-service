.class public final Lcom/android/server/wm/MiuiSizeCompatService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiSizeCompatService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiSizeCompatService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/android/server/wm/MiuiSizeCompatService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 213
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 214
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-direct {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$Lifecycle;->mService:Lcom/android/server/wm/MiuiSizeCompatService;

    .line 215
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 219
    const-string v0, "MiuiSizeCompat"

    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$Lifecycle;->mService:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/MiuiSizeCompatService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 220
    return-void
.end method
