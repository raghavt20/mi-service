public class com.android.server.wm.MiuiMiPerfStubImpl implements com.android.server.wm.MiuiMiPerfStub {
	 /* .source "MiuiMiPerfStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String DEFAULT_COMMON_PACKAGENAME;
	 private static final Integer PROFILE_MAX_BYTE;
	 private static final java.lang.String TAG;
	 private static final java.lang.String THERMAL_SCONFIG;
	 private static java.util.HashMap mMiperfXmlMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;>;" */
	 /* } */
} // .end annotation
} // .end field
private static java.util.HashMap mThermalSconfigMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean isCommon;
private Boolean isDebug;
private Boolean isRecoverWrite;
private Boolean isRelease;
private Boolean isdefault;
private java.lang.String lastActName;
private java.lang.String lastPackName;
private java.util.Timer timer;
private java.util.TimerTask timerTask;
/* # direct methods */
static void -$$Nest$mmiPerfSystemBoostNotify ( com.android.server.wm.MiuiMiPerfStubImpl p0, Integer p1, java.lang.String p2, java.lang.String p3, java.lang.String p4 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiMiPerfStubImpl;->miPerfSystemBoostNotify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
static com.android.server.wm.MiuiMiPerfStubImpl ( ) {
/* .locals 1 */
/* .line 42 */
android.os.MiPerf .miPerfGetXmlMap ( );
/* .line 43 */
android.os.MiPerf .getThermalMap ( );
/* .line 44 */
return;
} // .end method
public com.android.server.wm.MiuiMiPerfStubImpl ( ) {
/* .locals 2 */
/* .line 46 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z */
/* .line 29 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRelease:Z */
/* .line 30 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z */
/* .line 31 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isdefault:Z */
/* .line 32 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isDebug:Z */
/* .line 33 */
final String v0 = " "; // const-string v0, " "
this.lastPackName = v0;
/* .line 34 */
this.lastActName = v0;
/* .line 47 */
final String v0 = "MiuiMiPerfStubImpl"; // const-string v0, "MiuiMiPerfStubImpl"
final String v1 = "MiuiMiPerfStubImpl is Initialized!"; // const-string v1, "MiuiMiPerfStubImpl is Initialized!"
android.util.Slog .d ( v0,v1 );
/* .line 48 */
return;
} // .end method
private void miPerfSystemBoostNotify ( Integer p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 5 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "activityName" # Ljava/lang/String; */
/* .param p4, "BoostScenes" # Ljava/lang/String; */
/* .line 116 */
int v0 = 0; // const/4 v0, 0x0
/* .line 117 */
/* .local v0, "ret":I */
(( com.android.server.wm.MiuiMiPerfStubImpl ) p0 ).getSystemBoostMode ( p2, p3 ); // invoke-virtual {p0, p2, p3}, Lcom/android/server/wm/MiuiMiPerfStubImpl;->getSystemBoostMode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 118 */
/* .local v1, "mBoostMode":Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
final String p3 = "Common"; // const-string p3, "Common"
/* .line 119 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isdefault:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
final String p2 = "default.common.packagename"; // const-string p2, "default.common.packagename"
/* .line 121 */
} // :cond_1
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z */
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 122 */
v2 = this.lastPackName;
/* if-ne p2, v2, :cond_2 */
v2 = this.lastActName;
/* if-eq p3, v2, :cond_3 */
/* .line 123 */
} // :cond_2
final String v2 = "WRITENODE_RECOVER"; // const-string v2, "WRITENODE_RECOVER"
android.os.MiPerf .miPerfSystemBoostAcquire ( p1,p2,p3,v2 );
/* .line 124 */
/* iput-boolean v3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z */
/* .line 128 */
} // :cond_3
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRelease:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 129 */
final String v2 = "PERFLOCK_RELEASE"; // const-string v2, "PERFLOCK_RELEASE"
android.os.MiPerf .miPerfSystemBoostAcquire ( p1,p2,p3,v2 );
/* .line 130 */
/* iput-boolean v3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRelease:Z */
/* .line 133 */
} // :cond_4
v2 = this.lastPackName;
/* if-ne p2, v2, :cond_5 */
v2 = this.lastActName;
/* if-ne p3, v2, :cond_5 */
/* .line 134 */
final String v1 = "BOOSTMODE_NULL"; // const-string v1, "BOOSTMODE_NULL"
/* .line 136 */
} // :cond_5
this.lastPackName = p2;
/* .line 137 */
this.lastActName = p3;
/* .line 138 */
v2 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
int v4 = 1; // const/4 v4, 0x1
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_6
/* :sswitch_0 */
final String v2 = "PERFLOCK_ACQUIRE"; // const-string v2, "PERFLOCK_ACQUIRE"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* move v3, v4 */
/* :sswitch_1 */
final String v2 = "WRITENODE_ACQUIRE"; // const-string v2, "WRITENODE_ACQUIRE"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
int v3 = 2; // const/4 v3, 0x2
/* :sswitch_2 */
final String v2 = "BOOSTMODE_NULL"; // const-string v2, "BOOSTMODE_NULL"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* :sswitch_3 */
final String v2 = "MULTIPLEMODE"; // const-string v2, "MULTIPLEMODE"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
int v3 = 3; // const/4 v3, 0x3
} // :goto_0
int v3 = -1; // const/4 v3, -0x1
} // :goto_1
final String v2 = "MiuiMiPerfStubImpl"; // const-string v2, "MiuiMiPerfStubImpl"
/* packed-switch v3, :pswitch_data_0 */
/* .line 150 */
/* :pswitch_0 */
/* iput-boolean v4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z */
/* .line 151 */
/* .line 147 */
/* :pswitch_1 */
/* iput-boolean v4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z */
/* .line 148 */
/* .line 144 */
/* :pswitch_2 */
/* iput-boolean v4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRelease:Z */
/* .line 145 */
/* .line 140 */
/* :pswitch_3 */
/* iget-boolean v3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isDebug:Z */
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 141 */
final String v3 = "miPerfSystemBoostNotify: no MiPerfBoost!"; // const-string v3, "miPerfSystemBoostNotify: no MiPerfBoost!"
android.util.Slog .d ( v2,v3 );
/* .line 142 */
} // :cond_7
return;
/* .line 155 */
} // :goto_2
v0 = android.os.MiPerf .miPerfSystemBoostAcquire ( p1,p2,p3,v1 );
/* .line 156 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "miPerfSystemBoostNotify: return = "; // const-string v4, "miPerfSystemBoostNotify: return = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", System BoostScenes = "; // const-string v4, ", System BoostScenes = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p4 ); // invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", BoostMode = "; // const-string v4, ", BoostMode = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", pid = "; // const-string v4, ", pid = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", packagename = "; // const-string v4, ", packagename = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", activityname = "; // const-string v4, ", activityname = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 158 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x6696c04d -> :sswitch_3 */
/* -0x2cfa1580 -> :sswitch_2 */
/* 0x46b9d0b8 -> :sswitch_1 */
/* 0x53234b0b -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static java.lang.String readFile ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p0, "fileName" # Ljava/lang/String; */
/* .line 51 */
final String v0 = "readFromFile, finally fis.close() IOException e:"; // const-string v0, "readFromFile, finally fis.close() IOException e:"
final String v1 = "MiuiMiPerfStubImpl"; // const-string v1, "MiuiMiPerfStubImpl"
int v2 = 0; // const/4 v2, 0x0
/* .line 52 */
/* .local v2, "jString":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 53 */
/* .local v3, "reader":Ljava/io/BufferedReader; */
/* const/16 v4, 0x8 */
/* .line 54 */
/* .local v4, "num":I */
/* new-array v5, v4, [C */
/* .line 56 */
/* .local v5, "buffer":[C */
try { // :try_start_0
/* new-instance v6, Ljava/io/BufferedReader; */
/* new-instance v7, Ljava/io/FileReader; */
/* invoke-direct {v7, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v3, v6 */
/* .line 57 */
(( java.io.BufferedReader ) v3 ).read ( v5 ); // invoke-virtual {v3, v5}, Ljava/io/BufferedReader;->read([C)I
/* .line 58 */
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 63 */
/* nop */
/* .line 65 */
try { // :try_start_1
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 66 */
/* :catch_0 */
/* move-exception v6 */
/* .line 67 */
/* .local v6, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_0
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v6 ).getMessage ( ); // invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 68 */
int v2 = 0; // const/4 v2, 0x0
/* .line 69 */
} // .end local v6 # "e":Ljava/io/IOException;
} // :goto_1
/* .line 63 */
/* :catchall_0 */
/* move-exception v6 */
/* .line 59 */
/* :catch_1 */
/* move-exception v6 */
/* .line 60 */
/* .restart local v6 # "e":Ljava/io/IOException; */
try { // :try_start_2
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "readFile, IOException e:"; // const-string v8, "readFile, IOException e:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v6 ).getMessage ( ); // invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v7 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 61 */
int v2 = 0; // const/4 v2, 0x0
/* .line 63 */
} // .end local v6 # "e":Ljava/io/IOException;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 65 */
try { // :try_start_3
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 66 */
/* :catch_2 */
/* move-exception v6 */
/* .line 67 */
/* .restart local v6 # "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 72 */
} // .end local v6 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
/* aget-char v1, v5, v0 */
/* const/16 v6, 0x2d */
/* if-ne v1, v6, :cond_1 */
/* .line 73 */
final String v0 = "-1"; // const-string v0, "-1"
/* .line 75 */
} // :cond_1
/* aget-char v0, v5, v0 */
java.lang.String .valueOf ( v0 );
/* .line 76 */
} // .end local v2 # "jString":Ljava/lang/String;
/* .local v0, "jString":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* .local v1, "i":I */
} // :goto_3
/* if-ge v1, v4, :cond_2 */
/* .line 77 */
/* aget-char v2, v5, v1 */
/* const/16 v6, 0x30 */
/* if-lt v2, v6, :cond_2 */
/* aget-char v2, v5, v1 */
/* const/16 v6, 0x39 */
/* if-gt v2, v6, :cond_2 */
/* .line 78 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-char v6, v5, v1 */
java.lang.String .valueOf ( v6 );
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 76 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 83 */
} // .end local v1 # "i":I
} // :cond_2
/* .line 63 */
} // .end local v0 # "jString":Ljava/lang/String;
/* .restart local v2 # "jString":Ljava/lang/String; */
} // :goto_4
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 65 */
try { // :try_start_4
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 69 */
/* .line 66 */
/* :catch_3 */
/* move-exception v7 */
/* .line 67 */
/* .local v7, "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v7 ).getMessage ( ); // invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 68 */
int v2 = 0; // const/4 v2, 0x0
/* .line 71 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :cond_3
} // :goto_5
/* throw v6 */
} // .end method
/* # virtual methods */
public java.lang.String getSystemBoostMode ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "activityName" # Ljava/lang/String; */
/* .line 87 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z */
/* .line 88 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isdefault:Z */
/* .line 89 */
v0 = com.android.server.wm.MiuiMiPerfStubImpl.mMiperfXmlMap;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
final String v1 = "MiuiMiPerfStubImpl"; // const-string v1, "MiuiMiPerfStubImpl"
int v2 = 1; // const/4 v2, 0x1
final String v3 = "Common"; // const-string v3, "Common"
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 90 */
v0 = com.android.server.wm.MiuiMiPerfStubImpl.mMiperfXmlMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 91 */
/* .local v0, "act_map":Ljava/lang/Object; */
/* move-object v4, v0 */
/* check-cast v4, Ljava/util/HashMap; */
/* .line 92 */
/* .local v4, "map_tmp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;" */
v5 = (( java.util.HashMap ) v4 ).containsKey ( p2 ); // invoke-virtual {v4, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 93 */
/* iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isDebug:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 94 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Match BoostMode successfully, BoostMode is "; // const-string v3, "Match BoostMode successfully, BoostMode is "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.util.HashMap ) v4 ).get ( p2 ); // invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 95 */
} // :cond_0
(( java.util.HashMap ) v4 ).get ( p2 ); // invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
/* .line 96 */
} // :cond_1
v5 = (( java.util.HashMap ) v4 ).containsKey ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 97 */
/* iput-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z */
/* .line 98 */
(( java.util.HashMap ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
/* .line 100 */
} // .end local v0 # "act_map":Ljava/lang/Object;
} // .end local v4 # "map_tmp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
} // :cond_2
/* .line 101 */
} // :cond_3
final String v0 = "/sys/class/thermal/thermal_message/sconfig"; // const-string v0, "/sys/class/thermal/thermal_message/sconfig"
com.android.server.wm.MiuiMiPerfStubImpl .readFile ( v0 );
/* .line 102 */
/* .local v0, "thermalSconfig":Ljava/lang/String; */
v4 = com.android.server.wm.MiuiMiPerfStubImpl.mThermalSconfigMap;
v4 = (( java.util.HashMap ) v4 ).containsKey ( v0 ); // invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_4
v4 = com.android.server.wm.MiuiMiPerfStubImpl.mMiperfXmlMap;
final String v5 = "default.common.packagename"; // const-string v5, "default.common.packagename"
v4 = (( java.util.HashMap ) v4 ).containsKey ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 103 */
/* iput-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isdefault:Z */
/* .line 104 */
/* iput-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z */
/* .line 105 */
v1 = com.android.server.wm.MiuiMiPerfStubImpl.mMiperfXmlMap;
(( java.util.HashMap ) v1 ).get ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 106 */
/* .local v1, "act_map":Ljava/lang/Object; */
/* move-object v2, v1 */
/* check-cast v2, Ljava/util/HashMap; */
/* .line 107 */
/* .local v2, "map_tmp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;" */
(( java.util.HashMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
/* .line 110 */
} // .end local v0 # "thermalSconfig":Ljava/lang/String;
} // .end local v1 # "act_map":Ljava/lang/Object;
} // .end local v2 # "map_tmp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
} // :cond_4
} // :goto_0
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isDebug:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 111 */
final String v0 = "Match BoostMode failed, there is no MiPerfBoost!"; // const-string v0, "Match BoostMode failed, there is no MiPerfBoost!"
android.util.Slog .d ( v1,v0 );
/* .line 112 */
} // :cond_5
final String v0 = "BOOSTMODE_NULL"; // const-string v0, "BOOSTMODE_NULL"
} // .end method
public void onAfterActivityResumed ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 7 */
/* .param p1, "resumedActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 163 */
v0 = this.app;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.info;
/* if-nez v0, :cond_0 */
/* .line 166 */
} // :cond_0
v0 = this.app;
v0 = (( com.android.server.wm.WindowProcessController ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* .line 167 */
/* .local v0, "pid":I */
v1 = this.info;
v1 = this.name;
/* .line 168 */
/* .local v1, "activityName":Ljava/lang/String; */
v2 = this.info;
v2 = this.packageName;
/* .line 169 */
/* .local v2, "packageName":Ljava/lang/String; */
/* new-instance v3, Lcom/android/server/wm/MiuiMiPerfStubImpl$1; */
/* invoke-direct {v3, p0, v0, v2, v1}, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;-><init>(Lcom/android/server/wm/MiuiMiPerfStubImpl;ILjava/lang/String;Ljava/lang/String;)V */
this.timerTask = v3;
/* .line 184 */
/* new-instance v3, Ljava/util/Timer; */
/* invoke-direct {v3}, Ljava/util/Timer;-><init>()V */
this.timer = v3;
/* .line 185 */
v4 = this.timerTask;
/* const-wide/16 v5, 0x64 */
(( java.util.Timer ) v3 ).schedule ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
/* .line 186 */
return;
/* .line 164 */
} // .end local v0 # "pid":I
} // .end local v1 # "activityName":Ljava/lang/String;
} // .end local v2 # "packageName":Ljava/lang/String;
} // :cond_1
} // :goto_0
return;
} // .end method
