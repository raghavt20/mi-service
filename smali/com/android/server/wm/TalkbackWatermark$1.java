class com.android.server.wm.TalkbackWatermark$1 extends android.database.ContentObserver {
	 /* .source "TalkbackWatermark.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/TalkbackWatermark;->init(Lcom/android/server/wm/WindowManagerService;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.TalkbackWatermark this$0; //synthetic
final android.net.Uri val$talkbackWatermarkEnableUri; //synthetic
/* # direct methods */
 com.android.server.wm.TalkbackWatermark$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/TalkbackWatermark; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 105 */
this.this$0 = p1;
this.val$talkbackWatermarkEnableUri = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 108 */
v0 = this.val$talkbackWatermarkEnableUri;
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 109 */
	 v0 = this.this$0;
	 com.android.server.wm.TalkbackWatermark .-$$Nest$mupdateWaterMark ( v0 );
	 /* .line 111 */
} // :cond_0
return;
} // .end method
