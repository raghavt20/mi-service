.class public Lcom/android/server/wm/AppResurrectionTrackManager;
.super Ljava/lang/Object;
.source "AppResurrectionTrackManager.java"


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000401629"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final PACKAGE_NAME:Ljava/lang/String; = "android"

.field private static final SERVICE_NAME:Ljava/lang/String; = "com.miui.analytics.onetrack.TrackService"

.field private static final SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final TAG:Ljava/lang/String; = "AppResurrectionTrackManager"


# instance fields
.field private DEBUG:Z

.field private final mConnection:Landroid/content/ServiceConnection;

.field private final mContext:Landroid/content/Context;

.field private mITrackBinder:Lcom/miui/analytics/ITrackBinder;

.field private mIsBind:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmITrackBinder(Lcom/android/server/wm/AppResurrectionTrackManager;)Lcom/miui/analytics/ITrackBinder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmITrackBinder(Lcom/android/server/wm/AppResurrectionTrackManager;Lcom/miui/analytics/ITrackBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsBind(Lcom/android/server/wm/AppResurrectionTrackManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mIsBind:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->DEBUG:Z

    .line 36
    new-instance v0, Lcom/android/server/wm/AppResurrectionTrackManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/AppResurrectionTrackManager$1;-><init>(Lcom/android/server/wm/AppResurrectionTrackManager;)V

    iput-object v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mConnection:Landroid/content/ServiceConnection;

    .line 52
    iput-object p1, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mContext:Landroid/content/Context;

    .line 53
    return-void
.end method

.method private trackEvent(Lorg/json/JSONObject;)V
    .locals 6
    .param p1, "jsonData"    # Lorg/json/JSONObject;

    .line 99
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    const-string v1, "AppResurrectionTrackManager"

    if-nez v0, :cond_0

    .line 100
    const-string v0, "TrackService Not Bound,Please Try Again"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual {p0}, Lcom/android/server/wm/AppResurrectionTrackManager;->bindTrackBinder()V

    .line 102
    return-void

    .line 104
    :cond_0
    if-nez p1, :cond_1

    .line 105
    const-string v0, "JSONObject is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-void

    .line 109
    :cond_1
    :try_start_0
    const-string v2, "31000401629"

    const-string v3, "android"

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 110
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Send to TrackService Success:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 113
    :cond_2
    const-string v0, "Binder to Success"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    goto :goto_1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "trackEvent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method


# virtual methods
.method public bindTrackBinder()V
    .locals 5

    .line 56
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-nez v0, :cond_0

    .line 58
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 59
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    const-string v2, "com.miui.analytics.onetrack.TrackService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mConnection:Landroid/content/ServiceConnection;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v2, v4, v3}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mIsBind:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 63
    const-string v1, "AppResurrectionTrackManager"

    const-string v2, "Bind Service Exception"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method public sendTrack(Ljava/lang/String;JLjava/lang/String;)V
    .locals 6
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "launchTime"    # J
    .param p4, "killReason"    # Ljava/lang/String;

    .line 81
    const-string v2, ""

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/wm/AppResurrectionTrackManager;->sendTrack(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 82
    return-void
.end method

.method public sendTrack(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "pkgVersion"    # Ljava/lang/String;
    .param p3, "launchTime"    # J
    .param p5, "killReason"    # Ljava/lang/String;

    .line 85
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 87
    .local v0, "jsonData":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "EVENT_NAME"

    const-string v2, "event_appres_launch_time"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 88
    const-string v1, "pkg_name"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 89
    const-string v1, "pkg_version"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 90
    const-string v1, "kill_reason"

    invoke-virtual {v0, v1, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 91
    const-string v1, "launch_time_num"

    invoke-virtual {v0, v1, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    goto :goto_0

    .line 92
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 95
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    invoke-direct {p0, v0}, Lcom/android/server/wm/AppResurrectionTrackManager;->trackEvent(Lorg/json/JSONObject;)V

    .line 96
    return-void
.end method

.method public unbindTrackBinder()V
    .locals 3

    .line 69
    iget-boolean v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mIsBind:Z

    if-eqz v0, :cond_0

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/wm/AppResurrectionTrackManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 74
    const-string v1, "AppResurrectionTrackManager"

    const-string v2, "Unbind Service Exception"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method
