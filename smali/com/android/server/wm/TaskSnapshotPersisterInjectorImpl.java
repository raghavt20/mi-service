class com.android.server.wm.TaskSnapshotPersisterInjectorImpl extends com.android.server.wm.TaskSnapshotPersisterInjectorStub {
	 /* .source "TaskSnapshotPersisterInjectorImpl.java" */
	 /* # static fields */
	 public static final java.lang.String BITMAP_EXTENSION;
	 public static final java.lang.String LOW_RES_FILE_POSTFIX;
	 public static final java.lang.String PROTO_EXTENSION;
	 public static final java.lang.String SNAPSHOTS_DIRNAME_QS;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 com.android.server.wm.TaskSnapshotPersisterInjectorImpl ( ) {
		 /* .locals 0 */
		 /* .line 22 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorStub;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String checkName ( android.window.TaskSnapshot p0 ) {
		 /* .locals 3 */
		 /* .param p1, "snapshot" # Landroid/window/TaskSnapshot; */
		 /* .line 184 */
		 if ( p1 != null) { // if-eqz p1, :cond_2
			 (( android.window.TaskSnapshot ) p1 ).getTopActivityComponent ( ); // invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getTopActivityComponent()Landroid/content/ComponentName;
			 /* if-nez v0, :cond_0 */
			 /* .line 189 */
		 } // :cond_0
		 (( android.window.TaskSnapshot ) p1 ).getTopActivityComponent ( ); // invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getTopActivityComponent()Landroid/content/ComponentName;
		 (( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
		 /* .line 190 */
		 /* .local v0, "name":Ljava/lang/String; */
		 v1 = 		 android.text.TextUtils .isEmpty ( v0 );
		 /* if-nez v1, :cond_1 */
		 v1 = 		 v1 = android.app.TaskSnapshotHelperImpl.QUICK_START_NAME_WITH_ACTIVITY_LIST;
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 191 */
			 (( android.window.TaskSnapshot ) p1 ).getClassNameQS ( ); // invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getClassNameQS()Ljava/lang/String;
			 if ( v1 != null) { // if-eqz v1, :cond_1
				 /* .line 192 */
				 /* new-instance v1, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
				 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( android.window.TaskSnapshot ) p1 ).getClassNameQS ( ); // invoke-virtual {p1}, Landroid/window/TaskSnapshot;->getClassNameQS()Ljava/lang/String;
				 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 /* .line 194 */
			 } // :cond_1
			 /* .line 185 */
		 } // .end local v0 # "name":Ljava/lang/String;
	 } // :cond_2
} // :goto_0
final String v0 = ""; // const-string v0, ""
} // .end method
public void closeQuitely ( java.io.Closeable p0 ) {
/* .locals 3 */
/* .param p1, "stream" # Ljava/io/Closeable; */
/* .line 199 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 200 */
	 try { // :try_start_0
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 202 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 203 */
		 /* .local v0, "e":Ljava/lang/Exception; */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "closeQuitely()...close file failed!"; // const-string v2, "closeQuitely()...close file failed!"
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v2 = "TaskSnapshot_PInjector"; // const-string v2, "TaskSnapshot_PInjector"
		 android.util.Slog .e ( v2,v1 );
		 /* .line 204 */
	 } // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
/* nop */
/* .line 205 */
} // :goto_1
return;
} // .end method
public Boolean couldPersist ( com.android.server.wm.BaseAppSnapshotPersister$DirectoryResolver p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 1 */
/* .param p1, "resolver" # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver; */
/* .param p2, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 38 */
int v0 = 1; // const/4 v0, 0x1
v0 = (( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).couldPersist ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->couldPersist(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;Lcom/android/server/wm/ActivityRecord;Z)Z
} // .end method
public Boolean couldPersist ( com.android.server.wm.BaseAppSnapshotPersister$DirectoryResolver p0, com.android.server.wm.ActivityRecord p1, Boolean p2 ) {
/* .locals 18 */
/* .param p1, "resolver" # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver; */
/* .param p2, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "forceUpdate" # Z */
/* .line 52 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p2 */
/* move/from16 v4, p3 */
int v5 = 0; // const/4 v5, 0x0
if ( v3 != null) { // if-eqz v3, :cond_7
/* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_7
v0 = this.mActivityComponent;
/* if-nez v0, :cond_0 */
/* goto/16 :goto_5 */
/* .line 56 */
} // :cond_0
v0 = this.mActivityComponent;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 57 */
/* .local v12, "pkg":Ljava/lang/String; */
v0 = this.mWmService;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mWmService;
v0 = this.mContext;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* move-object v13, v0 */
/* .line 59 */
/* .local v13, "context":Landroid/content/Context; */
android.app.TaskSnapshotHelperStub .get ( );
v0 = v6 = this.shortComponentName;
/* if-nez v0, :cond_2 */
/* .line 60 */
/* .line 63 */
} // :cond_2
/* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task; */
/* .line 64 */
/* .local v14, "task":Lcom/android/server/wm/Task; */
/* iget v0, v14, Lcom/android/server/wm/Task;->mUserId:I */
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) v1 ).getBitmapFileQS ( v2, v0, v12, v5 ); // invoke-virtual {v1, v2, v0, v12, v5}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getBitmapFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;Z)Ljava/io/File;
/* .line 65 */
/* .local v0, "file":Ljava/io/File; */
v6 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
int v15 = 1; // const/4 v15, 0x1
final String v7 = "TaskSnapshot_PInjector"; // const-string v7, "TaskSnapshot_PInjector"
/* if-nez v6, :cond_3 */
/* .line 66 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "couldPersist()...return true (shot not exist)! file="; // const-string v6, "couldPersist()...return true (shot not exist)! file="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v0 ).getName ( ); // invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v7,v5 );
/* .line 67 */
/* .line 70 */
} // :cond_3
/* iget v6, v14, Lcom/android/server/wm/Task;->mUserId:I */
v8 = this.mActivityComponent;
(( android.content.ComponentName ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) v1 ).getProtoFileQS ( v2, v6, v8 ); // invoke-virtual {v1, v2, v6, v8}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getProtoFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;)Ljava/io/File;
/* .line 71 */
} // .end local v0 # "file":Ljava/io/File;
/* .local v6, "file":Ljava/io/File; */
v0 = (( java.io.File ) v6 ).exists ( ); // invoke-virtual {v6}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_4 */
/* .line 72 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "couldPersist()...return true (proto not exist)! file="; // const-string v5, "couldPersist()...return true (proto not exist)! file="
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v6 ).getName ( ); // invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v7,v0 );
/* .line 73 */
/* .line 75 */
} // :cond_4
int v8 = 0; // const/4 v8, 0x0
/* .line 77 */
/* .local v8, "br":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v9, Ljava/io/FileReader; */
/* invoke-direct {v9, v6}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v0, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_3 */
/* move-object v8, v0 */
/* .line 78 */
try { // :try_start_1
(( java.io.BufferedReader ) v8 ).readLine ( ); // invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* .line 79 */
/* .local v0, "val":Ljava/lang/String; */
java.lang.Long .parseLong ( v0 );
/* move-result-wide v10 */
/* .line 80 */
/* .local v10, "beforeTime":J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v16 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* move-object v5, v8 */
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .local v5, "br":Ljava/io/BufferedReader; */
/* move-wide/from16 v8, v16 */
/* .line 81 */
/* .local v8, "current":J */
try { // :try_start_2
android.app.TaskSnapshotHelperStub .get ( );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* move-object/from16 v17, v6 */
} // .end local v6 # "file":Ljava/io/File;
/* .local v17, "file":Ljava/io/File; */
/* move-object/from16 v6, v16 */
/* move-object v15, v7 */
/* move-object v7, v12 */
try { // :try_start_3
v6 = /* invoke-interface/range {v6 ..v11}, Landroid/app/TaskSnapshotHelperStub;->checkExpired(Ljava/lang/String;JJ)Z */
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* if-nez v6, :cond_6 */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 88 */
} // .end local v0 # "val":Ljava/lang/String;
} // .end local v8 # "current":J
} // .end local v10 # "beforeTime":J
} // :cond_5
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) v1 ).closeQuitely ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V
/* .line 89 */
/* move-object v8, v5 */
/* goto/16 :goto_3 */
/* .line 82 */
/* .restart local v0 # "val":Ljava/lang/String; */
/* .restart local v8 # "current":J */
/* .restart local v10 # "beforeTime":J */
} // :cond_6
} // :goto_1
try { // :try_start_4
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "couldPersist()...return true (expired) forceUpdate = "; // const-string v7, "couldPersist()...return true (expired) forceUpdate = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v15,v6 );
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 83 */
/* nop */
/* .line 88 */
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) v1 ).closeQuitely ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V
/* .line 83 */
int v6 = 1; // const/4 v6, 0x1
/* .line 88 */
} // .end local v0 # "val":Ljava/lang/String;
} // .end local v8 # "current":J
} // .end local v10 # "beforeTime":J
/* :catchall_0 */
/* move-exception v0 */
/* move-object v8, v5 */
/* .line 85 */
/* :catch_0 */
/* move-exception v0 */
/* move-object v8, v5 */
/* .line 88 */
} // .end local v17 # "file":Ljava/io/File;
/* .restart local v6 # "file":Ljava/io/File; */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v17, v6 */
/* move-object v8, v5 */
} // .end local v6 # "file":Ljava/io/File;
/* .restart local v17 # "file":Ljava/io/File; */
/* .line 85 */
} // .end local v17 # "file":Ljava/io/File;
/* .restart local v6 # "file":Ljava/io/File; */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v17, v6 */
/* move-object v15, v7 */
/* move-object v8, v5 */
} // .end local v6 # "file":Ljava/io/File;
/* .restart local v17 # "file":Ljava/io/File; */
/* .line 88 */
} // .end local v5 # "br":Ljava/io/BufferedReader;
} // .end local v17 # "file":Ljava/io/File;
/* .restart local v6 # "file":Ljava/io/File; */
/* .local v8, "br":Ljava/io/BufferedReader; */
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v17, v6 */
/* move-object v5, v8 */
} // .end local v6 # "file":Ljava/io/File;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v5 # "br":Ljava/io/BufferedReader; */
/* .restart local v17 # "file":Ljava/io/File; */
/* .line 85 */
} // .end local v5 # "br":Ljava/io/BufferedReader;
} // .end local v17 # "file":Ljava/io/File;
/* .restart local v6 # "file":Ljava/io/File; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v17, v6 */
/* move-object v15, v7 */
/* move-object v5, v8 */
} // .end local v6 # "file":Ljava/io/File;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v5 # "br":Ljava/io/BufferedReader; */
/* .restart local v17 # "file":Ljava/io/File; */
/* .line 88 */
} // .end local v5 # "br":Ljava/io/BufferedReader;
} // .end local v17 # "file":Ljava/io/File;
/* .restart local v6 # "file":Ljava/io/File; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catchall_3 */
/* move-exception v0 */
/* move-object/from16 v17, v6 */
} // .end local v6 # "file":Ljava/io/File;
/* .restart local v17 # "file":Ljava/io/File; */
/* .line 85 */
} // .end local v17 # "file":Ljava/io/File;
/* .restart local v6 # "file":Ljava/io/File; */
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v17, v6 */
/* move-object v15, v7 */
/* .line 86 */
} // .end local v6 # "file":Ljava/io/File;
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v17 # "file":Ljava/io/File; */
} // :goto_2
try { // :try_start_5
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "couldPersist()...exception:"; // const-string v6, "couldPersist()...exception:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v15,v5 );
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_4 */
/* .line 88 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) v1 ).closeQuitely ( v8 ); // invoke-virtual {v1, v8}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V
/* .line 89 */
/* nop */
/* .line 90 */
} // :goto_3
int v5 = 0; // const/4 v5, 0x0
/* .line 88 */
/* :catchall_4 */
/* move-exception v0 */
} // :goto_4
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) v1 ).closeQuitely ( v8 ); // invoke-virtual {v1, v8}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V
/* .line 89 */
/* throw v0 */
/* .line 53 */
} // .end local v8 # "br":Ljava/io/BufferedReader;
} // .end local v12 # "pkg":Ljava/lang/String;
} // .end local v13 # "context":Landroid/content/Context;
} // .end local v14 # "task":Lcom/android/server/wm/Task;
} // .end local v17 # "file":Ljava/io/File;
} // :cond_7
} // :goto_5
int v5 = 0; // const/4 v5, 0x0
} // .end method
public Boolean createDirectory ( com.android.server.wm.BaseAppSnapshotPersister$DirectoryResolver p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "resolver" # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver; */
/* .param p2, "userId" # I */
/* .line 126 */
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).getDirectoryQS ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getDirectoryQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;I)Ljava/io/File;
/* .line 127 */
/* .local v0, "dirQS":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
v1 = (( java.io.File ) v0 ).mkdir ( ); // invoke-virtual {v0}, Ljava/io/File;->mkdir()Z
/* if-nez v1, :cond_0 */
/* .line 128 */
final String v1 = "TaskSnapshot_PInjector"; // const-string v1, "TaskSnapshot_PInjector"
final String v2 = "Fail to create dir!"; // const-string v2, "Fail to create dir!"
android.util.Slog .e ( v1,v2 );
/* .line 129 */
int v1 = 0; // const/4 v1, 0x0
/* .line 131 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // .end method
public java.io.File getBitmapFileQS ( com.android.server.wm.BaseAppSnapshotPersister$DirectoryResolver p0, Integer p1, java.lang.String p2, Boolean p3 ) {
/* .locals 4 */
/* .param p1, "resolver" # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver; */
/* .param p2, "userId" # I */
/* .param p3, "name" # Ljava/lang/String; */
/* .param p4, "lowRes" # Z */
/* .line 154 */
/* new-instance v0, Ljava/io/File; */
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).getDirectoryQS ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getDirectoryQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;I)Ljava/io/File;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 155 */
if ( p4 != null) { // if-eqz p4, :cond_0
final String v3 = "_reduced"; // const-string v3, "_reduced"
} // :cond_0
final String v3 = ""; // const-string v3, ""
} // :goto_0
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ".jpg"; // const-string v3, ".jpg"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 154 */
} // .end method
public java.io.File getBitmapFileQS ( com.android.server.wm.BaseAppSnapshotPersister$DirectoryResolver p0, android.window.TaskSnapshot p1, Integer p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "resolver" # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver; */
/* .param p2, "snapshot" # Landroid/window/TaskSnapshot; */
/* .param p3, "userId" # I */
/* .param p4, "lowRes" # Z */
/* .line 147 */
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).checkName ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->checkName(Landroid/window/TaskSnapshot;)Ljava/lang/String;
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).getBitmapFileQS ( p1, p3, v0, p4 ); // invoke-virtual {p0, p1, p3, v0, p4}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getBitmapFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;Z)Ljava/io/File;
} // .end method
public java.io.File getDirectoryQS ( com.android.server.wm.BaseAppSnapshotPersister$DirectoryResolver p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "resolver" # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver; */
/* .param p2, "userId" # I */
/* .line 174 */
/* new-instance v0, Ljava/io/File; */
final String v2 = "qs_snapshots"; // const-string v2, "qs_snapshots"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
} // .end method
public java.io.File getProtoFileQS ( com.android.server.wm.BaseAppSnapshotPersister$DirectoryResolver p0, Integer p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "resolver" # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver; */
/* .param p2, "userId" # I */
/* .param p3, "name" # Ljava/lang/String; */
/* .line 168 */
/* new-instance v0, Ljava/io/File; */
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).getDirectoryQS ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getDirectoryQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;I)Ljava/io/File;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ".proto"; // const-string v3, ".proto"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
} // .end method
public Boolean writeProto ( com.android.server.wm.BaseAppSnapshotPersister$DirectoryResolver p0, android.window.TaskSnapshot p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "resolver" # Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver; */
/* .param p2, "snapshot" # Landroid/window/TaskSnapshot; */
/* .param p3, "userId" # I */
/* .line 104 */
int v0 = 0; // const/4 v0, 0x0
/* .line 106 */
/* .local v0, "bw":Ljava/io/BufferedWriter; */
try { // :try_start_0
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).checkName ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->checkName(Landroid/window/TaskSnapshot;)Ljava/lang/String;
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).getProtoFileQS ( p1, p3, v1 ); // invoke-virtual {p0, p1, p3, v1}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->getProtoFileQS(Lcom/android/server/wm/BaseAppSnapshotPersister$DirectoryResolver;ILjava/lang/String;)Ljava/io/File;
/* .line 107 */
/* .local v1, "file":Ljava/io/File; */
/* new-instance v2, Ljava/io/BufferedWriter; */
/* new-instance v3, Ljava/io/FileWriter; */
/* invoke-direct {v3, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v0, v2 */
/* .line 108 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
java.lang.String .valueOf ( v2,v3 );
(( java.io.BufferedWriter ) v0 ).write ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 112 */
} // .end local v1 # "file":Ljava/io/File;
/* nop */
} // :goto_0
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).closeQuitely ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V
/* .line 113 */
/* .line 112 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 109 */
/* :catch_0 */
/* move-exception v1 */
/* .line 110 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v2 = "TaskSnapshot_PInjector"; // const-string v2, "TaskSnapshot_PInjector"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Unable to open for proto write."; // const-string v4, "Unable to open for proto write."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 112 */
/* nop */
} // .end local v1 # "e":Ljava/lang/Exception;
/* .line 114 */
} // :goto_1
int v1 = 1; // const/4 v1, 0x1
/* .line 112 */
} // :goto_2
(( com.android.server.wm.TaskSnapshotPersisterInjectorImpl ) p0 ).closeQuitely ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl;->closeQuitely(Ljava/io/Closeable;)V
/* .line 113 */
/* throw v1 */
} // .end method
