.class Lcom/android/server/wm/MIUIWatermark$1;
.super Ljava/lang/Thread;
.source "MIUIWatermark.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MIUIWatermark;->getImeiInfo(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MIUIWatermark;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MIUIWatermark;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MIUIWatermark;

    .line 244
    iput-object p1, p0, Lcom/android/server/wm/MIUIWatermark$1;->this$0:Lcom/android/server/wm/MIUIWatermark;

    iput-object p2, p0, Lcom/android/server/wm/MIUIWatermark$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 248
    const/16 v0, 0xa

    .line 249
    .local v0, "time":I
    :goto_0
    const/4 v1, 0x0

    if-lez v0, :cond_2

    .line 250
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark$1;->val$context:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$smgetImeiID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 251
    .local v2, "imei":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v3, "02:00:00:00:00:00"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    .line 254
    :cond_0
    iget-object v3, p0, Lcom/android/server/wm/MIUIWatermark$1;->this$0:Lcom/android/server/wm/MIUIWatermark;

    invoke-static {v3, v2}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$msetImei(Lcom/android/server/wm/MIUIWatermark;Ljava/lang/String;)V

    .line 255
    goto :goto_2

    .line 252
    :cond_1
    :goto_1
    const-wide/16 v3, 0x7d0

    invoke-static {v3, v4}, Lcom/android/server/wm/MIUIWatermark$1;->sleep(J)V

    .line 257
    nop

    .end local v2    # "imei":Ljava/lang/String;
    add-int/lit8 v0, v0, -0x1

    .line 258
    goto :goto_0

    .line 259
    :cond_2
    :goto_2
    const-string v2, "Watermark"

    const-string v3, "Failed to get imei"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 263
    nop

    .end local v0    # "time":I
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark$1;->this$0:Lcom/android/server/wm/MIUIWatermark;

    monitor-enter v0

    .line 264
    :try_start_1
    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark$1;->this$0:Lcom/android/server/wm/MIUIWatermark;

    invoke-static {v2, v1}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$fputmImeiThreadisRuning(Lcom/android/server/wm/MIUIWatermark;Z)V

    .line 265
    monitor-exit v0

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 263
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 263
    .end local v0    # "e":Ljava/lang/InterruptedException;
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark$1;->this$0:Lcom/android/server/wm/MIUIWatermark;

    monitor-enter v0

    .line 264
    :try_start_3
    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark$1;->this$0:Lcom/android/server/wm/MIUIWatermark;

    invoke-static {v2, v1}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$fputmImeiThreadisRuning(Lcom/android/server/wm/MIUIWatermark;Z)V

    .line 265
    monitor-exit v0

    .line 266
    :goto_3
    nop

    .line 267
    return-void

    .line 265
    :catchall_2
    move-exception v1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v1

    .line 263
    :goto_4
    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark$1;->this$0:Lcom/android/server/wm/MIUIWatermark;

    monitor-enter v2

    .line 264
    :try_start_4
    iget-object v3, p0, Lcom/android/server/wm/MIUIWatermark$1;->this$0:Lcom/android/server/wm/MIUIWatermark;

    invoke-static {v3, v1}, Lcom/android/server/wm/MIUIWatermark;->-$$Nest$fputmImeiThreadisRuning(Lcom/android/server/wm/MIUIWatermark;Z)V

    .line 265
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 266
    throw v0

    .line 265
    :catchall_3
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0
.end method
