.class Lcom/android/server/wm/MiuiFreeformTrackManager$1;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiFreeformTrackManager;->bindOneTrackService()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiFreeformTrackManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiFreeformTrackManager;

    .line 199
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$1;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 202
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$1;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mFreeFormTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 203
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$1;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {p2}, Lcom/miui/analytics/ITrackBinder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fputmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;Lcom/miui/analytics/ITrackBinder;)V

    .line 204
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    const-string v0, "MiuiFreeformOneTrackManager"

    const-string v1, "BindOneTrackService Success"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    return-void

    .line 204
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .line 210
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$1;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v0}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiFreeformTrackManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/android/server/wm/MiuiFreeformTrackManager;->closeOneTrackService(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 211
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$1;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v0}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmHandler(Lcom/android/server/wm/MiuiFreeformTrackManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$1;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmBindOneTrackService(Lcom/android/server/wm/MiuiFreeformTrackManager;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 212
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$1;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v0}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmHandler(Lcom/android/server/wm/MiuiFreeformTrackManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$1;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmBindOneTrackService(Lcom/android/server/wm/MiuiFreeformTrackManager;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 213
    const-string v0, "MiuiFreeformOneTrackManager"

    const-string v1, "OneTrack Service Disconnected"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    return-void
.end method
