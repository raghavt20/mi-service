class com.android.server.wm.MiuiDragAndDropPermissionsHandler extends com.android.server.wm.DragAndDropPermissionsHandler {
	 /* .source "MiuiDragAndDropPermissionsHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener;, */
	 /* Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener; */
	 /* } */
} // .end annotation
/* # instance fields */
private com.android.server.wm.MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener mOnPermissionReleaseListener;
private com.android.server.wm.MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener mOnPermissionTakeListener;
private java.util.List mUris;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/net/Uri;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
 com.android.server.wm.MiuiDragAndDropPermissionsHandler ( ) {
/* .locals 1 */
/* .param p1, "lock" # Lcom/android/server/wm/WindowManagerGlobalLock; */
/* .param p2, "clipData" # Landroid/content/ClipData; */
/* .param p3, "sourceUid" # I */
/* .param p4, "targetPackage" # Ljava/lang/String; */
/* .param p5, "mode" # I */
/* .param p6, "sourceUserId" # I */
/* .param p7, "targetUserId" # I */
/* .line 18 */
/* invoke-direct/range {p0 ..p7}, Lcom/android/server/wm/DragAndDropPermissionsHandler;-><init>(Lcom/android/server/wm/WindowManagerGlobalLock;Landroid/content/ClipData;ILjava/lang/String;III)V */
/* .line 19 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mUris = v0;
/* .line 20 */
(( android.content.ClipData ) p2 ).collectUris ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/ClipData;->collectUris(Ljava/util/List;)V
/* .line 21 */
return;
} // .end method
/* # virtual methods */
public void release ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 33 */
/* invoke-super {p0}, Lcom/android/server/wm/DragAndDropPermissionsHandler;->release()V */
/* .line 34 */
v0 = this.mOnPermissionReleaseListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 35 */
v1 = this.mUris;
/* .line 37 */
} // :cond_0
return;
} // .end method
public void setOnPermissionReleaseListener ( com.android.server.wm.MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionReleaseListener; */
/* .line 44 */
this.mOnPermissionReleaseListener = p1;
/* .line 45 */
return;
} // .end method
public void setOnPermissionTakeListener ( com.android.server.wm.MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Lcom/android/server/wm/MiuiDragAndDropPermissionsHandler$OnPermissionTakeListener; */
/* .line 40 */
this.mOnPermissionTakeListener = p1;
/* .line 41 */
return;
} // .end method
public void take ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p1, "activityToken" # Landroid/os/IBinder; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 25 */
/* invoke-super {p0, p1}, Lcom/android/server/wm/DragAndDropPermissionsHandler;->take(Landroid/os/IBinder;)V */
/* .line 26 */
v0 = this.mOnPermissionTakeListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 27 */
v1 = this.mUris;
/* .line 29 */
} // :cond_0
return;
} // .end method
