.class public Lcom/android/server/wm/MiuiMiPerfStubImpl;
.super Ljava/lang/Object;
.source "MiuiMiPerfStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiMiPerfStub;


# static fields
.field private static final DEFAULT_COMMON_PACKAGENAME:Ljava/lang/String; = "default.common.packagename"

.field private static final PROFILE_MAX_BYTE:I = 0x100000

.field private static final TAG:Ljava/lang/String; = "MiuiMiPerfStubImpl"

.field private static final THERMAL_SCONFIG:Ljava/lang/String; = "/sys/class/thermal/thermal_message/sconfig"

.field private static mMiperfXmlMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static mThermalSconfigMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isCommon:Z

.field private isDebug:Z

.field private isRecoverWrite:Z

.field private isRelease:Z

.field private isdefault:Z

.field private lastActName:Ljava/lang/String;

.field private lastPackName:Ljava/lang/String;

.field private timer:Ljava/util/Timer;

.field private timerTask:Ljava/util/TimerTask;


# direct methods
.method static bridge synthetic -$$Nest$mmiPerfSystemBoostNotify(Lcom/android/server/wm/MiuiMiPerfStubImpl;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiMiPerfStubImpl;->miPerfSystemBoostNotify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 42
    invoke-static {}, Landroid/os/MiPerf;->miPerfGetXmlMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->mMiperfXmlMap:Ljava/util/HashMap;

    .line 43
    invoke-static {}, Landroid/os/MiPerf;->getThermalMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->mThermalSconfigMap:Ljava/util/HashMap;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z

    .line 29
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRelease:Z

    .line 30
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z

    .line 31
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isdefault:Z

    .line 32
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isDebug:Z

    .line 33
    const-string v0, " "

    iput-object v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->lastPackName:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->lastActName:Ljava/lang/String;

    .line 47
    const-string v0, "MiuiMiPerfStubImpl"

    const-string v1, "MiuiMiPerfStubImpl is Initialized!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    return-void
.end method

.method private miPerfSystemBoostNotify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "activityName"    # Ljava/lang/String;
    .param p4, "BoostScenes"    # Ljava/lang/String;

    .line 116
    const/4 v0, 0x0

    .line 117
    .local v0, "ret":I
    invoke-virtual {p0, p2, p3}, Lcom/android/server/wm/MiuiMiPerfStubImpl;->getSystemBoostMode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "mBoostMode":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z

    if-eqz v2, :cond_0

    const-string p3, "Common"

    .line 119
    :cond_0
    iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isdefault:Z

    if-eqz v2, :cond_1

    const-string p2, "default.common.packagename"

    .line 121
    :cond_1
    iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    .line 122
    iget-object v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->lastPackName:Ljava/lang/String;

    if-ne p2, v2, :cond_2

    iget-object v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->lastActName:Ljava/lang/String;

    if-eq p3, v2, :cond_3

    .line 123
    :cond_2
    const-string v2, "WRITENODE_RECOVER"

    invoke-static {p1, p2, p3, v2}, Landroid/os/MiPerf;->miPerfSystemBoostAcquire(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iput-boolean v3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z

    .line 128
    :cond_3
    iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRelease:Z

    if-eqz v2, :cond_4

    .line 129
    const-string v2, "PERFLOCK_RELEASE"

    invoke-static {p1, p2, p3, v2}, Landroid/os/MiPerf;->miPerfSystemBoostAcquire(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iput-boolean v3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRelease:Z

    .line 133
    :cond_4
    iget-object v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->lastPackName:Ljava/lang/String;

    if-ne p2, v2, :cond_5

    iget-object v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->lastActName:Ljava/lang/String;

    if-ne p3, v2, :cond_5

    .line 134
    const-string v1, "BOOSTMODE_NULL"

    .line 136
    :cond_5
    iput-object p2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->lastPackName:Ljava/lang/String;

    .line 137
    iput-object p3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->lastActName:Ljava/lang/String;

    .line 138
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v4, 0x1

    sparse-switch v2, :sswitch_data_0

    :cond_6
    goto :goto_0

    :sswitch_0
    const-string v2, "PERFLOCK_ACQUIRE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v3, v4

    goto :goto_1

    :sswitch_1
    const-string v2, "WRITENODE_ACQUIRE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v3, 0x2

    goto :goto_1

    :sswitch_2
    const-string v2, "BOOSTMODE_NULL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    goto :goto_1

    :sswitch_3
    const-string v2, "MULTIPLEMODE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v3, 0x3

    goto :goto_1

    :goto_0
    const/4 v3, -0x1

    :goto_1
    const-string v2, "MiuiMiPerfStubImpl"

    packed-switch v3, :pswitch_data_0

    goto :goto_2

    .line 150
    :pswitch_0
    iput-boolean v4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z

    .line 151
    goto :goto_2

    .line 147
    :pswitch_1
    iput-boolean v4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRecoverWrite:Z

    .line 148
    goto :goto_2

    .line 144
    :pswitch_2
    iput-boolean v4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isRelease:Z

    .line 145
    goto :goto_2

    .line 140
    :pswitch_3
    iget-boolean v3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isDebug:Z

    if-eqz v3, :cond_7

    .line 141
    const-string v3, "miPerfSystemBoostNotify: no MiPerfBoost!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_7
    return-void

    .line 155
    :goto_2
    invoke-static {p1, p2, p3, v1}, Landroid/os/MiPerf;->miPerfSystemBoostAcquire(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "miPerfSystemBoostNotify: return = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", System BoostScenes = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", BoostMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", packagename = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", activityname = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6696c04d -> :sswitch_3
        -0x2cfa1580 -> :sswitch_2
        0x46b9d0b8 -> :sswitch_1
        0x53234b0b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static readFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "fileName"    # Ljava/lang/String;

    .line 51
    const-string v0, "readFromFile, finally fis.close() IOException e:"

    const-string v1, "MiuiMiPerfStubImpl"

    const/4 v2, 0x0

    .line 52
    .local v2, "jString":Ljava/lang/String;
    const/4 v3, 0x0

    .line 53
    .local v3, "reader":Ljava/io/BufferedReader;
    const/16 v4, 0x8

    .line 54
    .local v4, "num":I
    new-array v5, v4, [C

    .line 56
    .local v5, "buffer":[C
    :try_start_0
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/FileReader;

    invoke-direct {v7, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v3, v6

    .line 57
    invoke-virtual {v3, v5}, Ljava/io/BufferedReader;->read([C)I

    .line 58
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    nop

    .line 65
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 66
    :catch_0
    move-exception v6

    .line 67
    .local v6, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const/4 v2, 0x0

    .line 69
    .end local v6    # "e":Ljava/io/IOException;
    :goto_1
    goto :goto_2

    .line 63
    :catchall_0
    move-exception v6

    goto :goto_4

    .line 59
    :catch_1
    move-exception v6

    .line 60
    .restart local v6    # "e":Ljava/io/IOException;
    :try_start_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "readFile, IOException e:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 61
    const/4 v2, 0x0

    .line 63
    .end local v6    # "e":Ljava/io/IOException;
    if-eqz v3, :cond_0

    .line 65
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 66
    :catch_2
    move-exception v6

    .line 67
    .restart local v6    # "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0

    .line 72
    .end local v6    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    const/4 v0, 0x0

    aget-char v1, v5, v0

    const/16 v6, 0x2d

    if-ne v1, v6, :cond_1

    .line 73
    const-string v0, "-1"

    return-object v0

    .line 75
    :cond_1
    aget-char v0, v5, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    .line 76
    .end local v2    # "jString":Ljava/lang/String;
    .local v0, "jString":Ljava/lang/String;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_3
    if-ge v1, v4, :cond_2

    .line 77
    aget-char v2, v5, v1

    const/16 v6, 0x30

    if-lt v2, v6, :cond_2

    aget-char v2, v5, v1

    const/16 v6, 0x39

    if-gt v2, v6, :cond_2

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-char v6, v5, v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 76
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 83
    .end local v1    # "i":I
    :cond_2
    return-object v0

    .line 63
    .end local v0    # "jString":Ljava/lang/String;
    .restart local v2    # "jString":Ljava/lang/String;
    :goto_4
    if-eqz v3, :cond_3

    .line 65
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 69
    goto :goto_5

    .line 66
    :catch_3
    move-exception v7

    .line 67
    .local v7, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const/4 v2, 0x0

    .line 71
    .end local v7    # "e":Ljava/io/IOException;
    :cond_3
    :goto_5
    throw v6
.end method


# virtual methods
.method public getSystemBoostMode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "activityName"    # Ljava/lang/String;

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z

    .line 88
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isdefault:Z

    .line 89
    sget-object v0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->mMiperfXmlMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "MiuiMiPerfStubImpl"

    const/4 v2, 0x1

    const-string v3, "Common"

    if-eqz v0, :cond_3

    .line 90
    sget-object v0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->mMiperfXmlMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 91
    .local v0, "act_map":Ljava/lang/Object;
    move-object v4, v0

    check-cast v4, Ljava/util/HashMap;

    .line 92
    .local v4, "map_tmp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v4, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 93
    iget-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isDebug:Z

    if-eqz v2, :cond_0

    .line 94
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Match BoostMode successfully, BoostMode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_0
    invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    return-object v1

    .line 96
    :cond_1
    invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 97
    iput-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z

    .line 98
    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    return-object v1

    .line 100
    .end local v0    # "act_map":Ljava/lang/Object;
    .end local v4    # "map_tmp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    goto :goto_0

    .line 101
    :cond_3
    const-string v0, "/sys/class/thermal/thermal_message/sconfig"

    invoke-static {v0}, Lcom/android/server/wm/MiuiMiPerfStubImpl;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "thermalSconfig":Ljava/lang/String;
    sget-object v4, Lcom/android/server/wm/MiuiMiPerfStubImpl;->mThermalSconfigMap:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    sget-object v4, Lcom/android/server/wm/MiuiMiPerfStubImpl;->mMiperfXmlMap:Ljava/util/HashMap;

    const-string v5, "default.common.packagename"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 103
    iput-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isdefault:Z

    .line 104
    iput-boolean v2, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isCommon:Z

    .line 105
    sget-object v1, Lcom/android/server/wm/MiuiMiPerfStubImpl;->mMiperfXmlMap:Ljava/util/HashMap;

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 106
    .local v1, "act_map":Ljava/lang/Object;
    move-object v2, v1

    check-cast v2, Ljava/util/HashMap;

    .line 107
    .local v2, "map_tmp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    return-object v3

    .line 110
    .end local v0    # "thermalSconfig":Ljava/lang/String;
    .end local v1    # "act_map":Ljava/lang/Object;
    .end local v2    # "map_tmp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    :goto_0
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->isDebug:Z

    if-eqz v0, :cond_5

    .line 111
    const-string v0, "Match BoostMode failed, there is no MiPerfBoost!"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_5
    const-string v0, "BOOSTMODE_NULL"

    return-object v0
.end method

.method public onAfterActivityResumed(Lcom/android/server/wm/ActivityRecord;)V
    .locals 7
    .param p1, "resumedActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 163
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    if-nez v0, :cond_0

    goto :goto_0

    .line 166
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v0

    .line 167
    .local v0, "pid":I
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 168
    .local v1, "activityName":Ljava/lang/String;
    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 169
    .local v2, "packageName":Ljava/lang/String;
    new-instance v3, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;

    invoke-direct {v3, p0, v0, v2, v1}, Lcom/android/server/wm/MiuiMiPerfStubImpl$1;-><init>(Lcom/android/server/wm/MiuiMiPerfStubImpl;ILjava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->timerTask:Ljava/util/TimerTask;

    .line 184
    new-instance v3, Ljava/util/Timer;

    invoke-direct {v3}, Ljava/util/Timer;-><init>()V

    iput-object v3, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->timer:Ljava/util/Timer;

    .line 185
    iget-object v4, p0, Lcom/android/server/wm/MiuiMiPerfStubImpl;->timerTask:Ljava/util/TimerTask;

    const-wide/16 v5, 0x64

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 186
    return-void

    .line 164
    .end local v0    # "pid":I
    .end local v1    # "activityName":Ljava/lang/String;
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void
.end method
