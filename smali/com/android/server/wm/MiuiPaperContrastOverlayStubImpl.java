class com.android.server.wm.MiuiPaperContrastOverlayStubImpl extends com.android.server.wm.MiuiPaperContrastOverlayStub {
	 /* .source "MiuiPaperContrastOverlayStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.MiuiPaperContrastOverlayStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$UserSwitchReceivere; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
android.content.Context mContext;
private Boolean mFoldDeviceReady;
com.android.server.wm.MiuiPaperContrastOverlay mMiuiPaperSurface;
private final android.net.Uri mPaperModeEnableUri;
private final android.net.Uri mPaperModeTypeUri;
private final android.net.Uri mSecurityModeGbUri;
private final android.net.Uri mSecurityModeVtbUri;
private final android.net.Uri mTextureEyeCareLevelUri;
com.android.server.wm.WindowManagerService mWmService;
/* # direct methods */
public static void $r8$lambda$S1KpKV9QLNHuolHZsT0VuWwW2ZM ( com.android.server.wm.MiuiPaperContrastOverlayStubImpl p0, Boolean p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->lambda$updateTextureEyeCareLevel$0(Z)V */
return;
} // .end method
static android.net.Uri -$$Nest$fgetmPaperModeEnableUri ( com.android.server.wm.MiuiPaperContrastOverlayStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPaperModeEnableUri;
} // .end method
static android.net.Uri -$$Nest$fgetmPaperModeTypeUri ( com.android.server.wm.MiuiPaperContrastOverlayStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPaperModeTypeUri;
} // .end method
static android.net.Uri -$$Nest$fgetmSecurityModeGbUri ( com.android.server.wm.MiuiPaperContrastOverlayStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSecurityModeGbUri;
} // .end method
static android.net.Uri -$$Nest$fgetmSecurityModeVtbUri ( com.android.server.wm.MiuiPaperContrastOverlayStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSecurityModeVtbUri;
} // .end method
static android.net.Uri -$$Nest$fgetmTextureEyeCareLevelUri ( com.android.server.wm.MiuiPaperContrastOverlayStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTextureEyeCareLevelUri;
} // .end method
static void -$$Nest$fputmFoldDeviceReady ( com.android.server.wm.MiuiPaperContrastOverlayStubImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mFoldDeviceReady:Z */
return;
} // .end method
static Boolean -$$Nest$mgetSecurityCenterStatus ( com.android.server.wm.MiuiPaperContrastOverlayStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->getSecurityCenterStatus()Z */
} // .end method
 com.android.server.wm.MiuiPaperContrastOverlayStubImpl ( ) {
/* .locals 1 */
/* .line 22 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStub;-><init>()V */
/* .line 33 */
/* nop */
/* .line 34 */
/* const-string/jumbo v0, "screen_texture_eyecare_level" */
android.provider.Settings$System .getUriFor ( v0 );
this.mTextureEyeCareLevelUri = v0;
/* .line 35 */
/* nop */
/* .line 36 */
final String v0 = "screen_mode_type"; // const-string v0, "screen_mode_type"
android.provider.Settings$System .getUriFor ( v0 );
this.mPaperModeTypeUri = v0;
/* .line 37 */
/* nop */
/* .line 38 */
/* const-string/jumbo v0, "screen_paper_mode_enabled" */
android.provider.Settings$System .getUriFor ( v0 );
this.mPaperModeEnableUri = v0;
/* .line 39 */
/* const-string/jumbo v0, "vtb_boosting" */
android.provider.Settings$Secure .getUriFor ( v0 );
this.mSecurityModeVtbUri = v0;
/* .line 40 */
final String v0 = "gb_boosting"; // const-string v0, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v0 );
this.mSecurityModeGbUri = v0;
return;
} // .end method
private Boolean getSecurityCenterStatus ( ) {
/* .locals 4 */
/* .line 130 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,v3 );
/* if-nez v0, :cond_0 */
v0 = this.mContext;
/* .line 132 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "vtb_boosting" */
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,v3 );
/* if-nez v0, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* nop */
/* .line 130 */
} // :goto_0
} // .end method
private void lambda$updateTextureEyeCareLevel$0 ( Boolean p0 ) { //synthethic
/* .locals 7 */
/* .param p1, "eyecareEnable" # Z */
/* .line 59 */
v0 = this.mWmService;
v0 = this.mWindowMap;
/* monitor-enter v0 */
/* .line 60 */
try { // :try_start_0
v1 = this.mWmService;
(( com.android.server.wm.WindowManagerService ) v1 ).openSurfaceTransaction ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 63 */
try { // :try_start_1
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "screen_mode_type"; // const-string v2, "screen_mode_type"
int v3 = -2; // const/4 v3, -0x2
int v4 = 0; // const/4 v4, 0x0
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v4,v3 );
/* .line 65 */
/* .local v1, "paperModeType":I */
v2 = this.mContext;
/* .line 66 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v5, "screen_paper_mode_enabled" */
/* .line 65 */
v2 = android.provider.MiuiSettings$System .getBooleanForUser ( v2,v5,v4,v3 );
/* .line 69 */
/* .local v2, "paperModeEnable":Z */
final String v3 = "MiuiPaperContrastOverlayStubImpl"; // const-string v3, "MiuiPaperContrastOverlayStubImpl"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Update paper-mode, param is "; // const-string v6, "Update paper-mode, param is "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v5 );
/* .line 70 */
if ( p1 != null) { // if-eqz p1, :cond_3
	 if ( v2 != null) { // if-eqz v2, :cond_3
		 int v3 = 1; // const/4 v3, 0x1
		 /* if-eq v1, v3, :cond_0 */
		 int v5 = 3; // const/4 v5, 0x3
		 /* if-ne v1, v5, :cond_3 */
		 /* .line 72 */
	 } // :cond_0
	 v4 = this.mMiuiPaperSurface;
	 /* if-nez v4, :cond_1 */
	 /* .line 73 */
	 v4 = this.mWmService;
	 v5 = this.mContext;
	 com.android.server.wm.MiuiPaperContrastOverlay .getInstance ( v4,v5 );
	 this.mMiuiPaperSurface = v4;
	 /* .line 75 */
} // :cond_1
/* sget-boolean v4, Lcom/android/server/wm/MiuiPaperContrastOverlay;->IS_FOLDABLE_DEVICE:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
	 /* iget-boolean v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->mFoldDeviceReady:Z */
	 if ( v4 != null) { // if-eqz v4, :cond_2
		 /* .line 76 */
		 v4 = this.mMiuiPaperSurface;
		 (( com.android.server.wm.MiuiPaperContrastOverlay ) v4 ).changeDeviceReady ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->changeDeviceReady()V
		 /* .line 78 */
	 } // :cond_2
	 v4 = this.mMiuiPaperSurface;
	 (( com.android.server.wm.MiuiPaperContrastOverlay ) v4 ).changeShowLayerStatus ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->changeShowLayerStatus(Z)V
	 /* .line 79 */
	 final String v3 = "MiuiPaperContrastOverlayStubImpl"; // const-string v3, "MiuiPaperContrastOverlayStubImpl"
	 final String v4 = "Show paper-mode surface."; // const-string v4, "Show paper-mode surface."
	 android.util.Slog .d ( v3,v4 );
	 /* .line 80 */
	 v3 = this.mMiuiPaperSurface;
	 (( com.android.server.wm.MiuiPaperContrastOverlay ) v3 ).showPaperModeSurface ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->showPaperModeSurface()V
	 /* .line 82 */
} // :cond_3
v3 = this.mMiuiPaperSurface;
if ( v3 != null) { // if-eqz v3, :cond_4
	 /* .line 83 */
	 (( com.android.server.wm.MiuiPaperContrastOverlay ) v3 ).changeShowLayerStatus ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->changeShowLayerStatus(Z)V
	 /* .line 84 */
	 v3 = this.mMiuiPaperSurface;
	 (( com.android.server.wm.MiuiPaperContrastOverlay ) v3 ).hidePaperModeSurface ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->hidePaperModeSurface()V
	 /* .line 86 */
} // :cond_4
int v3 = 0; // const/4 v3, 0x0
this.mMiuiPaperSurface = v3;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 89 */
} // .end local v1 # "paperModeType":I
} // .end local v2 # "paperModeEnable":Z
} // :goto_0
try { // :try_start_2
v1 = this.mWmService;
/* const-string/jumbo v2, "updateTextureEyeCareLevel" */
(( com.android.server.wm.WindowManagerService ) v1 ).closeSurfaceTransaction ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* .line 90 */
/* nop */
/* .line 91 */
/* monitor-exit v0 */
/* .line 92 */
return;
/* .line 89 */
/* :catchall_0 */
/* move-exception v1 */
v2 = this.mWmService;
/* const-string/jumbo v3, "updateTextureEyeCareLevel" */
(( com.android.server.wm.WindowManagerService ) v2 ).closeSurfaceTransaction ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* .line 90 */
/* nop */
} // .end local p0 # "this":Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;
} // .end local p1 # "eyecareEnable":Z
/* throw v1 */
/* .line 91 */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl; */
/* .restart local p1 # "eyecareEnable":Z */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
/* # virtual methods */
public android.view.SurfaceControl getMiuiPaperSurfaceControl ( ) {
/* .locals 1 */
/* .line 145 */
v0 = this.mMiuiPaperSurface;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 146 */
(( com.android.server.wm.MiuiPaperContrastOverlay ) v0 ).getSurfaceControl ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->getSurfaceControl()Landroid/view/SurfaceControl;
/* .line 148 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void init ( com.android.server.wm.WindowManagerService p0, android.content.Context p1 ) {
/* .locals 1 */
/* .param p1, "wms" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 44 */
this.mContext = p2;
/* .line 45 */
this.mWmService = p1;
/* .line 46 */
(( com.android.server.wm.MiuiPaperContrastOverlayStubImpl ) p0 ).registerObserver ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->registerObserver(Landroid/content/Context;)V
/* .line 48 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->getSecurityCenterStatus()Z */
(( com.android.server.wm.MiuiPaperContrastOverlayStubImpl ) p0 ).updateTextureEyeCareLevel ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->updateTextureEyeCareLevel(Z)V
/* .line 49 */
return;
} // .end method
public void registerObserver ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 96 */
/* new-instance v0, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$1;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Landroid/os/Handler;)V */
/* .line 112 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v3 = this.mTextureEyeCareLevelUri;
int v4 = 0; // const/4 v4, 0x0
int v5 = -1; // const/4 v5, -0x1
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v4, v0, v5 ); // invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 114 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v3 = this.mPaperModeTypeUri;
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v4, v0, v5 ); // invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 116 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v3 = this.mPaperModeEnableUri;
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v4, v0, v5 ); // invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 118 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v3 = this.mSecurityModeVtbUri;
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v4, v0, v5 ); // invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 120 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v3 = this.mSecurityModeGbUri;
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v4, v0, v5 ); // invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 123 */
(( android.database.ContentObserver ) v0 ).onChange ( v4 ); // invoke-virtual {v0, v4}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 125 */
v2 = this.mContext;
/* new-instance v3, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$UserSwitchReceivere; */
/* invoke-direct {v3, p0, v1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$UserSwitchReceivere;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$UserSwitchReceivere-IA;)V */
/* new-instance v1, Landroid/content/IntentFilter; */
final String v4 = "android.intent.action.USER_SWITCHED"; // const-string v4, "android.intent.action.USER_SWITCHED"
/* invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v2 ).registerReceiver ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 127 */
return;
} // .end method
public void updateTextureEyeCareLevel ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "eyecareEnable" # Z */
/* .line 58 */
v0 = this.mWmService;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;Z)V */
(( com.android.server.wm.WindowManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 93 */
return;
} // .end method
public void updateTextureEyeCareWhenAodShow ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "aodShowing" # Z */
/* .line 53 */
/* if-nez p1, :cond_0 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->getSecurityCenterStatus()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
(( com.android.server.wm.MiuiPaperContrastOverlayStubImpl ) p0 ).updateTextureEyeCareLevel ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiPaperContrastOverlayStubImpl;->updateTextureEyeCareLevel(Z)V
/* .line 54 */
return;
} // .end method
