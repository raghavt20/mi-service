.class public Lcom/android/server/wm/MiuiMultiWindowServiceImpl;
.super Ljava/lang/Object;
.source "MiuiMultiWindowServiceImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiMultiWindowServiceStub;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

.field mNavigationMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    const-class v0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mNavigationMode:I

    return-void
.end method

.method static synthetic lambda$isEventXInDotRegion$0(Lcom/android/server/wm/Task;)Z
    .locals 1
    .param p0, "t"    # Lcom/android/server/wm/Task;

    .line 43
    iget-boolean v0, p0, Lcom/android/server/wm/Task;->mSoScRoot:Z

    return v0
.end method

.method static synthetic lambda$isEventXInDotRegion$1(Lcom/android/server/wm/Task;)Z
    .locals 2
    .param p0, "t"    # Lcom/android/server/wm/Task;

    .line 75
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->hasDot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/server/wm/Task;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 75
    :goto_0
    return v1
.end method


# virtual methods
.method public init(Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 22
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 23
    return-void
.end method

.method public isEventXInDotRegion(Lcom/android/server/wm/DisplayContent;F)Z
    .locals 12
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "eventX"    # F

    .line 32
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 33
    return v0

    .line 36
    :cond_0
    const/high16 v1, 0x42780000    # 62.0f

    invoke-static {v1}, Landroid/util/MiuiMultiWindowUtils;->applyDip2Px(F)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 37
    .local v1, "halfDotWidth":F
    iget-object v3, p1, Lcom/android/server/wm/DisplayContent;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v3

    .line 39
    :try_start_0
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScModeActivated()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_9

    .line 40
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScMinimizedModeActivated()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 41
    monitor-exit v3

    return v0

    .line 43
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v4

    new-instance v6, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v6}, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {v4, v6}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;

    move-result-object v4

    .line 44
    .local v4, "soscRoot":Lcom/android/server/wm/Task;
    if-eqz v4, :cond_8

    invoke-virtual {v4}, Lcom/android/server/wm/Task;->getChildCount()I

    move-result v6

    const/4 v7, 0x2

    if-ge v6, v7, :cond_2

    goto/16 :goto_2

    .line 47
    :cond_2
    invoke-virtual {v4, v0}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 48
    .local v6, "splitBounds0":Landroid/graphics/Rect;
    invoke-virtual {v4, v5}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/server/wm/WindowContainer;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    .line 49
    .local v7, "splitBounds1":Landroid/graphics/Rect;
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    if-ne v8, v9, :cond_5

    .line 50
    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v7, Landroid/graphics/Rect;->top:I

    if-ge v8, v9, :cond_3

    .line 51
    invoke-virtual {v4, v0}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v8

    goto :goto_0

    :cond_3
    invoke-virtual {v4, v5}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v8

    :goto_0
    invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->getTopMostTask()Lcom/android/server/wm/Task;

    move-result-object v8

    .line 52
    .local v8, "topSplitTask":Lcom/android/server/wm/Task;
    iget v9, v6, Landroid/graphics/Rect;->left:I

    iget v10, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    div-float/2addr v9, v2

    .line 53
    .local v9, "centerX":F
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Lcom/android/server/wm/Task;->hasDot()Z

    move-result v2

    if-eqz v2, :cond_4

    sub-float v2, v9, v1

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_4

    add-float v2, v9, v1

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_4

    .line 55
    sget-object v0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Event x is in dot region, topSplitTask="

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    monitor-exit v3

    return v5

    .line 58
    .end local v8    # "topSplitTask":Lcom/android/server/wm/Task;
    .end local v9    # "centerX":F
    :cond_4
    goto/16 :goto_1

    .line 60
    :cond_5
    invoke-virtual {v4, v0}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->getTopMostTask()Lcom/android/server/wm/Task;

    move-result-object v8

    move-object v9, v8

    .local v9, "t0":Lcom/android/server/wm/Task;
    if-eqz v8, :cond_6

    invoke-virtual {v9}, Lcom/android/server/wm/Task;->hasDot()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 61
    iget v8, v6, Landroid/graphics/Rect;->left:I

    iget v10, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v10

    int-to-float v8, v8

    div-float/2addr v8, v2

    .line 62
    .local v8, "centerX":F
    sub-float v10, v8, v1

    cmpl-float v10, p2, v10

    if-ltz v10, :cond_6

    add-float v10, v8, v1

    cmpg-float v10, p2, v10

    if-gtz v10, :cond_6

    .line 63
    sget-object v0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Event x is in dot region, t0="

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    monitor-exit v3

    return v5

    .line 67
    .end local v8    # "centerX":F
    :cond_6
    invoke-virtual {v4, v5}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->getTopMostTask()Lcom/android/server/wm/Task;

    move-result-object v8

    move-object v10, v8

    .local v10, "t1":Lcom/android/server/wm/Task;
    if-eqz v8, :cond_7

    invoke-virtual {v10}, Lcom/android/server/wm/Task;->hasDot()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 68
    iget v8, v7, Landroid/graphics/Rect;->left:I

    iget v11, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v11

    int-to-float v8, v8

    div-float/2addr v8, v2

    .line 69
    .restart local v8    # "centerX":F
    sub-float v2, v8, v1

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_7

    add-float v2, v8, v1

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_7

    .line 70
    sget-object v0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Event x is in dot region, t1="

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    monitor-exit v3

    return v5

    .line 75
    .end local v4    # "soscRoot":Lcom/android/server/wm/Task;
    .end local v6    # "splitBounds0":Landroid/graphics/Rect;
    .end local v7    # "splitBounds1":Landroid/graphics/Rect;
    .end local v8    # "centerX":F
    .end local v9    # "t0":Lcom/android/server/wm/Task;
    .end local v10    # "t1":Lcom/android/server/wm/Task;
    :cond_7
    :goto_1
    goto :goto_3

    .line 45
    .restart local v4    # "soscRoot":Lcom/android/server/wm/Task;
    :cond_8
    :goto_2
    monitor-exit v3

    return v0

    .line 75
    .end local v4    # "soscRoot":Lcom/android/server/wm/Task;
    :cond_9
    new-instance v4, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$$ExternalSyntheticLambda1;

    invoke-direct {v4}, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-virtual {p1, v4}, Lcom/android/server/wm/DisplayContent;->getRootTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;

    move-result-object v4

    move-object v6, v4

    .local v6, "fullscreenTask":Lcom/android/server/wm/Task;
    if-eqz v4, :cond_a

    .line 77
    invoke-virtual {v6}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    .line 78
    .local v4, "taskBounds":Landroid/graphics/Rect;
    iget v7, v4, Landroid/graphics/Rect;->left:I

    iget v8, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v8

    int-to-float v7, v7

    div-float/2addr v7, v2

    .line 79
    .local v7, "centerX":F
    sub-float v2, v7, v1

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_a

    add-float v2, v7, v1

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_a

    .line 80
    sget-object v0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Event x is in dot region, fullscreenTask="

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    monitor-exit v3

    return v5

    .line 84
    .end local v4    # "taskBounds":Landroid/graphics/Rect;
    .end local v6    # "fullscreenTask":Lcom/android/server/wm/Task;
    .end local v7    # "centerX":F
    :cond_a
    :goto_3
    monitor-exit v3

    return v0

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isMiuiMultiWinChangeSupport()Z
    .locals 1

    .line 27
    sget-boolean v0, Landroid/util/MiuiMultiWindowUtils;->MULTIWIN_SWITCH_ENABLED:Z

    return v0
.end method

.method public isSupportSoScMode(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 89
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 90
    return v1

    .line 93
    :cond_0
    invoke-static {}, Landroid/util/MiuiMultiWindowUtils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/util/MiuiMultiWindowUtils;->isFoldInnerScreen(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 94
    return v1

    .line 97
    :cond_1
    iget v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mNavigationMode:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 98
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, -0x2

    const-string v4, "navigation_mode"

    invoke-static {v0, v4, v1, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mNavigationMode:I

    .line 101
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 102
    invoke-static {v4}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$1;

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    invoke-direct {v4, p0, v5}, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$1;-><init>(Lcom/android/server/wm/MiuiMultiWindowServiceImpl;Landroid/os/Handler;)V

    .line 101
    invoke-virtual {v0, v3, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 112
    :cond_2
    iget v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mNavigationMode:I

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method
