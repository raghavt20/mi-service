public class com.android.server.wm.MiuiActivityControllerImpl implements com.android.server.wm.MiuiActivityController {
	 /* .source "MiuiActivityControllerImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiActivityControllerImpl$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_DC_DISPLAYID;
private static final Boolean DEBUG_MESSAGES;
static final com.android.server.wm.MiuiActivityControllerImpl INSTANCE;
private static final Boolean MIUI_ACTIVITY_EMBEDDING_ENABLED;
private static final java.lang.String PREFIX_TAG;
private static final java.lang.String SUB_SETTINGS_ACT;
private static final java.lang.String SUB_SETTINGS_FRAGMENT_NAME;
private static final java.lang.String TAG;
/* # instance fields */
private final android.os.RemoteCallbackList mActivityObservers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Landroid/app/IMiuiActivityObserver;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.wm.MiuiActivityControllerImpl$H mH;
private final android.content.Intent mSendIntent;
/* # direct methods */
static android.os.RemoteCallbackList -$$Nest$fgetmActivityObservers ( com.android.server.wm.MiuiActivityControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mActivityObservers;
} // .end method
static android.content.Intent -$$Nest$fgetmSendIntent ( com.android.server.wm.MiuiActivityControllerImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSendIntent;
} // .end method
static void -$$Nest$mhandleSpecialExtras ( com.android.server.wm.MiuiActivityControllerImpl p0, android.content.Intent p1, com.android.server.wm.ActivityRecord p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiActivityControllerImpl;->handleSpecialExtras(Landroid/content/Intent;Lcom/android/server/wm/ActivityRecord;)V */
return;
} // .end method
static Boolean -$$Nest$sfgetDEBUG_MESSAGES ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->DEBUG_MESSAGES:Z */
} // .end method
static Boolean -$$Nest$sfgetMIUI_ACTIVITY_EMBEDDING_ENABLED ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->MIUI_ACTIVITY_EMBEDDING_ENABLED:Z */
} // .end method
static com.android.server.wm.MiuiActivityControllerImpl ( ) {
/* .locals 2 */
/* .line 27 */
final String v0 = "debug.miui.activity.log"; // const-string v0, "debug.miui.activity.log"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.MiuiActivityControllerImpl.DEBUG_MESSAGES = (v0!= 0);
/* .line 29 */
final String v0 = "ro.config.miui_activity_embedding_enable"; // const-string v0, "ro.config.miui_activity_embedding_enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.MiuiActivityControllerImpl.MIUI_ACTIVITY_EMBEDDING_ENABLED = (v0!= 0);
/* .line 34 */
/* new-instance v0, Lcom/android/server/wm/MiuiActivityControllerImpl; */
/* invoke-direct {v0}, Lcom/android/server/wm/MiuiActivityControllerImpl;-><init>()V */
return;
} // .end method
private com.android.server.wm.MiuiActivityControllerImpl ( ) {
/* .locals 3 */
/* .line 193 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 36 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
this.mSendIntent = v0;
/* .line 194 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mActivityObservers = v0;
/* .line 195 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "MiuiActivityController"; // const-string v1, "MiuiActivityController"
int v2 = -2; // const/4 v2, -0x2
/* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
/* .line 196 */
/* .local v0, "handlerThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 197 */
/* new-instance v1, Lcom/android/server/wm/MiuiActivityControllerImpl$H; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;-><init>(Lcom/android/server/wm/MiuiActivityControllerImpl;Landroid/os/Looper;)V */
this.mH = v1;
/* .line 198 */
return;
} // .end method
private void handleSpecialExtras ( android.content.Intent p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 4 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 203 */
int v0 = 0; // const/4 v0, 0x0
/* .line 204 */
/* .local v0, "isSubSettings":Z */
final String v1 = ":settings:show_fragment"; // const-string v1, ":settings:show_fragment"
if ( p2 != null) { // if-eqz p2, :cond_0
try { // :try_start_0
	 final String v2 = "com.android.settings/.SubSettings"; // const-string v2, "com.android.settings/.SubSettings"
	 v3 = this.shortComponentName;
	 v2 = 	 (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 v2 = this.intent;
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 205 */
			 v2 = this.intent;
			 (( android.content.Intent ) v2 ).getStringExtra ( v1 ); // invoke-virtual {v2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
			 /* .line 206 */
			 /* .local v2, "fragment":Ljava/lang/String; */
			 v3 = 			 android.text.TextUtils .isEmpty ( v2 );
			 /* if-nez v3, :cond_0 */
			 /* .line 207 */
			 (( android.content.Intent ) p1 ).putExtra ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 208 */
			 int v0 = 1; // const/4 v0, 0x1
			 /* .line 214 */
		 } // .end local v0 # "isSubSettings":Z
	 } // .end local v2 # "fragment":Ljava/lang/String;
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 211 */
	 /* .restart local v0 # "isSubSettings":Z */
} // :cond_0
} // :goto_0
/* if-nez v0, :cond_1 */
/* .line 212 */
(( android.content.Intent ) p1 ).removeExtra ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 215 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_1
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 216 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_2
/* nop */
/* .line 218 */
} // :goto_3
if ( p2 != null) { // if-eqz p2, :cond_2
v0 = this.intent;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 219 */
v0 = this.intent;
(( android.content.Intent ) v0 ).getSender ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getSender()Ljava/lang/String;
(( android.content.Intent ) p1 ).setSender ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setSender(Ljava/lang/String;)V
/* .line 221 */
} // :cond_2
return;
} // .end method
public static void logMessage ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p0, "prefix" # Ljava/lang/String; */
/* .param p1, "msg" # Ljava/lang/String; */
/* .line 290 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiActivityControllerImpl;->DEBUG_MESSAGES:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiActivityController"; // const-string v1, "MiuiActivityController"
android.util.Slog .d ( v1,v0 );
/* .line 291 */
} // :cond_0
return;
} // .end method
private void sendMessage ( Integer p0, java.lang.Object p1 ) {
/* .locals 6 */
/* .param p1, "what" # I */
/* .param p2, "obj" # Ljava/lang/Object; */
/* .line 259 */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move v1, p1 */
/* move-object v2, p2 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;IIZ)V */
/* .line 260 */
return;
} // .end method
private void sendMessage ( Integer p0, java.lang.Object p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "what" # I */
/* .param p2, "obj" # Ljava/lang/Object; */
/* .param p3, "arg1" # I */
/* .line 263 */
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move v1, p1 */
/* move-object v2, p2 */
/* move v3, p3 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;IIZ)V */
/* .line 264 */
return;
} // .end method
private void sendMessage ( Integer p0, java.lang.Object p1, Integer p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "what" # I */
/* .param p2, "obj" # Ljava/lang/Object; */
/* .param p3, "arg1" # I */
/* .param p4, "arg2" # I */
/* .line 267 */
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move v1, p1 */
/* move-object v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;IIZ)V */
/* .line 268 */
return;
} // .end method
private void sendMessage ( Integer p0, java.lang.Object p1, Integer p2, Integer p3, Boolean p4 ) {
/* .locals 3 */
/* .param p1, "what" # I */
/* .param p2, "obj" # Ljava/lang/Object; */
/* .param p3, "arg1" # I */
/* .param p4, "arg2" # I */
/* .param p5, "async" # Z */
/* .line 271 */
v0 = this.mActivityObservers;
v0 = (( android.os.RemoteCallbackList ) v0 ).getRegisteredCallbackCount ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I
/* .line 272 */
/* .local v0, "size":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SendMessage "; // const-string v2, "SendMessage "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mH;
/* .line 273 */
(( com.android.server.wm.MiuiActivityControllerImpl$H ) v2 ).codeToString ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->codeToString(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ": "; // const-string v2, ": "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " / "; // const-string v2, " / "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " observer size: "; // const-string v2, " observer size: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 272 */
final String v2 = "MiuiLog-ActivityObserver:"; // const-string v2, "MiuiLog-ActivityObserver:"
com.android.server.wm.MiuiActivityControllerImpl .logMessage ( v2,v1 );
/* .line 275 */
/* if-gtz v0, :cond_0 */
/* .line 276 */
return;
/* .line 278 */
} // :cond_0
android.os.Message .obtain ( );
/* .line 279 */
/* .local v1, "msg":Landroid/os/Message; */
/* iput p1, v1, Landroid/os/Message;->what:I */
/* .line 280 */
this.obj = p2;
/* .line 281 */
/* iput p3, v1, Landroid/os/Message;->arg1:I */
/* .line 282 */
/* iput p4, v1, Landroid/os/Message;->arg2:I */
/* .line 283 */
if ( p5 != null) { // if-eqz p5, :cond_1
/* .line 284 */
int v2 = 1; // const/4 v2, 0x1
(( android.os.Message ) v1 ).setAsynchronous ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Message;->setAsynchronous(Z)V
/* .line 286 */
} // :cond_1
v2 = this.mH;
(( com.android.server.wm.MiuiActivityControllerImpl$H ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiActivityControllerImpl$H;->sendMessage(Landroid/os/Message;)Z
/* .line 287 */
return;
} // .end method
/* # virtual methods */
public void activityDestroyed ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 255 */
int v0 = 5; // const/4 v0, 0x5
/* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 256 */
return;
} // .end method
public void activityIdle ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 235 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 236 */
return;
} // .end method
public void activityPaused ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 245 */
int v0 = 3; // const/4 v0, 0x3
/* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 246 */
return;
} // .end method
public void activityResumed ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 240 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 241 */
return;
} // .end method
public void activityStopped ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 250 */
int v0 = 4; // const/4 v0, 0x4
/* invoke-direct {p0, v0, p1}, Lcom/android/server/wm/MiuiActivityControllerImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 251 */
return;
} // .end method
public void registerActivityObserver ( android.app.IMiuiActivityObserver p0, android.content.Intent p1 ) {
/* .locals 1 */
/* .param p1, "observer" # Landroid/app/IMiuiActivityObserver; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 225 */
v0 = this.mActivityObservers;
(( android.os.RemoteCallbackList ) v0 ).register ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z
/* .line 226 */
return;
} // .end method
public void unregisterActivityObserver ( android.app.IMiuiActivityObserver p0 ) {
/* .locals 1 */
/* .param p1, "observer" # Landroid/app/IMiuiActivityObserver; */
/* .line 230 */
v0 = this.mActivityObservers;
(( android.os.RemoteCallbackList ) v0 ).unregister ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 231 */
return;
} // .end method
