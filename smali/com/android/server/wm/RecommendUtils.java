public class com.android.server.wm.RecommendUtils {
	 /* .source "RecommendUtils.java" */
	 /* # static fields */
	 public static final Boolean MIUI_FREEFORM_SHADOW_V2_SUPPORTED;
	 static final java.lang.String TAG;
	 /* # direct methods */
	 static com.android.server.wm.RecommendUtils ( ) {
		 /* .locals 2 */
		 /* .line 20 */
		 final String v0 = "persist.sys.mi_shadow_supported"; // const-string v0, "persist.sys.mi_shadow_supported"
		 int v1 = 1; // const/4 v1, 0x1
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.android.server.wm.RecommendUtils.MIUI_FREEFORM_SHADOW_V2_SUPPORTED = (v0!= 0);
		 return;
	 } // .end method
	 private com.android.server.wm.RecommendUtils ( ) {
		 /* .locals 0 */
		 /* .line 22 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 24 */
		 return;
	 } // .end method
	 public static Float applyDip2Px ( android.content.Context p0, Float p1 ) {
		 /* .locals 2 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "value" # F */
		 /* .line 155 */
		 (( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
		 (( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
		 int v1 = 1; // const/4 v1, 0x1
		 v0 = 		 android.util.TypedValue .applyDimension ( v1,p1,v0 );
	 } // .end method
	 public static Boolean isInEmbeddedWindowingMode ( com.android.server.wm.Task p0 ) {
		 /* .locals 4 */
		 /* .param p0, "task" # Lcom/android/server/wm/Task; */
		 /* .line 57 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* if-nez p0, :cond_0 */
		 /* .line 58 */
	 } // :cond_0
	 if ( p0 != null) { // if-eqz p0, :cond_1
		 try { // :try_start_0
			 v1 = 			 (( com.android.server.wm.Task ) p0 ).hasChild ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->hasChild()Z
			 if ( v1 != null) { // if-eqz v1, :cond_1
				 /* .line 59 */
				 v1 = 				 (( com.android.server.wm.Task ) p0 ).getWindowingMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I
				 int v2 = 1; // const/4 v2, 0x1
				 /* if-ne v1, v2, :cond_1 */
				 /* .line 60 */
				 (( com.android.server.wm.Task ) p0 ).getTopChild ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
				 v1 = 				 (( com.android.server.wm.WindowContainer ) v1 ).isEmbedded ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowContainer;->isEmbedded()Z
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 if ( v1 != null) { // if-eqz v1, :cond_1
					 /* move v0, v2 */
					 /* .line 61 */
					 /* :catch_0 */
					 /* move-exception v1 */
					 /* .line 62 */
					 /* .local v1, "e":Ljava/lang/Exception; */
					 /* new-instance v2, Ljava/lang/StringBuilder; */
					 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
					 final String v3 = "exception in isInSplitScreenWindowingMode:"; // const-string v3, "exception in isInSplitScreenWindowingMode:"
					 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
					 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
					 final String v3 = "RecommendUtils"; // const-string v3, "RecommendUtils"
					 android.util.Slog .d ( v3,v2 );
					 /* .line 63 */
					 /* .line 60 */
				 } // .end local v1 # "e":Ljava/lang/Exception;
			 } // :cond_1
			 /* nop */
			 /* .line 58 */
		 } // :goto_0
	 } // .end method
	 public static Boolean isInSplitScreenWindowingMode ( com.android.server.wm.Task p0 ) {
		 /* .locals 6 */
		 /* .param p0, "task" # Lcom/android/server/wm/Task; */
		 /* .line 33 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* if-nez p0, :cond_0 */
		 /* .line 34 */
	 } // :cond_0
	 try { // :try_start_0
		 v1 = 		 (( com.android.server.wm.Task ) p0 ).isRootTask ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->isRootTask()Z
		 /* .line 35 */
		 /* .local v1, "isRoot":Z */
		 int v2 = 6; // const/4 v2, 0x6
		 int v3 = 1; // const/4 v3, 0x1
		 if ( v1 != null) { // if-eqz v1, :cond_2
			 /* .line 36 */
			 /* iget-boolean v4, p0, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
			 if ( v4 != null) { // if-eqz v4, :cond_1
				 v4 = 				 (( com.android.server.wm.Task ) p0 ).hasChild ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->hasChild()Z
				 if ( v4 != null) { // if-eqz v4, :cond_1
					 /* .line 37 */
					 v4 = 					 (( com.android.server.wm.Task ) p0 ).getWindowingMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I
					 /* if-ne v4, v3, :cond_1 */
					 /* .line 38 */
					 (( com.android.server.wm.Task ) p0 ).getTopChild ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
					 if ( v4 != null) { // if-eqz v4, :cond_1
						 /* .line 39 */
						 (( com.android.server.wm.Task ) p0 ).getTopChild ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
						 (( com.android.server.wm.WindowContainer ) v4 ).asTask ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
						 if ( v4 != null) { // if-eqz v4, :cond_1
							 /* .line 40 */
							 (( com.android.server.wm.Task ) p0 ).getTopChild ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
							 v4 = 							 (( com.android.server.wm.WindowContainer ) v4 ).getWindowingMode ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
							 /* if-ne v4, v2, :cond_1 */
							 /* move v0, v3 */
						 } // :cond_1
						 /* nop */
						 /* .line 36 */
					 } // :goto_0
					 /* .line 42 */
				 } // :cond_2
				 (( com.android.server.wm.Task ) p0 ).getRootTask ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
				 /* .line 43 */
				 /* .local v4, "rootTask":Lcom/android/server/wm/Task; */
				 if ( v4 != null) { // if-eqz v4, :cond_3
					 /* iget-boolean v5, v4, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
					 if ( v5 != null) { // if-eqz v5, :cond_3
						 v5 = 						 (( com.android.server.wm.Task ) v4 ).hasChild ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->hasChild()Z
						 if ( v5 != null) { // if-eqz v5, :cond_3
							 /* .line 44 */
							 v5 = 							 (( com.android.server.wm.Task ) v4 ).getWindowingMode ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getWindowingMode()I
							 /* if-ne v5, v3, :cond_3 */
							 /* .line 45 */
							 (( com.android.server.wm.Task ) v4 ).getTopChild ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
							 if ( v5 != null) { // if-eqz v5, :cond_3
								 /* .line 46 */
								 (( com.android.server.wm.Task ) v4 ).getTopChild ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
								 (( com.android.server.wm.WindowContainer ) v5 ).asTask ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
								 if ( v5 != null) { // if-eqz v5, :cond_3
									 /* .line 47 */
									 (( com.android.server.wm.Task ) v4 ).getTopChild ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
									 v5 = 									 (( com.android.server.wm.WindowContainer ) v5 ).getWindowingMode ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
									 /* :try_end_0 */
									 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
									 /* if-ne v5, v2, :cond_3 */
									 /* move v0, v3 */
								 } // :cond_3
								 /* nop */
								 /* .line 43 */
							 } // :goto_1
							 /* .line 49 */
						 } // .end local v1 # "isRoot":Z
					 } // .end local v4 # "rootTask":Lcom/android/server/wm/Task;
					 /* :catch_0 */
					 /* move-exception v1 */
					 /* .line 50 */
					 /* .local v1, "e":Ljava/lang/Exception; */
					 /* new-instance v2, Ljava/lang/StringBuilder; */
					 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
					 final String v3 = "exception in isInSplitScreenWindowingMode:"; // const-string v3, "exception in isInSplitScreenWindowingMode:"
					 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
					 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
					 final String v3 = "RecommendUtils"; // const-string v3, "RecommendUtils"
					 android.util.Slog .d ( v3,v2 );
					 /* .line 51 */
				 } // .end method
				 public static Boolean isKeyguardLocked ( android.content.Context p0 ) {
					 /* .locals 2 */
					 /* .param p0, "context" # Landroid/content/Context; */
					 /* .line 103 */
					 /* const-class v0, Landroid/app/KeyguardManager; */
					 (( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
					 /* check-cast v0, Landroid/app/KeyguardManager; */
					 /* .line 104 */
					 /* .local v0, "keyguardManager":Landroid/app/KeyguardManager; */
					 if ( v0 != null) { // if-eqz v0, :cond_0
						 v1 = 						 (( android.app.KeyguardManager ) v0 ).isKeyguardLocked ( ); // invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z
						 if ( v1 != null) { // if-eqz v1, :cond_0
							 int v1 = 1; // const/4 v1, 0x1
						 } // :cond_0
						 int v1 = 0; // const/4 v1, 0x0
					 } // :goto_0
				 } // .end method
				 public static Boolean isMiuiBuild ( ) {
					 /* .locals 2 */
					 /* .line 27 */
					 /* nop */
					 /* .line 28 */
					 final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
					 android.os.SystemProperties .get ( v0 );
					 final String v1 = "1"; // const-string v1, "1"
					 v0 = 					 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 /* .line 27 */
					 /* xor-int/lit8 v0, v0, 0x1 */
					 final String v1 = "persist.sys.miui_optimization"; // const-string v1, "persist.sys.miui_optimization"
					 v0 = 					 android.os.SystemProperties .getBoolean ( v1,v0 );
				 } // .end method
				 public static Boolean isSupportMiuiMultiWindowRecommend ( ) {
					 /* .locals 1 */
					 /* .line 68 */
					 int v0 = 1; // const/4 v0, 0x1
				 } // .end method
				 public static Boolean isSupportMiuiShadowV2 ( ) {
					 /* .locals 8 */
					 /* .line 108 */
					 /* sget-boolean v0, Lcom/android/server/wm/RecommendUtils;->MIUI_FREEFORM_SHADOW_V2_SUPPORTED:Z */
					 int v1 = 0; // const/4 v1, 0x0
					 /* if-nez v0, :cond_0 */
					 /* .line 109 */
					 /* .line 111 */
				 } // :cond_0
				 /* const-class v2, Landroid/view/SurfaceControl; */
				 v3 = java.lang.Integer.TYPE;
				 v4 = java.lang.Float.TYPE;
				 v5 = java.lang.Float.TYPE;
				 v6 = java.lang.Float.TYPE;
				 v7 = java.lang.Float.TYPE;
				 /* filled-new-array/range {v2 ..v7}, [Ljava/lang/Class; */
				 /* .line 114 */
				 /* .local v0, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
				 try { // :try_start_0
					 /* const-class v2, Landroid/view/SurfaceControl$Transaction; */
					 /* const-string/jumbo v3, "setMiShadow" */
					 android.util.MiuiMultiWindowUtils .getDeclaredMethod ( v2,v3,v0 );
					 /* :try_end_0 */
					 /* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_0 */
					 /* .line 116 */
					 /* .local v2, "setShadowSettingsMethod":Ljava/lang/reflect/Method; */
					 if ( v2 != null) { // if-eqz v2, :cond_1
						 /* .line 117 */
						 int v1 = 1; // const/4 v1, 0x1
						 /* .line 120 */
					 } // .end local v2 # "setShadowSettingsMethod":Ljava/lang/reflect/Method;
				 } // :cond_1
				 /* .line 119 */
				 /* :catch_0 */
				 /* move-exception v2 */
				 /* .line 121 */
			 } // :goto_0
		 } // .end method
		 public static void resetMiShadowV2 ( android.view.SurfaceControl p0, android.content.Context p1 ) {
			 /* .locals 10 */
			 /* .param p0, "sc" # Landroid/view/SurfaceControl; */
			 /* .param p1, "context" # Landroid/content/Context; */
			 /* .line 147 */
			 if ( p0 != null) { // if-eqz p0, :cond_1
				 v0 = 				 (( android.view.SurfaceControl ) p0 ).isValid ( ); // invoke-virtual {p0}, Landroid/view/SurfaceControl;->isValid()Z
				 if ( v0 != null) { // if-eqz v0, :cond_1
					 /* if-nez p1, :cond_0 */
					 /* .line 151 */
				 } // :cond_0
				 int v2 = 0; // const/4 v2, 0x0
				 int v3 = 0; // const/4 v3, 0x0
				 int v4 = 0; // const/4 v4, 0x0
				 int v5 = 0; // const/4 v5, 0x0
				 int v6 = 0; // const/4 v6, 0x0
				 int v7 = 0; // const/4 v7, 0x0
				 /* new-instance v8, Landroid/graphics/RectF; */
				 /* invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V */
				 /* move-object v1, p0 */
				 /* move-object v9, p1 */
				 /* invoke-static/range {v1 ..v9}, Lcom/android/server/wm/RecommendUtils;->setMiShadowV2(Landroid/view/SurfaceControl;IFFFFFLandroid/graphics/RectF;Landroid/content/Context;)V */
				 /* .line 152 */
				 return;
				 /* .line 148 */
			 } // :cond_1
		 } // :goto_0
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "invalid SurfaceControl = "; // const-string v1, "invalid SurfaceControl = "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v1 = "RecommendUtils"; // const-string v1, "RecommendUtils"
		 android.util.Slog .e ( v1,v0 );
		 /* .line 149 */
		 return;
	 } // .end method
	 public static void resetShadowSettingsInTransactionForSurfaceControl ( android.view.SurfaceControl p0, Integer p1, Float p2, Float[] p3, Float p4, Float p5, Float p6, Integer p7 ) {
		 /* .locals 0 */
		 /* .param p0, "surface" # Landroid/view/SurfaceControl; */
		 /* .param p1, "shadowType" # I */
		 /* .param p2, "length" # F */
		 /* .param p3, "color" # [F */
		 /* .param p4, "offsetX" # F */
		 /* .param p5, "offsetY" # F */
		 /* .param p6, "outset" # F */
		 /* .param p7, "numOfLayers" # I */
		 /* .line 98 */
		 /* invoke-static/range {p0 ..p7}, Lcom/android/server/wm/RecommendUtils;->setShadowSettingsInTransactionForSurfaceControl(Landroid/view/SurfaceControl;IF[FFFFI)V */
		 /* .line 100 */
		 return;
	 } // .end method
	 public static void setCornerRadiusForSurfaceControl ( android.view.SurfaceControl p0, Integer p1 ) {
		 /* .locals 2 */
		 /* .param p0, "surface" # Landroid/view/SurfaceControl; */
		 /* .param p1, "radius" # I */
		 /* .line 72 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( android.view.SurfaceControl ) p0 ).isValid ( ); // invoke-virtual {p0}, Landroid/view/SurfaceControl;->isValid()Z
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 73 */
				 /* new-instance v0, Landroid/view/SurfaceControl$Transaction; */
				 /* invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
				 /* .line 74 */
				 /* .local v0, "t":Landroid/view/SurfaceControl$Transaction; */
				 /* int-to-float v1, p1 */
				 (( android.view.SurfaceControl$Transaction ) v0 ).setCornerRadius ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Landroid/view/SurfaceControl$Transaction;->setCornerRadius(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
				 /* .line 75 */
				 (( android.view.SurfaceControl$Transaction ) v0 ).apply ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
				 /* .line 77 */
			 } // .end local v0 # "t":Landroid/view/SurfaceControl$Transaction;
		 } // :cond_0
		 return;
	 } // .end method
	 public static void setMiShadowV2 ( android.view.SurfaceControl p0, Integer p1, Float p2, Float p3, Float p4, Float p5, Float p6, android.graphics.RectF p7, android.content.Context p8 ) {
		 /* .locals 16 */
		 /* .param p0, "sc" # Landroid/view/SurfaceControl; */
		 /* .param p1, "color" # I */
		 /* .param p2, "offsetX" # F */
		 /* .param p3, "offsetY" # F */
		 /* .param p4, "radius" # F */
		 /* .param p5, "dispersion" # F */
		 /* .param p6, "cornerRadius" # F */
		 /* .param p7, "bounds" # Landroid/graphics/RectF; */
		 /* .param p8, "context" # Landroid/content/Context; */
		 /* .line 126 */
		 /* move-object/from16 v9, p0 */
		 /* move-object/from16 v10, p8 */
		 if ( v9 != null) { // if-eqz v9, :cond_1
			 v0 = 			 /* invoke-virtual/range {p0 ..p0}, Landroid/view/SurfaceControl;->isValid()Z */
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 /* if-nez v10, :cond_0 */
				 /* move/from16 v13, p2 */
				 /* move/from16 v14, p3 */
				 /* move/from16 v15, p4 */
				 /* .line 130 */
			 } // :cond_0
			 /* new-instance v0, Landroid/view/SurfaceControl$Transaction; */
			 /* invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
			 /* move-object v11, v0 */
			 /* .line 131 */
			 /* .local v11, "t":Landroid/view/SurfaceControl$Transaction; */
			 /* const-class v0, Landroid/view/SurfaceControl; */
			 v1 = java.lang.Integer.TYPE;
			 v2 = java.lang.Float.TYPE;
			 v3 = java.lang.Float.TYPE;
			 v4 = java.lang.Float.TYPE;
			 v5 = java.lang.Float.TYPE;
			 v6 = java.lang.Float.TYPE;
			 v7 = java.lang.Boolean.TYPE;
			 /* const-class v8, Landroid/graphics/RectF; */
			 /* filled-new-array/range {v0 ..v8}, [Ljava/lang/Class; */
			 /* move-object v12, v0 */
			 /* .line 133 */
			 /* .local v12, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
			 /* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
			 /* .line 134 */
			 /* move/from16 v13, p2 */
			 v0 = 			 com.android.server.wm.RecommendUtils .applyDip2Px ( v10,v13 );
			 java.lang.Float .valueOf ( v0 );
			 /* .line 135 */
			 /* move/from16 v14, p3 */
			 v0 = 			 com.android.server.wm.RecommendUtils .applyDip2Px ( v10,v14 );
			 java.lang.Float .valueOf ( v0 );
			 /* .line 136 */
			 /* move/from16 v15, p4 */
			 v0 = 			 com.android.server.wm.RecommendUtils .applyDip2Px ( v10,v15 );
			 java.lang.Float .valueOf ( v0 );
			 /* .line 137 */
			 /* invoke-static/range {p5 ..p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float; */
			 /* .line 138 */
			 /* move/from16 v8, p6 */
			 v0 = 			 com.android.server.wm.RecommendUtils .applyDip2Px ( v10,v8 );
			 java.lang.Float .valueOf ( v0 );
			 /* .line 139 */
			 int v0 = 0; // const/4 v0, 0x0
			 java.lang.Boolean .valueOf ( v0 );
			 /* move-object/from16 v0, p0 */
			 /* move-object/from16 v8, p7 */
			 /* filled-new-array/range {v0 ..v8}, [Ljava/lang/Object; */
			 /* .line 141 */
			 /* .local v0, "values":[Ljava/lang/Object; */
			 /* const-class v1, Landroid/view/SurfaceControl$Transaction; */
			 /* const-string/jumbo v2, "setMiShadow" */
			 android.util.MiuiMultiWindowUtils .callObjectMethod ( v11,v1,v2,v12,v0 );
			 /* .line 143 */
			 (( android.view.SurfaceControl$Transaction ) v11 ).apply ( ); // invoke-virtual {v11}, Landroid/view/SurfaceControl$Transaction;->apply()V
			 /* .line 144 */
			 return;
			 /* .line 126 */
		 } // .end local v0 # "values":[Ljava/lang/Object;
	 } // .end local v11 # "t":Landroid/view/SurfaceControl$Transaction;
} // .end local v12 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // :cond_1
/* move/from16 v13, p2 */
/* move/from16 v14, p3 */
/* move/from16 v15, p4 */
/* .line 127 */
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "invalid SurfaceControl = "; // const-string v1, "invalid SurfaceControl = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "RecommendUtils"; // const-string v1, "RecommendUtils"
android.util.Slog .e ( v1,v0 );
/* .line 128 */
return;
} // .end method
public static void setShadowSettingsInTransactionForSurfaceControl ( android.view.SurfaceControl p0, Integer p1, Float p2, Float[] p3, Float p4, Float p5, Float p6, Integer p7 ) {
/* .locals 10 */
/* .param p0, "surface" # Landroid/view/SurfaceControl; */
/* .param p1, "shadowType" # I */
/* .param p2, "length" # F */
/* .param p3, "color" # [F */
/* .param p4, "offsetX" # F */
/* .param p5, "offsetY" # F */
/* .param p6, "outset" # F */
/* .param p7, "numOfLayers" # I */
/* .line 83 */
if ( p0 != null) { // if-eqz p0, :cond_0
v0 = (( android.view.SurfaceControl ) p0 ).isValid ( ); // invoke-virtual {p0}, Landroid/view/SurfaceControl;->isValid()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = android.util.MiuiMultiWindowUtils .isSupportMiuiShadow ( );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 84 */
	 /* new-instance v0, Landroid/view/SurfaceControl$Transaction; */
	 /* invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
	 /* move-object v8, v0 */
	 /* .line 85 */
	 /* .local v8, "t":Landroid/view/SurfaceControl$Transaction; */
	 /* const-class v0, Landroid/view/SurfaceControl; */
	 v1 = java.lang.Integer.TYPE;
	 v2 = java.lang.Float.TYPE;
	 /* const-class v3, [F */
	 v4 = java.lang.Float.TYPE;
	 v5 = java.lang.Float.TYPE;
	 v6 = java.lang.Float.TYPE;
	 v7 = java.lang.Integer.TYPE;
	 /* filled-new-array/range {v0 ..v7}, [Ljava/lang/Class; */
	 /* move-object v9, v0 */
	 /* .line 87 */
	 /* .local v9, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
	 java.lang.Integer .valueOf ( p1 );
	 java.lang.Float .valueOf ( p2 );
	 java.lang.Float .valueOf ( p4 );
	 java.lang.Float .valueOf ( p5 );
	 /* invoke-static/range {p6 ..p6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float; */
	 /* invoke-static/range {p7 ..p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
	 /* move-object v0, p0 */
	 /* move-object v3, p3 */
	 /* filled-new-array/range {v0 ..v7}, [Ljava/lang/Object; */
	 /* .line 88 */
	 /* .local v0, "values":[Ljava/lang/Object; */
	 /* const-class v1, Landroid/view/SurfaceControl$Transaction; */
	 /* const-string/jumbo v2, "setShadowSettings" */
	 android.util.MiuiMultiWindowUtils .callObjectMethod ( v8,v1,v2,v9,v0 );
	 /* .line 90 */
	 (( android.view.SurfaceControl$Transaction ) v8 ).apply ( ); // invoke-virtual {v8}, Landroid/view/SurfaceControl$Transaction;->apply()V
	 /* .line 92 */
} // .end local v0 # "values":[Ljava/lang/Object;
} // .end local v8 # "t":Landroid/view/SurfaceControl$Transaction;
} // .end local v9 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // :cond_0
return;
} // .end method
