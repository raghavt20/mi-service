class com.android.server.wm.AppTransitionInjector$ActivityTranstionInterpolator implements android.view.animation.Interpolator {
	 /* .source "AppTransitionInjector.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AppTransitionInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ActivityTranstionInterpolator" */
} // .end annotation
/* # static fields */
private static Float c;
private static Float c1;
private static Float c2;
private static Float initial;
private static Float k;
private static Float m;
private static Float mDamping;
private static Float mResponse;
private static Float r;
private static Float w;
/* # direct methods */
static com.android.server.wm.AppTransitionInjector$ActivityTranstionInterpolator ( ) {
/* .locals 1 */
/* .line 1269 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 1270 */
/* .line 1271 */
/* .line 1272 */
/* .line 1273 */
/* .line 1274 */
/* .line 1275 */
/* .line 1276 */
/* .line 1277 */
/* .line 1278 */
return;
} // .end method
public com.android.server.wm.AppTransitionInjector$ActivityTranstionInterpolator ( ) {
/* .locals 7 */
/* .param p1, "response" # F */
/* .param p2, "damping" # F */
/* .line 1279 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1280 */
/* .line 1281 */
/* .line 1282 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* .line 1283 */
/* const-wide v0, 0x401921fb54442d18L # 6.283185307179586 */
/* float-to-double v2, p1 */
/* div-double/2addr v0, v2 */
/* const-wide/high16 v2, 0x4000000000000000L # 2.0 */
java.lang.Math .pow ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* float-to-double v3, v2 */
/* mul-double/2addr v0, v3 */
/* double-to-float v0, v0 */
/* .line 1284 */
/* float-to-double v3, v1 */
/* const-wide v5, 0x402921fb54442d18L # 12.566370614359172 */
/* mul-double/2addr v3, v5 */
/* float-to-double v5, v2 */
/* mul-double/2addr v3, v5 */
/* float-to-double v5, v1 */
/* div-double/2addr v3, v5 */
/* double-to-float v1, v3 */
/* .line 1285 */
/* const/high16 v3, 0x40800000 # 4.0f */
/* mul-float/2addr v2, v3 */
/* mul-float/2addr v2, v0 */
/* mul-float/2addr v1, v1 */
/* sub-float/2addr v2, v1 */
/* float-to-double v0, v2 */
java.lang.Math .sqrt ( v0,v1 );
/* move-result-wide v0 */
/* double-to-float v0, v0 */
/* const/high16 v2, 0x40000000 # 2.0f */
/* mul-float v3, v1, v2 */
/* div-float/2addr v0, v3 */
/* .line 1286 */
/* div-float/2addr v3, v2 */
/* mul-float/2addr v3, v1 */
/* neg-float v1, v3 */
/* .line 1287 */
/* .line 1288 */
int v3 = 0; // const/4 v3, 0x0
/* mul-float/2addr v1, v2 */
/* sub-float/2addr v3, v1 */
/* div-float/2addr v3, v0 */
/* .line 1289 */
return;
} // .end method
/* # virtual methods */
public Float getInterpolation ( Float p0 ) {
/* .locals 8 */
/* .param p1, "input" # F */
/* .line 1293 */
/* mul-float/2addr v0, p1 */
/* float-to-double v0, v0 */
/* const-wide v2, 0x4005bf0a8b145769L # Math.E */
java.lang.Math .pow ( v2,v3,v0,v1 );
/* move-result-wide v0 */
/* float-to-double v2, v2 */
/* mul-float/2addr v4, p1 */
/* float-to-double v4, v4 */
java.lang.Math .cos ( v4,v5 );
/* move-result-wide v4 */
/* mul-double/2addr v2, v4 */
/* float-to-double v4, v4 */
/* mul-float/2addr v6, p1 */
/* float-to-double v6, v6 */
java.lang.Math .sin ( v6,v7 );
/* move-result-wide v6 */
/* mul-double/2addr v4, v6 */
/* add-double/2addr v2, v4 */
/* mul-double/2addr v0, v2 */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
/* add-double/2addr v0, v2 */
/* double-to-float v0, v0 */
} // .end method
