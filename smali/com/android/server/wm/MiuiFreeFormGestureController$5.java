class com.android.server.wm.MiuiFreeFormGestureController$5 extends android.database.ContentObserver {
	 /* .source "MiuiFreeFormGestureController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiFreeFormGestureController;->registerFreeformCloudDataObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiFreeFormGestureController this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiFreeFormGestureController$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiFreeFormGestureController; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 214 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .line 217 */
final String v0 = "MiuiFreeFormGestureController"; // const-string v0, "MiuiFreeFormGestureController"
final String v1 = "receive notification of data update from app"; // const-string v1, "receive notification of data update from app"
android.util.Slog .d ( v0,v1 );
/* .line 218 */
v0 = this.this$0;
v0 = this.mService;
v0 = this.mContext;
android.util.MiuiMultiWindowUtils .updateListFromCloud ( v0 );
/* .line 219 */
return;
} // .end method
