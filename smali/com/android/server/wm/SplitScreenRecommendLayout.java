public class com.android.server.wm.SplitScreenRecommendLayout extends android.widget.FrameLayout {
	 /* .source "SplitScreenRecommendLayout.java" */
	 /* # static fields */
	 private static final Float RECOMMEND_VIEW_TOP_MARGIN;
	 private static final Float RECOMMEND_VIEW_TOP_MARGIN_OFFSET;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private android.widget.ImageView mPrimaryImageView;
	 private android.widget.ImageView mSecondaryImageView;
	 private android.widget.RelativeLayout mSplitScreenIconContainer;
	 private android.view.WindowManager mWindowManager;
	 /* # direct methods */
	 public com.android.server.wm.SplitScreenRecommendLayout ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 42 */
		 /* invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V */
		 /* .line 43 */
		 (( com.android.server.wm.SplitScreenRecommendLayout ) p0 ).init ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->init(Landroid/content/Context;)V
		 /* .line 44 */
		 return;
	 } // .end method
	 public com.android.server.wm.SplitScreenRecommendLayout ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .line 47 */
		 /* invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V */
		 /* .line 48 */
		 (( com.android.server.wm.SplitScreenRecommendLayout ) p0 ).init ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->init(Landroid/content/Context;)V
		 /* .line 49 */
		 return;
	 } // .end method
	 public com.android.server.wm.SplitScreenRecommendLayout ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .param p3, "defStyle" # I */
		 /* .line 52 */
		 /* invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V */
		 /* .line 53 */
		 (( com.android.server.wm.SplitScreenRecommendLayout ) p0 ).init ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->init(Landroid/content/Context;)V
		 /* .line 54 */
		 return;
	 } // .end method
	 public com.android.server.wm.SplitScreenRecommendLayout ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .param p3, "defStyleAttr" # I */
		 /* .param p4, "defStyleRes" # I */
		 /* .line 57 */
		 /* invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V */
		 /* .line 58 */
		 (( com.android.server.wm.SplitScreenRecommendLayout ) p0 ).init ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/SplitScreenRecommendLayout;->init(Landroid/content/Context;)V
		 /* .line 59 */
		 return;
	 } // .end method
	 private Float dipToPx ( Float p0 ) {
		 /* .locals 2 */
		 /* .param p1, "dip" # F */
		 /* .line 138 */
		 /* nop */
		 /* .line 139 */
		 (( com.android.server.wm.SplitScreenRecommendLayout ) p0 ).getResources ( ); // invoke-virtual {p0}, Lcom/android/server/wm/SplitScreenRecommendLayout;->getResources()Landroid/content/res/Resources;
		 (( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
		 /* .line 138 */
		 int v1 = 1; // const/4 v1, 0x1
		 v0 = 		 android.util.TypedValue .applyDimension ( v1,p1,v0 );
	 } // .end method
	 private void setRadius ( android.view.View p0, Float p1 ) {
		 /* .locals 1 */
		 /* .param p1, "content" # Landroid/view/View; */
		 /* .param p2, "radius" # F */
		 /* .line 143 */
		 /* new-instance v0, Lcom/android/server/wm/SplitScreenRecommendLayout$1; */
		 /* invoke-direct {v0, p0, p2}, Lcom/android/server/wm/SplitScreenRecommendLayout$1;-><init>(Lcom/android/server/wm/SplitScreenRecommendLayout;F)V */
		 (( android.view.View ) p1 ).setOutlineProvider ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V
		 /* .line 149 */
		 int v0 = 1; // const/4 v0, 0x1
		 (( android.view.View ) p1 ).setClipToOutline ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->setClipToOutline(Z)V
		 /* .line 150 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public android.view.WindowManager$LayoutParams createLayoutParams ( Integer p0 ) {
		 /* .locals 9 */
		 /* .param p1, "topMargin" # I */
		 /* .line 94 */
		 int v6 = -2; // const/4 v6, -0x2
		 /* .line 95 */
		 /* .local v6, "width":I */
		 int v7 = -2; // const/4 v7, -0x2
		 /* .line 96 */
		 /* .local v7, "height":I */
		 /* new-instance v8, Landroid/view/WindowManager$LayoutParams; */
		 /* const/16 v3, 0x7f6 */
		 /* const v4, 0x1000528 */
		 int v5 = 1; // const/4 v5, 0x1
		 /* move-object v0, v8 */
		 /* move v1, v6 */
		 /* move v2, v7 */
		 /* invoke-direct/range {v0 ..v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V */
		 /* .line 106 */
		 /* .local v0, "lp":Landroid/view/WindowManager$LayoutParams; */
		 /* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* or-int/lit8 v1, v1, 0x10 */
		 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* .line 107 */
		 /* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* or-int/lit8 v1, v1, 0x40 */
		 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* .line 108 */
		 /* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* const/high16 v2, 0x20000000 */
		 /* or-int/2addr v1, v2 */
		 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* .line 109 */
		 int v1 = 0; // const/4 v1, 0x0
		 (( android.view.WindowManager$LayoutParams ) v0 ).setFitInsetsTypes ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setFitInsetsTypes(I)V
		 /* .line 110 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
		 /* .line 111 */
		 int v2 = -1; // const/4 v2, -0x1
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I */
		 /* .line 112 */
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->rotationAnimation:I */
		 /* .line 113 */
		 /* const/16 v2, 0x31 */
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I */
		 /* .line 114 */
		 /* const/high16 v2, 0x41200000 # 10.0f */
		 v2 = 		 /* invoke-direct {p0, v2}, Lcom/android/server/wm/SplitScreenRecommendLayout;->dipToPx(F)F */
		 /* float-to-int v2, v2 */
		 /* add-int/2addr v2, p1 */
		 /* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I */
		 /* .line 115 */
		 /* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I */
		 /* .line 116 */
		 final String v1 = "SplitScreen-RecommendView"; // const-string v1, "SplitScreen-RecommendView"
		 (( android.view.WindowManager$LayoutParams ) v0 ).setTitle ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
		 /* .line 117 */
	 } // .end method
	 public android.widget.ImageView getPrimaryImageView ( ) {
		 /* .locals 1 */
		 /* .line 125 */
		 v0 = this.mPrimaryImageView;
	 } // .end method
	 public android.widget.ImageView getSecondaryImageView ( ) {
		 /* .locals 1 */
		 /* .line 129 */
		 v0 = this.mSecondaryImageView;
	 } // .end method
	 public android.widget.RelativeLayout getSplitScreenIconContainer ( ) {
		 /* .locals 1 */
		 /* .line 121 */
		 v0 = this.mSplitScreenIconContainer;
	 } // .end method
	 public void init ( android.content.Context p0 ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 62 */
		 final String v0 = "SplitScreenRecommendLayout"; // const-string v0, "SplitScreenRecommendLayout"
		 final String v1 = "init splitScreenRecommendLayout "; // const-string v1, "init splitScreenRecommendLayout "
		 android.util.Slog .d ( v0,v1 );
		 /* .line 63 */
		 this.mContext = p1;
		 /* .line 64 */
		 /* const-string/jumbo v0, "window" */
		 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/view/WindowManager; */
		 this.mWindowManager = v0;
		 /* .line 65 */
		 return;
	 } // .end method
	 protected void onAttachedToWindow ( ) {
		 /* .locals 0 */
		 /* .line 83 */
		 /* invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V */
		 /* .line 84 */
		 return;
	 } // .end method
	 protected void onConfigurationChanged ( android.content.res.Configuration p0 ) {
		 /* .locals 2 */
		 /* .param p1, "newConfig" # Landroid/content/res/Configuration; */
		 /* .line 69 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "onConfigurationChanged newConfig.orientation="; // const-string v1, "onConfigurationChanged newConfig.orientation="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p1, Landroid/content/res/Configuration;->orientation:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v1 = "SplitScreenRecommendLayout"; // const-string v1, "SplitScreenRecommendLayout"
		 android.util.Slog .d ( v1,v0 );
		 /* .line 70 */
		 return;
	 } // .end method
	 protected void onDetachedFromWindow ( ) {
		 /* .locals 2 */
		 /* .line 89 */
		 /* invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V */
		 /* .line 90 */
		 final String v0 = "SplitScreenRecommendLayout"; // const-string v0, "SplitScreenRecommendLayout"
		 final String v1 = "onDetachedFromWindow"; // const-string v1, "onDetachedFromWindow"
		 android.util.Log .d ( v0,v1 );
		 /* .line 91 */
		 return;
	 } // .end method
	 protected void onFinishInflate ( ) {
		 /* .locals 2 */
		 /* .line 74 */
		 /* invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V */
		 /* .line 75 */
		 int v0 = 0; // const/4 v0, 0x0
		 (( com.android.server.wm.SplitScreenRecommendLayout ) p0 ).getChildAt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/SplitScreenRecommendLayout;->getChildAt(I)Landroid/view/View;
		 /* check-cast v1, Landroid/widget/RelativeLayout; */
		 this.mSplitScreenIconContainer = v1;
		 /* .line 76 */
		 (( android.widget.RelativeLayout ) v1 ).getChildAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;
		 /* check-cast v0, Landroid/widget/ImageView; */
		 this.mPrimaryImageView = v0;
		 /* .line 77 */
		 v0 = this.mSplitScreenIconContainer;
		 int v1 = 1; // const/4 v1, 0x1
		 (( android.widget.RelativeLayout ) v0 ).getChildAt ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;
		 /* check-cast v0, Landroid/widget/ImageView; */
		 this.mSecondaryImageView = v0;
		 /* .line 78 */
		 final String v0 = "SplitScreenRecommendLayout"; // const-string v0, "SplitScreenRecommendLayout"
		 final String v1 = "onFinishInflate"; // const-string v1, "onFinishInflate"
		 android.util.Slog .d ( v0,v1 );
		 /* .line 79 */
		 return;
	 } // .end method
	 public void setSplitScreenIcon ( android.graphics.drawable.Drawable p0, android.graphics.drawable.Drawable p1 ) {
		 /* .locals 1 */
		 /* .param p1, "primaryDrawable" # Landroid/graphics/drawable/Drawable; */
		 /* .param p2, "secondaryDrawble" # Landroid/graphics/drawable/Drawable; */
		 /* .line 133 */
		 v0 = this.mPrimaryImageView;
		 (( android.widget.ImageView ) v0 ).setImageDrawable ( p1 ); // invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
		 /* .line 134 */
		 v0 = this.mSecondaryImageView;
		 (( android.widget.ImageView ) v0 ).setImageDrawable ( p2 ); // invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
		 /* .line 135 */
		 return;
	 } // .end method
