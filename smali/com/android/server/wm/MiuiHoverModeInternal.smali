.class public Lcom/android/server/wm/MiuiHoverModeInternal;
.super Ljava/lang/Object;
.source "MiuiHoverModeInternal.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public adaptLetterboxInsets(Lcom/android/server/wm/ActivityRecord;Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "activity"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "letterboxInsets"    # Landroid/graphics/Rect;

    .line 78
    return-void
.end method

.method public calcHoverAnimatingColor([F)V
    .locals 0
    .param p1, "startColor"    # [F

    .line 87
    return-void
.end method

.method public computeHoverModeBounds(Landroid/content/res/Configuration;Landroid/graphics/Rect;Landroid/graphics/Rect;Lcom/android/server/wm/ActivityRecord;)V
    .locals 0
    .param p1, "newParentConfiguration"    # Landroid/content/res/Configuration;
    .param p2, "parentBounds"    # Landroid/graphics/Rect;
    .param p3, "mTmpBounds"    # Landroid/graphics/Rect;
    .param p4, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 29
    return-void
.end method

.method public enterFreeformForHoverMode(Lcom/android/server/wm/Task;Z)V
    .locals 0
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "enter"    # Z

    .line 93
    return-void
.end method

.method public getHoverModeRecommendRotation(Lcom/android/server/wm/DisplayContent;)I
    .locals 1
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 96
    const/4 v0, -0x1

    return v0
.end method

.method public isDeviceInOpenedOrHalfOpenedState()Z
    .locals 1

    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityPipModeChangedForHoverMode(ZLcom/android/server/wm/ActivityRecord;)V
    .locals 0
    .param p1, "inPip"    # Z
    .param p2, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 81
    return-void
.end method

.method public onArCreated(Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 0
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "isUnStandardLaunchMode"    # Z

    .line 21
    return-void
.end method

.method public onArParentChanged(Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/ActivityRecord;)V
    .locals 0
    .param p1, "oldParent"    # Lcom/android/server/wm/TaskFragment;
    .param p2, "newParent"    # Lcom/android/server/wm/TaskFragment;
    .param p3, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 58
    return-void
.end method

.method public onArStateChanged(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;)V
    .locals 0
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "state"    # Lcom/android/server/wm/ActivityRecord$State;

    .line 66
    return-void
.end method

.method public onArVisibleChanged(Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 0
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "visible"    # Z

    .line 36
    return-void
.end method

.method public onDisplayOverrideConfigUpdate(Lcom/android/server/wm/DisplayContent;)V
    .locals 0
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 69
    return-void
.end method

.method public onFinishTransition()V
    .locals 0

    .line 54
    return-void
.end method

.method public onHoverModeRecentAnimStart()V
    .locals 0

    .line 51
    return-void
.end method

.method public onHoverModeTaskParentChanged(Lcom/android/server/wm/Task;Lcom/android/server/wm/WindowContainer;)V
    .locals 0
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "newParent"    # Lcom/android/server/wm/WindowContainer;

    .line 62
    return-void
.end method

.method public onHoverModeTaskPrepareSurfaces(Lcom/android/server/wm/Task;)V
    .locals 0
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 72
    return-void
.end method

.method public onScreenRotationAnimationEnd()V
    .locals 0

    .line 90
    return-void
.end method

.method public onStopFreezingDisplayLocked()V
    .locals 0

    .line 84
    return-void
.end method

.method public onSystemReady(Lcom/android/server/wm/WindowManagerService;)V
    .locals 0
    .param p1, "wms"    # Lcom/android/server/wm/WindowManagerService;

    .line 18
    return-void
.end method

.method public onTaskConfigurationChanged(II)V
    .locals 0
    .param p1, "prevWindowingMode"    # I
    .param p2, "overrideWindowingMode"    # I

    .line 104
    return-void
.end method

.method public resetTaskFragmentRequestWindowMode(Lcom/android/server/wm/TaskFragment;Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "tf"    # Lcom/android/server/wm/TaskFragment;
    .param p2, "newParentConfiguration"    # Landroid/content/res/Configuration;

    .line 45
    return-void
.end method

.method public setOrientationForHoverMode(Lcom/android/server/wm/ActivityRecord;II)V
    .locals 0
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "requestOrientation"    # I
    .param p3, "orientation"    # I

    .line 48
    return-void
.end method

.method public setRequestedWindowModeForHoverMode(Lcom/android/server/wm/ActivityRecord;Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "newParentConfiguration"    # Landroid/content/res/Configuration;

    .line 42
    return-void
.end method

.method public shouldHookHoverConfig(Landroid/content/res/Configuration;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p1, "newParentConfiguration"    # Landroid/content/res/Configuration;
    .param p2, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public shouldHoverModeEnableSensor(Lcom/android/server/wm/DisplayContent;)Z
    .locals 1
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public updateHoverGuidePanel(Lcom/android/server/wm/WindowState;Z)V
    .locals 0
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;
    .param p2, "add"    # Z

    .line 75
    return-void
.end method

.method public updateLastRotation(Lcom/android/server/wm/DisplayContent;I)V
    .locals 0
    .param p1, "dc"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "lastRotation"    # I

    .line 39
    return-void
.end method
