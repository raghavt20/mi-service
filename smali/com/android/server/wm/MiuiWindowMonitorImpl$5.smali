.class Lcom/android/server/wm/MiuiWindowMonitorImpl$5;
.super Ljava/lang/Object;
.source "MiuiWindowMonitorImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiWindowMonitorImpl;->onProcessDiedLocked(ILcom/android/server/am/ProcessRecord;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

.field final synthetic val$key:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

.field final synthetic val$pid:I


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiWindowMonitorImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 324
    iput-object p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    iput-object p2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->val$key:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    iput p3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->val$pid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 327
    iget-object v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    monitor-enter v0

    .line 328
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    invoke-static {v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->-$$Nest$misEnabled(Lcom/android/server/wm/MiuiWindowMonitorImpl;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 329
    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    iget-object v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->val$key:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    invoke-static {v1, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->-$$Nest$mremoveWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl;Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    .line 330
    monitor-exit v0

    return-void

    .line 332
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->this$0:Lcom/android/server/wm/MiuiWindowMonitorImpl;

    iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->val$pid:I

    invoke-static {v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->-$$Nest$fgetmWindowsByApp(Lcom/android/server/wm/MiuiWindowMonitorImpl;)Ljava/util/HashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;->val$key:Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->-$$Nest$mreportIfNeeded(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V

    .line 333
    monitor-exit v0

    .line 334
    return-void

    .line 333
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
