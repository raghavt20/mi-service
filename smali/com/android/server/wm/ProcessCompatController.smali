.class public Lcom/android/server/wm/ProcessCompatController;
.super Ljava/lang/Object;
.source "ProcessCompatController.java"

# interfaces
.implements Lcom/android/server/wm/ProcessCompatControllerStub;


# static fields
.field private static final TAG:Ljava/lang/String; = "ProcessCompat"


# instance fields
.field private mAtm:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mBounds:Landroid/graphics/Rect;

.field private mCurrentScale:F

.field private mFixedAspectRatio:F

.field private mGlobalScale:F

.field private mProcess:Lcom/android/server/wm/WindowProcessController;

.field private mState:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I

    .line 31
    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v0, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/android/server/wm/ProcessCompatController;->mBounds:Landroid/graphics/Rect;

    .line 52
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F

    .line 53
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F

    return-void
.end method

.method private updateState()V
    .locals 4

    .line 118
    iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I

    .line 119
    .local v0, "newState":I
    invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->canUseFixedAspectRatio()Z

    move-result v1

    const-string v2, "ProcessCompat"

    if-eqz v1, :cond_0

    .line 120
    or-int/lit8 v0, v0, 0x8

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    iget-object v3, v3, Lcom/android/server/wm/WindowProcessController;->mName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " fixed-aspect-ratio "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 123
    :cond_0
    and-int/lit8 v0, v0, -0x9

    .line 125
    :goto_0
    iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I

    if-eq v1, v0, :cond_1

    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    iget-object v3, v3, Lcom/android/server/wm/WindowProcessController;->mName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " comapt state "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "->"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I

    .line 128
    invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->sendCompatState()V

    .line 130
    :cond_1
    return-void
.end method


# virtual methods
.method public canUseFixedAspectRatio()Z
    .locals 3

    .line 133
    iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_3

    .line 134
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlip()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    iget v0, v0, Lcom/android/server/wm/WindowProcessController;->mUid:I

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ProcessCompatController;->mAtm:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 136
    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 138
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->shouldNotUseFixedAspectRatio()Z

    move-result v0

    if-nez v0, :cond_3

    .line 139
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 140
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/ProcessCompatController;->mAtm:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    iget-object v2, v2, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 144
    :cond_2
    const/4 v0, 0x1

    return v0

    .line 142
    :cond_3
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 106
    iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 107
    return-void

    .line 109
    :cond_0
    monitor-enter p0

    .line 110
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MiuiSizeCompat Scale:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mGlobalScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mCurrentScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 114
    monitor-exit p0

    .line 115
    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCompatConfiguration(Landroid/content/res/Configuration;)Landroid/content/res/Configuration;
    .locals 5
    .param p1, "config"    # Landroid/content/res/Configuration;

    .line 56
    invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->isFixedAspectRatioEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 57
    iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 58
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/wm/ProcessCompatController;->mAtm:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    iget-object v3, v3, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 59
    invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I

    move-result v0

    .line 60
    .local v0, "mode":I
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    iget-object v3, v3, Lcom/android/server/wm/WindowProcessController;->mName:Ljava/lang/String;

    iget-object v4, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    .line 61
    invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    .line 60
    invoke-virtual {v2, v3, v0, v4}, Lcom/android/server/wm/BoundsCompatUtils;->getGlobalScaleByName(Ljava/lang/String;ILandroid/graphics/Rect;)F

    move-result v2

    iput v2, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F

    .line 64
    .end local v0    # "mode":I
    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    .line 65
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mGlobalScale:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    .line 67
    iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F

    goto :goto_0

    .line 69
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F

    .line 71
    :goto_0
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F

    iget-object v2, p0, Lcom/android/server/wm/ProcessCompatController;->mAtm:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    .line 72
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v2

    iget v3, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F

    .line 71
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/android/server/wm/BoundsCompatUtils;->getCompatConfiguration(Landroid/content/res/Configuration;FLcom/android/server/wm/DisplayContent;F)Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0

    .line 74
    :cond_3
    return-object p1
.end method

.method public initBoundsCompatController(Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/WindowProcessController;)V
    .locals 3
    .param p1, "atm"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "process"    # Lcom/android/server/wm/WindowProcessController;

    .line 35
    iput-object p1, p0, Lcom/android/server/wm/ProcessCompatController;->mAtm:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 36
    iput-object p2, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    .line 37
    iget-object v0, p2, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 38
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v1

    invoke-interface {v1}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    invoke-static {}, Lcom/android/server/wm/BoundsCompatUtils;->getInstance()Lcom/android/server/wm/BoundsCompatUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/ProcessCompatController;->mAtm:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/BoundsCompatUtils;->getFlipCompatModeByApp(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 41
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F

    goto :goto_0

    .line 43
    :cond_0
    const v1, 0x3fdc3e06

    iput v1, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F

    goto :goto_0

    .line 47
    :cond_1
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getAspectRatio(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/android/server/wm/ProcessCompatController;->mFixedAspectRatio:F

    .line 49
    :goto_0
    invoke-direct {p0}, Lcom/android/server/wm/ProcessCompatController;->updateState()V

    .line 50
    return-void
.end method

.method public isFixedAspectRatioEnabled()Z
    .locals 2

    .line 79
    invoke-static {}, Landroid/app/servertransaction/BoundsCompatStub;->get()Landroid/app/servertransaction/BoundsCompatStub;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I

    invoke-virtual {v0, v1}, Landroid/app/servertransaction/BoundsCompatStub;->isFixedAspectRatioModeEnabled(I)Z

    move-result v0

    return v0
.end method

.method public onSetThread()V
    .locals 1

    .line 100
    iget v0, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/android/server/wm/ProcessCompatController;->sendCompatState()V

    .line 103
    :cond_0
    return-void
.end method

.method public sendCompatState()V
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v0

    if-nez v0, :cond_0

    .line 84
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/app/servertransaction/ClientTransaction;->obtain(Landroid/app/IApplicationThread;Landroid/os/IBinder;)Landroid/app/servertransaction/ClientTransaction;

    move-result-object v0

    .line 87
    .local v0, "transaction":Landroid/app/servertransaction/ClientTransaction;
    iget v1, p0, Lcom/android/server/wm/ProcessCompatController;->mState:I

    iget-object v2, p0, Lcom/android/server/wm/ProcessCompatController;->mBounds:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/server/wm/ProcessCompatController;->mCurrentScale:F

    invoke-static {v1, v2, v3}, Landroid/app/servertransaction/BoundsCompatInfoChangeItem;->obtain(ILandroid/graphics/Rect;F)Landroid/app/servertransaction/BoundsCompatInfoChangeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ProcessCompatController;->mAtm:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    goto :goto_0

    .line 90
    :catch_0
    move-exception v1

    .line 93
    :goto_0
    return-void
.end method

.method public shouldNotUseFixedAspectRatio()Z
    .locals 4

    .line 148
    iget-object v0, p0, Lcom/android/server/wm/ProcessCompatController;->mProcess:Lcom/android/server/wm/WindowProcessController;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getActivities()Ljava/util/List;

    move-result-object v0

    .line 149
    .local v0, "mActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    const/4 v1, 0x0

    return v1

    .line 153
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 154
    .local v1, "lastIndex":I
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/ActivityRecord;

    .line 155
    .local v2, "topRecord":Lcom/android/server/wm/ActivityRecord;
    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v3

    return v3
.end method

.method public updateConfiguration(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .line 96
    invoke-direct {p0}, Lcom/android/server/wm/ProcessCompatController;->updateState()V

    .line 97
    return-void
.end method
