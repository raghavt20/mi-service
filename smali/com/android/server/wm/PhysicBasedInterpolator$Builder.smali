.class public final Lcom/android/server/wm/PhysicBasedInterpolator$Builder;
.super Ljava/lang/Object;
.source "PhysicBasedInterpolator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/PhysicBasedInterpolator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mDamping:F

.field private mResponse:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const v0, 0x3f733333    # 0.95f

    iput v0, p0, Lcom/android/server/wm/PhysicBasedInterpolator$Builder;->mDamping:F

    .line 35
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/android/server/wm/PhysicBasedInterpolator$Builder;->mResponse:F

    .line 38
    return-void
.end method


# virtual methods
.method public build()Lcom/android/server/wm/PhysicBasedInterpolator;
    .locals 3

    .line 51
    new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator;

    iget v1, p0, Lcom/android/server/wm/PhysicBasedInterpolator$Builder;->mDamping:F

    iget v2, p0, Lcom/android/server/wm/PhysicBasedInterpolator$Builder;->mResponse:F

    invoke-direct {v0, v1, v2}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    return-object v0
.end method

.method public setDamping(F)Lcom/android/server/wm/PhysicBasedInterpolator$Builder;
    .locals 0
    .param p1, "damping"    # F

    .line 41
    iput p1, p0, Lcom/android/server/wm/PhysicBasedInterpolator$Builder;->mDamping:F

    .line 42
    return-object p0
.end method

.method public setResponse(F)Lcom/android/server/wm/PhysicBasedInterpolator$Builder;
    .locals 0
    .param p1, "response"    # F

    .line 46
    iput p1, p0, Lcom/android/server/wm/PhysicBasedInterpolator$Builder;->mResponse:F

    .line 47
    return-object p0
.end method
