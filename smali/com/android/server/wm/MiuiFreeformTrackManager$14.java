class com.android.server.wm.MiuiFreeformTrackManager$14 implements java.lang.Runnable {
	 /* .source "MiuiFreeformTrackManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiFreeformTrackManager;->trackMiniWindowPinedMoveEvent(Ljava/lang/String;Ljava/lang/String;II)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiFreeformTrackManager this$0; //synthetic
final java.lang.String val$applicationName; //synthetic
final java.lang.String val$packageName; //synthetic
final Integer val$posX; //synthetic
final Integer val$posY; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiFreeformTrackManager$14 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiFreeformTrackManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 531 */
this.this$0 = p1;
this.val$packageName = p2;
this.val$applicationName = p3;
/* iput p4, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$14;->val$posX:I */
/* iput p5, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$14;->val$posY:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 533 */
v0 = this.this$0;
v0 = this.mFreeFormTrackLock;
/* monitor-enter v0 */
/* .line 535 */
try { // :try_start_0
v1 = this.this$0;
com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmITrackBinder ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 536 */
	 /* new-instance v1, Lorg/json/JSONObject; */
	 /* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
	 /* .line 537 */
	 /* .local v1, "jsonData":Lorg/json/JSONObject; */
	 /* const-string/jumbo v2, "tip" */
	 final String v3 = "621.2.1.1.21750"; // const-string v3, "621.2.1.1.21750"
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 538 */
	 v2 = this.this$0;
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$mputCommomParam ( v2,v1 );
	 /* .line 539 */
	 final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
	 final String v3 = "move_hidden_window"; // const-string v3, "move_hidden_window"
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 540 */
	 final String v2 = "app_package_name"; // const-string v2, "app_package_name"
	 v3 = this.val$packageName;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 541 */
	 final String v2 = "app_display_name"; // const-string v2, "app_display_name"
	 v3 = this.val$applicationName;
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 542 */
	 final String v2 = "hide_window_x_coordinate"; // const-string v2, "hide_window_x_coordinate"
	 /* iget v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$14;->val$posX:I */
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
	 /* .line 543 */
	 final String v2 = "hide_window_y_coordinate"; // const-string v2, "hide_window_y_coordinate"
	 /* iget v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$14;->val$posY:I */
	 (( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
	 /* .line 544 */
	 v2 = this.this$0;
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmITrackBinder ( v2 );
	 final String v3 = "31000000538"; // const-string v3, "31000000538"
	 final String v4 = "android"; // const-string v4, "android"
	 /* .line 545 */
	 (( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
	 /* .line 544 */
	 int v6 = 0; // const/4 v6, 0x0
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 549 */
} // .end local v1 # "jsonData":Lorg/json/JSONObject;
} // :cond_0
/* .line 550 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 547 */
/* :catch_0 */
/* move-exception v1 */
/* .line 548 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 550 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit v0 */
/* .line 551 */
return;
/* .line 550 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
