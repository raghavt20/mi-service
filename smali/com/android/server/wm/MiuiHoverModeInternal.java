public class com.android.server.wm.MiuiHoverModeInternal {
	 /* .source "MiuiHoverModeInternal.java" */
	 /* # direct methods */
	 public com.android.server.wm.MiuiHoverModeInternal ( ) {
		 /* .locals 0 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void adaptLetterboxInsets ( com.android.server.wm.ActivityRecord p0, android.graphics.Rect p1 ) {
		 /* .locals 0 */
		 /* .param p1, "activity" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p2, "letterboxInsets" # Landroid/graphics/Rect; */
		 /* .line 78 */
		 return;
	 } // .end method
	 public void calcHoverAnimatingColor ( Float[] p0 ) {
		 /* .locals 0 */
		 /* .param p1, "startColor" # [F */
		 /* .line 87 */
		 return;
	 } // .end method
	 public void computeHoverModeBounds ( android.content.res.Configuration p0, android.graphics.Rect p1, android.graphics.Rect p2, com.android.server.wm.ActivityRecord p3 ) {
		 /* .locals 0 */
		 /* .param p1, "newParentConfiguration" # Landroid/content/res/Configuration; */
		 /* .param p2, "parentBounds" # Landroid/graphics/Rect; */
		 /* .param p3, "mTmpBounds" # Landroid/graphics/Rect; */
		 /* .param p4, "record" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 29 */
		 return;
	 } // .end method
	 public void enterFreeformForHoverMode ( com.android.server.wm.Task p0, Boolean p1 ) {
		 /* .locals 0 */
		 /* .param p1, "task" # Lcom/android/server/wm/Task; */
		 /* .param p2, "enter" # Z */
		 /* .line 93 */
		 return;
	 } // .end method
	 public Integer getHoverModeRecommendRotation ( com.android.server.wm.DisplayContent p0 ) {
		 /* .locals 1 */
		 /* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
		 /* .line 96 */
		 int v0 = -1; // const/4 v0, -0x1
	 } // .end method
	 public Boolean isDeviceInOpenedOrHalfOpenedState ( ) {
		 /* .locals 1 */
		 /* .line 24 */
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 public void onActivityPipModeChangedForHoverMode ( Boolean p0, com.android.server.wm.ActivityRecord p1 ) {
		 /* .locals 0 */
		 /* .param p1, "inPip" # Z */
		 /* .param p2, "ar" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 81 */
		 return;
	 } // .end method
	 public void onArCreated ( com.android.server.wm.ActivityRecord p0, Boolean p1 ) {
		 /* .locals 0 */
		 /* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p2, "isUnStandardLaunchMode" # Z */
		 /* .line 21 */
		 return;
	 } // .end method
	 public void onArParentChanged ( com.android.server.wm.TaskFragment p0, com.android.server.wm.TaskFragment p1, com.android.server.wm.ActivityRecord p2 ) {
		 /* .locals 0 */
		 /* .param p1, "oldParent" # Lcom/android/server/wm/TaskFragment; */
		 /* .param p2, "newParent" # Lcom/android/server/wm/TaskFragment; */
		 /* .param p3, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 58 */
		 return;
	 } // .end method
	 public void onArStateChanged ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord$State p1 ) {
		 /* .locals 0 */
		 /* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p2, "state" # Lcom/android/server/wm/ActivityRecord$State; */
		 /* .line 66 */
		 return;
	 } // .end method
	 public void onArVisibleChanged ( com.android.server.wm.ActivityRecord p0, Boolean p1 ) {
		 /* .locals 0 */
		 /* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p2, "visible" # Z */
		 /* .line 36 */
		 return;
	 } // .end method
	 public void onDisplayOverrideConfigUpdate ( com.android.server.wm.DisplayContent p0 ) {
		 /* .locals 0 */
		 /* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
		 /* .line 69 */
		 return;
	 } // .end method
	 public void onFinishTransition ( ) {
		 /* .locals 0 */
		 /* .line 54 */
		 return;
	 } // .end method
	 public void onHoverModeRecentAnimStart ( ) {
		 /* .locals 0 */
		 /* .line 51 */
		 return;
	 } // .end method
	 public void onHoverModeTaskParentChanged ( com.android.server.wm.Task p0, com.android.server.wm.WindowContainer p1 ) {
		 /* .locals 0 */
		 /* .param p1, "task" # Lcom/android/server/wm/Task; */
		 /* .param p2, "newParent" # Lcom/android/server/wm/WindowContainer; */
		 /* .line 62 */
		 return;
	 } // .end method
	 public void onHoverModeTaskPrepareSurfaces ( com.android.server.wm.Task p0 ) {
		 /* .locals 0 */
		 /* .param p1, "task" # Lcom/android/server/wm/Task; */
		 /* .line 72 */
		 return;
	 } // .end method
	 public void onScreenRotationAnimationEnd ( ) {
		 /* .locals 0 */
		 /* .line 90 */
		 return;
	 } // .end method
	 public void onStopFreezingDisplayLocked ( ) {
		 /* .locals 0 */
		 /* .line 84 */
		 return;
	 } // .end method
	 public void onSystemReady ( com.android.server.wm.WindowManagerService p0 ) {
		 /* .locals 0 */
		 /* .param p1, "wms" # Lcom/android/server/wm/WindowManagerService; */
		 /* .line 18 */
		 return;
	 } // .end method
	 public void onTaskConfigurationChanged ( Integer p0, Integer p1 ) {
		 /* .locals 0 */
		 /* .param p1, "prevWindowingMode" # I */
		 /* .param p2, "overrideWindowingMode" # I */
		 /* .line 104 */
		 return;
	 } // .end method
	 public void resetTaskFragmentRequestWindowMode ( com.android.server.wm.TaskFragment p0, android.content.res.Configuration p1 ) {
		 /* .locals 0 */
		 /* .param p1, "tf" # Lcom/android/server/wm/TaskFragment; */
		 /* .param p2, "newParentConfiguration" # Landroid/content/res/Configuration; */
		 /* .line 45 */
		 return;
	 } // .end method
	 public void setOrientationForHoverMode ( com.android.server.wm.ActivityRecord p0, Integer p1, Integer p2 ) {
		 /* .locals 0 */
		 /* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p2, "requestOrientation" # I */
		 /* .param p3, "orientation" # I */
		 /* .line 48 */
		 return;
	 } // .end method
	 public void setRequestedWindowModeForHoverMode ( com.android.server.wm.ActivityRecord p0, android.content.res.Configuration p1 ) {
		 /* .locals 0 */
		 /* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p2, "newParentConfiguration" # Landroid/content/res/Configuration; */
		 /* .line 42 */
		 return;
	 } // .end method
	 public Boolean shouldHookHoverConfig ( android.content.res.Configuration p0, com.android.server.wm.ActivityRecord p1 ) {
		 /* .locals 1 */
		 /* .param p1, "newParentConfiguration" # Landroid/content/res/Configuration; */
		 /* .param p2, "record" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 32 */
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 public Boolean shouldHoverModeEnableSensor ( com.android.server.wm.DisplayContent p0 ) {
		 /* .locals 1 */
		 /* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
		 /* .line 100 */
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 public void updateHoverGuidePanel ( com.android.server.wm.WindowState p0, Boolean p1 ) {
		 /* .locals 0 */
		 /* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
		 /* .param p2, "add" # Z */
		 /* .line 75 */
		 return;
	 } // .end method
	 public void updateLastRotation ( com.android.server.wm.DisplayContent p0, Integer p1 ) {
		 /* .locals 0 */
		 /* .param p1, "dc" # Lcom/android/server/wm/DisplayContent; */
		 /* .param p2, "lastRotation" # I */
		 /* .line 39 */
		 return;
	 } // .end method
