.class public Lcom/android/server/wm/ScreenRotationAnimationImpl;
.super Ljava/lang/Object;
.source "ScreenRotationAnimationImpl.java"

# interfaces
.implements Lcom/android/server/wm/ScreenRotationAnimationStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;,
        Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;,
        Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;,
        Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;,
        Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;
    }
.end annotation


# static fields
.field public static final BLACK_SURFACE_INVALID_POSITION:I = -0x2710

.field public static final COVER_EGE:I = 0x320

.field public static final COVER_OFFSET:I = 0x1f4

.field private static OPAQUE_APPNAME_LIST:[Ljava/lang/String; = null

.field private static final SCALE_FACTOR:F = 0.94f

.field private static final TAG:Ljava/lang/String; = "ScreenRotationAnimationImpl"

.field public static final TYPE_APP:I = 0x2

.field public static final TYPE_BACKGROUND:I = 0x3

.field public static final TYPE_SCREEN_SHOT:I = 0x1

.field public static sTmpFloats:[F


# instance fields
.field private mAlphaDuration:I

.field private mAlphaInterpolator:Landroid/view/animation/Interpolator;

.field private mContext:Landroid/content/Context;

.field private mEnableRotationAnimation:Z

.field private mFirstPhaseDuration:I

.field private mFirstPhaseInterpolator:Landroid/view/animation/Interpolator;

.field private mLongDuration:I

.field private mLongEaseInterpolator:Landroid/view/animation/Interpolator;

.field private mMiddleDuration:I

.field private mMiddleEaseInterpolater:Landroid/view/animation/Interpolator;

.field private mMiuiScreenRotationMode:I

.field private final mQuartEaseOutInterpolator:Landroid/view/animation/Interpolator;

.field private mScaleDelayTime:I

.field private mScaleDuration:I

.field private mScaleFactor:F

.field private mScreenRotationDuration:I

.field private mShortAlphaDuration:I

.field private mShortAlphaInterpolator:Landroid/view/animation/Interpolator;

.field private mShortDuration:I

.field private mShortEaseInterpolater:Landroid/view/animation/Interpolator;

.field private final mSinEaseInOutInterpolator:Landroid/view/animation/Interpolator;

.field private mSinEaseOutInterpolator:Landroid/view/animation/Interpolator;

.field mSurfaceControlCoverBt:Landroid/view/SurfaceControl;

.field mSurfaceControlCoverLt:Landroid/view/SurfaceControl;

.field mSurfaceControlCoverRt:Landroid/view/SurfaceControl;

.field mSurfaceControlCoverTp:Landroid/view/SurfaceControl;

.field private mWms:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 47
    const/16 v0, 0x9

    new-array v0, v0, [F

    sput-object v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->sTmpFloats:[F

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseOutInterpolater;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseOutInterpolater;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSinEaseOutInterpolator:Landroid/view/animation/Interpolator;

    .line 55
    new-instance v0, Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseQuartOutInterpolator;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseQuartOutInterpolator;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mQuartEaseOutInterpolator:Landroid/view/animation/Interpolator;

    .line 57
    new-instance v0, Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseSineInOutInterpolator;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiRotationAnimationUtils$EaseSineInOutInterpolator;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSinEaseInOutInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method


# virtual methods
.method public createRotation180Enter()Landroid/view/animation/Animation;
    .locals 18

    .line 785
    move-object/from16 v0, p0

    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 787
    .local v1, "set":Landroid/view/animation/AnimationSet;
    invoke-static {}, Lcom/android/server/wm/MiuiRotationAnimationUtils;->getRotationDuration()I

    move-result v3

    .line 788
    .local v3, "screenrotationduration":I
    new-instance v11, Landroid/view/animation/RotateAnimation;

    const/high16 v5, 0x43340000    # 180.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v9, 0x1

    const/high16 v10, 0x3f000000    # 0.5f

    move-object v4, v11

    invoke-direct/range {v4 .. v10}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 791
    .local v4, "rotateAnimation":Landroid/view/animation/RotateAnimation;
    iget-object v5, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mQuartEaseOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 792
    int-to-long v5, v3

    invoke-virtual {v4, v5, v6}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 793
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 794
    invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillBefore(Z)V

    .line 795
    invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 796
    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 798
    new-instance v15, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;

    const/high16 v7, 0x3f800000    # 1.0f

    const v8, 0x3f70a3d7    # 0.94f

    const/high16 v9, 0x3f800000    # 1.0f

    const v10, 0x3f70a3d7    # 0.94f

    const/4 v11, 0x1

    const/high16 v12, 0x3f000000    # 0.5f

    const/4 v13, 0x1

    const/high16 v14, 0x3f000000    # 0.5f

    move-object v6, v15

    invoke-direct/range {v6 .. v14}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;-><init>(FFFFIFIF)V

    .line 803
    .local v6, "scalephase1":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;
    iget-object v7, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mQuartEaseOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v6, v7}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 804
    div-int/lit8 v7, v3, 0x3

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setDuration(J)V

    .line 805
    invoke-virtual {v6, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillAfter(Z)V

    .line 806
    invoke-virtual {v6, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillBefore(Z)V

    .line 807
    invoke-virtual {v6, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillEnabled(Z)V

    .line 808
    invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 810
    new-instance v7, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;

    const/high16 v11, 0x3f800000    # 1.0f

    const v12, 0x3f70a3d7    # 0.94f

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x1

    const/high16 v15, 0x3f000000    # 0.5f

    const/16 v16, 0x1

    const/high16 v17, 0x3f000000    # 0.5f

    move-object v9, v7

    invoke-direct/range {v9 .. v17}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;-><init>(FFFFIFIF)V

    .line 814
    .local v7, "scalephase2":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;
    iget-object v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSinEaseInOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v7, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 815
    div-int/lit8 v8, v3, 0x3

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setStartOffset(J)V

    .line 816
    mul-int/lit8 v8, v3, 0x2

    div-int/lit8 v8, v8, 0x3

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setDuration(J)V

    .line 817
    invoke-virtual {v7, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillAfter(Z)V

    .line 818
    invoke-virtual {v7, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillBefore(Z)V

    .line 819
    invoke-virtual {v7, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillEnabled(Z)V

    .line 820
    invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 822
    return-object v1
.end method

.method public createRotation180Exit()Landroid/view/animation/Animation;
    .locals 19

    .line 730
    move-object/from16 v0, p0

    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 732
    .local v1, "set":Landroid/view/animation/AnimationSet;
    invoke-static {}, Lcom/android/server/wm/MiuiRotationAnimationUtils;->getRotationDuration()I

    move-result v3

    .line 733
    .local v3, "screenrotationduration":I
    new-instance v11, Landroid/view/animation/RotateAnimation;

    const/4 v5, 0x0

    const/high16 v6, -0x3ccc0000    # -180.0f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v9, 0x1

    const/high16 v10, 0x3f000000    # 0.5f

    move-object v4, v11

    invoke-direct/range {v4 .. v10}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 736
    .local v4, "rotateAnimation":Landroid/view/animation/RotateAnimation;
    iget-object v5, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mQuartEaseOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 737
    int-to-long v5, v3

    invoke-virtual {v4, v5, v6}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 738
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 739
    invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillBefore(Z)V

    .line 740
    invoke-virtual {v4, v5}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 741
    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 743
    new-instance v6, Landroid/view/animation/AlphaAnimation;

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 744
    .local v6, "alphaAnimation":Landroid/view/animation/AlphaAnimation;
    iget-object v7, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mQuartEaseOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v6, v7}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 745
    div-int/lit8 v7, v3, 0x2

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 746
    invoke-virtual {v6, v5}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 747
    invoke-virtual {v6, v5}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    .line 748
    invoke-virtual {v6, v5}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    .line 749
    invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 751
    new-instance v7, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;

    const/high16 v10, 0x3f800000    # 1.0f

    const v11, 0x3f70a3d7    # 0.94f

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, 0x3f70a3d7    # 0.94f

    const/4 v14, 0x1

    const/high16 v15, 0x3f000000    # 0.5f

    const/16 v16, 0x1

    const/high16 v17, 0x3f000000    # 0.5f

    move-object v9, v7

    invoke-direct/range {v9 .. v17}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;-><init>(FFFFIFIF)V

    .line 756
    .local v7, "scalephase1":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;
    iget-object v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mQuartEaseOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v7, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 757
    div-int/lit8 v8, v3, 0x3

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setDuration(J)V

    .line 758
    invoke-virtual {v7, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillAfter(Z)V

    .line 759
    invoke-virtual {v7, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillBefore(Z)V

    .line 760
    invoke-virtual {v7, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillEnabled(Z)V

    .line 761
    invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 763
    new-instance v8, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x1

    const/high16 v16, 0x3f000000    # 0.5f

    const/16 v17, 0x1

    const/high16 v18, 0x3f000000    # 0.5f

    move-object v10, v8

    invoke-direct/range {v10 .. v18}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;-><init>(FFFFIFIF)V

    .line 768
    .local v8, "scalephase2":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;
    iget-object v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSinEaseInOutInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 769
    div-int/lit8 v9, v3, 0x3

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setStartOffset(J)V

    .line 770
    mul-int/lit8 v9, v3, 0x2

    div-int/lit8 v9, v9, 0x3

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setDuration(J)V

    .line 771
    invoke-virtual {v8, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillAfter(Z)V

    .line 772
    invoke-virtual {v8, v2}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillBefore(Z)V

    .line 773
    invoke-virtual {v8, v5}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScale180Animation;->setFillEnabled(Z)V

    .line 774
    invoke-virtual {v1, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 776
    return-object v1
.end method

.method public createRotationEnterAnimation(IIII)Landroid/view/animation/Animation;
    .locals 25
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "originRotation"    # I
    .param p4, "curRotation"    # I

    .line 260
    move-object/from16 v0, p0

    move/from16 v1, p4

    new-instance v2, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 262
    .local v2, "set":Landroid/view/animation/AnimationSet;
    move/from16 v4, p2

    int-to-float v5, v4

    move/from16 v6, p1

    int-to-float v7, v6

    div-float/2addr v5, v7

    .line 263
    .local v5, "scale":F
    invoke-static/range {p3 .. p4}, Landroid/util/RotationUtils;->deltaRotation(II)I

    move-result v7

    .line 265
    .local v7, "delta":I
    const/4 v8, 0x3

    const/4 v15, 0x1

    if-eq v1, v15, :cond_1

    if-ne v1, v8, :cond_0

    goto :goto_0

    .line 270
    :cond_0
    move/from16 v9, p1

    .line 271
    .local v9, "curWidth":I
    move/from16 v10, p2

    move v14, v9

    move v13, v10

    .local v10, "curHeight":I
    goto :goto_1

    .line 267
    .end local v9    # "curWidth":I
    .end local v10    # "curHeight":I
    :cond_1
    :goto_0
    move/from16 v9, p2

    .line 268
    .restart local v9    # "curWidth":I
    move/from16 v10, p1

    move v14, v9

    move v13, v10

    .line 274
    .end local v9    # "curWidth":I
    .local v13, "curHeight":I
    .local v14, "curWidth":I
    :goto_1
    new-instance v9, Landroid/view/animation/RotateAnimation;

    .line 275
    if-ne v7, v8, :cond_2

    const/high16 v8, 0x42b40000    # 90.0f

    goto :goto_2

    :cond_2
    const/high16 v8, -0x3d4c0000    # -90.0f

    :goto_2
    move/from16 v17, v8

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/high16 v20, 0x3f000000    # 0.5f

    const/16 v21, 0x1

    const/high16 v22, 0x3f000000    # 0.5f

    move-object/from16 v16, v9

    invoke-direct/range {v16 .. v22}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    move-object v12, v9

    .line 278
    .local v12, "rotateAnimation":Landroid/view/animation/RotateAnimation;
    iget-object v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiddleEaseInterpolater:Landroid/view/animation/Interpolator;

    invoke-virtual {v12, v8}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 279
    iget v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiddleDuration:I

    int-to-long v8, v8

    invoke-virtual {v12, v8, v9}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 280
    invoke-virtual {v12, v15}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 281
    invoke-virtual {v12, v15}, Landroid/view/animation/RotateAnimation;->setFillBefore(Z)V

    .line 282
    invoke-virtual {v12, v15}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 283
    invoke-virtual {v2, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 285
    new-instance v8, Landroid/view/animation/ClipRectAnimation;

    int-to-float v9, v14

    int-to-float v10, v13

    div-float/2addr v10, v5

    sub-float/2addr v9, v10

    float-to-int v9, v9

    div-int/lit8 v17, v9, 0x2

    int-to-float v9, v13

    int-to-float v10, v14

    div-float/2addr v10, v5

    sub-float/2addr v9, v10

    float-to-int v9, v9

    div-int/lit8 v18, v9, 0x2

    int-to-float v9, v14

    int-to-float v10, v13

    div-float/2addr v10, v5

    add-float/2addr v9, v10

    float-to-int v9, v9

    div-int/lit8 v19, v9, 0x2

    int-to-float v9, v13

    int-to-float v10, v14

    div-float/2addr v10, v5

    add-float/2addr v9, v10

    float-to-int v9, v9

    div-int/lit8 v20, v9, 0x2

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v16, v8

    move/from16 v23, v14

    move/from16 v24, v13

    invoke-direct/range {v16 .. v24}, Landroid/view/animation/ClipRectAnimation;-><init>(IIIIIIII)V

    move-object v11, v8

    .line 291
    .local v11, "clipRectAnimation":Landroid/view/animation/ClipRectAnimation;
    iget-object v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortEaseInterpolater:Landroid/view/animation/Interpolator;

    invoke-virtual {v11, v8}, Landroid/view/animation/ClipRectAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 292
    iget v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I

    int-to-long v8, v8

    invoke-virtual {v11, v8, v9}, Landroid/view/animation/ClipRectAnimation;->setDuration(J)V

    .line 293
    invoke-virtual {v11, v15}, Landroid/view/animation/ClipRectAnimation;->setFillAfter(Z)V

    .line 294
    invoke-virtual {v11, v15}, Landroid/view/animation/ClipRectAnimation;->setFillBefore(Z)V

    .line 295
    invoke-virtual {v11, v15}, Landroid/view/animation/ClipRectAnimation;->setFillEnabled(Z)V

    .line 296
    invoke-virtual {v2, v11}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 298
    new-instance v17, Landroid/view/animation/ScaleAnimation;

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v16, 0x3f800000    # 1.0f

    const/16 v18, 0x1

    const/high16 v19, 0x3f000000    # 0.5f

    const/16 v20, 0x1

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v8, v17

    move v9, v5

    move-object/from16 v22, v11

    .end local v11    # "clipRectAnimation":Landroid/view/animation/ClipRectAnimation;
    .local v22, "clipRectAnimation":Landroid/view/animation/ClipRectAnimation;
    move v11, v5

    move-object/from16 v23, v12

    .end local v12    # "rotateAnimation":Landroid/view/animation/RotateAnimation;
    .local v23, "rotateAnimation":Landroid/view/animation/RotateAnimation;
    move/from16 v12, v16

    .end local v13    # "curHeight":I
    .local v24, "curHeight":I
    move/from16 v13, v18

    move/from16 v18, v14

    .end local v14    # "curWidth":I
    .local v18, "curWidth":I
    move/from16 v14, v19

    move/from16 v15, v20

    move/from16 v16, v21

    invoke-direct/range {v8 .. v16}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 303
    .local v8, "scaleAnimation":Landroid/view/animation/ScaleAnimation;
    iget-object v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortEaseInterpolater:Landroid/view/animation/Interpolator;

    invoke-virtual {v8, v9}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 304
    iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 305
    invoke-virtual {v8, v3}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 306
    invoke-virtual {v8, v3}, Landroid/view/animation/ScaleAnimation;->setFillBefore(Z)V

    .line 307
    const/4 v3, 0x1

    invoke-virtual {v8, v3}, Landroid/view/animation/ScaleAnimation;->setFillEnabled(Z)V

    .line 308
    invoke-virtual {v2, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 310
    new-instance v16, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;

    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleFactor:F

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x1

    const/high16 v13, 0x3f000000    # 0.5f

    const/4 v14, 0x1

    const/high16 v15, 0x3f000000    # 0.5f

    move-object/from16 v9, v16

    invoke-direct/range {v9 .. v15}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;-><init>(FFIFIF)V

    .line 314
    .local v9, "screenScaleAnimation":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;
    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setDuration(J)V

    .line 315
    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mFirstPhaseDuration:I

    invoke-virtual {v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFirstPhaseDuration(I)V

    .line 316
    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongDuration:I

    invoke-virtual {v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setSecPhaseDuration(I)V

    .line 317
    iget-object v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongEaseInterpolator:Landroid/view/animation/Interpolator;

    iget-object v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mFirstPhaseInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v9, v10, v11}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setAnimationInterpolator(Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V

    .line 319
    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I

    int-to-float v10, v10

    const/high16 v11, 0x3f800000    # 1.0f

    mul-float/2addr v10, v11

    iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    invoke-virtual {v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setScaleBreakOffset(F)V

    .line 321
    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I

    invoke-virtual {v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setScaleDelayTime(I)V

    .line 322
    invoke-virtual {v9, v3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillAfter(Z)V

    .line 323
    invoke-virtual {v9, v3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillBefore(Z)V

    .line 324
    invoke-virtual {v9, v3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillEnabled(Z)V

    .line 325
    invoke-virtual {v2, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 327
    return-object v2
.end method

.method public createRotationExitAnimation(IIII)Landroid/view/animation/Animation;
    .locals 29
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "originRotation"    # I
    .param p4, "curRotation"    # I

    .line 173
    move-object/from16 v0, p0

    move/from16 v1, p4

    new-instance v2, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 175
    .local v2, "set":Landroid/view/animation/AnimationSet;
    move/from16 v4, p2

    int-to-float v5, v4

    move/from16 v6, p1

    int-to-float v7, v6

    div-float/2addr v5, v7

    .line 176
    .local v5, "scale":F
    invoke-static/range {p3 .. p4}, Landroid/util/RotationUtils;->deltaRotation(II)I

    move-result v7

    .line 178
    .local v7, "delta":I
    const/4 v9, 0x2

    const/4 v12, 0x1

    if-eq v1, v12, :cond_1

    const/4 v8, 0x3

    if-ne v1, v8, :cond_0

    goto :goto_0

    .line 186
    :cond_0
    move/from16 v8, p1

    .line 187
    .local v8, "curWidth":I
    move/from16 v10, p2

    .line 188
    .local v10, "curHeight":I
    iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I

    if-ne v11, v9, :cond_2

    .line 189
    iget-object v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortAlphaInterpolator:Landroid/view/animation/Interpolator;

    iput-object v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaInterpolator:Landroid/view/animation/Interpolator;

    .line 190
    iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortAlphaDuration:I

    iput v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaDuration:I

    goto :goto_1

    .line 180
    .end local v8    # "curWidth":I
    .end local v10    # "curHeight":I
    :cond_1
    :goto_0
    move/from16 v8, p2

    .line 181
    .restart local v8    # "curWidth":I
    move/from16 v10, p1

    .line 182
    .restart local v10    # "curHeight":I
    iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I

    if-ne v11, v9, :cond_2

    .line 183
    iget v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDuration:I

    iput v11, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaDuration:I

    .line 194
    :cond_2
    :goto_1
    move v15, v10

    move v10, v8

    .end local v8    # "curWidth":I
    .local v10, "curWidth":I
    .local v15, "curHeight":I
    new-instance v8, Landroid/view/animation/RotateAnimation;

    const/16 v17, 0x0

    .line 195
    if-ne v7, v12, :cond_3

    const/high16 v11, 0x42b40000    # 90.0f

    goto :goto_2

    :cond_3
    const/high16 v11, -0x3d4c0000    # -90.0f

    :goto_2
    move/from16 v18, v11

    const/16 v19, 0x1

    const/high16 v20, 0x3f000000    # 0.5f

    const/16 v21, 0x1

    const/high16 v22, 0x3f000000    # 0.5f

    move-object/from16 v16, v8

    invoke-direct/range {v16 .. v22}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    move-object v14, v8

    .line 198
    .local v14, "rotateAnimation":Landroid/view/animation/RotateAnimation;
    iget-object v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiddleEaseInterpolater:Landroid/view/animation/Interpolator;

    invoke-virtual {v14, v8}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 199
    iget v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiddleDuration:I

    int-to-long v3, v8

    invoke-virtual {v14, v3, v4}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 200
    invoke-virtual {v14, v12}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 201
    invoke-virtual {v14, v12}, Landroid/view/animation/RotateAnimation;->setFillBefore(Z)V

    .line 202
    invoke-virtual {v14, v12}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 203
    invoke-virtual {v2, v14}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 205
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-direct {v3, v13, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 206
    .local v3, "alphaAnimation":Landroid/view/animation/AlphaAnimation;
    iget-object v4, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 207
    iget v4, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaDuration:I

    move-object/from16 v16, v14

    .end local v14    # "rotateAnimation":Landroid/view/animation/RotateAnimation;
    .local v16, "rotateAnimation":Landroid/view/animation/RotateAnimation;
    int-to-long v13, v4

    invoke-virtual {v3, v13, v14}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 208
    invoke-virtual {v3, v12}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 209
    invoke-virtual {v3, v12}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    .line 210
    invoke-virtual {v3, v12}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    .line 211
    invoke-virtual {v2, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 213
    new-instance v4, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;

    move-object v8, v4

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v13, 0x1

    const/high16 v25, 0x3f800000    # 1.0f

    const/high16 v14, 0x3f000000    # 0.5f

    move-object/from16 v26, v16

    .end local v16    # "rotateAnimation":Landroid/view/animation/RotateAnimation;
    .local v26, "rotateAnimation":Landroid/view/animation/RotateAnimation;
    const/16 v16, 0x1

    move v11, v15

    .end local v15    # "curHeight":I
    .local v11, "curHeight":I
    move/from16 v15, v16

    const/high16 v16, 0x3f000000    # 0.5f

    const/16 v17, 0x0

    const/16 v18, 0x0

    int-to-float v12, v10

    int-to-float v13, v11

    div-float/2addr v13, v5

    sub-float/2addr v12, v13

    float-to-int v12, v12

    div-int/lit8 v21, v12, 0x2

    int-to-float v12, v11

    int-to-float v13, v10

    div-float/2addr v13, v5

    sub-float/2addr v12, v13

    float-to-int v12, v12

    div-int/lit8 v22, v12, 0x2

    int-to-float v12, v10

    int-to-float v13, v11

    div-float/2addr v13, v5

    add-float/2addr v12, v13

    float-to-int v12, v12

    div-int/lit8 v23, v12, 0x2

    int-to-float v12, v11

    int-to-float v13, v10

    div-float/2addr v13, v5

    add-float/2addr v12, v13

    float-to-int v12, v12

    div-int/lit8 v24, v12, 0x2

    move v9, v10

    move/from16 v28, v10

    .end local v10    # "curWidth":I
    .local v28, "curWidth":I
    move v10, v11

    const/4 v13, 0x1

    move v12, v5

    move/from16 v19, v28

    move/from16 v20, v11

    move/from16 v27, v11

    move v1, v13

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v13, 0x1

    .end local v11    # "curHeight":I
    .local v27, "curHeight":I
    invoke-direct/range {v8 .. v24}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;-><init>(IIFFIFIFIIIIIIII)V

    .line 222
    .local v4, "shotClipAnimation":Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;
    iget v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I

    int-to-long v8, v8

    invoke-virtual {v4, v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setDuration(J)V

    .line 223
    iget-object v8, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortEaseInterpolater:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 224
    invoke-virtual {v4, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setFillAfter(Z)V

    .line 225
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setFillBefore(Z)V

    .line 226
    invoke-virtual {v4, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ShotClipAnimation;->setFillEnabled(Z)V

    .line 227
    invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 229
    new-instance v8, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;

    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleFactor:F

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x1

    const/high16 v13, 0x3f000000    # 0.5f

    const/4 v14, 0x1

    const/high16 v15, 0x3f000000    # 0.5f

    move-object v9, v8

    invoke-direct/range {v9 .. v15}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;-><init>(FFIFIF)V

    .line 233
    .local v8, "screenScaleAnimation":Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;
    iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setDuration(J)V

    .line 234
    iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mFirstPhaseDuration:I

    invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFirstPhaseDuration(I)V

    .line 235
    iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongDuration:I

    invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setSecPhaseDuration(I)V

    .line 236
    iget-object v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongEaseInterpolator:Landroid/view/animation/Interpolator;

    iget-object v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mFirstPhaseInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v8, v9, v10}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setAnimationInterpolator(Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V

    .line 238
    iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I

    int-to-float v9, v9

    mul-float v9, v9, v25

    iget v10, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setScaleBreakOffset(F)V

    .line 240
    iget v9, v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I

    invoke-virtual {v8, v9}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setScaleDelayTime(I)V

    .line 241
    invoke-virtual {v8, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillAfter(Z)V

    .line 242
    invoke-virtual {v8, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillBefore(Z)V

    .line 243
    invoke-virtual {v8, v1}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenScaleAnimation;->setFillEnabled(Z)V

    .line 244
    invoke-virtual {v2, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 246
    return-object v2
.end method

.method public createScreenRotationSpec(Landroid/view/animation/Animation;II)Lcom/android/server/wm/LocalAnimationAdapter$AnimationSpec;
    .locals 1
    .param p1, "animation"    # Landroid/view/animation/Animation;
    .param p2, "originalWidth"    # I
    .param p3, "originalHeight"    # I

    .line 332
    new-instance v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;

    invoke-direct {v0, p1, p2, p3}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;-><init>(Landroid/view/animation/Animation;II)V

    return-object v0
.end method

.method public getDisplayRoundCornerRadius()I
    .locals 1

    .line 672
    sget v0, Lcom/android/server/wm/AppTransitionInjector;->DISPLAY_ROUND_CORNER_RADIUS:I

    return v0
.end method

.method public getIsCancelRotationAnimation()Z
    .locals 1

    .line 141
    iget-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mEnableRotationAnimation:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->isKeyguardShowingAndNotOccluded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getScreenRotationAnimationMode()I
    .locals 1

    .line 137
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I

    return v0
.end method

.method public init(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "wms"    # Lcom/android/server/wm/WindowManagerService;

    .line 84
    iput-object p1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    .line 85
    iput-object p2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mWms:Lcom/android/server/wm/WindowManagerService;

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 87
    const v1, 0x10e000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I

    .line 88
    new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator;

    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    .line 89
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10500ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    .line 90
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x10500ae

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v3

    invoke-direct {v0, v1, v3}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongEaseInterpolator:Landroid/view/animation/Interpolator;

    .line 91
    new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator;

    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    .line 92
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iget-object v3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    .line 93
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x10500af

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v3

    invoke-direct {v0, v1, v3}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiddleEaseInterpolater:Landroid/view/animation/Interpolator;

    .line 94
    new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator;

    iget-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    .line 95
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    .line 96
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x10500b0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortEaseInterpolater:Landroid/view/animation/Interpolator;

    .line 97
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSinEaseOutInterpolator:Landroid/view/animation/Interpolator;

    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaInterpolator:Landroid/view/animation/Interpolator;

    .line 98
    new-instance v0, Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseInInterpolater;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiRotationAnimationUtils$SineEaseInInterpolater;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortAlphaInterpolator:Landroid/view/animation/Interpolator;

    .line 99
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 100
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSinEaseOutInterpolator:Landroid/view/animation/Interpolator;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortEaseInterpolater:Landroid/view/animation/Interpolator;

    :goto_0
    iput-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mFirstPhaseInterpolator:Landroid/view/animation/Interpolator;

    .line 101
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 102
    const v2, 0x10e000a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongDuration:I

    .line 103
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 104
    const v2, 0x10e000c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiddleDuration:I

    .line 105
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 106
    const v2, 0x10e0011

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I

    .line 107
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 108
    const v2, 0x10e000e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDuration:I

    .line 109
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 110
    const v2, 0x10e000f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I

    .line 111
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 112
    const v2, 0x10e0009

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mAlphaDuration:I

    .line 113
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 114
    const v2, 0x10e0010

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortAlphaDuration:I

    .line 115
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mMiuiScreenRotationMode:I

    if-ne v0, v1, :cond_1

    .line 116
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDuration:I

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mShortDuration:I

    :goto_1
    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mFirstPhaseDuration:I

    .line 117
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 118
    const v1, 0x10500b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleFactor:F

    .line 119
    iget v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScaleDelayTime:I

    iget v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mLongDuration:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mScreenRotationDuration:I

    .line 120
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 121
    const v1, 0x1110158

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mEnableRotationAnimation:Z

    .line 122
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    const-string v1, "com.youku.phone"

    const-string v2, "com.miui.mediaviewer"

    const-string v3, "com.miui.video"

    if-eqz v0, :cond_2

    .line 123
    filled-new-array {v3, v2, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->OPAQUE_APPNAME_LIST:[Ljava/lang/String;

    goto :goto_2

    .line 128
    :cond_2
    const-string/jumbo v0, "tv.danmaku.bili"

    filled-new-array {v3, v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->OPAQUE_APPNAME_LIST:[Ljava/lang/String;

    .line 134
    :goto_2
    return-void
.end method

.method public isOpaqueHasVideo(Lcom/android/server/wm/DisplayContent;)Z
    .locals 1
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 719
    const/4 v0, 0x0

    return v0
.end method

.method public kill(Landroid/view/SurfaceControl$Transaction;)V
    .locals 2
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;

    .line 336
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverLt:Landroid/view/SurfaceControl;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 337
    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverLt:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 340
    :cond_0
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverLt:Landroid/view/SurfaceControl;

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverTp:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_3

    .line 343
    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverTp:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 346
    :cond_2
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverTp:Landroid/view/SurfaceControl;

    .line 348
    :cond_3
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverRt:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_5

    .line 349
    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 350
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverRt:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 352
    :cond_4
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverRt:Landroid/view/SurfaceControl;

    .line 354
    :cond_5
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverBt:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_7

    .line 355
    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 356
    iget-object v0, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverBt:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v0}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 358
    :cond_6
    iput-object v1, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl;->mSurfaceControlCoverBt:Landroid/view/SurfaceControl;

    .line 360
    :cond_7
    return-void
.end method

.method public loadEnterAnimation(IIIILandroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;)Landroid/view/animation/Animation;
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "originRotation"    # I
    .param p4, "curRotation"    # I
    .param p5, "surfaceControlBg"    # Landroid/view/SurfaceControl;
    .param p6, "t"    # Landroid/view/SurfaceControl$Transaction;

    .line 150
    invoke-virtual {p6, p5}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 151
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/server/wm/ScreenRotationAnimationImpl;->createRotationEnterAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public loadExitAnimation(IIII)Landroid/view/animation/Animation;
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "originRotation"    # I
    .param p4, "curRotation"    # I

    .line 145
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/server/wm/ScreenRotationAnimationImpl;->createRotationExitAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public loadRotation180Enter()Landroid/view/animation/Animation;
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl;->createRotation180Enter()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public loadRotation180Exit()Landroid/view/animation/Animation;
    .locals 1

    .line 155
    invoke-virtual {p0}, Lcom/android/server/wm/ScreenRotationAnimationImpl;->createRotation180Exit()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method
