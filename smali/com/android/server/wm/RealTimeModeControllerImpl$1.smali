.class Lcom/android/server/wm/RealTimeModeControllerImpl$1;
.super Landroid/os/Handler;
.source "RealTimeModeControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/RealTimeModeControllerImpl;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/RealTimeModeControllerImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/RealTimeModeControllerImpl;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/RealTimeModeControllerImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 120
    iput-object p1, p0, Lcom/android/server/wm/RealTimeModeControllerImpl$1;->this$0:Lcom/android/server/wm/RealTimeModeControllerImpl;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 123
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 124
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 126
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl$1;->this$0:Lcom/android/server/wm/RealTimeModeControllerImpl;

    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->-$$Nest$sfgetmContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/wm/RealTimeModeControllerImpl;->-$$Nest$mregisterCloudObserver(Lcom/android/server/wm/RealTimeModeControllerImpl;Landroid/content/Context;)V

    .line 127
    iget-object v0, p0, Lcom/android/server/wm/RealTimeModeControllerImpl$1;->this$0:Lcom/android/server/wm/RealTimeModeControllerImpl;

    invoke-static {v0}, Lcom/android/server/wm/RealTimeModeControllerImpl;->-$$Nest$mupdateCloudControlParas(Lcom/android/server/wm/RealTimeModeControllerImpl;)V

    .line 128
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->-$$Nest$smupdateGestureCloudControlParas()V

    .line 129
    nop

    .line 133
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
