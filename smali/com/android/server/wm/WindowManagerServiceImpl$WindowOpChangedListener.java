class com.android.server.wm.WindowManagerServiceImpl$WindowOpChangedListener implements android.app.AppOpsManager$OnOpChangedListener {
	 /* .source "WindowManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/WindowManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "WindowOpChangedListener" */
} // .end annotation
/* # static fields */
private static final Integer SCREEN_SHARE_PROTECTION_OPEN;
/* # instance fields */
private final android.content.Context mContext;
private final com.android.server.wm.WindowManagerService mWmService;
/* # direct methods */
public com.android.server.wm.WindowManagerServiceImpl$WindowOpChangedListener ( ) {
/* .locals 0 */
/* .param p1, "mWmService" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 575 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 576 */
this.mWmService = p1;
/* .line 577 */
this.mContext = p2;
/* .line 578 */
return;
} // .end method
static void lambda$onOpChanged$0 ( java.lang.String p0, Integer p1, com.android.server.wm.WindowState p2 ) { //synthethic
/* .locals 3 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "newMode" # I */
/* .param p2, "w" # Lcom/android/server/wm/WindowState; */
/* .line 602 */
(( com.android.server.wm.WindowState ) p2 ).getOwningPackage ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
v0 = android.text.TextUtils .equals ( p0,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 v0 = this.mWinAnimator;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 v0 = this.mWinAnimator;
		 v0 = this.mSurfaceController;
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 v0 = this.mWinAnimator;
			 v0 = this.mSurfaceController;
			 v0 = this.mSurfaceControl;
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 /* .line 605 */
				 v0 = this.mWinAnimator;
				 v0 = this.mSurfaceController;
				 v0 = this.mSurfaceControl;
				 /* .line 606 */
				 /* .local v0, "sc":Landroid/view/SurfaceControl; */
				 int v1 = 1; // const/4 v1, 0x1
				 /* if-ne p1, v1, :cond_0 */
				 /* .line 608 */
				 (( android.view.SurfaceControl ) v0 ).setScreenProjection ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl;->setScreenProjection(I)V
				 /* .line 610 */
			 } // :cond_0
			 int v1 = 0; // const/4 v1, 0x0
			 (( android.view.SurfaceControl ) v0 ).setScreenProjection ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl;->setScreenProjection(I)V
			 /* .line 612 */
		 } // :goto_0
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "MIUILOG- Adjust ShareProjectFlag for:"; // const-string v2, "MIUILOG- Adjust ShareProjectFlag for:"
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v2 = " newMode: "; // const-string v2, " newMode: "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v2 = "WindowManagerService"; // const-string v2, "WindowManagerService"
		 android.util.Slog .i ( v2,v1 );
		 /* .line 615 */
	 } // .end local v0 # "sc":Landroid/view/SurfaceControl;
} // :cond_1
return;
} // .end method
/* # virtual methods */
public void onOpChanged ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "op" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 583 */
v0 = this.mContext;
/* .line 584 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "screen_share_protection_on" */
/* .line 583 */
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,v3 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
/* move v1, v2 */
} // :goto_0
/* move v0, v1 */
/* .line 588 */
/* .local v0, "isScreenShareProtection":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 589 */
/* const/16 v1, 0x2739 */
android.app.AppOpsManager .opToPublicName ( v1 );
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 592 */
try { // :try_start_0
	 v3 = this.mWmService;
	 v3 = this.mContext;
	 (( android.content.Context ) v3 ).getPackageManager ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 /* .line 593 */
	 (( android.content.pm.PackageManager ) v3 ).getApplicationInfo ( p2, v2 ); // invoke-virtual {v3, p2, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
	 /* :try_end_0 */
	 /* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 597 */
	 /* .local v3, "appInfo":Landroid/content/pm/ApplicationInfo; */
	 /* nop */
	 /* .line 598 */
	 v4 = this.mWmService;
	 v4 = this.mAppOps;
	 /* iget v5, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
	 v1 = 	 (( android.app.AppOpsManager ) v4 ).noteOpNoThrow ( v1, v5, p2 ); // invoke-virtual {v4, v1, v5, p2}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;)I
	 /* .line 600 */
	 /* .local v1, "newMode":I */
	 v4 = this.mWmService;
	 v4 = this.mGlobalLock;
	 /* monitor-enter v4 */
	 /* .line 601 */
	 try { // :try_start_1
		 v5 = this.mWmService;
		 v5 = this.mRoot;
		 /* new-instance v6, Lcom/android/server/wm/WindowManagerServiceImpl$WindowOpChangedListener$$ExternalSyntheticLambda0; */
		 /* invoke-direct {v6, p2, v1}, Lcom/android/server/wm/WindowManagerServiceImpl$WindowOpChangedListener$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;I)V */
		 (( com.android.server.wm.RootWindowContainer ) v5 ).forAllWindows ( v6, v2 ); // invoke-virtual {v5, v6, v2}, Lcom/android/server/wm/RootWindowContainer;->forAllWindows(Ljava/util/function/Consumer;Z)V
		 /* .line 616 */
		 /* monitor-exit v4 */
		 /* :catchall_0 */
		 /* move-exception v2 */
		 /* monitor-exit v4 */
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* throw v2 */
		 /* .line 594 */
	 } // .end local v1 # "newMode":I
} // .end local v3 # "appInfo":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v1 */
/* .line 595 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
(( android.content.pm.PackageManager$NameNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 596 */
return;
/* .line 618 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :cond_1
} // :goto_1
return;
} // .end method
