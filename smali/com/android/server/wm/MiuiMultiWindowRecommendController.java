public class com.android.server.wm.MiuiMultiWindowRecommendController {
	 /* .source "MiuiMultiWindowRecommendController.java" */
	 /* # static fields */
	 private static final Integer MULTI_WINDOW_RECOMMEND_RADIUS;
	 public static final MULTI_WINDOW_RECOMMEND_SHADOW_COLOR;
	 public static final Float MULTI_WINDOW_RECOMMEND_SHADOW_OFFSETY;
	 public static final Float MULTI_WINDOW_RECOMMEND_SHADOW_OUTSET;
	 public static final Float MULTI_WINDOW_RECOMMEND_SHADOW_RADIUS;
	 public static final MULTI_WINDOW_RECOMMEND_SHADOW_RESET_COLOR;
	 public static final Integer MULTI_WINDOW_RECOMMEND_SHADOW_V2_COLOR;
	 public static final Float MULTI_WINDOW_RECOMMEND_SHADOW_V2_DISPERSION;
	 public static final Float MULTI_WINDOW_RECOMMEND_SHADOW_V2_OFFSET_X;
	 public static final Float MULTI_WINDOW_RECOMMEND_SHADOW_V2_OFFSET_Y;
	 public static final Float MULTI_WINDOW_RECOMMEND_SHADOW_V2_RADIUS;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.lang.Runnable dismissFreeFormRecommendViewRunnable;
	 private java.lang.Runnable dismissSplitScreenRecommendViewRunnable;
	 android.view.View$OnClickListener freeFormClickListener;
	 private Long mAnimationDelay;
	 private android.content.Context mContext;
	 private com.android.server.wm.RecommendDataEntry mFreeFormRecommendDataEntry;
	 private android.widget.RelativeLayout mFreeFormRecommendIconContainer;
	 private com.android.server.wm.FreeFormRecommendLayout mFreeFormRecommendLayout;
	 private android.view.WindowManager$LayoutParams mLayoutParams;
	 private Long mRecommendAutoDisapperTime;
	 com.android.server.wm.MiuiMultiWindowRecommendHelper mRecommendHelper;
	 com.android.server.wm.WindowManagerService mService;
	 private miuix.animation.base.AnimConfig mShowHideAnimConfig;
	 private com.android.server.wm.RecommendDataEntry mSpiltScreenRecommendDataEntry;
	 private android.widget.RelativeLayout mSplitScreenRecommendIconContainer;
	 private com.android.server.wm.SplitScreenRecommendLayout mSplitScreenRecommendLayout;
	 android.view.WindowManager mWindowManager;
	 java.lang.Runnable removeFreeFormRecomendRunnable;
	 java.lang.Runnable removeSplitScreenRecommendRunnable;
	 android.view.View$OnClickListener splitScreenClickListener;
	 /* # direct methods */
	 static android.content.Context -$$Nest$fgetmContext ( com.android.server.wm.MiuiMultiWindowRecommendController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mContext;
	 } // .end method
	 static com.android.server.wm.FreeFormRecommendLayout -$$Nest$fgetmFreeFormRecommendLayout ( com.android.server.wm.MiuiMultiWindowRecommendController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mFreeFormRecommendLayout;
	 } // .end method
	 static android.view.WindowManager$LayoutParams -$$Nest$fgetmLayoutParams ( com.android.server.wm.MiuiMultiWindowRecommendController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mLayoutParams;
	 } // .end method
	 static com.android.server.wm.RecommendDataEntry -$$Nest$fgetmSpiltScreenRecommendDataEntry ( com.android.server.wm.MiuiMultiWindowRecommendController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mSpiltScreenRecommendDataEntry;
	 } // .end method
	 static com.android.server.wm.SplitScreenRecommendLayout -$$Nest$fgetmSplitScreenRecommendLayout ( com.android.server.wm.MiuiMultiWindowRecommendController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mSplitScreenRecommendLayout;
	 } // .end method
	 static void -$$Nest$fputmFreeFormRecommendDataEntry ( com.android.server.wm.MiuiMultiWindowRecommendController p0, com.android.server.wm.RecommendDataEntry p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mFreeFormRecommendDataEntry = p1;
		 return;
	 } // .end method
	 static void -$$Nest$fputmFreeFormRecommendIconContainer ( com.android.server.wm.MiuiMultiWindowRecommendController p0, android.widget.RelativeLayout p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mFreeFormRecommendIconContainer = p1;
		 return;
	 } // .end method
	 static void -$$Nest$fputmFreeFormRecommendLayout ( com.android.server.wm.MiuiMultiWindowRecommendController p0, com.android.server.wm.FreeFormRecommendLayout p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mFreeFormRecommendLayout = p1;
		 return;
	 } // .end method
	 static void -$$Nest$fputmLayoutParams ( com.android.server.wm.MiuiMultiWindowRecommendController p0, android.view.WindowManager$LayoutParams p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mLayoutParams = p1;
		 return;
	 } // .end method
	 static void -$$Nest$fputmSpiltScreenRecommendDataEntry ( com.android.server.wm.MiuiMultiWindowRecommendController p0, com.android.server.wm.RecommendDataEntry p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mSpiltScreenRecommendDataEntry = p1;
		 return;
	 } // .end method
	 static void -$$Nest$fputmSplitScreenRecommendIconContainer ( com.android.server.wm.MiuiMultiWindowRecommendController p0, android.widget.RelativeLayout p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mSplitScreenRecommendIconContainer = p1;
		 return;
	 } // .end method
	 static void -$$Nest$fputmSplitScreenRecommendLayout ( com.android.server.wm.MiuiMultiWindowRecommendController p0, com.android.server.wm.SplitScreenRecommendLayout p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mSplitScreenRecommendLayout = p1;
		 return;
	 } // .end method
	 static void -$$Nest$menterSmallFreeForm ( com.android.server.wm.MiuiMultiWindowRecommendController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->enterSmallFreeForm()V */
		 return;
	 } // .end method
	 static void -$$Nest$menterSplitScreen ( com.android.server.wm.MiuiMultiWindowRecommendController p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->enterSplitScreen()V */
		 return;
	 } // .end method
	 static com.android.server.wm.MiuiMultiWindowRecommendController ( ) {
		 /* .locals 3 */
		 /* .line 58 */
		 /* nop */
		 /* .line 60 */
		 android.content.res.Resources .getSystem ( );
		 (( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
		 /* .line 59 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* const/high16 v2, 0x41900000 # 18.0f */
		 v0 = 		 android.util.TypedValue .applyDimension ( v1,v2,v0 );
		 /* float-to-int v0, v0 */
		 /* .line 65 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* new-array v1, v0, [F */
		 /* fill-array-data v1, :array_0 */
		 /* .line 66 */
		 /* new-array v0, v0, [F */
		 /* fill-array-data v0, :array_1 */
		 return;
		 /* nop */
		 /* :array_0 */
		 /* .array-data 4 */
		 /* 0x0 */
		 /* 0x0 */
		 /* 0x0 */
		 /* 0x0 */
	 } // .end array-data
	 /* :array_1 */
	 /* .array-data 4 */
	 /* 0x0 */
	 /* 0x0 */
	 /* 0x0 */
	 /* 0x3ecccccd # 0.4f */
} // .end array-data
} // .end method
public com.android.server.wm.MiuiMultiWindowRecommendController ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p3, "miuiMultiWindowRecommendHelper" # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
/* .line 74 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 50 */
/* new-instance v0, Lcom/android/server/wm/RecommendDataEntry; */
/* invoke-direct {v0}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V */
this.mSpiltScreenRecommendDataEntry = v0;
/* .line 51 */
/* new-instance v0, Lcom/android/server/wm/RecommendDataEntry; */
/* invoke-direct {v0}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V */
this.mFreeFormRecommendDataEntry = v0;
/* .line 55 */
/* const-wide/16 v0, 0x1388 */
/* iput-wide v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendAutoDisapperTime:J */
/* .line 56 */
/* const-wide/16 v0, 0x47e */
/* iput-wide v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mAnimationDelay:J */
/* .line 84 */
/* new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$1;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V */
this.splitScreenClickListener = v0;
/* .line 93 */
/* new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$2;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V */
this.removeSplitScreenRecommendRunnable = v0;
/* .line 105 */
/* new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$3;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V */
this.removeFreeFormRecomendRunnable = v0;
/* .line 148 */
/* new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$4; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$4;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V */
this.dismissSplitScreenRecommendViewRunnable = v0;
/* .line 163 */
/* new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$5; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$5;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V */
this.dismissFreeFormRecommendViewRunnable = v0;
/* .line 282 */
/* new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$8; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$8;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V */
this.freeFormClickListener = v0;
/* .line 75 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 76 */
	 this.mContext = p1;
	 /* .line 77 */
	 /* const-string/jumbo v0, "window" */
	 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/view/WindowManager; */
	 this.mWindowManager = v0;
	 /* .line 78 */
	 this.mService = p2;
	 /* .line 79 */
	 this.mRecommendHelper = p3;
	 /* .line 80 */
	 (( com.android.server.wm.MiuiMultiWindowRecommendController ) p0 ).initFolmeConfig ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->initFolmeConfig()V
	 /* .line 82 */
} // :cond_0
return;
} // .end method
private void enterSmallFreeForm ( ) {
/* .locals 5 */
/* .line 134 */
final String v0 = "MiuiMultiWindowRecommendController"; // const-string v0, "MiuiMultiWindowRecommendController"
final String v1 = "enterSmallFreeForm "; // const-string v1, "enterSmallFreeForm "
android.util.Slog .d ( v0,v1 );
/* .line 135 */
v0 = this.mFreeFormRecommendDataEntry;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 136 */
	 v0 = this.mService;
	 v0 = this.mAtmService;
	 v1 = this.mFreeFormRecommendDataEntry;
	 /* .line 138 */
	 v1 = 	 (( com.android.server.wm.RecommendDataEntry ) v1 ).getFreeFormTaskId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RecommendDataEntry;->getFreeFormTaskId()I
	 java.lang.Integer .valueOf ( v1 );
	 int v2 = 2; // const/4 v2, 0x2
	 java.lang.Integer .valueOf ( v2 );
	 /* new-instance v3, Landroid/graphics/Rect; */
	 /* invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V */
	 final String v4 = "enterSmallFreeFormByFreeFormRecommend"; // const-string v4, "enterSmallFreeFormByFreeFormRecommend"
	 /* filled-new-array {v1, v2, v4, v3}, [Ljava/lang/Object; */
	 /* .line 136 */
	 final String v2 = "launchMiniFreeFormWindowVersion2"; // const-string v2, "launchMiniFreeFormWindowVersion2"
	 android.util.MiuiMultiWindowUtils .invoke ( v0,v2,v1 );
	 /* .line 140 */
	 v0 = this.mRecommendHelper;
	 v0 = this.mFreeFormManagerService;
	 v0 = this.mFreeFormGestureController;
	 v0 = this.mTrackManager;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 141 */
		 v0 = this.mRecommendHelper;
		 v0 = this.mFreeFormManagerService;
		 v0 = this.mFreeFormGestureController;
		 v0 = this.mTrackManager;
		 v1 = this.mFreeFormRecommendDataEntry;
		 /* .line 142 */
		 (( com.android.server.wm.RecommendDataEntry ) v1 ).getFreeformPackageName ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;
		 v2 = this.mFreeFormRecommendDataEntry;
		 /* .line 143 */
		 (( com.android.server.wm.RecommendDataEntry ) v2 ).getFreeformPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;
		 (( com.android.server.wm.MiuiMultiWindowRecommendController ) p0 ).getApplicationName ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;
		 /* .line 142 */
		 (( com.android.server.wm.MiuiFreeformTrackManager ) v0 ).trackFreeFormRecommendClickEvent ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/MiuiFreeformTrackManager;->trackFreeFormRecommendClickEvent(Ljava/lang/String;Ljava/lang/String;)V
		 /* .line 146 */
	 } // :cond_0
	 return;
} // .end method
private void enterSplitScreen ( ) {
	 /* .locals 5 */
	 /* .line 118 */
	 final String v0 = "MiuiMultiWindowRecommendController"; // const-string v0, "MiuiMultiWindowRecommendController"
	 final String v1 = "enterSplitScreen "; // const-string v1, "enterSplitScreen "
	 android.util.Slog .d ( v0,v1 );
	 /* .line 119 */
	 v0 = this.mSpiltScreenRecommendDataEntry;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 120 */
		 v0 = this.mService;
		 v0 = this.mAtmService;
		 v0 = this.mTaskOrganizerController;
		 v1 = this.mSpiltScreenRecommendDataEntry;
		 /* .line 122 */
		 v1 = 		 (( com.android.server.wm.RecommendDataEntry ) v1 ).getPrimaryTaskId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryTaskId()I
		 java.lang.Integer .valueOf ( v1 );
		 v2 = this.mSpiltScreenRecommendDataEntry;
		 v2 = 		 (( com.android.server.wm.RecommendDataEntry ) v2 ).getSecondaryTaskId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryTaskId()I
		 java.lang.Integer .valueOf ( v2 );
		 /* filled-new-array {v1, v2}, [Ljava/lang/Object; */
		 /* .line 120 */
		 /* const-string/jumbo v2, "startTasksForSystem" */
		 android.util.MiuiMultiWindowUtils .invoke ( v0,v2,v1 );
		 /* .line 123 */
		 v0 = this.mRecommendHelper;
		 v0 = this.mFreeFormManagerService;
		 v0 = this.mFreeFormGestureController;
		 v0 = this.mTrackManager;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 124 */
			 v0 = this.mRecommendHelper;
			 v0 = this.mFreeFormManagerService;
			 v0 = this.mFreeFormGestureController;
			 v0 = this.mTrackManager;
			 /* new-instance v1, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
			 v2 = this.mSpiltScreenRecommendDataEntry;
			 /* .line 125 */
			 (( com.android.server.wm.RecommendDataEntry ) v2 ).getPrimaryPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 final String v2 = "_"; // const-string v2, "_"
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v3 = this.mSpiltScreenRecommendDataEntry;
			 /* .line 126 */
			 (( com.android.server.wm.RecommendDataEntry ) v3 ).getSecondaryPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;
			 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 /* new-instance v3, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
			 v4 = this.mSpiltScreenRecommendDataEntry;
			 /* .line 127 */
			 (( com.android.server.wm.RecommendDataEntry ) v4 ).getPrimaryPackageName ( ); // invoke-virtual {v4}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;
			 (( com.android.server.wm.MiuiMultiWindowRecommendController ) p0 ).getApplicationName ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;
			 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v3 = this.mSpiltScreenRecommendDataEntry;
			 /* .line 128 */
			 (( com.android.server.wm.RecommendDataEntry ) v3 ).getSecondaryPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;
			 (( com.android.server.wm.MiuiMultiWindowRecommendController ) p0 ).getApplicationName ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 /* .line 125 */
			 (( com.android.server.wm.MiuiFreeformTrackManager ) v0 ).trackSplitScreenRecommendClickEvent ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/MiuiFreeformTrackManager;->trackSplitScreenRecommendClickEvent(Ljava/lang/String;Ljava/lang/String;)V
			 /* .line 131 */
		 } // :cond_0
		 return;
	 } // .end method
	 public static Integer getDisplayCutoutHeight ( com.android.server.wm.DisplayFrames p0 ) {
		 /* .locals 4 */
		 /* .param p0, "displayFrames" # Lcom/android/server/wm/DisplayFrames; */
		 /* .line 517 */
		 /* if-nez p0, :cond_0 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 518 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 519 */
	 /* .local v0, "displayCutoutHeight":I */
	 v1 = this.mInsetsState;
	 (( android.view.InsetsState ) v1 ).getDisplayCutout ( ); // invoke-virtual {v1}, Landroid/view/InsetsState;->getDisplayCutout()Landroid/view/DisplayCutout;
	 /* .line 520 */
	 /* .local v1, "cutout":Landroid/view/DisplayCutout; */
	 /* iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I */
	 /* if-nez v2, :cond_1 */
	 /* .line 521 */
	 v0 = 	 (( android.view.DisplayCutout ) v1 ).getSafeInsetTop ( ); // invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetTop()I
	 /* .line 522 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 523 */
v0 = (( android.view.DisplayCutout ) v1 ).getSafeInsetBottom ( ); // invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetBottom()I
/* .line 524 */
} // :cond_2
/* iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_3 */
/* .line 525 */
v0 = (( android.view.DisplayCutout ) v1 ).getSafeInsetLeft ( ); // invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetLeft()I
/* .line 526 */
} // :cond_3
/* iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v2, v3, :cond_4 */
/* .line 527 */
v0 = (( android.view.DisplayCutout ) v1 ).getSafeInsetRight ( ); // invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetRight()I
/* .line 529 */
} // :cond_4
} // :goto_0
} // .end method
public static Integer getStatusBarHeight ( com.android.server.wm.InsetsStateController p0, Boolean p1 ) {
/* .locals 7 */
/* .param p0, "insetsStateController" # Lcom/android/server/wm/InsetsStateController; */
/* .param p1, "ignoreVisibility" # Z */
/* .line 533 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 534 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 535 */
/* .local v0, "statusBarHeight":I */
(( com.android.server.wm.InsetsStateController ) p0 ).getSourceProviders ( ); // invoke-virtual {p0}, Lcom/android/server/wm/InsetsStateController;->getSourceProviders()Landroid/util/SparseArray;
/* .line 536 */
/* .local v1, "providers":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/wm/InsetsSourceProvider;>;" */
v2 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_4 */
/* .line 537 */
(( android.util.SparseArray ) v1 ).valueAt ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/InsetsSourceProvider; */
/* .line 538 */
/* .local v3, "provider":Lcom/android/server/wm/InsetsSourceProvider; */
(( com.android.server.wm.InsetsSourceProvider ) v3 ).getSource ( ); // invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;
v4 = (( android.view.InsetsSource ) v4 ).getType ( ); // invoke-virtual {v4}, Landroid/view/InsetsSource;->getType()I
v5 = android.view.WindowInsets$Type .statusBars ( );
/* if-eq v4, v5, :cond_1 */
/* .line 539 */
/* .line 541 */
} // :cond_1
/* if-nez p1, :cond_2 */
(( com.android.server.wm.InsetsSourceProvider ) v3 ).getSource ( ); // invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;
v4 = (( android.view.InsetsSource ) v4 ).isVisible ( ); // invoke-virtual {v4}, Landroid/view/InsetsSource;->isVisible()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 542 */
} // :cond_2
(( com.android.server.wm.InsetsSourceProvider ) v3 ).getSource ( ); // invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;
(( android.view.InsetsSource ) v4 ).getFrame ( ); // invoke-virtual {v4}, Landroid/view/InsetsSource;->getFrame()Landroid/graphics/Rect;
/* .line 543 */
/* .local v4, "frame":Landroid/graphics/Rect; */
if ( v4 != null) { // if-eqz v4, :cond_3
v5 = (( android.graphics.Rect ) v4 ).isEmpty ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v5, :cond_3 */
/* .line 544 */
v5 = (( android.graphics.Rect ) v4 ).height ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->height()I
v6 = (( android.graphics.Rect ) v4 ).width ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->width()I
v0 = java.lang.Math .min ( v5,v6 );
/* .line 536 */
} // .end local v3 # "provider":Lcom/android/server/wm/InsetsSourceProvider;
} // .end local v4 # "frame":Landroid/graphics/Rect;
} // :cond_3
} // :goto_1
/* add-int/lit8 v2, v2, -0x1 */
/* .line 549 */
} // .end local v2 # "i":I
} // :cond_4
} // .end method
public static android.graphics.drawable.Drawable loadDrawableByPackageName ( java.lang.String p0, android.content.Context p1, Integer p2 ) {
/* .locals 4 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "userId" # I */
/* .line 179 */
int v0 = 0; // const/4 v0, 0x0
/* .line 181 */
/* .local v0, "drawable":Landroid/graphics/drawable/Drawable; */
try { // :try_start_0
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 182 */
/* .local v1, "packageManager":Landroid/content/pm/PackageManager; */
(( android.content.pm.PackageManager ) v1 ).getApplicationIcon ( p0 ); // invoke-virtual {v1, p0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
miui.os.UserHandleEx .getUserHandle ( p2 );
(( android.content.pm.PackageManager ) v1 ).getUserBadgedIcon ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getUserBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v2 */
/* .line 185 */
} // .end local v1 # "packageManager":Landroid/content/pm/PackageManager;
/* .line 183 */
/* :catch_0 */
/* move-exception v1 */
/* .line 184 */
/* .local v1, "unused":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "not found application by pkgName "; // const-string v3, "not found application by pkgName "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiMultiWindowRecommendController"; // const-string v3, "MiuiMultiWindowRecommendController"
android.util.Slog .e ( v3,v2 );
/* .line 186 */
} // .end local v1 # "unused":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
} // .end method
private void setRadius ( android.view.View p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "content" # Landroid/view/View; */
/* .param p2, "radius" # F */
/* .line 458 */
/* new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$12; */
/* invoke-direct {v0, p0, p2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$12;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;F)V */
(( android.view.View ) p1 ).setOutlineProvider ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V
/* .line 464 */
int v0 = 1; // const/4 v0, 0x1
(( android.view.View ) p1 ).setClipToOutline ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->setClipToOutline(Z)V
/* .line 465 */
return;
} // .end method
/* # virtual methods */
public void addFreeFormRecommendView ( com.android.server.wm.RecommendDataEntry p0 ) {
/* .locals 2 */
/* .param p1, "recommendDataEntry" # Lcom/android/server/wm/RecommendDataEntry; */
/* .line 241 */
final String v0 = "MiuiMultiWindowRecommendController"; // const-string v0, "MiuiMultiWindowRecommendController"
final String v1 = " addFreeFormRecommendView start"; // const-string v1, " addFreeFormRecommendView start"
android.util.Slog .d ( v0,v1 );
/* .line 242 */
v0 = this.mService;
v0 = this.mAtmService;
v0 = this.mUiHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V */
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->post(Ljava/lang/Runnable;)Z
/* .line 279 */
return;
} // .end method
public void addSplitScreenRecommendView ( com.android.server.wm.RecommendDataEntry p0 ) {
/* .locals 4 */
/* .param p1, "recommendDataEntry" # Lcom/android/server/wm/RecommendDataEntry; */
/* .line 190 */
final String v0 = "MiuiMultiWindowRecommendController"; // const-string v0, "MiuiMultiWindowRecommendController"
final String v1 = " addSplitScreenRecommendView start"; // const-string v1, " addSplitScreenRecommendView start"
android.util.Slog .d ( v0,v1 );
/* .line 191 */
v0 = this.mService;
v0 = this.mAtmService;
v0 = this.mUiHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V */
/* iget-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mAnimationDelay:J */
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 238 */
return;
} // .end method
public java.lang.String getApplicationName ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 501 */
int v0 = 0; // const/4 v0, 0x0
/* .line 502 */
/* .local v0, "packageManager":Landroid/content/pm/PackageManager; */
int v1 = 0; // const/4 v1, 0x0
/* .line 504 */
/* .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
try { // :try_start_0
v2 = this.mService;
v2 = this.mAtmService;
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* move-object v0, v2 */
/* .line 505 */
int v2 = 0; // const/4 v2, 0x0
(( android.content.pm.PackageManager ) v0 ).getApplicationInfo ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v1, v2 */
/* .line 508 */
/* .line 506 */
/* :catch_0 */
/* move-exception v2 */
/* .line 507 */
/* .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
int v1 = 0; // const/4 v1, 0x0
/* .line 509 */
} // .end local v2 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
final String v2 = ""; // const-string v2, ""
/* .line 510 */
/* .local v2, "applicationName":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 511 */
(( android.content.pm.PackageManager ) v0 ).getApplicationLabel ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
/* move-object v2, v3 */
/* check-cast v2, Ljava/lang/String; */
/* .line 513 */
} // :cond_0
} // .end method
public com.android.server.wm.SplitScreenRecommendLayout getSplitScreenRecommendLayout ( ) {
/* .locals 1 */
/* .line 454 */
v0 = this.mSplitScreenRecommendLayout;
} // .end method
public Boolean inFreeFormRecommendState ( ) {
/* .locals 1 */
/* .line 483 */
v0 = this.mFreeFormRecommendDataEntry;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 484 */
v0 = (( com.android.server.wm.RecommendDataEntry ) v0 ).getFreeFormRecommendState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getFreeFormRecommendState()Z
/* .line 486 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean inSplitScreenRecommendState ( ) {
/* .locals 1 */
/* .line 468 */
v0 = this.mSpiltScreenRecommendDataEntry;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 469 */
v0 = (( com.android.server.wm.RecommendDataEntry ) v0 ).getSplitScreenRecommendState ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getSplitScreenRecommendState()Z
/* .line 471 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void initFolmeConfig ( ) {
/* .locals 4 */
/* .line 304 */
/* new-instance v0, Lmiuix/animation/base/AnimConfig; */
/* invoke-direct {v0}, Lmiuix/animation/base/AnimConfig;-><init>()V */
int v1 = 2; // const/4 v1, 0x2
/* new-array v1, v1, [F */
/* fill-array-data v1, :array_0 */
int v2 = -2; // const/4 v2, -0x2
(( miuix.animation.base.AnimConfig ) v0 ).setEase ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lmiuix/animation/base/AnimConfig;->setEase(I[F)Lmiuix/animation/base/AnimConfig;
int v1 = 1; // const/4 v1, 0x1
/* new-array v1, v1, [Lmiuix/animation/listener/TransitionListener; */
/* new-instance v2, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9; */
/* invoke-direct {v2, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V */
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
/* .line 305 */
(( miuix.animation.base.AnimConfig ) v0 ).addListeners ( v1 ); // invoke-virtual {v0, v1}, Lmiuix/animation/base/AnimConfig;->addListeners([Lmiuix/animation/listener/TransitionListener;)Lmiuix/animation/base/AnimConfig;
this.mShowHideAnimConfig = v0;
/* .line 334 */
return;
/* :array_0 */
/* .array-data 4 */
/* 0x3f400000 # 0.75f */
/* 0x3eb33333 # 0.35f */
} // .end array-data
} // .end method
public void removeFreeFormRecommendView ( ) {
/* .locals 2 */
/* .line 432 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " removeFreeFormRecommendView mFreeFormRecommendLayout= "; // const-string v1, " removeFreeFormRecommendView mFreeFormRecommendLayout= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mFreeFormRecommendLayout;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMultiWindowRecommendController"; // const-string v1, "MiuiMultiWindowRecommendController"
android.util.Slog .d ( v1,v0 );
/* .line 433 */
v0 = this.mService;
v0 = this.mAtmService;
v0 = this.mUiHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController$11; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$11;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V */
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->post(Ljava/lang/Runnable;)Z
/* .line 451 */
return;
} // .end method
public void removeFreeFormRecommendViewForTimer ( ) {
/* .locals 4 */
/* .line 298 */
final String v0 = "MiuiMultiWindowRecommendController"; // const-string v0, "MiuiMultiWindowRecommendController"
final String v1 = "removeFreeFormRecommendViewForTimer"; // const-string v1, "removeFreeFormRecommendViewForTimer"
android.util.Slog .d ( v0,v1 );
/* .line 299 */
v0 = this.mService;
v0 = this.mAtmService;
v0 = this.mUiHandler;
v1 = this.dismissFreeFormRecommendViewRunnable;
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 300 */
v0 = this.mService;
v0 = this.mAtmService;
v0 = this.mUiHandler;
v1 = this.dismissFreeFormRecommendViewRunnable;
/* iget-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendAutoDisapperTime:J */
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 301 */
return;
} // .end method
public void removeRecommendView ( ) {
/* .locals 0 */
/* .line 496 */
(( com.android.server.wm.MiuiMultiWindowRecommendController ) p0 ).removeFreeFormRecommendView ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V
/* .line 497 */
(( com.android.server.wm.MiuiMultiWindowRecommendController ) p0 ).removeSplitScreenRecommendView ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V
/* .line 498 */
return;
} // .end method
public void removeSplitScreenRecommendView ( ) {
/* .locals 2 */
/* .line 410 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " removeSplitScreenRecommendView mSplitScreenRecommendLayout= "; // const-string v1, " removeSplitScreenRecommendView mSplitScreenRecommendLayout= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSplitScreenRecommendLayout;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMultiWindowRecommendController"; // const-string v1, "MiuiMultiWindowRecommendController"
android.util.Slog .d ( v1,v0 );
/* .line 411 */
v0 = this.mService;
v0 = this.mAtmService;
v0 = this.mUiHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V */
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->post(Ljava/lang/Runnable;)Z
/* .line 429 */
return;
} // .end method
public void removeSplitScreenRecommendViewForTimer ( ) {
/* .locals 4 */
/* .line 292 */
final String v0 = "MiuiMultiWindowRecommendController"; // const-string v0, "MiuiMultiWindowRecommendController"
final String v1 = "removeSplitScreenRecommendViewForTimer"; // const-string v1, "removeSplitScreenRecommendViewForTimer"
android.util.Slog .d ( v0,v1 );
/* .line 293 */
v0 = this.mService;
v0 = this.mAtmService;
v0 = this.mUiHandler;
v1 = this.dismissSplitScreenRecommendViewRunnable;
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 294 */
v0 = this.mService;
v0 = this.mAtmService;
v0 = this.mUiHandler;
v1 = this.dismissSplitScreenRecommendViewRunnable;
/* iget-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendAutoDisapperTime:J */
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 295 */
return;
} // .end method
public void resetShadow ( android.view.View p0 ) {
/* .locals 11 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 358 */
/* if-nez p1, :cond_0 */
/* .line 359 */
return;
/* .line 361 */
} // :cond_0
(( android.view.View ) p1 ).getViewRootImpl ( ); // invoke-virtual {p1}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;
/* .line 362 */
/* .local v0, "viewRootImpl":Landroid/view/ViewRootImpl; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 363 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " resetShadow: view= "; // const-string v2, " resetShadow: view= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiMultiWindowRecommendController"; // const-string v2, "MiuiMultiWindowRecommendController"
android.util.Slog .d ( v2,v1 );
/* .line 364 */
v1 = com.android.server.wm.RecommendUtils .isSupportMiuiShadowV2 ( );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 365 */
(( android.view.ViewRootImpl ) v0 ).getSurfaceControl ( ); // invoke-virtual {v0}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;
v2 = this.mContext;
com.android.server.wm.RecommendUtils .resetMiShadowV2 ( v1,v2 );
/* .line 367 */
} // :cond_1
/* nop */
/* .line 368 */
(( android.view.ViewRootImpl ) v0 ).getSurfaceControl ( ); // invoke-virtual {v0}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
v6 = com.android.server.wm.MiuiMultiWindowRecommendController.MULTI_WINDOW_RECOMMEND_SHADOW_RESET_COLOR;
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
int v10 = 1; // const/4 v10, 0x1
/* .line 367 */
/* invoke-static/range {v3 ..v10}, Lcom/android/server/wm/RecommendUtils;->resetShadowSettingsInTransactionForSurfaceControl(Landroid/view/SurfaceControl;IF[FFFFI)V */
/* .line 372 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void setCornerRadiusAndShadow ( android.view.View p0 ) {
/* .locals 20 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 337 */
/* move-object/from16 v0, p1 */
/* if-nez v0, :cond_0 */
/* .line 338 */
return;
/* .line 340 */
} // :cond_0
/* invoke-virtual/range {p1 ..p1}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl; */
/* .line 341 */
/* .local v1, "viewRootImpl":Landroid/view/ViewRootImpl; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 342 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " setCornerRadiusAndShadow: view= "; // const-string v3, " setCornerRadiusAndShadow: view= "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiMultiWindowRecommendController"; // const-string v3, "MiuiMultiWindowRecommendController"
android.util.Slog .d ( v3,v2 );
/* .line 343 */
v2 = com.android.server.wm.RecommendUtils .isSupportMiuiShadowV2 ( );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 344 */
(( android.view.ViewRootImpl ) v1 ).getSurfaceControl ( ); // invoke-virtual {v1}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;
/* const/high16 v4, 0x73000000 */
int v5 = 0; // const/4 v5, 0x0
/* const/high16 v6, 0x41a00000 # 20.0f */
/* const/high16 v7, 0x43af0000 # 350.0f */
/* const/high16 v8, 0x3f800000 # 1.0f */
/* int-to-float v9, v2 */
/* new-instance v10, Landroid/graphics/RectF; */
/* invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V */
/* move-object/from16 v2, p0 */
v11 = this.mContext;
/* invoke-static/range {v3 ..v11}, Lcom/android/server/wm/RecommendUtils;->setMiShadowV2(Landroid/view/SurfaceControl;IFFFFFLandroid/graphics/RectF;Landroid/content/Context;)V */
/* .line 349 */
} // :cond_1
/* move-object/from16 v2, p0 */
/* .line 350 */
(( android.view.ViewRootImpl ) v1 ).getSurfaceControl ( ); // invoke-virtual {v1}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;
int v13 = 1; // const/4 v13, 0x1
/* const/high16 v14, 0x43af0000 # 350.0f */
v15 = com.android.server.wm.MiuiMultiWindowRecommendController.MULTI_WINDOW_RECOMMEND_SHADOW_COLOR;
/* const/16 v16, 0x0 */
/* const/high16 v17, 0x41c80000 # 25.0f */
/* const/high16 v18, 0x41700000 # 15.0f */
/* const/16 v19, 0x1 */
/* .line 349 */
/* invoke-static/range {v12 ..v19}, Lcom/android/server/wm/RecommendUtils;->setShadowSettingsInTransactionForSurfaceControl(Landroid/view/SurfaceControl;IF[FFFFI)V */
/* .line 341 */
} // :cond_2
/* move-object/from16 v2, p0 */
/* .line 355 */
} // :goto_0
return;
} // .end method
public void setFreeFormRecommendState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "inRecommend" # Z */
/* .line 490 */
v0 = this.mFreeFormRecommendDataEntry;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 491 */
(( com.android.server.wm.RecommendDataEntry ) v0 ).setFreeFormRecommendState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/RecommendDataEntry;->setFreeFormRecommendState(Z)V
/* .line 493 */
} // :cond_0
return;
} // .end method
public void setSplitScreenRecommendState ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "inRecommend" # Z */
/* .line 475 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setSplitScreenRecommendState: mSpiltScreenRecommendDataEntry= " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSpiltScreenRecommendDataEntry;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " inRecommend= "; // const-string v1, " inRecommend= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMultiWindowRecommendController"; // const-string v1, "MiuiMultiWindowRecommendController"
android.util.Slog .d ( v1,v0 );
/* .line 477 */
v0 = this.mSpiltScreenRecommendDataEntry;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 478 */
(( com.android.server.wm.RecommendDataEntry ) v0 ).setSplitScreenRecommendState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/RecommendDataEntry;->setSplitScreenRecommendState(Z)V
/* .line 480 */
} // :cond_0
return;
} // .end method
public void startMultiWindowRecommendAnimation ( android.view.View p0, Boolean p1 ) {
/* .locals 10 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "appear" # Z */
/* .line 375 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " startMultiWindowRecommendAnimation view= "; // const-string v1, " startMultiWindowRecommendAnimation view= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " appear= "; // const-string v1, " appear= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMultiWindowRecommendController"; // const-string v1, "MiuiMultiWindowRecommendController"
android.util.Slog .d ( v1,v0 );
/* .line 376 */
/* if-nez p1, :cond_0 */
/* .line 377 */
return;
/* .line 379 */
} // :cond_0
/* filled-new-array {p1}, [Landroid/view/View; */
miuix.animation.Folme .useAt ( v0 );
/* .line 380 */
int v0 = 0; // const/4 v0, 0x0
/* .line 381 */
/* .local v0, "hiddenState":Lmiuix/animation/controller/AnimState; */
int v1 = 0; // const/4 v1, 0x0
/* .line 382 */
/* .local v1, "shownState":Lmiuix/animation/controller/AnimState; */
/* instance-of v2, p1, Lcom/android/server/wm/FreeFormRecommendLayout; */
/* const-wide/16 v3, 0x0 */
/* const-wide v5, 0x3fee666660000000L # 0.949999988079071 */
/* const-wide/high16 v7, 0x3ff0000000000000L # 1.0 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 383 */
/* new-instance v2, Lmiuix/animation/controller/AnimState; */
final String v9 = "freeFormRecommendHide"; // const-string v9, "freeFormRecommendHide"
/* invoke-direct {v2, v9}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V */
v9 = miuix.animation.property.ViewProperty.SCALE_X;
/* .line 384 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v9, v5, v6 ); // invoke-virtual {v2, v9, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
v9 = miuix.animation.property.ViewProperty.SCALE_Y;
/* .line 385 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v9, v5, v6 ); // invoke-virtual {v2, v9, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
v5 = miuix.animation.property.ViewProperty.ALPHA;
/* .line 386 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v5, v3, v4 ); // invoke-virtual {v2, v5, v3, v4}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
/* .line 387 */
/* new-instance v2, Lmiuix/animation/controller/AnimState; */
final String v3 = "freeFormRecommendShow"; // const-string v3, "freeFormRecommendShow"
/* invoke-direct {v2, v3}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V */
v3 = miuix.animation.property.ViewProperty.SCALE_X;
/* .line 388 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v3, v7, v8 ); // invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
v3 = miuix.animation.property.ViewProperty.SCALE_Y;
/* .line 389 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v3, v7, v8 ); // invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
v3 = miuix.animation.property.ViewProperty.ALPHA;
/* .line 390 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v3, v7, v8 ); // invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
/* .line 392 */
} // :cond_1
/* instance-of v2, p1, Lcom/android/server/wm/SplitScreenRecommendLayout; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 393 */
/* new-instance v2, Lmiuix/animation/controller/AnimState; */
/* const-string/jumbo v9, "splitScreenRecommendHide" */
/* invoke-direct {v2, v9}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V */
v9 = miuix.animation.property.ViewProperty.SCALE_X;
/* .line 394 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v9, v5, v6 ); // invoke-virtual {v2, v9, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
v9 = miuix.animation.property.ViewProperty.SCALE_Y;
/* .line 395 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v9, v5, v6 ); // invoke-virtual {v2, v9, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
v5 = miuix.animation.property.ViewProperty.ALPHA;
/* .line 396 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v5, v3, v4 ); // invoke-virtual {v2, v5, v3, v4}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
/* .line 397 */
/* new-instance v2, Lmiuix/animation/controller/AnimState; */
/* const-string/jumbo v3, "splitScreenRecommendShow" */
/* invoke-direct {v2, v3}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V */
v3 = miuix.animation.property.ViewProperty.SCALE_X;
/* .line 398 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v3, v7, v8 ); // invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
v3 = miuix.animation.property.ViewProperty.SCALE_Y;
/* .line 399 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v3, v7, v8 ); // invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
v3 = miuix.animation.property.ViewProperty.ALPHA;
/* .line 400 */
(( miuix.animation.controller.AnimState ) v2 ).add ( v3, v7, v8 ); // invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;
/* .line 402 */
} // :cond_2
} // :goto_0
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 403 */
/* filled-new-array {p1}, [Landroid/view/View; */
miuix.animation.Folme .useAt ( v2 );
v3 = this.mShowHideAnimConfig;
/* filled-new-array {v3}, [Lmiuix/animation/base/AnimConfig; */
/* .line 405 */
} // :cond_3
/* filled-new-array {p1}, [Landroid/view/View; */
miuix.animation.Folme .useAt ( v2 );
v3 = this.mShowHideAnimConfig;
/* filled-new-array {v3}, [Lmiuix/animation/base/AnimConfig; */
/* .line 407 */
} // :goto_1
return;
} // .end method
