.class final Lcom/android/server/wm/MiuiSizeCompatService$H;
.super Landroid/os/Handler;
.source "MiuiSizeCompatService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiSizeCompatService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "H"
.end annotation


# static fields
.field public static final MSG_USER_SWITCH:I = 0x1

.field public static final MSG_USER_UNLOCKED:I = 0x2

.field public static final MSG_WRITE_CONFIRM:I = 0x3

.field public static final MSG_WRITE_SETTINGS:I = 0x4


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatService;


# direct methods
.method public static synthetic $r8$lambda$c-RWQpTFYVW_vvoBmCB4OHfSsjs(Lcom/android/server/wm/MiuiSizeCompatService$H;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatService$H;->lambda$handleMessage$0()V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiSizeCompatService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 556
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    .line 557
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 558
    return-void
.end method

.method private synthetic lambda$handleMessage$0()V
    .locals 1

    .line 568
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiSizeCompatService;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatJob;->sheduleJob(Landroid/content/Context;)V

    .line 569
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$mregisterDataObserver(Lcom/android/server/wm/MiuiSizeCompatService;)V

    .line 570
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 562
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 582
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 579
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->writeSetting()V

    .line 580
    goto :goto_1

    .line 573
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSecurityManager(Lcom/android/server/wm/MiuiSizeCompatService;)Lmiui/security/SecurityManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 574
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 575
    .local v0, "confirm":Z
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSecurityManager(Lcom/android/server/wm/MiuiSizeCompatService;)Lmiui/security/SecurityManager;

    move-result-object v1

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2, v0, v3}, Lmiui/security/SecurityManager;->setScRelaunchNeedConfirmForUser(Ljava/lang/String;ZI)V

    .line 576
    .end local v0    # "confirm":Z
    goto :goto_1

    .line 567
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmBgHandler(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/MiuiSizeCompatService$H;

    move-result-object v0

    new-instance v1, Lcom/android/server/wm/MiuiSizeCompatService$H$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiSizeCompatService$H$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiSizeCompatService$H;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/MiuiSizeCompatService$H;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 571
    goto :goto_1

    .line 564
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$H;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->onUserSwitch()V

    .line 565
    nop

    .line 584
    :cond_1
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
