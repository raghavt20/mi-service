.class Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;
.super Ljava/lang/Object;
.source "PreloadStateManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/PreloadStateManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UidInfo"
.end annotation


# instance fields
.field public uid:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 601
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    .line 604
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 606
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 601
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    .line 607
    iput p1, p0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    .line 608
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .line 617
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 618
    return v0

    .line 621
    :cond_0
    move-object v1, p1

    check-cast v1, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;

    .line 622
    .local v1, "info":Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;
    iget v2, p0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    iget v3, v1, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    if-eq v2, v3, :cond_1

    .line 623
    return v0

    .line 625
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 612
    iget v0, p0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I

    return v0
.end method
