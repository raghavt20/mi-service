public class com.android.server.wm.MiuiMultiWindowServiceImpl implements com.android.server.wm.MiuiMultiWindowServiceStub {
	 /* .source "MiuiMultiWindowServiceImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 public static final java.lang.String TAG;
	 /* # instance fields */
	 com.android.server.wm.ActivityTaskManagerService mAtms;
	 Integer mNavigationMode;
	 /* # direct methods */
	 static com.android.server.wm.MiuiMultiWindowServiceImpl ( ) {
		 /* .locals 1 */
		 /* .line 17 */
		 /* const-class v0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.server.wm.MiuiMultiWindowServiceImpl ( ) {
		 /* .locals 1 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 19 */
		 int v0 = -1; // const/4 v0, -0x1
		 /* iput v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mNavigationMode:I */
		 return;
	 } // .end method
	 static Boolean lambda$isEventXInDotRegion$0 ( com.android.server.wm.Task p0 ) { //synthethic
		 /* .locals 1 */
		 /* .param p0, "t" # Lcom/android/server/wm/Task; */
		 /* .line 43 */
		 /* iget-boolean v0, p0, Lcom/android/server/wm/Task;->mSoScRoot:Z */
	 } // .end method
	 static Boolean lambda$isEventXInDotRegion$1 ( com.android.server.wm.Task p0 ) { //synthethic
		 /* .locals 2 */
		 /* .param p0, "t" # Lcom/android/server/wm/Task; */
		 /* .line 75 */
		 v0 = 		 (( com.android.server.wm.Task ) p0 ).hasDot ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->hasDot()Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 76 */
			 v0 = 			 (( com.android.server.wm.Task ) p0 ).getWindowingMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I
			 int v1 = 1; // const/4 v1, 0x1
			 /* if-ne v0, v1, :cond_0 */
			 v0 = 			 (( com.android.server.wm.Task ) p0 ).isVisible ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->isVisible()Z
			 if ( v0 != null) { // if-eqz v0, :cond_0
			 } // :cond_0
			 int v1 = 0; // const/4 v1, 0x0
			 /* .line 75 */
		 } // :goto_0
	 } // .end method
	 /* # virtual methods */
	 public void init ( com.android.server.wm.ActivityTaskManagerService p0 ) {
		 /* .locals 0 */
		 /* .param p1, "service" # Lcom/android/server/wm/ActivityTaskManagerService; */
		 /* .line 22 */
		 this.mAtms = p1;
		 /* .line 23 */
		 return;
	 } // .end method
	 public Boolean isEventXInDotRegion ( com.android.server.wm.DisplayContent p0, Float p1 ) {
		 /* .locals 12 */
		 /* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
		 /* .param p2, "eventX" # F */
		 /* .line 32 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* if-nez p1, :cond_0 */
		 /* .line 33 */
		 /* .line 36 */
	 } // :cond_0
	 /* const/high16 v1, 0x42780000 # 62.0f */
	 v1 = 	 android.util.MiuiMultiWindowUtils .applyDip2Px ( v1 );
	 /* const/high16 v2, 0x40000000 # 2.0f */
	 /* div-float/2addr v1, v2 */
	 /* .line 37 */
	 /* .local v1, "halfDotWidth":F */
	 v3 = this.mAtmService;
	 v3 = this.mGlobalLock;
	 /* monitor-enter v3 */
	 /* .line 39 */
	 try { // :try_start_0
		 com.android.server.wm.MiuiSoScManagerStub .get ( );
		 v4 = 		 (( com.android.server.wm.MiuiSoScManagerStub ) v4 ).isSoScModeActivated ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScModeActivated()Z
		 int v5 = 1; // const/4 v5, 0x1
		 if ( v4 != null) { // if-eqz v4, :cond_9
			 /* .line 40 */
			 com.android.server.wm.MiuiSoScManagerStub .get ( );
			 v4 = 			 (( com.android.server.wm.MiuiSoScManagerStub ) v4 ).isSoScMinimizedModeActivated ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScMinimizedModeActivated()Z
			 if ( v4 != null) { // if-eqz v4, :cond_1
				 /* .line 41 */
				 /* monitor-exit v3 */
				 /* .line 43 */
			 } // :cond_1
			 (( com.android.server.wm.DisplayContent ) p1 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
			 /* new-instance v6, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$$ExternalSyntheticLambda0; */
			 /* invoke-direct {v6}, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$$ExternalSyntheticLambda0;-><init>()V */
			 (( com.android.server.wm.TaskDisplayArea ) v4 ).getTask ( v6 ); // invoke-virtual {v4, v6}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;
			 /* .line 44 */
			 /* .local v4, "soscRoot":Lcom/android/server/wm/Task; */
			 if ( v4 != null) { // if-eqz v4, :cond_8
				 v6 = 				 (( com.android.server.wm.Task ) v4 ).getChildCount ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getChildCount()I
				 int v7 = 2; // const/4 v7, 0x2
				 /* if-ge v6, v7, :cond_2 */
				 /* goto/16 :goto_2 */
				 /* .line 47 */
			 } // :cond_2
			 (( com.android.server.wm.Task ) v4 ).getChildAt ( v0 ); // invoke-virtual {v4, v0}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
			 (( com.android.server.wm.WindowContainer ) v6 ).getBounds ( ); // invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->getBounds()Landroid/graphics/Rect;
			 /* .line 48 */
			 /* .local v6, "splitBounds0":Landroid/graphics/Rect; */
			 (( com.android.server.wm.Task ) v4 ).getChildAt ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
			 (( com.android.server.wm.WindowContainer ) v7 ).getBounds ( ); // invoke-virtual {v7}, Lcom/android/server/wm/WindowContainer;->getBounds()Landroid/graphics/Rect;
			 /* .line 49 */
			 /* .local v7, "splitBounds1":Landroid/graphics/Rect; */
			 v8 = 			 (( android.graphics.Rect ) v6 ).width ( ); // invoke-virtual {v6}, Landroid/graphics/Rect;->width()I
			 (( com.android.server.wm.DisplayContent ) p1 ).getBounds ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getBounds()Landroid/graphics/Rect;
			 v9 = 			 (( android.graphics.Rect ) v9 ).width ( ); // invoke-virtual {v9}, Landroid/graphics/Rect;->width()I
			 /* if-ne v8, v9, :cond_5 */
			 /* .line 50 */
			 /* iget v8, v6, Landroid/graphics/Rect;->top:I */
			 /* iget v9, v7, Landroid/graphics/Rect;->top:I */
			 /* if-ge v8, v9, :cond_3 */
			 /* .line 51 */
			 (( com.android.server.wm.Task ) v4 ).getChildAt ( v0 ); // invoke-virtual {v4, v0}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
		 } // :cond_3
		 (( com.android.server.wm.Task ) v4 ).getChildAt ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
	 } // :goto_0
	 (( com.android.server.wm.WindowContainer ) v8 ).getTopMostTask ( ); // invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->getTopMostTask()Lcom/android/server/wm/Task;
	 /* .line 52 */
	 /* .local v8, "topSplitTask":Lcom/android/server/wm/Task; */
	 /* iget v9, v6, Landroid/graphics/Rect;->left:I */
	 /* iget v10, v6, Landroid/graphics/Rect;->right:I */
	 /* add-int/2addr v9, v10 */
	 /* int-to-float v9, v9 */
	 /* div-float/2addr v9, v2 */
	 /* .line 53 */
	 /* .local v9, "centerX":F */
	 if ( v8 != null) { // if-eqz v8, :cond_4
		 v2 = 		 (( com.android.server.wm.Task ) v8 ).hasDot ( ); // invoke-virtual {v8}, Lcom/android/server/wm/Task;->hasDot()Z
		 if ( v2 != null) { // if-eqz v2, :cond_4
			 /* sub-float v2, v9, v1 */
			 /* cmpl-float v2, p2, v2 */
			 /* if-ltz v2, :cond_4 */
			 /* add-float v2, v9, v1 */
			 /* cmpg-float v2, p2, v2 */
			 /* if-gtz v2, :cond_4 */
			 /* .line 55 */
			 v0 = com.android.server.wm.MiuiMultiWindowServiceImpl.TAG;
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v10 = "Event x is in dot region, topSplitTask="; // const-string v10, "Event x is in dot region, topSplitTask="
			 (( java.lang.StringBuilder ) v2 ).append ( v10 ); // invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .d ( v0,v2 );
			 /* .line 56 */
			 /* monitor-exit v3 */
			 /* .line 58 */
		 } // .end local v8 # "topSplitTask":Lcom/android/server/wm/Task;
	 } // .end local v9 # "centerX":F
} // :cond_4
/* goto/16 :goto_1 */
/* .line 60 */
} // :cond_5
(( com.android.server.wm.Task ) v4 ).getChildAt ( v0 ); // invoke-virtual {v4, v0}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v8 ).getTopMostTask ( ); // invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->getTopMostTask()Lcom/android/server/wm/Task;
/* move-object v9, v8 */
/* .local v9, "t0":Lcom/android/server/wm/Task; */
if ( v8 != null) { // if-eqz v8, :cond_6
v8 = (( com.android.server.wm.Task ) v9 ).hasDot ( ); // invoke-virtual {v9}, Lcom/android/server/wm/Task;->hasDot()Z
if ( v8 != null) { // if-eqz v8, :cond_6
	 /* .line 61 */
	 /* iget v8, v6, Landroid/graphics/Rect;->left:I */
	 /* iget v10, v6, Landroid/graphics/Rect;->right:I */
	 /* add-int/2addr v8, v10 */
	 /* int-to-float v8, v8 */
	 /* div-float/2addr v8, v2 */
	 /* .line 62 */
	 /* .local v8, "centerX":F */
	 /* sub-float v10, v8, v1 */
	 /* cmpl-float v10, p2, v10 */
	 /* if-ltz v10, :cond_6 */
	 /* add-float v10, v8, v1 */
	 /* cmpg-float v10, p2, v10 */
	 /* if-gtz v10, :cond_6 */
	 /* .line 63 */
	 v0 = com.android.server.wm.MiuiMultiWindowServiceImpl.TAG;
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v10 = "Event x is in dot region, t0="; // const-string v10, "Event x is in dot region, t0="
	 (( java.lang.StringBuilder ) v2 ).append ( v10 ); // invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v0,v2 );
	 /* .line 64 */
	 /* monitor-exit v3 */
	 /* .line 67 */
} // .end local v8 # "centerX":F
} // :cond_6
(( com.android.server.wm.Task ) v4 ).getChildAt ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v8 ).getTopMostTask ( ); // invoke-virtual {v8}, Lcom/android/server/wm/WindowContainer;->getTopMostTask()Lcom/android/server/wm/Task;
/* move-object v10, v8 */
/* .local v10, "t1":Lcom/android/server/wm/Task; */
if ( v8 != null) { // if-eqz v8, :cond_7
v8 = (( com.android.server.wm.Task ) v10 ).hasDot ( ); // invoke-virtual {v10}, Lcom/android/server/wm/Task;->hasDot()Z
if ( v8 != null) { // if-eqz v8, :cond_7
	 /* .line 68 */
	 /* iget v8, v7, Landroid/graphics/Rect;->left:I */
	 /* iget v11, v7, Landroid/graphics/Rect;->right:I */
	 /* add-int/2addr v8, v11 */
	 /* int-to-float v8, v8 */
	 /* div-float/2addr v8, v2 */
	 /* .line 69 */
	 /* .restart local v8 # "centerX":F */
	 /* sub-float v2, v8, v1 */
	 /* cmpl-float v2, p2, v2 */
	 /* if-ltz v2, :cond_7 */
	 /* add-float v2, v8, v1 */
	 /* cmpg-float v2, p2, v2 */
	 /* if-gtz v2, :cond_7 */
	 /* .line 70 */
	 v0 = com.android.server.wm.MiuiMultiWindowServiceImpl.TAG;
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v11 = "Event x is in dot region, t1="; // const-string v11, "Event x is in dot region, t1="
	 (( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v10 ); // invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v0,v2 );
	 /* .line 71 */
	 /* monitor-exit v3 */
	 /* .line 75 */
} // .end local v4 # "soscRoot":Lcom/android/server/wm/Task;
} // .end local v6 # "splitBounds0":Landroid/graphics/Rect;
} // .end local v7 # "splitBounds1":Landroid/graphics/Rect;
} // .end local v8 # "centerX":F
} // .end local v9 # "t0":Lcom/android/server/wm/Task;
} // .end local v10 # "t1":Lcom/android/server/wm/Task;
} // :cond_7
} // :goto_1
/* .line 45 */
/* .restart local v4 # "soscRoot":Lcom/android/server/wm/Task; */
} // :cond_8
} // :goto_2
/* monitor-exit v3 */
/* .line 75 */
} // .end local v4 # "soscRoot":Lcom/android/server/wm/Task;
} // :cond_9
/* new-instance v4, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v4}, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$$ExternalSyntheticLambda1;-><init>()V */
(( com.android.server.wm.DisplayContent ) p1 ).getRootTask ( v4 ); // invoke-virtual {p1, v4}, Lcom/android/server/wm/DisplayContent;->getRootTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;
/* move-object v6, v4 */
/* .local v6, "fullscreenTask":Lcom/android/server/wm/Task; */
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 77 */
(( com.android.server.wm.Task ) v6 ).getBounds ( ); // invoke-virtual {v6}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
/* .line 78 */
/* .local v4, "taskBounds":Landroid/graphics/Rect; */
/* iget v7, v4, Landroid/graphics/Rect;->left:I */
/* iget v8, v4, Landroid/graphics/Rect;->right:I */
/* add-int/2addr v7, v8 */
/* int-to-float v7, v7 */
/* div-float/2addr v7, v2 */
/* .line 79 */
/* .local v7, "centerX":F */
/* sub-float v2, v7, v1 */
/* cmpl-float v2, p2, v2 */
/* if-ltz v2, :cond_a */
/* add-float v2, v7, v1 */
/* cmpg-float v2, p2, v2 */
/* if-gtz v2, :cond_a */
/* .line 80 */
v0 = com.android.server.wm.MiuiMultiWindowServiceImpl.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Event x is in dot region, fullscreenTask="; // const-string v8, "Event x is in dot region, fullscreenTask="
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 81 */
/* monitor-exit v3 */
/* .line 84 */
} // .end local v4 # "taskBounds":Landroid/graphics/Rect;
} // .end local v6 # "fullscreenTask":Lcom/android/server/wm/Task;
} // .end local v7 # "centerX":F
} // :cond_a
} // :goto_3
/* monitor-exit v3 */
/* .line 85 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public Boolean isMiuiMultiWinChangeSupport ( ) {
/* .locals 1 */
/* .line 27 */
/* sget-boolean v0, Landroid/util/MiuiMultiWindowUtils;->MULTIWIN_SWITCH_ENABLED:Z */
} // .end method
public Boolean isSupportSoScMode ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 89 */
v0 = this.mAtms;
v0 = (( com.android.server.wm.ActivityTaskManagerService ) v0 ).isInSplitScreenWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 90 */
/* .line 93 */
} // :cond_0
v0 = android.util.MiuiMultiWindowUtils .isTablet ( );
/* if-nez v0, :cond_1 */
v0 = android.util.MiuiMultiWindowUtils .isFoldInnerScreen ( p1 );
/* if-nez v0, :cond_1 */
/* .line 94 */
/* .line 97 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mNavigationMode:I */
int v2 = -1; // const/4 v2, -0x1
/* if-ne v0, v2, :cond_2 */
/* .line 98 */
v0 = this.mAtms;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = -2; // const/4 v3, -0x2
final String v4 = "navigation_mode"; // const-string v4, "navigation_mode"
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v4,v1,v3 );
/* iput v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mNavigationMode:I */
/* .line 101 */
v0 = this.mAtms;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 102 */
android.provider.Settings$Secure .getUriFor ( v4 );
/* new-instance v4, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$1; */
v5 = this.mAtms;
v5 = this.mUiHandler;
/* invoke-direct {v4, p0, v5}, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$1;-><init>(Lcom/android/server/wm/MiuiMultiWindowServiceImpl;Landroid/os/Handler;)V */
/* .line 101 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v3, v1, v4, v2 ); // invoke-virtual {v0, v3, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 112 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl;->mNavigationMode:I */
if ( v0 != null) { // if-eqz v0, :cond_3
int v1 = 1; // const/4 v1, 0x1
} // :cond_3
} // .end method
