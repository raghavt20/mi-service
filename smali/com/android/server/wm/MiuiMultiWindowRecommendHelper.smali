.class public Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;
    }
.end annotation


# static fields
.field private static final MULTI_WINDOW_RECOMMEND_SWITCH:Ljava/lang/String; = "MiuiMultiWindowRecommendSwitch"

.field public static final RECENT_APP_LIST_SIZE:I = 0x4

.field private static final RECOMMEND_SWITCH_ENABLE_STATE:I = 0x1

.field private static final RECOMMEND_SWITCH_STATELESS:I = -0x1

.field private static final TAG:Ljava/lang/String; = "MiuiMultiWindowRecommendHelper"


# instance fields
.field private volatile lastSplitScreenRecommendTime:J

.field private listener:Lmiui/process/IForegroundInfoListener$Stub;

.field private mContext:Landroid/content/Context;

.field private mFirstWindowHasDraw:Z

.field mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

.field private mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

.field private mLastConfiguration:Landroid/content/res/Configuration;

.field private mLastIsWideScreen:Z

.field final mLock:Ljava/lang/Object;

.field private mMaxTimeFrame:J

.field mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

.field private mMuiltiWindowRecommendObserver:Landroid/database/ContentObserver;

.field private mMultiWindowRecommendReceiver:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;

.field private volatile mMultiWindowRecommendSwitchEnabled:Z

.field private mRecentAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field final mRecentAppListLock:Ljava/lang/Object;

.field private mRecentAppListMaxSize:I

.field private mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

.field private mSplitScreenRecommendPredictHelper:Lcom/android/server/wm/SplitScreenRecommendPredictHelper;

.field private final mTaskStackListener:Landroid/app/TaskStackListener;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFreeFormRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Lcom/android/server/wm/RecommendDataEntry;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastConfiguration(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Landroid/content/res/Configuration;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLastConfiguration:Landroid/content/res/Configuration;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastIsWideScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLastIsWideScreen:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMultiWindowRecommendSwitchEnabled(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Lcom/android/server/wm/RecommendDataEntry;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmLastIsWideScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLastIsWideScreen:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMultiWindowRecommendSwitchEnabled(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mFreeFormRecommendIfNeeded(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->FreeFormRecommendIfNeeded(Ljava/lang/String;II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckPreConditionsForSplitScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Lcom/android/server/wm/Task;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->checkPreConditionsForSplitScreen(Lcom/android/server/wm/Task;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$minFreeFormRecommendState(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->inFreeFormRecommendState()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$minSplitScreenRecommendState(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->inSplitScreenRecommendState()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misDeviceSupportFreeFormRecommend(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->isDeviceSupportFreeFormRecommend()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misDeviceSupportSplitScreenRecommend(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->isDeviceSupportSplitScreenRecommend()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mpredictSplitScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->predictSplitScreen(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/server/wm/MiuiFreeFormManagerService;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    .line 54
    const-wide/32 v2, 0x1d4c0

    iput-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMaxTimeFrame:J

    .line 55
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->lastSplitScreenRecommendTime:J

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z

    .line 58
    iput v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppListMaxSize:I

    .line 63
    new-instance v1, Lcom/android/server/wm/RecommendDataEntry;

    invoke-direct {v1}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 64
    new-instance v1, Lcom/android/server/wm/RecommendDataEntry;

    invoke-direct {v1}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 68
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z

    .line 69
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLastConfiguration:Landroid/content/res/Configuration;

    .line 73
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLock:Ljava/lang/Object;

    .line 74
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppListLock:Ljava/lang/Object;

    .line 92
    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$1;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->listener:Lmiui/process/IForegroundInfoListener$Stub;

    .line 116
    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$2;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mTaskStackListener:Landroid/app/TaskStackListener;

    .line 151
    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$3;

    new-instance v2, Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V

    invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$3;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMuiltiWindowRecommendObserver:Landroid/database/ContentObserver;

    .line 77
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mContext:Landroid/content/Context;

    .line 78
    iput-object p2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 79
    new-instance v1, Lcom/android/server/wm/SplitScreenRecommendPredictHelper;

    invoke-direct {v1, p0}, Lcom/android/server/wm/SplitScreenRecommendPredictHelper;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mSplitScreenRecommendPredictHelper:Lcom/android/server/wm/SplitScreenRecommendPredictHelper;

    .line 80
    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendReceiver:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;

    .line 81
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mContext:Landroid/content/Context;

    iget-object v4, v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$MultiWindowRecommendReceiver;->mFilter:Landroid/content/IntentFilter;

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v5, v5, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1, v4, v3, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 83
    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v3, v3, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    invoke-direct {v1, v2, v3, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    .line 85
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "MiuiMultiWindowRecommendSwitch"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMuiltiWindowRecommendObserver:Landroid/database/ContentObserver;

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 87
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->initMultiWindowRecommendSwitchState()V

    .line 88
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->registerForegroundInfoListener()V

    .line 89
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->registerTaskStackListener()V

    .line 90
    return-void
.end method

.method private FreeFormRecommendIfNeeded(Ljava/lang/String;II)V
    .locals 8
    .param p1, "senderPackageName"    # Ljava/lang/String;
    .param p2, "recommendTransactionType"    # I
    .param p3, "recommendScene"    # I

    .line 230
    if-nez p1, :cond_0

    .line 231
    return-void

    .line 234
    :cond_0
    const/4 v0, 0x3

    const/4 v1, 0x1

    if-eq p2, v1, :cond_1

    if-eq p2, v0, :cond_1

    .line 236
    return-void

    .line 239
    :cond_1
    if-eq p3, v1, :cond_2

    const/4 v2, 0x2

    if-eq p3, v2, :cond_2

    .line 241
    return-void

    .line 244
    :cond_2
    sget-object v2, Landroid/util/MiuiMultiWindowAdapter;->LIST_ABOUT_FREEFORM_RECOMMEND_WAITING_APPLICATION:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "MiuiMultiWindowRecommendHelper"

    if-nez v2, :cond_3

    .line 245
    const-string v0, "FreeFormRecommendIfNeeded senderPackageName is not in LIST_ABOUT_FREEFORM_RECOMMEND_WAITING_APPLICATION "

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    return-void

    .line 250
    :cond_3
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v2, v2, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 251
    const-string v0, "FreeFormRecommendIfNeeded isInSplitScreenWindowingMode "

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    return-void

    .line 255
    :cond_4
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v2, v2, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v2

    .line 256
    invoke-virtual {v2, v1}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;

    move-result-object v1

    .line 257
    .local v1, "currentFullRootTask":Lcom/android/server/wm/Task;
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_0

    .line 262
    :cond_5
    if-ne p2, v0, :cond_6

    .line 263
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V

    .line 264
    return-void

    .line 267
    :cond_6
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->checkPreConditionsForFreeForm()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 268
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->inSplitScreenRecommendState()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 269
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V

    .line 271
    :cond_7
    nop

    .line 272
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v6

    iget v7, v1, Lcom/android/server/wm/Task;->mUserId:I

    .line 271
    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v2 .. v7}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildFreeFormRecommendDataEntry(Ljava/lang/String;IIII)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 273
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildAndAddFreeFormRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V

    .line 275
    :cond_8
    return-void

    .line 258
    :cond_9
    :goto_0
    const-string v0, "FreeFormRecommendIfNeeded senderPackageName not equals currentFullRootTask.getPackageName() "

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    return-void
.end method

.method private addNewTaskToRecentAppList(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V
    .locals 6
    .param p1, "splitScreenRecommendTaskInfo"    # Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    .line 434
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const-string v2, " addNewTaskToRecentAppList task id = "

    const-string v3, "MiuiMultiWindowRecommendHelper"

    if-le v0, v1, :cond_2

    .line 435
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 436
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    .line 437
    .local v1, "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    invoke-virtual {v1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;

    move-result-object v5

    if-ne v4, v5, :cond_0

    .line 438
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440
    return-void

    .line 435
    .end local v1    # "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 443
    .end local v0    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 445
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 447
    return-void
.end method

.method private buildAndAddFreeFormRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V
    .locals 1
    .param p1, "recommendDataEntry"    # Lcom/android/server/wm/RecommendDataEntry;

    .line 356
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V

    .line 357
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->addFreeFormRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V

    .line 358
    return-void
.end method

.method private buildAndAddSpiltScreenRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V
    .locals 2
    .param p1, "recommendDataEntry"    # Lcom/android/server/wm/RecommendDataEntry;

    .line 349
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->setLastSplitScreenRecommendTime(J)V

    .line 350
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V

    .line 351
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V

    .line 352
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->addSplitScreenRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V

    .line 353
    return-void
.end method

.method private buildAndAddSpiltScreenRecommendViewIfNeeded(Lcom/android/server/wm/RecommendDataEntry;)V
    .locals 9
    .param p1, "recommendDataEntry"    # Lcom/android/server/wm/RecommendDataEntry;

    .line 329
    const-wide/16 v0, 0x3e8

    .line 330
    .local v0, "timeoutMs":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, v0

    .line 331
    .local v2, "timeoutAtTimeMs":J
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 332
    const/4 v5, 0x0

    :try_start_0
    iput-boolean v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    :goto_0
    :try_start_1
    iget-boolean v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z

    if-eqz v5, :cond_1

    .line 335
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v5, v2, v5

    .line 336
    .local v5, "waitMillis":J
    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-gtz v7, :cond_0

    .line 337
    goto :goto_1

    .line 339
    :cond_0
    iget-object v7, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLock:Ljava/lang/Object;

    invoke-virtual {v7, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 340
    .end local v5    # "waitMillis":J
    goto :goto_0

    .line 343
    :cond_1
    :goto_1
    goto :goto_2

    .line 341
    :catch_0
    move-exception v5

    .line 342
    .local v5, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    .line 344
    .end local v5    # "e":Ljava/lang/InterruptedException;
    :goto_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 345
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildAndAddSpiltScreenRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V

    .line 346
    return-void

    .line 344
    :catchall_0
    move-exception v5

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5
.end method

.method private buildFreeFormRecommendDataEntry(Ljava/lang/String;IIII)Lcom/android/server/wm/RecommendDataEntry;
    .locals 1
    .param p1, "senderPackageName"    # Ljava/lang/String;
    .param p2, "recommendTransactionType"    # I
    .param p3, "recommendScene"    # I
    .param p4, "taskId"    # I
    .param p5, "userId"    # I

    .line 319
    new-instance v0, Lcom/android/server/wm/RecommendDataEntry;

    invoke-direct {v0}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V

    .line 320
    .local v0, "dataEntry":Lcom/android/server/wm/RecommendDataEntry;
    invoke-virtual {v0, p2}, Lcom/android/server/wm/RecommendDataEntry;->setTransactionType(I)V

    .line 321
    invoke-virtual {v0, p3}, Lcom/android/server/wm/RecommendDataEntry;->setRecommendSceneType(I)V

    .line 322
    invoke-virtual {v0, p1}, Lcom/android/server/wm/RecommendDataEntry;->setFreeformPackageName(Ljava/lang/String;)V

    .line 323
    invoke-virtual {v0, p4}, Lcom/android/server/wm/RecommendDataEntry;->setFreeFormTaskId(I)V

    .line 324
    invoke-virtual {v0, p5}, Lcom/android/server/wm/RecommendDataEntry;->setFreeformUserId(I)V

    .line 325
    return-object v0
.end method

.method private buildSpiltScreenRecommendDataEntry(Ljava/util/List;Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)Lcom/android/server/wm/RecommendDataEntry;
    .locals 5
    .param p2, "splitScreenRecommendTaskInfo"    # Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;",
            ">;",
            "Lcom/android/server/wm/SplitScreenRecommendTaskInfo;",
            ")",
            "Lcom/android/server/wm/RecommendDataEntry;"
        }
    .end annotation

    .line 299
    .local p1, "frequentSwitchedTaskList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
    new-instance v0, Lcom/android/server/wm/RecommendDataEntry;

    invoke-direct {v0}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V

    .line 300
    .local v0, "dataEntry":Lcom/android/server/wm/RecommendDataEntry;
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/wm/RecommendDataEntry;->setTransactionType(I)V

    .line 301
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/server/wm/RecommendDataEntry;->setRecommendSceneType(I)V

    .line 302
    invoke-virtual {p2}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v1

    .line 303
    .local v1, "currentTaskId":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    .line 304
    .local v3, "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v4

    if-ne v4, v1, :cond_0

    .line 305
    invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setPrimaryPackageName(Ljava/lang/String;)V

    .line 306
    invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setPrimaryTaskId(I)V

    .line 307
    invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;

    move-result-object v4

    iget v4, v4, Lcom/android/server/wm/Task;->mUserId:I

    invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setPrimaryUserId(I)V

    goto :goto_1

    .line 309
    :cond_0
    invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setSecondaryPackageName(Ljava/lang/String;)V

    .line 310
    invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setSecondaryTaskId(I)V

    .line 311
    invoke-virtual {v3}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTask()Lcom/android/server/wm/Task;

    move-result-object v4

    iget v4, v4, Lcom/android/server/wm/Task;->mUserId:I

    invoke-virtual {v0, v4}, Lcom/android/server/wm/RecommendDataEntry;->setSecondaryUserId(I)V

    .line 313
    .end local v3    # "taskInfo":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    :goto_1
    goto :goto_0

    .line 314
    :cond_1
    return-object v0
.end method

.method private checkPreConditionsForFreeForm()Z
    .locals 2

    .line 388
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->hasNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    const-string v0, "MiuiMultiWindowRecommendHelper"

    const-string v1, "checkPreConditionsForFreeForm  hasNotification"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    const/4 v0, 0x0

    return v0

    .line 392
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private checkPreConditionsForSplitScreen(Lcom/android/server/wm/Task;)Z
    .locals 4
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 366
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 367
    return v0

    .line 369
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 370
    return v0

    .line 372
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v1

    const-string v2, "MiuiMultiWindowRecommendHelper"

    const/4 v3, 0x1

    if-ne v1, v3, :cond_4

    .line 373
    invoke-static {p1}, Lcom/android/server/wm/RecommendUtils;->isInSplitScreenWindowingMode(Lcom/android/server/wm/Task;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    .line 379
    :cond_2
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->inSplitScreenRecommendState()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 380
    const-string v1, " InSplitScreenRecommendState "

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V

    .line 382
    return v0

    .line 384
    :cond_3
    return v3

    .line 374
    :cond_4
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " task window mode is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " isInSplitScreenWindowingMode= "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 375
    invoke-static {p1}, Lcom/android/server/wm/RecommendUtils;->isInSplitScreenWindowingMode(Lcom/android/server/wm/Task;)Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 374
    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V

    .line 377
    return v0
.end method

.method private inFreeFormRecommendState()Z
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->inFreeFormRecommendState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    const/4 v0, 0x1

    return v0

    .line 406
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private inSplitScreenRecommendState()Z
    .locals 1

    .line 396
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMiuiMultiWindowRecommendController:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->inSplitScreenRecommendState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    const/4 v0, 0x1

    return v0

    .line 399
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private initMultiWindowRecommendSwitchState()V
    .locals 6

    .line 166
    const-string v0, "MiuiMultiWindowRecommendSwitch"

    :try_start_0
    invoke-static {}, Lcom/android/server/wm/RecommendUtils;->isSupportMiuiMultiWindowRecommend()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 167
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 168
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-static {v1, v0, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v3, :cond_0

    .line 171
    iput-boolean v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z

    goto :goto_1

    .line 173
    :cond_0
    invoke-static {v1, v0, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v5, :cond_1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    :goto_0
    iput-boolean v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z

    .line 178
    .end local v1    # "contentResolver":Landroid/content/ContentResolver;
    :cond_2
    :goto_1
    const-string v0, "MiuiMultiWindowRecommendHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " initMultiWindowRecommendSwitchState mMultiWindowRecommendSwitchEnabled= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    goto :goto_2

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 183
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method private isDeviceSupportFreeFormRecommend()Z
    .locals 2

    .line 220
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/server/wm/RecommendUtils;->isSupportMiuiMultiWindowRecommend()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 223
    :cond_0
    invoke-static {}, Landroid/util/MiuiMultiWindowUtils;->supportFreeform()Z

    move-result v0

    if-nez v0, :cond_1

    .line 224
    return v1

    .line 226
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 221
    :cond_2
    :goto_0
    return v1
.end method

.method private isDeviceSupportSplitScreenRecommend()Z
    .locals 2

    .line 139
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMultiWindowRecommendSwitchEnabled:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/android/server/wm/RecommendUtils;->isSupportMiuiMultiWindowRecommend()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 142
    :cond_0
    invoke-static {}, Landroid/util/MiuiMultiWindowUtils;->isSupportSplitScreenFeature()Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    return v1

    .line 145
    :cond_1
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/util/MiuiMultiWindowUtils;->isFoldInnerScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 148
    :cond_2
    return v1

    .line 146
    :cond_3
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 140
    :cond_4
    :goto_1
    return v1
.end method

.method static synthetic lambda$printRecentAppInfo$0(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "taskInfo"    # Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    .line 483
    invoke-virtual {p0}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private predictSplitScreen(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V
    .locals 4
    .param p1, "splitScreenRecommendTaskInfo"    # Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    .line 278
    const-string v0, "MiuiMultiWindowRecommendHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current foreground task: taskId is"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " package name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 279
    invoke-virtual {p1}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getPkgName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 278
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppListLock:Ljava/lang/Object;

    monitor-enter v0

    .line 282
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->updateAppDataList(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V

    .line 283
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mSplitScreenRecommendPredictHelper:Lcom/android/server/wm/SplitScreenRecommendPredictHelper;

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/android/server/wm/SplitScreenRecommendPredictHelper;->getFrequentSwitchedTask(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 284
    .local v1, "frequentSwitchedTask":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    if-eqz v1, :cond_1

    .line 286
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->hasNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    const-string v0, "MiuiMultiWindowRecommendHelper"

    const-string v2, "predictSplitScreen hasNotification "

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->setLastSplitScreenRecommendTime(J)V

    .line 289
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V

    goto :goto_0

    .line 291
    :cond_0
    invoke-direct {p0, v1, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildSpiltScreenRecommendDataEntry(Ljava/util/List;Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 292
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->buildAndAddSpiltScreenRecommendViewIfNeeded(Lcom/android/server/wm/RecommendDataEntry;)V

    .line 295
    :cond_1
    :goto_0
    return-void

    .line 284
    .end local v1    # "frequentSwitchedTask":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/SplitScreenRecommendTaskInfo;>;"
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private printRecentAppInfo()V
    .locals 3

    .line 483
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v0

    .line 484
    const-string v1, ","

    invoke-static {v1}, Ljava/util/stream/Collectors;->joining(Ljava/lang/CharSequence;)Ljava/util/stream/Collector;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 485
    .local v0, "recentAppInfo":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "printRecentAppInfo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRecentAppList size= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiMultiWindowRecommendHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    return-void
.end method

.method private registerForegroundInfoListener()V
    .locals 1

    .line 499
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->listener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 500
    return-void
.end method

.method private registerTaskStackListener()V
    .locals 2

    .line 507
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mTaskStackListener:Landroid/app/TaskStackListener;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V

    .line 508
    return-void
.end method

.method private removeExcessTasks()V
    .locals 4

    .line 469
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppListMaxSize:I

    sub-int/2addr v0, v1

    .line 470
    .local v0, "size":I
    if-lez v0, :cond_0

    .line 471
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "excess task size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiMultiWindowRecommendHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 473
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 472
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 476
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method private removeTimeOutTasks()V
    .locals 9

    .line 451
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    invoke-virtual {v0}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getSwitchTime()J

    move-result-wide v0

    .line 453
    .local v0, "switchTime":J
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v2

    new-instance v3, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$4;

    invoke-direct {v3, p0, v0, v1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$4;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;J)V

    invoke-interface {v2, v3}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v2

    .line 458
    invoke-interface {v2}, Ljava/util/stream/Stream;->count()J

    move-result-wide v2

    .line 459
    .local v2, "count":J
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    int-to-long v5, v4

    cmp-long v5, v5, v2

    if-gez v5, :cond_0

    .line 460
    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    .line 461
    .local v5, "remove":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "remove task exceed max time limit, taskId is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getTaskId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " switchTime= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 462
    invoke-virtual {v5}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;->getSwitchTime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 461
    const-string v7, "MiuiMultiWindowRecommendHelper"

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    .end local v5    # "remove":Lcom/android/server/wm/SplitScreenRecommendTaskInfo;
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 465
    .end local v0    # "switchTime":J
    .end local v2    # "count":J
    .end local v4    # "i":I
    :cond_0
    return-void
.end method

.method private unregisterForegroundInfoListener()V
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->listener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 504
    return-void
.end method

.method private unregisterTaskStackListener()V
    .locals 2

    .line 511
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mTaskStackListener:Landroid/app/TaskStackListener;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService;->unregisterTaskStackListener(Landroid/app/ITaskStackListener;)V

    .line 512
    return-void
.end method

.method private updateAppDataList(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V
    .locals 0
    .param p1, "splitScreenRecommendTaskInfo"    # Lcom/android/server/wm/SplitScreenRecommendTaskInfo;

    .line 426
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->addNewTaskToRecentAppList(Lcom/android/server/wm/SplitScreenRecommendTaskInfo;)V

    .line 427
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->removeTimeOutTasks()V

    .line 428
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->removeExcessTasks()V

    .line 429
    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->printRecentAppInfo()V

    .line 430
    return-void
.end method


# virtual methods
.method clearRecentAppList()V
    .locals 3

    .line 489
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppListLock:Ljava/lang/Object;

    monitor-enter v0

    .line 490
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 491
    monitor-exit v0

    return-void

    .line 493
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mRecentAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 494
    const-string v1, "MiuiMultiWindowRecommendHelper"

    const-string v2, "clear recent app list"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    monitor-exit v0

    .line 496
    return-void

    .line 495
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public displayConfigurationChange(Lcom/android/server/wm/DisplayContent;Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "configuration"    # Landroid/content/res/Configuration;

    .line 577
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper$5;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayContent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 599
    return-void
.end method

.method public getFocusedTask()Lcom/android/server/wm/Task;
    .locals 3

    .line 515
    const/4 v0, 0x0

    .line 516
    .local v0, "focusedTask":Lcom/android/server/wm/Task;
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    .line 517
    .local v1, "displayContent":Lcom/android/server/wm/DisplayContent;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/DisplayContent;->mFocusedApp:Lcom/android/server/wm/ActivityRecord;

    if-eqz v2, :cond_0

    .line 518
    iget-object v2, v1, Lcom/android/server/wm/DisplayContent;->mFocusedApp:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 520
    :cond_0
    return-object v0
.end method

.method public getMaxTimeFrame()J
    .locals 2

    .line 479
    iget-wide v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mMaxTimeFrame:J

    return-wide v0
.end method

.method public hasNotification()Z
    .locals 3

    .line 524
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;

    move-result-object v0

    monitor-enter v0

    .line 525
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 526
    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayPolicy;->getNotificationShade()Lcom/android/server/wm/WindowState;

    move-result-object v1

    .line 527
    .local v1, "notificationShade":Lcom/android/server/wm/WindowState;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    monitor-exit v0

    return v2

    .line 528
    .end local v1    # "notificationShade":Lcom/android/server/wm/WindowState;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method isSplitScreenRecommendValid(Lcom/android/server/wm/RecommendDataEntry;)Z
    .locals 6
    .param p1, "recommendDataEntry"    # Lcom/android/server/wm/RecommendDataEntry;

    .line 410
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 411
    return v0

    .line 413
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 414
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v2, v2, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {p1}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryTaskId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/server/wm/RootWindowContainer;->getRootTask(I)Lcom/android/server/wm/Task;

    move-result-object v2

    .line 415
    .local v2, "primaryTask":Lcom/android/server/wm/Task;
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v3, v3, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {p1}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryTaskId()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/server/wm/RootWindowContainer;->getRootTask(I)Lcom/android/server/wm/Task;

    move-result-object v3

    .line 416
    .local v3, "secondaryTask":Lcom/android/server/wm/Task;
    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 417
    monitor-exit v1

    const/4 v0, 0x1

    return v0

    .line 419
    :cond_1
    const-string v4, "MiuiMultiWindowRecommendHelper"

    const-string v5, " SplitScreenRecommend invalid "

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    monitor-exit v1

    return v0

    .line 422
    .end local v2    # "primaryTask":Lcom/android/server/wm/Task;
    .end local v3    # "secondaryTask":Lcom/android/server/wm/Task;
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onFirstWindowDrawn(Lcom/android/server/wm/ActivityRecord;)V
    .locals 5
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 532
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 535
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v0

    .line 536
    .local v0, "taskId":I
    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 537
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    if-eqz v2, :cond_1

    .line 538
    invoke-virtual {v2}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryTaskId()I

    move-result v2

    if-ne v2, v0, :cond_1

    iget-boolean v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z

    if-nez v2, :cond_1

    .line 540
    const-string v2, "MiuiMultiWindowRecommendHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFirstWindowDrawn: taskId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " activityRecord: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFirstWindowHasDraw:Z

    .line 542
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 544
    :cond_1
    monitor-exit v1

    .line 545
    return-void

    .line 544
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 533
    .end local v0    # "taskId":I
    :cond_2
    :goto_0
    return-void
.end method

.method setLastSplitScreenRecommendTime(J)V
    .locals 2
    .param p1, "recommendTime"    # J

    .line 361
    iput-wide p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->lastSplitScreenRecommendTime:J

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " setLastSplitScreenRecommendTime= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiWindowRecommendHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    return-void
.end method

.method public startSmallFreeformFromNotification()I
    .locals 8

    .line 549
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/wm/RecommendUtils;->isKeyguardLocked(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    .line 550
    return v1

    .line 552
    :cond_0
    invoke-static {}, Lcom/android/server/wm/RecommendUtils;->isSupportMiuiMultiWindowRecommend()Z

    move-result v0

    if-nez v0, :cond_1

    .line 553
    return v1

    .line 555
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 556
    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;

    move-result-object v0

    .line 557
    .local v0, "currentFullTask":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_6

    iget-object v3, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    if-nez v3, :cond_2

    goto :goto_1

    .line 560
    :cond_2
    invoke-static {v0}, Lcom/android/server/wm/RecommendUtils;->isInSplitScreenWindowingMode(Lcom/android/server/wm/Task;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 561
    return v1

    .line 563
    :cond_3
    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v3, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getStackPackageName(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v3

    .line 564
    .local v3, "packageName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "startSmallFreeformFromNotification: currentFullTask packageName= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiMultiWindowRecommendHelper"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    sget-object v4, Landroid/util/MiuiMultiWindowAdapter;->LIST_ABOUT_FREEFORM_RECOMMEND_MAP_APPLICATION:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    sget-object v4, Landroid/util/MiuiMultiWindowAdapter;->LIST_ABOUT_FREEFORM_RECOMMEND_MAP_APPLICATION:Ljava/util/List;

    iget-object v5, v0, Lcom/android/server/wm/Task;->realActivity:Landroid/content/ComponentName;

    .line 566
    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    goto :goto_0

    .line 573
    :cond_4
    return v1

    .line 567
    :cond_5
    :goto_0
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormManagerService;->mActivityTaskManagerService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 569
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    const-string v7, "enterSmallFreeFormByNotificationRecommend"

    filled-new-array {v5, v1, v7, v6}, [Ljava/lang/Object;

    move-result-object v1

    .line 567
    const-string v5, "launchMiniFreeFormWindowVersion2"

    invoke-static {v4, v5, v1}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 571
    return v2

    .line 558
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_6
    :goto_1
    return v1
.end method
