.class Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;
.super Ljava/lang/Object;
.source "MiuiMultiTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiMultiTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LaunchAppInfo"
.end annotation


# instance fields
.field private returnTarget:Landroid/content/ComponentName;

.field private supports:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetreturnTarget(Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;)Landroid/content/ComponentName;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;->returnTarget:Landroid/content/ComponentName;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetsupports(Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;->supports:Ljava/util/ArrayList;

    return-object p0
.end method

.method public constructor <init>(Ljava/util/ArrayList;Landroid/content/ComponentName;)V
    .locals 0
    .param p2, "returnTarget"    # Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/ComponentName;",
            ")V"
        }
    .end annotation

    .line 28
    .local p1, "supports":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;->supports:Ljava/util/ArrayList;

    .line 30
    iput-object p2, p0, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;->returnTarget:Landroid/content/ComponentName;

    .line 31
    return-void
.end method
