class com.android.server.wm.AppTransitionInjector$ScaleYAnimation extends android.view.animation.Animation {
	 /* .source "AppTransitionInjector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AppTransitionInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ScaleYAnimation" */
} // .end annotation
/* # instance fields */
private Float mFromY;
private Float mPivotY;
private Float mToY;
/* # direct methods */
public com.android.server.wm.AppTransitionInjector$ScaleYAnimation ( ) {
/* .locals 1 */
/* .param p1, "fromY" # F */
/* .param p2, "toY" # F */
/* .line 1395 */
/* invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V */
/* .line 1396 */
/* iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mFromY:F */
/* .line 1397 */
/* iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mToY:F */
/* .line 1398 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mPivotY:F */
/* .line 1399 */
return;
} // .end method
public com.android.server.wm.AppTransitionInjector$ScaleYAnimation ( ) {
/* .locals 0 */
/* .param p1, "fromY" # F */
/* .param p2, "toY" # F */
/* .param p3, "pivotY" # F */
/* .line 1401 */
/* invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V */
/* .line 1402 */
/* iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mFromY:F */
/* .line 1403 */
/* iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mToY:F */
/* .line 1404 */
/* iput p3, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mPivotY:F */
/* .line 1405 */
return;
} // .end method
/* # virtual methods */
protected void applyTransformation ( Float p0, android.view.animation.Transformation p1 ) {
/* .locals 6 */
/* .param p1, "interpolatedTime" # F */
/* .param p2, "t" # Landroid/view/animation/Transformation; */
/* .line 1409 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 1410 */
/* .local v0, "sy":F */
v1 = (( com.android.server.wm.AppTransitionInjector$ScaleYAnimation ) p0 ).getScaleFactor ( ); // invoke-virtual {p0}, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->getScaleFactor()F
/* .line 1412 */
/* .local v1, "scale":F */
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mFromY:F */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* cmpl-float v4, v2, v3 */
/* if-nez v4, :cond_0 */
/* iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mToY:F */
/* cmpl-float v4, v4, v3 */
if ( v4 != null) { // if-eqz v4, :cond_1
	 /* .line 1413 */
} // :cond_0
/* iget v4, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mToY:F */
/* sub-float/2addr v4, v2 */
/* mul-float/2addr v4, p1 */
/* add-float v0, v2, v4 */
/* .line 1416 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mPivotY:F */
int v4 = 0; // const/4 v4, 0x0
/* cmpl-float v2, v2, v4 */
/* if-nez v2, :cond_2 */
/* .line 1417 */
(( android.view.animation.Transformation ) p2 ).getMatrix ( ); // invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
(( android.graphics.Matrix ) v2 ).setScale ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/graphics/Matrix;->setScale(FF)V
/* .line 1419 */
} // :cond_2
(( android.view.animation.Transformation ) p2 ).getMatrix ( ); // invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
/* iget v5, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;->mPivotY:F */
/* mul-float/2addr v5, v1 */
(( android.graphics.Matrix ) v2 ).setScale ( v3, v0, v4, v5 ); // invoke-virtual {v2, v3, v0, v4, v5}, Landroid/graphics/Matrix;->setScale(FFFF)V
/* .line 1421 */
} // :goto_0
return;
} // .end method
