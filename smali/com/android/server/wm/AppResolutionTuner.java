public class com.android.server.wm.AppResolutionTuner {
	 /* .source "AppResolutionTuner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/AppResolutionTuner$AppRTConfig; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
private static final Float DEFAULT_SCALE;
private static final java.lang.String FILTERED_WINDOWS_SEPARATOR;
private static final java.lang.String NODE_APP_RT_ACTIVITY_WHITELIST;
private static final java.lang.String NODE_APP_RT_CONFIG;
private static final java.lang.String NODE_APP_RT_FILTERED_WINDOWS;
private static final java.lang.String NODE_APP_RT_FLOW;
private static final java.lang.String NODE_APP_RT_PACKAGE_NAME;
private static final java.lang.String NODE_APP_RT_SCALE;
public static final java.lang.String TAG;
private static final java.lang.String VALUE_SCALING_FLOW_WMS;
private static volatile com.android.server.wm.AppResolutionTuner sAppResolutionTuner;
private static final java.lang.Object sLock;
/* # instance fields */
private java.util.Map mAppRTConfigMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mAppRTEnable;
/* # direct methods */
static com.android.server.wm.AppResolutionTuner ( ) {
/* .locals 2 */
/* .line 41 */
/* nop */
/* .line 42 */
final String v0 = "persist.sys.resolutiontuner.debug"; // const-string v0, "persist.sys.resolutiontuner.debug"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.AppResolutionTuner.DEBUG = (v0!= 0);
/* .line 49 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private com.android.server.wm.AppResolutionTuner ( ) {
/* .locals 2 */
/* .line 51 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 46 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAppRTConfigMap = v0;
/* .line 52 */
final String v0 = "persist.sys.resolutiontuner.enable"; // const-string v0, "persist.sys.resolutiontuner.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTEnable:Z */
/* .line 53 */
return;
} // .end method
public static com.android.server.wm.AppResolutionTuner getInstance ( ) {
/* .locals 2 */
/* .line 59 */
v0 = com.android.server.wm.AppResolutionTuner.sAppResolutionTuner;
/* if-nez v0, :cond_1 */
/* .line 60 */
v0 = com.android.server.wm.AppResolutionTuner.sLock;
/* monitor-enter v0 */
/* .line 61 */
try { // :try_start_0
v1 = com.android.server.wm.AppResolutionTuner.sAppResolutionTuner;
/* if-nez v1, :cond_0 */
/* .line 62 */
/* new-instance v1, Lcom/android/server/wm/AppResolutionTuner; */
/* invoke-direct {v1}, Lcom/android/server/wm/AppResolutionTuner;-><init>()V */
/* .line 63 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 65 */
} // :cond_1
} // :goto_0
v0 = com.android.server.wm.AppResolutionTuner.sAppResolutionTuner;
} // .end method
/* # virtual methods */
public Float getScaleValue ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 148 */
v0 = v0 = this.mAppRTConfigMap;
/* const/high16 v1, 0x3f800000 # 1.0f */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 149 */
} // :cond_0
v0 = this.mAppRTConfigMap;
/* check-cast v0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig; */
/* .line 150 */
/* .local v0, "config":Lcom/android/server/wm/AppResolutionTuner$AppRTConfig; */
/* if-nez v0, :cond_1 */
/* .line 151 */
} // :cond_1
v1 = com.android.server.wm.AppResolutionTuner$AppRTConfig .-$$Nest$mgetScale ( v0 );
} // .end method
public Boolean isAppRTEnable ( ) {
/* .locals 2 */
/* .line 127 */
/* sget-boolean v0, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "isAppRTEnable: "; // const-string v1, "isAppRTEnable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "APP_RT"; // const-string v1, "APP_RT"
android.util.Slog .d ( v1,v0 );
/* .line 128 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTEnable:Z */
} // .end method
public Boolean isScaledByWMS ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "windowName" # Ljava/lang/String; */
/* .line 135 */
v0 = v0 = this.mAppRTConfigMap;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 136 */
} // :cond_0
v0 = this.mAppRTConfigMap;
/* check-cast v0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig; */
/* .line 137 */
/* .local v0, "config":Lcom/android/server/wm/AppResolutionTuner$AppRTConfig; */
/* if-nez v0, :cond_1 */
/* .line 138 */
} // :cond_1
/* const-string/jumbo v2, "wms" */
com.android.server.wm.AppResolutionTuner$AppRTConfig .-$$Nest$mgetScalingFlow ( v0 );
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 139 */
v2 = com.android.server.wm.AppResolutionTuner$AppRTConfig .-$$Nest$mgetFilteredWindows ( v0 );
/* if-nez v2, :cond_2 */
/* .line 140 */
v2 = com.android.server.wm.AppResolutionTuner$AppRTConfig .-$$Nest$misActivityWhitelist ( v0,p2 );
if ( v2 != null) { // if-eqz v2, :cond_2
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
/* nop */
/* .line 141 */
/* .local v1, "ret":Z */
} // :goto_0
} // .end method
public void setAppRTEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 119 */
/* sget-boolean v0, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setAppRTEnable: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "APP_RT"; // const-string v1, "APP_RT"
android.util.Slog .d ( v1,v0 );
/* .line 120 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTEnable:Z */
/* .line 121 */
return;
} // .end method
public Boolean updateResolutionTunerConfig ( java.lang.String p0 ) {
/* .locals 26 */
/* .param p1, "configStr" # Ljava/lang/String; */
/* .line 72 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
final String v0 = ";"; // const-string v0, ";"
final String v9 = "flow"; // const-string v9, "flow"
final String v10 = "filtered_windows"; // const-string v10, "filtered_windows"
final String v11 = "scale"; // const-string v11, "scale"
final String v12 = "package_name"; // const-string v12, "package_name"
final String v1 = "app_rt_config"; // const-string v1, "app_rt_config"
/* sget-boolean v2, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z */
/* const-string/jumbo v13, "updateResolutionTunerConfig: " */
final String v14 = "APP_RT"; // const-string v14, "APP_RT"
if ( v2 != null) { // if-eqz v2, :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v13 ); // invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v14,v2 );
/* .line 73 */
} // :cond_0
if ( v8 != null) { // if-eqz v8, :cond_8
v2 = /* invoke-virtual/range {p1 ..p1}, Ljava/lang/String;->isEmpty()Z */
if ( v2 != null) { // if-eqz v2, :cond_1
int v1 = 0; // const/4 v1, 0x0
/* goto/16 :goto_3 */
/* .line 74 */
} // :cond_1
/* new-instance v2, Landroid/util/ArrayMap; */
/* invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V */
/* move-object v3, v2 */
/* .line 76 */
/* .local v3, "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
try { // :try_start_0
/* new-instance v2, Lorg/json/JSONObject; */
/* invoke-direct {v2, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 77 */
/* .local v2, "object":Lorg/json/JSONObject; */
v4 = (( org.json.JSONObject ) v2 ).has ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 78 */
(( org.json.JSONObject ) v2 ).getJSONArray ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 79 */
/* .local v1, "configs":Lorg/json/JSONArray; */
int v4 = 0; // const/4 v4, 0x0
/* move v6, v4 */
/* .local v6, "i":I */
} // :goto_0
v4 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-ge v6, v4, :cond_5 */
/* .line 80 */
(( org.json.JSONArray ) v1 ).getJSONObject ( v6 ); // invoke-virtual {v1, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 81 */
/* .local v4, "config":Lorg/json/JSONObject; */
v5 = (( org.json.JSONObject ) v4 ).has ( v12 ); // invoke-virtual {v4, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_4
v5 = (( org.json.JSONObject ) v4 ).has ( v11 ); // invoke-virtual {v4, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 82 */
v5 = (( org.json.JSONObject ) v4 ).has ( v10 ); // invoke-virtual {v4, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 83 */
v5 = (( org.json.JSONObject ) v4 ).has ( v9 ); // invoke-virtual {v4, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
/* if-nez v5, :cond_2 */
/* move-object/from16 v17, v1 */
/* move-object/from16 v16, v2 */
/* move-object v8, v3 */
/* move-object/from16 v25, v4 */
/* move/from16 v22, v6 */
/* goto/16 :goto_1 */
/* .line 87 */
} // :cond_2
(( org.json.JSONObject ) v4 ).getString ( v12 ); // invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 88 */
/* .local v5, "packageName":Ljava/lang/String; */
/* move-object/from16 v17, v1 */
/* move-object/from16 v16, v2 */
} // .end local v1 # "configs":Lorg/json/JSONArray;
} // .end local v2 # "object":Lorg/json/JSONObject;
/* .local v16, "object":Lorg/json/JSONObject; */
/* .local v17, "configs":Lorg/json/JSONArray; */
(( org.json.JSONObject ) v4 ).getDouble ( v11 ); // invoke-virtual {v4, v11}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D
/* move-result-wide v1 */
/* double-to-float v2, v1 */
/* .line 89 */
/* .local v2, "scale":F */
(( org.json.JSONObject ) v4 ).getString ( v9 ); // invoke-virtual {v4, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* move-object v15, v5 */
} // .end local v5 # "packageName":Ljava/lang/String;
/* .local v15, "packageName":Ljava/lang/String; */
/* move-object v5, v1 */
/* .line 90 */
/* .local v5, "scalingFlow":Ljava/lang/String; */
(( org.json.JSONObject ) v4 ).getString ( v10 ); // invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 91 */
/* .local v1, "filteredWindowsStr":Ljava/lang/String; */
/* nop */
/* .line 92 */
(( java.lang.String ) v1 ).split ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 93 */
/* .local v18, "filteredWindowsArray":[Ljava/lang/String; */
/* invoke-static/range {v18 ..v18}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List; */
/* move-object v8, v4 */
} // .end local v4 # "config":Lorg/json/JSONObject;
/* .local v8, "config":Lorg/json/JSONObject; */
/* move-object/from16 v4, v19 */
/* .line 95 */
/* .local v4, "filteredWindows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* move-object/from16 v19, v1 */
} // .end local v1 # "filteredWindowsStr":Ljava/lang/String;
/* .local v19, "filteredWindowsStr":Ljava/lang/String; */
final String v1 = "activity_whitelist"; // const-string v1, "activity_whitelist"
(( org.json.JSONObject ) v8 ).getString ( v1 ); // invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 96 */
/* .local v1, "activityWhitelistStr":Ljava/lang/String; */
(( java.lang.String ) v1 ).split ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 97 */
/* .local v20, "activityWhitelistArray":[Ljava/lang/String; */
/* invoke-static/range {v20 ..v20}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List; */
/* move/from16 v22, v6 */
} // .end local v6 # "i":I
/* .local v22, "i":I */
/* move-object/from16 v6, v21 */
/* .line 99 */
/* .local v6, "activityWhitelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* move-object/from16 v21, v0 */
/* new-instance v0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig; */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* move-object/from16 v23, v1 */
} // .end local v1 # "activityWhitelistStr":Ljava/lang/String;
/* .local v23, "activityWhitelistStr":Ljava/lang/String; */
/* move-object v1, v0 */
/* move/from16 v24, v2 */
} // .end local v2 # "scale":F
/* .local v24, "scale":F */
/* move-object/from16 v2, p0 */
/* move-object/from16 v25, v8 */
/* move-object v8, v3 */
} // .end local v3 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
/* .local v8, "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
/* .local v25, "config":Lorg/json/JSONObject; */
/* move/from16 v3, v24 */
try { // :try_start_1
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;-><init>(Lcom/android/server/wm/AppResolutionTuner;FLjava/util/List;Ljava/lang/String;Ljava/util/List;)V */
/* .line 79 */
/* nop */
} // .end local v4 # "filteredWindows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v5 # "scalingFlow":Ljava/lang/String;
} // .end local v6 # "activityWhitelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v15 # "packageName":Ljava/lang/String;
} // .end local v18 # "filteredWindowsArray":[Ljava/lang/String;
} // .end local v19 # "filteredWindowsStr":Ljava/lang/String;
} // .end local v20 # "activityWhitelistArray":[Ljava/lang/String;
} // .end local v23 # "activityWhitelistStr":Ljava/lang/String;
} // .end local v24 # "scale":F
} // .end local v25 # "config":Lorg/json/JSONObject;
/* add-int/lit8 v6, v22, 0x1 */
/* move-object v3, v8 */
/* move-object/from16 v2, v16 */
/* move-object/from16 v1, v17 */
/* move-object/from16 v0, v21 */
/* move-object/from16 v8, p1 */
} // .end local v22 # "i":I
/* .local v6, "i":I */
/* goto/16 :goto_0 */
/* .line 82 */
} // .end local v8 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
} // .end local v16 # "object":Lorg/json/JSONObject;
} // .end local v17 # "configs":Lorg/json/JSONArray;
/* .local v1, "configs":Lorg/json/JSONArray; */
/* .local v2, "object":Lorg/json/JSONObject; */
/* .restart local v3 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
/* .local v4, "config":Lorg/json/JSONObject; */
} // :cond_3
/* move-object/from16 v17, v1 */
/* move-object/from16 v16, v2 */
/* move-object v8, v3 */
/* move-object/from16 v25, v4 */
/* move/from16 v22, v6 */
} // .end local v1 # "configs":Lorg/json/JSONArray;
} // .end local v2 # "object":Lorg/json/JSONObject;
} // .end local v3 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
} // .end local v4 # "config":Lorg/json/JSONObject;
} // .end local v6 # "i":I
/* .restart local v8 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
/* .restart local v16 # "object":Lorg/json/JSONObject; */
/* .restart local v17 # "configs":Lorg/json/JSONArray; */
/* .restart local v22 # "i":I */
/* .restart local v25 # "config":Lorg/json/JSONObject; */
/* .line 81 */
} // .end local v8 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
} // .end local v16 # "object":Lorg/json/JSONObject;
} // .end local v17 # "configs":Lorg/json/JSONArray;
} // .end local v22 # "i":I
} // .end local v25 # "config":Lorg/json/JSONObject;
/* .restart local v1 # "configs":Lorg/json/JSONArray; */
/* .restart local v2 # "object":Lorg/json/JSONObject; */
/* .restart local v3 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
/* .restart local v4 # "config":Lorg/json/JSONObject; */
/* .restart local v6 # "i":I */
} // :cond_4
/* move-object/from16 v17, v1 */
/* move-object/from16 v16, v2 */
/* move-object v8, v3 */
/* move-object/from16 v25, v4 */
/* move/from16 v22, v6 */
/* .line 84 */
} // .end local v1 # "configs":Lorg/json/JSONArray;
} // .end local v2 # "object":Lorg/json/JSONObject;
} // .end local v3 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
} // .end local v4 # "config":Lorg/json/JSONObject;
} // .end local v6 # "i":I
/* .restart local v8 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
/* .restart local v16 # "object":Lorg/json/JSONObject; */
/* .restart local v17 # "configs":Lorg/json/JSONArray; */
/* .restart local v22 # "i":I */
/* .restart local v25 # "config":Lorg/json/JSONObject; */
} // :goto_1
/* const-string/jumbo v0, "uncompleted config!" */
android.util.Log .e ( v14,v0 );
/* .line 85 */
int v1 = 0; // const/4 v1, 0x0
/* .line 79 */
} // .end local v8 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
} // .end local v16 # "object":Lorg/json/JSONObject;
} // .end local v17 # "configs":Lorg/json/JSONArray;
} // .end local v22 # "i":I
} // .end local v25 # "config":Lorg/json/JSONObject;
/* .restart local v1 # "configs":Lorg/json/JSONArray; */
/* .restart local v2 # "object":Lorg/json/JSONObject; */
/* .restart local v3 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
/* .restart local v6 # "i":I */
} // :cond_5
/* move-object/from16 v17, v1 */
/* move-object/from16 v16, v2 */
/* move-object v8, v3 */
/* move/from16 v22, v6 */
/* .line 102 */
} // .end local v1 # "configs":Lorg/json/JSONArray;
} // .end local v2 # "object":Lorg/json/JSONObject;
} // .end local v3 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
} // .end local v6 # "i":I
/* .restart local v8 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
/* .restart local v16 # "object":Lorg/json/JSONObject; */
/* .restart local v17 # "configs":Lorg/json/JSONArray; */
this.mAppRTConfigMap = v8;
/* .line 103 */
/* sget-boolean v0, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAppRTConfigMap;
/* .line 104 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 103 */
android.util.Slog .d ( v14,v0 );
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 105 */
} // .end local v17 # "configs":Lorg/json/JSONArray;
} // :cond_6
/* nop */
/* .line 111 */
} // .end local v16 # "object":Lorg/json/JSONObject;
/* nop */
/* .line 112 */
int v0 = 1; // const/4 v0, 0x1
/* .line 108 */
/* :catch_0 */
/* move-exception v0 */
/* .line 106 */
} // .end local v8 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
/* .restart local v2 # "object":Lorg/json/JSONObject; */
/* .restart local v3 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
} // :cond_7
int v1 = 0; // const/4 v1, 0x0
/* .line 108 */
} // .end local v2 # "object":Lorg/json/JSONObject;
/* :catch_1 */
/* move-exception v0 */
/* move-object v8, v3 */
/* .line 109 */
} // .end local v3 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
/* .local v0, "e":Lorg/json/JSONException; */
/* .restart local v8 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;" */
} // :goto_2
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 110 */
int v1 = 0; // const/4 v1, 0x0
/* .line 73 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // .end local v8 # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
} // :cond_8
int v1 = 0; // const/4 v1, 0x0
} // :goto_3
} // .end method
