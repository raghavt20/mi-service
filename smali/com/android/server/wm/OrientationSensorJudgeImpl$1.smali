.class Lcom/android/server/wm/OrientationSensorJudgeImpl$1;
.super Ljava/lang/Object;
.source "OrientationSensorJudgeImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/OrientationSensorJudgeImpl;->updateForegroundApp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/OrientationSensorJudgeImpl;

    .line 81
    iput-object p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$1;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$1;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    invoke-static {v0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$fgetmActivityTaskManager(Lcom/android/server/wm/OrientationSensorJudgeImpl;)Landroid/app/IActivityTaskManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;

    move-result-object v0

    .line 86
    .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    if-eqz v0, :cond_7

    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_0

    goto/16 :goto_1

    .line 89
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x5

    const-string v3, "OrientationSensorJudgeImpl"

    if-eq v1, v2, :cond_5

    :try_start_1
    invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 96
    :cond_1
    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "packageName":Ljava/lang/String;
    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$1;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    invoke-static {v2}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$fgetmForegroundAppPackageName(Lcom/android/server/wm/OrientationSensorJudgeImpl;)Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 99
    invoke-static {}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$sfgetLOG()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 100
    const-string/jumbo v2, "updateForegroundApp, app didn\'t change, nothing to do!"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_2
    return-void

    .line 104
    :cond_3
    iget-object v2, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$1;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    invoke-static {v2, v1}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$fputmPendingForegroundAppPackageName(Lcom/android/server/wm/OrientationSensorJudgeImpl;Ljava/lang/String;)V

    .line 105
    invoke-static {}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$sfgetLOG()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 106
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateForegroundApp, packageName = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_4
    iget-object v2, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl$1;->this$0:Lcom/android/server/wm/OrientationSensorJudgeImpl;

    invoke-static {v2}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$fgetmHandler(Lcom/android/server/wm/OrientationSensorJudgeImpl;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 111
    nop

    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v1    # "packageName":Ljava/lang/String;
    goto :goto_2

    .line 91
    .restart local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :cond_5
    :goto_0
    invoke-static {}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->-$$Nest$sfgetLOG()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateForegroundApp, WindowingMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 94
    :cond_6
    return-void

    .line 87
    :cond_7
    :goto_1
    return-void

    .line 109
    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :catch_0
    move-exception v0

    .line 112
    :goto_2
    return-void
.end method
