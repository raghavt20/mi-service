public class com.android.server.wm.PreloadStateManagerImpl implements com.android.server.wm.PreloadStateManagerStub {
	 /* .source "PreloadStateManagerImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;, */
	 /* Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;, */
	 /* Lcom/android/server/wm/PreloadStateManagerImpl$GreezeCallback; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ADVANCE_KILL_PRELOADAPP_REASON;
public static final Integer DELAY_STOP_PRELOAD_ACTIVITIES_MSG;
public static final java.lang.String MUTE_PRELOAD_UID;
private static final java.lang.String PERMISSION_ACTIVITY_NAME;
private static final java.lang.String PRELOAD_DISPLAY_NAME;
public static final java.lang.String REMOVE_MUTE_PRELOAD_UID;
private static final java.lang.String TAG;
public static final java.lang.String TIMEOUT_KILL_PRELOADAPP_REASON;
private static com.android.server.wm.ActivityTaskManagerService mService;
private static final java.lang.Object sEnableAudioLock;
private static com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler sHandler;
private static java.util.Set sHorizontalPacakges;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static android.hardware.display.VirtualDisplay sHorizontalPreloadDisplay;
private static Boolean sPreloadAppStarted;
private static android.hardware.display.VirtualDisplay sPreloadDisplay;
private static java.util.Map sPreloadUidInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static com.miui.server.process.ProcessManagerInternal sProcessManagerInternal;
private static java.util.List sWaitEnableAudioUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.util.Map -$$Nest$sfgetsPreloadUidInfos ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
} // .end method
static void -$$Nest$smdisableAudio ( Boolean p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.wm.PreloadStateManagerImpl .disableAudio ( p0,p1 );
return;
} // .end method
static void -$$Nest$smonPreloadAppKilled ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.wm.PreloadStateManagerImpl .onPreloadAppKilled ( p0 );
return;
} // .end method
static com.android.server.wm.PreloadStateManagerImpl ( ) {
/* .locals 1 */
/* .line 63 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 69 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
/* .line 70 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 71 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 90 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.wm.PreloadStateManagerImpl.sPreloadAppStarted = (v0!= 0);
return;
} // .end method
public com.android.server.wm.PreloadStateManagerImpl ( ) {
/* .locals 0 */
/* .line 52 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static Boolean checkEnablePreload ( ) {
/* .locals 4 */
/* .line 75 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* const-class v1, Lcom/android/server/wm/Task; */
final String v2 = "removeStopPreloadActivityMsg"; // const-string v2, "removeStopPreloadActivityMsg"
/* new-array v3, v0, [Ljava/lang/Class; */
miui.util.ReflectionUtils .findMethodBestMatch ( v1,v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 82 */
/* nop */
/* .line 83 */
int v0 = 1; // const/4 v0, 0x1
/* .line 77 */
/* :catch_0 */
/* move-exception v1 */
/* .line 78 */
/* .local v1, "e":Ljava/lang/Exception; */
/* sget-boolean v2, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 79 */
final String v2 = "PreloadStateManagerImpl"; // const-string v2, "PreloadStateManagerImpl"
final String v3 = "preloadApp lack of some changes"; // const-string v3, "preloadApp lack of some changes"
android.util.Slog .e ( v2,v3,v1 );
/* .line 81 */
} // :cond_0
} // .end method
private static android.hardware.display.VirtualDisplay createPreloadDisplay ( Integer p0 ) {
/* .locals 11 */
/* .param p0, "orientation" # I */
/* .line 568 */
/* new-instance v0, Landroid/view/DisplayInfo; */
/* invoke-direct {v0}, Landroid/view/DisplayInfo;-><init>()V */
/* .line 569 */
/* .local v0, "info":Landroid/view/DisplayInfo; */
/* new-instance v6, Landroid/view/Surface; */
/* invoke-direct {v6}, Landroid/view/Surface;-><init>()V */
/* .line 570 */
/* .local v6, "surface":Landroid/view/Surface; */
v1 = com.android.server.wm.PreloadStateManagerImpl.mService;
v1 = this.mContext;
/* const-class v2, Landroid/hardware/display/DisplayManager; */
/* .line 571 */
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* move-object v8, v1 */
/* check-cast v8, Landroid/hardware/display/DisplayManager; */
/* .line 572 */
/* .local v8, "displayManager":Landroid/hardware/display/DisplayManager; */
int v1 = 0; // const/4 v1, 0x0
(( android.hardware.display.DisplayManager ) v8 ).getDisplay ( v1 ); // invoke-virtual {v8, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
(( android.view.Display ) v1 ).getDisplayInfo ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z
/* .line 573 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v9 */
/* .line 574 */
/* .local v9, "origId":J */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p0, v1, :cond_0 */
/* .line 575 */
/* iget v1, v0, Landroid/view/DisplayInfo;->logicalHeight:I */
/* .line 576 */
/* .local v1, "h":I */
/* iget v2, v0, Landroid/view/DisplayInfo;->logicalWidth:I */
/* iput v2, v0, Landroid/view/DisplayInfo;->logicalHeight:I */
/* .line 577 */
/* iput v1, v0, Landroid/view/DisplayInfo;->logicalWidth:I */
/* .line 580 */
} // .end local v1 # "h":I
} // :cond_0
final String v2 = "PreloadDisplay"; // const-string v2, "PreloadDisplay"
/* iget v3, v0, Landroid/view/DisplayInfo;->logicalWidth:I */
/* iget v4, v0, Landroid/view/DisplayInfo;->logicalHeight:I */
/* iget v5, v0, Landroid/view/DisplayInfo;->logicalDensityDpi:I */
int v7 = 4; // const/4 v7, 0x4
/* move-object v1, v8 */
/* invoke-virtual/range {v1 ..v7}, Landroid/hardware/display/DisplayManager;->createVirtualDisplay(Ljava/lang/String;IIILandroid/view/Surface;I)Landroid/hardware/display/VirtualDisplay; */
/* .line 583 */
/* .local v1, "display":Landroid/hardware/display/VirtualDisplay; */
android.os.Binder .restoreCallingIdentity ( v9,v10 );
/* .line 584 */
} // .end method
private void delayFreezeAppForUid ( com.android.server.wm.PreloadStateManagerImpl$UidInfo p0 ) {
/* .locals 5 */
/* .param p1, "info" # Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo; */
/* .line 492 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 493 */
/* .local v0, "msgFreeze":Landroid/os/Message; */
this.obj = p1;
/* .line 494 */
/* iget v1, p1, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I */
com.android.server.am.PreloadAppControllerImpl .queryFreezeTimeoutFromUid ( v1 );
/* move-result-wide v1 */
/* .line 495 */
/* .local v1, "freezeTimeout":J */
v3 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v3 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {v3, v0, v1, v2}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 496 */
/* sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 497 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "preloadApp onAttachApplication uid "; // const-string v4, "preloadApp onAttachApplication uid "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p1, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " freezeTimeout="; // const-string v4, " freezeTimeout="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "PreloadStateManagerImpl"; // const-string v4, "PreloadStateManagerImpl"
android.util.Slog .w ( v4,v3 );
/* .line 500 */
} // :cond_0
return;
} // .end method
private static void disableAudio ( Boolean p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "disable" # Z */
/* .param p1, "uid" # I */
/* .line 153 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 154 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "preloadApp enableAudio "; // const-string v1, "preloadApp enableAudio "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " uid="; // const-string v1, " uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PreloadStateManagerImpl"; // const-string v1, "PreloadStateManagerImpl"
android.util.Slog .w ( v1,v0 );
/* .line 156 */
} // :cond_0
if ( p0 != null) { // if-eqz p0, :cond_1
/* .line 157 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mute_preload_uid="; // const-string v1, "mute_preload_uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.media.AudioSystem .setParameters ( v0 );
/* .line 159 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "remove_mute_preload_uid="; // const-string v1, "remove_mute_preload_uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.media.AudioSystem .setParameters ( v0 );
/* .line 161 */
} // :goto_0
return;
} // .end method
public static void enableAudio ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "uid" # I */
/* .line 588 */
android.os.Message .obtain ( );
/* .line 589 */
/* .local v0, "enableMsg":Landroid/os/Message; */
/* iput p0, v0, Landroid/os/Message;->arg1:I */
/* .line 590 */
int v1 = 3; // const/4 v1, 0x3
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 591 */
v1 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
/* const-wide/16 v2, 0x12c */
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 592 */
return;
} // .end method
public static Integer getOrCreatePreloadDisplayId ( ) {
/* .locals 4 */
/* .line 520 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sPreloadDisplay;
/* if-nez v0, :cond_1 */
/* .line 521 */
/* const-class v0, Lcom/android/server/wm/PreloadStateManagerImpl; */
/* monitor-enter v0 */
/* .line 522 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
com.android.server.wm.PreloadStateManagerImpl .createPreloadDisplay ( v1 );
/* .line 523 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 525 */
/* .local v1, "service":Lcom/miui/server/greeze/GreezeManagerInternal; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 526 */
try { // :try_start_1
/* new-instance v2, Lcom/android/server/wm/PreloadStateManagerImpl$GreezeCallback; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, v3}, Lcom/android/server/wm/PreloadStateManagerImpl$GreezeCallback;-><init>(Lcom/android/server/wm/PreloadStateManagerImpl$GreezeCallback-IA;)V */
(( com.miui.server.greeze.GreezeManagerInternal ) v1 ).registerCallback ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerInternal;->registerCallback(Lmiui/greeze/IGreezeCallback;I)Z
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 529 */
/* :catch_0 */
/* move-exception v2 */
/* .line 530 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_2
(( android.os.RemoteException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 531 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
/* nop */
/* .line 532 */
} // .end local v1 # "service":Lcom/miui/server/greeze/GreezeManagerInternal;
} // :goto_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 534 */
} // :cond_1
} // :goto_2
v0 = com.android.server.wm.PreloadStateManagerImpl.sPreloadDisplay;
(( android.hardware.display.VirtualDisplay ) v0 ).getDisplay ( ); // invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;
v0 = (( android.view.Display ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I
} // .end method
public static Integer getOrCreatePreloadDisplayId ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 538 */
v0 = v0 = com.android.server.wm.PreloadStateManagerImpl.sHorizontalPacakges;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 539 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sHorizontalPreloadDisplay;
/* if-nez v0, :cond_0 */
/* .line 540 */
/* const-class v0, Lcom/android/server/wm/PreloadStateManagerImpl; */
/* monitor-enter v0 */
/* .line 541 */
int v1 = 1; // const/4 v1, 0x1
try { // :try_start_0
com.android.server.wm.PreloadStateManagerImpl .createPreloadDisplay ( v1 );
/* .line 542 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 544 */
} // :cond_0
} // :goto_0
v0 = com.android.server.wm.PreloadStateManagerImpl.sHorizontalPreloadDisplay;
(( android.hardware.display.VirtualDisplay ) v0 ).getDisplay ( ); // invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;
v0 = (( android.view.Display ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I
/* .line 547 */
} // :cond_1
v0 = com.android.server.wm.PreloadStateManagerImpl .getOrCreatePreloadDisplayId ( );
} // .end method
private java.util.List getRunningProcessInfos ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lmiui/process/RunningProcessInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 146 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sProcessManagerInternal;
/* if-nez v0, :cond_0 */
/* .line 147 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
/* .line 149 */
} // :cond_0
v0 = com.android.server.wm.PreloadStateManagerImpl.sProcessManagerInternal;
(( com.miui.server.process.ProcessManagerInternal ) v0 ).getAllRunningProcessInfo ( ); // invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getAllRunningProcessInfo()Ljava/util/List;
} // .end method
public static void killPreloadApp ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "uid" # I */
/* .line 595 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
/* .line 596 */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "advance_kill_preloadApp"; // const-string v2, "advance_kill_preloadApp"
(( com.miui.server.process.ProcessManagerInternal ) v0 ).forceStopPackage ( p0, v1, v2 ); // invoke-virtual {v0, p0, v1, v2}, Lcom/miui/server/process/ProcessManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 597 */
com.android.server.wm.PreloadStateManagerImpl .onPreloadAppKilled ( p1 );
/* .line 598 */
return;
} // .end method
private com.android.server.wm.PreloadStateManagerImpl$UidInfo onFirstProcAttachForUid ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 503 */
/* new-instance v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo; */
/* invoke-direct {v0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;-><init>(I)V */
/* .line 504 */
/* .local v0, "info":Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo; */
v1 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
java.lang.Integer .valueOf ( p1 );
/* .line 506 */
v1 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
v2 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
java.lang.Integer .valueOf ( p1 );
int v3 = 2; // const/4 v3, 0x2
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v1 ).removeMessages ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->removeMessages(ILjava/lang/Object;)V
/* .line 507 */
v1 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v1 ).obtainMessage ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 508 */
/* .local v1, "msg":Landroid/os/Message; */
this.obj = v0;
/* .line 509 */
com.android.server.am.PreloadAppControllerImpl .queryKillTimeoutFromUid ( p1 );
/* move-result-wide v2 */
/* .line 510 */
/* .local v2, "killTimeout":J */
v4 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v4 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v4, v1, v2, v3}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 512 */
/* sget-boolean v4, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 513 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "preloadApp onFirstProcAttach uid "; // const-string v5, "preloadApp onFirstProcAttach uid "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " killTimeout="; // const-string v5, " killTimeout="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "PreloadStateManagerImpl"; // const-string v5, "PreloadStateManagerImpl"
android.util.Slog .w ( v5,v4 );
/* .line 516 */
} // :cond_0
} // .end method
private static void onPreloadAppKilled ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "uid" # I */
/* .line 555 */
com.android.server.am.PreloadAppControllerImpl .getInstance ( );
/* .line 556 */
/* .local v0, "controller":Lcom/android/server/am/PreloadAppControllerImpl; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 557 */
(( com.android.server.am.PreloadAppControllerImpl ) v0 ).onPreloadAppKilled ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/am/PreloadAppControllerImpl;->onPreloadAppKilled(I)V
/* .line 559 */
} // :cond_0
v1 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
java.lang.Integer .valueOf ( p0 );
/* .line 560 */
int v1 = 0; // const/4 v1, 0x0
com.android.server.wm.PreloadStateManagerImpl .disableAudio ( v1,p0 );
/* .line 561 */
/* sget-boolean v1, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 562 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "preloadApp is Killed, remove uid "; // const-string v2, "preloadApp is Killed, remove uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "PreloadStateManagerImpl"; // const-string v2, "PreloadStateManagerImpl"
android.util.Slog .w ( v2,v1 );
/* .line 564 */
} // :cond_1
v1 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
v2 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
java.lang.Integer .valueOf ( p0 );
int v3 = 2; // const/4 v3, 0x2
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v1 ).removeMessages ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->removeMessages(ILjava/lang/Object;)V
/* .line 565 */
return;
} // .end method
private void onPreloadStackHasReparent ( com.android.server.wm.Task p0, com.android.server.wm.ActivityRecord p1, Boolean p2, android.app.ActivityOptions p3 ) {
/* .locals 4 */
/* .param p1, "targetStack" # Lcom/android/server/wm/Task; */
/* .param p2, "intentActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "noAnimation" # Z */
/* .param p4, "options" # Landroid/app/ActivityOptions; */
/* .line 314 */
v0 = com.android.server.wm.PreloadStateManagerImpl.mService;
v0 = this.mTaskSupervisor;
v0 = this.mRecentTasks;
(( com.android.server.wm.ActivityRecord ) p2 ).getTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.RecentTasks ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/RecentTasks;->add(Lcom/android/server/wm/Task;)V
/* .line 316 */
v0 = this.app;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 317 */
v0 = this.app;
/* iput-boolean v1, v0, Lcom/android/server/wm/WindowProcessController;->mIsPreloaded:Z */
/* .line 321 */
} // :cond_0
try { // :try_start_0
/* const-class v0, Lcom/android/server/wm/Task; */
final String v2 = "removeStopPreloadActivityMsg"; // const-string v2, "removeStopPreloadActivityMsg"
/* new-array v3, v1, [Ljava/lang/Class; */
miui.util.ReflectionUtils .findMethodBestMatch ( v0,v2,v3 );
/* .line 323 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
/* new-array v2, v1, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v0 ).invoke ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 328 */
/* nop */
} // .end local v0 # "method":Ljava/lang/reflect/Method;
/* .line 324 */
/* :catch_0 */
/* move-exception v0 */
/* .line 325 */
/* .local v0, "e":Ljava/lang/Exception; */
/* sget-boolean v2, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 326 */
final String v2 = "PreloadStateManagerImpl"; // const-string v2, "PreloadStateManagerImpl"
final String v3 = "preloadApp removeStopPreloadActivityMsg reflect Exception:"; // const-string v3, "preloadApp removeStopPreloadActivityMsg reflect Exception:"
android.util.Slog .e ( v2,v3,v0 );
/* .line 329 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
/* if-nez p3, :cond_3 */
/* .line 331 */
v0 = com.android.server.wm.ActivityRecord$State.RESUMED;
v0 = (( com.android.server.wm.ActivityRecord ) p2 ).isState ( v0 ); // invoke-virtual {p2, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 332 */
v0 = com.android.server.wm.ActivityRecord$State.PAUSED;
final String v2 = "preload activity reset state"; // const-string v2, "preload activity reset state"
(( com.android.server.wm.ActivityRecord ) p2 ).setState ( v0, v2 ); // invoke-virtual {p2, v0, v2}, Lcom/android/server/wm/ActivityRecord;->setState(Lcom/android/server/wm/ActivityRecord$State;Ljava/lang/String;)V
/* .line 333 */
(( com.android.server.wm.ActivityRecord ) p2 ).setVisible ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/wm/ActivityRecord;->setVisible(Z)V
/* .line 335 */
} // :cond_2
(( com.android.server.wm.ActivityRecord ) p2 ).updateOptionsLocked ( p4 ); // invoke-virtual {p2, p4}, Lcom/android/server/wm/ActivityRecord;->updateOptionsLocked(Landroid/app/ActivityOptions;)V
/* .line 336 */
v0 = this.mDisplayContent;
/* .line 337 */
int v1 = 3; // const/4 v1, 0x3
(( com.android.server.wm.DisplayContent ) v0 ).prepareAppTransition ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayContent;->prepareAppTransition(I)V
/* .line 339 */
} // :cond_3
return;
} // .end method
private Boolean setSchedAffinity ( Integer p0, Integer[] p1 ) {
/* .locals 6 */
/* .param p1, "pid" # I */
/* .param p2, "schedAffinity" # [I */
/* .line 171 */
/* const-class v0, Landroid/os/Process; */
/* .line 172 */
java.lang.Integer .valueOf ( p1 );
/* filled-new-array {v1, p2}, [Ljava/lang/Object; */
/* .line 171 */
/* const-string/jumbo v2, "setSchedAffinity" */
miui.util.ReflectionUtils .tryFindMethodBestMatch ( v0,v2,v1 );
/* .line 173 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 174 */
/* .line 178 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
try { // :try_start_0
/* const-class v3, Landroid/os/Process; */
int v4 = 2; // const/4 v4, 0x2
/* new-array v4, v4, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
/* aput-object v5, v4, v1 */
/* aput-object p2, v4, v2 */
(( java.lang.reflect.Method ) v0 ).invoke ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 181 */
/* .line 179 */
/* :catch_0 */
/* move-exception v3 */
/* .line 180 */
/* .local v3, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* .line 182 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
/* sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 183 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 184 */
/* .local v3, "sb":Ljava/lang/StringBuilder; */
/* array-length v4, p2 */
} // :goto_1
/* if-ge v1, v4, :cond_1 */
/* aget v5, p2, v1 */
/* .line 185 */
/* .local v5, "i":I */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 184 */
} // .end local v5 # "i":I
/* add-int/lit8 v1, v1, 0x1 */
/* .line 187 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "preloadApp reset cpuset pid:"; // const-string v4, "preloadApp reset cpuset pid:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " schedAffinity:"; // const-string v4, " schedAffinity:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "PreloadStateManagerImpl"; // const-string v4, "PreloadStateManagerImpl"
android.util.Slog .w ( v4,v1 );
/* .line 189 */
} // .end local v3 # "sb":Ljava/lang/StringBuilder;
} // :cond_2
} // .end method
/* # virtual methods */
public Integer adjustDisplayId ( com.android.server.wm.Session p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "session" # Lcom/android/server/wm/Session; */
/* .param p2, "displayId" # I */
/* .line 427 */
/* iget v0, p1, Lcom/android/server/wm/Session;->mUid:I */
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez p2, :cond_0 */
/* .line 428 */
p2 = com.android.server.wm.PreloadStateManagerImpl .getOrCreatePreloadDisplayId ( );
/* .line 429 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "preloadApp adjustDisplayId displayId="; // const-string v1, "preloadApp adjustDisplayId displayId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PreloadStateManagerImpl"; // const-string v1, "PreloadStateManagerImpl"
android.util.Slog .w ( v1,v0 );
/* .line 431 */
} // :cond_0
} // .end method
public Boolean alreadyPreload ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 194 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
v0 = java.lang.Integer .valueOf ( p1 );
} // .end method
public void checkEnableAudio ( ) {
/* .locals 7 */
/* .line 399 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sEnableAudioLock;
/* monitor-enter v0 */
/* .line 400 */
try { // :try_start_0
v1 = v1 = com.android.server.wm.PreloadStateManagerImpl.sWaitEnableAudioUids;
/* if-nez v1, :cond_0 */
/* .line 401 */
/* monitor-exit v0 */
return;
/* .line 403 */
} // :cond_0
v1 = com.android.server.wm.PreloadStateManagerImpl.sWaitEnableAudioUids;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 404 */
/* .local v2, "uid":I */
v3 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v3 ).obtainMessage ( ); // invoke-virtual {v3}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->obtainMessage()Landroid/os/Message;
/* .line 405 */
/* .local v3, "message":Landroid/os/Message; */
/* iput v2, v3, Landroid/os/Message;->arg1:I */
/* .line 406 */
int v4 = 4; // const/4 v4, 0x4
/* iput v4, v3, Landroid/os/Message;->what:I */
/* .line 407 */
v4 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
/* const-wide/16 v5, 0x64 */
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v4 ).sendMessageDelayed ( v3, v5, v6 ); // invoke-virtual {v4, v3, v5, v6}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 408 */
/* nop */
} // .end local v2 # "uid":I
} // .end local v3 # "message":Landroid/os/Message;
/* .line 409 */
} // :cond_1
v1 = com.android.server.wm.PreloadStateManagerImpl.sWaitEnableAudioUids;
/* .line 410 */
/* monitor-exit v0 */
/* .line 411 */
return;
/* .line 410 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 761 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "preload enable:"; // const-string v1, "preload enable:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.am.PreloadAppControllerImpl .isEnable ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 762 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "preload debug:"; // const-string v1, "preload debug:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 763 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "preload display:"; // const-string v1, "preload display:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.wm.PreloadStateManagerImpl.sPreloadDisplay;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 765 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/lang/Integer; */
/* .line 766 */
/* .local v1, "uid":Ljava/lang/Integer; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "already preload uid="; // const-string v3, "already preload uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 767 */
} // .end local v1 # "uid":Ljava/lang/Integer;
/* .line 768 */
} // :cond_0
return;
} // .end method
public Integer getPreloadDisplayId ( ) {
/* .locals 1 */
/* .line 551 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sPreloadDisplay;
/* if-nez v0, :cond_0 */
int v0 = -1; // const/4 v0, -0x1
} // :cond_0
(( android.hardware.display.VirtualDisplay ) v0 ).getDisplay ( ); // invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;
v0 = (( android.view.Display ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I
} // :goto_0
} // .end method
public Boolean hidePreloadActivity ( com.android.server.wm.ActivityRecord p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "next" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "forceStop" # Z */
/* .line 375 */
if ( p2 != null) { // if-eqz p2, :cond_0
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 376 */
int v0 = 1; // const/4 v0, 0x1
/* .line 378 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void init ( com.android.server.wm.ActivityTaskManagerService p0 ) {
/* .locals 2 */
/* .param p1, "mService" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 94 */
/* .line 95 */
/* new-instance v0, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler; */
v1 = this.mH;
(( com.android.server.wm.ActivityTaskManagerService$H ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService$H;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;-><init>(Landroid/os/Looper;)V */
/* .line 96 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sHorizontalPacakges;
final String v1 = "com.tencent.tmgp.sgame"; // const-string v1, "com.tencent.tmgp.sgame"
/* .line 97 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sHorizontalPacakges;
final String v1 = "com.tencent.tmgp.pubgmhd"; // const-string v1, "com.tencent.tmgp.pubgmhd"
/* .line 98 */
return;
} // .end method
public Boolean isPreloadDisplay ( android.view.Display p0 ) {
/* .locals 1 */
/* .param p1, "display" # Landroid/view/Display; */
/* .line 198 */
/* if-nez p1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
v0 = (( android.view.Display ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Landroid/view/Display;->getDisplayId()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
} // :goto_0
} // .end method
public Boolean isPreloadDisplayContent ( com.android.server.wm.DisplayContent p0 ) {
/* .locals 1 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 203 */
/* if-nez p1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
v0 = (( com.android.server.wm.DisplayContent ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
} // :goto_0
} // .end method
public Boolean isPreloadDisplayId ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "displayId" # I */
/* .line 208 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sHorizontalPreloadDisplay;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 209 */
(( android.hardware.display.VirtualDisplay ) v0 ).getDisplay ( ); // invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;
v0 = (( android.view.Display ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I
/* if-ne v0, p1, :cond_0 */
/* .line 210 */
/* .line 214 */
} // :cond_0
v0 = com.android.server.wm.PreloadStateManagerImpl.sPreloadDisplay;
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_1 */
/* .line 216 */
} // :cond_1
(( android.hardware.display.VirtualDisplay ) v0 ).getDisplay ( ); // invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;
v0 = (( android.view.Display ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I
/* if-ne v0, p1, :cond_2 */
} // :cond_2
/* move v1, v2 */
} // :goto_0
} // .end method
public Boolean needNotRelaunch ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .param p2, "packagename" # Ljava/lang/String; */
/* .line 228 */
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 229 */
v0 = com.android.server.am.PreloadAppControllerImpl .inRelaunchBlackList ( p2 );
/* if-nez v0, :cond_0 */
/* .line 230 */
int v0 = 1; // const/4 v0, 0x1
/* .line 232 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean needReparent ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1, com.android.server.wm.ActivityStarter$Request p2, com.android.server.wm.Task p3 ) {
/* .locals 4 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "request" # Lcom/android/server/wm/ActivityStarter$Request; */
/* .param p4, "inTask" # Lcom/android/server/wm/Task; */
/* .line 277 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 278 */
/* .line 281 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-nez p2, :cond_3 */
/* .line 282 */
/* const-string/jumbo v2, "startActivityFromRecents" */
v3 = this.reason;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 283 */
if ( p4 != null) { // if-eqz p4, :cond_1
/* .line 284 */
/* iput-boolean v1, p4, Lcom/android/server/wm/Task;->inRecents:Z */
/* .line 286 */
} // :cond_1
/* .line 288 */
} // :cond_2
/* .line 289 */
} // :cond_3
v2 = (( com.android.server.wm.ActivityRecord ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v2 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
/* if-nez v2, :cond_4 */
/* .line 290 */
/* .line 293 */
} // :cond_4
} // .end method
public Boolean notReplaceDisplayContent ( Integer p0, com.android.server.wm.WindowToken p1 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .param p2, "wToken" # Lcom/android/server/wm/WindowToken; */
/* .line 464 */
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 465 */
(( com.android.server.wm.WindowToken ) p2 ).getDisplayContent ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowToken;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 466 */
(( com.android.server.wm.WindowToken ) p2 ).getDisplayContent ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowToken;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
v0 = (( com.android.server.wm.DisplayContent ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
/* if-eq p1, v0, :cond_0 */
/* .line 467 */
final String v0 = "PreloadStateManagerImpl"; // const-string v0, "PreloadStateManagerImpl"
final String v1 = "preloadApp need replace display content"; // const-string v1, "preloadApp need replace display content"
android.util.Slog .w ( v0,v1 );
/* .line 468 */
int v0 = 0; // const/4 v0, 0x0
/* .line 470 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void onAttachApplication ( com.android.server.wm.WindowProcessController p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/wm/WindowProcessController; */
/* .line 475 */
/* iget v0, p1, Lcom/android/server/wm/WindowProcessController;->mUid:I */
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
/* if-nez v0, :cond_0 */
/* .line 476 */
return;
/* .line 478 */
} // :cond_0
v0 = (( com.android.server.wm.WindowProcessController ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* iget v1, p1, Lcom/android/server/wm/WindowProcessController;->mUid:I */
com.android.server.am.PreloadAppControllerImpl .querySchedAffinityFromUid ( v1 );
/* invoke-direct {p0, v0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->setSchedAffinity(I[I)Z */
/* .line 479 */
v0 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
/* iget v1, p1, Lcom/android/server/wm/WindowProcessController;->mUid:I */
java.lang.Integer .valueOf ( v1 );
/* check-cast v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo; */
/* .line 480 */
/* .local v0, "info":Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v1, v0, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;->uid:I */
/* if-gez v1, :cond_2 */
/* .line 481 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p1, Lcom/android/server/wm/WindowProcessController;->mIsPreloaded:Z */
/* .line 482 */
/* iget v1, p1, Lcom/android/server/wm/WindowProcessController;->mUid:I */
/* invoke-direct {p0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->onFirstProcAttachForUid(I)Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo; */
/* .line 485 */
} // :cond_2
v1 = com.miui.server.greeze.GreezeManagerDebugConfig .isEnable ( );
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 486 */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->delayFreezeAppForUid(Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;)V */
/* .line 489 */
} // :cond_3
return;
} // .end method
public void onMoveStackToDefaultDisplay ( Integer p0, com.android.server.wm.Task p1, com.android.server.wm.ActivityRecord p2, Boolean p3, android.app.ActivityOptions p4 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .param p2, "targetStack" # Lcom/android/server/wm/Task; */
/* .param p3, "intentActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "noAnimation" # Z */
/* .param p5, "options" # Landroid/app/ActivityOptions; */
/* .line 299 */
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 300 */
v0 = (( com.android.server.wm.Task ) p2 ).getDisplayId ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getDisplayId()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
/* if-nez v0, :cond_1 */
/* .line 302 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 303 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "preloadApp move task to DefaultDisplay uid="; // const-string v1, "preloadApp move task to DefaultDisplay uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 304 */
v1 = (( com.android.server.wm.ActivityRecord ) p3 ).getUid ( ); // invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->getUid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ";packageName="; // const-string v1, ";packageName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 303 */
final String v1 = "PreloadStateManagerImpl"; // const-string v1, "PreloadStateManagerImpl"
android.util.Slog .w ( v1,v0 );
/* .line 308 */
} // :cond_0
/* invoke-direct {p0, p2, p3, p4, p5}, Lcom/android/server/wm/PreloadStateManagerImpl;->onPreloadStackHasReparent(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;ZLandroid/app/ActivityOptions;)V */
/* .line 310 */
} // :cond_1
return;
} // .end method
public void removePreloadTask ( Integer p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 5 */
/* .param p1, "displayId" # I */
/* .param p2, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 415 */
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 416 */
(( com.android.server.wm.ActivityRecord ) p2 ).getTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* .line 417 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 418 */
v1 = com.android.server.wm.PreloadStateManagerImpl.mService;
v1 = this.mTaskSupervisor;
int v2 = 1; // const/4 v2, 0x1
final String v3 = "remove preloadApp task"; // const-string v3, "remove preloadApp task"
int v4 = 0; // const/4 v4, 0x0
(( com.android.server.wm.ActivityTaskSupervisor ) v1 ).removeTask ( v0, v4, v2, v3 ); // invoke-virtual {v1, v0, v4, v2, v3}, Lcom/android/server/wm/ActivityTaskSupervisor;->removeTask(Lcom/android/server/wm/Task;ZZLjava/lang/String;)V
/* .line 422 */
} // .end local v0 # "task":Lcom/android/server/wm/Task;
} // :cond_0
return;
} // .end method
public void reparentPreloadStack ( com.android.server.wm.Task p0, com.android.server.wm.ActivityRecord p1, Boolean p2, android.app.ActivityOptions p3 ) {
/* .locals 4 */
/* .param p1, "targetStack" # Lcom/android/server/wm/Task; */
/* .param p2, "intentActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "noAnimation" # Z */
/* .param p4, "options" # Landroid/app/ActivityOptions; */
/* .line 437 */
v0 = (( com.android.server.wm.Task ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 438 */
v0 = (( com.android.server.wm.ActivityRecord ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
/* if-nez v0, :cond_1 */
/* .line 439 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 440 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "preloadApp reparentPreloadStack "; // const-string v1, "preloadApp reparentPreloadStack "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 441 */
v1 = (( com.android.server.wm.ActivityRecord ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ";packageName="; // const-string v1, ";packageName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 440 */
final String v1 = "PreloadStateManagerImpl"; // const-string v1, "PreloadStateManagerImpl"
android.util.Slog .w ( v1,v0 );
/* .line 444 */
} // :cond_0
v0 = com.android.server.wm.PreloadStateManagerImpl.mService;
v0 = this.mRootWindowContainer;
v1 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.wm.RootWindowContainer ) v0 ).moveRootTaskToDisplay ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/RootWindowContainer;->moveRootTaskToDisplay(IIZ)V
/* .line 446 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/PreloadStateManagerImpl;->onPreloadStackHasReparent(Lcom/android/server/wm/Task;Lcom/android/server/wm/ActivityRecord;ZLandroid/app/ActivityOptions;)V */
/* .line 448 */
} // :cond_1
return;
} // .end method
public Integer replaceIdForPreloadApp ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .line 220 */
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = android.os.Binder .getCallingUid ( );
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 221 */
int v0 = 0; // const/4 v0, 0x0
/* .line 223 */
} // :cond_0
} // .end method
public Integer repositionDisplayContent ( Integer p0, java.lang.Object p1 ) {
/* .locals 2 */
/* .param p1, "position" # I */
/* .param p2, "displayContent" # Ljava/lang/Object; */
/* .line 452 */
/* instance-of v0, p2, Lcom/android/server/wm/DisplayContent; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 453 */
/* move-object v0, p2 */
/* check-cast v0, Lcom/android/server/wm/DisplayContent; */
v0 = (( com.android.server.wm.DisplayContent ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 454 */
final String v0 = "PreloadStateManagerImpl"; // const-string v0, "PreloadStateManagerImpl"
final String v1 = "preloadApp need reposition display content"; // const-string v1, "preloadApp need reposition display content"
android.util.Slog .w ( v0,v1 );
/* .line 455 */
int v0 = 0; // const/4 v0, 0x0
/* .line 458 */
} // :cond_0
} // .end method
public com.android.server.wm.Task resetInTask ( com.android.server.wm.Task p0 ) {
/* .locals 1 */
/* .param p1, "inTask" # Lcom/android/server/wm/Task; */
/* .line 164 */
/* sget-boolean v0, Lcom/android/server/wm/PreloadStateManagerImpl;->sPreloadAppStarted:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 165 */
int v0 = 0; // const/4 v0, 0x0
/* .line 167 */
} // :cond_0
} // .end method
public void resumeTopActivityInjector ( com.android.server.wm.ActivityRecord p0, android.os.Handler p1 ) {
/* .locals 4 */
/* .param p1, "next" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 383 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
/* if-nez v0, :cond_0 */
/* .line 384 */
return;
/* .line 387 */
} // :cond_0
/* const/16 v0, 0xc7 */
v1 = (( android.os.Handler ) p2 ).hasMessages ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Handler;->hasMessages(I)Z
/* if-nez v1, :cond_1 */
/* .line 388 */
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
com.android.server.am.PreloadAppControllerImpl .queryStopTimeoutFromUid ( v1 );
/* move-result-wide v1 */
/* .line 389 */
/* .local v1, "stopTimeout":J */
(( android.os.Handler ) p2 ).sendEmptyMessageDelayed ( v0, v1, v2 ); // invoke-virtual {p2, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 390 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 391 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "preloadApp uid="; // const-string v3, "preloadApp uid="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " pid="; // const-string v3, " pid="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( com.android.server.wm.ActivityRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " will stop after:"; // const-string v3, " will stop after:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "PreloadStateManagerImpl"; // const-string v3, "PreloadStateManagerImpl"
android.util.Slog .w ( v3,v0 );
/* .line 395 */
} // .end local v1 # "stopTimeout":J
} // :cond_1
return;
} // .end method
public Boolean stackOnParentChanged ( com.android.server.wm.DisplayContent p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "display" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "forceStop" # Z */
/* .line 367 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( com.android.server.wm.DisplayContent ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
/* if-nez v0, :cond_0 */
/* .line 370 */
} // :cond_0
/* .line 368 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public android.app.ActivityOptions startActivityInjector ( com.android.server.wm.ActivityStarter$Request p0, com.android.server.wm.ActivityRecord p1, com.android.server.wm.ActivityRecord p2, android.app.ActivityOptions p3, com.android.server.wm.Task p4 ) {
/* .locals 4 */
/* .param p1, "request" # Lcom/android/server/wm/ActivityStarter$Request; */
/* .param p2, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "options" # Landroid/app/ActivityOptions; */
/* .param p5, "inTask" # Lcom/android/server/wm/Task; */
/* .line 238 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.wm.PreloadStateManagerImpl.sPreloadAppStarted = (v0!= 0);
/* .line 239 */
/* iget v1, p1, Lcom/android/server/wm/ActivityStarter$Request;->realCallingUid:I */
int v2 = -1; // const/4 v2, -0x1
/* if-eq v1, v2, :cond_0 */
/* .line 240 */
/* iget v1, p1, Lcom/android/server/wm/ActivityStarter$Request;->realCallingUid:I */
/* .line 241 */
} // :cond_0
v1 = android.os.Binder .getCallingUid ( );
} // :goto_0
/* nop */
/* .line 243 */
/* .local v1, "realCallingUid":I */
v2 = (( com.android.server.wm.ActivityRecord ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v2 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
if ( v2 != null) { // if-eqz v2, :cond_1
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 244 */
v2 = (( com.android.server.wm.ActivityRecord ) p3 ).getUid ( ); // invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v2 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
/* if-nez v2, :cond_2 */
/* .line 245 */
} // :cond_1
v2 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 246 */
} // :cond_2
/* sget-boolean v2, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 247 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "set preloadApp launch PreloadDisplay r=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " uid="; // const-string v3, " uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 248 */
v3 = (( com.android.server.wm.ActivityRecord ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " realCallingUid="; // const-string v3, " realCallingUid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 247 */
final String v3 = "PreloadStateManagerImpl"; // const-string v3, "PreloadStateManagerImpl"
android.util.Slog .w ( v3,v2 );
/* .line 251 */
} // :cond_3
/* if-nez p4, :cond_4 */
/* .line 252 */
android.app.ActivityOptions .makeBasic ( );
/* .line 254 */
} // :cond_4
v2 = this.info;
v2 = this.packageName;
v2 = com.android.server.wm.PreloadStateManagerImpl .getOrCreatePreloadDisplayId ( v2 );
(( android.app.ActivityOptions ) p4 ).setLaunchDisplayId ( v2 ); // invoke-virtual {p4, v2}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;
/* .line 257 */
} // :cond_5
v2 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).needReparent ( p2, p3, p1, p5 ); // invoke-virtual {p0, p2, p3, p1, p5}, Lcom/android/server/wm/PreloadStateManagerImpl;->needReparent(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityStarter$Request;Lcom/android/server/wm/Task;)Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 259 */
/* if-nez p4, :cond_6 */
/* .line 260 */
android.app.ActivityOptions .makeBasic ( );
/* .line 262 */
} // :cond_6
int v2 = 1; // const/4 v2, 0x1
com.android.server.wm.PreloadStateManagerImpl.sPreloadAppStarted = (v2!= 0);
/* .line 263 */
(( com.android.server.wm.PreloadStateManagerImpl ) p0 ).switchAppNormalState ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/PreloadStateManagerImpl;->switchAppNormalState(Lcom/android/server/wm/ActivityRecord;)V
/* .line 264 */
(( android.app.ActivityOptions ) p4 ).setLaunchDisplayId ( v0 ); // invoke-virtual {p4, v0}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;
/* .line 265 */
} // :cond_7
v0 = (( com.android.server.wm.ActivityRecord ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I
/* const/16 v2, 0x3e8 */
/* if-le v0, v2, :cond_8 */
if ( p4 != null) { // if-eqz p4, :cond_8
/* .line 266 */
v0 = (( android.app.ActivityOptions ) p4 ).getLaunchDisplayId ( ); // invoke-virtual {p4}, Landroid/app/ActivityOptions;->getLaunchDisplayId()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).isPreloadDisplayId ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->isPreloadDisplayId(I)Z
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 267 */
v0 = (( com.android.server.wm.ActivityRecord ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v0 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
/* if-nez v0, :cond_8 */
/* .line 269 */
(( com.android.server.wm.PreloadStateManagerImpl ) p0 ).switchAppPreloadState ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/PreloadStateManagerImpl;->switchAppPreloadState(Lcom/android/server/wm/ActivityRecord;)V
/* .line 272 */
} // :cond_8
} // :goto_1
} // .end method
public Boolean stopPreloadActivity ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1, com.android.server.wm.Task p2 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "prev" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "task" # Lcom/android/server/wm/Task; */
/* .line 343 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_1 */
/* .line 344 */
(( com.android.server.wm.Task ) p3 ).getTopNonFinishingActivity ( ); // invoke-virtual {p3}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 345 */
/* .local v1, "top":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v1, :cond_0 */
/* .line 346 */
/* .line 348 */
} // :cond_0
/* move-object p1, v1 */
/* .line 350 */
} // .end local v1 # "top":Lcom/android/server/wm/ActivityRecord;
} // :cond_1
(( com.android.server.wm.ActivityRecord ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getName()Ljava/lang/String;
final String v2 = "permission.ui.GrantPermissionsActivity"; // const-string v2, "permission.ui.GrantPermissionsActivity"
v1 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 351 */
/* move-object p1, p2 */
/* .line 353 */
} // :cond_2
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v1 = (( com.android.server.wm.PreloadStateManagerImpl ) p0 ).alreadyPreload ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->alreadyPreload(I)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 354 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 355 */
final String v0 = "PreloadStateManagerImpl"; // const-string v0, "PreloadStateManagerImpl"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "preloadApp mReadyPauseActivity="; // const-string v2, "preloadApp mReadyPauseActivity="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 357 */
} // :cond_3
v0 = com.android.server.wm.PreloadStateManagerImpl.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getGlobalLock ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getGlobalLock()Lcom/android/server/wm/WindowManagerGlobalLock;
/* monitor-enter v1 */
/* .line 358 */
try { // :try_start_0
(( com.android.server.wm.ActivityRecord ) p1 ).makeInvisible ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->makeInvisible()V
/* .line 359 */
/* monitor-exit v1 */
/* .line 360 */
int v0 = 1; // const/4 v0, 0x1
/* .line 359 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
/* .line 362 */
} // :cond_4
} // .end method
public void switchAppNormalState ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 7 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 110 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 111 */
final String v0 = "PreloadStateManagerImpl"; // const-string v0, "PreloadStateManagerImpl"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "move preloadApp to defaultDisplay uid:"; // const-string v2, "move preloadApp to defaultDisplay uid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "callingUid="; // const-string v2, "callingUid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 112 */
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "pid="; // const-string v2, "pid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 111 */
android.util.Slog .w ( v0,v1 );
/* .line 115 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/wm/PreloadStateManagerImpl;->getRunningProcessInfos()Ljava/util/List; */
/* .line 116 */
/* .local v0, "runningProcessInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 117 */
/* .local v1, "procIsRunninng":Z */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lmiui/process/RunningProcessInfo; */
/* .line 118 */
/* .local v3, "info":Lmiui/process/RunningProcessInfo; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget v4, v3, Lmiui/process/RunningProcessInfo;->mUid:I */
v5 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
/* if-ne v4, v5, :cond_1 */
/* .line 119 */
int v1 = 1; // const/4 v1, 0x1
/* .line 120 */
/* iget v4, v3, Lmiui/process/RunningProcessInfo;->mPid:I */
v5 = com.android.server.am.PreloadAppControllerImpl.sDefaultSchedAffinity;
/* invoke-direct {p0, v4, v5}, Lcom/android/server/wm/PreloadStateManagerImpl;->setSchedAffinity(I[I)Z */
/* .line 122 */
} // .end local v3 # "info":Lmiui/process/RunningProcessInfo;
} // :cond_1
/* .line 124 */
} // :cond_2
com.android.server.am.PreloadAppControllerImpl .getInstance ( );
/* .line 125 */
/* .local v2, "preloadController":Lcom/android/server/am/PreloadAppControllerImpl; */
if ( v1 != null) { // if-eqz v1, :cond_5
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 126 */
v3 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
/* .line 127 */
v4 = this.info;
if ( v4 != null) { // if-eqz v4, :cond_3
v4 = this.info;
v4 = this.packageName;
} // :cond_3
final String v4 = ""; // const-string v4, ""
} // :goto_1
v5 = this.app;
if ( v5 != null) { // if-eqz v5, :cond_4
v5 = this.app;
v5 = (( com.android.server.wm.WindowProcessController ) v5 ).getPid ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowProcessController;->getPid()I
} // :cond_4
int v5 = -1; // const/4 v5, -0x1
/* .line 126 */
} // :goto_2
(( com.android.server.am.PreloadAppControllerImpl ) v2 ).onPreloadAppStarted ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/am/PreloadAppControllerImpl;->onPreloadAppStarted(ILjava/lang/String;I)V
/* .line 130 */
} // :cond_5
v3 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
v4 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
/* .line 131 */
v5 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
java.lang.Integer .valueOf ( v5 );
/* .line 130 */
int v5 = 2; // const/4 v5, 0x2
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v3 ).removeMessages ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->removeMessages(ILjava/lang/Object;)V
/* .line 132 */
v3 = com.android.server.wm.PreloadStateManagerImpl.sHandler;
v4 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
/* .line 133 */
v5 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
java.lang.Integer .valueOf ( v5 );
/* .line 132 */
int v5 = 1; // const/4 v5, 0x1
(( com.android.server.wm.PreloadStateManagerImpl$PreloadStateHandler ) v3 ).removeMessages ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/wm/PreloadStateManagerImpl$PreloadStateHandler;->removeMessages(ILjava/lang/Object;)V
/* .line 134 */
v3 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
v4 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
java.lang.Integer .valueOf ( v4 );
/* .line 135 */
v3 = com.miui.server.greeze.GreezeManagerDebugConfig .isEnable ( );
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 136 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
v4 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
/* filled-new-array {v4}, [I */
/* const-string/jumbo v6, "thaw preloadApp" */
(( com.miui.server.greeze.GreezeManagerInternal ) v3 ).thawUids ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Lcom/miui/server/greeze/GreezeManagerInternal;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 140 */
} // :cond_6
v3 = com.android.server.wm.PreloadStateManagerImpl.sEnableAudioLock;
/* monitor-enter v3 */
/* .line 141 */
try { // :try_start_0
v4 = com.android.server.wm.PreloadStateManagerImpl.sWaitEnableAudioUids;
v5 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
java.lang.Integer .valueOf ( v5 );
/* .line 142 */
/* monitor-exit v3 */
/* .line 143 */
return;
/* .line 142 */
/* :catchall_0 */
/* move-exception v4 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v4 */
} // .end method
public void switchAppPreloadState ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 101 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 102 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "First start preloadApp to VirtualDisplay activity uid:"; // const-string v1, "First start preloadApp to VirtualDisplay activity uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " callingUid="; // const-string v1, " callingUid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 103 */
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "pid="; // const-string v1, "pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 102 */
final String v1 = "PreloadStateManagerImpl"; // const-string v1, "PreloadStateManagerImpl"
android.util.Slog .w ( v1,v0 );
/* .line 105 */
} // :cond_0
v0 = com.android.server.wm.PreloadStateManagerImpl.sPreloadUidInfos;
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
java.lang.Integer .valueOf ( v1 );
/* new-instance v2, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo; */
/* invoke-direct {v2}, Lcom/android/server/wm/PreloadStateManagerImpl$UidInfo;-><init>()V */
/* .line 106 */
int v0 = 1; // const/4 v0, 0x1
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
com.android.server.wm.PreloadStateManagerImpl .disableAudio ( v0,v1 );
/* .line 107 */
return;
} // .end method
