.class public Lcom/android/server/wm/MiuiSplitInputMethodImpl;
.super Ljava/lang/Object;
.source "MiuiSplitInputMethodImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiSplitInputMethodStub;


# static fields
.field static DEBUG:Z = false

.field public static final TAG:Ljava/lang/String; = "MiuiSplitInputMethodImpl"

.field private static sIsSplitIMESupport:Z

.field private static sSplitMinVersionSupport:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccuracy:F

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mImms:Lcom/android/server/inputmethod/InputMethodManagerService;

.field private mSplitImeSwitch:Z

.field private final mTmpBounds:Landroid/graphics/Rect;

.field private final mTmpConfiguration:Landroid/content/res/Configuration;

.field private sVersionCode:I


# direct methods
.method public static synthetic $r8$lambda$OlYhkjpgLpK-_ttsHB0Tw3y_e0E(Lcom/android/server/wm/MiuiSplitInputMethodImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->lambda$init$0()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 40
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->DEBUG:Z

    .line 52
    sput-boolean v0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sIsSplitIMESupport:Z

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sSplitMinVersionSupport:Ljava/util/HashMap;

    .line 62
    const/16 v1, 0x1f7d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "com.iflytek.inputmethod.miui"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sSplitMinVersionSupport:Ljava/util/HashMap;

    const/16 v1, 0x6f6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "com.sohu.inputmethod.sogou.xiaomi"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sSplitMinVersionSupport:Ljava/util/HashMap;

    const/16 v1, 0x587

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "com.baidu.input_mi"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpConfiguration:Landroid/content/res/Configuration;

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sVersionCode:I

    .line 56
    const-string v0, "persist.spltiIme"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z

    .line 58
    const v0, 0x3c23d70a    # 0.01f

    iput v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mAccuracy:F

    return-void
.end method

.method private clearImebounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayArea$Tokens;)V
    .locals 2
    .param p1, "configuration"    # Landroid/content/res/Configuration;
    .param p2, "imeWindowsContainer"    # Lcom/android/server/wm/DisplayArea$Tokens;

    .line 166
    iget v0, p1, Landroid/content/res/Configuration;->seq:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/content/res/Configuration;->seq:I

    .line 167
    iget-object v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 168
    iget-object v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "updateImeAppBounds"

    invoke-static {p1, v1, v0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    if-eqz p2, :cond_0

    .line 170
    invoke-virtual {p2, p1}, Lcom/android/server/wm/DisplayArea$Tokens;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 172
    :cond_0
    return-void
.end method

.method private getImeVersionCode()I
    .locals 4

    .line 97
    invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->getInputMethodPackageName()Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, "curMethodPackageName":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 100
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 101
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v2, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sVersionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    goto :goto_0

    .line 103
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, -0x1

    iput v2, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sVersionCode:I

    .line 105
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "check "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Version failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiSplitInputMethodImpl"

    invoke-static {v3, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 107
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    iget v1, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sVersionCode:I

    return v1
.end method

.method private getInputMethodPackageName()Ljava/lang/String;
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mImms:Lcom/android/server/inputmethod/InputMethodManagerService;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 86
    return-object v1

    .line 88
    :cond_0
    const-string v2, "getCurIdLocked"

    invoke-static {v0, v2, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 90
    .local v0, "curMethod":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 91
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    return-object v1

    .line 93
    :cond_1
    return-object v1
.end method

.method public static getInstance()Lcom/android/server/wm/MiuiSplitInputMethodImpl;
    .locals 1

    .line 76
    invoke-static {}, Lcom/android/server/wm/MiuiSplitInputMethodStub;->getInstance()Lcom/android/server/wm/MiuiSplitInputMethodStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;

    return-object v0
.end method

.method public static varargs invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "values"    # [Ljava/lang/Object;

    .line 129
    const/4 v0, 0x0

    .line 131
    .local v0, "method":Ljava/lang/reflect/Method;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 132
    .local v2, "clazz":Ljava/lang/Class;
    const/4 v3, 0x1

    if-nez p2, :cond_0

    .line 133
    invoke-virtual {v2, p1, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    move-object v0, v4

    .line 134
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 135
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1

    .line 137
    :cond_0
    array-length v4, p2

    new-array v4, v4, [Ljava/lang/Class;

    .line 138
    .local v4, "argsClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v6, p2

    if-ge v5, v6, :cond_4

    .line 139
    aget-object v6, p2, v5

    instance-of v6, v6, Ljava/lang/Integer;

    if-eqz v6, :cond_1

    .line 140
    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    goto :goto_1

    .line 141
    :cond_1
    aget-object v6, p2, v5

    instance-of v6, v6, Ljava/lang/Boolean;

    if-eqz v6, :cond_2

    .line 142
    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    goto :goto_1

    .line 143
    :cond_2
    aget-object v6, p2, v5

    instance-of v6, v6, Ljava/lang/Float;

    if-eqz v6, :cond_3

    .line 144
    sget-object v6, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    goto :goto_1

    .line 146
    :cond_3
    aget-object v6, p2, v5

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    .line 138
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 149
    .end local v5    # "i":I
    :cond_4
    invoke-virtual {v2, p1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    move-object v0, v5

    .line 150
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 151
    invoke-virtual {v0, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 152
    .end local v2    # "clazz":Ljava/lang/Class;
    .end local v4    # "argsClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :catch_0
    move-exception v2

    .line 153
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDeclaredMethod:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiSplitInputMethodImpl"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    .end local v2    # "e":Ljava/lang/Exception;
    return-object v1
.end method

.method private isFoldDevice()Z
    .locals 2

    .line 81
    const-string v0, "babylon"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isInLargeScreen(Landroid/content/res/Configuration;)Z
    .locals 3
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .line 111
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 112
    :cond_0
    iget v1, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v2, 0x258

    if-lt v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private synthetic lambda$init$0()V
    .locals 2

    .line 71
    const-string v0, "persist.spltiIme"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z

    return-void
.end method


# virtual methods
.method public adjustCutoutForSplitIme(Landroid/view/InsetsState;Lcom/android/server/wm/WindowState;)V
    .locals 7
    .param p1, "state"    # Landroid/view/InsetsState;
    .param p2, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 208
    if-nez p2, :cond_0

    .line 209
    return-void

    .line 211
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 212
    return-void

    .line 214
    :cond_1
    iget-object v0, p2, Lcom/android/server/wm/WindowState;->mWmService:Lcom/android/server/wm/WindowManagerService;

    .line 215
    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;

    move-result-object v0

    .line 217
    .local v0, "imeTarget":Lcom/android/server/wm/InsetsControlTarget;
    if-eqz v0, :cond_9

    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v2

    .line 218
    invoke-interface {v0}, Lcom/android/server/wm/InsetsControlTarget;->getWindow()Lcom/android/server/wm/WindowState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_3

    .line 221
    :cond_2
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v2

    const/16 v3, 0x7dc

    const/4 v4, 0x1

    const/16 v5, 0x7db

    if-eq v2, v5, :cond_4

    .line 222
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v2

    if-ne v2, v3, :cond_3

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    :goto_0
    move v2, v4

    .line 223
    .local v2, "isImeWindow":Z
    :goto_1
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 224
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    instance-of v6, v6, Lcom/android/server/wm/WindowState;

    if-eqz v6, :cond_6

    .line 225
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->getWindowType()I

    move-result v6

    if-eq v6, v5, :cond_5

    .line 226
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->getWindowType()I

    move-result v5

    if-ne v5, v3, :cond_6

    :cond_5
    move v1, v4

    goto :goto_2

    :cond_6
    nop

    .line 227
    .local v1, "isImeChildWindow":Z
    :goto_2
    if-nez v2, :cond_7

    if-nez v1, :cond_7

    return-void

    .line 228
    :cond_7
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_8

    .line 229
    return-void

    .line 232
    :cond_8
    sget-object v3, Landroid/view/DisplayCutout;->NO_CUTOUT:Landroid/view/DisplayCutout;

    invoke-virtual {p1, v3}, Landroid/view/InsetsState;->setDisplayCutout(Landroid/view/DisplayCutout;)V

    .line 233
    invoke-static {}, Landroid/view/WindowInsets$Type;->displayCutout()I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/view/InsetsState;->removeSource(I)V

    .line 234
    const-string v3, "MiuiSplitInputMethodImpl"

    const-string v4, "remove cutout for ime"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    return-void

    .line 219
    .end local v1    # "isImeChildWindow":Z
    .end local v2    # "isImeWindow":Z
    :cond_9
    :goto_3
    return-void
.end method

.method public adjustWindowFrameForSplitIme(Lcom/android/server/wm/WindowState;Landroid/window/ClientWindowFrames;)V
    .locals 0
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;
    .param p2, "clientWindowFrames"    # Landroid/window/ClientWindowFrames;

    .line 162
    return-void
.end method

.method public init(Lcom/android/server/inputmethod/InputMethodManagerService;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 1
    .param p1, "imms"    # Lcom/android/server/inputmethod/InputMethodManagerService;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;

    .line 68
    iput-object p1, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mImms:Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 69
    iput-object p2, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mHandler:Landroid/os/Handler;

    .line 70
    iput-object p3, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mContext:Landroid/content/Context;

    .line 71
    new-instance v0, Lcom/android/server/wm/MiuiSplitInputMethodImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiSplitInputMethodImpl;)V

    invoke-static {v0}, Landroid/os/SystemProperties;->addChangeCallback(Ljava/lang/Runnable;)V

    .line 73
    return-void
.end method

.method public isSplitIMESupport(Landroid/content/res/Configuration;)Z
    .locals 6
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .line 116
    invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->getInputMethodPackageName()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "curMethodPackageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    if-nez v0, :cond_0

    goto :goto_0

    .line 118
    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sIsSplitIMESupport:Z

    .line 119
    sget-object v3, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sSplitMinVersionSupport:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 120
    .local v3, "minSupportVersionCode":Ljava/lang/Integer;
    invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->getImeVersionCode()I

    move-result v4

    .line 121
    .local v4, "imeCurVersionCode":I
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-lt v4, v5, :cond_1

    .line 122
    sput-boolean v1, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sIsSplitIMESupport:Z

    .line 125
    :cond_1
    iget-boolean v5, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z

    if-eqz v5, :cond_2

    sget-boolean v5, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sIsSplitIMESupport:Z

    if-eqz v5, :cond_2

    invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isFoldDevice()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isInLargeScreen(Landroid/content/res/Configuration;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v1

    :cond_2
    return v2

    .line 117
    .end local v3    # "minSupportVersionCode":Ljava/lang/Integer;
    .end local v4    # "imeCurVersionCode":I
    :cond_3
    :goto_0
    return v2
.end method

.method public onConfigurationChangedForSplitIme(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayArea$Tokens;)V
    .locals 6
    .param p1, "newParentConfig"    # Landroid/content/res/Configuration;
    .param p2, "imeContainer"    # Lcom/android/server/wm/DisplayArea$Tokens;

    .line 264
    if-eqz p1, :cond_7

    if-nez p2, :cond_0

    goto/16 :goto_1

    .line 265
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z

    if-nez v0, :cond_1

    return-void

    .line 266
    :cond_1
    iget-object v0, p2, Lcom/android/server/wm/DisplayArea$Tokens;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 267
    .local v0, "dc":Lcom/android/server/wm/DisplayContent;
    if-nez v0, :cond_2

    return-void

    .line 268
    :cond_2
    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "getmImeAppBounds"

    invoke-static {p1, v3, v2}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 269
    .local v2, "parentImeBounds":Landroid/graphics/Rect;
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v4, v3, v5}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    .line 270
    .local v3, "imeBounds":Landroid/graphics/Rect;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    .line 271
    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    nop

    .line 272
    .local v1, "needToClearImeBounds":Z
    :goto_0
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z

    move-result v4

    if-nez v4, :cond_5

    if-nez v1, :cond_5

    .line 273
    sget-boolean v4, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->DEBUG:Z

    if-eqz v4, :cond_4

    .line 274
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ime not support split mode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiSplitInputMethodImpl"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :cond_4
    return-void

    .line 285
    :cond_5
    iget-object v4, p2, Lcom/android/server/wm/DisplayArea$Tokens;->mWmService:Lcom/android/server/wm/WindowManagerService;

    if-eqz v4, :cond_6

    iget-object v4, p2, Lcom/android/server/wm/DisplayArea$Tokens;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    if-eqz v4, :cond_6

    .line 286
    iget-object v4, p2, Lcom/android/server/wm/DisplayArea$Tokens;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v4, v4, Lcom/android/server/wm/WindowManagerService;->mWindowContextListenerController:Lcom/android/server/wm/WindowContextListenerController;

    iget-object v5, p2, Lcom/android/server/wm/DisplayArea$Tokens;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    iget v5, v5, Lcom/android/server/wm/DisplayContent;->mDisplayId:I

    .line 287
    invoke-virtual {v4, v5}, Lcom/android/server/wm/WindowContextListenerController;->dispatchPendingConfigurationIfNeeded(I)V

    .line 289
    :cond_6
    return-void

    .line 264
    .end local v0    # "dc":Lcom/android/server/wm/DisplayContent;
    .end local v1    # "needToClearImeBounds":Z
    .end local v2    # "parentImeBounds":Landroid/graphics/Rect;
    .end local v3    # "imeBounds":Landroid/graphics/Rect;
    :cond_7
    :goto_1
    return-void
.end method

.method public onConfigurationChangedForTask(Landroid/content/res/Configuration;Lcom/android/server/wm/WindowContainer;)V
    .locals 12
    .param p1, "newParentConfig"    # Landroid/content/res/Configuration;
    .param p2, "wc"    # Lcom/android/server/wm/WindowContainer;

    .line 322
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z

    if-nez v0, :cond_0

    return-void

    .line 323
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 324
    :cond_1
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    .line 325
    .local v0, "dc":Lcom/android/server/wm/DisplayContent;
    if-nez v0, :cond_2

    return-void

    .line 327
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getImeContainer()Lcom/android/server/wm/DisplayArea$Tokens;

    move-result-object v1

    .line 328
    .local v1, "imeContainer":Lcom/android/server/wm/DisplayArea$Tokens;
    if-nez v1, :cond_3

    return-void

    .line 331
    :cond_3
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 332
    .local v2, "preConfiguration":Landroid/content/res/Configuration;
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z

    move-result v3

    const-string v4, "MiuiSplitInputMethodImpl"

    const-string v5, "isImeMultiWindowMode"

    const/4 v6, 0x0

    if-nez v3, :cond_5

    .line 333
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v3, v5, v7}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 334
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v5, v6}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 335
    invoke-direct {p0, v2, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->clearImebounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayArea$Tokens;)V

    .line 337
    :cond_4
    const-string v3, "IME or device not support split ime"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    return-void

    .line 341
    :cond_5
    invoke-virtual {v0, v6}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;

    move-result-object v3

    if-eqz v3, :cond_e

    invoke-virtual {v0, v6}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;

    move-result-object v3

    .line 342
    invoke-interface {v3}, Lcom/android/server/wm/InsetsControlTarget;->getWindow()Lcom/android/server/wm/WindowState;

    move-result-object v3

    if-nez v3, :cond_6

    goto/16 :goto_1

    .line 343
    :cond_6
    invoke-virtual {v0, v6}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/server/wm/InsetsControlTarget;->getWindow()Lcom/android/server/wm/WindowState;

    move-result-object v3

    .line 345
    .local v3, "imeTarget":Lcom/android/server/wm/WindowState;
    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v7

    if-eqz v7, :cond_7

    invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v7

    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v8

    .line 346
    invoke-virtual {v8}, Lcom/android/server/wm/Task;->getTopVisibleActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v8

    if-ne v7, v8, :cond_8

    :cond_7
    invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;

    move-result-object v7

    iget-object v7, v7, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    if-nez v7, :cond_9

    :cond_8
    return-void

    .line 349
    :cond_9
    iget-object v7, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v7}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    .line 350
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getBounds()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    .line 351
    .local v7, "scale":F
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    new-array v9, v6, [Ljava/lang/Object;

    invoke-static {v8, v5, v9}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 354
    .local v5, "isPrevSplitIme":Z
    float-to-double v8, v7

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    iget v10, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mAccuracy:F

    float-to-double v10, v10

    cmpg-double v8, v8, v10

    const/4 v9, 0x1

    if-gez v8, :cond_a

    move v8, v9

    goto :goto_0

    :cond_a
    move v8, v6

    .line 355
    .local v8, "isImeTargetSplitMode":Z
    :goto_0
    if-nez v8, :cond_b

    if-eqz v5, :cond_b

    .line 356
    invoke-direct {p0, v2, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->clearImebounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayArea$Tokens;)V

    .line 357
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "invalid scale:"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ",clear ime bounds"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    return-void

    .line 360
    :cond_b
    if-eqz v8, :cond_d

    .line 361
    iget-object v4, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    .line 362
    iget-object v4, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    iget-object v10, p1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v10}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 363
    iget-object v4, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpConfiguration:Landroid/content/res/Configuration;

    invoke-virtual {v4}, Landroid/content/res/Configuration;->unset()V

    .line 364
    iget-object v4, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpConfiguration:Landroid/content/res/Configuration;

    invoke-virtual {v4, v2}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    .line 365
    iget-object v4, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpConfiguration:Landroid/content/res/Configuration;

    iget v10, v4, Landroid/content/res/Configuration;->seq:I

    add-int/2addr v10, v9

    iput v10, v4, Landroid/content/res/Configuration;->seq:I

    .line 366
    iget-object v4, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    const-string v9, "getmImeAppBounds"

    new-array v10, v6, [Ljava/lang/Object;

    invoke-static {v2, v9, v10}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 367
    if-nez v5, :cond_c

    invoke-virtual {p0, v6}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->updateImeVisiblityIfNeed(Z)V

    .line 368
    :cond_c
    iget-object v4, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpConfiguration:Landroid/content/res/Configuration;

    iget-object v6, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    filled-new-array {v6}, [Ljava/lang/Object;

    move-result-object v6

    const-string/jumbo v9, "updateImeAppBounds"

    invoke-static {v4, v9, v6}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    iget-object v4, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpConfiguration:Landroid/content/res/Configuration;

    invoke-virtual {v1, v4}, Lcom/android/server/wm/DisplayArea$Tokens;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 372
    :cond_d
    return-void

    .line 342
    .end local v3    # "imeTarget":Lcom/android/server/wm/WindowState;
    .end local v5    # "isPrevSplitIme":Z
    .end local v7    # "scale":F
    .end local v8    # "isImeTargetSplitMode":Z
    :cond_e
    :goto_1
    return-void
.end method

.method public resolveOverrideConfigurationForSplitIme(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayArea$Tokens;)V
    .locals 5
    .param p1, "newParentConfig"    # Landroid/content/res/Configuration;
    .param p2, "imeContainer"    # Lcom/android/server/wm/DisplayArea$Tokens;

    .line 293
    if-eqz p1, :cond_5

    if-nez p2, :cond_0

    goto :goto_1

    .line 294
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z

    if-nez v0, :cond_1

    return-void

    .line 295
    :cond_1
    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getmImeAppBounds"

    invoke-static {p1, v2, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 296
    .local v1, "parentImeBounds":Landroid/graphics/Rect;
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v2, v4}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 297
    .local v2, "imeBounds":Landroid/graphics/Rect;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 298
    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    nop

    .line 300
    .local v0, "needToClearImeBounds":Z
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v0, :cond_4

    .line 302
    :cond_3
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayArea$Tokens;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    .line 304
    :cond_4
    return-void

    .line 293
    .end local v0    # "needToClearImeBounds":Z
    .end local v1    # "parentImeBounds":Landroid/graphics/Rect;
    .end local v2    # "imeBounds":Landroid/graphics/Rect;
    :cond_5
    :goto_1
    return-void
.end method

.method public shouldAdjustCutoutForSplitIme(Lcom/android/server/wm/WindowState;)Z
    .locals 7
    .param p1, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 238
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 239
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 240
    return v0

    .line 242
    :cond_1
    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mWmService:Lcom/android/server/wm/WindowManagerService;

    .line 243
    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;

    move-result-object v1

    .line 245
    .local v1, "imeTarget":Lcom/android/server/wm/InsetsControlTarget;
    if-eqz v1, :cond_9

    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v2

    .line 246
    invoke-interface {v1}, Lcom/android/server/wm/InsetsControlTarget;->getWindow()Lcom/android/server/wm/WindowState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_3

    .line 249
    :cond_2
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v2

    const/16 v3, 0x7dc

    const/16 v4, 0x7db

    const/4 v5, 0x1

    if-eq v2, v4, :cond_4

    .line 250
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I

    move-result v2

    if-ne v2, v3, :cond_3

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    :goto_0
    move v2, v5

    .line 251
    .local v2, "isImeWindow":Z
    :goto_1
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 252
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    instance-of v6, v6, Lcom/android/server/wm/WindowState;

    if-eqz v6, :cond_6

    .line 253
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->getWindowType()I

    move-result v6

    if-eq v6, v4, :cond_5

    .line 254
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->getWindowType()I

    move-result v4

    if-ne v4, v3, :cond_6

    :cond_5
    move v3, v5

    goto :goto_2

    :cond_6
    move v3, v0

    .line 255
    .local v3, "isImeChildWindow":Z
    :goto_2
    if-nez v2, :cond_7

    if-nez v3, :cond_7

    return v0

    .line 256
    :cond_7
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-eq v4, v6, :cond_8

    .line 257
    return v0

    .line 259
    :cond_8
    return v5

    .line 247
    .end local v2    # "isImeWindow":Z
    .end local v3    # "isImeChildWindow":Z
    :cond_9
    :goto_3
    return v0
.end method

.method public updateImeBoundsInImeTargetChanged(Lcom/android/server/wm/DisplayContent;Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;)V
    .locals 7
    .param p1, "dc"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "curFocusedWindow"    # Lcom/android/server/wm/WindowState;
    .param p3, "newFocusedWindow"    # Lcom/android/server/wm/WindowState;

    .line 176
    if-ne p2, p3, :cond_0

    return-void

    .line 178
    :cond_0
    if-nez p1, :cond_1

    .line 179
    return-void

    .line 181
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getImeContainer()Lcom/android/server/wm/DisplayArea$Tokens;

    move-result-object v0

    .line 182
    .local v0, "imeWindowsContainer":Lcom/android/server/wm/DisplayArea$Tokens;
    new-instance v1, Landroid/content/res/Configuration;

    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 183
    .local v1, "configuration":Landroid/content/res/Configuration;
    invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 184
    return-void

    .line 186
    :cond_2
    iget-object v2, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    .line 188
    if-eqz p3, :cond_5

    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v2

    .line 189
    invoke-virtual {v2, p3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    .line 193
    :cond_3
    iget-object v2, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    invoke-virtual {p3}, Lcom/android/server/wm/WindowState;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 194
    iget-object v2, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    .line 195
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 197
    .local v2, "scale":F
    iget-object v3, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "getmImeAppBounds"

    invoke-static {v1, v5, v4}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    float-to-double v3, v2

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v3, v5

    .line 198
    invoke-static {v3, v4}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    iget v5, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mAccuracy:F

    float-to-double v5, v5

    cmpg-double v3, v3, v5

    if-gez v3, :cond_4

    .line 199
    iget-object v3, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mTmpBounds:Landroid/graphics/Rect;

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const-string/jumbo v4, "updateImeAppBounds"

    invoke-static {v1, v4, v3}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    iget v3, v1, Landroid/content/res/Configuration;->seq:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Landroid/content/res/Configuration;->seq:I

    .line 201
    if-eqz v0, :cond_4

    .line 202
    invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayArea$Tokens;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 205
    :cond_4
    return-void

    .line 190
    .end local v2    # "scale":F
    :cond_5
    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->clearImebounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayArea$Tokens;)V

    .line 191
    return-void
.end method

.method public updateImeVisiblityIfNeed(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .line 307
    const-class v0, Lcom/android/server/inputmethod/InputMethodManagerInternal;

    .line 308
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/inputmethod/InputMethodManagerInternal;

    .line 309
    .local v0, "service":Lcom/android/server/inputmethod/InputMethodManagerInternal;
    if-nez v0, :cond_0

    .line 310
    return-void

    .line 312
    :cond_0
    if-nez p1, :cond_1

    .line 313
    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerInternal;->hideCurrentInputMethod(I)V

    .line 317
    :cond_1
    return-void
.end method
