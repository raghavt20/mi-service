class com.android.server.wm.MiuiFreeformTrackManager$1 implements android.content.ServiceConnection {
	 /* .source "MiuiFreeformTrackManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiFreeformTrackManager;->bindOneTrackService()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiFreeformTrackManager this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiFreeformTrackManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiFreeformTrackManager; */
/* .line 199 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 202 */
v0 = this.this$0;
v0 = this.mFreeFormTrackLock;
/* monitor-enter v0 */
/* .line 203 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.miui.analytics.ITrackBinder$Stub .asInterface ( p2 );
	 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fputmITrackBinder ( v1,v2 );
	 /* .line 204 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 205 */
	 final String v0 = "MiuiFreeformOneTrackManager"; // const-string v0, "MiuiFreeformOneTrackManager"
	 final String v1 = "BindOneTrackService Success"; // const-string v1, "BindOneTrackService Success"
	 android.util.Slog .d ( v0,v1 );
	 /* .line 206 */
	 return;
	 /* .line 204 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 try { // :try_start_1
		 /* monitor-exit v0 */
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* throw v1 */
	 } // .end method
	 public void onServiceDisconnected ( android.content.ComponentName p0 ) {
		 /* .locals 4 */
		 /* .param p1, "componentName" # Landroid/content/ComponentName; */
		 /* .line 210 */
		 v0 = this.this$0;
		 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmContext ( v0 );
		 (( com.android.server.wm.MiuiFreeformTrackManager ) v0 ).closeOneTrackService ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Lcom/android/server/wm/MiuiFreeformTrackManager;->closeOneTrackService(Landroid/content/Context;Landroid/content/ServiceConnection;)V
		 /* .line 211 */
		 v0 = this.this$0;
		 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmHandler ( v0 );
		 v1 = this.this$0;
		 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmBindOneTrackService ( v1 );
		 (( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
		 /* .line 212 */
		 v0 = this.this$0;
		 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmHandler ( v0 );
		 v1 = this.this$0;
		 com.android.server.wm.MiuiFreeformTrackManager .-$$Nest$fgetmBindOneTrackService ( v1 );
		 /* const-wide/16 v2, 0xc8 */
		 (( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
		 /* .line 213 */
		 final String v0 = "MiuiFreeformOneTrackManager"; // const-string v0, "MiuiFreeformOneTrackManager"
		 final String v1 = "OneTrack Service Disconnected"; // const-string v1, "OneTrack Service Disconnected"
		 android.util.Slog .d ( v0,v1 );
		 /* .line 214 */
		 return;
	 } // .end method
