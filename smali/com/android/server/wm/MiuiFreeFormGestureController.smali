.class public Lcom/android/server/wm/MiuiFreeFormGestureController;
.super Ljava/lang/Object;
.source "MiuiFreeFormGestureController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;
    }
.end annotation


# static fields
.field public static DEBUG:Z = false

.field public static final GB_BOOSTING:Ljava/lang/String; = "gb_boosting"

.field private static final MIUI_OPTIMIZATION:Ljava/lang/String; = "miui_optimization"

.field private static final TAG:Ljava/lang/String; = "MiuiFreeFormGestureController"

.field public static final VTB_BOOSTING:Ljava/lang/String; = "vtb_boosting"


# instance fields
.field mAudioManager:Landroid/media/AudioManager;

.field mDisplayContent:Lcom/android/server/wm/DisplayContent;

.field private mFreeFormReceiver:Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;

.field private mGameModeObserver:Landroid/database/ContentObserver;

.field mHandler:Landroid/os/Handler;

.field mIsGameMode:Z

.field mIsPortrait:Z

.field mIsVideoMode:Z

.field mLastOrientation:I

.field mMiuiFreeFormKeyCombinationHelper:Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;

.field mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

.field mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

.field private mMiuiOptObserver:Landroid/database/ContentObserver;

.field mService:Lcom/android/server/wm/ActivityTaskManagerService;

.field mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

.field private mVideoModeObserver:Landroid/database/ContentObserver;


# direct methods
.method public static synthetic $r8$lambda$CTIG1B2cu7ST6iU8pQ0hjvv3FGE(Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/Task;Lcom/android/server/wm/MiuiFreeFormActivityStack;Ljava/lang/String;Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/wm/MiuiFreeFormGestureController;->lambda$deliverResultForExitFreeform$0(Lcom/android/server/wm/Task;Lcom/android/server/wm/MiuiFreeFormActivityStack;Ljava/lang/String;Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCtsMode(Lcom/android/server/wm/MiuiFreeFormGestureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->updateCtsMode()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 81
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/MiuiFreeFormManagerService;Landroid/os/Handler;)V
    .locals 4
    .param p1, "service"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p2, "miuiFreeFormManagerService"    # Lcom/android/server/wm/MiuiFreeFormManagerService;
    .param p3, "handler"    # Landroid/os/Handler;

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mFreeFormReceiver:Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsVideoMode:Z

    .line 97
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsGameMode:Z

    .line 165
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController$2;

    new-instance v2, Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V

    invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController$2;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiOptObserver:Landroid/database/ContentObserver;

    .line 193
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController$3;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V

    invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController$3;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mVideoModeObserver:Landroid/database/ContentObserver;

    .line 201
    new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController$4;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V

    invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController$4;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mGameModeObserver:Landroid/database/ContentObserver;

    .line 104
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 105
    iput-object p3, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mHandler:Landroid/os/Handler;

    .line 106
    iput-object p2, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 107
    return-void
.end method

.method private clearFreeFormSurface(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 27
    .param p1, "stack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 287
    move-object/from16 v0, p1

    if-eqz v0, :cond_9

    iget-object v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-nez v1, :cond_0

    goto/16 :goto_5

    .line 288
    :cond_0
    iget-object v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v1

    .line 289
    .local v1, "leash":Landroid/view/SurfaceControl;
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_4

    .line 290
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clear freeform surface: taskId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v3, v3, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFreeFormGestureController"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    new-instance v2, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v2}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    move-object v14, v2

    .line 292
    .local v14, "tx":Landroid/view/SurfaceControl$Transaction;
    const/4 v15, 0x0

    invoke-virtual {v14, v1, v15, v15}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 293
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v14, v1, v2, v2}, Landroid/view/SurfaceControl$Transaction;->setScale(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 294
    invoke-virtual {v14, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 295
    const/4 v13, 0x0

    invoke-virtual {v14, v1, v13, v13}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;

    .line 296
    invoke-virtual {v14, v1, v15}, Landroid/view/SurfaceControl$Transaction;->setCornerRadius(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 297
    const/4 v12, 0x3

    new-array v2, v12, [F

    fill-array-data v2, :array_0

    invoke-virtual {v14, v1, v2, v15, v15}, Landroid/view/SurfaceControl$Transaction;->setSurfaceStroke(Landroid/view/SurfaceControl;[FFF)Landroid/view/SurfaceControl$Transaction;

    .line 299
    const/high16 v2, -0x3ec00000    # -12.0f

    invoke-static {v2}, Landroid/util/MiuiMultiWindowUtils;->applyDip2Px(F)F

    move-result v16

    .line 300
    .local v16, "cornerX":F
    new-array v5, v12, [F

    fill-array-data v5, :array_1

    .line 301
    .local v5, "tipsColors":[F
    const/high16 v2, 0x41900000    # 18.0f

    invoke-static {v2}, Landroid/util/MiuiMultiWindowUtils;->applyDip2Px(F)F

    move-result v17

    .line 302
    .local v17, "radius":F
    const/high16 v2, 0x40800000    # 4.0f

    invoke-static {v2}, Landroid/util/MiuiMultiWindowUtils;->applyDip2Px(F)F

    move-result v18

    .line 303
    .local v18, "thickness":F
    const/4 v6, 0x0

    const/high16 v9, 0x42580000    # 54.0f

    move-object v2, v14

    move-object v3, v1

    move/from16 v4, v16

    move/from16 v7, v18

    move/from16 v8, v17

    invoke-virtual/range {v2 .. v9}, Landroid/view/SurfaceControl$Transaction;->setLeftBottomCornerTip(Landroid/view/SurfaceControl;F[FFFFF)Landroid/view/SurfaceControl$Transaction;

    .line 304
    const/4 v10, 0x0

    const/high16 v2, 0x42580000    # 54.0f

    move-object v6, v14

    move-object v7, v1

    move/from16 v8, v16

    move-object v9, v5

    move/from16 v11, v18

    move v3, v12

    move/from16 v12, v17

    move v4, v13

    move v13, v2

    invoke-virtual/range {v6 .. v13}, Landroid/view/SurfaceControl$Transaction;->setRightBottomCornerTip(Landroid/view/SurfaceControl;F[FFFFF)Landroid/view/SurfaceControl$Transaction;

    .line 306
    invoke-static {}, Landroid/util/MiuiMultiWindowUtils;->isSupportMiuiShadowV2()Z

    move-result v2

    const/4 v6, 0x1

    if-eqz v2, :cond_6

    .line 307
    const-class v19, Landroid/view/SurfaceControl;

    sget-object v20, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    sget-object v21, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    sget-object v22, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    sget-object v23, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    sget-object v24, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    sget-object v25, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v26, Landroid/graphics/RectF;

    filled-new-array/range {v19 .. v26}, [Ljava/lang/Class;

    move-result-object v2

    .line 309
    .local v2, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/16 v7, 0x8

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v6

    const/4 v4, 0x2

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v7, v4

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x4

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x5

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v7, v3

    .line 310
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inNormalPinMode()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 311
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inMiniPinMode()Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_3
    sget v15, Landroid/util/MiuiMultiWindowUtils;->MINI_FREEFORM_ROUND_CORNER_DIP_MIUI15:F

    goto :goto_1

    .line 310
    :cond_4
    :goto_0
    sget v15, Landroid/util/MiuiMultiWindowUtils;->FREEFORM_ROUND_CORNER_DIP_MIUI15:F

    :cond_5
    :goto_1
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const/4 v4, 0x6

    aput-object v3, v7, v4

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    const/4 v4, 0x7

    aput-object v3, v7, v4

    move-object v3, v7

    .line 313
    .local v3, "values":[Ljava/lang/Object;
    const-class v4, Landroid/view/SurfaceControl$Transaction;

    const-string/jumbo v6, "setMiShadow"

    invoke-static {v14, v4, v6, v2, v3}, Landroid/util/MiuiMultiWindowUtils;->callObjectMethod(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .end local v2    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v3    # "values":[Ljava/lang/Object;
    goto :goto_2

    .line 315
    :cond_6
    invoke-static {}, Landroid/util/MiuiMultiWindowUtils;->isSupportMiuiShadowV1()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 316
    const-class v7, Landroid/view/SurfaceControl;

    sget-object v8, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v9, [F

    sget-object v10, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    sget-object v11, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    sget-object v12, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    sget-object v13, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    filled-new-array/range {v7 .. v13}, [Ljava/lang/Class;

    move-result-object v2

    .line 318
    .restart local v2    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    sget-object v8, Landroid/util/MiuiMultiWindowUtils;->MIUI_FREEFORM_RESET_COLOR:[F

    .line 319
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    move-object v6, v1

    filled-new-array/range {v6 .. v12}, [Ljava/lang/Object;

    move-result-object v3

    .line 320
    .restart local v3    # "values":[Ljava/lang/Object;
    const-class v4, Landroid/view/SurfaceControl$Transaction;

    const-string/jumbo v6, "setShadowSettings"

    invoke-static {v14, v4, v6, v2, v3}, Landroid/util/MiuiMultiWindowUtils;->callObjectMethod(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 315
    .end local v2    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v3    # "values":[Ljava/lang/Object;
    :cond_7
    :goto_2
    nop

    .line 324
    :goto_3
    invoke-virtual {v14}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 325
    return-void

    .line 289
    .end local v5    # "tipsColors":[F
    .end local v14    # "tx":Landroid/view/SurfaceControl$Transaction;
    .end local v16    # "cornerX":F
    .end local v17    # "radius":F
    .end local v18    # "thickness":F
    :cond_8
    :goto_4
    return-void

    .line 287
    .end local v1    # "leash":Landroid/view/SurfaceControl;
    :cond_9
    :goto_5
    return-void

    nop

    :array_0
    .array-data 4
        0x3dc8b439
        0x3dc8b439
        0x3dc8b439
    .end array-data

    :array_1
    .array-data 4
        0x3d978d50    # 0.074f
        0x3d978d50    # 0.074f
        0x3d978d50    # 0.074f
    .end array-data
.end method

.method private exitFreeForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 4
    .param p1, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 328
    const-string v0, "MiuiFreeFormGestureController"

    const-string v1, "exitFreeForm"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 330
    if-eqz p1, :cond_1

    :try_start_0
    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v1, :cond_1

    .line 331
    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 332
    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getMiuiFlags()I

    move-result v2

    and-int/lit16 v2, v2, -0x101

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setMiuiFlags(I)Landroid/content/Intent;

    .line 334
    :cond_0
    const-string v1, "MiuiFreeFormGestureController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exitFreeForm freeform stack :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->moveFreeformToFullScreen(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 336
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v2, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    iget v2, v2, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->onExitFreeform(I)V

    .line 338
    :cond_1
    monitor-exit v0

    .line 339
    return-void

    .line 338
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getDisplayCutoutHeight(Lcom/android/server/wm/DisplayFrames;)I
    .locals 4
    .param p0, "displayFrames"    # Lcom/android/server/wm/DisplayFrames;

    .line 672
    if-nez p0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 673
    :cond_0
    const/4 v0, 0x0

    .line 674
    .local v0, "displayCutoutHeight":I
    iget-object v1, p0, Lcom/android/server/wm/DisplayFrames;->mInsetsState:Landroid/view/InsetsState;

    invoke-virtual {v1}, Landroid/view/InsetsState;->getDisplayCutout()Landroid/view/DisplayCutout;

    move-result-object v1

    .line 675
    .local v1, "cutout":Landroid/view/DisplayCutout;
    iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I

    if-nez v2, :cond_1

    .line 676
    invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetTop()I

    move-result v0

    goto :goto_0

    .line 677
    :cond_1
    iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 678
    invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetBottom()I

    move-result v0

    goto :goto_0

    .line 679
    :cond_2
    iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 680
    invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetLeft()I

    move-result v0

    goto :goto_0

    .line 681
    :cond_3
    iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 682
    invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetRight()I

    move-result v0

    .line 684
    :cond_4
    :goto_0
    sget-boolean v2, Lcom/android/server/wm/MiuiFreeFormGestureController;->DEBUG:Z

    if-eqz v2, :cond_5

    .line 685
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDisplayCutoutHeight displayCutoutHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFreeFormGestureController"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_5
    return v0
.end method

.method public static getNavBarHeight(Lcom/android/server/wm/InsetsStateController;)I
    .locals 1
    .param p0, "insetsStateController"    # Lcom/android/server/wm/InsetsStateController;

    .line 644
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->getNavBarHeight(Lcom/android/server/wm/InsetsStateController;Z)I

    move-result v0

    return v0
.end method

.method public static getNavBarHeight(Lcom/android/server/wm/InsetsStateController;Z)I
    .locals 7
    .param p0, "insetsStateController"    # Lcom/android/server/wm/InsetsStateController;
    .param p1, "ignoreVisibility"    # Z

    .line 648
    if-nez p0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 649
    :cond_0
    const/4 v0, 0x0

    .line 650
    .local v0, "navBarHeight":I
    nop

    .line 651
    invoke-virtual {p0}, Lcom/android/server/wm/InsetsStateController;->getSourceProviders()Landroid/util/SparseArray;

    move-result-object v1

    .line 652
    .local v1, "providers":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/wm/InsetsSourceProvider;>;"
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_4

    .line 653
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/InsetsSourceProvider;

    .line 654
    .local v3, "provider":Lcom/android/server/wm/InsetsSourceProvider;
    invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/InsetsSource;->getType()I

    move-result v4

    invoke-static {}, Landroid/view/WindowInsets$Type;->navigationBars()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 655
    goto :goto_1

    .line 657
    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/InsetsSource;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 658
    :cond_2
    invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/InsetsSource;->getFrame()Landroid/graphics/Rect;

    move-result-object v4

    .line 659
    .local v4, "frame":Landroid/graphics/Rect;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 660
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 652
    .end local v3    # "provider":Lcom/android/server/wm/InsetsSourceProvider;
    .end local v4    # "frame":Landroid/graphics/Rect;
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 665
    .end local v2    # "i":I
    :cond_4
    sget-boolean v2, Lcom/android/server/wm/MiuiFreeFormGestureController;->DEBUG:Z

    if-eqz v2, :cond_5

    .line 666
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNavBarHeight navBarHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFreeFormGestureController"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    :cond_5
    return v0
.end method

.method public static getStatusBarHeight(Lcom/android/server/wm/InsetsStateController;)I
    .locals 1
    .param p0, "insetsStateController"    # Lcom/android/server/wm/InsetsStateController;

    .line 616
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->getStatusBarHeight(Lcom/android/server/wm/InsetsStateController;Z)I

    move-result v0

    return v0
.end method

.method public static getStatusBarHeight(Lcom/android/server/wm/InsetsStateController;Z)I
    .locals 7
    .param p0, "insetsStateController"    # Lcom/android/server/wm/InsetsStateController;
    .param p1, "ignoreVisibility"    # Z

    .line 620
    if-nez p0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 621
    :cond_0
    const/4 v0, 0x0

    .line 622
    .local v0, "statusBarHeight":I
    nop

    .line 623
    invoke-virtual {p0}, Lcom/android/server/wm/InsetsStateController;->getSourceProviders()Landroid/util/SparseArray;

    move-result-object v1

    .line 624
    .local v1, "providers":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/wm/InsetsSourceProvider;>;"
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_4

    .line 625
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/InsetsSourceProvider;

    .line 626
    .local v3, "provider":Lcom/android/server/wm/InsetsSourceProvider;
    invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/InsetsSource;->getType()I

    move-result v4

    invoke-static {}, Landroid/view/WindowInsets$Type;->statusBars()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 627
    goto :goto_1

    .line 629
    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/InsetsSource;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 630
    :cond_2
    invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/InsetsSource;->getFrame()Landroid/graphics/Rect;

    move-result-object v4

    .line 631
    .local v4, "frame":Landroid/graphics/Rect;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 632
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 624
    .end local v3    # "provider":Lcom/android/server/wm/InsetsSourceProvider;
    .end local v4    # "frame":Landroid/graphics/Rect;
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 637
    .end local v2    # "i":I
    :cond_4
    sget-boolean v2, Lcom/android/server/wm/MiuiFreeFormGestureController;->DEBUG:Z

    if-eqz v2, :cond_5

    .line 638
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStatusBarHeight statusBarHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFreeFormGestureController"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    :cond_5
    return v0
.end method

.method private hide(Lcom/android/server/wm/WindowContainer;)V
    .locals 3
    .param p1, "wc"    # Lcom/android/server/wm/WindowContainer;

    .line 259
    if-nez p1, :cond_0

    return-void

    .line 260
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/WindowContainer;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 261
    .local v0, "sc":Landroid/view/SurfaceControl;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 262
    new-instance v1, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v1}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    .line 263
    .local v1, "t":Landroid/view/SurfaceControl$Transaction;
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/SurfaceControl$Transaction;->setCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;

    .line 264
    invoke-virtual {v1, v0}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 265
    invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 267
    .end local v1    # "t":Landroid/view/SurfaceControl$Transaction;
    :cond_1
    return-void
.end method

.method private hideStack(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 2
    .param p1, "stack"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 250
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 251
    if-eqz p1, :cond_1

    :try_start_0
    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-nez v1, :cond_0

    goto :goto_0

    .line 254
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->hide(Lcom/android/server/wm/WindowContainer;)V

    .line 255
    monitor-exit v0

    .line 256
    return-void

    .line 252
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    .line 255
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isPlaybackActive(Ljava/lang/String;I)Z
    .locals 16
    .param p1, "curPkg"    # Ljava/lang/String;
    .param p2, "curUid"    # I

    .line 723
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    iget-object v0, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mAudioManager:Landroid/media/AudioManager;

    const-string v4, "audio_active_track"

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 724
    .local v4, "activeTrackString":Ljava/lang/String;
    const/16 v0, 0x3b

    const/16 v5, 0x2c

    invoke-virtual {v4, v0, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 725
    .local v5, "activeTrackArray":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isPlaybackActive: activeTrackArray[PID/UID]:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " curPkg:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " curUid:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v7, "MiuiFreeFormGestureController"

    invoke-static {v7, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    iget-object v0, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 727
    .local v8, "pm":Landroid/content/pm/PackageManager;
    array-length v9, v5

    const/4 v10, 0x0

    move v11, v10

    :goto_0
    if-ge v11, v9, :cond_3

    aget-object v12, v5, v11

    .line 728
    .local v12, "str":Ljava/lang/String;
    const-string v0, "/"

    invoke-virtual {v12, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    goto :goto_2

    .line 729
    :cond_0
    invoke-virtual {v12, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 731
    .local v13, "split":[Ljava/lang/String;
    const/4 v0, 0x1

    :try_start_0
    aget-object v14, v13, v0

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 732
    .local v14, "uid":I
    invoke-virtual {v8, v14}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v15

    .line 733
    .local v15, "packageNames":[Ljava/lang/String;
    if-eq v3, v14, :cond_2

    const/4 v0, -0x1

    if-ne v3, v0, :cond_1

    aget-object v0, v15, v10

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 737
    .end local v14    # "uid":I
    .end local v15    # "packageNames":[Ljava/lang/String;
    :cond_1
    goto :goto_2

    .line 734
    .restart local v14    # "uid":I
    .restart local v15    # "packageNames":[Ljava/lang/String;
    :cond_2
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 735
    .end local v14    # "uid":I
    .end local v15    # "packageNames":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 736
    .local v0, "e":Ljava/lang/Exception;
    nop

    .line 727
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v12    # "str":Ljava/lang/String;
    .end local v13    # "split":[Ljava/lang/String;
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 739
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isPlaybackActive:false curPkg:"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    return v10
.end method

.method private synthetic lambda$deliverResultForExitFreeform$0(Lcom/android/server/wm/Task;Lcom/android/server/wm/MiuiFreeFormActivityStack;Ljava/lang/String;Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;)V
    .locals 10
    .param p1, "currentFullRootTask"    # Lcom/android/server/wm/Task;
    .param p2, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .param p3, "DELIVER_RESULT_REASON"    # Ljava/lang/String;
    .param p4, "EXIT_FREEFORM"    # Ljava/lang/String;
    .param p5, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 405
    const/4 v0, 0x0

    const-string v1, "MiuiFreeFormGestureController"

    if-eqz p5, :cond_0

    iget-object v2, p5, Lcom/android/server/wm/ActivityRecord;->resultTo:Lcom/android/server/wm/ActivityRecord;

    if-eqz v2, :cond_0

    iget-object v2, p5, Lcom/android/server/wm/ActivityRecord;->resultTo:Lcom/android/server/wm/ActivityRecord;

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v2, :cond_0

    .line 406
    iget-object v2, p5, Lcom/android/server/wm/ActivityRecord;->resultTo:Lcom/android/server/wm/ActivityRecord;

    .line 408
    .local v2, "resultTo":Lcom/android/server/wm/ActivityRecord;
    :try_start_0
    iget-object v5, p5, Lcom/android/server/wm/ActivityRecord;->resultWho:Ljava/lang/String;

    iget v6, v2, Lcom/android/server/wm/ActivityRecord;->requestCode:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v2

    move-object v4, p5

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    .line 410
    iget-object v3, v2, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    .line 411
    invoke-virtual {v3}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-static {v3, v4}, Landroid/app/servertransaction/ClientTransaction;->obtain(Landroid/app/IApplicationThread;Landroid/os/IBinder;)Landroid/app/servertransaction/ClientTransaction;

    move-result-object v3

    .line 412
    .local v3, "transaction":Landroid/app/servertransaction/ClientTransaction;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " deliverResultForExitFreeform: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " delivering results to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "results: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    iget-object v4, v2, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;

    invoke-static {v4}, Landroid/app/servertransaction/ActivityResultItem;->obtain(Ljava/util/List;)Landroid/app/servertransaction/ActivityResultItem;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 415
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V

    .line 416
    iput-object v0, v2, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 419
    .end local v3    # "transaction":Landroid/app/servertransaction/ClientTransaction;
    goto :goto_0

    .line 417
    :catch_0
    move-exception v0

    .line 418
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " deliverResultForExitFreeform: fail "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v2    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    :goto_0
    goto/16 :goto_2

    .line 423
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z

    move-result v2

    if-eqz v2, :cond_5

    if-eqz p5, :cond_5

    if-eqz p1, :cond_5

    .line 424
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 425
    .restart local v2    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    if-eqz v2, :cond_4

    iget-object v3, v2, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-nez v3, :cond_1

    goto/16 :goto_1

    .line 429
    :cond_1
    :try_start_1
    iget-object v5, p5, Lcom/android/server/wm/ActivityRecord;->resultWho:Ljava/lang/String;

    iget v6, v2, Lcom/android/server/wm/ActivityRecord;->requestCode:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v2

    move-object v4, p5

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    .line 431
    iget-object v3, v2, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    .line 432
    invoke-virtual {v3}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-static {v3, v4}, Landroid/app/servertransaction/ClientTransaction;->obtain(Landroid/app/IApplicationThread;Landroid/os/IBinder;)Landroid/app/servertransaction/ClientTransaction;

    move-result-object v3

    .line 433
    .restart local v3    # "transaction":Landroid/app/servertransaction/ClientTransaction;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " deliverResultForExitFreeform: delivering results to fullscreen activity: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 435
    .local v4, "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 436
    .local v5, "data":Landroid/content/Intent;
    const-string v6, "com.babycloud.hanju/com.tencent.connect.common.AssistActivity"

    iget-object v7, v2, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "com.tencent.mobileqq"

    .line 437
    invoke-virtual {p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 438
    :cond_2
    invoke-virtual {v5, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 440
    :cond_3
    new-instance v6, Landroid/app/ResultInfo;

    iget-object v7, p5, Lcom/android/server/wm/ActivityRecord;->resultWho:Ljava/lang/String;

    iget v8, v2, Lcom/android/server/wm/ActivityRecord;->requestCode:I

    const/4 v9, 0x0

    invoke-direct {v6, v7, v8, v9, v5}, Landroid/app/ResultInfo;-><init>(Ljava/lang/String;IILandroid/content/Intent;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441
    invoke-static {v4}, Landroid/app/servertransaction/ActivityResultItem;->obtain(Ljava/util/List;)Landroid/app/servertransaction/ActivityResultItem;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 442
    iget-object v6, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v6}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V

    .line 443
    iput-object v0, v2, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 446
    .end local v3    # "transaction":Landroid/app/servertransaction/ClientTransaction;
    .end local v4    # "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
    .end local v5    # "data":Landroid/content/Intent;
    goto :goto_2

    .line 444
    :catch_1
    move-exception v0

    .line 445
    .restart local v0    # "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " deliverResultForExitFreeform: fail to fullscreen activity "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 426
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_4
    :goto_1
    return-void

    .line 449
    .end local v2    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    :cond_5
    :goto_2
    return-void
.end method

.method static synthetic lambda$deliverResultForFinishActivity$1(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p0, "resultFrom"    # Lcom/android/server/wm/ActivityRecord;
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 479
    iget-boolean v0, p1, Lcom/android/server/wm/ActivityRecord;->finishing:Z

    if-nez v0, :cond_0

    .line 480
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getLaunchedFromPid()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->getPid()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 479
    :goto_0
    return v0
.end method

.method private moveFreeformToFullScreen(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 8
    .param p1, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 565
    iget-object v0, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 566
    .local v0, "rootTask":Lcom/android/server/wm/Task;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v1

    if-nez v1, :cond_0

    goto/16 :goto_1

    .line 567
    :cond_0
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 568
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "moveFreeformToFullScreen::::::remove pin task ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFreeFormGestureController"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/ActivityTaskSupervisor;->removeRootTask(Lcom/android/server/wm/Task;)V

    .line 570
    return-void

    .line 572
    :cond_1
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 573
    invoke-virtual {p1, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setIsFrontFreeFormStackInfo(Z)V

    .line 574
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 577
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v1}, Lcom/android/server/wm/Task;->setForceHidden(IZ)Z

    .line 578
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2, v1}, Lcom/android/server/wm/Task;->ensureActivitiesVisible(Lcom/android/server/wm/ActivityRecord;IZ)V

    .line 579
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    invoke-virtual {v4, v3, v2, v1, v3}, Lcom/android/server/wm/ActivityTaskSupervisor;->activityIdleInternal(Lcom/android/server/wm/ActivityRecord;ZZLandroid/content/res/Configuration;)V

    .line 581
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskManagerService;->deferWindowLayout()V

    .line 583
    :try_start_0
    filled-new-array {v0}, [Lcom/android/server/wm/Task;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/collect/Sets;->newArraySet([Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v4

    .line 584
    .local v4, "tasks":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/Task;>;"
    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mTaskSnapshotController:Lcom/android/server/wm/TaskSnapshotController;

    invoke-virtual {v5, v4}, Lcom/android/server/wm/TaskSnapshotController;->snapshotTasks(Landroid/util/ArraySet;)V

    .line 585
    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v5, v5, Lcom/android/server/wm/WindowManagerService;->mTaskSnapshotController:Lcom/android/server/wm/TaskSnapshotController;

    invoke-virtual {v5, v4}, Lcom/android/server/wm/TaskSnapshotController;->addSkipClosingAppSnapshotTasks(Ljava/util/Set;)V

    .line 592
    new-instance v5, Landroid/content/res/Configuration;

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRequestedOverrideConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 593
    .local v5, "c":Landroid/content/res/Configuration;
    iget-object v6, v5, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v6, v2}, Landroid/app/WindowConfiguration;->setAlwaysOnTop(Z)V

    .line 594
    iget-object v6, v5, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v6, v2}, Landroid/app/WindowConfiguration;->setWindowingMode(I)V

    .line 595
    iget-object v6, v5, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v6, v3}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V

    .line 596
    invoke-virtual {v0, v5}, Lcom/android/server/wm/Task;->onRequestedOverrideConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 598
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;

    move-result-object v6

    .line 603
    .local v6, "taskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    invoke-virtual {v6, v0}, Lcom/android/server/wm/TaskDisplayArea;->positionTaskBehindHome(Lcom/android/server/wm/Task;)V

    .line 606
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/Task;->setForceHidden(IZ)Z

    .line 607
    iget-object v7, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v7, v7, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v7, v7, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v7, v3, v2, v1}, Lcom/android/server/wm/RootWindowContainer;->ensureActivitiesVisible(Lcom/android/server/wm/ActivityRecord;IZ)V

    .line 608
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->resumeFocusedTasksTopActivities()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 611
    nop

    .end local v4    # "tasks":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/Task;>;"
    .end local v5    # "c":Landroid/content/res/Configuration;
    .end local v6    # "taskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->continueWindowLayout()V

    .line 612
    throw v1

    .line 609
    :catch_0
    move-exception v1

    .line 611
    nop

    :goto_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->continueWindowLayout()V

    .line 612
    nop

    .line 613
    return-void

    .line 566
    :cond_3
    :goto_1
    return-void
.end method

.method private updateCtsMode()V
    .locals 5

    .line 174
    const-string v0, "persist.sys.miui_optimization"

    const-string v1, "1"

    const-string v2, "ro.miui.cts"

    .line 175
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 174
    const/4 v2, 0x1

    xor-int/2addr v1, v2

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/2addr v0, v2

    .line 177
    .local v0, "isCtsMode":Z
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 178
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v3, "android.software.freeform_window_management"

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 180
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "enable_freeform_support"

    .line 179
    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v4, v2

    :cond_1
    move v1, v4

    .line 182
    .local v1, "freeformWindowManagement":Z
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v3, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v3

    .line 183
    if-eqz v0, :cond_2

    .line 184
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iput-boolean v1, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z

    goto :goto_0

    .line 186
    :cond_2
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iput-boolean v2, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z

    .line 188
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    const-string v2, "MiuiFreeFormGestureController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isCtsMode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "freeformWindowManagement \uff1a "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mService.mSupportsFreeformWindowManagement: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-boolean v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    return-void

    .line 188
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method


# virtual methods
.method public clearAllFreeFormForProcessReboot()V
    .locals 6

    .line 270
    const-string v0, "MiuiFreeFormGestureController"

    const-string v1, "clearAllFreeForm for process dead"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0

    .line 272
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormActivityStacks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 273
    .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v2, :cond_1

    iget-object v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v3, :cond_1

    .line 274
    iget-object v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 275
    iget-object v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v4}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getMiuiFlags()I

    move-result v4

    and-int/lit16 v4, v4, -0x101

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setMiuiFlags(I)Landroid/content/Intent;

    .line 277
    :cond_0
    const-string v3, "MiuiFreeFormGestureController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exit freeform stack:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->moveFreeformToFullScreen(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 280
    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->clearFreeFormSurface(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 282
    .end local v2    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_1
    goto :goto_0

    .line 283
    :cond_2
    monitor-exit v0

    .line 284
    return-void

    .line 283
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public deliverDestroyItemToAlipay(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 10
    .param p1, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 539
    const-string v0, "MiuiFreeFormGestureController"

    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-nez v1, :cond_0

    .line 540
    return-void

    .line 543
    :cond_0
    :try_start_0
    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->topRunningActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 544
    .local v1, "topActivity":Lcom/android/server/wm/ActivityRecord;
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 545
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/wm/DisplayContent;->getActivityBelow(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    move-object v9, v2

    .line 546
    .local v9, "bottomActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_2

    if-eqz v9, :cond_2

    iget-object v2, v9, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v2, :cond_2

    const-string v2, "com.eg.android.AlipayGphone/com.alipay.mobile.quinox.SchemeLauncherActivity"

    iget-object v3, v9, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    .line 547
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "com.eg.android.AlipayGphone/com.alipay.mobile.nebulax.integration.mpaas.activity.NebulaActivity$Main"

    iget-object v3, v1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    .line 548
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.eg.android.AlipayGphone/com.alipay.mobile.nebulax.xriver.activity.XRiverActivity"

    iget-object v3, v1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    .line 549
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 550
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deliverDestroyItemToAlipay: delivering destroyitem to bottomactivity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    iget-object v4, v1, Lcom/android/server/wm/ActivityRecord;->resultWho:Ljava/lang/String;

    iget v5, v9, Lcom/android/server/wm/ActivityRecord;->requestCode:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v9

    move-object v3, v1

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    .line 552
    iget-object v2, v9, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    .line 553
    invoke-virtual {v2}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v2

    iget-object v3, v9, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-static {v2, v3}, Landroid/app/servertransaction/ClientTransaction;->obtain(Landroid/app/IApplicationThread;Landroid/os/IBinder;)Landroid/app/servertransaction/ClientTransaction;

    move-result-object v2

    .line 554
    .local v2, "transaction":Landroid/app/servertransaction/ClientTransaction;
    invoke-static {v8, v8}, Landroid/app/servertransaction/DestroyActivityItem;->obtain(ZI)Landroid/app/servertransaction/DestroyActivityItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 555
    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V

    .line 556
    const/4 v3, 0x0

    iput-object v3, v9, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;

    .line 557
    invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/Task;->removeImmediately()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 561
    .end local v1    # "topActivity":Lcom/android/server/wm/ActivityRecord;
    .end local v2    # "transaction":Landroid/app/servertransaction/ClientTransaction;
    .end local v9    # "bottomActivity":Lcom/android/server/wm/ActivityRecord;
    :cond_2
    goto :goto_0

    .line 559
    :catch_0
    move-exception v1

    .line 560
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deliverDestroyItemToAlipay: fail "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public deliverResultForExitFreeform(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 19
    .param p1, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 369
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    const-string v1, "MiuiFreeFormGestureController"

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z

    move-result v0

    if-eqz v0, :cond_6

    if-nez v8, :cond_0

    goto/16 :goto_3

    .line 374
    :cond_0
    iget-object v0, v7, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskSupervisor;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    .line 375
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;

    move-result-object v9

    .line 376
    .local v9, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    const/4 v0, 0x0

    if-nez v9, :cond_1

    move-object v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v2

    :goto_0
    move-object v10, v2

    .line 377
    .local v10, "currentFullRootTask":Lcom/android/server/wm/Task;
    if-eqz v10, :cond_5

    invoke-virtual {v10}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v2

    .line 378
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I

    move-result v3

    if-eq v2, v3, :cond_2

    goto/16 :goto_2

    .line 381
    :cond_2
    const-string v11, "deliverResultsReason"

    .line 382
    .local v11, "DELIVER_RESULT_REASON":Ljava/lang/String;
    const-string v12, "ExitFreeform"

    .line 383
    .local v12, "EXIT_FREEFORM":Ljava/lang/String;
    iget-object v2, v8, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    if-eqz v2, :cond_4

    iget-object v2, v8, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, v8, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    .line 384
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    const-string v3, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 385
    iget-object v2, v8, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 386
    .local v2, "resultFrom":Lcom/android/server/wm/ActivityRecord;
    move-object v3, v9

    .line 387
    .local v3, "resultTo":Lcom/android/server/wm/ActivityRecord;
    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    iget-object v4, v3, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v4, :cond_3

    .line 389
    :try_start_0
    iget-object v15, v2, Lcom/android/server/wm/ActivityRecord;->resultWho:Ljava/lang/String;

    iget v4, v3, Lcom/android/server/wm/ActivityRecord;->requestCode:I

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v13, v3

    move-object v14, v2

    move/from16 v16, v4

    invoke-virtual/range {v13 .. v18}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    .line 391
    iget-object v4, v3, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    .line 392
    invoke-virtual {v4}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v4

    iget-object v5, v3, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-static {v4, v5}, Landroid/app/servertransaction/ClientTransaction;->obtain(Landroid/app/IApplicationThread;Landroid/os/IBinder;)Landroid/app/servertransaction/ClientTransaction;

    move-result-object v4

    .line 393
    .local v4, "transaction":Landroid/app/servertransaction/ClientTransaction;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " deliverResultForExitFreeform for applicationlock"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " delivering results to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "results: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v5, v3, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;

    invoke-static {v5}, Landroid/app/servertransaction/ActivityResultItem;->obtain(Ljava/util/List;)Landroid/app/servertransaction/ActivityResultItem;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 396
    iget-object v5, v7, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V

    .line 397
    iput-object v0, v3, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    .end local v4    # "transaction":Landroid/app/servertransaction/ClientTransaction;
    goto :goto_1

    .line 398
    :catch_0
    move-exception v0

    .line 399
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " deliverResultForExitFreeform: fail "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    :goto_1
    return-void

    .line 404
    .end local v2    # "resultFrom":Lcom/android/server/wm/ActivityRecord;
    .end local v3    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    :cond_4
    iget-object v0, v8, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    new-instance v13, Lcom/android/server/wm/MiuiFreeFormGestureController$$ExternalSyntheticLambda0;

    move-object v1, v13

    move-object/from16 v2, p0

    move-object v3, v10

    move-object/from16 v4, p1

    move-object v5, v11

    move-object v6, v12

    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/MiuiFreeFormGestureController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/Task;Lcom/android/server/wm/MiuiFreeFormActivityStack;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v13}, Lcom/android/server/wm/Task;->forAllActivities(Ljava/util/function/Consumer;)V

    .line 450
    return-void

    .line 379
    .end local v11    # "DELIVER_RESULT_REASON":Ljava/lang/String;
    .end local v12    # "EXIT_FREEFORM":Ljava/lang/String;
    :cond_5
    :goto_2
    return-void

    .line 370
    .end local v9    # "activityRecord":Lcom/android/server/wm/ActivityRecord;
    .end local v10    # "currentFullRootTask":Lcom/android/server/wm/Task;
    :cond_6
    :goto_3
    return-void
.end method

.method public deliverResultForFinishActivity(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;)V
    .locals 16
    .param p1, "resultTo"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "resultFrom"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "intent"    # Landroid/content/Intent;

    .line 464
    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    if-eqz v8, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_5

    .line 467
    :cond_0
    const/4 v2, 0x0

    .line 468
    .local v2, "deliverResult":Z
    const/4 v10, 0x0

    const-string v3, "com.tencent.mobileqq/cooperation.qzone.share.QZoneShareActivity"

    const-string v4, "com.tencent.mobileqq/.activity.JumpActivity"

    const-string v11, "MiuiFreeFormGestureController"

    if-eqz v0, :cond_4

    .line 470
    iget-object v5, v0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v8, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    .line 471
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 472
    const/4 v2, 0x1

    .line 473
    iget-object v3, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 474
    .end local p1    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    .local v0, "resultTo":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    move-object v12, v0

    move v13, v2

    goto/16 :goto_2

    .line 475
    :cond_2
    :goto_0
    return-void

    .line 516
    .end local v0    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    .restart local p1    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    :cond_3
    move-object v12, v0

    move v13, v2

    goto/16 :goto_2

    .line 479
    :cond_4
    iget-object v5, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    new-instance v6, Lcom/android/server/wm/MiuiFreeFormGestureController$$ExternalSyntheticLambda1;

    invoke-direct {v6, v8}, Lcom/android/server/wm/MiuiFreeFormGestureController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/ActivityRecord;)V

    invoke-virtual {v5, v6}, Lcom/android/server/wm/DisplayContent;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v5

    .line 481
    .local v5, "launchActivity":Lcom/android/server/wm/ActivityRecord;
    iget-object v6, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v6}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;

    move-result-object v6

    .line 482
    .end local p1    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    .local v6, "resultTo":Lcom/android/server/wm/ActivityRecord;
    iget-object v0, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 483
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;

    move-result-object v7

    .line 484
    .local v7, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v6, :cond_d

    invoke-virtual {v6}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, v6, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-eqz v0, :cond_d

    if-eqz v7, :cond_d

    .line 485
    invoke-virtual {v6}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v0

    invoke-virtual {v7}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I

    move-result v12

    if-eq v0, v12, :cond_5

    goto/16 :goto_4

    .line 488
    :cond_5
    if-nez v5, :cond_6

    iget-object v0, v8, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, v8, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    .line 489
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    if-eqz v5, :cond_8

    .line 490
    invoke-virtual {v6}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    if-ne v0, v3, :cond_8

    .line 491
    :cond_7
    const/4 v2, 0x1

    .line 493
    :cond_8
    const-string v0, "com.sina.weibo/.composerinde.ComposerDispatchActivity"

    iget-object v3, v8, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 494
    const-string v0, "com.youku.phone/com.sina.weibo.sdk.share.ShareTransActivity"

    iget-object v3, v6, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 495
    const/4 v0, 0x1

    move v2, v0

    .line 498
    :cond_9
    if-eqz v9, :cond_b

    const-string v0, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    iget-object v3, v8, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 499
    const-string v0, "android.intent.extra.INTENT"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/content/Intent;

    .line 500
    .local v3, "rawIntent":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 501
    .local v0, "rawPackageName":Ljava/lang/String;
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 502
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    .line 504
    :cond_a
    move-object v4, v0

    .end local v0    # "rawPackageName":Ljava/lang/String;
    .local v4, "rawPackageName":Ljava/lang/String;
    :goto_1
    const-string/jumbo v0, "security"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v12

    .line 505
    .local v12, "b":Landroid/os/IBinder;
    if-eqz v12, :cond_b

    .line 507
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    .line 508
    .local v0, "userId":I
    invoke-static {v12}, Lmiui/security/ISecurityManager$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/security/ISecurityManager;

    move-result-object v13

    .line 509
    .local v13, "service":Lmiui/security/ISecurityManager;
    invoke-interface {v13, v4, v10, v0}, Lmiui/security/ISecurityManager;->checkAccessControlPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z

    move-result v14
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    xor-int/lit8 v14, v14, 0x1

    move v2, v14

    .line 512
    .end local v0    # "userId":I
    .end local v13    # "service":Lmiui/security/ISecurityManager;
    move v13, v2

    move-object v12, v6

    goto :goto_2

    .line 510
    :catch_0
    move-exception v0

    .line 511
    .local v0, "e":Ljava/lang/Exception;
    const-string v13, "deliverResultForFinishActivity checkAccessControlPass error: "

    invoke-static {v11, v13, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 516
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "rawIntent":Landroid/content/Intent;
    .end local v4    # "rawPackageName":Ljava/lang/String;
    .end local v5    # "launchActivity":Lcom/android/server/wm/ActivityRecord;
    .end local v7    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .end local v12    # "b":Landroid/os/IBinder;
    :cond_b
    move v13, v2

    move-object v12, v6

    .end local v2    # "deliverResult":Z
    .end local v6    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    .local v12, "resultTo":Lcom/android/server/wm/ActivityRecord;
    .local v13, "deliverResult":Z
    :goto_2
    if-eqz v13, :cond_c

    .line 518
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 519
    .local v0, "data":Landroid/content/Intent;
    const-string v2, "deliverResultsReason"

    move-object v14, v2

    .line 520
    .local v14, "DELIVER_RESULT_REASON":Ljava/lang/String;
    const-string v2, "ExitFreeform"

    move-object v15, v2

    .line 521
    .local v15, "EXIT_FREEFORM":Ljava/lang/String;
    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 522
    iget-object v4, v8, Lcom/android/server/wm/ActivityRecord;->resultWho:Ljava/lang/String;

    iget v5, v12, Lcom/android/server/wm/ActivityRecord;->requestCode:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v12

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    .line 524
    iget-object v2, v12, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    .line 525
    invoke-virtual {v2}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v2

    iget-object v3, v12, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-static {v2, v3}, Landroid/app/servertransaction/ClientTransaction;->obtain(Landroid/app/IApplicationThread;Landroid/os/IBinder;)Landroid/app/servertransaction/ClientTransaction;

    move-result-object v2

    .line 526
    .local v2, "transaction":Landroid/app/servertransaction/ClientTransaction;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deliverResultForFinishActivity: delivering results to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " resultFrom= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 528
    .local v3, "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
    new-instance v4, Landroid/app/ResultInfo;

    iget-object v5, v8, Lcom/android/server/wm/ActivityRecord;->resultWho:Ljava/lang/String;

    iget v6, v12, Lcom/android/server/wm/ActivityRecord;->requestCode:I

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7, v0}, Landroid/app/ResultInfo;-><init>(Ljava/lang/String;IILandroid/content/Intent;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 529
    invoke-static {v3}, Landroid/app/servertransaction/ActivityResultItem;->obtain(Ljava/util/List;)Landroid/app/servertransaction/ActivityResultItem;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 530
    iget-object v4, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V

    .line 531
    iput-object v10, v12, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 534
    .end local v0    # "data":Landroid/content/Intent;
    .end local v2    # "transaction":Landroid/app/servertransaction/ClientTransaction;
    .end local v3    # "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
    .end local v14    # "DELIVER_RESULT_REASON":Ljava/lang/String;
    .end local v15    # "EXIT_FREEFORM":Ljava/lang/String;
    goto :goto_3

    .line 532
    :catch_1
    move-exception v0

    .line 533
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deliverResultForFinishActivity: fail deliver results to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_c
    :goto_3
    return-void

    .line 486
    .end local v12    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    .end local v13    # "deliverResult":Z
    .local v2, "deliverResult":Z
    .restart local v5    # "launchActivity":Lcom/android/server/wm/ActivityRecord;
    .restart local v6    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    .restart local v7    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_d
    :goto_4
    return-void

    .line 465
    .end local v2    # "deliverResult":Z
    .end local v5    # "launchActivity":Lcom/android/server/wm/ActivityRecord;
    .end local v6    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    .end local v7    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    .restart local p1    # "resultTo":Lcom/android/server/wm/ActivityRecord;
    :cond_e
    :goto_5
    return-void
.end method

.method public deliverResultForResumeActivityInFreeform(Lcom/android/server/wm/ActivityRecord;)V
    .locals 10
    .param p1, "resultTo"    # Lcom/android/server/wm/ActivityRecord;

    .line 343
    const-string v0, "MiuiFreeFormGestureController"

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    if-nez v1, :cond_0

    goto/16 :goto_1

    .line 346
    :cond_0
    const-string v1, "com.xiaomi.payment/com.mipay.common.ui.TranslucentActivity"

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 348
    :try_start_0
    const-string v1, "deliverResultsReason"

    .line 349
    .local v1, "DELIVER_RESULT_REASON":Ljava/lang/String;
    const-string v2, "ExitFreeform"

    .line 350
    .local v2, "EXIT_FREEFORM":Ljava/lang/String;
    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p1, Lcom/android/server/wm/ActivityRecord;->requestCode:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V

    .line 352
    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->app:Lcom/android/server/wm/WindowProcessController;

    .line 353
    invoke-virtual {v3}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;

    move-result-object v3

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-static {v3, v4}, Landroid/app/servertransaction/ClientTransaction;->obtain(Landroid/app/IApplicationThread;Landroid/os/IBinder;)Landroid/app/servertransaction/ClientTransaction;

    move-result-object v3

    .line 354
    .local v3, "transaction":Landroid/app/servertransaction/ClientTransaction;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " deliverResultForResumeActivityInFreeform: delivering results to PAYMENT_TRANSLUCENTACTIVITY: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 356
    .local v4, "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 357
    .local v5, "data":Landroid/content/Intent;
    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    new-instance v6, Landroid/app/ResultInfo;

    iget v7, p1, Lcom/android/server/wm/ActivityRecord;->requestCode:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v6, v9, v7, v8, v5}, Landroid/app/ResultInfo;-><init>(Ljava/lang/String;IILandroid/content/Intent;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    invoke-static {v4}, Landroid/app/servertransaction/ActivityResultItem;->obtain(Ljava/util/List;)Landroid/app/servertransaction/ActivityResultItem;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V

    .line 360
    iget-object v6, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v6}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V

    .line 361
    iput-object v9, p1, Lcom/android/server/wm/ActivityRecord;->results:Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    .end local v1    # "DELIVER_RESULT_REASON":Ljava/lang/String;
    .end local v2    # "EXIT_FREEFORM":Ljava/lang/String;
    .end local v3    # "transaction":Landroid/app/servertransaction/ClientTransaction;
    .end local v4    # "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
    .end local v5    # "data":Landroid/content/Intent;
    goto :goto_0

    .line 362
    :catch_0
    move-exception v1

    .line 363
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deliverResultForResumeActivityInFreeform: fail to PAYMENT_TRANSLUCENTACTIVITY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    return-void

    .line 344
    :cond_2
    :goto_1
    return-void
.end method

.method public displayConfigurationChange(Lcom/android/server/wm/DisplayContent;Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "configuration"    # Landroid/content/res/Configuration;

    .line 691
    iget v0, p2, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsPortrait:Z

    .line 692
    iget v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mLastOrientation:I

    iget v3, p2, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v3, :cond_2

    .line 693
    iget v0, p2, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mLastOrientation:I

    .line 694
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mInputMethodWindow:Lcom/android/server/wm/WindowState;

    .line 695
    .local v0, "imeWin":Lcom/android/server/wm/WindowState;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 696
    invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->isDisplayed()Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    goto :goto_1

    :cond_1
    nop

    .line 697
    .local v1, "imeVisible":Z
    :goto_1
    if-eqz v1, :cond_2

    .line 698
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notify Ime showStatus for new orientation:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p2, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiFreeFormGestureController"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getInputMethodWindowVisibleHeight()I

    move-result v3

    .line 700
    .local v3, "imeHeight":I
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v4, v2, v3, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setAdjustedForIme(ZIZ)V

    .line 703
    .end local v0    # "imeWin":Lcom/android/server/wm/WindowState;
    .end local v1    # "imeVisible":Z
    .end local v3    # "imeHeight":I
    :cond_2
    return-void
.end method

.method public exitFreeFormByKeyCombination(Lcom/android/server/wm/Task;)V
    .locals 1
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 748
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormKeyCombinationHelper:Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->exitFreeFormByKeyCombination(Lcom/android/server/wm/Task;)V

    .line 749
    return-void
.end method

.method public freeFormAndFullScreenToggleByKeyCombination(Z)V
    .locals 1
    .param p1, "isStartFreeForm"    # Z

    .line 744
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormKeyCombinationHelper:Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->freeFormAndFullScreenToggleByKeyCombination(Z)V

    .line 745
    return-void
.end method

.method public ignoreDeliverResultForFreeForm(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 3
    .param p1, "resultTo"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "resultFrom"    # Lcom/android/server/wm/ActivityRecord;

    .line 453
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 454
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 457
    :cond_0
    const-string v1, "com.tencent.mobileqq/.activity.JumpActivity"

    iget-object v2, p2, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 458
    const/4 v0, 0x1

    return v0

    .line 460
    :cond_1
    return v0

    .line 455
    :cond_2
    :goto_0
    return v0
.end method

.method isInVideoOrGameScene()Z
    .locals 1

    .line 210
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsVideoMode:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsGameMode:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public needForegroundPin(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z
    .locals 4
    .param p1, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 710
    invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;

    move-result-object v0

    .line 711
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mTask:Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 712
    .local v1, "r":Lcom/android/server/wm/ActivityRecord;
    const/4 v2, -0x1

    .line 713
    .local v2, "uid":I
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v3, :cond_0

    .line 714
    iget-object v3, v1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 716
    :cond_0
    invoke-static {}, Landroid/util/MiuiMultiWindowUtils;->supportForeGroundPin()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v0}, Landroid/util/MiuiMultiWindowAdapter;->isInForegroundPinAppBlackList(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 717
    invoke-direct {p0, v0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isPlaybackActive(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v0}, Landroid/util/MiuiMultiWindowAdapter;->isInAudioForegroundPinAppList(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 718
    :cond_1
    invoke-static {v0}, Landroid/util/MiuiMultiWindowAdapter;->isInForegroundPinAppWhiteList(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 719
    invoke-static {v0}, Landroid/util/MiuiMultiWindowAdapter;->isInTopGameList(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    .line 716
    :goto_0
    return v3
.end method

.method public onATMSSystemReady()V
    .locals 6

    .line 110
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 111
    new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-direct {v0, v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;-><init>(Landroid/content/Context;Lcom/android/server/wm/MiuiFreeFormManagerService;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    .line 112
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mAudioManager:Landroid/media/AudioManager;

    .line 113
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->registerFreeformCloudDataObserver()V

    .line 114
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mFreeFormReceiver:Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;

    iget-object v3, v2, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;->mFilter:Landroid/content/IntentFilter;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 115
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 116
    const-string v2, "miui_optimization"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiOptObserver:Landroid/database/ContentObserver;

    const/4 v4, -0x1

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 118
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 119
    const-string/jumbo v2, "vtb_boosting"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mVideoModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v1, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 121
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 122
    const-string v2, "gb_boosting"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mGameModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v1, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 124
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->updateDataFromSetting()V

    .line 125
    invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->updateCtsMode()V

    .line 126
    new-instance v0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormKeyCombinationHelper:Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;

    .line 127
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsPortrait:Z

    .line 128
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController$1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeFormGestureController$1;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 133
    return-void
.end method

.method public onFirstWindowDrawn(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 706
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiMultiWindowRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->onFirstWindowDrawn(Lcom/android/server/wm/ActivityRecord;)V

    .line 707
    return-void
.end method

.method public reflectRemoveUnreachableFreeformTask()V
    .locals 7

    .line 226
    const/4 v0, 0x0

    .line 228
    .local v0, "method":Ljava/lang/reflect/Method;
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskSupervisor;->mRecentTasks:Lcom/android/server/wm/RecentTasks;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 229
    .local v1, "clazz":Ljava/lang/Class;
    const-string v2, "removeUnreachableFreeformTask"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    move-object v0, v2

    .line 230
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 231
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskSupervisor:Lcom/android/server/wm/ActivityTaskSupervisor;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskSupervisor;->mRecentTasks:Lcom/android/server/wm/RecentTasks;

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v4, v6

    invoke-virtual {v0, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    nop

    .end local v1    # "clazz":Ljava/lang/Class;
    goto :goto_0

    .line 232
    :catch_0
    move-exception v1

    .line 233
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDeclaredMethod:reflectRemoveUnreachableFreeformTask"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFreeFormGestureController"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public registerFreeformCloudDataObserver()V
    .locals 4

    .line 214
    new-instance v0, Lcom/android/server/wm/MiuiFreeFormGestureController$5;

    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController$5;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Landroid/os/Handler;)V

    .line 221
    .local v0, "contentObserver":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 223
    return-void
.end method

.method public startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
    .locals 3
    .param p1, "mffas"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 239
    const-string v0, "MiuiFreeFormGestureController"

    const-string/jumbo v1, "startExitApplication"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->hideStack(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 241
    new-instance v0, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    .line 242
    .local v0, "inputTransaction":Landroid/view/SurfaceControl$Transaction;
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v1

    .line 243
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getInputMonitor()Lcom/android/server/wm/InputMonitor;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsImmediately(Landroid/view/SurfaceControl$Transaction;)V

    .line 244
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 246
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->exitFreeForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V

    .line 247
    return-void

    .line 244
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
