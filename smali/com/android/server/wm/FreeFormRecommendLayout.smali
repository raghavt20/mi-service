.class public Lcom/android/server/wm/FreeFormRecommendLayout;
.super Landroid/widget/FrameLayout;
.source "FreeFormRecommendLayout.java"


# static fields
.field private static final RECOMMEND_VIEW_TOP_MARGIN:F = 37.0f

.field private static final RECOMMEND_VIEW_TOP_MARGIN_OFFSET:F = 10.0f

.field private static final TAG:Ljava/lang/String; = "FreeFormRecommendLayout"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFreeFormIconContainer:Landroid/widget/RelativeLayout;

.field private mFreeFormImageView:Landroid/widget/ImageView;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-virtual {p0, p1}, Lcom/android/server/wm/FreeFormRecommendLayout;->init(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-virtual {p0, p1}, Lcom/android/server/wm/FreeFormRecommendLayout;->init(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    invoke-virtual {p0, p1}, Lcom/android/server/wm/FreeFormRecommendLayout;->init(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 50
    invoke-virtual {p0, p1}, Lcom/android/server/wm/FreeFormRecommendLayout;->init(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method private dipToPx(F)F
    .locals 2
    .param p1, "dip"    # F

    .line 121
    nop

    .line 122
    invoke-virtual {p0}, Lcom/android/server/wm/FreeFormRecommendLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 121
    const/4 v1, 0x1

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method


# virtual methods
.method public createLayoutParams(I)Landroid/view/WindowManager$LayoutParams;
    .locals 9
    .param p1, "topMargin"    # I

    .line 86
    const/4 v6, -0x2

    .line 87
    .local v6, "width":I
    const/4 v7, -0x2

    .line 88
    .local v7, "height":I
    new-instance v8, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7f6

    const v4, 0x1000528

    const/4 v5, 0x1

    move-object v0, v8

    move v1, v6

    move v2, v7

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 98
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 99
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x40

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 100
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    const/high16 v2, 0x20000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 101
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setFitInsetsTypes(I)V

    .line 102
    const/4 v2, 0x1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 103
    const/4 v2, -0x1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 104
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->rotationAnimation:I

    .line 105
    const/16 v2, 0x31

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 106
    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {p0, v2}, Lcom/android/server/wm/FreeFormRecommendLayout;->dipToPx(F)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v2, p1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 107
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 108
    const-string v1, "FreeForm-RecommendView"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    return-object v0
.end method

.method public getFreeFormIconContainer()Landroid/widget/RelativeLayout;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/android/server/wm/FreeFormRecommendLayout;->mFreeFormIconContainer:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 54
    const-string v0, "FreeFormRecommendLayout"

    const-string v1, "init FreeFormRecommendLayout "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iput-object p1, p0, Lcom/android/server/wm/FreeFormRecommendLayout;->mContext:Landroid/content/Context;

    .line 56
    const-string/jumbo v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/server/wm/FreeFormRecommendLayout;->mWindowManager:Landroid/view/WindowManager;

    .line 57
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .line 75
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 76
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConfigurationChanged newConfig.orientation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FreeFormRecommendLayout"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 81
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 82
    const-string v0, "FreeFormRecommendLayout"

    const-string v1, "onDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 66
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 67
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/FreeFormRecommendLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/android/server/wm/FreeFormRecommendLayout;->mFreeFormIconContainer:Landroid/widget/RelativeLayout;

    .line 68
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/server/wm/FreeFormRecommendLayout;->mFreeFormImageView:Landroid/widget/ImageView;

    .line 70
    const-string v0, "FreeFormRecommendLayout"

    const-string v1, "onFinishInflate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    return-void
.end method

.method public setFreeFormIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "freeFormDrawable"    # Landroid/graphics/drawable/Drawable;

    .line 117
    iget-object v0, p0, Lcom/android/server/wm/FreeFormRecommendLayout;->mFreeFormImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 118
    return-void
.end method
