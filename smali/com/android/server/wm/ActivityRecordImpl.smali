.class public Lcom/android/server/wm/ActivityRecordImpl;
.super Ljava/lang/Object;
.source "ActivityRecordImpl.java"

# interfaces
.implements Lcom/android/server/wm/ActivityRecordStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl;
    }
.end annotation


# static fields
.field private static final BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN_IN_SPLIT_MODE:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final COMPAT_WHITE_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static FORCE_SKIP_RELAUNCH_ACTIVITY_NAMES:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final NON_RESIZEABLE_PORTRAIT_ACTIVITY:Ljava/lang/String; = "android.server.wm.CompatChangeTests$NonResizeablePortraitActivity"

.field public static final PERMISSION_ACTIVITY:Ljava/lang/String; = "com.android.packageinstaller.permission.ui.GrantPermissionsActivity"

.field public static final PROPERTY_MIUI_BLUR_CHANGED:Ljava/lang/String; = "miui.blurChanged"

.field public static final PROPERTY_MIUI_BLUR_RELAUNCH:Ljava/lang/String; = "miui.blurRelaunch"

.field private static final RESIZEABLE_PORTRAIT_ACTIVITY:Ljava/lang/String; = "android.server.wm.CompatChangeTests$ResizeablePortraitActivity"

.field private static SKIP_RELAUNCH_ACTIVITY_NAMES:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final STATSD_NON_RESIZEABLE_PORTRAIT_ACTIVITY:Ljava/lang/String; = "com.android.server.cts.device.statsdatom.StatsdCtsNonResizeablePortraitActivity"

.field private static final SUPPORT_SIZE_CHANGE_PORTRAIT_ACTIVITY:Ljava/lang/String; = "android.server.wm.CompatChangeTests$SupportsSizeChangesPortraitActivity"

.field private static final TAG:Ljava/lang/String; = "ActivityRecordImpl"

.field private static sStartWindowBlackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mSecurityInternal:Lmiui/security/SecurityManagerInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/ActivityRecordImpl;->COMPAT_WHITE_LIST:Ljava/util/List;

    .line 89
    new-instance v1, Landroid/util/ArraySet;

    invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V

    sput-object v1, Lcom/android/server/wm/ActivityRecordImpl;->BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN:Landroid/util/ArraySet;

    .line 91
    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V

    sput-object v2, Lcom/android/server/wm/ActivityRecordImpl;->BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN_IN_SPLIT_MODE:Landroid/util/ArraySet;

    .line 95
    const-string v3, "com.iflytek.inputmethod.miui/com.iflytek.inputmethod.LauncherSettingsActivity"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 96
    const-string v3, "com.android.camera/.AssistantCamera"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 97
    const-string v3, "com.tencent.mm/.plugin.voip.ui.VideoActivity"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 98
    const-string v3, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 99
    const-string v3, "com.android.deskclock/.alarm.alert.AlarmAlertFullScreenActivity"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 100
    const-string/jumbo v3, "tv.danmaku.bili/com.bilibili.video.story.StoryVideoActivity"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 101
    const-string/jumbo v3, "tv.danmaku.bili/com.bilibili.video.videodetail.VideoDetailsActivity"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 102
    const-string v3, "com.miui.securitycenter/com.miui.applicationlock.TransitionHelper"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 103
    const-string v3, "com.android.incallui/.InCallActivity"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v3, "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"

    invoke-virtual {v1, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 105
    const-string v1, "com.xiaomi.smarthome/.SmartHomeMainActivity"

    invoke-virtual {v2, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 106
    const-string v1, "android.server.wm.CompatChangeTests$ResizeablePortraitActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v1, "android.server.wm.CompatChangeTests$SupportsSizeChangesPortraitActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v1, "android.server.wm.CompatChangeTests$NonResizeablePortraitActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    const-string v1, "com.android.server.cts.device.statsdatom.StatsdCtsNonResizeablePortraitActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.miui.securitycenter"

    const-string v2, "com.xiaomi.market"

    const-string v3, "com.google.android.calendar"

    filled-new-array {v3, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 113
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/server/wm/ActivityRecordImpl;->sStartWindowBlackList:Ljava/util/List;

    .line 937
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/ActivityRecordImpl;->SKIP_RELAUNCH_ACTIVITY_NAMES:Ljava/util/List;

    .line 938
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/ActivityRecordImpl;->FORCE_SKIP_RELAUNCH_ACTIVITY_NAMES:Ljava/util/List;

    .line 941
    sget-object v0, Lcom/android/server/wm/ActivityRecordImpl;->SKIP_RELAUNCH_ACTIVITY_NAMES:Ljava/util/List;

    const-string v1, "com.miui.child.home.home.MainActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 942
    sget-object v0, Lcom/android/server/wm/ActivityRecordImpl;->SKIP_RELAUNCH_ACTIVITY_NAMES:Ljava/util/List;

    const-string v1, "com.android.packageinstaller.permission.ui.GrantPermissionsActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 943
    sget-object v0, Lcom/android/server/wm/ActivityRecordImpl;->FORCE_SKIP_RELAUNCH_ACTIVITY_NAMES:Ljava/util/List;

    const-string v1, "com.android.provision.activities.DefaultActivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 944
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    return-void
.end method

.method private checkSlow(JJLjava/lang/String;)V
    .locals 4
    .param p1, "startTime"    # J
    .param p3, "threshold"    # J
    .param p5, "where"    # Ljava/lang/String;

    .line 870
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 871
    .local v0, "took":J
    cmp-long v2, v0, p3

    if-lez v2, :cond_0

    .line 873
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Slow operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " took "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ActivityRecordImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    :cond_0
    return-void
.end method

.method static getInstance()Lcom/android/server/wm/ActivityRecordImpl;
    .locals 1

    .line 116
    invoke-static {}, Lcom/android/server/wm/ActivityRecordStub;->get()Lcom/android/server/wm/ActivityRecordStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityRecordImpl;

    return-object v0
.end method


# virtual methods
.method public addThemeFlag(Ljava/lang/String;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 400
    const-string v0, "com.android.settings"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x400000

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public adjustSortIfNeeded(Ljava/util/ArrayList;Landroid/util/ArrayMap;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/wm/Transition$ChangeInfo;",
            ">;",
            "Landroid/util/ArrayMap<",
            "Lcom/android/server/wm/WindowContainer;",
            "Lcom/android/server/wm/Transition$ChangeInfo;",
            ">;)V"
        }
    .end annotation

    .line 835
    .local p1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/Transition$ChangeInfo;>;"
    .local p2, "changes":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Lcom/android/server/wm/WindowContainer;Lcom/android/server/wm/Transition$ChangeInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 836
    .local v0, "targetActivityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/wm/ActivityRecord;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 837
    .local v1, "needSortIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ge v2, v3, :cond_4

    .line 838
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/Transition$ChangeInfo;

    .line 839
    .local v3, "changeInfo":Lcom/android/server/wm/Transition$ChangeInfo;
    const/4 v5, 0x0

    .line 840
    .local v5, "windowContainer":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {p2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/wm/WindowContainer;

    .line 841
    .local v7, "wc":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {p2, v7}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/wm/Transition$ChangeInfo;

    .line 842
    .local v8, "curChangeInfo":Lcom/android/server/wm/Transition$ChangeInfo;
    if-eqz v8, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 843
    move-object v5, v7

    .line 844
    goto :goto_2

    .line 846
    .end local v7    # "wc":Lcom/android/server/wm/WindowContainer;
    .end local v8    # "curChangeInfo":Lcom/android/server/wm/Transition$ChangeInfo;
    :cond_0
    goto :goto_1

    .line 847
    :cond_1
    :goto_2
    if-eqz v5, :cond_2

    invoke-virtual {v5, v4, v4}, Lcom/android/server/wm/WindowContainer;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    goto :goto_3

    :cond_2
    const/4 v4, 0x0

    .line 848
    .local v4, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    :goto_3
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 849
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->fillsParent()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->isFinishing()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, v4, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v6}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isSplitMode()Z

    move-result v6

    if-nez v6, :cond_3

    .line 850
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 837
    .end local v3    # "changeInfo":Lcom/android/server/wm/Transition$ChangeInfo;
    .end local v4    # "activityRecord":Lcom/android/server/wm/ActivityRecord;
    .end local v5    # "windowContainer":Lcom/android/server/wm/WindowContainer;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 853
    .end local v2    # "i":I
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_8

    .line 854
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 855
    .local v3, "index":I
    if-le v3, v4, :cond_7

    .line 856
    add-int/lit8 v5, v3, -0x1

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/Transition$ChangeInfo;

    .line 857
    .local v5, "info":Lcom/android/server/wm/Transition$ChangeInfo;
    if-eqz v5, :cond_5

    iget-object v6, v5, Lcom/android/server/wm/Transition$ChangeInfo;->mContainer:Lcom/android/server/wm/WindowContainer;

    invoke-virtual {v5, v6}, Lcom/android/server/wm/Transition$ChangeInfo;->getTransitMode(Lcom/android/server/wm/WindowContainer;)I

    move-result v6

    goto :goto_5

    :cond_5
    const/4 v6, -0x1

    .line 858
    .local v6, "mode":I
    :goto_5
    if-eq v6, v4, :cond_7

    const/4 v7, 0x3

    if-ne v6, v7, :cond_6

    .line 859
    goto :goto_6

    .line 861
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Adjust the order of animation targets, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " need to higher level,"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v8, v3, -0x1

    .line 862
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " need  to lower level."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 861
    const-string v8, "ActivityRecordImpl"

    invoke-static {v8, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/wm/Transition$ChangeInfo;

    .line 864
    .local v7, "tempInfo":Lcom/android/server/wm/Transition$ChangeInfo;
    add-int/lit8 v8, v3, -0x1

    invoke-virtual {p1, v8, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 853
    .end local v3    # "index":I
    .end local v5    # "info":Lcom/android/server/wm/Transition$ChangeInfo;
    .end local v6    # "mode":I
    .end local v7    # "tempInfo":Lcom/android/server/wm/Transition$ChangeInfo;
    :cond_7
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 867
    .end local v2    # "i":I
    :cond_8
    return-void
.end method

.method public allowShowSlpha(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 518
    invoke-static {p1}, Lcom/android/server/wm/AppTransitionInjector;->ignoreLaunchedFromSystemSurface(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    const/4 v0, 0x0

    return v0

    .line 521
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public allowTaskSnapshot(Ljava/lang/String;ZLandroid/window/TaskSnapshot;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 13
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "allowTaskSnapshot"    # Z
    .param p3, "snapshot"    # Landroid/window/TaskSnapshot;
    .param p4, "mActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 443
    move-object v0, p1

    invoke-static {p1}, Lcom/android/server/wm/AppTransitionInjector;->disableSnapshot(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 444
    return v2

    .line 446
    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    invoke-static {p1, v1}, Lcom/android/server/wm/AppTransitionInjector;->disableSnapshotForApplock(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 447
    return v2

    .line 449
    :cond_1
    invoke-static/range {p4 .. p4}, Lcom/android/server/wm/AppTransitionInjector;->disableSnapshotByComponent(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 450
    return v2

    .line 452
    :cond_2
    if-nez p3, :cond_3

    .line 453
    return p2

    .line 455
    :cond_3
    invoke-virtual/range {p3 .. p3}, Landroid/window/TaskSnapshot;->getWindowingMode()I

    move-result v1

    const/4 v3, 0x5

    if-ne v1, v3, :cond_4

    .line 456
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v1

    if-nez v1, :cond_4

    .line 457
    return v2

    .line 460
    :cond_4
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/ActivityRecord;->findMainWindow()Lcom/android/server/wm/WindowState;

    move-result-object v1

    .line 461
    .local v1, "mainWin":Lcom/android/server/wm/WindowState;
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isSecureLocked()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 462
    return v2

    .line 465
    :cond_5
    invoke-static {}, Lcom/android/server/wm/MiuiFreezeStub;->getInstance()Lcom/android/server/wm/MiuiFreezeStub;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/android/server/wm/MiuiFreezeStub;->needShowLoading(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 466
    return p2

    .line 469
    :cond_6
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/ActivityRecord;->isLetterboxedForFixedOrientationAndAspectRatio()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 470
    return v2

    .line 473
    :cond_7
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v3

    invoke-interface {v3}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 474
    return v2

    .line 477
    :cond_8
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    if-nez v3, :cond_9

    .line 478
    return p2

    .line 480
    :cond_9
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 481
    .local v3, "taskWidth":I
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    .line 482
    .local v4, "taskHeight":I
    invoke-virtual/range {p3 .. p3}, Landroid/window/TaskSnapshot;->getSnapshot()Landroid/graphics/GraphicBuffer;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/GraphicBuffer;->getWidth()I

    move-result v5

    .line 483
    .local v5, "snapshotWidth":I
    invoke-virtual/range {p3 .. p3}, Landroid/window/TaskSnapshot;->getSnapshot()Landroid/graphics/GraphicBuffer;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/GraphicBuffer;->getHeight()I

    move-result v6

    .line 484
    .local v6, "snapshotHeight":I
    if-eqz v4, :cond_d

    if-nez v6, :cond_a

    move-object/from16 v10, p4

    goto/16 :goto_1

    .line 488
    :cond_a
    int-to-double v7, v3

    int-to-double v9, v4

    div-double/2addr v7, v9

    int-to-double v9, v5

    int-to-double v11, v6

    div-double/2addr v9, v11

    sub-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    move-result-wide v7

    .line 489
    .local v7, "difference":D
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/server/wm/DisplayContent;->getRotation()I

    move-result v9

    invoke-virtual/range {p3 .. p3}, Landroid/window/TaskSnapshot;->getRotation()I

    move-result v10

    if-ne v9, v10, :cond_c

    const-wide v9, 0x3fb999999999999aL    # 0.1

    cmpl-double v9, v7, v9

    if-lez v9, :cond_c

    .line 490
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v9

    if-nez v9, :cond_b

    .line 491
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Task bounds and snapshot don\'t match. task width="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", height="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": snapshot w="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", h="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", difference = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", name = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v10, p4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v11, "ActivityRecordImpl"

    invoke-static {v11, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    return v2

    .line 490
    :cond_b
    move-object/from16 v10, p4

    goto :goto_0

    .line 489
    :cond_c
    move-object/from16 v10, p4

    .line 497
    :goto_0
    return p2

    .line 484
    .end local v7    # "difference":D
    :cond_d
    move-object/from16 v10, p4

    .line 485
    :goto_1
    return p2
.end method

.method public avoidWhiteScreen(Ljava/lang/String;ILjava/lang/String;Lcom/android/server/wm/Task;ZIILcom/android/server/wm/ActivityRecord;)I
    .locals 15
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "resolvedTheme"    # I
    .param p3, "taskAffinity"    # Ljava/lang/String;
    .param p4, "task"    # Lcom/android/server/wm/Task;
    .param p5, "taskSwitch"    # Z
    .param p6, "theme"    # I
    .param p7, "mUserId"    # I
    .param p8, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 629
    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    const/4 v0, 0x0

    .line 630
    .local v0, "sameAffinity":Z
    move/from16 v4, p2

    .line 631
    .local v4, "overrideTheme":I
    const/4 v5, 0x1

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    iget-object v6, v3, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 632
    invoke-virtual/range {p8 .. p8}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I

    move-result v6

    if-ne v6, v5, :cond_0

    .line 633
    const/4 v0, 0x1

    move v6, v0

    goto :goto_0

    .line 635
    :cond_0
    move v6, v0

    .end local v0    # "sameAffinity":Z
    .local v6, "sameAffinity":Z
    :goto_0
    if-eqz p5, :cond_3

    if-eqz v6, :cond_3

    sget-object v0, Lcom/android/server/wm/ActivityRecordImpl;->sStartWindowBlackList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 636
    new-instance v0, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v7, v0

    .line 637
    .local v7, "launcherIntent":Landroid/content/Intent;
    invoke-virtual {v7, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 638
    const-string v0, "android.intent.category.LAUNCHER"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 639
    const/4 v14, 0x0

    .line 641
    .local v14, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v8

    const/4 v10, 0x0

    const-wide/16 v11, 0x1

    move-object v9, v7

    move/from16 v13, p7

    invoke-interface/range {v8 .. v13}, Landroid/content/pm/IPackageManager;->queryIntentActivities(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ParceledListSlice;

    move-result-object v0

    .line 642
    invoke-virtual {v0}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    .end local v14    # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .local v0, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    goto :goto_1

    .line 643
    .end local v0    # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v14    # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :catch_0
    move-exception v0

    .line 644
    .local v0, "e":Landroid/os/RemoteException;
    const-string v8, "ActivityRecordImpl"

    const-string v9, "RemoteException when queryIntentActivities"

    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v8

    .line 648
    .end local v14    # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .local v0, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    if-ne v8, v5, :cond_2

    .line 649
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 650
    .local v5, "ri":Landroid/content/pm/ResolveInfo;
    if-eqz v5, :cond_1

    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v8, :cond_1

    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v8, v8, Landroid/content/pm/ActivityInfo;->theme:I

    if-eqz v8, :cond_1

    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v8, v8, Landroid/content/pm/ActivityInfo;->theme:I

    move/from16 v9, p6

    if-eq v8, v9, :cond_4

    .line 652
    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v4, v8, Landroid/content/pm/ActivityInfo;->theme:I

    goto :goto_2

    .line 650
    :cond_1
    move/from16 v9, p6

    goto :goto_2

    .line 648
    .end local v5    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_2
    move/from16 v9, p6

    goto :goto_2

    .line 635
    .end local v0    # "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v7    # "launcherIntent":Landroid/content/Intent;
    :cond_3
    move/from16 v9, p6

    .line 656
    :cond_4
    :goto_2
    return v4
.end method

.method public canShowWhenLocked(Landroid/app/AppOpsManager;ILjava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "appOpsManager"    # Landroid/app/AppOpsManager;
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "extra"    # Ljava/lang/String;

    .line 405
    const/16 v1, 0x2724

    const/4 v4, 0x0

    const-string v5, "ActivityRecordImpl#canShowWhenLocked"

    move-object v0, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 408
    .local v0, "mode":I
    iget-object v1, p0, Lcom/android/server/wm/ActivityRecordImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v1, :cond_0

    .line 409
    const-class v1, Lmiui/security/SecurityManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/security/SecurityManagerInternal;

    iput-object v1, p0, Lcom/android/server/wm/ActivityRecordImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    .line 411
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/ActivityRecordImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-eqz v2, :cond_1

    .line 412
    const/16 v3, 0x24

    const-wide/16 v5, 0x1

    move-object v4, p3

    move-object v7, p4

    invoke-virtual/range {v2 .. v7}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    .line 415
    :cond_1
    if-eqz v0, :cond_2

    .line 416
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MIUILOG- Show when locked PermissionDenied pkg : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActivityRecordImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const/4 v1, 0x0

    return v1

    .line 419
    :cond_2
    const/4 v1, 0x1

    return v1
.end method

.method public canStartCollectingNow(ILcom/android/server/wm/Transition;ILandroid/app/ActivityOptions;Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 15
    .param p1, "type"    # I
    .param p2, "collectingTransition"    # Lcom/android/server/wm/Transition;
    .param p3, "realCallingUid"    # I
    .param p4, "options"    # Landroid/app/ActivityOptions;
    .param p5, "atm"    # Lcom/android/server/wm/ActivityTaskManagerService;
    .param p6, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 880
    move/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p6

    const v3, 0x7fffffeb

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-ne v0, v3, :cond_2

    .line 881
    iget v3, v1, Lcom/android/server/wm/Transition;->mType:I

    const/16 v6, 0xc

    if-ne v3, v6, :cond_0

    .line 882
    return v5

    .line 885
    :cond_0
    iget v3, v1, Lcom/android/server/wm/Transition;->mType:I

    if-ne v3, v4, :cond_2

    .line 886
    iget-object v3, v1, Lcom/android/server/wm/Transition;->mParticipants:Landroid/util/ArraySet;

    invoke-virtual {v3}, Landroid/util/ArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 887
    iget-object v3, v1, Lcom/android/server/wm/Transition;->mParticipants:Landroid/util/ArraySet;

    invoke-virtual {v3}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wm/WindowContainer;

    .line 888
    .local v6, "wc":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v7

    .line 889
    .local v7, "ar":Lcom/android/server/wm/ActivityRecord;
    if-eqz v7, :cond_1

    iget-object v8, v7, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v8}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->isKeyguardEditor()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 890
    return v5

    .line 892
    .end local v6    # "wc":Lcom/android/server/wm/WindowContainer;
    .end local v7    # "ar":Lcom/android/server/wm/ActivityRecord;
    :cond_1
    goto :goto_0

    .line 897
    :cond_2
    const-string v3, "ActivityRecordImpl"

    const/4 v6, 0x3

    const/4 v7, 0x0

    if-eq v0, v5, :cond_4

    if-ne v0, v6, :cond_3

    goto :goto_1

    :cond_3
    move/from16 v10, p3

    move-object/from16 v6, p5

    goto :goto_3

    :cond_4
    :goto_1
    iget v8, v1, Lcom/android/server/wm/Transition;->mType:I

    if-eq v8, v5, :cond_5

    iget v8, v1, Lcom/android/server/wm/Transition;->mType:I

    if-ne v8, v6, :cond_3

    .line 899
    :cond_5
    move-object/from16 v6, p5

    iget-object v8, v6, Lcom/android/server/wm/ActivityTaskManagerService;->mHomeProcess:Lcom/android/server/wm/WindowProcessController;

    .line 901
    .local v8, "homeProcess":Lcom/android/server/wm/WindowProcessController;
    if-eqz v8, :cond_9

    iget v9, v8, Lcom/android/server/wm/WindowProcessController;->mUid:I

    move/from16 v10, p3

    if-ne v9, v10, :cond_a

    if-eqz p4, :cond_a

    .line 902
    invoke-virtual/range {p4 .. p4}, Landroid/app/ActivityOptions;->getRemoteTransition()Landroid/window/RemoteTransition;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 903
    iget-object v9, v1, Lcom/android/server/wm/Transition;->mParticipants:Landroid/util/ArraySet;

    if-eqz v9, :cond_8

    iget-object v9, v1, Lcom/android/server/wm/Transition;->mParticipants:Landroid/util/ArraySet;

    .line 904
    invoke-virtual {v9}, Landroid/util/ArraySet;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_8

    .line 905
    iget-object v9, v1, Lcom/android/server/wm/Transition;->mParticipants:Landroid/util/ArraySet;

    invoke-virtual {v9}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/server/wm/WindowContainer;

    .line 906
    .local v11, "wc":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {v11}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v12

    .line 907
    .local v12, "ar":Lcom/android/server/wm/ActivityRecord;
    if-eqz v12, :cond_6

    invoke-virtual {v12, v4}, Lcom/android/server/wm/ActivityRecord;->isLaunchSourceType(I)Z

    move-result v13

    if-eqz v13, :cond_6

    if-eqz v2, :cond_6

    iget-object v13, v2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    if-eqz v13, :cond_6

    iget-object v13, v2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget-object v14, v12, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 909
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 910
    return v7

    .line 912
    .end local v11    # "wc":Lcom/android/server/wm/WindowContainer;
    .end local v12    # "ar":Lcom/android/server/wm/ActivityRecord;
    :cond_6
    goto :goto_2

    .line 913
    :cond_7
    invoke-virtual/range {p6 .. p6}, Lcom/android/server/wm/ActivityRecord;->isActivityTypeHomeOrRecents()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 914
    return v7

    .line 917
    :cond_8
    const-string v4, "force create transition."

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    return v5

    .line 901
    :cond_9
    move/from16 v10, p3

    .line 921
    .end local v8    # "homeProcess":Lcom/android/server/wm/WindowProcessController;
    :cond_a
    :goto_3
    if-ne v0, v5, :cond_c

    iget v8, v1, Lcom/android/server/wm/Transition;->mType:I

    if-ne v8, v4, :cond_c

    .line 922
    iget-object v8, v1, Lcom/android/server/wm/Transition;->mParticipants:Landroid/util/ArraySet;

    invoke-virtual {v8}, Landroid/util/ArraySet;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_c

    .line 923
    iget-object v8, v1, Lcom/android/server/wm/Transition;->mParticipants:Landroid/util/ArraySet;

    invoke-virtual {v8}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/wm/WindowContainer;

    .line 924
    .local v9, "wc":Lcom/android/server/wm/WindowContainer;
    invoke-virtual {v9}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v11

    .line 925
    .local v11, "ar":Lcom/android/server/wm/ActivityRecord;
    if-eqz v11, :cond_b

    iget-boolean v12, v11, Lcom/android/server/wm/ActivityRecord;->finishing:Z

    if-eqz v12, :cond_b

    invoke-virtual {v11, v4}, Lcom/android/server/wm/ActivityRecord;->isLaunchSourceType(I)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 926
    invoke-virtual {v11, v5}, Lcom/android/server/wm/ActivityRecord;->occludesParent(Z)Z

    move-result v12

    if-nez v12, :cond_b

    if-eqz v2, :cond_b

    invoke-virtual/range {p6 .. p6}, Lcom/android/server/wm/ActivityRecord;->occludesParent()Z

    move-result v12

    if-eqz v12, :cond_b

    .line 927
    const-string v4, "Finish translucent activity start from home , force create open transition."

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 928
    return v5

    .line 930
    .end local v9    # "wc":Lcom/android/server/wm/WindowContainer;
    .end local v11    # "ar":Lcom/android/server/wm/ActivityRecord;
    :cond_b
    goto :goto_4

    .line 933
    :cond_c
    return v7
.end method

.method public computeHoverModeBounds(Landroid/content/res/Configuration;Landroid/graphics/Rect;Landroid/graphics/Rect;Lcom/android/server/wm/ActivityRecord;)V
    .locals 2
    .param p1, "newParentConfiguration"    # Landroid/content/res/Configuration;
    .param p2, "parentBounds"    # Landroid/graphics/Rect;
    .param p3, "mTmpBounds"    # Landroid/graphics/Rect;
    .param p4, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 702
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 703
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 704
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 705
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiHoverModeInternal;->computeHoverModeBounds(Landroid/content/res/Configuration;Landroid/graphics/Rect;Landroid/graphics/Rect;Lcom/android/server/wm/ActivityRecord;)V

    .line 708
    :cond_0
    return-void
.end method

.method public disableSnapshotForApplock(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 660
    invoke-static {p1, p2}, Lcom/android/server/wm/AppTransitionInjector;->disableSnapshotForApplock(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public disableSplashScreen(Ljava/lang/String;)Z
    .locals 1
    .param p1, "componentName"    # Ljava/lang/String;

    .line 501
    sget-object v0, Lcom/android/server/wm/ActivityRecordImpl;->BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 502
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isFlipFolded()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 506
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 503
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public disableSplashScreenForSplitScreen(Ljava/lang/String;)Z
    .locals 1
    .param p1, "componentName"    # Ljava/lang/String;

    .line 510
    sget-object v0, Lcom/android/server/wm/ActivityRecordImpl;->BLACK_LIST_NOT_ALLOWED_SPLASHSCREEN_IN_SPLIT_MODE:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    const/4 v0, 0x1

    return v0

    .line 514
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public enterFreeformForHoverMode(Lcom/android/server/wm/Task;Z)V
    .locals 2
    .param p1, "task"    # Lcom/android/server/wm/Task;
    .param p2, "enter"    # Z

    .line 778
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 779
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 780
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 781
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->enterFreeformForHoverMode(Lcom/android/server/wm/Task;Z)V

    .line 783
    :cond_0
    return-void
.end method

.method public getClassName(Lcom/android/server/wm/ActivityRecord;)Ljava/lang/String;
    .locals 1
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 541
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 544
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getOptionsStyle(Landroid/app/ActivityOptions;Z)I
    .locals 2
    .param p1, "options"    # Landroid/app/ActivityOptions;
    .param p2, "isLaunchSourceType"    # Z

    .line 664
    invoke-virtual {p1}, Landroid/app/ActivityOptions;->getSplashScreenStyle()I

    move-result v0

    .line 665
    .local v0, "optionStyle":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    if-nez p2, :cond_0

    .line 666
    const/4 v0, 0x0

    .line 668
    :cond_0
    return v0
.end method

.method public getOwnerPackageName(Lcom/android/server/wm/Task;)Ljava/lang/String;
    .locals 9
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 579
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 580
    .local v0, "resumedActivity":Lcom/android/server/wm/ActivityRecord;
    const-string v1, " ownerPackageName = "

    const-string v2, "displayId = "

    const-string v3, "ActivityRecordImpl"

    if-eqz v0, :cond_0

    .line 581
    iget-object v4, v0, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 582
    .local v4, "display":Lcom/android/server/wm/DisplayContent;
    if-eqz v4, :cond_0

    .line 583
    iget-object v5, v4, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget-object v5, v5, Landroid/view/DisplayInfo;->ownerPackageName:Ljava/lang/String;

    .line 584
    .local v5, "ownerPackageName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    return-object v5

    .line 588
    .end local v4    # "display":Lcom/android/server/wm/DisplayContent;
    .end local v5    # "ownerPackageName":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    .line 589
    .local v4, "rootActivity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v4, :cond_1

    .line 590
    iget-object v5, v4, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 591
    .local v5, "display":Lcom/android/server/wm/DisplayContent;
    if-eqz v5, :cond_1

    .line 592
    iget-object v6, v5, Lcom/android/server/wm/DisplayContent;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget-object v6, v6, Landroid/view/DisplayInfo;->ownerPackageName:Ljava/lang/String;

    .line 593
    .local v6, "ownerPackageName":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "rootActivity = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 594
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 593
    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    return-object v6

    .line 598
    .end local v5    # "display":Lcom/android/server/wm/DisplayContent;
    .end local v6    # "ownerPackageName":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    return-object v1
.end method

.method public initSplitMode(Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;)Z
    .locals 5
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 549
    invoke-virtual {p2}, Landroid/content/Intent;->isWebIntent()Z

    move-result v0

    const/16 v1, 0x8

    if-eqz v0, :cond_0

    .line 550
    invoke-virtual {p2, v1}, Landroid/content/Intent;->addMiuiFlags(I)Landroid/content/Intent;

    .line 552
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getMiuiFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    .line 553
    invoke-virtual {p2}, Landroid/content/Intent;->getMiuiFlags()I

    move-result v0

    and-int/2addr v0, v1

    if-nez v0, :cond_1

    move v0, v3

    goto :goto_0

    :cond_1
    move v0, v2

    .line 554
    .local v0, "isSplitMode":Z
    :goto_0
    if-eqz v0, :cond_5

    .line 555
    invoke-virtual {p1, v3}, Lcom/android/server/wm/ActivityRecord;->occludesParent(Z)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 556
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v1}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    move v2, v3

    :cond_3
    invoke-virtual {p1, v2}, Lcom/android/server/wm/ActivityRecord;->setOccludesParent(Z)Z

    goto :goto_1

    .line 557
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getMiuiFlags()I

    move-result v2

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_5

    .line 558
    invoke-virtual {p2, v1}, Landroid/content/Intent;->addMiuiFlags(I)Landroid/content/Intent;

    .line 559
    const/4 v0, 0x0

    .line 562
    :cond_5
    :goto_1
    return v0
.end method

.method public isCompatibilityMode()Z
    .locals 2

    .line 532
    nop

    .line 533
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 532
    xor-int/lit8 v0, v0, 0x1

    const-string v1, "persist.sys.miui_optimization"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isInCompatWhiteList(Landroid/content/pm/ActivityInfo;)Z
    .locals 2
    .param p1, "info"    # Landroid/content/pm/ActivityInfo;

    .line 954
    sget-object v0, Lcom/android/server/wm/ActivityRecordImpl;->COMPAT_WHITE_LIST:Ljava/util/List;

    iget-object v1, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMiuiMwAnimationBelowStack(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p1, "activity"    # Lcom/android/server/wm/ActivityRecord;

    .line 537
    const/4 v0, 0x0

    return v0
.end method

.method public isSecSplit(ZLandroid/content/Intent;)Z
    .locals 1
    .param p1, "isSplitMode"    # Z
    .param p2, "intent"    # Landroid/content/Intent;

    .line 567
    if-eqz p1, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getMiuiFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isWorldCirculate(Lcom/android/server/wm/Task;)Z
    .locals 4
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 571
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 572
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I

    move-result v1

    .line 573
    .local v1, "displayId":I
    if-eqz v1, :cond_0

    const-string v2, "com.xiaomi.mirror"

    invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityRecordImpl;->getOwnerPackageName(Lcom/android/server/wm/Task;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 575
    .end local v1    # "displayId":I
    :cond_1
    return v0
.end method

.method public miuiFullscreen(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 525
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    const/4 v0, 0x0

    return v0

    .line 528
    :cond_0
    invoke-static {p1}, Lmiui/os/MiuiInit;->isRestrictAspect(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public needSkipRelaunch(Ljava/lang/String;II)Z
    .locals 1
    .param p1, "activityName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "changes"    # I

    .line 947
    sget-object v0, Lcom/android/server/wm/ActivityRecordImpl;->SKIP_RELAUNCH_ACTIVITY_NAMES:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/wm/ActivityRecordImpl;->FORCE_SKIP_RELAUNCH_ACTIVITY_NAMES:Ljava/util/List;

    .line 948
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/high16 v0, -0x80000000

    and-int/2addr v0, p3

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 947
    :goto_0
    return v0
.end method

.method public notifyAppResumedFinished(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 424
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mChildren:Lcom/android/server/wm/WindowList;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/DisplayContent;->mUnknownAppVisibilityController:Lcom/android/server/wm/UnknownAppVisibilityController;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/UnknownAppVisibilityController;->appRemovedOrHidden(Lcom/android/server/wm/ActivityRecord;)V

    goto :goto_0

    .line 427
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/DisplayContent;->mUnknownAppVisibilityController:Lcom/android/server/wm/UnknownAppVisibilityController;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/UnknownAppVisibilityController;->notifyAppResumedFinished(Lcom/android/server/wm/ActivityRecord;)V

    .line 429
    :goto_0
    return-void
.end method

.method public onArCreated(Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 1
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "isUnStandardLaunchMode"    # Z

    .line 693
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 694
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 695
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    .line 696
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onArCreated(Lcom/android/server/wm/ActivityRecord;Z)V

    .line 698
    :cond_0
    return-void
.end method

.method public onArParentChanged(Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/ActivityRecord;)V
    .locals 2
    .param p1, "oldParent"    # Lcom/android/server/wm/TaskFragment;
    .param p2, "newParent"    # Lcom/android/server/wm/TaskFragment;
    .param p3, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 755
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 756
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 757
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 758
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/MiuiHoverModeInternal;->onArParentChanged(Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/TaskFragment;Lcom/android/server/wm/ActivityRecord;)V

    .line 760
    :cond_0
    return-void
.end method

.method public onArStateChanged(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;)V
    .locals 9
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "state"    # Lcom/android/server/wm/ActivityRecord$State;

    .line 763
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 764
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 765
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 766
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onArStateChanged(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;)V

    .line 769
    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    if-eqz v1, :cond_1

    .line 770
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 771
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->processName:Ljava/lang/String;

    .line 772
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord$State;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    filled-new-array/range {v2 .. v8}, [Ljava/lang/Object;

    move-result-object v1

    .line 770
    const-string v2, "notifyActivityStateChange"

    invoke-static {v2, v1}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 775
    :cond_1
    return-void
.end method

.method public onArVisibleChanged(Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 2
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "visible"    # Z

    .line 720
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 721
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 722
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 723
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->onArVisibleChanged(Lcom/android/server/wm/ActivityRecord;Z)V

    .line 725
    :cond_0
    return-void
.end method

.method public onWindowsVisible(Lcom/android/server/wm/ActivityRecord;)V
    .locals 2
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 603
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v0

    .line 604
    .local v0, "atmsImpl":Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMiuiSizeCompatIn()Lcom/android/server/wm/MiuiSizeCompatInternal;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 605
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMiuiSizeCompatIn()Lcom/android/server/wm/MiuiSizeCompatInternal;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->showWarningNotification(Lcom/android/server/wm/ActivityRecord;)V

    .line 607
    :cond_0
    return-void
.end method

.method public resetTaskFragmentWindowModeForHoverMode(Lcom/android/server/wm/TaskFragment;Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "tf"    # Lcom/android/server/wm/TaskFragment;
    .param p2, "newParentConfiguration"    # Landroid/content/res/Configuration;

    .line 738
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 739
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 740
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 741
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->resetTaskFragmentRequestWindowMode(Lcom/android/server/wm/TaskFragment;Landroid/content/res/Configuration;)V

    .line 743
    :cond_0
    return-void
.end method

.method public setRequestedOrientationForHover(Lcom/android/server/wm/ActivityRecord;II)V
    .locals 2
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "requestOrientation"    # I
    .param p3, "orientation"    # I

    .line 746
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 747
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 748
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 749
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/MiuiHoverModeInternal;->setOrientationForHoverMode(Lcom/android/server/wm/ActivityRecord;II)V

    .line 751
    :cond_0
    return-void
.end method

.method public setRequestedWindowModeForHoverMode(Lcom/android/server/wm/ActivityRecord;Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "newParentConfiguration"    # Landroid/content/res/Configuration;

    .line 729
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 730
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 731
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 732
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->setRequestedWindowModeForHoverMode(Lcom/android/server/wm/ActivityRecord;Landroid/content/res/Configuration;)V

    .line 734
    :cond_0
    return-void
.end method

.method public shouldClearActivityInfoFlags()I
    .locals 1

    .line 625
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->shouldClearActivityInfoFlags()I

    move-result v0

    return v0
.end method

.method public shouldHookHoverConfig(Landroid/content/res/Configuration;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "newParentConfiguration"    # Landroid/content/res/Configuration;
    .param p2, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 711
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 712
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 713
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiHoverModeInternal;->isDeviceInOpenedOrHalfOpenedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 714
    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiHoverModeInternal;->shouldHookHoverConfig(Landroid/content/res/Configuration;Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    return v1

    .line 716
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public shouldNotBeResume(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 433
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    .line 435
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayId()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 436
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayContent()Lcom/android/server/wm/DisplayContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->isSleeping()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 437
    const-string v1, "com.xiaomi.misubscreenui"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 433
    :goto_0
    return v0
.end method

.method public shouldRelaunchForBlur(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 11
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 787
    const-string v0, "miui.blurRelaunch"

    const/4 v1, 0x0

    if-eqz p1, :cond_5

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    if-nez v2, :cond_0

    goto :goto_2

    .line 790
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 791
    .local v9, "startTime":J
    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    .line 793
    .local v2, "pm":Landroid/content/pm/IPackageManager;
    :try_start_0
    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget v4, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    const/4 v5, 0x0

    invoke-interface {v2, v0, v3, v5, v4}, Landroid/content/pm/IPackageManager;->getPropertyAsUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/pm/PackageManager$Property;

    move-result-object v3

    .line 795
    .local v3, "appProp":Landroid/content/pm/PackageManager$Property;
    const/4 v4, 0x1

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v4

    goto :goto_0

    :cond_1
    move v5, v1

    .line 796
    .local v5, "appBlur":Z
    :goto_0
    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 797
    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v7

    iget v8, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    .line 796
    invoke-interface {v2, v0, v6, v7, v8}, Landroid/content/pm/IPackageManager;->getPropertyAsUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/pm/PackageManager$Property;

    move-result-object v0

    .line 798
    .local v0, "actProp":Landroid/content/pm/PackageManager$Property;
    if-eqz v5, :cond_3

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z

    move-result v6

    if-nez v6, :cond_3

    .line 799
    :cond_2
    return v4

    .line 801
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z

    move-result v6
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v6, :cond_4

    .line 802
    return v4

    .line 806
    .end local v0    # "actProp":Landroid/content/pm/PackageManager$Property;
    .end local v3    # "appProp":Landroid/content/pm/PackageManager$Property;
    .end local v5    # "appBlur":Z
    :cond_4
    goto :goto_1

    .line 804
    :catch_0
    move-exception v0

    .line 805
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ActivityRecordImpl"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    const-wide/16 v6, 0x32

    const-string/jumbo v8, "shouldRelaunchForBlur"

    move-object v3, p0

    move-wide v4, v9

    invoke-direct/range {v3 .. v8}, Lcom/android/server/wm/ActivityRecordImpl;->checkSlow(JJLjava/lang/String;)V

    .line 808
    return v1

    .line 788
    .end local v2    # "pm":Landroid/content/pm/IPackageManager;
    .end local v9    # "startTime":J
    :cond_5
    :goto_2
    return v1
.end method

.method public shouldSkipChanged(Lcom/android/server/wm/ActivityRecord;I)Z
    .locals 11
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "changes"    # I

    .line 813
    const-string v0, "miui.blurChanged"

    const/4 v1, 0x0

    if-eqz p1, :cond_6

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    if-eqz v2, :cond_6

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    if-eqz v2, :cond_6

    const/high16 v2, 0x100000

    and-int/2addr v2, p2

    if-nez v2, :cond_0

    goto/16 :goto_4

    .line 817
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 818
    .local v9, "startTime":J
    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    .line 820
    .local v2, "pm":Landroid/content/pm/IPackageManager;
    :try_start_0
    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget v4, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    const/4 v5, 0x0

    invoke-interface {v2, v0, v3, v5, v4}, Landroid/content/pm/IPackageManager;->getPropertyAsUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/pm/PackageManager$Property;

    move-result-object v3

    .line 822
    .local v3, "appProp":Landroid/content/pm/PackageManager$Property;
    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z

    move-result v5

    if-nez v5, :cond_1

    goto :goto_0

    :cond_1
    move v5, v1

    goto :goto_1

    :cond_2
    :goto_0
    move v5, v4

    .line 823
    .local v5, "appPropFalse":Z
    :goto_1
    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 824
    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v7

    iget v8, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I

    .line 823
    invoke-interface {v2, v0, v6, v7, v8}, Landroid/content/pm/IPackageManager;->getPropertyAsUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/pm/PackageManager$Property;

    move-result-object v0

    .line 825
    .local v0, "actProp":Landroid/content/pm/PackageManager$Property;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->isBoolean()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z

    move-result v6
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v6, :cond_3

    goto :goto_2

    :cond_3
    move v6, v1

    goto :goto_3

    :cond_4
    :goto_2
    move v6, v4

    .line 826
    .local v6, "actPropFalse":Z
    :goto_3
    if-eqz v5, :cond_5

    if-eqz v6, :cond_5

    move v1, v4

    :cond_5
    return v1

    .line 827
    .end local v0    # "actProp":Landroid/content/pm/PackageManager$Property;
    .end local v3    # "appProp":Landroid/content/pm/PackageManager$Property;
    .end local v5    # "appPropFalse":Z
    .end local v6    # "actPropFalse":Z
    :catch_0
    move-exception v0

    .line 828
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ActivityRecordImpl"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    .end local v0    # "e":Ljava/lang/Exception;
    const-wide/16 v6, 0x32

    const-string/jumbo v8, "shouldSkipChanged"

    move-object v3, p0

    move-wide v4, v9

    invoke-direct/range {v3 .. v8}, Lcom/android/server/wm/ActivityRecordImpl;->checkSlow(JJLjava/lang/String;)V

    .line 831
    return v1

    .line 815
    .end local v2    # "pm":Landroid/content/pm/IPackageManager;
    .end local v9    # "startTime":J
    :cond_6
    :goto_4
    return v1
.end method

.method public shouldSkipCompatMode(Lcom/android/server/wm/WindowContainer;Lcom/android/server/wm/WindowContainer;)Z
    .locals 2
    .param p1, "oldParent"    # Lcom/android/server/wm/WindowContainer;
    .param p2, "newParent"    # Lcom/android/server/wm/WindowContainer;

    .line 674
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v0

    .line 676
    .local v0, "atmsImpl":Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 677
    invoke-virtual {v0, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 678
    :cond_0
    const/4 v1, 0x1

    return v1

    .line 680
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public skipLetterboxInSplitScreen(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 686
    const/4 v0, 0x1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 687
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;

    move-result-object v1

    iget-boolean v1, v1, Lcom/android/server/wm/Task;->mSoScRoot:Z

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 686
    :cond_2
    :goto_0
    return v0
.end method

.method public updateLatterboxParam(ZLandroid/graphics/Point;)V
    .locals 1
    .param p1, "isEmbedded"    # Z
    .param p2, "surfaceOrigin"    # Landroid/graphics/Point;

    .line 618
    if-eqz p1, :cond_0

    .line 619
    const/4 v0, 0x0

    invoke-virtual {p2, v0, v0}, Landroid/graphics/Point;->set(II)V

    .line 621
    :cond_0
    return-void
.end method

.method public updateSpaceToFill(ZLandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "isEmbedded"    # Z
    .param p2, "spaceToFill"    # Landroid/graphics/Rect;
    .param p3, "windowFrame"    # Landroid/graphics/Rect;

    .line 613
    if-eqz p1, :cond_0

    .line 614
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p3, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->right:I

    iget v3, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 616
    :cond_0
    return-void
.end method
