public class com.android.server.wm.MiuiFreeFormManagerService extends miui.app.IMiuiFreeFormManager$Stub implements com.android.server.wm.MiuiFreeFormManagerServiceStub {
	 /* .source "MiuiFreeFormManagerService.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 com.android.server.wm.ActivityTaskManagerService mActivityTaskManagerService;
	 private android.util.ArraySet mAppBehindHome;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/ArraySet<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.lang.String mApplicationUsedInFreeform;
private Boolean mAutoLayoutModeOn;
private final android.os.RemoteCallbackList mCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lmiui/app/IFreeformCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mCorrectScaleList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final java.util.concurrent.ConcurrentHashMap mFreeFormActivityStacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/wm/MiuiFreeFormActivityStack;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
com.android.server.wm.MiuiFreeFormCameraStrategy mFreeFormCameraStrategy;
com.android.server.wm.MiuiFreeFormGestureController mFreeFormGestureController;
com.android.server.wm.MiuiFreeFormStackDisplayStrategy mFreeFormStackDisplayStrategy;
private miui.app.IMiuiFreeformModeControl mFreeformModeControl;
private final android.os.IBinder$DeathRecipient mFreeformModeControlDeath;
android.os.Handler mHandler;
private Integer mImeHeight;
private Boolean mIsImeShowing;
/* # direct methods */
public static void $r8$lambda$7izAiEB8PoL1-S1Oje3FKpXFrk0 ( com.android.server.wm.MiuiFreeFormManagerService p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$new$0()V */
return;
} // .end method
public static Boolean $r8$lambda$Bqquvlh8rG7WxT8sZTzwMZRDNm0 ( com.android.server.wm.MiuiFreeFormManagerService p0, com.android.server.wm.MiuiFreeFormActivityStack p1, Boolean p2, Boolean p3, com.android.server.wm.Task p4 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getBottomFreeformTask$3(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZLcom/android/server/wm/Task;)Z */
} // .end method
public static void $r8$lambda$EJbMKE5acRWEMzCj_UhtQ1OkWJ8 ( com.android.server.wm.MiuiFreeFormManagerService p0, com.android.server.wm.MiuiFreeFormActivityStack p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$dispatchFreeFormStackModeChanged$6(Lcom/android/server/wm/MiuiFreeFormActivityStack;I)V */
return;
} // .end method
public static void $r8$lambda$Kljm1N4FZGtUch9ixmNtvRwLNZ8 ( com.android.server.wm.MiuiFreeFormManagerService p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$fromFreefromToMini$10(I)V */
return;
} // .end method
public static void $r8$lambda$S2tFuE_mJ0W3BRmaMIvImHHlQkk ( com.android.server.wm.MiuiFreeFormManagerService p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$fullscreenFreeformTask$15(I)V */
return;
} // .end method
public static Boolean $r8$lambda$d-_PaSwNB2ZaD72GwvymMr8JoPs ( com.android.server.wm.MiuiFreeFormManagerService p0, com.android.server.wm.MiuiFreeFormActivityStack p1, Boolean p2, Boolean p3, com.android.server.wm.Task p4 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getLastFreeformTask$1(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZLcom/android/server/wm/Task;)Z */
} // .end method
public static void $r8$lambda$fpM-xuREdLTfKdDwCA3Lccpjlxg ( com.android.server.wm.MiuiFreeFormManagerService p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$unPinFloatingWindowForActive$7(I)V */
return;
} // .end method
public static Boolean $r8$lambda$mm_AKnhjp-wY25TNSTGT0CXmvVk ( com.android.server.wm.MiuiFreeFormManagerService p0, com.android.server.wm.MiuiFreeFormActivityStack p1, com.android.server.wm.Task p2 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getBottomGameFreeFormActivityStack$4(Lcom/android/server/wm/MiuiFreeFormActivityStack;Lcom/android/server/wm/Task;)Z */
} // .end method
public static Boolean $r8$lambda$oQY6W05var7hgiZQH-5tB1XVpfM ( com.android.server.wm.MiuiFreeFormManagerService p0, com.android.server.wm.Task p1, com.android.server.wm.Task p2 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getLastVisibleFreeformTask$2(Lcom/android/server/wm/Task;Lcom/android/server/wm/Task;)Z */
} // .end method
public static void $r8$lambda$pLSudP9ZOkzthgsGU9NkQfYNPic ( com.android.server.wm.MiuiFreeFormManagerService p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$freeformKillAll$17()V */
return;
} // .end method
public static void $r8$lambda$rPHaFgGcc3Zh5DqOMcZ3HDywkvQ ( com.android.server.wm.MiuiFreeFormManagerService p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$freeformFullscreenTask$16(I)V */
return;
} // .end method
public static void $r8$lambda$sJj-lpr-1P4fBYw5-P2lZ7CzPxc ( com.android.server.wm.MiuiFreeFormManagerService p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$allFreeformFormFreefromToMini$9()V */
return;
} // .end method
public static void $r8$lambda$tDwk7sHxedwGV75PTko6X-Jm7Uo ( com.android.server.wm.MiuiFreeFormManagerService p0, com.android.server.wm.Task p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$setRequestedOrientation$12(Lcom/android/server/wm/Task;I)V */
return;
} // .end method
public static Boolean $r8$lambda$tskTCCcQhUpP3JTTcbpkrCJa26c ( com.android.server.wm.MiuiFreeFormManagerService p0, Integer p1, com.android.server.wm.Task p2 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$getFreeFormStackToAvoid$13(ILcom/android/server/wm/Task;)Z */
} // .end method
public static void $r8$lambda$vNiR9-28tAymv8LuB53QQIzleZk ( com.android.server.wm.MiuiFreeFormManagerService p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$onExitFreeform$11(I)V */
return;
} // .end method
public static void $r8$lambda$xvAbAMPU7COXn2aoWqImy5aHpHk ( com.android.server.wm.MiuiFreeFormManagerService p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$removeFreeformParamsForAutoLayout$14(I)V */
return;
} // .end method
public static void $r8$lambda$zvzJM_7O52NQQydOXhEo7h74glE ( com.android.server.wm.MiuiFreeFormManagerService p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->lambda$restoreMiniToFreeformMode$8(I)V */
return;
} // .end method
public com.android.server.wm.MiuiFreeFormManagerService ( ) {
/* .locals 1 */
/* .line 104 */
/* invoke-direct {p0}, Lmiui/app/IMiuiFreeFormManager$Stub;-><init>()V */
/* .line 106 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mFreeFormActivityStacks = v0;
/* .line 107 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mCallbacks = v0;
/* .line 117 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAutoLayoutModeOn:Z */
/* .line 118 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCorrectScaleList = v0;
/* .line 119 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mAppBehindHome = v0;
/* .line 120 */
/* new-instance v0, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V */
this.mFreeformModeControlDeath = v0;
return;
} // .end method
private Boolean applockMatched ( com.android.server.wm.Task p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "lastFreeformTask" # Lcom/android/server/wm/Task; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .line 777 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 778 */
int v1 = 0; // const/4 v1, 0x0
/* .line 779 */
/* .local v1, "lastFreeformActivity":Lcom/android/server/wm/ActivityRecord; */
v2 = this.mActivityTaskManagerService;
v2 = this.mWindowManager;
v2 = this.mGlobalLock;
/* monitor-enter v2 */
/* .line 780 */
int v3 = 1; // const/4 v3, 0x1
try { // :try_start_0
(( com.android.server.wm.Task ) p1 ).getTopActivity ( v3, v3 ); // invoke-virtual {p1, v3, v3}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
/* move-object v1, v4 */
/* .line 781 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 782 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 783 */
v2 = this.mActivityComponent;
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 784 */
/* .local v2, "lastFreeformComponentName":Landroid/content/ComponentName; */
} // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
final String v4 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v4, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
/* .line 785 */
(( android.content.ComponentName ) v2 ).flattenToString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
/* .line 784 */
v4 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
v4 = this.behindAppLockPkg;
if ( v4 != null) { // if-eqz v4, :cond_1
v4 = this.behindAppLockPkg;
/* .line 786 */
v4 = (( java.lang.String ) v4 ).equals ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 787 */
final String v4 = "MiuiFreeFormManagerService"; // const-string v4, "MiuiFreeFormManagerService"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "unlock: " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " to addingTaskPkg:"; // const-string v6, " to addingTaskPkg:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,v4,v5 );
/* .line 788 */
/* .line 781 */
} // .end local v2 # "lastFreeformComponentName":Landroid/content/ComponentName;
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 791 */
} // .end local v1 # "lastFreeformActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_1
} // .end method
private void centerRect ( android.graphics.Rect p0, android.graphics.Rect p1, Boolean p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "outBounds" # Landroid/graphics/Rect; */
/* .param p2, "accessibleArea" # Landroid/graphics/Rect; */
/* .param p3, "isDisplayLandscape" # Z */
/* .param p4, "startPoint" # I */
/* .line 1663 */
int v0 = 0; // const/4 v0, 0x0
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 1664 */
v1 = (( android.graphics.Rect ) p2 ).centerY ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I
v2 = (( android.graphics.Rect ) p1 ).centerY ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I
/* sub-int/2addr v1, v2 */
(( android.graphics.Rect ) p1 ).offset ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->offset(II)V
/* .line 1665 */
/* iget v0, p1, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) p1 ).offsetTo ( p4, v0 ); // invoke-virtual {p1, p4, v0}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1667 */
} // :cond_0
v1 = (( android.graphics.Rect ) p2 ).centerX ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I
v2 = (( android.graphics.Rect ) p1 ).centerX ( ); // invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I
/* sub-int/2addr v1, v2 */
(( android.graphics.Rect ) p1 ).offset ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/graphics/Rect;->offset(II)V
/* .line 1668 */
/* iget v0, p1, Landroid/graphics/Rect;->left:I */
(( android.graphics.Rect ) p1 ).offsetTo ( v0, p4 ); // invoke-virtual {p1, v0, p4}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1670 */
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "centerRect to ("; // const-string v1, "centerRect to ("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Landroid/graphics/Rect;->left:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Landroid/graphics/Rect;->top:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 1671 */
return;
} // .end method
private void detachFreeformModeControl ( ) {
/* .locals 1 */
/* .line 147 */
v0 = this.mFreeformModeControl;
/* if-nez v0, :cond_0 */
return;
/* .line 148 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mFreeformModeControl = v0;
/* .line 149 */
return;
} // .end method
private com.android.server.wm.Task getBottomFreeformTask ( com.android.server.wm.MiuiFreeFormActivityStack p0, Boolean p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "addingTask" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .param p2, "findFreeformMode" # Z */
/* .param p3, "findMini" # Z */
/* .line 835 */
final String v0 = "MiuiFreeFormManagerService"; // const-string v0, "MiuiFreeFormManagerService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getBottomFreeformTask addingTask = "; // const-string v2, "getBottomFreeformTask addingTask = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " findFreeformMode: "; // const-string v2, " findFreeformMode: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " findMini: "; // const-string v2, " findMini: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 837 */
v0 = this.mActivityTaskManagerService;
v0 = this.mWindowManager;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 838 */
try { // :try_start_0
v1 = this.mActivityTaskManagerService;
v1 = this.mRootWindowContainer;
/* .line 839 */
(( com.android.server.wm.RootWindowContainer ) v1 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 840 */
/* .local v1, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea; */
/* new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda12; */
/* invoke-direct {v2, p0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda12;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)V */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.wm.TaskDisplayArea ) v1 ).getTask ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;
/* .line 854 */
/* .local v2, "replacedTask":Lcom/android/server/wm/Task; */
/* monitor-exit v0 */
/* .line 855 */
} // .end local v1 # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
} // .end local v2 # "replacedTask":Lcom/android/server/wm/Task;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.android.server.wm.MiuiFreeFormActivityStack getFreeformTaskHasAppLock ( com.android.server.wm.Task p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "addingTask" # Lcom/android/server/wm/Task; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .line 756 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 757 */
/* .line 759 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getLastVisibleFreeformTask(Lcom/android/server/wm/Task;)Lcom/android/server/wm/Task; */
/* .line 760 */
/* .local v1, "lastVisibleFreeformTask":Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = /* invoke-direct {p0, v1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->applockMatched(Lcom/android/server/wm/Task;Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 761 */
v0 = (( com.android.server.wm.Task ) v1 ).getRootTaskId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 762 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "get freeformTask with applock: "; // const-string v3, "get freeformTask with applock: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v3 = 0; // const/4 v3, 0x0
final String v4 = "MiuiFreeFormManagerService"; // const-string v4, "MiuiFreeFormManagerService"
com.android.server.wm.MiuiFreeFormManagerService .logd ( v3,v4,v2 );
/* .line 763 */
/* .line 765 */
} // .end local v0 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_1
} // .end method
private com.android.server.wm.Task getLastFreeformTask ( com.android.server.wm.MiuiFreeFormActivityStack p0, Boolean p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "addingTask" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .param p2, "findFreeformMode" # Z */
/* .param p3, "findMini" # Z */
/* .line 796 */
v0 = this.mActivityTaskManagerService;
v0 = this.mWindowManager;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 797 */
try { // :try_start_0
v1 = this.mActivityTaskManagerService;
v1 = this.mRootWindowContainer;
/* .line 798 */
(( com.android.server.wm.RootWindowContainer ) v1 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 799 */
/* .local v1, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea; */
/* new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda6; */
/* invoke-direct {v2, p0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)V */
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.wm.TaskDisplayArea ) v1 ).getTask ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;
/* .line 810 */
/* .local v2, "lastTask":Lcom/android/server/wm/Task; */
/* monitor-exit v0 */
/* .line 811 */
} // .end local v1 # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
} // .end local v2 # "lastTask":Lcom/android/server/wm/Task;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.android.server.wm.Task getLastVisibleFreeformTask ( com.android.server.wm.Task p0 ) {
/* .locals 4 */
/* .param p1, "addingTask" # Lcom/android/server/wm/Task; */
/* .line 815 */
v0 = this.mActivityTaskManagerService;
v0 = this.mWindowManager;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 816 */
try { // :try_start_0
v1 = this.mActivityTaskManagerService;
v1 = this.mRootWindowContainer;
/* .line 817 */
(( com.android.server.wm.RootWindowContainer ) v1 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 818 */
/* .local v1, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea; */
/* new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda9; */
/* invoke-direct {v2, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda9;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/Task;)V */
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.wm.TaskDisplayArea ) v1 ).getTask ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;
/* .line 829 */
/* .local v2, "lastVisibleTask":Lcom/android/server/wm/Task; */
/* monitor-exit v0 */
/* .line 830 */
} // .end local v1 # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
} // .end local v2 # "lastVisibleTask":Lcom/android/server/wm/Task;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.android.server.wm.Task getReplacePinModeTask ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 7 */
/* .param p1, "addingTask" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 859 */
int v0 = 0; // const/4 v0, 0x0
/* .line 860 */
/* .local v0, "replacedPinTask":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
v1 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 861 */
/* .local v2, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
v3 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).inPinMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* if-eq v2, p1, :cond_1 */
/* .line 862 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-wide v3, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->pinActiveTime:J */
/* iget-wide v5, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->pinActiveTime:J */
/* cmp-long v3, v3, v5 */
/* if-lez v3, :cond_1 */
/* .line 863 */
} // :cond_0
/* move-object v0, v2 */
/* .line 866 */
} // .end local v2 # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_1
/* .line 867 */
} // :cond_2
/* if-nez v0, :cond_3 */
int v1 = 0; // const/4 v1, 0x0
/* .line 868 */
} // :cond_3
v1 = this.mTask;
} // .end method
private Boolean isCandidateForAutoLayout ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 1 */
/* .param p1, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1655 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1657 */
v0 = (( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mTask;
/* .line 1658 */
v0 = (( com.android.server.wm.Task ) v0 ).inFreeformWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1659 */
v0 = (( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).isInFreeFormMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mTask;
v0 = (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
v0 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).isAppBehindHome ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1655 */
} // :goto_0
} // .end method
private Boolean isHomeTopTask ( ) {
/* .locals 3 */
/* .line 442 */
v0 = this.mActivityTaskManagerService;
v0 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v0 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 443 */
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.wm.TaskDisplayArea ) v0 ).getTopRootTaskInWindowingMode ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;
/* .line 444 */
/* .local v0, "currentFullRootTask":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = (( com.android.server.wm.Task ) v0 ).isActivityTypeHomeOrRecents ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 445 */
/* .line 447 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean isLaunchForSplitMode ( com.android.server.wm.Task p0 ) {
/* .locals 5 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 1939 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 1940 */
} // :cond_0
(( com.android.server.wm.Task ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
/* .line 1941 */
/* .local v1, "rootTask":Lcom/android/server/wm/Task; */
/* if-nez v1, :cond_1 */
/* .line 1944 */
} // :cond_1
/* iget-boolean v2, v1, Lcom/android/server/wm/Task;->mSoScRoot:Z */
int v3 = 1; // const/4 v3, 0x1
/* if-nez v2, :cond_3 */
/* iget-boolean v2, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1945 */
v2 = (( com.android.server.wm.Task ) v1 ).getWindowingMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-ne v2, v3, :cond_2 */
/* .line 1946 */
v2 = (( com.android.server.wm.Task ) v1 ).getChildCount ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getChildCount()I
int v4 = 2; // const/4 v4, 0x2
/* if-ne v2, v4, :cond_2 */
/* .line 1947 */
v2 = (( com.android.server.wm.Task ) p1 ).getWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v4 = 6; // const/4 v4, 0x6
/* if-ne v2, v4, :cond_2 */
/* .line 1950 */
} // :cond_2
/* .line 1948 */
} // :cond_3
} // :goto_0
} // .end method
private void lambda$allFreeformFormFreefromToMini$9 ( ) { //synthethic
/* .locals 1 */
/* .line 1169 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1172 */
/* .line 1170 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1171 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1173 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
static void lambda$dispatchFreeFormStackModeChanged$5 ( Integer p0, miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo p1, com.android.server.wm.WindowState p2 ) { //synthethic
/* .locals 0 */
/* .param p0, "action" # I */
/* .param p1, "freeFormASInfo" # Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
/* .param p2, "win" # Lcom/android/server/wm/WindowState; */
/* .line 1081 */
(( com.android.server.wm.WindowState ) p2 ).dispatchFreeFormStackModeChanged ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/wm/WindowState;->dispatchFreeFormStackModeChanged(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
/* .line 1082 */
return;
} // .end method
private void lambda$dispatchFreeFormStackModeChanged$6 ( com.android.server.wm.MiuiFreeFormActivityStack p0, Integer p1 ) { //synthethic
/* .locals 7 */
/* .param p1, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .param p2, "action" # I */
/* .line 1060 */
/* if-nez p1, :cond_0 */
return;
/* .line 1061 */
} // :cond_0
(( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* .line 1062 */
/* .local v0, "freeFormASInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "action: "; // const-string v3, "action: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " dispatchFreeFormStackModeChanged freeFormASInfo = "; // const-string v3, " dispatchFreeFormStackModeChanged freeFormASInfo = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v3 = 1; // const/4 v3, 0x1
com.android.server.wm.MiuiFreeFormManagerService .logd ( v3,v1,v2 );
/* .line 1063 */
/* iget-wide v1, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->timestamp:J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v1, v1, v4 */
/* if-nez v1, :cond_1 */
/* .line 1064 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).setFreeformTimestamp ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setFreeformTimestamp(J)V
/* .line 1066 */
} // :cond_1
v1 = this.mCallbacks;
/* monitor-enter v1 */
/* .line 1067 */
try { // :try_start_0
v2 = this.mCallbacks;
v2 = (( android.os.RemoteCallbackList ) v2 ).beginBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 1068 */
/* .local v2, "callbacksCount":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v2, :cond_2 */
/* .line 1069 */
v5 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v5 ).getBroadcastItem ( v4 ); // invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v5, Lmiui/app/IFreeformCallback; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1071 */
/* .local v5, "freeformCallback":Lmiui/app/IFreeformCallback; */
try { // :try_start_1
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 1074 */
/* .line 1072 */
/* :catch_0 */
/* move-exception v6 */
/* .line 1073 */
/* .local v6, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v6 ).printStackTrace ( ); // invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1068 */
} // .end local v5 # "freeformCallback":Lmiui/app/IFreeformCallback;
} // .end local v6 # "e":Ljava/lang/Exception;
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1076 */
} // .end local v4 # "i":I
} // :cond_2
v4 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v4 ).finishBroadcast ( ); // invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 1077 */
} // .end local v2 # "callbacksCount":I
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 1078 */
v1 = this.mTask;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1079 */
v1 = this.mTask;
v1 = this.mWmService;
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 1080 */
try { // :try_start_3
v2 = this.mTask;
/* new-instance v4, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda11; */
/* invoke-direct {v4, p2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda11;-><init>(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V */
(( com.android.server.wm.Task ) v2 ).forAllWindows ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Lcom/android/server/wm/Task;->forAllWindows(Ljava/util/function/Consumer;Z)V
/* .line 1083 */
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1084 */
/* if-nez p2, :cond_3 */
/* .line 1085 */
v1 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v1 ).deliverDestroyItemToAlipay ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverDestroyItemToAlipay(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 1083 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_4
/* monitor-exit v1 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* throw v2 */
/* .line 1088 */
} // :cond_3
} // :goto_2
return;
/* .line 1077 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_5
/* monitor-exit v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* throw v2 */
} // .end method
private void lambda$freeformFullscreenTask$16 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 1835 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1838 */
/* .line 1836 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1837 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1839 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void lambda$freeformKillAll$17 ( ) { //synthethic
/* .locals 1 */
/* .line 1846 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1849 */
/* .line 1847 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1848 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1850 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void lambda$fromFreefromToMini$10 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 1180 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1183 */
/* .line 1181 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1182 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1184 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void lambda$fullscreenFreeformTask$15 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 1824 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1827 */
/* .line 1825 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1826 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1828 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean lambda$getBottomFreeformTask$3 ( com.android.server.wm.MiuiFreeFormActivityStack p0, Boolean p1, Boolean p2, com.android.server.wm.Task p3 ) { //synthethic
/* .locals 3 */
/* .param p1, "addingTask" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .param p2, "findFreeformMode" # Z */
/* .param p3, "findMini" # Z */
/* .param p4, "t" # Lcom/android/server/wm/Task; */
/* .line 841 */
v0 = (( com.android.server.wm.Task ) p4 ).inFreeformWindowingMode ( ); // invoke-virtual {p4}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 842 */
v0 = this.mFreeFormActivityStacks;
v1 = (( com.android.server.wm.Task ) p4 ).getRootTaskId ( ); // invoke-virtual {p4}, Lcom/android/server/wm/Task;->getRootTaskId()I
java.lang.Integer .valueOf ( v1 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 843 */
/* .local v0, "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getBottomFreeformTask formActivityStack = "; // const-string v2, "getBottomFreeformTask formActivityStack = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFreeFormManagerService"; // const-string v2, "MiuiFreeFormManagerService"
android.util.Slog .i ( v2,v1 );
/* .line 844 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-eq v0, p1, :cond_2 */
/* .line 845 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.mAppBehindHome;
/* .line 846 */
v2 = (( com.android.server.wm.Task ) p4 ).getRootTaskId ( ); // invoke-virtual {p4}, Lcom/android/server/wm/Task;->getRootTaskId()I
java.lang.Integer .valueOf ( v2 );
v1 = (( android.util.ArraySet ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_2 */
/* .line 847 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* if-nez p2, :cond_1 */
/* .line 848 */
} // :cond_0
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInMiniFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
if ( v1 != null) { // if-eqz v1, :cond_2
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 849 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* .line 852 */
} // .end local v0 # "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean lambda$getBottomGameFreeFormActivityStack$4 ( com.android.server.wm.MiuiFreeFormActivityStack p0, com.android.server.wm.Task p1 ) { //synthethic
/* .locals 4 */
/* .param p1, "addingStack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .param p2, "t" # Lcom/android/server/wm/Task; */
/* .line 893 */
v0 = (( com.android.server.wm.Task ) p2 ).inFreeformWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 894 */
v0 = this.mFreeFormActivityStacks;
v2 = (( com.android.server.wm.Task ) p2 ).getRootTaskId ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I
java.lang.Integer .valueOf ( v2 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 895 */
/* .local v0, "curFreeFormActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-eq v0, p1, :cond_0 */
/* .line 896 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).getStackPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v2 = android.util.MiuiMultiWindowAdapter .isInTopGameList ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 897 */
v2 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mAppBehindHome;
/* .line 898 */
v3 = (( com.android.server.wm.Task ) p2 ).getRootTaskId ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I
java.lang.Integer .valueOf ( v3 );
v2 = (( android.util.ArraySet ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 899 */
int v1 = 1; // const/4 v1, 0x1
/* .line 901 */
} // :cond_0
/* .line 903 */
} // .end local v0 # "curFreeFormActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_1
} // .end method
private Boolean lambda$getFreeFormStackToAvoid$13 ( Integer p0, com.android.server.wm.Task p1 ) { //synthethic
/* .locals 5 */
/* .param p1, "displayId" # I */
/* .param p2, "t" # Lcom/android/server/wm/Task; */
/* .line 1566 */
v0 = (( com.android.server.wm.Task ) p2 ).inFreeformWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1567 */
v0 = (( com.android.server.wm.Task ) p2 ).getRootTaskId ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1568 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
v3 = this.mTask;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.mTask;
v3 = this.realActivity;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = android.util.MiuiMultiWindowAdapter.NOT_AVOID_LAUNCH_OTHER_FREEFORM_LIST;
v4 = this.mTask;
v4 = this.realActivity;
/* .line 1571 */
(( android.content.ComponentName ) v4 ).getClassName ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v3 = /* .line 1570 */
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.mTask;
v3 = this.realActivity;
/* .line 1573 */
(( android.content.ComponentName ) v3 ).flattenToShortString ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* .line 1572 */
final String v4 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v4, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
v3 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_0 */
/* move v3, v2 */
} // :cond_0
/* move v3, v1 */
/* .line 1574 */
/* .local v3, "notAvoid":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
int v4 = -1; // const/4 v4, -0x1
/* if-eq v4, p1, :cond_1 */
v4 = this.mTask;
v4 = (( com.android.server.wm.Task ) v4 ).getDisplayId ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getDisplayId()I
/* if-ne v4, p1, :cond_2 */
/* .line 1576 */
} // :cond_1
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).inPinMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
/* if-nez v4, :cond_2 */
/* if-nez v3, :cond_2 */
/* .line 1577 */
/* .line 1579 */
} // .end local v0 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v3 # "notAvoid":Z
} // :cond_2
} // .end method
private Boolean lambda$getLastFreeformTask$1 ( com.android.server.wm.MiuiFreeFormActivityStack p0, Boolean p1, Boolean p2, com.android.server.wm.Task p3 ) { //synthethic
/* .locals 2 */
/* .param p1, "addingTask" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .param p2, "findFreeformMode" # Z */
/* .param p3, "findMini" # Z */
/* .param p4, "t" # Lcom/android/server/wm/Task; */
/* .line 800 */
v0 = (( com.android.server.wm.Task ) p4 ).inFreeformWindowingMode ( ); // invoke-virtual {p4}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 801 */
v0 = this.mFreeFormActivityStacks;
v1 = (( com.android.server.wm.Task ) p4 ).getRootTaskId ( ); // invoke-virtual {p4}, Lcom/android/server/wm/Task;->getRootTaskId()I
java.lang.Integer .valueOf ( v1 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 802 */
/* .local v0, "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-eq v0, p1, :cond_2 */
/* .line 803 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* if-nez p2, :cond_1 */
/* .line 804 */
} // :cond_0
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInMiniFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
if ( v1 != null) { // if-eqz v1, :cond_2
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 805 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* .line 808 */
} // .end local v0 # "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean lambda$getLastVisibleFreeformTask$2 ( com.android.server.wm.Task p0, com.android.server.wm.Task p1 ) { //synthethic
/* .locals 3 */
/* .param p1, "addingTask" # Lcom/android/server/wm/Task; */
/* .param p2, "t" # Lcom/android/server/wm/Task; */
/* .line 819 */
v0 = (( com.android.server.wm.Task ) p2 ).inFreeformWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 820 */
v0 = this.mFreeFormActivityStacks;
v1 = (( com.android.server.wm.Task ) p2 ).getRootTaskId ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I
java.lang.Integer .valueOf ( v1 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 821 */
/* .local v0, "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 822 */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isCandidateForAutoLayout(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mTask;
/* .line 823 */
v1 = (( com.android.server.wm.Task ) v1 ).getRootTaskId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I */
/* if-eq v1, v2, :cond_0 */
/* .line 824 */
int v1 = 1; // const/4 v1, 0x1
/* .line 827 */
} // .end local v0 # "formActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 2 */
/* .line 121 */
v0 = this.mActivityTaskManagerService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 122 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->detachFreeformModeControl()V */
/* .line 124 */
v1 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v1 ).clearAllFreeFormForProcessReboot ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->clearAllFreeFormForProcessReboot()V
/* .line 125 */
/* monitor-exit v0 */
/* .line 126 */
return;
/* .line 125 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$onExitFreeform$11 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 1191 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1194 */
/* .line 1192 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1193 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1195 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void lambda$removeFreeformParamsForAutoLayout$14 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 1677 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1680 */
/* .line 1678 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1679 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1681 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void lambda$restoreMiniToFreeformMode$8 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 1158 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1161 */
/* .line 1159 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1160 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1162 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void lambda$setRequestedOrientation$12 ( com.android.server.wm.Task p0, Integer p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "requestedOrientation" # I */
/* .line 1208 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
v1 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1211 */
/* .line 1209 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1210 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1212 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void lambda$unPinFloatingWindowForActive$7 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 1147 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1150 */
/* .line 1148 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1149 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1151 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
static Boolean lambda$unSupportedFreeformInDesktop$18 ( Integer p0, com.android.server.wm.Task p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "taskId" # I */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 2193 */
/* iget v0, p1, Lcom/android/server/wm/Task;->mTaskId:I */
/* if-ne v0, p0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
static void logd ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .param p1, "string" # Ljava/lang/String; */
/* .line 1361 */
int v0 = 1; // const/4 v0, 0x1
com.android.server.wm.MiuiFreeFormManagerService .logd ( v0,p0,p1 );
/* .line 1362 */
return;
} // .end method
static void logd ( Boolean p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p0, "enable" # Z */
/* .param p1, "tag" # Ljava/lang/String; */
/* .param p2, "string" # Ljava/lang/String; */
/* .line 1365 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->DEBUG:Z */
/* if-nez v0, :cond_0 */
if ( p0 != null) { // if-eqz p0, :cond_1
/* .line 1366 */
} // :cond_0
android.util.Slog .d ( p1,p2 );
/* .line 1368 */
} // :cond_1
return;
} // .end method
private void notifyImeVisibilityChanged ( Boolean p0, Integer p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "imeVisible" # Z */
/* .param p2, "imeHeight" # I */
/* .param p3, "adjustedForRotation" # Z */
/* .line 1216 */
v0 = this.mFreeformModeControl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1218 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1221 */
/* .line 1219 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1220 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
final String v2 = "Error delivering bounds changed event."; // const-string v2, "Error delivering bounds changed event."
android.util.Slog .e ( v1,v2,v0 );
/* .line 1223 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
private void onMiuiFreeFormStasckAdded ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 3 */
/* .param p1, "stack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 577 */
v0 = this.mFreeFormStackDisplayStrategy;
v1 = this.mFreeFormActivityStacks;
v2 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormStackDisplayStrategy ) v0 ).onMiuiFreeFormStasckAdded ( v1, v2, p1 ); // invoke-virtual {v0, v1, v2, p1}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->onMiuiFreeFormStasckAdded(Ljava/util/concurrent/ConcurrentHashMap;Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 580 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).setCameraRotationIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setCameraRotationIfNeeded()V
/* .line 581 */
return;
} // .end method
private void removeFreeformParamsForAutoLayout ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1674 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1675 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda16; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda16;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1682 */
return;
} // .end method
private Float scaleDownIfNeeded ( Float p0, android.graphics.Rect p1, android.graphics.Rect p2 ) {
/* .locals 6 */
/* .param p1, "scale" # F */
/* .param p2, "bounds" # Landroid/graphics/Rect; */
/* .param p3, "stableBounds" # Landroid/graphics/Rect; */
/* .line 1685 */
/* move v0, p1 */
/* .line 1686 */
/* .local v0, "finalScale":F */
v1 = (( android.graphics.Rect ) p2 ).width ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->width()I
/* int-to-float v1, v1 */
/* mul-float/2addr v1, v0 */
/* .line 1687 */
/* .local v1, "currentVisualWidth":F */
v2 = (( android.graphics.Rect ) p2 ).height ( ); // invoke-virtual {p2}, Landroid/graphics/Rect;->height()I
/* int-to-float v2, v2 */
/* mul-float/2addr v2, v0 */
/* .line 1688 */
/* .local v2, "currentVisualHeight":F */
v3 = (( android.graphics.Rect ) p3 ).width ( ); // invoke-virtual {p3}, Landroid/graphics/Rect;->width()I
/* int-to-float v3, v3 */
/* cmpl-float v3, v1, v3 */
/* if-gtz v3, :cond_0 */
/* .line 1689 */
v3 = (( android.graphics.Rect ) p3 ).height ( ); // invoke-virtual {p3}, Landroid/graphics/Rect;->height()I
/* int-to-float v3, v3 */
/* cmpl-float v3, v2, v3 */
/* if-lez v3, :cond_1 */
/* .line 1690 */
} // :cond_0
v3 = (( android.graphics.Rect ) p3 ).width ( ); // invoke-virtual {p3}, Landroid/graphics/Rect;->width()I
/* int-to-float v3, v3 */
/* div-float/2addr v3, v1 */
/* .line 1691 */
v4 = (( android.graphics.Rect ) p3 ).height ( ); // invoke-virtual {p3}, Landroid/graphics/Rect;->height()I
/* int-to-float v4, v4 */
/* div-float/2addr v4, v2 */
/* .line 1690 */
v3 = java.lang.Math .min ( v3,v4 );
/* .line 1692 */
/* .local v3, "scaleDown":F */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "scaleDownIfNeeded scaleDown: "; // const-string v5, "scaleDownIfNeeded scaleDown: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = " currentVisualWidth: "; // const-string v5, " currentVisualWidth: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = " currentVisualHeight: "; // const-string v5, " currentVisualHeight: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = " stableBounds: "; // const-string v5, " stableBounds: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p3 ); // invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " scale: "; // const-string v5, " scale: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiFreeFormManagerService"; // const-string v5, "MiuiFreeFormManagerService"
android.util.Slog .d ( v5,v4 );
/* .line 1695 */
/* mul-float v0, p1, v3 */
/* .line 1697 */
} // .end local v3 # "scaleDown":F
} // :cond_1
} // .end method
private void showFreeformIfNeeded ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .line 451 */
final String v0 = "MiuiFreeFormManagerService"; // const-string v0, "MiuiFreeFormManagerService"
v1 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 452 */
v1 = this.mFreeformModeControl;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 454 */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "show hidden task ...." */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 455 */
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isHomeTopTask()Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 456 */
v1 = this.mFreeformModeControl;
/* .line 458 */
} // :cond_0
v1 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 462 */
} // :goto_0
/* .line 460 */
/* :catch_0 */
/* move-exception v1 */
/* .line 461 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Error showFreeformIfNeeded."; // const-string v2, "Error showFreeformIfNeeded."
android.util.Slog .e ( v0,v2,v1 );
/* .line 465 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_1
return;
} // .end method
private void showPropotionalFreeformToast ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 5 */
/* .param p1, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 547 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* iget-boolean v0, p1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
/* if-nez v0, :cond_2 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).getStackPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 548 */
v0 = this.mApplicationUsedInFreeform;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 549 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 550 */
/* .local v0, "applications":[Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
/* .line 551 */
/* .local v2, "applicationsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v3 = (( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).getStackPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
/* if-nez v3, :cond_0 */
/* .line 552 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.mApplicationUsedInFreeform;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).getStackPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mApplicationUsedInFreeform = v1;
/* .line 553 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).showPropotionalFreeformToast ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->showPropotionalFreeformToast(Ljava/lang/String;)V
/* .line 555 */
} // .end local v0 # "applications":[Ljava/lang/String;
} // .end local v2 # "applicationsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_0
/* .line 556 */
} // :cond_1
(( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).getStackPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
this.mApplicationUsedInFreeform = v0;
/* .line 557 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).showPropotionalFreeformToast ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->showPropotionalFreeformToast(Ljava/lang/String;)V
/* .line 560 */
} // :cond_2
} // :goto_0
return;
} // .end method
private Boolean unlockingAppLock ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 2 */
/* .param p1, "addingTask" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 769 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, p1, v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getLastFreeformTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)Lcom/android/server/wm/Task; */
/* .line 770 */
/* .local v0, "lastFreeformTask":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_0 */
/* .line 773 */
} // :cond_0
(( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).getStackPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v1 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->applockMatched(Lcom/android/server/wm/Task;Ljava/lang/String;)Z */
/* .line 771 */
} // :cond_1
} // :goto_0
} // .end method
/* # virtual methods */
public void activeFreeformTask ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .line 1807 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1808 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1810 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).inPinMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1811 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setStackFreeFormMode ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V
/* .line 1812 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).unPinFloatingWindowForActive ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->unPinFloatingWindowForActive(I)V
/* .line 1813 */
} // :cond_0
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInMiniFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1814 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setStackFreeFormMode ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V
/* .line 1815 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).restoreMiniToFreeformMode ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->restoreMiniToFreeformMode(I)V
/* .line 1818 */
} // :cond_1
} // :goto_0
return;
} // .end method
public Boolean activeFreeformTaskIfNeed ( Integer p0, android.app.ActivityOptions p1, com.android.server.wm.RootWindowContainer p2 ) {
/* .locals 8 */
/* .param p1, "taskId" # I */
/* .param p2, "activityOptions" # Landroid/app/ActivityOptions; */
/* .param p3, "rootWindowContainer" # Lcom/android/server/wm/RootWindowContainer; */
/* .line 1893 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1894 */
/* .local v0, "activeFreeformTask":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 1895 */
/* .local v1, "startForFullscreen":Z */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.RootWindowContainer ) p3 ).anyTaskForId ( p1, v2 ); // invoke-virtual {p3, p1, v2}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;
/* .line 1896 */
/* .local v2, "preFindTask":Lcom/android/server/wm/Task; */
int v3 = 0; // const/4 v3, 0x0
/* .line 1897 */
/* .local v3, "startForSosc":Z */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1898 */
(( android.app.ActivityOptions ) p2 ).getLaunchRootTask ( ); // invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchRootTask()Landroid/window/WindowContainerToken;
com.android.server.wm.Task .fromWindowContainerToken ( v4 );
v4 = /* invoke-direct {p0, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isLaunchForSplitMode(Lcom/android/server/wm/Task;)Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1899 */
int v3 = 1; // const/4 v3, 0x1
/* .line 1901 */
} // :cond_0
int v4 = 1; // const/4 v4, 0x1
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 1902 */
v5 = (( android.app.ActivityOptions ) p2 ).getLaunchWindowingMode ( ); // invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I
/* if-ne v5, v4, :cond_1 */
/* .line 1903 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1906 */
} // :cond_1
if ( v2 != null) { // if-eqz v2, :cond_4
v5 = (( com.android.server.wm.Task ) v2 ).inFreeformWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* if-nez v3, :cond_4 */
/* .line 1907 */
final String v5 = "MiuiFreeFormManagerService"; // const-string v5, "MiuiFreeFormManagerService"
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1908 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).fullscreenFreeformTask ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fullscreenFreeformTask(I)V
/* .line 1909 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "fullscreenFreeformTask "; // const-string v7, "fullscreenFreeformTask "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.Task ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " from freeform to fullscreen for options.getLaunchWindowingMode() == WINDOWING_MODE_FULLSCREEN"; // const-string v7, " from freeform to fullscreen for options.getLaunchWindowingMode() == WINDOWING_MODE_FULLSCREEN"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 1911 */
/* .line 1913 */
} // :cond_2
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1914 */
/* .local v4, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v4 != null) { // if-eqz v4, :cond_4
v6 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v4 ).inPinMode ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
/* if-nez v6, :cond_3 */
v6 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v4 ).isInMiniFreeFormMode ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 1915 */
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
/* .line 1916 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).activeFreeformTask ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->activeFreeformTask(I)V
/* .line 1917 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "activeFreeformTaskIfNeed taskId: "; // const-string v7, "activeFreeformTaskIfNeed taskId: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " activityOptions: "; // const-string v7, " activityOptions: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = " startForSosc: "; // const-string v7, " startForSosc: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1921 */
} // .end local v4 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_4
} // .end method
public Boolean activeOrFullscreenFreeformTaskIfNeed ( com.android.server.wm.Task p0, android.app.ActivityOptions p1, com.android.server.wm.ActivityRecord p2, com.android.server.wm.ActivityRecord p3 ) {
/* .locals 7 */
/* .param p1, "reusedTask" # Lcom/android/server/wm/Task; */
/* .param p2, "options" # Landroid/app/ActivityOptions; */
/* .param p3, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "startingAr" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1855 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1856 */
/* .local v0, "startForSosc":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 1857 */
/* .local v1, "startForFullscreen":Z */
int v2 = 0; // const/4 v2, 0x0
/* .line 1858 */
/* .local v2, "startDefaultAr":Z */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1859 */
(( android.app.ActivityOptions ) p2 ).getLaunchRootTask ( ); // invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchRootTask()Landroid/window/WindowContainerToken;
com.android.server.wm.Task .fromWindowContainerToken ( v3 );
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isLaunchForSplitMode(Lcom/android/server/wm/Task;)Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1860 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1862 */
} // :cond_0
int v3 = 1; // const/4 v3, 0x1
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 1863 */
v4 = (( android.app.ActivityOptions ) p2 ).getLaunchWindowingMode ( ); // invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I
/* if-ne v4, v3, :cond_1 */
/* .line 1864 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1867 */
} // :cond_1
final String v4 = "MiuiFreeFormManagerService"; // const-string v4, "MiuiFreeFormManagerService"
if ( p4 != null) { // if-eqz p4, :cond_2
v5 = this.intent;
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1868 */
v5 = this.intent;
v2 = com.android.server.wm.ActivityRecord .isMainIntent ( v5 );
/* .line 1869 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "activeOrFullscreenFreeformTaskIfNeed startDefaultAr:"; // const-string v6, "activeOrFullscreenFreeformTaskIfNeed startDefaultAr:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 1872 */
} // :cond_2
/* if-nez v0, :cond_6 */
if ( p1 != null) { // if-eqz p1, :cond_6
v5 = (( com.android.server.wm.Task ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v5 != null) { // if-eqz v5, :cond_6
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 1873 */
v5 = (( com.android.server.wm.ActivityRecord ) p3 ).inFreeformWindowingMode ( ); // invoke-virtual {p3}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z
/* if-nez v5, :cond_6 */
/* .line 1874 */
} // :cond_3
/* const/high16 v5, 0x100000 */
/* iget v6, p1, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).dispatchFreeFormStackModeChanged ( v5, v6 ); // invoke-virtual {p0, v5, v6}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(II)V
/* .line 1875 */
/* iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I */
v5 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).inPinMode ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->inPinMode(I)Z
/* if-nez v5, :cond_4 */
/* iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I */
v5 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).isInMiniFreeFormMode ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isInMiniFreeFormMode(I)Z
if ( v5 != null) { // if-eqz v5, :cond_5
} // :cond_4
/* if-nez v1, :cond_5 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 1877 */
/* iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).activeFreeformTask ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->activeFreeformTask(I)V
/* .line 1878 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "activFreeformTask "; // const-string v6, "activFreeformTask "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.Task ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " is in pinmode or minifreeform, so just active it!"; // const-string v6, " is in pinmode or minifreeform, so just active it!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v5 );
/* .line 1880 */
/* .line 1881 */
} // :cond_5
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 1882 */
/* iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).fullscreenFreeformTask ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fullscreenFreeformTask(I)V
/* .line 1883 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "fullscreenFreeformTask "; // const-string v6, "fullscreenFreeformTask "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.wm.Task ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " from freeform to fullscreen for options.getLaunchWindowingMode() == WINDOWING_MODE_FULLSCREEN"; // const-string v6, " from freeform to fullscreen for options.getLaunchWindowingMode() == WINDOWING_MODE_FULLSCREEN"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v5 );
/* .line 1885 */
/* .line 1888 */
} // :cond_6
int v3 = 0; // const/4 v3, 0x0
} // .end method
public void addFreeFormActivityStack ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p1, "rootTask" # Lcom/android/server/wm/Task; */
/* .line 380 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).addFreeFormActivityStack ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;ILjava/lang/String;)V
/* .line 381 */
return;
} // .end method
public void addFreeFormActivityStack ( com.android.server.wm.Task p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "rootTask" # Lcom/android/server/wm/Task; */
/* .param p2, "miuiFreeFromWindowMode" # I */
/* .line 388 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).addFreeFormActivityStack ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;ILjava/lang/String;)V
/* .line 389 */
return;
} // .end method
public void addFreeFormActivityStack ( com.android.server.wm.Task p0, Integer p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p1, "rootTask" # Lcom/android/server/wm/Task; */
/* .param p2, "miuiFreeFromWindowMode" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 393 */
v0 = (( com.android.server.wm.Task ) p1 ).isActivityTypeHome ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isActivityTypeHome()Z
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 394 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "addFreeFormActivityStack reason="; // const-string v1, "addFreeFormActivityStack reason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
com.android.server.wm.MiuiFreeFormManagerService .logd ( v1,v0 );
/* .line 395 */
v0 = this.mFreeFormActivityStacks;
/* iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v2 );
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).containsKey ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
final String v2 = ", miuiFreeFromWindowMode="; // const-string v2, ", miuiFreeFromWindowMode="
final String v3 = " mffas.isInFreeFormMode()="; // const-string v3, " mffas.isInFreeFormMode()="
final String v4 = "addFreeFormActivityStack as = "; // const-string v4, "addFreeFormActivityStack as = "
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 396 */
v0 = this.mFreeFormActivityStacks;
/* iget v5, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v5 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 397 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
v5 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = v5 = this.mCorrectScaleList;
/* if-nez v5, :cond_1 */
v5 = this.mCorrectScaleList;
/* iget v6, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v6 );
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 398 */
v5 = this.mCorrectScaleList;
/* iget v6, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v6 );
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* iput v5, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* .line 399 */
v5 = this.mCorrectScaleList;
/* iget v6, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v6 );
/* .line 401 */
} // :cond_1
v5 = this.mFreeFormStackDisplayStrategy;
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).getStackPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v5 = (( com.android.server.wm.MiuiFreeFormStackDisplayStrategy ) v5 ).getMaxMiuiFreeFormStackCount ( v6, v0 ); // invoke-virtual {v5, v6, v0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->getMaxMiuiFreeFormStackCount(Ljava/lang/String;Lcom/android/server/wm/MiuiFreeFormActivityStack;)I
/* .line 402 */
/* .local v5, "maxStackCount":I */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).getStackPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v6 = android.util.MiuiMultiWindowAdapter .isInTopGameList ( v6 );
/* .line 403 */
/* .local v6, "isAddingTopGame":Z */
v7 = this.mFreeFormActivityStacks;
v7 = (( java.util.concurrent.ConcurrentHashMap ) v7 ).size ( ); // invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentHashMap;->size()I
final String v8 = ", isAddingTopGame="; // const-string v8, ", isAddingTopGame="
/* if-gt v7, v5, :cond_2 */
/* if-nez v6, :cond_2 */
/* .line 404 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ",maxStackCount="; // const-string v3, ",maxStackCount="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", mFreeFormActivityStacks.size()="; // const-string v3, ", mFreeFormActivityStacks.size()="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mFreeFormActivityStacks;
/* .line 406 */
v3 = (( java.util.concurrent.ConcurrentHashMap ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->size()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 404 */
com.android.server.wm.MiuiFreeFormManagerService .logd ( v1,v2 );
/* .line 408 */
} // :cond_2
v2 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
if ( v2 != null) { // if-eqz v2, :cond_4
v2 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
if ( v2 != null) { // if-eqz v2, :cond_4
v2 = (( com.android.server.wm.Task ) p1 ).hasActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->hasActivity()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 409 */
v2 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getFrontFreeformNum ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFrontFreeformNum(Lcom/android/server/wm/MiuiFreeFormActivityStack;)I
/* .line 410 */
/* .local v2, "frontSize":I */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v8 ); // invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ",frontSize="; // const-string v4, ",frontSize="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.wm.MiuiFreeFormManagerService .logd ( v1,v3 );
/* .line 413 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* if-ge v2, v5, :cond_3 */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 414 */
} // :cond_3
v1 = this.mFreeFormStackDisplayStrategy;
v3 = this.mFreeFormActivityStacks;
v4 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormStackDisplayStrategy ) v1 ).onMiuiFreeFormStasckAdded ( v3, v4, v0 ); // invoke-virtual {v1, v3, v4, v0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->onMiuiFreeFormStasckAdded(Ljava/util/concurrent/ConcurrentHashMap;Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 417 */
} // .end local v2 # "frontSize":I
} // :cond_4
} // :goto_0
return;
/* .line 420 */
} // .end local v0 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v5 # "maxStackCount":I
} // .end local v6 # "isAddingTopGame":Z
} // :cond_5
/* new-instance v0, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* invoke-direct {v0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;-><init>(Lcom/android/server/wm/Task;I)V */
/* .line 421 */
/* .restart local v0 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.wm.MiuiFreeFormManagerService .logd ( v1,v2 );
/* .line 423 */
v2 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
if ( v2 != null) { // if-eqz v2, :cond_6
v2 = v2 = this.mCorrectScaleList;
/* if-nez v2, :cond_6 */
v2 = this.mCorrectScaleList;
/* iget v3, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v3 );
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 424 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "addFreeFormActivityStack mCorrectScale "; // const-string v3, "addFreeFormActivityStack mCorrectScale "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCorrectScaleList;
/* iget v4, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v4 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.wm.MiuiFreeFormManagerService .logd ( v1,v2 );
/* .line 425 */
v1 = this.mCorrectScaleList;
/* iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v2 );
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iput v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* .line 426 */
v1 = this.mCorrectScaleList;
/* iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v2 );
/* .line 428 */
} // :cond_6
v1 = this.mFreeFormActivityStacks;
/* iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v2 );
(( java.util.concurrent.ConcurrentHashMap ) v1 ).put ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 429 */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->onMiuiFreeFormStasckAdded(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V */
/* .line 430 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).getStackPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 431 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 432 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).dispatchFreeFormStackModeChanged ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 433 */
} // :cond_7
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInMiniFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 434 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).dispatchFreeFormStackModeChanged ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 437 */
} // :cond_8
/* iput-boolean v2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z */
/* .line 439 */
} // :cond_9
} // :goto_1
return;
} // .end method
public void addFreeFormActivityStack ( com.android.server.wm.Task p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "rootTask" # Lcom/android/server/wm/Task; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 384 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).addFreeFormActivityStack ( p1, v0, p2 ); // invoke-virtual {p0, p1, v0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;ILjava/lang/String;)V
/* .line 385 */
return;
} // .end method
public void addFreeFormActivityStackFromStartSmallFreeform ( com.android.server.wm.Task p0, Integer p1, java.lang.String p2, android.graphics.Rect p3 ) {
/* .locals 7 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "cornerPosition" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .param p4, "fromRect" # Landroid/graphics/Rect; */
/* .line 1489 */
final String v0 = "FromStartSmallFreeform"; // const-string v0, "FromStartSmallFreeform"
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).addFreeFormActivityStack ( p1, v1, v0 ); // invoke-virtual {p0, p1, v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;ILjava/lang/String;)V
/* .line 1490 */
v0 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1491 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setCornerPosition ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setCornerPosition(I)V
/* .line 1492 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setEnterMiniFreeformReason ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setEnterMiniFreeformReason(Ljava/lang/String;)V
/* .line 1493 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setEnterMiniFreeformRect ( p4 ); // invoke-virtual {v0, p4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setEnterMiniFreeformRect(Landroid/graphics/Rect;)V
/* .line 1495 */
v2 = this.mTask;
v2 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v2 ).getLaunchParamsController ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
v2 = (( com.android.server.wm.LaunchParamsController ) v2 ).hasFreeformDesktopMemory ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z
/* if-nez v2, :cond_0 */
/* .line 1496 */
v2 = this.mAtmService;
v2 = this.mContext;
/* iget-boolean v3, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
/* iget-boolean v4, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
v5 = this.mAtmService;
v5 = this.mContext;
/* .line 1498 */
v5 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( v5 );
/* .line 1499 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).getStackPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
/* .line 1496 */
v2 = android.util.MiuiMultiWindowUtils .getOriFreeformScale ( v2,v3,v4,v5,v6 );
/* iput v2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* .line 1501 */
} // :cond_0
v2 = this.mTask;
v2 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v2 ).getLaunchParamsController ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
v2 = (( com.android.server.wm.LaunchParamsController ) v2 ).getFreeformLastScale ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F
/* iput v2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* .line 1503 */
} // :goto_0
(( com.android.server.wm.Task ) p1 ).setAlwaysOnTop ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->setAlwaysOnTop(Z)V
/* .line 1504 */
return;
} // .end method
public void addFullScreenTasksBehindHome ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 2012 */
v0 = this.mAppBehindHome;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 2013 */
return;
} // .end method
public Boolean adjustBoundsAndScaleIfNeeded ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 2136 */
try { // :try_start_0
v0 = v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2137 */
/* :catch_0 */
/* move-exception v0 */
/* .line 2138 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 2140 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void adjustFreeformTouchRegion ( android.graphics.Rect p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "inOutRect" # Landroid/graphics/Rect; */
/* .param p2, "taskId" # I */
/* .line 1777 */
com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
v1 = this.mActivityTaskManagerService;
v0 = v1 = this.mContext;
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1778 */
v0 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getFreeformScale ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeformScale(I)F
/* .line 1779 */
/* .local v0, "freefromScale":F */
v3 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getFreeFormWindowMode ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeFormWindowMode(I)I
/* if-nez v3, :cond_0 */
/* .line 1780 */
v1 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v1, v0 */
/* float-to-double v1, v1 */
java.lang.Math .ceil ( v1,v2 );
/* move-result-wide v1 */
/* double-to-int v1, v1 */
/* mul-int/lit8 v1, v1, -0x1 */
/* .line 1781 */
v2 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v2, v0 */
/* float-to-double v2, v2 */
java.lang.Math .ceil ( v2,v3 );
/* move-result-wide v2 */
/* double-to-int v2, v2 */
/* mul-int/lit8 v2, v2, -0x1 */
/* .line 1782 */
v3 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v3, v0 */
/* float-to-double v3, v3 */
java.lang.Math .ceil ( v3,v4 );
/* move-result-wide v3 */
/* double-to-int v3, v3 */
/* mul-int/lit8 v3, v3, -0x1 */
/* .line 1783 */
v4 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v4, v0 */
/* float-to-double v4, v4 */
java.lang.Math .ceil ( v4,v5 );
/* move-result-wide v4 */
/* double-to-int v4, v4 */
/* mul-int/lit8 v4, v4, -0x1 */
/* .line 1780 */
(( android.graphics.Rect ) p1 ).inset ( v1, v2, v3, v4 ); // invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Rect;->inset(IIII)V
/* .line 1784 */
} // :cond_0
v3 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getFreeFormWindowMode ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeFormWindowMode(I)I
/* if-ne v3, v2, :cond_1 */
/* .line 1785 */
v2 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v2, v0 */
/* float-to-double v2, v2 */
java.lang.Math .ceil ( v2,v3 );
/* move-result-wide v2 */
/* double-to-int v2, v2 */
/* mul-int/lit8 v2, v2, -0x1 */
/* .line 1787 */
v3 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v3, v0 */
/* float-to-double v3, v3 */
java.lang.Math .ceil ( v3,v4 );
/* move-result-wide v3 */
/* double-to-int v3, v3 */
/* mul-int/lit8 v3, v3, -0x1 */
/* .line 1788 */
v4 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v4, v0 */
/* float-to-double v4, v4 */
java.lang.Math .ceil ( v4,v5 );
/* move-result-wide v4 */
/* double-to-int v4, v4 */
/* mul-int/lit8 v4, v4, -0x1 */
/* .line 1785 */
(( android.graphics.Rect ) p1 ).inset ( v2, v1, v3, v4 ); // invoke-virtual {p1, v2, v1, v3, v4}, Landroid/graphics/Rect;->inset(IIII)V
/* .line 1790 */
} // .end local v0 # "freefromScale":F
} // :cond_1
} // :goto_0
/* goto/16 :goto_1 */
/* .line 1791 */
} // :cond_2
v0 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getFreeformScale ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeformScale(I)F
/* .line 1792 */
/* .restart local v0 # "freefromScale":F */
v3 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getFreeFormWindowMode ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeFormWindowMode(I)I
/* if-nez v3, :cond_3 */
/* .line 1793 */
v1 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v1, v0 */
/* float-to-double v1, v1 */
java.lang.Math .ceil ( v1,v2 );
/* move-result-wide v1 */
/* double-to-int v1, v1 */
/* mul-int/lit8 v1, v1, -0x1 */
/* .line 1794 */
v2 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v2, v0 */
/* float-to-double v2, v2 */
java.lang.Math .ceil ( v2,v3 );
/* move-result-wide v2 */
/* double-to-int v2, v2 */
/* mul-int/lit8 v2, v2, -0x1 */
/* .line 1793 */
(( android.graphics.Rect ) p1 ).inset ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/graphics/Rect;->inset(II)V
/* .line 1795 */
} // :cond_3
v3 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getFreeFormWindowMode ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeFormWindowMode(I)I
/* if-ne v3, v2, :cond_4 */
/* .line 1796 */
v2 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* .line 1797 */
v3 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* add-float/2addr v2, v3 */
/* div-float/2addr v2, v0 */
/* float-to-double v2, v2 */
/* .line 1796 */
java.lang.Math .ceil ( v2,v3 );
/* move-result-wide v2 */
/* double-to-int v2, v2 */
/* mul-int/lit8 v2, v2, -0x1 */
/* .line 1799 */
v3 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* .line 1800 */
v4 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* add-float/2addr v3, v4 */
/* div-float/2addr v3, v0 */
/* float-to-double v3, v3 */
/* .line 1799 */
java.lang.Math .ceil ( v3,v4 );
/* move-result-wide v3 */
/* double-to-int v3, v3 */
/* mul-int/lit8 v3, v3, -0x1 */
/* .line 1801 */
v4 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* div-float/2addr v4, v0 */
/* float-to-double v4, v4 */
java.lang.Math .ceil ( v4,v5 );
/* move-result-wide v4 */
/* double-to-int v4, v4 */
/* mul-int/lit8 v4, v4, -0x1 */
/* .line 1796 */
(( android.graphics.Rect ) p1 ).inset ( v2, v1, v3, v4 ); // invoke-virtual {p1, v2, v1, v3, v4}, Landroid/graphics/Rect;->inset(IIII)V
/* .line 1804 */
} // .end local v0 # "freefromScale":F
} // :cond_4
} // :goto_1
return;
} // .end method
public Boolean adjustMovedToTopIfNeed ( Boolean p0, com.android.server.wm.Task p1, Boolean p2, com.android.server.wm.Task p3 ) {
/* .locals 2 */
/* .param p1, "alreadyResumed" # Z */
/* .param p2, "topFocusableTask" # Lcom/android/server/wm/Task; */
/* .param p3, "movedToTop" # Z */
/* .param p4, "focusTask" # Lcom/android/server/wm/Task; */
/* .line 205 */
if ( p3 != null) { // if-eqz p3, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 206 */
v0 = (( com.android.server.wm.Task ) p2 ).inFreeformWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 207 */
v0 = (( com.android.server.wm.Task ) p2 ).isAlwaysOnTop ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->isAlwaysOnTop()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 208 */
v0 = (( com.android.server.wm.Task ) p4 ).isAlwaysOnTop ( ); // invoke-virtual {p4}, Lcom/android/server/wm/Task;->isAlwaysOnTop()Z
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 209 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "adjustMovedToTop false topFocusableTask: "; // const-string v1, "adjustMovedToTop false topFocusableTask: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 210 */
int p3 = 0; // const/4 p3, 0x0
/* .line 212 */
} // :cond_0
} // .end method
public com.android.server.wm.Task adjustedReuseableFreeformTask ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 8 */
/* .param p1, "startActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1269 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1270 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/Task;>;" */
v1 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_9
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1271 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;" */
/* check-cast v3, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1272 */
/* .local v3, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v3 != null) { // if-eqz v3, :cond_8
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v3 ).inPinMode ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
if ( v4 != null) { // if-eqz v4, :cond_8
v4 = this.mTask;
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 1273 */
v4 = this.intent;
(( android.content.Intent ) v4 ).getComponent ( ); // invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 1274 */
/* .local v4, "cls":Landroid/content/ComponentName; */
/* iget v5, p1, Lcom/android/server/wm/ActivityRecord;->mAnimationType:I */
v6 = this.mTask;
/* .line 1275 */
v6 = (( com.android.server.wm.Task ) v6 ).getActivityType ( ); // invoke-virtual {v6}, Lcom/android/server/wm/Task;->getActivityType()I
/* .line 1274 */
v5 = com.android.server.wm.ConfigurationContainer .isCompatibleActivityType ( v5,v6 );
/* if-nez v5, :cond_0 */
/* .line 1276 */
/* goto/16 :goto_1 */
/* .line 1279 */
} // :cond_0
v5 = this.mTask;
/* iget v5, v5, Lcom/android/server/wm/Task;->mUserId:I */
/* iget v6, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
/* if-eq v5, v6, :cond_1 */
/* .line 1280 */
/* goto/16 :goto_1 */
/* .line 1283 */
} // :cond_1
v5 = this.mTask;
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.wm.Task ) v5 ).getTopNonFinishingActivity ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity(Z)Lcom/android/server/wm/ActivityRecord;
/* .line 1285 */
/* .local v5, "r":Lcom/android/server/wm/ActivityRecord; */
if ( v5 != null) { // if-eqz v5, :cond_9
/* iget-boolean v6, v5, Lcom/android/server/wm/ActivityRecord;->finishing:Z */
/* if-nez v6, :cond_9 */
/* iget v6, v5, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
/* iget v7, p1, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
/* if-ne v6, v7, :cond_9 */
/* iget v6, v5, Lcom/android/server/wm/ActivityRecord;->launchMode:I */
int v7 = 3; // const/4 v7, 0x3
/* if-ne v6, v7, :cond_2 */
/* .line 1287 */
/* goto/16 :goto_1 */
/* .line 1290 */
} // :cond_2
v6 = (( com.android.server.wm.ActivityRecord ) v5 ).getActivityType ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord;->getActivityType()I
/* iget v7, p1, Lcom/android/server/wm/ActivityRecord;->mAnimationType:I */
v6 = com.android.server.wm.ConfigurationContainer .isCompatibleActivityType ( v6,v7 );
/* if-nez v6, :cond_3 */
/* .line 1292 */
/* .line 1295 */
} // :cond_3
v6 = this.mTask;
v6 = this.realActivity;
if ( v6 != null) { // if-eqz v6, :cond_4
v6 = this.mTask;
v6 = this.realActivity;
v6 = (( android.content.ComponentName ) v6 ).compareTo ( v4 ); // invoke-virtual {v6, v4}, Landroid/content/ComponentName;->compareTo(Landroid/content/ComponentName;)I
if ( v6 != null) { // if-eqz v6, :cond_6
} // :cond_4
v6 = this.mTask;
v6 = this.affinityIntent;
if ( v6 != null) { // if-eqz v6, :cond_5
v6 = this.mTask;
v6 = this.affinityIntent;
/* .line 1296 */
(( android.content.Intent ) v6 ).getComponent ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v6 != null) { // if-eqz v6, :cond_5
v6 = this.mTask;
v6 = this.affinityIntent;
/* .line 1297 */
(( android.content.Intent ) v6 ).getComponent ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
v6 = (( android.content.ComponentName ) v6 ).compareTo ( v4 ); // invoke-virtual {v6, v4}, Landroid/content/ComponentName;->compareTo(Landroid/content/ComponentName;)I
if ( v6 != null) { // if-eqz v6, :cond_6
} // :cond_5
v6 = this.mTask;
v6 = this.rootAffinity;
if ( v6 != null) { // if-eqz v6, :cond_8
v6 = this.mTask;
v6 = this.rootAffinity;
v7 = this.taskAffinity;
/* .line 1299 */
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_8
/* .line 1300 */
} // :cond_6
v6 = this.mActivityComponent;
v6 = android.util.MiuiMultiWindowAdapter .isAppLockActivity ( v6,v4 );
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 1301 */
v6 = this.intent;
final String v7 = "android.intent.extra.shortcut.NAME"; // const-string v7, "android.intent.extra.shortcut.NAME"
(( android.content.Intent ) v6 ).getStringExtra ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 1302 */
/* .local v6, "pkgBehindIntentActivity":Ljava/lang/String; */
if ( v6 != null) { // if-eqz v6, :cond_7
(( com.android.server.wm.MiuiFreeFormActivityStack ) v3 ).getStackPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v7 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v7, :cond_7 */
/* .line 1303 */
/* goto/16 :goto_0 */
/* .line 1305 */
} // .end local v6 # "pkgBehindIntentActivity":Ljava/lang/String;
} // :cond_7
v6 = this.mTask;
/* .line 1308 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
} // .end local v3 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v4 # "cls":Landroid/content/ComponentName;
} // .end local v5 # "r":Lcom/android/server/wm/ActivityRecord;
} // :cond_8
/* goto/16 :goto_0 */
/* .line 1309 */
} // :cond_9
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_a
int v1 = 0; // const/4 v1, 0x0
v1 = } // :cond_a
/* add-int/lit8 v1, v1, -0x1 */
/* check-cast v1, Lcom/android/server/wm/Task; */
} // :goto_2
} // .end method
public void allFreeformFormFreefromToMini ( ) {
/* .locals 2 */
/* .line 1166 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1167 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1174 */
return;
} // .end method
public Boolean appLockAlreadyInFront ( com.android.server.wm.Task p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 3 */
/* .param p1, "targetRootTask" # Lcom/android/server/wm/Task; */
/* .param p2, "startActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 2223 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
/* if-nez p2, :cond_0 */
/* .line 2226 */
} // :cond_0
v1 = (( com.android.server.wm.Task ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget v1, p1, Lcom/android/server/wm/Task;->mTaskId:I */
v1 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).isFrontFreeFormStackInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isFrontFreeFormStackInfo(I)Z
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.mActivityComponent;
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.mActivityComponent;
/* .line 2227 */
(( android.content.ComponentName ) v1 ).flattenToShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
final String v2 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v2, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
v1 = (( java.lang.String ) v2 ).contains ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* nop */
/* .line 2226 */
} // :goto_0
/* .line 2224 */
} // :cond_2
} // :goto_1
} // .end method
public Integer autoLayoutFreeFormStackIfNeed ( android.graphics.Rect p0, Float p1, android.graphics.Rect p2, com.android.server.wm.Task p3, java.lang.String p4, com.android.server.wm.MiuiFreeFormActivityStack p5 ) {
/* .locals 17 */
/* .param p1, "outBounds" # Landroid/graphics/Rect; */
/* .param p2, "scale" # F */
/* .param p3, "accessibleArea" # Landroid/graphics/Rect; */
/* .param p4, "excludedTask" # Lcom/android/server/wm/Task; */
/* .param p5, "pkg" # Ljava/lang/String; */
/* .param p6, "freeformTaskHasAppLock" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1593 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-object/from16 v2, p3 */
/* move-object/from16 v3, p4 */
/* move-object/from16 v4, p6 */
v5 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->width()I */
v6 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->height()I */
int v8 = 1; // const/4 v8, 0x1
/* if-le v5, v6, :cond_0 */
/* move v5, v8 */
} // :cond_0
int v5 = 0; // const/4 v5, 0x0
/* .line 1594 */
/* .local v5, "isDisplayLandscape":Z */
} // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_1
v6 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->width()I */
} // :cond_1
v6 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->height()I */
/* .line 1596 */
/* .local v6, "remainingSpace":I */
} // :goto_1
/* const/high16 v9, 0x41a00000 # 20.0f */
v9 = android.util.MiuiMultiWindowUtils .applyDip2Px ( v9 );
/* const/high16 v10, 0x3f000000 # 0.5f */
/* add-float/2addr v9, v10 */
/* float-to-int v9, v9 */
/* .line 1597 */
/* .local v9, "gap":I */
int v11 = 0; // const/4 v11, 0x0
/* .line 1598 */
/* .local v11, "frontNum":I */
v12 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v12 ).keySet ( ); // invoke-virtual {v12}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v13 = } // :goto_2
final String v14 = "MiuiFreeFormManagerService"; // const-string v14, "MiuiFreeFormManagerService"
if ( v13 != null) { // if-eqz v13, :cond_4
/* check-cast v13, Ljava/lang/Integer; */
/* .line 1599 */
/* .local v13, "taskId":Ljava/lang/Integer; */
v15 = (( java.lang.Integer ) v13 ).intValue ( ); // invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I
/* iget v7, v3, Lcom/android/server/wm/Task;->mTaskId:I */
/* if-ne v15, v7, :cond_2 */
/* .line 1600 */
} // :cond_2
v7 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v7 ).get ( v13 ); // invoke-virtual {v7, v13}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1601 */
/* .local v7, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
v15 = /* invoke-direct {v0, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isCandidateForAutoLayout(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z */
if ( v15 != null) { // if-eqz v15, :cond_3
/* .line 1603 */
v15 = this.mTask;
(( com.android.server.wm.Task ) v15 ).getBounds ( ); // invoke-virtual {v15}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
/* iget v10, v7, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
v10 = android.util.MiuiMultiWindowUtils .getVisualWidthOrHeight ( v5,v15,v10 );
/* sub-int/2addr v6, v10 */
/* .line 1604 */
/* sub-int/2addr v6, v9 */
/* .line 1605 */
/* add-int/lit8 v11, v11, 0x1 */
/* .line 1606 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "auto layout: CandidateForAutoLayout: "; // const-string v15, "auto layout: CandidateForAutoLayout: "
(( java.lang.StringBuilder ) v10 ).append ( v15 ); // invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v13 ); // invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v15 = " remainingSpace: "; // const-string v15, " remainingSpace: "
(( java.lang.StringBuilder ) v10 ).append ( v15 ); // invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v6 ); // invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v14,v10 );
/* .line 1608 */
} // .end local v7 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v13 # "taskId":Ljava/lang/Integer;
} // :cond_3
/* const/high16 v10, 0x3f000000 # 0.5f */
/* .line 1609 */
} // :cond_4
/* iget v7, v3, Lcom/android/server/wm/Task;->mTaskId:I */
v7 = (( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).getFrontFreeformNum ( v7 ); // invoke-virtual {v0, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFrontFreeformNum(I)I
/* .line 1610 */
/* .local v7, "freeformNum":I */
int v10 = 4; // const/4 v10, 0x4
/* if-lt v7, v10, :cond_5 */
/* move/from16 v16, v8 */
} // :cond_5
/* const/16 v16, 0x0 */
} // :goto_3
/* move/from16 v10, v16 */
/* .line 1611 */
/* .local v10, "replaceFreeForm":Z */
/* if-ne v11, v8, :cond_6 */
if ( v10 != null) { // if-eqz v10, :cond_6
/* .line 1612 */
final String v12 = "auto layout: There is only one freeform at the front desk and it will be replaced"; // const-string v12, "auto layout: There is only one freeform at the front desk and it will be replaced"
android.util.Slog .d ( v14,v12 );
/* .line 1613 */
/* add-int/lit8 v11, v11, -0x1 */
/* .line 1615 */
} // :cond_6
if ( v11 != null) { // if-eqz v11, :cond_8
v12 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAutoLayoutModeOn()Z */
if ( v12 != null) { // if-eqz v12, :cond_7
} // :cond_7
/* move/from16 v12, p2 */
/* goto/16 :goto_8 */
/* .line 1616 */
} // :cond_8
} // :goto_4
if ( v4 != null) { // if-eqz v4, :cond_9
/* .line 1617 */
/* add-int/2addr v6, v9 */
/* .line 1618 */
v12 = this.mTask;
(( com.android.server.wm.Task ) v12 ).getBounds ( ); // invoke-virtual {v12}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
/* iget v13, v4, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
v12 = android.util.MiuiMultiWindowUtils .getVisualWidthOrHeight ( v5,v12,v13 );
/* add-int/2addr v6, v12 */
/* .line 1621 */
v12 = this.mTask;
v12 = (( com.android.server.wm.Task ) v12 ).getRootTaskId ( ); // invoke-virtual {v12}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* invoke-direct {v0, v12}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFreeformParamsForAutoLayout(I)V */
/* .line 1622 */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "auto layout: found app lock is going to exit freeform: increase left space to "; // const-string v13, "auto layout: found app lock is going to exit freeform: increase left space to "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v6 ); // invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v14,v12 );
/* .line 1623 */
} // :cond_9
if ( v10 != null) { // if-eqz v10, :cond_a
/* .line 1624 */
/* iget v12, v3, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).getMiuiFreeFormActivityStackForMiuiFB ( v12 ); // invoke-virtual {v0, v12}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1625 */
/* .local v12, "excludedMffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
(( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).getReplaceFreeForm ( v12 ); // invoke-virtual {v0, v12}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getReplaceFreeForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1626 */
/* .local v13, "subMffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
v15 = /* invoke-direct {v0, v13}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isCandidateForAutoLayout(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z */
if ( v15 != null) { // if-eqz v15, :cond_a
/* .line 1628 */
/* add-int/2addr v6, v9 */
/* .line 1629 */
v15 = this.mTask;
(( com.android.server.wm.Task ) v15 ).getBounds ( ); // invoke-virtual {v15}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
/* iget v8, v13, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
v8 = android.util.MiuiMultiWindowUtils .getVisualWidthOrHeight ( v5,v15,v8 );
/* add-int/2addr v6, v8 */
/* .line 1632 */
v8 = this.mTask;
v8 = (( com.android.server.wm.Task ) v8 ).getRootTaskId ( ); // invoke-virtual {v8}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* invoke-direct {v0, v8}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFreeformParamsForAutoLayout(I)V */
/* .line 1633 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "auto layout: found one is going to exit freeform: increase left space to "; // const-string v15, "auto layout: found one is going to exit freeform: increase left space to "
(( java.lang.StringBuilder ) v8 ).append ( v15 ); // invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v14,v8 );
/* .line 1637 */
} // .end local v12 # "excludedMffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v13 # "subMffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_a
} // :goto_5
if ( v5 != null) { // if-eqz v5, :cond_b
v8 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->width()I */
/* sub-int/2addr v8, v6 */
/* .line 1638 */
} // :cond_b
v8 = /* invoke-virtual/range {p3 ..p3}, Landroid/graphics/Rect;->height()I */
/* sub-int/2addr v8, v6 */
} // :goto_6
/* nop */
/* .line 1639 */
/* .local v8, "freeformPlusGapSpace":I */
/* move/from16 v12, p2 */
v13 = android.util.MiuiMultiWindowUtils .getVisualWidthOrHeight ( v5,v1,v12 );
/* sub-int/2addr v6, v13 */
/* .line 1640 */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "auto layout: left space = "; // const-string v15, "auto layout: left space = "
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v6 ); // invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v15 = " freeformPlusGapSpace: "; // const-string v15, " freeformPlusGapSpace: "
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v8 ); // invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v15 = " outBounds: "; // const-string v15, " outBounds: "
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v1 ); // invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v14,v13 );
/* .line 1641 */
/* if-ltz v6, :cond_d */
/* .line 1642 */
/* div-int/lit8 v13, v6, 0x2 */
/* int-to-float v13, v13 */
/* const/high16 v15, 0x3f000000 # 0.5f */
/* add-float/2addr v13, v15 */
/* float-to-int v13, v13 */
/* .line 1643 */
/* .local v13, "margin":I */
if ( v5 != null) { // if-eqz v5, :cond_c
/* iget v15, v2, Landroid/graphics/Rect;->left:I */
/* add-int/2addr v15, v13 */
/* .line 1644 */
} // :cond_c
/* iget v15, v2, Landroid/graphics/Rect;->top:I */
/* add-int/2addr v15, v13 */
} // :goto_7
/* nop */
/* .line 1645 */
/* .local v15, "startPoint":I */
/* add-int/2addr v15, v8 */
/* .line 1646 */
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).updateAutoLayoutModeStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->updateAutoLayoutModeStatus(Z)V
/* .line 1647 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v0 = "auto layout: left start point = "; // const-string v0, "auto layout: left start point = "
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v14,v0 );
/* .line 1648 */
/* .line 1651 */
} // .end local v8 # "freeformPlusGapSpace":I
} // .end local v13 # "margin":I
} // .end local v15 # "startPoint":I
} // :cond_d
} // :goto_8
int v0 = -1; // const/4 v0, -0x1
} // .end method
public void autoLayoutOthersIfNeed ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 2232 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2235 */
/* .line 2233 */
/* :catch_0 */
/* move-exception v0 */
/* .line 2234 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 2236 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void clearFullScreenTasksBehindHome ( ) {
/* .locals 1 */
/* .line 2018 */
v0 = this.mAppBehindHome;
(( android.util.ArraySet ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->clear()V
/* .line 2020 */
return;
} // .end method
public void deliverResultForFinishActivity ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1, android.content.Intent p2 ) {
/* .locals 1 */
/* .param p1, "resultTo" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "resultFrom" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .line 2050 */
v0 = this.mFreeFormGestureController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2051 */
(( com.android.server.wm.MiuiFreeFormGestureController ) v0 ).deliverResultForFinishActivity ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverResultForFinishActivity(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;Landroid/content/Intent;)V
/* .line 2053 */
} // :cond_0
return;
} // .end method
public void deliverResultForResumeActivityInFreeform ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "resultTo" # Lcom/android/server/wm/ActivityRecord; */
/* .line 2056 */
v0 = this.mFreeFormGestureController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2057 */
(( com.android.server.wm.MiuiFreeFormGestureController ) v0 ).deliverResultForResumeActivityInFreeform ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverResultForResumeActivityInFreeform(Lcom/android/server/wm/ActivityRecord;)V
/* .line 2059 */
} // :cond_0
return;
} // .end method
public Boolean disableIgnoreOrientationRequest ( com.android.server.wm.TransitionController p0 ) {
/* .locals 2 */
/* .param p1, "transitionController" # Lcom/android/server/wm/TransitionController; */
/* .line 2130 */
v0 = (( com.android.server.wm.TransitionController ) p1 ).getCollectingTransitionType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/TransitionController;->getCollectingTransitionType()I
/* const v1, 0x7fffff97 */
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean disableSplashScreenForWpsInFreeForm ( android.content.ComponentName p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .param p2, "userId" # I */
/* .line 1478 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 1479 */
/* .line 1481 */
} // :cond_0
final String v1 = "cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity"; // const-string v1, "cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity"
(( android.content.ComponentName ) p1 ).flattenToShortString ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1482 */
(( android.content.ComponentName ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStack ( v1, p2 ); // invoke-virtual {p0, v1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(Ljava/lang/String;I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1483 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1485 */
} // :cond_1
} // .end method
public void dispatchFreeFormStackModeChanged ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "action" # I */
/* .param p2, "taskId" # I */
/* .line 1047 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1048 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1049 */
/* .local v1, "i":I */
} // :goto_0
/* const/16 v2, 0x15 */
/* if-gt v1, v2, :cond_1 */
/* .line 1050 */
int v2 = 1; // const/4 v2, 0x1
/* shl-int/2addr v2, v1 */
/* .line 1051 */
/* .local v2, "mask":I */
/* and-int v3, p1, v2 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1052 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).dispatchFreeFormStackModeChanged ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 1049 */
} // .end local v2 # "mask":I
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1055 */
} // .end local v1 # "i":I
} // :cond_1
return;
} // .end method
public void dispatchFreeFormStackModeChanged ( Integer p0, com.android.server.wm.MiuiFreeFormActivityStack p1 ) {
/* .locals 2 */
/* .param p1, "action" # I */
/* .param p2, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1058 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1059 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda8; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda8;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1089 */
return;
} // .end method
public void displayConfigurationChange ( com.android.server.wm.DisplayContent p0, android.content.res.Configuration p1 ) {
/* .locals 1 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "configuration" # Landroid/content/res/Configuration; */
/* .line 1769 */
v0 = this.mFreeFormGestureController;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mMiuiMultiWindowRecommendHelper;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1770 */
v0 = this.mFreeFormGestureController;
v0 = this.mMiuiMultiWindowRecommendHelper;
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) v0 ).displayConfigurationChange ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->displayConfigurationChange(Lcom/android/server/wm/DisplayContent;Landroid/content/res/Configuration;)V
/* .line 1772 */
v0 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v0 ).displayConfigurationChange ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->displayConfigurationChange(Lcom/android/server/wm/DisplayContent;Landroid/content/res/Configuration;)V
/* .line 1774 */
} // :cond_0
return;
} // .end method
public void exitAllFreeform ( ) {
/* .locals 4 */
/* .line 2206 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getAllMiuiFreeFormActivityStack ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getAllMiuiFreeFormActivityStack()Ljava/util/List;
/* .line 2207 */
/* .local v0, "miuiFreeFormActivityStackList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/MiuiFreeFormActivityStack;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 2208 */
/* .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 2209 */
v3 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v3 ).startExitApplication ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 2211 */
} // .end local v2 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* .line 2212 */
} // :cond_1
return;
} // .end method
public void exitFreeformIfEnterSplitScreen ( com.android.server.wm.Task p0, com.android.server.wm.Task p1 ) {
/* .locals 3 */
/* .param p1, "candidateTask" # Lcom/android/server/wm/Task; */
/* .param p2, "candidateRoot" # Lcom/android/server/wm/Task; */
/* .line 1925 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( com.android.server.wm.Task ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1926 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isLaunchForSplitMode(Lcom/android/server/wm/Task;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1927 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "exitFreeformEnterSplitScreen candidateTask: "; // const-string v1, "exitFreeformEnterSplitScreen candidateTask: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " candidateRoot: "; // const-string v1, " candidateRoot: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 1929 */
/* new-instance v0, Landroid/content/res/Configuration; */
(( com.android.server.wm.Task ) p1 ).getRequestedOverrideConfiguration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRequestedOverrideConfiguration()Landroid/content/res/Configuration;
/* invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V */
/* .line 1930 */
/* .local v0, "c":Landroid/content/res/Configuration; */
v1 = this.windowConfiguration;
int v2 = 0; // const/4 v2, 0x0
(( android.app.WindowConfiguration ) v1 ).setAlwaysOnTop ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/WindowConfiguration;->setAlwaysOnTop(Z)V
/* .line 1931 */
v1 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v1 ).setWindowingMode ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/WindowConfiguration;->setWindowingMode(I)V
/* .line 1932 */
v1 = this.windowConfiguration;
int v2 = 0; // const/4 v2, 0x0
(( android.app.WindowConfiguration ) v1 ).setBounds ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V
/* .line 1933 */
(( com.android.server.wm.Task ) p1 ).onRequestedOverrideConfigurationChanged ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->onRequestedOverrideConfigurationChanged(Landroid/content/res/Configuration;)V
/* .line 1935 */
} // .end local v0 # "c":Landroid/content/res/Configuration;
} // :cond_0
return;
} // .end method
public void freeformFullscreenTask ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1832 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1833 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda17; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda17;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1840 */
return;
} // .end method
public void freeformKillAll ( ) {
/* .locals 2 */
/* .line 1843 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1844 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1851 */
return;
} // .end method
public void freeformToFullscreenByBottomCaption ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "taskId" # I */
/* .line 2069 */
v0 = this.mActivityTaskManagerService;
v0 = this.mWindowManager;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 2070 */
try { // :try_start_0
v1 = this.mFreeFormGestureController;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2071 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStack ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
/* check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 2072 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
final String v2 = "MiuiFreeFormManagerService"; // const-string v2, "MiuiFreeFormManagerService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " freeformToFullscreenByBottomCaption: taskId= "; // const-string v4, " freeformToFullscreenByBottomCaption: taskId= "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 2073 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2074 */
v2 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v2 ).deliverResultForExitFreeform ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverResultForExitFreeform(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 2077 */
} // .end local v1 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* monitor-exit v0 */
/* .line 2078 */
return;
/* .line 2077 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void fromFreefromToMini ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1177 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1178 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1185 */
return;
} // .end method
public void fromSoscToFreeform ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .line 1237 */
v0 = this.mFreeformModeControl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1239 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1242 */
/* .line 1240 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1241 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
final String v2 = "fromSoscToFreeform."; // const-string v2, "fromSoscToFreeform."
android.util.Slog .e ( v1,v2,v0 );
/* .line 1244 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
public void fullscreenFreeformTask ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1821 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1822 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1829 */
return;
} // .end method
public java.util.List getAllFreeFormStackInfosOnDisplay ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "displayId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 912 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 914 */
/* .local v0, "ident":J */
try { // :try_start_0
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 915 */
/* .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;" */
int v3 = -1; // const/4 v3, -0x1
final String v4 = "MiuiFreeFormManagerService"; // const-string v4, "MiuiFreeFormManagerService"
/* if-ne v3, p1, :cond_2 */
/* .line 916 */
try { // :try_start_1
v3 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).keySet ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_1
/* check-cast v5, Ljava/lang/Integer; */
/* .line 917 */
/* .local v5, "taskId":Ljava/lang/Integer; */
v6 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v6 ).get ( v5 ); // invoke-virtual {v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 918 */
/* .local v6, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v6 != null) { // if-eqz v6, :cond_0
v7 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
v7 = miui.app.MiuiFreeFormManager .isFrontFreeFormStackInfo ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 919 */
v7 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
v7 = miui.app.MiuiFreeFormManager .isHideStackFromFullScreen ( v7 );
/* if-nez v7, :cond_0 */
v7 = this.mAppBehindHome;
/* .line 920 */
v7 = (( android.util.ArraySet ) v7 ).contains ( v5 ); // invoke-virtual {v7, v5}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v7, :cond_0 */
/* .line 922 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v6 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v6}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* .line 924 */
} // .end local v5 # "taskId":Ljava/lang/Integer;
} // .end local v6 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* .line 925 */
} // :cond_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getAllFreeFormStackInfosOnDisplay INVALID_DISPLAY list="; // const-string v5, "getAllFreeFormStackInfosOnDisplay INVALID_DISPLAY list="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.wm.MiuiFreeFormManagerService .logd ( v4,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 926 */
/* nop */
/* .line 941 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 926 */
/* .line 928 */
} // :cond_2
try { // :try_start_2
v3 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).keySet ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_4
/* check-cast v5, Ljava/lang/Integer; */
/* .line 929 */
/* .restart local v5 # "taskId":Ljava/lang/Integer; */
v6 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v6 ).get ( v5 ); // invoke-virtual {v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 930 */
/* .restart local v6 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v6 != null) { // if-eqz v6, :cond_3
v7 = this.mTask;
v7 = (( com.android.server.wm.Task ) v7 ).getDisplayId ( ); // invoke-virtual {v7}, Lcom/android/server/wm/Task;->getDisplayId()I
/* if-ne v7, p1, :cond_3 */
/* .line 931 */
v7 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
v7 = miui.app.MiuiFreeFormManager .isFrontFreeFormStackInfo ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 932 */
v7 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
v7 = miui.app.MiuiFreeFormManager .isHideStackFromFullScreen ( v7 );
/* if-nez v7, :cond_3 */
v7 = this.mAppBehindHome;
/* .line 933 */
v7 = (( android.util.ArraySet ) v7 ).contains ( v5 ); // invoke-virtual {v7, v5}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v7, :cond_3 */
/* .line 935 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v6 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v6}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* .line 937 */
} // .end local v5 # "taskId":Ljava/lang/Integer;
} // .end local v6 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_3
/* .line 938 */
} // :cond_4
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getAllFreeFormStackInfosOnDisplay list="; // const-string v5, "getAllFreeFormStackInfosOnDisplay list="
v5 = (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.wm.MiuiFreeFormManagerService .logd ( v4,v3 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 939 */
/* nop */
/* .line 941 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 939 */
/* .line 941 */
} // .end local v2 # "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 942 */
/* throw v2 */
} // .end method
public java.util.List getAllFrontFreeFormStackInfosOnDesktopMode ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "displayId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 946 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 948 */
/* .local v0, "ident":J */
try { // :try_start_0
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 949 */
/* .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;" */
int v3 = -1; // const/4 v3, -0x1
/* if-ne v3, p1, :cond_2 */
/* .line 950 */
v3 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).keySet ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/lang/Integer; */
/* .line 951 */
/* .local v4, "taskId":Ljava/lang/Integer; */
v5 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v5 ).get ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 952 */
/* .local v5, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v5 != null) { // if-eqz v5, :cond_0
v6 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
v6 = miui.app.MiuiFreeFormManager .isFrontFreeFormStackInfo ( v6 );
if ( v6 != null) { // if-eqz v6, :cond_0
v6 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
v6 = miui.app.MiuiFreeFormManager .isHideStackFromFullScreen ( v6 );
/* if-nez v6, :cond_0 */
/* .line 953 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 955 */
} // .end local v4 # "taskId":Ljava/lang/Integer;
} // .end local v5 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* .line 956 */
} // :cond_1
/* nop */
/* .line 967 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 956 */
/* .line 958 */
} // :cond_2
try { // :try_start_1
v3 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).keySet ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Ljava/lang/Integer; */
/* .line 959 */
/* .restart local v4 # "taskId":Ljava/lang/Integer; */
v5 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v5 ).get ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 960 */
/* .restart local v5 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v5 != null) { // if-eqz v5, :cond_3
v6 = this.mTask;
v6 = (( com.android.server.wm.Task ) v6 ).getDisplayId ( ); // invoke-virtual {v6}, Lcom/android/server/wm/Task;->getDisplayId()I
/* if-ne v6, p1, :cond_3 */
/* .line 961 */
v6 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
v6 = miui.app.MiuiFreeFormManager .isFrontFreeFormStackInfo ( v6 );
if ( v6 != null) { // if-eqz v6, :cond_3
v6 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
v6 = miui.app.MiuiFreeFormManager .isHideStackFromFullScreen ( v6 );
/* if-nez v6, :cond_3 */
/* .line 962 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 964 */
} // .end local v4 # "taskId":Ljava/lang/Integer;
} // .end local v5 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_3
/* .line 965 */
} // :cond_4
/* nop */
/* .line 967 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 965 */
/* .line 967 */
} // .end local v2 # "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 968 */
/* throw v2 */
} // .end method
public java.util.List getAllMiuiFreeFormActivityStack ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/wm/MiuiFreeFormActivityStack;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 652 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 653 */
/* .local v0, "stackList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/MiuiFreeFormActivityStack;>;" */
v1 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 654 */
/* .local v2, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 655 */
} // .end local v2 # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 656 */
} // :cond_0
} // .end method
public java.util.List getAllPinedFreeFormStackInfosOnDisplay ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "displayId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 971 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 973 */
/* .local v0, "ident":J */
try { // :try_start_0
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 974 */
/* .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;" */
int v3 = -1; // const/4 v3, -0x1
/* if-ne v3, p1, :cond_2 */
/* .line 975 */
v3 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).entrySet ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 976 */
/* .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;" */
/* check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 977 */
/* .local v5, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v5 != null) { // if-eqz v5, :cond_0
v6 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).inPinMode ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 978 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 980 */
} // .end local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
} // .end local v5 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* .line 981 */
} // :cond_1
/* nop */
/* .line 991 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 981 */
/* .line 983 */
} // :cond_2
try { // :try_start_1
v3 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).entrySet ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 984 */
/* .restart local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;" */
/* check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 985 */
/* .restart local v5 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v5 != null) { // if-eqz v5, :cond_3
v6 = this.mTask;
v6 = (( com.android.server.wm.Task ) v6 ).getDisplayId ( ); // invoke-virtual {v6}, Lcom/android/server/wm/Task;->getDisplayId()I
/* if-ne v6, p1, :cond_3 */
v6 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).inPinMode ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 986 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 988 */
} // .end local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
} // .end local v5 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_3
/* .line 989 */
} // :cond_4
/* nop */
/* .line 991 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 989 */
/* .line 991 */
} // .end local v2 # "list":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 992 */
/* throw v2 */
} // .end method
public java.lang.String getApplicationUsedInFreeform ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 1104 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "application_used_freeform"; // const-string v1, "application_used_freeform"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$Secure .getStringForUser ( v0,v1,v2 );
} // .end method
public com.android.server.wm.MiuiFreeFormActivityStack getBottomGameFreeFormActivityStack ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 4 */
/* .param p1, "addingStack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 887 */
v0 = this.mActivityTaskManagerService;
v0 = this.mWindowManager;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 888 */
try { // :try_start_0
v1 = this.mActivityTaskManagerService;
v1 = this.mRootWindowContainer;
/* .line 889 */
(( com.android.server.wm.RootWindowContainer ) v1 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 892 */
/* .local v1, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea; */
/* new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda10; */
/* invoke-direct {v2, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda10;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/MiuiFreeFormActivityStack;)V */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.wm.TaskDisplayArea ) v1 ).getTask ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;
/* .line 905 */
/* .local v2, "bottomTask":Lcom/android/server/wm/Task; */
/* if-nez v2, :cond_0 */
/* .line 906 */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 908 */
} // :cond_0
v3 = (( com.android.server.wm.Task ) v2 ).getRootTaskId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* monitor-exit v0 */
/* .line 909 */
} // .end local v1 # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
} // .end local v2 # "bottomTask":Lcom/android/server/wm/Task;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Integer getCurrentMiuiFreeFormNum ( ) {
/* .locals 1 */
/* .line 376 */
v0 = this.mFreeFormActivityStacks;
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I
} // .end method
public miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo getFreeFormStackInfoByActivity ( android.os.IBinder p0 ) {
/* .locals 6 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 996 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 998 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mActivityTaskManagerService;
v2 = this.mWindowManager;
v2 = this.mGlobalLock;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 999 */
try { // :try_start_1
com.android.server.wm.ActivityRecord .isInRootTaskLocked ( p1 );
/* .line 1000 */
/* .local v3, "r":Lcom/android/server/wm/ActivityRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1001 */
v4 = (( com.android.server.wm.ActivityRecord ) v3 ).getRootTaskId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1002 */
/* .local v4, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1003 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v4 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1009 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1003 */
/* .line 1006 */
} // .end local v4 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
try { // :try_start_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1009 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1006 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1007 */
} // .end local v3 # "r":Lcom/android/server/wm/ActivityRecord;
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
/* monitor-exit v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v0 # "ident":J
} // .end local p0 # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
} // .end local p1 # "token":Landroid/os/IBinder;
try { // :try_start_4
/* throw v3 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 1009 */
/* .restart local v0 # "ident":J */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiFreeFormManagerService; */
/* .restart local p1 # "token":Landroid/os/IBinder; */
/* :catchall_1 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1010 */
/* throw v2 */
} // .end method
public miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo getFreeFormStackInfoByStackId ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "stackId" # I */
/* .line 1032 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 1034 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mActivityTaskManagerService;
v2 = this.mWindowManager;
v2 = this.mGlobalLock;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1035 */
try { // :try_start_1
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1036 */
/* .local v3, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1037 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v3 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1042 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1037 */
/* .line 1039 */
} // :cond_0
try { // :try_start_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1042 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1039 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1040 */
} // .end local v3 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
/* monitor-exit v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v0 # "ident":J
} // .end local p0 # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
} // .end local p1 # "stackId":I
try { // :try_start_4
/* throw v3 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 1042 */
/* .restart local v0 # "ident":J */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiFreeFormManagerService; */
/* .restart local p1 # "stackId":I */
/* :catchall_1 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1043 */
/* throw v2 */
} // .end method
public miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo getFreeFormStackInfoByWindow ( android.os.IBinder p0 ) {
/* .locals 6 */
/* .param p1, "wtoken" # Landroid/os/IBinder; */
/* .line 1014 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 1016 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mActivityTaskManagerService;
v2 = this.mWindowManager;
v2 = this.mGlobalLock;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1017 */
try { // :try_start_1
v3 = this.mActivityTaskManagerService;
v3 = this.mWindowManager;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.wm.WindowManagerService ) v3 ).windowForClientLocked ( v5, p1, v4 ); // invoke-virtual {v3, v5, p1, v4}, Lcom/android/server/wm/WindowManagerService;->windowForClientLocked(Lcom/android/server/wm/Session;Landroid/os/IBinder;Z)Lcom/android/server/wm/WindowState;
/* .line 1018 */
/* .local v3, "win":Lcom/android/server/wm/WindowState; */
if ( v3 != null) { // if-eqz v3, :cond_0
v4 = this.mActivityRecord;
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1019 */
v4 = this.mActivityRecord;
v4 = (( com.android.server.wm.ActivityRecord ) v4 ).getRootTaskId ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1020 */
/* .local v4, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1021 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v4 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1027 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1021 */
/* .line 1024 */
} // .end local v4 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
try { // :try_start_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1027 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1024 */
/* .line 1025 */
} // .end local v3 # "win":Lcom/android/server/wm/WindowState;
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
/* monitor-exit v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v0 # "ident":J
} // .end local p0 # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
} // .end local p1 # "wtoken":Landroid/os/IBinder;
try { // :try_start_4
/* throw v3 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 1027 */
/* .restart local v0 # "ident":J */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiFreeFormManagerService; */
/* .restart local p1 # "wtoken":Landroid/os/IBinder; */
/* :catchall_1 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1028 */
/* throw v2 */
} // .end method
public miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo getFreeFormStackToAvoid ( Integer p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "displayId" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 1554 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 1556 */
/* .local v0, "ident":J */
try { // :try_start_0
v2 = this.mActivityTaskManagerService;
v2 = this.mRootWindowContainer;
/* .line 1557 */
(( com.android.server.wm.RootWindowContainer ) v2 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v2}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 1559 */
/* .local v2, "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea; */
v3 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).values ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v4 = } // :goto_0
int v5 = 0; // const/4 v5, 0x0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1560 */
/* .local v4, "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
v6 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v4 ).isInMiniFreeFormMode ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
/* if-nez v6, :cond_1 */
v6 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v4 ).inPinMode ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 1563 */
} // .end local v4 # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* .line 1561 */
/* .restart local v4 # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
} // :cond_1
} // :goto_1
/* nop */
/* .line 1586 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1561 */
/* .line 1564 */
} // .end local v4 # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_2
try { // :try_start_1
v3 = this.mActivityTaskManagerService;
v3 = this.mWindowManager;
v3 = this.mGlobalLock;
/* monitor-enter v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 1565 */
try { // :try_start_2
/* new-instance v4, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda0; */
/* invoke-direct {v4, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V */
int v6 = 1; // const/4 v6, 0x1
(( com.android.server.wm.TaskDisplayArea ) v2 ).getTask ( v4, v6 ); // invoke-virtual {v2, v4, v6}, Lcom/android/server/wm/TaskDisplayArea;->getTask(Ljava/util/function/Predicate;Z)Lcom/android/server/wm/Task;
/* .line 1580 */
/* .local v4, "topTaskToAvoid":Lcom/android/server/wm/Task; */
/* if-nez v4, :cond_3 */
/* .line 1581 */
/* monitor-exit v3 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1586 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1581 */
/* .line 1583 */
} // :cond_3
try { // :try_start_3
v5 = (( com.android.server.wm.Task ) v4 ).getRootTaskId ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
(( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* monitor-exit v3 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1586 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1583 */
/* .line 1584 */
} // .end local v4 # "topTaskToAvoid":Lcom/android/server/wm/Task;
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_4
/* monitor-exit v3 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
} // .end local v0 # "ident":J
} // .end local p0 # "this":Lcom/android/server/wm/MiuiFreeFormManagerService;
} // .end local p1 # "displayId":I
} // .end local p2 # "packageName":Ljava/lang/String;
try { // :try_start_5
/* throw v4 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 1586 */
} // .end local v2 # "defaultTaskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
/* .restart local v0 # "ident":J */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiFreeFormManagerService; */
/* .restart local p1 # "displayId":I */
/* .restart local p2 # "packageName":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1587 */
/* throw v2 */
} // .end method
public Integer getFreeFormWindowMode ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "rootStackId" # I */
/* .line 671 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 672 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_0 */
/* .line 673 */
int v1 = -1; // const/4 v1, -0x1
/* .line 675 */
} // :cond_0
/* iget v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mMiuiFreeFromWindowMode:I */
} // .end method
public android.graphics.Rect getFreeformLastPosition ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1959 */
v0 = this.mActivityTaskManagerService;
v0 = this.mRootWindowContainer;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).anyTaskForId ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;
/* .line 1960 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
v1 = this.mActivityTaskManagerService;
v1 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v1 ).getLaunchParamsController ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
(( com.android.server.wm.LaunchParamsController ) v1 ).getFreeformLastPosition ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastPosition(Lcom/android/server/wm/Task;)Landroid/graphics/Rect;
} // .end method
public Float getFreeformLastScale ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1954 */
v0 = this.mActivityTaskManagerService;
v0 = this.mRootWindowContainer;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).anyTaskForId ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;
/* .line 1955 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
v1 = this.mActivityTaskManagerService;
v1 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v1 ).getLaunchParamsController ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
v1 = (( com.android.server.wm.LaunchParamsController ) v1 ).getFreeformLastScale ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F
} // .end method
public Float getFreeformRectDesktop ( android.graphics.Rect p0, Float p1, Integer p2, Boolean p3 ) {
/* .locals 19 */
/* .param p1, "outBounds" # Landroid/graphics/Rect; */
/* .param p2, "scale" # F */
/* .param p3, "taskId" # I */
/* .param p4, "isDefaultPosition" # Z */
/* .line 1701 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
/* move/from16 v0, p2 */
/* move/from16 v9, p3 */
v1 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
final String v10 = "getFreeformRectDesktop taskId="; // const-string v10, "getFreeformRectDesktop taskId="
final String v11 = "MiuiFreeFormManagerService"; // const-string v11, "MiuiFreeFormManagerService"
if ( v1 != null) { // if-eqz v1, :cond_10
/* .line 1702 */
int v1 = 0; // const/4 v1, 0x0
/* cmpg-float v1, v0, v1 */
/* if-lez v1, :cond_f */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* cmpl-float v1, v0, v1 */
/* if-lez v1, :cond_0 */
/* goto/16 :goto_3 */
/* .line 1706 */
} // :cond_0
v1 = this.mActivityTaskManagerService;
v1 = this.mRootWindowContainer;
int v12 = 0; // const/4 v12, 0x0
(( com.android.server.wm.RootWindowContainer ) v1 ).anyTaskForId ( v9, v12 ); // invoke-virtual {v1, v9, v12}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;
/* .line 1707 */
/* .local v13, "task":Lcom/android/server/wm/Task; */
/* if-nez v13, :cond_1 */
/* .line 1708 */
} // :cond_1
v1 = this.mActivityTaskManagerService;
v1 = this.mWindowManager;
v1 = this.mContext;
v2 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
android.util.MiuiMultiWindowUtils .getFreeFormAccessibleArea ( v1,v2 );
/* .line 1709 */
/* .local v14, "accessibleArea":Landroid/graphics/Rect; */
/* invoke-static/range {p1 ..p2}, Landroid/util/MiuiMultiWindowUtils;->getVisualBounds(Landroid/graphics/Rect;F)Landroid/graphics/Rect; */
/* .line 1710 */
/* .local v1, "visibleBounds":Landroid/graphics/Rect; */
v2 = this.origActivity;
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.origActivity;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1711 */
} // :cond_2
v2 = this.realActivity;
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = this.realActivity;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1712 */
} // :cond_3
(( com.android.server.wm.Task ) v13 ).getTopNonFinishingActivity ( ); // invoke-virtual {v13}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1713 */
(( com.android.server.wm.Task ) v13 ).getTopNonFinishingActivity ( ); // invoke-virtual {v13}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
v2 = this.packageName;
} // :cond_4
/* const-string/jumbo v2, "unknown" */
} // :goto_0
/* move-object v15, v2 */
/* .line 1714 */
/* .local v15, "packageName":Ljava/lang/String; */
int v6 = 1; // const/4 v6, 0x1
if ( p4 != null) { // if-eqz p4, :cond_5
/* .line 1715 */
v2 = this.mActivityTaskManagerService;
v2 = this.mWindowManager;
v2 = this.mContext;
android.util.MiuiMultiWindowUtils .getActivityOptions ( v2,v15,v6,v12,v6 );
/* .line 1717 */
(( android.app.ActivityOptions ) v2 ).getLaunchBounds ( ); // invoke-virtual {v2}, Landroid/app/ActivityOptions;->getLaunchBounds()Landroid/graphics/Rect;
/* .line 1718 */
/* .local v2, "launchBounds":Landroid/graphics/Rect; */
/* iget v3, v2, Landroid/graphics/Rect;->left:I */
/* iget v4, v2, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v1 ).offsetTo ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1720 */
} // .end local v2 # "launchBounds":Landroid/graphics/Rect;
} // :cond_5
v2 = (( android.graphics.Rect ) v1 ).width ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->width()I
v3 = (( android.graphics.Rect ) v14 ).width ( ); // invoke-virtual {v14}, Landroid/graphics/Rect;->width()I
/* if-gt v2, v3, :cond_6 */
v2 = (( android.graphics.Rect ) v1 ).height ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->height()I
v3 = (( android.graphics.Rect ) v14 ).height ( ); // invoke-virtual {v14}, Landroid/graphics/Rect;->height()I
/* if-le v2, v3, :cond_7 */
/* .line 1721 */
} // :cond_6
v2 = /* invoke-direct {v7, v0, v8, v14}, Lcom/android/server/wm/MiuiFreeFormManagerService;->scaleDownIfNeeded(FLandroid/graphics/Rect;Landroid/graphics/Rect;)F */
/* .line 1722 */
/* .local v2, "newscale":F */
/* cmpl-float v3, v2, v0 */
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 1723 */
/* move v0, v2 */
/* .line 1724 */
} // .end local p2 # "scale":F
/* .local v0, "scale":F */
android.util.MiuiMultiWindowUtils .getVisualBounds ( v8,v0 );
/* .line 1725 */
v3 = this.mCorrectScaleList;
/* iget v4, v13, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v4 );
java.lang.Float .valueOf ( v2 );
/* .line 1726 */
(( com.android.server.wm.Task ) v13 ).setMiuiFreeformScale ( v2 ); // invoke-virtual {v13, v2}, Lcom/android/server/wm/Task;->setMiuiFreeformScale(F)V
/* .line 1727 */
(( com.android.server.wm.Task ) v13 ).setLastNonFullscreenBounds ( v8 ); // invoke-virtual {v13, v8}, Lcom/android/server/wm/Task;->setLastNonFullscreenBounds(Landroid/graphics/Rect;)V
/* .line 1728 */
v3 = this.mActivityTaskManagerService;
v3 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v3 ).getLaunchParamsController ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
(( com.android.server.wm.LaunchParamsController ) v3 ).saveFreeformDesktopMemory ( v13 ); // invoke-virtual {v3, v13}, Lcom/android/server/wm/LaunchParamsController;->saveFreeformDesktopMemory(Lcom/android/server/wm/Task;)V
/* move v5, v0 */
/* move-object v4, v1 */
/* .line 1732 */
} // .end local v0 # "scale":F
} // .end local v2 # "newscale":F
/* .restart local p2 # "scale":F */
} // :cond_7
/* move v5, v0 */
/* move-object v4, v1 */
} // .end local v1 # "visibleBounds":Landroid/graphics/Rect;
} // .end local p2 # "scale":F
/* .local v4, "visibleBounds":Landroid/graphics/Rect; */
/* .local v5, "scale":F */
} // :goto_1
/* invoke-direct {v7, v13, v15}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeformTaskHasAppLock(Lcom/android/server/wm/Task;Ljava/lang/String;)Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1733 */
/* .local v3, "freeformTaskHasAppLock":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 1734 */
v0 = this.mTask;
(( com.android.server.wm.Task ) v0 ).getBounds ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
/* .line 1735 */
/* .local v0, "posBeforeUnlocking":Landroid/graphics/Rect; */
/* iget v1, v0, Landroid/graphics/Rect;->left:I */
/* iget v2, v0, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v8 ).offsetTo ( v1, v2 ); // invoke-virtual {v8, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1736 */
/* iget v1, v8, Landroid/graphics/Rect;->left:I */
/* iget v2, v8, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v4 ).offsetTo ( v1, v2 ); // invoke-virtual {v4, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1737 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " unlock app lock and stay at"; // const-string v2, " unlock app lock and stay at"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v11,v1 );
/* .line 1740 */
} // .end local v0 # "posBeforeUnlocking":Landroid/graphics/Rect;
} // :cond_8
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move v2, v5 */
/* move-object/from16 v16, v3 */
} // .end local v3 # "freeformTaskHasAppLock":Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .local v16, "freeformTaskHasAppLock":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* move-object v3, v14 */
/* move-object v12, v4 */
} // .end local v4 # "visibleBounds":Landroid/graphics/Rect;
/* .local v12, "visibleBounds":Landroid/graphics/Rect; */
/* move-object v4, v13 */
/* move-object/from16 v17, v13 */
/* move v13, v5 */
} // .end local v5 # "scale":F
/* .local v13, "scale":F */
/* .local v17, "task":Lcom/android/server/wm/Task; */
/* move-object v5, v15 */
/* move/from16 v18, v6 */
/* move-object/from16 v6, v16 */
v0 = /* invoke-virtual/range {v0 ..v6}, Lcom/android/server/wm/MiuiFreeFormManagerService;->autoLayoutFreeFormStackIfNeed(Landroid/graphics/Rect;FLandroid/graphics/Rect;Lcom/android/server/wm/Task;Ljava/lang/String;Lcom/android/server/wm/MiuiFreeFormActivityStack;)I */
/* .line 1743 */
/* .local v0, "startPoint":I */
/* if-lez v0, :cond_a */
/* .line 1744 */
android.util.MiuiMultiWindowUtils .scaleBounds ( v8,v13 );
/* .line 1745 */
/* .local v1, "tempScaledBounds":Landroid/graphics/Rect; */
v2 = (( android.graphics.Rect ) v14 ).width ( ); // invoke-virtual {v14}, Landroid/graphics/Rect;->width()I
v3 = (( android.graphics.Rect ) v14 ).height ( ); // invoke-virtual {v14}, Landroid/graphics/Rect;->height()I
/* if-le v2, v3, :cond_9 */
/* move/from16 v2, v18 */
} // :cond_9
int v2 = 0; // const/4 v2, 0x0
} // :goto_2
/* invoke-direct {v7, v1, v14, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->centerRect(Landroid/graphics/Rect;Landroid/graphics/Rect;ZI)V */
/* .line 1746 */
/* iget v2, v1, Landroid/graphics/Rect;->left:I */
/* iget v3, v1, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v12 ).offsetTo ( v2, v3 ); // invoke-virtual {v12, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1747 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v10 ); // invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " auto layout move to"; // const-string v3, " auto layout move to"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v12 ); // invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v11,v2 );
/* .line 1750 */
} // .end local v1 # "tempScaledBounds":Landroid/graphics/Rect;
} // :cond_a
/* iget v1, v12, Landroid/graphics/Rect;->left:I */
/* iget v2, v14, Landroid/graphics/Rect;->left:I */
/* if-ge v1, v2, :cond_b */
/* .line 1751 */
/* iget v1, v14, Landroid/graphics/Rect;->left:I */
/* iget v2, v12, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v12 ).offsetTo ( v1, v2 ); // invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1753 */
} // :cond_b
/* iget v1, v12, Landroid/graphics/Rect;->top:I */
/* iget v2, v14, Landroid/graphics/Rect;->top:I */
/* if-ge v1, v2, :cond_c */
/* .line 1754 */
/* iget v1, v12, Landroid/graphics/Rect;->left:I */
/* iget v2, v14, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v12 ).offsetTo ( v1, v2 ); // invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1756 */
} // :cond_c
/* iget v1, v12, Landroid/graphics/Rect;->left:I */
v2 = (( android.graphics.Rect ) v12 ).width ( ); // invoke-virtual {v12}, Landroid/graphics/Rect;->width()I
/* add-int/2addr v1, v2 */
/* iget v2, v14, Landroid/graphics/Rect;->right:I */
/* if-le v1, v2, :cond_d */
/* .line 1757 */
/* iget v1, v14, Landroid/graphics/Rect;->right:I */
v2 = (( android.graphics.Rect ) v12 ).width ( ); // invoke-virtual {v12}, Landroid/graphics/Rect;->width()I
/* sub-int/2addr v1, v2 */
/* iget v2, v12, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v12 ).offsetTo ( v1, v2 ); // invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1759 */
} // :cond_d
/* iget v1, v12, Landroid/graphics/Rect;->top:I */
v2 = (( android.graphics.Rect ) v12 ).height ( ); // invoke-virtual {v12}, Landroid/graphics/Rect;->height()I
/* add-int/2addr v1, v2 */
/* iget v2, v14, Landroid/graphics/Rect;->bottom:I */
/* if-le v1, v2, :cond_e */
/* .line 1760 */
/* iget v1, v12, Landroid/graphics/Rect;->left:I */
/* iget v2, v14, Landroid/graphics/Rect;->bottom:I */
v3 = (( android.graphics.Rect ) v12 ).height ( ); // invoke-virtual {v12}, Landroid/graphics/Rect;->height()I
/* sub-int/2addr v2, v3 */
(( android.graphics.Rect ) v12 ).offsetTo ( v1, v2 ); // invoke-virtual {v12, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1762 */
} // :cond_e
/* iget v1, v12, Landroid/graphics/Rect;->left:I */
/* iget v2, v12, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v8 ).offsetTo ( v1, v2 ); // invoke-virtual {v8, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1703 */
} // .end local v0 # "startPoint":I
} // .end local v12 # "visibleBounds":Landroid/graphics/Rect;
} // .end local v13 # "scale":F
} // .end local v14 # "accessibleArea":Landroid/graphics/Rect;
} // .end local v15 # "packageName":Ljava/lang/String;
} // .end local v16 # "freeformTaskHasAppLock":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v17 # "task":Lcom/android/server/wm/Task;
/* .restart local p2 # "scale":F */
} // :cond_f
} // :goto_3
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getFreeformRectDesktop failed! taskId="; // const-string v2, "getFreeformRectDesktop failed! taskId="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " scale="; // const-string v2, " scale="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v1 );
/* .line 1704 */
/* .line 1701 */
} // :cond_10
/* move v13, v0 */
/* .line 1764 */
} // .end local p2 # "scale":F
/* .restart local v13 # "scale":F */
} // :goto_4
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " ,scale="; // const-string v1, " ,scale="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", outBounds="; // const-string v1, ", outBounds="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v11,v0 );
/* .line 1765 */
} // .end method
public Float getFreeformScale ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 326 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 327 */
/* .local v0, "freeformScale":F */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 328 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 329 */
v0 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).getFreeFormScale ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormScale()F
/* .line 331 */
} // :cond_0
} // .end method
public Boolean getFreeformStackBounds ( com.android.server.wm.Task p0, Integer p1, Integer p2, android.graphics.Rect p3, android.graphics.Rect p4 ) {
/* .locals 18 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "oldRotation" # I */
/* .param p3, "newRotation" # I */
/* .param p4, "preBounds" # Landroid/graphics/Rect; */
/* .param p5, "newBounds" # Landroid/graphics/Rect; */
/* .line 2144 */
/* move-object/from16 v0, p0 */
v1 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->getRootTaskId()I */
(( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).getMiuiFreeFormActivityStack ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
/* check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 2145 */
/* .local v1, "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
v3 = this.mTask;
/* if-nez v3, :cond_0 */
/* move/from16 v7, p3 */
/* move-object/from16 v5, p4 */
/* move-object/from16 v6, p5 */
/* goto/16 :goto_1 */
/* .line 2146 */
} // :cond_0
v3 = this.mActivityTaskManagerService;
v3 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v3 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v3}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 2147 */
int v4 = 5; // const/4 v4, 0x5
(( com.android.server.wm.TaskDisplayArea ) v3 ).getTopRootTaskInWindowingMode ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;
/* .line 2148 */
/* .local v3, "topRootTask":Lcom/android/server/wm/Task; */
v4 = this.mActivityTaskManagerService;
v4 = this.mWindowManager;
v5 = this.mContext;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* iget-boolean v9, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
/* .line 2149 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).getStackPackageName ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
int v12 = 1; // const/4 v12, 0x1
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget v10, v3, Lcom/android/server/wm/Task;->mTaskId:I */
/* .line 2150 */
v13 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->getRootTaskId()I */
/* if-eq v10, v13, :cond_1 */
/* move v13, v4 */
} // :cond_1
/* move v13, v2 */
} // :goto_0
int v14 = 0; // const/4 v14, 0x0
int v15 = 0; // const/4 v15, 0x0
/* iget-boolean v2, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
v10 = this.mActivityTaskManagerService;
v10 = this.mContext;
/* .line 2151 */
v17 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( v10 );
/* .line 2148 */
/* move-object/from16 v10, p5 */
/* move/from16 v16, v2 */
/* invoke-static/range {v5 ..v17}, Landroid/util/MiuiMultiWindowUtils;->getFreeformRect(Landroid/content/Context;ZZZZLandroid/graphics/Rect;Ljava/lang/String;ZZIIZZ)Landroid/graphics/Rect; */
/* .line 2152 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).setFreeformScale ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setFreeformScale(F)V
/* .line 2153 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " preBounds= "; // const-string v5, " preBounds= "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v5, p4 */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " newBounds= "; // const-string v6, " newBounds= "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v6, p5 */
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = " newRotation= "; // const-string v7, " newRotation= "
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v7, p3 */
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " scale= "; // const-string v8, " scale= "
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "MiuiFreeFormManagerService"; // const-string v8, "MiuiFreeFormManagerService"
com.android.server.wm.MiuiFreeFormManagerService .logd ( v8,v2 );
/* .line 2155 */
/* .line 2145 */
} // .end local v3 # "topRootTask":Lcom/android/server/wm/Task;
} // :cond_2
/* move/from16 v7, p3 */
/* move-object/from16 v5, p4 */
/* move-object/from16 v6, p5 */
} // :goto_1
} // .end method
public Integer getFrontFreeformNum ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "addingTaskId" # I */
/* .line 737 */
v0 = this.mActivityTaskManagerService;
v0 = this.mContext;
v0 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( v0 );
/* if-nez v0, :cond_0 */
/* .line 738 */
v0 = this.mFreeFormActivityStacks;
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I
/* .line 740 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 741 */
/* .local v0, "frontSize":I */
v1 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/Integer; */
/* .line 742 */
/* .local v2, "taskId":Ljava/lang/Integer; */
v3 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 743 */
/* .local v3, "mffas_temp":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v3 != null) { // if-eqz v3, :cond_1
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v3 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 744 */
v4 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
v4 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).isAppBehindHome ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z
/* if-nez v4, :cond_1 */
v4 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* if-eq v4, p1, :cond_1 */
/* .line 745 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 747 */
} // .end local v2 # "taskId":Ljava/lang/Integer;
} // .end local v3 # "mffas_temp":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_1
/* .line 748 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "current FrontFreeformNum = "; // const-string v2, "current FrontFreeformNum = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", addingTaskId="; // const-string v2, ", addingTaskId="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFreeFormManagerService"; // const-string v2, "MiuiFreeFormManagerService"
android.util.Slog .d ( v2,v1 );
/* .line 749 */
} // .end method
public Integer getFrontFreeformNum ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 3 */
/* .param p1, "addingStack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 727 */
int v0 = -1; // const/4 v0, -0x1
if ( p1 != null) { // if-eqz p1, :cond_0
v1 = this.mTask;
v1 = (( com.android.server.wm.Task ) v1 ).getRootTaskId ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I
} // :cond_0
/* move v1, v0 */
/* .line 728 */
/* .local v1, "addingTaskId":I */
} // :goto_0
if ( p1 != null) { // if-eqz p1, :cond_1
v2 = (( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 729 */
v2 = (( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).isHideStackFromFullScreen ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isHideStackFromFullScreen()Z
/* if-nez v2, :cond_1 */
/* .line 730 */
v2 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).isAppBehindHome ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z
/* if-nez v2, :cond_1 */
/* .line 731 */
v0 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getFrontFreeformNum ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFrontFreeformNum(I)I
/* .line 733 */
} // :cond_1
} // .end method
public Integer getGameFreeFormCount ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 6 */
/* .param p1, "addingStack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 872 */
int v0 = 0; // const/4 v0, 0x0
/* .line 873 */
/* .local v0, "existingGameFreeform":I */
v1 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/Integer; */
/* .line 874 */
/* .local v2, "taskId":Ljava/lang/Integer; */
v3 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 875 */
/* .local v3, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
v4 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
v5 = this.mTask;
/* iget v5, v5, Lcom/android/server/wm/Task;->mTaskId:I */
/* if-eq v4, v5, :cond_0 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v3 ).getStackPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v4 = android.util.MiuiMultiWindowAdapter .isInTopGameList ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* iget-boolean v4, v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsFrontFreeFormStackInfo:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 877 */
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v3 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = this.mAppBehindHome;
/* .line 878 */
v4 = (( android.util.ArraySet ) v4 ).contains ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v4, :cond_0 */
/* .line 879 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 881 */
} // .end local v2 # "taskId":Ljava/lang/Integer;
} // .end local v3 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* .line 882 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "existingGameFreeform="; // const-string v2, "existingGameFreeform="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFreeFormManagerService"; // const-string v2, "MiuiFreeFormManagerService"
android.util.Slog .i ( v2,v1 );
/* .line 883 */
} // .end method
public android.graphics.Rect getMiniFreeformBounds ( Integer p0, android.graphics.Rect p1, Boolean p2 ) {
/* .locals 8 */
/* .param p1, "taskId" # I */
/* .param p2, "stableBounds" # Landroid/graphics/Rect; */
/* .param p3, "rotated" # Z */
/* .line 1511 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V */
/* .line 1512 */
/* .local v0, "miniFreeformBounds":Landroid/graphics/Rect; */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1513 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1514 */
com.xiaomi.freeform.MiuiFreeformStub .getInstance ( );
v3 = this.mActivityTaskManagerService;
v3 = this.mContext;
/* xor-int/lit8 v4, p3, 0x1 */
/* iget-boolean v5, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
/* .line 1516 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).getStackPackageName ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
/* iget-boolean v7, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
/* .line 1514 */
/* invoke-virtual/range {v2 ..v7}, Lcom/xiaomi/freeform/MiuiFreeformStub;->getPossibleBounds(Landroid/content/Context;ZZLjava/lang/String;Z)Landroid/graphics/RectF; */
/* .line 1518 */
/* .local v2, "possibleBounds":Landroid/graphics/RectF; */
/* iget v3, v2, Landroid/graphics/RectF;->left:F */
/* float-to-int v3, v3 */
/* iget v4, v2, Landroid/graphics/RectF;->top:F */
/* float-to-int v4, v4 */
/* iget v5, v2, Landroid/graphics/RectF;->right:F */
/* float-to-int v5, v5 */
/* iget v6, v2, Landroid/graphics/RectF;->bottom:F */
/* float-to-int v6, v6 */
(( android.graphics.Rect ) v0 ).set ( v3, v4, v5, v6 ); // invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V
/* .line 1520 */
v3 = this.mActivityTaskManagerService;
v3 = this.mContext;
/* .line 1521 */
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).isLandcapeFreeform ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isLandcapeFreeform()Z
(( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).getStackPackageName ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
/* .line 1520 */
v3 = android.util.MiuiMultiWindowUtils .getMiniFreeformScale ( v3,v4,v0,v5 );
/* .line 1522 */
/* .local v3, "miniScale":F */
v4 = this.mActivityTaskManagerService;
v4 = this.mContext;
v4 = android.util.MiuiMultiWindowUtils .isPadScreen ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1523 */
} // :cond_0
} // :goto_0
/* float-to-int v4, v4 */
/* .line 1524 */
/* .local v4, "margin":I */
/* add-int/lit8 v5, v4, 0x14 */
/* const/16 v6, 0x14 */
(( android.graphics.Rect ) p2 ).inset ( v5, v6 ); // invoke-virtual {p2, v5, v6}, Landroid/graphics/Rect;->inset(II)V
/* .line 1526 */
/* iget v5, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mCornerPosition:I */
int v6 = 1; // const/4 v6, 0x1
/* if-ne v5, v6, :cond_1 */
/* .line 1527 */
/* iget v5, p2, Landroid/graphics/Rect;->left:I */
/* .line 1528 */
/* .local v5, "toPosX":I */
/* iget v6, p2, Landroid/graphics/Rect;->top:I */
/* .local v6, "toPosY":I */
/* .line 1530 */
} // .end local v5 # "toPosX":I
} // .end local v6 # "toPosY":I
} // :cond_1
/* iget v5, p2, Landroid/graphics/Rect;->right:I */
v6 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
/* int-to-float v6, v6 */
/* mul-float/2addr v6, v3 */
/* float-to-int v6, v6 */
/* sub-int/2addr v5, v6 */
/* .line 1531 */
/* .restart local v5 # "toPosX":I */
/* iget v6, p2, Landroid/graphics/Rect;->top:I */
/* .line 1533 */
/* .restart local v6 # "toPosY":I */
} // :goto_1
(( android.graphics.Rect ) v0 ).offsetTo ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 1535 */
} // .end local v2 # "possibleBounds":Landroid/graphics/RectF;
} // .end local v3 # "miniScale":F
} // .end local v4 # "margin":I
} // .end local v5 # "toPosX":I
} // .end local v6 # "toPosY":I
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " getMiniFreeformBounds taskId: "; // const-string v3, " getMiniFreeformBounds taskId: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " stableBounds: "; // const-string v3, " stableBounds: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " rotated: "; // const-string v3, " rotated: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " miniFreeformBounds: "; // const-string v3, " miniFreeformBounds: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiFreeFormManagerService"; // const-string v3, "MiuiFreeFormManagerService"
android.util.Slog .d ( v3,v2 );
/* .line 1537 */
} // .end method
public com.android.server.wm.MiuiFreeFormActivityStackStub getMiuiFreeFormActivityStack ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 633 */
v0 = this.mFreeFormActivityStacks;
java.lang.Integer .valueOf ( p1 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStackStub; */
} // .end method
public com.android.server.wm.MiuiFreeFormActivityStackStub getMiuiFreeFormActivityStack ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 641 */
v0 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/Integer; */
/* .line 642 */
/* .local v1, "taskId":Ljava/lang/Integer; */
v2 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 643 */
/* .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v2 != null) { // if-eqz v2, :cond_0
(( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).getStackPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 644 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).getStackPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.mTask;
/* iget v3, v3, Lcom/android/server/wm/Task;->mUserId:I */
/* if-ne v3, p2, :cond_0 */
/* .line 645 */
/* .line 647 */
} // .end local v1 # "taskId":Ljava/lang/Integer;
} // .end local v2 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* .line 648 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.android.server.wm.MiuiFreeFormActivityStack getMiuiFreeFormActivityStackForMiuiFB ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 637 */
v0 = this.mFreeFormActivityStacks;
java.lang.Integer .valueOf ( p1 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
} // .end method
public miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo getMiuiFreeFormStackInfo ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 196 */
int v0 = 0; // const/4 v0, 0x0
/* .line 197 */
/* .local v0, "res":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 198 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 199 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).getMiuiFreeFormStackInfo ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFormStackInfo()Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
/* .line 201 */
} // :cond_0
} // .end method
public android.graphics.Rect getMiuiFreeformBounds ( Integer p0, android.graphics.Rect p1 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .param p2, "originalBounds" # Landroid/graphics/Rect; */
/* .line 2110 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V */
/* .line 2111 */
/* .local v0, "bounds":Landroid/graphics/Rect; */
v1 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getFreeformScale ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getFreeformScale(I)F
(( android.graphics.Rect ) v0 ).scale ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Rect;->scale(F)V
/* .line 2112 */
/* iget v1, p2, Landroid/graphics/Rect;->left:I */
/* iget v2, p2, Landroid/graphics/Rect;->top:I */
(( android.graphics.Rect ) v0 ).offsetTo ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V
/* .line 2113 */
} // .end method
public Float getMiuiFreeformCornerRadius ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .line 2117 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2118 */
/* .local v0, "cornerRadius":F */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 2119 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2120 */
v2 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).isInFreeFormMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 2121 */
/* .line 2122 */
} // :cond_0
v2 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).isInMiniFreeFormMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 2123 */
/* .line 2126 */
} // :cond_1
} // :goto_0
} // .end method
public Boolean getMiuiFreeformIsForegroundPin ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 320 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 321 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
/* .line 322 */
} // :cond_0
/* iget-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z */
} // .end method
public Integer getMiuiFreeformMode ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 342 */
int v0 = -1; // const/4 v0, -0x1
/* .line 343 */
/* .local v0, "freeformMode":I */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 344 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 345 */
v0 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).getMiuiFreeFromWindowMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFromWindowMode()I
/* .line 347 */
} // :cond_0
} // .end method
public com.android.server.wm.MiuiFreeFormActivityStack getReplaceFreeForm ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 8 */
/* .param p1, "addingTask" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 683 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->unlockingAppLock(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 684 */
/* .line 686 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 687 */
/* .local v0, "hasFreeformModeTask":Z */
int v2 = 0; // const/4 v2, 0x0
/* .line 688 */
/* .local v2, "hasPinModeTask":Z */
int v3 = 0; // const/4 v3, 0x0
/* .line 690 */
/* .local v3, "hasMiniFreeformModeTask":Z */
v4 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v4 ).values ( ); // invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v5 = } // :goto_0
int v6 = 1; // const/4 v6, 0x1
if ( v5 != null) { // if-eqz v5, :cond_4
/* check-cast v5, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 691 */
/* .local v5, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-eq v5, p1, :cond_1 */
v7 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).getMiuiFreeFromWindowMode ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFromWindowMode()I
/* if-nez v7, :cond_1 */
/* .line 692 */
v7 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
if ( v7 != null) { // if-eqz v7, :cond_1
v7 = this.mTask;
/* iget v7, v7, Lcom/android/server/wm/Task;->mTaskId:I */
/* .line 693 */
v7 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).isAppBehindHome ( v7 ); // invoke-virtual {p0, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z
/* if-nez v7, :cond_1 */
/* .line 694 */
/* or-int/lit8 v0, v0, 0x1 */
/* .line 696 */
} // :cond_1
/* if-eq v5, p1, :cond_2 */
v7 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).inPinMode ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 697 */
/* or-int/lit8 v2, v2, 0x1 */
/* .line 699 */
} // :cond_2
/* if-eq v5, p1, :cond_3 */
v7 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v5 ).getMiuiFreeFromWindowMode ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFromWindowMode()I
/* if-ne v7, v6, :cond_3 */
/* .line 700 */
/* or-int/lit8 v3, v3, 0x1 */
/* .line 702 */
} // .end local v5 # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_3
/* .line 703 */
} // :cond_4
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getReplaceFreeForm stacks = "; // const-string v5, "getReplaceFreeForm stacks = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v5 ).values ( ); // invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " addingTask: "; // const-string v5, " addingTask: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " hasFreeformModeTask: "; // const-string v5, " hasFreeformModeTask: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = " hasPinModeTask: "; // const-string v5, " hasPinModeTask: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = " hasMiniFreeformModeTask: "; // const-string v5, " hasMiniFreeformModeTask: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiFreeFormManagerService"; // const-string v5, "MiuiFreeFormManagerService"
android.util.Slog .i ( v5,v4 );
/* .line 708 */
int v4 = 0; // const/4 v4, 0x0
/* .line 709 */
/* .local v4, "replacedTask":Lcom/android/server/wm/Task; */
int v7 = 0; // const/4 v7, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 710 */
/* invoke-direct {p0, p1, v6, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getBottomFreeformTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)Lcom/android/server/wm/Task; */
/* .line 711 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "getReplaceFreeForm getBottomFreeformTask replacedTask = "; // const-string v7, "getReplaceFreeForm getBottomFreeformTask replacedTask = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 712 */
} // :cond_5
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 713 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getReplacePinModeTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Lcom/android/server/wm/Task; */
/* .line 714 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "getReplaceFreeForm getReplacePinModeTask replacedTask = "; // const-string v7, "getReplaceFreeForm getReplacePinModeTask replacedTask = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 715 */
} // :cond_6
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 716 */
/* invoke-direct {p0, p1, v7, v6}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getBottomFreeformTask(Lcom/android/server/wm/MiuiFreeFormActivityStack;ZZ)Lcom/android/server/wm/Task; */
/* .line 718 */
} // :cond_7
} // :goto_1
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "getReplaceFreeForm replacedTask = "; // const-string v7, "getReplaceFreeForm replacedTask = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = ", hasFreeformModeTask="; // const-string v7, ", hasFreeformModeTask="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v7 = ", hasPinModeTask="; // const-string v7, ", hasPinModeTask="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v7 = ", hasMiniFreeformModeTask="; // const-string v7, ", hasMiniFreeformModeTask="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 720 */
/* if-nez v4, :cond_8 */
/* .line 721 */
/* .line 723 */
} // :cond_8
v1 = (( com.android.server.wm.Task ) v4 ).getRootTaskId ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end method
public com.android.server.wm.MiuiFreeFormActivityStack getSchemeLauncherTask ( ) {
/* .locals 5 */
/* .line 1313 */
v0 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 1314 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;" */
/* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1315 */
/* .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v2 != null) { // if-eqz v2, :cond_0
v3 = this.mTask;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.mTask;
v3 = this.intent;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.mTask;
v3 = this.intent;
/* .line 1316 */
(( android.content.Intent ) v3 ).getComponent ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.mTask;
v3 = this.intent;
/* .line 1317 */
(( android.content.Intent ) v3 ).getComponent ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v3 ).getShortClassName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;
/* .line 1318 */
final String v4 = "com.alipay.mobile.quinox.SchemeLauncherActivity"; // const-string v4, "com.alipay.mobile.quinox.SchemeLauncherActivity"
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1319 */
/* .line 1321 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/wm/MiuiFreeFormActivityStack;>;"
} // .end local v2 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* .line 1322 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.android.server.wm.MiuiFreeFormActivityStackStub getSchemeLauncherTask ( ) { //bridge//synthethic
/* .locals 1 */
/* .line 103 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getSchemeLauncherTask ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getSchemeLauncherTask()Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end method
public java.lang.String getStackPackageName ( com.android.server.wm.Task p0 ) {
/* .locals 4 */
/* .param p1, "targetTask" # Lcom/android/server/wm/Task; */
/* .line 1326 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 1327 */
/* .line 1329 */
} // :cond_0
v1 = this.origActivity;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1330 */
v0 = this.origActivity;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1332 */
} // :cond_1
v1 = this.realActivity;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1333 */
v0 = this.realActivity;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1335 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.wm.Task ) p1 ).getTopActivity ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1336 */
(( com.android.server.wm.Task ) p1 ).getTopActivity ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
v0 = this.packageName;
/* .line 1338 */
} // :cond_3
} // .end method
public Boolean hasFreeformDesktopMemory ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1964 */
v0 = this.mActivityTaskManagerService;
v0 = this.mRootWindowContainer;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).anyTaskForId ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;
/* .line 1965 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
v1 = this.mActivityTaskManagerService;
v1 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v1 ).getLaunchParamsController ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
v1 = (( com.android.server.wm.LaunchParamsController ) v1 ).hasFreeformDesktopMemory ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z
} // .end method
public Boolean hasMiuiFreeformOnShellFeature ( ) {
/* .locals 1 */
/* .line 372 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void hideCallingTaskIfAddSpecificChild ( com.android.server.wm.WindowContainer p0 ) {
/* .locals 4 */
/* .param p1, "child" # Lcom/android/server/wm/WindowContainer; */
/* .line 1375 */
if ( p1 != null) { // if-eqz p1, :cond_1
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
(( com.android.server.wm.ActivityRecord ) v0 ).getTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1376 */
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
v0 = this.intent;
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
v0 = this.intent;
(( android.content.Intent ) v0 ).getComponent ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = android.util.MiuiMultiWindowAdapter.SHOW_HIDDEN_TASK_IF_FINISHED_WHITE_LIST_ACTIVITY;
/* .line 1378 */
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
v1 = this.intent;
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
v0 = (( android.content.ComponentName ) v1 ).getClassName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1379 */
v0 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1380 */
/* .local v1, "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.mTask;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
v2 = this.intent;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
v2 = this.intent;
/* .line 1381 */
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
v2 = this.intent;
/* .line 1382 */
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
v2 = this.intent;
/* .line 1383 */
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
(( com.android.server.wm.ActivityRecord ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
v3 = this.mCallingPackage;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
/* .line 1384 */
(( com.android.server.wm.Task ) v2 ).getResumedActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
(( com.android.server.wm.Task ) v2 ).getResumedActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
v2 = this.intent;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
/* .line 1385 */
(( com.android.server.wm.Task ) v2 ).getResumedActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
v2 = this.intent;
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = android.util.MiuiMultiWindowAdapter.HIDE_SELF_IF_NEW_FREEFORM_TASK_WHITE_LIST_ACTIVITY;
v3 = this.mTask;
/* .line 1387 */
(( com.android.server.wm.Task ) v3 ).getResumedActivity ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getResumedActivity()Lcom/android/server/wm/ActivityRecord;
v3 = this.intent;
(( android.content.Intent ) v3 ).getComponent ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
v2 = (( android.content.ComponentName ) v3 ).getClassName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1388 */
v2 = this.mTask;
(( com.android.server.wm.Task ) v2 ).getSyncTransaction ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getSyncTransaction()Landroid/view/SurfaceControl$Transaction;
v3 = this.mTask;
(( com.android.server.wm.Task ) v3 ).getSurfaceControl ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;
(( android.view.SurfaceControl$Transaction ) v2 ).hide ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 1390 */
} // .end local v1 # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* goto/16 :goto_0 */
/* .line 1392 */
} // :cond_1
return;
} // .end method
public Boolean ignoreDeliverResultForFreeForm ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 1 */
/* .param p1, "resultTo" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "resultFrom" # Lcom/android/server/wm/ActivityRecord; */
/* .line 2062 */
v0 = this.mFreeFormGestureController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2063 */
v0 = (( com.android.server.wm.MiuiFreeFormGestureController ) v0 ).ignoreDeliverResultForFreeForm ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->ignoreDeliverResultForFreeForm(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
/* .line 2065 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean inPinMode ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 216 */
int v0 = 0; // const/4 v0, 0x0
/* .line 217 */
/* .local v0, "res":Z */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 218 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 219 */
v0 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).inPinMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
/* .line 221 */
} // :cond_0
} // .end method
public void init ( com.android.server.wm.ActivityTaskManagerService p0 ) {
/* .locals 4 */
/* .param p1, "ams" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 185 */
this.mActivityTaskManagerService = p1;
/* .line 186 */
/* new-instance v0, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V */
this.mFreeFormStackDisplayStrategy = v0;
/* .line 187 */
/* new-instance v0, Lcom/android/server/wm/MiuiFreeFormCameraStrategy; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V */
this.mFreeFormCameraStrategy = v0;
/* .line 188 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
int v2 = -4; // const/4 v2, -0x4
/* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
/* .line 189 */
/* .local v0, "handlerThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 190 */
/* new-instance v1, Landroid/os/Handler; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 191 */
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController; */
v2 = this.mActivityTaskManagerService;
v3 = this.mHandler;
/* invoke-direct {v1, v2, p0, v3}, Lcom/android/server/wm/MiuiFreeFormGestureController;-><init>(Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/MiuiFreeFormManagerService;Landroid/os/Handler;)V */
this.mFreeFormGestureController = v1;
/* .line 193 */
return;
} // .end method
public Boolean isAppBehindHome ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "rootStackId" # I */
/* .line 678 */
v0 = this.mAppBehindHome;
java.lang.Integer .valueOf ( p1 );
v0 = (( android.util.ArraySet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
} // .end method
public Boolean isAutoLayoutModeOn ( ) {
/* .locals 1 */
/* .line 155 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAutoLayoutModeOn:Z */
} // .end method
public Boolean isFrontFreeFormStackInfo ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 2004 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 2005 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2006 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
/* .line 2008 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isFullScreenStrategyNeededInDesktopMode ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "taskId" # I */
/* .line 2022 */
v0 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 2023 */
/* .line 2024 */
} // :cond_0
v0 = this.mActivityTaskManagerService;
v0 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v0 ).anyTaskForId ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;
/* .line 2025 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
(( com.android.server.wm.Task ) v0 ).getRootActivity ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 2026 */
/* .local v2, "ar":Lcom/android/server/wm/ActivityRecord; */
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_2
com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
v4 = v5 = this.packageName;
/* if-nez v4, :cond_1 */
/* .line 2027 */
com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
v4 = v5 = this.shortComponentName;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 2028 */
} // :cond_1
/* .line 2030 */
} // :cond_2
com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
v5 = this.mActivityTaskManagerService;
v4 = v5 = this.mContext;
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 2031 */
/* .line 2033 */
} // :cond_3
} // .end method
public Boolean isHideStackFromFullScreen ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1987 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1988 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1989 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isHideStackFromFullScreen ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isHideStackFromFullScreen()Z
/* .line 1991 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isInFreeForm ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 660 */
v0 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/Integer; */
/* .line 661 */
/* .local v1, "taskId":Ljava/lang/Integer; */
v2 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 662 */
/* .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v2 != null) { // if-eqz v2, :cond_0
(( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).getStackPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 663 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).getStackPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 664 */
int v0 = 1; // const/4 v0, 0x1
/* .line 666 */
} // .end local v1 # "taskId":Ljava/lang/Integer;
} // .end local v2 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* .line 667 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isInInfiniteDragTaskResizeAnim ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .line 2215 */
try { // :try_start_0
v0 = v0 = this.mFreeformModeControl;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2216 */
/* :catch_0 */
/* move-exception v0 */
/* .line 2217 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 2219 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isInMiniFreeFormMode ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 351 */
int v0 = 0; // const/4 v0, 0x0
/* .line 352 */
/* .local v0, "res":Z */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 353 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 354 */
v0 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).isInMiniFreeFormMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
/* .line 356 */
} // :cond_0
} // .end method
public Boolean isInVideoOrGameScene ( ) {
/* .locals 1 */
/* .line 2043 */
v0 = this.mFreeFormGestureController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2044 */
v0 = (( com.android.server.wm.MiuiFreeFormGestureController ) v0 ).isInVideoOrGameScene ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z
/* .line 2046 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isNormalFreeForm ( com.android.server.wm.Task p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .line 2170 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 2171 */
/* .line 2174 */
} // :cond_0
v1 = android.util.MiuiMultiWindowAdapter .inFreeformWhiteList ( p2 );
final String v2 = "Task "; // const-string v2, "Task "
final String v3 = "MiuiFreeFormManagerService"; // const-string v3, "MiuiFreeFormManagerService"
/* if-nez v1, :cond_3 */
v1 = this.realActivity;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2175 */
android.util.MiuiMultiWindowAdapter .getFreeformResizeableWhiteList ( );
v4 = this.realActivity;
/* .line 2176 */
v1 = (( android.content.ComponentName ) v4 ).flattenToString ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2182 */
} // :cond_1
v1 = android.util.MiuiMultiWindowAdapter .getFreeformBlackList ( );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 2183 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " is Abnormal freeform because pkg is in freeform black list"; // const-string v2, " is Abnormal freeform because pkg is in freeform black list"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 2185 */
/* .line 2188 */
} // :cond_2
/* iget v0, p1, Lcom/android/server/wm/Task;->mResizeMode:I */
v0 = android.content.pm.ActivityInfo .isResizeableMode ( v0 );
/* .line 2177 */
} // :cond_3
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/android/server/wm/Task;->mTaskId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is Normal freeform because pkg or activity is in freeform white list"; // const-string v1, " is Normal freeform because pkg or activity is in freeform white list"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v0 );
/* .line 2179 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isSplitRootTask ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 2036 */
v0 = this.mActivityTaskManagerService;
v0 = this.mRootWindowContainer;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).anyTaskForId ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;
/* .line 2037 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2038 */
v1 = (( com.android.server.wm.Task ) v0 ).isSplitScreenRootTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->isSplitScreenRootTask()Z
/* .line 2040 */
} // :cond_0
} // .end method
public Boolean isSupportPin ( ) {
/* .locals 1 */
/* .line 368 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isWpsPreStartActivity ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "shortComponentName" # Ljava/lang/String; */
/* .line 1470 */
final String v0 = "cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity"; // const-string v0, "cn.wps.moffice_eng/cn.wps.moffice.documentmanager.PreStartActivity"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isWpsSameActivity ( android.content.ComponentName p0, android.content.ComponentName p1 ) {
/* .locals 1 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .param p2, "otherComponentName" # Landroid/content/ComponentName; */
/* .line 1474 */
v0 = android.util.MiuiMultiWindowAdapter .isWpsSameActivity ( p1,p2 );
} // .end method
public void launchFullscreenInDesktopModeIfNeeded ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.Task p1 ) {
/* .locals 4 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "reusedTask" # Lcom/android/server/wm/Task; */
/* .line 2093 */
v0 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
if ( v0 != null) { // if-eqz v0, :cond_3
if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez p2, :cond_0 */
/* .line 2096 */
} // :cond_0
android.util.MiuiMultiWindowUtils .getDesktopModeLaunchFullscreenAppList ( );
/* .line 2097 */
/* .local v0, "launchFullscreenAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_2 */
/* .line 2098 */
/* check-cast v2, Ljava/lang/String; */
v3 = this.shortComponentName;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 2099 */
/* iget v2, p2, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStack ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
/* .line 2100 */
/* .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStackStub; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 2101 */
/* iget v3, p2, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).fullscreenFreeformTask ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fullscreenFreeformTask(I)V
/* .line 2102 */
return;
/* .line 2097 */
} // .end local v2 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 2106 */
} // .end local v1 # "i":I
} // :cond_2
return;
/* .line 2094 */
} // .end local v0 # "launchFullscreenAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_3
} // :goto_1
return;
} // .end method
public void miniFreeformToFullscreenByBottomCaption ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "taskId" # I */
/* .line 2081 */
v0 = this.mActivityTaskManagerService;
v0 = this.mWindowManager;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 2082 */
try { // :try_start_0
v1 = this.mFreeFormGestureController;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2083 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStack ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
/* check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 2084 */
/* .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
final String v2 = "MiuiFreeFormManagerService"; // const-string v2, "MiuiFreeFormManagerService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " miniFreeformToFullscreenByBottomCaption: taskId= "; // const-string v4, " miniFreeformToFullscreenByBottomCaption: taskId= "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 2085 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2086 */
v2 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v2 ).deliverResultForExitFreeform ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->deliverResultForExitFreeform(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 2089 */
} // .end local v1 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* monitor-exit v0 */
/* .line 2090 */
return;
/* .line 2089 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void moveTaskToBack ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 2198 */
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = (( com.android.server.wm.Task ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
/* if-nez v0, :cond_0 */
/* .line 2199 */
} // :cond_0
v0 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 2200 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).inPinMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
/* if-nez v1, :cond_1 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInMiniFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
/* if-nez v1, :cond_1 */
/* .line 2201 */
v1 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v1 ).startExitApplication ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->startExitApplication(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 2203 */
} // :cond_1
return;
/* .line 2198 */
} // .end local v0 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_2
} // :goto_0
return;
} // .end method
public void moveTaskToFront ( com.android.server.wm.Task p0, android.app.ActivityOptions p1 ) {
/* .locals 3 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "options" # Landroid/app/ActivityOptions; */
/* .line 1123 */
if ( p1 != null) { // if-eqz p1, :cond_4
v0 = (( com.android.server.wm.Task ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
/* if-nez v0, :cond_0 */
/* .line 1124 */
} // :cond_0
v0 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1125 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1126 */
/* iget v1, p1, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).removeFullScreenTasksBehindHome ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFullScreenTasksBehindHome(I)V
/* .line 1128 */
v1 = (( com.android.server.wm.Task ) p1 ).isVisible ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isVisible()Z
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
/* if-nez v1, :cond_2 */
} // :cond_1
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).inPinMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
/* if-nez v1, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 1129 */
(( android.app.ActivityOptions ) p2 ).getLaunchBounds ( ); // invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchBounds()Landroid/graphics/Rect;
/* .line 1130 */
/* .local v1, "bounds":Landroid/graphics/Rect; */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = (( android.graphics.Rect ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v2, :cond_2 */
/* .line 1131 */
(( com.android.server.wm.Task ) p1 ).setBounds ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->setBounds(Landroid/graphics/Rect;)I
/* .line 1134 */
} // .end local v1 # "bounds":Landroid/graphics/Rect;
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setIsFrontFreeFormStackInfo ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setIsFrontFreeFormStackInfo(Z)V
/* .line 1135 */
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isHideStackFromFullScreen ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isHideStackFromFullScreen()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1136 */
v1 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->showFreeformIfNeeded(I)V */
/* .line 1141 */
} // :cond_3
return;
/* .line 1123 */
} // .end local v0 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_4
} // :goto_0
return;
} // .end method
public void moveToFront ( com.android.server.wm.Task p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 2159 */
v0 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 2160 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInMiniFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z
/* if-nez v1, :cond_0 */
v1 = (( com.android.server.wm.Task ) p1 ).isVisible ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isVisible()Z
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_0
v1 = this.mFreeformModeControl;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2162 */
try { // :try_start_0
v2 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2165 */
/* .line 2163 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2164 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "MiuiFreeFormManagerService"; // const-string v2, "MiuiFreeFormManagerService"
final String v3 = "Error moveToFront."; // const-string v3, "Error moveToFront."
android.util.Slog .e ( v2,v3,v1 );
/* .line 2167 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
return;
} // .end method
public void notifyCameraStateChanged ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "cameraState" # I */
/* .line 1351 */
v0 = this.mFreeFormCameraStrategy;
/* .line 1352 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p2, v1, :cond_0 */
/* .line 1353 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1351 */
} // :goto_0
(( com.android.server.wm.MiuiFreeFormCameraStrategy ) v0 ).onCameraStateChanged ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->onCameraStateChanged(Ljava/lang/String;I)V
/* .line 1354 */
return;
} // .end method
public void onATMSSystemReady ( ) {
/* .locals 1 */
/* .line 1507 */
v0 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v0 ).onATMSSystemReady ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->onATMSSystemReady()V
/* .line 1508 */
return;
} // .end method
public void onActivityStackConfigurationChanged ( com.android.server.wm.Task p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "prevWindowingMode" # I */
/* .param p3, "overrideWindowingMode" # I */
/* .line 623 */
v0 = (( com.android.server.wm.Task ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 624 */
final String v0 = "onActivityStackConfigurationChanged"; // const-string v0, "onActivityStackConfigurationChanged"
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).addFreeFormActivityStack ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;Ljava/lang/String;)V
/* .line 626 */
} // :cond_0
int v0 = 5; // const/4 v0, 0x5
/* if-ne p2, v0, :cond_1 */
/* if-eq p3, v0, :cond_1 */
/* .line 627 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).removeFreeFormActivityStack ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFreeFormActivityStack(Lcom/android/server/wm/Task;Z)V
/* .line 630 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void onActivityStackFirstActivityRecordAdded ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "rootId" # I */
/* .line 1541 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1542 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_0 */
return;
/* .line 1543 */
} // :cond_0
/* iget-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1544 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mShouldDelayDispatchFreeFormStackModeChanged:Z */
/* .line 1545 */
v2 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isInFreeFormMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1546 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* .line 1545 */
} // :goto_0
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).dispatchFreeFormStackModeChanged ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 1548 */
} // :cond_2
v1 = this.mFreeFormGestureController;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1549 */
v2 = this.mFreeFormStackDisplayStrategy;
v3 = this.mFreeFormActivityStacks;
(( com.android.server.wm.MiuiFreeFormStackDisplayStrategy ) v2 ).onMiuiFreeFormStasckAdded ( v3, v1, v0 ); // invoke-virtual {v2, v3, v1, v0}, Lcom/android/server/wm/MiuiFreeFormStackDisplayStrategy;->onMiuiFreeFormStasckAdded(Ljava/util/concurrent/ConcurrentHashMap;Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 1551 */
} // :cond_3
return;
} // .end method
public void onActivityStackWindowModeSet ( com.android.server.wm.Task p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "rootTask" # Lcom/android/server/wm/Task; */
/* .param p2, "mode" # I */
/* .line 615 */
int v0 = 5; // const/4 v0, 0x5
/* if-ne p2, v0, :cond_0 */
/* .line 616 */
final String v0 = "onActivityStackWindowModeSet"; // const-string v0, "onActivityStackWindowModeSet"
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).addFreeFormActivityStack ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;Ljava/lang/String;)V
/* .line 618 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).removeFreeFormActivityStack ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->removeFreeFormActivityStack(Lcom/android/server/wm/Task;Z)V
/* .line 620 */
} // :goto_0
return;
} // .end method
public void onExitFreeform ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1188 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1189 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda18; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda18;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1196 */
return;
} // .end method
public void onFirstWindowDrawn ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 539 */
v0 = this.mFreeFormGestureController;
v0 = this.mMiuiMultiWindowRecommendHelper;
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) v0 ).onFirstWindowDrawn ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->onFirstWindowDrawn(Lcom/android/server/wm/ActivityRecord;)V
/* .line 540 */
return;
} // .end method
public void onShowToastIfNeeded ( com.android.server.wm.Task p0, android.app.ActivityOptions p1 ) {
/* .locals 2 */
/* .param p1, "as" # Lcom/android/server/wm/Task; */
/* .param p2, "options" # Landroid/app/ActivityOptions; */
/* .line 530 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( com.android.server.wm.Task ) p1 ).isActivityTypeHome ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isActivityTypeHome()Z
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 531 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_2
v0 = (( android.app.ActivityOptions ) p2 ).getLaunchWindowingMode ( ); // invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_2 */
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = this.mAtmService;
/* iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 533 */
v0 = (( com.android.server.wm.Task ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p1, Lcom/android/server/wm/Task;->mTaskId:I */
} // :cond_1
v0 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
} // :goto_0
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 534 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->showPropotionalFreeformToast(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V */
/* .line 536 */
} // .end local v0 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_2
return;
} // .end method
public void onStartActivity ( com.android.server.wm.Task p0, android.app.ActivityOptions p1, android.content.Intent p2 ) {
/* .locals 17 */
/* .param p1, "as" # Lcom/android/server/wm/Task; */
/* .param p2, "options" # Landroid/app/ActivityOptions; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .line 468 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p2 */
/* move-object/from16 v4, p3 */
if ( v2 != null) { // if-eqz v2, :cond_0
v0 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->isActivityTypeHome()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 469 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "onStartActivityInner as = "; // const-string v5, "onStartActivityInner as = "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " op = "; // const-string v6, " op = "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "MiuiFreeFormManagerService"; // const-string v7, "MiuiFreeFormManagerService"
com.android.server.wm.MiuiFreeFormManagerService .logd ( v7,v0 );
/* .line 470 */
if ( v4 != null) { // if-eqz v4, :cond_1
final String v0 = "android.intent.extra.shortcut.NAME"; // const-string v0, "android.intent.extra.shortcut.NAME"
(( android.content.Intent ) v4 ).getStringExtra ( v0 ); // invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* move-object v8, v0 */
/* .line 471 */
/* .local v8, "oriPkg":Ljava/lang/String; */
int v9 = 0; // const/4 v9, 0x0
if ( v4 != null) { // if-eqz v4, :cond_2
final String v0 = "originating_uid"; // const-string v0, "originating_uid"
v0 = (( android.content.Intent ) v4 ).getIntExtra ( v0, v9 ); // invoke-virtual {v4, v0, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
} // :cond_2
/* move v0, v9 */
} // :goto_1
/* move v10, v0 */
/* .line 472 */
/* .local v10, "oriUid":I */
if ( v8 != null) { // if-eqz v8, :cond_4
v0 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
/* if-lez v0, :cond_4 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 473 */
v0 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 474 */
this.behindAppLockPkg = v8;
/* .line 475 */
/* iput v10, v2, Lcom/android/server/wm/Task;->originatingUid:I */
/* .line 477 */
} // :cond_3
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task; */
/* .line 478 */
/* .local v0, "task":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 479 */
this.behindAppLockPkg = v8;
/* .line 480 */
/* iput v10, v0, Lcom/android/server/wm/Task;->originatingUid:I */
/* .line 484 */
} // .end local v0 # "task":Lcom/android/server/wm/Task;
} // :cond_4
} // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_8
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I */
int v11 = 5; // const/4 v11, 0x5
/* if-ne v0, v11, :cond_8 */
if ( v2 != null) { // if-eqz v2, :cond_8
v0 = this.mAtmService;
/* iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 486 */
final String v0 = "getActivityOptionsInjector"; // const-string v0, "getActivityOptionsInjector"
/* new-array v11, v9, [Ljava/lang/Object; */
android.util.MiuiMultiWindowUtils .isMethodExist ( v3,v0,v11 );
/* .line 487 */
/* .local v11, "method":Ljava/lang/reflect/Method; */
if ( v11 != null) { // if-eqz v11, :cond_8
/* .line 489 */
try { // :try_start_0
/* new-array v0, v9, [Ljava/lang/Object; */
/* .line 490 */
(( java.lang.reflect.Method ) v11 ).invoke ( v3, v0 ); // invoke-virtual {v11, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
final String v12 = "getFreeformAnimation"; // const-string v12, "getFreeformAnimation"
/* new-array v13, v9, [Ljava/lang/Object; */
/* .line 489 */
android.util.MiuiMultiWindowUtils .invoke ( v0,v12,v13 );
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 496 */
/* .local v0, "needAnimation":Z */
v12 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->inMultiWindowMode()Z */
if ( v12 != null) { // if-eqz v12, :cond_5
/* move-object v12, v2 */
} // :cond_5
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task; */
} // :goto_3
final String v13 = "onStartActivity"; // const-string v13, "onStartActivity"
(( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).addFreeFormActivityStack ( v12, v13 ); // invoke-virtual {v1, v12, v13}, Lcom/android/server/wm/MiuiFreeFormManagerService;->addFreeFormActivityStack(Lcom/android/server/wm/Task;Ljava/lang/String;)V
/* .line 497 */
v12 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->getRootTaskId()I */
(( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).getMiuiFreeFormActivityStackForMiuiFB ( v12 ); // invoke-virtual {v1, v12}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 498 */
/* .local v12, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* new-array v13, v9, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v11 ).invoke ( v3, v13 ); // invoke-virtual {v11, v3, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
final String v14 = "isNormalFreeForm"; // const-string v14, "isNormalFreeForm"
/* new-array v15, v9, [Ljava/lang/Object; */
android.util.MiuiMultiWindowUtils .invoke ( v13,v14,v15 );
/* check-cast v13, Ljava/lang/Boolean; */
v13 = (( java.lang.Boolean ) v13 ).booleanValue ( ); // invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z
/* iput-boolean v13, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
/* .line 499 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v12 ).setNeedAnimation ( v0 ); // invoke-virtual {v12, v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setNeedAnimation(Z)V
/* .line 500 */
/* new-array v13, v9, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v11 ).invoke ( v3, v13 ); // invoke-virtual {v11, v3, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
final String v14 = "getFreeformScale"; // const-string v14, "getFreeformScale"
/* new-array v15, v9, [Ljava/lang/Object; */
android.util.MiuiMultiWindowUtils .invoke ( v13,v14,v15 );
/* check-cast v13, Ljava/lang/Float; */
v13 = (( java.lang.Float ) v13 ).floatValue ( ); // invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F
/* .line 501 */
/* .local v13, "scaleInOptions":F */
/* const/high16 v14, -0x40800000 # -1.0f */
/* cmpl-float v14, v13, v14 */
if ( v14 != null) { // if-eqz v14, :cond_6
/* move/from16 v16, v0 */
/* move v0, v13 */
/* .line 502 */
} // :cond_6
v14 = this.mActivityTaskManagerService;
v14 = this.mContext;
/* iget-boolean v15, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
/* iget-boolean v9, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
/* move/from16 v16, v0 */
} // .end local v0 # "needAnimation":Z
/* .local v16, "needAnimation":Z */
v0 = this.mActivityTaskManagerService;
v0 = this.mContext;
v0 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( v0 );
(( com.android.server.wm.MiuiFreeFormActivityStack ) v12 ).getStackPackageName ( ); // invoke-virtual {v12}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v0 = android.util.MiuiMultiWindowUtils .getOriFreeformScale ( v14,v15,v9,v0,v4 );
} // :goto_4
/* iput v0, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F */
/* .line 503 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/app/ActivityOptions;->getLaunchFromTaskId()I */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 504 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/app/ActivityOptions;->getLaunchFromTaskId()I */
/* iput v0, v12, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormLaunchFromTaskId:I */
/* .line 508 */
} // :cond_7
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " options.getLaunchFromTaskId()= "; // const-string v4, " options.getLaunchFromTaskId()= "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 509 */
v4 = /* invoke-virtual/range {p2 ..p2}, Landroid/app/ActivityOptions;->getLaunchFromTaskId()I */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 508 */
com.android.server.wm.MiuiFreeFormManagerService .logd ( v7,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 511 */
} // .end local v12 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v13 # "scaleInOptions":F
} // .end local v16 # "needAnimation":Z
/* .line 510 */
/* :catch_0 */
/* move-exception v0 */
/* .line 514 */
} // .end local v11 # "method":Ljava/lang/reflect/Method;
} // :cond_8
} // :goto_5
v0 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 515 */
v0 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->getRootTaskId()I */
(( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 516 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_c
v4 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->isVisible()Z */
if ( v4 != null) { // if-eqz v4, :cond_9
v4 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
/* if-nez v4, :cond_c */
/* .line 517 */
} // :cond_9
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).getStackPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
if ( v4 != null) { // if-eqz v4, :cond_c
v4 = this.mTaskSupervisor;
/* .line 518 */
(( com.android.server.wm.ActivityTaskSupervisor ) v4 ).getLaunchParamsController ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
v4 = (( com.android.server.wm.LaunchParamsController ) v4 ).hasFreeformDesktopMemory ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 519 */
v4 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v4 ).getLaunchParamsController ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
v5 = this.mTask;
v4 = (( com.android.server.wm.LaunchParamsController ) v4 ).getFreeformLastScale ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F
/* .line 520 */
/* .local v4, "lastScale":F */
v5 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/Task;->getRootTaskId()I */
(( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).setMiuiFreeformScale ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setMiuiFreeformScale(IF)V
/* .line 521 */
v5 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v5 ).getLaunchParamsController ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
v6 = this.mTask;
(( com.android.server.wm.LaunchParamsController ) v5 ).getFreeformLastPosition ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastPosition(Lcom/android/server/wm/Task;)Landroid/graphics/Rect;
/* .line 522 */
/* .local v5, "lastBounds":Landroid/graphics/Rect; */
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 523 */
v6 = (( android.graphics.Rect ) v5 ).width ( ); // invoke-virtual {v5}, Landroid/graphics/Rect;->width()I
v7 = (( android.graphics.Rect ) v5 ).height ( ); // invoke-virtual {v5}, Landroid/graphics/Rect;->height()I
/* if-le v6, v7, :cond_a */
int v9 = 1; // const/4 v9, 0x1
} // :cond_a
int v9 = 0; // const/4 v9, 0x0
} // :goto_6
/* iput-boolean v9, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
/* .line 524 */
} // :cond_b
/* invoke-virtual/range {p0 ..p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getStackPackageName(Lcom/android/server/wm/Task;)Ljava/lang/String; */
v6 = (( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).isNormalFreeForm ( v2, v6 ); // invoke-virtual {v1, v2, v6}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isNormalFreeForm(Lcom/android/server/wm/Task;Ljava/lang/String;)Z
/* iput-boolean v6, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNormalFreeForm:Z */
/* .line 527 */
} // .end local v0 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v4 # "lastScale":F
} // .end local v5 # "lastBounds":Landroid/graphics/Rect;
} // :cond_c
return;
} // .end method
public Boolean openCameraInFreeForm ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1357 */
v0 = this.mFreeFormCameraStrategy;
v0 = (( com.android.server.wm.MiuiFreeFormCameraStrategy ) v0 ).openCameraInFreeForm ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->openCameraInFreeForm(Ljava/lang/String;)Z
} // .end method
public void registerFreeformCallback ( miui.app.IFreeformCallback p0 ) {
/* .locals 2 */
/* .param p1, "freeformCallback" # Lmiui/app/IFreeformCallback; */
/* .line 1092 */
v0 = this.mCallbacks;
/* monitor-enter v0 */
/* .line 1093 */
try { // :try_start_0
v1 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v1 ).register ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 1094 */
/* monitor-exit v0 */
/* .line 1095 */
return;
/* .line 1094 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerMiuiFreeformModeControl ( miui.app.IMiuiFreeformModeControl p0 ) {
/* .locals 3 */
/* .param p1, "freeformModeControl" # Lmiui/app/IMiuiFreeformModeControl; */
/* .line 131 */
try { // :try_start_0
v0 = this.mFreeformModeControl;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 132 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 133 */
v0 = this.mFreeformModeControl;
v2 = this.mFreeformModeControlDeath;
/* .line 135 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->detachFreeformModeControl()V */
/* .line 137 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 138 */
v2 = this.mFreeformModeControlDeath;
/* .line 140 */
} // :cond_2
this.mFreeformModeControl = p1;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 143 */
/* nop */
/* .line 144 */
return;
/* .line 141 */
/* :catch_0 */
/* move-exception v0 */
/* .line 142 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/RuntimeException; */
final String v2 = "Unable to set transition player"; // const-string v2, "Unable to set transition player"
/* invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void removeFreeFormActivityStack ( com.android.server.wm.Task p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "stackRemoved" # Z */
/* .line 588 */
if ( p1 != null) { // if-eqz p1, :cond_7
v0 = this.mFreeFormActivityStacks;
v1 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
java.lang.Integer .valueOf ( v1 );
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 591 */
} // :cond_0
v0 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 592 */
/* .local v0, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_1 */
return;
/* .line 593 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "removeFreeFormActivityStack as = "; // const-string v2, "removeFreeFormActivityStack as = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFreeFormManagerService"; // const-string v2, "MiuiFreeFormManagerService"
android.util.Slog .d ( v2,v1 );
/* .line 594 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.Task ) p1 ).setAlwaysOnTop ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/Task;->setAlwaysOnTop(Z)V
/* .line 595 */
(( com.android.server.wm.Task ) p1 ).getDisplayArea ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 596 */
(( com.android.server.wm.Task ) p1 ).getDisplayArea ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* const/high16 v3, -0x80000000 */
(( com.android.server.wm.TaskDisplayArea ) v2 ).positionChildAt ( v3, p1, v1 ); // invoke-virtual {v2, v3, p1, v1}, Lcom/android/server/wm/TaskDisplayArea;->positionChildAt(ILcom/android/server/wm/WindowContainer;Z)V
/* .line 598 */
} // :cond_2
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).getMiuiFreeFromWindowMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getMiuiFreeFromWindowMode()I
/* .line 599 */
/* .local v1, "windowMode":I */
int v2 = -1; // const/4 v2, -0x1
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setStackFreeFormMode ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V
/* .line 600 */
int v2 = 3; // const/4 v2, 0x3
/* if-nez v1, :cond_3 */
/* .line 601 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).dispatchFreeFormStackModeChanged ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 602 */
} // :cond_3
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v3, :cond_4 */
/* .line 603 */
int v2 = 5; // const/4 v2, 0x5
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).dispatchFreeFormStackModeChanged ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 604 */
} // :cond_4
int v3 = 2; // const/4 v3, 0x2
/* if-ne v1, v3, :cond_5 */
/* .line 605 */
/* const/16 v2, 0x10 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).dispatchFreeFormStackModeChanged ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 606 */
} // :cond_5
/* if-ne v1, v2, :cond_6 */
/* .line 607 */
/* const/16 v2, 0x11 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).dispatchFreeFormStackModeChanged ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 609 */
} // :cond_6
} // :goto_0
v2 = this.mFreeFormActivityStacks;
v3 = (( com.android.server.wm.Task ) p1 ).getRootTaskId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I
java.lang.Integer .valueOf ( v3 );
(( java.util.concurrent.ConcurrentHashMap ) v2 ).remove ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 610 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).setCameraRotationIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setCameraRotationIfNeeded()V
/* .line 612 */
return;
/* .line 589 */
} // .end local v0 # "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v1 # "windowMode":I
} // :cond_7
} // :goto_1
return;
} // .end method
public void removeFullScreenTasksBehindHome ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 2015 */
v0 = this.mAppBehindHome;
java.lang.Integer .valueOf ( p1 );
(( android.util.ArraySet ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
/* .line 2016 */
return;
} // .end method
public void restoreMiniToFreeformMode ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1155 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1156 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda15; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda15;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1163 */
return;
} // .end method
public void setAdjustedForIme ( Boolean p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "adjustedForIme" # Z */
/* .param p2, "imeHeight" # I */
/* .param p3, "adjustedForRotation" # Z */
/* .line 173 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-lez p2, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* move v1, v0 */
/* .line 174 */
/* .local v1, "imeShowing":Z */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v0, p2 */
} // :cond_1
/* move p2, v0 */
/* .line 175 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mIsImeShowing:Z */
/* if-ne v1, v0, :cond_2 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mImeHeight:I */
/* if-ne p2, v0, :cond_2 */
/* .line 176 */
return;
/* .line 179 */
} // :cond_2
/* iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mIsImeShowing:Z */
/* .line 180 */
/* iput p2, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mImeHeight:I */
/* .line 181 */
/* invoke-direct {p0, v1, p2, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->notifyImeVisibilityChanged(ZIZ)V */
/* .line 182 */
return;
} // .end method
public void setApplicationUsedInFreeform ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "applicationUsedInFreeform" # Ljava/lang/String; */
/* .line 1109 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 1111 */
/* .local v0, "origId":J */
try { // :try_start_0
v2 = this.mActivityTaskManagerService;
v2 = this.mWindowManager;
v2 = this.mContext;
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).setApplicationUsedInFreeform ( p1, v2 ); // invoke-virtual {p0, p1, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setApplicationUsedInFreeform(Ljava/lang/String;Landroid/content/Context;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1113 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1114 */
/* nop */
/* .line 1115 */
return;
/* .line 1113 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1114 */
/* throw v2 */
} // .end method
public void setApplicationUsedInFreeform ( java.lang.String p0, android.content.Context p1 ) {
/* .locals 3 */
/* .param p1, "applicationUsedInFreeform" # Ljava/lang/String; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 1118 */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "application_used_freeform"; // const-string v1, "application_used_freeform"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$Secure .putStringForUser ( v0,v1,p1,v2 );
/* .line 1120 */
return;
} // .end method
public void setCameraOrientation ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "newRotation" # I */
/* .line 1371 */
v0 = this.mFreeFormCameraStrategy;
(( com.android.server.wm.MiuiFreeFormCameraStrategy ) v0 ).setCameraOrientation ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->setCameraOrientation(I)V
/* .line 1372 */
return;
} // .end method
public void setCameraRotationIfNeeded ( ) {
/* .locals 1 */
/* .line 584 */
v0 = this.mFreeFormCameraStrategy;
(( com.android.server.wm.MiuiFreeFormCameraStrategy ) v0 ).rotateCameraIfNeeded ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormCameraStrategy;->rotateCameraIfNeeded()V
/* .line 585 */
return;
} // .end method
public void setFreeformForceHideIfNeed ( java.util.List p0, Integer p1, java.util.Map$Entry p2, com.android.server.wm.WindowContainer p3 ) {
/* .locals 8 */
/* .param p2, "hopSize" # I */
/* .param p4, "wc" # Lcom/android/server/wm/WindowContainer; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/window/WindowContainerTransaction$HierarchyOp;", */
/* ">;I", */
/* "Ljava/util/Map$Entry<", */
/* "Landroid/os/IBinder;", */
/* "Landroid/window/WindowContainerTransaction$Change;", */
/* ">;", */
/* "Lcom/android/server/wm/WindowContainer;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 226 */
/* .local p1, "hops":Ljava/util/List;, "Ljava/util/List<Landroid/window/WindowContainerTransaction$HierarchyOp;>;" */
/* .local p3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/os/IBinder;Landroid/window/WindowContainerTransaction$Change;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 227 */
/* .local v0, "reorderToBack":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 229 */
/* .local v1, "unsetAlwaysOnTop":Z */
(( com.android.server.wm.WindowContainer ) p4 ).getTaskDisplayArea ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->getTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 230 */
(( com.android.server.wm.WindowContainer ) p4 ).getTaskDisplayArea ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->getTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
v2 = (( com.android.server.wm.TaskDisplayArea ) v2 ).inFreeformWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/TaskDisplayArea;->inFreeformWindowingMode()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v3 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 231 */
/* .local v2, "isFreeformDisplayArea":Z */
} // :goto_0
(( com.android.server.wm.WindowContainer ) p4 ).asTask ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
if ( v4 != null) { // if-eqz v4, :cond_6
v4 = (( com.android.server.wm.WindowContainer ) p4 ).inFreeformWindowingMode ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->inFreeformWindowingMode()Z
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 232 */
/* check-cast v4, Landroid/window/WindowContainerTransaction$Change; */
v4 = (( android.window.WindowContainerTransaction$Change ) v4 ).getWindowingMode ( ); // invoke-virtual {v4}, Landroid/window/WindowContainerTransaction$Change;->getWindowingMode()I
/* if-nez v4, :cond_6 */
/* if-nez v2, :cond_6 */
/* .line 234 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_1
/* if-ge v4, p2, :cond_3 */
/* .line 235 */
/* check-cast v5, Landroid/window/WindowContainerTransaction$HierarchyOp; */
/* .line 236 */
/* .local v5, "hop":Landroid/window/WindowContainerTransaction$HierarchyOp; */
v6 = (( android.window.WindowContainerTransaction$HierarchyOp ) v5 ).getType ( ); // invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getType()I
/* if-eq v6, v3, :cond_1 */
/* .line 237 */
} // :cond_1
/* nop */
/* .line 238 */
(( android.window.WindowContainerTransaction$HierarchyOp ) v5 ).getContainer ( ); // invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getContainer()Landroid/os/IBinder;
/* .line 237 */
com.android.server.wm.WindowContainer .fromBinder ( v6 );
/* .line 239 */
/* .local v6, "hopWc":Lcom/android/server/wm/WindowContainer; */
v7 = (( java.lang.Object ) p4 ).equals ( v6 ); // invoke-virtual {p4, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
/* if-nez v7, :cond_2 */
/* .line 240 */
} // :cond_2
v7 = (( android.window.WindowContainerTransaction$HierarchyOp ) v5 ).getToTop ( ); // invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getToTop()Z
/* xor-int/2addr v7, v3 */
/* move v0, v7 */
/* .line 234 */
} // .end local v5 # "hop":Landroid/window/WindowContainerTransaction$HierarchyOp;
} // .end local v6 # "hopWc":Lcom/android/server/wm/WindowContainer;
} // :goto_2
/* add-int/lit8 v4, v4, 0x1 */
/* .line 242 */
} // .end local v4 # "i":I
} // :cond_3
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "i":I */
} // :goto_3
/* if-ge v4, p2, :cond_6 */
/* .line 243 */
/* check-cast v5, Landroid/window/WindowContainerTransaction$HierarchyOp; */
/* .line 244 */
/* .restart local v5 # "hop":Landroid/window/WindowContainerTransaction$HierarchyOp; */
v6 = (( android.window.WindowContainerTransaction$HierarchyOp ) v5 ).getType ( ); // invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getType()I
/* const/16 v7, 0xc */
/* if-eq v6, v7, :cond_4 */
/* .line 245 */
} // :cond_4
/* nop */
/* .line 246 */
(( android.window.WindowContainerTransaction$HierarchyOp ) v5 ).getContainer ( ); // invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->getContainer()Landroid/os/IBinder;
/* .line 245 */
com.android.server.wm.WindowContainer .fromBinder ( v6 );
/* .line 247 */
/* .restart local v6 # "hopWc":Lcom/android/server/wm/WindowContainer; */
v7 = (( java.lang.Object ) p4 ).equals ( v6 ); // invoke-virtual {p4, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
/* if-nez v7, :cond_5 */
/* .line 248 */
} // :cond_5
v7 = (( android.window.WindowContainerTransaction$HierarchyOp ) v5 ).isAlwaysOnTop ( ); // invoke-virtual {v5}, Landroid/window/WindowContainerTransaction$HierarchyOp;->isAlwaysOnTop()Z
/* xor-int/2addr v7, v3 */
/* move v1, v7 */
/* .line 242 */
} // .end local v5 # "hop":Landroid/window/WindowContainerTransaction$HierarchyOp;
} // .end local v6 # "hopWc":Lcom/android/server/wm/WindowContainer;
} // :goto_4
/* add-int/lit8 v4, v4, 0x1 */
/* .line 251 */
} // .end local v4 # "i":I
} // :cond_6
if ( v0 != null) { // if-eqz v0, :cond_7
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 252 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setforceHiden wc: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p4 ); // invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " hops: "; // const-string v5, " hops: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiFreeFormManagerService"; // const-string v5, "MiuiFreeFormManagerService"
android.util.Slog .d ( v5,v4 );
/* .line 253 */
(( com.android.server.wm.WindowContainer ) p4 ).asTask ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
/* const/high16 v5, 0x10000 */
(( com.android.server.wm.Task ) v4 ).setForceHidden ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/android/server/wm/Task;->setForceHidden(IZ)Z
/* .line 255 */
} // :cond_7
return;
} // .end method
public void setFrontFreeFormStackInfo ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .param p2, "isFront" # Z */
/* .line 1996 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1997 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1998 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " setFrontFreeFormStackInfo taskId="; // const-string v2, " setFrontFreeFormStackInfo taskId="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", isFront="; // const-string v2, ", isFront="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mffas="; // const-string v2, ", mffas="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFreeFormManagerService"; // const-string v2, "MiuiFreeFormManagerService"
android.util.Slog .d ( v2,v1 );
/* .line 1999 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setIsFrontFreeFormStackInfo ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setIsFrontFreeFormStackInfo(Z)V
/* .line 2001 */
} // :cond_0
return;
} // .end method
public void setHideStackFromFullScreen ( Integer p0, Boolean p1 ) {
/* .locals 6 */
/* .param p1, "taskId" # I */
/* .param p2, "hidden" # Z */
/* .line 1969 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1970 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1971 */
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " setHideStackFromFullScreen taskId="; // const-string v3, " setHideStackFromFullScreen taskId="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", hiden="; // const-string v3, ", hiden="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", mffas="; // const-string v3, ", mffas="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1972 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setHideStackFromFullScreen ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setHideStackFromFullScreen(Z)V
/* .line 1973 */
v1 = this.mActivityTaskManagerService;
v1 = this.mWindowManager;
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 1975 */
try { // :try_start_0
v2 = this.mActivityTaskManagerService;
v2 = this.mRootWindowContainer;
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.wm.RootWindowContainer ) v2 ).anyTaskForId ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(II)Lcom/android/server/wm/Task;
/* .line 1976 */
/* .local v2, "curentTask":Lcom/android/server/wm/Task; */
if ( v2 != null) { // if-eqz v2, :cond_0
v4 = (( com.android.server.wm.Task ) v2 ).isFocused ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->isFocused()Z
if ( v4 != null) { // if-eqz v4, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1977 */
(( com.android.server.wm.Task ) v2 ).getNextFocusableTask ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/Task;->getNextFocusableTask(Z)Lcom/android/server/wm/Task;
/* .line 1978 */
/* .local v3, "nextFocusdTask":Lcom/android/server/wm/Task; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1979 */
v4 = this.mActivityTaskManagerService;
v5 = (( com.android.server.wm.Task ) v3 ).getRootTaskId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.ActivityTaskManagerService ) v4 ).setFocusedRootTask ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->setFocusedRootTask(I)V
/* .line 1982 */
} // .end local v2 # "curentTask":Lcom/android/server/wm/Task;
} // .end local v3 # "nextFocusdTask":Lcom/android/server/wm/Task;
} // :cond_0
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 1984 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void setMiuiFreeFormTouchExcludeRegion ( android.graphics.Region p0, android.graphics.Region p1 ) {
/* .locals 3 */
/* .param p1, "region" # Landroid/graphics/Region; */
/* .param p2, "region2" # Landroid/graphics/Region; */
/* .line 1227 */
v0 = this.mFreeformModeControl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1229 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1232 */
/* .line 1230 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1231 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
/* const-string/jumbo v2, "setMiuiFreeFormTouchExcludeRegion." */
android.util.Slog .e ( v1,v2,v0 );
/* .line 1234 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
public void setMiuiFreeformIsForegroundPin ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .param p2, "isForegroundPin" # Z */
/* .line 308 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 309 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_0 */
return;
/* .line 310 */
} // :cond_0
/* iput-boolean p2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z */
/* .line 311 */
return;
} // .end method
public void setMiuiFreeformIsNeedAnimation ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .param p2, "needAnimation" # Z */
/* .line 314 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 315 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_0 */
return;
/* .line 316 */
} // :cond_0
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setNeedAnimation ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setNeedAnimation(Z)V
/* .line 317 */
return;
} // .end method
public void setMiuiFreeformMode ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .param p2, "miuiFreeformMode" # I */
/* .line 335 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 336 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 337 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setStackFreeFormMode ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setStackFreeFormMode(I)V
/* .line 339 */
} // :cond_0
return;
} // .end method
public void setMiuiFreeformOrientation ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .param p2, "isLandscape" # Z */
/* .line 290 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 291 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_0 */
return;
/* .line 292 */
} // :cond_0
/* iput-boolean p2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
/* .line 293 */
return;
} // .end method
public void setMiuiFreeformPinPos ( Integer p0, android.graphics.Rect p1 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .param p2, "pinPos" # Landroid/graphics/Rect; */
/* .line 296 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 297 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_0 */
return;
/* .line 298 */
} // :cond_0
v1 = this.mPinFloatingWindowPos;
(( android.graphics.Rect ) v1 ).set ( p2 ); // invoke-virtual {v1, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 299 */
return;
} // .end method
public void setMiuiFreeformPinedActiveTime ( Integer p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "taskId" # I */
/* .param p2, "activeTime" # J */
/* .line 302 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 303 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_0 */
return;
/* .line 304 */
} // :cond_0
/* iput-wide p2, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->pinActiveTime:J */
/* .line 305 */
return;
} // .end method
public void setMiuiFreeformScale ( Integer p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .param p2, "miuiFreeformScale" # F */
/* .line 283 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 284 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* if-nez v0, :cond_0 */
return;
/* .line 285 */
} // :cond_0
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).getFreeFormScale ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormScale()F
/* sub-float/2addr v1, p2 */
v1 = java.lang.Math .abs ( v1 );
/* const v2, 0x3a83126f # 0.001f */
/* cmpg-float v1, v1, v2 */
/* if-gez v1, :cond_1 */
return;
/* .line 286 */
} // :cond_1
(( com.android.server.wm.MiuiFreeFormActivityStack ) v0 ).setFreeformScale ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setFreeformScale(F)V
/* .line 287 */
return;
} // .end method
public void setRequestedOrientation ( Integer p0, com.android.server.wm.Task p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "requestedOrientation" # I */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .param p3, "noAnimation" # Z */
/* .line 1199 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setRequestedOrientation task: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " requestedOrientation: "; // const-string v1, " requestedOrientation: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 1201 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-nez p2, :cond_0 */
/* .line 1202 */
} // :cond_0
v0 = (( com.android.server.wm.Task ) p2 ).getRootTaskId ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTaskId()I
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1203 */
/* .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1204 */
v1 = android.util.MiuiMultiWindowUtils .isOrientationLandscape ( p1 );
/* iput-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsLandcapeFreeform:Z */
/* .line 1206 */
} // :cond_1
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda14; */
/* invoke-direct {v2, p0, p2, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda14;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Lcom/android/server/wm/Task;I)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1213 */
return;
/* .line 1201 */
} // .end local v0 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_2
} // :goto_0
return;
} // .end method
public Boolean shouldAddingToTask ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "shortComponentName" # Ljava/lang/String; */
/* .line 364 */
v0 = android.util.MiuiMultiWindowAdapter .getLaunchInTaskList ( );
} // .end method
public Boolean shouldAdjustFreeformLayer ( Integer p0, android.window.TransitionInfo p1 ) {
/* .locals 6 */
/* .param p1, "type" # I */
/* .param p2, "info" # Landroid/window/TransitionInfo; */
/* .line 2239 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2240 */
/* .local v0, "shouldAdjustLayer":Z */
int v1 = 1; // const/4 v1, 0x1
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 2241 */
v2 = (( android.window.TransitionInfo ) p2 ).getChanges ( ); // invoke-virtual {p2}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;
/* sub-int/2addr v2, v1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_1 */
/* .line 2242 */
(( android.window.TransitionInfo ) p2 ).getChanges ( ); // invoke-virtual {p2}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;
/* check-cast v3, Landroid/window/TransitionInfo$Change; */
/* .line 2243 */
/* .local v3, "c":Landroid/window/TransitionInfo$Change; */
(( android.window.TransitionInfo$Change ) v3 ).getContainer ( ); // invoke-virtual {v3}, Landroid/window/TransitionInfo$Change;->getContainer()Landroid/window/WindowContainerToken;
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 2244 */
(( android.window.TransitionInfo$Change ) v3 ).getContainer ( ); // invoke-virtual {v3}, Landroid/window/TransitionInfo$Change;->getContainer()Landroid/window/WindowContainerToken;
(( android.window.WindowContainerToken ) v4 ).asBinder ( ); // invoke-virtual {v4}, Landroid/window/WindowContainerToken;->asBinder()Landroid/os/IBinder;
com.android.server.wm.WindowContainer .fromBinder ( v4 );
/* .line 2245 */
/* .local v4, "wc":Lcom/android/server/wm/WindowContainer; */
if ( v4 != null) { // if-eqz v4, :cond_0
v5 = (( com.android.server.wm.WindowContainer ) v4 ).inFreeformWindowingMode ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->inFreeformWindowingMode()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 2246 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2247 */
/* .line 2241 */
} // .end local v3 # "c":Landroid/window/TransitionInfo$Change;
} // .end local v4 # "wc":Lcom/android/server/wm/WindowContainer;
} // :cond_0
/* add-int/lit8 v2, v2, -0x1 */
/* .line 2253 */
} // .end local v2 # "i":I
} // :cond_1
} // :goto_1
if ( p2 != null) { // if-eqz p2, :cond_2
v2 = (( android.window.TransitionInfo ) p2 ).getChanges ( ); // invoke-virtual {p2}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;
/* if-nez v2, :cond_4 */
} // :cond_2
/* if-eq p1, v1, :cond_3 */
int v2 = 3; // const/4 v2, 0x3
/* if-ne p1, v2, :cond_5 */
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 2256 */
} // :cond_4
/* .line 2258 */
} // :cond_5
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean shouldForegroundPin ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1342 */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getMiuiFreeFormActivityStackForMiuiFB ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 1343 */
/* .local v0, "stack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = this.mTask;
/* if-nez v1, :cond_0 */
/* .line 1346 */
} // :cond_0
v1 = this.mFreeFormGestureController;
v1 = (( com.android.server.wm.MiuiFreeFormGestureController ) v1 ).needForegroundPin ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->needForegroundPin(Lcom/android/server/wm/MiuiFreeFormActivityStack;)Z
/* iput-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z */
/* .line 1347 */
/* iget-boolean v1, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mIsForegroundPin:Z */
/* .line 1344 */
} // :cond_1
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void showHiddenTaskIfRemoveSpecificChild ( com.android.server.wm.WindowContainer p0 ) {
/* .locals 4 */
/* .param p1, "child" # Lcom/android/server/wm/WindowContainer; */
/* .line 1395 */
if ( p1 != null) { // if-eqz p1, :cond_1
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
(( com.android.server.wm.ActivityRecord ) v0 ).getTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1396 */
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
v0 = this.intent;
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
v0 = this.intent;
(( android.content.Intent ) v0 ).getComponent ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = android.util.MiuiMultiWindowAdapter.SHOW_HIDDEN_TASK_IF_FINISHED_WHITE_LIST_ACTIVITY;
/* .line 1398 */
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
v1 = this.intent;
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
v0 = (( android.content.ComponentName ) v1 ).getClassName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1399 */
v0 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1400 */
/* .local v1, "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.mTask;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
v2 = this.intent;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
v2 = this.intent;
/* .line 1401 */
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
v2 = this.intent;
/* .line 1402 */
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
v2 = this.intent;
/* .line 1403 */
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( com.android.server.wm.WindowContainer ) p1 ).asActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
(( com.android.server.wm.ActivityRecord ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
v3 = this.mCallingPackage;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
/* .line 1404 */
(( com.android.server.wm.Task ) v2 ).getTopNonFinishingActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
(( com.android.server.wm.Task ) v2 ).getTopNonFinishingActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
v2 = this.intent;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mTask;
/* .line 1405 */
(( com.android.server.wm.Task ) v2 ).getTopNonFinishingActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
v2 = this.intent;
(( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = android.util.MiuiMultiWindowAdapter.HIDE_SELF_IF_NEW_FREEFORM_TASK_WHITE_LIST_ACTIVITY;
v3 = this.mTask;
/* .line 1407 */
(( com.android.server.wm.Task ) v3 ).getTopNonFinishingActivity ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
v3 = this.intent;
(( android.content.Intent ) v3 ).getComponent ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
v2 = (( android.content.ComponentName ) v3 ).getClassName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1408 */
v2 = this.mTask;
(( com.android.server.wm.Task ) v2 ).getSyncTransaction ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getSyncTransaction()Landroid/view/SurfaceControl$Transaction;
v3 = this.mTask;
(( com.android.server.wm.Task ) v3 ).getSurfaceControl ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;
(( android.view.SurfaceControl$Transaction ) v2 ).show ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 1410 */
} // .end local v1 # "ffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_0
/* goto/16 :goto_0 */
/* .line 1412 */
} // :cond_1
return;
} // .end method
public void showOpenMiuiOptimizationToast ( ) {
/* .locals 2 */
/* .line 1415 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeFormManagerService$2;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1424 */
return;
} // .end method
public void showPropotionalFreeformToast ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "applicationUsedInFreeform" # Ljava/lang/String; */
/* .line 563 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$1; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$1;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 574 */
return;
} // .end method
public Boolean skipStartActivity ( com.android.server.wm.Task p0, android.app.ActivityOptions p1, com.android.server.wm.ActivityRecord p2, com.android.server.wm.ActivityRecord p3, com.android.server.wm.TaskDisplayArea p4 ) {
/* .locals 5 */
/* .param p1, "reusedTask" # Lcom/android/server/wm/Task; */
/* .param p2, "activityOption" # Landroid/app/ActivityOptions; */
/* .param p3, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .param p5, "taskDisplayArea" # Lcom/android/server/wm/TaskDisplayArea; */
/* .line 1249 */
int v0 = 1; // const/4 v0, 0x1
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
if ( p1 != null) { // if-eqz p1, :cond_0
com.android.server.wm.MiuiSoScManagerStub .get ( );
v2 = (( com.android.server.wm.MiuiSoScManagerStub ) v2 ).isInSoScMode ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/wm/MiuiSoScManagerStub;->isInSoScMode(Lcom/android/server/wm/WindowContainer;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1250 */
v2 = (( android.app.ActivityOptions ) p2 ).getLaunchWindowingMode ( ); // invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I
int v3 = 5; // const/4 v3, 0x5
/* if-ne v2, v3, :cond_0 */
/* .line 1251 */
/* iget v2, p1, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).fromSoscToFreeform ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fromSoscToFreeform(I)V
/* .line 1252 */
final String v2 = "Skip start activity from sosc to freeform."; // const-string v2, "Skip start activity from sosc to freeform."
android.util.Slog .i ( v1,v2 );
/* .line 1253 */
/* .line 1256 */
} // :cond_0
if ( p4 != null) { // if-eqz p4, :cond_1
(( com.android.server.wm.ActivityRecord ) p4 ).getTask ( ); // invoke-virtual {p4}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .line 1257 */
/* .local v2, "sourceTask":Lcom/android/server/wm/Task; */
} // :goto_0
if ( p1 != null) { // if-eqz p1, :cond_2
v3 = (( com.android.server.wm.Task ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v3 != null) { // if-eqz v3, :cond_2
v3 = (( com.android.server.wm.Task ) p1 ).isMiuiFreeformExiting ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->isMiuiFreeformExiting()Z
if ( v3 != null) { // if-eqz v3, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 1259 */
com.android.server.wm.MiuiSoScManagerStub .get ( );
v4 = (( android.app.ActivityOptions ) p2 ).getLaunchWindowingMode ( ); // invoke-virtual {p2}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I
v3 = (( com.android.server.wm.MiuiSoScManagerStub ) v3 ).validateWindowingModeForSoSc ( v4, p3, v2, p5 ); // invoke-virtual {v3, v4, p3, v2, p5}, Lcom/android/server/wm/MiuiSoScManagerStub;->validateWindowingModeForSoSc(ILcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/Task;Lcom/android/server/wm/TaskDisplayArea;)I
int v4 = 6; // const/4 v4, 0x6
/* if-ne v3, v4, :cond_2 */
/* .line 1261 */
final String v3 = "Skip start activity via freeform is exiting"; // const-string v3, "Skip start activity via freeform is exiting"
android.util.Slog .i ( v1,v3 );
/* .line 1262 */
/* .line 1265 */
} // :cond_2
v0 = (( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).activeOrFullscreenFreeformTaskIfNeed ( p1, p2, p4, p3 ); // invoke-virtual {p0, p1, p2, p4, p3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->activeOrFullscreenFreeformTaskIfNeed(Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
} // .end method
public Integer startSmallFreeformFromNotification ( ) {
/* .locals 1 */
/* .line 543 */
v0 = this.mFreeFormGestureController;
v0 = this.mMiuiMultiWindowRecommendHelper;
v0 = (( com.android.server.wm.MiuiMultiWindowRecommendHelper ) v0 ).startSmallFreeformFromNotification ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->startSmallFreeformFromNotification()I
} // .end method
public void traverseTask ( com.android.server.wm.Task p0, java.util.function.Consumer p1 ) {
/* .locals 6 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/wm/Task;", */
/* "Ljava/util/function/Consumer<", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1427 */
/* .local p2, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Lcom/android/server/wm/ActivityRecord;>;" */
v0 = this.mActivityTaskManagerService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 1428 */
/* if-nez p1, :cond_0 */
/* .line 1429 */
try { // :try_start_0
/* monitor-exit v0 */
return;
/* .line 1432 */
} // :cond_0
int v1 = -2; // const/4 v1, -0x2
/* .line 1434 */
/* .local v1, "candidate":I */
v2 = (( com.android.server.wm.Task ) p1 ).getChildCount ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "activityIndex":I */
} // :goto_0
/* if-ltz v2, :cond_6 */
/* .line 1435 */
(( com.android.server.wm.Task ) p1 ).getChildAt ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/wm/Task;->getChildAt(I)Lcom/android/server/wm/WindowContainer;
(( com.android.server.wm.WindowContainer ) v3 ).asActivityRecord ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;
/* .line 1436 */
/* .local v3, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v3, :cond_1 */
/* .line 1437 */
/* .line 1439 */
} // :cond_1
v4 = (( com.android.server.wm.ActivityRecord ) v3 ).getOrientation ( v1 ); // invoke-virtual {v3, v1}, Lcom/android/server/wm/ActivityRecord;->getOrientation(I)I
/* .line 1440 */
/* .local v4, "orientation":I */
/* move v1, v4 */
/* .line 1446 */
int v5 = -1; // const/4 v5, -0x1
/* if-eq v4, v5, :cond_5 */
/* const/16 v5, 0xd */
/* if-eq v4, v5, :cond_5 */
/* const/16 v5, 0xa */
/* if-eq v4, v5, :cond_5 */
int v5 = 4; // const/4 v5, 0x4
/* if-eq v4, v5, :cond_5 */
int v5 = 2; // const/4 v5, 0x2
/* if-eq v4, v5, :cond_5 */
/* const/16 v5, 0xe */
/* if-ne v4, v5, :cond_2 */
/* .line 1455 */
} // :cond_2
int v5 = 3; // const/4 v5, 0x3
/* if-eq v4, v5, :cond_4 */
int v5 = -2; // const/4 v5, -0x2
/* if-ne v4, v5, :cond_3 */
/* .line 1457 */
/* .line 1463 */
} // :cond_3
/* .line 1464 */
/* monitor-exit v0 */
return;
/* .line 1434 */
} // .end local v3 # "activityRecord":Lcom/android/server/wm/ActivityRecord;
} // .end local v4 # "orientation":I
} // :cond_4
} // :goto_1
/* add-int/lit8 v2, v2, -0x1 */
/* .line 1452 */
/* .restart local v3 # "activityRecord":Lcom/android/server/wm/ActivityRecord; */
/* .restart local v4 # "orientation":I */
} // :cond_5
} // :goto_2
/* monitor-exit v0 */
return;
/* .line 1466 */
} // .end local v1 # "candidate":I
} // .end local v2 # "activityIndex":I
} // .end local v3 # "activityRecord":Lcom/android/server/wm/ActivityRecord;
} // .end local v4 # "orientation":I
} // :cond_6
/* monitor-exit v0 */
/* .line 1467 */
return;
/* .line 1466 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unPinFloatingWindowForActive ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 1144 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 1145 */
} // :cond_0
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda13; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda13;-><init>(Lcom/android/server/wm/MiuiFreeFormManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1152 */
return;
} // .end method
public Boolean unSupportedFreeformInDesktop ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "taskId" # I */
/* .line 2192 */
v0 = this.mActivityTaskManagerService;
v0 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v0 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda7; */
/* invoke-direct {v1, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService$$ExternalSyntheticLambda7;-><init>(I)V */
/* .line 2193 */
(( com.android.server.wm.TaskDisplayArea ) v0 ).getRootTask ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/TaskDisplayArea;->getRootTask(Ljava/util/function/Predicate;)Lcom/android/server/wm/Task;
/* .line 2194 */
/* .local v0, "target":Lcom/android/server/wm/Task; */
v1 = com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
/* xor-int/lit8 v1, v1, 0x1 */
} // .end method
public void unregisterFreeformCallback ( miui.app.IFreeformCallback p0 ) {
/* .locals 2 */
/* .param p1, "freeformCallback" # Lmiui/app/IFreeformCallback; */
/* .line 1098 */
v0 = this.mCallbacks;
/* monitor-enter v0 */
/* .line 1099 */
try { // :try_start_0
v1 = this.mCallbacks;
(( android.os.RemoteCallbackList ) v1 ).unregister ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 1100 */
/* monitor-exit v0 */
/* .line 1101 */
return;
/* .line 1100 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void unsetFreeformForceHideIfNeed ( android.window.WindowContainerTransaction$HierarchyOp p0, com.android.server.wm.WindowContainer p1 ) {
/* .locals 3 */
/* .param p1, "hop" # Landroid/window/WindowContainerTransaction$HierarchyOp; */
/* .param p2, "container" # Lcom/android/server/wm/WindowContainer; */
/* .line 259 */
(( com.android.server.wm.WindowContainer ) p2 ).asTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
if ( v0 != null) { // if-eqz v0, :cond_0
(( com.android.server.wm.WindowContainer ) p2 ).asTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
v0 = (( com.android.server.wm.Task ) v0 ).isForceHidden ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->isForceHidden()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( android.window.WindowContainerTransaction$HierarchyOp ) p1 ).isAlwaysOnTop ( ); // invoke-virtual {p1}, Landroid/window/WindowContainerTransaction$HierarchyOp;->isAlwaysOnTop()Z
/* if-nez v0, :cond_0 */
/* .line 260 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unsetforceHiden wc: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreeFormManagerService"; // const-string v1, "MiuiFreeFormManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 261 */
(( com.android.server.wm.WindowContainer ) p2 ).asTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
/* const/high16 v1, 0x10000 */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.Task ) v0 ).setForceHidden ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/Task;->setForceHidden(IZ)Z
/* .line 263 */
} // :cond_0
return;
} // .end method
public void updateAutoLayoutModeStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isOn" # Z */
/* .line 162 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mAutoLayoutModeOn:Z */
/* .line 163 */
return;
} // .end method
void updateDataFromSetting ( ) {
/* .locals 1 */
/* .line 360 */
v0 = this.mActivityTaskManagerService;
v0 = this.mContext;
(( com.android.server.wm.MiuiFreeFormManagerService ) p0 ).getApplicationUsedInFreeform ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getApplicationUsedInFreeform(Landroid/content/Context;)Ljava/lang/String;
this.mApplicationUsedInFreeform = v0;
/* .line 361 */
return;
} // .end method
