class com.android.server.wm.AccountHelper$1 implements android.accounts.AccountManagerCallback {
	 /* .source "AccountHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/AccountHelper;->addAccount(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Landroid/accounts/AccountManagerCallback<", */
/* "Landroid/os/Bundle;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.wm.AccountHelper this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.wm.AccountHelper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/AccountHelper; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 199 */
this.this$0 = p1;
this.val$context = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( android.accounts.AccountManagerFuture p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/accounts/AccountManagerFuture<", */
/* "Landroid/os/Bundle;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 203 */
/* .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;" */
try { // :try_start_0
/* check-cast v0, Landroid/os/Bundle; */
final String v1 = "intent"; // const-string v1, "intent"
(( android.os.Bundle ) v0 ).getParcelable ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v0, Landroid/content/Intent; */
/* .line 204 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 205 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 206 */
v1 = this.val$context;
(( android.content.Context ) v1 ).startActivity ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 211 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_0
/* .line 208 */
/* :catch_0 */
/* move-exception v0 */
/* .line 209 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MiuiPermision"; // const-string v1, "MiuiPermision"
final String v2 = "addAccount"; // const-string v2, "addAccount"
android.util.Log .i ( v1,v2 );
/* .line 210 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 212 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
