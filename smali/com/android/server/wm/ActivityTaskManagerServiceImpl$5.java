class com.android.server.wm.ActivityTaskManagerServiceImpl$5 implements java.lang.Runnable {
	 /* .source "ActivityTaskManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.ActivityTaskManagerServiceImpl this$0; //synthetic
final android.content.pm.ApplicationInfo val$info; //synthetic
final java.lang.String val$reason; //synthetic
/* # direct methods */
 com.android.server.wm.ActivityTaskManagerServiceImpl$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 917 */
this.this$0 = p1;
this.val$info = p2;
this.val$reason = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 921 */
try { // :try_start_0
v0 = this.this$0;
v1 = this.val$info;
v2 = this.val$reason;
com.android.server.wm.ActivityTaskManagerServiceImpl .-$$Nest$mstartSubScreenUi ( v0,v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 924 */
/* .line 922 */
/* :catch_0 */
/* move-exception v0 */
/* .line 923 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ATMSImpl"; // const-string v1, "ATMSImpl"
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 925 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
