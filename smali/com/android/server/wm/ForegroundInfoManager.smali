.class public Lcom/android/server/wm/ForegroundInfoManager;
.super Ljava/lang/Object;
.source "ForegroundInfoManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ProcessManager"


# instance fields
.field private final mActivityChangeListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lmiui/process/IActivityChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mActivityLock:Ljava/lang/Object;

.field private mForegroundInfo:Lmiui/process/ForegroundInfo;

.field private final mForegroundInfoListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lmiui/process/IForegroundInfoListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mForegroundLock:Ljava/lang/Object;

.field private mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

.field private final mForegroundWindowListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lmiui/process/IForegroundWindowListener;",
            ">;"
        }
    .end annotation
.end field

.field private mLastActivityComponent:Landroid/content/ComponentName;

.field private mProcessManagerService:Lcom/android/server/am/ProcessManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ProcessManagerService;)V
    .locals 1
    .param p1, "pms"    # Lcom/android/server/am/ProcessManagerService;

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    .line 34
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    .line 36
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowListeners:Landroid/os/RemoteCallbackList;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityLock:Ljava/lang/Object;

    .line 76
    iput-object p1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mProcessManagerService:Lcom/android/server/am/ProcessManagerService;

    .line 77
    new-instance v0, Lmiui/process/ForegroundInfo;

    invoke-direct {v0}, Lmiui/process/ForegroundInfo;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    .line 78
    new-instance v0, Lmiui/process/ForegroundInfo;

    invoke-direct {v0}, Lmiui/process/ForegroundInfo;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    .line 79
    return-void
.end method

.method private isMultiWindowChanged(Landroid/content/pm/ApplicationInfo;)Z
    .locals 3
    .param p1, "multiWindowAppInfo"    # Landroid/content/pm/ApplicationInfo;

    .line 82
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v0, v0, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundPackageName:Ljava/lang/String;

    .line 83
    .local v0, "lastPkg":Ljava/lang/String;
    const/4 v1, 0x1

    if-nez p1, :cond_1

    .line 84
    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 86
    :cond_1
    iget-object v2, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/2addr v1, v2

    return v1
.end method

.method private notifyForegroundInfoLocked()V
    .locals 3

    .line 183
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lmiui/process/IForegroundInfoListener;

    iget-object v2, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    invoke-interface {v1, v2}, Lmiui/process/IForegroundInfoListener;->onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    goto :goto_1

    .line 186
    :catch_0
    move-exception v1

    .line 187
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 183
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 190
    .end local v0    # "i":I
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 191
    return-void
.end method

.method private notifyForegroundWindowLocked()V
    .locals 3

    .line 194
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 196
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lmiui/process/IForegroundWindowListener;

    iget-object v2, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    invoke-interface {v1, v2}, Lmiui/process/IForegroundWindowListener;->onForegroundWindowChanged(Lmiui/process/ForegroundInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    goto :goto_1

    .line 197
    :catch_0
    move-exception v1

    .line 198
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 194
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 201
    .end local v0    # "i":I
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 202
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 320
    const-string v0, "ForegroundInfo Listener:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v0

    .line 322
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 323
    const-string v2, "  #"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 324
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    .line 325
    const-string v2, ": "

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 326
    iget-object v2, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lmiui/process/IForegroundInfoListener;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 322
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 328
    .end local v1    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 329
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mForegroundInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 332
    const-string v0, "ActivityChange Listener:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 333
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityLock:Ljava/lang/Object;

    monitor-enter v1

    .line 334
    :try_start_1
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_1

    .line 335
    const-string v2, "  #"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 336
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 337
    const-string v2, ": "

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 338
    iget-object v2, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 334
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 340
    .end local v0    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 341
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mLastActivityComponent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 343
    return-void

    .line 341
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 329
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method public getForegroundInfo()Lmiui/process/ForegroundInfo;
    .locals 3

    .line 205
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v0

    .line 206
    :try_start_0
    new-instance v1, Lmiui/process/ForegroundInfo;

    iget-object v2, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    invoke-direct {v1, v2}, Lmiui/process/ForegroundInfo;-><init>(Lmiui/process/ForegroundInfo;)V

    monitor-exit v0

    return-object v1

    .line 207
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isForegroundApp(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 215
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v0, v0, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    if-ne v0, p2, :cond_0

    goto :goto_0

    .line 219
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 217
    :cond_1
    :goto_0
    if-nez p1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v0, v0, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_1
    return v0
.end method

.method public notifyActivitiesChangedLocked(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "curComponent"    # Landroid/content/ComponentName;

    .line 286
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 288
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lmiui/process/IActivityChangeListener;

    .line 289
    .local v1, "listener":Lmiui/process/IActivityChangeListener;
    iget-object v2, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v2

    .line 290
    .local v2, "cookie":Ljava/lang/Object;
    invoke-virtual {p0, v1, v2, p1}, Lcom/android/server/wm/ForegroundInfoManager;->notifyActivityChangedIfNeededLocked(Lmiui/process/IActivityChangeListener;Ljava/lang/Object;Landroid/content/ComponentName;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    .end local v1    # "listener":Lmiui/process/IActivityChangeListener;
    .end local v2    # "cookie":Ljava/lang/Object;
    goto :goto_1

    .line 291
    :catch_0
    move-exception v1

    .line 292
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 286
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 295
    .end local v0    # "i":I
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 296
    return-void
.end method

.method public notifyActivityChanged(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "curActivityComponent"    # Landroid/content/ComponentName;

    .line 274
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityLock:Ljava/lang/Object;

    monitor-enter v0

    .line 275
    if-eqz p1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    .line 276
    invoke-virtual {p1, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 280
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->notifyActivitiesChangedLocked(Landroid/content/ComponentName;)V

    .line 281
    iput-object p1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    .line 282
    monitor-exit v0

    .line 283
    return-void

    .line 277
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    .line 282
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public notifyActivityChangedIfNeededLocked(Lmiui/process/IActivityChangeListener;Ljava/lang/Object;Landroid/content/ComponentName;)V
    .locals 5
    .param p1, "listener"    # Lmiui/process/IActivityChangeListener;
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "curComponent"    # Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 300
    if-eqz p2, :cond_5

    instance-of v0, p2, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;

    if-nez v0, :cond_0

    goto :goto_2

    .line 304
    :cond_0
    move-object v0, p2

    check-cast v0, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;

    .line 305
    .local v0, "info":Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 306
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 307
    .local v1, "lastPackage":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/android/server/wm/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    if-eqz v3, :cond_2

    .line 308
    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    nop

    .line 310
    .local v2, "lastActivity":Ljava/lang/String;
    :goto_1
    iget-object v3, v0, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;->targetPackages:Ljava/util/List;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;->targetActivities:Ljava/util/List;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;->targetPackages:Ljava/util/List;

    .line 311
    invoke-virtual {p3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;->targetPackages:Ljava/util/List;

    .line 312
    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;->targetActivities:Ljava/util/List;

    .line 313
    invoke-virtual {p3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;->targetActivities:Ljava/util/List;

    .line 314
    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 315
    :cond_3
    iget-object v3, p0, Lcom/android/server/wm/ForegroundInfoManager;->mLastActivityComponent:Landroid/content/ComponentName;

    invoke-interface {p1, v3, p3}, Lmiui/process/IActivityChangeListener;->onActivityChanged(Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    .line 317
    :cond_4
    return-void

    .line 301
    .end local v0    # "info":Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;
    .end local v1    # "lastPackage":Ljava/lang/String;
    .end local v2    # "lastActivity":Ljava/lang/String;
    :cond_5
    :goto_2
    return-void
.end method

.method public notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V
    .locals 10
    .param p1, "changedInfo"    # Lcom/android/server/wm/FgActivityChangedInfo;

    .line 91
    iget-object v0, p1, Lcom/android/server/wm/FgActivityChangedInfo;->record:Lcom/android/server/wm/ActivityRecord;

    .line 92
    .local v0, "foregroundRecord":Lcom/android/server/wm/ActivityRecord;
    iget-object v1, p1, Lcom/android/server/wm/FgActivityChangedInfo;->state:Lcom/android/server/wm/ActivityRecord$State;

    .line 93
    .local v1, "state":Lcom/android/server/wm/ActivityRecord$State;
    iget v2, p1, Lcom/android/server/wm/FgActivityChangedInfo;->pid:I

    .line 94
    .local v2, "pid":I
    iget-object v3, p1, Lcom/android/server/wm/FgActivityChangedInfo;->multiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

    .line 96
    .local v3, "multiWindowAppInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v4, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v4

    .line 97
    :try_start_0
    iget-object v5, v0, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 98
    .local v5, "foregroundAppInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v5, :cond_4

    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v6, v6, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 99
    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 100
    invoke-direct {p0, v3}, Lcom/android/server/wm/ForegroundInfoManager;->isMultiWindowChanged(Landroid/content/pm/ApplicationInfo;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v6, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    iget v7, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v6, v7, :cond_0

    goto/16 :goto_2

    .line 106
    :cond_0
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    invoke-virtual {v6}, Lmiui/process/ForegroundInfo;->resetFlags()V

    .line 107
    iget-object v6, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v6}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->getIsColdStart()Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v6, Lcom/android/server/wm/ActivityRecord$State;->INITIALIZING:Lcom/android/server/wm/ActivityRecord$State;

    if-ne v1, v6, :cond_1

    .line 108
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lmiui/process/ForegroundInfo;->addFlags(I)V

    .line 111
    :cond_1
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v7, v6, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iput-object v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    .line 112
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v7, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    iput v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    .line 113
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v7, v6, Lmiui/process/ForegroundInfo;->mForegroundPid:I

    iput v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundPid:I

    .line 115
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v7, v6, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    .line 116
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v7, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v7, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    .line 117
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iput v2, v6, Lmiui/process/ForegroundInfo;->mForegroundPid:I

    .line 119
    if-eqz v3, :cond_2

    .line 120
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v7, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundPackageName:Ljava/lang/String;

    .line 121
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v7, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I

    goto :goto_0

    .line 123
    :cond_2
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    const/4 v7, 0x0

    iput-object v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundPackageName:Ljava/lang/String;

    .line 124
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    const/4 v7, -0x1

    iput v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I

    .line 127
    :goto_0
    invoke-direct {p0}, Lcom/android/server/wm/ForegroundInfoManager;->notifyForegroundInfoLocked()V

    .line 128
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mProcessManagerService:Lcom/android/server/am/ProcessManagerService;

    iget-object v7, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v7, v7, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v8, v0, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 130
    iget-object v9, v0, Lcom/android/server/wm/ActivityRecord;->resultTo:Lcom/android/server/wm/ActivityRecord;

    if-eqz v9, :cond_3

    iget-object v9, v0, Lcom/android/server/wm/ActivityRecord;->resultTo:Lcom/android/server/wm/ActivityRecord;

    iget-object v9, v9, Lcom/android/server/wm/ActivityRecord;->processName:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string v9, ""

    .line 128
    :goto_1
    invoke-virtual {v6, v7, v8, v9}, Lcom/android/server/am/ProcessManagerService;->foregroundInfoChanged(Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 131
    invoke-static {}, Lcom/android/server/wm/ActivityStarterStub;->get()Lcom/android/server/wm/ActivityStarterStub;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v7, v7, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v8, v8, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-virtual {v6, v7, v8}, Lcom/android/server/wm/ActivityStarterStub;->updateLastStartActivityUid(Ljava/lang/String;I)V

    .line 136
    invoke-static {}, Lcom/android/server/am/MemoryStandardProcessControlStub;->getInstance()Lcom/android/server/am/MemoryStandardProcessControlStub;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget v7, v7, Lmiui/process/ForegroundInfo;->mLastForegroundPid:I

    iget-object v8, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfo:Lmiui/process/ForegroundInfo;

    iget-object v8, v8, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    invoke-interface {v6, v7, v8}, Lcom/android/server/am/MemoryStandardProcessControlStub;->reportAppStopped(ILjava/lang/String;)V

    .line 139
    invoke-static {}, Lcom/android/server/am/MemoryStandardProcessControlStub;->getInstance()Lcom/android/server/am/MemoryStandardProcessControlStub;

    move-result-object v6

    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v6, v2, v7}, Lcom/android/server/am/MemoryStandardProcessControlStub;->reportAppResumed(ILjava/lang/String;)V

    .line 141
    .end local v5    # "foregroundAppInfo":Landroid/content/pm/ApplicationInfo;
    monitor-exit v4

    .line 142
    return-void

    .line 102
    .restart local v5    # "foregroundAppInfo":Landroid/content/pm/ApplicationInfo;
    :cond_4
    :goto_2
    const-string v6, "ProcessManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "skip notify foregroundAppInfo:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    monitor-exit v4

    return-void

    .line 141
    .end local v5    # "foregroundAppInfo":Landroid/content/pm/ApplicationInfo;
    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public notifyForegroundWindowChanged(Lcom/android/server/wm/FgWindowChangedInfo;)V
    .locals 9
    .param p1, "changedInfo"    # Lcom/android/server/wm/FgWindowChangedInfo;

    .line 145
    iget-object v0, p1, Lcom/android/server/wm/FgWindowChangedInfo;->record:Lcom/android/server/wm/ActivityRecord;

    .line 146
    .local v0, "foregroundRecord":Lcom/android/server/wm/ActivityRecord;
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;

    move-result-object v1

    .line 147
    .local v1, "state":Lcom/android/server/wm/ActivityRecord$State;
    iget v2, p1, Lcom/android/server/wm/FgWindowChangedInfo;->pid:I

    .line 148
    .local v2, "pid":I
    iget-object v3, p1, Lcom/android/server/wm/FgWindowChangedInfo;->multiWindowAppInfo:Landroid/content/pm/ApplicationInfo;

    .line 150
    .local v3, "multiWindowAppInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v4, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v4

    .line 151
    :try_start_0
    iget-object v5, v0, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 152
    .local v5, "foregroundAppInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v5, :cond_0

    .line 153
    const-string v6, "ProcessManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "skip notify foregroundAppInfo:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    monitor-exit v4

    return-void

    .line 157
    :cond_0
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    invoke-virtual {v6}, Lmiui/process/ForegroundInfo;->resetFlags()V

    .line 158
    iget-object v6, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v6}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->getIsColdStart()Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v6, Lcom/android/server/wm/ActivityRecord$State;->INITIALIZING:Lcom/android/server/wm/ActivityRecord$State;

    if-ne v1, v6, :cond_1

    .line 159
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lmiui/process/ForegroundInfo;->addFlags(I)V

    .line 162
    :cond_1
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    iget-object v7, v6, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iput-object v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    .line 163
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    iget v7, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    iput v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundUid:I

    .line 164
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    iget v7, v6, Lmiui/process/ForegroundInfo;->mForegroundPid:I

    iput v7, v6, Lmiui/process/ForegroundInfo;->mLastForegroundPid:I

    .line 166
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    iget-object v7, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v7, v6, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    .line 167
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    iget v7, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v7, v6, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    .line 168
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    iput v2, v6, Lmiui/process/ForegroundInfo;->mForegroundPid:I

    .line 170
    if-eqz v3, :cond_2

    .line 171
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    iget-object v7, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundPackageName:Ljava/lang/String;

    .line 172
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    iget v7, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I

    goto :goto_0

    .line 174
    :cond_2
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    const/4 v7, 0x0

    iput-object v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundPackageName:Ljava/lang/String;

    .line 175
    iget-object v6, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowInfo:Lmiui/process/ForegroundInfo;

    const/4 v7, -0x1

    iput v7, v6, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I

    .line 178
    :goto_0
    invoke-direct {p0}, Lcom/android/server/wm/ForegroundInfoManager;->notifyForegroundWindowLocked()V

    .line 179
    .end local v5    # "foregroundAppInfo":Landroid/content/pm/ApplicationInfo;
    monitor-exit v4

    .line 180
    return-void

    .line 179
    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public registerActivityChangeListener(Ljava/util/List;Ljava/util/List;Lmiui/process/IActivityChangeListener;)V
    .locals 3
    .param p3, "listener"    # Lmiui/process/IActivityChangeListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lmiui/process/IActivityChangeListener;",
            ")V"
        }
    .end annotation

    .line 256
    .local p1, "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p3, :cond_0

    .line 257
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityLock:Ljava/lang/Object;

    monitor-enter v0

    .line 258
    :try_start_0
    new-instance v1, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;-><init>(Lcom/android/server/wm/ForegroundInfoManager;ILjava/util/List;Ljava/util/List;)V

    .line 260
    .local v1, "info":Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;
    iget-object v2, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, p3, v1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 261
    nop

    .end local v1    # "info":Lcom/android/server/wm/ForegroundInfoManager$ActivityChangeInfo;
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 263
    :cond_0
    :goto_0
    return-void
.end method

.method public registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/process/IForegroundInfoListener;

    .line 223
    if-eqz p1, :cond_0

    .line 224
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v0

    .line 225
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 226
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 228
    :cond_0
    :goto_0
    return-void
.end method

.method public registerForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/process/IForegroundWindowListener;

    .line 239
    if-eqz p1, :cond_0

    .line 240
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v0

    .line 241
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 242
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 244
    :cond_0
    :goto_0
    return-void
.end method

.method public unregisterActivityChangeListener(Lmiui/process/IActivityChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/process/IActivityChangeListener;

    .line 266
    if-eqz p1, :cond_0

    .line 267
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityLock:Ljava/lang/Object;

    monitor-enter v0

    .line 268
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mActivityChangeListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 269
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 271
    :cond_0
    :goto_0
    return-void
.end method

.method public unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/process/IForegroundInfoListener;

    .line 231
    if-eqz p1, :cond_0

    .line 232
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v0

    .line 233
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundInfoListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 234
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 236
    :cond_0
    :goto_0
    return-void
.end method

.method public unregisterForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/process/IForegroundWindowListener;

    .line 247
    if-eqz p1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundLock:Ljava/lang/Object;

    monitor-enter v0

    .line 249
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/ForegroundInfoManager;->mForegroundWindowListeners:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 250
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 252
    :cond_0
    :goto_0
    return-void
.end method
