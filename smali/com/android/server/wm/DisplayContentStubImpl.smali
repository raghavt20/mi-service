.class public Lcom/android/server/wm/DisplayContentStubImpl;
.super Ljava/lang/Object;
.source "DisplayContentStubImpl.java"

# interfaces
.implements Lcom/android/server/wm/DisplayContentStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;,
        Lcom/android/server/wm/DisplayContentStubImpl$MutableDisplayContentImpl;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "miui.systemui.keyguard.Wallpaper"

.field private static final DISPLAY_DISABLE_SYSTEM_ANIMATION:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final FOCUSED_WINDOW_RETRY_TIME:I = 0xbb8

.field private static final FPS_COMMON:I = 0x3c

.field private static final NO_FOCUS_WINDOW_TIMEOUT:I = 0x1388

.field private static final PROVISION_CONGRATULATION_ACTIVITY:Ljava/lang/String; = "com.android.provision/.activities.CongratulationActivity"

.field private static final PROVISION_DEFAULT_ACTIVITY:Ljava/lang/String; = "com.android.provision/.activities.DefaultActivity"

.field private static final SCREEN_DPI_MODE:I = 0x18

.field private static final WHITE_LIST_ALLOW_FIXED_ROTATION_FOR_NOT_OCCLUDES_PARENT:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static sCurrentRefreshRate:I

.field public static sLastUserRefreshRate:I


# instance fields
.field private final KEY_BLUR_ENABLE:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private final blurEnabled:Z

.field private mBlur:I

.field private mContext:Landroid/content/Context;

.field private mFocusRunnable:Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;

.field private mHiddenWindowList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lcom/android/server/wm/WindowState;",
            ">;"
        }
    .end annotation
.end field

.field private mIsCameraRotating:Z

.field private mPowerConsumptionServiceInternal:Lcom/android/server/PowerConsumptionServiceInternal;

.field private mSkipUpdateSurfaceRotation:Z

.field private mSkipVisualDisplayTransition:Z

.field private mStatusBarVisible:Z

.field private mTimestamp:J


# direct methods
.method public static synthetic $r8$lambda$2tFncwLoQkNxfq66P9eO3es2Sms(Lcom/android/server/wm/WindowToken;)I
    .locals 0

    invoke-virtual {p0}, Lcom/android/server/wm/WindowToken;->getWindowLayerFromType()I

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$VG-blJgiOwYfKhnQUYIgLfpxRUg(Lcom/android/server/wm/DisplayContentStubImpl;Lcom/android/server/wm/WindowState;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/DisplayContentStubImpl;->lambda$focusChangedLw$0(Lcom/android/server/wm/WindowState;J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDISPLAY_DISABLE_SYSTEM_ANIMATION()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/android/server/wm/DisplayContentStubImpl;->DISPLAY_DISABLE_SYSTEM_ANIMATION:Ljava/util/ArrayList;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetWHITE_LIST_ALLOW_FIXED_ROTATION_FOR_NOT_OCCLUDES_PARENT()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/android/server/wm/DisplayContentStubImpl;->WHITE_LIST_ALLOW_FIXED_ROTATION_FOR_NOT_OCCLUDES_PARENT:Ljava/util/ArrayList;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 94
    const/4 v0, -0x1

    sput v0, Lcom/android/server/wm/DisplayContentStubImpl;->sLastUserRefreshRate:I

    .line 95
    sput v0, Lcom/android/server/wm/DisplayContentStubImpl;->sCurrentRefreshRate:I

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/DisplayContentStubImpl;->WHITE_LIST_ALLOW_FIXED_ROTATION_FOR_NOT_OCCLUDES_PARENT:Ljava/util/ArrayList;

    .line 118
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/wm/DisplayContentStubImpl;->DISPLAY_DISABLE_SYSTEM_ANIMATION:Ljava/util/ArrayList;

    .line 122
    const-string v2, "com.miui.mediaviewer/com.miui.video.gallery.galleryvideo.FrameLocalPlayActivity"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v2, "com.miui.gallery/.activity.InternalPhotoPageActivity"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    const-string v0, "com.miui.carlink"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    const-string v0, "com.xiaomi.ucar.minimap"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const-string v0, "DisplayContentStubImpl"

    iput-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->TAG:Ljava/lang/String;

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mStatusBarVisible:Z

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipVisualDisplayTransition:Z

    .line 102
    const-string v1, "persist.sys.background_blur_status_default"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->blurEnabled:Z

    .line 104
    const-string v1, "background_blur_enable"

    iput-object v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->KEY_BLUR_ENABLE:Ljava/lang/String;

    .line 107
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mFocusRunnable:Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;

    .line 108
    iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipUpdateSurfaceRotation:Z

    .line 109
    iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mIsCameraRotating:Z

    .line 110
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mHiddenWindowList:Ljava/util/HashSet;

    .line 190
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J

    return-void
.end method

.method private computeBlurConfiguration(Landroid/content/Context;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .line 216
    if-nez p1, :cond_0

    return-void

    .line 217
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "background_blur_enable"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 218
    .local v0, "blur":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 219
    iput v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I

    goto :goto_0

    .line 220
    :cond_1
    if-nez v0, :cond_2

    .line 221
    const/4 v1, 0x2

    iput v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I

    goto :goto_0

    .line 222
    :cond_2
    iget-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->blurEnabled:Z

    if-eqz v2, :cond_3

    .line 223
    iput v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I

    goto :goto_0

    .line 225
    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I

    .line 227
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "computeBlurConfiguration blurEnabled= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->blurEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " blur= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mBlur= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DisplayContentStubImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    return-void
.end method

.method private static getCurrentRefreshRate()I
    .locals 4

    .line 284
    const/4 v0, -0x1

    .line 285
    .local v0, "refreshRate":I
    nop

    .line 286
    const-string v1, "persist.vendor.dfps.level"

    const/16 v2, 0x3c

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 287
    .local v1, "fps":I
    nop

    .line 288
    const-string v2, "persist.vendor.power.dfps.level"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 289
    .local v2, "powerFps":I
    if-eqz v2, :cond_0

    .line 290
    move v0, v2

    goto :goto_0

    .line 292
    :cond_0
    move v0, v1

    .line 294
    :goto_0
    return v0
.end method

.method static getFullScreenIndex(Lcom/android/server/wm/Task;Lcom/android/server/wm/WindowList;I)I
    .locals 5
    .param p0, "stack"    # Lcom/android/server/wm/Task;
    .param p2, "targetPosition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/wm/Task;",
            "Lcom/android/server/wm/WindowList<",
            "Lcom/android/server/wm/Task;",
            ">;I)I"
        }
    .end annotation

    .line 237
    .local p1, "children":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<Lcom/android/server/wm/Task;>;"
    invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 238
    invoke-virtual {p1}, Lcom/android/server/wm/WindowList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/Task;

    .line 239
    .local v2, "tStack":Lcom/android/server/wm/Task;
    invoke-virtual {v2}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_0

    .line 240
    invoke-virtual {p1, v2}, Lcom/android/server/wm/WindowList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_0

    .line 241
    invoke-virtual {p1, v2}, Lcom/android/server/wm/WindowList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 p2, v0, -0x1

    .line 242
    goto :goto_1

    .line 244
    .end local v2    # "tStack":Lcom/android/server/wm/Task;
    :cond_0
    goto :goto_0

    .line 246
    :cond_1
    :goto_1
    return p2
.end method

.method static getFullScreenIndex(ZLcom/android/server/wm/Task;Lcom/android/server/wm/WindowList;IZ)I
    .locals 4
    .param p0, "toTop"    # Z
    .param p1, "stack"    # Lcom/android/server/wm/Task;
    .param p3, "targetPosition"    # I
    .param p4, "adding"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/android/server/wm/Task;",
            "Lcom/android/server/wm/WindowList<",
            "Lcom/android/server/wm/Task;",
            ">;IZ)I"
        }
    .end annotation

    .line 251
    .local p2, "children":Lcom/android/server/wm/WindowList;, "Lcom/android/server/wm/WindowList<Lcom/android/server/wm/Task;>;"
    if-eqz p0, :cond_3

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 252
    invoke-virtual {p2}, Lcom/android/server/wm/WindowList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/Task;

    .line 253
    .local v1, "tStack":Lcom/android/server/wm/Task;
    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_2

    .line 254
    invoke-virtual {p2, v1}, Lcom/android/server/wm/WindowList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 255
    .local v0, "topChildPosition":I
    if-eqz p4, :cond_0

    move v2, v0

    goto :goto_1

    :cond_0
    if-lez v0, :cond_1

    add-int/lit8 v2, v0, -0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    move p3, v2

    .line 256
    goto :goto_2

    .line 258
    .end local v0    # "topChildPosition":I
    .end local v1    # "tStack":Lcom/android/server/wm/Task;
    :cond_2
    goto :goto_0

    .line 260
    :cond_3
    :goto_2
    return p3
.end method

.method private getInputMethodWindowVisibleRegion(Lcom/android/server/wm/DisplayContent;)Landroid/graphics/Rect;
    .locals 9
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 335
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getInsetsStateController()Lcom/android/server/wm/InsetsStateController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/InsetsStateController;->getRawInsetsState()Landroid/view/InsetsState;

    move-result-object v0

    .line 336
    .local v0, "state":Landroid/view/InsetsState;
    sget v1, Landroid/view/InsetsSource;->ID_IME:I

    invoke-virtual {v0, v1}, Landroid/view/InsetsState;->peekSource(I)Landroid/view/InsetsSource;

    move-result-object v1

    .line 337
    .local v1, "imeSource":Landroid/view/InsetsSource;
    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/view/InsetsSource;->isVisible()Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    .line 340
    :cond_0
    invoke-virtual {v1}, Landroid/view/InsetsSource;->getVisibleFrame()Landroid/graphics/Rect;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 341
    invoke-virtual {v1}, Landroid/view/InsetsSource;->getVisibleFrame()Landroid/graphics/Rect;

    move-result-object v3

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/view/InsetsSource;->getFrame()Landroid/graphics/Rect;

    move-result-object v3

    .line 342
    .local v3, "imeFrame":Landroid/graphics/Rect;
    :goto_0
    const-string v4, "mTmpRect"

    const-class v5, Landroid/graphics/Rect;

    invoke-static {p1, v4, v5}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    .line 343
    .local v4, "dockFrame":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/view/InsetsState;->getDisplayFrame()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 344
    invoke-static {}, Landroid/view/WindowInsets$Type;->systemBars()I

    move-result v5

    invoke-static {}, Landroid/view/WindowInsets$Type;->displayCutout()I

    move-result v6

    or-int/2addr v5, v6

    invoke-virtual {v0, v4, v5, v2}, Landroid/view/InsetsState;->calculateInsets(Landroid/graphics/Rect;IZ)Landroid/graphics/Insets;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/graphics/Rect;->inset(Landroid/graphics/Insets;)V

    .line 346
    new-instance v2, Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->left:I

    iget v6, v3, Landroid/graphics/Rect;->top:I

    iget v7, v4, Landroid/graphics/Rect;->right:I

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v2

    .line 338
    .end local v3    # "imeFrame":Landroid/graphics/Rect;
    .end local v4    # "dockFrame":Landroid/graphics/Rect;
    :cond_2
    :goto_1
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v3
.end method

.method private getSmallScreenLayout(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayPolicy;FZII)I
    .locals 11
    .param p1, "outConfig"    # Landroid/content/res/Configuration;
    .param p2, "mDisplayPolicy"    # Lcom/android/server/wm/DisplayPolicy;
    .param p3, "density"    # F
    .param p4, "rotated"    # Z
    .param p5, "dw"    # I
    .param p6, "dh"    # I

    .line 569
    if-eqz p4, :cond_0

    .line 570
    move/from16 v0, p6

    .line 571
    .local v0, "unrotDw":I
    move/from16 v1, p5

    .local v1, "unrotDh":I
    goto :goto_0

    .line 573
    .end local v0    # "unrotDw":I
    .end local v1    # "unrotDh":I
    :cond_0
    move/from16 v0, p5

    .line 574
    .restart local v0    # "unrotDw":I
    move/from16 v1, p6

    .line 576
    .restart local v1    # "unrotDh":I
    :goto_0
    move-object v9, p1

    iget v2, v9, Landroid/content/res/Configuration;->screenLayout:I

    invoke-static {v2}, Landroid/content/res/Configuration;->resetScreenLayout(I)I

    move-result v10

    .line 577
    .local v10, "sl":I
    const/4 v5, 0x0

    move-object v2, p0

    move-object v3, p2

    move v4, v10

    move v6, p3

    move v7, v0

    move v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/android/server/wm/DisplayContentStubImpl;->reduceConfigLayout(Lcom/android/server/wm/DisplayPolicy;IIFII)I

    move-result v10

    .line 578
    const/4 v5, 0x1

    move v4, v10

    move v7, v1

    move v8, v0

    invoke-direct/range {v2 .. v8}, Lcom/android/server/wm/DisplayContentStubImpl;->reduceConfigLayout(Lcom/android/server/wm/DisplayPolicy;IIFII)I

    move-result v10

    .line 579
    const/4 v5, 0x2

    move v4, v10

    move v7, v0

    move v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/android/server/wm/DisplayContentStubImpl;->reduceConfigLayout(Lcom/android/server/wm/DisplayPolicy;IIFII)I

    move-result v10

    .line 580
    const/4 v5, 0x3

    move v4, v10

    move v7, v1

    move v8, v0

    invoke-direct/range {v2 .. v8}, Lcom/android/server/wm/DisplayContentStubImpl;->reduceConfigLayout(Lcom/android/server/wm/DisplayPolicy;IIFII)I

    move-result v2

    .line 581
    .end local v10    # "sl":I
    .local v2, "sl":I
    return v2
.end method

.method private synthetic lambda$focusChangedLw$0(Lcom/android/server/wm/WindowState;J)V
    .locals 0
    .param p1, "focus"    # Lcom/android/server/wm/WindowState;
    .param p2, "durationMillis"    # J

    .line 379
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/DisplayContentStubImpl;->trackNoFocusWindowTimeout(Lcom/android/server/wm/WindowState;J)V

    return-void
.end method

.method private reduceConfigLayout(Lcom/android/server/wm/DisplayPolicy;IIFII)I
    .locals 6
    .param p1, "mDisplayPolicy"    # Lcom/android/server/wm/DisplayPolicy;
    .param p2, "curLayout"    # I
    .param p3, "rotation"    # I
    .param p4, "density"    # F
    .param p5, "dw"    # I
    .param p6, "dh"    # I

    .line 585
    nop

    .line 586
    invoke-virtual {p1, p3, p5, p6}, Lcom/android/server/wm/DisplayPolicy;->getDecorInsetsInfo(III)Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/DisplayPolicy$DecorInsets$Info;->mNonDecorFrame:Landroid/graphics/Rect;

    .line 587
    .local v0, "nonDecorSize":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 588
    .local v1, "w":I
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 591
    .local v2, "h":I
    move v3, v1

    .line 592
    .local v3, "longSize":I
    move v4, v2

    .line 593
    .local v4, "shortSize":I
    if-ge v3, v4, :cond_0

    .line 594
    move v5, v3

    .line 595
    .local v5, "tmp":I
    move v3, v4

    .line 596
    move v4, v5

    .line 598
    .end local v5    # "tmp":I
    :cond_0
    int-to-float v5, v3

    div-float/2addr v5, p4

    float-to-int v3, v5

    .line 599
    int-to-float v5, v4

    div-float/2addr v5, p4

    float-to-int v4, v5

    .line 600
    invoke-static {p2, v3, v4}, Landroid/content/res/Configuration;->reduceScreenLayout(III)I

    move-result v5

    return v5
.end method

.method private static setCurrentRefreshRate(I)V
    .locals 2
    .param p0, "fps"    # I

    .line 298
    sput p0, Lcom/android/server/wm/DisplayContentStubImpl;->sCurrentRefreshRate:I

    .line 299
    invoke-static {}, Lmiui/hardware/display/DisplayFeatureManager;->getInstance()Lmiui/hardware/display/DisplayFeatureManager;

    move-result-object v0

    const/16 v1, 0x18

    invoke-virtual {v0, v1, p0}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V

    .line 300
    return-void
.end method

.method private trackNoFocusWindowTimeout(Lcom/android/server/wm/WindowState;J)V
    .locals 6
    .param p1, "focus"    # Lcom/android/server/wm/WindowState;
    .param p2, "durationMillis"    # J

    .line 386
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    .line 387
    .local v0, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowTag()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 388
    .local v1, "window":Ljava/lang/String;
    invoke-static {v1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 389
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 392
    :cond_0
    new-instance v2, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;

    invoke-direct {v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V

    .line 393
    .local v2, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    const/16 v3, 0x1b2

    invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V

    .line 394
    invoke-virtual {v2, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V

    .line 395
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V

    .line 396
    const-string v3, "no focus window timeout"

    invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V

    .line 397
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "window="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " duration="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V

    .line 398
    long-to-int v3, p2

    invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V

    .line 399
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v3

    invoke-virtual {v3, v2}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z

    .line 401
    new-instance v3, Lcom/miui/misight/MiEvent;

    const v4, 0x35b4375d

    invoke-direct {v3, v4}, Lcom/miui/misight/MiEvent;-><init>(I)V

    .line 402
    .local v3, "miEvent":Lcom/miui/misight/MiEvent;
    const-string v4, "PackageName"

    invoke-virtual {v3, v4, v0}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    .line 403
    const-string v4, "WindowName"

    invoke-virtual {v3, v4, v1}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    .line 404
    const-string v4, "Duration"

    long-to-int v5, p2

    invoke-virtual {v3, v4, v5}, Lcom/miui/misight/MiEvent;->addInt(Ljava/lang/String;I)Lcom/miui/misight/MiEvent;

    .line 405
    invoke-static {v3}, Lcom/miui/misight/MiSight;->sendEvent(Lcom/miui/misight/MiEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "window":Ljava/lang/String;
    .end local v2    # "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    .end local v3    # "miEvent":Lcom/miui/misight/MiEvent;
    goto :goto_0

    .line 406
    :catch_0
    move-exception v0

    .line 409
    :goto_0
    return-void
.end method

.method static updateRefreshRateIfNeed(Z)V
    .locals 3
    .param p0, "isInMultiWindow"    # Z

    .line 264
    invoke-static {}, Lcom/android/server/wm/DisplayContentStubImpl;->getCurrentRefreshRate()I

    move-result v0

    .line 266
    .local v0, "currentFps":I
    sget v1, Lcom/android/server/wm/DisplayContentStubImpl;->sCurrentRefreshRate:I

    if-eq v1, v0, :cond_0

    .line 267
    sput v0, Lcom/android/server/wm/DisplayContentStubImpl;->sCurrentRefreshRate:I

    .line 270
    :cond_0
    const/16 v1, 0x3c

    if-eqz p0, :cond_1

    .line 271
    sget v2, Lcom/android/server/wm/DisplayContentStubImpl;->sCurrentRefreshRate:I

    if-le v2, v1, :cond_2

    .line 272
    sput v2, Lcom/android/server/wm/DisplayContentStubImpl;->sLastUserRefreshRate:I

    .line 273
    invoke-static {v1}, Lcom/android/server/wm/DisplayContentStubImpl;->setCurrentRefreshRate(I)V

    goto :goto_0

    .line 276
    :cond_1
    sget v2, Lcom/android/server/wm/DisplayContentStubImpl;->sLastUserRefreshRate:I

    if-lt v2, v1, :cond_2

    .line 277
    invoke-static {v2}, Lcom/android/server/wm/DisplayContentStubImpl;->setCurrentRefreshRate(I)V

    .line 278
    const/4 v1, -0x1

    sput v1, Lcom/android/server/wm/DisplayContentStubImpl;->sLastUserRefreshRate:I

    .line 281
    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public attachToDisplayCompatMode(Lcom/android/server/wm/WindowState;)Z
    .locals 3
    .param p1, "mImeLayeringTarget"    # Lcom/android/server/wm/WindowState;

    .line 421
    const/4 v0, 0x0

    .line 422
    .local v0, "result":Z
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v1, :cond_0

    .line 423
    iget-object v1, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v1

    .line 424
    .local v1, "task":Lcom/android/server/wm/Task;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/android/server/wm/Task;->mDisplayCompatAvailable:Z

    if-eqz v2, :cond_0

    .line 425
    const/4 v0, 0x1

    .line 428
    .end local v1    # "task":Lcom/android/server/wm/Task;
    :cond_0
    return v0
.end method

.method public checkWindowHiddenWhenFixedRotation(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/DisplayContent;Lcom/android/server/wm/WindowState;)V
    .locals 5
    .param p1, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p3, "windowState"    # Lcom/android/server/wm/WindowState;

    .line 671
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->hasTopFixedRotationLaunchingApp()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 672
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getLastOrientationSource()Lcom/android/server/wm/WindowContainer;

    move-result-object v0

    .line 674
    .local v0, "orientationSource":Lcom/android/server/wm/WindowContainer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 675
    .local v1, "r":Lcom/android/server/wm/ActivityRecord;
    :goto_0
    if-eqz v1, :cond_3

    .line 676
    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->isVisibleRequested()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->topRunningActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 677
    .local v2, "topCandidate":Lcom/android/server/wm/ActivityRecord;
    :goto_1
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->hasFixedRotationTransform()Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz p3, :cond_3

    iget-object v3, p3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    if-eqz v3, :cond_3

    iget-object v3, p3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    const/high16 v4, 0x20000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_3

    .line 680
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getPendingTransaction()Landroid/view/SurfaceControl$Transaction;

    move-result-object v3

    iget-object v4, p3, Lcom/android/server/wm/WindowState;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 681
    iget-object v3, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mHiddenWindowList:Ljava/util/HashSet;

    invoke-virtual {v3, p3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 684
    .end local v2    # "topCandidate":Lcom/android/server/wm/ActivityRecord;
    :cond_3
    return-void
.end method

.method public compare(Lcom/android/server/wm/WindowToken;Lcom/android/server/wm/WindowToken;)I
    .locals 2
    .param p1, "token1"    # Lcom/android/server/wm/WindowToken;
    .param p2, "token2"    # Lcom/android/server/wm/WindowToken;

    .line 307
    :try_start_0
    iget v0, p1, Lcom/android/server/wm/WindowToken;->windowType:I

    iget v1, p2, Lcom/android/server/wm/WindowToken;->windowType:I

    if-ne v0, v1, :cond_1

    iget v0, p1, Lcom/android/server/wm/WindowToken;->windowType:I

    const/16 v1, 0x7dd

    if-ne v0, v1, :cond_1

    .line 309
    iget-object v0, p1, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "miui.systemui.keyguard.Wallpaper"

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p1, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    invoke-interface {v0}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    const/4 v0, 0x1

    return v0

    .line 311
    :cond_0
    iget-object v0, p2, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    invoke-interface {v0}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_1

    .line 312
    const/4 v0, -0x1

    return v0

    .line 316
    :cond_1
    goto :goto_0

    .line 314
    :catch_0
    move-exception v0

    .line 315
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 317
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    new-instance v0, Lcom/android/server/wm/DisplayContentStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v0}, Lcom/android/server/wm/DisplayContentStubImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-static {v0}, Ljava/util/Comparator;->comparingInt(Ljava/util/function/ToIntFunction;)Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public displayReady(Landroid/content/Context;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;

    .line 198
    iput-object p1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mContext:Landroid/content/Context;

    .line 199
    invoke-direct {p0, p1}, Lcom/android/server/wm/DisplayContentStubImpl;->computeBlurConfiguration(Landroid/content/Context;)V

    .line 200
    return-void
.end method

.method public finishLayoutLw(Lcom/android/server/policy/WindowManagerPolicy;Lcom/android/server/wm/DisplayContent;Lcom/android/server/wm/DisplayFrames;)V
    .locals 4
    .param p1, "policy"    # Lcom/android/server/policy/WindowManagerPolicy;
    .param p2, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p3, "displayFrames"    # Lcom/android/server/wm/DisplayFrames;

    .line 322
    instance-of v0, p1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-eqz v0, :cond_0

    .line 323
    invoke-direct {p0, p2}, Lcom/android/server/wm/DisplayContentStubImpl;->getInputMethodWindowVisibleRegion(Lcom/android/server/wm/DisplayContent;)Landroid/graphics/Rect;

    move-result-object v0

    .line 324
    .local v0, "inputMethodWindowRegion":Landroid/graphics/Rect;
    move-object v1, p1

    check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    iget v2, p2, Lcom/android/server/wm/DisplayContent;->mDisplayId:I

    invoke-virtual {v1, p3, v0, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->finishLayoutLw(Lcom/android/server/wm/DisplayFrames;Landroid/graphics/Rect;I)V

    .line 325
    invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/DisplayPolicy;->getStatusBar()Lcom/android/server/wm/WindowState;

    move-result-object v1

    .line 326
    .local v1, "statusBar":Lcom/android/server/wm/WindowState;
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mStatusBarVisible:Z

    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isVisible()Z

    move-result v3

    if-eq v2, v3, :cond_0

    .line 327
    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isVisible()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mStatusBarVisible:Z

    .line 328
    invoke-static {v2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->onStatusBarVisibilityChangeStatic(Z)V

    .line 329
    iget-boolean v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mStatusBarVisible:Z

    invoke-static {v2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->finishPostLayoutPolicyLw(Z)V

    .line 332
    .end local v0    # "inputMethodWindowRegion":Landroid/graphics/Rect;
    .end local v1    # "statusBar":Lcom/android/server/wm/WindowState;
    :cond_0
    return-void
.end method

.method public focusChangedLw(ILcom/android/server/wm/WindowState;)V
    .locals 6
    .param p1, "displayId"    # I
    .param p2, "focus"    # Lcom/android/server/wm/WindowState;

    .line 366
    if-eqz p2, :cond_0

    .line 367
    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getPid()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getUid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 368
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v3

    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/Object;

    move-result-object v0

    .line 367
    const-string v1, "notifyFocusWindowChanged"

    invoke-static {v1, v0}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    :cond_0
    if-eqz p1, :cond_1

    return-void

    .line 372
    :cond_1
    if-nez p2, :cond_2

    .line 373
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J

    goto :goto_0

    .line 374
    :cond_2
    iget-wide v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 375
    iget-wide v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J

    .line 376
    .local v0, "timestamp":J
    iput-wide v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mTimestamp:J

    .line 377
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    .line 378
    .local v2, "durationMillis":J
    const-wide/16 v4, 0x1388

    cmp-long v4, v2, v4

    if-lez v4, :cond_3

    .line 379
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/android/server/wm/DisplayContentStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v5, p0, p2, v2, v3}, Lcom/android/server/wm/DisplayContentStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/DisplayContentStubImpl;Lcom/android/server/wm/WindowState;J)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 382
    .end local v0    # "timestamp":J
    .end local v2    # "durationMillis":J
    :cond_3
    :goto_0
    return-void
.end method

.method public forceEnableSeamless(Lcom/android/server/wm/WindowState;)Z
    .locals 2
    .param p1, "w"    # Lcom/android/server/wm/WindowState;

    .line 492
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->extraFlags:I

    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 494
    const/4 v0, 0x1

    return v0

    .line 496
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getSmallScreenLayout(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayPolicy;FII)I
    .locals 8
    .param p1, "outConfig"    # Landroid/content/res/Configuration;
    .param p2, "mDisplayPolicy"    # Lcom/android/server/wm/DisplayPolicy;
    .param p3, "density"    # F
    .param p4, "dw"    # I
    .param p5, "dh"    # I

    .line 561
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/server/wm/ActivityRecordStub;->get()Lcom/android/server/wm/ActivityRecordStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub;->isCompatibilityMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562
    if-gt p4, p5, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move v5, v0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/android/server/wm/DisplayContentStubImpl;->getSmallScreenLayout(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayPolicy;FZII)I

    move-result v0

    return v0

    .line 564
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->screenLayout:I

    return v0
.end method

.method public initOneTrackRotationHelper()V
    .locals 1

    .line 412
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->getInstance()Lcom/android/server/wm/OneTrackRotationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/OneTrackRotationHelper;->init()V

    .line 413
    return-void
.end method

.method public isCameraRotating()Z
    .locals 1

    .line 624
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_FOLD:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_TABLET:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 625
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mIsCameraRotating:Z

    return v0
.end method

.method public isSubDisplay(I)Z
    .locals 1
    .param p1, "displayId"    # I

    .line 359
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSubDisplayOff(Lcom/android/server/wm/DisplayContent;)Z
    .locals 2
    .param p1, "display"    # Lcom/android/server/wm/DisplayContent;

    .line 351
    if-eqz p1, :cond_0

    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 354
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 351
    :goto_0
    return v1
.end method

.method public isSupportedImeSnapShot()Z
    .locals 1

    .line 443
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_IME_SNAPSHOT:Z

    return v0
.end method

.method public needEnsureVisible(Lcom/android/server/wm/DisplayContent;Lcom/android/server/wm/Task;)Z
    .locals 3
    .param p1, "dc"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "task"    # Lcom/android/server/wm/Task;

    .line 432
    const/4 v0, 0x1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    iget-object v1, p2, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_1

    .line 435
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/wm/DisplayContentStubImpl;->isSubDisplayOff(Lcom/android/server/wm/DisplayContent;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p2, Lcom/android/server/wm/Task;->affinity:Ljava/lang/String;

    const-string v2, "com.xiaomi.misubscreenui"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return v0

    .line 433
    :cond_3
    :goto_1
    return v0
.end method

.method public needSkipUpdateSurfaceRotation()Z
    .locals 1

    .line 613
    iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipUpdateSurfaceRotation:Z

    return v0
.end method

.method public needSkipVisualDisplayTransition(I)Z
    .locals 1
    .param p1, "displayId"    # I

    .line 475
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipVisualDisplayTransition:Z

    return v0

    .line 476
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public noteFoucsChangeForPowerConsumptionIfNeeded(Lcom/android/server/wm/WindowState;)V
    .locals 5
    .param p1, "w"    # Lcom/android/server/wm/WindowState;

    .line 453
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    .line 454
    .local v0, "mAttrs":Landroid/view/WindowManager$LayoutParams;
    if-eqz v0, :cond_0

    .line 455
    invoke-virtual {v0}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    .line 456
    .local v1, "tag":Ljava/lang/CharSequence;
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mMiuiWindowStateEx:Lcom/android/server/wm/IMiuiWindowStateEx;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mPowerConsumptionServiceInternal:Lcom/android/server/PowerConsumptionServiceInternal;

    if-eqz v2, :cond_0

    .line 459
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/android/server/wm/WindowState;->mMiuiWindowStateEx:Lcom/android/server/wm/IMiuiWindowStateEx;

    invoke-virtual {v2, v3, v4}, Lcom/android/server/PowerConsumptionServiceInternal;->noteFoucsChangeForPowerConsumption(Ljava/lang/String;Lcom/android/server/wm/IMiuiWindowStateEx;)V

    .line 462
    .end local v1    # "tag":Ljava/lang/CharSequence;
    :cond_0
    return-void
.end method

.method public onDisplayOverrideConfigUpdate(Lcom/android/server/wm/DisplayContent;)V
    .locals 1
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;

    .line 466
    const-class v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 467
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiHoverModeInternal;

    .line 468
    .local v0, "miuiHoverIn":Lcom/android/server/wm/MiuiHoverModeInternal;
    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiHoverModeInternal;->onDisplayOverrideConfigUpdate(Lcom/android/server/wm/DisplayContent;)V

    .line 471
    :cond_0
    return-void
.end method

.method public onFocusedWindowChanged(Lcom/android/server/wm/DisplayContent;)V
    .locals 3
    .param p1, "display"    # Lcom/android/server/wm/DisplayContent;

    .line 501
    iget-boolean v0, p1, Lcom/android/server/wm/DisplayContent;->isDefaultDisplay:Z

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isControllerAMonkey()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 504
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mWmService:Lcom/android/server/wm/WindowManagerService;

    .line 505
    .local v0, "wmService":Lcom/android/server/wm/WindowManagerService;
    iget-object v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mFocusRunnable:Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;

    if-nez v1, :cond_1

    .line 506
    new-instance v1, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;

    invoke-direct {v1, v0}, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;-><init>(Lcom/android/server/wm/WindowManagerService;)V

    iput-object v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mFocusRunnable:Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;

    .line 508
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mFocusRunnable:Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;

    iget-object v2, p1, Lcom/android/server/wm/DisplayContent;->mCurrentFocus:Lcom/android/server/wm/WindowState;

    if-nez v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    invoke-static {v1, v2}, Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;->-$$Nest$mpost(Lcom/android/server/wm/DisplayContentStubImpl$UpdateFocusRunnable;Z)V

    .line 509
    return-void

    .line 502
    .end local v0    # "wmService":Lcom/android/server/wm/WindowManagerService;
    :cond_3
    :goto_1
    return-void
.end method

.method public onSettingsObserverChange(ZLandroid/net/Uri;)Z
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 208
    if-eqz p2, :cond_0

    const-string v0, "background_blur_enable"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/wm/DisplayContentStubImpl;->computeBlurConfiguration(Landroid/content/Context;)V

    .line 210
    const/4 v0, 0x1

    return v0

    .line 212
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onSystemReady()V
    .locals 1

    .line 194
    const-class v0, Lcom/android/server/PowerConsumptionServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/PowerConsumptionServiceInternal;

    iput-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mPowerConsumptionServiceInternal:Lcom/android/server/PowerConsumptionServiceInternal;

    .line 195
    return-void
.end method

.method public registerSettingsObserver(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService$SettingsObserver;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "settingsObserver"    # Lcom/android/server/wm/WindowManagerService$SettingsObserver;

    .line 203
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 204
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "background_blur_enable"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 205
    return-void
.end method

.method public reportDisplayRotationChanged(II)V
    .locals 1
    .param p1, "displayId"    # I
    .param p2, "rotation"    # I

    .line 416
    invoke-static {}, Lcom/android/server/wm/OneTrackRotationHelper;->getInstance()Lcom/android/server/wm/OneTrackRotationHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/OneTrackRotationHelper;->reportRotationChanged(II)V

    .line 417
    return-void
.end method

.method public restoreHiddenWindow(Lcom/android/server/wm/DisplayContent;Landroid/view/SurfaceControl$Transaction;)V
    .locals 3
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "transaction"    # Landroid/view/SurfaceControl$Transaction;

    .line 687
    iget-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mHiddenWindowList:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 688
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mHiddenWindowList:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/WindowState;

    .line 689
    .local v1, "w":Lcom/android/server/wm/WindowState;
    iget-object v2, v1, Lcom/android/server/wm/WindowState;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {p2, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 690
    .end local v1    # "w":Lcom/android/server/wm/WindowState;
    goto :goto_0

    .line 691
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mHiddenWindowList:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 692
    return-void
.end method

.method public setBlur(Landroid/content/res/Configuration;Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "current"    # Landroid/content/res/Configuration;
    .param p2, "tmp"    # Landroid/content/res/Configuration;

    .line 231
    iget v0, p1, Landroid/content/res/Configuration;->blur:I

    if-nez v0, :cond_0

    .line 232
    iget v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mBlur:I

    iput v0, p2, Landroid/content/res/Configuration;->blur:I

    .line 234
    :cond_0
    return-void
.end method

.method public setCameraRotationState(Z)V
    .locals 1
    .param p1, "isCameraRotating"    # Z

    .line 617
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_FOLD:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationStub;->IS_TABLET:Z

    if-nez v0, :cond_0

    return-void

    .line 618
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mIsCameraRotating:Z

    if-eq v0, p1, :cond_1

    .line 619
    iput-boolean p1, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mIsCameraRotating:Z

    .line 621
    :cond_1
    return-void
.end method

.method public setFixedRotationState(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)V
    .locals 2
    .param p1, "prevActivity"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "nowActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 604
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 605
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipUpdateSurfaceRotation:Z

    .line 607
    return-void

    .line 609
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipUpdateSurfaceRotation:Z

    .line 610
    return-void
.end method

.method public shouldInterceptRelaunch(II)Z
    .locals 1
    .param p1, "vendorId"    # I
    .param p2, "productId"    # I

    .line 439
    invoke-static {p1, p2}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->isXiaomiKeyboard(II)Z

    move-result v0

    return v0
.end method

.method public skipChangeTransition(ILandroid/window/TransitionRequestInfo$DisplayChange;Ljava/lang/String;)Z
    .locals 1
    .param p1, "changes"    # I
    .param p2, "displayChange"    # Landroid/window/TransitionRequestInfo$DisplayChange;
    .param p3, "resumeActivity"    # Ljava/lang/String;

    .line 514
    if-nez p2, :cond_1

    if-eqz p3, :cond_1

    .line 515
    const-string v0, "com.android.provision/.activities.DefaultActivity"

    invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    const-string v0, "com.android.provision/.activities.CongratulationActivity"

    invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 514
    :goto_0
    return v0
.end method

.method public skipImeWindowsForMiui(Lcom/android/server/wm/DisplayContent;)Z
    .locals 2
    .param p1, "display"    # Lcom/android/server/wm/DisplayContent;

    .line 447
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isVerticalSplit()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 448
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z

    move-result v0

    xor-int/2addr v0, v1

    return v0
.end method

.method public updateCurrentDisplayRotation(Lcom/android/server/wm/DisplayContent;ZZ)V
    .locals 7
    .param p1, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "alwaysSendConfiguration"    # Z
    .param p3, "forceRelayout"    # Z

    .line 630
    const-string/jumbo v0, "updateRotation"

    const-wide/16 v1, 0x20

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 632
    :try_start_0
    iget-object v0, p1, Lcom/android/server/wm/DisplayContent;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mGlobalLock:Lcom/android/server/wm/WindowManagerGlobalLock;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 633
    const/4 v3, 0x0

    .line 635
    .local v3, "layoutNeeded":Z
    :try_start_1
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->updateRotationUnchecked()Z

    move-result v4

    .line 637
    .local v4, "rotationChanged":Z
    if-eqz v4, :cond_0

    .line 638
    iget-object v5, p1, Lcom/android/server/wm/DisplayContent;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getTaskChangeNotificationController()Lcom/android/server/wm/TaskChangeNotificationController;

    move-result-object v5

    iget v6, p1, Lcom/android/server/wm/DisplayContent;->mDisplayId:I

    .line 639
    invoke-virtual {v5, v6}, Lcom/android/server/wm/TaskChangeNotificationController;->notifyOnActivityRotation(I)V

    .line 642
    :cond_0
    if-eqz v4, :cond_2

    iget-object v5, p1, Lcom/android/server/wm/DisplayContent;->mRemoteDisplayChangeController:Lcom/android/server/wm/RemoteDisplayChangeController;

    .line 643
    invoke-virtual {v5}, Lcom/android/server/wm/RemoteDisplayChangeController;->isWaitingForRemoteDisplayChange()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p1, Lcom/android/server/wm/DisplayContent;->mTransitionController:Lcom/android/server/wm/TransitionController;

    .line 644
    invoke-virtual {v5}, Lcom/android/server/wm/TransitionController;->isCollecting()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    .line 646
    .local v5, "pendingRemoteDisplayChange":Z
    :goto_0
    if-nez v5, :cond_5

    .line 648
    if-eqz p3, :cond_3

    .line 649
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->setLayoutNeeded()V

    .line 650
    const/4 v3, 0x1

    .line 652
    :cond_3
    if-nez v4, :cond_4

    if-eqz p2, :cond_5

    .line 653
    :cond_4
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->sendNewConfiguration()V

    .line 657
    :cond_5
    if-eqz v3, :cond_6

    .line 658
    const-string/jumbo v6, "updateRotation: performSurfacePlacement"

    invoke-static {v1, v2, v6}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 660
    iget-object v6, p1, Lcom/android/server/wm/DisplayContent;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v6, v6, Lcom/android/server/wm/WindowManagerService;->mWindowPlacerLocked:Lcom/android/server/wm/WindowSurfacePlacer;

    invoke-virtual {v6}, Lcom/android/server/wm/WindowSurfacePlacer;->performSurfacePlacement()V

    .line 661
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 663
    .end local v3    # "layoutNeeded":Z
    .end local v4    # "rotationChanged":Z
    .end local v5    # "pendingRemoteDisplayChange":Z
    :cond_6
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 665
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 666
    nop

    .line 667
    return-void

    .line 663
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local p0    # "this":Lcom/android/server/wm/DisplayContentStubImpl;
    .end local p1    # "displayContent":Lcom/android/server/wm/DisplayContent;
    .end local p2    # "alwaysSendConfiguration":Z
    .end local p3    # "forceRelayout":Z
    :try_start_3
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 665
    .restart local p0    # "this":Lcom/android/server/wm/DisplayContentStubImpl;
    .restart local p1    # "displayContent":Lcom/android/server/wm/DisplayContent;
    .restart local p2    # "alwaysSendConfiguration":Z
    .restart local p3    # "forceRelayout":Z
    :catchall_1
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 666
    throw v0
.end method

.method public updateSkipVisualDisplayTransition(ILjava/lang/String;Z)V
    .locals 1
    .param p1, "displayId"    # I
    .param p2, "tokenTag"    # Ljava/lang/String;
    .param p3, "shouldSkip"    # Z

    .line 480
    if-eqz p1, :cond_1

    const-string v0, "Display-off"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 481
    if-eqz p3, :cond_0

    .line 482
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipVisualDisplayTransition:Z

    goto :goto_0

    .line 484
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/DisplayContentStubImpl;->mSkipVisualDisplayTransition:Z

    .line 487
    :cond_1
    :goto_0
    return-void
.end method
