class com.android.server.wm.MiuiMultiWindowRecommendController$6 implements java.lang.Runnable {
	 /* .source "MiuiMultiWindowRecommendController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->addSplitScreenRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendController this$0; //synthetic
final com.android.server.wm.RecommendDataEntry val$recommendDataEntry; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendController$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendController; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 191 */
this.this$0 = p1;
this.val$recommendDataEntry = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 9 */
/* .line 195 */
final String v0 = "_"; // const-string v0, "_"
final String v1 = "MiuiMultiWindowRecommendController"; // const-string v1, "MiuiMultiWindowRecommendController"
try { // :try_start_0
v2 = this.this$0;
v2 = this.mRecommendHelper;
v2 = (( com.android.server.wm.MiuiMultiWindowRecommendHelper ) v2 ).hasNotification ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->hasNotification()Z
/* if-nez v2, :cond_4 */
v2 = this.this$0;
v2 = this.mRecommendHelper;
v3 = this.val$recommendDataEntry;
v2 = (( com.android.server.wm.MiuiMultiWindowRecommendHelper ) v2 ).isSplitScreenRecommendValid ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->isSplitScreenRecommendValid(Lcom/android/server/wm/RecommendDataEntry;)Z
/* if-nez v2, :cond_0 */
/* goto/16 :goto_1 */
/* .line 201 */
} // :cond_0
v2 = this.this$0;
v3 = this.val$recommendDataEntry;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmSpiltScreenRecommendDataEntry ( v2,v3 );
/* .line 202 */
v2 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmContext ( v2 );
android.view.LayoutInflater .from ( v3 );
/* const v4, 0x110c003b */
int v5 = 0; // const/4 v5, 0x0
(( android.view.LayoutInflater ) v3 ).inflate ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
/* check-cast v3, Lcom/android/server/wm/SplitScreenRecommendLayout; */
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmSplitScreenRecommendLayout ( v2,v3 );
/* .line 204 */
v2 = this.this$0;
v2 = this.mService;
/* .line 205 */
(( com.android.server.wm.WindowManagerService ) v2 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v2 ).getInsetsStateController ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getInsetsStateController()Lcom/android/server/wm/InsetsStateController;
/* .line 204 */
int v3 = 0; // const/4 v3, 0x0
v2 = com.android.server.wm.MiuiMultiWindowRecommendController .getStatusBarHeight ( v2,v3 );
v4 = this.this$0;
v4 = this.mService;
/* .line 206 */
(( com.android.server.wm.WindowManagerService ) v4 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
v4 = this.mDisplayFrames;
v4 = com.android.server.wm.MiuiMultiWindowRecommendController .getDisplayCutoutHeight ( v4 );
/* .line 204 */
v2 = java.lang.Math .max ( v2,v4 );
/* .line 207 */
/* .local v2, "statusBarHeight":I */
v4 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v4 );
(( com.android.server.wm.SplitScreenRecommendLayout ) v5 ).createLayoutParams ( v2 ); // invoke-virtual {v5, v2}, Lcom/android/server/wm/SplitScreenRecommendLayout;->createLayoutParams(I)Landroid/view/WindowManager$LayoutParams;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmLayoutParams ( v4,v5 );
/* .line 208 */
v4 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v4 );
(( com.android.server.wm.SplitScreenRecommendLayout ) v5 ).getSplitScreenIconContainer ( ); // invoke-virtual {v5}, Lcom/android/server/wm/SplitScreenRecommendLayout;->getSplitScreenIconContainer()Landroid/widget/RelativeLayout;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fputmSplitScreenRecommendIconContainer ( v4,v5 );
/* .line 209 */
v4 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v4 );
v5 = this.val$recommendDataEntry;
/* .line 210 */
(( com.android.server.wm.RecommendDataEntry ) v5 ).getPrimaryPackageName ( ); // invoke-virtual {v5}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;
v6 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmContext ( v6 );
v7 = this.val$recommendDataEntry;
v7 = (( com.android.server.wm.RecommendDataEntry ) v7 ).getPrimaryUserId ( ); // invoke-virtual {v7}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryUserId()I
/* .line 209 */
com.android.server.wm.MiuiMultiWindowRecommendController .loadDrawableByPackageName ( v5,v6,v7 );
v6 = this.val$recommendDataEntry;
/* .line 211 */
(( com.android.server.wm.RecommendDataEntry ) v6 ).getSecondaryPackageName ( ); // invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;
v7 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmContext ( v7 );
v8 = this.val$recommendDataEntry;
/* .line 212 */
v8 = (( com.android.server.wm.RecommendDataEntry ) v8 ).getSecondaryUserId ( ); // invoke-virtual {v8}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryUserId()I
/* .line 211 */
com.android.server.wm.MiuiMultiWindowRecommendController .loadDrawableByPackageName ( v6,v7,v8 );
/* .line 209 */
(( com.android.server.wm.SplitScreenRecommendLayout ) v4 ).setSplitScreenIcon ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Lcom/android/server/wm/SplitScreenRecommendLayout;->setSplitScreenIcon(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
/* .line 213 */
v4 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v4 );
v5 = this.this$0;
v5 = this.splitScreenClickListener;
(( com.android.server.wm.SplitScreenRecommendLayout ) v4 ).setOnClickListener ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/SplitScreenRecommendLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V
/* .line 214 */
int v4 = 1; // const/4 v4, 0x1
/* new-array v5, v4, [Landroid/view/View; */
v6 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v6 );
/* aput-object v6, v5, v3 */
miuix.animation.Folme .useAt ( v5 );
v6 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v6 );
/* new-array v3, v3, [Lmiuix/animation/base/AnimConfig; */
/* .line 215 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
int v5 = 2; // const/4 v5, 0x2
/* if-ge v3, v5, :cond_3 */
/* .line 216 */
v5 = this.this$0;
v5 = this.mWindowManager;
v6 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v6 );
v7 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmLayoutParams ( v7 );
/* .line 217 */
android.view.inspector.WindowInspector .getGlobalWindowViews ( );
v6 = this.this$0;
v5 = com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v6 );
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 218 */
v5 = this.this$0;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v5 ).setSplitScreenRecommendState ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setSplitScreenRecommendState(Z)V
/* .line 219 */
v5 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSplitScreenRecommendLayout ( v5 );
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v5 ).startMultiWindowRecommendAnimation ( v6, v4 ); // invoke-virtual {v5, v6, v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->startMultiWindowRecommendAnimation(Landroid/view/View;Z)V
/* .line 220 */
v4 = this.this$0;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v4 ).removeSplitScreenRecommendViewForTimer ( ); // invoke-virtual {v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendViewForTimer()V
/* .line 221 */
v4 = this.this$0;
v4 = this.mRecommendHelper;
v4 = this.mFreeFormManagerService;
v4 = this.mFreeFormGestureController;
v4 = this.mTrackManager;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 222 */
v4 = this.this$0;
v4 = this.mRecommendHelper;
v4 = this.mFreeFormManagerService;
v4 = this.mFreeFormGestureController;
v4 = this.mTrackManager;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSpiltScreenRecommendDataEntry ( v6 );
/* .line 223 */
(( com.android.server.wm.RecommendDataEntry ) v6 ).getPrimaryPackageName ( ); // invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSpiltScreenRecommendDataEntry ( v6 );
/* .line 224 */
(( com.android.server.wm.RecommendDataEntry ) v6 ).getSecondaryPackageName ( ); // invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
v7 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSpiltScreenRecommendDataEntry ( v7 );
/* .line 225 */
(( com.android.server.wm.RecommendDataEntry ) v8 ).getPrimaryPackageName ( ); // invoke-virtual {v8}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v7 ).getApplicationName ( v8 ); // invoke-virtual {v7, v8}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.this$0;
com.android.server.wm.MiuiMultiWindowRecommendController .-$$Nest$fgetmSpiltScreenRecommendDataEntry ( v6 );
/* .line 226 */
(( com.android.server.wm.RecommendDataEntry ) v7 ).getSecondaryPackageName ( ); // invoke-virtual {v7}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v6 ).getApplicationName ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 223 */
(( com.android.server.wm.MiuiFreeformTrackManager ) v4 ).trackSplitScreenRecommendExposeEvent ( v5, v0 ); // invoke-virtual {v4, v5, v0}, Lcom/android/server/wm/MiuiFreeformTrackManager;->trackSplitScreenRecommendExposeEvent(Ljava/lang/String;Ljava/lang/String;)V
/* .line 228 */
} // :cond_1
return;
/* .line 215 */
} // :cond_2
/* add-int/lit8 v3, v3, 0x1 */
/* goto/16 :goto_0 */
/* .line 231 */
} // .end local v3 # "i":I
} // :cond_3
final String v0 = " addSplitScreenRecommendView twice fail "; // const-string v0, " addSplitScreenRecommendView twice fail "
android.util.Slog .d ( v1,v0 );
/* .line 235 */
/* nop */
} // .end local v2 # "statusBarHeight":I
/* .line 196 */
} // :cond_4
} // :goto_1
final String v0 = "addSplitScreenRecommendView: hasNotification or invalid, return"; // const-string v0, "addSplitScreenRecommendView: hasNotification or invalid, return"
android.util.Slog .d ( v1,v0 );
/* .line 197 */
v0 = this.this$0;
v0 = this.mRecommendHelper;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) v0 ).setLastSplitScreenRecommendTime ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->setLastSplitScreenRecommendTime(J)V
/* .line 198 */
v0 = this.this$0;
v0 = this.mRecommendHelper;
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) v0 ).clearRecentAppList ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 199 */
return;
/* .line 232 */
/* :catch_0 */
/* move-exception v0 */
/* .line 233 */
/* .local v0, "e":Ljava/lang/Exception; */
v2 = this.this$0;
(( com.android.server.wm.MiuiMultiWindowRecommendController ) v2 ).removeSplitScreenRecommendViewForTimer ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendViewForTimer()V
/* .line 234 */
final String v2 = " addSplitScreenRecommendView fail"; // const-string v2, " addSplitScreenRecommendView fail"
android.util.Slog .d ( v1,v2,v0 );
/* .line 236 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
