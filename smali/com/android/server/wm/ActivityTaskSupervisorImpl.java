public class com.android.server.wm.ActivityTaskSupervisorImpl extends com.android.server.wm.ActivityTaskSupervisorStub {
	 /* .source "ActivityTaskSupervisorImpl.java" */
	 /* # static fields */
	 private static final Integer ACTIVITY_RESUME_TIMEOUT;
	 public static final java.lang.String EXTRA_PACKAGE_NAME;
	 private static final java.lang.String INCALL_PACKAGE_NAME;
	 private static final java.lang.String INCALL_UI_NAME;
	 private static final Integer MAX_SWITCH_INTERVAL;
	 public static final java.lang.String MIUI_APP_LOCK_ACTION;
	 public static final java.lang.String MIUI_APP_LOCK_ACTIVITY_NAME;
	 public static final java.lang.String MIUI_APP_LOCK_PACKAGE_NAME;
	 public static final Integer MIUI_APP_LOCK_REQUEST_CODE;
	 private static final java.lang.String TAG;
	 private static Long mLastIncallUiLaunchTime;
	 private static Integer sActivityRequestId;
	 static final java.util.ArrayList sSupportsMultiTaskInDockList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private com.android.server.wm.ActivityTaskManagerService mAtmService;
private android.content.Context mContext;
private volatile Boolean mFromKeyguard;
private volatile Long mTimestamp;
/* # direct methods */
public static void $r8$lambda$zZ0PRlQIOg8CB3-j4YEjE0qsDWw ( com.android.server.wm.ActivityTaskSupervisorImpl p0, android.content.pm.ActivityInfo p1, Long p2, Boolean p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->lambda$activityIdle$0(Landroid/content/pm/ActivityInfo;JZ)V */
return;
} // .end method
static com.android.server.wm.ActivityTaskSupervisorImpl ( ) {
/* .locals 2 */
/* .line 166 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 168 */
final String v1 = "com.miui.hybrid"; // const-string v1, "com.miui.hybrid"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 181 */
/* const-wide/16 v0, -0x1 */
/* sput-wide v0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mLastIncallUiLaunchTime:J */
return;
} // .end method
public com.android.server.wm.ActivityTaskSupervisorImpl ( ) {
/* .locals 2 */
/* .line 52 */
/* invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskSupervisorStub;-><init>()V */
/* .line 68 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J */
/* .line 69 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mFromKeyguard:Z */
return;
} // .end method
public static com.android.server.wm.Task exitfreeformIfNeeded ( com.android.server.wm.Task p0, Integer p1, Integer p2, com.android.server.wm.ActivityTaskSupervisor p3 ) {
/* .locals 5 */
/* .param p0, "task" # Lcom/android/server/wm/Task; */
/* .param p1, "taskId" # I */
/* .param p2, "windowMode" # I */
/* .param p3, "supervisor" # Lcom/android/server/wm/ActivityTaskSupervisor; */
/* .line 243 */
/* move-object v0, p0 */
/* .line 244 */
/* .local v0, "tTask":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 245 */
	 v1 = 	 (( com.android.server.wm.Task ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getWindowingMode()I
	 int v2 = 5; // const/4 v2, 0x5
	 /* if-ne v1, v2, :cond_0 */
	 /* if-eq p2, v2, :cond_0 */
	 /* .line 246 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 247 */
	 /* .local v1, "op":Landroid/app/ActivityOptions; */
	 android.app.ActivityOptions .makeBasic ( );
	 /* .line 248 */
	 int v2 = 1; // const/4 v2, 0x1
	 (( android.app.ActivityOptions ) v1 ).setLaunchWindowingMode ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V
	 /* .line 249 */
	 v3 = this.mRootWindowContainer;
	 int v4 = 2; // const/4 v4, 0x2
	 (( com.android.server.wm.RootWindowContainer ) v3 ).anyTaskForId ( p1, v4, v1, v2 ); // invoke-virtual {v3, p1, v4, v1, v2}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(IILandroid/app/ActivityOptions;Z)Lcom/android/server/wm/Task;
	 /* .line 254 */
} // .end local v1 # "op":Landroid/app/ActivityOptions;
} // :cond_0
} // .end method
private static Integer getNextRequestIdLocked ( ) {
/* .locals 2 */
/* .line 221 */
/* const v1, 0x7fffffff */
/* if-lt v0, v1, :cond_0 */
/* .line 222 */
int v0 = 0; // const/4 v0, 0x0
/* .line 224 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 225 */
} // .end method
static Boolean isAllowedAppSwitch ( com.android.server.wm.Task p0, java.lang.String p1, android.content.pm.ActivityInfo p2 ) {
/* .locals 7 */
/* .param p0, "stack" # Lcom/android/server/wm/Task; */
/* .param p1, "callingPackageName" # Ljava/lang/String; */
/* .param p2, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .line 191 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 192 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.Task ) p0 ).topRunningNonDelayedActivityLocked ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/Task;->topRunningNonDelayedActivityLocked(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;
/* .line 193 */
/* .local v1, "topr":Lcom/android/server/wm/ActivityRecord; */
final String v2 = "com.android.incallui.InCallActivity"; // const-string v2, "com.android.incallui.InCallActivity"
if ( v1 != null) { // if-eqz v1, :cond_1
v3 = this.info;
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = this.info;
v3 = this.name;
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 194 */
final String v3 = "com.android.incallui"; // const-string v3, "com.android.incallui"
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_1 */
if ( p2 != null) { // if-eqz p2, :cond_1
	 v3 = this.name;
	 /* .line 195 */
	 v3 = 	 (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v3, :cond_1 */
	 /* sget-wide v3, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mLastIncallUiLaunchTime:J */
	 /* const-wide/16 v5, 0x3e8 */
	 /* add-long/2addr v3, v5 */
	 /* .line 196 */
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v5 */
	 /* cmp-long v3, v3, v5 */
	 /* if-lez v3, :cond_1 */
	 /* .line 197 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "app switch:"; // const-string v4, "app switch:"
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v4 = this.name;
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v4 = " stopped for "; // const-string v4, " stopped for "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v3 = "in "; // const-string v3, "in "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* const/16 v3, 0x3e8 */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v3 = " ms.Try later."; // const-string v3, " ms.Try later."
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "ActivityManager"; // const-string v3, "ActivityManager"
	 android.util.Slog .w ( v3,v2 );
	 /* .line 199 */
	 /* .line 201 */
} // :cond_1
if ( p2 != null) { // if-eqz p2, :cond_2
	 v0 = this.name;
	 v0 = 	 (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 202 */
		 java.lang.System .currentTimeMillis ( );
		 /* move-result-wide v2 */
		 /* sput-wide v2, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mLastIncallUiLaunchTime:J */
		 /* .line 204 */
	 } // :cond_2
	 int v0 = 1; // const/4 v0, 0x1
} // .end method
static Boolean isAllowedAppSwitch ( com.android.server.wm.Task p0, java.lang.String p1, android.content.pm.ActivityInfo p2, Long p3 ) {
	 /* .locals 1 */
	 /* .param p0, "stack" # Lcom/android/server/wm/Task; */
	 /* .param p1, "callingPackageName" # Ljava/lang/String; */
	 /* .param p2, "aInfo" # Landroid/content/pm/ActivityInfo; */
	 /* .param p3, "lastTime" # J */
	 /* .line 186 */
	 v0 = 	 com.android.server.wm.ActivityTaskSupervisorImpl .isAllowedAppSwitch ( p0,p1,p2 );
} // .end method
private void lambda$activityIdle$0 ( android.content.pm.ActivityInfo p0, Long p1, Boolean p2 ) { //synthethic
	 /* .locals 0 */
	 /* .param p1, "info" # Landroid/content/pm/ActivityInfo; */
	 /* .param p2, "durationMillis" # J */
	 /* .param p4, "fromKeyguard" # Z */
	 /* .line 137 */
	 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->trackActivityResumeTimeout(Landroid/content/pm/ActivityInfo;JZ)V */
	 return;
} // .end method
public static Boolean notPauseAtFreeformMode ( com.android.server.wm.Task p0, com.android.server.wm.Task p1 ) {
	 /* .locals 4 */
	 /* .param p0, "focusStack" # Lcom/android/server/wm/Task; */
	 /* .param p1, "curStack" # Lcom/android/server/wm/Task; */
	 /* .line 229 */
	 v0 = 	 com.android.server.wm.ActivityTaskSupervisorImpl .supportsFreeform ( );
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-nez v0, :cond_0 */
	 /* .line 230 */
	 /* .line 232 */
} // :cond_0
v0 = (( com.android.server.wm.Task ) p0 ).getWindowingMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v2 = 5; // const/4 v2, 0x5
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v3, :cond_1 */
v0 = (( com.android.server.wm.Task ) p1 ).getWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-eq v0, v2, :cond_2 */
/* .line 233 */
} // :cond_1
v0 = (( com.android.server.wm.Task ) p0 ).getWindowingMode ( ); // invoke-virtual {p0}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-ne v0, v2, :cond_3 */
} // :cond_2
/* move v1, v3 */
/* .line 232 */
} // :cond_3
} // .end method
private static android.content.pm.ResolveInfo resolveIntent ( android.content.Intent p0, java.lang.String p1, Integer p2 ) {
/* .locals 6 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "resolvedType" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 213 */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/32 v3, 0x10400 */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move v5, p2 */
/* invoke-interface/range {v0 ..v5}, Landroid/content/pm/IPackageManager;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 215 */
/* :catch_0 */
/* move-exception v0 */
/* .line 217 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean supportsFreeform ( ) {
/* .locals 3 */
/* .line 237 */
/* nop */
/* .line 238 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
/* .line 237 */
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 1; // const/4 v1, 0x1
/* xor-int/2addr v0, v1 */
final String v2 = "persist.sys.miui_optimization"; // const-string v2, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v2,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 238 */
v0 = android.util.MiuiMultiWindowUtils .isForceResizeable ( );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 237 */
} // :goto_0
} // .end method
public static Boolean supportsMultiTaskInDock ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 208 */
v0 = com.android.server.wm.ActivityTaskSupervisorImpl.sSupportsMultiTaskInDockList;
v0 = (( java.util.ArrayList ) v0 ).contains ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
} // .end method
private void trackActivityResumeTimeout ( android.content.pm.ActivityInfo p0, Long p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "info" # Landroid/content/pm/ActivityInfo; */
/* .param p2, "durationMillis" # J */
/* .param p4, "fromKeyguard" # Z */
/* .line 143 */
try { // :try_start_0
/* new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V */
/* .line 144 */
/* .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* const/16 v1, 0x1b1 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setType ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 145 */
v1 = this.packageName;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPackageName ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V
/* .line 146 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setTimeStamp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V
/* .line 147 */
final String v1 = "activity resume timeout"; // const-string v1, "activity resume timeout"
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setSummary ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V
/* .line 148 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "activity="; // const-string v2, "activity="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.name;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " duration="; // const-string v2, " duration="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "ms fromKeyguard="; // const-string v2, "ms fromKeyguard="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setDetails ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 149 */
/* long-to-int v1, p2 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPid ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V
/* .line 150 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v1 ).reportGeneralException ( v0 ); // invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z
/* .line 152 */
/* new-instance v1, Lcom/miui/misight/MiEvent; */
/* const v2, 0x35b436fc */
/* invoke-direct {v1, v2}, Lcom/miui/misight/MiEvent;-><init>(I)V */
/* .line 153 */
/* .local v1, "miEvent":Lcom/miui/misight/MiEvent; */
final String v2 = "PackageName"; // const-string v2, "PackageName"
v3 = this.packageName;
(( com.miui.misight.MiEvent ) v1 ).addStr ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 154 */
final String v2 = "ActivityName"; // const-string v2, "ActivityName"
v3 = this.name;
(( com.miui.misight.MiEvent ) v1 ).addStr ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 155 */
final String v2 = "Duration"; // const-string v2, "Duration"
/* long-to-int v3, p2 */
(( com.miui.misight.MiEvent ) v1 ).addInt ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/miui/misight/MiEvent;->addInt(Ljava/lang/String;I)Lcom/miui/misight/MiEvent;
/* .line 156 */
final String v2 = "FromKeyguard"; // const-string v2, "FromKeyguard"
(( com.miui.misight.MiEvent ) v1 ).addBool ( v2, p4 ); // invoke-virtual {v1, v2, p4}, Lcom/miui/misight/MiEvent;->addBool(Ljava/lang/String;Z)Lcom/miui/misight/MiEvent;
/* .line 157 */
com.miui.misight.MiSight .sendEvent ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 160 */
} // .end local v0 # "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
} // .end local v1 # "miEvent":Lcom/miui/misight/MiEvent;
/* .line 158 */
/* :catch_0 */
/* move-exception v0 */
/* .line 161 */
} // :goto_0
return;
} // .end method
public static void updateApplicationConfiguration ( com.android.server.wm.ActivityTaskSupervisor p0, android.content.res.Configuration p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p0, "stackSupervisor" # Lcom/android/server/wm/ActivityTaskSupervisor; */
/* .param p1, "globalConfiguration" # Landroid/content/res/Configuration; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 261 */
v0 = this.mService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 262 */
try { // :try_start_0
v1 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v1 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 263 */
/* .local v1, "topStack":Lcom/android/server/wm/Task; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 264 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 266 */
v0 = this.mService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 267 */
try { // :try_start_1
(( com.android.server.wm.Task ) v1 ).topRunningActivityLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 268 */
/* .local v2, "topActivity":Lcom/android/server/wm/ActivityRecord; */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 270 */
if ( v2 != null) { // if-eqz v2, :cond_1
v0 = (( com.android.server.wm.ActivityRecord ) v2 ).getWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I
int v3 = 5; // const/4 v3, 0x5
/* if-ne v0, v3, :cond_1 */
v0 = this.packageName;
/* .line 272 */
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 273 */
	 (( com.android.server.wm.ActivityRecord ) v2 ).getConfiguration ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getConfiguration()Landroid/content/res/Configuration;
	 v0 = this.windowConfiguration;
	 (( android.app.WindowConfiguration ) v0 ).getBounds ( ); // invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
	 /* .line 274 */
	 /* .local v0, "rect":Landroid/graphics/Rect; */
	 v4 = 	 (( android.graphics.Rect ) v0 ).height ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
	 v5 = 	 (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
	 /* if-le v4, v5, :cond_0 */
	 /* .line 275 */
	 int v4 = 1; // const/4 v4, 0x1
} // :cond_0
int v4 = 2; // const/4 v4, 0x2
} // :goto_0
/* iput v4, p1, Landroid/content/res/Configuration;->orientation:I */
/* .line 276 */
v4 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v4 ).setWindowingMode ( v3 ); // invoke-virtual {v4, v3}, Landroid/app/WindowConfiguration;->setWindowingMode(I)V
/* .line 278 */
v3 = this.windowConfiguration;
/* .line 279 */
(( com.android.server.wm.ActivityRecord ) v2 ).getConfiguration ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getConfiguration()Landroid/content/res/Configuration;
v4 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v4 ).getBounds ( ); // invoke-virtual {v4}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
/* .line 278 */
(( android.app.WindowConfiguration ) v3 ).setBounds ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V
/* .line 268 */
} // .end local v0 # "rect":Landroid/graphics/Rect;
} // .end local v2 # "topActivity":Lcom/android/server/wm/ActivityRecord;
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v2 */
/* .line 282 */
} // :cond_1
} // :goto_1
return;
/* .line 263 */
} // .end local v1 # "topStack":Lcom/android/server/wm/Task;
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v1 */
} // .end method
static void updateInfoBeforeRealStartActivity ( com.android.server.wm.Task p0, android.app.IApplicationThread p1, Integer p2, java.lang.String p3, android.content.Intent p4, android.content.pm.ActivityInfo p5, android.os.IBinder p6, Integer p7, Integer p8 ) {
/* .locals 1 */
/* .param p0, "stack" # Lcom/android/server/wm/Task; */
/* .param p1, "caller" # Landroid/app/IApplicationThread; */
/* .param p2, "callingUid" # I */
/* .param p3, "callingPackage" # Ljava/lang/String; */
/* .param p4, "intent" # Landroid/content/Intent; */
/* .param p5, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p6, "resultTo" # Landroid/os/IBinder; */
/* .param p7, "requestCode" # I */
/* .param p8, "userId" # I */
/* .line 174 */
com.android.server.wm.MiuiMultiTaskManagerStub .get ( );
/* .line 175 */
return;
} // .end method
/* # virtual methods */
void acquireLaunchWakelock ( ) {
/* .locals 0 */
/* .line 115 */
return;
} // .end method
void activityIdle ( android.content.pm.ActivityInfo p0 ) {
/* .locals 13 */
/* .param p1, "info" # Landroid/content/pm/ActivityInfo; */
/* .line 130 */
/* iget-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
return;
/* .line 131 */
} // :cond_0
/* iget-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J */
/* .line 132 */
/* .local v0, "timestamp":J */
/* iget-boolean v10, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mFromKeyguard:Z */
/* .line 133 */
/* .local v10, "fromKeyguard":Z */
/* iput-wide v2, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J */
/* .line 134 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mFromKeyguard:Z */
/* .line 135 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* sub-long/2addr v2, v0 */
/* .line 136 */
/* .local v2, "durationMillis":J */
/* const-wide/16 v4, 0x1388 */
/* cmp-long v4, v2, v4 */
/* if-lez v4, :cond_1 */
/* .line 137 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v12, Lcom/android/server/wm/ActivityTaskSupervisorImpl$$ExternalSyntheticLambda0; */
/* move-object v4, v12 */
/* move-object v5, p0 */
/* move-object v6, p1 */
/* move-wide v7, v2 */
/* move v9, v10 */
/* invoke-direct/range {v4 ..v9}, Lcom/android/server/wm/ActivityTaskSupervisorImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityTaskSupervisorImpl;Landroid/content/pm/ActivityInfo;JZ)V */
(( android.os.Handler ) v11 ).post ( v12 ); // invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 139 */
} // :cond_1
return;
} // .end method
void addSender ( com.android.server.wm.ActivityTaskManagerService p0, android.content.Intent p1, Integer p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "service" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "callingPid" # I */
/* .param p4, "filterCallingUid" # I */
/* .line 285 */
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 286 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerService ) p1 ).getProcessController ( p3, p4 ); // invoke-virtual {p1, p3, p4}, Lcom/android/server/wm/ActivityTaskManagerService;->getProcessController(II)Lcom/android/server/wm/WindowProcessController;
/* .line 287 */
/* .local v1, "wpc":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.mInfo;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 288 */
v2 = this.mInfo;
v2 = this.packageName;
(( android.content.Intent ) p2 ).setSender ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->setSender(Ljava/lang/String;)V
/* .line 290 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
(( android.content.Intent ) p2 ).setSender ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->setSender(Ljava/lang/String;)V
/* .line 292 */
} // .end local v1 # "wpc":Lcom/android/server/wm/WindowProcessController;
} // :goto_0
/* monitor-exit v0 */
/* .line 293 */
return;
/* .line 292 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void init ( com.android.server.wm.ActivityTaskManagerService p0, android.os.Looper p1 ) {
/* .locals 1 */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 76 */
this.mAtmService = p1;
/* .line 77 */
v0 = this.mContext;
this.mContext = v0;
/* .line 78 */
return;
} // .end method
Integer isAppLockActivity ( com.android.server.wm.ActivityRecord p0, android.content.Intent p1, android.content.pm.ActivityInfo p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p4, "requestCode" # I */
/* .line 108 */
/* nop */
/* .line 103 */
(( com.android.server.wm.ActivityRecord ) p1 ).getTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = -1; // const/4 v0, -0x1
/* if-ne p4, v0, :cond_0 */
if ( p2 != null) { // if-eqz p2, :cond_0
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 106 */
(( android.content.Intent ) p2 ).getPackage ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 107 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v1 = "miui.intent.action.CHECK_ACCESS_CONTROL"; // const-string v1, "miui.intent.action.CHECK_ACCESS_CONTROL"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.name;
/* .line 108 */
final String v1 = "com.miui.applicationlock.ConfirmAccessControl"; // const-string v1, "com.miui.applicationlock.ConfirmAccessControl"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 109 */
	 /* const/16 v0, -0x3e9 */
} // :cond_0
/* move v0, p4 */
/* .line 103 */
} // :goto_0
} // .end method
void keyguardGoingAway ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "flags" # I */
/* .line 124 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J */
/* .line 125 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mFromKeyguard:Z */
/* .line 126 */
return;
} // .end method
void startActivityFromRecentsFlag ( android.content.Intent p0 ) {
/* .locals 3 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 82 */
(( android.content.Intent ) p1 ).getPackage ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
/* .line 83 */
/* .local v0, "targetPkg":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 84 */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 85 */
/* .local v1, "component":Landroid/content/ComponentName; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 86 */
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 89 */
} // .end local v1 # "component":Landroid/content/ComponentName;
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 90 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 91 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
int v2 = 0; // const/4 v2, 0x0
(( android.content.pm.PackageManager ) v1 ).resolveActivity ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
/* .line 92 */
/* .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 93 */
v0 = this.resolvePackageName;
/* .line 96 */
} // .end local v1 # "pm":Landroid/content/pm/PackageManager;
} // .end local v2 # "resolveInfo":Landroid/content/pm/ResolveInfo;
} // :cond_1
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_2 */
/* .line 97 */
int v1 = 1; // const/4 v1, 0x1
com.android.server.am.PendingIntentRecordImpl .exemptTemporarily ( v0,v1 );
/* .line 99 */
} // :cond_2
return;
} // .end method
void startPausingResumedActivity ( ) {
/* .locals 2 */
/* .line 119 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/wm/ActivityTaskSupervisorImpl;->mTimestamp:J */
/* .line 120 */
return;
} // .end method
