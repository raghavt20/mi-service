public class com.android.server.wm.MiuiSplitInputMethodImpl implements com.android.server.wm.MiuiSplitInputMethodStub {
	 /* .source "MiuiSplitInputMethodImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 static Boolean DEBUG;
	 public static final java.lang.String TAG;
	 private static Boolean sIsSplitIMESupport;
	 private static java.util.HashMap sSplitMinVersionSupport;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private Float mAccuracy;
private android.content.Context mContext;
private android.os.Handler mHandler;
private com.android.server.inputmethod.InputMethodManagerService mImms;
private Boolean mSplitImeSwitch;
private final android.graphics.Rect mTmpBounds;
private final android.content.res.Configuration mTmpConfiguration;
private Integer sVersionCode;
/* # direct methods */
public static void $r8$lambda$OlYhkjpgLpK-_ttsHB0Tw3y_e0E ( com.android.server.wm.MiuiSplitInputMethodImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->lambda$init$0()V */
return;
} // .end method
static com.android.server.wm.MiuiSplitInputMethodImpl ( ) {
/* .locals 3 */
/* .line 40 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.wm.MiuiSplitInputMethodImpl.DEBUG = (v0!= 0);
/* .line 52 */
com.android.server.wm.MiuiSplitInputMethodImpl.sIsSplitIMESupport = (v0!= 0);
/* .line 54 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 62 */
/* const/16 v1, 0x1f7d */
java.lang.Integer .valueOf ( v1 );
final String v2 = "com.iflytek.inputmethod.miui"; // const-string v2, "com.iflytek.inputmethod.miui"
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 63 */
v0 = com.android.server.wm.MiuiSplitInputMethodImpl.sSplitMinVersionSupport;
/* const/16 v1, 0x6f6 */
java.lang.Integer .valueOf ( v1 );
final String v2 = "com.sohu.inputmethod.sogou.xiaomi"; // const-string v2, "com.sohu.inputmethod.sogou.xiaomi"
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 64 */
v0 = com.android.server.wm.MiuiSplitInputMethodImpl.sSplitMinVersionSupport;
/* const/16 v1, 0x587 */
java.lang.Integer .valueOf ( v1 );
final String v2 = "com.baidu.input_mi"; // const-string v2, "com.baidu.input_mi"
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 65 */
return;
} // .end method
public com.android.server.wm.MiuiSplitInputMethodImpl ( ) {
/* .locals 2 */
/* .line 35 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 38 */
/* new-instance v0, Landroid/content/res/Configuration; */
/* invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V */
this.mTmpConfiguration = v0;
/* .line 42 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V */
this.mTmpBounds = v0;
/* .line 50 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sVersionCode:I */
/* .line 56 */
final String v0 = "persist.spltiIme"; // const-string v0, "persist.spltiIme"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z */
/* .line 58 */
/* const v0, 0x3c23d70a # 0.01f */
/* iput v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mAccuracy:F */
return;
} // .end method
private void clearImebounds ( android.content.res.Configuration p0, com.android.server.wm.DisplayArea$Tokens p1 ) {
/* .locals 2 */
/* .param p1, "configuration" # Landroid/content/res/Configuration; */
/* .param p2, "imeWindowsContainer" # Lcom/android/server/wm/DisplayArea$Tokens; */
/* .line 166 */
/* iget v0, p1, Landroid/content/res/Configuration;->seq:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p1, Landroid/content/res/Configuration;->seq:I */
/* .line 167 */
v0 = this.mTmpBounds;
(( android.graphics.Rect ) v0 ).setEmpty ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V
/* .line 168 */
v0 = this.mTmpBounds;
/* filled-new-array {v0}, [Ljava/lang/Object; */
/* const-string/jumbo v1, "updateImeAppBounds" */
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( p1,v1,v0 );
/* .line 169 */
if ( p2 != null) { // if-eqz p2, :cond_0
	 /* .line 170 */
	 (( com.android.server.wm.DisplayArea$Tokens ) p2 ).onConfigurationChanged ( p1 ); // invoke-virtual {p2, p1}, Lcom/android/server/wm/DisplayArea$Tokens;->onConfigurationChanged(Landroid/content/res/Configuration;)V
	 /* .line 172 */
} // :cond_0
return;
} // .end method
private Integer getImeVersionCode ( ) {
/* .locals 4 */
/* .line 97 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->getInputMethodPackageName()Ljava/lang/String; */
/* .line 99 */
/* .local v0, "curMethodPackageName":Ljava/lang/String; */
try { // :try_start_0
	 v1 = this.mContext;
	 (( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 int v2 = 0; // const/4 v2, 0x0
	 (( android.content.pm.PackageManager ) v1 ).getPackageInfo ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
	 /* .line 100 */
	 /* .local v1, "packageInfo":Landroid/content/pm/PackageInfo; */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 101 */
		 /* iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I */
		 /* iput v2, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sVersionCode:I */
		 /* :try_end_0 */
		 /* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 106 */
	 } // .end local v1 # "packageInfo":Landroid/content/pm/PackageInfo;
} // :cond_0
/* .line 103 */
/* :catch_0 */
/* move-exception v1 */
/* .line 104 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
int v2 = -1; // const/4 v2, -0x1
/* iput v2, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sVersionCode:I */
/* .line 105 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "check "; // const-string v3, "check "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " Version failed"; // const-string v3, " Version failed"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiSplitInputMethodImpl"; // const-string v3, "MiuiSplitInputMethodImpl"
android.util.Slog .e ( v3,v2,v1 );
/* .line 107 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
/* iget v1, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sVersionCode:I */
} // .end method
private java.lang.String getInputMethodPackageName ( ) {
/* .locals 3 */
/* .line 85 */
v0 = this.mImms;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 86 */
/* .line 88 */
} // :cond_0
final String v2 = "getCurIdLocked"; // const-string v2, "getCurIdLocked"
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v0,v2,v1 );
/* check-cast v0, Ljava/lang/String; */
/* .line 90 */
/* .local v0, "curMethod":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 91 */
final String v1 = "/"; // const-string v1, "/"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
/* aget-object v1, v1, v2 */
/* .line 93 */
} // :cond_1
} // .end method
public static com.android.server.wm.MiuiSplitInputMethodImpl getInstance ( ) {
/* .locals 1 */
/* .line 76 */
com.android.server.wm.MiuiSplitInputMethodStub .getInstance ( );
/* check-cast v0, Lcom/android/server/wm/MiuiSplitInputMethodImpl; */
} // .end method
public static java.lang.Object invoke ( java.lang.Object p0, java.lang.String p1, java.lang.Object...p2 ) {
/* .locals 7 */
/* .param p0, "obj" # Ljava/lang/Object; */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .param p2, "values" # [Ljava/lang/Object; */
/* .line 129 */
int v0 = 0; // const/4 v0, 0x0
/* .line 131 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 132 */
/* .local v2, "clazz":Ljava/lang/Class; */
int v3 = 1; // const/4 v3, 0x1
/* if-nez p2, :cond_0 */
/* .line 133 */
(( java.lang.Class ) v2 ).getDeclaredMethod ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* move-object v0, v4 */
/* .line 134 */
(( java.lang.reflect.Method ) v0 ).setAccessible ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 135 */
int v3 = 0; // const/4 v3, 0x0
/* new-array v3, v3, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v0 ).invoke ( p0, v3 ); // invoke-virtual {v0, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 137 */
} // :cond_0
/* array-length v4, p2 */
/* new-array v4, v4, [Ljava/lang/Class; */
/* .line 138 */
/* .local v4, "argsClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* array-length v6, p2 */
/* if-ge v5, v6, :cond_4 */
/* .line 139 */
/* aget-object v6, p2, v5 */
/* instance-of v6, v6, Ljava/lang/Integer; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 140 */
v6 = java.lang.Integer.TYPE;
/* aput-object v6, v4, v5 */
/* .line 141 */
} // :cond_1
/* aget-object v6, p2, v5 */
/* instance-of v6, v6, Ljava/lang/Boolean; */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 142 */
v6 = java.lang.Boolean.TYPE;
/* aput-object v6, v4, v5 */
/* .line 143 */
} // :cond_2
/* aget-object v6, p2, v5 */
/* instance-of v6, v6, Ljava/lang/Float; */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 144 */
v6 = java.lang.Float.TYPE;
/* aput-object v6, v4, v5 */
/* .line 146 */
} // :cond_3
/* aget-object v6, p2, v5 */
(( java.lang.Object ) v6 ).getClass ( ); // invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* aput-object v6, v4, v5 */
/* .line 138 */
} // :goto_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 149 */
} // .end local v5 # "i":I
} // :cond_4
(( java.lang.Class ) v2 ).getDeclaredMethod ( p1, v4 ); // invoke-virtual {v2, p1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* move-object v0, v5 */
/* .line 150 */
(( java.lang.reflect.Method ) v0 ).setAccessible ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 151 */
(( java.lang.reflect.Method ) v0 ).invoke ( p0, p2 ); // invoke-virtual {v0, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 152 */
} // .end local v2 # "clazz":Ljava/lang/Class;
} // .end local v4 # "argsClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
/* :catch_0 */
/* move-exception v2 */
/* .line 153 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getDeclaredMethod:"; // const-string v4, "getDeclaredMethod:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiSplitInputMethodImpl"; // const-string v4, "MiuiSplitInputMethodImpl"
android.util.Slog .d ( v4,v3 );
/* .line 155 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // .end method
private Boolean isFoldDevice ( ) {
/* .locals 2 */
/* .line 81 */
final String v0 = "babylon"; // const-string v0, "babylon"
v1 = miui.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
private Boolean isInLargeScreen ( android.content.res.Configuration p0 ) {
/* .locals 3 */
/* .param p1, "configuration" # Landroid/content/res/Configuration; */
/* .line 111 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 112 */
} // :cond_0
/* iget v1, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I */
/* const/16 v2, 0x258 */
/* if-lt v1, v2, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
} // .end method
private void lambda$init$0 ( ) { //synthethic
/* .locals 2 */
/* .line 71 */
final String v0 = "persist.spltiIme"; // const-string v0, "persist.spltiIme"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z */
return;
} // .end method
/* # virtual methods */
public void adjustCutoutForSplitIme ( android.view.InsetsState p0, com.android.server.wm.WindowState p1 ) {
/* .locals 7 */
/* .param p1, "state" # Landroid/view/InsetsState; */
/* .param p2, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 208 */
/* if-nez p2, :cond_0 */
/* .line 209 */
return;
/* .line 211 */
} // :cond_0
(( com.android.server.wm.WindowState ) p2 ).getConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;
v0 = (( com.android.server.wm.MiuiSplitInputMethodImpl ) p0 ).isSplitIMESupport ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z
/* if-nez v0, :cond_1 */
/* .line 212 */
return;
/* .line 214 */
} // :cond_1
v0 = this.mWmService;
/* .line 215 */
(( com.android.server.wm.WindowManagerService ) v0 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.DisplayContent ) v0 ).getImeTarget ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;
/* .line 217 */
/* .local v0, "imeTarget":Lcom/android/server/wm/InsetsControlTarget; */
if ( v0 != null) { // if-eqz v0, :cond_9
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
/* .line 218 */
v2 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v2 ).isInSystemSplitScreen ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
/* if-nez v2, :cond_2 */
/* .line 221 */
} // :cond_2
v2 = (( com.android.server.wm.WindowState ) p2 ).getWindowType ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* const/16 v3, 0x7dc */
int v4 = 1; // const/4 v4, 0x1
/* const/16 v5, 0x7db */
/* if-eq v2, v5, :cond_4 */
/* .line 222 */
v2 = (( com.android.server.wm.WindowState ) p2 ).getWindowType ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* if-ne v2, v3, :cond_3 */
} // :cond_3
/* move v2, v1 */
} // :cond_4
} // :goto_0
/* move v2, v4 */
/* .line 223 */
/* .local v2, "isImeWindow":Z */
} // :goto_1
(( com.android.server.wm.WindowState ) p2 ).getParent ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 224 */
(( com.android.server.wm.WindowState ) p2 ).getParent ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;
/* instance-of v6, v6, Lcom/android/server/wm/WindowState; */
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 225 */
(( com.android.server.wm.WindowState ) p2 ).getParent ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;
v6 = (( com.android.server.wm.WindowContainer ) v6 ).getWindowType ( ); // invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->getWindowType()I
/* if-eq v6, v5, :cond_5 */
/* .line 226 */
(( com.android.server.wm.WindowState ) p2 ).getParent ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;
v5 = (( com.android.server.wm.WindowContainer ) v5 ).getWindowType ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->getWindowType()I
/* if-ne v5, v3, :cond_6 */
} // :cond_5
/* move v1, v4 */
} // :cond_6
/* nop */
/* .line 227 */
/* .local v1, "isImeChildWindow":Z */
} // :goto_2
/* if-nez v2, :cond_7 */
/* if-nez v1, :cond_7 */
return;
/* .line 228 */
} // :cond_7
(( com.android.server.wm.WindowState ) p2 ).getConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;
/* iget v3, v3, Landroid/content/res/Configuration;->orientation:I */
int v4 = 2; // const/4 v4, 0x2
/* if-eq v3, v4, :cond_8 */
/* .line 229 */
return;
/* .line 232 */
} // :cond_8
v3 = android.view.DisplayCutout.NO_CUTOUT;
(( android.view.InsetsState ) p1 ).setDisplayCutout ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/InsetsState;->setDisplayCutout(Landroid/view/DisplayCutout;)V
/* .line 233 */
v3 = android.view.WindowInsets$Type .displayCutout ( );
(( android.view.InsetsState ) p1 ).removeSource ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/InsetsState;->removeSource(I)V
/* .line 234 */
final String v3 = "MiuiSplitInputMethodImpl"; // const-string v3, "MiuiSplitInputMethodImpl"
final String v4 = "remove cutout for ime"; // const-string v4, "remove cutout for ime"
android.util.Slog .d ( v3,v4 );
/* .line 235 */
return;
/* .line 219 */
} // .end local v1 # "isImeChildWindow":Z
} // .end local v2 # "isImeWindow":Z
} // :cond_9
} // :goto_3
return;
} // .end method
public void adjustWindowFrameForSplitIme ( com.android.server.wm.WindowState p0, android.window.ClientWindowFrames p1 ) {
/* .locals 0 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .param p2, "clientWindowFrames" # Landroid/window/ClientWindowFrames; */
/* .line 162 */
return;
} // .end method
public void init ( com.android.server.inputmethod.InputMethodManagerService p0, android.os.Handler p1, android.content.Context p2 ) {
/* .locals 1 */
/* .param p1, "imms" # Lcom/android/server/inputmethod/InputMethodManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 68 */
this.mImms = p1;
/* .line 69 */
this.mHandler = p2;
/* .line 70 */
this.mContext = p3;
/* .line 71 */
/* new-instance v0, Lcom/android/server/wm/MiuiSplitInputMethodImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiSplitInputMethodImpl;)V */
android.os.SystemProperties .addChangeCallback ( v0 );
/* .line 73 */
return;
} // .end method
public Boolean isSplitIMESupport ( android.content.res.Configuration p0 ) {
/* .locals 6 */
/* .param p1, "configuration" # Landroid/content/res/Configuration; */
/* .line 116 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->getInputMethodPackageName()Ljava/lang/String; */
/* .line 117 */
/* .local v0, "curMethodPackageName":Ljava/lang/String; */
v1 = this.mContext;
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_3
/* if-nez v0, :cond_0 */
/* .line 118 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
com.android.server.wm.MiuiSplitInputMethodImpl.sIsSplitIMESupport = (v1!= 0);
/* .line 119 */
v3 = com.android.server.wm.MiuiSplitInputMethodImpl.sSplitMinVersionSupport;
(( java.util.HashMap ) v3 ).get ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
/* .line 120 */
/* .local v3, "minSupportVersionCode":Ljava/lang/Integer; */
v4 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->getImeVersionCode()I */
/* .line 121 */
/* .local v4, "imeCurVersionCode":I */
if ( v3 != null) { // if-eqz v3, :cond_1
v5 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* if-lt v4, v5, :cond_1 */
/* .line 122 */
com.android.server.wm.MiuiSplitInputMethodImpl.sIsSplitIMESupport = (v1!= 0);
/* .line 125 */
} // :cond_1
/* iget-boolean v5, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* sget-boolean v5, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->sIsSplitIMESupport:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
v5 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isFoldDevice()Z */
if ( v5 != null) { // if-eqz v5, :cond_2
v5 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isInLargeScreen(Landroid/content/res/Configuration;)Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* move v2, v1 */
} // :cond_2
/* .line 117 */
} // .end local v3 # "minSupportVersionCode":Ljava/lang/Integer;
} // .end local v4 # "imeCurVersionCode":I
} // :cond_3
} // :goto_0
} // .end method
public void onConfigurationChangedForSplitIme ( android.content.res.Configuration p0, com.android.server.wm.DisplayArea$Tokens p1 ) {
/* .locals 6 */
/* .param p1, "newParentConfig" # Landroid/content/res/Configuration; */
/* .param p2, "imeContainer" # Lcom/android/server/wm/DisplayArea$Tokens; */
/* .line 264 */
if ( p1 != null) { // if-eqz p1, :cond_7
/* if-nez p2, :cond_0 */
/* goto/16 :goto_1 */
/* .line 265 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z */
/* if-nez v0, :cond_1 */
return;
/* .line 266 */
} // :cond_1
v0 = this.mDisplayContent;
/* .line 267 */
/* .local v0, "dc":Lcom/android/server/wm/DisplayContent; */
/* if-nez v0, :cond_2 */
return;
/* .line 268 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* new-array v2, v1, [Ljava/lang/Object; */
final String v3 = "getmImeAppBounds"; // const-string v3, "getmImeAppBounds"
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( p1,v3,v2 );
/* check-cast v2, Landroid/graphics/Rect; */
/* .line 269 */
/* .local v2, "parentImeBounds":Landroid/graphics/Rect; */
(( com.android.server.wm.DisplayArea$Tokens ) p2 ).getConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;
/* new-array v5, v1, [Ljava/lang/Object; */
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v4,v3,v5 );
/* check-cast v3, Landroid/graphics/Rect; */
/* .line 270 */
/* .local v3, "imeBounds":Landroid/graphics/Rect; */
if ( v2 != null) { // if-eqz v2, :cond_3
v4 = (( android.graphics.Rect ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z
if ( v4 != null) { // if-eqz v4, :cond_3
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 271 */
v4 = (( android.graphics.Rect ) v3 ).isEmpty ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v4, :cond_3 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_3
/* nop */
/* .line 272 */
/* .local v1, "needToClearImeBounds":Z */
} // :goto_0
(( com.android.server.wm.DisplayContent ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getConfiguration()Landroid/content/res/Configuration;
v4 = (( com.android.server.wm.MiuiSplitInputMethodImpl ) p0 ).isSplitIMESupport ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z
/* if-nez v4, :cond_5 */
/* if-nez v1, :cond_5 */
/* .line 273 */
/* sget-boolean v4, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 274 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Ime not support split mode:"; // const-string v5, "Ime not support split mode:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiSplitInputMethodImpl"; // const-string v5, "MiuiSplitInputMethodImpl"
android.util.Slog .d ( v5,v4 );
/* .line 276 */
} // :cond_4
return;
/* .line 285 */
} // :cond_5
v4 = this.mWmService;
if ( v4 != null) { // if-eqz v4, :cond_6
v4 = this.mDisplayContent;
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 286 */
v4 = this.mWmService;
v4 = this.mWindowContextListenerController;
v5 = this.mDisplayContent;
/* iget v5, v5, Lcom/android/server/wm/DisplayContent;->mDisplayId:I */
/* .line 287 */
(( com.android.server.wm.WindowContextListenerController ) v4 ).dispatchPendingConfigurationIfNeeded ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/wm/WindowContextListenerController;->dispatchPendingConfigurationIfNeeded(I)V
/* .line 289 */
} // :cond_6
return;
/* .line 264 */
} // .end local v0 # "dc":Lcom/android/server/wm/DisplayContent;
} // .end local v1 # "needToClearImeBounds":Z
} // .end local v2 # "parentImeBounds":Landroid/graphics/Rect;
} // .end local v3 # "imeBounds":Landroid/graphics/Rect;
} // :cond_7
} // :goto_1
return;
} // .end method
public void onConfigurationChangedForTask ( android.content.res.Configuration p0, com.android.server.wm.WindowContainer p1 ) {
/* .locals 12 */
/* .param p1, "newParentConfig" # Landroid/content/res/Configuration; */
/* .param p2, "wc" # Lcom/android/server/wm/WindowContainer; */
/* .line 322 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 323 */
} // :cond_0
(( com.android.server.wm.WindowContainer ) p2 ).asTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
/* if-nez v0, :cond_1 */
return;
/* .line 324 */
} // :cond_1
(( com.android.server.wm.WindowContainer ) p2 ).getDisplayContent ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
/* .line 325 */
/* .local v0, "dc":Lcom/android/server/wm/DisplayContent; */
/* if-nez v0, :cond_2 */
return;
/* .line 327 */
} // :cond_2
(( com.android.server.wm.DisplayContent ) v0 ).getImeContainer ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getImeContainer()Lcom/android/server/wm/DisplayArea$Tokens;
/* .line 328 */
/* .local v1, "imeContainer":Lcom/android/server/wm/DisplayArea$Tokens; */
/* if-nez v1, :cond_3 */
return;
/* .line 331 */
} // :cond_3
(( com.android.server.wm.DisplayArea$Tokens ) v1 ).getConfiguration ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;
/* .line 332 */
/* .local v2, "preConfiguration":Landroid/content/res/Configuration; */
(( com.android.server.wm.DisplayContent ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getConfiguration()Landroid/content/res/Configuration;
v3 = (( com.android.server.wm.MiuiSplitInputMethodImpl ) p0 ).isSplitIMESupport ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z
final String v4 = "MiuiSplitInputMethodImpl"; // const-string v4, "MiuiSplitInputMethodImpl"
final String v5 = "isImeMultiWindowMode"; // const-string v5, "isImeMultiWindowMode"
int v6 = 0; // const/4 v6, 0x0
/* if-nez v3, :cond_5 */
/* .line 333 */
(( com.android.server.wm.DisplayArea$Tokens ) v1 ).getConfiguration ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;
/* new-array v7, v6, [Ljava/lang/Object; */
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v3,v5,v7 );
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 334 */
(( com.android.server.wm.DisplayArea$Tokens ) v1 ).getConfiguration ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;
/* new-array v6, v6, [Ljava/lang/Object; */
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v3,v5,v6 );
/* check-cast v3, Ljava/lang/Boolean; */
v3 = (( java.lang.Boolean ) v3 ).booleanValue ( ); // invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 335 */
/* invoke-direct {p0, v2, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->clearImebounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayArea$Tokens;)V */
/* .line 337 */
} // :cond_4
final String v3 = "IME or device not support split ime"; // const-string v3, "IME or device not support split ime"
android.util.Log .d ( v4,v3 );
/* .line 338 */
return;
/* .line 341 */
} // :cond_5
(( com.android.server.wm.DisplayContent ) v0 ).getImeTarget ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;
if ( v3 != null) { // if-eqz v3, :cond_e
(( com.android.server.wm.DisplayContent ) v0 ).getImeTarget ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;
/* .line 342 */
/* if-nez v3, :cond_6 */
/* goto/16 :goto_1 */
/* .line 343 */
} // :cond_6
(( com.android.server.wm.DisplayContent ) v0 ).getImeTarget ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;
/* .line 345 */
/* .local v3, "imeTarget":Lcom/android/server/wm/WindowState; */
(( com.android.server.wm.WindowState ) v3 ).getActivityRecord ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;
if ( v7 != null) { // if-eqz v7, :cond_7
(( com.android.server.wm.WindowState ) v3 ).getActivityRecord ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;
(( com.android.server.wm.WindowContainer ) p2 ).asTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
/* .line 346 */
(( com.android.server.wm.Task ) v8 ).getTopVisibleActivity ( ); // invoke-virtual {v8}, Lcom/android/server/wm/Task;->getTopVisibleActivity()Lcom/android/server/wm/ActivityRecord;
/* if-ne v7, v8, :cond_8 */
} // :cond_7
(( com.android.server.wm.WindowContainer ) p2 ).asTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
v7 = this.affinity;
/* if-nez v7, :cond_9 */
} // :cond_8
return;
/* .line 349 */
} // :cond_9
v7 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v7 ).getBounds ( ); // invoke-virtual {v7}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
v7 = (( android.graphics.Rect ) v7 ).width ( ); // invoke-virtual {v7}, Landroid/graphics/Rect;->width()I
/* int-to-float v7, v7 */
/* .line 350 */
(( com.android.server.wm.DisplayContent ) v0 ).getBounds ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getBounds()Landroid/graphics/Rect;
v8 = (( android.graphics.Rect ) v8 ).width ( ); // invoke-virtual {v8}, Landroid/graphics/Rect;->width()I
/* int-to-float v8, v8 */
/* div-float/2addr v7, v8 */
/* .line 351 */
/* .local v7, "scale":F */
(( com.android.server.wm.DisplayArea$Tokens ) v1 ).getConfiguration ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;
/* new-array v9, v6, [Ljava/lang/Object; */
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v8,v5,v9 );
/* check-cast v5, Ljava/lang/Boolean; */
v5 = (( java.lang.Boolean ) v5 ).booleanValue ( ); // invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 354 */
/* .local v5, "isPrevSplitIme":Z */
/* float-to-double v8, v7 */
/* const-wide/high16 v10, 0x3fe0000000000000L # 0.5 */
/* sub-double/2addr v8, v10 */
java.lang.Math .abs ( v8,v9 );
/* move-result-wide v8 */
/* iget v10, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mAccuracy:F */
/* float-to-double v10, v10 */
/* cmpg-double v8, v8, v10 */
int v9 = 1; // const/4 v9, 0x1
/* if-gez v8, :cond_a */
/* move v8, v9 */
} // :cond_a
/* move v8, v6 */
/* .line 355 */
/* .local v8, "isImeTargetSplitMode":Z */
} // :goto_0
/* if-nez v8, :cond_b */
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 356 */
/* invoke-direct {p0, v2, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->clearImebounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayArea$Tokens;)V */
/* .line 357 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "invalid scale:"; // const-string v9, "invalid scale:"
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ",clear ime bounds"; // const-string v9, ",clear ime bounds"
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v6 );
/* .line 358 */
return;
/* .line 360 */
} // :cond_b
if ( v8 != null) { // if-eqz v8, :cond_d
/* .line 361 */
v4 = this.mTmpBounds;
(( android.graphics.Rect ) v4 ).setEmpty ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V
/* .line 362 */
v4 = this.mTmpBounds;
v10 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v10 ).getBounds ( ); // invoke-virtual {v10}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;
(( android.graphics.Rect ) v4 ).set ( v10 ); // invoke-virtual {v4, v10}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 363 */
v4 = this.mTmpConfiguration;
(( android.content.res.Configuration ) v4 ).unset ( ); // invoke-virtual {v4}, Landroid/content/res/Configuration;->unset()V
/* .line 364 */
v4 = this.mTmpConfiguration;
(( android.content.res.Configuration ) v4 ).setTo ( v2 ); // invoke-virtual {v4, v2}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V
/* .line 365 */
v4 = this.mTmpConfiguration;
/* iget v10, v4, Landroid/content/res/Configuration;->seq:I */
/* add-int/2addr v10, v9 */
/* iput v10, v4, Landroid/content/res/Configuration;->seq:I */
/* .line 366 */
v4 = this.mTmpBounds;
final String v9 = "getmImeAppBounds"; // const-string v9, "getmImeAppBounds"
/* new-array v10, v6, [Ljava/lang/Object; */
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v2,v9,v10 );
v4 = (( android.graphics.Rect ) v4 ).equals ( v9 ); // invoke-virtual {v4, v9}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_d */
/* .line 367 */
/* if-nez v5, :cond_c */
(( com.android.server.wm.MiuiSplitInputMethodImpl ) p0 ).updateImeVisiblityIfNeed ( v6 ); // invoke-virtual {p0, v6}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->updateImeVisiblityIfNeed(Z)V
/* .line 368 */
} // :cond_c
v4 = this.mTmpConfiguration;
v6 = this.mTmpBounds;
/* filled-new-array {v6}, [Ljava/lang/Object; */
/* const-string/jumbo v9, "updateImeAppBounds" */
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v4,v9,v6 );
/* .line 369 */
v4 = this.mTmpConfiguration;
(( com.android.server.wm.DisplayArea$Tokens ) v1 ).onConfigurationChanged ( v4 ); // invoke-virtual {v1, v4}, Lcom/android/server/wm/DisplayArea$Tokens;->onConfigurationChanged(Landroid/content/res/Configuration;)V
/* .line 372 */
} // :cond_d
return;
/* .line 342 */
} // .end local v3 # "imeTarget":Lcom/android/server/wm/WindowState;
} // .end local v5 # "isPrevSplitIme":Z
} // .end local v7 # "scale":F
} // .end local v8 # "isImeTargetSplitMode":Z
} // :cond_e
} // :goto_1
return;
} // .end method
public void resolveOverrideConfigurationForSplitIme ( android.content.res.Configuration p0, com.android.server.wm.DisplayArea$Tokens p1 ) {
/* .locals 5 */
/* .param p1, "newParentConfig" # Landroid/content/res/Configuration; */
/* .param p2, "imeContainer" # Lcom/android/server/wm/DisplayArea$Tokens; */
/* .line 293 */
if ( p1 != null) { // if-eqz p1, :cond_5
/* if-nez p2, :cond_0 */
/* .line 294 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mSplitImeSwitch:Z */
/* if-nez v0, :cond_1 */
return;
/* .line 295 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* new-array v1, v0, [Ljava/lang/Object; */
final String v2 = "getmImeAppBounds"; // const-string v2, "getmImeAppBounds"
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( p1,v2,v1 );
/* check-cast v1, Landroid/graphics/Rect; */
/* .line 296 */
/* .local v1, "parentImeBounds":Landroid/graphics/Rect; */
(( com.android.server.wm.DisplayArea$Tokens ) p2 ).getConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayArea$Tokens;->getConfiguration()Landroid/content/res/Configuration;
/* new-array v4, v0, [Ljava/lang/Object; */
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v3,v2,v4 );
/* check-cast v2, Landroid/graphics/Rect; */
/* .line 297 */
/* .local v2, "imeBounds":Landroid/graphics/Rect; */
if ( v1 != null) { // if-eqz v1, :cond_2
v3 = (( android.graphics.Rect ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z
if ( v3 != null) { // if-eqz v3, :cond_2
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 298 */
v3 = (( android.graphics.Rect ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v3, :cond_2 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
/* nop */
/* .line 300 */
/* .local v0, "needToClearImeBounds":Z */
} // :goto_0
v3 = (( com.android.server.wm.MiuiSplitInputMethodImpl ) p0 ).isSplitIMESupport ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z
/* if-nez v3, :cond_3 */
v3 = (( com.android.server.wm.MiuiSplitInputMethodImpl ) p0 ).isSplitIMESupport ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z
/* if-nez v3, :cond_4 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 302 */
} // :cond_3
(( com.android.server.wm.DisplayArea$Tokens ) p2 ).getResolvedOverrideConfiguration ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayArea$Tokens;->getResolvedOverrideConfiguration()Landroid/content/res/Configuration;
(( android.content.res.Configuration ) v3 ).setTo ( p1 ); // invoke-virtual {v3, p1}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V
/* .line 304 */
} // :cond_4
return;
/* .line 293 */
} // .end local v0 # "needToClearImeBounds":Z
} // .end local v1 # "parentImeBounds":Landroid/graphics/Rect;
} // .end local v2 # "imeBounds":Landroid/graphics/Rect;
} // :cond_5
} // :goto_1
return;
} // .end method
public Boolean shouldAdjustCutoutForSplitIme ( com.android.server.wm.WindowState p0 ) {
/* .locals 7 */
/* .param p1, "windowState" # Lcom/android/server/wm/WindowState; */
/* .line 238 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 239 */
} // :cond_0
(( com.android.server.wm.WindowState ) p1 ).getConfiguration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;
v1 = (( com.android.server.wm.MiuiSplitInputMethodImpl ) p0 ).isSplitIMESupport ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z
/* if-nez v1, :cond_1 */
/* .line 240 */
/* .line 242 */
} // :cond_1
v1 = this.mWmService;
/* .line 243 */
(( com.android.server.wm.WindowManagerService ) v1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v1 ).getImeTarget ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/DisplayContent;->getImeTarget(I)Lcom/android/server/wm/InsetsControlTarget;
/* .line 245 */
/* .local v1, "imeTarget":Lcom/android/server/wm/InsetsControlTarget; */
if ( v1 != null) { // if-eqz v1, :cond_9
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
/* .line 246 */
v2 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v2 ).isInSystemSplitScreen ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
/* if-nez v2, :cond_2 */
/* .line 249 */
} // :cond_2
v2 = (( com.android.server.wm.WindowState ) p1 ).getWindowType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* const/16 v3, 0x7dc */
/* const/16 v4, 0x7db */
int v5 = 1; // const/4 v5, 0x1
/* if-eq v2, v4, :cond_4 */
/* .line 250 */
v2 = (( com.android.server.wm.WindowState ) p1 ).getWindowType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getWindowType()I
/* if-ne v2, v3, :cond_3 */
} // :cond_3
/* move v2, v0 */
} // :cond_4
} // :goto_0
/* move v2, v5 */
/* .line 251 */
/* .local v2, "isImeWindow":Z */
} // :goto_1
(( com.android.server.wm.WindowState ) p1 ).getParent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 252 */
(( com.android.server.wm.WindowState ) p1 ).getParent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;
/* instance-of v6, v6, Lcom/android/server/wm/WindowState; */
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 253 */
(( com.android.server.wm.WindowState ) p1 ).getParent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;
v6 = (( com.android.server.wm.WindowContainer ) v6 ).getWindowType ( ); // invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->getWindowType()I
/* if-eq v6, v4, :cond_5 */
/* .line 254 */
(( com.android.server.wm.WindowState ) p1 ).getParent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getParent()Lcom/android/server/wm/WindowContainer;
v4 = (( com.android.server.wm.WindowContainer ) v4 ).getWindowType ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowContainer;->getWindowType()I
/* if-ne v4, v3, :cond_6 */
} // :cond_5
/* move v3, v5 */
} // :cond_6
/* move v3, v0 */
/* .line 255 */
/* .local v3, "isImeChildWindow":Z */
} // :goto_2
/* if-nez v2, :cond_7 */
/* if-nez v3, :cond_7 */
/* .line 256 */
} // :cond_7
(( com.android.server.wm.WindowState ) p1 ).getConfiguration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getConfiguration()Landroid/content/res/Configuration;
/* iget v4, v4, Landroid/content/res/Configuration;->orientation:I */
int v6 = 2; // const/4 v6, 0x2
/* if-eq v4, v6, :cond_8 */
/* .line 257 */
/* .line 259 */
} // :cond_8
/* .line 247 */
} // .end local v2 # "isImeWindow":Z
} // .end local v3 # "isImeChildWindow":Z
} // :cond_9
} // :goto_3
} // .end method
public void updateImeBoundsInImeTargetChanged ( com.android.server.wm.DisplayContent p0, com.android.server.wm.WindowState p1, com.android.server.wm.WindowState p2 ) {
/* .locals 7 */
/* .param p1, "dc" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "curFocusedWindow" # Lcom/android/server/wm/WindowState; */
/* .param p3, "newFocusedWindow" # Lcom/android/server/wm/WindowState; */
/* .line 176 */
/* if-ne p2, p3, :cond_0 */
return;
/* .line 178 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 179 */
return;
/* .line 181 */
} // :cond_1
(( com.android.server.wm.DisplayContent ) p1 ).getImeContainer ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getImeContainer()Lcom/android/server/wm/DisplayArea$Tokens;
/* .line 182 */
/* .local v0, "imeWindowsContainer":Lcom/android/server/wm/DisplayArea$Tokens; */
/* new-instance v1, Landroid/content/res/Configuration; */
(( com.android.server.wm.DisplayContent ) p1 ).getConfiguration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getConfiguration()Landroid/content/res/Configuration;
/* invoke-direct {v1, v2}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V */
/* .line 183 */
/* .local v1, "configuration":Landroid/content/res/Configuration; */
v2 = (( com.android.server.wm.MiuiSplitInputMethodImpl ) p0 ).isSplitIMESupport ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->isSplitIMESupport(Landroid/content/res/Configuration;)Z
/* if-nez v2, :cond_2 */
/* .line 184 */
return;
/* .line 186 */
} // :cond_2
v2 = this.mTmpBounds;
(( android.graphics.Rect ) v2 ).setEmpty ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V
/* .line 188 */
if ( p3 != null) { // if-eqz p3, :cond_5
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
/* .line 189 */
v2 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v2 ).isInSystemSplitScreen ( p3 ); // invoke-virtual {v2, p3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
/* if-nez v2, :cond_3 */
/* .line 193 */
} // :cond_3
v2 = this.mTmpBounds;
(( com.android.server.wm.WindowState ) p3 ).getBounds ( ); // invoke-virtual {p3}, Lcom/android/server/wm/WindowState;->getBounds()Landroid/graphics/Rect;
(( android.graphics.Rect ) v2 ).set ( v3 ); // invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
/* .line 194 */
v2 = this.mTmpBounds;
v2 = (( android.graphics.Rect ) v2 ).width ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->width()I
/* int-to-float v2, v2 */
/* .line 195 */
(( com.android.server.wm.DisplayContent ) p1 ).getBounds ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getBounds()Landroid/graphics/Rect;
v3 = (( android.graphics.Rect ) v3 ).width ( ); // invoke-virtual {v3}, Landroid/graphics/Rect;->width()I
/* int-to-float v3, v3 */
/* div-float/2addr v2, v3 */
/* .line 197 */
/* .local v2, "scale":F */
v3 = this.mTmpBounds;
int v4 = 0; // const/4 v4, 0x0
/* new-array v4, v4, [Ljava/lang/Object; */
final String v5 = "getmImeAppBounds"; // const-string v5, "getmImeAppBounds"
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v1,v5,v4 );
v3 = (( android.graphics.Rect ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_4 */
/* float-to-double v3, v2 */
/* const-wide/high16 v5, 0x3fe0000000000000L # 0.5 */
/* sub-double/2addr v3, v5 */
/* .line 198 */
java.lang.Math .abs ( v3,v4 );
/* move-result-wide v3 */
/* iget v5, p0, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->mAccuracy:F */
/* float-to-double v5, v5 */
/* cmpg-double v3, v3, v5 */
/* if-gez v3, :cond_4 */
/* .line 199 */
v3 = this.mTmpBounds;
/* filled-new-array {v3}, [Ljava/lang/Object; */
/* const-string/jumbo v4, "updateImeAppBounds" */
com.android.server.wm.MiuiSplitInputMethodImpl .invoke ( v1,v4,v3 );
/* .line 200 */
/* iget v3, v1, Landroid/content/res/Configuration;->seq:I */
/* add-int/lit8 v3, v3, 0x1 */
/* iput v3, v1, Landroid/content/res/Configuration;->seq:I */
/* .line 201 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 202 */
(( com.android.server.wm.DisplayArea$Tokens ) v0 ).onConfigurationChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/DisplayArea$Tokens;->onConfigurationChanged(Landroid/content/res/Configuration;)V
/* .line 205 */
} // :cond_4
return;
/* .line 190 */
} // .end local v2 # "scale":F
} // :cond_5
} // :goto_0
/* invoke-direct {p0, v1, v0}, Lcom/android/server/wm/MiuiSplitInputMethodImpl;->clearImebounds(Landroid/content/res/Configuration;Lcom/android/server/wm/DisplayArea$Tokens;)V */
/* .line 191 */
return;
} // .end method
public void updateImeVisiblityIfNeed ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "visible" # Z */
/* .line 307 */
/* const-class v0, Lcom/android/server/inputmethod/InputMethodManagerInternal; */
/* .line 308 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/inputmethod/InputMethodManagerInternal; */
/* .line 309 */
/* .local v0, "service":Lcom/android/server/inputmethod/InputMethodManagerInternal; */
/* if-nez v0, :cond_0 */
/* .line 310 */
return;
/* .line 312 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 313 */
/* const/16 v1, 0x16 */
(( com.android.server.inputmethod.InputMethodManagerInternal ) v0 ).hideCurrentInputMethod ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerInternal;->hideCurrentInputMethod(I)V
/* .line 317 */
} // :cond_1
return;
} // .end method
