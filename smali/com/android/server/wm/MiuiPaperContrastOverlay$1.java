class com.android.server.wm.MiuiPaperContrastOverlay$1 implements android.hardware.devicestate.DeviceStateManager$DeviceStateCallback {
	 /* .source "MiuiPaperContrastOverlay.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MiuiPaperContrastOverlay;-><init>(Lcom/android/server/wm/WindowManagerService;Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiPaperContrastOverlay this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$qIIL2sVAcckrdwBuMeo8JD6sNB8 ( com.android.server.wm.MiuiPaperContrastOverlay$1 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$1;->lambda$onStateChanged$0()V */
return;
} // .end method
 com.android.server.wm.MiuiPaperContrastOverlay$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiPaperContrastOverlay; */
/* .line 124 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void lambda$onStateChanged$0 ( ) { //synthethic
/* .locals 1 */
/* .line 139 */
v0 = this.this$0;
(( com.android.server.wm.MiuiPaperContrastOverlay ) v0 ).showPaperModeSurface ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiPaperContrastOverlay;->showPaperModeSurface()V
/* .line 140 */
return;
} // .end method
/* # virtual methods */
public void onBaseStateChanged ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "state" # I */
/* .line 133 */
return;
} // .end method
public void onStateChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .line 137 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 v0 = this.this$0;
	 v0 = 	 com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmNeedShowPaperSurface ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 v0 = this.this$0;
		 v0 = 		 com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmFoldDeviceReady ( v0 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 138 */
			 v0 = this.this$0;
			 com.android.server.wm.MiuiPaperContrastOverlay .-$$Nest$fgetmWms ( v0 );
			 v0 = this.mH;
			 /* new-instance v1, Lcom/android/server/wm/MiuiPaperContrastOverlay$1$$ExternalSyntheticLambda0; */
			 /* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiPaperContrastOverlay$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiPaperContrastOverlay$1;)V */
			 (( com.android.server.wm.WindowManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
			 /* .line 142 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void onSupportedStatesChanged ( Integer[] p0 ) {
		 /* .locals 0 */
		 /* .param p1, "supportedStates" # [I */
		 /* .line 128 */
		 return;
	 } // .end method
