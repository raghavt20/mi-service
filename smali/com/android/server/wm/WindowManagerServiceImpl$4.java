class com.android.server.wm.WindowManagerServiceImpl$4 extends android.hardware.camera2.CameraManager$AvailabilityCallback {
	 /* .source "WindowManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/WindowManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.WindowManagerServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.WindowManagerServiceImpl$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/WindowManagerServiceImpl; */
/* .line 484 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCameraAvailable ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 487 */
/* invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraAvailable(Ljava/lang/String;)V */
/* .line 488 */
v0 = java.lang.Integer .parseInt ( p1 );
/* .line 489 */
/* .local v0, "id":I */
/* const/16 v1, 0x64 */
/* if-lt v0, v1, :cond_0 */
/* .line 490 */
return;
/* .line 492 */
} // :cond_0
v1 = this.this$0;
com.android.server.wm.WindowManagerServiceImpl .-$$Nest$fgetmOpeningCameraID ( v1 );
java.lang.Integer .valueOf ( v0 );
/* .line 493 */
v1 = this.this$0;
v1 = com.android.server.wm.WindowManagerServiceImpl .-$$Nest$fgetmOpeningCameraID ( v1 );
/* if-nez v1, :cond_1 */
v1 = this.this$0;
v1 = this.mDisplayContent;
/* .line 499 */
} // :cond_1
return;
} // .end method
public void onCameraUnavailable ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 503 */
/* invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraUnavailable(Ljava/lang/String;)V */
/* .line 504 */
v0 = java.lang.Integer .parseInt ( p1 );
/* .line 505 */
/* .local v0, "id":I */
/* const/16 v1, 0x64 */
/* if-lt v0, v1, :cond_0 */
/* .line 506 */
return;
/* .line 508 */
} // :cond_0
v1 = this.this$0;
com.android.server.wm.WindowManagerServiceImpl .-$$Nest$fgetmOpeningCameraID ( v1 );
java.lang.Integer .valueOf ( v0 );
/* .line 509 */
return;
} // .end method
