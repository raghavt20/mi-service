public class com.android.server.wm.MiuiDragAndDropStubImpl implements com.android.server.wm.MiuiDragAndDropStub {
	 /* .source "MiuiDragAndDropStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private com.miui.app.MiuiFreeDragServiceInternal mMFDSInternal;
/* # direct methods */
 com.android.server.wm.MiuiDragAndDropStubImpl ( ) {
	 /* .locals 1 */
	 /* .line 26 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 27 */
	 /* const-class v0, Lcom/miui/app/MiuiFreeDragServiceInternal; */
	 com.android.server.LocalServices .getService ( v0 );
	 /* check-cast v0, Lcom/miui/app/MiuiFreeDragServiceInternal; */
	 this.mMFDSInternal = v0;
	 /* .line 28 */
	 return;
} // .end method
/* # virtual methods */
public void checkMiuiDragAndDropMetaData ( com.android.server.wm.ActivityRecord p0 ) {
	 /* .locals 5 */
	 /* .param p1, "ar" # Lcom/android/server/wm/ActivityRecord; */
	 /* .line 31 */
	 v0 = this.intent;
	 v1 = this.mAtmService;
	 v1 = this.mContext;
	 /* .line 32 */
	 (( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 /* .line 31 */
	 /* const/16 v2, 0x80 */
	 (( android.content.Intent ) v0 ).resolveActivityInfo ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;
	 /* .line 34 */
	 /* .local v0, "infoWithMetaData":Landroid/content/pm/ActivityInfo; */
	 /* if-nez v0, :cond_0 */
	 int v1 = 0; // const/4 v1, 0x0
} // :cond_0
v1 = this.metaData;
/* .line 35 */
/* .local v1, "metaData":Landroid/os/Bundle; */
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 36 */
final String v3 = "miui.hasDragAndDropFeature"; // const-string v3, "miui.hasDragAndDropFeature"
v3 = (( android.os.Bundle ) v1 ).getBoolean ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
if ( v3 != null) { // if-eqz v3, :cond_1
	 int v2 = 1; // const/4 v2, 0x1
} // :cond_1
/* nop */
} // :goto_1
/* iput-boolean v2, p1, Lcom/android/server/wm/ActivityRecord;->mHasDragAndDropFeature:Z */
/* .line 37 */
/* iget-boolean v2, p1, Lcom/android/server/wm/ActivityRecord;->mHasDragAndDropFeature:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 38 */
v2 = this.intent;
if ( v2 != null) { // if-eqz v2, :cond_2
	 v2 = this.intent;
	 (( android.content.Intent ) v2 ).getComponent ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
	 if ( v2 != null) { // if-eqz v2, :cond_2
		 /* .line 39 */
		 v2 = this.mMFDSInternal;
		 if ( v2 != null) { // if-eqz v2, :cond_2
			 /* .line 40 */
			 v3 = this.packageName;
			 v4 = this.intent;
			 /* .line 41 */
			 (( android.content.Intent ) v4 ).getComponent ( ); // invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
			 (( android.content.ComponentName ) v4 ).getClassName ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
			 /* .line 40 */
			 /* .line 45 */
		 } // :cond_2
		 return;
	 } // .end method
	 public Boolean getDragNotAllowPackages ( com.android.server.wm.WindowState p0 ) {
		 /* .locals 4 */
		 /* .param p1, "callingWin" # Lcom/android/server/wm/WindowState; */
		 /* .line 53 */
		 com.android.server.MiuiCommonCloudServiceStub .getInstance ( );
		 final String v1 = "drag_not_allow_packages"; // const-string v1, "drag_not_allow_packages"
		 (( com.android.server.MiuiCommonCloudServiceStub ) v0 ).getDataByModuleName ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudServiceStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages; */
		 /* .line 54 */
		 /* .local v0, "packages":Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages; */
		 (( com.android.server.wm.MiuiDragAndDropStubImpl$DragNotAllowPackages ) v0 ).getPackages ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiDragAndDropStubImpl$DragNotAllowPackages;->getPackages()Landroid/util/ArraySet;
		 /* .line 55 */
		 /* .local v1, "dragNotAllowPackages":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 56 */
			 (( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
			 v2 = 			 (( android.util.ArraySet ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
			 if ( v2 != null) { // if-eqz v2, :cond_0
				 /* .line 57 */
				 /* new-instance v2, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v3 = "Not allow dragging, package: "; // const-string v3, "Not allow dragging, package: "
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 /* .line 58 */
				 (( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 /* .line 57 */
				 final String v3 = "MiuiDragAndDropStubImpl"; // const-string v3, "MiuiDragAndDropStubImpl"
				 android.util.Slog .w ( v3,v2 );
				 /* .line 59 */
				 int v2 = 1; // const/4 v2, 0x1
				 /* .line 61 */
			 } // :cond_0
			 int v2 = 0; // const/4 v2, 0x0
		 } // .end method
		 public Boolean isMiuiFreeForm ( com.android.server.wm.WindowState p0, com.android.server.wm.WindowManagerService p1 ) {
			 /* .locals 3 */
			 /* .param p1, "callingWin" # Lcom/android/server/wm/WindowState; */
			 /* .param p2, "mWmService" # Lcom/android/server/wm/WindowManagerService; */
			 /* .line 65 */
			 (( com.android.server.wm.WindowState ) p1 ).getActivityRecord ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getActivityRecord()Lcom/android/server/wm/ActivityRecord;
			 /* .line 66 */
			 /* .local v0, "ar":Lcom/android/server/wm/ActivityRecord; */
			 /* if-nez v0, :cond_0 */
			 /* .line 67 */
			 int v1 = 0; // const/4 v1, 0x0
			 /* .line 69 */
		 } // :cond_0
		 v1 = 		 (( com.android.server.wm.ActivityRecord ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
		 /* .line 70 */
		 /* .local v1, "arId":I */
		 v2 = this.mAtmService;
		 v2 = 		 v2 = this.mMiuiFreeFormManagerService;
		 /* .line 71 */
		 /* .local v2, "result":Z */
	 } // .end method
	 public void notifyDragDropResultToOneTrack ( com.android.server.wm.WindowState p0, com.android.server.wm.WindowState p1, Boolean p2, android.content.ClipData p3 ) {
		 /* .locals 1 */
		 /* .param p1, "dragWindow" # Lcom/android/server/wm/WindowState; */
		 /* .param p2, "dropWindow" # Lcom/android/server/wm/WindowState; */
		 /* .param p3, "result" # Z */
		 /* .param p4, "clipData" # Landroid/content/ClipData; */
		 /* .line 48 */
		 com.android.server.wm.OneTrackDragDropHelper .getInstance ( );
		 (( com.android.server.wm.OneTrackDragDropHelper ) v0 ).notifyDragDropResult ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/OneTrackDragDropHelper;->notifyDragDropResult(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;ZLandroid/content/ClipData;)V
		 /* .line 50 */
		 return;
	 } // .end method
