class com.android.server.wm.MiuiOrientationImpl$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiOrientationImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiOrientationImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
android.content.ContentResolver resolver;
final com.android.server.wm.MiuiOrientationImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiOrientationImpl$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 208 */
this.this$0 = p1;
/* .line 209 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 210 */
com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmContext ( p1 );
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.resolver = p1;
/* .line 211 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 5 */
/* .line 214 */
v0 = this.resolver;
final String v1 = "miui_smart_rotation"; // const-string v1, "miui_smart_rotation"
android.provider.Settings$System .getUriFor ( v1 );
int v3 = -1; // const/4 v3, -0x1
int v4 = 0; // const/4 v4, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, p0, v3 ); // invoke-virtual {v0, v2, v4, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 216 */
android.provider.Settings$System .getUriFor ( v1 );
(( com.android.server.wm.MiuiOrientationImpl$MiuiSettingsObserver ) p0 ).onChange ( v4, v0 ); // invoke-virtual {p0, v4, v0}, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 217 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 221 */
final String v0 = "miui_smart_rotation"; // const-string v0, "miui_smart_rotation"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 222 */
	 v1 = this.resolver;
	 v2 = 	 com.android.server.wm.MiuiOrientationImpl .-$$Nest$sfgetENABLED ( );
	 int v3 = -2; // const/4 v3, -0x2
	 v0 = 	 android.provider.MiuiSettings$System .getBooleanForUser ( v1,v0,v2,v3 );
	 /* .line 224 */
	 /* .local v0, "isEnabled":Z */
	 v1 = this.this$0;
	 v2 = 	 com.android.server.wm.MiuiOrientationImpl .-$$Nest$sfgetENABLED ( );
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 v2 = 		 com.android.server.wm.MiuiOrientationImpl .-$$Nest$sfgetIS_MIUI_OPTIMIZATION ( );
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 int v2 = 1; // const/4 v2, 0x1
			 } // :cond_0
			 int v2 = 0; // const/4 v2, 0x0
		 } // :goto_0
		 com.android.server.wm.MiuiOrientationImpl .-$$Nest$fputmSmartRotationEnabled ( v1,v2 );
		 /* .line 225 */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "onChanged "; // const-string v2, "onChanged "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = this.this$0;
		 v2 = 		 com.android.server.wm.MiuiOrientationImpl .-$$Nest$fgetmSmartRotationEnabled ( v2 );
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v2 = "MiuiOrientationImpl"; // const-string v2, "MiuiOrientationImpl"
		 android.util.Slog .d ( v2,v1 );
		 /* .line 227 */
	 } // .end local v0 # "isEnabled":Z
} // :cond_1
return;
} // .end method
