.class Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;
.super Ljava/lang/Object;
.source "ActivityTaskManagerServiceImpl.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->animationOut()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

.field final synthetic val$t:Landroid/view/SurfaceControl$Transaction;


# direct methods
.method constructor <init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/view/SurfaceControl$Transaction;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1436
    iput-object p1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    iput-object p2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->val$t:Landroid/view/SurfaceControl$Transaction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .line 1451
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    invoke-static {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->-$$Nest$fgetmScreenshotLayer(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)Landroid/view/SurfaceControl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1452
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->val$t:Landroid/view/SurfaceControl$Transaction;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->-$$Nest$fgetmScreenshotLayer(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)Landroid/view/SurfaceControl;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 1453
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->-$$Nest$fputmScreenshotLayer(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/view/SurfaceControl;)V

    .line 1455
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->-$$Nest$fputmInAnimationOut(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Z)V

    .line 1456
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .line 1442
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    invoke-static {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->-$$Nest$fgetmScreenshotLayer(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)Landroid/view/SurfaceControl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1443
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->val$t:Landroid/view/SurfaceControl$Transaction;

    iget-object v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->-$$Nest$fgetmScreenshotLayer(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)Landroid/view/SurfaceControl;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 1444
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->-$$Nest$fputmScreenshotLayer(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/view/SurfaceControl;)V

    .line 1446
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->-$$Nest$fputmInAnimationOut(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Z)V

    .line 1447
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .line 1459
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .line 1438
    return-void
.end method
