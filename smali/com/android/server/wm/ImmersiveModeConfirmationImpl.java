class com.android.server.wm.ImmersiveModeConfirmationImpl implements com.android.server.wm.ImmersiveModeConfirmationStub {
	 /* .source "ImmersiveModeConfirmationImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String FACTORY_BUILD_SIGN;
	 private static final java.lang.String IS_FACTORY_BUILD;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static com.android.server.wm.ImmersiveModeConfirmationImpl ( ) {
		 /* .locals 2 */
		 /* .line 33 */
		 final String v0 = "ro.boot.factorybuild"; // const-string v0, "ro.boot.factorybuild"
		 final String v1 = "0"; // const-string v1, "0"
		 android.os.SystemProperties .get ( v0,v1 );
		 return;
	 } // .end method
	 com.android.server.wm.ImmersiveModeConfirmationImpl ( ) {
		 /* .locals 0 */
		 /* .line 27 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean disableImmersiveConfirmation ( java.lang.String p0 ) {
		 /* .locals 1 */
		 /* .param p1, "pkg" # Ljava/lang/String; */
		 /* .line 42 */
		 v0 = 		 com.android.server.wm.PolicyControl .disableImmersiveConfirmation ( p1 );
	 } // .end method
	 public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
		 /* .locals 0 */
		 /* .param p1, "prefix" # Ljava/lang/String; */
		 /* .param p2, "pw" # Ljava/io/PrintWriter; */
		 /* .line 47 */
		 com.android.server.wm.PolicyControl .dump ( p1,p2 );
		 /* .line 48 */
		 return;
	 } // .end method
	 public void handleMessageShow ( com.android.server.wm.ImmersiveModeConfirmation p0, Integer p1, Boolean p2 ) {
		 /* .locals 3 */
		 /* .param p1, "immersiveModeConfirmation" # Lcom/android/server/wm/ImmersiveModeConfirmation; */
		 /* .param p2, "what" # I */
		 /* .param p3, "isDebug" # Z */
		 /* .line 54 */
		 v0 = com.android.server.wm.ImmersiveModeConfirmationImpl.IS_FACTORY_BUILD;
		 v1 = 		 android.text.TextUtils .isEmpty ( v0 );
		 final String v2 = "ImmersiveModeConfirmation"; // const-string v2, "ImmersiveModeConfirmation"
		 /* if-nez v1, :cond_1 */
		 final String v1 = "1"; // const-string v1, "1"
		 v0 = 		 android.text.TextUtils .equals ( v0,v1 );
		 /* if-nez v0, :cond_0 */
		 /* .line 60 */
	 } // :cond_0
	 if ( p3 != null) { // if-eqz p3, :cond_3
		 /* .line 61 */
		 final String v0 = "factory build not show immersive mode confirmation"; // const-string v0, "factory build not show immersive mode confirmation"
		 android.util.Slog .d ( v2,v0 );
		 /* .line 55 */
	 } // :cond_1
} // :goto_0
if ( p3 != null) { // if-eqz p3, :cond_2
	 /* .line 56 */
	 final String v0 = "Not factory build"; // const-string v0, "Not factory build"
	 android.util.Slog .d ( v2,v0 );
	 /* .line 58 */
} // :cond_2
v0 = com.android.server.wm.ImmersiveModeConfirmationProxy.handleShow;
java.lang.Integer .valueOf ( p2 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
(( com.xiaomi.reflect.RefMethod ) v0 ).invoke ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 64 */
} // :cond_3
} // :goto_1
return;
} // .end method
public Boolean reloadFromSetting ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "cxt" # Landroid/content/Context; */
/* .line 37 */
v0 = com.android.server.wm.PolicyControl .reloadFromSetting ( p1 );
} // .end method
