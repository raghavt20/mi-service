class com.android.server.wm.MiuiFreezeImpl$DialogShowListener implements miuix.appcompat.app.AlertDialog$OnDialogShowAnimListener {
	 /* .source "MiuiFreezeImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiFreezeImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DialogShowListener" */
} // .end annotation
/* # instance fields */
private Boolean isNeedDismissDialog;
final com.android.server.wm.MiuiFreezeImpl this$0; //synthetic
/* # direct methods */
private com.android.server.wm.MiuiFreezeImpl$DialogShowListener ( ) {
/* .locals 0 */
/* .line 497 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 499 */
int p1 = 0; // const/4 p1, 0x0
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->isNeedDismissDialog:Z */
return;
} // .end method
 com.android.server.wm.MiuiFreezeImpl$DialogShowListener ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
return;
} // .end method
/* # virtual methods */
public void onShowAnimComplete ( ) {
/* .locals 2 */
/* .line 507 */
final String v0 = "MiuiFreezeImpl"; // const-string v0, "MiuiFreezeImpl"
final String v1 = "onShowAnimComplete"; // const-string v1, "onShowAnimComplete"
android.util.Log .d ( v0,v1 );
/* .line 508 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.wm.MiuiFreezeImpl .-$$Nest$fputmDialogShowing ( v0,v1 );
/* .line 510 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->isNeedDismissDialog:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 511 */
	 int v0 = 0; // const/4 v0, 0x0
	 (( com.android.server.wm.MiuiFreezeImpl$DialogShowListener ) p0 ).setNeedDismissDialog ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->setNeedDismissDialog(Z)V
	 /* .line 512 */
	 v0 = this.this$0;
	 com.android.server.wm.MiuiFreezeImpl .-$$Nest$mdismissDialog ( v0 );
	 /* .line 514 */
} // :cond_0
return;
} // .end method
public void onShowAnimStart ( ) {
/* .locals 0 */
/* .line 518 */
return;
} // .end method
void setNeedDismissDialog ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "needDismissDialog" # Z */
/* .line 502 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiFreezeImpl$DialogShowListener;->isNeedDismissDialog:Z */
/* .line 503 */
return;
} // .end method
