public class com.android.server.wm.MiuiWindowMonitorImpl extends com.android.server.wm.MiuiWindowMonitorStub {
	 /* .source "MiuiWindowMonitorImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;, */
	 /* Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;, */
	 /* Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID;
private static final java.lang.String CONTENT;
private static final Integer DEFAULT_EXIT_REPORT_THRESHOLD;
private static final Integer DEFAULT_KILL_THRESHOLD;
private static final Integer DEFAULT_REPORT_INTERVAL;
private static final Integer DEFAULT_TOTAL_WINDOW_THRESHOLD;
private static final Integer DEFAULT_WARN_INTERVAL;
private static final java.lang.String EVENT_NAME;
private static final java.lang.String EXIT_REPORT_THRESHOLD_PROPERTY;
private static final java.lang.String EXTRA_APP_ID;
private static final java.lang.String EXTRA_EVENT_NAME;
private static final java.lang.String EXTRA_PACKAGE_NAME;
private static final Integer FLAG_MQS_REPORT;
private static final Integer FLAG_NON_ANONYMOUS;
private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
private static final Integer FLAG_ONETRACK_REPORT;
private static final java.util.List FOREGROUND_PERSISTENT_APPS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String INTENT_ACTION_ONETRACK;
private static final java.lang.String INTENT_PACKAGE_ONETRACK;
private static final java.lang.String KEY_TOP_TYPE_WINDOWS;
private static final java.lang.String KILL_REASON;
private static final java.lang.String KILL_THRESHOLD_PROPERTY;
private static final java.lang.String KILL_WHEN_LEAK_PROPERTY;
private static final Integer LEAK_TYPE_APPLICATION;
private static final Integer LEAK_TYPE_NONE;
private static final Integer LEAK_TYPE_SYSTEM;
private static final java.lang.String PACKAGE;
private static final java.lang.String REPORT_INTERVAL_PROPERTY;
private static final java.lang.String SWITCH_PROPERTY;
private static final java.lang.String TAG;
private static final java.lang.String TOTAL_WINDOW_THRESHOLD_PROPERTY;
private static final java.lang.String WARN_INTERVAL_PROPERTY;
/* # instance fields */
private android.content.Context mContext;
private android.os.Handler mHandler;
private android.os.HandlerThread mMonitorThread;
private com.android.server.wm.WindowManagerService mService;
private final java.util.HashMap mWindowsByApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;", */
/* "Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mWindowsByToken;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Landroid/os/IBinder;", */
/* "Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.util.HashMap -$$Nest$fgetmWindowsByApp ( com.android.server.wm.MiuiWindowMonitorImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWindowsByApp;
} // .end method
static void -$$Nest$maddWindowInfo ( com.android.server.wm.MiuiWindowMonitorImpl p0, Integer p1, java.lang.String p2, android.os.IBinder p3, java.lang.String p4, Integer p5 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->addWindowInfo(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)V */
return;
} // .end method
static Boolean -$$Nest$misEnabled ( com.android.server.wm.MiuiWindowMonitorImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->isEnabled()Z */
} // .end method
static void -$$Nest$mkillApp ( com.android.server.wm.MiuiWindowMonitorImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->killApp(I)V */
return;
} // .end method
static com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList -$$Nest$mremoveWindowInfo ( com.android.server.wm.MiuiWindowMonitorImpl p0, com.android.server.wm.MiuiWindowMonitorImpl$ProcessKey p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->removeWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
} // .end method
static void -$$Nest$mremoveWindowInfo ( com.android.server.wm.MiuiWindowMonitorImpl p0, android.os.IBinder p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->removeWindowInfo(Landroid/os/IBinder;)V */
return;
} // .end method
static void -$$Nest$mreportIfNeeded ( com.android.server.wm.MiuiWindowMonitorImpl p0, Integer p1, com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V */
return;
} // .end method
static void -$$Nest$mreportWindowInfo ( com.android.server.wm.MiuiWindowMonitorImpl p0, Integer p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, Integer p5 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportWindowInfo(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V */
return;
} // .end method
static com.android.server.wm.MiuiWindowMonitorImpl ( ) {
/* .locals 4 */
/* .line 91 */
final String v0 = "com.android.nfc"; // const-string v0, "com.android.nfc"
final String v1 = "com.miui.voicetrigger"; // const-string v1, "com.miui.voicetrigger"
final String v2 = "com.android.systemui"; // const-string v2, "com.android.systemui"
final String v3 = "com.android.phone"; // const-string v3, "com.android.phone"
/* filled-new-array {v2, v3, v0, v1}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
return;
} // .end method
public com.android.server.wm.MiuiWindowMonitorImpl ( ) {
/* .locals 1 */
/* .line 39 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorStub;-><init>()V */
/* .line 99 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mWindowsByToken = v0;
/* .line 102 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mWindowsByApp = v0;
return;
} // .end method
private synchronized void addWindowInfo ( Integer p0, java.lang.String p1, android.os.IBinder p2, java.lang.String p3, Integer p4 ) {
/* .locals 8 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "token" # Landroid/os/IBinder; */
/* .param p4, "windowName" # Ljava/lang/String; */
/* .param p5, "type" # I */
/* monitor-enter p0 */
/* .line 150 */
try { // :try_start_0
/* new-instance v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
/* invoke-direct {v0, p1, p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;-><init>(ILjava/lang/String;)V */
/* .line 151 */
/* .local v0, "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
/* new-instance v7, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo; */
/* move-object v1, v7 */
/* move v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move v6, p5 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;-><init>(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)V */
/* move-object v1, v7 */
/* .line 152 */
/* .local v1, "info":Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo; */
v2 = this.mWindowsByToken;
(( java.util.HashMap ) v2 ).put ( p3, v1 ); // invoke-virtual {v2, p3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 153 */
v2 = this.mWindowsByApp;
(( java.util.HashMap ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 154 */
/* .local v2, "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* if-nez v2, :cond_0 */
/* .line 155 */
/* new-instance v3, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* invoke-direct {v3, p1, p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;-><init>(ILjava/lang/String;)V */
/* move-object v2, v3 */
/* .line 156 */
v3 = this.mWindowsByApp;
(( java.util.HashMap ) v3 ).put ( v0, v2 ); // invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 158 */
} // .end local p0 # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl;
} // :cond_0
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v2 ).put ( p3, v1 ); // invoke-virtual {v2, p3, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->put(Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;)V
/* .line 162 */
v3 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->isEnabled()Z */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v3, :cond_1 */
/* .line 163 */
/* monitor-exit p0 */
return;
/* .line 165 */
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl; */
} // :cond_1
try { // :try_start_1
/* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->checkProcess(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 166 */
/* monitor-exit p0 */
return;
/* .line 149 */
} // .end local v0 # "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
} // .end local v1 # "info":Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;
} // .end local v2 # "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
} // .end local p0 # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl;
} // .end local p1 # "pid":I
} // .end local p2 # "packageName":Ljava/lang/String;
} // .end local p3 # "token":Landroid/os/IBinder;
} // .end local p4 # "windowName":Ljava/lang/String;
} // .end local p5 # "type":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList anyProcessLeak ( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList p0 ) {
/* .locals 3 */
/* .param p1, "processWindows" # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 479 */
v0 = this.mWindowsByToken;
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getTotalWindowThreshold()I */
/* if-lt v0, v1, :cond_0 */
/* .line 480 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->selectTopProcess()Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 481 */
/* .local v0, "most":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 482 */
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v0 ).setLeakType ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLeakType(I)V
/* .line 483 */
/* .line 488 */
} // .end local v0 # "most":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_1 */
/* .line 489 */
/* .line 492 */
} // :cond_1
v1 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p1 ).getCurrentCount ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
v2 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getKillThreshold()I */
/* if-lt v1, v2, :cond_2 */
/* .line 493 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p1 ).setLeakType ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLeakType(I)V
/* .line 494 */
/* .line 496 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p1 ).setLeakType ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLeakType(I)V
/* .line 497 */
} // .end method
private void checkProcess ( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList p0 ) {
/* .locals 2 */
/* .param p1, "processWindows" # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 171 */
v0 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p1 ).warned ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->warned()Z
/* if-nez v0, :cond_3 */
v0 = this.mWindowsByApp;
v0 = (( java.util.HashMap ) v0 ).containsValue ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 177 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->anyProcessLeak(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 178 */
/* .local v0, "leakProcess":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 179 */
v1 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPid()I
/* invoke-direct {p0, v1, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->dealWithLeakProcess(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;)Z */
/* .line 180 */
return;
/* .line 184 */
} // :cond_1
v1 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p1 ).warned ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->warned()Z
/* if-nez v1, :cond_2 */
/* .line 185 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p1 ).setWarned ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V
/* .line 186 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p1 ).setWillKill ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V
/* .line 188 */
} // :cond_2
return;
/* .line 172 */
} // .end local v0 # "leakProcess":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
} // :cond_3
} // :goto_0
return;
} // .end method
private Boolean dealWithLeakProcess ( Integer p0, com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList p1 ) {
/* .locals 7 */
/* .param p1, "pid" # I */
/* .param p2, "processWindows" # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 192 */
v0 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).getPid ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPid()I
v1 = android.os.Process .myPid ( );
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_0 */
/* .line 193 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setWarned ( v2 ); // invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V
/* .line 194 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setWillKill ( v2 ); // invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V
/* .line 195 */
/* invoke-direct {p0, p1, p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V */
/* .line 196 */
/* .line 200 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->killByPolicy()Z */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
v0 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).getLeakType ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLeakType()I
/* if-ne v0, v1, :cond_1 */
/* .line 201 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setWarned ( v2 ); // invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V
/* .line 202 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setWillKill ( v2 ); // invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V
/* .line 203 */
/* invoke-direct {p0, p1, p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V */
/* .line 204 */
/* .line 207 */
} // :cond_1
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPackageName()Ljava/lang/String;
/* .line 208 */
/* .local v0, "pkgName":Ljava/lang/String; */
v3 = this.mContext;
v3 = com.android.server.am.ProcessUtils .isPersistent ( v3,v0 );
/* .line 209 */
/* .local v3, "persistent":Z */
v4 = com.android.server.am.ProcessUtils .getCurAdjByPid ( p1 );
/* const/16 v5, 0xc8 */
/* if-gt v4, v5, :cond_2 */
if ( v3 != null) { // if-eqz v3, :cond_3
} // :cond_2
v4 = com.android.server.wm.MiuiWindowMonitorImpl.FOREGROUND_PERSISTENT_APPS;
v4 = /* .line 210 */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 212 */
} // :cond_3
v4 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).getCurrentCount ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
/* .line 214 */
/* .local v4, "curCount":I */
v5 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).getLastWarnCount ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLastWarnCount()I
/* sub-int v5, v4, v5 */
v6 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getWarnInterval()I */
/* if-lt v5, v6, :cond_4 */
/* .line 215 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setWarned ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V
/* .line 216 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setLastWarnCount ( v4 ); // invoke-virtual {p2, v4}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLastWarnCount(I)V
/* .line 217 */
/* invoke-direct {p0, p2, v0, v3}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->showKillAppDialog(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Ljava/lang/String;Z)V */
/* .line 219 */
} // :cond_4
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setWarned ( v2 ); // invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V
/* .line 220 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setWillKill ( v2 ); // invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V
/* .line 221 */
/* invoke-direct {p0, p1, p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V */
/* .line 223 */
} // :goto_0
/* .line 227 */
} // .end local v4 # "curCount":I
} // :cond_5
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setWarned ( v2 ); // invoke-virtual {p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWarned(Z)V
/* .line 228 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setWillKill ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setWillKill(Z)V
/* .line 229 */
/* invoke-direct {p0, p1, p2, v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->reportIfNeeded(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)V */
/* .line 230 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->killApp(I)V */
/* .line 231 */
} // .end method
private void doMqsReport ( Integer p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "details" # Ljava/lang/String; */
/* .param p4, "topTypeWindows" # Ljava/lang/String; */
/* .line 554 */
/* new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V */
/* .line 555 */
/* .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* const/16 v1, 0x1a7 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setType ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 556 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPid ( p1 ); // invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V
/* .line 557 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPackageName ( p2 ); // invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V
/* .line 558 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setTimeStamp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V
/* .line 559 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "window leaked! package=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", pid="; // const-string v2, ", pid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setSummary ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V
/* .line 560 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setDetails ( p3 ); // invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 561 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).getBundle ( ); // invoke-virtual {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->getBundle()Landroid/os/Bundle;
/* .line 562 */
/* .local v1, "bundle":Landroid/os/Bundle; */
final String v2 = "key_top_type_windows"; // const-string v2, "key_top_type_windows"
(( android.os.Bundle ) v1 ).putString ( v2, p4 ); // invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 563 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v2 ).reportGeneralException ( v0 ); // invoke-virtual {v2, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z
/* .line 564 */
return;
} // .end method
private void doOneTrackReport ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "content" # Ljava/lang/String; */
/* .line 538 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 539 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 540 */
final String v1 = "APP_ID"; // const-string v1, "APP_ID"
final String v2 = "31000401516"; // const-string v2, "31000401516"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 541 */
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
final String v2 = "app_resource_info"; // const-string v2, "app_resource_info"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 542 */
final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
final String v2 = "android"; // const-string v2, "android"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 543 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 544 */
/* .local v1, "params":Landroid/os/Bundle; */
final String v2 = "content"; // const-string v2, "content"
(( android.os.Bundle ) v1 ).putString ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 545 */
(( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 546 */
int v2 = 3; // const/4 v2, 0x3
(( android.content.Intent ) v0 ).setFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 547 */
v2 = this.mContext;
v3 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v2 ).startServiceAsUser ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 550 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v1 # "params":Landroid/os/Bundle;
/* .line 548 */
/* :catch_0 */
/* move-exception v0 */
/* .line 549 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 551 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Integer getExitReportThreshold ( ) {
/* .locals 2 */
/* .line 391 */
final String v0 = "persist.sys.stability.window_monitor.exit_report_threshold"; // const-string v0, "persist.sys.stability.window_monitor.exit_report_threshold"
/* const/16 v1, 0x32 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
} // .end method
private Integer getKillThreshold ( ) {
/* .locals 2 */
/* .line 387 */
final String v0 = "persist.sys.stability.window_monitor.kill_threshold"; // const-string v0, "persist.sys.stability.window_monitor.kill_threshold"
/* const/16 v1, 0xc8 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
} // .end method
private Integer getReportInterval ( ) {
/* .locals 2 */
/* .line 396 */
final String v0 = "persist.sys.stability.window_monitor.report_interval"; // const-string v0, "persist.sys.stability.window_monitor.report_interval"
/* const/16 v1, 0x14 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
} // .end method
private Integer getTotalWindowThreshold ( ) {
/* .locals 2 */
/* .line 404 */
final String v0 = "persist.sys.stability.window_monitor.total_window_threshold"; // const-string v0, "persist.sys.stability.window_monitor.total_window_threshold"
/* const/16 v1, 0xf33 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
} // .end method
private Integer getWarnInterval ( ) {
/* .locals 2 */
/* .line 400 */
final String v0 = "persist.sys.stability.window_monitor.warn_interval"; // const-string v0, "persist.sys.stability.window_monitor.warn_interval"
/* const/16 v1, 0x14 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
} // .end method
private Boolean isEnabled ( ) {
/* .locals 2 */
/* .line 379 */
final String v0 = "persist.sys.stability.window_monitor.enabled"; // const-string v0, "persist.sys.stability.window_monitor.enabled"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
private void killApp ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 236 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "kill proc "; // const-string v1, "kill proc "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " because it creates too many windows!"; // const-string v1, " because it creates too many windows!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiWindowMonitorImpl"; // const-string v1, "MiuiWindowMonitorImpl"
android.util.Slog .w ( v1,v0 );
/* .line 237 */
android.os.Process .killProcess ( p1 );
/* .line 238 */
return;
} // .end method
private Boolean killByPolicy ( ) {
/* .locals 2 */
/* .line 383 */
final String v0 = "persist.sys.stability.window_monitor.kill_when_leak"; // const-string v0, "persist.sys.stability.window_monitor.kill_when_leak"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
private com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList removeWindowInfo ( com.android.server.wm.MiuiWindowMonitorImpl$ProcessKey p0 ) {
/* .locals 10 */
/* .param p1, "key" # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
/* .line 431 */
v0 = this.mWindowsByApp;
(( java.util.HashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 432 */
/* .local v0, "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* if-nez v0, :cond_0 */
/* .line 433 */
int v1 = 0; // const/4 v1, 0x0
/* .line 436 */
} // :cond_0
/* nop */
/* .line 437 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v0 ).getWindows ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getWindows()Landroid/util/SparseArray;
/* .line 438 */
/* .local v1, "windowsByType":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* .local v3, "size":I */
} // :goto_0
/* if-ge v2, v3, :cond_3 */
/* .line 439 */
/* nop */
/* .line 440 */
(( android.util.SparseArray ) v1 ).valueAt ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Ljava/util/HashMap; */
/* .line 441 */
/* .local v4, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;" */
if ( v4 != null) { // if-eqz v4, :cond_2
v5 = (( java.util.HashMap ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->size()I
/* if-lez v5, :cond_2 */
/* .line 442 */
(( java.util.HashMap ) v4 ).values ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_2
/* check-cast v6, Ljava/util/HashMap; */
/* .line 443 */
/* .local v6, "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;" */
if ( v6 != null) { // if-eqz v6, :cond_1
v7 = (( java.util.HashMap ) v6 ).size ( ); // invoke-virtual {v6}, Ljava/util/HashMap;->size()I
/* if-lez v7, :cond_1 */
/* .line 444 */
(( java.util.HashMap ) v6 ).keySet ( ); // invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v8 = } // :goto_2
if ( v8 != null) { // if-eqz v8, :cond_1
/* check-cast v8, Landroid/os/IBinder; */
/* .line 445 */
/* .local v8, "token":Landroid/os/IBinder; */
v9 = this.mWindowsByToken;
(( java.util.HashMap ) v9 ).remove ( v8 ); // invoke-virtual {v9, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 446 */
} // .end local v8 # "token":Landroid/os/IBinder;
/* .line 448 */
} // .end local v6 # "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
} // :cond_1
/* .line 438 */
} // .end local v4 # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
} // :cond_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 452 */
} // .end local v2 # "i":I
} // .end local v3 # "size":I
} // :cond_3
} // .end method
private synchronized void removeWindowInfo ( android.os.IBinder p0 ) {
/* .locals 5 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* monitor-enter p0 */
/* .line 410 */
try { // :try_start_0
v0 = this.mWindowsByToken;
(( java.util.HashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo; */
/* .line 411 */
/* .local v0, "info":Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 412 */
/* new-instance v1, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
/* iget v2, v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPid:I */
v3 = this.mPackageName;
/* invoke-direct {v1, v2, v3}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;-><init>(ILjava/lang/String;)V */
/* .line 413 */
/* .local v1, "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
v2 = this.mWindowsByApp;
(( java.util.HashMap ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 414 */
/* .local v2, "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 415 */
/* iget v3, v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I */
v4 = this.mWindowName;
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v2 ).remove ( v3, v4, p1 ); // invoke-virtual {v2, v3, v4, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->remove(ILjava/lang/String;Landroid/os/IBinder;)V
/* .line 417 */
v3 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v2 ).getCurrentCount ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
/* if-gtz v3, :cond_0 */
/* .line 418 */
v3 = this.mWindowsByApp;
(( java.util.HashMap ) v3 ).remove ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 422 */
} // .end local v1 # "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
} // .end local v2 # "processWindows":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
} // .end local p0 # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 409 */
} // .end local v0 # "info":Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;
} // .end local p1 # "token":Landroid/os/IBinder;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private void reportIfNeeded ( Integer p0, com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList p1, Boolean p2 ) {
/* .locals 16 */
/* .param p1, "pid" # I */
/* .param p2, "processWindows" # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .param p3, "died" # Z */
/* .line 589 */
v7 = /* invoke-direct/range {p0 ..p3}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->shouldReport(ILcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;Z)I */
/* .line 593 */
/* .local v7, "report":I */
if ( p3 != null) { // if-eqz p3, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 594 */
/* new-instance v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
/* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPackageName()Ljava/lang/String; */
/* move/from16 v8, p1 */
/* invoke-direct {v0, v8, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;-><init>(ILjava/lang/String;)V */
/* .line 595 */
/* .local v0, "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
/* move-object/from16 v9, p0 */
/* invoke-direct {v9, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->removeWindowInfo(Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;)Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 593 */
} // .end local v0 # "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;
} // :cond_0
/* move-object/from16 v9, p0 */
/* move/from16 v8, p1 */
/* .line 597 */
} // :goto_0
/* if-nez v7, :cond_1 */
/* .line 598 */
return;
/* .line 601 */
} // :cond_1
/* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->toJson()Lorg/json/JSONObject; */
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 602 */
/* .local v10, "infoStr":Ljava/lang/String; */
v0 = android.text.TextUtils .isEmpty ( v10 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 603 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "app:"; // const-string v1, "app:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->toString()Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", to json string error!"; // const-string v1, ", to json string error!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiWindowMonitorImpl"; // const-string v1, "MiuiWindowMonitorImpl"
android.util.Slog .w ( v1,v0 );
/* .line 604 */
return;
/* .line 607 */
} // :cond_2
/* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getTopTypeWindows()Ljava/lang/String; */
/* .line 610 */
/* .local v11, "topTypeWindows":Ljava/lang/String; */
v12 = /* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPid()I */
/* .line 611 */
/* .local v12, "reportedPid":I */
/* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPackageName()Ljava/lang/String; */
/* .line 612 */
/* .local v13, "reportedPkg":Ljava/lang/String; */
com.android.server.ScoutSystemMonitor .getInstance ( );
(( com.android.server.ScoutSystemMonitor ) v0 ).getSystemWorkerHandler ( ); // invoke-virtual {v0}, Lcom/android/server/ScoutSystemMonitor;->getSystemWorkerHandler()Landroid/os/Handler;
/* .line 613 */
/* .local v14, "handler":Landroid/os/Handler; */
if ( v14 != null) { // if-eqz v14, :cond_3
/* .line 614 */
/* new-instance v15, Lcom/android/server/wm/MiuiWindowMonitorImpl$6; */
/* move-object v0, v15 */
/* move-object/from16 v1, p0 */
/* move v2, v12 */
/* move-object v3, v13 */
/* move-object v4, v10 */
/* move-object v5, v11 */
/* move v6, v7 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/wm/MiuiWindowMonitorImpl$6;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V */
(( android.os.Handler ) v14 ).post ( v15 ); // invoke-virtual {v14, v15}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 621 */
} // :cond_3
return;
} // .end method
private void reportWindowInfo ( Integer p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, Integer p4 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "infoStr" # Ljava/lang/String; */
/* .param p4, "topTypeWindows" # Ljava/lang/String; */
/* .param p5, "report" # I */
/* .line 569 */
/* and-int/lit8 v0, p5, 0x1 */
final String v1 = "MiuiWindowMonitorImpl"; // const-string v1, "MiuiWindowMonitorImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 570 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "doOneTrackReport:"; // const-string v2, "doOneTrackReport:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 571 */
/* invoke-direct {p0, p3}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->doOneTrackReport(Ljava/lang/String;)V */
/* .line 575 */
} // :cond_0
/* and-int/lit8 v0, p5, 0x2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 576 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "doMqsReport:"; // const-string v2, "doMqsReport:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 577 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->doMqsReport(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 579 */
} // :cond_1
return;
} // .end method
private com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList selectTopProcess ( ) {
/* .locals 5 */
/* .line 460 */
int v0 = 0; // const/4 v0, 0x0
/* .line 461 */
/* .local v0, "most":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
int v1 = -1; // const/4 v1, -0x1
/* .line 462 */
/* .local v1, "count":I */
v2 = this.mWindowsByApp;
(( java.util.HashMap ) v2 ).values ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 463 */
/* .local v3, "pl":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
v4 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v3 ).getCurrentCount ( ); // invoke-virtual {v3}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
/* .line 464 */
/* .local v4, "plCount":I */
/* if-le v4, v1, :cond_0 */
/* .line 465 */
/* move v1, v4 */
/* .line 466 */
/* move-object v0, v3 */
/* .line 468 */
} // .end local v3 # "pl":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
} // .end local v4 # "plCount":I
} // :cond_0
/* .line 469 */
} // :cond_1
} // .end method
private Integer shouldReport ( Integer p0, com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .param p2, "processWindows" # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .param p3, "died" # Z */
/* .line 502 */
int v0 = 0; // const/4 v0, 0x0
/* if-ltz p1, :cond_4 */
/* if-nez p2, :cond_0 */
/* .line 507 */
} // :cond_0
v1 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).getCurrentCount ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
/* .line 510 */
/* .local v1, "curCount":I */
/* if-nez v1, :cond_1 */
/* .line 511 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setLastReportCount ( v0 ); // invoke-virtual {p2, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLastReportCount(I)V
/* .line 512 */
/* .line 514 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 516 */
/* .local v0, "report":I */
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 519 */
v2 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).getLeakType ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLeakType()I
/* if-nez v2, :cond_3 */
/* .line 520 */
v2 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getExitReportThreshold()I */
/* if-le v1, v2, :cond_3 */
/* .line 521 */
/* or-int/lit8 v0, v0, 0x1 */
/* .line 526 */
} // :cond_2
v2 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).getLeakType ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLeakType()I
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 527 */
v2 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).getLastReportCount ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getLastReportCount()I
/* sub-int v2, v1, v2 */
v3 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getReportInterval()I */
/* if-lt v2, v3, :cond_3 */
/* .line 528 */
/* or-int/lit8 v0, v0, 0x3 */
/* .line 529 */
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p2 ).setLastReportCount ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->setLastReportCount(I)V
/* .line 533 */
} // :cond_3
} // :goto_0
/* .line 503 */
} // .end local v0 # "report":I
} // .end local v1 # "curCount":I
} // :cond_4
} // :goto_1
} // .end method
private void showKillAppDialog ( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList p0, java.lang.String p1, Boolean p2 ) {
/* .locals 10 */
/* .param p1, "processWindows" # Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "persistent" # Z */
/* .line 241 */
v7 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getPid()I
/* .line 242 */
/* .local v7, "pid":I */
v0 = this.mContext;
com.android.server.am.ProcessUtils .getApplicationLabel ( v0,p2 );
/* .line 243 */
/* .local v8, "appLabel":Ljava/lang/String; */
/* new-instance v9, Lcom/android/server/wm/MiuiWindowMonitorImpl$2; */
/* move-object v0, v9 */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move v3, v7 */
/* move-object v4, p2 */
/* move-object v5, v8 */
/* move v6, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/wm/MiuiWindowMonitorImpl$2;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;ILjava/lang/String;Ljava/lang/String;Z)V */
/* .line 268 */
/* .local v0, "callback":Lcom/android/server/am/MiuiWarnings$WarningCallback; */
com.android.server.UiThread .getHandler ( );
/* new-instance v2, Lcom/android/server/wm/MiuiWindowMonitorImpl$3; */
/* invoke-direct {v2, p0, v8, v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$3;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Ljava/lang/String;Lcom/android/server/am/MiuiWarnings$WarningCallback;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 273 */
return;
} // .end method
/* # virtual methods */
public Boolean addWindowInfoLocked ( Integer p0, java.lang.String p1, android.os.IBinder p2, java.lang.String p3, Integer p4 ) {
/* .locals 10 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "token" # Landroid/os/IBinder; */
/* .param p4, "windowName" # Ljava/lang/String; */
/* .param p5, "type" # I */
/* .line 131 */
int v0 = 0; // const/4 v0, 0x0
/* if-ltz p1, :cond_2 */
if ( p3 != null) { // if-eqz p3, :cond_2
v1 = android.text.TextUtils .isEmpty ( p2 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 136 */
} // :cond_0
v1 = this.mHandler;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 137 */
/* new-instance v9, Lcom/android/server/wm/MiuiWindowMonitorImpl$1; */
/* move-object v2, v9 */
/* move-object v3, p0 */
/* move v4, p1 */
/* move-object v5, p2 */
/* move-object v6, p3 */
/* move-object v7, p4 */
/* move v8, p5 */
/* invoke-direct/range {v2 ..v8}, Lcom/android/server/wm/MiuiWindowMonitorImpl$1;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I)V */
(( android.os.Handler ) v1 ).post ( v9 ); // invoke-virtual {v1, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 144 */
} // :cond_1
/* .line 132 */
} // :cond_2
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "invalid data: pid="; // const-string v2, "invalid data: pid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", token="; // const-string v2, ", token="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ", packageName="; // const-string v2, ", packageName="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiWindowMonitorImpl"; // const-string v2, "MiuiWindowMonitorImpl"
android.util.Slog .w ( v2,v1 );
/* .line 133 */
} // .end method
public synchronized Integer dump ( java.io.PrintWriter p0 ) {
/* .locals 7 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* monitor-enter p0 */
/* .line 342 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 343 */
/* monitor-exit p0 */
/* .line 346 */
} // :cond_0
try { // :try_start_0
final String v1 = "DUMP MiuiWindowMonitor INFO"; // const-string v1, "DUMP MiuiWindowMonitor INFO"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 347 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
final String v1 = "enabled="; // const-string v1, "enabled="
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->isEnabled()Z */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V
/* .line 348 */
final String v1 = "kill_when_leak="; // const-string v1, "kill_when_leak="
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->killByPolicy()Z */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Z)V
/* .line 349 */
final String v1 = "kill_threshold="; // const-string v1, "kill_threshold="
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getKillThreshold()I */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V
/* .line 350 */
final String v1 = "exit_report_threshold="; // const-string v1, "exit_report_threshold="
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getExitReportThreshold()I */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V
/* .line 351 */
/* const-string/jumbo v1, "warn_interval=" */
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getWarnInterval()I */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V
/* .line 352 */
final String v1 = "report_interval="; // const-string v1, "report_interval="
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getReportInterval()I */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V
/* .line 353 */
/* const-string/jumbo v1, "total_window_threshold=" */
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
v1 = /* invoke-direct {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl;->getTotalWindowThreshold()I */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(I)V
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 355 */
v1 = this.mWindowsByApp;
v1 = (( java.util.HashMap ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->size()I
/* .line 356 */
/* .local v1, "procCount":I */
/* if-nez v1, :cond_1 */
/* .line 357 */
final String v2 = "no process has windows"; // const-string v2, "no process has windows"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 358 */
/* monitor-exit p0 */
/* .line 361 */
} // .end local p0 # "this":Lcom/android/server/wm/MiuiWindowMonitorImpl;
} // :cond_1
try { // :try_start_1
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 363 */
int v2 = 0; // const/4 v2, 0x0
/* .line 364 */
/* .local v2, "index":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 365 */
/* .local v3, "windowCount":I */
v4 = this.mWindowsByApp;
(( java.util.HashMap ) v4 ).values ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_3
/* check-cast v5, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
/* .line 366 */
/* .local v5, "pwl":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList; */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 367 */
final String v6 = " Process#"; // const-string v6, " Process#"
(( java.io.PrintWriter ) p1 ).print ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(I)V
final String v6 = " "; // const-string v6, " "
(( java.io.PrintWriter ) p1 ).print ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
(( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v5 ).toString ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 368 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 369 */
v6 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) v5 ).getCurrentCount ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
/* add-int/2addr v3, v6 */
/* .line 370 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 372 */
} // .end local v5 # "pwl":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;
} // :cond_2
/* .line 373 */
} // :cond_3
final String v4 = "TOTAL:"; // const-string v4, "TOTAL:"
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V
final String v4 = " processes, "; // const-string v4, " processes, "
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
(( java.io.PrintWriter ) p1 ).print ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(I)V
/* .line 374 */
final String v4 = " windows"; // const-string v4, " windows"
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 375 */
/* monitor-exit p0 */
/* .line 341 */
} // .end local v1 # "procCount":I
} // .end local v2 # "index":I
} // .end local v3 # "windowCount":I
} // .end local p1 # "pw":Ljava/io/PrintWriter;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void init ( android.content.Context p0, com.android.server.wm.WindowManagerService p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .line 112 */
this.mService = p2;
/* .line 113 */
this.mContext = p1;
/* .line 114 */
/* new-instance v0, Landroid/os/HandlerThread; */
/* const-string/jumbo v1, "window-leak-monitor-thread" */
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mMonitorThread = v0;
/* .line 115 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 116 */
/* new-instance v0, Landroid/os/Handler; */
v1 = this.mMonitorThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 117 */
return;
} // .end method
public void onProcessDiedLocked ( Integer p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 322 */
/* new-instance v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
com.android.server.am.ProcessUtils .getPackageNameByApp ( p2 );
/* invoke-direct {v0, p1, v1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;-><init>(ILjava/lang/String;)V */
/* .line 323 */
/* .local v0, "key":Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey; */
v1 = this.mHandler;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 324 */
/* new-instance v2, Lcom/android/server/wm/MiuiWindowMonitorImpl$5; */
/* invoke-direct {v2, p0, v0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$5;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessKey;I)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 337 */
} // :cond_0
return;
} // .end method
public void removeWindowInfoLocked ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 302 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 303 */
/* new-instance v1, Lcom/android/server/wm/MiuiWindowMonitorImpl$4; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiWindowMonitorImpl$4;-><init>(Lcom/android/server/wm/MiuiWindowMonitorImpl;Landroid/os/IBinder;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 310 */
} // :cond_0
return;
} // .end method
