.class Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;
.super Ljava/lang/Object;
.source "PackageSettingsManager.java"

# interfaces
.implements Ljava/util/function/BiConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;-><init>(Lcom/android/server/wm/PackageSettingsManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/function/BiConsumer<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;


# direct methods
.method constructor <init>(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    .line 167
    iput-object p1, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 167
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->accept(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public accept(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DisplayCompatPackageManager key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " value = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BoundsCompat"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string/jumbo v0, "w"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-static {v0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    :cond_0
    const-string v0, "b"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-static {v0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    :cond_1
    const-string v0, "r"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-static {v0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    :cond_2
    const-string v0, "i"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 181
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-static {v0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)Ljava/util/Map;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    :cond_3
    const-string v0, "ic"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 184
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-static {v0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)Ljava/util/Map;

    move-result-object v0

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    :cond_4
    const-string v0, "ia"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 187
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-static {v0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)Ljava/util/Map;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    :cond_5
    const-string v0, "ica"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 190
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-static {v0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)Ljava/util/Map;

    move-result-object v0

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    :cond_6
    const-string v0, "c"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 193
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;->this$1:Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;

    invoke-static {v0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->-$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    :cond_7
    return-void
.end method
