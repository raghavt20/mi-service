.class Lcom/android/server/wm/ActivityTaskManagerServiceImpl$6;
.super Landroid/database/ContentObserver;
.source "ActivityTaskManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->registerSubScreenSwitchObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 989
    iput-object p1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$6;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;I)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "flags"    # I

    .line 992
    invoke-super {p0, p1, p2, p3}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;I)V

    .line 993
    iget-object v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$6;->this$0:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    const-string/jumbo v1, "subscreen feature switch on"

    invoke-virtual {v0, p3, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(ILjava/lang/String;)V

    .line 994
    return-void
.end method
