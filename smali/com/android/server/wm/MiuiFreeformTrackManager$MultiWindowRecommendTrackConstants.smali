.class public final Lcom/android/server/wm/MiuiFreeformTrackManager$MultiWindowRecommendTrackConstants;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiFreeformTrackManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MultiWindowRecommendTrackConstants"
.end annotation


# static fields
.field public static final FREEFORM_RECOMMEND_CLICK:Ljava/lang/String; = "621.10.2.1.29121"

.field public static final FREEFORM_RECOMMEND_EXPOSE:Ljava/lang/String; = "621.10.2.1.29119"

.field public static final MULTI_WINDOW_RECOMMEND_CLICK_EVENT_NAME:Ljava/lang/String; = "recommendation_click"

.field public static final MULTI_WINDOW_RECOMMEND_EXPOSE_EVENT_NAME:Ljava/lang/String; = "recommendation_expose"

.field public static final SPLIT_SCREEN_RECOMMEND_CLICK:Ljava/lang/String; = "621.10.1.1.29120"

.field public static final SPLIT_SCREEN_RECOMMEND_EXPOSE:Ljava/lang/String; = "621.10.1.1.29118"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
