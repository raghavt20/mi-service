.class Lcom/android/server/wm/MiuiSizeCompatService$1;
.super Landroid/database/ContentObserver;
.source "MiuiSizeCompatService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiSizeCompatService;->registerDataObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatService;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiSizeCompatService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 435
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .line 438
    const-string v0, "MiuiSizeCompatService"

    const-string v1, "MiuiSizeCompat policySettingConfigsFromCloud onChange--"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$1;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$mupdateSettingConfigsFromCloud(Lcom/android/server/wm/MiuiSizeCompatService;)V

    .line 440
    return-void
.end method
