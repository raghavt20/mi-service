class com.android.server.wm.AppTransitionInjector$ScaleTaskAnimation extends android.view.animation.Animation {
	 /* .source "AppTransitionInjector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AppTransitionInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ScaleTaskAnimation" */
} // .end annotation
/* # instance fields */
private Boolean mEnter;
private Float mFromX;
private Float mFromY;
private Float mPivotX;
private Float mPivotY;
private Float mToX;
private Float mToY;
/* # direct methods */
public com.android.server.wm.AppTransitionInjector$ScaleTaskAnimation ( ) {
/* .locals 0 */
/* .param p1, "fromX" # F */
/* .param p2, "toX" # F */
/* .param p3, "fromY" # F */
/* .param p4, "toY" # F */
/* .param p5, "pivotX" # F */
/* .param p6, "pivotY" # F */
/* .param p7, "enter" # Z */
/* .line 1306 */
/* invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V */
/* .line 1307 */
/* iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mFromX:F */
/* .line 1308 */
/* iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mToX:F */
/* .line 1309 */
/* iput p3, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mFromY:F */
/* .line 1310 */
/* iput p4, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mToY:F */
/* .line 1311 */
/* iput p5, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mPivotX:F */
/* .line 1312 */
/* iput p6, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mPivotY:F */
/* .line 1313 */
/* iput-boolean p7, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mEnter:Z */
/* .line 1314 */
return;
} // .end method
/* # virtual methods */
protected void applyTransformation ( Float p0, android.view.animation.Transformation p1 ) {
/* .locals 12 */
/* .param p1, "interpolatedTime" # F */
/* .param p2, "t" # Landroid/view/animation/Transformation; */
/* .line 1318 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 1319 */
/* .local v0, "scale":F */
/* iget-boolean v1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mEnter:Z */
/* const-wide v2, 0x3fed70a3d70a3d71L # 0.92 */
/* const-wide/high16 v4, 0x4008000000000000L # 3.0 */
/* const-wide v6, 0x3fb47ae147ae147bL # 0.08 */
/* const-wide/high16 v8, 0x3ff0000000000000L # 1.0 */
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 1320 */
	 /* const/high16 v1, 0x3f800000 # 1.0f */
	 /* sub-float/2addr v1, p1 */
	 /* float-to-double v10, v1 */
	 /* mul-double/2addr v10, v6 */
	 /* mul-double/2addr v10, v4 */
	 /* sub-double v4, v8, v10 */
	 java.lang.Math .max ( v4,v5,v2,v3 );
	 /* move-result-wide v1 */
	 /* double-to-float v0, v1 */
	 /* .line 1322 */
} // :cond_0
/* float-to-double v10, p1 */
/* mul-double/2addr v10, v6 */
/* mul-double/2addr v10, v4 */
/* sub-double v4, v8, v10 */
java.lang.Math .max ( v4,v5,v2,v3 );
/* move-result-wide v1 */
/* double-to-float v0, v1 */
/* .line 1324 */
} // :goto_0
/* float-to-double v1, v0 */
/* cmpl-double v1, v1, v8 */
/* if-lez v1, :cond_1 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 1325 */
} // :cond_1
(( android.view.animation.Transformation ) p2 ).getMatrix ( ); // invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mPivotX:F */
/* iget v3, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;->mPivotY:F */
(( android.graphics.Matrix ) v1 ).setScale ( v0, v0, v2, v3 ); // invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FFFF)V
/* .line 1326 */
return;
} // .end method
