.class public interface abstract Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;
.super Ljava/lang/Object;
.source "SchedBoostGesturesEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/SchedBoostGesturesEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GesturesEventListener"
.end annotation


# virtual methods
.method public abstract onDown()V
.end method

.method public abstract onFling(FFI)V
.end method

.method public abstract onMove()V
.end method

.method public abstract onScroll(Z)V
.end method
