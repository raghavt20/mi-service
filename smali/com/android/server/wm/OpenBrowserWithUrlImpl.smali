.class public Lcom/android/server/wm/OpenBrowserWithUrlImpl;
.super Ljava/lang/Object;
.source "OpenBrowserWithUrlImpl.java"

# interfaces
.implements Lcom/android/server/wm/OpenBrowserWithUrlStub;


# static fields
.field private static final IS_SHORTCUT:Ljava/lang/String; = "isShortCut"

.field private static final KEY_CLOUD:Ljava/lang/String; = "openBrowserWithUrl"

.field private static final KEY_ENABLE:Ljava/lang/String; = "openbrowserwithurl_enable"

.field private static final MODULE_CLOUD:Ljava/lang/String; = "kamiCloudDataConfig"

.field private static final TAG:Ljava/lang/String; = "KamiOpenBrowserWithUrl"

.field private static mAppName:Ljava/lang/String;

.field private static mDeviceList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mDocUrlActivity:Ljava/lang/String;

.field private static mUrlActivityList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mUrlRedirect:Ljava/lang/String;

.field private static mVersionCode:Ljava/lang/String;


# instance fields
.field private isSwitch:Z

.field private mContext:Landroid/content/Context;

.field private mExecuteThread:Landroid/os/HandlerThread;


# direct methods
.method static bridge synthetic -$$Nest$mupdateCloudData(Lcom/android/server/wm/OpenBrowserWithUrlImpl;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->updateCloudData(Landroid/content/Context;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mDeviceList:Ljava/util/HashMap;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mUrlActivityList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    return-void
.end method

.method private checkSlow(JJLjava/lang/String;)V
    .locals 4
    .param p1, "startTime"    # J
    .param p3, "threshold"    # J
    .param p5, "where"    # Ljava/lang/String;

    .line 262
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 263
    .local v0, "took":J
    cmp-long v2, v0, p3

    if-lez v2, :cond_0

    .line 265
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Slow operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " took "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "KamiOpenBrowserWithUrl"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :cond_0
    return-void
.end method

.method static synthetic lambda$updateCloudData$0(Ljava/lang/String;)V
    .locals 0
    .param p0, "v"    # Ljava/lang/String;

    .line 127
    const/4 p0, 0x0

    return-void
.end method

.method private updateCloudData(Landroid/content/Context;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;

    .line 127
    move-object/from16 v1, p0

    sget-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mAppName:Ljava/lang/String;

    sget-object v2, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mVersionCode:Ljava/lang/String;

    sget-object v3, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mUrlRedirect:Ljava/lang/String;

    sget-object v4, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mDocUrlActivity:Ljava/lang/String;

    filled-new-array {v0, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/stream/Stream;->of([Ljava/lang/Object;)Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v2, Lcom/android/server/wm/OpenBrowserWithUrlImpl$$ExternalSyntheticLambda0;

    invoke-direct {v2}, Lcom/android/server/wm/OpenBrowserWithUrlImpl$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {v0, v2}, Ljava/util/stream/Stream;->forEach(Ljava/util/function/Consumer;)V

    .line 128
    sget-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mUrlActivityList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 129
    sget-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mDeviceList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 130
    iget-object v0, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    .line 131
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 130
    const-string v2, "kamiCloudDataConfig"

    const-string v3, "openBrowserWithUrl"

    const-string v4, ""

    invoke-static {v0, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 132
    .local v2, "data":Ljava/lang/String;
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "deviceName":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v5, "1"

    const/4 v6, 0x0

    const-string v7, "openbrowserwithurl_enable"

    const-string v8, "0"

    const-string v9, "KamiOpenBrowserWithUrl"

    if-nez v0, :cond_6

    .line 136
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 137
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const-string/jumbo v10, "switchStatus"

    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 138
    .local v10, "switchStatus":Ljava/lang/String;
    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 139
    iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    .line 140
    iget-object v4, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 141
    const-string/jumbo v4, "switchStatus is zero, not updating cloud data"

    invoke-static {v9, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    return-void

    .line 146
    :cond_0
    iget-object v11, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    .line 147
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/pm/PackageInfo;->getLongVersionCode()J

    move-result-wide v11

    .line 146
    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    .line 148
    .local v11, "localAppVersionCode":Ljava/lang/String;
    const-string/jumbo v12, "versionCode"

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mVersionCode:Ljava/lang/String;

    .line 149
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    sget-object v13, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mVersionCode:Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    if-lt v12, v13, :cond_1

    .line 150
    iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    .line 151
    iget-object v4, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 152
    const-string v4, "app versionCode is greater than cloudData, not updating cloud data"

    invoke-static {v9, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    return-void

    .line 156
    :cond_1
    const-string v12, "device"

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 157
    .local v12, "deviceArray":Lorg/json/JSONObject;
    invoke-virtual {v12}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v13

    .line 158
    .local v13, "keys_device":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 159
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 160
    .local v14, "key":Ljava/lang/String;
    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 161
    .local v15, "value":Ljava/lang/String;
    sget-object v4, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mDeviceList:Ljava/util/HashMap;

    invoke-virtual {v4, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    nop

    .end local v14    # "key":Ljava/lang/String;
    .end local v15    # "value":Ljava/lang/String;
    goto :goto_0

    .line 164
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mDeviceList:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 165
    iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    .line 166
    iget-object v4, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 167
    const-string/jumbo v4, "the current device is in the blacklist, not updating cloud data"

    invoke-static {v9, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    return-void

    .line 171
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isTablet()Z

    move-result v4

    if-nez v4, :cond_4

    sget-object v4, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mDeviceList:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 172
    iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    .line 173
    iget-object v4, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 174
    const-string/jumbo v4, "the current device is not in the whitelist, not updating cloud data"

    invoke-static {v9, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    return-void

    .line 177
    :cond_4
    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    .line 178
    iget-object v4, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v7, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 179
    const-string v4, "appName"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mAppName:Ljava/lang/String;

    .line 180
    const-string/jumbo v4, "urlRedirect"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mUrlRedirect:Ljava/lang/String;

    .line 181
    const-string v4, "docUrlActivity"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mDocUrlActivity:Ljava/lang/String;

    .line 182
    const-string/jumbo v4, "urlActivity"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 183
    .local v4, "urlActivity":Lorg/json/JSONObject;
    invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    .line 184
    .local v5, "keys_url":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 185
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 186
    .local v6, "key":Ljava/lang/String;
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 187
    .local v7, "value":Ljava/lang/String;
    sget-object v8, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mUrlActivityList:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    nop

    .end local v6    # "key":Ljava/lang/String;
    .end local v7    # "value":Ljava/lang/String;
    goto :goto_1

    .line 189
    :cond_5
    const-string/jumbo v6, "updateCloudData success"

    invoke-static {v9, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    nop

    .end local v0    # "jsonObject":Lorg/json/JSONObject;
    .end local v4    # "urlActivity":Lorg/json/JSONObject;
    .end local v5    # "keys_url":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v10    # "switchStatus":Ljava/lang/String;
    .end local v11    # "localAppVersionCode":Ljava/lang/String;
    .end local v12    # "deviceArray":Lorg/json/JSONObject;
    .end local v13    # "keys_device":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    goto :goto_3

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "updateCloudData error :"

    invoke-static {v9, v4, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 192
    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_3

    .line 195
    :cond_6
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->initializeParam()V

    .line 196
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 197
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    .line 198
    iget-object v0, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v7, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 199
    const-string v0, "cloud data is null and device is tablet, open by default"

    invoke-static {v9, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 201
    :cond_7
    iput-boolean v6, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    .line 202
    iget-object v0, v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 203
    const-string v0, "cloud data is null and device is not tablet, off by default"

    invoke-static {v9, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 207
    :goto_2
    goto :goto_3

    .line 205
    :catch_1
    move-exception v0

    .line 206
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, "reset switch error :"

    invoke-static {v9, v4, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 209
    .end local v0    # "ex":Ljava/lang/Exception;
    :goto_3
    return-void
.end method


# virtual methods
.method public getSwitch()Z
    .locals 1

    .line 103
    iget-boolean v0, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    return v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 53
    const-string v0, "KamiOpenBrowserWithUrl"

    if-nez p1, :cond_0

    .line 54
    return-void

    .line 57
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->initializeParam()V

    .line 58
    iput-object p1, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    .line 59
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "kami_open_browser_thread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mExecuteThread:Landroid/os/HandlerThread;

    .line 60
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 61
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mExecuteThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 62
    .local v1, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/android/server/wm/OpenBrowserWithUrlImpl$1;

    invoke-direct {v2, p0, v1, p1}, Lcom/android/server/wm/OpenBrowserWithUrlImpl$1;-><init>(Lcom/android/server/wm/OpenBrowserWithUrlImpl;Landroid/os/Handler;Landroid/content/Context;)V

    .line 68
    .local v2, "kamiCloudOberver":Landroid/database/ContentObserver;
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v4, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;

    invoke-direct {v4, p0, p1, v2, v1}, Lcom/android/server/wm/OpenBrowserWithUrlImpl$2;-><init>(Lcom/android/server/wm/OpenBrowserWithUrlImpl;Landroid/content/Context;Landroid/database/ContentObserver;Landroid/os/Handler;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 82
    const-string v3, "init success"

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    nop

    .end local v1    # "handler":Landroid/os/Handler;
    .end local v2    # "kamiCloudOberver":Landroid/database/ContentObserver;
    goto :goto_0

    .line 83
    :catch_0
    move-exception v1

    .line 84
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "init error :"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 86
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public initializeParam()V
    .locals 4

    .line 89
    const-string v0, "com.ss.android.lark.kami"

    sput-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mAppName:Ljava/lang/String;

    .line 90
    const-string v0, "2147483647"

    sput-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mVersionCode:Ljava/lang/String;

    .line 91
    const-string v0, "https://www.f.mioffice.cn/suite/passport/inbound/redirect"

    sput-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mUrlRedirect:Ljava/lang/String;

    .line 92
    sget-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mUrlActivityList:Ljava/util/List;

    const-string v1, "com.ss.android.lark.kami/com.bytedance.ee.bear.document.DocActivity"

    const-string v2, "com.ss.android.lark.kami/com.bytedance.ee.bear.wikiv2.WikiActivity"

    const-string v3, "com.ss.android.lark.kami/com.bytedance.lark.webview.container.impl.WebContainerMainProcessActivity"

    filled-new-array {v3, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 97
    const-string v0, "com.ss.android.lark.kami/com.bytedance.ee.bear.basesdk.DocRouteActivity"

    sput-object v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mDocUrlActivity:Ljava/lang/String;

    .line 98
    const-string v0, "KamiOpenBrowserWithUrl"

    const-string v1, "initializeParam success"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    return-void
.end method

.method public isTablet()Z
    .locals 2

    .line 257
    const-string v0, "ro.build.characteristics"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 258
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 257
    :goto_0
    return v0
.end method

.method public openBrowserWithUrl(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callingPackage"    # Ljava/lang/String;

    .line 213
    const-string v0, "doc_url"

    const-string/jumbo v1, "url"

    const-string v2, "KamiOpenBrowserWithUrl"

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 214
    .local v4, "start":J
    iget-object v3, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 215
    const/4 v6, 0x0

    invoke-virtual {v3, p2, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/pm/PackageInfo;->getLongVersionCode()J

    move-result-wide v6

    .line 214
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    .line 216
    .local v9, "localAppVersionCode":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->isSwitch:Z

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_7

    sget-object v3, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mAppName:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 217
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    sget-object v6, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mVersionCode:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    if-lt v3, v6, :cond_0

    goto/16 :goto_2

    .line 221
    :cond_0
    const/4 v3, 0x0

    .line 222
    .local v3, "url":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 223
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_1

    sget-object v6, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mUrlActivityList:Ljava/util/List;

    .line 224
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 225
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    .line 226
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mDocUrlActivity:Ljava/lang/String;

    .line 227
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 228
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    .line 231
    :cond_2
    move-object v0, v3

    .end local v3    # "url":Ljava/lang/String;
    .local v0, "url":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_5

    sget-object v1, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mUrlRedirect:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_1

    .line 234
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v1, v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 235
    .local v1, "browserIntent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 236
    const/high16 v6, 0x10000

    invoke-virtual {v3, v1, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    move-object v10, v3

    .line 237
    .local v10, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-nez v10, :cond_4

    .line 238
    return-object p1

    .line 241
    :cond_4
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 242
    iget-object v3, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    move-object v11, v3

    .line 244
    .local v11, "bundle":Landroid/os/Bundle;
    const-string v3, "isShortCut"

    const/4 v6, 0x1

    invoke-virtual {v11, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 245
    invoke-virtual {v1, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 246
    const-wide/16 v6, 0x32

    const-string v8, "check openBrowserWithUrl start time"

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/server/wm/OpenBrowserWithUrlImpl;->checkSlow(JJLjava/lang/String;)V

    .line 247
    const-string v3, "openBrowserWithUrl success"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    return-object v1

    .line 232
    .end local v1    # "browserIntent":Landroid/content/Intent;
    .end local v10    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v11    # "bundle":Landroid/os/Bundle;
    :cond_5
    :goto_1
    return-object p1

    .line 252
    .end local v0    # "url":Ljava/lang/String;
    .end local v4    # "start":J
    .end local v9    # "localAppVersionCode":Ljava/lang/String;
    :cond_6
    goto :goto_3

    .line 218
    .restart local v4    # "start":J
    .restart local v9    # "localAppVersionCode":Ljava/lang/String;
    :cond_7
    :goto_2
    return-object p1

    .line 250
    .end local v4    # "start":J
    .end local v9    # "localAppVersionCode":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 251
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "openBrowserWithUrl error :"

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 253
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    return-object p1
.end method
