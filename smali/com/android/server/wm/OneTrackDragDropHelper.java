public class com.android.server.wm.OneTrackDragDropHelper {
	 /* .source "OneTrackDragDropHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID;
private static final java.lang.String EVENT_NAME;
private static final Integer FLAG_NON_ANONYMOUS;
private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
private static final Integer MSG_DRAG_DROP;
private static final java.lang.String ONETRACK_ACTION;
private static final java.lang.String ONETRACK_PACKAGE_NAME;
private static final java.lang.String PACKAGE;
private static final java.lang.String TAG;
private static com.android.server.wm.OneTrackDragDropHelper sInstance;
/* # instance fields */
private final android.content.Context mContext;
private final android.os.Handler mHandler;
/* # direct methods */
static void -$$Nest$mreportOneTrack ( com.android.server.wm.OneTrackDragDropHelper p0, com.android.server.wm.OneTrackDragDropHelper$DragDropData p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/wm/OneTrackDragDropHelper;->reportOneTrack(Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;)V */
	 return;
} // .end method
static com.android.server.wm.OneTrackDragDropHelper ( ) {
	 /* .locals 1 */
	 /* .line 24 */
	 /* const-class v0, Lcom/android/server/wm/OneTrackDragDropHelper; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 return;
} // .end method
private com.android.server.wm.OneTrackDragDropHelper ( ) {
	 /* .locals 2 */
	 /* .line 48 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 49 */
	 android.app.ActivityThread .currentApplication ( );
	 this.mContext = v0;
	 /* .line 50 */
	 /* new-instance v0, Lcom/android/server/wm/OneTrackDragDropHelper$1; */
	 com.android.server.MiuiBgThread .get ( );
	 (( com.android.server.MiuiBgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/OneTrackDragDropHelper$1;-><init>(Lcom/android/server/wm/OneTrackDragDropHelper;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 63 */
	 return;
} // .end method
public static synchronized com.android.server.wm.OneTrackDragDropHelper getInstance ( ) {
	 /* .locals 2 */
	 /* const-class v0, Lcom/android/server/wm/OneTrackDragDropHelper; */
	 /* monitor-enter v0 */
	 /* .line 42 */
	 try { // :try_start_0
		 v1 = com.android.server.wm.OneTrackDragDropHelper.sInstance;
		 /* if-nez v1, :cond_0 */
		 /* .line 43 */
		 /* new-instance v1, Lcom/android/server/wm/OneTrackDragDropHelper; */
		 /* invoke-direct {v1}, Lcom/android/server/wm/OneTrackDragDropHelper;-><init>()V */
		 /* .line 45 */
	 } // :cond_0
	 v1 = com.android.server.wm.OneTrackDragDropHelper.sInstance;
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* monitor-exit v0 */
	 /* .line 41 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* throw v1 */
} // .end method
private void reportOneTrack ( com.android.server.wm.OneTrackDragDropHelper$DragDropData p0 ) {
	 /* .locals 5 */
	 /* .param p1, "dragDropData" # Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData; */
	 /* .line 73 */
	 /* new-instance v0, Landroid/content/Intent; */
	 final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
	 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* .line 74 */
	 /* .local v0, "intent":Landroid/content/Intent; */
	 final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
	 (( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 75 */
	 final String v1 = "APP_ID"; // const-string v1, "APP_ID"
	 final String v2 = "31000000538"; // const-string v2, "31000000538"
	 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 76 */
	 final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
	 final String v2 = "android"; // const-string v2, "android"
	 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 77 */
	 final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
	 final String v2 = "content_drag"; // const-string v2, "content_drag"
	 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 79 */
	 /* const-string/jumbo v1, "send_package_name" */
	 v2 = this.mDragPackage;
	 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 80 */
	 /* const-string/jumbo v1, "target_package_name" */
	 v2 = this.mDropPackage;
	 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 81 */
	 final String v1 = "drag_result"; // const-string v1, "drag_result"
	 v2 = this.mResult;
	 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 82 */
	 final String v1 = "content_style"; // const-string v1, "content_style"
	 v2 = this.mContentStyle;
	 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 84 */
	 /* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
	 /* if-nez v1, :cond_0 */
	 /* .line 85 */
	 int v1 = 3; // const/4 v1, 0x3
	 (( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
	 /* .line 89 */
} // :cond_0
try { // :try_start_0
	 v1 = this.mContext;
	 v2 = android.os.UserHandle.CURRENT;
	 (( android.content.Context ) v1 ).startServiceAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 92 */
	 /* .line 90 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 91 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 v2 = com.android.server.wm.OneTrackDragDropHelper.TAG;
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "Upload DragDropData exception! "; // const-string v4, "Upload DragDropData exception! "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v2,v3,v1 );
	 /* .line 93 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void notifyDragDropResult ( com.android.server.wm.WindowState p0, com.android.server.wm.WindowState p1, Boolean p2, android.content.ClipData p3 ) {
/* .locals 7 */
/* .param p1, "dragWindow" # Lcom/android/server/wm/WindowState; */
/* .param p2, "dropWindow" # Lcom/android/server/wm/WindowState; */
/* .param p3, "result" # Z */
/* .param p4, "clipData" # Landroid/content/ClipData; */
/* .line 67 */
/* new-instance v6, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder; */
v0 = this.mContext;
/* .line 68 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* move-object v0, v6 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move v3, p3 */
/* move-object v4, p4 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;-><init>(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;ZLandroid/content/ClipData;Landroid/content/ContentResolver;)V */
/* .line 69 */
/* .local v0, "dragDropDataBuilder":Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder; */
v1 = this.mHandler;
int v2 = 0; // const/4 v2, 0x0
(( android.os.Handler ) v1 ).obtainMessage ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 70 */
return;
} // .end method
