.class public Lcom/android/server/wm/OneTrackDragDropHelper;
.super Ljava/lang/Object;
.source "OneTrackDragDropHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;
    }
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000000538"

.field private static final EVENT_NAME:Ljava/lang/String; = "content_drag"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final MSG_DRAG_DROP:I = 0x0

.field private static final ONETRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final ONETRACK_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final PACKAGE:Ljava/lang/String; = "android"

.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/android/server/wm/OneTrackDragDropHelper;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static bridge synthetic -$$Nest$mreportOneTrack(Lcom/android/server/wm/OneTrackDragDropHelper;Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/OneTrackDragDropHelper;->reportOneTrack(Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 24
    const-class v0, Lcom/android/server/wm/OneTrackDragDropHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/OneTrackDragDropHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/OneTrackDragDropHelper;->mContext:Landroid/content/Context;

    .line 50
    new-instance v0, Lcom/android/server/wm/OneTrackDragDropHelper$1;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/OneTrackDragDropHelper$1;-><init>(Lcom/android/server/wm/OneTrackDragDropHelper;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/OneTrackDragDropHelper;->mHandler:Landroid/os/Handler;

    .line 63
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/android/server/wm/OneTrackDragDropHelper;
    .locals 2

    const-class v0, Lcom/android/server/wm/OneTrackDragDropHelper;

    monitor-enter v0

    .line 42
    :try_start_0
    sget-object v1, Lcom/android/server/wm/OneTrackDragDropHelper;->sInstance:Lcom/android/server/wm/OneTrackDragDropHelper;

    if-nez v1, :cond_0

    .line 43
    new-instance v1, Lcom/android/server/wm/OneTrackDragDropHelper;

    invoke-direct {v1}, Lcom/android/server/wm/OneTrackDragDropHelper;-><init>()V

    sput-object v1, Lcom/android/server/wm/OneTrackDragDropHelper;->sInstance:Lcom/android/server/wm/OneTrackDragDropHelper;

    .line 45
    :cond_0
    sget-object v1, Lcom/android/server/wm/OneTrackDragDropHelper;->sInstance:Lcom/android/server/wm/OneTrackDragDropHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 41
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private reportOneTrack(Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;)V
    .locals 5
    .param p1, "dragDropData"    # Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;

    .line 73
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 74
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const-string v1, "APP_ID"

    const-string v2, "31000000538"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-string v1, "PACKAGE"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const-string v1, "EVENT_NAME"

    const-string v2, "content_drag"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string/jumbo v1, "send_package_name"

    iget-object v2, p1, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;->mDragPackage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    const-string/jumbo v1, "target_package_name"

    iget-object v2, p1, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;->mDropPackage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 81
    const-string v1, "drag_result"

    iget-object v2, p1, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;->mResult:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    const-string v1, "content_style"

    iget-object v2, p1, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData;->mContentStyle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_0

    .line 85
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 89
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/OneTrackDragDropHelper;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    goto :goto_0

    .line 90
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/android/server/wm/OneTrackDragDropHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Upload DragDropData exception! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 93
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public notifyDragDropResult(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;ZLandroid/content/ClipData;)V
    .locals 7
    .param p1, "dragWindow"    # Lcom/android/server/wm/WindowState;
    .param p2, "dropWindow"    # Lcom/android/server/wm/WindowState;
    .param p3, "result"    # Z
    .param p4, "clipData"    # Landroid/content/ClipData;

    .line 67
    new-instance v6, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;

    iget-object v0, p0, Lcom/android/server/wm/OneTrackDragDropHelper;->mContext:Landroid/content/Context;

    .line 68
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;-><init>(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WindowState;ZLandroid/content/ClipData;Landroid/content/ContentResolver;)V

    .line 69
    .local v0, "dragDropDataBuilder":Lcom/android/server/wm/OneTrackDragDropHelper$DragDropData$Builder;
    iget-object v1, p0, Lcom/android/server/wm/OneTrackDragDropHelper;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 70
    return-void
.end method
