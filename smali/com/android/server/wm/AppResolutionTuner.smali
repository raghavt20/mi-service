.class public Lcom/android/server/wm/AppResolutionTuner;
.super Ljava/lang/Object;
.source "AppResolutionTuner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;
    }
.end annotation


# static fields
.field public static final DEBUG:Z

.field private static final DEFAULT_SCALE:F = 1.0f

.field private static final FILTERED_WINDOWS_SEPARATOR:Ljava/lang/String; = ";"

.field private static final NODE_APP_RT_ACTIVITY_WHITELIST:Ljava/lang/String; = "activity_whitelist"

.field private static final NODE_APP_RT_CONFIG:Ljava/lang/String; = "app_rt_config"

.field private static final NODE_APP_RT_FILTERED_WINDOWS:Ljava/lang/String; = "filtered_windows"

.field private static final NODE_APP_RT_FLOW:Ljava/lang/String; = "flow"

.field private static final NODE_APP_RT_PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field private static final NODE_APP_RT_SCALE:Ljava/lang/String; = "scale"

.field public static final TAG:Ljava/lang/String; = "APP_RT"

.field private static final VALUE_SCALING_FLOW_WMS:Ljava/lang/String; = "wms"

.field private static volatile sAppResolutionTuner:Lcom/android/server/wm/AppResolutionTuner;

.field private static final sLock:Ljava/lang/Object;


# instance fields
.field private mAppRTConfigMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;",
            ">;"
        }
    .end annotation
.end field

.field private mAppRTEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 41
    nop

    .line 42
    const-string v0, "persist.sys.resolutiontuner.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z

    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppResolutionTuner;->sLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTConfigMap:Ljava/util/Map;

    .line 52
    const-string v0, "persist.sys.resolutiontuner.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTEnable:Z

    .line 53
    return-void
.end method

.method public static getInstance()Lcom/android/server/wm/AppResolutionTuner;
    .locals 2

    .line 59
    sget-object v0, Lcom/android/server/wm/AppResolutionTuner;->sAppResolutionTuner:Lcom/android/server/wm/AppResolutionTuner;

    if-nez v0, :cond_1

    .line 60
    sget-object v0, Lcom/android/server/wm/AppResolutionTuner;->sLock:Ljava/lang/Object;

    monitor-enter v0

    .line 61
    :try_start_0
    sget-object v1, Lcom/android/server/wm/AppResolutionTuner;->sAppResolutionTuner:Lcom/android/server/wm/AppResolutionTuner;

    if-nez v1, :cond_0

    .line 62
    new-instance v1, Lcom/android/server/wm/AppResolutionTuner;

    invoke-direct {v1}, Lcom/android/server/wm/AppResolutionTuner;-><init>()V

    sput-object v1, Lcom/android/server/wm/AppResolutionTuner;->sAppResolutionTuner:Lcom/android/server/wm/AppResolutionTuner;

    .line 63
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 65
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/wm/AppResolutionTuner;->sAppResolutionTuner:Lcom/android/server/wm/AppResolutionTuner;

    return-object v0
.end method


# virtual methods
.method public getScaleValue(Ljava/lang/String;)F
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 148
    iget-object v0, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTConfigMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    if-eqz v0, :cond_0

    return v1

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTConfigMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;

    .line 150
    .local v0, "config":Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;
    if-nez v0, :cond_1

    return v1

    .line 151
    :cond_1
    invoke-static {v0}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->-$$Nest$mgetScale(Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;)F

    move-result v1

    return v1
.end method

.method public isAppRTEnable()Z
    .locals 2

    .line 127
    sget-boolean v0, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isAppRTEnable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "APP_RT"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTEnable:Z

    return v0
.end method

.method public isScaledByWMS(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "windowName"    # Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTConfigMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTConfigMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;

    .line 137
    .local v0, "config":Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;
    if-nez v0, :cond_1

    return v1

    .line 138
    :cond_1
    const-string/jumbo v2, "wms"

    invoke-static {v0}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->-$$Nest$mgetScalingFlow(Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 139
    invoke-static {v0}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->-$$Nest$mgetFilteredWindows(Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 140
    invoke-static {v0, p2}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;->-$$Nest$misActivityWhitelist(Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    nop

    .line 141
    .local v1, "ret":Z
    :goto_0
    return v1
.end method

.method public setAppRTEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 119
    sget-boolean v0, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setAppRTEnable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "APP_RT"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/wm/AppResolutionTuner;->mAppRTEnable:Z

    .line 121
    return-void
.end method

.method public updateResolutionTunerConfig(Ljava/lang/String;)Z
    .locals 26
    .param p1, "configStr"    # Ljava/lang/String;

    .line 72
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    const-string v0, ";"

    const-string v9, "flow"

    const-string v10, "filtered_windows"

    const-string v11, "scale"

    const-string v12, "package_name"

    const-string v1, "app_rt_config"

    sget-boolean v2, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z

    const-string/jumbo v13, "updateResolutionTunerConfig: "

    const-string v14, "APP_RT"

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v14, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_0
    if-eqz v8, :cond_8

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto/16 :goto_3

    .line 74
    :cond_1
    new-instance v2, Landroid/util/ArrayMap;

    invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V

    move-object v3, v2

    .line 76
    .local v3, "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 77
    .local v2, "object":Lorg/json/JSONObject;
    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 78
    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 79
    .local v1, "configs":Lorg/json/JSONArray;
    const/4 v4, 0x0

    move v6, v4

    .local v6, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v6, v4, :cond_5

    .line 80
    invoke-virtual {v1, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 81
    .local v4, "config":Lorg/json/JSONObject;
    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 82
    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 83
    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    move-object/from16 v17, v1

    move-object/from16 v16, v2

    move-object v8, v3

    move-object/from16 v25, v4

    move/from16 v22, v6

    goto/16 :goto_1

    .line 87
    :cond_2
    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 88
    .local v5, "packageName":Ljava/lang/String;
    move-object/from16 v17, v1

    move-object/from16 v16, v2

    .end local v1    # "configs":Lorg/json/JSONArray;
    .end local v2    # "object":Lorg/json/JSONObject;
    .local v16, "object":Lorg/json/JSONObject;
    .local v17, "configs":Lorg/json/JSONArray;
    invoke-virtual {v4, v11}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    double-to-float v2, v1

    .line 89
    .local v2, "scale":F
    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v15, v5

    .end local v5    # "packageName":Ljava/lang/String;
    .local v15, "packageName":Ljava/lang/String;
    move-object v5, v1

    .line 90
    .local v5, "scalingFlow":Ljava/lang/String;
    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "filteredWindowsStr":Ljava/lang/String;
    nop

    .line 92
    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 93
    .local v18, "filteredWindowsArray":[Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v19

    move-object v8, v4

    .end local v4    # "config":Lorg/json/JSONObject;
    .local v8, "config":Lorg/json/JSONObject;
    move-object/from16 v4, v19

    .line 95
    .local v4, "filteredWindows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v19, v1

    .end local v1    # "filteredWindowsStr":Ljava/lang/String;
    .local v19, "filteredWindowsStr":Ljava/lang/String;
    const-string v1, "activity_whitelist"

    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    .local v1, "activityWhitelistStr":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 97
    .local v20, "activityWhitelistArray":[Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    move/from16 v22, v6

    .end local v6    # "i":I
    .local v22, "i":I
    move-object/from16 v6, v21

    .line 99
    .local v6, "activityWhitelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v21, v0

    new-instance v0, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-object/from16 v23, v1

    .end local v1    # "activityWhitelistStr":Ljava/lang/String;
    .local v23, "activityWhitelistStr":Ljava/lang/String;
    move-object v1, v0

    move/from16 v24, v2

    .end local v2    # "scale":F
    .local v24, "scale":F
    move-object/from16 v2, p0

    move-object/from16 v25, v8

    move-object v8, v3

    .end local v3    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .local v8, "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .local v25, "config":Lorg/json/JSONObject;
    move/from16 v3, v24

    :try_start_1
    invoke-direct/range {v1 .. v6}, Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;-><init>(Lcom/android/server/wm/AppResolutionTuner;FLjava/util/List;Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v8, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    nop

    .end local v4    # "filteredWindows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "scalingFlow":Ljava/lang/String;
    .end local v6    # "activityWhitelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v18    # "filteredWindowsArray":[Ljava/lang/String;
    .end local v19    # "filteredWindowsStr":Ljava/lang/String;
    .end local v20    # "activityWhitelistArray":[Ljava/lang/String;
    .end local v23    # "activityWhitelistStr":Ljava/lang/String;
    .end local v24    # "scale":F
    .end local v25    # "config":Lorg/json/JSONObject;
    add-int/lit8 v6, v22, 0x1

    move-object v3, v8

    move-object/from16 v2, v16

    move-object/from16 v1, v17

    move-object/from16 v0, v21

    move-object/from16 v8, p1

    .end local v22    # "i":I
    .local v6, "i":I
    goto/16 :goto_0

    .line 82
    .end local v8    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .end local v16    # "object":Lorg/json/JSONObject;
    .end local v17    # "configs":Lorg/json/JSONArray;
    .local v1, "configs":Lorg/json/JSONArray;
    .local v2, "object":Lorg/json/JSONObject;
    .restart local v3    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .local v4, "config":Lorg/json/JSONObject;
    :cond_3
    move-object/from16 v17, v1

    move-object/from16 v16, v2

    move-object v8, v3

    move-object/from16 v25, v4

    move/from16 v22, v6

    .end local v1    # "configs":Lorg/json/JSONArray;
    .end local v2    # "object":Lorg/json/JSONObject;
    .end local v3    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .end local v4    # "config":Lorg/json/JSONObject;
    .end local v6    # "i":I
    .restart local v8    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .restart local v16    # "object":Lorg/json/JSONObject;
    .restart local v17    # "configs":Lorg/json/JSONArray;
    .restart local v22    # "i":I
    .restart local v25    # "config":Lorg/json/JSONObject;
    goto :goto_1

    .line 81
    .end local v8    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .end local v16    # "object":Lorg/json/JSONObject;
    .end local v17    # "configs":Lorg/json/JSONArray;
    .end local v22    # "i":I
    .end local v25    # "config":Lorg/json/JSONObject;
    .restart local v1    # "configs":Lorg/json/JSONArray;
    .restart local v2    # "object":Lorg/json/JSONObject;
    .restart local v3    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .restart local v4    # "config":Lorg/json/JSONObject;
    .restart local v6    # "i":I
    :cond_4
    move-object/from16 v17, v1

    move-object/from16 v16, v2

    move-object v8, v3

    move-object/from16 v25, v4

    move/from16 v22, v6

    .line 84
    .end local v1    # "configs":Lorg/json/JSONArray;
    .end local v2    # "object":Lorg/json/JSONObject;
    .end local v3    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .end local v4    # "config":Lorg/json/JSONObject;
    .end local v6    # "i":I
    .restart local v8    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .restart local v16    # "object":Lorg/json/JSONObject;
    .restart local v17    # "configs":Lorg/json/JSONArray;
    .restart local v22    # "i":I
    .restart local v25    # "config":Lorg/json/JSONObject;
    :goto_1
    const-string/jumbo v0, "uncompleted config!"

    invoke-static {v14, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v1, 0x0

    return v1

    .line 79
    .end local v8    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .end local v16    # "object":Lorg/json/JSONObject;
    .end local v17    # "configs":Lorg/json/JSONArray;
    .end local v22    # "i":I
    .end local v25    # "config":Lorg/json/JSONObject;
    .restart local v1    # "configs":Lorg/json/JSONArray;
    .restart local v2    # "object":Lorg/json/JSONObject;
    .restart local v3    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .restart local v6    # "i":I
    :cond_5
    move-object/from16 v17, v1

    move-object/from16 v16, v2

    move-object v8, v3

    move/from16 v22, v6

    .line 102
    .end local v1    # "configs":Lorg/json/JSONArray;
    .end local v2    # "object":Lorg/json/JSONObject;
    .end local v3    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .end local v6    # "i":I
    .restart local v8    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .restart local v16    # "object":Lorg/json/JSONObject;
    .restart local v17    # "configs":Lorg/json/JSONArray;
    iput-object v8, v7, Lcom/android/server/wm/AppResolutionTuner;->mAppRTConfigMap:Ljava/util/Map;

    .line 103
    sget-boolean v0, Lcom/android/server/wm/AppResolutionTuner;->DEBUG:Z

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/android/server/wm/AppResolutionTuner;->mAppRTConfigMap:Ljava/util/Map;

    .line 104
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    invoke-static {v14, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 105
    .end local v17    # "configs":Lorg/json/JSONArray;
    :cond_6
    nop

    .line 111
    .end local v16    # "object":Lorg/json/JSONObject;
    nop

    .line 112
    const/4 v0, 0x1

    return v0

    .line 108
    :catch_0
    move-exception v0

    goto :goto_2

    .line 106
    .end local v8    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .restart local v2    # "object":Lorg/json/JSONObject;
    .restart local v3    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    :cond_7
    const/4 v1, 0x0

    return v1

    .line 108
    .end local v2    # "object":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v8, v3

    .line 109
    .end local v3    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    .local v0, "e":Lorg/json/JSONException;
    .restart local v8    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 110
    const/4 v1, 0x0

    return v1

    .line 73
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v8    # "appRTConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/wm/AppResolutionTuner$AppRTConfig;>;"
    :cond_8
    const/4 v1, 0x0

    :goto_3
    return v1
.end method
