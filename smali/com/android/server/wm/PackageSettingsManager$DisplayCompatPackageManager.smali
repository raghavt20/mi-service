.class Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;
.super Ljava/lang/Object;
.source "PackageSettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/PackageSettingsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisplayCompatPackageManager"
.end annotation


# instance fields
.field final mCallback:Ljava/util/function/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Consumer<",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mPackagesMapBySystem:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPackagesMapByUserSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/wm/PackageSettingsManager;


# direct methods
.method public static synthetic $r8$lambda$uQDd_O5hx4wGzk3LKvsZQKDw_js(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->lambda$new$0(Ljava/util/concurrent/ConcurrentHashMap;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapBySystem:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mdump(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/PackageSettingsManager;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/wm/PackageSettingsManager;

    .line 162
    iput-object p1, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->this$0:Lcom/android/server/wm/PackageSettingsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapBySystem:Ljava/util/Map;

    .line 165
    new-instance v0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)V

    iput-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mCallback:Ljava/util/function/Consumer;

    return-void
.end method

.method private dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 8
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "innerPrefix":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapBySystem:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    const-string v2, "] "

    const-string v3, "["

    if-nez v1, :cond_0

    .line 202
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "DisplayCompatPackages(System)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 203
    iget-object v1, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapBySystem:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 204
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 205
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/android/server/wm/PackageSettingsManager;->displayCompatPolicyToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 204
    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 206
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapByUserSettings:Ljava/util/Map;

    .line 210
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapByUserSettings:Ljava/util/Map;

    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 211
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DisplayCompatPackages(UserSetting)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 212
    iget-object v4, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapByUserSettings:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 213
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 214
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Lcom/android/server/wm/PackageSettingsManager;->displayCompatPolicyToString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 213
    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 215
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_1

    .line 217
    :cond_1
    return-void
.end method

.method private synthetic lambda$new$0(Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 1
    .param p1, "map"    # Ljava/util/concurrent/ConcurrentHashMap;

    .line 166
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapBySystem:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 167
    new-instance v0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;-><init>(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)V

    invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 197
    return-void
.end method


# virtual methods
.method public clearUserSettings()V
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapByUserSettings:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 221
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 223
    :cond_0
    return-void
.end method

.method public getPolicy(Ljava/lang/String;)I
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DisplayCompatPackageManager getPolicy packageName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PackageSettingsManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->this$0:Lcom/android/server/wm/PackageSettingsManager;

    invoke-static {v0}, Lcom/android/server/wm/PackageSettingsManager;->-$$Nest$fgetmAtmService(Lcom/android/server/wm/PackageSettingsManager;)Lcom/android/server/wm/ActivityTaskManagerService;

    move-result-object v0

    iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mForceResizableActivities:Z

    const/4 v1, 0x3

    if-eqz v0, :cond_0

    .line 229
    return v1

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapByUserSettings:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 233
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 234
    .local v0, "value":Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 235
    move-object v2, v0

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 236
    .local v2, "policy":I
    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 238
    :cond_1
    return v2

    .line 242
    .end local v0    # "value":Ljava/lang/Object;
    .end local v2    # "policy":I
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapBySystem:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 243
    .restart local v0    # "value":Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/Integer;

    const-string v3, "android.supports_size_changes"

    if-eqz v2, :cond_5

    .line 244
    move-object v2, v0

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 245
    .restart local v2    # "policy":I
    if-eq v2, v1, :cond_4

    const/4 v1, 0x4

    if-eq v2, v1, :cond_4

    const/4 v1, 0x7

    if-eq v2, v1, :cond_4

    const/16 v1, 0x8

    if-eq v2, v1, :cond_4

    const/16 v1, 0x9

    if-eq v2, v1, :cond_4

    const/16 v1, 0xa

    if-eq v2, v1, :cond_4

    const/16 v1, 0xb

    if-eq v2, v1, :cond_4

    const/4 v1, 0x6

    if-ne v2, v1, :cond_3

    goto :goto_0

    .line 255
    .end local v2    # "policy":I
    :cond_3
    goto :goto_1

    .line 253
    .restart local v2    # "policy":I
    :cond_4
    :goto_0
    return v2

    .line 256
    .end local v2    # "policy":I
    :cond_5
    iget-object v2, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->this$0:Lcom/android/server/wm/PackageSettingsManager;

    invoke-static {v2}, Lcom/android/server/wm/PackageSettingsManager;->-$$Nest$fgetmAtmServiceImpl(Lcom/android/server/wm/PackageSettingsManager;)Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v2

    sget-object v4, Landroid/appcompat/ApplicationCompatUtilsStub;->MIUI_SUPPORT_APP_CONTINUITY:Ljava/lang/String;

    invoke-virtual {v2, p1, v4}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->this$0:Lcom/android/server/wm/PackageSettingsManager;

    invoke-static {v2}, Lcom/android/server/wm/PackageSettingsManager;->-$$Nest$fgetmAtmServiceImpl(Lcom/android/server/wm/PackageSettingsManager;)Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v2

    .line 257
    invoke-virtual {v2, p1, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    goto :goto_2

    .line 262
    :cond_6
    :goto_1
    iget-object v1, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->this$0:Lcom/android/server/wm/PackageSettingsManager;

    invoke-static {v1}, Lcom/android/server/wm/PackageSettingsManager;->-$$Nest$fgetmAtmServiceImpl(Lcom/android/server/wm/PackageSettingsManager;)Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v1

    sget-object v2, Landroid/appcompat/ApplicationCompatUtilsStub;->MIUI_SUPPORT_APP_CONTINUITY:Ljava/lang/String;

    invoke-virtual {v1, p1, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->this$0:Lcom/android/server/wm/PackageSettingsManager;

    invoke-static {v1}, Lcom/android/server/wm/PackageSettingsManager;->-$$Nest$fgetmAtmServiceImpl(Lcom/android/server/wm/PackageSettingsManager;)Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v1

    .line 263
    invoke-virtual {v1, p1, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 264
    const/4 v1, 0x0

    return v1

    .line 266
    :cond_7
    const/4 v1, 0x5

    return v1

    .line 258
    :cond_8
    :goto_2
    return v1
.end method

.method public setPolicy(Ljava/util/Map;Z)V
    .locals 1
    .param p2, "replaceAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "+",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .line 270
    .local p1, "requestedPackages":Ljava/util/Map;, "Ljava/util/Map<+Ljava/lang/String;+Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapByUserSettings:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 271
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapByUserSettings:Ljava/util/Map;

    goto :goto_0

    .line 272
    :cond_0
    if-eqz p2, :cond_1

    .line 273
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 275
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->mPackagesMapByUserSettings:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 276
    return-void
.end method
