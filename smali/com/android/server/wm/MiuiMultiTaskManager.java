public class com.android.server.wm.MiuiMultiTaskManager implements com.android.server.wm.MiuiMultiTaskManagerStub {
	 /* .source "MiuiMultiTaskManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean FEATURE_SUPPORT;
public static final java.lang.String FLAG_LAUNCH_APP_IN_ONE_TASK_GROUP;
public static final java.lang.String TASK_RETURN_TO_TARGET;
private static java.lang.String sSupportUI;
private static java.util.HashMap sTargetMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.wm.MiuiMultiTaskManager ( ) {
/* .locals 2 */
/* .line 17 */
final String v0 = "com.tencent.mm.plugin.webview.ui.tools.WebViewUI"; // const-string v0, "com.tencent.mm.plugin.webview.ui.tools.WebViewUI"
/* filled-new-array {v0}, [Ljava/lang/String; */
/* .line 18 */
final String v0 = "miui.multitask.enable"; // const-string v0, "miui.multitask.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.wm.MiuiMultiTaskManager.FEATURE_SUPPORT = (v0!= 0);
/* .line 19 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 21 */
com.android.server.wm.MiuiMultiTaskManager .init ( );
/* .line 22 */
return;
} // .end method
public com.android.server.wm.MiuiMultiTaskManager ( ) {
/* .locals 0 */
/* .line 13 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static Boolean checkMultiTaskAffinity ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 7 */
/* .param p0, "target" # Lcom/android/server/wm/ActivityRecord; */
/* .param p1, "checkRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 92 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiMultiTaskManager;->FEATURE_SUPPORT:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 93 */
} // :cond_0
v0 = com.android.server.wm.MiuiMultiTaskManager.sSupportUI;
/* array-length v2, v0 */
/* move v3, v1 */
} // :goto_0
/* if-ge v3, v2, :cond_2 */
/* aget-object v4, v0, v3 */
/* .line 94 */
/* .local v4, "className":Ljava/lang/String; */
if ( p1 != null) { // if-eqz p1, :cond_1
v5 = this.info;
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = this.info;
v5 = this.name;
/* .line 95 */
v5 = java.util.Objects .equals ( v4,v5 );
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 96 */
if ( p0 != null) { // if-eqz p0, :cond_1
	 v5 = this.packageName;
	 v6 = this.packageName;
	 v5 = 	 java.util.Objects .equals ( v5,v6 );
	 if ( v5 != null) { // if-eqz v5, :cond_1
		 /* .line 97 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 93 */
	 } // .end local v4 # "className":Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 101 */
} // :cond_2
} // .end method
private static com.android.server.wm.MiuiMultiTaskManager$LaunchAppInfo getLaunchAppInfoByName ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "name" # Ljava/lang/String; */
/* .line 41 */
final String v0 = "com.tencent.mm.plugin.webview.ui.tools.WebViewUI"; // const-string v0, "com.tencent.mm.plugin.webview.ui.tools.WebViewUI"
v1 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 42 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 43 */
/* .local v1, "supports":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
final String v2 = "com.tencent.mm.ui.LauncherUI"; // const-string v2, "com.tencent.mm.ui.LauncherUI"
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 44 */
final String v3 = "com.tencent.mm.ui.chatting.ChattingUI"; // const-string v3, "com.tencent.mm.ui.chatting.ChattingUI"
(( java.util.ArrayList ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 45 */
final String v3 = "com.tencent.mm.plugin.sns.ui.SnsTimeLineUI"; // const-string v3, "com.tencent.mm.plugin.sns.ui.SnsTimeLineUI"
(( java.util.ArrayList ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 46 */
final String v3 = "com.tencent.mm.plugin.readerapp.ui.ReaderAppUI"; // const-string v3, "com.tencent.mm.plugin.readerapp.ui.ReaderAppUI"
(( java.util.ArrayList ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 47 */
final String v3 = "com.tencent.mm.ui.conversation.BizConversationUI"; // const-string v3, "com.tencent.mm.ui.conversation.BizConversationUI"
(( java.util.ArrayList ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 48 */
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 49 */
/* new-instance v0, Landroid/content/ComponentName; */
final String v3 = "com.tencent.mm"; // const-string v3, "com.tencent.mm"
/* invoke-direct {v0, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 51 */
/* .local v0, "returnTarget":Landroid/content/ComponentName; */
/* new-instance v2, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo; */
/* invoke-direct {v2, v1, v0}, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;-><init>(Ljava/util/ArrayList;Landroid/content/ComponentName;)V */
/* .line 53 */
} // .end local v0 # "returnTarget":Landroid/content/ComponentName;
} // .end local v1 # "supports":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static void init ( ) {
/* .locals 4 */
/* .line 35 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = com.android.server.wm.MiuiMultiTaskManager.sSupportUI;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_0 */
/* .line 36 */
v2 = com.android.server.wm.MiuiMultiTaskManager.sTargetMap;
/* aget-object v1, v1, v0 */
com.android.server.wm.MiuiMultiTaskManager .getLaunchAppInfoByName ( v1 );
(( java.util.HashMap ) v2 ).put ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 35 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 38 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
static Boolean isMultiTaskSupport ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 6 */
/* .param p0, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .line 81 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiMultiTaskManager;->FEATURE_SUPPORT:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 82 */
} // :cond_0
v0 = com.android.server.wm.MiuiMultiTaskManager.sSupportUI;
/* array-length v2, v0 */
/* move v3, v1 */
} // :goto_0
/* if-ge v3, v2, :cond_2 */
/* aget-object v4, v0, v3 */
/* .line 83 */
/* .local v4, "className":Ljava/lang/String; */
if ( p0 != null) { // if-eqz p0, :cond_1
v5 = this.info;
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = this.info;
v5 = this.name;
/* .line 84 */
v5 = java.util.Objects .equals ( v4,v5 );
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 85 */
int v0 = 1; // const/4 v0, 0x1
/* .line 82 */
} // .end local v4 # "className":Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 88 */
} // :cond_2
} // .end method
static Boolean isVersionSupport ( ) {
/* .locals 1 */
/* .line 105 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
/* # virtual methods */
public void updateMultiTaskInfoIfNeed ( com.android.server.wm.Task p0, android.content.pm.ActivityInfo p1, android.content.Intent p2 ) {
/* .locals 4 */
/* .param p1, "stack" # Lcom/android/server/wm/Task; */
/* .param p2, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .line 58 */
/* sget-boolean v0, Lcom/android/server/wm/MiuiMultiTaskManager;->FEATURE_SUPPORT:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 59 */
} // :cond_0
v0 = com.android.server.wm.MiuiMultiTaskManager .isVersionSupport ( );
if ( v0 != null) { // if-eqz v0, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_3
v0 = com.android.server.wm.MiuiMultiTaskManager.sTargetMap;
v1 = this.name;
v0 = (( java.util.HashMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 61 */
v0 = com.android.server.wm.MiuiMultiTaskManager.sTargetMap;
v1 = this.name;
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo; */
/* .line 62 */
/* .local v0, "info":Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo; */
if ( p1 != null) { // if-eqz p1, :cond_1
(( com.android.server.wm.Task ) p1 ).topRunningActivityLocked ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 64 */
/* .local v1, "topr":Lcom/android/server/wm/ActivityRecord; */
} // :goto_0
try { // :try_start_0
com.android.server.wm.MiuiMultiTaskManager$LaunchAppInfo .-$$Nest$fgetsupports ( v0 );
if ( v2 != null) { // if-eqz v2, :cond_2
com.android.server.wm.MiuiMultiTaskManager$LaunchAppInfo .-$$Nest$fgetreturnTarget ( v0 );
if ( v2 != null) { // if-eqz v2, :cond_2
if ( v1 != null) { // if-eqz v1, :cond_2
com.android.server.wm.MiuiMultiTaskManager$LaunchAppInfo .-$$Nest$fgetsupports ( v0 );
v3 = this.info;
v3 = this.name;
/* .line 66 */
v2 = (( java.util.ArrayList ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 67 */
v2 = (( android.content.Intent ) p3 ).getFlags ( ); // invoke-virtual {p3}, Landroid/content/Intent;->getFlags()I
/* const v3, -0x8000001 */
/* and-int/2addr v2, v3 */
(( android.content.Intent ) p3 ).setFlags ( v2 ); // invoke-virtual {p3, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 69 */
/* const v2, 0x8000 */
(( android.content.Intent ) p3 ).addFlags ( v2 ); // invoke-virtual {p3, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 70 */
/* const/high16 v2, 0x80000 */
(( android.content.Intent ) p3 ).addFlags ( v2 ); // invoke-virtual {p3, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 71 */
final String v2 = "miui_launch_app_in_one_task_group"; // const-string v2, "miui_launch_app_in_one_task_group"
int v3 = 1; // const/4 v3, 0x1
(( android.content.Intent ) p3 ).putExtra ( v2, v3 ); // invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 72 */
final String v2 = "miui_task_return_to_target"; // const-string v2, "miui_task_return_to_target"
com.android.server.wm.MiuiMultiTaskManager$LaunchAppInfo .-$$Nest$fgetreturnTarget ( v0 );
(( android.content.Intent ) p3 ).putExtra ( v2, v3 ); // invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 76 */
} // :cond_2
/* .line 74 */
/* :catch_0 */
/* move-exception v2 */
/* .line 78 */
} // .end local v0 # "info":Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;
} // .end local v1 # "topr":Lcom/android/server/wm/ActivityRecord;
} // :cond_3
} // :goto_1
return;
} // .end method
