public class com.android.server.wm.MiuiFreeFormGestureController {
	 /* .source "MiuiFreeFormGestureController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
public static Boolean DEBUG;
public static final java.lang.String GB_BOOSTING;
private static final java.lang.String MIUI_OPTIMIZATION;
private static final java.lang.String TAG;
public static final java.lang.String VTB_BOOSTING;
/* # instance fields */
android.media.AudioManager mAudioManager;
com.android.server.wm.DisplayContent mDisplayContent;
private com.android.server.wm.MiuiFreeFormGestureController$FreeFormReceiver mFreeFormReceiver;
private android.database.ContentObserver mGameModeObserver;
android.os.Handler mHandler;
Boolean mIsGameMode;
Boolean mIsPortrait;
Boolean mIsVideoMode;
Integer mLastOrientation;
com.android.server.wm.MiuiFreeFormKeyCombinationHelper mMiuiFreeFormKeyCombinationHelper;
com.android.server.wm.MiuiFreeFormManagerService mMiuiFreeFormManagerService;
com.android.server.wm.MiuiMultiWindowRecommendHelper mMiuiMultiWindowRecommendHelper;
private android.database.ContentObserver mMiuiOptObserver;
com.android.server.wm.ActivityTaskManagerService mService;
com.android.server.wm.MiuiFreeformTrackManager mTrackManager;
private android.database.ContentObserver mVideoModeObserver;
/* # direct methods */
public static void $r8$lambda$CTIG1B2cu7ST6iU8pQ0hjvv3FGE ( com.android.server.wm.MiuiFreeFormGestureController p0, com.android.server.wm.Task p1, com.android.server.wm.MiuiFreeFormActivityStack p2, java.lang.String p3, java.lang.String p4, com.android.server.wm.ActivityRecord p5 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct/range {p0 ..p5}, Lcom/android/server/wm/MiuiFreeFormGestureController;->lambda$deliverResultForExitFreeform$0(Lcom/android/server/wm/Task;Lcom/android/server/wm/MiuiFreeFormActivityStack;Ljava/lang/String;Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;)V */
	 return;
} // .end method
static void -$$Nest$mupdateCtsMode ( com.android.server.wm.MiuiFreeFormGestureController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->updateCtsMode()V */
	 return;
} // .end method
static com.android.server.wm.MiuiFreeFormGestureController ( ) {
	 /* .locals 1 */
	 /* .line 81 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.android.server.wm.MiuiFreeFormGestureController.DEBUG = (v0!= 0);
	 return;
} // .end method
public com.android.server.wm.MiuiFreeFormGestureController ( ) {
	 /* .locals 4 */
	 /* .param p1, "service" # Lcom/android/server/wm/ActivityTaskManagerService; */
	 /* .param p2, "miuiFreeFormManagerService" # Lcom/android/server/wm/MiuiFreeFormManagerService; */
	 /* .param p3, "handler" # Landroid/os/Handler; */
	 /* .line 103 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 89 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormGestureController$FreeFormReceiver;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;)V */
	 this.mFreeFormReceiver = v0;
	 /* .line 96 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsVideoMode:Z */
	 /* .line 97 */
	 /* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsGameMode:Z */
	 /* .line 165 */
	 /* new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController$2; */
	 /* new-instance v2, Landroid/os/Handler; */
	 int v3 = 0; // const/4 v3, 0x0
	 /* invoke-direct {v2, v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V */
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController$2;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Landroid/os/Handler;)V */
	 this.mMiuiOptObserver = v1;
	 /* .line 193 */
	 /* new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController$3; */
	 /* new-instance v2, Landroid/os/Handler; */
	 /* invoke-direct {v2, v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V */
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController$3;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Landroid/os/Handler;)V */
	 this.mVideoModeObserver = v1;
	 /* .line 201 */
	 /* new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController$4; */
	 /* new-instance v2, Landroid/os/Handler; */
	 /* invoke-direct {v2, v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V */
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController$4;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Landroid/os/Handler;)V */
	 this.mGameModeObserver = v1;
	 /* .line 104 */
	 this.mService = p1;
	 /* .line 105 */
	 this.mHandler = p3;
	 /* .line 106 */
	 this.mMiuiFreeFormManagerService = p2;
	 /* .line 107 */
	 return;
} // .end method
private void clearFreeFormSurface ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
	 /* .locals 27 */
	 /* .param p1, "stack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
	 /* .line 287 */
	 /* move-object/from16 v0, p1 */
	 if ( v0 != null) { // if-eqz v0, :cond_9
		 v1 = this.mTask;
		 /* if-nez v1, :cond_0 */
		 /* goto/16 :goto_5 */
		 /* .line 288 */
	 } // :cond_0
	 v1 = this.mTask;
	 (( com.android.server.wm.Task ) v1 ).getSurfaceControl ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getSurfaceControl()Landroid/view/SurfaceControl;
	 /* .line 289 */
	 /* .local v1, "leash":Landroid/view/SurfaceControl; */
	 if ( v1 != null) { // if-eqz v1, :cond_8
		 v2 = 		 (( android.view.SurfaceControl ) v1 ).isValid ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl;->isValid()Z
		 /* if-nez v2, :cond_1 */
		 /* goto/16 :goto_4 */
		 /* .line 290 */
	 } // :cond_1
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "clear freeform surface: taskId="; // const-string v3, "clear freeform surface: taskId="
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v3 = this.mTask;
	 /* iget v3, v3, Lcom/android/server/wm/Task;->mTaskId:I */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "MiuiFreeFormGestureController"; // const-string v3, "MiuiFreeFormGestureController"
	 android.util.Slog .d ( v3,v2 );
	 /* .line 291 */
	 /* new-instance v2, Landroid/view/SurfaceControl$Transaction; */
	 /* invoke-direct {v2}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
	 /* move-object v14, v2 */
	 /* .line 292 */
	 /* .local v14, "tx":Landroid/view/SurfaceControl$Transaction; */
	 int v15 = 0; // const/4 v15, 0x0
	 (( android.view.SurfaceControl$Transaction ) v14 ).setPosition ( v1, v15, v15 ); // invoke-virtual {v14, v1, v15, v15}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;
	 /* .line 293 */
	 /* const/high16 v2, 0x3f800000 # 1.0f */
	 (( android.view.SurfaceControl$Transaction ) v14 ).setScale ( v1, v2, v2 ); // invoke-virtual {v14, v1, v2, v2}, Landroid/view/SurfaceControl$Transaction;->setScale(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;
	 /* .line 294 */
	 (( android.view.SurfaceControl$Transaction ) v14 ).setAlpha ( v1, v2 ); // invoke-virtual {v14, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
	 /* .line 295 */
	 int v13 = 0; // const/4 v13, 0x0
	 (( android.view.SurfaceControl$Transaction ) v14 ).setWindowCrop ( v1, v13, v13 ); // invoke-virtual {v14, v1, v13, v13}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;
	 /* .line 296 */
	 (( android.view.SurfaceControl$Transaction ) v14 ).setCornerRadius ( v1, v15 ); // invoke-virtual {v14, v1, v15}, Landroid/view/SurfaceControl$Transaction;->setCornerRadius(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
	 /* .line 297 */
	 int v12 = 3; // const/4 v12, 0x3
	 /* new-array v2, v12, [F */
	 /* fill-array-data v2, :array_0 */
	 (( android.view.SurfaceControl$Transaction ) v14 ).setSurfaceStroke ( v1, v2, v15, v15 ); // invoke-virtual {v14, v1, v2, v15, v15}, Landroid/view/SurfaceControl$Transaction;->setSurfaceStroke(Landroid/view/SurfaceControl;[FFF)Landroid/view/SurfaceControl$Transaction;
	 /* .line 299 */
	 /* const/high16 v2, -0x3ec00000 # -12.0f */
	 v16 = 	 android.util.MiuiMultiWindowUtils .applyDip2Px ( v2 );
	 /* .line 300 */
	 /* .local v16, "cornerX":F */
	 /* new-array v5, v12, [F */
	 /* fill-array-data v5, :array_1 */
	 /* .line 301 */
	 /* .local v5, "tipsColors":[F */
	 /* const/high16 v2, 0x41900000 # 18.0f */
	 v17 = 	 android.util.MiuiMultiWindowUtils .applyDip2Px ( v2 );
	 /* .line 302 */
	 /* .local v17, "radius":F */
	 /* const/high16 v2, 0x40800000 # 4.0f */
	 v18 = 	 android.util.MiuiMultiWindowUtils .applyDip2Px ( v2 );
	 /* .line 303 */
	 /* .local v18, "thickness":F */
	 int v6 = 0; // const/4 v6, 0x0
	 /* const/high16 v9, 0x42580000 # 54.0f */
	 /* move-object v2, v14 */
	 /* move-object v3, v1 */
	 /* move/from16 v4, v16 */
	 /* move/from16 v7, v18 */
	 /* move/from16 v8, v17 */
	 /* invoke-virtual/range {v2 ..v9}, Landroid/view/SurfaceControl$Transaction;->setLeftBottomCornerTip(Landroid/view/SurfaceControl;F[FFFFF)Landroid/view/SurfaceControl$Transaction; */
	 /* .line 304 */
	 int v10 = 0; // const/4 v10, 0x0
	 /* const/high16 v2, 0x42580000 # 54.0f */
	 /* move-object v6, v14 */
	 /* move-object v7, v1 */
	 /* move/from16 v8, v16 */
	 /* move-object v9, v5 */
	 /* move/from16 v11, v18 */
	 /* move v3, v12 */
	 /* move/from16 v12, v17 */
	 /* move v4, v13 */
	 /* move v13, v2 */
	 /* invoke-virtual/range {v6 ..v13}, Landroid/view/SurfaceControl$Transaction;->setRightBottomCornerTip(Landroid/view/SurfaceControl;F[FFFFF)Landroid/view/SurfaceControl$Transaction; */
	 /* .line 306 */
	 v2 = 	 android.util.MiuiMultiWindowUtils .isSupportMiuiShadowV2 ( );
	 int v6 = 1; // const/4 v6, 0x1
	 if ( v2 != null) { // if-eqz v2, :cond_6
		 /* .line 307 */
		 /* const-class v19, Landroid/view/SurfaceControl; */
		 v20 = java.lang.Integer.TYPE;
		 v21 = java.lang.Float.TYPE;
		 v22 = java.lang.Float.TYPE;
		 v23 = java.lang.Float.TYPE;
		 v24 = java.lang.Float.TYPE;
		 v25 = java.lang.Float.TYPE;
		 /* const-class v26, Landroid/graphics/RectF; */
		 /* filled-new-array/range {v19 ..v26}, [Ljava/lang/Class; */
		 /* .line 309 */
		 /* .local v2, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
		 /* const/16 v7, 0x8 */
		 /* new-array v7, v7, [Ljava/lang/Object; */
		 /* aput-object v1, v7, v4 */
		 java.lang.Integer .valueOf ( v4 );
		 /* aput-object v4, v7, v6 */
		 int v4 = 2; // const/4 v4, 0x2
		 java.lang.Float .valueOf ( v15 );
		 /* aput-object v6, v7, v4 */
		 java.lang.Float .valueOf ( v15 );
		 /* aput-object v4, v7, v3 */
		 int v3 = 4; // const/4 v3, 0x4
		 java.lang.Float .valueOf ( v15 );
		 /* aput-object v4, v7, v3 */
		 int v3 = 5; // const/4 v3, 0x5
		 java.lang.Float .valueOf ( v15 );
		 /* aput-object v4, v7, v3 */
		 /* .line 310 */
		 v3 = 		 /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z */
		 /* if-nez v3, :cond_4 */
		 v3 = 		 /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inNormalPinMode()Z */
		 if ( v3 != null) { // if-eqz v3, :cond_2
			 /* .line 311 */
		 } // :cond_2
		 v3 = 		 /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z */
		 /* if-nez v3, :cond_3 */
		 v3 = 		 /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inMiniPinMode()Z */
		 if ( v3 != null) { // if-eqz v3, :cond_5
		 } // :cond_3
		 /* .line 310 */
	 } // :cond_4
} // :goto_0
} // :cond_5
} // :goto_1
java.lang.Float .valueOf ( v15 );
int v4 = 6; // const/4 v4, 0x6
/* aput-object v3, v7, v4 */
/* new-instance v3, Landroid/graphics/RectF; */
/* invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V */
int v4 = 7; // const/4 v4, 0x7
/* aput-object v3, v7, v4 */
/* move-object v3, v7 */
/* .line 313 */
/* .local v3, "values":[Ljava/lang/Object; */
/* const-class v4, Landroid/view/SurfaceControl$Transaction; */
/* const-string/jumbo v6, "setMiShadow" */
android.util.MiuiMultiWindowUtils .callObjectMethod ( v14,v4,v6,v2,v3 );
} // .end local v2 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // .end local v3 # "values":[Ljava/lang/Object;
/* .line 315 */
} // :cond_6
v2 = android.util.MiuiMultiWindowUtils .isSupportMiuiShadowV1 ( );
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 316 */
/* const-class v7, Landroid/view/SurfaceControl; */
v8 = java.lang.Float.TYPE;
/* const-class v9, [F */
v10 = java.lang.Float.TYPE;
v11 = java.lang.Float.TYPE;
v12 = java.lang.Float.TYPE;
v13 = java.lang.Integer.TYPE;
/* filled-new-array/range {v7 ..v13}, [Ljava/lang/Class; */
/* .line 318 */
/* .restart local v2 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
java.lang.Float .valueOf ( v15 );
v8 = android.util.MiuiMultiWindowUtils.MIUI_FREEFORM_RESET_COLOR;
/* .line 319 */
java.lang.Integer .valueOf ( v4 );
java.lang.Integer .valueOf ( v4 );
java.lang.Integer .valueOf ( v4 );
java.lang.Integer .valueOf ( v6 );
/* move-object v6, v1 */
/* filled-new-array/range {v6 ..v12}, [Ljava/lang/Object; */
/* .line 320 */
/* .restart local v3 # "values":[Ljava/lang/Object; */
/* const-class v4, Landroid/view/SurfaceControl$Transaction; */
/* const-string/jumbo v6, "setShadowSettings" */
android.util.MiuiMultiWindowUtils .callObjectMethod ( v14,v4,v6,v2,v3 );
/* .line 315 */
} // .end local v2 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // .end local v3 # "values":[Ljava/lang/Object;
} // :cond_7
} // :goto_2
/* nop */
/* .line 324 */
} // :goto_3
(( android.view.SurfaceControl$Transaction ) v14 ).apply ( ); // invoke-virtual {v14}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 325 */
return;
/* .line 289 */
} // .end local v5 # "tipsColors":[F
} // .end local v14 # "tx":Landroid/view/SurfaceControl$Transaction;
} // .end local v16 # "cornerX":F
} // .end local v17 # "radius":F
} // .end local v18 # "thickness":F
} // :cond_8
} // :goto_4
return;
/* .line 287 */
} // .end local v1 # "leash":Landroid/view/SurfaceControl;
} // :cond_9
} // :goto_5
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x3dc8b439 */
/* 0x3dc8b439 */
/* 0x3dc8b439 */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x3d978d50 # 0.074f */
/* 0x3d978d50 # 0.074f */
/* 0x3d978d50 # 0.074f */
} // .end array-data
} // .end method
private void exitFreeForm ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 4 */
/* .param p1, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 328 */
final String v0 = "MiuiFreeFormGestureController"; // const-string v0, "MiuiFreeFormGestureController"
final String v1 = "exitFreeForm"; // const-string v1, "exitFreeForm"
android.util.Slog .d ( v0,v1 );
/* .line 329 */
v0 = this.mService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 330 */
if ( p1 != null) { // if-eqz p1, :cond_1
try { // :try_start_0
v1 = this.mTask;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 331 */
v1 = this.mTask;
(( com.android.server.wm.Task ) v1 ).getBaseIntent ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 332 */
v1 = this.mTask;
(( com.android.server.wm.Task ) v1 ).getBaseIntent ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
v2 = this.mTask;
(( com.android.server.wm.Task ) v2 ).getBaseIntent ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
v2 = (( android.content.Intent ) v2 ).getMiuiFlags ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getMiuiFlags()I
/* and-int/lit16 v2, v2, -0x101 */
(( android.content.Intent ) v1 ).setMiuiFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setMiuiFlags(I)Landroid/content/Intent;
/* .line 334 */
} // :cond_0
final String v1 = "MiuiFreeFormGestureController"; // const-string v1, "MiuiFreeFormGestureController"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "exitFreeForm freeform stack :"; // const-string v3, "exitFreeForm freeform stack :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mTask;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 335 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->moveFreeformToFullScreen(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V */
/* .line 336 */
v1 = this.mMiuiFreeFormManagerService;
v2 = this.mTask;
/* iget v2, v2, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).onExitFreeform ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->onExitFreeform(I)V
/* .line 338 */
} // :cond_1
/* monitor-exit v0 */
/* .line 339 */
return;
/* .line 338 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static Integer getDisplayCutoutHeight ( com.android.server.wm.DisplayFrames p0 ) {
/* .locals 4 */
/* .param p0, "displayFrames" # Lcom/android/server/wm/DisplayFrames; */
/* .line 672 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 673 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 674 */
/* .local v0, "displayCutoutHeight":I */
v1 = this.mInsetsState;
(( android.view.InsetsState ) v1 ).getDisplayCutout ( ); // invoke-virtual {v1}, Landroid/view/InsetsState;->getDisplayCutout()Landroid/view/DisplayCutout;
/* .line 675 */
/* .local v1, "cutout":Landroid/view/DisplayCutout; */
/* iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I */
/* if-nez v2, :cond_1 */
/* .line 676 */
v0 = (( android.view.DisplayCutout ) v1 ).getSafeInsetTop ( ); // invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetTop()I
/* .line 677 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 678 */
v0 = (( android.view.DisplayCutout ) v1 ).getSafeInsetBottom ( ); // invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetBottom()I
/* .line 679 */
} // :cond_2
/* iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_3 */
/* .line 680 */
v0 = (( android.view.DisplayCutout ) v1 ).getSafeInsetLeft ( ); // invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetLeft()I
/* .line 681 */
} // :cond_3
/* iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v2, v3, :cond_4 */
/* .line 682 */
v0 = (( android.view.DisplayCutout ) v1 ).getSafeInsetRight ( ); // invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetRight()I
/* .line 684 */
} // :cond_4
} // :goto_0
/* sget-boolean v2, Lcom/android/server/wm/MiuiFreeFormGestureController;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 685 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getDisplayCutoutHeight displayCutoutHeight="; // const-string v3, "getDisplayCutoutHeight displayCutoutHeight="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiFreeFormGestureController"; // const-string v3, "MiuiFreeFormGestureController"
android.util.Slog .d ( v3,v2 );
/* .line 687 */
} // :cond_5
} // .end method
public static Integer getNavBarHeight ( com.android.server.wm.InsetsStateController p0 ) {
/* .locals 1 */
/* .param p0, "insetsStateController" # Lcom/android/server/wm/InsetsStateController; */
/* .line 644 */
int v0 = 1; // const/4 v0, 0x1
v0 = com.android.server.wm.MiuiFreeFormGestureController .getNavBarHeight ( p0,v0 );
} // .end method
public static Integer getNavBarHeight ( com.android.server.wm.InsetsStateController p0, Boolean p1 ) {
/* .locals 7 */
/* .param p0, "insetsStateController" # Lcom/android/server/wm/InsetsStateController; */
/* .param p1, "ignoreVisibility" # Z */
/* .line 648 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 649 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 650 */
/* .local v0, "navBarHeight":I */
/* nop */
/* .line 651 */
(( com.android.server.wm.InsetsStateController ) p0 ).getSourceProviders ( ); // invoke-virtual {p0}, Lcom/android/server/wm/InsetsStateController;->getSourceProviders()Landroid/util/SparseArray;
/* .line 652 */
/* .local v1, "providers":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/wm/InsetsSourceProvider;>;" */
v2 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_4 */
/* .line 653 */
(( android.util.SparseArray ) v1 ).valueAt ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/InsetsSourceProvider; */
/* .line 654 */
/* .local v3, "provider":Lcom/android/server/wm/InsetsSourceProvider; */
(( com.android.server.wm.InsetsSourceProvider ) v3 ).getSource ( ); // invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;
v4 = (( android.view.InsetsSource ) v4 ).getType ( ); // invoke-virtual {v4}, Landroid/view/InsetsSource;->getType()I
v5 = android.view.WindowInsets$Type .navigationBars ( );
/* if-ne v4, v5, :cond_1 */
/* .line 655 */
/* .line 657 */
} // :cond_1
/* if-nez p1, :cond_2 */
(( com.android.server.wm.InsetsSourceProvider ) v3 ).getSource ( ); // invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;
v4 = (( android.view.InsetsSource ) v4 ).isVisible ( ); // invoke-virtual {v4}, Landroid/view/InsetsSource;->isVisible()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 658 */
} // :cond_2
(( com.android.server.wm.InsetsSourceProvider ) v3 ).getSource ( ); // invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;
(( android.view.InsetsSource ) v4 ).getFrame ( ); // invoke-virtual {v4}, Landroid/view/InsetsSource;->getFrame()Landroid/graphics/Rect;
/* .line 659 */
/* .local v4, "frame":Landroid/graphics/Rect; */
if ( v4 != null) { // if-eqz v4, :cond_3
v5 = (( android.graphics.Rect ) v4 ).isEmpty ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v5, :cond_3 */
/* .line 660 */
v5 = (( android.graphics.Rect ) v4 ).height ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->height()I
v6 = (( android.graphics.Rect ) v4 ).width ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->width()I
v0 = java.lang.Math .min ( v5,v6 );
/* .line 652 */
} // .end local v3 # "provider":Lcom/android/server/wm/InsetsSourceProvider;
} // .end local v4 # "frame":Landroid/graphics/Rect;
} // :cond_3
} // :goto_1
/* add-int/lit8 v2, v2, -0x1 */
/* .line 665 */
} // .end local v2 # "i":I
} // :cond_4
/* sget-boolean v2, Lcom/android/server/wm/MiuiFreeFormGestureController;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 666 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getNavBarHeight navBarHeight="; // const-string v3, "getNavBarHeight navBarHeight="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiFreeFormGestureController"; // const-string v3, "MiuiFreeFormGestureController"
android.util.Slog .d ( v3,v2 );
/* .line 668 */
} // :cond_5
} // .end method
public static Integer getStatusBarHeight ( com.android.server.wm.InsetsStateController p0 ) {
/* .locals 1 */
/* .param p0, "insetsStateController" # Lcom/android/server/wm/InsetsStateController; */
/* .line 616 */
int v0 = 1; // const/4 v0, 0x1
v0 = com.android.server.wm.MiuiFreeFormGestureController .getStatusBarHeight ( p0,v0 );
} // .end method
public static Integer getStatusBarHeight ( com.android.server.wm.InsetsStateController p0, Boolean p1 ) {
/* .locals 7 */
/* .param p0, "insetsStateController" # Lcom/android/server/wm/InsetsStateController; */
/* .param p1, "ignoreVisibility" # Z */
/* .line 620 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 621 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 622 */
/* .local v0, "statusBarHeight":I */
/* nop */
/* .line 623 */
(( com.android.server.wm.InsetsStateController ) p0 ).getSourceProviders ( ); // invoke-virtual {p0}, Lcom/android/server/wm/InsetsStateController;->getSourceProviders()Landroid/util/SparseArray;
/* .line 624 */
/* .local v1, "providers":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/wm/InsetsSourceProvider;>;" */
v2 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_4 */
/* .line 625 */
(( android.util.SparseArray ) v1 ).valueAt ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/InsetsSourceProvider; */
/* .line 626 */
/* .local v3, "provider":Lcom/android/server/wm/InsetsSourceProvider; */
(( com.android.server.wm.InsetsSourceProvider ) v3 ).getSource ( ); // invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;
v4 = (( android.view.InsetsSource ) v4 ).getType ( ); // invoke-virtual {v4}, Landroid/view/InsetsSource;->getType()I
v5 = android.view.WindowInsets$Type .statusBars ( );
/* if-ne v4, v5, :cond_1 */
/* .line 627 */
/* .line 629 */
} // :cond_1
/* if-nez p1, :cond_2 */
(( com.android.server.wm.InsetsSourceProvider ) v3 ).getSource ( ); // invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;
v4 = (( android.view.InsetsSource ) v4 ).isVisible ( ); // invoke-virtual {v4}, Landroid/view/InsetsSource;->isVisible()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 630 */
} // :cond_2
(( com.android.server.wm.InsetsSourceProvider ) v3 ).getSource ( ); // invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;
(( android.view.InsetsSource ) v4 ).getFrame ( ); // invoke-virtual {v4}, Landroid/view/InsetsSource;->getFrame()Landroid/graphics/Rect;
/* .line 631 */
/* .local v4, "frame":Landroid/graphics/Rect; */
if ( v4 != null) { // if-eqz v4, :cond_3
v5 = (( android.graphics.Rect ) v4 ).isEmpty ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z
/* if-nez v5, :cond_3 */
/* .line 632 */
v5 = (( android.graphics.Rect ) v4 ).height ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->height()I
v6 = (( android.graphics.Rect ) v4 ).width ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->width()I
v0 = java.lang.Math .min ( v5,v6 );
/* .line 624 */
} // .end local v3 # "provider":Lcom/android/server/wm/InsetsSourceProvider;
} // .end local v4 # "frame":Landroid/graphics/Rect;
} // :cond_3
} // :goto_1
/* add-int/lit8 v2, v2, -0x1 */
/* .line 637 */
} // .end local v2 # "i":I
} // :cond_4
/* sget-boolean v2, Lcom/android/server/wm/MiuiFreeFormGestureController;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 638 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getStatusBarHeight statusBarHeight="; // const-string v3, "getStatusBarHeight statusBarHeight="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiFreeFormGestureController"; // const-string v3, "MiuiFreeFormGestureController"
android.util.Slog .d ( v3,v2 );
/* .line 640 */
} // :cond_5
} // .end method
private void hide ( com.android.server.wm.WindowContainer p0 ) {
/* .locals 3 */
/* .param p1, "wc" # Lcom/android/server/wm/WindowContainer; */
/* .line 259 */
/* if-nez p1, :cond_0 */
return;
/* .line 260 */
} // :cond_0
v0 = this.mSurfaceControl;
/* .line 261 */
/* .local v0, "sc":Landroid/view/SurfaceControl; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( android.view.SurfaceControl ) v0 ).isValid ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 262 */
/* new-instance v1, Landroid/view/SurfaceControl$Transaction; */
/* invoke-direct {v1}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
/* .line 263 */
/* .local v1, "t":Landroid/view/SurfaceControl$Transaction; */
int v2 = 0; // const/4 v2, 0x0
(( android.view.SurfaceControl$Transaction ) v1 ).setCrop ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/view/SurfaceControl$Transaction;->setCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
/* .line 264 */
(( android.view.SurfaceControl$Transaction ) v1 ).hide ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 265 */
(( android.view.SurfaceControl$Transaction ) v1 ).apply ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 267 */
} // .end local v1 # "t":Landroid/view/SurfaceControl$Transaction;
} // :cond_1
return;
} // .end method
private void hideStack ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 2 */
/* .param p1, "stack" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 250 */
v0 = this.mService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 251 */
if ( p1 != null) { // if-eqz p1, :cond_1
try { // :try_start_0
v1 = this.mTask;
/* if-nez v1, :cond_0 */
/* .line 254 */
} // :cond_0
v1 = this.mTask;
/* invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->hide(Lcom/android/server/wm/WindowContainer;)V */
/* .line 255 */
/* monitor-exit v0 */
/* .line 256 */
return;
/* .line 252 */
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
return;
/* .line 255 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isPlaybackActive ( java.lang.String p0, Integer p1 ) {
/* .locals 16 */
/* .param p1, "curPkg" # Ljava/lang/String; */
/* .param p2, "curUid" # I */
/* .line 723 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move/from16 v3, p2 */
v0 = this.mAudioManager;
final String v4 = "audio_active_track"; // const-string v4, "audio_active_track"
(( android.media.AudioManager ) v0 ).getParameters ( v4 ); // invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;
/* .line 724 */
/* .local v4, "activeTrackString":Ljava/lang/String; */
/* const/16 v0, 0x3b */
/* const/16 v5, 0x2c */
(( java.lang.String ) v4 ).replace ( v0, v5 ); // invoke-virtual {v4, v0, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;
final String v5 = ","; // const-string v5, ","
(( java.lang.String ) v0 ).split ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 725 */
/* .local v5, "activeTrackArray":[Ljava/lang/String; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "isPlaybackActive: activeTrackArray[PID/UID]:"; // const-string v6, "isPlaybackActive: activeTrackArray[PID/UID]:"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v5 );
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " curPkg:"; // const-string v6, " curPkg:"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " curUid:"; // const-string v6, " curUid:"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "MiuiFreeFormGestureController"; // const-string v7, "MiuiFreeFormGestureController"
android.util.Slog .d ( v7,v0 );
/* .line 726 */
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 727 */
/* .local v8, "pm":Landroid/content/pm/PackageManager; */
/* array-length v9, v5 */
int v10 = 0; // const/4 v10, 0x0
/* move v11, v10 */
} // :goto_0
/* if-ge v11, v9, :cond_3 */
/* aget-object v12, v5, v11 */
/* .line 728 */
/* .local v12, "str":Ljava/lang/String; */
final String v0 = "/"; // const-string v0, "/"
v13 = (( java.lang.String ) v12 ).contains ( v0 ); // invoke-virtual {v12, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v13, :cond_0 */
/* .line 729 */
} // :cond_0
(( java.lang.String ) v12 ).split ( v0 ); // invoke-virtual {v12, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 731 */
/* .local v13, "split":[Ljava/lang/String; */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* aget-object v14, v13, v0 */
v14 = java.lang.Integer .parseInt ( v14 );
/* .line 732 */
/* .local v14, "uid":I */
(( android.content.pm.PackageManager ) v8 ).getPackagesForUid ( v14 ); // invoke-virtual {v8, v14}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 733 */
/* .local v15, "packageNames":[Ljava/lang/String; */
/* if-eq v3, v14, :cond_2 */
int v0 = -1; // const/4 v0, -0x1
/* if-ne v3, v0, :cond_1 */
/* aget-object v0, v15, v10 */
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 737 */
} // .end local v14 # "uid":I
} // .end local v15 # "packageNames":[Ljava/lang/String;
} // :cond_1
/* .line 734 */
/* .restart local v14 # "uid":I */
/* .restart local v15 # "packageNames":[Ljava/lang/String; */
} // :cond_2
} // :goto_1
int v0 = 1; // const/4 v0, 0x1
/* .line 735 */
} // .end local v14 # "uid":I
} // .end local v15 # "packageNames":[Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 736 */
/* .local v0, "e":Ljava/lang/Exception; */
/* nop */
/* .line 727 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v12 # "str":Ljava/lang/String;
} // .end local v13 # "split":[Ljava/lang/String;
} // :goto_2
/* add-int/lit8 v11, v11, 0x1 */
/* .line 739 */
} // :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "isPlaybackActive:false curPkg:"; // const-string v9, "isPlaybackActive:false curPkg:"
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v0 );
/* .line 740 */
} // .end method
private void lambda$deliverResultForExitFreeform$0 ( com.android.server.wm.Task p0, com.android.server.wm.MiuiFreeFormActivityStack p1, java.lang.String p2, java.lang.String p3, com.android.server.wm.ActivityRecord p4 ) { //synthethic
/* .locals 10 */
/* .param p1, "currentFullRootTask" # Lcom/android/server/wm/Task; */
/* .param p2, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .param p3, "DELIVER_RESULT_REASON" # Ljava/lang/String; */
/* .param p4, "EXIT_FREEFORM" # Ljava/lang/String; */
/* .param p5, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 405 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "MiuiFreeFormGestureController"; // const-string v1, "MiuiFreeFormGestureController"
if ( p5 != null) { // if-eqz p5, :cond_0
v2 = this.resultTo;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.resultTo;
v2 = this.app;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 406 */
v2 = this.resultTo;
/* .line 408 */
/* .local v2, "resultTo":Lcom/android/server/wm/ActivityRecord; */
try { // :try_start_0
v5 = this.resultWho;
/* iget v6, v2, Lcom/android/server/wm/ActivityRecord;->requestCode:I */
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v2 */
/* move-object v4, p5 */
/* invoke-virtual/range {v3 ..v8}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V */
/* .line 410 */
v3 = this.app;
/* .line 411 */
(( com.android.server.wm.WindowProcessController ) v3 ).getThread ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
v4 = this.token;
android.app.servertransaction.ClientTransaction .obtain ( v3,v4 );
/* .line 412 */
/* .local v3, "transaction":Landroid/app/servertransaction/ClientTransaction; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " deliverResultForExitFreeform: "; // const-string v5, " deliverResultForExitFreeform: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p5 ); // invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " delivering results to "; // const-string v5, " delivering results to "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = "results: "; // const-string v5, "results: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.results;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 414 */
v4 = this.results;
android.app.servertransaction.ActivityResultItem .obtain ( v4 );
(( android.app.servertransaction.ClientTransaction ) v3 ).addCallback ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 415 */
v4 = this.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v4 ).getLifecycleManager ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;
(( com.android.server.wm.ClientLifecycleManager ) v4 ).scheduleTransaction ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V
/* .line 416 */
this.results = v0;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 419 */
} // .end local v3 # "transaction":Landroid/app/servertransaction/ClientTransaction;
/* .line 417 */
/* :catch_0 */
/* move-exception v0 */
/* .line 418 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " deliverResultForExitFreeform: fail "; // const-string v4, " deliverResultForExitFreeform: fail "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 420 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // .end local v2 # "resultTo":Lcom/android/server/wm/ActivityRecord;
} // :goto_0
/* goto/16 :goto_2 */
/* .line 423 */
} // :cond_0
v2 = (( com.android.server.wm.MiuiFreeFormGestureController ) p0 ).isInVideoOrGameScene ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z
if ( v2 != null) { // if-eqz v2, :cond_5
if ( p5 != null) { // if-eqz p5, :cond_5
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 424 */
(( com.android.server.wm.Task ) p1 ).getTopNonFinishingActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 425 */
/* .restart local v2 # "resultTo":Lcom/android/server/wm/ActivityRecord; */
if ( v2 != null) { // if-eqz v2, :cond_4
v3 = this.app;
/* if-nez v3, :cond_1 */
/* goto/16 :goto_1 */
/* .line 429 */
} // :cond_1
try { // :try_start_1
v5 = this.resultWho;
/* iget v6, v2, Lcom/android/server/wm/ActivityRecord;->requestCode:I */
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v2 */
/* move-object v4, p5 */
/* invoke-virtual/range {v3 ..v8}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V */
/* .line 431 */
v3 = this.app;
/* .line 432 */
(( com.android.server.wm.WindowProcessController ) v3 ).getThread ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
v4 = this.token;
android.app.servertransaction.ClientTransaction .obtain ( v3,v4 );
/* .line 433 */
/* .restart local v3 # "transaction":Landroid/app/servertransaction/ClientTransaction; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " deliverResultForExitFreeform: delivering results to fullscreen activity: "; // const-string v5, " deliverResultForExitFreeform: delivering results to fullscreen activity: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 434 */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 435 */
/* .local v4, "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;" */
/* new-instance v5, Landroid/content/Intent; */
/* invoke-direct {v5}, Landroid/content/Intent;-><init>()V */
/* .line 436 */
/* .local v5, "data":Landroid/content/Intent; */
final String v6 = "com.babycloud.hanju/com.tencent.connect.common.AssistActivity"; // const-string v6, "com.babycloud.hanju/com.tencent.connect.common.AssistActivity"
v7 = this.shortComponentName;
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
final String v6 = "com.tencent.mobileqq"; // const-string v6, "com.tencent.mobileqq"
/* .line 437 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p2 ).getStackPackageName ( ); // invoke-virtual {p2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_3 */
/* .line 438 */
} // :cond_2
(( android.content.Intent ) v5 ).putExtra ( p3, p4 ); // invoke-virtual {v5, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 440 */
} // :cond_3
/* new-instance v6, Landroid/app/ResultInfo; */
v7 = this.resultWho;
/* iget v8, v2, Lcom/android/server/wm/ActivityRecord;->requestCode:I */
int v9 = 0; // const/4 v9, 0x0
/* invoke-direct {v6, v7, v8, v9, v5}, Landroid/app/ResultInfo;-><init>(Ljava/lang/String;IILandroid/content/Intent;)V */
(( java.util.ArrayList ) v4 ).add ( v6 ); // invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 441 */
android.app.servertransaction.ActivityResultItem .obtain ( v4 );
(( android.app.servertransaction.ClientTransaction ) v3 ).addCallback ( v6 ); // invoke-virtual {v3, v6}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 442 */
v6 = this.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v6 ).getLifecycleManager ( ); // invoke-virtual {v6}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;
(( com.android.server.wm.ClientLifecycleManager ) v6 ).scheduleTransaction ( v3 ); // invoke-virtual {v6, v3}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V
/* .line 443 */
this.results = v0;
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 446 */
} // .end local v3 # "transaction":Landroid/app/servertransaction/ClientTransaction;
} // .end local v4 # "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
} // .end local v5 # "data":Landroid/content/Intent;
/* .line 444 */
/* :catch_1 */
/* move-exception v0 */
/* .line 445 */
/* .restart local v0 # "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " deliverResultForExitFreeform: fail to fullscreen activity "; // const-string v4, " deliverResultForExitFreeform: fail to fullscreen activity "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 426 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_4
} // :goto_1
return;
/* .line 449 */
} // .end local v2 # "resultTo":Lcom/android/server/wm/ActivityRecord;
} // :cond_5
} // :goto_2
return;
} // .end method
static Boolean lambda$deliverResultForFinishActivity$1 ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "resultFrom" # Lcom/android/server/wm/ActivityRecord; */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 479 */
/* iget-boolean v0, p1, Lcom/android/server/wm/ActivityRecord;->finishing:Z */
/* if-nez v0, :cond_0 */
/* .line 480 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).getLaunchedFromPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getLaunchedFromPid()I
v1 = (( com.android.server.wm.ActivityRecord ) p0 ).getPid ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->getPid()I
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 479 */
} // :goto_0
} // .end method
private void moveFreeformToFullScreen ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 8 */
/* .param p1, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 565 */
v0 = this.mTask;
/* .line 566 */
/* .local v0, "rootTask":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_3
(( com.android.server.wm.Task ) v0 ).getDisplayArea ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* if-nez v1, :cond_0 */
/* goto/16 :goto_1 */
/* .line 567 */
} // :cond_0
v1 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = (( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).inPinMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->inPinMode()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 568 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "moveFreeformToFullScreen::::::remove pin task ="; // const-string v2, "moveFreeformToFullScreen::::::remove pin task ="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFreeFormGestureController"; // const-string v2, "MiuiFreeFormGestureController"
android.util.Slog .i ( v2,v1 );
/* .line 569 */
v1 = this.mService;
v1 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v1 ).removeRootTask ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/ActivityTaskSupervisor;->removeRootTask(Lcom/android/server/wm/Task;)V
/* .line 570 */
return;
/* .line 572 */
} // :cond_1
v1 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 573 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).setIsFrontFreeFormStackInfo ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setIsFrontFreeFormStackInfo(Z)V
/* .line 574 */
v1 = this.mMiuiFreeFormManagerService;
int v3 = 3; // const/4 v3, 0x3
(( com.android.server.wm.MiuiFreeFormManagerService ) v1 ).dispatchFreeFormStackModeChanged ( v3, p1 ); // invoke-virtual {v1, v3, p1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->dispatchFreeFormStackModeChanged(ILcom/android/server/wm/MiuiFreeFormActivityStack;)V
/* .line 577 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.wm.Task ) v0 ).setForceHidden ( v1, v1 ); // invoke-virtual {v0, v1, v1}, Lcom/android/server/wm/Task;->setForceHidden(IZ)Z
/* .line 578 */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.wm.Task ) v0 ).ensureActivitiesVisible ( v3, v2, v1 ); // invoke-virtual {v0, v3, v2, v1}, Lcom/android/server/wm/Task;->ensureActivitiesVisible(Lcom/android/server/wm/ActivityRecord;IZ)V
/* .line 579 */
v4 = this.mService;
v4 = this.mTaskSupervisor;
(( com.android.server.wm.ActivityTaskSupervisor ) v4 ).activityIdleInternal ( v3, v2, v1, v3 ); // invoke-virtual {v4, v3, v2, v1, v3}, Lcom/android/server/wm/ActivityTaskSupervisor;->activityIdleInternal(Lcom/android/server/wm/ActivityRecord;ZZLandroid/content/res/Configuration;)V
/* .line 581 */
v4 = this.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v4 ).deferWindowLayout ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskManagerService;->deferWindowLayout()V
/* .line 583 */
try { // :try_start_0
/* filled-new-array {v0}, [Lcom/android/server/wm/Task; */
com.google.android.collect.Sets .newArraySet ( v4 );
/* .line 584 */
/* .local v4, "tasks":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/Task;>;" */
v5 = this.mService;
v5 = this.mWindowManager;
v5 = this.mTaskSnapshotController;
(( com.android.server.wm.TaskSnapshotController ) v5 ).snapshotTasks ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/wm/TaskSnapshotController;->snapshotTasks(Landroid/util/ArraySet;)V
/* .line 585 */
v5 = this.mService;
v5 = this.mWindowManager;
v5 = this.mTaskSnapshotController;
(( com.android.server.wm.TaskSnapshotController ) v5 ).addSkipClosingAppSnapshotTasks ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/wm/TaskSnapshotController;->addSkipClosingAppSnapshotTasks(Ljava/util/Set;)V
/* .line 592 */
/* new-instance v5, Landroid/content/res/Configuration; */
(( com.android.server.wm.Task ) v0 ).getRequestedOverrideConfiguration ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRequestedOverrideConfiguration()Landroid/content/res/Configuration;
/* invoke-direct {v5, v6}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V */
/* .line 593 */
/* .local v5, "c":Landroid/content/res/Configuration; */
v6 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v6 ).setAlwaysOnTop ( v2 ); // invoke-virtual {v6, v2}, Landroid/app/WindowConfiguration;->setAlwaysOnTop(Z)V
/* .line 594 */
v6 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v6 ).setWindowingMode ( v2 ); // invoke-virtual {v6, v2}, Landroid/app/WindowConfiguration;->setWindowingMode(I)V
/* .line 595 */
v6 = this.windowConfiguration;
(( android.app.WindowConfiguration ) v6 ).setBounds ( v3 ); // invoke-virtual {v6, v3}, Landroid/app/WindowConfiguration;->setBounds(Landroid/graphics/Rect;)V
/* .line 596 */
(( com.android.server.wm.Task ) v0 ).onRequestedOverrideConfigurationChanged ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/wm/Task;->onRequestedOverrideConfigurationChanged(Landroid/content/res/Configuration;)V
/* .line 598 */
(( com.android.server.wm.Task ) v0 ).getDisplayArea ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 603 */
/* .local v6, "taskDisplayArea":Lcom/android/server/wm/TaskDisplayArea; */
(( com.android.server.wm.TaskDisplayArea ) v6 ).positionTaskBehindHome ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/server/wm/TaskDisplayArea;->positionTaskBehindHome(Lcom/android/server/wm/Task;)V
/* .line 606 */
(( com.android.server.wm.Task ) v0 ).setForceHidden ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/Task;->setForceHidden(IZ)Z
/* .line 607 */
v7 = this.mService;
v7 = this.mTaskSupervisor;
v7 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v7 ).ensureActivitiesVisible ( v3, v2, v1 ); // invoke-virtual {v7, v3, v2, v1}, Lcom/android/server/wm/RootWindowContainer;->ensureActivitiesVisible(Lcom/android/server/wm/ActivityRecord;IZ)V
/* .line 608 */
v1 = this.mService;
v1 = this.mTaskSupervisor;
v1 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v1 ).resumeFocusedTasksTopActivities ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->resumeFocusedTasksTopActivities()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 611 */
/* nop */
} // .end local v4 # "tasks":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/Task;>;"
} // .end local v5 # "c":Landroid/content/res/Configuration;
} // .end local v6 # "taskDisplayArea":Lcom/android/server/wm/TaskDisplayArea;
/* :catchall_0 */
/* move-exception v1 */
v2 = this.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v2 ).continueWindowLayout ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->continueWindowLayout()V
/* .line 612 */
/* throw v1 */
/* .line 609 */
/* :catch_0 */
/* move-exception v1 */
/* .line 611 */
/* nop */
} // :goto_0
v1 = this.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).continueWindowLayout ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->continueWindowLayout()V
/* .line 612 */
/* nop */
/* .line 613 */
return;
/* .line 566 */
} // :cond_3
} // :goto_1
return;
} // .end method
private void updateCtsMode ( ) {
/* .locals 5 */
/* .line 174 */
final String v0 = "persist.sys.miui_optimization"; // const-string v0, "persist.sys.miui_optimization"
final String v1 = "1"; // const-string v1, "1"
final String v2 = "ro.miui.cts"; // const-string v2, "ro.miui.cts"
/* .line 175 */
android.os.SystemProperties .get ( v2 );
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 174 */
int v2 = 1; // const/4 v2, 0x1
/* xor-int/2addr v1, v2 */
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* xor-int/2addr v0, v2 */
/* .line 177 */
/* .local v0, "isCtsMode":Z */
v1 = this.mService;
v1 = this.mContext;
/* .line 178 */
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
final String v3 = "android.software.freeform_window_management"; // const-string v3, "android.software.freeform_window_management"
v1 = (( android.content.pm.PackageManager ) v1 ).hasSystemFeature ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z
/* if-nez v1, :cond_0 */
v1 = this.mService;
v1 = this.mContext;
/* .line 180 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "enable_freeform_support"; // const-string v3, "enable_freeform_support"
/* .line 179 */
int v4 = 0; // const/4 v4, 0x0
v1 = android.provider.Settings$Global .getInt ( v1,v3,v4 );
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_0
/* move v4, v2 */
} // :cond_1
/* move v1, v4 */
/* .line 182 */
/* .local v1, "freeformWindowManagement":Z */
v3 = this.mService;
v3 = this.mGlobalLock;
/* monitor-enter v3 */
/* .line 183 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 184 */
try { // :try_start_0
v2 = this.mService;
/* iput-boolean v1, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z */
/* .line 186 */
} // :cond_2
v4 = this.mService;
/* iput-boolean v2, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z */
/* .line 188 */
} // :goto_0
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 189 */
final String v2 = "MiuiFreeFormGestureController"; // const-string v2, "MiuiFreeFormGestureController"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "isCtsMode: "; // const-string v4, "isCtsMode: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = "freeformWindowManagement \uff1a "; // const-string v4, "freeformWindowManagement \uff1a "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " mService.mSupportsFreeformWindowManagement: "; // const-string v4, " mService.mSupportsFreeformWindowManagement: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mService;
/* iget-boolean v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 191 */
return;
/* .line 188 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
/* # virtual methods */
public void clearAllFreeFormForProcessReboot ( ) {
/* .locals 6 */
/* .line 270 */
final String v0 = "MiuiFreeFormGestureController"; // const-string v0, "MiuiFreeFormGestureController"
final String v1 = "clearAllFreeForm for process dead"; // const-string v1, "clearAllFreeForm for process dead"
android.util.Slog .d ( v0,v1 );
/* .line 271 */
v0 = this.mService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 272 */
try { // :try_start_0
v1 = this.mMiuiFreeFormManagerService;
v1 = this.mFreeFormActivityStacks;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 273 */
/* .local v2, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v2 != null) { // if-eqz v2, :cond_1
v3 = this.mTask;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 274 */
v3 = this.mTask;
(( com.android.server.wm.Task ) v3 ).getBaseIntent ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 275 */
v3 = this.mTask;
(( com.android.server.wm.Task ) v3 ).getBaseIntent ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
v4 = this.mTask;
(( com.android.server.wm.Task ) v4 ).getBaseIntent ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
v4 = (( android.content.Intent ) v4 ).getMiuiFlags ( ); // invoke-virtual {v4}, Landroid/content/Intent;->getMiuiFlags()I
/* and-int/lit16 v4, v4, -0x101 */
(( android.content.Intent ) v3 ).setMiuiFlags ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Intent;->setMiuiFlags(I)Landroid/content/Intent;
/* .line 277 */
} // :cond_0
final String v3 = "MiuiFreeFormGestureController"; // const-string v3, "MiuiFreeFormGestureController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "exit freeform stack:"; // const-string v5, "exit freeform stack:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mTask;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 278 */
/* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->moveFreeformToFullScreen(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V */
/* .line 280 */
/* invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->clearFreeFormSurface(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V */
/* .line 282 */
} // .end local v2 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // :cond_1
/* .line 283 */
} // :cond_2
/* monitor-exit v0 */
/* .line 284 */
return;
/* .line 283 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void deliverDestroyItemToAlipay ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 10 */
/* .param p1, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 539 */
final String v0 = "MiuiFreeFormGestureController"; // const-string v0, "MiuiFreeFormGestureController"
v1 = this.mTask;
/* if-nez v1, :cond_0 */
/* .line 540 */
return;
/* .line 543 */
} // :cond_0
try { // :try_start_0
v1 = this.mTask;
(( com.android.server.wm.Task ) v1 ).topRunningActivity ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->topRunningActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 544 */
/* .local v1, "topActivity":Lcom/android/server/wm/ActivityRecord; */
v2 = this.mService;
v2 = this.mTaskSupervisor;
v2 = this.mRootWindowContainer;
/* .line 545 */
int v8 = 0; // const/4 v8, 0x0
(( com.android.server.wm.RootWindowContainer ) v2 ).getDisplayContent ( v8 ); // invoke-virtual {v2, v8}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v2 ).getActivityBelow ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/DisplayContent;->getActivityBelow(Lcom/android/server/wm/ActivityRecord;)Lcom/android/server/wm/ActivityRecord;
/* move-object v9, v2 */
/* .line 546 */
/* .local v9, "bottomActivity":Lcom/android/server/wm/ActivityRecord; */
if ( v1 != null) { // if-eqz v1, :cond_2
if ( v9 != null) { // if-eqz v9, :cond_2
v2 = this.app;
if ( v2 != null) { // if-eqz v2, :cond_2
final String v2 = "com.eg.android.AlipayGphone/com.alipay.mobile.quinox.SchemeLauncherActivity"; // const-string v2, "com.eg.android.AlipayGphone/com.alipay.mobile.quinox.SchemeLauncherActivity"
v3 = this.shortComponentName;
/* .line 547 */
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
final String v2 = "com.eg.android.AlipayGphone/com.alipay.mobile.nebulax.integration.mpaas.activity.NebulaActivity$Main"; // const-string v2, "com.eg.android.AlipayGphone/com.alipay.mobile.nebulax.integration.mpaas.activity.NebulaActivity$Main"
v3 = this.shortComponentName;
/* .line 548 */
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
final String v2 = "com.eg.android.AlipayGphone/com.alipay.mobile.nebulax.xriver.activity.XRiverActivity"; // const-string v2, "com.eg.android.AlipayGphone/com.alipay.mobile.nebulax.xriver.activity.XRiverActivity"
v3 = this.shortComponentName;
/* .line 549 */
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 550 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " deliverDestroyItemToAlipay: delivering destroyitem to bottomactivity: "; // const-string v3, " deliverDestroyItemToAlipay: delivering destroyitem to bottomactivity: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 551 */
v4 = this.resultWho;
/* iget v5, v9, Lcom/android/server/wm/ActivityRecord;->requestCode:I */
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move-object v2, v9 */
/* move-object v3, v1 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V */
/* .line 552 */
v2 = this.app;
/* .line 553 */
(( com.android.server.wm.WindowProcessController ) v2 ).getThread ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
v3 = this.token;
android.app.servertransaction.ClientTransaction .obtain ( v2,v3 );
/* .line 554 */
/* .local v2, "transaction":Landroid/app/servertransaction/ClientTransaction; */
android.app.servertransaction.DestroyActivityItem .obtain ( v8,v8 );
(( android.app.servertransaction.ClientTransaction ) v2 ).addCallback ( v3 ); // invoke-virtual {v2, v3}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 555 */
v3 = this.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v3 ).getLifecycleManager ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;
(( com.android.server.wm.ClientLifecycleManager ) v3 ).scheduleTransaction ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V
/* .line 556 */
int v3 = 0; // const/4 v3, 0x0
this.results = v3;
/* .line 557 */
(( com.android.server.wm.ActivityRecord ) v9 ).getTask ( ); // invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.Task ) v3 ).removeImmediately ( ); // invoke-virtual {v3}, Lcom/android/server/wm/Task;->removeImmediately()V
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 561 */
} // .end local v1 # "topActivity":Lcom/android/server/wm/ActivityRecord;
} // .end local v2 # "transaction":Landroid/app/servertransaction/ClientTransaction;
} // .end local v9 # "bottomActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_2
/* .line 559 */
/* :catch_0 */
/* move-exception v1 */
/* .line 560 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " deliverDestroyItemToAlipay: fail "; // const-string v3, " deliverDestroyItemToAlipay: fail "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 562 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void deliverResultForExitFreeform ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 19 */
/* .param p1, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 369 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
final String v1 = "MiuiFreeFormGestureController"; // const-string v1, "MiuiFreeFormGestureController"
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* if-nez v8, :cond_0 */
/* goto/16 :goto_3 */
/* .line 374 */
} // :cond_0
v0 = this.mService;
v0 = this.mTaskSupervisor;
v0 = this.mRootWindowContainer;
/* .line 375 */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).getDisplayContent ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v0 ).topRunningActivityExcludeFreeform ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;
/* .line 376 */
/* .local v9, "activityRecord":Lcom/android/server/wm/ActivityRecord; */
int v0 = 0; // const/4 v0, 0x0
/* if-nez v9, :cond_1 */
/* move-object v2, v0 */
} // :cond_1
(( com.android.server.wm.ActivityRecord ) v9 ).getTask ( ); // invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
} // :goto_0
/* move-object v10, v2 */
/* .line 377 */
/* .local v10, "currentFullRootTask":Lcom/android/server/wm/Task; */
if ( v10 != null) { // if-eqz v10, :cond_5
v2 = (( com.android.server.wm.Task ) v10 ).getRootTaskId ( ); // invoke-virtual {v10}, Lcom/android/server/wm/Task;->getRootTaskId()I
/* .line 378 */
v3 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I */
/* if-eq v2, v3, :cond_2 */
/* goto/16 :goto_2 */
/* .line 381 */
} // :cond_2
final String v11 = "deliverResultsReason"; // const-string v11, "deliverResultsReason"
/* .line 382 */
/* .local v11, "DELIVER_RESULT_REASON":Ljava/lang/String; */
final String v12 = "ExitFreeform"; // const-string v12, "ExitFreeform"
/* .line 383 */
/* .local v12, "EXIT_FREEFORM":Ljava/lang/String; */
v2 = this.mTask;
if ( v2 != null) { // if-eqz v2, :cond_4
v2 = this.mTask;
(( com.android.server.wm.Task ) v2 ).getTopNonFinishingActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
if ( v2 != null) { // if-eqz v2, :cond_4
v2 = this.mTask;
/* .line 384 */
(( com.android.server.wm.Task ) v2 ).getTopNonFinishingActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
v2 = this.shortComponentName;
final String v3 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v3, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 385 */
v2 = this.mTask;
(( com.android.server.wm.Task ) v2 ).getTopNonFinishingActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 386 */
/* .local v2, "resultFrom":Lcom/android/server/wm/ActivityRecord; */
/* move-object v3, v9 */
/* .line 387 */
/* .local v3, "resultTo":Lcom/android/server/wm/ActivityRecord; */
if ( v2 != null) { // if-eqz v2, :cond_3
if ( v3 != null) { // if-eqz v3, :cond_3
v4 = this.app;
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 389 */
try { // :try_start_0
v15 = this.resultWho;
/* iget v4, v3, Lcom/android/server/wm/ActivityRecord;->requestCode:I */
/* const/16 v17, 0x0 */
/* const/16 v18, 0x0 */
/* move-object v13, v3 */
/* move-object v14, v2 */
/* move/from16 v16, v4 */
/* invoke-virtual/range {v13 ..v18}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V */
/* .line 391 */
v4 = this.app;
/* .line 392 */
(( com.android.server.wm.WindowProcessController ) v4 ).getThread ( ); // invoke-virtual {v4}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
v5 = this.token;
android.app.servertransaction.ClientTransaction .obtain ( v4,v5 );
/* .line 393 */
/* .local v4, "transaction":Landroid/app/servertransaction/ClientTransaction; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " deliverResultForExitFreeform for applicationlock"; // const-string v6, " deliverResultForExitFreeform for applicationlock"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " delivering results to "; // const-string v6, " delivering results to "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = "results: "; // const-string v6, "results: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.results;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v5 );
/* .line 395 */
v5 = this.results;
android.app.servertransaction.ActivityResultItem .obtain ( v5 );
(( android.app.servertransaction.ClientTransaction ) v4 ).addCallback ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 396 */
v5 = this.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v5 ).getLifecycleManager ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;
(( com.android.server.wm.ClientLifecycleManager ) v5 ).scheduleTransaction ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V
/* .line 397 */
this.results = v0;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 400 */
} // .end local v4 # "transaction":Landroid/app/servertransaction/ClientTransaction;
/* .line 398 */
/* :catch_0 */
/* move-exception v0 */
/* .line 399 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " deliverResultForExitFreeform: fail "; // const-string v5, " deliverResultForExitFreeform: fail "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v4 );
/* .line 402 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_3
} // :goto_1
return;
/* .line 404 */
} // .end local v2 # "resultFrom":Lcom/android/server/wm/ActivityRecord;
} // .end local v3 # "resultTo":Lcom/android/server/wm/ActivityRecord;
} // :cond_4
v0 = this.mTask;
/* new-instance v13, Lcom/android/server/wm/MiuiFreeFormGestureController$$ExternalSyntheticLambda0; */
/* move-object v1, v13 */
/* move-object/from16 v2, p0 */
/* move-object v3, v10 */
/* move-object/from16 v4, p1 */
/* move-object v5, v11 */
/* move-object v6, v12 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiFreeFormGestureController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Lcom/android/server/wm/Task;Lcom/android/server/wm/MiuiFreeFormActivityStack;Ljava/lang/String;Ljava/lang/String;)V */
(( com.android.server.wm.Task ) v0 ).forAllActivities ( v13 ); // invoke-virtual {v0, v13}, Lcom/android/server/wm/Task;->forAllActivities(Ljava/util/function/Consumer;)V
/* .line 450 */
return;
/* .line 379 */
} // .end local v11 # "DELIVER_RESULT_REASON":Ljava/lang/String;
} // .end local v12 # "EXIT_FREEFORM":Ljava/lang/String;
} // :cond_5
} // :goto_2
return;
/* .line 370 */
} // .end local v9 # "activityRecord":Lcom/android/server/wm/ActivityRecord;
} // .end local v10 # "currentFullRootTask":Lcom/android/server/wm/Task;
} // :cond_6
} // :goto_3
return;
} // .end method
public void deliverResultForFinishActivity ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1, android.content.Intent p2 ) {
/* .locals 16 */
/* .param p1, "resultTo" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "resultFrom" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .line 464 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v0, p1 */
/* move-object/from16 v8, p2 */
/* move-object/from16 v9, p3 */
if ( v8 != null) { // if-eqz v8, :cond_e
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z */
if ( v2 != null) { // if-eqz v2, :cond_e
v2 = /* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z */
/* if-nez v2, :cond_0 */
/* goto/16 :goto_5 */
/* .line 467 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 468 */
/* .local v2, "deliverResult":Z */
int v10 = 0; // const/4 v10, 0x0
final String v3 = "com.tencent.mobileqq/cooperation.qzone.share.QZoneShareActivity"; // const-string v3, "com.tencent.mobileqq/cooperation.qzone.share.QZoneShareActivity"
final String v4 = "com.tencent.mobileqq/.activity.JumpActivity"; // const-string v4, "com.tencent.mobileqq/.activity.JumpActivity"
final String v11 = "MiuiFreeFormGestureController"; // const-string v11, "MiuiFreeFormGestureController"
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 470 */
v5 = this.shortComponentName;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
v4 = this.shortComponentName;
/* .line 471 */
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 472 */
int v2 = 1; // const/4 v2, 0x1
/* .line 473 */
v3 = this.mDisplayContent;
(( com.android.server.wm.DisplayContent ) v3 ).topRunningActivityExcludeFreeform ( ); // invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;
/* .line 474 */
} // .end local p1 # "resultTo":Lcom/android/server/wm/ActivityRecord;
/* .local v0, "resultTo":Lcom/android/server/wm/ActivityRecord; */
if ( v0 != null) { // if-eqz v0, :cond_2
(( com.android.server.wm.ActivityRecord ) v0 ).getTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
if ( v3 != null) { // if-eqz v3, :cond_2
v3 = this.app;
/* if-nez v3, :cond_1 */
} // :cond_1
/* move-object v12, v0 */
/* move v13, v2 */
/* goto/16 :goto_2 */
/* .line 475 */
} // :cond_2
} // :goto_0
return;
/* .line 516 */
} // .end local v0 # "resultTo":Lcom/android/server/wm/ActivityRecord;
/* .restart local p1 # "resultTo":Lcom/android/server/wm/ActivityRecord; */
} // :cond_3
/* move-object v12, v0 */
/* move v13, v2 */
/* goto/16 :goto_2 */
/* .line 479 */
} // :cond_4
v5 = this.mDisplayContent;
/* new-instance v6, Lcom/android/server/wm/MiuiFreeFormGestureController$$ExternalSyntheticLambda1; */
/* invoke-direct {v6, v8}, Lcom/android/server/wm/MiuiFreeFormGestureController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/ActivityRecord;)V */
(( com.android.server.wm.DisplayContent ) v5 ).getActivity ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/wm/DisplayContent;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;
/* .line 481 */
/* .local v5, "launchActivity":Lcom/android/server/wm/ActivityRecord; */
v6 = this.mDisplayContent;
(( com.android.server.wm.DisplayContent ) v6 ).topRunningActivityExcludeFreeform ( ); // invoke-virtual {v6}, Lcom/android/server/wm/DisplayContent;->topRunningActivityExcludeFreeform()Lcom/android/server/wm/ActivityRecord;
/* .line 482 */
} // .end local p1 # "resultTo":Lcom/android/server/wm/ActivityRecord;
/* .local v6, "resultTo":Lcom/android/server/wm/ActivityRecord; */
v0 = this.mMiuiFreeFormManagerService;
/* .line 483 */
v7 = /* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I */
(( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).getMiuiFreeFormActivityStackForMiuiFB ( v7 ); // invoke-virtual {v0, v7}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStackForMiuiFB(I)Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .line 484 */
/* .local v7, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
if ( v6 != null) { // if-eqz v6, :cond_d
(( com.android.server.wm.ActivityRecord ) v6 ).getTask ( ); // invoke-virtual {v6}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
if ( v0 != null) { // if-eqz v0, :cond_d
v0 = this.app;
if ( v0 != null) { // if-eqz v0, :cond_d
if ( v7 != null) { // if-eqz v7, :cond_d
/* .line 485 */
v0 = (( com.android.server.wm.ActivityRecord ) v6 ).getRootTaskId ( ); // invoke-virtual {v6}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
v12 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v7 ).getFreeFormLaunchFromTaskId ( ); // invoke-virtual {v7}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I
/* if-eq v0, v12, :cond_5 */
/* goto/16 :goto_4 */
/* .line 488 */
} // :cond_5
/* if-nez v5, :cond_6 */
v0 = this.shortComponentName;
v0 = (( java.lang.String ) v4 ).equals ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_7 */
v0 = this.shortComponentName;
/* .line 489 */
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_7 */
} // :cond_6
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 490 */
(( com.android.server.wm.ActivityRecord ) v6 ).getTask ( ); // invoke-virtual {v6}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.ActivityRecord ) v5 ).getTask ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* if-ne v0, v3, :cond_8 */
/* .line 491 */
} // :cond_7
int v2 = 1; // const/4 v2, 0x1
/* .line 493 */
} // :cond_8
final String v0 = "com.sina.weibo/.composerinde.ComposerDispatchActivity"; // const-string v0, "com.sina.weibo/.composerinde.ComposerDispatchActivity"
v3 = this.shortComponentName;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 494 */
final String v0 = "com.youku.phone/com.sina.weibo.sdk.share.ShareTransActivity"; // const-string v0, "com.youku.phone/com.sina.weibo.sdk.share.ShareTransActivity"
v3 = this.shortComponentName;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 495 */
int v0 = 1; // const/4 v0, 0x1
/* move v2, v0 */
/* .line 498 */
} // :cond_9
if ( v9 != null) { // if-eqz v9, :cond_b
final String v0 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v0, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
v3 = this.shortComponentName;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 499 */
final String v0 = "android.intent.extra.INTENT"; // const-string v0, "android.intent.extra.INTENT"
(( android.content.Intent ) v9 ).getParcelableExtra ( v0 ); // invoke-virtual {v9, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* move-object v3, v0 */
/* check-cast v3, Landroid/content/Intent; */
/* .line 500 */
/* .local v3, "rawIntent":Landroid/content/Intent; */
int v0 = 0; // const/4 v0, 0x0
/* .line 501 */
/* .local v0, "rawPackageName":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_a
(( android.content.Intent ) v3 ).getComponent ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 502 */
(( android.content.Intent ) v3 ).getComponent ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* move-object v4, v0 */
/* .line 504 */
} // :cond_a
/* move-object v4, v0 */
} // .end local v0 # "rawPackageName":Ljava/lang/String;
/* .local v4, "rawPackageName":Ljava/lang/String; */
} // :goto_1
/* const-string/jumbo v0, "security" */
android.os.ServiceManager .getService ( v0 );
/* .line 505 */
/* .local v12, "b":Landroid/os/IBinder; */
if ( v12 != null) { // if-eqz v12, :cond_b
/* .line 507 */
try { // :try_start_0
v0 = /* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/ActivityRecord;->getUid()I */
v0 = android.os.UserHandle .getUserId ( v0 );
/* .line 508 */
/* .local v0, "userId":I */
miui.security.ISecurityManager$Stub .asInterface ( v12 );
/* .line 509 */
v14 = /* .local v13, "service":Lmiui/security/ISecurityManager; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* xor-int/lit8 v14, v14, 0x1 */
/* move v2, v14 */
/* .line 512 */
} // .end local v0 # "userId":I
} // .end local v13 # "service":Lmiui/security/ISecurityManager;
/* move v13, v2 */
/* move-object v12, v6 */
/* .line 510 */
/* :catch_0 */
/* move-exception v0 */
/* .line 511 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v13 = "deliverResultForFinishActivity checkAccessControlPass error: "; // const-string v13, "deliverResultForFinishActivity checkAccessControlPass error: "
android.util.Slog .e ( v11,v13,v0 );
/* .line 516 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v3 # "rawIntent":Landroid/content/Intent;
} // .end local v4 # "rawPackageName":Ljava/lang/String;
} // .end local v5 # "launchActivity":Lcom/android/server/wm/ActivityRecord;
} // .end local v7 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
} // .end local v12 # "b":Landroid/os/IBinder;
} // :cond_b
/* move v13, v2 */
/* move-object v12, v6 */
} // .end local v2 # "deliverResult":Z
} // .end local v6 # "resultTo":Lcom/android/server/wm/ActivityRecord;
/* .local v12, "resultTo":Lcom/android/server/wm/ActivityRecord; */
/* .local v13, "deliverResult":Z */
} // :goto_2
if ( v13 != null) { // if-eqz v13, :cond_c
/* .line 518 */
try { // :try_start_1
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 519 */
/* .local v0, "data":Landroid/content/Intent; */
final String v2 = "deliverResultsReason"; // const-string v2, "deliverResultsReason"
/* move-object v14, v2 */
/* .line 520 */
/* .local v14, "DELIVER_RESULT_REASON":Ljava/lang/String; */
final String v2 = "ExitFreeform"; // const-string v2, "ExitFreeform"
/* move-object v15, v2 */
/* .line 521 */
/* .local v15, "EXIT_FREEFORM":Ljava/lang/String; */
(( android.content.Intent ) v0 ).putExtra ( v14, v15 ); // invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 522 */
v4 = this.resultWho;
/* iget v5, v12, Lcom/android/server/wm/ActivityRecord;->requestCode:I */
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move-object v2, v12 */
/* move-object/from16 v3, p2 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V */
/* .line 524 */
v2 = this.app;
/* .line 525 */
(( com.android.server.wm.WindowProcessController ) v2 ).getThread ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
v3 = this.token;
android.app.servertransaction.ClientTransaction .obtain ( v2,v3 );
/* .line 526 */
/* .local v2, "transaction":Landroid/app/servertransaction/ClientTransaction; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "deliverResultForFinishActivity: delivering results to "; // const-string v4, "deliverResultForFinishActivity: delivering results to "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v12 ); // invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " resultFrom= "; // const-string v4, " resultFrom= "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v8 ); // invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v3 );
/* .line 527 */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 528 */
/* .local v3, "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;" */
/* new-instance v4, Landroid/app/ResultInfo; */
v5 = this.resultWho;
/* iget v6, v12, Lcom/android/server/wm/ActivityRecord;->requestCode:I */
int v7 = 0; // const/4 v7, 0x0
/* invoke-direct {v4, v5, v6, v7, v0}, Landroid/app/ResultInfo;-><init>(Ljava/lang/String;IILandroid/content/Intent;)V */
(( java.util.ArrayList ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 529 */
android.app.servertransaction.ActivityResultItem .obtain ( v3 );
(( android.app.servertransaction.ClientTransaction ) v2 ).addCallback ( v4 ); // invoke-virtual {v2, v4}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 530 */
v4 = this.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v4 ).getLifecycleManager ( ); // invoke-virtual {v4}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;
(( com.android.server.wm.ClientLifecycleManager ) v4 ).scheduleTransaction ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V
/* .line 531 */
this.results = v10;
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 534 */
} // .end local v0 # "data":Landroid/content/Intent;
} // .end local v2 # "transaction":Landroid/app/servertransaction/ClientTransaction;
} // .end local v3 # "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
} // .end local v14 # "DELIVER_RESULT_REASON":Ljava/lang/String;
} // .end local v15 # "EXIT_FREEFORM":Ljava/lang/String;
/* .line 532 */
/* :catch_1 */
/* move-exception v0 */
/* .line 533 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " deliverResultForFinishActivity: fail deliver results to "; // const-string v3, " deliverResultForFinishActivity: fail deliver results to "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v12 ); // invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v11,v2 );
/* .line 536 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_c
} // :goto_3
return;
/* .line 486 */
} // .end local v12 # "resultTo":Lcom/android/server/wm/ActivityRecord;
} // .end local v13 # "deliverResult":Z
/* .local v2, "deliverResult":Z */
/* .restart local v5 # "launchActivity":Lcom/android/server/wm/ActivityRecord; */
/* .restart local v6 # "resultTo":Lcom/android/server/wm/ActivityRecord; */
/* .restart local v7 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
} // :cond_d
} // :goto_4
return;
/* .line 465 */
} // .end local v2 # "deliverResult":Z
} // .end local v5 # "launchActivity":Lcom/android/server/wm/ActivityRecord;
} // .end local v6 # "resultTo":Lcom/android/server/wm/ActivityRecord;
} // .end local v7 # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
/* .restart local p1 # "resultTo":Lcom/android/server/wm/ActivityRecord; */
} // :cond_e
} // :goto_5
return;
} // .end method
public void deliverResultForResumeActivityInFreeform ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 10 */
/* .param p1, "resultTo" # Lcom/android/server/wm/ActivityRecord; */
/* .line 343 */
final String v0 = "MiuiFreeFormGestureController"; // const-string v0, "MiuiFreeFormGestureController"
if ( p1 != null) { // if-eqz p1, :cond_2
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.app;
/* if-nez v1, :cond_0 */
/* goto/16 :goto_1 */
/* .line 346 */
} // :cond_0
final String v1 = "com.xiaomi.payment/com.mipay.common.ui.TranslucentActivity"; // const-string v1, "com.xiaomi.payment/com.mipay.common.ui.TranslucentActivity"
v2 = this.shortComponentName;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 348 */
try { // :try_start_0
final String v1 = "deliverResultsReason"; // const-string v1, "deliverResultsReason"
/* .line 349 */
/* .local v1, "DELIVER_RESULT_REASON":Ljava/lang/String; */
final String v2 = "ExitFreeform"; // const-string v2, "ExitFreeform"
/* .line 350 */
/* .local v2, "EXIT_FREEFORM":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* iget v6, p1, Lcom/android/server/wm/ActivityRecord;->requestCode:I */
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, p1 */
/* invoke-virtual/range {v3 ..v8}, Lcom/android/server/wm/ActivityRecord;->addResultLocked(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;IILandroid/content/Intent;)V */
/* .line 352 */
v3 = this.app;
/* .line 353 */
(( com.android.server.wm.WindowProcessController ) v3 ).getThread ( ); // invoke-virtual {v3}, Lcom/android/server/wm/WindowProcessController;->getThread()Landroid/app/IApplicationThread;
v4 = this.token;
android.app.servertransaction.ClientTransaction .obtain ( v3,v4 );
/* .line 354 */
/* .local v3, "transaction":Landroid/app/servertransaction/ClientTransaction; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " deliverResultForResumeActivityInFreeform: delivering results to PAYMENT_TRANSLUCENTACTIVITY: "; // const-string v5, " deliverResultForResumeActivityInFreeform: delivering results to PAYMENT_TRANSLUCENTACTIVITY: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 355 */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 356 */
/* .local v4, "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;" */
/* new-instance v5, Landroid/content/Intent; */
/* invoke-direct {v5}, Landroid/content/Intent;-><init>()V */
/* .line 357 */
/* .local v5, "data":Landroid/content/Intent; */
(( android.content.Intent ) v5 ).putExtra ( v1, v2 ); // invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 358 */
/* new-instance v6, Landroid/app/ResultInfo; */
/* iget v7, p1, Lcom/android/server/wm/ActivityRecord;->requestCode:I */
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* invoke-direct {v6, v9, v7, v8, v5}, Landroid/app/ResultInfo;-><init>(Ljava/lang/String;IILandroid/content/Intent;)V */
(( java.util.ArrayList ) v4 ).add ( v6 ); // invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 359 */
android.app.servertransaction.ActivityResultItem .obtain ( v4 );
(( android.app.servertransaction.ClientTransaction ) v3 ).addCallback ( v6 ); // invoke-virtual {v3, v6}, Landroid/app/servertransaction/ClientTransaction;->addCallback(Landroid/app/servertransaction/ClientTransactionItem;)V
/* .line 360 */
v6 = this.mService;
(( com.android.server.wm.ActivityTaskManagerService ) v6 ).getLifecycleManager ( ); // invoke-virtual {v6}, Lcom/android/server/wm/ActivityTaskManagerService;->getLifecycleManager()Lcom/android/server/wm/ClientLifecycleManager;
(( com.android.server.wm.ClientLifecycleManager ) v6 ).scheduleTransaction ( v3 ); // invoke-virtual {v6, v3}, Lcom/android/server/wm/ClientLifecycleManager;->scheduleTransaction(Landroid/app/servertransaction/ClientTransaction;)V
/* .line 361 */
this.results = v9;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 364 */
} // .end local v1 # "DELIVER_RESULT_REASON":Ljava/lang/String;
} // .end local v2 # "EXIT_FREEFORM":Ljava/lang/String;
} // .end local v3 # "transaction":Landroid/app/servertransaction/ClientTransaction;
} // .end local v4 # "resultInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
} // .end local v5 # "data":Landroid/content/Intent;
/* .line 362 */
/* :catch_0 */
/* move-exception v1 */
/* .line 363 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " deliverResultForResumeActivityInFreeform: fail to PAYMENT_TRANSLUCENTACTIVITY "; // const-string v3, " deliverResultForResumeActivityInFreeform: fail to PAYMENT_TRANSLUCENTACTIVITY "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 366 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
return;
/* .line 344 */
} // :cond_2
} // :goto_1
return;
} // .end method
public void displayConfigurationChange ( com.android.server.wm.DisplayContent p0, android.content.res.Configuration p1 ) {
/* .locals 5 */
/* .param p1, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .param p2, "configuration" # Landroid/content/res/Configuration; */
/* .line 691 */
/* iget v0, p2, Landroid/content/res/Configuration;->orientation:I */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
/* move v0, v2 */
} // :cond_0
/* move v0, v1 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsPortrait:Z */
/* .line 692 */
/* iget v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mLastOrientation:I */
/* iget v3, p2, Landroid/content/res/Configuration;->orientation:I */
/* if-eq v0, v3, :cond_2 */
/* .line 693 */
/* iget v0, p2, Landroid/content/res/Configuration;->orientation:I */
/* iput v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mLastOrientation:I */
/* .line 694 */
v0 = this.mInputMethodWindow;
/* .line 695 */
/* .local v0, "imeWin":Lcom/android/server/wm/WindowState; */
if ( v0 != null) { // if-eqz v0, :cond_1
v3 = (( com.android.server.wm.WindowState ) v0 ).isVisible ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->isVisible()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 696 */
v3 = (( com.android.server.wm.WindowState ) v0 ).isDisplayed ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowState;->isDisplayed()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* move v1, v2 */
} // :cond_1
/* nop */
/* .line 697 */
/* .local v1, "imeVisible":Z */
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 698 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "notify Ime showStatus for new orientation:"; // const-string v4, "notify Ime showStatus for new orientation:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p2, Landroid/content/res/Configuration;->orientation:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiFreeFormGestureController"; // const-string v4, "MiuiFreeFormGestureController"
android.util.Slog .d ( v4,v3 );
/* .line 699 */
v3 = (( com.android.server.wm.DisplayContent ) p1 ).getInputMethodWindowVisibleHeight ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getInputMethodWindowVisibleHeight()I
/* .line 700 */
/* .local v3, "imeHeight":I */
v4 = this.mMiuiFreeFormManagerService;
(( com.android.server.wm.MiuiFreeFormManagerService ) v4 ).setAdjustedForIme ( v2, v3, v2 ); // invoke-virtual {v4, v2, v3, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->setAdjustedForIme(ZIZ)V
/* .line 703 */
} // .end local v0 # "imeWin":Lcom/android/server/wm/WindowState;
} // .end local v1 # "imeVisible":Z
} // .end local v3 # "imeHeight":I
} // :cond_2
return;
} // .end method
public void exitFreeFormByKeyCombination ( com.android.server.wm.Task p0 ) {
/* .locals 1 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 748 */
v0 = this.mMiuiFreeFormKeyCombinationHelper;
(( com.android.server.wm.MiuiFreeFormKeyCombinationHelper ) v0 ).exitFreeFormByKeyCombination ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->exitFreeFormByKeyCombination(Lcom/android/server/wm/Task;)V
/* .line 749 */
return;
} // .end method
public void freeFormAndFullScreenToggleByKeyCombination ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isStartFreeForm" # Z */
/* .line 744 */
v0 = this.mMiuiFreeFormKeyCombinationHelper;
(( com.android.server.wm.MiuiFreeFormKeyCombinationHelper ) v0 ).freeFormAndFullScreenToggleByKeyCombination ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->freeFormAndFullScreenToggleByKeyCombination(Z)V
/* .line 745 */
return;
} // .end method
public Boolean ignoreDeliverResultForFreeForm ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 3 */
/* .param p1, "resultTo" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "resultFrom" # Lcom/android/server/wm/ActivityRecord; */
/* .line 453 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
v1 = (( com.android.server.wm.ActivityRecord ) p2 ).inFreeformWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 454 */
v1 = (( com.android.server.wm.MiuiFreeFormGestureController ) p0 ).isInVideoOrGameScene ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isInVideoOrGameScene()Z
/* if-nez v1, :cond_0 */
/* .line 457 */
} // :cond_0
final String v1 = "com.tencent.mobileqq/.activity.JumpActivity"; // const-string v1, "com.tencent.mobileqq/.activity.JumpActivity"
v2 = this.shortComponentName;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 458 */
int v0 = 1; // const/4 v0, 0x1
/* .line 460 */
} // :cond_1
/* .line 455 */
} // :cond_2
} // :goto_0
} // .end method
Boolean isInVideoOrGameScene ( ) {
/* .locals 1 */
/* .line 210 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsVideoMode:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsGameMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean needForegroundPin ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 4 */
/* .param p1, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 710 */
(( com.android.server.wm.MiuiFreeFormActivityStack ) p1 ).getStackPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
/* .line 711 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = this.mTask;
(( com.android.server.wm.Task ) v1 ).getTopNonFinishingActivity ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 712 */
/* .local v1, "r":Lcom/android/server/wm/ActivityRecord; */
int v2 = -1; // const/4 v2, -0x1
/* .line 713 */
/* .local v2, "uid":I */
if ( v1 != null) { // if-eqz v1, :cond_0
v3 = this.info;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.info;
v3 = this.applicationInfo;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 714 */
v3 = this.info;
v3 = this.applicationInfo;
/* iget v2, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 716 */
} // :cond_0
v3 = android.util.MiuiMultiWindowUtils .supportForeGroundPin ( );
if ( v3 != null) { // if-eqz v3, :cond_3
v3 = android.util.MiuiMultiWindowAdapter .isInForegroundPinAppBlackList ( v0 );
/* if-nez v3, :cond_3 */
/* .line 717 */
v3 = /* invoke-direct {p0, v0, v2}, Lcom/android/server/wm/MiuiFreeFormGestureController;->isPlaybackActive(Ljava/lang/String;I)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = android.util.MiuiMultiWindowAdapter .isInAudioForegroundPinAppList ( v0 );
/* if-nez v3, :cond_2 */
/* .line 718 */
} // :cond_1
v3 = android.util.MiuiMultiWindowAdapter .isInForegroundPinAppWhiteList ( v0 );
/* if-nez v3, :cond_2 */
/* .line 719 */
v3 = android.util.MiuiMultiWindowAdapter .isInTopGameList ( v0 );
if ( v3 != null) { // if-eqz v3, :cond_3
} // :cond_2
int v3 = 1; // const/4 v3, 0x1
} // :cond_3
int v3 = 0; // const/4 v3, 0x0
/* .line 716 */
} // :goto_0
} // .end method
public void onATMSSystemReady ( ) {
/* .locals 6 */
/* .line 110 */
v0 = this.mService;
v0 = this.mRootWindowContainer;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.RootWindowContainer ) v0 ).getDisplayContent ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
this.mDisplayContent = v0;
/* .line 111 */
/* new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
v2 = this.mService;
v2 = this.mContext;
v3 = this.mMiuiFreeFormManagerService;
/* invoke-direct {v0, v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;-><init>(Landroid/content/Context;Lcom/android/server/wm/MiuiFreeFormManagerService;)V */
this.mMiuiMultiWindowRecommendHelper = v0;
/* .line 112 */
v0 = this.mService;
v0 = this.mContext;
v2 = this.mService;
v2 = this.mContext;
final String v2 = "audio"; // const-string v2, "audio"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
this.mAudioManager = v0;
/* .line 113 */
(( com.android.server.wm.MiuiFreeFormGestureController ) p0 ).registerFreeformCloudDataObserver ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->registerFreeformCloudDataObserver()V
/* .line 114 */
v0 = this.mService;
v0 = this.mContext;
v2 = this.mFreeFormReceiver;
v3 = this.mFilter;
int v4 = 0; // const/4 v4, 0x0
v5 = this.mHandler;
(( android.content.Context ) v0 ).registerReceiver ( v2, v3, v4, v5 ); // invoke-virtual {v0, v2, v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 115 */
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 116 */
final String v2 = "miui_optimization"; // const-string v2, "miui_optimization"
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mMiuiOptObserver;
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v1, v3, v4 ); // invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 118 */
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 119 */
/* const-string/jumbo v2, "vtb_boosting" */
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mVideoModeObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v1, v3, v1 ); // invoke-virtual {v0, v2, v1, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 121 */
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 122 */
final String v2 = "gb_boosting"; // const-string v2, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mGameModeObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v1, v3, v1 ); // invoke-virtual {v0, v2, v1, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 124 */
v0 = this.mMiuiFreeFormManagerService;
(( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).updateDataFromSetting ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormManagerService;->updateDataFromSetting()V
/* .line 125 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiFreeFormGestureController;->updateCtsMode()V */
/* .line 126 */
/* new-instance v0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;)V */
this.mMiuiFreeFormKeyCombinationHelper = v0;
/* .line 127 */
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* iget v0, v0, Landroid/content/res/Configuration;->orientation:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
/* move v1, v2 */
} // :cond_0
/* iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mIsPortrait:Z */
/* .line 128 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/wm/MiuiFreeFormGestureController$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiFreeFormGestureController$1;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;)V */
/* const-wide/16 v2, 0x2710 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 133 */
return;
} // .end method
public void onFirstWindowDrawn ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 706 */
v0 = this.mMiuiMultiWindowRecommendHelper;
(( com.android.server.wm.MiuiMultiWindowRecommendHelper ) v0 ).onFirstWindowDrawn ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->onFirstWindowDrawn(Lcom/android/server/wm/ActivityRecord;)V
/* .line 707 */
return;
} // .end method
public void reflectRemoveUnreachableFreeformTask ( ) {
/* .locals 7 */
/* .line 226 */
int v0 = 0; // const/4 v0, 0x0
/* .line 228 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
try { // :try_start_0
v1 = this.mService;
v1 = this.mTaskSupervisor;
v1 = this.mRecentTasks;
(( java.lang.Object ) v1 ).getClass ( ); // invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 229 */
/* .local v1, "clazz":Ljava/lang/Class; */
final String v2 = "removeUnreachableFreeformTask"; // const-string v2, "removeUnreachableFreeformTask"
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Ljava/lang/Class; */
v5 = java.lang.Boolean.TYPE;
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v1 ).getDeclaredMethod ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* move-object v0, v2 */
/* .line 230 */
(( java.lang.reflect.Method ) v0 ).setAccessible ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 231 */
v2 = this.mService;
v2 = this.mTaskSupervisor;
v2 = this.mRecentTasks;
/* new-array v4, v3, [Ljava/lang/Object; */
java.lang.Boolean .valueOf ( v3 );
/* aput-object v3, v4, v6 */
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 234 */
/* nop */
} // .end local v1 # "clazz":Ljava/lang/Class;
/* .line 232 */
/* :catch_0 */
/* move-exception v1 */
/* .line 233 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getDeclaredMethod:reflectRemoveUnreachableFreeformTask"; // const-string v3, "getDeclaredMethod:reflectRemoveUnreachableFreeformTask"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiFreeFormGestureController"; // const-string v3, "MiuiFreeFormGestureController"
android.util.Slog .d ( v3,v2 );
/* .line 235 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void registerFreeformCloudDataObserver ( ) {
/* .locals 4 */
/* .line 214 */
/* new-instance v0, Lcom/android/server/wm/MiuiFreeFormGestureController$5; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController$5;-><init>(Lcom/android/server/wm/MiuiFreeFormGestureController;Landroid/os/Handler;)V */
/* .line 221 */
/* .local v0, "contentObserver":Landroid/database/ContentObserver; */
v1 = this.mService;
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"; // const-string v2, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"
android.net.Uri .parse ( v2 );
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 223 */
return;
} // .end method
public void startExitApplication ( com.android.server.wm.MiuiFreeFormActivityStack p0 ) {
/* .locals 3 */
/* .param p1, "mffas" # Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 239 */
final String v0 = "MiuiFreeFormGestureController"; // const-string v0, "MiuiFreeFormGestureController"
/* const-string/jumbo v1, "startExitApplication" */
android.util.Slog .d ( v0,v1 );
/* .line 240 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->hideStack(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V */
/* .line 241 */
/* new-instance v0, Landroid/view/SurfaceControl$Transaction; */
/* invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
/* .line 242 */
/* .local v0, "inputTransaction":Landroid/view/SurfaceControl$Transaction; */
v1 = this.mService;
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 243 */
try { // :try_start_0
v2 = this.mService;
v2 = this.mWindowManager;
(( com.android.server.wm.WindowManagerService ) v2 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v2 ).getInputMonitor ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getInputMonitor()Lcom/android/server/wm/InputMonitor;
(( com.android.server.wm.InputMonitor ) v2 ).updateInputWindowsImmediately ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/InputMonitor;->updateInputWindowsImmediately(Landroid/view/SurfaceControl$Transaction;)V
/* .line 244 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 245 */
(( android.view.SurfaceControl$Transaction ) v0 ).apply ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 246 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->exitFreeForm(Lcom/android/server/wm/MiuiFreeFormActivityStack;)V */
/* .line 247 */
return;
/* .line 244 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
