public class com.android.server.wm.ActivityTaskManagerServiceImpl extends com.android.server.wm.ActivityTaskManagerServiceStub {
	 /* .source "ActivityTaskManagerServiceImpl.java" */
	 /* # static fields */
	 private static final android.util.ArraySet BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/ArraySet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final Integer CONTINUITY_NOTIFICATION_ID;
private static final java.lang.String EXPAND_DOCK;
private static final java.lang.String EXPAND_OTHER;
private static final android.view.animation.Interpolator FAST_OUT_SLOW_IN_REVERSE;
public static final java.lang.String FLIP_HOME_CLASS;
public static final java.lang.String FLIP_HOME_PACKAGE;
public static final java.lang.String FLIP_HOME_SHORT_COMPONENT;
private static final java.lang.String GESTURE_LEFT_TO_RIGHT;
private static final java.lang.String GESTURE_RIGHT_TO_LEFT;
public static final java.lang.String KEY_MULTI_FINGER_SLIDE;
private static final java.lang.String MANAGED_PROFILE_NOT_SNAPSHOT_ACTIVITY;
private static final java.lang.String MIUI_THEMEMANAGER_PKG;
private static final Integer PACKAGE_FORE_BUFFER_SIZE;
private static final java.lang.String PACKAGE_NAME_CAMERA;
private static final java.lang.String PRESS_META_KEY_AND_W;
private static final java.lang.String SNAP_TO_LEFT;
private static final java.lang.String SNAP_TO_RIGHT;
private static final java.lang.String SPLIT_SCREEN_BLACK_LIST;
private static final java.lang.String SPLIT_SCREEN_BLACK_LIST_FOR_FOLD;
private static final java.lang.String SPLIT_SCREEN_BLACK_LIST_FOR_PAD;
private static final java.lang.String SPLIT_SCREEN_MODULE_NAME;
private static final java.lang.String SPLIT_SCREEN_MULTI_TASK_WHITE_LIST;
private static final java.lang.String SUBSCREEN_MAIN_ACTIVITY;
private static final java.lang.String SUBSCREEN_PKG;
private static final Boolean SUPPORT_MULTIPLE_TASK;
private static final java.lang.String TAG;
private static final java.lang.String THREE_GESTURE_DOCK_TASK;
private static final java.lang.String UPDATE_SPLIT_SNAP_TARGET;
private static final android.net.Uri URI_CLOUD_ALL_DATA_NOTIFY;
private static final Integer WIDE_SCREEN_SIZE;
private static java.lang.String lastForegroundPkg;
private static android.content.pm.ApplicationInfo lastMultiWindowAppInfo;
private static final java.util.HashSet mIgnoreUriCheckPkg;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static Integer mLastMainDisplayTopTaskId;
private static final java.util.List sCachedForegroundPackageList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/mqsas/sdk/event/PackageForegroundEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static Boolean sSystemBootCompleted;
/* # instance fields */
private final Boolean SUPPORT_DECLINE_BACKGROUND_COLOR;
private Boolean isHasActivityControl;
private Integer mActivityControlUid;
private java.util.List mAdvanceTaskIds;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.wm.AppCompatTask mAppCompatTask;
private Boolean mAppContinuityIsUnfocused;
public com.android.server.wm.ActivityTaskManagerService mAtmService;
public android.content.Context mContext;
private java.util.List mDeclineColorActivity;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.wm.FoldablePackagePolicy mFoldablePackagePolicy;
public com.android.server.wm.MiuiOrientationImpl$FullScreenPackageManager mFullScreenPackageManager;
private volatile Boolean mInAnimationOut;
private com.android.server.wm.MiuiSizeCompatInternal mMiuiSizeCompatIn;
private java.util.HashSet mMultipleTaskWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public com.android.server.wm.PackageConfigurationController mPackageConfigurationController;
java.lang.String mPackageHoldOn;
private com.android.server.pm.PackageManagerService$IPackageManagerImpl mPackageManager;
public com.android.server.wm.PackageSettingsManager mPackageSettingsManager;
com.miui.server.process.ProcessManagerInternal mProcessManagerIn;
private java.util.HashSet mResizeBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mRestartingTaskId;
private android.view.SurfaceControl mScreenshotLayer;
private com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
private java.lang.String mTargePackageName;
private com.android.server.wm.ActivityTaskSupervisor mTaskSupervisor;
private java.util.HashSet mTransitionSyncIdList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$IQuC6QqI36bWb7diJfr_ZQodPv4 ( com.android.server.wm.ActivityTaskManagerServiceImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lambda$onSystemReady$0()V */
return;
} // .end method
public static void $r8$lambda$JveBgNg0OlAxn4POHQ0NP48_yZg ( com.android.server.wm.ActivityTaskManagerServiceImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lambda$showScreenShotForSplitTask$2()V */
return;
} // .end method
public static void $r8$lambda$QLnmHxw2DGVfzrO7m_eXC20MLc0 ( com.android.server.wm.ActivityTaskManagerServiceImpl p0, android.view.SurfaceControl$Transaction p1, android.animation.ValueAnimator p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->lambda$animationOut$1(Landroid/view/SurfaceControl$Transaction;Landroid/animation/ValueAnimator;)V */
return;
} // .end method
public static void $r8$lambda$b6s6wuAF5h0xR_w0u9uVXDg8mx8 ( com.android.server.wm.ActivityTaskManagerServiceImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->animationOut()V */
return;
} // .end method
static android.view.SurfaceControl -$$Nest$fgetmScreenshotLayer ( com.android.server.wm.ActivityTaskManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mScreenshotLayer;
} // .end method
static void -$$Nest$fputmInAnimationOut ( com.android.server.wm.ActivityTaskManagerServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mInAnimationOut:Z */
return;
} // .end method
static void -$$Nest$fputmScreenshotLayer ( com.android.server.wm.ActivityTaskManagerServiceImpl p0, android.view.SurfaceControl p1 ) { //bridge//synthethic
/* .locals 0 */
this.mScreenshotLayer = p1;
return;
} // .end method
static void -$$Nest$mstartSubScreenUi ( com.android.server.wm.ActivityTaskManagerServiceImpl p0, android.content.pm.ApplicationInfo p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->startSubScreenUi(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V */
return;
} // .end method
static com.android.server.wm.ActivityTaskManagerServiceImpl ( ) {
/* .locals 5 */
/* .line 169 */
/* nop */
/* .line 170 */
final String v0 = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"; // const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"
android.net.Uri .parse ( v0 );
/* .line 196 */
/* new-instance v0, Landroid/view/animation/PathInterpolator; */
/* const v1, 0x3f19999a # 0.6f */
/* const/high16 v2, 0x3f800000 # 1.0f */
/* const v3, 0x3f4ccccd # 0.8f */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v0, v3, v4, v1, v2}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V */
/* .line 208 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 358 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 362 */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 363 */
final String v1 = "com.miui.mishare.connectivity"; // const-string v1, "com.miui.mishare.connectivity"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 364 */
final String v1 = "com.miui.securitycore"; // const-string v1, "com.miui.securitycore"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 373 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 377 */
/* nop */
/* .line 378 */
/* const-string/jumbo v0, "sys.proc.fore_pkg_buffer" */
/* const/16 v1, 0xf */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 417 */
int v0 = 0; // const/4 v0, 0x0
/* .line 418 */
return;
} // .end method
public com.android.server.wm.ActivityTaskManagerServiceImpl ( ) {
/* .locals 2 */
/* .line 120 */
/* invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;-><init>()V */
/* .line 174 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mAppContinuityIsUnfocused:Z */
/* .line 175 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.mResizeBlackList = v1;
/* .line 191 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.mMultipleTaskWhiteList = v1;
/* .line 198 */
/* nop */
/* .line 199 */
/* const-string/jumbo v1, "support_decline_background_color" */
v1 = miui.util.FeatureParser .getBoolean ( v1,v0 );
/* iput-boolean v1, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->SUPPORT_DECLINE_BACKGROUND_COLOR:Z */
/* .line 200 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mDeclineColorActivity = v1;
/* .line 202 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.mTransitionSyncIdList = v1;
/* .line 204 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mAdvanceTaskIds = v1;
/* .line 1680 */
/* iput-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isHasActivityControl:Z */
/* .line 1681 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mActivityControlUid:I */
return;
} // .end method
private void animationOut ( ) {
/* .locals 4 */
/* .line 1426 */
/* iget-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mInAnimationOut:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 1427 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [F */
/* fill-array-data v0, :array_0 */
android.animation.ValueAnimator .ofFloat ( v0 );
/* .line 1428 */
/* .local v0, "anim":Landroid/animation/ValueAnimator; */
/* new-instance v1, Landroid/view/SurfaceControl$Transaction; */
/* invoke-direct {v1}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
/* .line 1429 */
/* .local v1, "t":Landroid/view/SurfaceControl$Transaction; */
/* new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/view/SurfaceControl$Transaction;)V */
(( android.animation.ValueAnimator ) v0 ).addUpdateListener ( v2 ); // invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
/* .line 1436 */
/* new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7; */
/* invoke-direct {v2, p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$7;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/view/SurfaceControl$Transaction;)V */
(( android.animation.ValueAnimator ) v0 ).addListener ( v2 ); // invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V
/* .line 1462 */
/* const-wide/16 v2, 0x12c */
(( android.animation.ValueAnimator ) v0 ).setDuration ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;
/* .line 1463 */
/* const-wide/16 v2, 0xc8 */
(( android.animation.ValueAnimator ) v0 ).setStartDelay ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V
/* .line 1464 */
v2 = com.android.server.wm.ActivityTaskManagerServiceImpl.FAST_OUT_SLOW_IN_REVERSE;
(( android.animation.ValueAnimator ) v0 ).setInterpolator ( v2 ); // invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V
/* .line 1465 */
(( android.animation.ValueAnimator ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V
/* .line 1466 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mInAnimationOut:Z */
/* .line 1467 */
return;
/* :array_0 */
/* .array-data 4 */
/* 0x3f800000 # 1.0f */
/* 0x0 */
} // .end array-data
} // .end method
private Boolean canEnterPipOnTaskSwitch ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.Task p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "top" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .param p3, "userLeaving" # Z */
/* .line 1843 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_0 */
/* .line 1846 */
} // :cond_0
(( com.android.server.wm.Task ) p2 ).getRootTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
/* .line 1847 */
/* .local v1, "rootTask":Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = (( com.android.server.wm.Task ) v1 ).isActivityTypeAssistant ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->isActivityTypeAssistant()Z
/* if-nez v2, :cond_1 */
if ( p3 != null) { // if-eqz p3, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* .line 1844 */
} // .end local v1 # "rootTask":Lcom/android/server/wm/Task;
} // :cond_2
} // :goto_0
} // .end method
private void exitSplitScreen ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isExpandDock" # Z */
/* .line 1223 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "expand_dock"; // const-string v0, "expand_dock"
} // :cond_0
final String v0 = "expand_other"; // const-string v0, "expand_other"
/* .line 1224 */
/* .local v0, "str":Ljava/lang/String; */
} // :goto_0
v1 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getUiContext ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getUiContext()Landroid/content/Context;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "press_meta_key_and_w"; // const-string v2, "press_meta_key_and_w"
android.provider.MiuiSettings$System .putString ( v1,v2,v0 );
/* .line 1226 */
return;
} // .end method
private Boolean exitSplitScreenIfNeed ( com.android.server.wm.WindowContainer p0 ) {
/* .locals 5 */
/* .param p1, "wc" # Lcom/android/server/wm/WindowContainer; */
/* .line 1214 */
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).isVerticalSplit ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isVerticalSplit()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).isInSystemSplitScreen ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
/* if-nez v0, :cond_0 */
/* .line 1215 */
} // :cond_0
(( com.android.server.wm.WindowContainer ) p1 ).asTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.Task ) v0 ).getRootTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
/* .line 1216 */
/* .local v0, "rootTask":Lcom/android/server/wm/Task; */
(( com.android.server.wm.Task ) v0 ).getTopLeafTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getTopLeafTask()Lcom/android/server/wm/Task;
/* .line 1217 */
/* .local v2, "topLeafTask":Lcom/android/server/wm/Task; */
(( com.android.server.wm.Task ) v2 ).getBounds ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
/* iget v3, v3, Landroid/graphics/Rect;->left:I */
int v4 = 1; // const/4 v4, 0x1
/* if-nez v3, :cond_1 */
/* move v1, v4 */
/* .line 1218 */
/* .local v1, "isExpandDock":Z */
} // :cond_1
/* invoke-direct {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->exitSplitScreen(Z)V */
/* .line 1219 */
/* .line 1214 */
} // .end local v0 # "rootTask":Lcom/android/server/wm/Task;
} // .end local v1 # "isExpandDock":Z
} // .end local v2 # "topLeafTask":Lcom/android/server/wm/Task;
} // :cond_2
} // :goto_0
} // .end method
public static android.content.ComponentName getChildComponentNames ( android.app.ActivityTaskManager$RootTaskInfo p0 ) {
/* .locals 5 */
/* .param p0, "rootTaskInfo" # Landroid/app/ActivityTaskManager$RootTaskInfo; */
/* .line 1285 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Landroid/content/ComponentName; */
/* .line 1286 */
/* .local v0, "componentNames":[Landroid/content/ComponentName; */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 1287 */
v1 = this.childTaskNames;
/* .line 1288 */
/* .local v1, "childTaskNames":[Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1289 */
/* array-length v2, v1 */
/* .line 1290 */
/* .local v2, "childTaskCount":I */
/* new-array v0, v2, [Landroid/content/ComponentName; */
/* .line 1291 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v2, :cond_0 */
/* .line 1292 */
/* aget-object v4, v1, v3 */
android.content.ComponentName .unflattenFromString ( v4 );
/* aput-object v4, v0, v3 */
/* .line 1291 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1296 */
} // .end local v1 # "childTaskNames":[Ljava/lang/String;
} // .end local v2 # "childTaskCount":I
} // .end local v3 # "i":I
} // :cond_0
} // .end method
public static getChildTaskUserIds ( android.app.ActivityTaskManager$RootTaskInfo p0 ) {
/* .locals 4 */
/* .param p0, "rootTaskInfo" # Landroid/app/ActivityTaskManager$RootTaskInfo; */
/* .line 1299 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v1, v0, [I */
/* .line 1300 */
/* .local v1, "childTaskUserIds":[I */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 1301 */
v2 = this.childTaskUserIds;
/* .line 1302 */
/* .local v2, "childUserIds":[I */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1303 */
/* array-length v3, v2 */
/* .line 1304 */
/* .local v3, "childTaskCount":I */
/* new-array v1, v3, [I */
/* .line 1305 */
java.lang.System .arraycopy ( v2,v0,v1,v0,v3 );
/* .line 1308 */
} // .end local v2 # "childUserIds":[I
} // .end local v3 # "childTaskCount":I
} // :cond_0
} // .end method
public static com.android.server.wm.ActivityTaskManagerServiceImpl getInstance ( ) {
/* .locals 1 */
/* .line 212 */
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
/* check-cast v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
} // .end method
private void getSplitScreenBlackListFromXml ( ) {
/* .locals 3 */
/* .line 813 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 814 */
v0 = this.mResizeBlackList;
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 815 */
v0 = this.mResizeBlackList;
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300a4 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 817 */
} // :cond_0
final String v0 = "persist.sys.muiltdisplay_type"; // const-string v0, "persist.sys.muiltdisplay_type"
int v2 = 0; // const/4 v2, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v2 );
int v2 = 2; // const/4 v2, 0x2
/* if-ne v0, v2, :cond_1 */
/* .line 818 */
v0 = this.mResizeBlackList;
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 819 */
v0 = this.mResizeBlackList;
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300a3 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 822 */
} // :cond_1
v0 = this.mResizeBlackList;
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300a2 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 828 */
} // :goto_0
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 829 */
v0 = this.mMultipleTaskWhiteList;
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300a0 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 832 */
} // :cond_2
v0 = this.mMultipleTaskWhiteList;
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x1103009f */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 836 */
} // :goto_1
return;
} // .end method
public static Integer handleFreeformModeRequst ( android.os.IBinder p0, Integer p1, android.content.Context p2 ) {
/* .locals 7 */
/* .param p0, "token" # Landroid/os/IBinder; */
/* .param p1, "cmd" # I */
/* .param p2, "mContext" # Landroid/content/Context; */
/* .line 697 */
int v0 = -1; // const/4 v0, -0x1
/* .line 698 */
/* .local v0, "result":I */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 700 */
/* .local v1, "ident":J */
try { // :try_start_0
com.android.server.wm.ActivityRecord .forTokenLocked ( p0 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 701 */
/* .local v3, "r":Lcom/android/server/wm/ActivityRecord; */
final String v4 = "gamebox_stick"; // const-string v4, "gamebox_stick"
int v5 = 0; // const/4 v5, 0x0
/* packed-switch p1, :pswitch_data_0 */
/* .line 717 */
/* :pswitch_0 */
try { // :try_start_1
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .getString ( v6,v4 );
/* .line 720 */
/* .local v4, "component":Ljava/lang/String; */
/* if-nez v3, :cond_1 */
} // :cond_0
} // :cond_1
(( com.android.server.wm.ActivityRecord ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.Task ) v6 ).getBaseIntent ( ); // invoke-virtual {v6}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
(( android.content.Intent ) v6 ).getComponent ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v6 ).flattenToShortString ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
v6 = (( java.lang.String ) v6 ).equals ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
int v5 = 1; // const/4 v5, 0x1
} // :goto_0
/* move v0, v5 */
/* .line 721 */
/* .line 714 */
} // .end local v4 # "component":Ljava/lang/String;
/* :pswitch_1 */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = ""; // const-string v6, ""
android.provider.Settings$Secure .putString ( v5,v4,v6 );
/* .line 715 */
/* .line 706 */
/* :pswitch_2 */
/* nop */
/* .line 707 */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 711 */
(( com.android.server.wm.ActivityRecord ) v3 ).getTask ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.Task ) v6 ).getBaseIntent ( ); // invoke-virtual {v6}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
(( android.content.Intent ) v6 ).getComponent ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v6 ).flattenToShortString ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* .line 706 */
android.provider.Settings$Secure .putString ( v5,v4,v6 );
/* .line 712 */
/* .line 703 */
/* :pswitch_3 */
/* if-nez v3, :cond_2 */
} // :cond_2
v5 = (( com.android.server.wm.ActivityRecord ) v3 ).getWindowingMode ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // :goto_1
/* move v0, v5 */
/* .line 704 */
/* nop */
/* .line 725 */
} // :goto_2
/* nop */
/* .line 727 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 725 */
/* .line 727 */
} // .end local v3 # "r":Lcom/android/server/wm/ActivityRecord;
/* :catchall_0 */
/* move-exception v3 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 728 */
/* throw v3 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static java.lang.Object invoke ( java.lang.Object p0, java.lang.String p1, java.lang.Object...p2 ) {
/* .locals 7 */
/* .param p0, "obj" # Ljava/lang/Object; */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .param p2, "values" # [Ljava/lang/Object; */
/* .line 1775 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1777 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 1778 */
/* .local v2, "clazz":Ljava/lang/Class; */
int v3 = 1; // const/4 v3, 0x1
/* if-nez p2, :cond_0 */
/* .line 1779 */
(( java.lang.Class ) v2 ).getDeclaredMethod ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* move-object v0, v4 */
/* .line 1780 */
(( java.lang.reflect.Method ) v0 ).setAccessible ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 1781 */
int v3 = 0; // const/4 v3, 0x0
/* new-array v3, v3, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v0 ).invoke ( p0, v3 ); // invoke-virtual {v0, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1783 */
} // :cond_0
/* array-length v4, p2 */
/* new-array v4, v4, [Ljava/lang/Class; */
/* .line 1784 */
/* .local v4, "argsClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* array-length v6, p2 */
/* if-ge v5, v6, :cond_4 */
/* .line 1785 */
/* aget-object v6, p2, v5 */
/* instance-of v6, v6, Ljava/lang/Integer; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 1786 */
v6 = java.lang.Integer.TYPE;
/* aput-object v6, v4, v5 */
/* .line 1787 */
} // :cond_1
/* aget-object v6, p2, v5 */
/* instance-of v6, v6, Ljava/lang/Boolean; */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 1788 */
v6 = java.lang.Boolean.TYPE;
/* aput-object v6, v4, v5 */
/* .line 1789 */
} // :cond_2
/* aget-object v6, p2, v5 */
/* instance-of v6, v6, Ljava/lang/Float; */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 1790 */
v6 = java.lang.Float.TYPE;
/* aput-object v6, v4, v5 */
/* .line 1792 */
} // :cond_3
/* aget-object v6, p2, v5 */
(( java.lang.Object ) v6 ).getClass ( ); // invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* aput-object v6, v4, v5 */
/* .line 1784 */
} // :goto_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 1795 */
} // .end local v5 # "i":I
} // :cond_4
(( java.lang.Class ) v2 ).getDeclaredMethod ( p1, v4 ); // invoke-virtual {v2, p1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* move-object v0, v5 */
/* .line 1796 */
(( java.lang.reflect.Method ) v0 ).setAccessible ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 1797 */
(( java.lang.reflect.Method ) v0 ).invoke ( p0, p2 ); // invoke-virtual {v0, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1798 */
} // .end local v2 # "clazz":Ljava/lang/Class;
} // .end local v4 # "argsClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
/* :catch_0 */
/* move-exception v2 */
/* .line 1799 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getDeclaredMethod:"; // const-string v4, "getDeclaredMethod:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "ATMSImpl"; // const-string v4, "ATMSImpl"
android.util.Slog .d ( v4,v3 );
/* .line 1801 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // .end method
private Boolean isCTS ( ) {
/* .locals 2 */
/* .line 961 */
/* nop */
/* .line 962 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 961 */
/* xor-int/lit8 v0, v0, 0x1 */
final String v1 = "persist.sys.miui_optimization"; // const-string v1, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
private Boolean isSubScreenFeatureOn ( android.content.Context p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "userId" # I */
/* .line 966 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 967 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "subscreen_switch" */
v0 = android.provider.MiuiSettings$System .getBooleanForUser ( v0,v2,v1,p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* nop */
/* .line 966 */
} // :goto_0
} // .end method
private static Boolean isSystemBootCompleted ( ) {
/* .locals 2 */
/* .line 569 */
/* sget-boolean v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->sSystemBootCompleted:Z */
/* if-nez v0, :cond_0 */
/* .line 570 */
/* const-string/jumbo v0, "sys.boot_completed" */
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.android.server.wm.ActivityTaskManagerServiceImpl.sSystemBootCompleted = (v0!= 0);
/* .line 572 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->sSystemBootCompleted:Z */
} // .end method
private void lambda$animationOut$1 ( android.view.SurfaceControl$Transaction p0, android.animation.ValueAnimator p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "animation" # Landroid/animation/ValueAnimator; */
/* .line 1430 */
(( android.animation.ValueAnimator ) p2 ).getAnimatedValue ( ); // invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 1431 */
/* .local v0, "alpha":F */
v1 = this.mScreenshotLayer;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1432 */
(( android.view.SurfaceControl$Transaction ) p1 ).setAlpha ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
(( android.view.SurfaceControl$Transaction ) v1 ).apply ( ); // invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 1434 */
} // :cond_0
return;
} // .end method
static Boolean lambda$getTopTaskVisibleActivities$3 ( com.android.server.wm.ActivityRecord p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1719 */
v0 = com.android.server.wm.ActivityRecord$State.RESUMED;
v0 = (( com.android.server.wm.ActivityRecord ) p0 ).isState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z
} // .end method
static android.content.Intent lambda$getTopTaskVisibleActivities$4 ( com.android.server.wm.ActivityRecord p0 ) { //synthethic
/* .locals 2 */
/* .param p0, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1721 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1722 */
/* .local v0, "intent":Landroid/content/Intent; */
v1 = this.mActivityComponent;
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1723 */
} // .end method
private void lambda$onSystemReady$0 ( ) { //synthethic
/* .locals 0 */
/* .line 269 */
/* invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->registerObserver()V */
return;
} // .end method
private void lambda$showScreenShotForSplitTask$2 ( ) { //synthethic
/* .locals 0 */
/* .line 1501 */
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).removeSplitTaskShotIfNeed ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->removeSplitTaskShotIfNeed()Z
return;
} // .end method
public static void onForegroundWindowChanged ( com.android.server.wm.WindowProcessController p0, android.content.pm.ActivityInfo p1, com.android.server.wm.ActivityRecord p2, com.android.server.wm.ActivityRecord$State p3 ) {
/* .locals 4 */
/* .param p0, "app" # Lcom/android/server/wm/WindowProcessController; */
/* .param p1, "info" # Landroid/content/pm/ActivityInfo; */
/* .param p2, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "state" # Lcom/android/server/wm/ActivityRecord$State; */
/* .line 529 */
if ( p0 != null) { // if-eqz p0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 530 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
/* new-instance v1, Lcom/android/server/wm/FgWindowChangedInfo; */
v2 = this.applicationInfo;
/* .line 531 */
v3 = (( com.android.server.wm.WindowProcessController ) p0 ).getPid ( ); // invoke-virtual {p0}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* invoke-direct {v1, p2, v2, v3}, Lcom/android/server/wm/FgWindowChangedInfo;-><init>(Lcom/android/server/wm/ActivityRecord;Landroid/content/pm/ApplicationInfo;I)V */
/* .line 530 */
(( com.miui.server.process.ProcessManagerInternal ) v0 ).notifyForegroundWindowChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/process/ProcessManagerInternal;->notifyForegroundWindowChanged(Lcom/android/server/wm/FgWindowChangedInfo;)V
/* .line 533 */
} // :cond_0
return;
} // .end method
private void registerObserver ( ) {
/* .locals 5 */
/* .line 840 */
/* invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getSplitScreenBlackListFromXml()V */
/* .line 842 */
/* new-instance v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$4; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$4;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/os/Handler;)V */
/* .line 848 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = com.android.server.wm.ActivityTaskManagerServiceImpl.URI_CLOUD_ALL_DATA_NOTIFY;
int v3 = -1; // const/4 v3, -0x1
int v4 = 0; // const/4 v4, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v0, v3 ); // invoke-virtual {v1, v2, v4, v0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 852 */
(( android.database.ContentObserver ) v0 ).onChange ( v4 ); // invoke-virtual {v0, v4}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 853 */
return;
} // .end method
private void reportForegroundActivityChange ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 11 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 404 */
v0 = this.mProcessManagerIn;
/* .line 405 */
(( com.miui.server.process.ProcessManagerInternal ) v0 ).getMultiWindowForegroundAppInfoLocked ( ); // invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getMultiWindowForegroundAppInfoLocked()Landroid/content/pm/ApplicationInfo;
/* .line 406 */
/* .local v0, "multiWindowAppInfo":Landroid/content/pm/ApplicationInfo; */
(( com.android.server.wm.ActivityRecord ) p1 ).getState ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;
/* .line 407 */
/* .local v7, "state":Lcom/android/server/wm/ActivityRecord$State; */
v1 = this.app;
v8 = (( com.android.server.wm.WindowProcessController ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* .line 409 */
/* .local v8, "pid":I */
com.android.server.MiuiFgThread .getHandler ( );
/* new-instance v10, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2; */
/* move-object v1, v10 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, v7 */
/* move v5, v8 */
/* move-object v6, v0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$2;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;ILandroid/content/pm/ApplicationInfo;)V */
(( android.os.Handler ) v9 ).post ( v10 ); // invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 415 */
return;
} // .end method
private static void reportPackageForeground ( com.android.server.wm.ActivityRecord p0, Integer p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p0, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p1, "pid" # I */
/* .param p2, "lastPkgName" # Ljava/lang/String; */
/* .line 536 */
/* new-instance v0, Lmiui/mqsas/sdk/event/PackageForegroundEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;-><init>()V */
/* .line 537 */
/* .local v0, "event":Lmiui/mqsas/sdk/event/PackageForegroundEvent; */
v1 = this.packageName;
(( miui.mqsas.sdk.event.PackageForegroundEvent ) v0 ).setPackageName ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setPackageName(Ljava/lang/String;)V
/* .line 538 */
v1 = this.shortComponentName;
(( miui.mqsas.sdk.event.PackageForegroundEvent ) v0 ).setComponentName ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setComponentName(Ljava/lang/String;)V
/* .line 539 */
v1 = java.lang.System .identityHashCode ( p0 );
(( miui.mqsas.sdk.event.PackageForegroundEvent ) v0 ).setIdentity ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setIdentity(I)V
/* .line 540 */
(( miui.mqsas.sdk.event.PackageForegroundEvent ) v0 ).setPid ( p1 ); // invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setPid(I)V
/* .line 541 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
(( miui.mqsas.sdk.event.PackageForegroundEvent ) v0 ).setForegroundTime ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setForegroundTime(J)V
/* .line 542 */
v1 = v1 = this.mActivityRecordStub;
(( miui.mqsas.sdk.event.PackageForegroundEvent ) v0 ).setColdStart ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setColdStart(Z)V
/* .line 543 */
(( miui.mqsas.sdk.event.PackageForegroundEvent ) v0 ).setLastPackageName ( p2 ); // invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/PackageForegroundEvent;->setLastPackageName(Ljava/lang/String;)V
/* .line 544 */
v1 = com.android.server.wm.ActivityTaskManagerServiceImpl.sCachedForegroundPackageList;
v2 = /* .line 546 */
/* if-lt v2, v3, :cond_0 */
/* .line 547 */
v2 = com.android.server.wm.ActivityTaskManagerServiceImpl .isSystemBootCompleted ( );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 548 */
final String v2 = "ATMSImpl"; // const-string v2, "ATMSImpl"
final String v3 = "Begin to report package foreground events..."; // const-string v3, "Begin to report package foreground events..."
android.util.Slog .d ( v2,v3 );
/* .line 549 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 550 */
/* .local v2, "events":Ljava/util/List;, "Ljava/util/List<Lmiui/mqsas/sdk/event/PackageForegroundEvent;>;" */
/* .line 551 */
/* .line 553 */
com.android.server.wm.ActivityTaskManagerServiceImpl .reportPackageForegroundEvents ( v2 );
/* .line 555 */
} // .end local v2 # "events":Ljava/util/List;, "Ljava/util/List<Lmiui/mqsas/sdk/event/PackageForegroundEvent;>;"
} // :cond_0
return;
} // .end method
private static void reportPackageForegroundEvents ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lmiui/mqsas/sdk/event/PackageForegroundEvent;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 558 */
/* .local p0, "events":Ljava/util/List;, "Ljava/util/List<Lmiui/mqsas/sdk/event/PackageForegroundEvent;>;" */
/* new-instance v0, Landroid/content/pm/ParceledListSlice; */
/* invoke-direct {v0, p0}, Landroid/content/pm/ParceledListSlice;-><init>(Ljava/util/List;)V */
/* .line 560 */
/* .local v0, "reportEvents":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Lmiui/mqsas/sdk/event/PackageForegroundEvent;>;" */
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$3; */
/* invoke-direct {v2, v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$3;-><init>(Landroid/content/pm/ParceledListSlice;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 566 */
return;
} // .end method
private Boolean shouldNotStartSubscreen ( ) {
/* .locals 7 */
/* .line 972 */
v0 = this.mAtmService;
v0 = this.mRootWindowContainer;
/* .line 973 */
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.wm.RootWindowContainer ) v0 ).getDisplayContent ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/RootWindowContainer;->getDisplayContent(I)Lcom/android/server/wm/DisplayContent;
/* .line 974 */
/* .local v0, "display":Lcom/android/server/wm/DisplayContent; */
int v1 = 0; // const/4 v1, 0x0
/* .line 975 */
/* .local v1, "topRunningActivity":Lcom/android/server/wm/ActivityRecord; */
int v2 = 0; // const/4 v2, 0x0
/* .line 976 */
/* .local v2, "switchUser":Z */
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 977 */
(( com.android.server.wm.DisplayContent ) v0 ).topRunningActivity ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->topRunningActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 978 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v5, v1, Lcom/android/server/wm/ActivityRecord;->mUserId:I */
v6 = this.mAtmService;
v6 = (( com.android.server.wm.ActivityTaskManagerService ) v6 ).getCurrentUserId ( ); // invoke-virtual {v6}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I
/* if-eq v5, v6, :cond_0 */
/* move v5, v3 */
} // :cond_0
/* move v5, v4 */
} // :goto_0
/* move v2, v5 */
/* .line 979 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "shouldNotStartSubscreen topRunningActivity=" */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = ", switchUser="; // const-string v6, ", switchUser="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "ATMSImpl"; // const-string v6, "ATMSImpl"
android.util.Slog .d ( v6,v5 );
/* .line 981 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_3
v5 = (( com.android.server.wm.DisplayContent ) v0 ).isSleeping ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->isSleeping()Z
if ( v5 != null) { // if-eqz v5, :cond_2
if ( v1 != null) { // if-eqz v1, :cond_2
v5 = (( com.android.server.wm.ActivityRecord ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getPid()I
/* if-lez v5, :cond_2 */
/* if-nez v2, :cond_2 */
} // :cond_2
/* move v3, v4 */
} // :cond_3
} // :goto_1
} // .end method
private Boolean skipReportForegroundActivityChange ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 4 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 452 */
com.android.server.wm.PreloadStateManagerStub .get ( );
v0 = v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayId()I
int v1 = 1; // const/4 v1, 0x1
final String v2 = "ATMSImpl"; // const-string v2, "ATMSImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 453 */
final String v0 = "do not report preloadApp event"; // const-string v0, "do not report preloadApp event"
android.util.Slog .i ( v2,v0 );
/* .line 454 */
/* .line 456 */
} // :cond_0
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 457 */
v0 = (( com.android.server.wm.ActivityRecord ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayId()I
int v3 = 2; // const/4 v3, 0x2
/* if-ne v3, v0, :cond_1 */
/* .line 458 */
final String v0 = "do not report subscreen event"; // const-string v0, "do not report subscreen event"
android.util.Slog .i ( v2,v0 );
/* .line 459 */
/* .line 461 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void startSoSc ( com.android.server.wm.Task p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .param p2, "position" # I */
/* .line 1405 */
try { // :try_start_0
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).showScreenShotForSplitTask ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->showScreenShotForSplitTask()V
/* .line 1406 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.wm.Task ) p1 ).setHasBeenVisible ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->setHasBeenVisible(Z)V
/* .line 1407 */
(( com.android.server.wm.Task ) p1 ).sendTaskAppeared ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->sendTaskAppeared()V
/* .line 1408 */
v0 = this.mAtmService;
v0 = this.mTaskOrganizerController;
(( com.android.server.wm.TaskOrganizerController ) v0 ).dispatchPendingEvents ( ); // invoke-virtual {v0}, Lcom/android/server/wm/TaskOrganizerController;->dispatchPendingEvents()V
/* .line 1409 */
com.android.server.wm.MiuiSoScManagerStub .get ( );
/* iget v1, p1, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.MiuiSoScManagerStub ) v0 ).startTaskInSoSc ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lcom/android/server/wm/MiuiSoScManagerStub;->startTaskInSoSc(II)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1412 */
/* .line 1410 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1411 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1413 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void startSubScreenUi ( android.content.pm.ApplicationInfo p0, java.lang.String p1 ) {
/* .locals 9 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 931 */
/* const-string/jumbo v0, "starSubScreenActivity: " */
try { // :try_start_0
/* iget v1, p1, Landroid/content/pm/ApplicationInfo;->uid:I */
v1 = android.os.UserHandle .getUserId ( v1 );
/* .line 932 */
/* .local v1, "userId":I */
v2 = this.mAtmService;
v2 = (( com.android.server.wm.ActivityTaskManagerService ) v2 ).getCurrentUserId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getCurrentUserId()I
/* if-ne v1, v2, :cond_1 */
v2 = this.mAtmService;
v2 = this.mContext;
/* .line 933 */
v2 = /* invoke-direct {p0, v2, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isSubScreenFeatureOn(Landroid/content/Context;I)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 934 */
v2 = /* invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->shouldNotStartSubscreen()Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* goto/16 :goto_0 */
/* .line 937 */
} // :cond_0
android.app.ActivityOptions .makeBasic ( );
/* .line 938 */
/* .local v2, "options":Landroid/app/ActivityOptions; */
int v3 = 1; // const/4 v3, 0x1
(( android.app.ActivityOptions ) v2 ).setLaunchWindowingMode ( v3 ); // invoke-virtual {v2, v3}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V
/* .line 939 */
int v3 = 2; // const/4 v3, 0x2
(( android.app.ActivityOptions ) v2 ).setLaunchDisplayId ( v3 ); // invoke-virtual {v2, v3}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;
/* .line 940 */
/* new-instance v3, Landroid/content/Intent; */
final String v4 = "android.intent.action.MAIN"; // const-string v4, "android.intent.action.MAIN"
/* invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 941 */
/* .local v3, "intent":Landroid/content/Intent; */
/* new-instance v4, Landroid/content/ComponentName; */
final String v5 = "com.xiaomi.misubscreenui"; // const-string v5, "com.xiaomi.misubscreenui"
final String v6 = "com.xiaomi.misubscreenui.SubScreenMainActivity"; // const-string v6, "com.xiaomi.misubscreenui.SubScreenMainActivity"
/* invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 942 */
/* .local v4, "componentName":Landroid/content/ComponentName; */
(( android.content.Intent ) v3 ).setComponent ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 943 */
/* const/high16 v5, 0x10000000 */
(( android.content.Intent ) v3 ).setFlags ( v5 ); // invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 944 */
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v6, 0x400 */
/* .line 947 */
/* .local v5, "aInfo":Landroid/content/pm/ActivityInfo; */
final String v6 = "ATMSImpl"; // const-string v6, "ATMSImpl"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p2 ); // invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " for user "; // const-string v8, " for user "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v7 );
/* .line 948 */
v6 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v6 ).getActivityStartController ( ); // invoke-virtual {v6}, Lcom/android/server/wm/ActivityTaskManagerService;->getActivityStartController()Lcom/android/server/wm/ActivityStartController;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 949 */
(( com.android.server.wm.ActivityStartController ) v6 ).obtainStarter ( v3, v0 ); // invoke-virtual {v6, v3, v0}, Lcom/android/server/wm/ActivityStartController;->obtainStarter(Landroid/content/Intent;Ljava/lang/String;)Lcom/android/server/wm/ActivityStarter;
/* .line 950 */
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.wm.ActivityStarter ) v0 ).setCallingUid ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/wm/ActivityStarter;->setCallingUid(I)Lcom/android/server/wm/ActivityStarter;
/* .line 951 */
(( com.android.server.wm.ActivityStarter ) v0 ).setUserId ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityStarter;->setUserId(I)Lcom/android/server/wm/ActivityStarter;
/* .line 952 */
(( com.android.server.wm.ActivityStarter ) v0 ).setActivityInfo ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/wm/ActivityStarter;->setActivityInfo(Landroid/content/pm/ActivityInfo;)Lcom/android/server/wm/ActivityStarter;
/* .line 953 */
(( android.app.ActivityOptions ) v2 ).toBundle ( ); // invoke-virtual {v2}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;
(( com.android.server.wm.ActivityStarter ) v0 ).setActivityOptions ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/wm/ActivityStarter;->setActivityOptions(Landroid/os/Bundle;)Lcom/android/server/wm/ActivityStarter;
/* .line 954 */
(( com.android.server.wm.ActivityStarter ) v0 ).execute ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityStarter;->execute()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 957 */
/* nop */
} // .end local v1 # "userId":I
} // .end local v2 # "options":Landroid/app/ActivityOptions;
} // .end local v3 # "intent":Landroid/content/Intent;
} // .end local v4 # "componentName":Landroid/content/ComponentName;
} // .end local v5 # "aInfo":Landroid/content/pm/ActivityInfo;
/* .line 935 */
/* .restart local v1 # "userId":I */
} // :cond_1
} // :goto_0
return;
/* .line 955 */
} // .end local v1 # "userId":I
/* :catch_0 */
/* move-exception v0 */
/* .line 956 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 958 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void updateMultiTaskWhiteList ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 796 */
/* nop */
/* .line 797 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "split_screen_applist" */
final String v2 = "miui_multi_task_white_list"; // const-string v2, "miui_multi_task_white_list"
/* .line 796 */
int v3 = 0; // const/4 v3, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v1,v2,v3 );
/* .line 799 */
/* .local v0, "data":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_1 */
/* .line 800 */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 801 */
/* .local v1, "apps":Lorg/json/JSONArray; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-ge v2, v3, :cond_0 */
/* .line 802 */
v3 = this.mMultipleTaskWhiteList;
(( org.json.JSONArray ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 801 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 804 */
} // .end local v2 # "i":I
} // :cond_0
return;
/* .line 808 */
} // .end local v0 # "data":Ljava/lang/String;
} // .end local v1 # "apps":Lorg/json/JSONArray;
} // :cond_1
/* .line 806 */
/* :catch_0 */
/* move-exception v0 */
/* .line 807 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ATMSImpl"; // const-string v1, "ATMSImpl"
final String v2 = "Get multi_task whitelist from xml: "; // const-string v2, "Get multi_task whitelist from xml: "
android.util.Slog .e ( v1,v2,v0 );
/* .line 809 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void addFlipActivityFullScreen ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "setFlipActivityFullScreen" # Ljava/lang/String; */
/* .line 1885 */
v0 = com.android.server.wm.ActivityTaskManagerServiceImpl.BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO;
v1 = (( android.util.ArraySet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 1886 */
(( android.util.ArraySet ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 1888 */
} // :cond_0
return;
} // .end method
public void addTransitionSyncId ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "syncId" # I */
/* .line 1747 */
v0 = this.mTransitionSyncIdList;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1748 */
return;
} // .end method
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .line 1123 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* array-length v0, p2 */
int v1 = 1; // const/4 v1, 0x1
/* if-gt v0, v1, :cond_0 */
/* .line 1126 */
} // :cond_0
/* aget-object v0, p2, v1 */
/* .line 1127 */
/* .local v0, "extCmd":Ljava/lang/String; */
final String v1 = ""; // const-string v1, ""
/* .line 1128 */
/* .local v1, "prefix":Ljava/lang/String; */
final String v2 = "packages"; // const-string v2, "packages"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1129 */
v2 = this.mPackageSettingsManager;
(( com.android.server.wm.PackageSettingsManager ) v2 ).dump ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Lcom/android/server/wm/PackageSettingsManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 1124 */
} // .end local v0 # "extCmd":Ljava/lang/String;
} // .end local v1 # "prefix":Ljava/lang/String;
} // :cond_1
} // :goto_0
final String v0 = "dump nothing for ext!"; // const-string v0, "dump nothing for ext!"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1132 */
} // :cond_2
} // :goto_1
return;
} // .end method
Boolean executeShellCommand ( java.lang.String p0, java.lang.String[] p1, java.io.PrintWriter p2 ) {
/* .locals 2 */
/* .param p1, "command" # Ljava/lang/String; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 1089 */
v0 = this.mMiuiSizeCompatIn;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.wm.MiuiSizeCompatInternal ) v0 ).executeShellCommand ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/MiuiSizeCompatInternal;->executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1090 */
/* .line 1094 */
} // :cond_0
android.appcompat.ApplicationCompatUtilsStub .get ( );
v0 = (( android.appcompat.ApplicationCompatUtilsStub ) v0 ).isContinuityEnabled ( ); // invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isContinuityEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1095 */
com.android.server.wm.AppContinuityRouterStub .get ( );
v0 = (( com.android.server.wm.AppContinuityRouterStub ) v0 ).executeShellCommand ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/AppContinuityRouterStub;->executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1096 */
/* .line 1099 */
} // :cond_1
v0 = this.mPackageConfigurationController;
v0 = (( com.android.server.wm.PackageConfigurationController ) v0 ).executeShellCommand ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/PackageConfigurationController;->executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
} // .end method
public Boolean forceLaunchNewTaskForMultipleTask ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1, Integer p2, android.app.ActivityOptions p3 ) {
/* .locals 4 */
/* .param p1, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "intentActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "launchFlag" # I */
/* .param p4, "options" # Landroid/app/ActivityOptions; */
/* .line 878 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_2
if ( p4 != null) { // if-eqz p4, :cond_2
v1 = this.packageName;
/* .line 879 */
v1 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).inMultipleTaskWhiteList ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->inMultipleTaskWhiteList(Ljava/lang/String;)Z
/* if-nez v1, :cond_0 */
/* .line 883 */
} // :cond_0
(( android.app.ActivityOptions ) p4 ).getLaunchRootTask ( ); // invoke-virtual {p4}, Landroid/app/ActivityOptions;->getLaunchRootTask()Landroid/window/WindowContainerToken;
if ( v1 != null) { // if-eqz v1, :cond_1
/* const/high16 v1, 0x8000000 */
/* and-int/2addr v1, p3 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 885 */
(( android.app.ActivityOptions ) p4 ).getLaunchRootTask ( ); // invoke-virtual {p4}, Landroid/app/ActivityOptions;->getLaunchRootTask()Landroid/window/WindowContainerToken;
com.android.server.wm.Task .fromWindowContainerToken ( v1 );
/* .line 886 */
/* .local v1, "launchRootTask":Lcom/android/server/wm/Task; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-boolean v2, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 887 */
v2 = (( com.android.server.wm.Task ) v1 ).getWindowingMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v3 = 6; // const/4 v3, 0x6
/* if-ne v2, v3, :cond_1 */
/* .line 888 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Use new task for split, intentActivity: "; // const-string v2, "Use new task for split, intentActivity: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ATMSImpl"; // const-string v2, "ATMSImpl"
android.util.Slog .d ( v2,v0 );
/* .line 889 */
int v0 = 1; // const/4 v0, 0x1
/* .line 893 */
} // .end local v1 # "launchRootTask":Lcom/android/server/wm/Task;
} // :cond_1
/* .line 880 */
} // :cond_2
} // :goto_0
} // .end method
public Boolean freeFormAndFullScreenToggleByKeyCombination ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isStartFreeForm" # Z */
/* .line 1728 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "freeFormAndFullScreenToggleByKeyCombination "; // const-string v1, "freeFormAndFullScreenToggleByKeyCombination "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ATMSImpl"; // const-string v1, "ATMSImpl"
android.util.Slog .i ( v1,v0 );
/* .line 1729 */
v0 = this.mAtmService;
v0 = this.mMiuiFreeFormManagerService;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService; */
/* .line 1730 */
/* .local v0, "miuiFreeFormManagerService":Lcom/android/server/wm/MiuiFreeFormManagerService; */
v1 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v1 ).freeFormAndFullScreenToggleByKeyCombination ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->freeFormAndFullScreenToggleByKeyCombination(Z)V
/* .line 1731 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public Integer getActivityControllerUid ( ) {
/* .locals 1 */
/* .line 1690 */
/* iget v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mActivityControlUid:I */
} // .end method
public android.util.ArraySet getAllFlipActivityFullScreen ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1897 */
v0 = com.android.server.wm.ActivityTaskManagerServiceImpl.BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO;
} // .end method
public Integer getAspectGravity ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 591 */
v0 = this.mMiuiSizeCompatIn;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 592 */
v0 = (( com.android.server.wm.MiuiSizeCompatInternal ) v0 ).getAspectGravityByPackage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->getAspectGravityByPackage(Ljava/lang/String;)I
/* .line 594 */
} // :cond_0
/* const/16 v0, 0x11 */
} // .end method
public Float getAspectRatio ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 579 */
v0 = this.mMiuiSizeCompatIn;
/* if-nez v0, :cond_0 */
/* .line 580 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* .line 581 */
} // :cond_0
v0 = (( com.android.server.wm.MiuiSizeCompatInternal ) v0 ).getAspectRatioByPackage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->getAspectRatioByPackage(Ljava/lang/String;)F
} // .end method
public java.util.List getContinuityList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "policyName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 283 */
v0 = this.mFoldablePackagePolicy;
(( com.android.server.wm.FoldablePackagePolicy ) v0 ).getContinuityList ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/FoldablePackagePolicy;->getContinuityList(Ljava/lang/String;)Ljava/util/List;
} // .end method
public Long getContinuityVersion ( ) {
/* .locals 2 */
/* .line 287 */
v0 = this.mFoldablePackagePolicy;
(( com.android.server.wm.FoldablePackagePolicy ) v0 ).getContinuityVersion ( ); // invoke-virtual {v0}, Lcom/android/server/wm/FoldablePackagePolicy;->getContinuityVersion()J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
android.content.res.Configuration getGlobalConfigurationForMiui ( com.android.server.wm.ActivityTaskManagerService p0, com.android.server.wm.WindowProcessController p1 ) {
/* .locals 2 */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "app" # Lcom/android/server/wm/WindowProcessController; */
/* .line 1003 */
/* sget-boolean v0, Lmiui/os/Build;->IS_MIUI:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
v0 = this.mInfo;
v0 = this.packageName;
/* .line 1004 */
final String v1 = "com.android.thememanager"; // const-string v1, "com.android.thememanager"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1005 */
v0 = (( com.android.server.wm.WindowProcessController ) p2 ).getWindowingMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/WindowProcessController;->getWindowingMode()I
int v1 = 6; // const/4 v1, 0x6
/* if-ne v0, v1, :cond_0 */
/* .line 1006 */
(( com.android.server.wm.ActivityTaskManagerService ) p1 ).getGlobalConfiguration ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityTaskManagerService;->getGlobalConfiguration()Landroid/content/res/Configuration;
/* .line 1008 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public android.content.pm.ActivityInfo getLastResumedActivityInfo ( ) {
/* .locals 6 */
/* .line 1012 */
v0 = android.os.Binder .getCallingUid ( );
v0 = android.os.UserHandle .getAppId ( v0 );
/* .line 1013 */
/* .local v0, "uid":I */
v1 = android.os.Binder .getCallingPid ( );
/* .line 1014 */
/* .local v1, "pid":I */
/* const/16 v2, 0x3e8 */
int v3 = 0; // const/4 v3, 0x0
/* if-eq v0, v2, :cond_0 */
final String v2 = "android.permission.REAL_GET_TASKS"; // const-string v2, "android.permission.REAL_GET_TASKS"
/* .line 1015 */
v2 = com.android.server.wm.ActivityTaskManagerService .checkPermission ( v2,v1,v0 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1017 */
final String v2 = "ATMSImpl"; // const-string v2, "ATMSImpl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "permission denied for, callingPid:"; // const-string v5, "permission denied for, callingPid:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " , callingUid:"; // const-string v5, " , callingUid:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", requires: android.Manifest.permission.REAL_GET_TASKS"; // const-string v5, ", requires: android.Manifest.permission.REAL_GET_TASKS"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* .line 1019 */
/* .line 1021 */
} // :cond_0
v2 = this.mAtmService;
v2 = this.mGlobalLock;
/* monitor-enter v2 */
/* .line 1022 */
try { // :try_start_0
v4 = this.mAtmService;
v4 = this.mLastResumedActivity;
/* .line 1023 */
/* .local v4, "activity":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v4, :cond_1 */
} // :cond_1
v3 = this.info;
} // :goto_0
/* monitor-exit v2 */
/* .line 1024 */
} // .end local v4 # "activity":Lcom/android/server/wm/ActivityRecord;
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
Boolean getMetaDataBoolean ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "metaDataKey" # Ljava/lang/String; */
/* .line 1055 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mPackageManager;
v2 = android.os.UserHandle .myUserId ( );
/* const-wide/16 v3, 0x80 */
(( com.android.server.pm.PackageManagerService$IPackageManagerImpl ) v1 ).getApplicationInfo ( p1, v3, v4, v2 ); // invoke-virtual {v1, p1, v3, v4, v2}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;
/* .line 1056 */
/* .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v1, :cond_0 */
/* .line 1057 */
/* .line 1059 */
} // :cond_0
v2 = this.metaData;
/* .line 1060 */
/* .local v2, "metaData":Landroid/os/Bundle; */
if ( v2 != null) { // if-eqz v2, :cond_1
(( android.os.Bundle ) v2 ).get ( p2 ); // invoke-virtual {v2, p2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1061 */
v0 = (( android.os.Bundle ) v2 ).getBoolean ( p2 ); // invoke-virtual {v2, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1065 */
} // .end local v1 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v2 # "metaData":Landroid/os/Bundle;
} // :cond_1
/* .line 1063 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1064 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1067 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
Float getMetaDataFloat ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "metaDataKey" # Ljava/lang/String; */
/* .line 1072 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mPackageManager;
v2 = android.os.UserHandle .myUserId ( );
/* const-wide/16 v3, 0x80 */
(( com.android.server.pm.PackageManagerService$IPackageManagerImpl ) v1 ).getApplicationInfo ( p1, v3, v4, v2 ); // invoke-virtual {v1, p1, v3, v4, v2}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;
/* .line 1073 */
/* .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v1, :cond_0 */
/* .line 1074 */
/* .line 1076 */
} // :cond_0
v2 = this.metaData;
/* .line 1077 */
/* .local v2, "metaData":Landroid/os/Bundle; */
if ( v2 != null) { // if-eqz v2, :cond_1
(( android.os.Bundle ) v2 ).get ( p2 ); // invoke-virtual {v2, p2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1078 */
v0 = (( android.os.Bundle ) v2 ).getFloat ( p2 ); // invoke-virtual {v2, p2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1082 */
} // .end local v1 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v2 # "metaData":Landroid/os/Bundle;
} // :cond_1
/* .line 1080 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1081 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1083 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public com.android.server.wm.MiuiActivityController getMiuiActivityController ( ) {
/* .locals 1 */
/* .line 217 */
v0 = com.android.server.wm.MiuiActivityControllerImpl.INSTANCE;
} // .end method
public com.android.server.wm.MiuiSizeCompatInternal getMiuiSizeCompatIn ( ) {
/* .locals 1 */
/* .line 279 */
v0 = this.mMiuiSizeCompatIn;
} // .end method
public Integer getOrientation ( com.android.server.wm.Task p0 ) {
/* .locals 5 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 1530 */
int v0 = -1; // const/4 v0, -0x1
if ( p1 != null) { // if-eqz p1, :cond_1
(( com.android.server.wm.Task ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1531 */
(( com.android.server.wm.Task ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
(( com.android.server.wm.DisplayContent ) v1 ).getConfiguration ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getConfiguration()Landroid/content/res/Configuration;
/* iget v1, v1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I */
/* const/16 v2, 0x258 */
/* if-ge v1, v2, :cond_0 */
/* .line 1553 */
} // :cond_0
/* .line 1535 */
} // :cond_1
} // :goto_0
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 1536 */
(( com.android.server.wm.Task ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
/* .line 1537 */
/* .local v1, "rootTask":Lcom/android/server/wm/Task; */
/* iget-boolean v2, v1, Lcom/android/server/wm/Task;->mSoScRoot:Z */
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_3
com.android.server.wm.MiuiSoScManagerStub .get ( );
v2 = (( com.android.server.wm.MiuiSoScManagerStub ) v2 ).isInSoScSingleMode ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/MiuiSoScManagerStub;->isInSoScSingleMode(Lcom/android/server/wm/WindowContainer;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = this.mTransitionController;
/* .line 1538 */
v2 = (( com.android.server.wm.TransitionController ) v2 ).isCollecting ( ); // invoke-virtual {v2}, Lcom/android/server/wm/TransitionController;->isCollecting()Z
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = (( com.android.server.wm.Task ) v1 ).isFocusable ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->isFocusable()Z
/* if-nez v2, :cond_3 */
/* .line 1539 */
} // :cond_2
/* .line 1542 */
} // :cond_3
v2 = this.mTransitionSyncIdList;
v2 = (( java.util.HashSet ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z
/* if-nez v2, :cond_4 */
(( com.android.server.wm.Task ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1543 */
(( com.android.server.wm.Task ) p1 ).getDisplayContent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayContent()Lcom/android/server/wm/DisplayContent;
v2 = (( com.android.server.wm.DisplayContent ) v2 ).getRotation ( ); // invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getRotation()I
/* if-nez v2, :cond_4 */
/* iget-boolean v2, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1544 */
v2 = (( com.android.server.wm.Task ) v1 ).hasChild ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->hasChild()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1545 */
(( com.android.server.wm.Task ) v1 ).getTopChild ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
v2 = (( com.android.server.wm.WindowContainer ) v2 ).getWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
int v4 = 6; // const/4 v4, 0x6
/* if-ne v2, v4, :cond_4 */
/* .line 1546 */
/* .line 1550 */
} // .end local v1 # "rootTask":Lcom/android/server/wm/Task;
} // :cond_4
} // .end method
public java.lang.String getPackageHoldOn ( ) {
/* .locals 1 */
/* .line 610 */
v0 = this.mPackageHoldOn;
} // .end method
public Integer getPolicy ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 600 */
v0 = this.mPackageSettingsManager;
v0 = this.mDisplayCompatPackages;
v0 = (( com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager ) v0 ).getPolicy ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->getPolicy(Ljava/lang/String;)I
} // .end method
public Integer getScaleMode ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 585 */
v0 = this.mMiuiSizeCompatIn;
/* if-nez v0, :cond_0 */
/* .line 586 */
int v0 = 0; // const/4 v0, 0x0
/* .line 587 */
} // :cond_0
v0 = (( com.android.server.wm.MiuiSizeCompatInternal ) v0 ).getScaleModeByPackage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->getScaleModeByPackage(Ljava/lang/String;)I
} // .end method
public android.app.ActivityManager$RunningTaskInfo getSplitTaskInfo ( android.os.IBinder p0 ) {
/* .locals 3 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 1104 */
v0 = this.mAtmService;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 1105 */
try { // :try_start_0
com.android.server.wm.ActivityRecord .forTokenLocked ( p1 );
/* .line 1106 */
/* .local v1, "record":Lcom/android/server/wm/ActivityRecord; */
if ( v1 != null) { // if-eqz v1, :cond_1
(( com.android.server.wm.ActivityRecord ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1107 */
(( com.android.server.wm.ActivityRecord ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
v2 = v2 = this.mTaskStub;
/* if-nez v2, :cond_0 */
(( com.android.server.wm.ActivityRecord ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
v2 = (( com.android.server.wm.Task ) v2 ).inFreeformWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1108 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) v1 ).getTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
(( com.android.server.wm.Task ) v2 ).getTaskInfo ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;
/* monitor-exit v0 */
/* .line 1110 */
} // .end local v1 # "record":Lcom/android/server/wm/ActivityRecord;
} // :cond_1
/* monitor-exit v0 */
/* .line 1111 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1110 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.util.List getTopTaskVisibleActivities ( com.android.server.wm.Task p0 ) {
/* .locals 3 */
/* .param p1, "mainStack" # Lcom/android/server/wm/Task; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/wm/Task;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Landroid/content/Intent;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1710 */
/* if-nez p1, :cond_0 */
/* .line 1711 */
java.util.Collections .emptyList ( );
/* .line 1713 */
} // :cond_0
(( com.android.server.wm.Task ) p1 ).topRunningActivitiesLocked ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->topRunningActivitiesLocked()Ljava/util/List;
/* .line 1714 */
/* .local v0, "activityRecordList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;" */
/* if-nez v0, :cond_1 */
/* .line 1715 */
java.util.Collections .emptyList ( );
/* .line 1718 */
} // :cond_1
/* new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda3;-><init>()V */
/* .line 1719 */
/* new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda4; */
/* invoke-direct {v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda4;-><init>()V */
/* .line 1720 */
/* .line 1724 */
java.util.stream.Collectors .toList ( );
/* check-cast v1, Ljava/util/List; */
/* .line 1718 */
} // .end method
public void handleQSOnConfigureChanged ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .param p2, "change" # I */
/* .line 1352 */
android.app.TaskSnapshotHelperStub .get ( );
/* .line 1353 */
return;
} // .end method
Boolean hasMetaData ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "metaDataKey" # Ljava/lang/String; */
/* .line 1038 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mPackageManager;
v2 = android.os.UserHandle .myUserId ( );
/* const-wide/16 v3, 0x80 */
(( com.android.server.pm.PackageManagerService$IPackageManagerImpl ) v1 ).getApplicationInfo ( p1, v3, v4, v2 ); // invoke-virtual {v1, p1, v3, v4, v2}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;
/* .line 1039 */
/* .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v1, :cond_0 */
/* .line 1040 */
/* .line 1042 */
} // :cond_0
v2 = this.metaData;
/* .line 1043 */
/* .local v2, "metaData":Landroid/os/Bundle; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1044 */
(( android.os.Bundle ) v2 ).get ( p2 ); // invoke-virtual {v2, p2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v3 != null) { // if-eqz v3, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* .line 1048 */
} // .end local v1 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v2 # "metaData":Landroid/os/Bundle;
} // :cond_2
/* .line 1046 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1047 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1050 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public Boolean hideLockedProfile ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 3 */
/* .param p1, "mLastResumedActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1766 */
int v0 = 1; // const/4 v0, 0x1
/* if-nez p1, :cond_0 */
/* .line 1767 */
/* .line 1769 */
} // :cond_0
v1 = this.intent;
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v1 ).flattenToShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
final String v2 = "com.android.cts.verifier/.managedprovisioning.RecentsRedactionActivity"; // const-string v2, "com.android.cts.verifier/.managedprovisioning.RecentsRedactionActivity"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/2addr v0, v1 */
} // .end method
public java.lang.String hookGetCallingPkg ( android.os.IBinder p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "originCallingPkg" # Ljava/lang/String; */
/* .line 315 */
int v0 = 0; // const/4 v0, 0x0
/* .line 316 */
/* .local v0, "hostApp":Ljava/lang/String; */
v1 = this.mAtmService;
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 317 */
try { // :try_start_0
com.android.server.wm.ActivityRecord .isInRootTaskLocked ( p1 );
/* .line 318 */
/* .local v2, "r":Lcom/android/server/wm/ActivityRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 319 */
v3 = this.packageName;
/* move-object v0, v3 */
/* .line 321 */
} // .end local v2 # "r":Lcom/android/server/wm/ActivityRecord;
} // :cond_0
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 322 */
com.miui.hybrid.hook.HookClient .hookGetCallingPkg ( v0,p2 );
/* .line 321 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public android.content.Intent hookStartActivity ( android.content.Intent p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .line 308 */
com.miui.hybrid.hook.HookClient .redirectStartActivity ( p1,p2 );
/* .line 309 */
/* .local v0, "redirectedIntent":Landroid/content/Intent; */
final String v1 = "notify"; // const-string v1, "notify"
/* filled-new-array {p1}, [Ljava/lang/Object; */
com.android.server.camera.CameraOpt .callMethod ( v1,v2 );
/* .line 310 */
} // .end method
Boolean ignoreSpecifiedSource ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 368 */
v0 = com.android.server.wm.ActivityTaskManagerServiceImpl.mIgnoreUriCheckPkg;
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
} // .end method
public Boolean inMiuiGameSizeCompat ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 291 */
v0 = this.mMiuiSizeCompatIn;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.wm.MiuiSizeCompatInternal ) v0 ).inMiuiGameSizeCompat ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->inMiuiGameSizeCompat(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean inMultipleTaskWhiteList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 868 */
final String v0 = "android"; // const-string v0, "android"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 869 */
int v0 = 1; // const/4 v0, 0x1
/* .line 873 */
} // :cond_0
v0 = this.mMultipleTaskWhiteList;
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
} // .end method
public Boolean inResizeBlackList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 857 */
v0 = this.mResizeBlackList;
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
} // .end method
void init ( com.android.server.wm.ActivityTaskManagerService p0, android.content.Context p1 ) {
/* .locals 2 */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 222 */
this.mAtmService = p1;
/* .line 223 */
v0 = this.mTaskSupervisor;
this.mTaskSupervisor = v0;
/* .line 224 */
this.mContext = p2;
/* .line 234 */
com.android.server.wm.MiuiOrientationImpl .getInstance ( );
v1 = this.mContext;
(( com.android.server.wm.MiuiOrientationImpl ) v0 ).init ( v1, p0, p1 ); // invoke-virtual {v0, v1, p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->init(Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Lcom/android/server/wm/ActivityTaskManagerService;)V
/* .line 236 */
com.android.server.wm.ActivityStarterImpl .getInstance ( );
v1 = this.mContext;
(( com.android.server.wm.ActivityStarterImpl ) v0 ).init ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/ActivityStarterImpl;->init(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/content/Context;)V
/* .line 238 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v0;
/* .line 239 */
com.android.server.wm.PassivePenAppWhiteListImpl .getInstance ( );
v1 = this.mContext;
(( com.android.server.wm.PassivePenAppWhiteListImpl ) v0 ).init ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->init(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/content/Context;)V
/* .line 240 */
return;
} // .end method
public Boolean isAppSizeCompatRestarting ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1357 */
v0 = this.mMiuiSizeCompatIn;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.wm.MiuiSizeCompatInternal ) v0 ).isAppSizeCompatRestarting ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->isAppSizeCompatRestarting(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isCallerRecents ( com.android.server.wm.ActivityTaskManagerService p0, Integer p1, android.content.ComponentName p2 ) {
/* .locals 1 */
/* .param p1, "service" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "uid" # I */
/* .param p3, "componentName" # Landroid/content/ComponentName; */
/* .line 1947 */
v0 = (( com.android.server.wm.ActivityTaskManagerService ) p1 ).isCallerRecents ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/wm/ActivityTaskManagerService;->isCallerRecents(I)Z
/* if-nez v0, :cond_1 */
v0 = (( com.android.server.wm.ActivityTaskManagerService ) p1 ).isFlipComponent ( p3 ); // invoke-virtual {p1, p3}, Lcom/android/server/wm/ActivityTaskManagerService;->isFlipComponent(Landroid/content/ComponentName;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean isCameraForeground ( ) {
/* .locals 2 */
/* .line 1321 */
v0 = com.android.server.wm.ActivityTaskManagerServiceImpl.lastForegroundPkg;
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
v0 = android.text.TextUtils .equals ( v0,v1 );
} // .end method
public Boolean isControllerAMonkey ( ) {
/* .locals 1 */
/* .line 1805 */
v0 = miui.os.Build .isDebuggable ( );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAtmService;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mController;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAtmService;
/* iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mControllerIsAMonkey:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isFixedAspectRatioPackage ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 1115 */
v0 = this.mMiuiSizeCompatIn;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1116 */
/* .line 1117 */
} // :cond_0
v0 = (( com.android.server.wm.MiuiSizeCompatInternal ) v0 ).getAspectRatioByPackage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->getAspectRatioByPackage(Ljava/lang/String;)F
int v2 = 0; // const/4 v2, 0x0
/* cmpl-float v0, v0, v2 */
/* if-lez v0, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
public Boolean isFreeFormExit ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 1230 */
v0 = this.mAtmService;
v0 = this.mMiuiFreeFormManagerService;
/* check-cast v0, Lcom/android/server/wm/MiuiFreeFormManagerService; */
/* .line 1232 */
/* .local v0, "miuiFreeFormManagerService":Lcom/android/server/wm/MiuiFreeFormManagerService; */
/* nop */
/* .line 1233 */
(( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).getMiuiFreeFormActivityStack ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(Ljava/lang/String;I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
/* check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
/* .line 1234 */
/* .local v1, "miuiFreeFormActivityStack":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
v2 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1237 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1235 */
v2 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).isFrontFreeFormStackInfo ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isFrontFreeFormStackInfo()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1236 */
v2 = (( com.android.server.wm.MiuiFreeFormActivityStack ) v1 ).isHideStackFromFullScreen ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isHideStackFromFullScreen()Z
/* if-nez v2, :cond_0 */
v2 = this.mTask;
/* iget v2, v2, Lcom/android/server/wm/Task;->mTaskId:I */
/* .line 1237 */
v2 = (( com.android.server.wm.MiuiFreeFormManagerService ) v0 ).isAppBehindHome ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiFreeFormManagerService;->isAppBehindHome(I)Z
/* if-nez v2, :cond_0 */
} // :cond_0
/* move v3, v4 */
/* .line 1238 */
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_2
/* move v3, v4 */
/* .line 1234 */
} // :goto_0
} // .end method
Boolean isGetTasksOpAllowed ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "caller" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .param p3, "uid" # I */
/* .line 327 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 328 */
/* .line 330 */
} // :cond_0
final String v0 = "getRunningAppProcesses"; // const-string v0, "getRunningAppProcesses"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 331 */
/* .line 334 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 335 */
/* .local v0, "packageName":Ljava/lang/String; */
v2 = this.mAtmService;
v2 = this.mGlobalLock;
/* monitor-enter v2 */
/* .line 336 */
try { // :try_start_0
v3 = this.mAtmService;
v3 = this.mProcessMap;
(( com.android.server.wm.WindowProcessControllerMap ) v3 ).getProcess ( p2 ); // invoke-virtual {v3, p2}, Lcom/android/server/wm/WindowProcessControllerMap;->getProcess(I)Lcom/android/server/wm/WindowProcessController;
/* .line 337 */
/* .local v3, "wpc":Lcom/android/server/wm/WindowProcessController; */
if ( v3 != null) { // if-eqz v3, :cond_2
v4 = this.mInfo;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 338 */
v4 = this.mInfo;
v4 = this.packageName;
/* move-object v0, v4 */
/* .line 340 */
} // .end local v3 # "wpc":Lcom/android/server/wm/WindowProcessController;
} // :cond_2
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 341 */
/* if-nez v0, :cond_3 */
/* .line 342 */
/* .line 344 */
} // :cond_3
v2 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v2 ).getAppOpsManager ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getAppOpsManager()Landroid/app/AppOpsManager;
/* .line 345 */
/* .local v2, "opsManager":Landroid/app/AppOpsManager; */
/* const/16 v3, 0x2723 */
v3 = (( android.app.AppOpsManager ) v2 ).checkOp ( v3, p3, v0 ); // invoke-virtual {v2, v3, p3, v0}, Landroid/app/AppOpsManager;->checkOp(IILjava/lang/String;)I
/* if-nez v3, :cond_4 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_4
/* .line 340 */
} // .end local v2 # "opsManager":Landroid/app/AppOpsManager;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isInSystemSplitScreen ( com.android.server.wm.WindowContainer p0 ) {
/* .locals 8 */
/* .param p1, "wc" # Lcom/android/server/wm/WindowContainer; */
/* .line 1647 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 1648 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1649 */
/* .local v1, "task":Lcom/android/server/wm/Task; */
/* instance-of v2, p1, Lcom/android/server/wm/Task; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1650 */
(( com.android.server.wm.WindowContainer ) p1 ).asTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
/* .line 1651 */
} // :cond_1
/* instance-of v2, p1, Lcom/android/server/wm/ActivityRecord; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1652 */
/* move-object v2, p1 */
/* check-cast v2, Lcom/android/server/wm/ActivityRecord; */
(( com.android.server.wm.ActivityRecord ) v2 ).getTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;
/* .line 1653 */
} // :cond_2
/* instance-of v2, p1, Lcom/android/server/wm/WindowState; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1654 */
/* move-object v2, p1 */
/* check-cast v2, Lcom/android/server/wm/WindowState; */
(( com.android.server.wm.WindowState ) v2 ).getTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;
/* .line 1656 */
} // :cond_3
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_4
(( com.android.server.wm.Task ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getPackageName()Ljava/lang/String;
v3 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).skipTaskForMultiWindow ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->skipTaskForMultiWindow(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1657 */
(( com.android.server.wm.WindowContainer ) p1 ).getTaskDisplayArea ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowContainer;->getTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
(( com.android.server.wm.TaskDisplayArea ) v3 ).getNextFocusableRootTask ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Lcom/android/server/wm/TaskDisplayArea;->getNextFocusableRootTask(Lcom/android/server/wm/Task;Z)Lcom/android/server/wm/Task;
/* .line 1659 */
} // :cond_4
/* if-nez v1, :cond_5 */
/* .line 1660 */
} // :cond_5
v3 = (( com.android.server.wm.Task ) v1 ).isRootTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->isRootTask()Z
/* .line 1661 */
/* .local v3, "isRoot":Z */
int v4 = 6; // const/4 v4, 0x6
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 1662 */
(( com.android.server.wm.Task ) v1 ).getTopChild ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
/* .line 1663 */
/* .local v5, "container":Lcom/android/server/wm/WindowContainer; */
/* iget-boolean v6, v1, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v6 != null) { // if-eqz v6, :cond_6
v6 = (( com.android.server.wm.Task ) v1 ).getChildCount ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getChildCount()I
int v7 = 2; // const/4 v7, 0x2
/* if-ne v6, v7, :cond_6 */
/* .line 1664 */
v6 = (( com.android.server.wm.Task ) v1 ).getWindowingMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-ne v6, v2, :cond_6 */
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 1666 */
(( com.android.server.wm.WindowContainer ) v5 ).asTask ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 1667 */
v6 = (( com.android.server.wm.WindowContainer ) v5 ).getWindowingMode ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
/* if-ne v6, v4, :cond_6 */
/* .line 1668 */
v4 = (( com.android.server.wm.WindowContainer ) v5 ).hasChild ( ); // invoke-virtual {v5}, Lcom/android/server/wm/WindowContainer;->hasChild()Z
if ( v4 != null) { // if-eqz v4, :cond_6
/* move v0, v2 */
} // :cond_6
/* nop */
/* .line 1663 */
} // :goto_1
/* .line 1670 */
} // .end local v5 # "container":Lcom/android/server/wm/WindowContainer;
} // :cond_7
(( com.android.server.wm.Task ) v1 ).getRootTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
/* .line 1671 */
/* .local v5, "rootTask":Lcom/android/server/wm/Task; */
/* if-nez v5, :cond_8 */
/* .line 1672 */
} // :cond_8
(( com.android.server.wm.Task ) v5 ).getTopChild ( ); // invoke-virtual {v5}, Lcom/android/server/wm/Task;->getTopChild()Lcom/android/server/wm/WindowContainer;
/* .line 1673 */
/* .local v6, "container":Lcom/android/server/wm/WindowContainer; */
/* iget-boolean v7, v5, Lcom/android/server/wm/Task;->mCreatedByOrganizer:Z */
if ( v7 != null) { // if-eqz v7, :cond_9
v7 = (( com.android.server.wm.Task ) v5 ).hasChild ( ); // invoke-virtual {v5}, Lcom/android/server/wm/Task;->hasChild()Z
if ( v7 != null) { // if-eqz v7, :cond_9
/* .line 1674 */
v7 = (( com.android.server.wm.Task ) v5 ).getWindowingMode ( ); // invoke-virtual {v5}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* if-ne v7, v2, :cond_9 */
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 1675 */
(( com.android.server.wm.WindowContainer ) v6 ).asTask ( ); // invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->asTask()Lcom/android/server/wm/Task;
if ( v7 != null) { // if-eqz v7, :cond_9
/* .line 1676 */
v7 = (( com.android.server.wm.WindowContainer ) v6 ).getWindowingMode ( ); // invoke-virtual {v6}, Lcom/android/server/wm/WindowContainer;->getWindowingMode()I
/* if-ne v7, v4, :cond_9 */
/* move v0, v2 */
} // :cond_9
/* nop */
/* .line 1673 */
} // :goto_2
} // .end method
public Boolean isSplitExist ( java.lang.String p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 1243 */
v0 = this.mAtmService;
v0 = (( com.android.server.wm.ActivityTaskManagerService ) v0 ).isInSplitScreenWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z
/* if-nez v0, :cond_0 */
/* .line 1244 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1246 */
} // :cond_0
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).retrieveFirstSplitWindowRootTaskInfo ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->retrieveFirstSplitWindowRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;
/* .line 1247 */
/* .local v0, "rootTaskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1248 */
/* .local v1, "isInSplitWindow":Z */
final String v2 = "ATMSImpl"; // const-string v2, "ATMSImpl"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1249 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getChildComponentNames ( v0 );
/* .line 1250 */
/* .local v3, "componentNames":[Landroid/content/ComponentName; */
com.android.server.wm.ActivityTaskManagerServiceImpl .getChildTaskUserIds ( v0 );
/* .line 1251 */
/* .local v4, "childTaskUserIds":[I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "isInSplitWindow: childTaskNames = "; // const-string v6, "isInSplitWindow: childTaskNames = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " childTaskUserIds ="; // const-string v6, " childTaskUserIds ="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1252 */
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1251 */
android.util.Slog .d ( v2,v5 );
/* .line 1253 */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* array-length v6, v4 */
/* if-ge v5, v6, :cond_2 */
/* array-length v6, v3 */
/* if-ge v5, v6, :cond_2 */
/* .line 1254 */
/* aget-object v6, v3, v5 */
if ( v6 != null) { // if-eqz v6, :cond_1
/* aget-object v6, v3, v5 */
(( android.content.ComponentName ) v6 ).getPackageName ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v6 = android.text.TextUtils .equals ( p1,v6 );
if ( v6 != null) { // if-eqz v6, :cond_1
/* aget v6, v4, v5 */
/* if-ne v6, p2, :cond_1 */
/* .line 1256 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1257 */
/* .line 1253 */
} // :cond_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 1261 */
} // .end local v3 # "componentNames":[Landroid/content/ComponentName;
} // .end local v4 # "childTaskUserIds":[I
} // .end local v5 # "i":I
} // :cond_2
} // :goto_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "isInSplitWindow: "; // const-string v4, "isInSplitWindow: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1262 */
} // .end method
public Boolean isVerticalSplit ( ) {
/* .locals 2 */
/* .line 1561 */
v0 = this.mAtmService;
v0 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v0 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
(( com.android.server.wm.TaskDisplayArea ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Lcom/android/server/wm/TaskDisplayArea;->getConfiguration()Landroid/content/res/Configuration;
/* iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I */
/* const/16 v1, 0x258 */
/* if-lt v0, v1, :cond_0 */
/* .line 1563 */
com.android.server.wm.WindowManagerServiceImpl .getInstance ( );
v0 = (( com.android.server.wm.WindowManagerServiceImpl ) v0 ).isCtsModeEnabled ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerServiceImpl;->isCtsModeEnabled()Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1561 */
} // :goto_0
} // .end method
public android.content.ComponentName loadFlipComponent ( ) {
/* .locals 1 */
/* .line 1940 */
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
/* if-nez v0, :cond_0 */
/* .line 1941 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1943 */
} // :cond_0
final String v0 = "com.miui.fliphome/.FlipLauncher"; // const-string v0, "com.miui.fliphome/.FlipLauncher"
android.content.ComponentName .unflattenFromString ( v0 );
} // .end method
public void notifyActivityResumed ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.WindowManagerService p1 ) {
/* .locals 11 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "wms" # Lcom/android/server/wm/WindowManagerService; */
/* .line 1326 */
final String v0 = "ATMSImpl"; // const-string v0, "ATMSImpl"
/* if-nez p1, :cond_1 */
/* .line 1327 */
v1 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_STARTING_WINDOW;
v1 = (( com.android.internal.protolog.ProtoLogGroup ) v1 ).isLogToLogcat ( ); // invoke-virtual {v1}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1328 */
final String v1 = "NotifyActivityResumed failed, activity = null"; // const-string v1, "NotifyActivityResumed failed, activity = null"
android.util.Slog .i ( v0,v1 );
/* .line 1330 */
} // :cond_0
return;
/* .line 1332 */
} // :cond_1
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1333 */
v1 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_STARTING_WINDOW;
v1 = (( com.android.internal.protolog.ProtoLogGroup ) v1 ).isLogToLogcat ( ); // invoke-virtual {v1}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1334 */
final String v1 = "In MultiWindowMode, wont do snapshot, return !...."; // const-string v1, "In MultiWindowMode, wont do snapshot, return !...."
android.util.Slog .i ( v0,v1 );
/* .line 1336 */
} // :cond_2
return;
/* .line 1338 */
} // :cond_3
v2 = this.mSmartPowerService;
v1 = this.info;
v3 = this.name;
v4 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
/* .line 1339 */
v5 = (( com.android.server.wm.ActivityRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I
v6 = this.packageName;
/* iget v7, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromUid:I */
/* iget v8, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I */
v9 = this.launchedFromPackage;
v1 = this.mActivityRecordStub;
v10 = /* .line 1340 */
/* .line 1338 */
/* invoke-interface/range {v2 ..v10}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onActivityReusmeUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V */
/* .line 1343 */
com.android.server.wm.TaskSnapshotControllerInjectorStub .get ( );
v1 = (( com.android.server.wm.TaskSnapshotControllerInjectorStub ) v1 ).canTakeSnapshot ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/TaskSnapshotControllerInjectorStub;->canTakeSnapshot(Lcom/android/server/wm/ActivityRecord;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1344 */
v0 = this.mTaskSnapshotController;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.wm.TaskSnapshotController ) v0 ).notifyAppResumed ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/TaskSnapshotController;->notifyAppResumed(Lcom/android/server/wm/ActivityRecord;Z)V
/* .line 1345 */
} // :cond_4
v1 = com.android.internal.protolog.ProtoLogGroup.WM_DEBUG_STARTING_WINDOW;
v1 = (( com.android.internal.protolog.ProtoLogGroup ) v1 ).isLogToLogcat ( ); // invoke-virtual {v1}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1346 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "No snapshot since not start by launcher, activity="; // const-string v2, "No snapshot since not start by launcher, activity="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mActivityComponent;
/* .line 1347 */
(( android.content.ComponentName ) v2 ).flattenToShortString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1346 */
android.util.Slog .i ( v0,v1 );
/* .line 1349 */
} // :cond_5
} // :goto_0
return;
} // .end method
public void onConfigurationChanged ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 352 */
return;
} // .end method
void onForegroundActivityChanged ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord$State p1, Integer p2, android.content.pm.ApplicationInfo p3 ) {
/* .locals 5 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "state" # Lcom/android/server/wm/ActivityRecord$State; */
/* .param p3, "pid" # I */
/* .param p4, "multiWindowAppInfo" # Landroid/content/pm/ApplicationInfo; */
/* .line 466 */
if ( p1 != null) { // if-eqz p1, :cond_4
v0 = this.app;
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = this.packageName;
v0 = android.text.TextUtils .isEmpty ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_0 */
/* .line 471 */
} // :cond_0
com.android.server.wm.OneTrackRotationHelper .getInstance ( );
v1 = this.packageName;
(( com.android.server.wm.OneTrackRotationHelper ) v0 ).reportPackageForeground ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/OneTrackRotationHelper;->reportPackageForeground(Ljava/lang/String;)V
/* .line 473 */
int v0 = 0; // const/4 v0, 0x0
/* .line 474 */
/* .local v0, "skipReportForeground":Z */
v1 = this.mAtmService;
v1 = this.mGlobalLock;
/* monitor-enter v1 */
/* .line 475 */
try { // :try_start_0
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).isTopRunningActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isTopRunningActivity()Z
/* if-nez v2, :cond_1 */
v2 = this.mAtmService;
v2 = (( com.android.server.wm.ActivityTaskManagerService ) v2 ).isInSplitScreenWindowingMode ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 476 */
final String v2 = "ATMSImpl"; // const-string v2, "ATMSImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Don\'t report foreground because "; // const-string v4, "Don\'t report foreground because "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.shortComponentName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " is not top running."; // const-string v4, " is not top running."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 477 */
int v0 = 1; // const/4 v0, 0x1
/* .line 479 */
} // :cond_1
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 481 */
v1 = this.packageName;
v2 = com.android.server.wm.ActivityTaskManagerServiceImpl.lastForegroundPkg;
v1 = android.text.TextUtils .equals ( v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = com.android.server.wm.ActivityTaskManagerServiceImpl.lastMultiWindowAppInfo;
/* if-eq v1, p4, :cond_3 */
} // :cond_2
/* if-nez v0, :cond_3 */
/* .line 485 */
v1 = this.mProcessManagerIn;
/* new-instance v2, Lcom/android/server/wm/FgActivityChangedInfo; */
/* invoke-direct {v2, p1, p2, p3, p4}, Lcom/android/server/wm/FgActivityChangedInfo;-><init>(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;ILandroid/content/pm/ApplicationInfo;)V */
(( com.miui.server.process.ProcessManagerInternal ) v1 ).notifyForegroundInfoChanged ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/process/ProcessManagerInternal;->notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V
/* .line 489 */
v1 = com.android.server.wm.ActivityTaskManagerServiceImpl.lastForegroundPkg;
com.android.server.wm.ActivityTaskManagerServiceImpl .reportPackageForeground ( p1,p3,v1 );
/* .line 491 */
v1 = this.packageName;
/* .line 492 */
/* .line 496 */
} // :cond_3
com.android.server.wm.MiuiMiPerfStub .getInstance ( );
/* .line 499 */
com.android.server.appcacheopt.AppCacheOptimizerStub .getInstance ( );
v2 = this.packageName;
/* .line 505 */
v1 = this.mProcessManagerIn;
v2 = this.mActivityComponent;
(( com.miui.server.process.ProcessManagerInternal ) v1 ).notifyActivityChanged ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/process/ProcessManagerInternal;->notifyActivityChanged(Landroid/content/ComponentName;)V
/* .line 506 */
return;
/* .line 479 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* .line 467 */
} // .end local v0 # "skipReportForeground":Z
} // :cond_4
} // :goto_0
final String v0 = "ATMSImpl"; // const-string v0, "ATMSImpl"
final String v1 = "next or next process is null, skip report!"; // const-string v1, "next or next process is null, skip report!"
android.util.Slog .w ( v0,v1 );
/* .line 468 */
return;
} // .end method
public void onForegroundActivityChangedLocked ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 14 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 422 */
v0 = this.app;
if ( v0 != null) { // if-eqz v0, :cond_5
(( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
/* if-nez v0, :cond_0 */
/* goto/16 :goto_0 */
/* .line 425 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
v0 = (( com.android.server.wm.Task ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getWindowingMode()I
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_1 */
/* .line 426 */
final String v0 = "ATMSImpl"; // const-string v0, "ATMSImpl"
final String v1 = "do not report freeform event"; // const-string v1, "do not report freeform event"
android.util.Slog .i ( v0,v1 );
/* .line 427 */
return;
/* .line 429 */
} // :cond_1
v2 = this.mSmartPowerService;
v0 = this.info;
v3 = this.name;
/* .line 430 */
v4 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v5 = (( com.android.server.wm.ActivityRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getPid()I
v6 = this.packageName;
/* iget v7, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromUid:I */
/* iget v8, p1, Lcom/android/server/wm/ActivityRecord;->launchedFromPid:I */
v9 = this.launchedFromPackage;
v0 = this.mActivityRecordStub;
v10 = /* .line 432 */
/* .line 433 */
(( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
/* iget v11, v0, Lcom/android/server/wm/Task;->mTaskId:I */
(( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
/* iget v12, v0, Lcom/android/server/wm/Task;->mCallingUid:I */
/* .line 434 */
(( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
v13 = this.mCallingPackage;
/* .line 429 */
/* invoke-interface/range {v2 ..v13}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onForegroundActivityChangedLocked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;ZIILjava/lang/String;)V */
/* .line 435 */
com.android.server.PinnerServiceStub .get ( );
v1 = this.packageName;
/* .line 437 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->skipReportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
return;
/* .line 439 */
} // :cond_2
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 440 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getUid()I
v2 = this.packageName;
/* .line 441 */
v3 = (( com.android.server.wm.ActivityRecord ) p1 ).inMultiWindowMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z
/* .line 440 */
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).notifyResumeTopActivity ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyResumeTopActivity(ILjava/lang/String;Z)V
/* .line 443 */
} // :cond_3
/* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->reportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)V */
/* .line 445 */
android.appcompat.ApplicationCompatUtilsStub .get ( );
v0 = (( android.appcompat.ApplicationCompatUtilsStub ) v0 ).isContinuityEnabled ( ); // invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isContinuityEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 446 */
com.android.server.wm.AppContinuityRouterStub .get ( );
(( com.android.server.wm.AppContinuityRouterStub ) v0 ).onForegroundActivityChangedLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/AppContinuityRouterStub;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V
/* .line 449 */
} // :cond_4
return;
/* .line 423 */
} // :cond_5
} // :goto_0
return;
} // .end method
public void onFreeFormToFullScreen ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 4 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 381 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.app;
/* if-nez v0, :cond_0 */
/* .line 385 */
} // :cond_0
(( com.android.server.wm.ActivityRecord ) p1 ).getState ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getState()Lcom/android/server/wm/ActivityRecord$State;
/* .line 386 */
/* .local v0, "state":Lcom/android/server/wm/ActivityRecord$State; */
v1 = this.app;
v1 = (( com.android.server.wm.WindowProcessController ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* .line 387 */
/* .local v1, "pid":I */
com.android.server.MiuiFgThread .getHandler ( );
/* new-instance v3, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$1; */
/* invoke-direct {v3, p0, p1, v0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$1;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord$State;I)V */
(( android.os.Handler ) v2 ).post ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 393 */
return;
/* .line 382 */
} // .end local v0 # "state":Lcom/android/server/wm/ActivityRecord$State;
} // .end local v1 # "pid":I
} // :cond_1
} // :goto_0
return;
} // .end method
public void onFreeFormToFullScreen ( com.android.server.wm.Task p0 ) {
/* .locals 2 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 637 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onFreeFormToFullScreen task: "; // const-string v1, "onFreeFormToFullScreen task: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ATMSImpl"; // const-string v1, "ATMSImpl"
android.util.Slog .i ( v1,v0 );
/* .line 638 */
(( com.android.server.wm.Task ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTask()Lcom/android/server/wm/Task;
/* .line 639 */
/* .local v0, "rootTask":Lcom/android/server/wm/Task; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 640 */
(( com.android.server.wm.Task ) v0 ).topRunningActivityLocked ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).onFreeFormToFullScreen ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onFreeFormToFullScreen(Lcom/android/server/wm/ActivityRecord;)V
/* .line 642 */
} // :cond_0
return;
} // .end method
public void onLastResumedActivityChange ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1757 */
com.android.server.wm.WindowManagerServiceStub .get ( );
/* .line 1758 */
v0 = v0 = this.mActivityRecordStub;
/* if-nez v0, :cond_0 */
/* .line 1759 */
com.android.server.wm.WindowManagerServiceStub .get ( );
v1 = this.mActivityRecordStub;
v1 = /* .line 1760 */
/* .line 1759 */
/* .line 1762 */
} // :cond_0
return;
} // .end method
public void onMetaKeyCombination ( ) {
/* .locals 6 */
/* .line 1184 */
v0 = this.mAtmService;
/* if-nez v0, :cond_0 */
return;
/* .line 1185 */
} // :cond_0
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 1186 */
try { // :try_start_0
v1 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 1187 */
/* .local v1, "topFocusedStack":Lcom/android/server/wm/Task; */
/* if-nez v1, :cond_1 */
/* .line 1188 */
final String v2 = "ATMSImpl"; // const-string v2, "ATMSImpl"
final String v3 = "no stack focued, do noting."; // const-string v3, "no stack focued, do noting."
android.util.Slog .d ( v2,v3 );
/* .line 1189 */
/* monitor-exit v0 */
return;
/* .line 1192 */
} // :cond_1
v2 = (( com.android.server.wm.Task ) v1 ).getWindowingMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->getWindowingMode()I
/* .line 1193 */
/* .local v2, "windowingMode":I */
/* sparse-switch v2, :sswitch_data_0 */
/* .line 1208 */
/* monitor-exit v0 */
/* .line 1204 */
/* :sswitch_0 */
v3 = this.mAtmService;
v3 = this.mMiuiFreeFormManagerService;
/* check-cast v3, Lcom/android/server/wm/MiuiFreeFormManagerService; */
/* .line 1205 */
/* .local v3, "miuiFreeFormManagerService":Lcom/android/server/wm/MiuiFreeFormManagerService; */
v4 = this.mFreeFormGestureController;
(( com.android.server.wm.MiuiFreeFormGestureController ) v4 ).exitFreeFormByKeyCombination ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/server/wm/MiuiFreeFormGestureController;->exitFreeFormByKeyCombination(Lcom/android/server/wm/Task;)V
/* .line 1206 */
/* .line 1195 */
} // .end local v3 # "miuiFreeFormManagerService":Lcom/android/server/wm/MiuiFreeFormManagerService;
/* :sswitch_1 */
v3 = /* invoke-direct {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->exitSplitScreenIfNeed(Lcom/android/server/wm/WindowContainer;)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1197 */
} // :cond_2
com.android.server.wm.MiuiSoScManagerStub .get ( );
final String v4 = "onMetaKeyCombination"; // const-string v4, "onMetaKeyCombination"
int v5 = 0; // const/4 v5, 0x0
/* new-array v5, v5, [Ljava/lang/Object; */
com.android.server.wm.ActivityTaskManagerServiceImpl .invoke ( v3,v4,v5 );
/* .line 1198 */
/* .local v3, "result":Ljava/lang/Object; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* move-object v4, v3 */
/* check-cast v4, Ljava/lang/Boolean; */
v4 = (( java.lang.Boolean ) v4 ).booleanValue ( ); // invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1199 */
} // :cond_3
v4 = (( com.android.server.wm.Task ) v1 ).isActivityTypeHomeOrRecents ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z
/* if-nez v4, :cond_4 */
/* .line 1200 */
(( com.android.server.wm.Task ) v1 ).moveTaskToBack ( v1 ); // invoke-virtual {v1, v1}, Lcom/android/server/wm/Task;->moveTaskToBack(Lcom/android/server/wm/Task;)Z
/* .line 1210 */
} // .end local v1 # "topFocusedStack":Lcom/android/server/wm/Task;
} // .end local v2 # "windowingMode":I
} // .end local v3 # "result":Ljava/lang/Object;
} // :cond_4
} // :goto_0
/* monitor-exit v0 */
/* .line 1211 */
return;
/* .line 1208 */
/* .restart local v1 # "topFocusedStack":Lcom/android/server/wm/Task; */
/* .restart local v2 # "windowingMode":I */
} // :goto_1
return;
/* .line 1210 */
} // .end local v1 # "topFocusedStack":Lcom/android/server/wm/Task;
} // .end local v2 # "windowingMode":I
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_1 */
/* 0x5 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
void onSystemReady ( ) {
/* .locals 2 */
/* .line 245 */
/* const-class v0, Lcom/android/server/wm/MiuiSizeCompatInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/MiuiSizeCompatInternal; */
this.mMiuiSizeCompatIn = v0;
/* .line 246 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 247 */
v1 = this.mAtmService;
(( com.android.server.wm.MiuiSizeCompatInternal ) v0 ).onSystemReady ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiSizeCompatInternal;->onSystemReady(Lcom/android/server/wm/ActivityTaskManagerService;)V
/* .line 252 */
} // :cond_0
com.android.server.wm.MiuiOrientationImpl .getInstance ( );
(( com.android.server.wm.MiuiOrientationImpl ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getPackageManager()Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;
this.mFullScreenPackageManager = v0;
/* .line 253 */
com.android.server.wm.MiuiOrientationImpl .getInstance ( );
(( com.android.server.wm.MiuiOrientationImpl ) v0 ).onSystemReady ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->onSystemReady()V
/* .line 256 */
/* new-instance v0, Lcom/android/server/wm/PackageSettingsManager; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/PackageSettingsManager;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V */
this.mPackageSettingsManager = v0;
/* .line 257 */
/* new-instance v0, Lcom/android/server/wm/PackageConfigurationController; */
v1 = this.mAtmService;
/* invoke-direct {v0, v1}, Lcom/android/server/wm/PackageConfigurationController;-><init>(Lcom/android/server/wm/ActivityTaskManagerService;)V */
this.mPackageConfigurationController = v0;
/* .line 258 */
/* new-instance v0, Lcom/android/server/wm/FoldablePackagePolicy; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/FoldablePackagePolicy;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V */
this.mFoldablePackagePolicy = v0;
/* .line 259 */
v1 = this.mPackageConfigurationController;
(( com.android.server.wm.PackageConfigurationController ) v1 ).registerPolicy ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/PackageConfigurationController;->registerPolicy(Lcom/android/server/wm/PolicyImpl;)V
/* .line 260 */
v0 = this.mPackageConfigurationController;
(( com.android.server.wm.PackageConfigurationController ) v0 ).startThread ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PackageConfigurationController;->startThread()V
/* .line 261 */
final String v0 = "package"; // const-string v0, "package"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl; */
this.mPackageManager = v0;
/* .line 267 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
this.mProcessManagerIn = v0;
/* .line 268 */
com.android.server.wm.ActivityStarterImpl .getInstance ( );
(( com.android.server.wm.ActivityStarterImpl ) v0 ).onSystemReady ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityStarterImpl;->onSystemReady()V
/* .line 269 */
v0 = this.mAtmService;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V */
(( com.android.server.wm.ActivityTaskManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 274 */
com.android.server.wm.PassivePenAppWhiteListImpl .getInstance ( );
(( com.android.server.wm.PassivePenAppWhiteListImpl ) v0 ).onSystemReady ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->onSystemReady()V
/* .line 275 */
return;
} // .end method
void registerSubScreenSwitchObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 985 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_0 */
/* .line 987 */
} // :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 988 */
/* const-string/jumbo v1, "subscreen_switch" */
android.provider.Settings$System .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$6; */
/* .line 989 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {v2, p0, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$6;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/os/Handler;)V */
/* .line 987 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 996 */
return;
/* .line 986 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void removeFlipActivityFullScreen ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "removeFlipActivityFullScreen" # Ljava/lang/String; */
/* .line 1891 */
v0 = com.android.server.wm.ActivityTaskManagerServiceImpl.BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO;
v1 = (( android.util.ArraySet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1892 */
(( android.util.ArraySet ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
/* .line 1894 */
} // :cond_0
return;
} // .end method
public Boolean removeSplitTaskShotIfNeed ( ) {
/* .locals 2 */
/* .line 1418 */
v0 = this.mScreenshotLayer;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mInAnimationOut:Z */
/* if-nez v0, :cond_0 */
/* .line 1419 */
v0 = this.mAtmService;
v0 = this.mUiHandler;
/* new-instance v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V */
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->post(Ljava/lang/Runnable;)Z
/* .line 1420 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1422 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void removeTransitionSyncId ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "syncId" # I */
/* .line 1751 */
v0 = this.mTransitionSyncIdList;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashSet ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 1752 */
return;
} // .end method
public void restartSubScreenUiIfNeeded ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "userId" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 901 */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
final String v1 = "com.xiaomi.misubscreenui"; // const-string v1, "com.xiaomi.misubscreenui"
/* .line 902 */
/* const-wide/16 v2, 0x400 */
/* .line 903 */
/* .local v0, "aInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v0, :cond_0 */
/* .line 904 */
final String v1 = "ATMSImpl"; // const-string v1, "ATMSImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "aInfo is null when start subscreenui for user "; // const-string v3, "aInfo is null when start subscreenui for user "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 905 */
return;
/* .line 907 */
} // :cond_0
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).restartSubScreenUiIfNeeded ( v0, p2 ); // invoke-virtual {p0, v0, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 910 */
} // .end local v0 # "aInfo":Landroid/content/pm/ApplicationInfo;
/* .line 908 */
/* :catch_0 */
/* move-exception v0 */
/* .line 909 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 911 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void restartSubScreenUiIfNeeded ( android.content.pm.ApplicationInfo p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 914 */
if ( p1 != null) { // if-eqz p1, :cond_1
final String v0 = "com.xiaomi.misubscreenui"; // const-string v0, "com.xiaomi.misubscreenui"
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 915 */
v0 = /* invoke-direct {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isCTS()Z */
/* if-nez v0, :cond_1 */
v0 = this.mAtmService;
/* if-nez v0, :cond_0 */
/* .line 917 */
} // :cond_0
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v1, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$5;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)V */
/* const-wide/16 v2, 0x3e8 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 927 */
return;
/* .line 916 */
} // :cond_1
} // :goto_0
return;
} // .end method
public android.app.ActivityTaskManager$RootTaskInfo retrieveFirstSplitWindowRootTaskInfo ( ) {
/* .locals 7 */
/* .line 1273 */
v0 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getAllRootTaskInfos ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getAllRootTaskInfos()Ljava/util/List;
/* .line 1274 */
/* .local v0, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Landroid/app/ActivityTaskManager$RootTaskInfo; */
/* .line 1275 */
/* .local v2, "taskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo; */
v3 = (( android.app.ActivityTaskManager$RootTaskInfo ) v2 ).getWindowingMode ( ); // invoke-virtual {v2}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
/* .line 1276 */
/* .local v3, "windowMode":I */
/* iget-boolean v4, v2, Landroid/app/ActivityTaskManager$RootTaskInfo;->visible:Z */
/* .line 1277 */
/* .local v4, "visible":Z */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "retrieveFirstSplitWindowRootTaskInfo: windowMode = "; // const-string v6, "retrieveFirstSplitWindowRootTaskInfo: windowMode = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " visible = "; // const-string v6, " visible = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "ATMSImpl"; // const-string v6, "ATMSImpl"
android.util.Slog .d ( v6,v5 );
/* .line 1278 */
if ( v4 != null) { // if-eqz v4, :cond_0
int v5 = 1; // const/4 v5, 0x1
/* if-ne v3, v5, :cond_0 */
/* .line 1279 */
/* .line 1281 */
} // .end local v2 # "taskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v3 # "windowMode":I
} // .end local v4 # "visible":Z
} // :cond_0
/* .line 1282 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void setActivityController ( android.app.IActivityController p0 ) {
/* .locals 2 */
/* .param p1, "control" # Landroid/app/IActivityController; */
/* .line 1683 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setActivityController callingUid:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " control="; // const-string v1, " control="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
if ( p1 != null) { // if-eqz p1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ATMSImpl"; // const-string v1, "ATMSImpl"
android.util.Slog .d ( v1,v0 );
/* .line 1684 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 1685 */
v0 = android.os.Binder .getCallingUid ( );
/* iput v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mActivityControlUid:I */
/* .line 1687 */
} // :cond_1
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->mActivityControlUid:I */
/* .line 1688 */
} // :goto_1
return;
} // .end method
public void setPackageHoldOn ( com.android.server.wm.ActivityTaskManagerService p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "atms" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 615 */
v0 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v0, :cond_2 */
/* .line 616 */
v0 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getAllRootTaskInfos ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getAllRootTaskInfos()Ljava/util/List;
/* .line 617 */
/* .local v0, "stackList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Landroid/app/ActivityTaskManager$RootTaskInfo; */
/* .line 618 */
/* .local v2, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
v3 = this.topActivity;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.topActivity;
(( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 619 */
v3 = this.mRootWindowContainer;
/* iget v4, v2, Landroid/app/ActivityTaskManager$RootTaskInfo;->taskId:I */
(( com.android.server.wm.RootWindowContainer ) v3 ).getRootTask ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/RootWindowContainer;->getRootTask(I)Lcom/android/server/wm/Task;
/* .line 620 */
/* .local v3, "stack":Lcom/android/server/wm/Task; */
if ( v3 != null) { // if-eqz v3, :cond_0
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
(( com.android.server.wm.Task ) v3 ).getTopActivity ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 621 */
this.mPackageHoldOn = p2;
/* .line 622 */
/* const-class v1, Lcom/android/server/wm/WindowManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/wm/WindowManagerInternal; */
/* .line 623 */
/* .local v1, "wm":Lcom/android/server/wm/WindowManagerInternal; */
(( com.android.server.wm.Task ) v3 ).getTopActivity ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
v4 = this.token;
(( com.android.server.wm.WindowManagerInternal ) v1 ).setHoldOn ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Lcom/android/server/wm/WindowManagerInternal;->setHoldOn(Landroid/os/IBinder;Z)V
/* .line 624 */
v4 = this.mContext;
final String v5 = "power"; // const-string v5, "power"
(( android.content.Context ) v4 ).getSystemService ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v4, Landroid/os/PowerManager; */
/* .line 625 */
/* .local v4, "pm":Landroid/os/PowerManager; */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v5 */
(( android.os.PowerManager ) v4 ).goToSleep ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/os/PowerManager;->goToSleep(J)V
/* .line 626 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Going to sleep and hold on, package name in hold on: "; // const-string v6, "Going to sleep and hold on, package name in hold on: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mPackageHoldOn;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "ATMSImpl"; // const-string v6, "ATMSImpl"
android.util.Slog .i ( v6,v5 );
/* .line 627 */
/* .line 630 */
} // .end local v1 # "wm":Lcom/android/server/wm/WindowManagerInternal;
} // .end local v2 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v3 # "stack":Lcom/android/server/wm/Task;
} // .end local v4 # "pm":Landroid/os/PowerManager;
} // :cond_0
/* .line 631 */
} // .end local v0 # "stackList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
} // :cond_1
} // :goto_1
/* .line 632 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
this.mPackageHoldOn = v0;
/* .line 634 */
} // :goto_2
return;
} // .end method
public void setPauseAdvancedForTask ( Integer[] p0, Boolean p1 ) {
/* .locals 10 */
/* .param p1, "taskIds" # [I */
/* .param p2, "userLeaving" # Z */
/* .line 1812 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
/* move v2, v1 */
} // :goto_0
/* if-ge v2, v0, :cond_6 */
/* aget v3, p1, v2 */
/* .line 1813 */
/* .local v3, "taskId":I */
int v4 = -1; // const/4 v4, -0x1
/* if-le v3, v4, :cond_5 */
/* .line 1814 */
v4 = this.mAtmService;
v4 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v4 ).anyTaskForId ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(I)Lcom/android/server/wm/Task;
/* .line 1815 */
/* .local v4, "task":Lcom/android/server/wm/Task; */
/* if-nez v4, :cond_0 */
/* .line 1816 */
/* goto/16 :goto_2 */
/* .line 1818 */
} // :cond_0
(( com.android.server.wm.Task ) v4 ).getTopNonFinishingActivity ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 1819 */
/* .local v5, "topNonFinishingActivity":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v5, :cond_1 */
/* .line 1820 */
/* goto/16 :goto_2 */
/* .line 1822 */
} // :cond_1
(( com.android.server.wm.ActivityRecord ) v5 ).getTaskFragment ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityRecord;->getTaskFragment()Lcom/android/server/wm/TaskFragment;
/* .line 1823 */
/* .local v6, "taskFragment":Lcom/android/server/wm/TaskFragment; */
final String v7 = "ATMSImpl"; // const-string v7, "ATMSImpl"
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 1824 */
v8 = this.mAdvanceTaskIds;
java.lang.Integer .valueOf ( v3 );
/* .line 1825 */
int v8 = 1; // const/4 v8, 0x1
/* iput-boolean v8, v6, Lcom/android/server/wm/TaskFragment;->mPauseAdvance:Z */
/* .line 1826 */
v9 = this.mTransitionController;
v9 = (( com.android.server.wm.TransitionController ) v9 ).isTransientHide ( v4 ); // invoke-virtual {v9, v4}, Lcom/android/server/wm/TransitionController;->isTransientHide(Lcom/android/server/wm/Task;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
/* move v9, v8 */
} // :cond_2
/* move v9, v1 */
} // :goto_1
/* move p2, v9 */
/* .line 1828 */
v9 = /* invoke-direct {p0, v5, v4, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->canEnterPipOnTaskSwitch(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/Task;Z)Z */
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 1829 */
/* iput-boolean v8, v5, Lcom/android/server/wm/ActivityRecord;->supportsEnterPipOnTaskSwitch:Z */
/* .line 1831 */
} // :cond_3
int v8 = 0; // const/4 v8, 0x0
/* const-string/jumbo v9, "setPauseAdvanced" */
(( com.android.server.wm.TaskFragment ) v6 ).startPausing ( p2, v1, v8, v9 ); // invoke-virtual {v6, p2, v1, v8, v9}, Lcom/android/server/wm/TaskFragment;->startPausing(ZZLcom/android/server/wm/ActivityRecord;Ljava/lang/String;)Z
/* .line 1833 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "setPauseAdvanced: userLeaving= " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( p2 ); // invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v9 = ",Task="; // const-string v9, ",Task="
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v9 = ",resume="; // const-string v9, ",resume="
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.shortComponentName;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v8 );
/* .line 1836 */
} // :cond_4
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "setPauseAdvance: Task " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " not found."; // const-string v9, " not found."
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v8 );
/* .line 1812 */
} // .end local v3 # "taskId":I
} // .end local v4 # "task":Lcom/android/server/wm/Task;
} // .end local v5 # "topNonFinishingActivity":Lcom/android/server/wm/ActivityRecord;
} // .end local v6 # "taskFragment":Lcom/android/server/wm/TaskFragment;
} // :cond_5
} // :goto_2
/* add-int/lit8 v2, v2, 0x1 */
/* goto/16 :goto_0 */
/* .line 1840 */
} // :cond_6
return;
} // .end method
public Boolean setSplitScreenDirection ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "direction" # I */
/* .line 1142 */
com.android.server.wm.MiuiSoScManagerStub .get ( );
v0 = (( com.android.server.wm.MiuiSoScManagerStub ) v0 ).isSoScSupported ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScSupported()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1143 */
com.android.server.wm.MiuiSoScManagerStub .get ( );
v0 = (( com.android.server.wm.MiuiSoScManagerStub ) v0 ).setSplitScreenDirection ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiSoScManagerStub;->setSplitScreenDirection(I)Z
/* .line 1146 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
/* .line 1148 */
/* .local v0, "str":Ljava/lang/String; */
/* if-nez p1, :cond_1 */
/* .line 1149 */
/* const-string/jumbo v0, "snap_to_left" */
/* .line 1152 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_2 */
/* .line 1153 */
/* const-string/jumbo v0, "snap_to_right" */
/* .line 1155 */
} // :cond_2
v2 = android.text.TextUtils .isEmpty ( v0 );
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1157 */
} // :cond_3
v2 = this.mAtmService;
/* if-nez v2, :cond_4 */
/* .line 1159 */
} // :cond_4
(( com.android.server.wm.ActivityTaskManagerService ) v2 ).getTopDisplayFocusedRootTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getTopDisplayFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 1160 */
/* .local v2, "topFocusedStack":Lcom/android/server/wm/Task; */
/* if-nez v2, :cond_5 */
/* .line 1161 */
final String v1 = "ATMSImpl"; // const-string v1, "ATMSImpl"
final String v4 = "no stack focued, do noting."; // const-string v4, "no stack focued, do noting."
android.util.Slog .d ( v1,v4 );
/* .line 1162 */
/* .line 1165 */
} // :cond_5
v3 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).isInSystemSplitScreen ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 1166 */
v3 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v3 ).getUiContext ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerService;->getUiContext()Landroid/content/Context;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v4, "update_split_snap_target" */
android.provider.MiuiSettings$System .putString ( v3,v4,v0 );
/* .line 1169 */
} // :cond_6
int v3 = 1; // const/4 v3, 0x1
/* .line 1171 */
/* .local v3, "isLTR":Z */
/* if-nez p1, :cond_7 */
/* .line 1172 */
int v3 = 0; // const/4 v3, 0x0
/* .line 1175 */
} // :cond_7
/* if-ne p1, v1, :cond_8 */
/* .line 1176 */
int v3 = 1; // const/4 v3, 0x1
/* .line 1178 */
} // :cond_8
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).toggleSplitByGesture ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->toggleSplitByGesture(Z)Z
/* .line 1180 */
} // .end local v3 # "isLTR":Z
} // :goto_0
} // .end method
public Boolean shouldExcludeTaskFromRecents ( com.android.server.wm.Task p0 ) {
/* .locals 4 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 1028 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
(( com.android.server.wm.Task ) p1 ).getBaseIntent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1029 */
(( com.android.server.wm.Task ) p1 ).getBaseIntent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v1 != null) { // if-eqz v1, :cond_2
/* sget-boolean v1, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
/* if-nez v1, :cond_0 */
/* .line 1031 */
} // :cond_0
(( com.android.server.wm.Task ) p1 ).getBaseIntent ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
(( android.content.Intent ) v1 ).getComponent ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1032 */
/* .local v1, "packageName":Ljava/lang/String; */
final String v2 = "com.xiaomi.misubscreenui"; // const-string v2, "com.xiaomi.misubscreenui"
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 2; // const/4 v2, 0x2
v3 = (( com.android.server.wm.Task ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getDisplayId()I
/* if-ne v2, v3, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* .line 1030 */
} // .end local v1 # "packageName":Ljava/lang/String;
} // :cond_2
} // :goto_0
} // .end method
public Boolean shouldNotApplyAspectRatio ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 4 */
/* .param p1, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1901 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 1902 */
/* .line 1905 */
} // :cond_0
v1 = this.mActivityComponent;
/* .line 1906 */
(( android.content.ComponentName ) v1 ).toShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;
final String v2 = "[{}]"; // const-string v2, "[{}]"
final String v3 = ""; // const-string v3, ""
(( java.lang.String ) v1 ).replaceAll ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 1905 */
v1 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).shouldNotApplyAspectRatio ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->shouldNotApplyAspectRatio(Ljava/lang/String;)Z
/* if-nez v1, :cond_1 */
v1 = this.packageName;
/* .line 1907 */
v1 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).shouldNotApplyAspectRatio ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->shouldNotApplyAspectRatio(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 1905 */
} // :cond_2
} // .end method
public Boolean shouldNotApplyAspectRatio ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 1911 */
/* if-nez p1, :cond_0 */
/* .line 1912 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1914 */
} // :cond_0
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1915 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " shouldNotApplyAspectRatio = "; // const-string v1, " shouldNotApplyAspectRatio = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " ? "; // const-string v1, " ? "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.wm.ActivityTaskManagerServiceImpl.BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO;
/* .line 1916 */
v1 = (( android.util.ArraySet ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1915 */
final String v1 = "ATMSImpl"; // const-string v1, "ATMSImpl"
android.util.Slog .d ( v1,v0 );
/* .line 1918 */
} // :cond_1
v0 = com.android.server.wm.ActivityTaskManagerServiceImpl.BLACK_LIST_SHOULD_NOT_APPLY_ASPECT_RATIO;
v0 = (( android.util.ArraySet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
} // .end method
public void showScreenShotForSplitTask ( ) {
/* .locals 11 */
/* .line 1470 */
v0 = this.mScreenshotLayer;
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 1472 */
} // :cond_0
v0 = this.mAtmService;
v0 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v0 ).getDefaultDisplay ( ); // invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;
/* .line 1473 */
/* .local v0, "dc":Lcom/android/server/wm/DisplayContent; */
(( com.android.server.wm.DisplayContent ) v0 ).getDisplayInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;
/* .line 1474 */
/* .local v1, "info":Landroid/view/DisplayInfo; */
/* new-instance v2, Landroid/graphics/Rect; */
/* iget v3, v1, Landroid/view/DisplayInfo;->logicalWidth:I */
/* iget v4, v1, Landroid/view/DisplayInfo;->logicalHeight:I */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V */
/* .line 1476 */
/* .local v2, "bounds":Landroid/graphics/Rect; */
/* new-instance v3, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder; */
/* .line 1477 */
(( com.android.server.wm.DisplayContent ) v0 ).getSurfaceControl ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getSurfaceControl()Landroid/view/SurfaceControl;
/* invoke-direct {v3, v4}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;-><init>(Landroid/view/SurfaceControl;)V */
/* .line 1478 */
int v4 = 1; // const/4 v4, 0x1
(( android.window.ScreenCapture$LayerCaptureArgs$Builder ) v3 ).setCaptureSecureLayers ( v4 ); // invoke-virtual {v3, v4}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;->setCaptureSecureLayers(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;
/* check-cast v3, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder; */
/* .line 1479 */
(( android.window.ScreenCapture$LayerCaptureArgs$Builder ) v3 ).setAllowProtected ( v4 ); // invoke-virtual {v3, v4}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;->setAllowProtected(Z)Landroid/window/ScreenCapture$CaptureArgs$Builder;
/* check-cast v3, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder; */
/* .line 1480 */
(( android.window.ScreenCapture$LayerCaptureArgs$Builder ) v3 ).setSourceCrop ( v2 ); // invoke-virtual {v3, v2}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;->setSourceCrop(Landroid/graphics/Rect;)Landroid/window/ScreenCapture$CaptureArgs$Builder;
/* check-cast v3, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder; */
/* .line 1481 */
(( android.window.ScreenCapture$LayerCaptureArgs$Builder ) v3 ).build ( ); // invoke-virtual {v3}, Landroid/window/ScreenCapture$LayerCaptureArgs$Builder;->build()Landroid/window/ScreenCapture$LayerCaptureArgs;
/* .line 1482 */
/* .local v3, "displayCaptureArgs":Landroid/window/ScreenCapture$LayerCaptureArgs; */
/* nop */
/* .line 1483 */
android.window.ScreenCapture .captureLayers ( v3 );
/* .line 1484 */
/* .local v6, "screenshotHardwareBuffer":Landroid/window/ScreenCapture$ScreenshotHardwareBuffer; */
/* if-nez v6, :cond_1 */
return;
/* .line 1485 */
} // :cond_1
/* nop */
/* .line 1486 */
(( android.window.ScreenCapture$ScreenshotHardwareBuffer ) v6 ).getHardwareBuffer ( ); // invoke-virtual {v6}, Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;->getHardwareBuffer()Landroid/hardware/HardwareBuffer;
/* .line 1485 */
android.graphics.GraphicBuffer .createFromHardwareBuffer ( v7 );
/* .line 1488 */
/* .local v7, "buffer":Landroid/graphics/GraphicBuffer; */
/* new-instance v8, Landroid/view/SurfaceControl$Transaction; */
/* invoke-direct {v8}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
/* .line 1489 */
/* .local v8, "t":Landroid/view/SurfaceControl$Transaction; */
(( com.android.server.wm.DisplayContent ) v0 ).makeOverlay ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;
/* .line 1490 */
final String v10 = "SplitTaskScreenShot"; // const-string v10, "SplitTaskScreenShot"
(( android.view.SurfaceControl$Builder ) v9 ).setName ( v10 ); // invoke-virtual {v9, v10}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 1491 */
(( android.view.SurfaceControl$Builder ) v9 ).setOpaque ( v4 ); // invoke-virtual {v9, v4}, Landroid/view/SurfaceControl$Builder;->setOpaque(Z)Landroid/view/SurfaceControl$Builder;
/* .line 1492 */
(( android.view.SurfaceControl$Builder ) v4 ).setSecure ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setSecure(Z)Landroid/view/SurfaceControl$Builder;
/* .line 1493 */
(( android.view.SurfaceControl$Builder ) v4 ).setCallsite ( v10 ); // invoke-virtual {v4, v10}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
/* .line 1494 */
(( android.view.SurfaceControl$Builder ) v4 ).setBLASTLayer ( ); // invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;
/* .line 1495 */
(( android.view.SurfaceControl$Builder ) v4 ).build ( ); // invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
this.mScreenshotLayer = v4;
/* .line 1496 */
/* const v5, 0x1e8480 */
(( android.view.SurfaceControl$Transaction ) v8 ).setLayer ( v4, v5 ); // invoke-virtual {v8, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
/* .line 1497 */
v4 = this.mScreenshotLayer;
(( android.view.SurfaceControl$Transaction ) v8 ).setBuffer ( v4, v7 ); // invoke-virtual {v8, v4, v7}, Landroid/view/SurfaceControl$Transaction;->setBuffer(Landroid/view/SurfaceControl;Landroid/graphics/GraphicBuffer;)Landroid/view/SurfaceControl$Transaction;
/* .line 1498 */
v4 = this.mScreenshotLayer;
(( android.window.ScreenCapture$ScreenshotHardwareBuffer ) v6 ).getColorSpace ( ); // invoke-virtual {v6}, Landroid/window/ScreenCapture$ScreenshotHardwareBuffer;->getColorSpace()Landroid/graphics/ColorSpace;
(( android.view.SurfaceControl$Transaction ) v8 ).setColorSpace ( v4, v5 ); // invoke-virtual {v8, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setColorSpace(Landroid/view/SurfaceControl;Landroid/graphics/ColorSpace;)Landroid/view/SurfaceControl$Transaction;
/* .line 1499 */
v4 = this.mScreenshotLayer;
(( android.view.SurfaceControl$Transaction ) v8 ).show ( v4 ); // invoke-virtual {v8, v4}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 1500 */
(( android.view.SurfaceControl$Transaction ) v8 ).apply ( ); // invoke-virtual {v8}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 1501 */
v4 = this.mAtmService;
v4 = this.mUiHandler;
/* new-instance v5, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v5, p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V */
/* const-wide/16 v9, 0x3e8 */
(( com.android.server.wm.ActivityTaskManagerService$UiHandler ) v4 ).postDelayed ( v5, v9, v10 ); // invoke-virtual {v4, v5, v9, v10}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 1502 */
final String v4 = "ATMSImpl"; // const-string v4, "ATMSImpl"
/* const-string/jumbo v5, "show split task screen shot layer." */
android.util.Slog .i ( v4,v5 );
/* .line 1503 */
return;
} // .end method
public Boolean skipTaskForMultiWindow ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1735 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1736 */
/* .line 1739 */
} // :cond_0
final String v0 = "com.android.quicksearchbox"; // const-string v0, "com.android.quicksearchbox"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1740 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1742 */
} // :cond_1
} // .end method
public void splitTaskIfNeed ( android.app.ActivityOptions p0, com.android.server.wm.Task p1 ) {
/* .locals 9 */
/* .param p1, "options" # Landroid/app/ActivityOptions; */
/* .param p2, "startedActivityRootTask" # Lcom/android/server/wm/Task; */
/* .line 1362 */
if ( p2 != null) { // if-eqz p2, :cond_8
if ( p1 != null) { // if-eqz p1, :cond_8
v0 = this.mAtmService;
/* if-nez v0, :cond_0 */
/* goto/16 :goto_1 */
/* .line 1366 */
} // :cond_0
final String v0 = "getEnterAppPair"; // const-string v0, "getEnterAppPair"
int v1 = 0; // const/4 v1, 0x0
/* new-array v2, v1, [Ljava/lang/Object; */
com.android.server.wm.ActivityTaskManagerServiceImpl .invoke ( p1,v0,v2 );
/* .line 1367 */
/* .local v0, "enterApppair":Ljava/lang/Object; */
final String v2 = "getAppPairPrimary"; // const-string v2, "getAppPairPrimary"
/* new-array v1, v1, [Ljava/lang/Object; */
com.android.server.wm.ActivityTaskManagerServiceImpl .invoke ( p1,v2,v1 );
/* .line 1368 */
/* .local v1, "isPrimary":Ljava/lang/Object; */
if ( v0 != null) { // if-eqz v0, :cond_7
/* move-object v2, v0 */
/* check-cast v2, Ljava/lang/Boolean; */
v2 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 1369 */
(( com.android.server.wm.Task ) p2 ).getDisplayArea ( ); // invoke-virtual {p2}, Lcom/android/server/wm/Task;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 1370 */
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.wm.TaskDisplayArea ) v2 ).getTopRootTaskInWindowingMode ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;
/* .line 1371 */
/* .local v2, "task1":Lcom/android/server/wm/Task; */
/* move-object v4, p2 */
/* .line 1372 */
/* .local v4, "task2":Lcom/android/server/wm/Task; */
final String v5 = "The top app or drag\'s app is not supports multi window."; // const-string v5, "The top app or drag\'s app is not supports multi window."
final String v6 = "ATMSImpl"; // const-string v6, "ATMSImpl"
if ( v2 != null) { // if-eqz v2, :cond_1
v7 = (( com.android.server.wm.Task ) v2 ).supportsMultiWindow ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->supportsMultiWindow()Z
if ( v7 != null) { // if-eqz v7, :cond_1
v7 = (( com.android.server.wm.Task ) v4 ).supportsMultiWindow ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->supportsMultiWindow()Z
/* if-nez v7, :cond_2 */
/* .line 1373 */
} // :cond_1
(( com.android.server.wm.Task ) v2 ).getBaseIntent ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
if ( v7 != null) { // if-eqz v7, :cond_6
(( com.android.server.wm.Task ) v2 ).getBaseIntent ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
(( android.content.Intent ) v7 ).getComponent ( ); // invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 1374 */
(( com.android.server.wm.Task ) v2 ).getBaseIntent ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getBaseIntent()Landroid/content/Intent;
(( android.content.Intent ) v7 ).getComponent ( ); // invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v7 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).skipTaskForMultiWindow ( v7 ); // invoke-virtual {p0, v7}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->skipTaskForMultiWindow(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 1380 */
} // :cond_2
/* if-ne v2, v4, :cond_3 */
/* .line 1381 */
final String v3 = "The task has been front."; // const-string v3, "The task has been front."
android.util.Slog .w ( v6,v3 );
/* .line 1382 */
return;
/* .line 1385 */
} // :cond_3
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "pair t1:"; // const-string v8, "pair t1:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = " t2:"; // const-string v8, " t2:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = " isPrimary:"; // const-string v8, " isPrimary:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v7 );
/* .line 1386 */
com.android.server.wm.MiuiSoScManagerStub .get ( );
v7 = (( com.android.server.wm.MiuiSoScManagerStub ) v7 ).isSoScSupported ( ); // invoke-virtual {v7}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScSupported()Z
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 1387 */
/* move-object v5, v1 */
/* check-cast v5, Ljava/lang/Boolean; */
v5 = (( java.lang.Boolean ) v5 ).booleanValue ( ); // invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
/* xor-int/2addr v3, v5 */
/* invoke-direct {p0, p2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->startSoSc(Lcom/android/server/wm/Task;I)V */
/* .line 1388 */
return;
/* .line 1390 */
} // :cond_4
v7 = (( com.android.server.wm.Task ) v2 ).isActivityTypeHomeOrRecents ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 1391 */
android.util.Slog .w ( v6,v5 );
/* .line 1392 */
return;
/* .line 1395 */
} // :cond_5
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).showScreenShotForSplitTask ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->showScreenShotForSplitTask()V
/* .line 1396 */
(( com.android.server.wm.Task ) v4 ).setHasBeenVisible ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/wm/Task;->setHasBeenVisible(Z)V
/* .line 1397 */
(( com.android.server.wm.Task ) v4 ).sendTaskAppeared ( ); // invoke-virtual {v4}, Lcom/android/server/wm/Task;->sendTaskAppeared()V
/* .line 1398 */
v3 = this.mAtmService;
v3 = this.mTaskOrganizerController;
(( com.android.server.wm.TaskOrganizerController ) v3 ).dispatchPendingEvents ( ); // invoke-virtual {v3}, Lcom/android/server/wm/TaskOrganizerController;->dispatchPendingEvents()V
/* .line 1399 */
v3 = this.mAtmService;
/* iget v5, v4, Lcom/android/server/wm/Task;->mTaskId:I */
java.lang.Integer .valueOf ( v5 );
/* move-object v6, v1 */
/* check-cast v6, Ljava/lang/Boolean; */
/* filled-new-array {v5, v6}, [Ljava/lang/Object; */
final String v6 = "enterSplitScreen"; // const-string v6, "enterSplitScreen"
com.android.server.wm.ActivityTaskManagerServiceImpl .invoke ( v3,v6,v5 );
/* .line 1376 */
} // :cond_6
android.util.Slog .w ( v6,v5 );
/* .line 1377 */
return;
/* .line 1401 */
} // .end local v2 # "task1":Lcom/android/server/wm/Task;
} // .end local v4 # "task2":Lcom/android/server/wm/Task;
} // :cond_7
} // :goto_0
return;
/* .line 1363 */
} // .end local v0 # "enterApppair":Ljava/lang/Object;
} // .end local v1 # "isPrimary":Ljava/lang/Object;
} // :cond_8
} // :goto_1
return;
} // .end method
public Boolean toggleSplitByGesture ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "isLTR" # Z */
/* .line 1567 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1569 */
/* .local v0, "triggered":Z */
com.android.server.wm.MiuiSoScManagerStub .get ( );
v1 = (( com.android.server.wm.MiuiSoScManagerStub ) v1 ).isSoScSupported ( ); // invoke-virtual {v1}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScSupported()Z
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1570 */
com.android.server.wm.MiuiSplitInputMethodStub .getInstance ( );
/* .line 1571 */
com.android.server.wm.MiuiSoScManagerStub .get ( );
v1 = (( com.android.server.wm.MiuiSoScManagerStub ) v1 ).toggleSplitByGesture ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/MiuiSoScManagerStub;->toggleSplitByGesture(Z)Z
/* .line 1574 */
} // :cond_0
v1 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).isVerticalSplit ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isVerticalSplit()Z
/* if-nez v1, :cond_1 */
/* .line 1576 */
} // :cond_1
v1 = this.mAtmService;
/* if-nez v1, :cond_2 */
/* .line 1578 */
} // :cond_2
/* sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z */
final String v3 = "ATMSImpl"; // const-string v3, "ATMSImpl"
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = this.mAtmService;
/* .line 1579 */
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getUiContext ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getUiContext()Landroid/content/Context;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1578 */
final String v4 = "multi_finger_slide"; // const-string v4, "multi_finger_slide"
v1 = android.provider.Settings$Global .getInt ( v1,v4,v2 );
int v4 = 1; // const/4 v4, 0x1
/* if-ne v1, v4, :cond_4 */
/* .line 1580 */
v1 = this.mAtmService;
v1 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v1 ).getDefaultDisplay ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultDisplay()Lcom/android/server/wm/DisplayContent;
/* .line 1581 */
(( com.android.server.wm.DisplayContent ) v1 ).getDisplayPolicy ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayPolicy()Lcom/android/server/wm/DisplayPolicy;
(( com.android.server.wm.DisplayPolicy ) v1 ).getStatusBar ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayPolicy;->getStatusBar()Lcom/android/server/wm/WindowState;
/* .line 1582 */
/* .local v1, "statusBar":Lcom/android/server/wm/WindowState; */
int v4 = 0; // const/4 v4, 0x0
/* .line 1583 */
/* .local v4, "ignoreMultiFingerFlide":Z */
com.android.server.wm.MiuiSoScManagerStub .get ( );
v5 = (( com.android.server.wm.MiuiSoScManagerStub ) v5 ).isSoScSupported ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSoScSupported()Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 1584 */
com.android.server.wm.MiuiSoScManagerStub .get ( );
v5 = (( com.android.server.wm.MiuiSoScManagerStub ) v5 ).isSingleModeActivated ( ); // invoke-virtual {v5}, Lcom/android/server/wm/MiuiSoScManagerStub;->isSingleModeActivated()Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 1585 */
int v4 = 1; // const/4 v4, 0x1
/* .line 1586 */
final String v5 = "Ignore MULTI_FINGER_SLIDE."; // const-string v5, "Ignore MULTI_FINGER_SLIDE."
android.util.Slog .i ( v3,v5 );
/* .line 1588 */
} // :cond_3
if ( v1 != null) { // if-eqz v1, :cond_4
v5 = (( com.android.server.wm.WindowState ) v1 ).isVisible ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->isVisible()Z
/* if-nez v5, :cond_4 */
/* if-nez v4, :cond_4 */
/* .line 1589 */
final String v2 = "Ignore because MULTI_FINGER_SLIDE is setted and StatusBar is hidden."; // const-string v2, "Ignore because MULTI_FINGER_SLIDE is setted and StatusBar is hidden."
android.util.Slog .i ( v3,v2 );
/* .line 1590 */
/* .line 1594 */
} // .end local v1 # "statusBar":Lcom/android/server/wm/WindowState;
} // .end local v4 # "ignoreMultiFingerFlide":Z
} // :cond_4
v1 = this.mAtmService;
v1 = this.mWindowManager;
v1 = v1 = this.mPolicy;
/* if-nez v1, :cond_d */
v1 = this.mAtmService;
v1 = this.mWindowManager;
v1 = this.mPolicy;
v1 = /* .line 1595 */
/* if-nez v1, :cond_5 */
/* goto/16 :goto_1 */
/* .line 1600 */
} // :cond_5
v1 = this.mAtmService;
v1 = (( com.android.server.wm.ActivityTaskManagerService ) v1 ).getLockTaskModeState ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getLockTaskModeState()I
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_6 */
/* .line 1601 */
/* .line 1603 */
} // :cond_6
v1 = this.mAtmService;
v1 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v1 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {v1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
/* .line 1604 */
(( com.android.server.wm.TaskDisplayArea ) v1 ).getFocusedRootTask ( ); // invoke-virtual {v1}, Lcom/android/server/wm/TaskDisplayArea;->getFocusedRootTask()Lcom/android/server/wm/Task;
/* .line 1605 */
/* .local v1, "focusedRootTask":Lcom/android/server/wm/Task; */
/* if-nez v1, :cond_7 */
/* .line 1606 */
final String v2 = "Ignore because get none focused task."; // const-string v2, "Ignore because get none focused task."
android.util.Slog .i ( v3,v2 );
/* .line 1607 */
/* .line 1609 */
} // :cond_7
v2 = (( com.android.server.wm.Task ) v1 ).supportsMultiWindow ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->supportsMultiWindow()Z
/* if-nez v2, :cond_8 */
v2 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).isInSystemSplitScreen ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
/* if-nez v2, :cond_8 */
/* .line 1610 */
final String v2 = "Ignore because the task not support SplitScreen mode."; // const-string v2, "Ignore because the task not support SplitScreen mode."
android.util.Slog .i ( v3,v2 );
/* .line 1611 */
v2 = this.mAtmService;
(( com.android.server.wm.ActivityTaskManagerService ) v2 ).getTaskChangeNotificationController ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getTaskChangeNotificationController()Lcom/android/server/wm/TaskChangeNotificationController;
/* .line 1612 */
(( com.android.server.wm.TaskChangeNotificationController ) v2 ).notifyActivityDismissingDockedRootTask ( ); // invoke-virtual {v2}, Lcom/android/server/wm/TaskChangeNotificationController;->notifyActivityDismissingDockedRootTask()V
/* .line 1613 */
/* .line 1615 */
} // :cond_8
v2 = (( com.android.server.wm.Task ) v1 ).inFreeformWindowingMode ( ); // invoke-virtual {v1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 1616 */
final String v2 = "Ignore because the task at FreeForm mode."; // const-string v2, "Ignore because the task at FreeForm mode."
android.util.Slog .i ( v3,v2 );
/* .line 1617 */
/* .line 1623 */
} // :cond_9
v2 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).isInSystemSplitScreen ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isInSystemSplitScreen(Lcom/android/server/wm/WindowContainer;)Z
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 1624 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->exitSplitScreen(Z)V */
/* .line 1628 */
} // :cond_a
v2 = this.mContext;
/* .line 1629 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1628 */
/* const-string/jumbo v3, "three_gesture_dock_task" */
final String v4 = ""; // const-string v4, ""
android.provider.MiuiSettings$System .getString ( v2,v3,v4 );
/* .line 1630 */
/* .local v2, "threeGestureDockTask":Ljava/lang/String; */
v5 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v5, :cond_b */
/* .line 1631 */
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.MiuiSettings$System .putString ( v5,v3,v4 );
/* .line 1634 */
} // :cond_b
if ( p1 != null) { // if-eqz p1, :cond_c
/* .line 1635 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "gesture_left_to_right"; // const-string v5, "gesture_left_to_right"
v0 = android.provider.MiuiSettings$System .putString ( v4,v3,v5 );
/* .line 1638 */
} // :cond_c
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "gesture_right_to_left"; // const-string v5, "gesture_right_to_left"
v0 = android.provider.MiuiSettings$System .putString ( v4,v3,v5 );
/* .line 1643 */
} // .end local v2 # "threeGestureDockTask":Ljava/lang/String;
} // :goto_0
/* .line 1596 */
} // .end local v1 # "focusedRootTask":Lcom/android/server/wm/Task;
} // :cond_d
} // :goto_1
final String v1 = "Ignore because keyguard is showing or turning screen off."; // const-string v1, "Ignore because keyguard is showing or turning screen off."
android.util.Slog .i ( v3,v1 );
/* .line 1597 */
} // .end method
public void unSetPauseAdvancedInner ( Boolean p0 ) {
/* .locals 8 */
/* .param p1, "resume" # Z */
/* .line 1851 */
v0 = v0 = this.mAdvanceTaskIds;
/* if-nez v0, :cond_0 */
/* .line 1852 */
return;
/* .line 1854 */
} // :cond_0
v0 = this.mAdvanceTaskIds;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_6
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 1855 */
/* .local v1, "taskId":I */
v2 = this.mAtmService;
v2 = this.mRootWindowContainer;
(( com.android.server.wm.RootWindowContainer ) v2 ).anyTaskForId ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/RootWindowContainer;->anyTaskForId(I)Lcom/android/server/wm/Task;
/* .line 1856 */
/* .local v2, "task":Lcom/android/server/wm/Task; */
/* if-nez v2, :cond_1 */
/* .line 1857 */
/* .line 1859 */
} // :cond_1
(( com.android.server.wm.Task ) v2 ).getTopNonFinishingActivity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
/* .line 1860 */
/* .local v3, "topNonFinishingActivity":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v3, :cond_2 */
/* .line 1861 */
/* .line 1863 */
} // :cond_2
(( com.android.server.wm.ActivityRecord ) v3 ).getTaskFragment ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getTaskFragment()Lcom/android/server/wm/TaskFragment;
/* .line 1864 */
/* .local v4, "taskFragment":Lcom/android/server/wm/TaskFragment; */
final String v5 = "ATMSImpl"; // const-string v5, "ATMSImpl"
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 1865 */
v6 = this.mEnterPipDuringPauseAdvance;
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 1866 */
v6 = this.mAtmService;
v6 = this.mH;
v7 = this.mEnterPipDuringPauseAdvance;
(( com.android.server.wm.ActivityTaskManagerService$H ) v6 ).post ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/wm/ActivityTaskManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 1868 */
} // :cond_3
int v6 = 0; // const/4 v6, 0x0
/* iput-boolean v6, v4, Lcom/android/server/wm/TaskFragment;->mPauseAdvance:Z */
/* .line 1869 */
int v6 = 0; // const/4 v6, 0x0
this.mEnterPipDuringPauseAdvance = v6;
/* .line 1870 */
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 1871 */
(( com.android.server.wm.Task ) v2 ).resumeTopActivityUncheckedLocked ( v6, v6 ); // invoke-virtual {v2, v6, v6}, Lcom/android/server/wm/Task;->resumeTopActivityUncheckedLocked(Lcom/android/server/wm/ActivityRecord;Landroid/app/ActivityOptions;)Z
/* .line 1873 */
} // :cond_4
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "unSetPauseAdvance: Task=" */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = ",resume="; // const-string v7, ",resume="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1875 */
} // :cond_5
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "unsetPauseAdvance: Task " */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " not found."; // const-string v7, " not found."
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1877 */
} // .end local v1 # "taskId":I
} // .end local v2 # "task":Lcom/android/server/wm/Task;
} // .end local v3 # "topNonFinishingActivity":Lcom/android/server/wm/ActivityRecord;
} // .end local v4 # "taskFragment":Lcom/android/server/wm/TaskFragment;
} // :goto_1
/* goto/16 :goto_0 */
/* .line 1878 */
} // :cond_6
v0 = this.mAdvanceTaskIds;
/* .line 1882 */
return;
} // .end method
public android.content.Intent updateHomeIntent ( android.content.Intent p0 ) {
/* .locals 3 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1922 */
android.appcompat.ApplicationCompatUtilsStub .get ( );
v0 = (( android.appcompat.ApplicationCompatUtilsStub ) v0 ).isDialogContinuityEnabled ( ); // invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isDialogContinuityEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1924 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* if-nez v0, :cond_0 */
/* .line 1926 */
/* .line 1929 */
} // :cond_0
com.android.server.wm.ApplicationCompatRouterStub .get ( );
v0 = (( com.android.server.wm.ApplicationCompatRouterStub ) v0 ).getConfigDisplayID ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ApplicationCompatRouterStub;->getConfigDisplayID()I
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_1 */
/* .line 1931 */
/* new-instance v0, Landroid/content/ComponentName; */
final String v1 = "com.miui.fliphome"; // const-string v1, "com.miui.fliphome"
final String v2 = "com.miui.fliphome.FlipLauncher"; // const-string v2, "com.miui.fliphome.FlipLauncher"
/* invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) p1 ).setComponent ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1934 */
} // :cond_1
/* .line 1936 */
} // :cond_2
} // .end method
public void updateResizeBlackList ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 756 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->updateMultiTaskWhiteList(Landroid/content/Context;)V */
/* .line 762 */
try { // :try_start_0
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
int v1 = 0; // const/4 v1, 0x0
/* const-string/jumbo v2, "split_screen_applist" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 763 */
/* nop */
/* .line 764 */
try { // :try_start_1
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "miui_resize_black_list_for_pad"; // const-string v3, "miui_resize_black_list_for_pad"
/* .line 763 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v2,v3,v1 );
/* .local v0, "data":Ljava/lang/String; */
/* .line 766 */
} // .end local v0 # "data":Ljava/lang/String;
} // :cond_0
final String v0 = "persist.sys.muiltdisplay_type"; // const-string v0, "persist.sys.muiltdisplay_type"
int v3 = 0; // const/4 v3, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v3 );
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_1 */
/* .line 767 */
/* nop */
/* .line 768 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "miui_resize_black_list_for_fold"; // const-string v3, "miui_resize_black_list_for_fold"
/* .line 767 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v2,v3,v1 );
/* .restart local v0 # "data":Ljava/lang/String; */
/* .line 771 */
} // .end local v0 # "data":Ljava/lang/String;
} // :cond_1
/* nop */
/* .line 772 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "miui_resize_black_list"; // const-string v3, "miui_resize_black_list"
/* .line 771 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v2,v3,v1 );
/* .line 775 */
/* .restart local v0 # "data":Ljava/lang/String; */
} // :goto_0
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_3 */
/* .line 776 */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 777 */
/* .local v1, "apps":Lorg/json/JSONArray; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
v3 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-ge v2, v3, :cond_2 */
/* .line 778 */
v3 = this.mResizeBlackList;
(( org.json.JSONArray ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 777 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 780 */
} // .end local v2 # "i":I
} // :cond_2
return;
/* .line 784 */
} // .end local v0 # "data":Ljava/lang/String;
} // .end local v1 # "apps":Lorg/json/JSONArray;
} // :cond_3
/* .line 782 */
/* :catch_0 */
/* move-exception v0 */
/* .line 783 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ATMSImpl"; // const-string v1, "ATMSImpl"
final String v2 = "Get splitscreen blacklist from xml: "; // const-string v2, "Get splitscreen blacklist from xml: "
android.util.Slog .e ( v1,v2,v0 );
/* .line 785 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
public void updateTopActivity ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .line 396 */
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = this.app;
/* if-nez v0, :cond_0 */
/* .line 399 */
} // :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->skipReportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
return;
/* .line 400 */
} // :cond_1
/* invoke-direct {p0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->reportForegroundActivityChange(Lcom/android/server/wm/ActivityRecord;)V */
/* .line 401 */
return;
/* .line 397 */
} // :cond_2
} // :goto_0
return;
} // .end method
void updateTopResumedActivity ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 4 */
/* .param p1, "prev" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "top" # Lcom/android/server/wm/ActivityRecord; */
/* .line 509 */
if ( p2 != null) { // if-eqz p2, :cond_6
(( com.android.server.wm.ActivityRecord ) p2 ).getRootTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
/* if-nez v0, :cond_0 */
/* .line 512 */
} // :cond_0
v0 = this.mTransitionController;
/* .line 513 */
/* .local v0, "tc":Lcom/android/server/wm/TransitionController; */
if ( v0 != null) { // if-eqz v0, :cond_4
v1 = (( com.android.server.wm.TransitionController ) v0 ).isTransientLaunch ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/TransitionController;->isTransientLaunch(Lcom/android/server/wm/ActivityRecord;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
(( com.android.server.wm.ActivityRecord ) p2 ).getRootTask ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
v1 = (( com.android.server.wm.TransitionController ) v0 ).isTransientHide ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/TransitionController;->isTransientHide(Lcom/android/server/wm/Task;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 514 */
v1 = (( com.android.server.wm.ActivityRecord ) p1 ).isActivityTypeHomeOrRecents ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->isActivityTypeHomeOrRecents()Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 515 */
v1 = (( com.android.server.wm.TransitionController ) v0 ).getCollectingTransitionType ( ); // invoke-virtual {v0}, Lcom/android/server/wm/TransitionController;->getCollectingTransitionType()I
/* const v2, 0x7fffff9a */
/* if-ne v1, v2, :cond_2 */
/* .line 516 */
v1 = (( com.android.server.wm.TransitionController ) v0 ).inCollectingTransition ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/TransitionController;->inCollectingTransition(Lcom/android/server/wm/WindowContainer;)Z
/* if-nez v1, :cond_1 */
(( com.android.server.wm.TransitionController ) v0 ).getCollectingTransition ( ); // invoke-virtual {v0}, Lcom/android/server/wm/TransitionController;->getCollectingTransition()Lcom/android/server/wm/Transition;
v1 = this.mParticipants;
v1 = (( android.util.ArraySet ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->size()I
/* if-nez v1, :cond_2 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* .line 517 */
/* .local v1, "isTopEnterMini":Z */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 518 */
final String v2 = "ATMSImpl"; // const-string v2, "ATMSImpl"
/* const-string/jumbo v3, "updateTopResumedActivity do not report freeform event" */
android.util.Slog .i ( v2,v3 );
/* .line 519 */
return;
/* .line 521 */
} // :cond_3
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).onForegroundActivityChangedLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onForegroundActivityChangedLocked(Lcom/android/server/wm/ActivityRecord;)V
} // .end local v1 # "isTopEnterMini":Z
/* .line 522 */
} // :cond_4
v1 = (( com.android.server.wm.ActivityRecord ) p2 ).inMultiWindowMode ( ); // invoke-virtual {p2}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z
/* if-nez v1, :cond_5 */
/* .line 523 */
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) p0 ).updateTopActivity ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->updateTopActivity(Lcom/android/server/wm/ActivityRecord;)V
/* .line 522 */
} // :cond_5
} // :goto_1
/* nop */
/* .line 525 */
} // :goto_2
return;
/* .line 510 */
} // .end local v0 # "tc":Lcom/android/server/wm/TransitionController;
} // :cond_6
} // :goto_3
return;
} // .end method
