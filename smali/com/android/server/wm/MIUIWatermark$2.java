class com.android.server.wm.MIUIWatermark$2 implements com.android.server.policy.MiuiPhoneWindowManager$MIUIWatermarkCallback {
	 /* .source "MIUIWatermark.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/MIUIWatermark;->init(Lcom/android/server/wm/WindowManagerService;Ljava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MIUIWatermark this$0; //synthetic
final java.lang.String val$msg; //synthetic
final com.android.server.wm.WindowManagerService val$wms; //synthetic
/* # direct methods */
 com.android.server.wm.MIUIWatermark$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MIUIWatermark; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 303 */
this.this$0 = p1;
this.val$wms = p2;
this.val$msg = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onHideWatermark ( ) {
/* .locals 1 */
/* .line 317 */
v0 = this.this$0;
com.android.server.wm.MIUIWatermark .-$$Nest$mhideWaterMarker ( v0 );
/* .line 318 */
return;
} // .end method
public void onShowWatermark ( ) {
/* .locals 2 */
/* .line 306 */
v0 = this.this$0;
com.android.server.wm.MIUIWatermark .-$$Nest$fgetmSurfaceControl ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 307 */
v0 = this.this$0;
v1 = this.val$wms;
v1 = this.mContext;
com.android.server.wm.MIUIWatermark .-$$Nest$mupdateText ( v0,v1 );
/* .line 308 */
v0 = this.this$0;
com.android.server.wm.MIUIWatermark .-$$Nest$mshowWaterMarker ( v0 );
/* .line 309 */
return;
/* .line 311 */
} // :cond_0
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.wm.MIUIWatermark .-$$Nest$fputmEnableMIUIWatermark ( v0,v1 );
/* .line 312 */
v0 = this.this$0;
v1 = this.val$msg;
com.android.server.wm.MIUIWatermark .-$$Nest$mcreateSurfaceLocked ( v0,v1 );
/* .line 313 */
return;
} // .end method
