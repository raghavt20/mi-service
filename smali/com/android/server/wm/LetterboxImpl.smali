.class public Lcom/android/server/wm/LetterboxImpl;
.super Lcom/android/server/wm/LetterboxStub;
.source "LetterboxImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;
    }
.end annotation


# static fields
.field private static final BITMAP_SURFACE_NAME:Ljava/lang/String; = "bitmapBackground"

.field private static final FLIP_SMALL_SURFACE_NAME:Ljava/lang/String; = "flipSmallBackground"

.field private static final TAG:Ljava/lang/String; = "LetterboxImpl"


# instance fields
.field private mActivityRecord:Lcom/android/server/wm/ActivityRecord;

.field protected mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

.field private mIsDarkMode:Z

.field private mLetterbox:Lcom/android/server/wm/Letterbox;

.field private mNeedRedraw:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmActivityRecord(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/ActivityRecord;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLetterbox(Lcom/android/server/wm/LetterboxImpl;)Lcom/android/server/wm/Letterbox;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/LetterboxImpl;->mLetterbox:Lcom/android/server/wm/Letterbox;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateColor(Lcom/android/server/wm/LetterboxImpl;Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/wm/LetterboxImpl;->updateColor(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Lcom/android/server/wm/LetterboxStub;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z

    return-void
.end method

.method private updateColor(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;)V
    .locals 4
    .param p1, "sc"    # Landroid/view/SurfaceControl;
    .param p2, "sct"    # Landroid/view/SurfaceControl$Transaction;

    .line 428
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 429
    invoke-static {v1}, Landroid/graphics/Color;->valueOf(I)Landroid/graphics/Color;

    move-result-object v0

    .local v0, "color":Landroid/graphics/Color;
    goto :goto_0

    .line 431
    .end local v0    # "color":Landroid/graphics/Color;
    :cond_0
    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->getNavigationBarColor()I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->valueOf(I)Landroid/graphics/Color;

    move-result-object v0

    .line 433
    .restart local v0    # "color":Landroid/graphics/Color;
    :goto_0
    const/4 v2, 0x3

    new-array v2, v2, [F

    invoke-virtual {v0}, Landroid/graphics/Color;->red()F

    move-result v3

    aput v3, v2, v1

    const/4 v1, 0x1

    invoke-virtual {v0}, Landroid/graphics/Color;->green()F

    move-result v3

    aput v3, v2, v1

    const/4 v1, 0x2

    invoke-virtual {v0}, Landroid/graphics/Color;->blue()F

    move-result v3

    aput v3, v2, v1

    invoke-virtual {p2, p1, v2}, Landroid/view/SurfaceControl$Transaction;->setColor(Landroid/view/SurfaceControl;[F)Landroid/view/SurfaceControl$Transaction;

    .line 434
    return-void
.end method


# virtual methods
.method public applyLetterboxSurfaceChanges(Landroid/view/SurfaceControl$Transaction;Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/String;)Z
    .locals 1
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "mSurfaceFrameRelative"    # Landroid/graphics/Rect;
    .param p3, "mLayoutFrameRelative"    # Landroid/graphics/Rect;
    .param p4, "mType"    # Ljava/lang/String;

    .line 133
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->applyLetterboxSurfaceChanges(Landroid/view/SurfaceControl$Transaction;Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public applySurfaceChanges(Landroid/view/SurfaceControl$Transaction;[Lcom/android/server/wm/Letterbox$LetterboxSurface;Lcom/android/server/wm/Letterbox$LetterboxSurface;)V
    .locals 3
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "mSurfaces"    # [Lcom/android/server/wm/Letterbox$LetterboxSurface;
    .param p3, "mBehind"    # Lcom/android/server/wm/Letterbox$LetterboxSurface;

    .line 99
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->applySurfaceChanges(Landroid/view/SurfaceControl$Transaction;)V

    .line 101
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p2, v1

    .line 102
    .local v2, "surface":Lcom/android/server/wm/Letterbox$LetterboxSurface;
    invoke-virtual {v2}, Lcom/android/server/wm/Letterbox$LetterboxSurface;->remove()V

    .line 101
    .end local v2    # "surface":Lcom/android/server/wm/Letterbox$LetterboxSurface;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    :cond_0
    invoke-virtual {p3}, Lcom/android/server/wm/Letterbox$LetterboxSurface;->remove()V

    .line 106
    return-void
.end method

.method public attachInput(Lcom/android/server/wm/WindowState;)Z
    .locals 1
    .param p1, "win"    # Lcom/android/server/wm/WindowState;

    .line 57
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iput-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 58
    invoke-virtual {p0}, Lcom/android/server/wm/LetterboxImpl;->useMiuiBackgroundWindowSurface()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x0

    return v0

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->attachInput(Lcom/android/server/wm/WindowState;)V

    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public createLetterboxSurfaceSurface(Landroid/view/SurfaceControl$Transaction;Ljava/lang/String;Ljava/util/function/Supplier;)Landroid/view/SurfaceControl;
    .locals 1
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p2, "mType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/SurfaceControl$Transaction;",
            "Ljava/lang/String;",
            "Ljava/util/function/Supplier<",
            "Landroid/view/SurfaceControl$Builder;",
            ">;)",
            "Landroid/view/SurfaceControl;"
        }
    .end annotation

    .line 117
    .local p3, "mSurfaceControlFactory":Ljava/util/function/Supplier;, "Ljava/util/function/Supplier<Landroid/view/SurfaceControl$Builder;>;"
    invoke-virtual {p0, p2}, Lcom/android/server/wm/LetterboxImpl;->isFlipSmallSurface(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0, p1, p3}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->createFlipSmallSurface(Landroid/view/SurfaceControl$Transaction;Ljava/util/function/Supplier;)Landroid/view/SurfaceControl;

    move-result-object v0

    return-object v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->createSurface(Landroid/view/SurfaceControl$Transaction;Ljava/lang/String;Ljava/util/function/Supplier;)Landroid/view/SurfaceControl;

    move-result-object v0

    return-object v0
.end method

.method public darkModeChanged()Z
    .locals 5

    .line 80
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 82
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v2, v2, 0x30

    const/16 v3, 0x20

    const/4 v4, 0x1

    if-ne v2, v3, :cond_0

    move v2, v4

    goto :goto_0

    :cond_0
    move v2, v1

    .line 84
    .local v2, "isDarkMode":Z
    :goto_0
    iget-boolean v3, p0, Lcom/android/server/wm/LetterboxImpl;->mIsDarkMode:Z

    if-eq v3, v2, :cond_1

    .line 85
    iput-boolean v2, p0, Lcom/android/server/wm/LetterboxImpl;->mIsDarkMode:Z

    .line 86
    iput-boolean v4, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z

    .line 87
    return v4

    .line 90
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "isDarkMode":Z
    :cond_1
    return v1
.end method

.method public getNavigationBarColor()I
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-nez v0, :cond_0

    .line 199
    const/4 v0, 0x0

    return v0

    .line 200
    :cond_0
    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->getNavigationBarColor()I

    move-result v0

    return v0
.end method

.method public isBitmapSurface(Ljava/lang/String;)Z
    .locals 1
    .param p1, "mType"    # Ljava/lang/String;

    .line 124
    const-string v0, "bitmapBackground"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isFlipSmallSurface()Z
    .locals 1

    .line 438
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/wm/LetterboxImpl;->isFlipSmallSurface(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isFlipSmallSurface(Ljava/lang/String;)Z
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .line 128
    const-string v0, "flipSmallBackground"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public layout(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Point;)V
    .locals 6
    .param p1, "outer"    # Landroid/graphics/Rect;
    .param p2, "inner"    # Landroid/graphics/Rect;
    .param p3, "surfaceOrigin"    # Landroid/graphics/Point;

    .line 66
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->layout(IIIILandroid/graphics/Point;)V

    .line 67
    return-void
.end method

.method public navigationBarColorChanged()Z
    .locals 1

    .line 443
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->navigationBarColorChanged()Z

    move-result v0

    return v0
.end method

.method public needRemoveMiuiLetterbox()Z
    .locals 4

    .line 187
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->needRemoveMiuiLetterbox()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 190
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isClientVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    sget-object v1, Lcom/android/server/wm/ActivityRecord$State;->STOPPED:Lcom/android/server/wm/ActivityRecord$State;

    sget-object v2, Lcom/android/server/wm/ActivityRecord$State;->DESTROYING:Lcom/android/server/wm/ActivityRecord$State;

    sget-object v3, Lcom/android/server/wm/ActivityRecord$State;->DESTROYED:Lcom/android/server/wm/ActivityRecord$State;

    .line 191
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;Lcom/android/server/wm/ActivityRecord$State;Lcom/android/server/wm/ActivityRecord$State;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x1

    return v0

    .line 194
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public needShowLetterbox()Z
    .locals 3

    .line 173
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_2

    .line 174
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, v2}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isActivityInFixedOrientation(Lcom/android/server/wm/ActivityRecord;ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    iget-object v1, v1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isEmbeddingEnabledForPackage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    .line 176
    invoke-virtual {v0, v2}, Lcom/android/server/wm/ActivityRecord;->isActivityEmbedded(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    return v2

    .line 180
    :cond_1
    const/4 v0, 0x0

    return v0

    .line 183
    :cond_2
    invoke-super {p0}, Lcom/android/server/wm/LetterboxStub;->needShowLetterbox()Z

    move-result v0

    return v0
.end method

.method public needsApplySurfaceChanges()Z
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->needsApplySurfaceChanges()Z

    move-result v0

    return v0
.end method

.method public onMovedToDisplay(I)V
    .locals 1
    .param p1, "displayId"    # I

    .line 109
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getInputInterceptor()Lcom/android/server/wm/Letterbox$InputInterceptor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getInputInterceptor()Lcom/android/server/wm/Letterbox$InputInterceptor;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/Letterbox$InputInterceptor;->mWindowHandle:Landroid/view/InputWindowHandle;

    iput p1, v0, Landroid/view/InputWindowHandle;->displayId:I

    .line 112
    :cond_0
    return-void
.end method

.method public redraw(Z)V
    .locals 3
    .param p1, "forceRedraw"    # Z

    .line 138
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_0

    .line 139
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, v2}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isActivityInFixedOrientation(Lcom/android/server/wm/ActivityRecord;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->redrawBitmapSurface()V

    .line 142
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_3

    .line 146
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->isEmbedded()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_3

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->redrawBitmapSurface()V

    .line 148
    iget-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z

    if-eqz v0, :cond_2

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/LetterboxImpl;->mNeedRedraw:Z

    .line 151
    :cond_2
    return-void

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_4

    .line 155
    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 157
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->redrawBitmapSurface()V

    .line 160
    :cond_4
    invoke-virtual {p0}, Lcom/android/server/wm/LetterboxImpl;->navigationBarColorChanged()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 161
    :try_start_0
    new-instance v0, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .local v0, "sct":Landroid/view/SurfaceControl$Transaction;
    :try_start_1
    iget-object v1, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v1}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->getSurface()Landroid/view/SurfaceControl;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/android/server/wm/LetterboxImpl;->updateColor(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;)V

    .line 163
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    :try_start_2
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 165
    .end local v0    # "sct":Landroid/view/SurfaceControl$Transaction;
    goto :goto_1

    .line 161
    .restart local v0    # "sct":Landroid/view/SurfaceControl$Transaction;
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v2

    :try_start_4
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/wm/LetterboxImpl;
    .end local p1    # "forceRedraw":Z
    :goto_0
    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 164
    .end local v0    # "sct":Landroid/view/SurfaceControl$Transaction;
    .restart local p0    # "this":Lcom/android/server/wm/LetterboxImpl;
    .restart local p1    # "forceRedraw":Z
    :catch_0
    move-exception v0

    .line 166
    :goto_1
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_5

    .line 167
    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v0}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->updateNavigationBarColorFinish()V

    .line 170
    :cond_5
    return-void
.end method

.method public remove()V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    invoke-virtual {v0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;->remove()V

    .line 71
    return-void
.end method

.method public setLetterbox(Lcom/android/server/wm/Letterbox;)V
    .locals 3
    .param p1, "letterbox"    # Lcom/android/server/wm/Letterbox;

    .line 46
    iput-object p1, p0, Lcom/android/server/wm/LetterboxImpl;->mLetterbox:Lcom/android/server/wm/Letterbox;

    .line 47
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-static {}, Landroid/hardware/devicestate/DeviceStateManagerGlobal;->getInstance()Landroid/hardware/devicestate/DeviceStateManagerGlobal;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/devicestate/DeviceStateManagerGlobal;->getCurrentState()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 49
    .local v0, "isFlipSmallScreen":Z
    :goto_0
    new-instance v1, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    .line 50
    if-eqz v0, :cond_1

    .line 51
    const-string v2, "flipSmallBackground"

    goto :goto_1

    .line 52
    :cond_1
    const-string v2, "bitmapBackground"

    :goto_1
    invoke-direct {v1, p0, v2, p0}, Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;-><init>(Lcom/android/server/wm/LetterboxImpl;Ljava/lang/String;Lcom/android/server/wm/LetterboxImpl;)V

    iput-object v1, p0, Lcom/android/server/wm/LetterboxImpl;->mBackgroundSurfaceEx:Lcom/android/server/wm/LetterboxImpl$LetterboxSurfaceEx;

    .line 54
    return-void
.end method

.method public useMiuiBackgroundWindowSurface()Z
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    if-eqz v0, :cond_2

    .line 75
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->inMiuiSizeCompatMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 76
    :cond_0
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/LetterboxImpl;->mActivityRecord:Lcom/android/server/wm/ActivityRecord;

    invoke-interface {v0, v1}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->useMiuiBackgroundLetterbox(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 74
    :goto_0
    return v0
.end method
