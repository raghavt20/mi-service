.class Lcom/android/server/wm/AppTransitionInjector$2;
.super Ljava/lang/Object;
.source "AppTransitionInjector.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/AppTransitionInjector;->addAnimationListener(Landroid/view/animation/Animation;Landroid/os/Handler;Landroid/os/IRemoteCallback;Landroid/os/IRemoteCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic val$exitFinishCallback:Landroid/os/IRemoteCallback;

.field final synthetic val$exitStartCallback:Landroid/os/IRemoteCallback;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method public static synthetic $r8$lambda$sMT_oQ8Ga3BYZGB9ahzbMMbI2eo(Landroid/os/IRemoteCallback;)V
    .locals 0

    invoke-static {p0}, Lcom/android/server/wm/AppTransitionInjector;->-$$Nest$smdoAnimationCallback(Landroid/os/IRemoteCallback;)V

    return-void
.end method

.method constructor <init>(Landroid/os/IRemoteCallback;Landroid/os/Handler;Landroid/os/IRemoteCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 385
    iput-object p1, p0, Lcom/android/server/wm/AppTransitionInjector$2;->val$exitStartCallback:Landroid/os/IRemoteCallback;

    iput-object p2, p0, Lcom/android/server/wm/AppTransitionInjector$2;->val$handler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/android/server/wm/AppTransitionInjector$2;->val$exitFinishCallback:Landroid/os/IRemoteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .line 395
    iget-object v0, p0, Lcom/android/server/wm/AppTransitionInjector$2;->val$exitFinishCallback:Landroid/os/IRemoteCallback;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/android/server/wm/AppTransitionInjector$2;->val$handler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/AppTransitionInjector$2$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/android/server/wm/AppTransitionInjector$2$$ExternalSyntheticLambda0;-><init>()V

    iget-object v2, p0, Lcom/android/server/wm/AppTransitionInjector$2;->val$exitFinishCallback:Landroid/os/IRemoteCallback;

    invoke-static {v1, v2}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainMessage(Ljava/util/function/Consumer;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 399
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .line 403
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .line 388
    iget-object v0, p0, Lcom/android/server/wm/AppTransitionInjector$2;->val$exitStartCallback:Landroid/os/IRemoteCallback;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/android/server/wm/AppTransitionInjector$2;->val$handler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/AppTransitionInjector$2$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/android/server/wm/AppTransitionInjector$2$$ExternalSyntheticLambda0;-><init>()V

    iget-object v2, p0, Lcom/android/server/wm/AppTransitionInjector$2;->val$exitStartCallback:Landroid/os/IRemoteCallback;

    invoke-static {v1, v2}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainMessage(Ljava/util/function/Consumer;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 392
    :cond_0
    return-void
.end method
