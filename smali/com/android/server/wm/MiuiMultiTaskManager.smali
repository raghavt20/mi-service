.class public Lcom/android/server/wm/MiuiMultiTaskManager;
.super Ljava/lang/Object;
.source "MiuiMultiTaskManager.java"

# interfaces
.implements Lcom/android/server/wm/MiuiMultiTaskManagerStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;
    }
.end annotation


# static fields
.field private static final FEATURE_SUPPORT:Z

.field public static final FLAG_LAUNCH_APP_IN_ONE_TASK_GROUP:Ljava/lang/String; = "miui_launch_app_in_one_task_group"

.field public static final TASK_RETURN_TO_TARGET:Ljava/lang/String; = "miui_task_return_to_target"

.field private static sSupportUI:[Ljava/lang/String;

.field private static sTargetMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 17
    const-string v0, "com.tencent.mm.plugin.webview.ui.tools.WebViewUI"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/MiuiMultiTaskManager;->sSupportUI:[Ljava/lang/String;

    .line 18
    const-string v0, "miui.multitask.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiMultiTaskManager;->FEATURE_SUPPORT:Z

    .line 19
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/wm/MiuiMultiTaskManager;->sTargetMap:Ljava/util/HashMap;

    .line 21
    invoke-static {}, Lcom/android/server/wm/MiuiMultiTaskManager;->init()V

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static checkMultiTaskAffinity(Lcom/android/server/wm/ActivityRecord;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 7
    .param p0, "target"    # Lcom/android/server/wm/ActivityRecord;
    .param p1, "checkRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 92
    sget-boolean v0, Lcom/android/server/wm/MiuiMultiTaskManager;->FEATURE_SUPPORT:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 93
    :cond_0
    sget-object v0, Lcom/android/server/wm/MiuiMultiTaskManager;->sSupportUI:[Ljava/lang/String;

    array-length v2, v0

    move v3, v1

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v0, v3

    .line 94
    .local v4, "className":Ljava/lang/String;
    if-eqz p1, :cond_1

    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    if-eqz v5, :cond_1

    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 95
    invoke-static {v4, v5}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 96
    if-eqz p0, :cond_1

    iget-object v5, p0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-static {v5, v6}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 97
    const/4 v0, 0x1

    return v0

    .line 93
    .end local v4    # "className":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 101
    :cond_2
    return v1
.end method

.method private static getLaunchAppInfoByName(Ljava/lang/String;)Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .line 41
    const-string v0, "com.tencent.mm.plugin.webview.ui.tools.WebViewUI"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v1, "supports":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "com.tencent.mm.ui.LauncherUI"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    const-string v3, "com.tencent.mm.ui.chatting.ChattingUI"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    const-string v3, "com.tencent.mm.plugin.sns.ui.SnsTimeLineUI"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    const-string v3, "com.tencent.mm.plugin.readerapp.ui.ReaderAppUI"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    const-string v3, "com.tencent.mm.ui.conversation.BizConversationUI"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance v0, Landroid/content/ComponentName;

    const-string v3, "com.tencent.mm"

    invoke-direct {v0, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .local v0, "returnTarget":Landroid/content/ComponentName;
    new-instance v2, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;

    invoke-direct {v2, v1, v0}, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;-><init>(Ljava/util/ArrayList;Landroid/content/ComponentName;)V

    return-object v2

    .line 53
    .end local v0    # "returnTarget":Landroid/content/ComponentName;
    .end local v1    # "supports":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private static init()V
    .locals 4

    .line 35
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/android/server/wm/MiuiMultiTaskManager;->sSupportUI:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 36
    sget-object v2, Lcom/android/server/wm/MiuiMultiTaskManager;->sTargetMap:Ljava/util/HashMap;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/android/server/wm/MiuiMultiTaskManager;->getLaunchAppInfoByName(Ljava/lang/String;)Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method static isMultiTaskSupport(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 6
    .param p0, "record"    # Lcom/android/server/wm/ActivityRecord;

    .line 81
    sget-boolean v0, Lcom/android/server/wm/MiuiMultiTaskManager;->FEATURE_SUPPORT:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 82
    :cond_0
    sget-object v0, Lcom/android/server/wm/MiuiMultiTaskManager;->sSupportUI:[Ljava/lang/String;

    array-length v2, v0

    move v3, v1

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v0, v3

    .line 83
    .local v4, "className":Ljava/lang/String;
    if-eqz p0, :cond_1

    iget-object v5, p0, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 84
    invoke-static {v4, v5}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 85
    const/4 v0, 0x1

    return v0

    .line 82
    .end local v4    # "className":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 88
    :cond_2
    return v1
.end method

.method static isVersionSupport()Z
    .locals 1

    .line 105
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public updateMultiTaskInfoIfNeed(Lcom/android/server/wm/Task;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;)V
    .locals 4
    .param p1, "stack"    # Lcom/android/server/wm/Task;
    .param p2, "aInfo"    # Landroid/content/pm/ActivityInfo;
    .param p3, "intent"    # Landroid/content/Intent;

    .line 58
    sget-boolean v0, Lcom/android/server/wm/MiuiMultiTaskManager;->FEATURE_SUPPORT:Z

    if-nez v0, :cond_0

    return-void

    .line 59
    :cond_0
    invoke-static {}, Lcom/android/server/wm/MiuiMultiTaskManager;->isVersionSupport()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_3

    sget-object v0, Lcom/android/server/wm/MiuiMultiTaskManager;->sTargetMap:Ljava/util/HashMap;

    iget-object v1, p2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p3, :cond_3

    .line 61
    sget-object v0, Lcom/android/server/wm/MiuiMultiTaskManager;->sTargetMap:Ljava/util/HashMap;

    iget-object v1, p2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;

    .line 62
    .local v0, "info":Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 64
    .local v1, "topr":Lcom/android/server/wm/ActivityRecord;
    :goto_0
    :try_start_0
    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;->-$$Nest$fgetsupports(Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;->-$$Nest$fgetreturnTarget(Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;->-$$Nest$fgetsupports(Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, v1, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 66
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 67
    invoke-virtual {p3}, Landroid/content/Intent;->getFlags()I

    move-result v2

    const v3, -0x8000001

    and-int/2addr v2, v3

    invoke-virtual {p3, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 69
    const v2, 0x8000

    invoke-virtual {p3, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 70
    const/high16 v2, 0x80000

    invoke-virtual {p3, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 71
    const-string v2, "miui_launch_app_in_one_task_group"

    const/4 v3, 0x1

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 72
    const-string v2, "miui_task_return_to_target"

    invoke-static {v0}, Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;->-$$Nest$fgetreturnTarget(Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;)Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :cond_2
    goto :goto_1

    .line 74
    :catch_0
    move-exception v2

    .line 78
    .end local v0    # "info":Lcom/android/server/wm/MiuiMultiTaskManager$LaunchAppInfo;
    .end local v1    # "topr":Lcom/android/server/wm/ActivityRecord;
    :cond_3
    :goto_1
    return-void
.end method
