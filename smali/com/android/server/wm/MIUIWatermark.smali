.class Lcom/android/server/wm/MIUIWatermark;
.super Ljava/lang/Object;
.source "MIUIWatermark.java"

# interfaces
.implements Lcom/android/server/wm/MIUIWatermarkStub;


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "Watermark"

.field private static final degree:I = 0x1e

.field private static volatile mIMEI:Ljava/lang/String; = null

.field private static mTextSize:I = 0x0

.field private static final textSize:I = 0xd


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

.field private mCallback:Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

.field private mDrawNeeded:Z

.field private volatile mEnableMIUIWatermark:Z

.field private mImeiThreadisRuning:Z

.field private mLastDH:I

.field private mLastDW:I

.field private mSurface:Landroid/view/Surface;

.field private mSurfaceControl:Landroid/view/SurfaceControl;

.field private final mTextPaint:Landroid/graphics/Paint;

.field private mTransaction:Landroid/view/SurfaceControl$Transaction;

.field private mWmService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmSurfaceControl(Lcom/android/server/wm/MIUIWatermark;)Landroid/view/SurfaceControl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MIUIWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmEnableMIUIWatermark(Lcom/android/server/wm/MIUIWatermark;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/MIUIWatermark;->mEnableMIUIWatermark:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmImeiThreadisRuning(Lcom/android/server/wm/MIUIWatermark;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/MIUIWatermark;->mImeiThreadisRuning:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcreateSurfaceLocked(Lcom/android/server/wm/MIUIWatermark;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->createSurfaceLocked(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhideWaterMarker(Lcom/android/server/wm/MIUIWatermark;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MIUIWatermark;->hideWaterMarker()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetImei(Lcom/android/server/wm/MIUIWatermark;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->setImei(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowWaterMarker(Lcom/android/server/wm/MIUIWatermark;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MIUIWatermark;->showWaterMarker()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateText(Lcom/android/server/wm/MIUIWatermark;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->updateText(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smgetImeiID(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/android/server/wm/MIUIWatermark;->getImeiID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 49
    const/4 v0, 0x0

    sput v0, Lcom/android/server/wm/MIUIWatermark;->mTextSize:I

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDW:I

    .line 57
    iput v0, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDH:I

    .line 59
    iput-boolean v0, p0, Lcom/android/server/wm/MIUIWatermark;->mImeiThreadisRuning:Z

    return-void
.end method

.method private createSurfaceLocked(Ljava/lang/String;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;

    .line 327
    const-string v0, "createWatermarkInTransaction"

    iget-boolean v1, p0, Lcom/android/server/wm/MIUIWatermark;->mEnableMIUIWatermark:Z

    if-nez v1, :cond_0

    .line 328
    return-void

    .line 330
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v1, :cond_1

    return-void

    .line 332
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    const/high16 v2, 0x41500000    # 13.0f

    invoke-static {v1, v2}, Lcom/android/server/wm/MIUIWatermark;->dp2px(Landroid/content/Context;F)I

    move-result v1

    sput v1, Lcom/android/server/wm/MIUIWatermark;->mTextSize:I

    .line 333
    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V

    .line 335
    :try_start_0
    const-string v1, "Watermark"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "create water mark: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    invoke-direct {p0}, Lcom/android/server/wm/MIUIWatermark;->doCreateSurfaceLocked()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338
    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 339
    nop

    .line 340
    return-void

    .line 338
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V

    .line 339
    throw v1
.end method

.method private doCreateSurfaceLocked()V
    .locals 10

    .line 65
    const-string v0, "MIUIWatermarkSurface"

    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, v1, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/wm/MIUIWatermark;->loadAccountId(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    .line 66
    .local v1, "account":Landroid/accounts/Account;
    if-eqz v1, :cond_0

    iget-object v2, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, Lcom/android/server/wm/MIUIWatermark;->mAccountName:Ljava/lang/String;

    .line 67
    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/android/server/wm/MIUIWatermark;->getImei(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    .line 72
    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    sget v3, Lcom/android/server/wm/MIUIWatermark;->mTextSize:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 73
    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 74
    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    const v3, 0x50ffffff

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 75
    iget-object v2, p0, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x50000000

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v2, v3, v5, v5, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 77
    const/4 v2, 0x0

    .line 81
    .local v2, "ctrl":Landroid/view/SurfaceControl;
    :try_start_0
    iget-object v3, p0, Lcom/android/server/wm/MIUIWatermark;->mWmService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v3}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v3

    .line 82
    .local v3, "dc":Lcom/android/server/wm/DisplayContent;
    invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 83
    invoke-virtual {v4, v0}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 84
    invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 85
    const/4 v5, -0x3

    invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setFormat(I)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    .line 86
    invoke-virtual {v4, v0}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v0

    move-object v2, v0

    .line 90
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    const v4, 0xf4240

    invoke-virtual {v0, v2, v4}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    .line 91
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v4}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 92
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 93
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v3}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I

    move-result v4

    const-string v5, "MIUIWatermark"

    invoke-static {v2, v0, v4, v5}, Lcom/android/server/wm/InputMonitor;->setTrustedOverlayInputInfo(Landroid/view/SurfaceControl;Landroid/view/SurfaceControl$Transaction;ILjava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V

    .line 95
    iput-object v2, p0, Lcom/android/server/wm/MIUIWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 96
    new-instance v0, Landroid/graphics/BLASTBufferQueue;

    const-string v5, "MIUIWatermarkSurface"

    iget-object v6, p0, Lcom/android/server/wm/MIUIWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Landroid/graphics/BLASTBufferQueue;-><init>(Ljava/lang/String;Landroid/view/SurfaceControl;III)V

    iput-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    .line 98
    invoke-virtual {v0}, Landroid/graphics/BLASTBufferQueue;->createSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mSurface:Landroid/view/Surface;
    :try_end_0
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .end local v3    # "dc":Lcom/android/server/wm/DisplayContent;
    goto :goto_1

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Landroid/view/Surface$OutOfResourcesException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createrSurface e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Watermark"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    .end local v0    # "e":Landroid/view/Surface$OutOfResourcesException;
    :goto_1
    return-void
.end method

.method private static dp2px(Landroid/content/Context;F)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dpValue"    # F

    .line 343
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 344
    .local v0, "scale":F
    mul-float v1, p1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method private getImei(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 288
    invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->getImeiInfo(Landroid/content/Context;)V

    .line 289
    sget-object v0, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    return-object v0
.end method

.method private static getImeiID(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 272
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 273
    .local v0, "telephonyMgr":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getImei(I)Ljava/lang/String;

    move-result-object v1

    .line 274
    .local v1, "imei":Ljava/lang/String;
    :goto_0
    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 275
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 277
    :cond_1
    invoke-static {}, Lmiui/view/MiuiSecurityPermissionHandler;->noImei()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 278
    invoke-static {p0}, Lmiui/view/MiuiSecurityPermissionHandler;->getWifiMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 280
    :cond_2
    return-object v1
.end method

.method private getImeiInfo(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 233
    invoke-static {p1}, Lcom/android/server/wm/MIUIWatermark;->getImeiID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 234
    .local v0, "imei":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 235
    invoke-direct {p0, v0}, Lcom/android/server/wm/MIUIWatermark;->setImei(Ljava/lang/String;)V

    .line 236
    return-void

    .line 238
    :cond_0
    monitor-enter p0

    .line 239
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/wm/MIUIWatermark;->mImeiThreadisRuning:Z

    if-eqz v1, :cond_1

    .line 240
    monitor-exit p0

    return-void

    .line 242
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/wm/MIUIWatermark;->mImeiThreadisRuning:Z

    .line 243
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    new-instance v1, Lcom/android/server/wm/MIUIWatermark$1;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MIUIWatermark$1;-><init>(Lcom/android/server/wm/MIUIWatermark;Landroid/content/Context;)V

    .line 268
    invoke-virtual {v1}, Lcom/android/server/wm/MIUIWatermark$1;->start()V

    .line 269
    return-void

    .line 243
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private hideWaterMarker()V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_0

    .line 123
    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 125
    :cond_0
    return-void
.end method

.method private static loadAccountId(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 224
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 225
    const-string v1, "com.xiaomi"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 226
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 227
    const/4 v1, 0x0

    aget-object v1, v0, v1

    return-object v1

    .line 229
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method private declared-synchronized setImei(Ljava/lang/String;)V
    .locals 1
    .param p1, "imei"    # Ljava/lang/String;

    monitor-enter p0

    .line 284
    :try_start_0
    sput-object p1, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    monitor-exit p0

    return-void

    .line 283
    .end local p0    # "this":Lcom/android/server/wm/MIUIWatermark;
    .end local p1    # "imei":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private showWaterMarker()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_0

    .line 117
    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    .line 119
    :cond_0
    return-void
.end method

.method private updateText(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 128
    invoke-static {p1}, Lcom/android/server/wm/MIUIWatermark;->loadAccountId(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 129
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 130
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mAccountName:Ljava/lang/String;

    .line 133
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/MIUIWatermark;->getImei(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, "imei":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 135
    sput-object v1, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    .line 138
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z

    .line 139
    return-void
.end method


# virtual methods
.method public drawIfNeeded()V
    .locals 24

    .line 143
    move-object/from16 v1, p0

    const-string v2, "Watermark"

    iget-object v0, v1, Lcom/android/server/wm/MIUIWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_b

    iget-boolean v3, v1, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z

    if-nez v3, :cond_0

    goto/16 :goto_6

    .line 145
    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z

    .line 146
    iget v4, v1, Lcom/android/server/wm/MIUIWatermark;->mLastDW:I

    .line 147
    .local v4, "dw":I
    iget v5, v1, Lcom/android/server/wm/MIUIWatermark;->mLastDH:I

    .line 149
    .local v5, "dh":I
    iget-object v6, v1, Lcom/android/server/wm/MIUIWatermark;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    const/4 v7, 0x1

    invoke-virtual {v6, v0, v4, v5, v7}, Landroid/graphics/BLASTBufferQueue;->update(Landroid/view/SurfaceControl;III)V

    .line 151
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v6, v0

    .line 152
    .local v6, "dirty":Landroid/graphics/Rect;
    const/4 v7, 0x0

    .line 154
    .local v7, "c":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v0, v1, Lcom/android/server/wm/MIUIWatermark;->mSurface:Landroid/view/Surface;

    invoke-virtual {v0, v6}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v7, v0

    .line 157
    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v8, "Failed to lock canvas"

    invoke-static {v2, v8, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 159
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :goto_0
    if-nez v7, :cond_1

    .line 160
    return-void

    .line 164
    :cond_1
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v7, v3, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 166
    const-wide/high16 v8, 0x403e000000000000L    # 30.0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    .line 167
    .local v8, "radians":D
    const-wide v10, 0x3fe3333333333333L    # 0.6

    int-to-double v12, v4

    mul-double/2addr v12, v10

    double-to-int v0, v12

    .line 168
    .local v0, "x":I
    const-wide v10, 0x3fa999999999999aL    # 0.05

    int-to-double v12, v5

    mul-double/2addr v12, v10

    double-to-int v3, v12

    .line 169
    .local v3, "y":I
    if-le v4, v5, :cond_2

    .line 170
    const-wide v10, 0x3fd3333333333333L    # 0.3

    int-to-double v12, v4

    mul-double/2addr v12, v10

    double-to-int v0, v12

    .line 172
    :cond_2
    sget v10, Lcom/android/server/wm/MIUIWatermark;->mTextSize:I

    int-to-double v10, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->tan(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    double-to-int v10, v10

    .line 173
    .local v10, "incDeltx":I
    sget v11, Lcom/android/server/wm/MIUIWatermark;->mTextSize:I

    mul-int/lit8 v11, v11, -0x1

    .line 174
    .local v11, "incDelty":I
    div-int/lit8 v12, v4, 0x2

    .line 175
    .local v12, "columnDeltX":I
    const-wide v13, 0x3fc1cac083126e98L    # 0.139

    move-wide v15, v8

    .end local v8    # "radians":D
    .local v15, "radians":D
    int-to-double v8, v5

    mul-double/2addr v8, v13

    double-to-int v8, v8

    .line 176
    .local v8, "columnDeltY":I
    const-wide v13, 0x3fceb851eb851eb8L    # 0.24

    move/from16 v17, v8

    .end local v8    # "columnDeltY":I
    .local v17, "columnDeltY":I
    int-to-double v8, v5

    mul-double/2addr v8, v13

    double-to-int v8, v8

    .line 177
    .local v8, "deltLine":I
    const/16 v9, 0xa

    .line 179
    .local v9, "accountNameLength":I
    const-string v13, "cupid"

    sget-object v14, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 180
    add-int/lit16 v3, v3, 0xb4

    .line 181
    add-int/lit8 v8, v8, -0x32

    .line 185
    :cond_3
    div-int/lit8 v13, v4, 0x2

    int-to-float v13, v13

    div-int/lit8 v14, v5, 0x2

    int-to-float v14, v14

    move/from16 v18, v0

    .end local v0    # "x":I
    .local v18, "x":I
    const/high16 v0, 0x43a50000    # 330.0f

    invoke-virtual {v7, v0, v13, v14}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 188
    const/4 v0, 0x0

    move v13, v9

    move v9, v3

    move/from16 v3, v18

    .line 189
    .end local v18    # "x":I
    .local v0, "i":I
    .local v3, "x":I
    .local v9, "y":I
    .local v13, "accountNameLength":I
    :goto_1
    const/4 v14, 0x4

    if-ge v0, v14, :cond_a

    .line 191
    iget-object v14, v1, Lcom/android/server/wm/MIUIWatermark;->mAccountName:Ljava/lang/String;

    if-eqz v14, :cond_4

    .line 192
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v13

    .line 193
    iget-object v14, v1, Lcom/android/server/wm/MIUIWatermark;->mAccountName:Ljava/lang/String;

    move/from16 v18, v4

    .end local v4    # "dw":I
    .local v18, "dw":I
    int-to-float v4, v3

    move-object/from16 v19, v6

    .end local v6    # "dirty":Landroid/graphics/Rect;
    .local v19, "dirty":Landroid/graphics/Rect;
    int-to-float v6, v9

    move-object/from16 v20, v2

    iget-object v2, v1, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v14, v4, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 194
    sget-object v2, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 195
    sget-object v2, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    sget-object v4, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v4, v13

    mul-int/2addr v4, v10

    div-int/lit8 v4, v4, 0x2

    sub-int v4, v3, v4

    int-to-float v4, v4

    sub-int v6, v9, v11

    int-to-float v6, v6

    iget-object v14, v1, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 197
    .end local v18    # "dw":I
    .end local v19    # "dirty":Landroid/graphics/Rect;
    .restart local v4    # "dw":I
    .restart local v6    # "dirty":Landroid/graphics/Rect;
    :cond_4
    move-object/from16 v20, v2

    move/from16 v18, v4

    move-object/from16 v19, v6

    .end local v4    # "dw":I
    .end local v6    # "dirty":Landroid/graphics/Rect;
    .restart local v18    # "dw":I
    .restart local v19    # "dirty":Landroid/graphics/Rect;
    sget-object v2, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 198
    sget-object v2, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    sget-object v4, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v4, v13

    mul-int/2addr v4, v10

    div-int/lit8 v4, v4, 0x2

    sub-int v4, v3, v4

    int-to-float v4, v4

    sub-int v6, v9, v11

    int-to-float v6, v6

    iget-object v14, v1, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 202
    :cond_5
    :goto_2
    iget-object v2, v1, Lcom/android/server/wm/MIUIWatermark;->mAccountName:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 203
    add-int v4, v3, v12

    int-to-float v4, v4

    add-int v6, v9, v17

    int-to-float v6, v6

    iget-object v14, v1, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 204
    sget-object v2, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 205
    sget-object v2, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    add-int v4, v3, v12

    sget-object v6, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v13

    mul-int/2addr v6, v10

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v4, v6

    int-to-float v4, v4

    add-int v6, v9, v17

    sub-int/2addr v6, v11

    int-to-float v6, v6

    iget-object v14, v1, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 207
    :cond_6
    sget-object v2, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 208
    sget-object v2, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    add-int v4, v3, v12

    sget-object v6, Lcom/android/server/wm/MIUIWatermark;->mIMEI:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v13

    mul-int/2addr v6, v10

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v4, v6

    int-to-float v4, v4

    add-int v6, v9, v17

    sub-int/2addr v6, v11

    int-to-float v6, v6

    iget-object v14, v1, Lcom/android/server/wm/MIUIWatermark;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v4, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 212
    :cond_7
    :goto_3
    if-eqz v5, :cond_9

    if-nez v8, :cond_8

    move v6, v3

    move v14, v10

    move/from16 v21, v11

    move/from16 v22, v12

    move/from16 v23, v13

    goto :goto_4

    .line 216
    :cond_8
    move v2, v10

    move v4, v11

    .end local v10    # "incDeltx":I
    .end local v11    # "incDelty":I
    .local v2, "incDeltx":I
    .local v4, "incDelty":I
    int-to-double v10, v3

    move v14, v2

    move v6, v3

    .end local v2    # "incDeltx":I
    .end local v3    # "x":I
    .local v6, "x":I
    .local v14, "incDeltx":I
    int-to-double v2, v5

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->tan(D)D

    move-result-wide v21

    mul-double v2, v2, v21

    move/from16 v21, v4

    .end local v4    # "incDelty":I
    .local v21, "incDelty":I
    div-int v4, v5, v8

    move/from16 v22, v12

    move/from16 v23, v13

    .end local v12    # "columnDeltX":I
    .end local v13    # "accountNameLength":I
    .local v22, "columnDeltX":I
    .local v23, "accountNameLength":I
    int-to-double v12, v4

    div-double/2addr v2, v12

    sub-double/2addr v10, v2

    double-to-int v3, v10

    .line 217
    .end local v6    # "x":I
    .restart local v3    # "x":I
    add-int/2addr v9, v8

    .line 218
    add-int/lit8 v0, v0, 0x1

    move v10, v14

    move/from16 v4, v18

    move-object/from16 v6, v19

    move-object/from16 v2, v20

    move/from16 v11, v21

    move/from16 v12, v22

    move/from16 v13, v23

    goto/16 :goto_1

    .line 212
    .end local v14    # "incDeltx":I
    .end local v21    # "incDelty":I
    .end local v22    # "columnDeltX":I
    .end local v23    # "accountNameLength":I
    .restart local v10    # "incDeltx":I
    .restart local v11    # "incDelty":I
    .restart local v12    # "columnDeltX":I
    .restart local v13    # "accountNameLength":I
    :cond_9
    move v6, v3

    move v14, v10

    move/from16 v21, v11

    move/from16 v22, v12

    move/from16 v23, v13

    .line 213
    .end local v3    # "x":I
    .end local v10    # "incDeltx":I
    .end local v11    # "incDelty":I
    .end local v12    # "columnDeltX":I
    .end local v13    # "accountNameLength":I
    .restart local v6    # "x":I
    .restart local v14    # "incDeltx":I
    .restart local v21    # "incDelty":I
    .restart local v22    # "columnDeltX":I
    .restart local v23    # "accountNameLength":I
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dh: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "deltLine: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v3, v20

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    move/from16 v13, v23

    goto :goto_5

    .line 189
    .end local v14    # "incDeltx":I
    .end local v18    # "dw":I
    .end local v19    # "dirty":Landroid/graphics/Rect;
    .end local v21    # "incDelty":I
    .end local v22    # "columnDeltX":I
    .end local v23    # "accountNameLength":I
    .restart local v3    # "x":I
    .local v4, "dw":I
    .local v6, "dirty":Landroid/graphics/Rect;
    .restart local v10    # "incDeltx":I
    .restart local v11    # "incDelty":I
    .restart local v12    # "columnDeltX":I
    .restart local v13    # "accountNameLength":I
    :cond_a
    move/from16 v18, v4

    move-object/from16 v19, v6

    move v14, v10

    move/from16 v21, v11

    move/from16 v22, v12

    move v6, v3

    .line 220
    .end local v3    # "x":I
    .end local v4    # "dw":I
    .end local v10    # "incDeltx":I
    .end local v11    # "incDelty":I
    .end local v12    # "columnDeltX":I
    .local v6, "x":I
    .restart local v14    # "incDeltx":I
    .restart local v18    # "dw":I
    .restart local v19    # "dirty":Landroid/graphics/Rect;
    .restart local v21    # "incDelty":I
    .restart local v22    # "columnDeltX":I
    :goto_5
    iget-object v2, v1, Lcom/android/server/wm/MIUIWatermark;->mSurface:Landroid/view/Surface;

    invoke-virtual {v2, v7}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 221
    return-void

    .line 143
    .end local v0    # "i":I
    .end local v5    # "dh":I
    .end local v6    # "x":I
    .end local v7    # "c":Landroid/graphics/Canvas;
    .end local v8    # "deltLine":I
    .end local v9    # "y":I
    .end local v13    # "accountNameLength":I
    .end local v14    # "incDeltx":I
    .end local v15    # "radians":D
    .end local v17    # "columnDeltY":I
    .end local v18    # "dw":I
    .end local v19    # "dirty":Landroid/graphics/Rect;
    .end local v21    # "incDelty":I
    .end local v22    # "columnDeltX":I
    :cond_b
    :goto_6
    return-void
.end method

.method public declared-synchronized init(Lcom/android/server/wm/WindowManagerService;Ljava/lang/String;)V
    .locals 2
    .param p1, "wms"    # Lcom/android/server/wm/WindowManagerService;
    .param p2, "msg"    # Ljava/lang/String;

    monitor-enter p0

    .line 295
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    .line 296
    monitor-exit p0

    return-void

    .line 299
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/android/server/wm/MIUIWatermark;->mWmService:Lcom/android/server/wm/WindowManagerService;

    .line 300
    iget-object v0, p1, Lcom/android/server/wm/WindowManagerService;->mTransactionFactory:Ljava/util/function/Supplier;

    invoke-interface {v0}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceControl$Transaction;

    iput-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    .line 302
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mCallback:Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    instance-of v0, v0, Lcom/android/server/policy/MiuiPhoneWindowManager;

    if-eqz v0, :cond_1

    .line 303
    new-instance v0, Lcom/android/server/wm/MIUIWatermark$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/wm/MIUIWatermark$2;-><init>(Lcom/android/server/wm/MIUIWatermark;Lcom/android/server/wm/WindowManagerService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mCallback:Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    .line 320
    iget-object v0, p1, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    check-cast v0, Lcom/android/server/policy/MiuiPhoneWindowManager;

    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mCallback:Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiPhoneWindowManager;->registerMIUIWatermarkCallback(Lcom/android/server/policy/MiuiPhoneWindowManager$MIUIWatermarkCallback;)V

    .line 322
    .end local p0    # "this":Lcom/android/server/wm/MIUIWatermark;
    :cond_1
    invoke-direct {p0, p2}, Lcom/android/server/wm/MIUIWatermark;->createSurfaceLocked(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 323
    monitor-exit p0

    return-void

    .line 294
    .end local p1    # "wms":Lcom/android/server/wm/WindowManagerService;
    .end local p2    # "msg":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public positionSurface(II)V
    .locals 2
    .param p1, "dw"    # I
    .param p2, "dh"    # I

    .line 107
    iget-object v0, p0, Lcom/android/server/wm/MIUIWatermark;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDW:I

    if-ne v1, p1, :cond_0

    iget v1, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDH:I

    if-eq v1, p2, :cond_1

    .line 108
    :cond_0
    iput p1, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDW:I

    .line 109
    iput p2, p0, Lcom/android/server/wm/MIUIWatermark;->mLastDH:I

    .line 110
    iget-object v1, p0, Lcom/android/server/wm/MIUIWatermark;->mTransaction:Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v1, v0, p1, p2}, Landroid/view/SurfaceControl$Transaction;->setBufferSize(Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MIUIWatermark;->mDrawNeeded:Z

    .line 113
    :cond_1
    return-void
.end method
